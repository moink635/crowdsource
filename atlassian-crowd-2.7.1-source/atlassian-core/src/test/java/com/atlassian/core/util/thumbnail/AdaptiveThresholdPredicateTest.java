package com.atlassian.core.util.thumbnail;

import junit.framework.TestCase;

public class AdaptiveThresholdPredicateTest extends TestCase
{
    private static final long KB = 1024L;
    private static final long MB = KB * 1024;
    private static final long GB = MB * 1024;

    public void testBasicFunctionality()
    {
        AdaptiveThresholdPredicate p = withFreeMemory(KB);
        assertFalse(p.apply(new Dimensions(100, 100))); // 40,000 b
        assertFalse(p.apply(new Dimensions(10, 100))); // 4,000 b
        assertFalse(p.apply(new Dimensions(10, 25))); // 1000 b
        assertTrue(p.apply(new Dimensions(10, 10))); // 400 b
        assertTrue(p.apply(new Dimensions(4, 10))); // 160 b

        p = withFreeMemory(Math.round(1.2 * GB));
        assertFalse(p.apply(new Dimensions(20000, 20000)));

        assertTrue(p.apply(new Dimensions(100, 100))); // 40,000 b
        assertTrue(p.apply(new Dimensions(10, 100))); // 4,000 b
        assertTrue(p.apply(new Dimensions(10, 25))); // 1000 b
    }

    private AdaptiveThresholdPredicate withFreeMemory(final long freeMemory)
    {
        return new AdaptiveThresholdPredicate()
        {
            @Override
            long freeMemory()
            {
                return freeMemory;
            }
        };
    }
}