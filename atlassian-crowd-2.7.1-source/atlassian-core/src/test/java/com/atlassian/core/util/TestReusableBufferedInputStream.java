package com.atlassian.core.util;

import junit.framework.TestCase;
import org.junit.Test;

import javax.imageio.ImageIO;
import java.io.IOException;
import java.io.InputStream;

public class TestReusableBufferedInputStream extends TestCase
{
    public void testReusableInputStreamIsReusable() throws Exception
    {
        InputStream inputStream = this.getClass().getResourceAsStream("/image.gif");
        InputStream reusableInputStream = new ReusableBufferedInputStream(inputStream);

        ImageIO.read(reusableInputStream);
        assertTrue("Expect end of the input stream after reading from it", reusableInputStream.available() == 0);

        reusableInputStream.close();
        assertTrue("Expect to be at the start of the reusable input stream after calling close()", reusableInputStream.available() > 0);
    }

    @Test(expected = IOException.class)
    public void testReusableInputStreamIsClosable() throws Exception
    {
        InputStream inputStream = this.getClass().getResourceAsStream("/image.gif");
        ReusableBufferedInputStream reusableInputStream = new ReusableBufferedInputStream(inputStream);

        reusableInputStream.destroy();
        ImageIO.read(reusableInputStream);
    }
}
