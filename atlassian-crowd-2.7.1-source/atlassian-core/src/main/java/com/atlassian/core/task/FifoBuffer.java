package com.atlassian.core.task;

import java.util.Collection;

public interface FifoBuffer<T>
{
    /**
     * Get the oldest object from the buffer
     * @return the oldest Object, or null if the queue is empty
     */
    T remove();

    /**
     * Add an Object to the buffer
     * @param o the Object to add
     */
    void add(T o);

    /**
     * The number of buffer in the queue
     */
    int size();

    /**
     * The buffer in the queue
     */
    Collection<T> getItems();

    /**
     * Clear all the objects from the buffer
     */
    void clear();
}