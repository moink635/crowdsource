package com.atlassian.core.util.thumbnail.loader;

import com.google.common.base.Optional;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

public interface ImageLoader {

    /**
     * Loads entire image into memory and creates BufferedImage instance.
     *
     */
    Optional<BufferedImage> loadImage(InputStream inputStream) throws IOException;

}
