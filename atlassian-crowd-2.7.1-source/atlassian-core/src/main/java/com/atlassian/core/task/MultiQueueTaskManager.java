package com.atlassian.core.task;

import java.util.Map;

public interface MultiQueueTaskManager
{
    TaskQueue getTaskQueue(String queueName);

    void addTaskQueue(String queueName, TaskQueue taskQueue);

    TaskQueue removeTaskQueue(String queueName, TaskQueue taskQueue, boolean flush);

    void setTaskQueues(Map<String, TaskQueue> queues);

    void addTask(String queueName, Task task);

    void flush(String queueName);

    void flush();
}
