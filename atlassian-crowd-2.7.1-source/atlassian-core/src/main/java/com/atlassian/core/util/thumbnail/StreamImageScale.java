package com.atlassian.core.util.thumbnail;

import javax.imageio.ImageIO;
import javax.imageio.ImageReadParam;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public class StreamImageScale {

    private final Thumber thumber;

    public StreamImageScale(final Thumber thumber) {
        this.thumber = thumber;
    }

    public BufferedImage renderThumbnail(final InputStream inputStream, int maxWidth, int maxHeight) throws IOException
    {
        ImageInputStream iis = ImageIO.createImageInputStream(inputStream);
        return scaleDown(iis, maxWidth, maxHeight);
    }

    private BufferedImage scaleDown(ImageInputStream inputStream, int maxWidth, int maxHeight) throws IOException
    {
        final Iterator<ImageReader> readers = ImageIO.getImageReaders(inputStream);
        if (!readers.hasNext())
        {
            throw new IOException("Cannot read the image");
        }
        // Use the first reader
        ImageReader reader = readers.next();
        ImageReadParam param = reader.getDefaultReadParam();
        reader.setInput(inputStream);

        final Thumber.WidthHeightHelper wh = thumber.determineScaleSize(maxWidth, maxHeight, reader.getWidth(0), reader.getHeight(0));
        int ratio = (int) Math.floor(((double) reader.getWidth(0) / (double) wh.getWidth()));
        param.setSourceSubsampling(ratio, ratio, 0, 0);

        final BufferedImage scaledImage = reader.read(0, param);
        //Subsampling does not return an exact size of an image - we can force it by cropping an image
        return scaledImage.getSubimage(0, 0,
                Math.min(wh.getWidth(),scaledImage.getWidth()),
                Math.min(wh.getHeight(), scaledImage.getHeight()));
    }
}
