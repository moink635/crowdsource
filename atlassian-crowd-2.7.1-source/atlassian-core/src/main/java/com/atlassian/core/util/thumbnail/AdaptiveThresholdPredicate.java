package com.atlassian.core.util.thumbnail;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicate;
import org.apache.log4j.Logger;

import javax.annotation.Nullable;

public class AdaptiveThresholdPredicate implements Predicate<Dimensions>
{
    private final static Logger log = Logger.getLogger(AdaptiveThresholdPredicate.class);

    private static final int BYTES_PER_PIXEL = 4;
    private static final float PROCESSING_HEADROOM = 1.2F; // We need a bit of headroom for processing, still pulling numbers out of thin air.

    @Override
    public boolean apply(@Nullable final Dimensions input)
    {
        return sufficientMemory(input);
    }

    private boolean sufficientMemory(final Dimensions dimensions)
    {
        long requiredMemory = dimensions.getHeight() * dimensions.getWidth() * BYTES_PER_PIXEL;
        long freeMemory = freeMemory();
        boolean result = requiredMemory * PROCESSING_HEADROOM < freeMemory;
        if (log.isDebugEnabled())
        {
            log.debug(String.format("Expected memory required for this image conversion: %d. " +
                    "Free memory: %d. Image dimension (%s) renderable: %s", requiredMemory, freeMemory, dimensions, result));
        }
        return result;
    }

    @VisibleForTesting
    long freeMemory()
    {
        return Runtime.getRuntime().freeMemory();
    }
}