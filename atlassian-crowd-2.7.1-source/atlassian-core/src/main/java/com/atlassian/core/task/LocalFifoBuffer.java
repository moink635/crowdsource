package com.atlassian.core.task;

import org.apache.commons.collections.Buffer;
import org.apache.commons.collections.BufferUtils;
import org.apache.commons.collections.buffer.UnboundedFifoBuffer;

import java.util.Collection;

public class LocalFifoBuffer<T> implements FifoBuffer<T>
{
    private final Buffer buffer = BufferUtils.synchronizedBuffer(new UnboundedFifoBuffer());

    public synchronized T remove()
    {
        if (!buffer.isEmpty())
            return (T) buffer.remove();
        else
            return null;
    }

    public void add(T o)
    {
        buffer.add(o);
    }

    public int size()
    {
        return buffer.size();
    }

    public Collection<T> getItems()
    {
        return buffer;
    }

    public void clear()
    {
        buffer.clear();
    }
}