package com.atlassian.core.util.thumbnail;

import com.atlassian.core.util.ReusableBufferedInputStream;
import com.google.common.base.Predicate;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;

public class ThumbnailRenderer {

    private final static Logger log = Logger.getLogger(ThumbnailRenderer.class);

    private final Predicate<Dimensions> rasterBasedRenderingThreshold;
    private final MemoryImageScale memoryImageScale;
    private final StreamImageScale streamImageScale;

    public ThumbnailRenderer(final Predicate<Dimensions> rasterBasedRenderingThreshold, final Thumber thumber) {
        this.rasterBasedRenderingThreshold = rasterBasedRenderingThreshold;
        this.memoryImageScale = new MemoryImageScale(thumber);
        this.streamImageScale = new StreamImageScale(thumber);
    }

    /**
     * Create a {@link Thumbnail} from the input stream. Thumbnails will always be PNG thumbnails!
     *
     * @param inputStream   - The stream that contains the image data
     * @param maxWidth      - The maximum width of the thumbnail - this renderer maintains the aspect ratio of the original image
     * @param maxHeight     - The maximum height of the thumbnail - this renderer maintains the aspect ratio of the original image
     * @return Thumbnail that is at most {@code maxWidth}x{@code maxHeight}, never returns null
     */
    public BufferedImage createThumbnailImage(final InputStream inputStream, final int maxWidth, final int maxHeight)
    {
        ReusableBufferedInputStream reusableInputStream = new ReusableBufferedInputStream(inputStream);

        try
        {
            final Dimensions originalImageDimensions = dimensionsForImage(reusableInputStream);
            if (!rasterBasedRenderingThreshold.apply(originalImageDimensions))
            {
                log.debug(String.format("Image dimensions (%s) exceed the threshold for raster based image manipulation. Using stream based renderer.", originalImageDimensions));
                try
                {
                    return streamImageScale.renderThumbnail(reusableInputStream, maxWidth, maxHeight);
                }
                catch (IOException e)
                {
                    throw new ThumbnailRenderException(e);
                }
            }
            try
            {
                return memoryImageScale.scaleImage(reusableInputStream, maxWidth, maxHeight);
            }
            catch (IOException e) {
                throw new ThumbnailRenderException(e);
            }
        }
        finally
        {
            IOUtils.closeQuietly(inputStream);
            try
            {
                reusableInputStream.destroy();
            }
            catch (IOException e)
            {
                // Do nothing
            }
        }
    }

    private static Dimensions dimensionsForImage(final InputStream inputStream)
    {
        final BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
        try
        {
            final ImageInputStream imageInputStream = ImageIO.createImageInputStream(bufferedInputStream);
            Iterator<ImageReader> readers = ImageIO.getImageReaders(imageInputStream);
            if (!readers.hasNext())
            {
                throw new IOException("There is no ImageReader available for the given ImageInputStream");
            }
            // Use the first one available
            ImageReader reader = readers.next();
            reader.setInput(imageInputStream);
            return new Dimensions(reader.getWidth(0), reader.getHeight(0));
        }
        catch (IOException e)
        {
            throw new ThumbnailRenderException(e);
        }
        finally
        {
            IOUtils.closeQuietly(bufferedInputStream);
        }
    }
}
