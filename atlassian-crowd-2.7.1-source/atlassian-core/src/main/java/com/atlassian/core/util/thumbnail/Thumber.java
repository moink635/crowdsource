package com.atlassian.core.util.thumbnail;

import com.atlassian.core.exception.FailedPredicateException;
import com.atlassian.core.util.ImageInfo;
import com.atlassian.core.util.ReusableBufferedInputStream;
import com.atlassian.core.util.thumbnail.loader.ImageFactory;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.j3d.util.ImageGenerator;

import javax.annotation.Nonnull;
import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.swing.*;
import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * A class to create and retrieve thumbnail of images.
 */
public class Thumber
{
    private static final Logger log = Logger.getLogger(Thumber.class);
    private final ImageInfo imageInfo = new ImageInfo();

    // According to RFC 2045, mime types are not case sensitive. You can't just use contains. You MUST use equalsIgnoreCase.
    public static final List<String> THUMBNAIL_MIME_TYPES = Collections.unmodifiableList(Arrays.asList(ImageIO.getReaderMIMETypes()));
    public static final List<String> THUMBNAIL_FORMATS = Collections.unmodifiableList(Arrays.asList(ImageIO.getReaderFormatNames()));
    private Thumbnail.MimeType mimeType;

    /**
     * Legacy compatible behaviour, all thumnails generated will be of type
     * {@link com.atlassian.core.util.thumbnail.Thumbnail.MimeType#JPG} which does not support transparency.
     */
    public Thumber()
    {
        this(Thumbnail.MimeType.JPG);
    }

    /**
     * Thumbnails will be generated of the given type and, if the type permits it (PNG), preserve transparency.
     *
     * @param mimeType the type of all thumbnails generated and retrieved.
     */
    public Thumber(Thumbnail.MimeType mimeType)
    {
        if (mimeType == null)
        {
            throw new IllegalArgumentException("mimeType cannot be null");
        }
        this.mimeType = mimeType;
    }

    // According to RFC 2045, mime types are not case sensitive. You can't just use contains. You MUST use equalsIgnoreCase.
    public static List<String> getThumbnailMimeTypes()
    {
        return THUMBNAIL_MIME_TYPES;
    }

    public static List<String> getThumbnailFormats()
    {
        return THUMBNAIL_FORMATS;
    }

    private float encodingQuality = 0.80f; // default to 0.80f, seems reasonable enough, and still provides good result.

    /**
     * @return True if the AWT default toolkit exists, false (with an error logged) if it does not.
     */
    public boolean checkToolkit()
    {
        try
        {
            Toolkit.getDefaultToolkit();
        }
        catch (Throwable e)
        {
            log.error("Unable to acquire AWT default toolkit - thumbnails will not be displayed. Check DISPLAY variable or use setting -Djava.awt.headless=true.", e);
            return false;
        }
        return true;
    }

    /**
     * Retrieves an existing thumbnail, or creates a new one.
     *
     * @param originalFile  The file which is being thumbnailed.
     * @param thumbnailFile The location of the existing thumbnail (if it exists), or the location to create a new
     *                      thumbnail.
     * @param maxWidth      The max width of the thumbnail.
     * @param maxHeight     The max height of the thumbnail.
     */
    public Thumbnail retrieveOrCreateThumbNail(File originalFile, File thumbnailFile, int maxWidth, int maxHeight, long thumbnailId)
            throws MalformedURLException
    {
        return retrieveOrCreateThumbNail(originalFile, thumbnailFile, maxWidth, maxHeight, thumbnailId, new AdaptiveThresholdPredicate());
    }

    /**
     * Retrieves an existing thumbnail, or creates a new one.
     * Creating of Thumbnail image will be processed in memory if specified rasterBasedRenderingPredicate is match
     *
     * @param originalFile  The file which is being thumbnailed.
     * @param thumbnailFile The location of the existing thumbnail (if it exists), or the location to create a new
     *                      thumbnail.
     * @param maxWidth      The max width of the thumbnail.
     * @param maxHeight     The max height of the thumbnail.
     */
    public Thumbnail retrieveOrCreateThumbNail(final File originalFile, final File thumbnailFile, final int maxWidth,
                                               final int maxHeight, final long thumbnailId, Predicate<Dimensions> rasterBasedRenderingPredicate) throws MalformedURLException {
        FileInputStream originalFileStream = null;
        try
        {
            originalFileStream = new FileInputStream(originalFile);
            return retrieveOrCreateThumbNail(originalFileStream, originalFile.getName(), thumbnailFile, maxWidth, maxHeight, thumbnailId, rasterBasedRenderingPredicate);
        }
        catch (FileNotFoundException e)
        {
            log.error("Unable to create thumbnail: file not found: " + originalFile.getAbsolutePath());
        }
        finally
        {
            IOUtils.closeQuietly(originalFileStream);
        }

        return null;
    }

    /**
     * Store Image in format defined by constructor
     * @param scaledImage
     * @param file
     * @throws FileNotFoundException
     */
    public void storeImage(BufferedImage scaledImage, File file) throws FileNotFoundException
    {
        if (scaledImage == null)
        {
            log.warn("Can't store a null scaledImage.");
            return;
        }
        checkOutputFileCreation(file);
        if(mimeType == Thumbnail.MimeType.JPG)
        {
            storeJPEG(scaledImage, file);
        }
        else
        {
            storeImageAsPng(scaledImage, file);
        }
    }

    private void checkOutputFileCreation(final File outputFile)
    {
        try
        {
            FileUtils.touch(outputFile);
            FileUtils.deleteQuietly(outputFile);
        }
        catch (IOException ioe)
        {
            throw new ThumbnailRenderException(ioe);
        }
    }

    private void storeJPEG(final BufferedImage scaledImage, final File file) {
        FileImageOutputStream fout = null;
        ImageWriter writer = null;
        try
        {
            fout = new FileImageOutputStream((file));

            writer = ImageIO.getImageWritersByFormatName("jpeg").next();

            ImageWriteParam param = writer.getDefaultWriteParam();
            param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
            param.setCompressionQuality(encodingQuality);
            writer.setOutput(fout);
            writer.write(null, new IIOImage(scaledImage, null, null), param);
        }
        catch (IOException e)
        {
            log.error("Error encoding the thumbnail image to JPEG", e);
        }
        finally
        {
            if (writer != null)
            {
                writer.dispose();
            }
            try
            {
                 if(fout != null)
                 {
                     fout.close();
                 }
            }
            catch (IOException ignore)
            {

            }
        }
    }

    private void storeImageAsPng(BufferedImage image, File file) throws FileNotFoundException
    {
        try
        {
            ImageIO.write(image, "png", file);
        }
        catch (IOException e)
        {
            log.error("Error encoding the thumbnail image to PNG", e);
        }
    }

    /**
     * This method should take BufferedImage argument, but takes just Image for backward compatibility (so that the
     * client code can stay intact). Normally anyway a BufferedImage instance will be provided and the image will be
     * directly processed without transforming it to BufferedImage first.
     *
     * @param imageToScale  image to scale (BufferedImage is welcome, other image types will be transformed to
     *                      BufferedImage first)
     * @param newDimensions desired max. dimensions
     * @return scaled image
     */
    public BufferedImage scaleImage(Image imageToScale, WidthHeightHelper newDimensions)
    {
        return MemoryImageScale.scaleImage(Pictures.toBufferedImage(imageToScale), newDimensions);
    }


    /**
     * This method provides extremely slow way to scale your image. There are generally much better alternatives (order
     * of magnitude faster with similar quality).
     * Consider using {@link Thumber#scaleImage(java.awt.Image, com.atlassian.core.util.thumbnail.Thumber.WidthHeightHelper)} instead
     *
     * @param imageToScale  input image
     * @param newDimensions desired max. dimensions (the ratio will be kept)
     * @return scaled image
     */
    @Deprecated
    public BufferedImage scaleImageOld(Image imageToScale, WidthHeightHelper newDimensions)
    {


        // If the original image is an instance of BufferedImage, we need to make sure
        // that it is an sRGB image. If it is not, we need to convert it before scaling it
        // as we run into these issue otherwise:
        // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4886071
        // http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4705399

        if (imageToScale instanceof BufferedImage)
        {
            BufferedImage bufferedImage = (BufferedImage) imageToScale;
            if (!bufferedImage.getColorModel().getColorSpace().isCS_sRGB())
            {
                BufferedImage sRGBImage = new BufferedImage(bufferedImage.getWidth(), bufferedImage.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics g = sRGBImage.getGraphics();
                g.drawImage(bufferedImage, 0, 0, null);
                g.dispose();
                imageToScale = sRGBImage;
            }
        }

        AreaAveragingScaleFilter scaleFilter =
                new AreaAveragingScaleFilter(newDimensions.getWidth(), newDimensions.getHeight());
        ImageProducer producer = new FilteredImageSource(imageToScale.getSource(),
                scaleFilter);

        ImageGenerator generator = new ImageGenerator();
        producer.startProduction(generator);
        BufferedImage scaled = generator.getImage();

        // CORE-101 getImage may return null
        if (scaled == null)
        {
            log.warn("Unabled to create scaled image.");
        }
        else
        {
            scaled.flush();
        }

        return scaled;
    }

    /**
     * Retrieves an existing thumbnail, or creates a new one.
     *
     * @param originalFileStream  The image stream
     * @param fileName            Filename for specified image
     * @param thumbnailFile       The location of the existing thumbnail (if it exists), or the location to create a new
     *                            thumbnail.
     * @param maxWidth            The max width of the thumbnail.
     * @param maxHeight           The max height of the thumbnail.
     */
    public Thumbnail retrieveOrCreateThumbNail(InputStream originalFileStream, String fileName, File thumbnailFile, int maxWidth, int maxHeight, long thumbnailId)
            throws MalformedURLException
    {
        return retrieveOrCreateThumbNail(originalFileStream, fileName, thumbnailFile, maxWidth, maxHeight, thumbnailId, new AdaptiveThresholdPredicate());
    }

    /**
     * Retrieves an existing thumbnail, or creates a new one.
     * Creating of Thumbnail image will be processed in memory if specified rasterBasedRenderingPredicate is match
     *
     * @param originalFileStream  The image stream
     * @param fileName            Filename for specified image
     * @param thumbnailFile       The location of the existing thumbnail (if it exists), or the location to create a new
     *                            thumbnail.
     * @param maxWidth            The max width of the thumbnail.
     * @param maxHeight           The max height of the thumbnail.
     */

    public Thumbnail retrieveOrCreateThumbNail(final InputStream originalFileStream, final String fileName, final File thumbnailFile,
                                                final int maxWidth, final int maxHeight, final long thumbnailId,
                                                Predicate<Dimensions> rasterBasedRenderingThreshold) {
        Thumbnail thumbnail = null;
        try
        {
            thumbnail = getThumbnail(thumbnailFile, fileName, thumbnailId);
        }
        catch (IOException e)
        {
            log.error("Unable to get thumbnail image for id " + thumbnailId, e);
            return null;
        }

        if (thumbnail == null)
        {
            try
            {
                thumbnail = createThumbnail(originalFileStream, thumbnailFile, maxWidth, maxHeight, thumbnailId, fileName, rasterBasedRenderingThreshold);
            }
            catch (ThumbnailRenderException e)
            {
                log.error("Unable to create thumbnail image for id " + thumbnailId, e);
                return null;
            }
            catch (IOException e)
            {
                log.error("Unable to create thumbnail image for id " + thumbnailId, e);
                return null;
            }
        }

        return thumbnail;
    }

    // PRIVATE METHODS -------------------------------------------------------------------------------------------
    private BufferedImage scaleImage(Image originalImage, int maxWidth, int maxHeight)
    {
        return scaleImage(originalImage, determineScaleSize(maxWidth, maxHeight, originalImage));
    }

    private WidthHeightHelper determineScaleSize(int maxWidth, int maxHeight, Image image)
    {
        return determineScaleSize(maxWidth, maxHeight, image.getWidth(null), image.getHeight(null));
    }

    private Thumbnail createThumbnail(InputStream inputStream, File thumbnailFile, int maxWidth, int maxHeight, long thumbId, String fileName, Predicate<Dimensions> rasterBasedRenderingThreshold)
            throws FileNotFoundException
    {
        final ThumbnailRenderer thumbnailRenderer = new ThumbnailRenderer(rasterBasedRenderingThreshold, this);
        final BufferedImage thumbnailImage = thumbnailRenderer.createThumbnailImage(inputStream, maxWidth, maxHeight);

        final int height = thumbnailImage.getHeight();
        final int width = thumbnailImage.getWidth();

        storeImage(thumbnailImage, thumbnailFile);

        return new Thumbnail(height, width, fileName, thumbId, mimeType);
    }


    private Thumbnail getThumbnail(File thumbnailFile, String filename, long thumbId) throws IOException
    {
        if (thumbnailFile.exists())
        {
            final Image thumbImage = getImage(thumbnailFile);
            return new Thumbnail(thumbImage.getHeight(null), thumbImage.getWidth(null), filename, thumbId, mimeType);
        }
        return null;
    }

    /**
     * @deprecated since 4.6.3.
     *
     * Use {@link Thumber#getImage(java.io.File, com.google.common.base.Predicate)} instead so that you can supply a predicate
     * to (for example) check that the image has reasonable dimensions that are unlikely to cause an {@link OutOfMemoryError}
     *
     * @return An Image object or null if there was no suitable ImageReader for the given data
     */
    public Image getImage(File file) throws IOException
    {
        return getImage(file, Predicates.<ReusableBufferedInputStream>alwaysTrue());
    }

    /**
     * Retrieves an image for the specified file if the supplied predicate is met.
     *
     * @param file  File to read an image from
     * @param predicate Predicate that needs to be met before attempting to read an image from the file (e.g. image
     *                  dimensions that shouldn't be exceeded)
     * @return An Image object or null if there was no suitable ImageReader for the given data
     * @throws FailedPredicateException If predicate was supplied but not met
     */
    public BufferedImage getImage(File file, @Nonnull Predicate<ReusableBufferedInputStream> predicate) throws IOException
    {
        ReusableBufferedInputStream reusableInputStream = new ReusableBufferedInputStream(new FileInputStream(file));

        try
        {
            return getImage(reusableInputStream, predicate);
        }
        finally
        {
            reusableInputStream.destroy();
        }
    }

    /**
     * @deprecated since 4.6.3.
     *
     * Use {@link Thumber#getImage(com.atlassian.core.util.ReusableBufferedInputStream, com.google.common.base.Predicate)}
     * instead so that you can supply a predicate to (for example) check that the image has reasonable dimensions that are
     * unlikely to cause an {@link OutOfMemoryError}
     *
     * @return An Image object or null if there was no suitable ImageReader for the given data
     */
    public BufferedImage getImage(InputStream is) throws IOException
    {
        ReusableBufferedInputStream reusableInputStream = new ReusableBufferedInputStream(is);

        try
        {
            return getImage(reusableInputStream, Predicates.<ReusableBufferedInputStream>alwaysTrue());
        }
        finally
        {
            reusableInputStream.destroy();
        }
    }

    /**
     * Reads an image from the specified input stream if the supplied predicate is met.
     * @param is Input stream to read an image from
     * @param predicate Predicate that needs to be met before attempting to read an image from the file (e.g. image
     *                  dimensions that shouldn't be exceeded)
     * @return An Image object or null if there was no suitable ImageReader for the given data
     * @throws FailedPredicateException If predicate was supplied but not met
     */
    public BufferedImage getImage(ReusableBufferedInputStream is, @Nonnull Predicate<ReusableBufferedInputStream> predicate) throws IOException
    {
        if (!predicate.apply(is))
        {
            throw new FailedPredicateException();
        }

        return ImageFactory.loadImage(is);
    }

    /**
     * Set the default encoding quality used by the thumber to encode jpegs.
     */
    public void setEncodingQuality(float f)
    {
        if (f > 1.0f || f < 0.0f)
        {
            throw new IllegalArgumentException("Invalid quality setting '" + f + "', value must be between 0 and 1. ");
        }
        encodingQuality = f;
    }

    public WidthHeightHelper determineScaleSize(int maxWidth, int maxHeight, int imageWidth, int imageHeight)
    {
        if (maxHeight > imageHeight && maxWidth > imageWidth)
        {
            return new Thumber.WidthHeightHelper(imageWidth, imageHeight);
        }
        // Determine scale size.
        // Retain original image proportions with scaled image.
        double thumbRatio = (double) maxWidth / (double) maxHeight;

        double imageRatio = (double) imageWidth / (double) imageHeight;

        if (thumbRatio < imageRatio)
        {
            return new Thumber.WidthHeightHelper(maxWidth, (int) Math.max(1, maxWidth / imageRatio));
        }
        else
        {
            return new Thumber.WidthHeightHelper((int) Math.max(1, maxHeight * imageRatio), maxHeight);
        }
    }

    public boolean isFileSupportedImage(File file)
    {
        try
        {
            return isFileSupportedImage(new FileInputStream(file));
        }
        catch (FileNotFoundException e)
        {
            return false;
        }
    }

    public boolean isFileSupportedImage(InputStream inputStream)
    {
        try
        {
            imageInfo.setInput(inputStream);
            imageInfo.check();
            for (String format : THUMBNAIL_FORMATS)
            {
                if (format.equalsIgnoreCase(imageInfo.getFormatName()))
                {
                    return true;
                }
            }
            return false;
        }
        finally
        {
            try
            {
                if (inputStream != null)
                {
                    inputStream.close();
                }
            }
            catch (Exception e)
            {
                log.error(e, e);
            }
        }
    }

    public static class WidthHeightHelper
    {
        private int width;
        private int height;

        public WidthHeightHelper(int width, int height)
        {
            this.width = width;
            this.height = height;
        }

        public int getWidth()
        {
            return width;
        }

        public void setWidth(int width)
        {
            this.width = width;
        }

        public int getHeight()
        {
            return height;
        }

        public void setHeight(int height)
        {
            this.height = height;
        }
    }

    /**
     * Code based on http://www.dreamincode.net/code/snippet1076.htm Looks like public domain
     * The change: I don't use screen-compatible image creation - I assume JIRA runs in headless mode anyway
     */
    static class Pictures
    {
        public static BufferedImage toBufferedImage(Image image)
        {
            if (image instanceof BufferedImage)
            {
                return (BufferedImage) image;
            }

            // This code ensures that all the pixels in the image are loaded
            image = new ImageIcon(image).getImage();

            // Determine if the image has transparent pixels
            boolean hasAlpha = hasAlpha(image);

            // Create a buffered image using the default color model
            int type = hasAlpha ? BufferedImage.TYPE_INT_ARGB : BufferedImage.TYPE_INT_RGB;
            BufferedImage bimage = new BufferedImage(image.getWidth(null), image.getHeight(null), type);

            // Copy image to buffered image
            Graphics g = bimage.createGraphics();

            // Paint the image onto the buffered image
            g.drawImage(image, 0, 0, null);
            g.dispose();

            return bimage;
        }

        public static boolean hasAlpha(Image image)
        {
            // If buffered image, the color model is readily available
            if (image instanceof BufferedImage)
            {
                return ((BufferedImage) image).getColorModel().hasAlpha();
            }

            // Use a pixel grabber to retrieve the image's color model;
            // grabbing a single pixel is usually sufficient
            PixelGrabber pg = new PixelGrabber(image, 0, 0, 1, 1, false);
            try
            {
                pg.grabPixels();
            }
            catch (InterruptedException ignored)
            {
            }

            // Get the image's color model
            return pg.getColorModel().hasAlpha();
        }
    }

}
