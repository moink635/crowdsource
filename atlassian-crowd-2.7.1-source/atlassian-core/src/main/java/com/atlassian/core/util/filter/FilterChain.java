package com.atlassian.core.util.filter;

import java.util.List;
import java.util.ArrayList;

/**
 * Composite Filter that applies a series of filters in turn.
 *
 * By default, the composite filter lets all objects through, so 
 * a composite filter with no filters added,
 *
 * @deprecated use the utility methods on Guava's {@code Predicates} to combine {@code Predicate}
 */
@Deprecated
public class FilterChain<T> implements Filter<T>
{
    private final List<Filter<T>> filters = new ArrayList<Filter<T>>();

    public FilterChain() {}

    public void addFilter(Filter<T> filter)
    {
        filters.add(filter);
    }

    public boolean isIncluded(T o)
    {
        for (Filter<T> filter : filters)
        {
            if (!filter.isIncluded(o))
                return false;
        }

        return true;
    }
}
