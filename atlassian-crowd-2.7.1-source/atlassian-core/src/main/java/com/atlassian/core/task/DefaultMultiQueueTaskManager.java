package com.atlassian.core.task;

import java.util.*;

/**
 * A TaskManager implementation that manages multiple queues
 */
public class DefaultMultiQueueTaskManager implements MultiQueueTaskManager
{
    private final Map<String, TaskQueue> queues = new HashMap<String, TaskQueue>();

    public DefaultMultiQueueTaskManager(String queueName, TaskQueue queue)
    {
        addTaskQueue(queueName, queue);
    }

    public DefaultMultiQueueTaskManager(Map<String, TaskQueue> queues)
    {
        setTaskQueues(queues);
    }

    public void addTaskQueue(String name, TaskQueue queue)
    {
        if(queues.keySet().contains(name))
        {
            throw new IllegalArgumentException("The queue specified already exists in the task manager");
        }
        queues.put(name, queue);
    }

    public TaskQueue removeTaskQueue(String queueName, TaskQueue taskQueue, boolean flush)
    {
        TaskQueue queue = getTaskQueue(queueName);
        if(queue!=null && flush)
        {
            queue.flush();
        }
        return queue;
    }

    public TaskQueue getTaskQueue(String name)
    {
        return queues.get(name);
    }

    public void setTaskQueues(Map<String, TaskQueue> queues)
    {
        for (Map.Entry<String, TaskQueue> entry : queues.entrySet())
        {
            addTaskQueue(entry.getKey(), entry.getValue());
        }
    }

    public void addTask(String queueName, Task task)
    {
        getTaskQueue(queueName).addTask(task);
    }

    public void flush(String queueName)
    {
        getTaskQueue(queueName).flush();
    }

    public void flush()
    {
        for (TaskQueue taskQueue : queues.values())
        {
            taskQueue.flush();
        }
    }
}
