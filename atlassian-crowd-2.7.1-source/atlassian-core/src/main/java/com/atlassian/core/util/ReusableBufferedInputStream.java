package com.atlassian.core.util;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Decorated subclass of {@link BufferedInputStream} that can be read multiple times as it marks the stream
 * with {@link Integer#MAX_VALUE} and resets the stream when {@code close()} is called.
 */
public class ReusableBufferedInputStream extends BufferedInputStream
{
    public ReusableBufferedInputStream(InputStream inputStream)
    {
        super(inputStream);
        super.mark(Integer.MAX_VALUE);
    }

    /**
     * Calls {@link java.io.BufferedInputStream#reset()} instead of closing the stream.
     * @throws IOException
     */
    @Override
    public void close() throws IOException
    {
        super.reset();
    }

    /**
     * Actually closes the ReusableBufferedInputStream by calling {@link java.io.BufferedInputStream#close()}.
     * @throws IOException
     */
    public void destroy() throws IOException
    {
        super.close();
    }
}
