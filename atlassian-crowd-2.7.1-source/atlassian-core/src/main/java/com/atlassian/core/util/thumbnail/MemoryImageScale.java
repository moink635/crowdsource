package com.atlassian.core.util.thumbnail;

import com.atlassian.core.util.ReusableBufferedInputStream;
import com.atlassian.core.util.thumbnail.loader.ImageFactory;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class MemoryImageScale {

    private final Thumber thumber;

    public MemoryImageScale(final Thumber thumber) {
        this.thumber = thumber;
    }

    public BufferedImage scaleImage(final ReusableBufferedInputStream imageStream, int maxWidth, int maxHeight) throws IOException {
        final BufferedImage imageToScale = ImageFactory.loadImage(imageStream);
        final Thumber.WidthHeightHelper newDimensions = thumber.determineScaleSize(maxWidth, maxHeight, imageToScale.getWidth(), imageToScale.getHeight());
        return scaleImage(imageToScale, newDimensions);
    }

    public static BufferedImage scaleImage(final BufferedImage imageToScale, final Thumber.WidthHeightHelper newDimensions) {
        if (newDimensions.getWidth() > imageToScale.getWidth() || newDimensions.getHeight() > imageToScale.getHeight())
        {
            return getScaledInstance(imageToScale, newDimensions.getWidth(), newDimensions.getHeight(),
                    RenderingHints.VALUE_INTERPOLATION_BICUBIC, false);
        }
        else
        {
            return getScaledInstance(imageToScale, newDimensions.getWidth(), newDimensions.getHeight(),
                    RenderingHints.VALUE_INTERPOLATION_BILINEAR, true);
        }
    }

    /**
     * Convenience method that returns a scaled instance of the provided {@code BufferedImage}.
     * <p/>
     * Borrowed from http://today.java.net/pub/a/today/2007/04/03/perils-of-image-getscaledinstance.html
     * Warning: this algorith enters endless loop, when target size is bigger than image size and higherQuality is true
     *
     * @param image         the original image to be scaled
     * @param targetWidth   the desired width of the scaled instance, in pixels
     * @param targetHeight  the desired height of the scaled instance, in pixels
     * @param hint          one of the rendering hints that corresponds to {@code RenderingHints.KEY_INTERPOLATION} (e.g. {@code
     *                      RenderingHints.VALUE_INTERPOLATION_NEAREST_NEIGHBOR}, {@code RenderingHints.VALUE_INTERPOLATION_BILINEAR}, {@code
     *                      RenderingHints.VALUE_INTERPOLATION_BICUBIC})
     * @param higherQuality if true, this method will use a multi-step scaling technique that provides higher quality
     *                      than the usual one-step technique (only useful in downscaling cases, where {@code targetWidth} or {@code
     *                      targetHeight} is smaller than the original dimensions, and generally only when the {@code BILINEAR} hint is
     *                      specified)
     * @return a scaled version of the original {@code BufferedImage}
     */
    public static BufferedImage getScaledInstance(BufferedImage image,
                                            int targetWidth,
                                            int targetHeight,
                                            Object hint,
                                            boolean higherQuality)
    {
        int type = (image.getTransparency() == Transparency.OPAQUE) ?
                BufferedImage.TYPE_INT_RGB : BufferedImage.TYPE_INT_ARGB;
        int w, h;
        if (higherQuality)
        {
            // Use multi-step technique: start with original size, then
            // scale down in multiple passes with drawImage()
            // until the target size is reached
            w = image.getWidth();
            h = image.getHeight();
        }
        else
        {
            // Use one-step technique: scale directly from original
            // size to target size with a single drawImage() callscaleImage
            w = targetWidth;
            h = targetHeight;
        }

        do
        {
            if (higherQuality && w > targetWidth)
            {
                w /= 2;
                if (w < targetWidth)
                {
                    w = targetWidth;
                }
            }

            if (higherQuality && h > targetHeight)
            {
                h /= 2;
                if (h < targetHeight)
                {
                    h = targetHeight;
                }
            }

            BufferedImage tmp = new BufferedImage(w, h, type);
            Graphics2D g2 = tmp.createGraphics();
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION, hint);
            g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.setComposite(AlphaComposite.SrcOver);
            g2.drawImage(image, 0, 0, w, h, null);
            g2.dispose();

            image = tmp;
        }
        while (w != targetWidth || h != targetHeight);

        return image;
    }
}
