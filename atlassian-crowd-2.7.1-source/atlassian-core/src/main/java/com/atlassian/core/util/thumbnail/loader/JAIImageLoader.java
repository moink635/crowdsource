package com.atlassian.core.util.thumbnail.loader;

import com.google.common.base.Optional;
import com.sun.media.jai.codec.SeekableStream;

import javax.media.jai.JAI;
import javax.media.jai.OpImage;
import javax.media.jai.PlanarImage;
import javax.media.jai.RenderedOp;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

class JAIImageLoader implements ImageLoader
{

    @Override
    public Optional<BufferedImage> loadImage(final InputStream inputStream) throws IOException
    {
        try
        {
            SeekableStream s = SeekableStream.wrapInputStream(inputStream, true);
            RenderedOp img = JAI.create("stream", s);
            if (img == null)
                return Optional.absent();
            removeTileCache(img);
            return Optional.fromNullable(img.getAsBufferedImage());
        }
        catch (Exception e)
        {
            //JAI throws RuntimeExceptions when image is corrupted, we need to wrap them
            throw new IOException(e);
        }
    }

    /**
     * We don't want to cache image tiles in memory.
     */
    private void removeTileCache(final RenderedOp img)
    {
        final PlanarImage imgRendering = img.getRendering();
        if (imgRendering instanceof OpImage)
        {
            ((OpImage) imgRendering).setTileCache(null);
        }
    }
}