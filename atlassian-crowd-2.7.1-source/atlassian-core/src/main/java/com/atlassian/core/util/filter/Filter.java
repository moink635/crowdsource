package com.atlassian.core.util.filter;

/**
 * Defines what objects should be filtered out of a list.
 *
 * @deprecated use Guava's {@code Predicate} instead
 */
@Deprecated
public interface Filter<T>
{
    /**
     * Should an object be allowed through the filter?
     *
     * @return true if the object can pass through the filter, false
     *         if the object is rejected by the filter.
     */
    boolean isIncluded(T o);
}
