package com.atlassian.core.util.thumbnail.loader;

import com.google.common.base.Optional;
import org.apache.sanselan.ImageReadException;
import org.apache.sanselan.Sanselan;
import org.apache.sanselan.common.byteSources.ByteSource;
import org.apache.sanselan.common.byteSources.ByteSourceInputStream;
import org.apache.sanselan.formats.jpeg.JpegImageParser;
import org.apache.sanselan.formats.jpeg.segments.UnknownSegment;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import java.awt.color.ColorSpace;
import java.awt.color.ICC_ColorSpace;
import java.awt.color.ICC_Profile;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.awt.image.Raster;
import java.awt.image.WritableRaster;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;

class CMYKImageLoader implements ImageLoader
{

    @Override
    public Optional<BufferedImage> loadImage(final InputStream inputStream) throws IOException
    {
        return new JpegWithCMYKReader().readImage(inputStream);
    }

    /**
     * Standard JDK image utils: javax.imageio cannot read JPEGs with CMYK color profile.
     * This class inputStream using Sanselan library to workaround Java limitations.
     */
    private static class JpegWithCMYKReader
    {

        private enum ColorType
        {
            CMYK, YCCK
        }

        private ColorType colorType = ColorType.CMYK;
        private boolean isAdobeMarkerPresent = false;

        /**
         * Tries to read provided stream as CMYK JPEG and converts it to RGB
         *
         * @return Optional with image with RGB colors;
         * @throws java.io.IOException
         */
        public Optional<BufferedImage> readImage(final InputStream inputStream) throws IOException
        {

            ICC_Profile iccProfile;

            try
            {
                checkAdobeMarkerAndYcck(inputStream);

                inputStream.reset();
                iccProfile = Sanselan.getICCProfile(inputStream, null);
            } catch (ImageReadException e)
            {
                return Optional.absent();
            }

            inputStream.reset();
            WritableRaster raster = getRasterFromStream(inputStream);
            if (raster == null)
                return Optional.absent();

            if (colorType == ColorType.YCCK)
            {
                convertYcckToCmyk(raster);
            }
            if (isAdobeMarkerPresent)
            {
                convertInvertedColors(raster);
            }

            return Optional.of(convertCmykToRgb(raster, iccProfile));
        }

        /**
         * Gets raster data from image stream
         *
         * @return WritableRaster from inpusStream
         * @throws IOException
         */
        private WritableRaster getRasterFromStream(InputStream inputStream) throws IOException
        {
            Iterator readers = ImageIO.getImageReadersByFormatName("JPEG");
            ImageReader reader = null;
            try
            {
                while (readers.hasNext())
                {
                    reader = (ImageReader) readers.next();
                    if (reader.canReadRaster())
                    {
                        break;
                    }
                    else
                    {
                        reader.dispose();
                        reader = null;
                    }
                }
                if (reader == null)
                {
                    return null;
                }
                reader.setInput(ImageIO.createImageInputStream(inputStream));
                return (WritableRaster) reader.readRaster(0, null);
            } finally
            {
                if (reader != null)
                {
                    reader.dispose();
                }
            }
        }

        /**
         * Checks if image contains Adobe marker and if image is saved in YCCK color profile
         *
         * @throws IOException
         * @throws ImageReadException
         */
        private void checkAdobeMarkerAndYcck(InputStream inputStream) throws IOException, ImageReadException
        {
            inputStream.reset();
            JpegImageParser parser = new JpegImageParser();
            ByteSource byteSource = new ByteSourceInputStream(inputStream, null);
            ArrayList segments = parser.readSegments(byteSource, new int[]{0xffee}, true);

            if (segments != null && segments.size() >= 1)
            {
                UnknownSegment app14Segment = (UnknownSegment) segments.get(0);
                byte[] data = app14Segment.bytes;
                if (data.length >= 12 && data[0] == 'A' && data[1] == 'd' && data[2] == 'o' && data[3] == 'b' && data[4] == 'e')
                {

                    isAdobeMarkerPresent = true;

                    int transform = app14Segment.bytes[11] & 0xff;
                    if (transform == 2)
                    {
                        colorType = ColorType.YCCK;
                    }
                }
            }
        }

        private static void convertYcckToCmyk(WritableRaster raster)
        {
            int height = raster.getHeight();
            int width = raster.getWidth();
            int stride = width * 4;
            int[] pixelRow = new int[stride];
            for (int h = 0; h < height; h++)
            {
                raster.getPixels(0, h, width, 1, pixelRow);

                for (int x = 0; x < stride; x += 4)
                {
                    int y = pixelRow[x];
                    int cb = pixelRow[x + 1];
                    int cr = pixelRow[x + 2];

                    int c = (int) (y + 1.402 * cr - 178.956);
                    int m = (int) (y - 0.34414 * cb - 0.71414 * cr + 135.95984);
                    y = (int) (y + 1.772 * cb - 226.316);

                    if (c < 0) c = 0;
                    else if (c > 255) c = 255;
                    if (m < 0) m = 0;
                    else if (m > 255) m = 255;
                    if (y < 0) y = 0;
                    else if (y > 255) y = 255;

                    pixelRow[x] = 255 - c;
                    pixelRow[x + 1] = 255 - m;
                    pixelRow[x + 2] = 255 - y;
                }

                raster.setPixels(0, h, width, 1, pixelRow);
            }
        }

        private static void convertInvertedColors(WritableRaster raster)
        {
            int height = raster.getHeight();
            int width = raster.getWidth();
            int stride = width * 4;
            int[] pixelRow = new int[stride];
            for (int h = 0; h < height; h++)
            {
                raster.getPixels(0, h, width, 1, pixelRow);
                for (int x = 0; x < stride; x++)
                    pixelRow[x] = 255 - pixelRow[x];
                raster.setPixels(0, h, width, 1, pixelRow);
            }
        }

        private static BufferedImage convertCmykToRgb(Raster cmykRaster, ICC_Profile cmykProfile) throws IOException
        {

            cmykProfile = fixCmykProfile(cmykProfile);

            final ICC_ColorSpace cmykCS = new ICC_ColorSpace(cmykProfile);
            final BufferedImage rgbImage = new BufferedImage(cmykRaster.getWidth(), cmykRaster.getHeight(), BufferedImage.TYPE_INT_RGB);
            final WritableRaster rgbRaster = rgbImage.getRaster();
            final ColorSpace rgbCS = rgbImage.getColorModel().getColorSpace();
            final ColorConvertOp cmykToRgb = new ColorConvertOp(cmykCS, rgbCS, null);

            cmykToRgb.filter(cmykRaster, rgbRaster);
            return rgbImage;
        }

        private static ICC_Profile fixCmykProfile(ICC_Profile cmykProfile)
        {
            if (cmykProfile.getProfileClass() != ICC_Profile.CLASS_DISPLAY)
            {
                byte[] profileData = cmykProfile.getData();

                if (profileData[ICC_Profile.icHdrRenderingIntent] == ICC_Profile.icPerceptual)
                {
                    intToBigEndian(ICC_Profile.icSigDisplayClass, profileData, ICC_Profile.icHdrDeviceClass);

                    cmykProfile = ICC_Profile.getInstance(profileData);
                }
            }
            return cmykProfile;
        }

        private static void intToBigEndian(int value, byte[] array, int index)
        {
            array[index] = (byte) (value >> 24);
            array[index + 1] = (byte) (value >> 16);
            array[index + 2] = (byte) (value >> 8);
            array[index + 3] = (byte) (value);
        }
    }
}
