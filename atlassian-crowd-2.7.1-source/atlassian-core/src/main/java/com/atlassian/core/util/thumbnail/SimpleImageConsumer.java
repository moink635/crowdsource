package com.atlassian.core.util.thumbnail;

import org.j3d.util.ImageGenerator;

/**
 * @deprecated Please use the following {@link org.j3d.util.ImageGenerator} directly via the
 *             com.atlassian.image:atlassian-image-consumer artifact
 */
public class SimpleImageConsumer extends ImageGenerator
{

}
