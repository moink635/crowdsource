package com.atlassian.core.util.collection;

import com.atlassian.core.util.ObjectUtils;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * A replacement for UtilMisc.toList(). <p/> Most methods here are not null safe
 *
 * @deprecated use the appropriate methods on Guava's {@code ImmutableList}, {@code Lists}, or {@code Iterables}
 */
@Deprecated
public class EasyList
{

    /**
     * Creates a list with one null value. Occasionally useful.
     * @return a list with one null value.
     */
    public static <T> List<T> buildNull()
    {
        List<T> list = createList(1);
        list.add(null);
        return list;
    }

    public static <T> List<T> build(T[] array)
    {
        List<T> list = createList(array.length);

        Collections.addAll(list, array);

        return list;
    }

    public static <T> List<T> buildNonNull(T[] array)
    {
        List<T> list;
        if (array != null && array.length > 0)
        {
            list = createList(array.length);
            for (T o : array)
            {
                if (ObjectUtils.isNotEmpty(o))
                {
                    list.add(o);
                }
            }
        }
        else
        {
            list = Collections.emptyList();
        }

        return list;
    }

    public static <T> List<T> buildNonNull(Collection<T> c)
    {
        List<T> list;
        if (c != null && !c.isEmpty())
        {
            list = build(CollectionUtils.predicatedCollection(c, ObjectUtils.getIsSetPredicate()));
        }
        else
        {
            list = Collections.emptyList();
        }

        return list;
    }

    public static <T> List<T> buildNonNull(T o)
    {
        if (ObjectUtils.isNotEmpty(o))
        {
            return build(o);
        }
        else
        {
            return build();
        }
    }

    public static <T> List<T> build()
    {
        return Collections.emptyList();
    }

    public static <T> List<T> build(T o1)
    {
        List<T> list = createList(1);

        list.add(o1);

        return list;
    }

    public static <A, B extends A> List<A> build(A o1, B o2)
    {
        List<A> list = createList(2);

        list.add(o1);
        list.add(o2);

        return list;
    }

    public static <A, B extends A, C extends A> List<A> build(A o1, B o2, C o3)
    {
        List<A> list = createList(3);

        list.add(o1);
        list.add(o2);
        list.add(o3);

        return list;
    }

    public static <A, B extends A, C extends A, D extends A> List<A> build(A o1, B o2, C o3, D o4)
    {
        List<A> list = createList(4);

        list.add(o1);
        list.add(o2);
        list.add(o3);
        list.add(o4);

        return list;
    }

    public static <A, B extends A, C extends A, D extends A, E extends A> List<A> build(A o1, B o2, C o3, D o4, E... others)
    {
        List<A> list = createList(5);

        list.add(o1);
        list.add(o2);
        list.add(o3);
        list.add(o4);
        Collections.addAll(list, others);

        return list;
    }

    public static <T> List<T> build(Collection<T> collection)
    {
        if (collection == null) return null;
        return new ArrayList<T>(collection);
    }

    public static <T> List<T> createList(int size)
    {
        return new ArrayList<T>(size);
    }

    /**
     * Merge a maximum of three lists.
     * Null lists passed in will be ignored.
     *
     * @param a The first list.
     * @param b The second list.
     * @param c The third list.
     * @return A merged list containing the objects of all three passed in lists.
     * @deprecated use Guava's {@code Iterables.concat()}
     */
    public static <T, U extends T, V extends T> List<T> mergeLists(List<T> a, List<U> b, List<V> c)
    {
        List<T> d = EasyList.createList(0);
        if (a != null)
        {
            d.addAll(a);
        }
        if (b != null)
        {
            d.addAll(b);
        }
        if (c != null)
        {
            d.addAll(c);
        }
        return d;
    }

    /**
     * Splits a list into a number of sublists of the correct length. Note this will create a 'shallow' split, in other
     * words if you set/remove on the sublists, this will modify the parent list as well (same vice versa). Therefore,
     * DO NOT publish the result of this method to clients as the you will be publishing read/write access to your
     * underlying data and the results will be unpredictable, especially in a multi-threaded context.
     * 
     * @param list
     *            The list to split
     * @param sublength
     *            Length of the sublists
     * @return A list of lists of the correct length
     * @deprecated use Guava's {@code Lists.partition()}
     */
    public static <T> List<List<T>> shallowSplit(List<T> list, int sublength)
    {
        // if we have any remainder, then we will add one to our division to correctly size the result list
        int overflow = ((list.size() % sublength) > 0) ? 1 : 0;
        List<List<T>> result = new ArrayList<List<T>>((list.size() / sublength) + overflow);
        int i = 0;
        while (i < list.size())
        {
            int endIndex = (i + sublength) > list.size() ? list.size() : i + sublength;
            result.add(list.subList(i, endIndex));
            i += sublength;
        }

        return result;
    }
}
