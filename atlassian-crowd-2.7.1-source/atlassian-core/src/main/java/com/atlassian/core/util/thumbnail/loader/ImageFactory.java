package com.atlassian.core.util.thumbnail.loader;

import com.atlassian.core.util.ReusableBufferedInputStream;
import com.google.common.base.Optional;
import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.List;

public class ImageFactory
{

    /**
     * Instances of all available image loaders. Loaders will be run in sequence and
     * first not null BufferedImage instance will be returned.
     * <p/>
     * Sequence of Loaders is important!
     */
    private static final List<ImageLoader> imageLoaders = ImmutableList.of(
            new DefaultImageLoader(),
            new CMYKImageLoader(),
            new JAIImageLoader()
    );

    /**
     * Loads image to memory from specified inputStream.
     *
     * @return Loaded Image or null if none of Loaders could load the image
     */
    public static BufferedImage loadImage(ReusableBufferedInputStream inputStream) throws IOException
    {

        IOException lastException = null;
        Optional<BufferedImage> image = Optional.absent();
        for (ImageLoader imageLoader : imageLoaders)
        {
            try
            {
                image = imageLoader.loadImage(inputStream);
                if (image.isPresent())
                    break;
                inputStream.reset();
            }
            catch (IOException e)
            {
                lastException = e;
            }
        }
        if (!image.isPresent())
        {
            //Throw last exception from loaders or return null
            Throwables.propagateIfInstanceOf(lastException, IOException.class);
            return null;
        }
        return image.get();
    }

}
