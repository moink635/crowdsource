package com.atlassian.core.util.thumbnail.loader;

import com.google.common.base.Optional;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

class DefaultImageLoader implements ImageLoader {
    @Override
    public Optional<BufferedImage> loadImage(final InputStream inputStream) throws IOException
    {
        try
        {
            return Optional.fromNullable(ImageIO.read(inputStream));
        }
        catch (Exception e)
        {
            //ImageIO throws also RuntimeExceptions when image is corrupted
            //we need to wrap them
            throw new IOException(e);
        }
    }
}
