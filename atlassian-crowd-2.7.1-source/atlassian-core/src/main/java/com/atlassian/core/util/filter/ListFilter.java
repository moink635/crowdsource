package com.atlassian.core.util.filter;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

/**
 * Filter the contents of a list based on some criteria.
 *
 * @deprecated use Guava's {@code Iterables.filter()}.
 */
@Deprecated
public class ListFilter<T>
{
    private final Filter<T> filter;
    private static final Object MYNULL = new Object();

    private class FilteredIterator<E extends T> implements Iterator<T>
    {
        private final Iterator<E> innerIterator;

        // We need to define our own "null", since there may be real
        // nulls in a list. Funky, eh.
        private T savedObject = myNull();

        public FilteredIterator(Iterator<E> innerIterator)
        {
            this.innerIterator = innerIterator;
        }

        public void remove()
        {
            innerIterator.remove();
        }

        public boolean hasNext()
        {
            if (savedObject != MYNULL)
                return true;

            while (innerIterator.hasNext())
            {
                savedObject = innerIterator.next();
                if (filter.isIncluded(savedObject))
                    return true;
            }
            savedObject = myNull();

            return false;
        }

        public T next()
        {
            if (savedObject != MYNULL)
                return clearSavedObject();

            while (true)
            {
                E o = innerIterator.next();
                if (filter.isIncluded(o))
                    return o;
            }
        }

        private T clearSavedObject()
        {
            T ret = savedObject;
            savedObject = myNull();
            return ret;
        }

        private T myNull() {
            return (T) MYNULL;
        }

    }

    /**
     * Constructor, taking the filter implementation that will be used to
     * filter the list.
     *
     * @param filter the filter implementation that will be used to filter the
     *        list.
     */
    public ListFilter(Filter<T> filter)
    {
        this.filter = filter;
    }

    /**
     * Filter the contents of a list. Returns a new list with the filtered
     * objects removed. Does not change the list passed in.
     *
     * @param list the list to filter
     * @return a new list with the filtered objects removed.
     */
    public List<T> filterList(List<T> list)
    {
        if (list ==  null)
            return list;
        
        List<T> filteredList = new ArrayList<T>();
        Iterator<T> i = filterIterator(list.iterator());
        while (i.hasNext())
            filteredList.add(i.next());

        return filteredList;
    }

    /**
     * Filter the contents of an iterator. Returns an iterator that will only
     * return un-filtered members of the supplied iterator.
     */
    public Iterator<T> filterIterator(Iterator<T> iterator)
    {
        return new FilteredIterator<T>(iterator);
    }

}
