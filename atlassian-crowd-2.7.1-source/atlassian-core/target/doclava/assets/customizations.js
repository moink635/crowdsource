$(document).ready(function() {

    // override default value of docs search box with start value
    // (populated from project name)
    window.DEFAULT_TEXT = $("#search_autocomplete").val();

    // populate the hidden inputs element in the feedback form
    $(".url-injector").attr("value", window.location.href);
    $(".page-title-injector").attr("value", document.title);

    /**
     * Returns a selector element for the current product set to be used in the search submission.
     */
    function renderProductSelector(products) {
        var space = $('<select/>').attr("name", "where").addClass("search-product-selector");
        var all = $("<option/>").attr("value", "").text("All Products");    // i18n somehow?
        space.append(all);
        $.each(products, function(family, value) {
            var optgroup = $('<optgroup/>').attr("label", family);
            $.each(value, function(i, product) {
                var option = $('<option/>').attr("value", product.key)
                        .text(product.displayName);
                optgroup.append(option);
            });
            space.append(optgroup);
        });

        $("#quick-search").append(space);
    }

    // force search to go to javadoc search
    $('#quick-search').submit(function(){
        $('<input type="hidden" name="searchButton" value="api" />').appendTo(this);
    });

    // allow toggle for showing/hiding atlassian annotation details
    function toggleAtlassianAnnotationInfo(link) {
        $(link).parent().siblings(".extended-info").slideToggle('fast');
        var img = $(link).find("img");
        if (img.attr("src").indexOf("closed") > 0) {
            img.attr("src", img.attr("src").replace("closed", "opened"));
        } else {
            img.attr("src", img.attr("src").replace("opened", "closed"));
        }
    }

    $(".annotation-extended-info-expando").click(function() {
        toggleAtlassianAnnotationInfo(this);
    });

    // load the product search selector data set
    $.ajax({
        url: '/rest/dac/2/provider/products.json',
        dataType: 'json',
        success: function(products) {
            renderProductSelector(products);
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log("couldn't retrieve product search options: '" + textStatus + "'; removing search form");
            $("#quicksearch-div").remove();
        }
    });

});
