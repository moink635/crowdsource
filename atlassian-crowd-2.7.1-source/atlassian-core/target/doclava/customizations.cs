<?cs # appears at the top of every page ?><?cs 
def:custom_masthead() ?>
<div id="header">
    <div id="header-div">
        <div id="logo">
            <a href="http://developer.atlassian.com" tabindex="-1"><img
               src="<?cs var:toassets ?>images/atlassian-logo.png" alt="" border="0"></a>
        </div>
        <div id="dac-menu-bar">
            <a href="http://developer.atlassian.com/display/DOCS" class="active">Docs</a>
            <a href="http://developer.atlassian.com/display/MARKET">Atlassian Marketplace</a>
            <a href="http://developer.atlassian.com/display/SUPPORT">Answers</a>
            <a href="http://developer.atlassian.com/display/NEWS">News</a>
            <a href="http://developer.atlassian.com/display/ABOUT">About</a>
            <span id="login-link" class="$paramclass" style="">
                <a href="http://developer.atlassian.com/display/LAND/Log+In+or+Sign+Up">Log In</a>
            </span>
        </div>
        <div id="quicksearch-div">
            <form id="quick-search" method="get" action="https://developer.atlassian.com/dosearchsite.action" _lpchecked="1">
                <input type="text" name="queryString" accesskey="q">
            </form>
        </div>
        <div id="dac-submenu">
            <ul>
                <li><a data-space-keys='DOCS' href="/display/DOCS/Introduction+to+the+Atlassian+Plugin+SDK" title="Introduction to the Atlassian Plugin SDK">SDK</a></li>
                <li><a data-space-keys='JIRADEV' href="/display/JIRADEV/JIRA+Developer+Documentation" title="JIRA Developer Documentation">JIRA</a></li>
                <li><a data-space-keys='CONFDEV' href="/display/CONFDEV/Confluence+Developer+Documentation" title="Confluence Developer Documentation">Confluence</a></li>
                <li><a data-space-keys='CROWDDEV' href="/display/CROWDDEV/Crowd+Developer+Documentation" title="Crowd Developer Documentation">Crowd</a></li>
                <li><a data-space-keys='BAMBOODEV' href="/display/BAMBOODEV/Bamboo+Developer+Documentation" title="Bamboo Developer Documentation">Bamboo</a></li>
                <li><a data-space-keys='GADGETS' href="/display/GADGETS/Gadget+Developer+Documentation" title="Gadget Developer Documentation">Gadgets</a></li>
                <li><a data-space-keys='AUI' href="/display/AUI/Atlassian+User+Interface+%28AUI%29+Developer+Documentation" title="Atlassian User Interface (AUI) Developer Documentation">AUI</a></li>
                <li><a class="active" href="/static/" title="API Reference">API&nbsp;Reference</a></li>
                <li><a data-space-keys='TOOLS' href="/display/TOOLS/Other+Topics" title="Other Topics">Other&nbsp;Topics</a></li>
            </ul>
        </div>
    </div><!-- header-div -->

    <div id="masthead">
        <?cs if:project.name ?>
            <span id="masthead-title"><?cs var:project.name ?></span>
        <?cs /if ?>
        <div id="search-box">
          <?cs call:default_search_box() ?>
          <?cs if:reference && reference.apilevels ?>
            <?cs call:default_api_filter() ?>
          <?cs /if ?>
        </div>
    </div><!-- masthead -->
</div><!-- header--><?cs
/def ?>

<?cs # Show any warnings related to atlassian annotations ?>
<?cs def:annotation_warning(obj) ?>
    <?cs each:annotation = obj.annotations ?>
    <?cs if:annotation.type.label == "PublicApi" ?>
        <div class="atlassian-api public">
            <img src="<?cs var:toassets ?>images/atlassian_icon.png">
            <h3><a class="annotation-extended-info-expando" href="#"><img class="expando-arrow" src="<?cs var:toassets ?>images/triangle-closed.png">@PublicApi</a></h3>
            <div class="extended-info">
                This <?cs var:obj.kind ?> is designed for plugins to <em>consume</em> (call its methods).
                <p>
                Clients of <code>@PublicApi</code> can expect
                that <b>programs compiled against a given version will remain binary compatible with later versions of the
                <code>@PublicApi</code></b> as per each product's API policy <b>as long as the client does not implement/extend
                <code>@PublicApi</code> interfaces or classes</b> (refer to each product's API policy for the exact
                guarantee---usually binary compatibility is guaranteed at least across minor versions).
                <p/>
                Note: since <code>@PublicApi</code> interfaces and classes are not designed to be implemented or extended by clients,
                we may perform certain types of binary-incompatible changes to these classes and interfaces, but these will not
                affect well-behaved clients that do not extend/implement these types (in general, only classes and interfaces
                annotated with <code>@PublicSpi</code> are safe to extend/implement).
            </div>
        </div>
        <div style="clear: both;"></div>
    <?cs elif:annotation.type.label == "Internal" ?>
        <div class="atlassian-api internal">
            <img src="<?cs var:toassets ?>images/atlassian_icon.png">
            <h3><a class="annotation-extended-info-expando" href="#"><img class="expando-arrow" src="<?cs var:toassets ?>images/triangle-closed.png">@Internal</a></h3>
            <div class="extended-info">
                This <?cs var:obj.kind ?> is an internal implementation detail and will change without notice.
                <p/>
                Clients that depend on <code>@Internal</code> classes and interfaces can not expect to be compatible with any version
                other than the version they were compiled against (even minor version and milestone releases may break binary
                compatibility with respect to <code>@Internal</code> elements).
            </div>
        </div>
        <div style="clear: both;"></div>
    <?cs elif:annotation.type.label == "PublicSpi" ?>
        <div class="atlassian-api spi">
            <img src="<?cs var:toassets ?>images/atlassian_icon.png">
            <h3><a class="annotation-extended-info-expando" href="#"><img class="expando-arrow" src="<?cs var:toassets ?>images/triangle-closed.png">@PublicSpi</a></h3>
            <div class="extended-info">
                This <?cs var:obj.kind ?> is designed for plugins to <em>implement</em>.
                <p>
                Clients of <code>@PublicSpi</code> can expect
                that <b>programs compiled against a given version will remain binary compatible with later versions of the
                <code>@PublicSpi</code></b> as per each product's API policy (clients should refer to each product's API policy for
                the exact guarantee -- usually binary compatibility is guaranteed at least across minor versions).
                <p/>
                Note: <code>@PublicSpi</code> interfaces and classes are specifically designed to be implemented/extended by clients.
                Hence, the guarantee of binary compatibility is different to that of <code>@PublicApi</code> elements (if an element
                is both <code>@PublicApi</code> and <code>@PublicSpi</code>, both guarantees apply).
            </div>
        </div>
        <div style="clear: both;"></div>
    <?cs elif:annotation.type.label == "ExperimentalApi" ?>
        <div class="atlassian-api experimental">
            <img src="<?cs var:toassets ?>images/atlassian_icon.png">
            <h3><a class="annotation-extended-info-expando" href="#"><img class="expando-arrow" src="<?cs var:toassets ?>images/triangle-closed.png">@ExperimentalApi</a></h3>
            <div class="extended-info">
                This <?cs var:obj.kind ?> is considered usable by external developers but its contracts have not stabilized.
                <p/>
                Experimental APIs may be changed at any time before being marked <code>@Internal</code> or <code>@PublicApi</code>.
            </div>
        </div>
        <div style="clear: both;"></div>
    <?cs /if ?>
    <?cs /each ?>
<?cs /def ?>
