package com.atlassian.util.profiling.filters;

import com.atlassian.util.profiling.UtilTimerStack;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;

/**
 * <p>Filter that will intercept requests &amp; time how long it takes for them to return.  It stores
 * this information in the ProfilingTimerBean.
 *
 * <p>Install the filter in your web.xml file as follows:
 * <pre>
 *   &lt;filter&gt;
 *      &lt;filter-name&gt;profiling&lt;/filter-name&gt;
 *      &lt;filter-class&gt;com.atlassian.util.profiling.filters.ProfilingFilter&lt;/filter-class&gt;
 *      &lt;init-param&gt;
 *             &lt;param-name&gt;activate.param&lt;/param-name&gt;
 *             &lt;param-value&gt;profilingfilter&lt;/param-value&gt;
 *         &lt;/init-param&gt;
 *         &lt;init-param&gt;
 *             &lt;param-name&gt;autostart&lt;/param-name&gt;
 *             &lt;param-value&gt;false&lt;/param-value&gt;
 *         &lt;/init-param&gt;
 *  &lt;/filter&gt;
 *
 *  &lt;filter-mapping&gt;
 *      &lt;filter-name&gt;profiling&lt;/filter-name&gt;
 *      &lt;url-pattern&gt;/*&lt;/url-pattern&gt;
 *  &lt;/filter-mapping&gt;
 * </pre>
 *
 * <p>With the above settings you can turn the filter on by accessing any URL with the
 * parameter <code>profilingfilter=on</code>.eg:
 *
 * <pre>    http://mywebsite.com/a.jsp?<b><i>profilingfilter=on</i></b></pre>
 *
 * <p>The above settings also sets the filter to not start automatically upon startup.  This
 * may be useful for production, but you will most likely want to set this true in
 * development.
 *
 * @author <a href="mailto:mike@atlassian.com">Mike Cannon-Brookes</a>
 * @author <a href="mailto:scott@atlassian.com">Scott Farquhar</a>
 */
public class ProfilingFilter implements javax.servlet.Filter
{


    /**
     * This is the parameter you pass to the init parameter &amp; specify in the web.xml file:
     * eg.
     * <pre>
     * &lt;filter&gt;
     *   &lt;filter-name&gt;profile&lt;/filter-name&gt;
     *   &lt;filter-class&gt;com.atlassian.util.profiling.filters.ProfilingFilter&lt;/filter-class&gt;
     *  &lt;init-param&gt;
     *    &lt;param-name&gt;activate.param&lt;/param-name&gt;
     *    &lt;param-value&gt;filter.changestatus&lt;/param-value&gt;
     *  &lt;/init-param&gt;
     * &lt;/filter&gt;
     *  </pre>
     */
    private static final String ON_OFF_INIT_PARAM = "activate.param";

    /**
     * This is the parameter you pass to the init parameter &amp; specify in the web.xml file:
     * eg.
     * <pre>
     * &lt;filter&gt;
     *   &lt;filter-name&gt;profile&lt;/filter-name&gt;
     *   &lt;filter-class&gt;com.atlassian.util.profiling.filters.ProfilingFilter&lt;/filter-class&gt;
     *  &lt;init-param&gt;
     *    &lt;param-name&gt;autostart&lt;/param-name&gt;
     *    &lt;param-value&gt;true&lt;/param-value&gt;
     *  &lt;/init-param&gt;
     * &lt;/filter&gt;
     *  </pre>
     *
     */
    private static final String START_STOP_PARAM = "autostart";


    /**
     * Default parameter for turning the filter on and off.
     * eg.
     * <pre>
     * http://mywebsite.com/a.jsp?profile.filter=on
     * http://mywebsite.com/a.jsp?profile.filter=off
     * </pre>
     * Most often you will want to change this default value by passing an init
     * parameter to the filter.  This is configurable as it could be a potential
     * security hole if users can randomly start and stop the profiling.
     * <p>
     * Security through obscurity - yes I know its bad!
     */
    private static final String DEFAULT_ON_OFF_PARAM = "profile.filter";

    /**
     * This is static as there may be more than one instance of a servlet filter.
     */
    private static String onOffParameter = DEFAULT_ON_OFF_PARAM;

    private static String onOffParameterEquals = onOffParameter + "=";

    private FilterConfig filterConfig;


    /**
     * Check for parameters to turn the filter on or off.  If parameters are given, change
     * the current state of the filter.  If current state is off then pass to filter chain.
     * If current state is on - record start time, pass to filter chain, and then record
     * total time on the return.
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws java.io.IOException, javax.servlet.ServletException
    {
        String paramValue = null;

        // Let's try to get the profiling value from the query string instead of getParameter()
        // to allow for the request to later have its encoding set
        if (request instanceof HttpServletRequest) {
            String queryString = ((HttpServletRequest)request).getQueryString();
            if (queryString != null)
            {
                int idx = queryString.indexOf(onOffParameterEquals);
                if (idx >= 0)
                {
                    paramValue = queryString.substring(idx+onOffParameterEquals.length());
                    idx = queryString.indexOf('&');
                    if (idx >= 0) 
                        paramValue = paramValue.substring(0, idx);
                }
            }    
        }
        if (paramValue != null)
            setFilterState(paramValue);

        //if filter is not on - then don't do any processing.
        if (!isFilterOn())
        {
            chain.doFilter(request, response);
            return;
        }

        String resource = getResourceName(request);
        UtilTimerStack.push(resource);

        try
        {
            // time and perform the request
            chain.doFilter(request, response);
        }
        finally
        {
            UtilTimerStack.pop(resource);
        }

    }

    private void setFilterState(String paramValue)
    {
        if ("on".equals(paramValue) || "true".equals(paramValue))
        {
            turnFilterOn();
        }
        else if ("off".equals(paramValue) || "false".equals(paramValue))
        {
            turnFilterOff();
        }
        else if (paramValue.length() > 0 && Character.isDigit(paramValue.charAt(0)))
        {
            try
            {
                turnFilterOnAndSetFilterThreshold(Long.parseLong(paramValue));
                turnFilterOn();
            }
            catch (NumberFormatException e)
            {
            }
        }
    }

    private String getResourceName(ServletRequest request)
    {
        //if an include file then get the proper resource name.
        if (request.getAttribute("javax.servlet.include.request_uri") != null)
            return (String) request.getAttribute("javax.servlet.include.request_uri");
        else
            return ((HttpServletRequest) request).getRequestURI();
    }

    public void setFilterConfig(FilterConfig filterConfig)
    {
        this.filterConfig = filterConfig;
        if (filterConfig.getInitParameter(ON_OFF_INIT_PARAM) != null)
        {
            System.out.println("[Filter: " + filterConfig.getFilterName() + "] Using parameter [" + filterConfig.getInitParameter(ON_OFF_INIT_PARAM) + "]");
            onOffParameter = filterConfig.getInitParameter(ON_OFF_INIT_PARAM);
            onOffParameterEquals = onOffParameter + "=";
        }
        if ("true".equals(filterConfig.getInitParameter(START_STOP_PARAM)))
        {
            System.out.println("[Filter: " + filterConfig.getFilterName() + "] defaulting to on [" + START_STOP_PARAM + "=true]");
            turnFilterOn();
        }
        else if ("false".equals(filterConfig.getInitParameter(START_STOP_PARAM)))
        {
            System.out.println("[Filter: " + filterConfig.getFilterName() + "] defaulting to off [" + START_STOP_PARAM + "=false]");
            turnFilterOff();
        }
    }

    public void init(FilterConfig filterConfig) throws ServletException
    {
        //some servlet containers set this to be null
        if (filterConfig != null)
            setFilterConfig(filterConfig);
    }

    public void destroy()
    {

    }

    private boolean isFilterOn()
    {
        return UtilTimerStack.isActive();
    }

    private void turnFilterOn()
    {
        System.out.println("[Filter: " + filterConfig.getFilterName() + "] Turning filter on [" + onOffParameter + "=on]");
        UtilTimerStack.setActive(true);
    }

    private void turnFilterOnAndSetFilterThreshold(long minTotalTime)
    {
        System.out.println("[Filter: " + filterConfig.getFilterName() + "] Turning filter on [" + onOffParameter + "=on] with threshold " + minTotalTime + "ms");
        UtilTimerStack.setMinTotalTime(minTotalTime);
        UtilTimerStack.setActive(true);
    }

    private void turnFilterOff()
    {
        System.out.println("[Filter: " + filterConfig.getFilterName() + "] Turning filter off [" + onOffParameter + "=off]");
        UtilTimerStack.setMinTotalTime(0);
        UtilTimerStack.setActive(false);
    }
}

