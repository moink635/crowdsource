package com.atlassian.util.profiling.object;

/**
 * Created by IntelliJ IDEA.
 * User: Mark Derricutt
 * Date: 10/03/2006
 * Time: 19:57:00
 * To change this template use File | Settings | File Templates.
 */
public interface Profilable
{
    Object profile() throws Exception;
}
