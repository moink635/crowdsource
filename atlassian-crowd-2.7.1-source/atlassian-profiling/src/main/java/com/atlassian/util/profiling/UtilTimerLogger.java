package com.atlassian.util.profiling;

/**
 * Created by IntelliJ IDEA.
 * User: Mark Derricutt
 * Date: 7/07/2006
 * Time: 10:10:03
 * To change this template use File | Settings | File Templates.
 */
public interface UtilTimerLogger {
    void log(String s);
}
