package com.atlassian.util.profiling.object;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class TimerInvocationHandler implements InvocationHandler, Serializable {
    protected Object target;

    public TimerInvocationHandler(Object target) {
        if (target == null)
            throw new IllegalArgumentException("Target Object passed to timer cannot be null");
        this.target = target;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return ObjectProfiler.profiledInvoke(method, target, args);
    }

}
