package com.atlassian.util.profiling.object;

import com.atlassian.util.profiling.UtilTimerStack;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.lang.reflect.InvocationTargetException;

/**
 * @author <a href="mailto:scott@atlassian.com">Scott Farquhar</a>
 */
public class ObjectProfiler
{

    /**
     * Given a class, and an interface that it implements, return a proxied version of the class that implements
     * the interface.
     * <p>
     * The usual use of this is to profile methods from Factory objects:
     * <pre>
     * public PersistenceManager getPersistenceManager()
     * {
     *   return new DefaultPersistenceManager();
     * }
     *
     * instead write:
     * public PersistenceManager getPersistenceManager()
     * {
     *   return ObjectProfiler.getProfiledObject(PersistenceManager.class, new DefaultPersistenceManager());
     * }
     * </pre>
     * <p>
     * A side effect of this is that you will no longer be able to downcast to DefaultPersistenceManager.  This is probably a *good* thing.
     *
     * @param interfaceClazz    The interface to implement.
     * @param o                 The object to proxy
     * @return                  A proxied object, or the input object if the interfaceClazz wasn't an interface.
     */
    public static Object getProfiledObject(Class interfaceClazz, Object o)
    {
        //if we are not active - then do nothing
        if (!UtilTimerStack.isActive())
            return o;

        //this should always be true - you shouldn't be passing something that isn't an interface
        if (interfaceClazz.isInterface())
        {
            InvocationHandler timerHandler = new TimerInvocationHandler(o);
            Object proxy = Proxy.newProxyInstance(interfaceClazz.getClassLoader(),
                    new Class[]{interfaceClazz}, timerHandler);
            return proxy;
        }
        else
        {
            return o;
        }
    }

    /**
     * A profiled call {@link Method#invoke(java.lang.Object, java.lang.Object[]). If {@link UtilTimerStack#isActive() }
     * returns false, then no profiling is performed.
     */
    public static Object profiledInvoke(Method target, Object value, Object[] args) throws Throwable
    {
        //if we are not active - then do nothing
        if (!UtilTimerStack.isActive())
        {
            try
            {
                return target.invoke(value, args);
            }
            catch (InvocationTargetException e)
            {
                // unpack target exception (PROF-16)
                if (e.getTargetException() != null)
                    throw e.getTargetException();
                else
                    throw e;
            }
        }

        String logLine = new String(getTrimmedClassName(target) + "." + target.getName() + "()");

        UtilTimerStack.push(logLine);
        try
        {
            Object returnValue = target.invoke(value, args);

            //if the return value is an interface then we should also proxy it!
            if (returnValue != null && target.getReturnType().isInterface())
            {
//                System.out.println("Return type " + returnValue.getClass().getName() + " is being proxied " + target.getReturnType().getName() + " " + logLine);
                InvocationHandler timerHandler = new TimerInvocationHandler(returnValue);
                Object objectProxy = Proxy.newProxyInstance(returnValue.getClass().getClassLoader(),
                        new Class[]{target.getReturnType()}, timerHandler);
                return objectProxy;
            }
            else
            {
                return returnValue;
            }
        }
        catch (InvocationTargetException e)
        {
            throw e.getTargetException(); //unpack target exception (PROF-16)
        }
        finally
        {
            UtilTimerStack.pop(logLine);
        }
    }

    /**
     * Given a method, get the Method name, with no package information.
     */
    public static String getTrimmedClassName(Method method)
    {
        String classname = method.getDeclaringClass().getName();
        return classname.substring(classname.lastIndexOf('.') + 1);
    }
    
    /**
     * A simple convience wrapper for profiling a block of code, reduces repetition of captions used by
     * the UtilTimerStack#push and UtilTimerStack#pop methods.
     * <p/>
     * Any exception that is thrown is wrapped as a RuntimeException so as to not adversely mess your
     * source code up.
     *
     * @param caption
     * @param profilable
     * @return
     */
    public static Object profile(final String caption, Profilable profilable) throws RuntimeException
    {
        Object o;
        try
        {
            UtilTimerStack.push(caption);
            o = profilable.profile();
        }
        catch (RuntimeException e)
        {
            throw e;
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            UtilTimerStack.pop(caption);
        }

        return o;
    }

}
