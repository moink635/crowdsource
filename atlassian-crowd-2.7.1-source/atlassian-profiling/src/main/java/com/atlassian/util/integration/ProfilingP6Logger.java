package com.atlassian.util.integration;

import com.p6spy.engine.logging.appender.P6Logger;
import com.atlassian.util.profiling.UtilTimerStack;

public class ProfilingP6Logger implements P6Logger
{
    public void logSQL(int now, String elapsed, long connectionId, String category, String prepared, String sql)
    {
        if ("statement".equals(category))
        {
            String logEntry = now + "|" + elapsed + "|" + (connectionId == -1 ? "" : String.valueOf(connectionId)) + "|" + category + "|" + sql;
            UtilTimerStack.push(logEntry);
            UtilTimerStack.pop(logEntry);  //todo - specify the elapsed time.
        }
    }

    public void logException(Exception e)
    {
        //not implemented - just print debugging.
        System.out.println("e = " + e);
    }

    public void logText(String s)
    {
        System.out.println("s = " + s);
    }

    public String getLastEntry()
    {
        System.out.println("ProfilingP6Logger.getLastEntry");
        return null;
    }
}
