/**
 * Atlassian Source Code Template.
 * User: Scott Farquhar
 * Date: Feb 19, 2003
 * Time: 6:56:26 PM
 * CVS Revision: $Revision: 3430 $
 * Last CVS Commit: $Date: 2006-10-02 23:24:14 +0000 (Mon, 02 Oct 2006) $
 * Author of last CVS Commit: $Author: dbrown $
 *
 * @author <a href="mailto:scott@atlassian.com">Scott Farquhar</a>
 */
package com.atlassian.util.profiling;


/**
 * A timer stack.
 * <p>
 * Usage:
 * <pre>
 * String logMessage = "Log message";
 * UtilTimerStack.push(logMessage);
 * try
 * {
 *   //do some code
 * }
 * finally
 * {
 *   UtilTimerStack.pop(logMessage); //this needs to be the same text as above
 * }
 * </pre>
 */
public class UtilTimerStack
{

    // A reference to the current ProfilingTimerBean
    private static ThreadLocal current = new ThreadLocal();

    /**
     * System property that controls whether this timer should be used or not.  Set to "true" activates
     * the timer.  Set to "false" to disactivate.
     */
    public static final String ACTIVATE_PROPERTY = "atlassian.profile.activate";

    /**
     * System property that controls whether memory should be profiled or not.  Set to "true" activates
     * the timer.  Set to "false" to disactivate.
     */
    public static final String ACTIVATE_MEMORY_PROPERTY = "atlassian.profile.activate.memory";
    

    /**
     * System property that controls the threshhold time below which a profiled event should not be reported
     */
    public static final String MIN_TIME = "atlassian.profile.mintime";

    /**
     * System property that controls the threshhold time below which an entire stack of profiled events should
     * not be reported.
     */
    public static final String MIN_TOTAL_TIME = "atlassian.profile.mintotaltime";

    private static long configuredMinTime;
    private static long configuredMinTotalTime;

    public static void push(String name)
    {
        if (!isActive())
            return;

        //create a new timer and start it
        ProfilingTimerBean newTimer = new ProfilingTimerBean(name);
        newTimer.setStartTime();
        
        if (isProfileMemory())
            newTimer.setStartMem();

        //if there is a current timer - add the new timer as a child of it
        ProfilingTimerBean currentTimer = (ProfilingTimerBean) current.get();
        if (currentTimer != null)
        {
            currentTimer.addChild(newTimer);
        }

        //set the new timer to be the current timer
        current.set(newTimer);
    }

    public static void pop(String name)
    {
        if (!isActive())
            return;

        ProfilingTimerBean currentTimer = (ProfilingTimerBean) current.get();

        if (isProfileMemory())
            currentTimer.setEndMem();
        
        //if the timers are matched up with each other (ie push("a"); pop("a"));
        if (currentTimer != null && name != null && name.equals(currentTimer.getResource()))
        {
            currentTimer.setEndTime();
            ProfilingTimerBean parent = currentTimer.getParent();
            //if we are the root timer, then print out the times
            if (parent == null)
            {
                if (currentTimer.getTotalTime() > getMinTotalTime())
                    printTimes(currentTimer);

                current.set(null); //for those servers that use thread pooling
            }
            else
            {
                current.set(parent);
            }
        }
        else
        {
            //if timers are not matched up, then print what we have, and then print warning.
            if (currentTimer != null)
            {
                printTimes(currentTimer);
                current.set(null); //prevent printing multiple times
                System.out.println("Unmatched Timer.  Was expecting " + currentTimer.getResource() + ", instead got " + name);
            }
        }


    }

    private static void printTimes(ProfilingTimerBean currentTimer)
    {
        String printable = currentTimer.getPrintable(getMinTime());
        if (printable != null & !"".equals(printable.trim()))
        {
            logger.log(printable);
        }
    }

    private static long getMinTime()
    {
        return (configuredMinTime > 0) ? configuredMinTime : Long.getLong(MIN_TIME, 0).longValue();
    }

    private static long getMinTotalTime()
    {
        return (configuredMinTotalTime > 0) ? configuredMinTotalTime : Long.getLong(MIN_TOTAL_TIME, 0).longValue();
    }

    public static boolean isActive()
    {
        return "true".equalsIgnoreCase(System.getProperty(ACTIVATE_PROPERTY));
    }

    public static void setMinTime(long minTime)
    {
        configuredMinTime = minTime;
    }

    public static void setMinTotalTime(long minTotalTime)
    {
        configuredMinTotalTime = minTotalTime;
    }
    
    public static boolean isProfileMemory()
    {
        return "true".equalsIgnoreCase(System.getProperty(ACTIVATE_MEMORY_PROPERTY));
    }

    public static void setActive(boolean active)
    {
        if (active)
            System.setProperty(ACTIVATE_PROPERTY, "true");
        else
            System.setProperty(ACTIVATE_PROPERTY, "false");
    }

    public static void setProfileMemory(boolean active)
    {
        if (active)
            System.setProperty(ACTIVATE_MEMORY_PROPERTY, "true");
        else
            System.setProperty(ACTIVATE_MEMORY_PROPERTY, "false");
    }

    private static UtilTimerLogger logger = new UtilTimerLogger()
    {
        public void log(String s)
        {
            System.out.println(s);
        }
    };

    public static void setLogger(UtilTimerLogger logger)
    {
        UtilTimerStack.logger = logger;
    }

}
