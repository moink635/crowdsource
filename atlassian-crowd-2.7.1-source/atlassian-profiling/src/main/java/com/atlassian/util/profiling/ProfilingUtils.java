package com.atlassian.util.profiling;

public class ProfilingUtils
{
    /**
     * Get just the name of the class (without the package name)
     */
    public static String getJustClassName(Class clazz)
    {
        return getJustClassName(clazz.getName());
    }

    /**
     * Get just the name of the class (without the package name)
     */
    public static String getJustClassName(String name)
    {
        if (name.indexOf(".") >= 0)
            name = name.substring(name.lastIndexOf(".") + 1);

        return name;
    }
}
