package com.atlassian.util.profiling;

import java.util.*;

/**
 * Bean to contain information about the pages profiled
 *
 * @author <a href="mailto:mike@atlassian.com">Mike Cannon-Brookes</a>
 * @author <a href="mailto:scott@atlassian.com">Scott Farquhar</a>
 */
public class ProfilingTimerBean implements java.io.Serializable
{
    List children = new ArrayList();
    ProfilingTimerBean parent = null;

    String resource;

    long startTime;
    long totalTime;

    long startMem;
    long totalMem;
    boolean hasMem = false;

    public ProfilingTimerBean(String resource)
    {
        this.resource = resource;
    }

    protected void addParent(ProfilingTimerBean parent)
    {
        this.parent = parent;
    }

    public ProfilingTimerBean getParent()
    {
        return parent;
    }


    public void addChild(ProfilingTimerBean child)
    {
        children.add(child);
        child.addParent(this);
    }


    public void setStartTime()
    {
        this.startTime = System.currentTimeMillis();
    }

    public void setEndTime()
    {
        this.totalTime = System.currentTimeMillis() - startTime;
    }
    
    public void setStartMem()
    {
        this.startMem = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
        this.hasMem = true;
    }

    public void setEndMem()
    {
        this.totalMem = (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) - startMem;
    }

    public String getResource()
    {
        return resource;
    }

    /**
     * Get a formatted string representing all the methods that took longer than a specified time.
     */

    public String getPrintable(long minTime)
    {
        return getPrintable("", minTime);
    }

    protected String getPrintable(String indent, long minTime)
    {
        //only print the value if we are larger or equal to the min time.
        if (totalTime >= minTime)
        {
            StringBuffer buffer = new StringBuffer();
            buffer.append(indent);
            buffer.append("[" + totalTime + "ms] ");
            if (hasMem)
            {
                buffer.append("[" + totalMem / 1024 + "KB used] ");
                buffer.append("[" + Runtime.getRuntime().freeMemory() / 1024 + "KB Free] ");
            }

            buffer.append("- " + resource);
            buffer.append("\n");

            Iterator childrenIt = children.iterator();
            while (childrenIt.hasNext())
            {
                buffer.append(((ProfilingTimerBean) childrenIt.next()).getPrintable(indent + "  ", minTime));
            }

            return buffer.toString();
        }
        else
            return "";
    }

    public long getTotalTime()
    {
        return totalTime;
    }
}

