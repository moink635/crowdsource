#!/usr/bin/perl -w

# Parses the Crowd log to show breakdowns of how long synchronisation
#  took.
# Assumes only one synchronisation is taking place at a time

use strict;

use Date::Parse;

my @events;

sub recordEvent($$)
{
  my ($t, $expectedIndex) = @_;

  if (@events != $expectedIndex) {
    die "Expected event $expectedIndex, but was " . scalar(@events);
  }

  push @events, $t;
}

sub printEvent()
{
  my $o = shift @events;
  print $o;
  for my $t (@events) {
    print ',', sprintf('%0.03f', $t - $o);

    $o = $t;
  }
  print "\n";

  @events = ();
}

print "stamp,usersTime,groupsFindTime,groupsSyncTime,membershipsTime\n";

while (<>) {
  my ($stamp, $ms, $logger, $message) = /^(.{19}),(\d\d\d) .*\[(\S+)\] (.*)$/ or next;

  my $t = str2time($stamp) + ($ms / 1000);

#  print "$t, $logger, $message\n";

  if ($message =~ /synchronisation for directory .* starting/) {
    recordEvent($t, 0);
  } elsif ($message =~ /synchronised .* users in/) {
    recordEvent($t, 1);
  } elsif ($message =~ /found .* remote groups/) {
    recordEvent($t, 2);
  } elsif ($message =~ /synchronized .* groups in/) {
    recordEvent($t, 3);
  } elsif ($message =~ /FULL synchronisation complete/) {
    recordEvent($t, 4);
    printEvent();
  }
}
