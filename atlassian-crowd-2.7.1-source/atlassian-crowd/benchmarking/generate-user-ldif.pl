#!/usr/bin/perl -w

# Generate LDIF for a large directory

# The calls to generateOU define how many users;
#  tweak those numbers as appropriate.

use strict;

# By default, the schema will be for OpenLDAP.
# For Active Directory:
#   --groupclass=group --memberrdn=member --userclass=user --usernameattribute=sAMAccountName
#
# to configure the base DN:
#   --noincludebase --basedn dc=tpm,dc=atlassian,dc=com

use Getopt::Long;

my $DOMAIN = "dc=example,dc=com";

my $USER_CLASS='inetOrgPerson';

my $GROUP_CLASS = 'groupOfUniqueNames';
my $MEMBER_RDN = 'uniqueMember';

my $includebase = 1;

my $usernameAttribute;

GetOptions(
  "basedn=s" => \$DOMAIN,
  "includebase!" => \$includebase,
  "userclass=s" => \$USER_CLASS,
  "usernameattribute=s" => \$usernameAttribute,
  "groupclass=s" => \$GROUP_CLASS,
  "memberrdn=s" => \$MEMBER_RDN
) or die "Unable to parse arguments: $!";


my $OU_NAME = "loadTesting10k";
my $TOP_OU = "ou=${OU_NAME},${DOMAIN}";

if ($includebase) {
print <<EOH;
dn: $DOMAIN
objectClass: dcObject
objectClass: organization
dc: example
o: example

EOH
}

print <<EOH;
dn: ${TOP_OU}
objectClass: organizationalUnit
ou: ${OU_NAME}

EOH

my @allDns;

sub generateOU($$$$)
{
  my ($name, $users, $groups, $groupMembers) = @_;

  if ($groupMembers > $users) {
    die '$groupMembers > $users';
  }

  my $ouName = "childOU-${name}-${users}users";

  print <<EOG;
dn: ou=${ouName},${TOP_OU}
objectClass: organizationalUnit
ou: ${ouName}

EOG

my @dns;

  for (my $n = 0; $n < $users; $n++) {
    my $username = "${name}-user-$n";
    my $dn = "cn=${username},ou=${ouName},${TOP_OU}";
    print <<EOU;
dn: ${dn}
objectClass: ${USER_CLASS}
cn: ${username}
sn: User$n
givenName: ${name}Test
mail: ${name}-user-${n}\@example.com
userPassword:: cGFzc3dvcmQ=
EOU

    if ($usernameAttribute) {
        print "$usernameAttribute: ${username}\n";
    }

    print "\n";

    push @dns, $dn;
  }

  my $idx = 0;

  for (my $g = 0; $g < $groups; $g++) {
    my @members;
    for (my $member = 0; $member < $groupMembers; $member++) {
      push @members, $dns[$idx];
      $idx = ($idx + 1) % @dns;
    }

    my $cn = "${name}-group-${g}";
    generateGroup("cn=${cn},ou=${ouName},${TOP_OU}", $cn, @members);
  }

  push @allDns, @dns;
}

sub generateGroup($@)
{
  my ($dn, $cn, @members) = @_;

  print <<EOG;
dn: ${dn}
objectClass: ${GROUP_CLASS}
cn: ${cn}
EOG

  for my $dn (@members) {
    print <<EOM;
${MEMBER_RDN}: ${dn}
EOM
  }

  print "\n";
}

sub generateAllOU()
{
  generateGroup("cn=all-group,${TOP_OU}", 'all-group', @allDns);
}

# Generate a set ('A') with 4 users and 2 groups, each containing 2 of those
#  users
#generateOU('A', 4, 2, 2);

# Generate 10 thousand users, distributed among 1000 groups
generateOU('A', 5000, 500, 200);
generateOU('B', 2000, 200, 200);
generateOU('C', 2000, 200, 200);
generateOU('D', 1000, 100, 200);

# Generate a single group containing all users
generateAllOU();
