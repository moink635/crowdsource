package com.atlassian.crowd.pageobjects;

public class ViewProfilePage extends AbstractCrowdPage
{
    @Override
    public String getUrl()
    {
        return "/console/user/viewprofile.action";
    }
}
