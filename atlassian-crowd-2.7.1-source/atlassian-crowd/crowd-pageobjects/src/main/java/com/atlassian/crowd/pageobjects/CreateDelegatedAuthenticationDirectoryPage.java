package com.atlassian.crowd.pageobjects;

import java.net.MalformedURLException;

public class CreateDelegatedAuthenticationDirectoryPage extends CreateDirectoryPage
{
    @Override
    public String getUrl()
    {
        return "/console/secure/directory/createdelegated.action";
    }

    public ViewDelegatedDirectoryPage create() throws InterruptedException, MalformedURLException
    {
        return super.create(ViewDelegatedDirectoryPage.class);
    }

    public CreateDelegatedAuthenticationDirectoryPage populateDefaultAttributes()
    {
        return super.populateDefaultAttributes(CreateDelegatedAuthenticationDirectoryPage.class);
    }
}