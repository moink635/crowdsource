package com.atlassian.crowd.pageobjects;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.page.HomePage;
import com.atlassian.pageobjects.page.LoginPage;

/**
 * Page object implementation for the LoginPage in Crowd.
 */
public class CrowdLoginPage extends AbstractCrowdPage implements LoginPage
{
    private static final String URI = "/console/login.action";
    private static final String PAGE_TITLE = "Login to Crowd Console";

    @ElementBy (name = "j_username")
    private PageElement usernameField;

    @ElementBy (name = "j_password")
    private PageElement passwordField;

    @ElementBy (xpath = "//input[@type='submit']")
    private PageElement loginButton;

    @ElementBy(tagName = "h2")
    private PageElement title;

    protected void waitUntilContentLoad()
    {
        Poller.waitUntilTrue("Expected a title with text '" + PAGE_TITLE + "'", title.timed().hasText(PAGE_TITLE));
    }

    public String getUrl()
    {
        return URI;
    }

    /**
     * Logs in as a system administrator.
     *
     * @return the next page
     * @throws RuntimeException if the login wasn't successful
     */
    public CrowdConsole loginAsSysAdmin()
    {
        return loginAsSysAdmin(CrowdConsole.class);
    }

    /**
     * Logs in a system administrator with the given username and password.  Use this method only if the
     * authentication is expected to succeed.
     *
     * @param username the username
     * @param password the password
     * @return the next page
     * @throws RuntimeException if the login wasn't successful
     */
    public CrowdConsole login(String username, String password)
    {
        return login(username, password, CrowdConsole.class);
    }
    
    public <M extends Page> M loginAsSysAdmin(Class<M> nextPage)
    {
        return login("admin", "admin", nextPage);
    }

    /**
     * This method is provided for compatibility with the framework only,
     * as the client can't know for sure which nextPage is
     */
    public <M extends Page> M login(String username, String password, Class<M> nextPage)
    {
        usernameField.type(username);
        passwordField.type(password);

        loginButton.click();

        if (nextPage == null)
        {
            return null;
        }
        return HomePage.class.isAssignableFrom(nextPage) ? binder.bind(nextPage) : binder.navigateToAndBind(nextPage);
    }

}
