package com.atlassian.crowd.pageobjects;

public class DirectoryBrowserPage extends AbstractCrowdPage
{
    @Override
    public String getUrl()
    {
        return "/console/secure/directory/browse.action";
    }
}
