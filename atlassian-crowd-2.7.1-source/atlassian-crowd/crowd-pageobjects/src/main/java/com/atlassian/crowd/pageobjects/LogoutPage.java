package com.atlassian.crowd.pageobjects;

public class LogoutPage extends CrowdLoginPage
{
    private static final String URI = "/console/logoff.action";

    @Override
    public String getUrl()
    {
        return URI;
    }
}
