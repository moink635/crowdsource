package com.atlassian.crowd.pageobjects;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

import org.hamcrest.core.AnyOf;

import static org.hamcrest.core.Is.is;

public class ViewConnectorPage extends ViewDirectory
{
    @ElementBy(id = "synchroniseDirectoryButton")
    PageElement synchroniseButton;

    @ElementBy(id = "status")
    PageElement status;

    List<String> canSynchroniseStates = Arrays.asList("Full synchronisation completed successfully", "Not yet synchronised");

    // required by atlassian page objects to create a page when no query param is passed.
    public ViewConnectorPage()
    {
        super();
    }

    public ViewConnectorPage(String idParam)
    {
        super(idParam);
    }

    @Override
    public String getUrl()
    {
        return "/console/secure/directory/viewconnector.action?ID=" + getId();
    }

    public ViewConnectorPage synchronise()
    {
        if (canSynchroniseStates.contains(status.getText().trim()))
        {
            synchroniseButton.click();
        }

        /* Synchronisation of directories could potentially take a while. */
        Poller.waitUntil(status.timed().getText(),
                         AnyOf.anyOf(is("Full synchronisation completed successfully"), is("Synchronisation failed (see Crowd server logs for details)")),
                         Poller.by(TimeUnit.HOURS.toMillis(1)));
        return this;
    }
}