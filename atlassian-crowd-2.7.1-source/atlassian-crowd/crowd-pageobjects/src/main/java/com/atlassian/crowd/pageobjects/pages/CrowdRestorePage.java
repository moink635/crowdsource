package com.atlassian.crowd.pageobjects.pages;

import com.atlassian.crowd.pageobjects.AbstractCrowdPage;
import com.atlassian.crowd.pageobjects.CrowdLoginPage;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

public class CrowdRestorePage extends AbstractCrowdPage
{
    private final static String URL = "/console/secure/admin/restore!default.action";
    private final static String PAGE_TITLE = "Restore";

    @ElementBy(name = "importFilePath")
    protected PageElement filePathTextBox;

    @ElementBy(name = "import-submit")
    protected PageElement submitButton;

    @ElementBy(tagName = "h2")
    protected PageElement title;

    @ElementBy(className = "warningBox")
    protected PageElement errorBox;

    protected void waitUntilContentLoad()
    {
        Poller.waitUntilTrue("Expected a title with text '" + PAGE_TITLE + "'", title.timed().hasText(PAGE_TITLE));
    }

    public String getUrl()
    {
        return URL;
    }

    /**
     * Restores state from Crowd backup file. The current user will be logged out.
     * The method waits until the login page is displayed.
     * 
     * @param filename the backup filename. It is a full path, including directory names.
     * @return CrowdLoginPage the binded login page. A runtime exception is thrown if the import wasn't successful.
     */
    public CrowdLoginPage restoreExpectingSuccess(String filename)
    {
        filePathTextBox.type(filename);
        submitButton.click();
        
        return pageBinder.bind(CrowdLoginPage.class);
    }

    /**
     * Restores state from Crowd backup file, expecting a failure.
     * The method waits until the error message appears.
     * 
     * @param filename the backup filename. It is a full path, including directory names.
     * @return CrowdRestorePage the 'restore' page which displays the error. A runtime exception is thrown if the import was indeed successful.
     */
    public CrowdRestorePage restoreExpectingFailure(String filename)
    {
        filePathTextBox.type(filename);
        submitButton.click();
        
        Poller.waitUntilTrue(errorBox.timed().isPresent());
        return this;
    }
}
