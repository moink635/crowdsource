package com.atlassian.crowd.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;

import org.openqa.selenium.By;

public class ViewApplicationDirectoriesPage extends AbstractCrowdPage
{

    @ElementBy(id = "application-name")
    PageElement title;

    @ElementBy(xpath = "id('tab2')/div/form/div[2]/div/select")
    PageElement directorySelector;

    @ElementBy(id = "add-directory")
    PageElement add;

    @ElementBy(name = "directory2-allowAll")
    SelectElement allowAll;

    @ElementBy(xpath = "id('tab2')/div/form/div[2]/div/input")
    PageElement update;

    //required for atlassian-pageobjects
    public ViewApplicationDirectoriesPage()
    {
    }

    public ViewApplicationDirectoriesPage allowAllToAuthenticate()
    {
        driver.findElement(By.linkText("crowd")).click();

        waitUntilContentLoad();
        driver.findElement(By.id("application-directories")).click();

        add.click();
        waitUntilContentLoad();

        allowAll.type("True");
        update.click();

        return binder.bind(ViewApplicationDirectoriesPage.class);
    }

    @Override
    public String getUrl()
    {
        return "/console/secure/application/browse.action";
    }
}