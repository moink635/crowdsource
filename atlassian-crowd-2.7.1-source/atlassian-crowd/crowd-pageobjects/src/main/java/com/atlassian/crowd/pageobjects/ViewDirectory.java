package com.atlassian.crowd.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import com.google.common.base.Preconditions;

public abstract class ViewDirectory extends AbstractCrowdPage
{
    String idQueryParam;

    @ElementBy(id = "remove-directory")
    PageElement removeDirectoryLink;

    // required by atlassian page objects to create a page when no query param is passed.
    public ViewDirectory()
    {

    }

    public ViewDirectory(String idQueryParam)
    {
        Preconditions.checkNotNull(idQueryParam);
        this.idQueryParam = idQueryParam;
    }

    public String getId()
    {
        return idQueryParam;
    }

    public RemoveDirectoryPage delete()
    {
        removeDirectoryLink.click();
        return binder.bind(RemoveDirectoryPage.class, idQueryParam);
    }
}
