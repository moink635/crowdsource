package com.atlassian.crowd.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class ViewDelegatedDirectoryPage extends ViewDirectory
{
    @ElementBy(tagName = "h2")
    private PageElement title;

    @ElementBy(id = "delegated-connectiondetails")
    private PageElement connectionDetailsTab;

    @ElementBy(id = "importGroups")
    private PageElement importGroups;

    // Page structure is less than ideal and @ElementBy(name=) does not work.
    @ElementBy(xpath = "id('connectordetails')/div[2]/div/input[1]")
    private PageElement updateButton;

    // required by atlassian page objects to create a page when no query param is passed.
    public ViewDelegatedDirectoryPage()
    {
        super();
    }

    public ViewDelegatedDirectoryPage(String idQueryParam)
    {
        super(idQueryParam);
    }

    public ViewDelegatedDirectoryPage toggleImportGroups()
    {
        connectionDetailsTab.click();
        importGroups.click();
        updateButton.click();
        return this;
    }

    @Override
    public String getUrl()
    {
        return "/console/secure/directory/viewdelegated.action?ID="+ getId();
    }
}