package com.atlassian.crowd.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.page.HomePage;

import javax.inject.Inject;

/**
 * Page object implementation for the Dashboard page in Crowd.
 */
public class CrowdConsole extends AbstractCrowdPage implements HomePage<CrowdHeader>
{
    private static final String URI = "/console/secure/console.action";
    private static final String PAGE_TITLE = "Welcome to the Crowd Administration Console";
    
    @Inject
    PageBinder binder;

    @ElementBy(tagName = "h2")
    private PageElement title;
    
    protected void waitUntilContentLoad()
    {
        Poller.waitUntilTrue("Expected a title with text '" + PAGE_TITLE + "'", title.timed().hasText(PAGE_TITLE));
    }
    
    public String getUrl()
    {
        return URI;
    }
}
