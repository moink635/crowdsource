package com.atlassian.crowd.pageobjects;

import java.net.MalformedURLException;

public class CreateDirectoryConnectorPage extends CreateDirectoryPage
{
    @Override
    public String getUrl()
    {
        return "/console/secure/directory/createconnector.action";
    }

    public ViewConnectorPage create() throws InterruptedException, MalformedURLException
    {
        return super.create(ViewConnectorPage.class);
    }

    public CreateDirectoryConnectorPage populateDefaultAttributes()
    {
        return super.populateDefaultAttributes(CreateDirectoryConnectorPage.class);
    }
}