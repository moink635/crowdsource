package com.atlassian.crowd.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class RestoreFromXMLPage extends AbstractCrowdPage
{
    @ElementBy(id = "importFilePath")
    private PageElement importFilePath;

    // Page structure is less than ideal and @ElementBy(name=) does not work.
    @ElementBy(xpath = "id('content')/div/div/form/div[2]/div/input")
    private PageElement submitButton;

    @Override
    public String getUrl()
    {
        return "/console/secure/admin/restore.action";
    }

    public void restoreFromXml(String resourcePath)
    {
        importFilePath.type(resourcePath);
        submitButton.click();
    }
}
