package com.atlassian.crowd.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.component.Header;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.AtlassianWebDriver;

import javax.inject.Inject;

/**
 * PageObject representing the header of the pages
 */
public class CrowdHeader implements Header
{
    @Inject
    PageBinder pageBinder;

    @Inject
    AtlassianWebDriver driver;
    
    @ElementBy(id="userFullName")
    private PageElement usernameLabel;

    
    @ElementBy(partialLinkText="Log Out")
    private PageElement logoutLink;
    
    // The name of the user
    private String userFullName;

    @Init
    @SuppressWarnings("unused")
    private void init()
    {
        userFullName = usernameLabel.isPresent() ? usernameLabel.getText() : null;
    }

    public boolean isLoggedIn()
    {
        return userFullName != null;
    }
    
    public CrowdLoginPage logout()
    {
        if (isLoggedIn())
        {
            logoutLink.click();
        }
        // if the user is not logged in, the page is still the login page
        return pageBinder.bind(CrowdLoginPage.class);
    }
}
