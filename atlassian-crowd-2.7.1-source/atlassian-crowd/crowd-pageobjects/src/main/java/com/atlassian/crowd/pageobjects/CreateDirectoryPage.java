package com.atlassian.crowd.pageobjects;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.SelectElement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class CreateDirectoryPage extends AbstractCrowdPage
{
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @ElementBy(tagName = "h2")
    protected PageElement title;

    @ElementBy(id = "name")
    protected PageElement name;

    @ElementBy(id = "description")
    protected PageElement description;

    @ElementBy(id = "hreftab1")
    protected PageElement detailsTab;

    @ElementBy(id = "hreftab2")
    protected PageElement connectorTab;

    @ElementBy(id = "hreftab3")
    protected PageElement configurationTab;

    @ElementBy(id = "hreftab4")
    protected PageElement permissionsTab;

    @ElementBy(id = "URL")
    protected PageElement url;

    @ElementBy(id = "baseDN")
    protected PageElement baseDN;

    @ElementBy(id = "userDN")
    protected PageElement userDN;

    @ElementBy(id = "ldapPassword")
    protected PageElement ldapPassword;

    @ElementBy(id = "connector")
    protected SelectElement connector;

    // Page structure is less than ideal and @ElementBy(name=) does not work.
    @ElementBy(xpath = "id('tab2')/div/div[2]/div/input[1]")
    protected PageElement continueButton;

    @ElementBy(id = "readTimeoutInSec")
    protected PageElement readTimeout;

    @ElementBy(id = "searchTimeoutInSec")
    protected PageElement searchTimeout;

    <P> P populateDefaultAttributes(Class<P> returnClass)
    {
        name.type("LDAP Directory");
        description.type("Test directory");

        connectorTab.click();

        connector.type("OpenLDAP");  // Hack to get the select working
        url.clear();
        url.type("ldap://:8389/");
        baseDN.type("dc=example,dc=com");
        userDN.type("cn=admin,dc=example,dc=com");
        readTimeout.clear();
        readTimeout.type("600000");
        searchTimeout.clear();
        searchTimeout.type("600000");
        ldapPassword.type("secret");
        return binder.bind(returnClass);
    }

    <P> P create(Class<P> returnClass) throws MalformedURLException
    {
        continueButton.click();

        waitUntilPageLoad();

        String directoryID = getDirectoryID(new URL(driver.getCurrentUrl()).getQuery());
        return binder.bind(returnClass, directoryID);
    }

    protected String getDirectoryID(String currentUrl)
    {
        Pattern pattern = Pattern.compile("ID=([^,]+)");
        Matcher matcher = pattern.matcher(currentUrl);
        if (matcher.find())
        {
            return matcher.group(1);
        }
        else
        {
            logger.error("Directory creation failed" + driver.getPageSource());
            return "";
        }
    }
}