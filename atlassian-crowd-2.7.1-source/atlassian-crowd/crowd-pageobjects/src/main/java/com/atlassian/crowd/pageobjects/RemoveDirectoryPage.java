package com.atlassian.crowd.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class RemoveDirectoryPage extends AbstractCrowdPage
{
    private final String directoryId;

    // Page structure is less than ideal and @ElementBy(name=) does not work.
    @ElementBy(xpath = "id('content')/div/div[3]/div/form/input[3]")
    PageElement confirmButton;

    // required for the atlassian page objects
    public RemoveDirectoryPage()
    {
        this.directoryId = "";
    }

    public RemoveDirectoryPage(String directoryId)
    {
        this.directoryId = directoryId;
    }

    public DirectoryBrowserPage confirm()
    {
        confirmButton.click();
        waitUntilPageLoad();
        return binder.bind(DirectoryBrowserPage.class);
    }

    @Override
    public String getUrl()
    {
        return "/console/secure/directory/remove.action?ID=" + this.directoryId;
    }
}
