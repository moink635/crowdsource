package com.atlassian.crowd.pageobjects;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.AtlassianWebDriver;

import javax.inject.Inject;

/**
 * Base class for Crowd pages.
 */
public abstract class AbstractCrowdPage implements Page
{
    @Inject
    protected PageBinder binder;
    
    public CrowdHeader getHeader()
    {
        return binder.bind(CrowdHeader.class);
    }
    
    @Inject
    protected PageElementFinder finder;

    @Inject
    protected PageBinder pageBinder;

    @Inject
    protected AtlassianWebDriver driver;
    
    @ElementBy(id = "footer")
    protected PageElement footer;

    /**
     * Waits until the page has fully loaded.
     */
    @WaitUntil
    protected void waitUntilPageLoad()
    {
        waitUntilContentLoad();
        Poller.waitUntilTrue(footer.timed().isPresent());
    }

    /**
     * Waits until the content has loaded. Used in {@link #waitUntilPageLoad()}. Subclasses should override this
     * method to wait for a distinguishing content to appear. E.g. a page title.
     */
    protected void waitUntilContentLoad()
    {
        // default implementation does nothing
    }
}
