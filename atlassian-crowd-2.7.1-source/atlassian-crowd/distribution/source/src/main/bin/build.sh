#!/bin/sh

cd "`dirname "$0"`"

MAVEN_OPTS="-Xmx384m"
M2_HOME="$PWD/maven"

export MAVEN_OPTS M2_HOME
exec maven/bin/mvn --settings maven/conf/settings.xml clean install -Dsourcedistribution.skip -Dmaven.test.skip=true "$@"
