DROP DATABASE [crowd-integration-test]
-- DROP USER test
DROP LOGIN crowdtestuser
GO

CREATE LOGIN crowdtestuser WITH PASSWORD = 'test', CHECK_POLICY=OFF
CREATE DATABASE [crowd-integration-test]
GO

ALTER DATABASE [crowd-integration-test]
   SET READ_COMMITTED_SNAPSHOT ON
   WITH ROLLBACK IMMEDIATE;
GO

-- USE test
-- CREATE USER test FOR LOGIN test
-- GO

ALTER AUTHORIZATION ON DATABASE::[crowd-integration-test] TO crowdtestuser
GO
