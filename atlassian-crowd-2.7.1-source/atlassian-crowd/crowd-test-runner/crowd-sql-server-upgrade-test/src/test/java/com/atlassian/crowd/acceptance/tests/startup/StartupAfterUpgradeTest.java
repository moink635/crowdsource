package com.atlassian.crowd.acceptance.tests.startup;
import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;

import org.junit.Test;


public class StartupAfterUpgradeTest extends CrowdAcceptanceTestCase
{
    @Test(timeout = 10000)
    public void testEnsureAdminLoginSucceedsAfterUpgrade()
    {
        beginAt("console/login.action");
        
        /* Check for errors, as one is likely */
        String pageTitle = getElementTextByXPath("/html/head/title");
        
        if (pageTitle.contains("System Errors"))
        {
            fail("Startup error from Crowd: " + getElementTextByXPath("//table/tbody/tr/td[3]"));
        }

        assertFormPresent("login");
        setTextField("j_username", "admin");
        setTextField("j_password", "admin");
        submit();
        
        gotoPage("/");

        assertEquals("Admin Istrator", getElementTextByXPath("//span[@id='userFullName']"));
    }
}
