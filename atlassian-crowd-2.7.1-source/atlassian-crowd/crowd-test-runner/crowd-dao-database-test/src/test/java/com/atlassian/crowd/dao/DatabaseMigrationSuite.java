package com.atlassian.crowd.dao;

import com.atlassian.crowd.acceptance.tests.persistence.migration.CrowdDatabaseMigrationTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Contains integration tests for the database migration / upgrade tasks
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ CrowdDatabaseMigrationTest.class })
public class DatabaseMigrationSuite
{
}
