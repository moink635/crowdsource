package com.atlassian.crowd.dao;

import com.atlassian.crowd.acceptance.tests.persistence.dao.alias.AliasDAOHibernateTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.application.ApplicationDAOHibernateTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.directory.DirectoryDAOHibernateTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.group.GroupDAOHibernateTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.group.GroupDaoCRUDTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.group.GroupDaoSearchTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.membership.MembershipDAOHibernateTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.membership.MembershipDaoCRUDTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.membership.MembershipDaoSearchTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.property.PropertyDAOHibernateTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.token.TokenDAOHibernateTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.token.TokenDAOMemoryTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.user.UserDAOHibernateTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.user.UserDaoCRUDTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.user.UserDaoSearchTest;
import com.atlassian.crowd.acceptance.tests.persistence.dao.webhook.WebhookDaoCRUDTest;
import com.atlassian.crowd.acceptance.tests.persistence.directory.InternalDirectoryIntegrationTest;
import com.atlassian.crowd.acceptance.tests.persistence.importer.ImporterFactoryImplTest;
import com.atlassian.crowd.acceptance.tests.persistence.manager.application.ApplicationServiceIntegrationTest;
import com.atlassian.crowd.acceptance.tests.persistence.manager.directory.DirectoryManagerGenericNestedGroupsTest;
import com.atlassian.crowd.acceptance.tests.persistence.manager.token.SwitchableTokenManagerImplTest;
import com.atlassian.crowd.acceptance.tests.persistence.upgrade.UpgradeTask622ExternalIdIntegrationTest;
import com.atlassian.crowd.acceptance.tests.persistence.upgrade.UpgradeTask622IntegrationTest;
import com.atlassian.crowd.acceptance.tests.persistence.util.hibernate.MappingResourcesTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)

@SuiteClasses({
        AliasDAOHibernateTest.class,
        ApplicationDAOHibernateTest.class,
        DirectoryDAOHibernateTest.class,
        GroupDaoCRUDTest.class,
        GroupDaoSearchTest.class,
        GroupDAOHibernateTest.class,
        MembershipDaoCRUDTest.class,
        MembershipDaoSearchTest.class,
        MembershipDAOHibernateTest.class,
        PropertyDAOHibernateTest.class,
        TokenDAOHibernateTest.class,
        TokenDAOMemoryTest.class,
        UserDaoCRUDTest.class,
        UserDaoSearchTest.class,
        UserDAOHibernateTest.class,
        WebhookDaoCRUDTest.class,
        InternalDirectoryIntegrationTest.class,
        ImporterFactoryImplTest.class,
        ApplicationServiceIntegrationTest.class,
        DirectoryManagerGenericNestedGroupsTest.class,
        SwitchableTokenManagerImplTest.class,
        MappingResourcesTest.class,

        // upgrade tasks
        UpgradeTask622IntegrationTest.class,
        UpgradeTask622ExternalIdIntegrationTest.class

//        Too coupled to HSQL, and only a test of legacy behaviour
//        CrowdDatabaseMigrationTest.class
})
public class HibernateDaoSuite
{
}
