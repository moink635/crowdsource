package com.atlassian.crowd.dao;

import com.atlassian.crowd.acceptance.tests.persistence.migration.SchemaCreationIntegrationTest;
import com.atlassian.crowd.acceptance.tests.persistence.migration.SchemaUpgradeIntegrationTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * Tests that involve database schema changes.
 */
@RunWith(Suite.class)
@SuiteClasses({ SchemaCreationIntegrationTest.class,
                SchemaUpgradeIntegrationTest.class})
public class SchemaOperationsSuite
{
}
