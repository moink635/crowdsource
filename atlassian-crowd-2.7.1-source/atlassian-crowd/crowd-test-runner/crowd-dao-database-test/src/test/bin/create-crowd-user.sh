#!/bin/sh

# Create a schema for Crowd's tests
# Expects Oracle to be running locally

if [ -z "$ORACLE_CREDS" ]; then
  echo >&2 "Define ORACLE_CREDS as username/password"
  exit 10
fi

sqlplus "$ORACLE_CREDS@localhost:1521/XE" <<EOS

CREATE USER crowd IDENTIFIED BY crowd;
GRANT CONNECT, RESOURCE TO crowd;
EOS
