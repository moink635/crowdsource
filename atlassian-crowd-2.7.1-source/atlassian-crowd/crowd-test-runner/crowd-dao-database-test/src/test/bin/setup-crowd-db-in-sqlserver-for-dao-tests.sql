/*
Script to create a database called "crowd" with a login of "crowd" (password=crowd)
with the state required for DAO tests (i.e. tables exist, constraints set up).

On the server with the SQL Server installed, you can run it by cd'ing to the directory
where this file is and running:
> sqlcmd -S tcp:localhost,1433 -i setup-crowd-db-in-sqlserver-for-dao-tests.sql
or you can run it using Microsoft SQL Server Management Studio (open it, hit Ctrl+N, then
paste the contents of this file)
*/
if db_id('crowd') is not null
BEGIN
	PRINT N'Status: Closing connections other than this one';
	ALTER DATABASE[crowd] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
	PRINT N'Status: Dropping database crowd';
	DROP DATABASE [crowd]
END
ELSE
	PRINT N'Status: Database crowd does not exist so not trying to drop it';
GO

IF EXISTS
    (SELECT name
     FROM master.sys.server_principals
     WHERE name = 'crowd')
BEGIN
	PRINT N'Status: Dropping user crowd';
	DROP LOGIN crowd
END
ELSE
	PRINT N'Status: Login crowd does not exist so no need to drop it';
GO

PRINT N'Status: Creating database crowd and login crowd with password crowd';
GO
CREATE LOGIN crowd WITH PASSWORD = 'crowd', CHECK_POLICY=OFF
CREATE DATABASE [crowd] COLLATE Latin1_General_100_CS_AS_KS_WS
GO

PRINT N'Status: Setting crowd database isolation level to Read Committed with Row Versioning';
ALTER DATABASE crowd
	SET READ_COMMITTED_SNAPSHOT ON
	WITH ROLLBACK IMMEDIATE;
GO

PRINT N'Status: Giving login crowd access to database crowd';
GO
ALTER AUTHORIZATION ON DATABASE::[crowd] TO crowd
GO

PRINT N'Status: Script completed.';
GO
