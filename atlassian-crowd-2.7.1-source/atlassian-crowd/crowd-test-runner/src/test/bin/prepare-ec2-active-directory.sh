#!/bin/sh

set -e

DIR="`dirname "$0"`"

# Delete the old data
ldifde -d 'ou=loadTesting10k,dc=tpm,dc=atlassian,dc=com' -f current-entries.ldif
if [ -s current-entries.ldif ]; then
  python "$DIR"/make-deletion-ldif.py <current-entries.ldif >delete-current-entries.ldif
  ldifde -i -f delete-current-entries.ldif
fi

# Import the new data
"$DIR/../../../../benchmarking/generate-user-ldif.pl" >users.ldif --noincludebase --basedn dc=tpm,dc=atlassian,dc=com --groupclass=group --memberrdn=member --userclass=user --usernameattribute=sAMAccountName
ldifde -i -f users.ldif
