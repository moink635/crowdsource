#!/usr/bin/python

# Read an LDIF file and generate a modification
#  file to delete all entries, leaves first

from __future__ import print_function

import sys
import re

dnre = re.compile(r'dn:\s*(.*)', re.I)

dnList=[]

for l in sys.stdin:
  m = dnre.match(l)
  if m:
    dnList.append(m.group(1))

def leavesFirst(dn):
  return (-dn.count(','), dn)

dnList = sorted(set(dnList), key=leavesFirst)

for dn in dnList:
  print("DN: " + dn)
  print("changetype: delete")
  print()
