#!/bin/sh

set -e

DIR="`dirname "$0"`"

LDAP_ARGS="-H ldap://:389/ -D cn=admin,dc=example,dc=com -w secret"

# Set up an ec2 image so Crowd's tests can run against OpenLDAP

# Delete existing data
time ldapdelete $LDAP_ARGS -r 'dc=example,dc=com' || echo >&2 'Deletion failed; continuing to try adding new data.'

# Populate LDAP
time "$DIR/../../../../benchmarking/generate-user-ldif.pl" | ldapadd $LDAP_ARGS >/dev/null
