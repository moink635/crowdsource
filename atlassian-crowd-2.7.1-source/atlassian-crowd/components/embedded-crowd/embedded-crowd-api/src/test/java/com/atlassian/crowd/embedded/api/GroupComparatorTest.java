package com.atlassian.crowd.embedded.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GroupComparatorTest
{
    @Test
    public void testComparator() throws Exception
    {
        List<Group> groups = new ArrayList<Group>(3);
        groups.add(mockGroup("cats"));
        groups.add(mockGroup("ants"));
        groups.add(mockGroup("BATS"));

        // test sorting is case-insensitive
        Collections.sort(groups, GroupComparator.GROUP_COMPARATOR);
        assertEquals("ants", groups.get(0).getName());
        assertEquals("BATS", groups.get(1).getName());
        assertEquals("cats", groups.get(2).getName());
    }

    private static Group mockGroup(String name)
    {
        Group g = mock(Group.class);
        when(g.getName()).thenReturn(name);
        return g;
    }
}
