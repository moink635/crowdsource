package com.atlassian.crowd.embedded.api;

import com.google.common.collect.ImmutableList;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DirectoriesTest
{
    @Test
    public void nameFunctionShouldProjectDirectoryName()
    {
        Directory directory = mock(Directory.class);
        when(directory.getName()).thenReturn("dirname");

        assertThat(Directories.NAME_FUNCTION.apply(directory), is("dirname"));
    }

    @Test
    public void activeFilterShouldReturnTrueForActiveDirectories()
    {
        Directory directory = mock(Directory.class);
        when(directory.isActive()).thenReturn(true);

        assertThat(Directories.ACTIVE_FILTER.apply(directory), is(true));
    }

    @Test
    public void activeFilterShouldReturnFalseForInactiveDirectories()
    {
        Directory directory = mock(Directory.class);
        when(directory.isActive()).thenReturn(false);

        assertThat(Directories.ACTIVE_FILTER.apply(directory), is(false));
    }

    @Test
    public void namesOfShouldReturnDirectoryNames()
    {
        Directory dir1 = mock(Directory.class);
        Directory dir2 = mock(Directory.class);
        when(dir1.getName()).thenReturn("dir1");
        when(dir2.getName()).thenReturn("dir2");

        assertThat(Directories.namesOf(ImmutableList.of(dir1, dir2)), contains("dir1", "dir2"));
    }
}
