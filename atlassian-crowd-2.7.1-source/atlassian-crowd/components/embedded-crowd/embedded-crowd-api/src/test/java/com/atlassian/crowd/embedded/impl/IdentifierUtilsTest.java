package com.atlassian.crowd.embedded.impl;

import org.junit.Test;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.compareToInLowerCase;
import static com.atlassian.crowd.embedded.impl.IdentifierUtils.equalsInLowerCase;
import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class IdentifierUtilsTest
{
    @Test
    public void testToLowerCaseEqualInEnglish()
    {
        runTestToLowerCase("en", "THIS IS TEST", "this is test");
    }

    @Test
    public void testToLowerCaseEqualInTurkish()
    {
        runTestToLowerCase("tr", "THIS IS TEST", "thıs ıs test");
    }

    @Test
    public void testToLowerCaseHandlesNull()
    {
        assertNull(IdentifierUtils.toLowerCase(null));
    }

    @Test
    public void testCompareToInLowerCaseInEnglish()
    {
        runTestCompareToInLowerCase("en", "THIS IS TESs", "this is test", -1);
    }

    @Test
    public void testCompareToInLowerCaseInTurkish()
    {
        runTestCompareToInLowerCase("tr", "THIS IS TESs", "this is test", 200);
    }

    @Test
    public void testEqualsInLowerCaseInEnglish()
    {
        runTestEqualsInLowerCase("en", "THIS IS TEST", "this is test");
    }

    @Test
    public void testEqualsInLowerCaseInTurkish()
    {
        runTestEqualsInLowerCase("tr", "THIS IS TEST", "thıs ıs test");
    }

    @Test
    public void equalsInLowerCaseAcceptsNullsAsEqual()
    {
        assertTrue(IdentifierUtils.equalsInLowerCase(null, null));
    }

    @Test
    public void equalsInLowerCaseNothingEqualsNull()
    {
        assertFalse(IdentifierUtils.equalsInLowerCase("", null));
        assertFalse(IdentifierUtils.equalsInLowerCase(null, ""));
    }

    private static void withCrowdIdentifierLanguage(String lang, Runnable r)
    {
        String before = System.getProperty("crowd.identifier.language");
        try
        {
            System.setProperty("crowd.identifier.language", lang);
            IdentifierUtils.prepareIdentifierCompareLocale();
            r.run();
        }
        finally
        {
            if (before != null)
            {
                System.setProperty("crowd.identifier.language", before);
            }
            else
            {
                System.clearProperty("crowd.identifier.language");
            }
            IdentifierUtils.prepareIdentifierCompareLocale();
        }
    }

    private void runTestToLowerCase(String language, final String original, final String lowercase)
    {
        withCrowdIdentifierLanguage(language, new Runnable() {
            @Override
            public void run()
            {
                assertEquals(lowercase, toLowerCase(original));
            }
        });
    }

    private void runTestCompareToInLowerCase(String language, final String identifier1, final String identifier2, final int expectedResult)
    {
        withCrowdIdentifierLanguage(language, new Runnable() {
            @Override
            public void run()
            {
                assertEquals(expectedResult, compareToInLowerCase(identifier1, identifier2));
            }
        });
    }

    private void runTestEqualsInLowerCase(String language, final String identifier1, final String identifier2)
    {
        withCrowdIdentifierLanguage(language, new Runnable() {
            @Override
            public void run()
            {
                assertTrue(equalsInLowerCase(identifier1, identifier2));
            }
        });
    }

    @Test
    public void testHasLeadingOrTrailingWhitespaceNoSpace()
    {
        assertFalse(IdentifierUtils.hasLeadingOrTrailingWhitespace(""));
        assertFalse(IdentifierUtils.hasLeadingOrTrailingWhitespace("x"));
        assertFalse(IdentifierUtils.hasLeadingOrTrailingWhitespace("x y"));
    }

    @Test
    public void testHasLeadingOrTrailingWhitespaceWithSpace()
    {
        assertTrue(IdentifierUtils.hasLeadingOrTrailingWhitespace(" a"));
        assertTrue(IdentifierUtils.hasLeadingOrTrailingWhitespace("a "));
        assertTrue(IdentifierUtils.hasLeadingOrTrailingWhitespace(" a "));
    }

    @Test(expected = NullPointerException.class)
    public void whitespaceTestDoesNotAcceptNulls()
    {
        IdentifierUtils.hasLeadingOrTrailingWhitespace(null);
    }
}
