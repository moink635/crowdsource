package com.atlassian.crowd.embedded.api;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PasswordCredentialTest
{
    @Test
    public void equalityShouldMatchPasswords()
    {
        assertTrue(new PasswordCredential("password").equals(new PasswordCredential("password")));
    }

    @Test
    public void equalityShouldNotMatchDifferentPasswords()
    {
        PasswordCredential a = new PasswordCredential("password"),
                b = new PasswordCredential("wrong-password");

        assertFalse(a.equals(b));
        assertFalse(b.equals(a));
    }

    @Test
    public void equalityShouldNotMatchEncryptedWithNonEncryptedPasswords()
    {
        PasswordCredential a = new PasswordCredential("password", true),
                b = new PasswordCredential("password", false);

        assertFalse(a.equals(b));
        assertFalse(b.equals(a));
    }

    @Test
    public void copyConstructorCopiesFields()
    {
        PasswordCredential a = new PasswordCredential("password", true);

        assertEquals(a, new PasswordCredential(a));
    }

    @Test(expected = NullPointerException.class)
    public void copyConstructorFailsOnNulls()
    {
        new PasswordCredential((PasswordCredential) null);
    }
}
