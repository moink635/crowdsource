package com.atlassian.crowd.embedded.api;

public enum DirectoryType
{
    UNKNOWN,
    INTERNAL,
    /**
     * LDAP directories
     */
    CONNECTOR,
    CUSTOM,
    DELEGATING,
    /**
     * Remote crowd directory
     */
    CROWD
}
