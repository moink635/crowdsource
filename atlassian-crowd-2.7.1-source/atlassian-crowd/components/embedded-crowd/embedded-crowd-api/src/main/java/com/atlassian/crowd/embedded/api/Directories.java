package com.atlassian.crowd.embedded.api;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

/**
 * Utility class for {@link Directory}
 *
 * @since v2.7
 */
public final class Directories
{
    /**
     * A function that projects the directory name
     */
    public static final Function<Directory,String> NAME_FUNCTION = new Function<Directory, String>()
    {
        @Override
        public String apply(Directory directory)
        {
            return directory.getName();
        }
    };

    /**
     * A predicate that matches directories that are active
     */
    public static final Predicate<Directory> ACTIVE_FILTER = new Predicate<Directory>()
    {
        @Override
        public boolean apply(Directory directory)
        {
            return directory.isActive();
        }
    };

    private Directories()
    {
        // I'm a utility class. Please use my static methods, but don't instantiate me
    }

    /**
     * Transforms directories into their names.
     *
     * @param directories some directories
     * @return their names
     */
    public static Iterable<String> namesOf(Iterable<? extends Directory> directories)
    {
        return Iterables.transform(directories, NAME_FUNCTION);
    }
}
