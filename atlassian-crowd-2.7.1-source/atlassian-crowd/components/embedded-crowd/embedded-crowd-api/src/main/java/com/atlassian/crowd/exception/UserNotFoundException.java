package com.atlassian.crowd.exception;

/**
 * Thrown when the specified user could not be found.
 */
public class UserNotFoundException extends ObjectNotFoundException
{
    private final String userName;

    public UserNotFoundException(String userName)
    {
        this(userName, null);
    }

    public UserNotFoundException(String userName, Throwable t)
    {
        super(String.format("User <%s> does not exist", userName), t);
        this.userName = userName;
    }

    /**
     * Returns the name of the user that could not be found.
     *
     * @return name of the user that could not be found
     */
    public String getUserName()
    {
        return userName;
    }

    /**
     * Static factory to throw a UserNotFoundException when searching by externalId rather than username.
     *
     * @param externalId the external Id
     * @throws UserNotFoundException always
     */
    public static void throwNotFoundByExternalId(String externalId) throws UserNotFoundException
    {
        throw new UserNotFoundException("externalId=" + externalId);
    }
}
