package com.atlassian.crowd.exception;

import javax.annotation.Nullable;

/**
 * Thrown when the supplied credential is not valid.
 */
public class InvalidCredentialException extends CrowdException
{
    @Nullable
    private final String policyDescription;

    public InvalidCredentialException()
    {
        this.policyDescription = null;
    }

    public InvalidCredentialException(String message)
    {
        super(message);
        this.policyDescription = null;
    }

    /**
     * Use this constructor when you can identify a specific policy that has been violated.
     * If the policy is not known, use one of the other constructors.
     *
     * @param genericMessage a general message describing how this exception happened
     * @param policyDescription a message describing the policy that has been violated
     */
    public InvalidCredentialException(String genericMessage, @Nullable String policyDescription)
    {
        super(policyDescription == null ? genericMessage : genericMessage + ": " + policyDescription);
        this.policyDescription = policyDescription;
    }

    public InvalidCredentialException(String message, Throwable cause)
    {
        super(message, cause);
        this.policyDescription = null;
    }

    /**
     * Default constructor.
     *
     * @param throwable the {@link Exception Exception}.
     */
    public InvalidCredentialException(Throwable throwable)
    {
        super(throwable);
        this.policyDescription = null;
    }

    /**
     * @return a description of the policy that has been violated, if available. If such description is
     * not available, this method returns null. In that case, refer to {@link #getMessage()} for a general
     * description of the exception.
     */
    @Nullable
    public String getPolicyDescription()
    {
        return policyDescription;
    }
}