package com.atlassian.crowd.embedded.spi;


/**
 * Defines an interface that allows the host application to control the behaviour of the Crowd API.
 */
public interface HostApplicationControl
{
}
