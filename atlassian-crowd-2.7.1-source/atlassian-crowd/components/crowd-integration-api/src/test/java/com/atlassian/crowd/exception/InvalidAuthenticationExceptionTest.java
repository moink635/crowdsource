package com.atlassian.crowd.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.hamcrest.CoreMatchers;
import org.hamcrest.core.StringContains;
import org.junit.Test;

import static org.junit.Assert.assertThat;

public class InvalidAuthenticationExceptionTest
{
    @Test
    public void nullsAreNotIncludingInResultingException()
    {
        Throwable t = new Throwable("ASCII NUL: \u0000");
        t.fillInStackTrace();

        InvalidAuthenticationException e = InvalidAuthenticationException.newInstanceWithNameAndDescriptionFromCause("", t);

        assertThat(e.getMessage(), CoreMatchers.not(StringContains.containsString("\u0000")));

        StringWriter sw = new StringWriter();
        e.printStackTrace(new PrintWriter(sw));
        assertThat(sw.toString(), CoreMatchers.not(StringContains.containsString("\u0000")));
    }
}
