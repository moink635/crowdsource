package com.atlassian.crowd.model.group;

import com.google.common.collect.ImmutableList;

import org.junit.Test;

import static com.atlassian.crowd.model.group.Groups.namesOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GroupsTest
{
    @Test
    public void nameFunctionShouldProjectTheGroupName()
    {
        Group group = mock(Group.class);
        when(group.getName()).thenReturn("groupname");

        assertThat(Groups.NAME_FUNCTION.apply(group), is("groupname"));
    }

    @Test
    public void namesOfShouldReturnGroupNames()
    {
        Group group1 = mock(Group.class);
        Group group2 = mock(Group.class);
        when(group1.getName()).thenReturn("group1");
        when(group2.getName()).thenReturn("group2");

        assertThat(namesOf(ImmutableList.of(group1, group2)), contains("group1", "group2"));
    }
}
