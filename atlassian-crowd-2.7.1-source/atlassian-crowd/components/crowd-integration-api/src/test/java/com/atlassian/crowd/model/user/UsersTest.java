package com.atlassian.crowd.model.user;

import com.google.common.collect.ImmutableList;

import org.junit.Test;

import static com.atlassian.crowd.model.user.Users.namesOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UsersTest
{
    @Test
    public void nameFunctionShouldProjectUserName()
    {
        User user = mock(User.class);
        when(user.getName()).thenReturn("username");

        assertThat(Users.NAME_FUNCTION.apply(user), is("username"));
    }

    @Test
    public void namesOfShouldReturnUserNames()
    {
        User user1 = mock(User.class);
        User user2 = mock(User.class);
        when(user1.getName()).thenReturn("user1");
        when(user2.getName()).thenReturn("user2");

        assertThat(namesOf(ImmutableList.of(user1, user2)), contains("user1", "user2"));
    }
}
