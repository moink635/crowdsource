package com.atlassian.crowd.model.application;

import com.google.common.collect.ImmutableList;

import org.junit.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ApplicationsTest
{
    @Test
    public void nameFunctionShouldProjectApplicationName()
    {
        Application application = mock(Application.class);
        when(application.getName()).thenReturn("appname");

        assertThat(Applications.NAME_FUNCTION.apply(application), is("appname"));
    }

    @Test
    public void namesOfShouldReturnApplicationNames()
    {
        Application application1 = mock(Application.class);
        Application application2 = mock(Application.class);
        when(application1.getName()).thenReturn("app1");
        when(application2.getName()).thenReturn("app2");

        assertThat(Applications.namesOf(ImmutableList.of(application1, application2)), contains("app1", "app2"));
    }
}
