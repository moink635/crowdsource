package com.atlassian.crowd.exception;

import com.atlassian.crowd.model.webhook.Webhook;

/**
 * Thrown to indicate that a Webhook does not exist on the server
 *
 * @since v2.7
 */
public class WebhookNotFoundException extends ObjectNotFoundException
{
    public WebhookNotFoundException(long webhookId)
    {
        super(Webhook.class, "Webhook <" + webhookId + "> not found");
    }

    public WebhookNotFoundException(long applicationId, String endpointUrl)
    {
        super(Webhook.class, "applicationId=" + applicationId + ",endpointUrl=" + endpointUrl);
    }

    public WebhookNotFoundException(String message)
    {
        super(message);
    }
}
