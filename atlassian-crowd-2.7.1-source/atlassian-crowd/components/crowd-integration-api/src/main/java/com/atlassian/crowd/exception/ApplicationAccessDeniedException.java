package com.atlassian.crowd.exception;

/**
 * Thrown to indicate that a user does not have access to authenticate against an application.
 */
public class ApplicationAccessDeniedException extends Exception
{
    public ApplicationAccessDeniedException()
    {
        super();
    }

    public ApplicationAccessDeniedException(String s)
    {
        super(s);
    }

    public ApplicationAccessDeniedException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    public ApplicationAccessDeniedException(Throwable throwable)
    {
        super(throwable);
    }
}
