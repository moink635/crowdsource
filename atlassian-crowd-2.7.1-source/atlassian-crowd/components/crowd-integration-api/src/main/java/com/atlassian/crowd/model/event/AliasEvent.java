package com.atlassian.crowd.model.event;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.application.Application;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since 2.6.2
 */
public class AliasEvent implements OperationEvent
{
    private final Operation operation;
    private final Application application;
    private final String username;
    @Nullable private final String newAlias;

    private AliasEvent(Operation operation, Application application, String username, @Nullable String newAlias)
    {
        this.operation = checkNotNull(operation);
        this.application = checkNotNull(application);
        this.username = checkNotNull(username);
        this.newAlias = newAlias;
    }

    @Override
    public Operation getOperation()
    {
        return operation;
    }

    public Application getApplication()
    {
        return application;
    }

    public String getUsername()
    {
        return username;
    }

    /**
     * @return the new alias for CREATED and UPDATED events
     */
    @Nullable
    public String getNewAlias()
    {
        return newAlias;
    }

    /**
     * @return always return null, because aliases are not associated to any directory
     */
    @Override
    public Directory getDirectory()
    {
        return null;
    }

    public static AliasEvent created(Application application, String user, String newAlias)
    {
        return new AliasEvent(Operation.CREATED, application, user, newAlias);
    }

    public static AliasEvent updated(Application application, String user, String newAlias)
    {
        return new AliasEvent(Operation.UPDATED, application, user, newAlias);
    }

    public static AliasEvent deleted(Application application, String user)
    {
        return new AliasEvent(Operation.DELETED, application, user, null);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        AliasEvent that = (AliasEvent) o;

        if (!application.equals(that.application))
        {
            return false;
        }
        if (newAlias != null ? !newAlias.equals(that.newAlias) : that.newAlias != null)
        {
            return false;
        }
        if (operation != that.operation)
        {
            return false;
        }
        if (!username.equals(that.username))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = operation.hashCode();
        result = 31 * result + application.hashCode();
        result = 31 * result + username.hashCode();
        result = 31 * result + (newAlias != null ? newAlias.hashCode() : 0);
        return result;
    }
}
