package com.atlassian.crowd.model.group;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * Utility class for {@link Group}
 *
 * @since v2.7
 */
public final class Groups
{
    /**
     * A function that projects the group name
     */
    public static final Function<Group, String> NAME_FUNCTION = new Function<Group, String>()
    {
        @Override
        public String apply(Group group)
        {
            return group.getName();
        }
    };

    private Groups()
    {
        // I'm a utility class. Please use my static methods, but don't instantiate me
    }

    /**
     * Transforms groups into their names.
     *
     * @param groups some groups
     * @return their names
     */
    public static Iterable<String> namesOf(Iterable<? extends Group> groups)
    {
        return Iterables.transform(groups, NAME_FUNCTION);
    }
}
