package com.atlassian.crowd.model.user;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * Utility class for {@link User}
 *
 * @since v2.7
 */
public final class Users
{
    /**
     * A function that projects the user name
     */
    public static final Function<User,String> NAME_FUNCTION = new Function<User, String>()
    {
        @Override
        public String apply(User user)
        {
            return user.getName();
        }
    };

    private Users()
    {
        // I'm a utility class. Please use my static methods, but don't instantiate me
    }

    /**
     * Transforms users into their names.
     *
     * @param users some users
     * @return their names
     */
    public static Iterable<String> namesOf(Iterable<? extends User> users)
    {
        return Iterables.transform(users, NAME_FUNCTION);
    }
}
