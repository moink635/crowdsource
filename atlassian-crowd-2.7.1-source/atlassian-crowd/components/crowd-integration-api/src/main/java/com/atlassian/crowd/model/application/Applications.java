package com.atlassian.crowd.model.application;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

/**
 * Utility class for {@link Application}
 *
 * @since v2.7
 */
public final class Applications
{
    /**
     * A function that projects the application name
     */
    public static final Function<Application,String> NAME_FUNCTION = new Function<Application, String>()
    {
        @Override
        public String apply(Application application)
        {
            return application.getName();
        }
    };

    private Applications()
    {
        // I'm a utility class. Please use my static methods, but don't instantiate me
    }

    /**
     * Transforms applications into their names.
     *
     * @param applications some applications
     * @return their names
     */
    public static Iterable<String> namesOf(Iterable<? extends Application> applications)
    {
        return Iterables.transform(applications, NAME_FUNCTION);
    }
}
