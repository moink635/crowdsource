package com.atlassian.crowd.exception;

/**
 *
 * @since v2.7
 */
public class ObjectAlreadyExistsException extends CrowdException
{
    public ObjectAlreadyExistsException(String s)
    {
        super(s);
    }
}
