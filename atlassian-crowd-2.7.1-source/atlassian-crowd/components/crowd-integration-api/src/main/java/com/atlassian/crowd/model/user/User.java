package com.atlassian.crowd.model.user;

import com.atlassian.crowd.model.DirectoryEntity;

import java.security.Principal;

/**
 * Represents a user that exists in a directory.
 *
 * The order of the 'extends' statements is relevant, since {@link #getDirectoryId()} from {@link DirectoryEntity}
 * must shadow the deprecated method from {@link com.atlassian.crowd.embedded.api.User}.
 */
public interface User extends Principal, DirectoryEntity, com.atlassian.crowd.embedded.api.User
{
    String getFirstName();

    String getLastName();

    String getExternalId();

}
