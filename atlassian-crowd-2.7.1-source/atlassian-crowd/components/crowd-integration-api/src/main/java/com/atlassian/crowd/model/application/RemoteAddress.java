package com.atlassian.crowd.model.application;

import java.io.Serializable;

import org.apache.commons.lang3.Validate;

/**
 * Represents a valid IP address (IPv4, IPv6) or hostname for an Application
 */
public class RemoteAddress implements Serializable, Comparable<RemoteAddress>
{
    private String address;

    // Used by hibernate only
    private RemoteAddress()
    {
    }

    /**
     * Generates a RemoteAddress based on the value provided
     * @param address can be either a hostname or IP address (IPv4 or IPv6)
     *                An IPv4, IPv6 address can also have a mask defined in CIDR format
     *                Any input that is not recognised as IPv4 or IPv6 format will be treated as a hostname.
     */
    public RemoteAddress(String address)
    {
        Validate.notEmpty(address, "You cannot create a remote address with null address");
        this.address = address;
    }

    /**
     * Returns the address. The address could be a hostname, IPv4, IPv6 address or an IP address with a mask defined in
     * CIDR format.
     *
     * @return address
     */
    public String getAddress()
    {
        return address;
    }

    // Used by hibernate only
    private void setAddress(String address)
    {
        this.address = address;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RemoteAddress that = (RemoteAddress) o;

        if (address != null ? !address.equals(that.address) : that.address != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        return address != null ? address.hashCode() : 0;
    }

    public int compareTo(final RemoteAddress o)
    {
        return address.compareTo(o.getAddress());
    }

    @Override
    public String toString()
    {
        return "RemoteAddress{" +
               "address='" + address + '\'' +
               '}';
    }
}
