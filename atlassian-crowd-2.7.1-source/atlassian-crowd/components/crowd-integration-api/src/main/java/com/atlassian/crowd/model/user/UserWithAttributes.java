package com.atlassian.crowd.model.user;

/**
 * Represents a user with attributes that exists in a directory.
 *
 * The order of the 'extends' statements is relevant, since {@link #getDirectoryId()} from {@link User}
 * must shadow the deprecated method from {@link com.atlassian.crowd.embedded.api.User}.
 */
public interface UserWithAttributes extends User, com.atlassian.crowd.embedded.api.UserWithAttributes
{

}
    