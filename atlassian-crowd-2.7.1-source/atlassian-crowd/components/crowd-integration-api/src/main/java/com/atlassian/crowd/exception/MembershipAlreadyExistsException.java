package com.atlassian.crowd.exception;

/**
 * Thrown to indicate that a membership cannot be added because it already exists.
 *
 */
public class MembershipAlreadyExistsException extends ObjectAlreadyExistsException
{
    private final String childEntity;
    private final String parentEntity;

    public MembershipAlreadyExistsException(long directoryId, String childEntity, String parentEntity)
    {
        super("Membership already exists in directory [" + directoryId + "] from child entity [" + childEntity
              + "] to parent entity [" + parentEntity + "]");
        this.childEntity = childEntity;
        this.parentEntity = parentEntity;
    }

    public MembershipAlreadyExistsException(String childEntity, String parentEntity)
    {
        super("Membership already exists from child entity [" + childEntity + "] to parent entity ["
              + parentEntity + "]");
        this.childEntity = childEntity;
        this.parentEntity = parentEntity;
    }

    public String getChildEntity()
    {
        return childEntity;
    }

    public String getParentEntity()
    {
        return parentEntity;
    }
}
