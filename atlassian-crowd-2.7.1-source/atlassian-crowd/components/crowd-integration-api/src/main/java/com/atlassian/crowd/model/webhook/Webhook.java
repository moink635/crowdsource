package com.atlassian.crowd.model.webhook;

import java.util.Date;

import javax.annotation.Nullable;

import com.atlassian.crowd.model.application.Application;

/**
 * A Webhook is an application-provided HTTP endpoint that is pinged by Crowd to notify
 * the occurrence of certain events.
 *
 * @since v2.7
 */
public interface Webhook
{
    Long getId();

    String getEndpointUrl();

    Application getApplication();

    @Nullable
    String getToken();

    /**
     * @return Date of the last failed delivery that has not been followed by any successful delivery. May be null
     * if the last delivery was successful, or if no delivery has been attempted yet (i.e., new Webhooks).
     */
    @Nullable
    Date getOldestFailureDate();

    /**
     * @return Number of consecutive failed attempts to deliver the ping to the Webhook since the last successful
     * delivery, or since the Webhook was created. May be zero if the last delivery was successful, or if the Webhook
     * has just been created.
     */
    long getFailuresSinceLastSuccess();
}
