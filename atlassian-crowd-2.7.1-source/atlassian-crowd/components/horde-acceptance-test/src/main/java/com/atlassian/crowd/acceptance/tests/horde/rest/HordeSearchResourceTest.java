package com.atlassian.crowd.acceptance.tests.horde.rest;

import com.atlassian.crowd.acceptance.tests.rest.service.SearchResourceTest;

public class HordeSearchResourceTest extends SearchResourceTest
{
    public HordeSearchResourceTest(String name)
    {
        super(name, HordeRestServer.INSTANCE);
    }

    @Override
    public void testGetUserNames_Aliases()
    {
        // Horde does not support alias
    }

    @Override
    public void testGetUserNames_AliasesAll()
    {
        // Horde does not support alias
    }

    @Override
    public void testGetUserNames_AliasesIgnoreUsernameWhenAliasExists()
    {
        // Horde does not support alias
    }
}
