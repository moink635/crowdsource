package com.atlassian.crowd.acceptance.tests.horde.rest;

import com.atlassian.crowd.acceptance.tests.rest.service.GroupsResourceTest;

public class HordeGroupsResourceTest extends GroupsResourceTest
{
    public HordeGroupsResourceTest(String name)
    {
        super(name, HordeRestServer.INSTANCE);
    }
}
