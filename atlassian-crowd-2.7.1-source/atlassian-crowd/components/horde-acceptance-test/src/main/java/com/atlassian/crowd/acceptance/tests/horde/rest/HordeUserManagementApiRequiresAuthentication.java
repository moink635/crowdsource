package com.atlassian.crowd.acceptance.tests.horde.rest;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.atlassian.crowd.plugin.rest.entity.UserEntity;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class HordeUserManagementApiRequiresAuthentication
{

    private final HordeRestServer restServer = HordeRestServer.INSTANCE;

    @Test
    public void studioServiceShouldNotOpenUnauthenticatedUserManagementAPI() throws Exception
    {
        assertResourceDoesNotOpenUnauthenticatedUserManagementAPI("studio/latest");
    }

    @Test
    public void healthcheckServiceShouldNotOpenUnauthenticatedUserManagementAPI() throws Exception
    {
        assertResourceDoesNotOpenUnauthenticatedUserManagementAPI("healthcheck");
    }

    @Test
    public void ondemandLicenseServiceShouldNotOpenUnauthenticatedUserManagementAPI() throws Exception
    {
        assertResourceDoesNotOpenUnauthenticatedUserManagementAPI("ondemand/license/1.0");
    }

    /**
     * A misconfigured service config may expose all resources under the studio service. Because the studio service is
     * not authenticated, there's a risk it could expose all user management resources anonymously. Test that this hasn't
     * happened.
     */
    public void assertResourceDoesNotOpenUnauthenticatedUserManagementAPI(String service) throws Exception
    {
        URI uri = getServiceURIBuilder(service).path("user").queryParam("username", "{username}").build("admin");
        try
        {
            getWebResource(uri).get(UserEntity.class);
            fail(Response.Status.NOT_FOUND + " response expected");
        }
        catch (UniformInterfaceException e)
        {
            assertEquals(Response.Status.NOT_FOUND.getStatusCode(), e.getResponse().getStatus());
        }
    }

    private UriBuilder getServiceURIBuilder(String service) throws URISyntaxException
    {
        return UriBuilder.fromUri(restServer.getBaseUrl().toString()).path("rest").path(service);
    }


    private WebResource getWebResource(final URI uri)
    {
        Client client = Client.create();
        client = restServer.decorateClient(client);
        return client.resource(uri);
    }

}
