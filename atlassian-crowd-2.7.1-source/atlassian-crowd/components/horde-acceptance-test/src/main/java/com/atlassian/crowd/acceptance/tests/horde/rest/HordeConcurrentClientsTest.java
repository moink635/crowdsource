package com.atlassian.crowd.acceptance.tests.horde.rest;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.atlassian.crowd.acceptance.tests.rest.service.RestCrowdServiceAcceptanceTestCase;
import com.atlassian.crowd.plugin.rest.entity.AuthenticationContextEntity;
import com.atlassian.crowd.plugin.rest.entity.SessionEntity;
import com.atlassian.crowd.plugin.rest.entity.ValidationFactorEntity;
import com.atlassian.crowd.plugin.rest.entity.ValidationFactorEntityList;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import com.sun.jersey.api.client.WebResource;

import org.hamcrest.Matchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;

/**
 * This is a test for a race condition on the server (CWD-3568). It is not guaranteed to fail if the race
 * condition is present in the server, although my experience is that is was failing very consistently.
 * If it fails, then that means that we haven't really fixed CWD-3568. If it passes, then nothing has been proved.
 * This test is here to help identifying potential regressions, but it does not demonstrate the fix.
 */
public class HordeConcurrentClientsTest extends RestCrowdServiceAcceptanceTestCase
{
    private static final Logger logger = LoggerFactory.getLogger(HordeConcurrentClientsTest.class);
    private static final int CONCURRENT_CLIENTS = 2;
    private static final int TOTAL_REQUESTS = 2;
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";

    private ListeningExecutorService executor;

    public HordeConcurrentClientsTest(String name)
    {
        super(name, HordeRestServer.INSTANCE);
    }

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        executor = MoreExecutors.listeningDecorator(
                new ThreadPoolExecutor(CONCURRENT_CLIENTS, CONCURRENT_CLIENTS,
                        10, TimeUnit.SECONDS, new LinkedBlockingDeque<Runnable>(TOTAL_REQUESTS)));
    }

    @Override
    public void tearDown() throws Exception
    {
        executor.shutdown();
        super.tearDown();
    }

    public void testShouldAttendManyConcurrentClients() throws Exception
    {
        // kill all pre-existing sessions
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD)
                .path(SESSION_RESOURCE)
                .queryParam("username", USERNAME);
        webResource.delete();

        List<ListenableFuture<SessionEntity>> futures = Lists.newArrayList();
        for (int i = 0 ; i < TOTAL_REQUESTS ; i++)
        {
            futures.add(executor.submit(makeRequestToServletCallable));
        }

        List<SessionEntity> sessions = Futures.allAsList(futures).get();

        // all requests must succeed and return the same token
        assertThat(sessions, hasSize(TOTAL_REQUESTS));
        assertThat(sessions, not(Matchers.<SessionEntity>hasItem(nullValue(SessionEntity.class))));
        assertThat(ImmutableSet.copyOf(Lists.transform(sessions, GET_TOKENS)), hasSize(1));
    }

    private final Callable<SessionEntity> makeRequestToServletCallable = new Callable<SessionEntity>()
    {
        @Override
        public SessionEntity call()
        {
            try
            {
                ValidationFactorEntityList restFactors = new ValidationFactorEntityList(ImmutableList.<ValidationFactorEntity>of());
                AuthenticationContextEntity authContext = new AuthenticationContextEntity(USERNAME, PASSWORD, restFactors);

                WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);
                return webResource.entity(authContext, MT).post(SessionEntity.class);
            }
            catch (Exception e)
            {
                logger.error("REST request failed", e);
                return null;
            }
        }
    };

    private static final Function<SessionEntity,String> GET_TOKENS = new Function<SessionEntity, String>()
    {
        @Override
        public String apply(SessionEntity session)
        {
            return session.getToken();
        }
    };
}
