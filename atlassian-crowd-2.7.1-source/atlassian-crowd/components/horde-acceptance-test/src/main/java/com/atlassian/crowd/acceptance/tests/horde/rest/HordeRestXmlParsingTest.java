package com.atlassian.crowd.acceptance.tests.horde.rest;

import com.atlassian.crowd.acceptance.tests.rest.RestXmlParsingTest;

import net.sourceforge.jwebunit.exception.TestingEngineResponseException;

public class HordeRestXmlParsingTest extends RestXmlParsingTest
{
    public HordeRestXmlParsingTest()
    {
        super(HordeRestServer.INSTANCE);
    }

    @Override
    public void beginAt(String aRelativeURL) throws TestingEngineResponseException
    {
        // no need to go anywhere in the UI
    }
}
