package com.atlassian.crowd.acceptance.tests.horde.rest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
    HordeConcurrentClientsTest.class,
    HordeTokenResourceTest.class,
    HordeAuthenticationResourceTest.class,
    HordeGroupsResourceTest.class,
    HordeUsersResourceTest.class,
    HordeSearchResourceTest.class,
//    ApplicationResourceTest.class,
    HordeCookieConfigResourceTest.class,
    HordeWebhooksResourceTest.class,
    HordeEventsResourceTest.class,
    HordeRestXmlParsingTest.class,
    HordeCrowdWadlResourceTest.class,
    HordeRepresentationXmlTest.class,
    HordeRepresentationJsonTest.class,
    OnDemandDummyResourceTest.class,
    HordeHealthCheckResourceTest.class,
    HordeUserManagementApiRequiresAuthentication.class
    })
public class HordeRestAcceptanceTestHarness
{
}
