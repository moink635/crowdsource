package com.atlassian.crowd.acceptance.tests.horde.rest;

import java.io.IOException;

import com.atlassian.crowd.acceptance.tests.rest.service.RepresentationJsonTest;

public class HordeRepresentationJsonTest extends RepresentationJsonTest
{
    public HordeRepresentationJsonTest(String name) throws IOException
    {
        super(name, HordeRestServer.INSTANCE);
    }

    @Override
    public void testCookieConfig() throws Exception
    {
        // resource not supported
    }
}
