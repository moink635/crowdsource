package com.atlassian.crowd.acceptance.tests.horde.rest;

import java.io.IOException;

import com.atlassian.crowd.acceptance.tests.rest.service.RepresentationXmlTest;

public class HordeRepresentationXmlTest extends RepresentationXmlTest
{
    public HordeRepresentationXmlTest(String name) throws IOException
    {
        super(name, HordeRestServer.INSTANCE);
    }

    @Override
    public void testCookieConfig() throws Exception
    {
        // resource not supported
    }
}
