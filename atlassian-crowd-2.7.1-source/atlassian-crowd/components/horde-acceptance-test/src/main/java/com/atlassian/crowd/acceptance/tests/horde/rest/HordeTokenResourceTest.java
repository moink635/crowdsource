package com.atlassian.crowd.acceptance.tests.horde.rest;

import com.atlassian.crowd.acceptance.tests.rest.service.TokenResourceTest;

public class HordeTokenResourceTest extends TokenResourceTest
{
    public HordeTokenResourceTest(String name)
    {
        super(name, HordeRestServer.INSTANCE);
    }

    @Override
    protected void setAliasForUsername(String alias)
    {
        // Horde does not support alias
    }

    @Override
    public void testAliasChangesAreReflectedInSessionUsername()
    {
        // Horde does not support alias
    }

    @Override
    public void testAliasedApplicationAlsoAcceptsUnaliasedUsername()
    {
        // Horde does not support alias
    }

    @Override
    public void testSessionCreatedThroughAliasedApplicationShowsOriginalNameWhenRetrievedWithNonAliasingApplication()
    {
        // Horde does not support alias
    }

    @Override
    public void testSessionCreatedThroughAliasWithDifferentCaseReturnsCorrectCase()
    {
        // Horde does not support alias
    }

    @Override
    public void testSessionCreatedWithUnaliasedApplicationShowsAliasedNameWhenRetrievedWithAliasingApplication()
    {
        // Horde does not support alias
    }

    @Override
    public void testSessionCreatedWithUnaliasedApplicationShowsAliasedNameWhenValidatedWithAliasingApplication()
    {
        // Horde does not support alias
    }

    @Override
    public void testSessionForAliasedApplicationIncludesUnaliasedUsernameAsAnExtraField()
    {
        // Horde does not support alias
    }
}
