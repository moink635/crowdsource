package com.atlassian.crowd.acceptance.tests.horde.rest;

import com.atlassian.crowd.acceptance.tests.rest.service.CrowdWadlResourceTest;

public class HordeCrowdWadlResourceTest extends CrowdWadlResourceTest
{
    public HordeCrowdWadlResourceTest(String name)
    {
        super(name, HordeRestServer.INSTANCE);
    }

    @Override
    public void testShouldGetAWadlRepresentationForAppManagement()
    {
        // AppManagement not supported in Horde
    }
}
