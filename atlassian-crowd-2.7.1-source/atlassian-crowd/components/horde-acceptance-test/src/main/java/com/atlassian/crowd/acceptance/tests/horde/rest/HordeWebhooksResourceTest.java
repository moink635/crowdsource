package com.atlassian.crowd.acceptance.tests.horde.rest;

import com.atlassian.crowd.acceptance.tests.rest.service.WebhooksResourceTest;

public class HordeWebhooksResourceTest extends WebhooksResourceTest
{
    public HordeWebhooksResourceTest(String name)
    {
        super(name, HordeRestServer.INSTANCE);
    }
}
