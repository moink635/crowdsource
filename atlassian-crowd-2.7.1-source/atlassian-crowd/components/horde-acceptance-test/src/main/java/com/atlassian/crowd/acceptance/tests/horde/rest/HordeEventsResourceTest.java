package com.atlassian.crowd.acceptance.tests.horde.rest;

import com.atlassian.crowd.acceptance.tests.rest.service.EventsResourceTest;

public class HordeEventsResourceTest extends EventsResourceTest
{
    public HordeEventsResourceTest(String name)
    {
        super(name, HordeRestServer.INSTANCE);
    }
}
