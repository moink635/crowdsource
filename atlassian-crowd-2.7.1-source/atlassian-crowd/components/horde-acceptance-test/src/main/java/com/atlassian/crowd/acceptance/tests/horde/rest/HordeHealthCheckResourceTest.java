package com.atlassian.crowd.acceptance.tests.horde.rest;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static java.net.HttpURLConnection.HTTP_UNAVAILABLE;
import static org.junit.Assert.assertEquals;

public class HordeHealthCheckResourceTest
{
    private static final String SERVICE_NAME = "healthcheck";
    private static final String RESOURCE_NAME = "checkDetails";

    private final HordeRestServer restServer = HordeRestServer.INSTANCE;

    @Before
    public void setUp() throws Exception
    {
        restServer.before();
    }

    @After
    public void tearDown() throws Exception
    {
        restServer.after();
    }

    @Test
    public void testHealthCheckFailsByDefault() throws Exception
    {
        URI statusResourceUri = getStatusResourceURI();
        ClientResponse response = getWebResource(statusResourceUri).get(ClientResponse.class);
        // Expect failure: there is no sysadmin in test data set
        assertEquals(HTTP_UNAVAILABLE, response.getStatus());
    }

    private URI getStatusResourceURI() throws URISyntaxException
    {
        return getServiceURIBuilder().path(RESOURCE_NAME).build();
    }

    private UriBuilder getServiceURIBuilder() throws URISyntaxException
    {
        return UriBuilder.fromUri(restServer.getBaseUrl().toString()).path("rest").path(SERVICE_NAME);
    }

    private WebResource getWebResource(final URI uri)
    {
        Client client = Client.create();
        client = restServer.decorateClient(client);
        return client.resource(uri);
    }

}
