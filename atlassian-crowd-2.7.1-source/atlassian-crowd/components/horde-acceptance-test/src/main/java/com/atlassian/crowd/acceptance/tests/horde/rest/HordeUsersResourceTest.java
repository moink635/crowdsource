package com.atlassian.crowd.acceptance.tests.horde.rest;

import com.atlassian.crowd.acceptance.tests.rest.service.UsersResourceTest;

public class HordeUsersResourceTest extends UsersResourceTest
{
    public HordeUsersResourceTest(String name)
    {
        super(name, HordeRestServer.INSTANCE);
    }
}
