package com.atlassian.crowd.acceptance.tests.horde.rest;

import com.atlassian.crowd.acceptance.tests.rest.service.AuthenticationResourceTest;

public class HordeAuthenticationResourceTest extends AuthenticationResourceTest
{
    public HordeAuthenticationResourceTest(String name)
    {
        super(name, HordeRestServer.INSTANCE);
    }
}
