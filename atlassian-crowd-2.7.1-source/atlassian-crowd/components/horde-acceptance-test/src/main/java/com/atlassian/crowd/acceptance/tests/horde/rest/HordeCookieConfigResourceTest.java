package com.atlassian.crowd.acceptance.tests.horde.rest;

import com.atlassian.crowd.acceptance.tests.rest.service.CookieConfigResourceTest;

public class HordeCookieConfigResourceTest extends CookieConfigResourceTest
{
    public HordeCookieConfigResourceTest(String name)
    {
        super(name, HordeRestServer.INSTANCE);
    }

    @Override
    protected String getExpectedCookieName()
    {
        return "studio.crowd.tokenkey"; // default value in OD, unless -Dcookie.tokenkey is specified
    }
}
