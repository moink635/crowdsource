package com.atlassian.crowd.acceptance.tests.horde.rest;

import java.net.MalformedURLException;
import java.net.URL;

import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.horde.tenantsetup.Config;
import com.atlassian.crowd.horde.tenantsetup.Main;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.test.jdbc.JdbcTestUtils;

import liquibase.exception.LiquibaseException;

public class HordeRestServer implements RestServer
{
    private static final Logger logger = LoggerFactory.getLogger(HordeRestServer.class);

    public static final HordeRestServer INSTANCE = new HordeRestServer();

    private static final String JDBC_URL = System.getProperty("horde.jdbc.url",
                                                              "jdbc:hsqldb:http://localhost:8096/hsqldb/");
    private static final String JDBC_USERNAME = System.getProperty("horde.jdbc.username", "sa");
    private static final String JDBC_PASSWORD = System.getProperty("horde.jdbc.password", "");
    private static final String JDBC_DRIVER = System.getProperty("horde.jdbc.driver", "org.hsqldb.jdbcDriver");
    private static final String APPLICATION_PASSWORD = System.getProperty("horde.product.application.password", "qybhDMZh");
    private static final String BASE_URL =
        System.getProperty("horde.property.crowd.server.url", "http://localhost:8095/crowd");

    private static final String[] TABLE_NAMES = new String[]
        {
            "cwd_webhook",
            "cwd_property", "cwd_application", "cwd_user_credential_record", "cwd_membership", "cwd_app_dir_operation",
            "cwd_directory", "cwd_app_dir_mapping", "cwd_application_alias", "cwd_user_attribute",
            "cwd_directory_operation", "cwd_token", "cwd_application_address", "cwd_group_attribute",
            "hibernate_unique_key", "cwd_user", "cwd_group", "cwd_directory_attribute", "cwd_application_attribute",
            "cwd_app_dir_group_mapping",
            "databasechangelog", "databasechangeloglock" // liquibase internal tables
        };

    @Override
    public void before() throws Exception
    {
        deleteTables();
        provisionDatabase();
        flushHordeCache();
    }

    private void deleteTables() throws ClassNotFoundException
    {
        Class.forName(JDBC_DRIVER);
        DataSource dataSource = new DriverManagerDataSource(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD);
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        for (String table : TABLE_NAMES)
        {
            try
            {
                JdbcTestUtils.dropTables(jdbcTemplate, new String[] { table });
            }
            catch (BadSqlGrammarException e)
            {
                logger.info("Table {} does not exist, ignoring", table);
            }
        }
    }

    private void provisionDatabase() throws LiquibaseException
    {
        Config config = new Config(JDBC_URL, JDBC_USERNAME, JDBC_PASSWORD, JDBC_DRIVER,
                                   Config.DEFAULT_CHANGELOG_FILE, Config.DEFAULT_LIQUIBASE_LOGLEVEL,
                                   APPLICATION_PASSWORD, null, false, null, "restsetup.sql");
        Main.runWith(config);
    }

    private void flushHordeCache()
    {
        Client client = new Client();
        WebResource initDataResource = client.resource(getBaseUrl() + "/rest/studio/latest/flushCache");
        ClientResponse response = initDataResource.post(ClientResponse.class);
        if (response.getStatus() >= 300)
        {
            throw new RuntimeException("Cannot flush cache, server responded with status " + response.getStatus());
        }
        client.destroy();
    }

    @Override
    public void after()
    {
        // nothing
    }

    @Override
    public URL getBaseUrl()
    {
        try
        {
            return new URL(BASE_URL);
        }
        catch (MalformedURLException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Client decorateClient(Client client)
    {
        // nothing
        return client;
    }
}
