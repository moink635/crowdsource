package com.atlassian.crowd.acceptance.tests.horde.rest;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import org.codehaus.jettison.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static java.net.HttpURLConnection.HTTP_OK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OnDemandDummyResourceTest
{
    private static final String STATUS_RESOURCE_PATH = "/rest/studio/latest/status";
    private static final String LICENSE_LOAD_RESOURCE_PATH = "/rest/ondemand/license/1.0/license/load";
    private static final String READY_JSON_KEY = "ready";

    private final HordeRestServer restServer = HordeRestServer.INSTANCE;

    @Before
    public void setUp() throws Exception
    {
        restServer.before();
    }

    @After
    public void tearDown() throws Exception
    {
        restServer.after();
    }

    @Test
    public void testDummyStatusResource() throws Exception
    {
        URI uri = getStatusResourceURI();
        JSONObject response = getWebResource(uri).get(JSONObject.class);
        assertTrue("Unexpected response content, was: " + response, response.getBoolean(READY_JSON_KEY));
    }

    @Test
    public void testDummyLicenseResource() throws Exception
    {
        URI uri = getLicenseResourceURI();
        ClientResponse response = getWebResource(uri).get(ClientResponse.class);
        assertEquals(HTTP_OK, response.getStatus());
    }

    private URI getStatusResourceURI() throws URISyntaxException
    {
        return UriBuilder.fromUri(restServer.getBaseUrl().toString()).path(STATUS_RESOURCE_PATH).build();
    }

    private URI getLicenseResourceURI() throws URISyntaxException
    {
        return UriBuilder.fromUri(restServer.getBaseUrl().toString()).path(LICENSE_LOAD_RESOURCE_PATH).build();
    }

    private WebResource getWebResource(final URI uri)
    {
        Client client = Client.create();
        client = restServer.decorateClient(client);
        return client.resource(uri);
    }

}
