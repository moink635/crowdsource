--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.4
-- Dumped by pg_dump version 9.1.4
-- Started on 2013-07-31 11:29:36 EST

-- SET statement_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SET check_function_bodies = false;
-- SET client_min_messages = warning;
--
-- SET search_path = public, pg_catalog;

--
-- TOC entry 2305 (class 0 OID 27774)
-- Dependencies: 164
-- Data for Name: cwd_application; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (163841, 'crowd', 'crowd', '2009-04-03 11:50:50', '2010-12-17 12:02:52', 'T', 'Crowd Console', 'CROWD', '{PKCS5S2}Qb2a0uHTHMXbxzxr5hmiR74H65F511ZdTorf4md7IZSswGfUU/Hts3vhoW9ibWBi');
INSERT INTO cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (163842, 'google-apps', 'google-apps', '2009-04-03 11:50:11', '2010-12-17 11:38:52', 'T', 'Google Applications Connector', 'PLUGIN', '/iPwhVwwDOlh0cQqlpvtrYdwBLQ78c81Ld7yHhljWuoPT6RURvH1ZoSDYi2lQ9LfoPSyImrqQ0InFcv99NPSRg==');
INSERT INTO cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (884737, 'aliases', 'aliases', '2010-10-07 09:54:48', '2010-12-17 11:38:52', 'T', 'Used for testing support for aliases', 'GENERIC_APPLICATION', '{PKCS5S2}sNdodE/CSWS3vNxc3qIOQoyo86KQqmja9ez6ETpKnMTDjh4txVWWGoL2MVRMP0aJ');
INSERT INTO cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (1376257, 'no-user-application', 'no-user-application', '2011-08-17 11:58:56', '2011-08-17 12:35:03', 'T', '', 'GENERIC_APPLICATION', '{PKCS5S2}AJwTtUAtzgHMfMqUMQYLX0NkVSdRtjq3mX4APJtjBc26kSRcjKr4NuRhDO0mTNkG');


--
-- TOC entry 2309 (class 0 OID 27799)
-- Dependencies: 168
-- Data for Name: cwd_directory; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_directory (id, directory_name, lower_directory_name, created_date, updated_date, active, description, impl_class, lower_impl_class, directory_type) VALUES (1, 'Directory Two', 'directory two', '2009-04-23 10:59:35', '2010-12-17 11:38:52', 'T', 'Directory Without Modification Permissions', 'com.atlassian.crowd.directory.InternalDirectory', 'com.atlassian.crowd.directory.internaldirectory', 'INTERNAL');
INSERT INTO cwd_directory (id, directory_name, lower_directory_name, created_date, updated_date, active, description, impl_class, lower_impl_class, directory_type) VALUES (2, 'Directory One', 'directory one', '2009-04-03 11:50:40', '2010-12-17 11:38:52', 'T', '', 'com.atlassian.crowd.directory.InternalDirectory', 'com.atlassian.crowd.directory.internaldirectory', 'INTERNAL');


--
-- TOC entry 2303 (class 0 OID 27768)
-- Dependencies: 162 2305 2309
-- Data for Name: cwd_app_dir_mapping; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (196609, 163841, 2, 'F', 0);
INSERT INTO cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (589825, 163841, 1, 'T', 1);
INSERT INTO cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (917505, 884737, 2, 'T', 0);
INSERT INTO cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (1441793, 1376257, 2, 'F', 0);


--
-- TOC entry 2302 (class 0 OID 27765)
-- Dependencies: 161 2305 2309 2303
-- Data for Name: cwd_app_dir_group_mapping; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229377, 196609, 163841, 2, 'crowd-administrators');


--
-- TOC entry 2304 (class 0 OID 27771)
-- Dependencies: 163 2303
-- Data for Name: cwd_app_dir_operation; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'CREATE_GROUP');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'CREATE_ROLE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'CREATE_USER');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'DELETE_GROUP');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'DELETE_ROLE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'DELETE_USER');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'UPDATE_GROUP');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'UPDATE_GROUP_ATTRIBUTE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'UPDATE_ROLE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'UPDATE_ROLE_ATTRIBUTE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'UPDATE_USER');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'UPDATE_USER_ATTRIBUTE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (589825, 'CREATE_GROUP');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (589825, 'CREATE_ROLE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (589825, 'CREATE_USER');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (589825, 'DELETE_GROUP');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (589825, 'DELETE_ROLE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (589825, 'DELETE_USER');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (589825, 'UPDATE_GROUP');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (589825, 'UPDATE_GROUP_ATTRIBUTE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (589825, 'UPDATE_ROLE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (589825, 'UPDATE_ROLE_ATTRIBUTE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (589825, 'UPDATE_USER');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (589825, 'UPDATE_USER_ATTRIBUTE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (917505, 'CREATE_GROUP');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (917505, 'CREATE_ROLE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (917505, 'CREATE_USER');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (917505, 'DELETE_GROUP');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (917505, 'DELETE_ROLE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (917505, 'DELETE_USER');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (917505, 'UPDATE_GROUP');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (917505, 'UPDATE_GROUP_ATTRIBUTE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (917505, 'UPDATE_ROLE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (917505, 'UPDATE_ROLE_ATTRIBUTE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (917505, 'UPDATE_USER');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (917505, 'UPDATE_USER_ATTRIBUTE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (1441793, 'CREATE_GROUP');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (1441793, 'CREATE_ROLE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (1441793, 'CREATE_USER');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (1441793, 'DELETE_GROUP');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (1441793, 'DELETE_ROLE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (1441793, 'DELETE_USER');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (1441793, 'UPDATE_GROUP');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (1441793, 'UPDATE_GROUP_ATTRIBUTE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (1441793, 'UPDATE_ROLE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (1441793, 'UPDATE_ROLE_ATTRIBUTE');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (1441793, 'UPDATE_USER');
INSERT INTO cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (1441793, 'UPDATE_USER_ATTRIBUTE');


--
-- TOC entry 2306 (class 0 OID 27780)
-- Dependencies: 165 2305
-- Data for Name: cwd_application_address; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_application_address (application_id, remote_address) VALUES (163841, '127.0.0.1');
INSERT INTO cwd_application_address (application_id, remote_address) VALUES (163841, '192.168.3.242');
INSERT INTO cwd_application_address (application_id, remote_address) VALUES (163841, 'badger-badger.local');
INSERT INTO cwd_application_address (application_id, remote_address) VALUES (163841, 'localhost');
INSERT INTO cwd_application_address (application_id, remote_address) VALUES (884737, '127.0.0.1');
INSERT INTO cwd_application_address (application_id, remote_address) VALUES (1376257, '0:0:0:0:0:0:0:1');
INSERT INTO cwd_application_address (application_id, remote_address) VALUES (1376257, '127.0.0.1');


--
-- TOC entry 2307 (class 0 OID 27787)
-- Dependencies: 166 2305
-- Data for Name: cwd_application_alias; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_application_alias (id, application_id, user_name, lower_user_name, alias_name, lower_alias_name) VALUES (950273, 884737, 'admin', 'admin', 'Alias1', 'alias1');
INSERT INTO cwd_application_alias (id, application_id, user_name, lower_user_name, alias_name, lower_alias_name) VALUES (950274, 884737, 'eeeep', 'eeeep', 'Alias2', 'alias2');
INSERT INTO cwd_application_alias (id, application_id, user_name, lower_user_name, alias_name, lower_alias_name) VALUES (950275, 884737, 'penny', 'penny', 'Alias3', 'alias3');


--
-- TOC entry 2308 (class 0 OID 27793)
-- Dependencies: 167 2305
-- Data for Name: cwd_application_attribute; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (163841, 'CROWD', 'applicationType');
INSERT INTO cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (163841, 'true', 'atlassian_sha1_applied');
INSERT INTO cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (163842, 'PLUGIN', 'applicationType');
INSERT INTO cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (163842, 'true', 'atlassian_sha1_applied');
INSERT INTO cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (884737, 'true', 'aliasingEnabled');
INSERT INTO cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (884737, 'http://localhost', 'applicationURL');
INSERT INTO cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (884737, 'true', 'atlassian_sha1_applied');
INSERT INTO cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (884737, 'true', 'lowerCaseOutput');
INSERT INTO cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (1376257, 'http://localhost/', 'applicationURL');
INSERT INTO cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (1376257, 'true', 'atlassian_sha1_applied');


--
-- TOC entry 2310 (class 0 OID 27805)
-- Dependencies: 169 2309
-- Data for Name: cwd_directory_attribute; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (1, '0', 'password_history_count');
INSERT INTO cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (1, '0', 'password_max_attempts');
INSERT INTO cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (1, '0', 'password_max_change_time');
INSERT INTO cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (1, 'atlassian-security', 'user_encryption_method');
INSERT INTO cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (2, '0', 'password_history_count');
INSERT INTO cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (2, '0', 'password_max_attempts');
INSERT INTO cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (2, '0', 'password_max_change_time');
INSERT INTO cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (2, '', 'password_regex');
INSERT INTO cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (2, 'true', 'useNestedGroups');
INSERT INTO cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (2, 'atlassian-security', 'user_encryption_method');


--
-- TOC entry 2311 (class 0 OID 27811)
-- Dependencies: 170 2309
-- Data for Name: cwd_directory_operation; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (1, 'CREATE_GROUP');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (1, 'CREATE_USER');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (1, 'DELETE_GROUP');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (1, 'DELETE_USER');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (1, 'UPDATE_GROUP');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (1, 'UPDATE_GROUP_ATTRIBUTE');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (1, 'UPDATE_USER');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (1, 'UPDATE_USER_ATTRIBUTE');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (2, 'CREATE_GROUP');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (2, 'CREATE_ROLE');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (2, 'CREATE_USER');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (2, 'DELETE_GROUP');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (2, 'DELETE_ROLE');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (2, 'DELETE_USER');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (2, 'UPDATE_GROUP');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (2, 'UPDATE_GROUP_ATTRIBUTE');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (2, 'UPDATE_ROLE');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (2, 'UPDATE_ROLE_ATTRIBUTE');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (2, 'UPDATE_USER');
INSERT INTO cwd_directory_operation (directory_id, operation_type) VALUES (2, 'UPDATE_USER_ATTRIBUTE');


--
-- TOC entry 2312 (class 0 OID 27814)
-- Dependencies: 171 2309
-- Data for Name: cwd_group; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (98305, 'crowd-administrators', 'crowd-administrators', 'T', 'T', '2009-04-03 11:50:50', '2009-04-03 11:50:50', '', 'GROUP', 2);
INSERT INTO cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (98307, 'badgers', 'badgers', 'T', 'T', '2009-04-03 11:50:50', '2009-04-03 11:50:50', 'A group for badgers', 'GROUP', 2);
INSERT INTO cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (524289, 'crowd-users', 'crowd-users', 'T', 'T', '2010-06-09 13:59:25', '2010-06-09 13:59:25', '', 'GROUP', 2);
INSERT INTO cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (524290, 'animals', 'animals', 'T', 'T', '2010-06-09 14:01:20', '2010-06-09 14:01:20', '', 'GROUP', 1);
INSERT INTO cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (524291, 'birds', 'birds', 'T', 'T', '2010-06-09 14:01:31', '2010-06-09 14:01:31', '', 'GROUP', 1);
INSERT INTO cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (524292, 'dogs', 'dogs', 'T', 'T', '2010-06-09 14:01:39', '2010-06-09 14:01:39', '', 'GROUP', 1);
INSERT INTO cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (524293, 'cats', 'cats', 'T', 'T', '2010-06-09 14:01:46', '2010-06-09 14:01:46', '', 'GROUP', 1);
INSERT INTO cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (720897, 'crowd-testers', 'crowd-testers', 'T', 'T', '2010-06-10 11:47:08', '2010-06-10 11:47:08', '', 'GROUP', 2);


--
-- TOC entry 2313 (class 0 OID 27820)
-- Dependencies: 172 2309 2312
-- Data for Name: cwd_group_attribute; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_group_attribute (id, group_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (458753, 98307, 2, 'secret-location', 'hollow', 'hollow');


--
-- TOC entry 2314 (class 0 OID 27826)
-- Dependencies: 173 2309
-- Data for Name: cwd_membership; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (131074, 98305, 32770, 'GROUP_USER', 'GROUP', 'crowd-administrators', 'crowd-administrators', 'admin', 'admin', 2);
INSERT INTO cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (131075, 98307, 32770, 'GROUP_USER', 'GROUP', 'badgers', 'badgers', 'admin', 'admin', 2);
INSERT INTO cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (393217, 98305, 327681, 'GROUP_USER', 'GROUP', 'crowd-administrators', 'crowd-administrators', 'secondadmin', 'secondadmin', 2);
INSERT INTO cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (655361, 524289, 98307, 'GROUP_GROUP', 'GROUP', 'crowd-users', 'crowd-users', 'badgers', 'badgers', 2);
INSERT INTO cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (655362, 98307, 32771, 'GROUP_USER', 'GROUP', 'badgers', 'badgers', 'eeeep', 'eeeep', 2);
INSERT INTO cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (819201, 720897, 753665, 'GROUP_USER', 'GROUP', 'crowd-testers', 'crowd-testers', 'penny', 'penny', 2);
INSERT INTO cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (819202, 98305, 720897, 'GROUP_GROUP', 'GROUP', 'crowd-administrators', 'crowd-administrators', 'crowd-testers', 'crowd-testers', 2);
INSERT INTO cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (819203, 720897, 98307, 'GROUP_GROUP', 'GROUP', 'crowd-testers', 'crowd-testers', 'badgers', 'badgers', 2);


--
-- TOC entry 2315 (class 0 OID 27832)
-- Dependencies: 174
-- Data for Name: cwd_property; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'build.number', '622');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'cache.enabled', 'true');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'com.sun.jndi.ldap.connect.pool.authentication', 'simple');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'com.sun.jndi.ldap.connect.pool.initsize', '1');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'com.sun.jndi.ldap.connect.pool.maxsize', '0');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'com.sun.jndi.ldap.connect.pool.prefsize', '10');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'com.sun.jndi.ldap.connect.pool.protocol', 'plain ssl');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'com.sun.jndi.ldap.connect.pool.timeout', '30000');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'current.license.resource.total', '0');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'database.token.storage.enabled', 'true');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'deployment.title', 'Crowd Rest Testing');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'des.encryption.key', 'mNrjiZI3j4Y=');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'domain', '');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'email.template.forgotten.username', 'Hello $firstname $lastname, You have requested the username for your email: $email. Your username is: $username If you think it was sent incorrectly, please contact one of the administrators at: $admincontact $deploymenttitle Administrator');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'gzip.enabled', 'true');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.host', 'mail.atlassian.com');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.jndi', '');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.message.template', 'Hello $firstname $lastname, Your password has been reset by a $deploymenttitle administrator at $date. Your new password is: $password $deploymenttitle Administrator');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.password', 'yee9Pai1');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.port', '25');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.prefix', '[dof - Atlassian Crowd]');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.sender', 'doflynn@atlassian.com');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.username', 'doflynn');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'notification.email', 'doflynn@atlassian.com');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'secure.cookie', 'false');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'session.time', '1800000');
INSERT INTO cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'token.seed', 'Bm7CXmTp');


--
-- TOC entry 2316 (class 0 OID 27838)
-- Dependencies: 175
-- Data for Name: cwd_token; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_token (id, directory_id, entity_name, random_number, identifier_hash, random_hash, created_date, last_accessed_date, last_accessed_time, duration) VALUES (1474561, -1, 'crowd', 5816449790906142229, 'u489Iq0t0xJ3DKsvkNdMcQ00', 'evFGlJlzwdgSIF0cjKSVmA00', '2013-07-24 10:59:49.364', '2013-07-24 10:59:49.364', 1374627595187, NULL);
INSERT INTO cwd_token (id, directory_id, entity_name, random_number, identifier_hash, random_hash, created_date, last_accessed_date, last_accessed_time, duration) VALUES (1474562, -1, 'crowd', 2646843626490084087, 'HynTzREIXxOhhDEp3DsKxg00', 'lYsELmqX8mHdbiuOkX6pug00', '2013-07-24 10:59:49.503', '2013-07-24 10:59:49.503', 1374627594520, NULL);


--
-- TOC entry 2317 (class 0 OID 27845)
-- Dependencies: 176 2309
-- Data for Name: cwd_user; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_user (id, user_name, lower_user_name, active, created_date, updated_date, first_name, lower_first_name, last_name, lower_last_name, display_name, lower_display_name, email_address, lower_email_address, directory_id, credential) VALUES (32769, 'regularuser', 'regularuser', 'T', '2009-04-23 11:00:31', '2010-12-17 12:15:26', 'Reg', 'reg', 'User', 'user', 'Reg User', 'reg user', 'bob@example.net', 'bob@example.net', 1, '{PKCS5S2}8ZR7cazKNGUs98xL06oOOfU+Pe7EBDXMp+Q/j7IhpDGWXMOhnMhs6a0ildnR3UbD');
INSERT INTO cwd_user (id, user_name, lower_user_name, active, created_date, updated_date, first_name, lower_first_name, last_name, lower_last_name, display_name, lower_display_name, email_address, lower_email_address, directory_id, credential) VALUES (32770, 'admin', 'admin', 'T', '2009-04-03 11:50:50', '2010-10-07 09:53:18', 'bob', 'bob', 'the builder', 'the builder', 'bob the builder', 'bob the builder', 'bob@example.net', 'bob@example.net', 2, '{PKCS5S2}i3LoU/ZTDOmoYJKE7ZW6fCSA1LeBW6yPfxU2bfmo0RIlHgJSaej8ucw51qpTXI2M');
INSERT INTO cwd_user (id, user_name, lower_user_name, active, created_date, updated_date, first_name, lower_first_name, last_name, lower_last_name, display_name, lower_display_name, email_address, lower_email_address, directory_id, credential) VALUES (32771, 'eeeep', 'eeeep', 'T', '2009-04-17 16:06:09', '2010-10-07 09:53:17', 'Zeee', 'zeee', 'Pop!', 'pop!', 'Zeee Pop!', 'zeee pop!', 'doflynn@atlassian.com', 'doflynn@atlassian.com', 2, 'mufIYYF7Na/cWFdlA4nxFqOoY8xnAvUNsHn6bLgFgsqJ8EaHOnAncT+lQ0tg6cg7KUTu6Xqo7Ed+IMVJFwcMbA==');
INSERT INTO cwd_user (id, user_name, lower_user_name, active, created_date, updated_date, first_name, lower_first_name, last_name, lower_last_name, display_name, lower_display_name, email_address, lower_email_address, directory_id, credential) VALUES (327681, 'secondadmin', 'secondadmin', 'T', '2010-06-07 13:02:41', '2010-06-07 13:02:41', 'Second', 'second', 'Admin', 'admin', 'Second Admin', 'second admin', 'secondadmin@example.net', 'secondadmin@example.net', 2, 't6iJiuqtgmVgg4CmRfIfs4Uga4sbZ6Y6qNUK3u+R5FjiB9RD3oEtQgoQBMmpVVoUC5DhyeaN5V2JwdxjeRLE4Q==');
INSERT INTO cwd_user (id, user_name, lower_user_name, active, created_date, updated_date, first_name, lower_first_name, last_name, lower_last_name, display_name, lower_display_name, email_address, lower_email_address, directory_id, credential) VALUES (753665, 'penny', 'penny', 'T', '2010-06-10 11:47:31', '2010-06-10 11:47:31', 'Penny', 'penny', 'Small', 'small', 'Penny Small', 'penny small', 'penny@example.com', 'penny@example.com', 2, '0cJppBstdKTAbWDnKYpC6boOrITbCj4sgXLe3vcqdkcpgRH+gto9ChJzm1NrDlgvihi3vDzY0vwSRQWBs+hYvw==');
INSERT INTO cwd_user (id, user_name, lower_user_name, active, created_date, updated_date, first_name, lower_first_name, last_name, lower_last_name, display_name, lower_display_name, email_address, lower_email_address, directory_id, credential) VALUES (1114113, 'secondadmin', 'secondadmin', 'T', '2010-12-17 11:54:16', '2010-12-17 11:54:16', 'Second', 'second', 'admin in dir2', 'admin in dir2', 'Second admin in dir2', 'second admin in dir2', 'secondadmin@example.net', 'secondadmin@example.net', 1, '{PKCS5S2}FxhDoNshVIasEA/3O7Vf7NKGUG7cGaNCZ3fIaC0LoYkjkwsI71AwCmwKOhGZdR7/');
INSERT INTO cwd_user (id, user_name, lower_user_name, active, created_date, updated_date, first_name, lower_first_name, last_name, lower_last_name, display_name, lower_display_name, email_address, lower_email_address, directory_id, credential) VALUES (1212417, 'dir1user', 'dir1user', 'T', '2010-12-17 12:05:39', '2010-12-17 12:05:39', 'Dir1', 'dir1', 'User', 'user', 'Dir1 User', 'dir1 user', 'dir1user@example.net', 'dir1user@example.net', 2, '{PKCS5S2}E7FmA3nNBuRjSA7VtFav9NaYjkSfj76o8Mam526YAd8QPDoGLCkYln1CLit0EzrE');


--
-- TOC entry 2318 (class 0 OID 27851)
-- Dependencies: 177 2309 2317
-- Data for Name: cwd_user_attribute; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (65537, 32769, 1, 'requiresPasswordChange', 'false', 'false');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (65538, 32769, 1, 'passwordLastChanged', '1292548526810', '1292548526810');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (65539, 32770, 2, 'passwordLastChanged', '1238719850103', '1238719850103');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (65540, 32770, 2, 'invalidPasswordAttempts', '0', '0');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (65541, 32770, 2, 'lastAuthenticated', '1292548516184', '1292548516184');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (65542, 32770, 2, 'requiresPasswordChange', 'false', 'false');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (65543, 32771, 2, 'passwordLastChanged', '1239948369837', '1239948369837');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (65544, 32771, 2, 'requiresPasswordChange', 'false', 'false');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (360449, 327681, 2, 'requiresPasswordChange', 'false', 'false');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (360450, 327681, 2, 'passwordLastChanged', '1275879761824', '1275879761824');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (786433, 753665, 2, 'requiresPasswordChange', 'false', 'false');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (786434, 753665, 2, 'passwordLastChanged', '1276134451307', '1276134451307');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (1146881, 1114113, 1, 'requiresPasswordChange', 'false', 'false');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (1146882, 1114113, 1, 'passwordLastChanged', '1292547256709', '1292547256709');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (1245185, 1212417, 2, 'requiresPasswordChange', 'false', 'false');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (1245186, 1212417, 2, 'passwordLastChanged', '1292547939708', '1292547939708');
INSERT INTO cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (1310721, 32769, 1, 'invalidPasswordAttempts', '0', '0');


--
-- TOC entry 2319 (class 0 OID 27857)
-- Dependencies: 178 2317
-- Data for Name: cwd_user_credential_record; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--



--
-- TOC entry 2320 (class 0 OID 27860)
-- Dependencies: 179
-- Data for Name: hibernate_unique_key; Type: TABLE DATA; Schema: public; Owner: cwd_smoke
--

INSERT INTO hibernate_unique_key (next_hi) VALUES (46);


-- Completed on 2013-07-31 11:29:37 EST

--
-- PostgreSQL database dump complete
--

