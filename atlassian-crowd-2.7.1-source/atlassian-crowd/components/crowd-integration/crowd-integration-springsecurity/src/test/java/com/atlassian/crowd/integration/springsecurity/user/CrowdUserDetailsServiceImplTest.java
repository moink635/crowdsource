package com.atlassian.crowd.integration.springsecurity.user;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Collection;

import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.springsecurity.CrowdSSOTokenInvalidException;
import com.atlassian.crowd.service.AuthenticationManager;
import com.atlassian.crowd.service.GroupMembershipManager;
import com.atlassian.crowd.service.UserManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;

import com.google.common.collect.ImmutableMap;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CrowdUserDetailsServiceImplTest
{
    private UserManager mockUserManager;
    private GroupMembershipManager mockGroupMembershipManager;
    private SecurityServerClient mockSecurityServerClient;
    private AuthenticationManager mockAuthenticationManager;
    private CrowdUserDetailsServiceImpl crowdUserDetailsService;

    private String username;
    private String token;

    @Before
    public void setUp() throws Exception
    {
        mockUserManager = mock(UserManager.class);
        mockGroupMembershipManager = mock(GroupMembershipManager.class);
        mockAuthenticationManager = mock(AuthenticationManager.class);
        mockSecurityServerClient = mock(SecurityServerClient.class);

        crowdUserDetailsService = new CrowdUserDetailsServiceImpl();
        crowdUserDetailsService.setUserManager(mockUserManager);
        crowdUserDetailsService.setGroupMembershipManager(mockGroupMembershipManager);
        crowdUserDetailsService.setAuthenticationManager(mockAuthenticationManager);

        username = "username";
        token = "crowd-sso-token-string";
    }

    @Test
    public void testLoadUserByUsername_success() throws Exception
    {
        SOAPPrincipal principal = new SOAPPrincipal();
        principal.setName(username);

        when(mockUserManager.getUser(username)).thenReturn(principal);
        when(mockGroupMembershipManager.getMemberships(username)).thenReturn(Arrays.asList("group1", "group2"));

        UserDetails userDetails = crowdUserDetailsService.loadUserByUsername(username);

        assertTrue(userDetails instanceof CrowdUserDetails);
        assertEquals(userDetails.getUsername(), username);
        assertEquals(userDetails.getAuthorities().size(), 2);

        verify(mockUserManager).getUser(username);
        verify(mockGroupMembershipManager).getMemberships(username);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void testLoadUserByUsername_notFound() throws Exception
    {
        when(mockUserManager.getUser(username)).thenThrow(new UserNotFoundException(username));
        crowdUserDetailsService.loadUserByUsername(username);
    }

    @Test(expected = CrowdDataAccessException.class)
    public void testLoadUserByUsername_crowdError() throws Exception
    {
        when(mockUserManager.getUser(username)).thenThrow(new RemoteException());
        crowdUserDetailsService.loadUserByUsername(username);
    }

    @Test(expected = CrowdDataAccessException.class)
    public void testLoadUserByUsername_groupError() throws Exception
    {
        SOAPPrincipal principal = new SOAPPrincipal();
        principal.setName(username);

        when(mockUserManager.getUser(username)).thenReturn(principal);
        when(mockGroupMembershipManager.getMemberships(username)).thenThrow(new InvalidAuthorizationTokenException());

        crowdUserDetailsService.loadUserByUsername(username);
    }

    @Test
    public void testLoadUserByToken_success() throws Exception
    {
        SOAPPrincipal principal = new SOAPPrincipal();
        principal.setName(username);

        when(mockUserManager.getUserFromToken(token)).thenReturn(principal);
        when(mockGroupMembershipManager.getMemberships(username)).thenReturn(Arrays.asList("group1", "group2"));

        UserDetails userDetails = crowdUserDetailsService.loadUserByToken(token);

        assertTrue(userDetails instanceof CrowdUserDetails);
        assertEquals(userDetails.getUsername(), username);
        assertEquals(userDetails.getAuthorities().size(), 2);
    }

    @Test(expected = CrowdSSOTokenInvalidException.class)
    public void testLoadUserByToken_notFound() throws Exception
    {
        when(mockUserManager.getUserFromToken(token)).thenThrow(new InvalidTokenException());

        crowdUserDetailsService.loadUserByToken(token);
    }

    @Test(expected = CrowdDataAccessException.class)
    public void testLoadUserByToken_crowdError() throws Exception
    {
        when(mockUserManager.getUserFromToken(token)).thenThrow(new RemoteException());
        crowdUserDetailsService.loadUserByToken(token);
    }

    @Test(expected = CrowdDataAccessException.class)
    public void testLoadUserByToken_groupError() throws Exception
    {
        SOAPPrincipal principal = new SOAPPrincipal();
        principal.setName(username);

        when(mockUserManager.getUserFromToken(token)).thenReturn(principal);
        when(mockGroupMembershipManager.getMemberships(username)).thenThrow(new InvalidAuthorizationTokenException());

        crowdUserDetailsService.loadUserByToken(token);
    }

    @Test
    public void testGetAuthoritiesWithNoMappings() throws Exception
    {
        when(mockGroupMembershipManager.getMemberships(username)).thenReturn(Arrays.asList("group1", "group2"));

        Collection<GrantedAuthority> authorities = crowdUserDetailsService.getAuthorities(username);

        assertNotNull(authorities);
        assertEquals(2, authorities.size());

        assertTrue(authorities.contains(new SimpleGrantedAuthority("group1")));
        assertTrue(authorities.contains(new SimpleGrantedAuthority("group2")));
    }

    @Test
    public void testGetAuthoritiesWithMappings() throws Exception
    {
        crowdUserDetailsService.setGroupToAuthorityMappings(ImmutableMap.of(
            "group1", "ROLE_1",
            "group2", "ROLE_2",
            "group3", "ROLE_1", // same as group1
            "anotherGroup", "anotherRole").entrySet());
        when(mockAuthenticationManager.getSecurityServerClient()).thenReturn(mockSecurityServerClient);

        /* Mock both ways of discovering the group membership */
        when(mockGroupMembershipManager.getMemberships(username))
            .thenReturn(Arrays.asList("group1", "group2", "group3", "groupWithNoAuthority"));
        when(mockGroupMembershipManager.isMember(username, "group1")).thenReturn(true);
        when(mockGroupMembershipManager.isMember(username, "group2")).thenReturn(true);
        when(mockGroupMembershipManager.isMember(username, "group3")).thenReturn(true);
        when(mockGroupMembershipManager.isMember(username, "anotherGroup")).thenReturn(false);

        Collection<GrantedAuthority> authorities = crowdUserDetailsService.getAuthorities(username);

        assertNotNull(authorities);
        assertEquals(2, authorities.size());
        GrantedAuthority grantedAuthority1 = new SimpleGrantedAuthority("ROLE_1");
        GrantedAuthority grantedAuthority2 = new SimpleGrantedAuthority("ROLE_2");
        assertThat(authorities, containsInAnyOrder(grantedAuthority1, grantedAuthority2));
    }
}
