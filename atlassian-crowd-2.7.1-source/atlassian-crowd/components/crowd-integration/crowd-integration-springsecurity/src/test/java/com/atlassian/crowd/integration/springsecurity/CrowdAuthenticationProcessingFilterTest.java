package com.atlassian.crowd.integration.springsecurity;

import java.rmi.RemoteException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.integration.http.HttpAuthenticator;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CrowdAuthenticationProcessingFilterTest
{
    private CrowdSSOAuthenticationProcessingFilter processingFilter;
    private HttpAuthenticator mockHttpAuthenticator;
    private SoapClientProperties mockClientProperties;
    private AuthenticationManager mockAuthenticationManager;
    private MockHttpServletRequest mockRequest;
    private MockHttpServletResponse mockResponse;
    private String application = "crowd";

    private String baseUri = "crowd/base/uri";
    private String filterProcessUrl = "/console/j_security_check";
    private String authenticationFailureUrl = "/console/login.action?error=true";
    private String defaultTargetUrl = "/console/secure/console.action";
    private String tokenString = "crowd-sso-token-string";
    private String username = "username";
    private String password = "password";
    private CrowdUserDetails userDetails;
    private ValidationFactor[] validationFactors;
    private CrowdSSOAuthenticationDetails details;

    private static HttpServletRequest anyRequest()
    {
        return Mockito.<HttpServletRequest>any();
    }

    private static HttpServletResponse anyResponse()
    {
        return Mockito.<HttpServletResponse>any();
    }

    @Before
    public void setUp() throws Exception
    {
        mockHttpAuthenticator = mock(HttpAuthenticator.class);
        mockAuthenticationManager = mock(AuthenticationManager.class);

        mockClientProperties = mock(SoapClientProperties.class);

        when(mockHttpAuthenticator.getSoapClientProperties()).thenReturn(mockClientProperties);
        when(mockClientProperties.getApplicationName()).thenReturn(application);

        SavedRequestAwareAuthenticationSuccessHandler successHandler = new SavedRequestAwareAuthenticationSuccessHandler();
        SimpleUrlAuthenticationFailureHandler failureHandler = new SimpleUrlAuthenticationFailureHandler();

        successHandler.setDefaultTargetUrl(defaultTargetUrl);
        failureHandler.setDefaultFailureUrl(authenticationFailureUrl);

        processingFilter = new CrowdSSOAuthenticationProcessingFilter();
        processingFilter.setHttpAuthenticator(mockHttpAuthenticator);
        processingFilter.setAuthenticationManager(mockAuthenticationManager);
        processingFilter.setFilterProcessesUrl(filterProcessUrl);
        processingFilter.setAuthenticationFailureHandler(failureHandler);
        processingFilter.setAuthenticationSuccessHandler(successHandler);
        processingFilter.setRememberMeServices(Mockito.mock(RememberMeServices.class));

        mockRequest = new MockHttpServletRequest();
        mockResponse = new MockHttpServletResponse();

        validationFactors = new ValidationFactor[1];
        validationFactors[0] = new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, "127.0.0.1");

        details = new CrowdSSOAuthenticationDetails(application, validationFactors);

        SOAPPrincipal principal = new SOAPPrincipal();
        principal.setName(username);
        GrantedAuthority[] authorities = new GrantedAuthority[1];
        authorities[0] = new SimpleGrantedAuthority("role");
        userDetails = new CrowdUserDetails(principal, authorities);
    }

    @Test
    public void testRequiresAuthentication_up()
    {
        mockRequest.setRequestURI(baseUri + filterProcessUrl);
        boolean requiresAuth = processingFilter.requiresAuthentication(mockRequest, mockResponse);

        assertTrue(requiresAuth);
    }

    @Test
    public void testRequiresAuthentication_sso_noToken() throws Exception
    {
        mockRequest.setRequestURI(baseUri);
        when(mockHttpAuthenticator.getToken(anyRequest())).thenThrow(new InvalidTokenException());
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(username, password));

        boolean requiresAuth = processingFilter.requiresAuthentication(mockRequest, mockResponse);

        assertFalse(requiresAuth);
        assertNull(SecurityContextHolder.getContext().getAuthentication());
        verify(mockHttpAuthenticator).getToken(anyRequest());
    }

    @Test
    public void testRequiresAuthentication_sso_validToken() throws Exception
    {
        mockRequest.setRequestURI(baseUri);
        when(mockHttpAuthenticator.getToken(Mockito.<HttpServletRequest>any())).thenReturn(tokenString);
        when(mockHttpAuthenticator.getValidationFactors(Mockito.<HttpServletRequest>any())).thenReturn(validationFactors);
        CrowdSSOAuthenticationToken authRequest = new CrowdSSOAuthenticationToken(tokenString);
        authRequest.setDetails(details);
        CrowdSSOAuthenticationToken authResponse = new CrowdSSOAuthenticationToken(userDetails, tokenString, userDetails.getAuthorities());
        when(mockAuthenticationManager.authenticate(authRequest)).thenReturn(authResponse);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(username, password));
        boolean requiresAuth = processingFilter.requiresAuthentication(mockRequest, mockResponse);

        assertFalse(requiresAuth);
        assertEquals(SecurityContextHolder.getContext().getAuthentication(), authResponse);

        verify(mockHttpAuthenticator).getToken(Mockito.<HttpServletRequest>any());
        verify(mockHttpAuthenticator).getValidationFactors(anyRequest());
        verify(mockAuthenticationManager).authenticate(authRequest);
        verify(mockHttpAuthenticator).setPrincipalToken(mockRequest, mockResponse, tokenString);
    }

    @Test
    public void testRequiresAuthentication_sso_invalidToken() throws Exception
    {
        mockRequest.setRequestURI(baseUri);
        when(mockHttpAuthenticator.getToken(Mockito.<HttpServletRequest>any())).thenReturn(tokenString);
        when(mockHttpAuthenticator.getValidationFactors(Mockito.<HttpServletRequest>any())).thenReturn(validationFactors);
        CrowdSSOAuthenticationToken authRequest = new CrowdSSOAuthenticationToken(tokenString);
        authRequest.setDetails(details);
        Mockito.doThrow(new BadCredentialsException("")).when(mockAuthenticationManager).authenticate(authRequest);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(username, password));
        boolean requiresAuth = processingFilter.requiresAuthentication(mockRequest, mockResponse);

        assertFalse(requiresAuth);
        assertNull(SecurityContextHolder.getContext().getAuthentication());

        verify(mockHttpAuthenticator).getToken(Mockito.<HttpServletRequest>any());
        verify(mockHttpAuthenticator).getValidationFactors(anyRequest());
        verify(mockAuthenticationManager).authenticate(authRequest);
    }

    @Test
    public void testOnSuccessfulAuthentication_success() throws Exception
    {
        CrowdSSOAuthenticationToken authResponse = new CrowdSSOAuthenticationToken(userDetails, tokenString, userDetails.getAuthorities());
        processingFilter.successfulAuthentication(mockRequest, mockResponse, authResponse);
        verify(mockHttpAuthenticator).setPrincipalToken(mockRequest, mockResponse, tokenString);
    }

    @Test
    public void testOnSuccessfulAuthentication_fail() throws Exception
    {
        CrowdSSOAuthenticationToken authResponse = new CrowdSSOAuthenticationToken(userDetails, tokenString, userDetails.getAuthorities());
        Mockito.doThrow(new RemoteException()).when(mockHttpAuthenticator).setPrincipalToken(mockRequest, mockResponse, tokenString);
        processingFilter.successfulAuthentication(mockRequest, mockResponse, authResponse);
        verify(mockHttpAuthenticator).setPrincipalToken(mockRequest, mockResponse, tokenString);
    }

    @Test
    public void testOnUnsuccessfulAuthentication_success() throws Exception
    {
        processingFilter.unsuccessfulAuthentication(mockRequest, mockResponse, new BadCredentialsException(""));
        verify(mockHttpAuthenticator).logoff(anyRequest(), anyResponse());
    }

    @Test
    public void testOnUnsuccessfulAuthentication_fail() throws Exception
    {
        Mockito.doThrow(new RemoteException()).when(mockHttpAuthenticator).logoff(anyRequest(), anyResponse());
        processingFilter.unsuccessfulAuthentication(mockRequest, mockResponse, new BadCredentialsException(""));
    }
}
