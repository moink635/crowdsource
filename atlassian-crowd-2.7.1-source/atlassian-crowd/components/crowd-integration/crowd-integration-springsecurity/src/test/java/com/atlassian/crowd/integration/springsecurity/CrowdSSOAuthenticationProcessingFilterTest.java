package com.atlassian.crowd.integration.springsecurity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.atlassian.crowd.integration.http.HttpAuthenticator;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.web.PortResolverImpl;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.security.web.savedrequest.SavedRequest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CrowdSSOAuthenticationProcessingFilterTest
{
    @Mock
    RequestToApplicationMapper mapper;

    @Mock
    HttpSession session;

    CrowdSSOAuthenticationProcessingFilter filter;

    @Mock
    HttpAuthenticator httpAuthenticator;

    @Before
    public final void setupMocks()
    {
        when(mapper.getApplication("/j_spring_security_check")).thenReturn("console-app");
        when(mapper.getApplication("/profile")).thenReturn("console-app");
        when(mapper.getApplication("/plugin/")).thenReturn("plugin-app");

        filter = new CrowdSSOAuthenticationProcessingFilter();
        filter.setRequestToApplicationMapper(mapper);
        filter.setHttpAuthenticator(httpAuthenticator);
    }

    @Test
    public void requestForResourceUsesDefaultApplicationWhenNoMapperIsSet()
    {
        filter.setRequestToApplicationMapper(null);

        SoapClientProperties scp = mock(SoapClientProperties.class);
        when(scp.getApplicationName()).thenReturn("default-application-name");
        when(httpAuthenticator.getSoapClientProperties()).thenReturn(scp);

        CrowdSSOAuthenticationToken authRequest = new CrowdSSOAuthenticationToken(null);
        filter.doSetDetails(null, authRequest);

        CrowdSSOAuthenticationDetails details = (CrowdSSOAuthenticationDetails) authRequest.getDetails();
        assertEquals("default-application-name", details.getApplicationName());
    }

    @Test
    public void requestForResourceUsesApplicationFromMapperByDefault()
    {
        when(mapper.getApplication("/resource")).thenReturn("application");

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/context/resource");
        when(request.getContextPath()).thenReturn("/context");
        when(request.getSession()).thenReturn(session);
        when(request.getSession(anyBoolean())).thenReturn(session);

        CrowdSSOAuthenticationToken authRequest = new CrowdSSOAuthenticationToken(null);
        filter.doSetDetails(request, authRequest);

        CrowdSSOAuthenticationDetails details = (CrowdSSOAuthenticationDetails) authRequest.getDetails();
        assertEquals("application", details.getApplicationName());
        assertNull(details.getValidationFactors());

        verify(mapper).getApplication("/resource");
    }

    @Test
    public void authorisationUsesApplicationForSavedRequestWhenRequestIsForLoginPage()
    {
        CrowdSSOAuthenticationToken authRequest = new CrowdSSOAuthenticationToken(null);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/context/j_spring_security_check");
        when(request.getContextPath()).thenReturn("/context");

        MockHttpServletRequest previousRequest = new MockHttpServletRequest("http", "/context/plugin/");
        previousRequest.setContextPath("/context");

        SavedRequest sr = new DefaultSavedRequest(previousRequest, new PortResolverImpl());
        when(session.getAttribute("SPRING_SECURITY_SAVED_REQUEST")).thenReturn(sr);
        when(request.getSession()).thenReturn(session);
        when(request.getSession(anyBoolean())).thenReturn(session);

        filter.doSetDetails(request, authRequest);

        CrowdSSOAuthenticationDetails details = (CrowdSSOAuthenticationDetails) authRequest.getDetails();
        assertEquals("plugin-app", details.getApplicationName());
    }

    @Test
    public void authorisationUsesConsoleApplicationWhenRequestIsForRegularConsolePage()
    {
        CrowdSSOAuthenticationToken authRequest = new CrowdSSOAuthenticationToken(null);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/context/profile");
        when(request.getContextPath()).thenReturn("/context");

        MockHttpServletRequest previousRequest = new MockHttpServletRequest("http", "/context/plugin/");
        previousRequest.setContextPath("/context");

        SavedRequest sr = new DefaultSavedRequest(previousRequest, new PortResolverImpl());
        when(session.getAttribute("SPRING_SECURITY_SAVED_REQUEST")).thenReturn(sr);
        when(request.getSession()).thenReturn(session);
        when(request.getSession(anyBoolean())).thenReturn(session);

        filter.doSetDetails(request, authRequest);

        CrowdSSOAuthenticationDetails details = (CrowdSSOAuthenticationDetails) authRequest.getDetails();
        assertEquals("console-app", details.getApplicationName());
    }

    @Test
    public void authorisationUsesDefaultApplicationWhenRequestIsForLoginPageButNoSavedPage()
    {
        CrowdSSOAuthenticationToken authRequest = new CrowdSSOAuthenticationToken(null);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRequestURI()).thenReturn("/context/j_spring_security_check");
        when(request.getContextPath()).thenReturn("/context");
        when(request.getSession()).thenReturn(session);
        when(request.getSession(anyBoolean())).thenReturn(session);

        MockHttpServletRequest previousRequest = new MockHttpServletRequest("http", "/context/plugin/");
        previousRequest.setContextPath("/context");

        filter.doSetDetails(request, authRequest);

        CrowdSSOAuthenticationDetails details = (CrowdSSOAuthenticationDetails) authRequest.getDetails();
        assertEquals("console-app", details.getApplicationName());
    }

    @Test
    public void canUseSavedRequestOnlyKnowsAboutSecurityCheckByDefault()
    {
        MockHttpServletRequest request = new MockHttpServletRequest();

        request.setContextPath("/crowd");

        /* Uses the saved request URI */
        request.setRequestURI("/crowd/j_spring_security_check");
        assertTrue(filter.canUseSavedRequestToAuthenticate(request));

        /* Treated as themselves */
        request.setRequestURI("/crowd/anything-at-all/regular-page");
        assertFalse(filter.canUseSavedRequestToAuthenticate(request));

        request.setRequestURI("/crowd/console/login.action");
        assertFalse(filter.canUseSavedRequestToAuthenticate(request));
    }

    @Test
    public void canUseSavedRequestForLoginPageWhenFilterEntryPointIsSupplied()
    {
        LoginUrlAuthenticationEntryPoint filterEntryPoint = new LoginUrlAuthenticationEntryPoint("/console/custom-login.action");
        filter.setLoginUrlAuthenticationEntryPoint(filterEntryPoint);

        MockHttpServletRequest request = new MockHttpServletRequest();

        request.setContextPath("/crowd");

        /* Use the saved request URI */
        request.setRequestURI("/crowd/j_spring_security_check");
        assertTrue(filter.canUseSavedRequestToAuthenticate(request));

        request.setRequestURI("/crowd/console/custom-login.action");
        assertTrue(filter.canUseSavedRequestToAuthenticate(request));

        /* Treated as itself */
        request.setRequestURI("/crowd/anything-at-all/regular-page");
        assertFalse(filter.canUseSavedRequestToAuthenticate(request));
    }
}
