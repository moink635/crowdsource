package com.atlassian.crowd.integration.springsecurity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.atlassian.crowd.service.client.ClientProperties;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.security.web.PortResolver;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RequestToApplicationMapperImplTest
{
    @Test
    public void returnsDefaultApplicationWhenEmpty()
    {
        ClientProperties clientProperties = mock(ClientProperties.class);
        when(clientProperties.getApplicationName()).thenReturn("test");

        assertEquals("test",
                new RequestToApplicationMapperImpl(clientProperties).getApplication("/"));
    }

    @Test
    public void mostRecentDefinitionIsUsed()
    {
        ClientProperties clientProperties = mock(ClientProperties.class);

        RequestToApplicationMapperImpl mapper = new RequestToApplicationMapperImpl(clientProperties);
        mapper.addSecureMapping("/", "root");
        mapper.addSecureMapping("/", "root2");

        assertEquals("root2",
                mapper.getApplication("/"));
    }

    @Test
    public void pathFromSavedRequestInSessionIsUsed()
    {
        ClientProperties clientProperties = mock(ClientProperties.class);

        RequestToApplicationMapperImpl mapper = new RequestToApplicationMapperImpl(clientProperties);
        mapper.addSecureMapping("/subdirectory", "subdirectory");

        HttpServletRequest oldRequest = mock(HttpServletRequest.class);
        when(oldRequest.getContextPath()).thenReturn("/app");
        when(oldRequest.getRequestURI()).thenReturn("/app/subdirectory");

        when(oldRequest.getHeaderNames()).thenReturn(Collections.enumeration(Collections.emptyList()));
        when(oldRequest.getLocales()).thenReturn(Collections.enumeration(Collections.emptyList()));
        when(oldRequest.getRequestURL()).thenReturn(new StringBuffer());

        PortResolver portResolver = mock(PortResolver.class);
        when(portResolver.getServerPort(Mockito.<HttpServletRequest>any())).thenReturn(80);

        DefaultSavedRequest savedRequest = new DefaultSavedRequest(oldRequest, portResolver);

        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute("SPRING_SECURITY_SAVED_REQUEST")).thenReturn(savedRequest);

        HttpServletRequest request = mock(HttpServletRequest.class);

        when(request.getSession(false)).thenReturn(session);
        assertEquals("subdirectory",
                mapper.getApplication(request));
    }

    @Test
    public void pathFromCurrentRequestIsUsedWhenNoSavedRequestIsInSession()
    {
        ClientProperties clientProperties = mock(ClientProperties.class);

        RequestToApplicationMapperImpl mapper = new RequestToApplicationMapperImpl(clientProperties);
        mapper.addSecureMapping("/subdirectory", "subdirectory");

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getContextPath()).thenReturn("/app");
        when(request.getRequestURI()).thenReturn("/app/subdirectory");

        assertEquals("subdirectory",
                mapper.getApplication(request));
    }

    @Test
    public void mappingsCanBeRemoved()
    {
        ClientProperties clientProperties = mock(ClientProperties.class);
        when(clientProperties.getApplicationName()).thenReturn("default");


        RequestToApplicationMapperImpl mapper = new RequestToApplicationMapperImpl(clientProperties);
        mapper.addSecureMapping("/", "root");

        mapper.removeSecureMapping("/");

        assertEquals("default",
                mapper.getApplication("/"));
    }

    @Test
    public void allMappingsCanBeRemoved()
    {
        ClientProperties clientProperties = mock(ClientProperties.class);
        when(clientProperties.getApplicationName()).thenReturn("default");


        RequestToApplicationMapperImpl mapper = new RequestToApplicationMapperImpl(clientProperties);
        mapper.addSecureMapping("/", "root");

        assertEquals("root", mapper.getApplication("/"));

        mapper.removeAllMappings("no-such-application");
        assertEquals("root", mapper.getApplication("/"));

        mapper.removeAllMappings("root");
        assertEquals("default", mapper.getApplication("/"));
    }
}
