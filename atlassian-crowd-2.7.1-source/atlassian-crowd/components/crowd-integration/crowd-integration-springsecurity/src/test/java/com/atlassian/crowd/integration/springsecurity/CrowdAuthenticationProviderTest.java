package com.atlassian.crowd.integration.springsecurity;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Collections;

import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.integration.http.HttpAuthenticator;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.springsecurity.user.CrowdDataAccessException;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.AuthenticationManager;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This class tests the various types of authentication mechanisms
 * supported by the CrowdAuthenticationProvider:
 *
 * 1. UsernamePassword authentication for SSO
 * 2. UsernamePassword authentication for Centralised Authentication
 * 3. SSO Token authentication for SSO.
 *
 */

public class CrowdAuthenticationProviderTest
{
    private RemoteCrowdAuthenticationProvider crowdAuthProvider;
    private AuthenticationManager mockAuthenticationManager;
    private HttpAuthenticator mockHttpAuthenticator;
    private CrowdUserDetailsService mockCrowdUserDetailsService;
    private SoapClientProperties mockClientProperties;

    private String username = "username";
    private String password = "password";
    private String crowdTokenString = "crowd-sso-token-string";
    private ValidationFactor[] validationFactors;
    private String application = "crowd";
    private CrowdSSOAuthenticationDetails details;
    private CrowdUserDetails userDetails;
    private Collection<GrantedAuthority> authorities;

    @Before
    public void setUp() throws Exception
    {
        mockAuthenticationManager = mock(AuthenticationManager.class);
        mockHttpAuthenticator = mock(HttpAuthenticator.class);
        mockCrowdUserDetailsService = mock(CrowdUserDetailsService.class);
        mockClientProperties = mock(SoapClientProperties.class);

        when(mockHttpAuthenticator.getSoapClientProperties()).thenReturn(mockClientProperties);
        when(mockClientProperties.getApplicationName()).thenReturn(application);

        crowdAuthProvider = new RemoteCrowdAuthenticationProvider(
                mockAuthenticationManager,
                mockHttpAuthenticator,
                mockCrowdUserDetailsService
        );

        validationFactors = new ValidationFactor[1];
        validationFactors[0] = new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, "127.0.0.1");

        details = new CrowdSSOAuthenticationDetails(application, validationFactors);

        SOAPPrincipal principal = new SOAPPrincipal();
        principal.setName(username);
        authorities = Collections.<GrantedAuthority>singletonList(new SimpleGrantedAuthority("role"));
        userDetails = new CrowdUserDetails(principal, authorities);
    }

    @Test
    public void testSupports()
    {
        assertTrue(crowdAuthProvider.supports(UsernamePasswordAuthenticationToken.class));
        assertTrue(crowdAuthProvider.supports(CrowdSSOAuthenticationToken.class));
        assertFalse(crowdAuthProvider.supports(RememberMeAuthenticationToken.class));
        assertFalse(crowdAuthProvider.supports(AnonymousAuthenticationToken.class));
    }

    @Test
    public void testTranslateException()
    {
        Exception eIn;
        Exception eOut;

        eIn = new BadCredentialsException("");
        eOut = crowdAuthProvider.translateException(eIn);
        assertSame(eIn, eOut);

        eIn = new InvalidAuthenticationException("");
        eOut = crowdAuthProvider.translateException(eIn);
        assertTrue(eOut instanceof BadCredentialsException);

        eIn = new InvalidTokenException();
        eOut = crowdAuthProvider.translateException(eIn);
        assertTrue(eOut instanceof BadCredentialsException);

        eIn = new InactiveAccountException("");
        eOut = crowdAuthProvider.translateException(eIn);
        assertTrue(eOut instanceof DisabledException);

        eIn = new RemoteException();
        eOut = crowdAuthProvider.translateException(eIn);
        assertTrue(eOut instanceof AuthenticationException);
    }

    @Test
    public void testAuthenticate_unsupported()
    {
        Authentication unsupportedToken = new RememberMeAuthenticationToken(username, password, Collections.<GrantedAuthority>emptyList());
        Authentication out = crowdAuthProvider.authenticate(unsupportedToken);
        assertEquals(out, null);
    }

    @Test
    public void testAuthenticate_upBlank()
    {
        Authentication upToken = new UsernamePasswordAuthenticationToken(null, password);
        try
        {
            crowdAuthProvider.authenticate(upToken);
            fail("BadCredentialsException expected");
        }
        catch (BadCredentialsException e)
        {
            // expected
        }

        upToken = new UsernamePasswordAuthenticationToken(username, null);
        try
        {
            crowdAuthProvider.authenticate(upToken);
            fail("BadCredentialsException expected");
        }
        catch (BadCredentialsException e)
        {
            // expected
        }
    }

    @Test
    public void testAuthenticate_upWithValidationFactors_success() throws Exception
    {
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);
        upToken.setDetails(details);

        when(mockHttpAuthenticator.verifyAuthentication(username, password, validationFactors)).thenReturn(crowdTokenString);
        when(mockCrowdUserDetailsService.loadUserByUsername(username)).thenReturn(userDetails);

        CrowdSSOAuthenticationToken out = (CrowdSSOAuthenticationToken)crowdAuthProvider.authenticate(upToken);

        assertEquals(out.getCredentials(), crowdTokenString);
        assertEquals(out.getPrincipal(), userDetails);
        assertEquals(out.getAuthorities(), authorities);
    }

    @Test(expected = AuthenticationServiceException.class)
    public void testAuthenticate_upWithValidationFactors_authenticatorFail() throws Exception
    {
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);
        upToken.setDetails(details);

        when(mockHttpAuthenticator.verifyAuthentication(username, password, validationFactors)).thenThrow(new RemoteException());

        crowdAuthProvider.authenticate(upToken);
    }

    @Test(expected = AuthenticationException.class)
    public void testAuthenticate_upWithValidationFactors_userDetailsFail() throws Exception
    {
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);
        upToken.setDetails(details);

        when(mockHttpAuthenticator.verifyAuthentication(username, password, validationFactors)).thenReturn(crowdTokenString);
        when(mockCrowdUserDetailsService.loadUserByUsername(username)).thenThrow(new CrowdDataAccessException(""));

        crowdAuthProvider.authenticate(upToken);
    }

    @Test
    public void testAuthenticate_up_success() throws Exception
    {
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);

        when(mockHttpAuthenticator.verifyAuthentication(username, password, new ValidationFactor[0])).thenReturn(crowdTokenString);
        when(mockCrowdUserDetailsService.loadUserByUsername(username)).thenReturn(userDetails);

        UsernamePasswordAuthenticationToken out = (UsernamePasswordAuthenticationToken)crowdAuthProvider.authenticate(upToken);

        assertEquals(out.getCredentials(), password);
        assertEquals(out.getPrincipal(), userDetails);
        assertEquals(out.getAuthorities(), authorities);
    }

    @Test(expected = AuthenticationServiceException.class)
    public void testAuthenticate_up_authenticatorFail() throws Exception
    {
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);

        when(mockHttpAuthenticator.verifyAuthentication(username, password, new ValidationFactor[0])).thenThrow(new RemoteException());

        crowdAuthProvider.authenticate(upToken);
    }

    @Test
    public void testAuthenticate_up_userDetailsFail() throws Exception
    {
        UsernamePasswordAuthenticationToken upToken = new UsernamePasswordAuthenticationToken(username, password);

        when(mockCrowdUserDetailsService.loadUserByUsername(username)).thenThrow(new CrowdDataAccessException(""));

        try
        {
            crowdAuthProvider.authenticate(upToken);
            fail("AuthenticationException expected");
        }
        catch (AuthenticationException e)
        {
            // expected
        }

        verify(mockHttpAuthenticator).verifyAuthentication(username, password, new ValidationFactor[0]);
    }

    @Test
    public void testAuthenticate_sso_success() throws Exception
    {
        CrowdSSOAuthenticationToken ssoToken = new CrowdSSOAuthenticationToken(crowdTokenString);
        ssoToken.setDetails(details);

        when(mockAuthenticationManager.isAuthenticated(crowdTokenString, validationFactors)).thenReturn(true);
        when(mockCrowdUserDetailsService.loadUserByToken(crowdTokenString)).thenReturn(userDetails);

        CrowdSSOAuthenticationToken out = (CrowdSSOAuthenticationToken)crowdAuthProvider.authenticate(ssoToken);

        assertEquals(out.getCredentials(), crowdTokenString);
        assertEquals(out.getPrincipal(), userDetails);
        assertEquals(out.getAuthorities(), authorities);
    }

    @Test(expected = CrowdSSOTokenInvalidException.class)
    public void testAuthenticate_sso_invalidToken() throws Exception
    {
        CrowdSSOAuthenticationToken ssoToken = new CrowdSSOAuthenticationToken(crowdTokenString);
        ssoToken.setDetails(details);

        when(mockAuthenticationManager.isAuthenticated(crowdTokenString, validationFactors)).thenReturn(false);

        crowdAuthProvider.authenticate(ssoToken);
    }

    @Test(expected = AuthenticationServiceException.class)
    public void testAuthenticate_sso_authenticatorFail() throws Exception
    {
        CrowdSSOAuthenticationToken ssoToken = new CrowdSSOAuthenticationToken(crowdTokenString);
        ssoToken.setDetails(details);

        when(mockAuthenticationManager.isAuthenticated(crowdTokenString, validationFactors)).thenThrow(new RemoteException());

        crowdAuthProvider.authenticate(ssoToken);
    }

    @Test(expected = AuthenticationServiceException.class)
    public void testAuthenticate_sso_userDetailsFail() throws Exception
    {
        CrowdSSOAuthenticationToken ssoToken = new CrowdSSOAuthenticationToken(crowdTokenString);
        ssoToken.setDetails(details);

        when(mockAuthenticationManager.isAuthenticated(crowdTokenString, validationFactors)).thenReturn(true);
        when(mockCrowdUserDetailsService.loadUserByToken(crowdTokenString)).thenThrow(new CrowdDataAccessException(""));

        crowdAuthProvider.authenticate(ssoToken);
    }

    @Test
    public void testAuthenticate_sso_noValidationFactors()
    {
        CrowdSSOAuthenticationToken ssoToken = new CrowdSSOAuthenticationToken(crowdTokenString);
        try
        {
            crowdAuthProvider.authenticate(ssoToken);
            fail("BadCredentialsException expected");
        }
        catch (BadCredentialsException e)
        {
            // expected
        }
    }
}
