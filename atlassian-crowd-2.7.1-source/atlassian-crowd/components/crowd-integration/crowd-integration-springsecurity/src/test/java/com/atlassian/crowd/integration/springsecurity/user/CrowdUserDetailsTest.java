package com.atlassian.crowd.integration.springsecurity.user;

import java.util.Collection;

import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.model.user.UserConstants;

import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class CrowdUserDetailsTest
{
    @Test
    public void testCrowdUserDetails_fullDetails()
    {
        // set up principal
        String username = "username";
        boolean active = true;
        String[] email = {"email@email.com", "bob@bob.com"};
        String[] firstName = {"Robert"};
        String[] lastName = {"Smith"};
        String[] displayName = {"Bob Smith"};
        String[] pwChange = {"true"};

        SOAPPrincipal principal = new SOAPPrincipal();
        principal.setName(username);
        principal.setActive(active);
        SOAPAttribute[] attribs = new SOAPAttribute[5];
        attribs[0] = new SOAPAttribute(UserConstants.EMAIL, email);
        attribs[1] = new SOAPAttribute(UserConstants.FIRSTNAME, firstName);
        attribs[2] = new SOAPAttribute(UserConstants.LASTNAME, lastName);
        attribs[3] = new SOAPAttribute(UserConstants.DISPLAYNAME, displayName);
        attribs[4] = new SOAPAttribute(UserConstants.REQUIRES_PASSWORD_CHANGE, pwChange);
        principal.setAttributes(attribs);

        GrantedAuthority[] grantedAuths = new GrantedAuthority[1];
        String roleName = "admin-role";
        grantedAuths[0] = new SimpleGrantedAuthority(roleName);

        // create user details from principal
        CrowdUserDetails crowdUser = new CrowdUserDetails(principal, grantedAuths);

        // test attributes
        assertEquals(crowdUser.getUsername(), username);
        assertEquals(crowdUser.getEmail(), email[0]);
        assertEquals(crowdUser.getFirstName(), firstName[0]);
        assertEquals(crowdUser.getLastName(), lastName[0]);
        assertEquals(crowdUser.getFullName(), displayName[0]);
        assertEquals(crowdUser.getAttribute(UserConstants.REQUIRES_PASSWORD_CHANGE), pwChange[0]);
        assertEquals(crowdUser.isEnabled(), active);
        assertTrue(crowdUser.isAccountNonExpired());
        assertTrue(crowdUser.isAccountNonLocked());
        assertTrue(crowdUser.isCredentialsNonExpired());
        assertTrue(crowdUser.getAuthorities().iterator().next().getAuthority().equals(roleName));

        assertEquals(username, crowdUser.getRemotePrincipal().getName());
        assertEquals(email[0], crowdUser.getRemotePrincipal().getEmailAddress());
        assertEquals(firstName[0], crowdUser.getRemotePrincipal().getFirstName());
        assertEquals(lastName[0], crowdUser.getRemotePrincipal().getLastName());
        assertEquals(displayName[0], crowdUser.getRemotePrincipal().getDisplayName());
        assertEquals(pwChange[0], crowdUser.getAttribute(UserConstants.REQUIRES_PASSWORD_CHANGE));
        assertEquals(active, crowdUser.getRemotePrincipal().isActive());
    }

    @Test
    public void testCrowdUserDetails_minimalDetails()
    {
        // set up principal
        String username = "username";
        boolean active = false;
        String[] email = {"email@email.com", "bob@bob.com"};
        String[] firstName = {"Robert"};
        String[] lastName = {"Smith"};

        SOAPPrincipal principal = new SOAPPrincipal();
        principal.setName(username);
        principal.setActive(active);
        SOAPAttribute[] attribs = new SOAPAttribute[3];
        attribs[0] = new SOAPAttribute(UserConstants.EMAIL, email);
        attribs[1] = new SOAPAttribute(UserConstants.FIRSTNAME, firstName);
        attribs[2] = new SOAPAttribute(UserConstants.LASTNAME, lastName);
        principal.setAttributes(attribs);

        // create user details from principal
        CrowdUserDetails crowdUser = new CrowdUserDetails(principal, (Collection<GrantedAuthority>) null);

        // test attributes
        assertEquals(crowdUser.getUsername(), username);
        assertEquals(crowdUser.getEmail(), email[0]);
        assertEquals(crowdUser.getFirstName(), firstName[0]);
        assertEquals(crowdUser.getLastName(), lastName[0]);
        assertEquals(crowdUser.getFullName(), null);
        assertEquals(crowdUser.getAttribute(UserConstants.REQUIRES_PASSWORD_CHANGE), null);
        assertEquals(crowdUser.isEnabled(), active);
        assertTrue(crowdUser.isAccountNonExpired());
        assertTrue(crowdUser.isAccountNonLocked());
        assertTrue(crowdUser.isCredentialsNonExpired());

        assertEquals(username, crowdUser.getRemotePrincipal().getName());
        assertEquals(email[0], crowdUser.getRemotePrincipal().getEmailAddress());
        assertEquals(firstName[0], crowdUser.getRemotePrincipal().getFirstName());
        assertEquals(lastName[0], crowdUser.getRemotePrincipal().getLastName());
        assertNull(crowdUser.getRemotePrincipal().getDisplayName());
        assertNull(crowdUser.getAttribute(UserConstants.REQUIRES_PASSWORD_CHANGE));
        assertEquals(active, crowdUser.getRemotePrincipal().isActive());
        assertFalse(crowdUser.getRemotePrincipal().isActive());
    }

    @Test
    public void testCrowdUserDetails_noDetails()
    {
        String username = "username";
        SOAPPrincipal principal = new SOAPPrincipal();
        principal.setName(username);

         // create user details from principal
        CrowdUserDetails crowdUser = new CrowdUserDetails(principal, (Collection<GrantedAuthority>) null);

        // test attributes
        assertEquals(crowdUser.getUsername(), username);
        assertEquals(crowdUser.getEmail(), null);
        assertEquals(crowdUser.getFirstName(), null);
        assertEquals(crowdUser.getLastName(), null);
        assertEquals(crowdUser.getFullName(), null);
        assertEquals(crowdUser.getAttribute(UserConstants.REQUIRES_PASSWORD_CHANGE), null);
        assertEquals(crowdUser.isEnabled(), false);
        assertTrue(crowdUser.isAccountNonExpired());
        assertTrue(crowdUser.isAccountNonLocked());
        assertTrue(crowdUser.isCredentialsNonExpired());

        assertEquals(username, crowdUser.getRemotePrincipal().getName());
        assertFalse(crowdUser.getRemotePrincipal().isActive());
    }
}
