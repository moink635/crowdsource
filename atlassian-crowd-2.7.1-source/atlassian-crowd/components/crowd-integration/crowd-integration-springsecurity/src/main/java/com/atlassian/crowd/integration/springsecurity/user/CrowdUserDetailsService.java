package com.atlassian.crowd.integration.springsecurity.user;

import java.util.Map;

import com.atlassian.crowd.integration.springsecurity.CrowdSSOTokenInvalidException;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Interface for retrieving users from Crowd.
 *
 * @author Shihab Hamid
 */
public interface CrowdUserDetailsService extends UserDetailsService
{
    /**
     * Retrieves the user from Crowd by looking up the principal by username.
     *
     * @param username username of the principal.
     * @return CrowdUserDetails corresponding to the principal.
     * @throws org.springframework.security.userdetails.UsernameNotFoundException thrown if a principal with the requested username cannot be found in Crowd.
     * @throws org.springframework.dao.DataAccessException thrown if there was an underlying problem while communicating with the Crowd server.
     */
    @Override
    CrowdUserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException;

    /**
     * Retrieve a user from Crowd by looking up the principal by their authenticated Crowd token.
     *
     * @param token Crowd SSO token string.
     * @return CrowdUserDetails corresponding to the principal.
     * @throws com.atlassian.crowd.integration.springframework.security.CrowdSSOTokenInvalidException if the provided token is invalid.
     * @throws org.springframework.dao.DataAccessException thrown if there was an underlying problem while communicating with the Crowd server.
     */
    CrowdUserDetails loadUserByToken(String token) throws CrowdSSOTokenInvalidException, DataAccessException;

    /**
     * Return the authority prefix applied to group names
     * the principal is a member of when generating the
     * GrantedAuthority[] and no authorityMap is set.
     *
     * @return prefix.
     */
    String getAuthorityPrefix();

    /**
     * Set the authority prefix applied to group names
     * the principal is a member of when generating the
     * GrantedAuthority[] and no authorityMap is set,
     * e.g. ROLE_crowd-administrators.
     *
     * @param authorityPrefix prefix to apply. The default
     * is no prefix.
     */
    void setAuthorityPrefix(String authorityPrefix);

    /**
     * Return the group-to-authority mappings
     * @return an iterable over mappings from group names (key) to authority names (value)
     */
    Iterable<Map.Entry<String,String>> getGroupToAuthorityMappings();

    /**
     * Set the authority mappings. The default is no authority mappings, which implies
     * that authorities are derived from group names, with an optional
     * authorityPrefix. If not null, the mappings are used instead to transform
     * group names into authority names regardless of the authorityPrefix.
     * </p>
     * Versions of Crowd prior to 2.6 used to have a configurable
     * authoritySuffix parameter which has been replaced by these mappings.
     * @param groupToAuthorityMappings authority mappings. If absent, an authority prefix is used.
     */
    void setGroupToAuthorityMappings(Iterable<Map.Entry<String, String>> groupToAuthorityMappings);

    /**
     * Return the name of the admin authority.
     * @return name of the admin authority
     */
    String getAdminAuthority();

    /**
     * Set the name of the admin authority.
     * @param adminAuthority name of the admin authority. The default is ROLE_ADMIN.
     */
    void setAdminAuthority(String adminAuthority);
}
