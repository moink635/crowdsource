package com.atlassian.crowd.integration.springsecurity;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;

/**
 * An extension to the standard ProviderManager implementation
 * of the AuthenticationManager wich allows adding and removing
 * provider managers at runtime.
 * <p/>
 * This ensures that the provider manager is threadsafe (to
 * some degree). It is still possible to obtain a collection
 * of providers and manipulate it in a non-threadsafe manner
 * by using the getter/setter.
 * <p/>
 * Note: if you have no need for runtime-pluggable behaviour,
 * simply use org.springframework.security.providers.ProviderManager.
 */
public interface DynamicProviderManager extends AuthenticationManager
{
    void addProvider(AuthenticationProvider provider);

    boolean removeProvider(AuthenticationProvider provider);
}
