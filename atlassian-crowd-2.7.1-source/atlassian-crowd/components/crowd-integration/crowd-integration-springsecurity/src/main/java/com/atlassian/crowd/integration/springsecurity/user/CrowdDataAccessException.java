package com.atlassian.crowd.integration.springsecurity.user;

import org.springframework.dao.DataAccessException;

/**
 * Represents a Crowd-Server specific error, eg.
 * connection problems, application authentication problems, etc.
 *
 * @author Shihab Hamid
 */
public class CrowdDataAccessException extends DataAccessException
{
    /**
     * Constructor for DataAccessException.
     *
     * @param msg the detail message
     */
    public CrowdDataAccessException(String msg)
    {
        super(msg);
    }

    /**
     * Constructor for DataAccessException.
     *
     * @param msg   the detail message
     * @param cause the root cause (usually from using a underlying
     *              data access API such as JDBC)
     */
    public CrowdDataAccessException(String msg, Throwable cause)
    {
        super(msg, cause);
    }

    /**
     * Use CrowdDataAccessException to wrap underlying error.
     *
     * @param e underlying exception.
     */
    public CrowdDataAccessException(Exception e)
    {
        super(e.getMessage(), e);
    }
}
