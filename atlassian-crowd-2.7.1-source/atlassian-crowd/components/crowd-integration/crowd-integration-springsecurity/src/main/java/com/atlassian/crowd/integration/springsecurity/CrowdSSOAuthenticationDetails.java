package com.atlassian.crowd.integration.springsecurity;

import java.io.Serializable;
import java.util.Arrays;

import com.atlassian.crowd.model.authentication.ValidationFactor;

/**
 * The authentication details for an AuthenticationToken.
 * 
 * This consists of:
 * <ol>
 * <li>applicationName: name of the Crowd "application" to authenticate against</li>
 * <li>validationFactors: array of request validation factors like remote IP, user-agent, etc.</li>
 * </ol>
 * 
 */

public class CrowdSSOAuthenticationDetails implements Serializable
{
    private final String applicationName;
    private final ValidationFactor[] validationFactors;

    public CrowdSSOAuthenticationDetails(String applicationName, ValidationFactor[] validationFactors)
    {
        this.applicationName = applicationName;
        this.validationFactors = validationFactors;
    }

    public String getApplicationName()
    {
        return applicationName;
    }

    public ValidationFactor[] getValidationFactors()
    {
        return validationFactors;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CrowdSSOAuthenticationDetails that = (CrowdSSOAuthenticationDetails) o;

        if (applicationName != null ? !applicationName.equals(that.applicationName) : that.applicationName != null)
            return false;
        if (!Arrays.equals(validationFactors, that.validationFactors)) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = applicationName != null ? applicationName.hashCode() : 0;
        result = 31 * result + (validationFactors != null ? Arrays.hashCode(validationFactors) : 0);
        return result;
    }
}
