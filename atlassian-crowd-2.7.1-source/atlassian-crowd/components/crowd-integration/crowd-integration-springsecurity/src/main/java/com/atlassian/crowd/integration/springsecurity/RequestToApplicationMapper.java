package com.atlassian.crowd.integration.springsecurity;

import javax.servlet.http.HttpServletRequest;

/**
 * Maps request paths (Ant format) to application
 * names.
 *
 * This class is only required if you are using
 * multiple Crowd "applications" from the single
 * Spring Security context.
 */
public interface RequestToApplicationMapper
{
    String getApplication(String path);

    String getApplication(HttpServletRequest request);

    void addSecureMapping(String path, String applicationName);

    void removeSecureMapping(String path);

    void removeAllMappings(String applicationName);
}
