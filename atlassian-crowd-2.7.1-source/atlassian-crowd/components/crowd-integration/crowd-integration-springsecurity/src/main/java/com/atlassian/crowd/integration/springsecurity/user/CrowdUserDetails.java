package com.atlassian.crowd.integration.springsecurity.user;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.api.UserComparator;
import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.util.SOAPPrincipalHelper;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import static com.atlassian.crowd.model.user.UserConstants.DISPLAYNAME;
import static com.atlassian.crowd.model.user.UserConstants.EMAIL;
import static com.atlassian.crowd.model.user.UserConstants.FIRSTNAME;
import static com.atlassian.crowd.model.user.UserConstants.LASTNAME;
import static com.atlassian.crowd.model.user.UserConstants.USERNAME;

/**
 * Implements a basic SOAPPrincipal wrapper for Crowd principals.
 *
 * All attributes on this object are obtained directly from
 * the Crowd server.
 *
 * @author Shihab Hamid
 */

public class CrowdUserDetails implements UserDetails
{
    private final SOAPPrincipal principal;
    private final Collection<GrantedAuthority> authorities;

    public CrowdUserDetails(SOAPPrincipal principal, GrantedAuthority[] authorities)
    {
        this(principal, Arrays.asList(authorities));
    }

    public CrowdUserDetails(SOAPPrincipal principal, Collection<GrantedAuthority> authorities)
    {
        this.principal = principal;
        this.authorities = authorities;
    }

    /**
     * Returns the authorities granted to the user. Cannot return <code>null</code>.
     *
     * @return the authorities (never <code>null</code>)
     */
    @Override
    public Collection<GrantedAuthority> getAuthorities()
    {
        return authorities;
    }

    /**
     * Returns the remote principal that has authenticated.
     *
     * Before Crowd 2.7, this method was returning SOAPPrincipal. In Crowd 2.7, the return type was changed to
     * the API-neutral UserWithAttributes. Applications using this method will have to be updated.
     *
     * @return remote principal
     */
    public UserWithAttributes getRemotePrincipal()
    {
        return new UserWithAttributesAdapter(principal, buildUserAttributeMap(principal));
    }

    /**
     * Returns the password used to authenticate the user. Cannot return <code>null</code>.
     *
     * @return the password (never <code>null</code>). Always throws UnsupportedOperationException as we don't want to risk exposing the password of a user.
     */
    @Override
    public String getPassword()
    {
        throw new UnsupportedOperationException("Not giving you the password");
    }

    /**
     * Returns the username used to authenticate the user. Cannot return <code>null</code>.
     *
     * @return the username (never <code>null</code>)
     */
    @Override
    public String getUsername()
    {
        return principal.getName();
    }

    /**
     * Indicates whether the user's account has expired. An expired account cannot be authenticated.
     *
     * @return <code>true</code> always.
     */
    @Override
    public boolean isAccountNonExpired()
    {
        return true;
    }

    /**
     * Indicates whether the user is locked or unlocked. A locked user cannot be authenticated.
     *
     * @return <code>true</code> always.
     */
    @Override
    public boolean isAccountNonLocked()
    {
        return true;
    }

    /**
     * Indicates whether the user's credentials (password) has expired. Expired credentials prevent
     * authentication.
     *
     * @return <code>true</code> always.
     */
    @Override
    public boolean isCredentialsNonExpired()
    {
        return true;
    }

    /**
     * Indicates whether the user is enabled or disabled. A disabled user cannot be authenticated.
     *
     * @return <code>true</code> if the user is active, <code>false</code> otherwise.
     */
    @Override
    public boolean isEnabled()
    {
        return principal.isActive();
    }

    public String getFirstName()
    {
        return getFirstAttributeValue(UserConstants.FIRSTNAME, principal);
    }

    public String getLastName()
    {
        return getFirstAttributeValue(UserConstants.LASTNAME, principal);
    }

    public String getEmail()
    {
        return getFirstAttributeValue(UserConstants.EMAIL, principal);
    }

    public String getAttribute(String attributeName)
    {
        return getFirstAttributeValue(attributeName, principal);
    }

    public String getFullName()
    {
        return getFirstAttributeValue(UserConstants.DISPLAYNAME, principal);
    }

    private String getFirstAttributeValue(String name, SOAPPrincipal principal)
    {
        SOAPAttribute attribute = getAttribute(name, principal);
        if (attribute != null && attribute.getValues() != null && attribute.getValues().length > 0)
        {
            return attribute.getValues()[0];
        }
        else
        {
            return null;
        }
    }

    private SOAPAttribute getAttribute(String name, SOAPPrincipal principal)
    {
        SOAPAttribute[] attributes = principal.getAttributes();

        if (attributes != null)
        {
            for (SOAPAttribute attribute : attributes)
            {
                if (attribute.getName().equals(name))
                {
                    return attribute;
                }
            }
        }

        // no attribute found
        return null;
    }

    // The two methods and the inner class below adapt a SOAPPrincipal to a UserWithAttributes.
    // This code has been mostly borrowed from ObjectAdapter. Once CWD-2659 is completed, these
    // methods and class won't be necessary anymore.

    private static final Set<String> PRIMARY_USER_ATTRIBUTES =
        ImmutableSet.of(FIRSTNAME, LASTNAME, DISPLAYNAME, USERNAME, EMAIL);

    private static Map<String, Set<String>> buildUserAttributeMap(SOAPPrincipal soapUser)
    {
        Map<String, Set<String>> attributes = new HashMap<String, Set<String>>();

        if (soapUser.getAttributes() != null)
        {
            for (int i = 0; i < soapUser.getAttributes().length; i++)
            {
                // only add secondary attributes to the user
                SOAPAttribute soapAttribute = soapUser.getAttributes()[i];
                if (!PRIMARY_USER_ATTRIBUTES.contains(soapAttribute.getName()))
                {
                    attributes.put(soapAttribute.getName(), Sets.newHashSet(soapAttribute.getValues()));
                }
            }
        }
        return attributes;
    }

    private static class UserWithAttributesAdapter implements UserWithAttributes
    {
        private final SOAPPrincipal principal;
        private final Map<String, Set<String>> attributes;
        private final SOAPPrincipalHelper helper;

        public UserWithAttributesAdapter(SOAPPrincipal principal,
                                         Map<String, Set<String>> attributes)
        {
            this.principal = principal;
            this.attributes = attributes;
            this.helper = new SOAPPrincipalHelper();
        }

        @Override
        public Set<String> getValues(String key)
        {
            return attributes.get(key);
        }

        @Override
        public String getValue(String key)
        {
            return attributes.containsKey(key) ? attributes.get(key).iterator().next() : null;
        }

        @Override
        public Set<String> getKeys()
        {
            return attributes.keySet();
        }

        @Override
        public boolean isEmpty()
        {
            return attributes.isEmpty();
        }

        @Override
        public String getFirstName()
        {
            return helper.getFirstName(principal);
        }

        @Override
        public String getLastName()
        {
            return helper.getLastName(principal);
        }

        @Override
        public String getExternalId()
        {
            return null;
        }

        @Override
        public long getDirectoryId()
        {
            return principal.getDirectoryId();
        }

        @Override
        public boolean isActive()
        {
            return principal.isActive();
        }

        @Override
        public String getEmailAddress()
        {
            return helper.getEmail(principal);
        }

        @Override
        public String getDisplayName()
        {
            return helper.getFullName(principal);
        }

        @Override
        public int compareTo(User user)
        {
            return UserComparator.compareTo(this, user);
        }

        @Override
        public String getName()
        {
            return principal.getName();
        }
    }
}