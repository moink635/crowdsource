package com.atlassian.crowd.integration.springsecurity;

import org.springframework.security.authentication.BadCredentialsException;

/**
 * Represents a failed authentication attempt using an SSO token
 * that is not valid.
 */

public class CrowdSSOTokenInvalidException extends BadCredentialsException
{
    /**
     * Constructs an <code>AuthenticationException</code> with the specified
     * message and root cause.
     *
     * @param msg the detail message
     * @param t   the root cause
     */
    public CrowdSSOTokenInvalidException(String msg, Throwable t)
    {
        super(msg, t);
    }

    /**
     * Constructs an <code>AuthenticationException</code> with the specified
     * message and no root cause.
     *
     * @param msg the detail message
     */
    public CrowdSSOTokenInvalidException(String msg)
    {
        super(msg);
    }
}
