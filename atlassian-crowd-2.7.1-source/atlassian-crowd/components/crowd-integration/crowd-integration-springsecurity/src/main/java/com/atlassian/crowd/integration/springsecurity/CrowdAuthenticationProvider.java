package com.atlassian.crowd.integration.springsecurity;

import java.rmi.RemoteException;

import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.model.authentication.ValidationFactor;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * The CrowdAuthenticationProvider can be used in both SSO and non-SSO mode.
 *
 * When coupled with the CrowdSSOAuthenticationProcessingFilter, single-sign on
 * is establish via the Crowd server and Crowd SSO tokens.
 *
 * When coupled with the Spring Security AuthenticationProcessingFilter, centralised
 * authentication is established via the Crowd server.
 *
 * @author Shihab Hamid
 */
public abstract class CrowdAuthenticationProvider implements AuthenticationProvider
{
    private static final Log logger = LogFactory.getLog(CrowdAuthenticationProvider.class);

    protected final String applicationName;

    /**
     * The defaultApplicationName to use when an application name
     * has not been supplied in the AuthenticationToken.details().
     *
     * This applicationName should correspond to the name of an
     * application within Crowd.
     *
     * @param applicationName Crowd application name.
     */
    protected CrowdAuthenticationProvider(String applicationName)
    {
        this.applicationName = applicationName;
    }

    /**
     * Performs authentication with the same contract as {@link
     * org.springframework.security.AuthenticationManager#authenticate(org.springframework.security.Authentication)}.
     *
     * This AuthenticationProvider supports UsernamePasswordAuthenticationTokens for
     * login operations where a username, password and possiblyy validation factors (for SSO)
     * are provided. It also supports CrowdSSOAuthenticationToken for authentication
     * verification operations, where the SSO token and validation factors are provided
     * for SSO authentication.
     *
     * See CrowdAuthenticationProvider.authenticateUsernamePassword() and
     * CrowdAuthenticationProvider.authenticateCrowdSSO() for more specific
     * information on the authentication process.
     *
     * @param authentication the authentication request object.
     * @return a fully authenticated object including credentials. May return <code>null</code> if the
     *         <code>AuthenticationProvider</code> is unable to support authentication of the passed
     *         <code>Authentication</code> object. In such a case, the next <code>AuthenticationProvider</code> that
     *         supports the presented <code>Authentication</code> class will be tried.
     * @throws org.springframework.security.AuthenticationException
     *          if authentication fails.
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException
    {
        if (!supports(authentication.getClass()))
        {
            return null;
        }

        if (!supports((AbstractAuthenticationToken) authentication))
        {
            return null;
        }

        Authentication authenticatedToken = null;

        if (authentication instanceof UsernamePasswordAuthenticationToken)
        {
            // we need to authenticate the username/password
            logger.debug("Processing a UsernamePasswordAuthenticationToken");

            authenticatedToken = authenticateUsernamePassword((UsernamePasswordAuthenticationToken) authentication);

        }
        else if (authentication instanceof CrowdSSOAuthenticationToken)
        {
            // we need to authenticate an existing crowd token
            logger.debug("Processing a CrowdSSOAuthenticationToken");

            authenticatedToken = authenticateCrowdSSO((CrowdSSOAuthenticationToken) authentication);
        }

        return authenticatedToken;
    }

    /**
     * Attempts to authenticate a login request based on username (principal), password (credentials),
     * and (optional) ValidationFactor[]s (details).
     *
     * The returned Authentication will be either:
     * - a UsernamePasswordAuthenticationToken, if the request has no ValidationFactor[]s and hence is not SSO. The credentials will be the password.
     * - a CrowdSSOAuthenticationToken, if the request does have ValidationFactor[]s. The credentials will be set to the SSO token string.
     *
     * The principal will be set to the UserDetails object corresponding to the username.
     * The granted authorities will be UserDetails.getAuthorities().
     *
     * @param passwordToken authentication token containing the username, password and (optiona) ValidationFactor[]s.
     * @return an authenticated Authentication token.
     * @throws AuthenticationException if there was a problem authenticating the username/password combination.
     */
    protected Authentication authenticateUsernamePassword(UsernamePasswordAuthenticationToken passwordToken) throws AuthenticationException
    {
        if (passwordToken.getPrincipal() == null || StringUtils.isEmpty(passwordToken.getPrincipal().toString()))
        {
            throw new BadCredentialsException("UsernamePasswordAuthenticationToken contains empty username");
        }
        if (passwordToken.getCredentials() == null || StringUtils.isEmpty(passwordToken.getCredentials().toString()))
        {
            throw new BadCredentialsException("UsernamePasswordAuthenticationToken contains empty password");
        }

        Authentication authenticatedToken = null;
        try
        {
            if (passwordToken.getDetails() != null && passwordToken.getDetails() instanceof CrowdSSOAuthenticationDetails)
            {
                // we have an SSO authentication request (ie. we want a token back)
                CrowdSSOAuthenticationDetails details = (CrowdSSOAuthenticationDetails) passwordToken.getDetails();

                String crowdTokenString = authenticate(passwordToken.getPrincipal().toString(), passwordToken.getCredentials().toString(), details.getValidationFactors());
                CrowdUserDetails userDetails = loadUserByUsername(passwordToken.getPrincipal().toString());
                authenticatedToken = new CrowdSSOAuthenticationToken(userDetails, crowdTokenString, userDetails.getAuthorities());
            }
            else
            {
                // we have a centralised authentication request (ie. we don't care about the crowd token)
                authenticate(passwordToken.getPrincipal().toString(), passwordToken.getCredentials().toString(), new ValidationFactor[0]);
                CrowdUserDetails userDetails = loadUserByUsername(passwordToken.getPrincipal().toString());
                authenticatedToken = new UsernamePasswordAuthenticationToken(userDetails, passwordToken.getCredentials(), userDetails.getAuthorities());
            }
        }
        catch (Exception e)
        {
            throw translateException(e);
        }

        return authenticatedToken;
    }

    /**
     * Determine if a remote user is authenticated via SSO based on
     * the supplied SSO token string and validation factors.
     *
     * @param token Crowd SSO token.
     * @param validationFactors validation factors.
     * @return <code>true</code> iff the remote user is authenticated.
     * @throws InvalidAuthorizationTokenException invalid application client.
     * @throws RemoteException Crowd server error.
     * @throws ApplicationAccessDeniedException user does not have access to the application.
     */
    protected abstract boolean isAuthenticated(String token, ValidationFactor[] validationFactors)
            throws InvalidAuthorizationTokenException, RemoteException, ApplicationAccessDeniedException, InvalidAuthenticationException;

    /**
     * Authenticate a remote user and return the Crowd SSO token string.
     *
     * @param username username of the remote user.
     * @param password password of the remote user.
     * @param validationFactors validation factors from the remote user.
     * @return Crowd SSO token string
     * @throws InvalidAuthorizationTokenException invalid application client.
     * @throws com.atlassian.crowd.exception.InvalidAuthenticationException invalid username/password.
     * @throws RemoteException Crowd server error.
     * @throws InactiveAccountException inactive user account.
     * @throws com.atlassian.crowd.exception.ApplicationAccessDeniedException user does not have access to the application.
     * @throws ExpiredCredentialException The user's credentials have expired.  The user must change their credentials in order to successfully authenticate.
     */
    protected abstract String authenticate(String username, String password, ValidationFactor[] validationFactors) throws InvalidAuthorizationTokenException, InvalidAuthenticationException, InactiveAccountException, ApplicationAccessDeniedException, RemoteException, ExpiredCredentialException;

    /**
     * Retreive the user details for a user based on their username.
     *
     * @param username username of user.
     * @return user details of user.
     * @throws UsernameNotFoundException user with supplied username does not exist.
     * @throws DataAccessException error retrieving user.
     */
    protected abstract CrowdUserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException;

    /**
     * Retrieve a user from Crowd by looking up the principal by their authenticated Crowd token.
     *
     * @param token Crowd SSO token string.
     * @return CrowdUserDetails corresponding to the principal.
     * @throws com.atlassian.crowd.integration.springsecurity.CrowdSSOTokenInvalidException if the provided token is invalid.
     * @throws org.springframework.dao.DataAccessException error retrieveing user.
     */
    protected abstract CrowdUserDetails loadUserByToken(String token) throws CrowdSSOTokenInvalidException, DataAccessException;

    /**
     * Attempts to authenticate based on an existing Crowd token and validation factors
     * from a HttpServletRequest.
     *
     * The credentials of the ssoToken must be set to the String representation of the
     * Crowd SSO token, the details must be set to the ValidationFactor[]s from the
     * request.
     *
     * The returned authentication will be a CrowdSSOAuthenticationToken with the same
     * SSO token string credential. The principal will be set to the UserDetails object
     * corresponding to the username. The granted authorities will be UserDetails.getAuthorities().
     *
     * @param ssoToken ssoToken containing the token string credential and validation factors as details.
     * @return an authenticated Authentication token.
     * @throws AuthenticationException if there was a problem verifying the existing token is valid.
     */
    protected Authentication authenticateCrowdSSO(CrowdSSOAuthenticationToken ssoToken) throws AuthenticationException
    {
        if (ssoToken.getCredentials() == null || StringUtils.isEmpty(ssoToken.getCredentials().toString()))
        {
            throw new BadCredentialsException("CrowdSSOAuthenticationToken contains empty token credential");
        }
        if (ssoToken.getDetails() == null || !(ssoToken.getDetails() instanceof CrowdSSOAuthenticationDetails))
        {
            throw new BadCredentialsException("CrowdSSOAuthenticationToken does not contain any validation factors");
        }

        Authentication authenticatedToken = null;
        String crowdTokenString = ssoToken.getCredentials().toString();
        CrowdSSOAuthenticationDetails details = (CrowdSSOAuthenticationDetails) ssoToken.getDetails();
        try
        {
            if (!isAuthenticated(crowdTokenString, details.getValidationFactors()))
            {
                throw new CrowdSSOTokenInvalidException("Crowd SSO token is invalid");
            }

            CrowdUserDetails userDetails = loadUserByToken(crowdTokenString);
            authenticatedToken = new CrowdSSOAuthenticationToken(userDetails, crowdTokenString, userDetails.getAuthorities());
        }
        catch (Exception e)
        {
            throw translateException(e);
        }

        return authenticatedToken;
    }

    /**
     * Converts Crowd-specific exceptions to Spring Security-friendly exceptions.
     *
     * @param e Crowd-specific exception.
     * @return Spring Security-friendly exception.
     */
    protected AuthenticationException translateException(Exception e)
    {
        if (e instanceof AuthenticationException)
        {
            // no need to translate this
            return (AuthenticationException) e;
        }
        if (e instanceof ApplicationAccessDeniedException)
        {
            // special case: this is an authorisation exception
            return new CrowdAccessDeniedException("User does not have access to application: " + this.applicationName);
        }
        if (e instanceof ExpiredCredentialException)
        {
            // the user's credentials have expired
            return new CredentialsExpiredException(e.getMessage());
        }
        if (e instanceof InvalidAuthenticationException || e instanceof InvalidTokenException)
        {
            return new BadCredentialsException(e.getMessage(), e);
        }
        else if (e instanceof InactiveAccountException)
        {
            return new DisabledException(e.getMessage(), e);
        }
        else
        {
            return new AuthenticationServiceException(e.getMessage(), e);
        }
    }

    /**
     * Returns <code>true</code> if this <code>AuthenticationProvider</code> supports the indicated
     * <Code>Authentication</code> object.
     * <p/>
     * The <code>CrowdAuthenticationProvider</code> supports <code>UsernamePasswordAuthenticationToken</code>s
     * and <code>CrowdSSOAuthenticationToken</code>s.
     */
    @Override
    public boolean supports(Class<?> authentication)
    {
        return UsernamePasswordAuthenticationToken.class.isAssignableFrom(authentication)
                || CrowdSSOAuthenticationToken.class.isAssignableFrom(authentication);
    }

    public boolean supports(AbstractAuthenticationToken authenticationToken)
    {
        if (authenticationToken.getDetails() == null || !(authenticationToken.getDetails() instanceof CrowdSSOAuthenticationDetails))
        {
            // support all non-SSO authentication requests (for compatibility)
            return true;
        }
        else if (authenticationToken.getDetails() instanceof CrowdSSOAuthenticationDetails)
        {
            // support SSO requests that are only requests to the defaultApplication
            CrowdSSOAuthenticationDetails details = (CrowdSSOAuthenticationDetails) authenticationToken.getDetails();
            return details.getApplicationName().equals(applicationName);
        }
        else
        {
            return false;
        }
    }
}