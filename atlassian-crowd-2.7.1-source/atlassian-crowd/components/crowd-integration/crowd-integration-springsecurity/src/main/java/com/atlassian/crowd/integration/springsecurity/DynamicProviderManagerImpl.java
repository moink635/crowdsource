package com.atlassian.crowd.integration.springsecurity;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * An extension to the standard ProviderManager implementation
 * of the AuthenticationManager wich allows adding and removing
 * provider managers at runtime.
 * <p/>
 * This ensures that the provider manager is threadsafe (to
 * some degree). It is still possible to obtain a collection
 * of providers and manipulate it in a non-threadsafe manner
 * by using the getter/setter.
 * <p/>
 * Note: if you have no need for runtime-pluggable behaviour,
 * simply use org.springframework.security.providers.ProviderManager.
 */
public class DynamicProviderManagerImpl extends ProviderManager implements DynamicProviderManager
{
    private final ReadWriteLock readWriteLock;

    public DynamicProviderManagerImpl()
    {
        this.readWriteLock = new ReentrantReadWriteLock();
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException
    {
        readWriteLock.readLock().lock();

        try
        {
            return super.authenticate(authentication);
        }
        finally
        {
            readWriteLock.readLock().unlock();
        }
    }

    @Override
    public void addProvider(AuthenticationProvider provider)
    {
        readWriteLock.writeLock().lock();

        try
        {
            getProviders().add(provider);
        }
        finally
        {
            readWriteLock.writeLock().unlock();
        }
    }

    @Override
    public boolean removeProvider(AuthenticationProvider provider)
    {
        readWriteLock.writeLock().lock();

        try
        {
            return getProviders().remove(provider);
        }
        finally
        {
            readWriteLock.writeLock().unlock();
        }
    }
}
