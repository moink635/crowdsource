package com.atlassian.crowd.integration.springsecurity;

import org.springframework.security.core.AuthenticationException;

/**
 * Although this is an authorisation exception, Crowd
 * combines authentication and authorisation in one
 * call to the Crowd server to determine if a user is
 * allowed access to a particular remote application.
 *
 * This exception is thrown when the authentication
 * credentials are met for successful authentication,
 * but the user does not have access to the application.
 * For example, they are not in a directory with "allow
 * all to authenticate" or they are not a group that
 * has been allowed to authenticate.
 */
public class CrowdAccessDeniedException extends AuthenticationException
{
    public CrowdAccessDeniedException(String msg, Throwable t)
    {
        super(msg, t);
    }

    public CrowdAccessDeniedException(String msg)
    {
        super(msg);
    }
}
