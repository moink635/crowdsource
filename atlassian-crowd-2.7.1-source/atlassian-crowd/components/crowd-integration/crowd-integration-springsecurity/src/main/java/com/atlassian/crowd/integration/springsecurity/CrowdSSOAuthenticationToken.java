package com.atlassian.crowd.integration.springsecurity;

import java.util.Arrays;
import java.util.Collection;

import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;


/**
 * A CrowdSSOAuthenticationToken can be used to represent an
 * authentication request consisting of the Crowd SSO Token
 * String (credential) and HTTP ValidationFactors (details).
 *
 * It can also be used to represent a token for successful
 * authentication consisting of the CrowdUserDetails (principal),
 * the Crowd SSO Token String (credential) and a collection
 * of Group names the authenticated user is a member of
 * (GrantedAuthorities).
 */

public class CrowdSSOAuthenticationToken extends AbstractAuthenticationToken
{
    private static final String UNAUTHENTICATED_TOKEN_PRINCIPAL = "CROWD.SSO";

    private Object credentials;
    private Object principal;

    /**
     * Use constructor to create an unauthenticated SSO token.
     *
     * @param ssoToken token string.
     */
    public CrowdSSOAuthenticationToken(String ssoToken)
    {
        super(null);
        this.credentials = ssoToken;
        this.principal = UNAUTHENTICATED_TOKEN_PRINCIPAL;
        setAuthenticated(false);
    }

    /**
     * Use this constructor to create an authenticated SSO token.
     *
     * This should only be used by the AuthenticationProvider.
     *
     * @param principal authenticated user.
     * @param ssoToken authenticated SSO token as credential.
     * @param authorities granted authorities.
     */
    public CrowdSSOAuthenticationToken(CrowdUserDetails principal, String ssoToken, GrantedAuthority[] authorities)
    {
        this(principal, ssoToken, Arrays.asList(authorities));
    }

    public CrowdSSOAuthenticationToken(CrowdUserDetails principal, String ssoToken, Collection<GrantedAuthority> authorities)
    {
        super(authorities);
        this.principal = principal;
        this.credentials = ssoToken;
        super.setAuthenticated(true); // must use super, as we override
    }

    /**
     * The credentials that prove the principal is correct. This is usually a password, but could be anything
     * relevant to the <code>AuthenticationManager</code>. Callers are expected to populate the credentials.
     *
     * @return the credentials that prove the identity of the <code>Principal</code>
     */
    @Override
    public Object getCredentials()
    {
        return credentials;
    }

    /**
     * The identity of the principal being authenticated. This is usually a username. Callers are expected to
     * populate the principal.
     *
     * @return the <code>Principal</code> being authenticated
     */
    @Override
    public Object getPrincipal()
    {
        return principal;
    }

    /**
     * Disallow setAuthenticated(true).
     *
     * Use constructor containing GrantedAuthority[]s instead.
     *
     * @param isAuthenticated must be <code>false</code>.
     * @throws IllegalArgumentException throwd if isAuthenticated paramater is set to <code>true</code>.
     */
    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException
    {
        if (isAuthenticated)
        {
            throw new IllegalArgumentException("Cannot set this token to trusted - use constructor containing GrantedAuthority[]s instead");
        }

        super.setAuthenticated(false);
    }
}
