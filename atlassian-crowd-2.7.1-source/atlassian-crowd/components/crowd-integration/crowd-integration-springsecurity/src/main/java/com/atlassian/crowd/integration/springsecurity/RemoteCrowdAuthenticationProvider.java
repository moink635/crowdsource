package com.atlassian.crowd.integration.springsecurity;

import java.rmi.RemoteException;

import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.integration.http.HttpAuthenticator;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.AuthenticationManager;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * A concrete implementation of the CrowdAuthenticationProvider that uses
 * the crowd client libraries (SOAP) to communicate with the Crowd server.
 *
 * This should be the class developers use to integrate Crowd and Spring Security.
 */
public class RemoteCrowdAuthenticationProvider extends CrowdAuthenticationProvider
{
    protected final AuthenticationManager authenticationManager;
    protected final HttpAuthenticator httpAuthenticator;
    protected final CrowdUserDetailsService userDetailsService;

    public RemoteCrowdAuthenticationProvider(AuthenticationManager authenticationManager, HttpAuthenticator httpAuthenticator, CrowdUserDetailsService userDetailsService)
    {
        super(httpAuthenticator.getSoapClientProperties().getApplicationName());

        this.authenticationManager = authenticationManager;
        this.httpAuthenticator = httpAuthenticator;
        this.userDetailsService = userDetailsService;
    }

    @Override
    protected boolean isAuthenticated(String token, ValidationFactor[] validationFactors)
            throws InvalidAuthorizationTokenException, RemoteException, ApplicationAccessDeniedException, InvalidAuthenticationException
    {
        return authenticationManager.isAuthenticated(token, validationFactors);
    }

    @Override
    protected String authenticate(String username, String password, ValidationFactor[] validationFactors) throws InvalidAuthorizationTokenException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {
        return httpAuthenticator.verifyAuthentication(username, password, validationFactors);
    }

    @Override
    protected CrowdUserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException
    {
        return userDetailsService.loadUserByUsername(username);
    }

    @Override
    protected CrowdUserDetails loadUserByToken(String token) throws CrowdSSOTokenInvalidException, DataAccessException
    {
        return userDetailsService.loadUserByToken(token);
    }
}
