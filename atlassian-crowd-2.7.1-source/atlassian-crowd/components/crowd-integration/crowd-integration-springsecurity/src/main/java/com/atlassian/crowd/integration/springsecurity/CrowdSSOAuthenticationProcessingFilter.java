package com.atlassian.crowd.integration.springsecurity;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.integration.http.HttpAuthenticator;
import com.atlassian.crowd.model.authentication.ValidationFactor;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;

/**
 * The CrowdSSOAuthenticationProcessingFilter is to be used in
 * conjunction with the CrowdAuthenticationProvider to provide SSO
 * authentication.
 *
 * If single sign-on is not required, centralised authentication can
 * still be achieved by using the default AuthenticationProcessingFilter
 * in conjunction with the CrowdAuthenticationProvider.
 *
 * @author Shihab Hamid
 */
public class CrowdSSOAuthenticationProcessingFilter extends UsernamePasswordAuthenticationFilter
{
    private HttpAuthenticator httpAuthenticator;
    private RequestToApplicationMapper requestToApplicationMapper;
    private LoginUrlAuthenticationEntryPoint authenticationProcessingFilterEntryPoint;

    /**
     * This filter will process all requests, however, if the filterProcessesUrl
     * is part of the request URI, the filter will assume the request is a
     * username/password authentication (login) request and will not check
     * for Crowd SSO authentication. Authentication will proceed as defined in
     * the AuthenticationProcessingFilter.
     *
     * Otherwise, an authentication request to Crowd will be made to verify
     * any existing Crowd SSO token (via the ProviderManager).
     *
     * @param request servlet request containing either username/password paramaters
     * or the Crowd token as a cookie.
     * @param response servlet response to write out cookie.
     * @return <code>true</code> only if the filterProcessesUrl is in the request URI.
     */
    @Override
    protected boolean requiresAuthentication(final HttpServletRequest request, final HttpServletResponse response)
    {
        boolean usernamePasswordAuthentication = super.requiresAuthentication(request, response);

        if (!usernamePasswordAuthentication)
        {
            // if this request isn't a direct request for username/password authentication (login)
            // we MUST authenticate the request with Crowd (keep Spring Security's context in sync with
            // Crowd's SSO user)

            Authentication authenticatedToken = null;

            try
            {
                CrowdSSOAuthenticationToken crowdAuthRequest = new CrowdSSOAuthenticationToken(httpAuthenticator.getToken(request));
                doSetDetails(request, crowdAuthRequest);

                authenticatedToken = getAuthenticationManager().authenticate(crowdAuthRequest);
            }
            catch (InvalidTokenException e)
            {
                // no token in request, ie. we are not authenticated
            }
            catch (AuthenticationException e)
            {
                // token present but not valid (fails SSO authentication check)
            }

            // update the authentication token to a new token corresponding to the Crowd SSO user
            if (authenticatedToken == null)
            {
                // no authenticated SSO user, make sure Spring Security also knows there is no user authenticated
                SecurityContextHolder.clearContext();
            }
            else
            {
                SecurityContextHolder.getContext().setAuthentication(authenticatedToken);

                // write the cookie out to a token
                storeTokenIfCrowd(request, response, authenticatedToken);
            }
        }

        return usernamePasswordAuthentication;
    }


    /**
     * Provided so that subclasses may configure what is put into the authentication request's details
     * property.
     *
     * Sets the validation factors from the HttpServletRequest on the authentication request. Also sets
     * the application name to the name of application responsible for authorising a particular request.
     * For single-crowd-application-per-spring-security-context web apps, this will just return the application
     * name specified in the ClientProperties. For multi-crowd-applications-per-spring-security-context web apps,
     * the requestToApplicationMapper will be used to determine the application name.
     *
     * @param request     that an authentication request is being created for
     * @param authRequest the authentication request object that should have its details set
     */
    @Override
    protected void setDetails(HttpServletRequest request, UsernamePasswordAuthenticationToken authRequest)
    {
        doSetDetails(request, authRequest);
    }

    static String requestUriWithoutContext(HttpServletRequest request)
    {
        return request.getRequestURI().substring(request.getContextPath().length());
    }

    /**
     * <p>If the request has been redirected from a page it was not authorised to see, we want to
     * authenticate the login page using the application of the source page. The only pages that
     * should receive that special treatment are the login page itself and 'j_spring_security_check', the
     * submission target of the login page.</p>
     * <p>This method contains that definition, and will only return <code>true</code> for those pages.</p>
     *
     * @return is it safe to authenticate this resource as if it were the resource saved in the session?
     */
    boolean canUseSavedRequestToAuthenticate(HttpServletRequest request)
    {
        // Checks for j_spring_security_check
        if (super.requiresAuthentication(request, null))
        {
            return true;
        }

        if (authenticationProcessingFilterEntryPoint != null)
        {
            String loginFormUrl = authenticationProcessingFilterEntryPoint.getLoginFormUrl();

            return requestUriWithoutContext(request).equals(loginFormUrl);
        }
        else
        {
            return false;
        }
    }

    protected void doSetDetails(HttpServletRequest request, AbstractAuthenticationToken authRequest)
    {
        String application;

        if (requestToApplicationMapper != null)
        {
            // determine the target path
            String path;

            DefaultSavedRequest savedRequest = (DefaultSavedRequest) new HttpSessionRequestCache().getRequest(request, null);
            if (canUseSavedRequestToAuthenticate(request) && savedRequest != null)
            {
                path = savedRequest.getRequestURI().substring(savedRequest.getContextPath().length());
            }
            else
            {
                path = requestUriWithoutContext(request);
            }

            application = requestToApplicationMapper.getApplication(path);
        }
        else
        {
            // default to the "crowd" application
            application = httpAuthenticator.getSoapClientProperties().getApplicationName();
        }

        ValidationFactor[] validationFactors = httpAuthenticator.getValidationFactors(request);

        authRequest.setDetails(new CrowdSSOAuthenticationDetails(application, validationFactors));
    }

    private void storeTokenIfCrowd(HttpServletRequest request, HttpServletResponse response, Authentication authResult)
    {
        // write successful SSO token if there is one present
        if (authResult instanceof CrowdSSOAuthenticationToken)
        {
            if (authResult.getCredentials() != null)
            {
                try
                {
                    httpAuthenticator.setPrincipalToken(request, response, authResult.getCredentials().toString());
                }
                catch (Exception e)
                {
                    // occurs if application's auth token expires while trying to look up the domain property from the Crowd server
                    logger.error("Unable to set Crowd SSO token", e);
                }
            }
        }
    }

    /**
     * Attempts to write out the successful SSO token to a cookie,
     * if an SSO token was generated and stored via the AuthenticationProvider.
     *
     * This effectively establishes SSO when using the CrowdAuthenticationProvider
     * in conjunction with this filter.
     *
     * @param request servlet request.
     * @param response servlet response.
     * @param authResult result of a successful authentication. If it is a CrowdSSOAuthenticationToken
     * then the SSO token will be set to the "credentials" property.
     * @throws java.io.IOException not thrown.
     */
    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult)
            throws IOException, ServletException
    {
        storeTokenIfCrowd(request, response, authResult);

        super.successfulAuthentication(request, response, authResult);
    }

    /**
     * Attempts to remove any SSO tokens associated
     * with the request, effectively logging the user
     * out of Crowd.
     *
     * @param request servlet request.
     * @param response servlet response.
     * @param failed not required.
     * @throws IOException not thrown.
     */
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed)
            throws IOException, ServletException
    {
        try
        {
            httpAuthenticator.logoff(request, response);
        }
        catch (Exception e)
        {
            // occurs if application's auth token expires while trying to look up the domain property from the Crowd server
            logger.error("Unable to unset Crowd SSO token", e);
        }

        super.unsuccessfulAuthentication(request, response, failed);
    }

    /**
     * Mandatory dependency.
     *
     * @param httpAuthenticator used to extract validation factors, set cookies and perform logouts.
     */
    public void setHttpAuthenticator(HttpAuthenticator httpAuthenticator)
    {
        this.httpAuthenticator = httpAuthenticator;
    }

    /**
     * Optional dependency.
     *
     * @param requestToApplicationMapper only required if multiple Crowd "applications" need to
     * be accessed via the same Spring Security context, eg. when one web-application corresponds to
     * multiple Crowd "applications".
     */
    public void setRequestToApplicationMapper(RequestToApplicationMapper requestToApplicationMapper)
    {
        this.requestToApplicationMapper = requestToApplicationMapper;
    }

    /**
     * Optional dependency, only required if multiple Crowd applications are coexisting in the same
     * web-application. Used to discover the login page, through and treat it specially.
     */
    public void setLoginUrlAuthenticationEntryPoint(LoginUrlAuthenticationEntryPoint filterEntryPoint)
    {
        this.authenticationProcessingFilterEntryPoint = filterEntryPoint;
    }
}
