package com.atlassian.crowd.integration.springsecurity;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.integration.http.HttpAuthenticator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;

/**
 * Logout handler to logout of Crowd and remove the
 * Crowd SSO token cookie.
 *
 * This logout handler is required for SSO-based
 * Crowd authentication.
 *
 * @author Shihab Hamid.
 */

public class CrowdLogoutHandler implements LogoutHandler
{
    private static final Log logger = LogFactory.getLog(CrowdLogoutHandler.class);

    private HttpAuthenticator httpAuthenticator;

    /**
     * Causes a logout to be completed. The method must complete successfully.
     *
     * @param request        the HTTP request
     * @param response       the HTTP resonse
     * @param authentication the current principal details
     */
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
    {
        try
        {
            httpAuthenticator.logoff(request, response);
        }
        catch (Exception e)
        {
            logger.error("Could not logout SSO user from Crowd", e);
        }
    }

    public HttpAuthenticator getHttpAuthenticator()
    {
        return httpAuthenticator;
    }

    public void setHttpAuthenticator(HttpAuthenticator httpAuthenticator)
    {
        this.httpAuthenticator = httpAuthenticator;
    }
}
