package com.atlassian.crowd.integration.springsecurity;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.service.client.ClientProperties;

import org.springframework.security.web.savedrequest.DefaultSavedRequest;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.Assert;

/**
 * Maps request paths (Ant format) to application
 * names.
 *
 * This class is only required if you are using
 * multiple Crowd "applications" from the single
 * Spring Security context.
 */
public class RequestToApplicationMapperImpl implements RequestToApplicationMapper
{
    private final ClientProperties clientProperties;
    private final Map<String, String> pathToApplicationMap;
    private final AntPathMatcher pathMatcher;
    private ReadWriteLock readWriteLock;

    public RequestToApplicationMapperImpl(ClientProperties clientProperties)
    {
        this.clientProperties = clientProperties;
        this.pathToApplicationMap = new LinkedHashMap<String, String>(); // explicitly synchronised
        this.pathMatcher = new AntPathMatcher();
        this.readWriteLock = new ReentrantReadWriteLock();
    }

    @Override
    public String getApplication(String path)
    {
        Assert.notNull(path, "Path cannot be null");

        readWriteLock.readLock().lock();

        try
        {
            for (Map.Entry<String, String> entry : pathToApplicationMap.entrySet())
            {
                if (pathMatcher.match(entry.getKey(), path))
                {
                    // return the application name corresponding to the path
                    return entry.getValue();
                }
            }

            // if we fail to match a path, return the Crowd application
            return clientProperties.getApplicationName();
        }
        finally
        {
            readWriteLock.readLock().unlock();
        }
    }

    /**
     * Returns the application name for the target request.
     *
     * This corresponds to the application mapped for:
     * 1. The path of the target request saved in session (if any), or
     * 2. The path of the provided request (otherwise).
     *
     * @param request current HTTP request.
     * @return application name.
     */
    @Override
    public String getApplication(HttpServletRequest request)
    {
        // determine the target path
        String path;

        DefaultSavedRequest savedRequest = (DefaultSavedRequest) new HttpSessionRequestCache().getRequest(request, null);
        if (savedRequest != null)
        {
            path = savedRequest.getRequestURI().substring(savedRequest.getContextPath().length());
        }
        else
        {
            path = request.getRequestURI().substring(request.getContextPath().length());
        }

        return getApplication(path);
    }

    @Override
    public void addSecureMapping(String path, String applicationName)
    {
        Assert.notNull(path, "Path to add cannot be null");
        Assert.notNull(applicationName, "Application name to add cannot be null");

        readWriteLock.writeLock().lock();
        try
        {
            pathToApplicationMap.put(path, applicationName);
        }
        finally
        {
            readWriteLock.writeLock().unlock();
        }
    }

    @Override
    public void removeSecureMapping(String path)
    {
        Assert.notNull(path, "Path to remove cannot be null");

        readWriteLock.writeLock().lock();

        try
        {
            pathToApplicationMap.remove(path);
        }
        finally
        {
            readWriteLock.writeLock().unlock();
        }
    }

    @Override
    public void removeAllMappings(String applicationName)
    {
        Assert.notNull(applicationName, "Application name cannot be null");

        readWriteLock.writeLock().lock();

        try
        {
            for (Iterator<Map.Entry<String, String>> entries = pathToApplicationMap.entrySet().iterator(); entries.hasNext();)
            {
                Map.Entry entry = entries.next();
                if (entry.getValue().equals(applicationName))
                {
                    entries.remove();
                }
            }
        }
        finally
        {
            readWriteLock.writeLock().unlock();
        }
    }
}
