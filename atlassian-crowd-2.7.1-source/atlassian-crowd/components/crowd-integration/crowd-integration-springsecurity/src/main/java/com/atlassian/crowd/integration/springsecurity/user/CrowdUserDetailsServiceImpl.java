package com.atlassian.crowd.integration.springsecurity.user;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.springsecurity.CrowdSSOTokenInvalidException;
import com.atlassian.crowd.service.AuthenticationManager;
import com.atlassian.crowd.service.GroupMembershipManager;
import com.atlassian.crowd.service.UserManager;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Retrieves users from Crowd using Crowd's remote API.
 *
 * @author Shihab Hamid
 */
public class CrowdUserDetailsServiceImpl implements CrowdUserDetailsService
{
    private UserManager userManager;
    private AuthenticationManager authenticationManager;
    private GroupMembershipManager groupMembershipManager;
    private String authorityPrefix = "";
    private Iterable<Map.Entry<String, String>> groupToAuthorityMappings;
    private String adminAuthority = "ROLE_ADMIN";

    @Override
    public CrowdUserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException
    {
        try
        {
            SOAPPrincipal principal = userManager.getUser(username);
            return new CrowdUserDetails(principal, getAuthorities(principal.getName()));
        }
        catch (InvalidAuthorizationTokenException e)
        {
            throw new CrowdDataAccessException(e);
        }
        catch (RemoteException e)
        {
            throw new CrowdDataAccessException(e);
        }
        catch (UserNotFoundException e)
        {
            throw new UsernameNotFoundException("Could not find principal in Crowd with username: " + username, e);
        }
        catch (InvalidAuthenticationException e)
        {
            throw new CrowdDataAccessException(e);
        }
    }

    @Override
    public CrowdUserDetails loadUserByToken(String token) throws CrowdSSOTokenInvalidException, DataAccessException
    {
        try
        {
            SOAPPrincipal principal = userManager.getUserFromToken(token);
            return new CrowdUserDetails(principal, getAuthorities(principal.getName()));
        }
        catch (InvalidAuthorizationTokenException e)
        {
            throw new CrowdDataAccessException(e);
        }
        catch (RemoteException e)
        {
            throw new CrowdDataAccessException(e);
        }
        catch (UsernameNotFoundException e)
        {
            throw new CrowdDataAccessException(e);
        }
        catch (InvalidTokenException e)
        {
            throw new CrowdSSOTokenInvalidException(e.getMessage(), e);
        }
        catch (InvalidAuthenticationException e)
        {
            throw new CrowdDataAccessException(e);
        }
        catch (UserNotFoundException e)
        {
            throw new CrowdDataAccessException(e);
        }
    }

    /**
     * Calculates the authorities of a user.
     * </p>
     * If an authorityMap has been set, it is used to map group names in the user
     * membership to authority names. No authority is granted for group names that
     * do not exist in the map.
     * </p>
     * If an authorityMap has not been set, the user receives an authority for each one
     * of the groups he is a member of. The GrantedAuthority names match the group names, with
     * an optional prefix (authorityPrefix) prepended to them. Note that a request is
     * made to the Crowd server to retrieve the group memberships of the user, which may
     * be an expensive operation.
     *
     * @param username username of the user to look up.
     * @return groups memberships as GrantedAuthority objects.
     * @throws com.atlassian.crowd.exception.InvalidAuthorizationTokenException
     *                                  invalid application client.
     * @throws java.rmi.RemoteException underlying Crowd Server problem.
     * @throws UsernameNotFoundException  The user identified by <code>username</code> could not be found.
     */
    Collection<GrantedAuthority> getAuthorities(String username)
            throws InvalidAuthorizationTokenException, RemoteException, UserNotFoundException, InvalidAuthenticationException
    {
        if (groupToAuthorityMappings == null)
        {
            List/*<String>*/ userGroups = groupMembershipManager.getMemberships(username);

            return generateAuthoritiesFromGroupNames(userGroups);
        }
        else // We are going to apply a 'default' role to this user if there is a cross section between the authz groups and the user groups
        {
            return generateAuthorityFromMap(username);
        }
    }

    private Set<GrantedAuthority> generateAuthorityFromMap(String username)
            throws RemoteException, InvalidAuthenticationException, InvalidAuthorizationTokenException
    {
        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        for (Map.Entry<String, String> groupToAuthorityMapEntry : groupToAuthorityMappings)
        {
            if (groupMembershipManager.isMember(username, groupToAuthorityMapEntry.getKey()))
            {
                authorities.add(new SimpleGrantedAuthority(groupToAuthorityMapEntry.getValue()));
            }
        }
        return authorities;
    }

    private List<GrantedAuthority> generateAuthoritiesFromGroupNames(List userGroups)
    {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        for (Iterator iterator = userGroups.iterator(); iterator.hasNext();)
        {
            authorities.add(new SimpleGrantedAuthority(getAuthorityPrefix() + iterator.next()));
        }
        return authorities;
    }

    @Override
    public String getAuthorityPrefix()
    {
        return authorityPrefix;
    }

    @Override
    public void setAuthorityPrefix(String authorityPrefix)
    {
        this.authorityPrefix = authorityPrefix;
    }

    @Override
    public Iterable<Map.Entry<String,String>> getGroupToAuthorityMappings()
    {
        return groupToAuthorityMappings;
    }

    @Override
    public void setGroupToAuthorityMappings(Iterable<Map.Entry<String, String>> groupToAuthorityMappings)
    {
        this.groupToAuthorityMappings = groupToAuthorityMappings;
    }

    @Override
    public String getAdminAuthority()
    {
        return adminAuthority;
    }

    @Override
    public void setAdminAuthority(String adminAuthority)
    {
        this.adminAuthority = adminAuthority;
    }

    public void setUserManager(UserManager userManager)
    {
        this.userManager = userManager;
    }

    public void setGroupMembershipManager(GroupMembershipManager groupMembershipManager)
    {
        this.groupMembershipManager = groupMembershipManager;
    }

    public void setAuthenticationManager(AuthenticationManager authenticationManager)
    {
        this.authenticationManager = authenticationManager;
    }
}
