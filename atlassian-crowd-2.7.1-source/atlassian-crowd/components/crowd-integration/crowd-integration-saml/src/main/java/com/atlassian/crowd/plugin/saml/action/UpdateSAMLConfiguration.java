package com.atlassian.crowd.plugin.saml.action;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.plugin.saml.SAMLException;
import com.atlassian.crowd.plugin.saml.SAMLMessageManager;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;

import java.util.ArrayList;
import java.util.List;

public class UpdateSAMLConfiguration extends BaseAction
{
    protected long ID = -1; // remember App ID, so we can go back to others from this tab.

    // dependencies
    private SAMLMessageManager samlMessageManager;
    private ClientProperties clientProperties;

    // view output
    private String samlAuthURL;
    private String signOutURL;
    private String passwordURL;
    private String keyPath;
    private boolean keysFound;
    private String applicationName; // set as static param via xwork

    public String doDefault() throws Exception
    {
        String baseURL = clientProperties.getBaseURL();
        samlAuthURL = baseURL + "/console/plugin/secure/saml/samlauth.action";
        signOutURL  = baseURL + "/console/logoff.action";
        passwordURL = baseURL + "/console/user/viewchangepassword.action";
        keyPath = samlMessageManager.getKeyPath();
        keysFound = samlMessageManager.hasValidKeys();
   
        return INPUT;
    }

    public String doDeleteKeys() throws Exception
    {
        samlMessageManager.deleteKeys();
        addActionMessage("green", getText("saml.key.del.success"));

        return doDefault();
    }

    public String doGenerateKeys() throws Exception
    {
        try
        {
            samlMessageManager.generateKeys();
            addActionMessage("green", getText("saml.key.gen.success"));
        }
        catch (SAMLException e)
        {
            addActionError(getText("saml.key.gen.error") + e.getMessage());
            logger.error("Failed to generate key", e);
        }

        return doDefault();
    }

    public String getKeyPath()
    {
        return keyPath;
    }

    public boolean isKeysFound()
    {
        return keysFound;
    }

    public String getSamlAuthURL()
    {
        return samlAuthURL;
    }

    public String getSignOutURL()
    {
        return signOutURL;
    }

    public String getPasswordURL()
    {
        return passwordURL;
    }

    public void setSamlMessageManager(SAMLMessageManager samlMessageManager)
    {
        this.samlMessageManager = samlMessageManager;
    }

    public void setClientProperties(ClientProperties clientProperties)
    {
        this.clientProperties = clientProperties;
    }

    public String getApplicationName()
    {
        return applicationName;
    }

    public void setApplicationName(String applicationName)
    {
        this.applicationName = applicationName;
    }

    public List getWebItemsForApplication()
    {
        List<WebItemModuleDescriptor> appItems = new ArrayList<WebItemModuleDescriptor>();
        appItems.addAll(getWebInterfaceManager().getDisplayableItems("application-tabs", getWebFragmentsContextMap()));
        appItems.addAll(getWebInterfaceManager().getDisplayableItems("application-tabs:" + applicationName, getWebFragmentsContextMap()));
        return appItems;
    }

    public long getID()
    {
        return ID;
    }

    public void setID(long ID)
    {
        this.ID = ID;
    }
}
