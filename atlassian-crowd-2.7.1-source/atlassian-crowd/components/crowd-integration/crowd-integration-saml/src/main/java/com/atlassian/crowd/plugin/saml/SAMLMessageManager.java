package com.atlassian.crowd.plugin.saml;

public interface SAMLMessageManager
{
    /**
     * Parses the non null request parameters
     * for a SAML request into a SAML request
     * object.
     *
     * @param samlRequestXML request XML string, already URL-decoded, but still Base64 encoded and zipped
     * @param relayStateURL relay state URL.
     * @parm samlEncoding SAML encoding
     * @return SAML authentication request object.
     * @throws SAMLException error parsing request.
     */
    SAMLAuthRequest parseAuthRequest(String samlRequestXML, String relayStateURL, String samlEncoding)
        throws SAMLException;

    /**
     * Generates a successful authentication
     * response for an authentication request
     * given the username of the authenticated
     * user.
     *
     * @param authRequest authentication request object.
     * @param authenticatedUser username of authenticated user.
     * @return signed authentication response object.
     * @throws SAMLException error generating or signing XML response.
     */
    SAMLAuthResponse generateAuthResponse(SAMLAuthRequest authRequest, String authenticatedUser) throws SAMLException;

    /**
     * Generates and stores the private and public keys
     * in the Crowd home directory under the
     * "/plugin-data/crowd-saml-plugin" folder.
     * <p/>
     * This will overwrite any existing keys.
     *
     * @throws com.atlassian.crowd.plugin.saml.SAMLException if there was an error generating
     *                       or storing the new keys.
     */
    void generateKeys() throws SAMLException;

    /**
     * Deletes any generated DSA keys in the
     * "/plugin-data/crowd-saml-plugin" folder.
     * This also makes the SAMLMessageManager unable
     * to sign requests.
     */
    void deleteKeys();

    /**
     * @return directory path of the stored key-pair.
     */
    String getKeyPath();

    /**
     * The SAMLMessageManager is ready to perform generate signatures
     * if a valid key-pair has been loaded/generated.
     *
     * @return <code>true</code> if and only if a valid key-pair
     *         exists.
     */
    boolean hasValidKeys();
}
