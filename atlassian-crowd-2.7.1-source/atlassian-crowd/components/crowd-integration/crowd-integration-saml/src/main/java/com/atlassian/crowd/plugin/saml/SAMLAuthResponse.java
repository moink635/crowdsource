package com.atlassian.crowd.plugin.saml;

public class SAMLAuthResponse
{
    private final String signedResponseXML;
    private final String relayStateURL;
    private final String acsURL;

    public SAMLAuthResponse(String signedResponseXML, String relayStateURL, String acsURL)
    {
        this.signedResponseXML = signedResponseXML;
        this.relayStateURL = Util.htmlEncode(relayStateURL);
        this.acsURL = acsURL;
    }

    public String getSignedResponseXML()
    {
        return signedResponseXML;
    }

    public String getRelayStateURL()
    {
        return relayStateURL;
    }

    public String getAcsURL()
    {
        return acsURL;
    }
}

