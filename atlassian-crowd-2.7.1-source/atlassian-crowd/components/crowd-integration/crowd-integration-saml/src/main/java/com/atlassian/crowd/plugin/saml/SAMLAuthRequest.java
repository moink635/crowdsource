package com.atlassian.crowd.plugin.saml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;
import java.util.zip.ZipException;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.StringUtils;
import org.jdom.Document;

import com.google.common.base.Charsets;

public class SAMLAuthRequest
{
    /**
     * This is the encoding that MUST be supported by all SAML bindings, see SAML bindings spec section 3.4.4.1
     */
    public static final String DEFLATE_ENCODING = "urn:oasis:names:tc:SAML:2.0:bindings:URL-Encoding:DEFLATE";

    private final String issueInstant;
    private final String providerName;
    private final String acsURL;
    private final String requestID;
    private final String relayStateURL;

    /**
     * Parses the non null request parameters for a SAML request into a SAML request object.
     *
     * @param samlRequest request XML string, already URL-decoded, but still Base64 encoded
     * @param relayStateURL relay state URL.
     * @param samlEncoding encoding identification, see SAML bindings spec section 3.4.4.1
     * @throws SAMLException error parsing request.
     */
    public SAMLAuthRequest(String samlRequest, String relayStateURL, String samlEncoding) throws SAMLException
    {
        Validate.notNull(samlEncoding);

        if (StringUtils.isBlank(samlRequest) || StringUtils.isBlank(relayStateURL))
        {
            throw new SAMLException("Cannot process blank request");
        }

        // decode
        String requestXmlString = decodeAuthnRequestXML(samlRequest, samlEncoding);
        String[] samlRequestAttributes = getRequestAttributes(requestXmlString);

        // assign
        this.issueInstant = samlRequestAttributes[0];
        this.providerName = samlRequestAttributes[1];
        this.acsURL = samlRequestAttributes[2];
        this.requestID = samlRequestAttributes[3];
        this.relayStateURL = relayStateURL;
    }

    // just used for testing purposes
    protected SAMLAuthRequest(String issueInstant, String providerName, String acsURL, String requestID, String relayStateURL)
    {
        this.issueInstant = issueInstant;
        this.providerName = providerName;
        this.acsURL = acsURL;
        this.requestID = requestID;
        this.relayStateURL = relayStateURL;
    }

    public String getIssueInstant()
    {
        return issueInstant;
    }

    public String getProviderName()
    {
        return providerName;
    }

    public String getAcsURL()
    {
        return acsURL;
    }

    public String getRequestID()
    {
        return requestID;
    }

    public String getRelayStateURL()
    {
        return relayStateURL;
    }

    // The original version of these helper methods was adapted from Google sample code, which is now obsolete.

    // "decodeAuthnRequestXML" and "inflate" have been changed due to: CWD-1846
    // Code is based on:
    // http://groups.google.com/group/google-apps-apis/browse_thread/thread/e3127108876af7bb/6c1809c1167012ab?lnk=gst&q=inflate#6c1809c1167012ab

    /*
     * Retrieves the AuthnRequest from the encoded and compressed String extracted
     * from the URL. The AuthnRequest XML is retrieved in the following order: <p>
     * 1. URL decode <br> 2. Base64 decode <br> 3. Inflate <br> Returns the String
     * format of the AuthnRequest XML.
     */
    private static String decodeAuthnRequestXML(String encodedRequestXmlString, String encodingId) throws SAMLException
    {
        if (!encodingId.equals(DEFLATE_ENCODING))
        {
            throw new SAMLException("Unsupported SAML encoding: " + encodingId);
        }

        // No need to URL decode: auto decoded by request.getParameter() method

        byte[] base64DecodedByteArray = base64Decode(encodedRequestXmlString);

        try
        {
            return new String(inflate(base64DecodedByteArray));
        }
        catch (ZipException e)
        {
            throw new SAMLException("Error decoding SAML Authentication Request. Check decoding scheme. Base64 decoded SAML Request: "
                                    + Arrays.toString(base64DecodedByteArray), e);
        }
        catch (IOException e)
        {
            throw new SAMLException("Error decoding SAML Authentication Request. Check decoding scheme. Base64 decoded SAML Request: "
                                    + Arrays.toString(base64DecodedByteArray), e);
        }
    }

    /**
     * Decodes a SAML request using Base64.
     *
     * @param encodedRequestXmlString a Base64-encoded SAML request
     * @return Base64-decoded SAML request
     * @throws SAMLException if the parameter cannot be completely decoded as Base64
     */
    private static byte[] base64Decode(String encodedRequestXmlString) throws SAMLException
    {
        byte[] xmlBytes;
        xmlBytes = encodedRequestXmlString.getBytes(Charsets.UTF_8);

        if (!Base64.isBase64(xmlBytes))
        {
            throw new SAMLException("SAMLRequest is not Base64 encoded: " + encodedRequestXmlString);
        }
        return Base64.decodeBase64(xmlBytes);
    }

    private static byte[] inflate(byte[] bytes) throws IOException
    {
        Inflater decompressor = null;
        InflaterInputStream decompressorStream = null;
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try
        {
            decompressor = new Inflater(true);
            decompressorStream = new InflaterInputStream(new ByteArrayInputStream(bytes), decompressor);

            byte[] buf = new byte[1024];
            int count = decompressorStream.read(buf);
            while (count != -1)
            {
                out.write(buf, 0, count);
                count = decompressorStream.read(buf);
            }
            out.flush();
            return out.toByteArray();
        }
        finally
        {
            if (decompressor != null)
            {
                decompressor.end();
            }

            IOUtils.closeQuietly(decompressorStream);

            IOUtils.closeQuietly(out);
        }
    }

    /*
    * Creates a DOM document from the specified AuthnRequest xmlString and
    * extracts the value under the "AssertionConsumerServiceURL" attribute
    */
    private static String[] getRequestAttributes(String xmlString) throws SAMLException
    {
        try
        {
            Document doc = Util.createJdomDoc(xmlString);

            String[] samlRequestAttributes = new String[4];
            samlRequestAttributes[0] = doc.getRootElement().getAttributeValue("IssueInstant");
            samlRequestAttributes[1] = doc.getRootElement().getAttributeValue("ProviderName");
            samlRequestAttributes[2] = doc.getRootElement().getAttributeValue("AssertionConsumerServiceURL");
            samlRequestAttributes[3] = doc.getRootElement().getAttributeValue("ID");
            return samlRequestAttributes;
        }
        catch (SAMLException e)
        {
            throw new SAMLException("Error parsing AuthnRequest XML: " + xmlString, e);
        }
    }

}
