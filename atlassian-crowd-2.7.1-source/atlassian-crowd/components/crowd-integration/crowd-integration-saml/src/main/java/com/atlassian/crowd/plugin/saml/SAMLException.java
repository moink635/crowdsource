package com.atlassian.crowd.plugin.saml;

/**
 * An exception class for when there's a problem handling SAML
 * messages.
 */
public class SAMLException extends Exception
{
    protected String message = "";

    public SAMLException()
    {
    }

    public SAMLException(Throwable e)
    {
        super(e);
    }

    public SAMLException(String message)
    {
        this.message = message;
    }

    public SAMLException(String message, Throwable e)
    {
        super(message, e);
        this.message = message;        
    }

    public String getMessage()
    {
        return this.message;
    }

    public String toString()
    {
        return "SAML exception: " + message;
    }
}