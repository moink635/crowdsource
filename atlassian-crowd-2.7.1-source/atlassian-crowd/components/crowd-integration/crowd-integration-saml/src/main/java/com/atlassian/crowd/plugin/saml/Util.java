/*
 * Copyright (C) 2006 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.atlassian.crowd.plugin.saml;

import com.atlassian.security.random.DefaultSecureRandomService;
import com.atlassian.security.random.SecureRandomService;
import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.input.DOMBuilder;
import org.jdom.input.SAXBuilder;
import org.jdom.output.XMLOutputter;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * This utility class is used across the various servlets that make up the
 * SAML-based Single Sign-On Reference Tool. It includes various helper methods
 * that are used for the SAML transactions.
 */
public class Util
{
    // used for creating a randomly generated string
    private static SecureRandomService random = DefaultSecureRandomService.getInstance();
    private static final char[] charMapping = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'};

    /**
     * Converts a JDOM Document to a W3 DOM document.
     *
     * @param doc JDOM Document
     * @return W3 DOM Document if converted successfully, null otherwise
     */
    public static org.w3c.dom.Document toDom(org.jdom.Document doc) throws SAMLException
    {
        try
        {
            XMLOutputter xmlOutputter = new XMLOutputter();
            StringWriter elemStrWriter = new StringWriter();
            xmlOutputter.output(doc, elemStrWriter);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            dbf.setNamespaceAware(true);
            return dbf.newDocumentBuilder().parse(new InputSource(new StringReader(elemStrWriter.toString())));
        }
        catch (IOException e)
        {
            throw new SAMLException("Error converting JDOM document to W3 DOM document: " + e.getMessage(), e);
        }
        catch (ParserConfigurationException e)
        {
            throw new SAMLException("Error converting JDOM document to W3 DOM document: " + e.getMessage(), e);
        }
        catch (SAXException e)
        {
            throw new SAMLException("Error converting JDOM document to W3 DOM document: " + e.getMessage(), e);
        }
    }

    /**
     * Converts a JDOM Element to a W3 DOM Element
     *
     * @param element JDOM Element
     * @return W3 DOM Element if converted successfully, null otherwise
     */
    public static org.w3c.dom.Element toDom(org.jdom.Element element) throws SAMLException
    {
        return toDom(element.getDocument()).getDocumentElement();
    }

    /**
     * Converts a W3 DOM Element to a JDOM Element
     *
     * @param e W3 DOM Element
     * @return JDOM Element
     */
    public static org.jdom.Element toJdom(org.w3c.dom.Element e)
    {
        DOMBuilder builder = new DOMBuilder();
        org.jdom.Element jdomElem = builder.build(e);
        return jdomElem;
    }

    private static final InputSource emptyInputSource = new InputSource(new ByteArrayInputStream(new byte[0]));
    private static final EntityResolver emptyEntityResolver = new EntityResolver() {
        public InputSource resolveEntity(String arg0, String arg1) throws SAXException, IOException
        {
            return emptyInputSource;
        }
    };

    /**
     * Creates a JDOM Document from a string containing XML
     *
     * @param xmlString String version of XML
     * @return JDOM Document if file contents converted successfully, null
     *         otherwise
     */
    public static Document createJdomDoc(String xmlString) throws SAMLException
    {
        try
        {
            SAXBuilder builder = new SAXBuilder() {
                @Override
                protected XMLReader createParser() throws JDOMException
                {
                    try
                    {
                        SAXParserFactory spf = SAXParserFactory.newInstance();
                        spf.setNamespaceAware(true);
                        spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
                        spf.setFeature("http://javax.xml.XMLConstants/feature/secure-processing", true);

                        XMLReader xr = spf.newSAXParser().getXMLReader();
                        xr.setEntityResolver(emptyEntityResolver);

                        return xr;
                    }
                    catch (SAXException e)
                    {
                        throw new JDOMException("Failed to create XML parser", e);
                    }
                    catch (ParserConfigurationException e)
                    {
                        throw new JDOMException("Failed to create XML parser", e);
                    }
                }
            };
            return builder.build(new StringReader(xmlString));
        }
        catch (IOException e)
        {
            throw new SAMLException("Error creating JDOM document from XML string: " + e.getMessage(), e);
        }
        catch (JDOMException e)
        {
            throw new SAMLException("Error creating JDOM document from XML string: " + e.getMessage(), e);
        }
    }


    /**
     * Create a randomly generated string conforming to the xsd:ID datatype.
     * containing 160 bits of non-cryptographically strong pseudo-randomness, as
     * suggested by SAML 2.0 core 1.2.3. This will also apply to version 1.1
     *
     * @return the randomly generated string
     */
    public static String createID()
    {
        byte[] bytes = new byte[20]; // 160 bits
        random.nextBytes(bytes);

        char[] chars = new char[40];

        for (int i = 0; i < bytes.length; i++)
        {
            int left = (bytes[i] >> 4) & 0x0f;
            int right = bytes[i] & 0x0f;
            chars[i * 2] = charMapping[left];
            chars[i * 2 + 1] = charMapping[right];
        }

        return String.valueOf(chars);
    }

    /**
     * Gets the current date and time in the format specified by xsd:dateTime in
     * UTC form, as described in SAML 2.0 core 1.2.2 This will also apply to
     * Version 1.1
     *
     * @param date date of SAML timestamp.
     * @return the date and time as a String
     */
    public static String getDateAndTime(Date date)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(date);
    }

    /**
     * Returns HTML encoded version of the specified String s.
     *
     * @param s String to be HTML encoded
     * @return HTML encoded String
     */
    public static String htmlEncode(String s)
    {
        StringBuffer encodedString = new StringBuffer("");
        char[] chars = s.toCharArray();
        for (char c : chars)
        {
            if (c == '<')
            {
                encodedString.append("&lt;");
            }
            else if (c == '>')
            {
                encodedString.append("&gt;");
            }
            else if (c == '\'')
            {
                encodedString.append("&apos;");
            }
            else if (c == '"')
            {
                encodedString.append("&quot;");
            }
            else if (c == '&')
            {
                encodedString.append("&amp;");
            }
            else
            {
                encodedString.append(c);
            }
        }

        return encodedString.toString();
    }
}
