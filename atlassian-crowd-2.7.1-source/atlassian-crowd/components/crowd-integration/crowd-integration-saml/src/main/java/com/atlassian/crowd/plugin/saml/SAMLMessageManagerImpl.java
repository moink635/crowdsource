package com.atlassian.crowd.plugin.saml;

import com.atlassian.config.HomeLocator;
import com.atlassian.plugin.util.ClassLoaderUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Date;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static com.atlassian.crowd.CrowdConstants.CrowdHome.PLUGIN_DATA_LOCATION;
import static com.google.common.base.Preconditions.checkNotNull;

public class SAMLMessageManagerImpl implements SAMLMessageManager
{
    private static final Logger logger = LoggerFactory.getLogger(SAMLMessageManagerImpl.class);

    // constants
    private static final String SAML_RESPONSE_TEMPLATE_FILE = "com/atlassian/crowd/plugin/saml/template/saml-response-template.xml";
    private static final String PRIVATE_KEY = "Private.key";
    private static final String PUBLIC_KEY = "Public.key";
    private static final Provider JCE_PROVIDER = new BouncyCastleProvider();

    // injected dependencies
    private final HomeLocator homeLocator;

    // injected properties
    private String encryptionAlgorithm;
    private int keySize;
    private int assertionNotBeforeMinutes;
    private int assertionNotOnOrAfterMinutes;
    private String keyPath;

    // state
    private KeyPair keyPair;
    private String samlResponseTemplate;
    private final ReadWriteLock readWriteLock;

    public SAMLMessageManagerImpl(HomeLocator homeLocator)
    {
        this.homeLocator = checkNotNull(homeLocator);
        readWriteLock = new ReentrantReadWriteLock();
    }

    /**
     * Performs initialisation of the manager by
     * loading the template SAML XML response and
     * DSA keys from disk.
     *
     * @throws Exception
     */
    public void afterPropertiesSet() throws Exception
    {
        if (StringUtils.isBlank(encryptionAlgorithm))
        {
            throw new IllegalArgumentException("The 'encryptionAlgorithm' property cannot be blank.");
        }

        if (StringUtils.isBlank(keyPath))
        {
            throw new IllegalArgumentException("The 'keyPath' property cannot be blank.");
        }

        // load SAML XML response template
        InputStream templateStream = ClassLoaderUtils.getResourceAsStream(SAML_RESPONSE_TEMPLATE_FILE, this.getClass());
        samlResponseTemplate = IOUtils.toString(templateStream);

        // load DSA keys from crowd.home
        try
        {
            loadKeys();
            logger.info("Signature keys loaded from " + getKeyPath());
        }
        catch (SAMLException e)
        {
            // no keys exist
            logger.info("Signature keys not found in " + getKeyPath());
        }
    }

    @Override
    public SAMLAuthRequest parseAuthRequest(String samlRequestXML, String relayStateURL, String samlEncoding)
        throws SAMLException
    {
        return new SAMLAuthRequest(samlRequestXML, relayStateURL, samlEncoding);
    }

    @Override
    public SAMLAuthResponse generateAuthResponse(SAMLAuthRequest authRequest, String authenticatedUser) throws SAMLException
    {
        if (!hasValidKeys())
        {
            throw new SAMLException("SAML signature keys have not been set up.");
        }

        Date now = new Date();

        // build XML auth response by replacing the strings of a template (seems inefficient, from Google's sample code)
        String samlResponseXML = samlResponseTemplate
            .replace("<USERNAME_STRING>", authenticatedUser)
            .replace("<RESPONSE_ID>", Util.createID())
            .replace("<ISSUE_INSTANT>", Util.getDateAndTime(now))
            .replace("<AUTHN_INSTANT>", Util.getDateAndTime(now))
            .replace("<NOT_BEFORE>", Util.getDateAndTime(DateUtils.addMinutes(now, assertionNotBeforeMinutes)))
            .replace("<NOT_ON_OR_AFTER>", Util.getDateAndTime(DateUtils.addMinutes(now, assertionNotOnOrAfterMinutes)))
            .replace("<ASSERTION_ID>", Util.createID())
            .replace("<REQUEST_ID>", authRequest.getRequestID())
            .replace("<ACS_URL>", authRequest.getAcsURL());

        // sign the response
        String signedResponseXML = "";
        readWriteLock.readLock().lock();
        try
        {
            // we need to check this a second time as the keys may have been deleted during the lock release in "hasValidKeys()"
            if (this.keyPair != null)
            {
                signedResponseXML = XmlDigitalSigner.signXML(samlResponseXML, this.keyPair.getPublic(), this.keyPair.getPrivate());
            }
            else
            {
                throw new SAMLException("SAML signature keys have not been set up.");
            }
        }
        finally
        {
            readWriteLock.readLock().unlock();
        }

        // return response object
        return new SAMLAuthResponse(signedResponseXML, authRequest.getRelayStateURL(), authRequest.getAcsURL());
    }

    @Override
    public void generateKeys() throws SAMLException
    {
        readWriteLock.writeLock().lock();

        this.keyPair = null;

        try
        {
            // initialise the key generator
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance(encryptionAlgorithm, JCE_PROVIDER);
            keyGen.initialize(keySize);

            // generate the keypair
            KeyPair generatedPair = keyGen.generateKeyPair();

            // store the keypair
            FileUtils.forceMkdir(new File(getKeyPath()));
            FileUtils.writeByteArrayToFile(new File(getPrivateKeyFilePath()), generatedPair.getPrivate().getEncoded());
            FileUtils.writeByteArrayToFile(new File(getPublicKeyFilePath()), generatedPair.getPublic().getEncoded());

            // only set the thread-safe variable once ALL file i/o is complete
            this.keyPair = generatedPair;
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new SAMLException("Algorithm " + encryptionAlgorithm + " is not implemented by the security provider " + JCE_PROVIDER, e);
        }
        catch (IOException e)
        {
            throw new SAMLException("Could not write keys to " + getKeyPath(), e);
        }
        finally
        {
            readWriteLock.writeLock().unlock();
        }

        logger.info("SAML signature keys written to " + getKeyPath());
    }

    @Override
    public void deleteKeys()
    {
        readWriteLock.writeLock().lock();

        this.keyPair = null;

        try
        {
            File privKey = new File(getPrivateKeyFilePath());
            File pubKey = new File(getPublicKeyFilePath());

            if (privKey.exists())
            {
                FileUtils.forceDelete(privKey);
            }

            if (pubKey.exists())
            {
                FileUtils.forceDelete(pubKey);
            }
        }
        catch (IOException e)
        {
            logger.error("Could not delete keys from file system: " + e.getMessage(), e);

            // no need to bubble exception, just delete keypair state variable
        }
        finally
        {
            readWriteLock.writeLock().unlock();
        }
    }

    /**
     * Loads  the private and public keys in the Crowd home
     * directory under the "/saml" folder.
     * <p/>
     * This method is not thread-safe.
     *
     * @throws SAMLException if there was an error loading the
     *                       key-pair from disk.
     */
    protected void loadKeys() throws SAMLException
    {
        readWriteLock.writeLock().lock();

        this.keyPair = null;

        try
        {
            // load raw bytes
            byte[] privateKeyBytes = FileUtils.readFileToByteArray(new File(getPrivateKeyFilePath()));
            byte[] publicKeyBytes = FileUtils.readFileToByteArray(new File(getPublicKeyFilePath()));

            // deserialise keys
            PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
            X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);

            KeyFactory keyFactory = KeyFactory.getInstance(encryptionAlgorithm, JCE_PROVIDER);
            PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
            PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

            this.keyPair = new KeyPair(publicKey, privateKey);
        }
        catch (IOException e)
        {
            throw new SAMLException("Could not read keys from " + getKeyPath(), e);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new SAMLException("Algorithm " + encryptionAlgorithm + " is not implemented by the security provider " + JCE_PROVIDER, e);
        }
        catch (InvalidKeySpecException e)
        {
            throw new SAMLException("Keys read from " + getKeyPath() + " are invalid", e);
        }
        finally
        {
            readWriteLock.writeLock().unlock();
        }
    }

    /**
     * @return file path of the encoded public key.
     */
    protected String getPublicKeyFilePath()
    {
        return getKeyPath() + File.separator + encryptionAlgorithm + PUBLIC_KEY;
    }

    /**
     * @return file path of the encoded private key.
     */
    protected String getPrivateKeyFilePath()
    {
        return getKeyPath() + File.separator + encryptionAlgorithm + PRIVATE_KEY;
    }

    @Override
    public String getKeyPath()
    {
        return homeLocator.getHomePath() + File.separator + PLUGIN_DATA_LOCATION + File.separator + keyPath;
    }

    @Override
    public boolean hasValidKeys()
    {
        readWriteLock.readLock().lock();

        try
        {
            return keyPair != null;
        }
        finally
        {
            readWriteLock.readLock().unlock();
        }
    }

    public void setEncryptionAlgorithm(String encryptionAlgorithm)
    {
        this.encryptionAlgorithm = encryptionAlgorithm;
    }

    public void setKeySize(int keySize)
    {
        this.keySize = keySize;
    }

    public void setAssertionNotBeforeMinutes(int assertionNotBeforeMinutes)
    {
        this.assertionNotBeforeMinutes = assertionNotBeforeMinutes;
    }

    public void setAssertionNotOnOrAfterMinutes(int assertionNotOnOrAfterMinutes)
    {
        this.assertionNotOnOrAfterMinutes = assertionNotOnOrAfterMinutes;
    }

    public void setKeyPath(String keyPath)
    {
        this.keyPath = keyPath;
    }
}