package com.atlassian.crowd.plugin.saml.action;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.plugin.saml.SAMLAuthRequest;
import com.atlassian.crowd.plugin.saml.SAMLAuthResponse;
import com.atlassian.crowd.plugin.saml.SAMLException;
import com.atlassian.crowd.plugin.saml.SAMLMessageManager;

import com.google.common.annotations.VisibleForTesting;
import com.opensymphony.webwork.ServletActionContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Action responsible for servicing SAML
 * authentication requests. This has
 * been implemented for compatibility with
 * Google Apps SSO.
 */
public class SAMLAuthAction extends BaseAction
{
    private static final Logger logger = LoggerFactory.getLogger(SAMLAuthAction.class);

    // dependencies
    private SAMLMessageManager samlMessageManager;

    // request parameters according to the SAML bindings spec, section 3.4.4
    private String SAMLRequest;
    private String RelayState;
    private String SAMLEncoding = SAMLAuthRequest.DEFLATE_ENCODING; // default encoding id, as per the spec

    // view objects
    private SAMLAuthResponse samlResponse;

    @Override
    public String execute()
    {
        // check for required parameters
        if (!StringUtils.isBlank(SAMLRequest) && !StringUtils.isBlank(RelayState))
        {
            try
            {
                SAMLAuthRequest authReq = samlMessageManager.parseAuthRequest(SAMLRequest, RelayState, SAMLEncoding);

                User principal = getRemoteUser();
                if (principal != null)
                {
                    samlResponse = samlMessageManager.generateAuthResponse(authReq, principal.getName());
                }
                else
                {
                    addActionError("You are not signed in to Crowd.");
                    logger.warn("User accessed SAMLAuthAction without being logged in. Check Spring Security path security constraints.");
                }
            }
            catch (SAMLException e)
            {
                addActionError("SAML Error: " + e.getMessage());
                logger.error("SAML error parsing request and generating response", e);
            }
        }
        else
        {
            addActionError("Invalid SAML authentication request");
            logger.error("Invalid SAML authentication request. Either the 'SAMLRequest' or 'RelayState' parameters was undefined.");
        }

        if (hasActionErrors())
        {
            logger.error("SAML Request String: {} - URL-decoded SAML Request: {} - URL-decoded RelayState: {} - SAMLEncoding: {}",
                    getQueryString(), SAMLRequest, RelayState, SAMLEncoding);
        }

        return SUCCESS;
    }

    /**
     * This method exists just to allow tests to replace this static behaviour with a more "testeable" one.
     *
     * @return the HTTP query string
     */
    @VisibleForTesting
    protected String getQueryString()
    {
        return ServletActionContext.getRequest().getQueryString();
    }

    public SAMLAuthResponse getSamlResponse()
    {
        return samlResponse;
    }

    public void setSamlMessageManager(SAMLMessageManager samlMessageManager)
    {
        this.samlMessageManager = samlMessageManager;
    }

    public void setSAMLRequest(String SAMLRequest)
    {
        this.SAMLRequest = SAMLRequest;
    }

    public void setRelayState(String RelayState)
    {
        this.RelayState = RelayState;
    }

    public String getSAMLEncoding()
    {
        return SAMLEncoding;
    }

    public void setSAMLEncoding(String SAMLEncoding)
    {
        this.SAMLEncoding = SAMLEncoding;
    }
}
