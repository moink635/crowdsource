<html>
<head>
    <title>
        <@ww.text name='menu.viewapplication.label'/>
    </title>
    <meta name="section" content="applications"/>
    <meta name="pagename" content="view"/>
    <meta name="help.url" content="<@ww.text name='help.application.view.samlconfig'/>"/>
</head>
<body>

<h2 id="application-name">
    ${applicationName}
</h2>

<div class="page-content">

    <ol class="tabs">
        <#foreach webItem in webItemsForApplication>
            <li <#if webItem.key == "configuration">class="on"</#if> >
                <a id="${webItem.link.id}" href="${action.getLink(webItem.link)}"/>
                    ${action.getText(webItem.webLabel.key)}
                </a>
            </li>
        </#foreach>
    </ol>


    <div class="tabContent static">

        <div class="crowdForm">

            <#if action.containsErrorMessages()>
                <p class="warningBox">
                <#foreach error in actionErrors>
                    ${error}<br/>
                </#foreach>
                </p>
            </#if>

            <#if action.containsActionMessages()>
                <#switch actionMessageAlertColor>
                    <#case "green">
                        <p class="successBox">
                        <#break>
                    <#case "blue">
                        <p class="informationBox">
                        <#break>
                    <#default>
                        <p class="noteBox">
                </#switch>
                <#foreach msg in actionMessages>
                    ${msg}<br/>
                </#foreach>
                </p>
            </#if>

            <p>
                <@ww.text name="saml.description.text"/>
            </p>


            <div class="fieldArea required">
                <label class="fieldLabelArea"><@ww.text name='saml.auth.url.label'/>:</label>
                <div class="fieldValueArea">${samlAuthURL}</div>
            </div>

            <div class="fieldArea required">
                <label class="fieldLabelArea"><@ww.text name='saml.signout.url.label'/>:</label>
                <div class="fieldValueArea">${signOutURL}</div>
            </div>

            <div class="fieldArea required">
                <label class="fieldLabelArea"><@ww.text name='saml.password.url.label'/>:</label>
                <div class="fieldValueArea">${passwordURL}</div>
            </div>

            <div class="fieldArea required">
                <label class="fieldLabelArea"><@ww.text name='saml.key.label'/>:</label>
                <div class="fieldValueArea">
                    <#if keysFound>
                        ${keyPath}
                    <#else>
                        <@ww.text name='saml.key.none'/>
                    </#if>
                </div>
            </div>

            <div class="fieldArea required">
                <label class="fieldLabelArea">&nbsp;</label>
                <div class="fieldValueArea">
                    <form action="/" method="post">
                    <input id="keygenButton" type="button" value="<@ww.text name='saml.key.gen.button'/>" onClick="window.location='<@ww.url namespace="/console/secure/application" action="samlconfig" method="doGenerateKeys" includeParams="none" ><@ww.param name="ID" value="ID"/></@ww.url>';"/>
                    <#if keysFound>
                        <input id="keydelButton" type="button" value="<@ww.text name='saml.key.del.button'/>" onClick="window.location='<@ww.url namespace="/console/secure/application" action="samlconfig" method="doDeleteKeys" includeParams="none"><@ww.param name="ID" value="ID"/></@ww.url>';"/>
                    </#if>
                    </form>
                </div>
            </div>

            <div class="formFooter wizardFooter">&nbsp;</div>

        </div>
    </div>

</div>


</body>
</html>
