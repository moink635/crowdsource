<html>
    <#if actionErrors?? && (actionErrors.size()>0) >
        <body>
        <div class="warningBox">
        <span class="errorMessage">
            <#foreach error in actionErrors>
                ${error}
            </#foreach>
        </span>
        </div>
        </body>
    <#else>
        <body onload="document.forms['form-redirect'].submit();">
        <form name="form-redirect" action="${samlResponse.acsURL}" method="post">
            <div style="display:none">
                <textarea rows="10" cols="80" name="SAMLResponse">${samlResponse.signedResponseXML}</textarea>
                <textarea rows="10" cols="80" name="RelayState">${samlResponse.relayStateURL}</textarea>
                <button type="submit">Continue</button>
            </div>
        </form>
        </body>
    </#if>
</html>
