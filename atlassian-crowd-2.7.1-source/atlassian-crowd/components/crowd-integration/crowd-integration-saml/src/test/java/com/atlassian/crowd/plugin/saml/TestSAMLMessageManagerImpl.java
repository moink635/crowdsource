package com.atlassian.crowd.plugin.saml;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.xml.crypto.AlgorithmMethod;
import javax.xml.crypto.KeySelector;
import javax.xml.crypto.KeySelectorException;
import javax.xml.crypto.KeySelectorResult;
import javax.xml.crypto.MarshalException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureException;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMValidateContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import com.atlassian.config.HomeLocator;
import com.atlassian.plugin.util.ClassLoaderUtils;

import org.apache.commons.io.FileUtils;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestSAMLMessageManagerImpl
{
    private static final Provider JCE_PROVIDER = new BouncyCastleProvider();
    private static final String ENCRYPTION_ALGORITHM = "DSA";
    private static final String SIGNATURE_ALGORITHM = "SHA1withDSA";

    private static final String AUTHENTICATED_USER = "shihab";

    private static final int ASSERTION_NOT_BEFORE = -1;
    private static final int ASSERTION_NOT_ON_OR_AFTER = 2;

    @Rule
    public final TemporaryFolder crowdHomeDir = new TemporaryFolder();

    @Mock
    private HomeLocator mockHomeLocator;

    @InjectMocks
    private SAMLMessageManagerImpl messageManager;

    @Before
    public void setUp() throws Exception
    {
        messageManager.setEncryptionAlgorithm("DSA");
        messageManager.setAssertionNotBeforeMinutes(ASSERTION_NOT_BEFORE);
        messageManager.setAssertionNotOnOrAfterMinutes(ASSERTION_NOT_ON_OR_AFTER);
        messageManager.setKeyPath("/plugin-data/crowd-saml-plugin");
        messageManager.setKeySize(1024);

        when(mockHomeLocator.getHomePath()).thenReturn(crowdHomeDir.newFolder().getAbsolutePath());

        assertNoKeysOnDisk();
    }

    private void copyTestKeys() throws IOException, URISyntaxException
    {
        URL privateKeyURL = ClassLoaderUtils.getResource("DSAPrivate.key", this.getClass());
        URL publicKeyURL  = ClassLoaderUtils.getResource("DSAPublic.key", this.getClass());
        FileUtils.copyFileToDirectory(new File(privateKeyURL.toURI()), new File(messageManager.getKeyPath()));
        FileUtils.copyFileToDirectory(new File(publicKeyURL.toURI()), new File(messageManager.getKeyPath()));
    }

    private static KeyPair loadKeys(String privateKeyFile, String publicKeyFile) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException
    {
        byte[] privateKeyBytes = FileUtils.readFileToByteArray(new File(privateKeyFile));
        byte[] publicKeyBytes = FileUtils.readFileToByteArray(new File(publicKeyFile));

        // deserialise keys
        PKCS8EncodedKeySpec privateKeySpec = new PKCS8EncodedKeySpec(privateKeyBytes);
        X509EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(publicKeyBytes);

        KeyFactory keyFactory = KeyFactory.getInstance(ENCRYPTION_ALGORITHM, JCE_PROVIDER);
        PrivateKey privateKey = keyFactory.generatePrivate(privateKeySpec);
        PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

        return new KeyPair(publicKey, privateKey);
    }

    private static void validateKeys(KeyPair keys) throws NoSuchAlgorithmException, SignatureException, InvalidKeyException
    {
        String text = "This sample text will be signed";

        Signature signature = Signature.getInstance(SIGNATURE_ALGORITHM, JCE_PROVIDER);
        signature.initSign(keys.getPrivate());
        signature.update(text.getBytes());
        byte[] signedText = signature.sign();

        signature.initVerify(keys.getPublic());
        signature.update(text.getBytes());
        if (!signature.verify(signedText))
        {
            throw new SignatureException("Signature does not match document");
        }
    }

    @Test
    public void testInitialLoadKeys_noKeys() throws Exception
    {
        messageManager.afterPropertiesSet();

        assertFalse(messageManager.hasValidKeys());
    }

    @Test
    public void testInitialLoadKeys_keysPresent() throws Exception
    {
        // have existing keys in crowd-home
        copyTestKeys();

        messageManager.afterPropertiesSet();

        assertTrue(messageManager.hasValidKeys());
    }

    @Test
    public void testGenerateKeys_new() throws Exception
    {
        messageManager.generateKeys();

        // verify keys
        KeyPair keys = loadKeys(messageManager.getPrivateKeyFilePath(), messageManager.getPublicKeyFilePath());
        validateKeys(keys);
    }

    @Test
    public void testGenerateKeys_overwrite() throws Exception
    {
        // have existing keys in crowd-home
        copyTestKeys();

        // assert keys are there
        KeyPair oldKeys = loadKeys(messageManager.getPrivateKeyFilePath(), messageManager.getPublicKeyFilePath());

        // overwrite keys
        messageManager.generateKeys();

        KeyPair newKeys = loadKeys(messageManager.getPrivateKeyFilePath(), messageManager.getPublicKeyFilePath());

        // the chances of regenerating the same keypair is immensely unlikely
        assertFalse(newKeys.equals(oldKeys));
    }

    private void assertNoKeysOnDisk()
    {
        if (new File(messageManager.getPrivateKeyFilePath()).exists() || new File(messageManager.getPublicKeyFilePath()).exists())
        {
            fail("Keys have been found on disk");
        }
    }

    @Test
    public void testDeleteKeys_noneExists()
    {
        assertNoKeysOnDisk();

        messageManager.deleteKeys();

        assertNoKeysOnDisk();
    }

    @Test
    public void testDeleteKeys_existingKeys() throws Exception
    {
        // have existing keys
        copyTestKeys();

        messageManager.deleteKeys();

        assertNoKeysOnDisk();
    }

    private static SAMLAuthRequest makeTestAuthRequest()
    {
        return new SAMLAuthRequest(
                "2008-07-21T06:22:10Z",
                "google.com",
                "https://www.google.com/a/thanksforcomingin.com/acs",
                "cliofbcolgkmklnaoeafjjcakpnkblpccdjkpcla",
                "https://www.google.com/a/thanksforcomingin.com/ServiceLogin?service=mail&passive=true&rm=false&continue=http%3A%2F%2Fmail.google.com%2Fa%2Fthanksforcomingin.com%2F&bsv=1k96igf4806cy&ltmpl=default&ltmplcache=2"
            );
    }

    @Test(expected = SAMLException.class)
    public void testGenerateAuthResponse_noKeys() throws SAMLException
    {
        // do not set up keys
        SAMLAuthRequest authReq = makeTestAuthRequest();

        messageManager.generateAuthResponse(authReq, AUTHENTICATED_USER);
    }

    private static boolean validate(Document signedDoc)
            throws ParserConfigurationException, IOException, SAXException, ClassNotFoundException,
            IllegalAccessException, InstantiationException, MarshalException, XMLSignatureException
    {
        NodeList nl = signedDoc.getElementsByTagNameNS(XMLSignature.XMLNS, "Signature");
        if (nl.getLength() == 0)
        {
            throw new IllegalArgumentException("Cannot find Signature element");
        }

        String providerName = System.getProperty("jsr105Provider", "org.jcp.xml.dsig.internal.dom.XMLDSigRI");
        XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM", (Provider) Class.forName(providerName).newInstance());
        DOMValidateContext valContext = new DOMValidateContext
            (new KeyValueKeySelector(), nl.item(0));

        XMLSignature signature = fac.unmarshalXMLSignature(valContext);
        return signature.validate(valContext);
    }

    @Test
    public void testGenerateAuthResponse_success() throws Exception
    {
        // copy keys
        copyTestKeys();

        // load keys
        messageManager.afterPropertiesSet();

        // generate and sign response
        SAMLAuthRequest authReq = makeTestAuthRequest();
        SAMLAuthResponse authResp = messageManager.generateAuthResponse(authReq, AUTHENTICATED_USER);

        // verify acs and relay state urls
        assertEquals(authResp.getAcsURL(), authReq.getAcsURL());
        assertEquals(authResp.getRelayStateURL(), Util.htmlEncode(authReq.getRelayStateURL()));

        // verify xml response signature
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        Document doc = dbf.newDocumentBuilder().parse(new ByteArrayInputStream(authResp.getSignedResponseXML().getBytes()));

        boolean signOK = validate(doc);
        if (!signOK)
        {
            fail("Signature verification failed");
        }

        // verify XML response structure
        assertEquals(6, doc.getDocumentElement().getAttributes().getLength());
        assertEquals(40, doc.getDocumentElement().getAttribute("ID").length());
        String issueInstant = doc.getDocumentElement().getAttribute("IssueInstant");
        assertEquals("2.0", doc.getDocumentElement().getAttribute("Version"));

        org.w3c.dom.Node sigElt = doc.getDocumentElement().getFirstChild().getNextSibling();
        assertEquals("Signature", sigElt.getNodeName());
        assertEquals(3, sigElt.getChildNodes().getLength());

        org.w3c.dom.Node statusElt = sigElt.getNextSibling();
        assertEquals("samlp:Status", statusElt.getNodeName());
        assertEquals(3, statusElt.getChildNodes().getLength());
        assertEquals("samlp:StatusCode", statusElt.getFirstChild().getNextSibling().getNodeName());
        assertEquals("urn:oasis:names:tc:SAML:2.0:status:Success", statusElt.getFirstChild().getNextSibling().getAttributes().getNamedItem("Value").getNodeValue());

        org.w3c.dom.Node assertionElt = statusElt.getNextSibling().getNextSibling();
        assertEquals("Assertion", assertionElt.getNodeName());
        assertEquals(40, assertionElt.getAttributes().getNamedItem("ID").getNodeValue().length());
        assertEquals("2.0", assertionElt.getAttributes().getNamedItem("Version").getNodeValue());
        assertEquals(issueInstant, assertionElt.getAttributes().getNamedItem("IssueInstant").getNodeValue());
        assertEquals(9, assertionElt.getChildNodes().getLength()); // if the xml changes due to a change in libraries, this will fail

        org.w3c.dom.Node issuerElt = assertionElt.getFirstChild().getNextSibling();
        org.w3c.dom.Node subjectElt = issuerElt.getNextSibling().getNextSibling();
        org.w3c.dom.Node subjectConfirmationElt = subjectElt.getFirstChild().getNextSibling().getNextSibling().getNextSibling();
        org.w3c.dom.Node conditionsElt = subjectElt.getNextSibling().getNextSibling();
        org.w3c.dom.Node authStmtElt = conditionsElt.getNextSibling().getNextSibling();

        assertEquals("Issuer", issuerElt.getNodeName());
        assertEquals("https://www.opensaml.org/IDP", issuerElt.getFirstChild().getNodeValue().trim());

        assertEquals("Subject", subjectElt.getNodeName());
        assertEquals(AUTHENTICATED_USER, subjectElt.getFirstChild().getNextSibling().getFirstChild().getNodeValue().trim());

        assertEquals("SubjectConfirmation", subjectConfirmationElt.getNodeName());
        org.w3c.dom.NamedNodeMap attribs = subjectConfirmationElt.getFirstChild().getNextSibling().getAttributes();
        assertEquals(authReq.getRequestID(), attribs.getNamedItem("InResponseTo").getNodeValue());
        String notOnOrAfter = attribs.getNamedItem("NotOnOrAfter").getNodeValue();
        assertEquals(authReq.getAcsURL(), attribs.getNamedItem("Recipient").getNodeValue());

        assertEquals("Conditions", conditionsElt.getNodeName());
        String notBefore = conditionsElt.getAttributes().getNamedItem("NotBefore").getNodeValue();
        assertEquals(notOnOrAfter, conditionsElt.getAttributes().getNamedItem("NotOnOrAfter").getNodeValue());
        assertEquals(authReq.getAcsURL(), conditionsElt.getFirstChild().getNextSibling().getFirstChild().getNextSibling().getFirstChild().getNodeValue().trim());

        assertEquals("AuthnStatement", authStmtElt.getNodeName());
        assertEquals(issueInstant, authStmtElt.getAttributes().getNamedItem("AuthnInstant").getNodeValue());
        assertEquals("urn:oasis:names:tc:SAML:2.0:ac:classes:Password", authStmtElt.getFirstChild().getNextSibling().getFirstChild().getNextSibling().getFirstChild().getNodeValue().trim());

        // verify date stamp values
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date stamp = sdf.parse(issueInstant);
        Date now = new Date();
        assertTrue(now.getTime() - stamp.getTime() > 0);
        assertTrue(now.getTime() - stamp.getTime() < 120 * 1000); // verification did not take longer than 2 minutes

        long validSpan = sdf.parse(notOnOrAfter).getTime() - sdf.parse(notBefore).getTime();
        assertEquals((ASSERTION_NOT_ON_OR_AFTER - ASSERTION_NOT_BEFORE) * 60 * 1000, validSpan);

        long validSinceIssue = sdf.parse(notOnOrAfter).getTime() - sdf.parse(issueInstant).getTime();
        assertEquals(ASSERTION_NOT_ON_OR_AFTER * 60 * 1000, validSinceIssue);
    }


    // signature verification helper classes (from xml-security samples)
    private static class KeyValueKeySelector extends KeySelector
    {
        @Override
        public KeySelectorResult select(KeyInfo keyInfo,
                                        KeySelector.Purpose purpose,
                                        AlgorithmMethod method,
                                        XMLCryptoContext context)
                throws KeySelectorException
        {
            if (keyInfo == null)
            {
                throw new KeySelectorException("Null KeyInfo object!");
            }
            SignatureMethod sm = (SignatureMethod) method;
            List list = keyInfo.getContent();

            for (Object item : list)
            {
                XMLStructure xmlStructure = (XMLStructure) item;
                if (xmlStructure instanceof KeyValue)
                {
                    PublicKey pk = null;
                    try
                    {
                        pk = ((KeyValue) xmlStructure).getPublicKey();
                    }
                    catch (KeyException ke)
                    {
                        throw new KeySelectorException(ke);
                    }
                    // make sure algorithm is compatible with method
                    if (algEquals(sm.getAlgorithm(), pk.getAlgorithm()))
                    {
                        return new SimpleKeySelectorResult(pk);
                    }
                }
            }
            throw new KeySelectorException("No KeyValue element found!");
        }

        static boolean algEquals(String algURI, String algName)
        {
            return algName.equalsIgnoreCase("DSA") && algURI.equalsIgnoreCase(SignatureMethod.DSA_SHA1)
                    || algName.equalsIgnoreCase("RSA") && algURI.equalsIgnoreCase(SignatureMethod.RSA_SHA1);
        }
    }

    private static class SimpleKeySelectorResult implements KeySelectorResult
    {
        private final PublicKey pk;

        SimpleKeySelectorResult(PublicKey pk)
        {
            this.pk = pk;
        }

        @Override
        public Key getKey()
        {
            return pk;
        }
    }
}
