package com.atlassian.crowd.plugin.saml;

import org.jdom.Document;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UtilTest
{
    @Test
    public void createJdomDocParsesValidDocuments() throws Exception
    {
        Document doc = Util.createJdomDoc("<x>&amp;</x>");
        assertEquals("&", doc.getRootElement().getText());
    }

    @Test
    public void createJdomDocIgnoresExternalDtds() throws Exception
    {
        String EXTERNAL_DTD = "<!DOCTYPE root SYSTEM '/no-such-file'> <root/>";
        Document doc = Util.createJdomDoc(EXTERNAL_DTD);
        assertEquals("", doc.getRootElement().getText());
    }
}
