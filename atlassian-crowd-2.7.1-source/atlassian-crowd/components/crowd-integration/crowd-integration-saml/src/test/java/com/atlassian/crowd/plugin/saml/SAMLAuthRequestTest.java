package com.atlassian.crowd.plugin.saml;

import java.net.URLEncoder;

import org.apache.commons.codec.binary.Base64;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * If you need to add tests here, you may find this online
 * <a href="https://rnd.feide.no/simplesaml/module.php/saml2debug/debug.php">encoder/decoder</a> useful.
 */
public class SAMLAuthRequestTest
{
    private static final String VALID_SAML_REQUEST_IN_BASE64 =
        "fVLLTsMwELwj8Q+W73keoLKaoFKEqFQgagMHbq6zad04dvA6Lfw9bkoFHOhxx+Odmd0d33y0iuzAojQ6o0kYUwJamErqdUZfyvtgRG/yy4sx8lZ1bNK7jV7Aew/oiP+pkQ0PGe2tZoajRKZ5C8icYMvJ45ylYcw6a5wRRlEyu8uoUNLUK1+um7ZRmhvg9XYreNPpZqU6Iapt0wnFKXk92UoPtmaIPcw0Oq6dh+J4FMTXQZqU8RVLU5bEb5QU30q3Uh8TnLO1OpKQPZRlERTPy3JosJMV2CfPzujamLWCUJj2IF9wRLnzcM0VAiUTRLDOG5wajX0Ldgl2JwW8LOYZ3TjXIYui/X4f/rSJeOQ2XDdYG+tLry71ERdI82HIbMhpf033fAp+ckHzH51x9KtV/r28Q6bZXWGUFJ9kopTZTy1w5wM52/s898a23P2vloTJgMgqqAcq6zV2IGQtoaIkyo+qf6/E384X";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void parseValidSAMLRequest() throws Exception
    {
        SAMLAuthRequest samlAuthRequest = new SAMLAuthRequest(VALID_SAML_REQUEST_IN_BASE64, "relay",
                                                              SAMLAuthRequest.DEFLATE_ENCODING);

        assertEquals("cliofbcolgkmklnaoeafjjcakpnkblpccdjkpcla", samlAuthRequest.getRequestID());
        assertEquals("https://www.google.com/a/thanksforcomingin.com/acs", samlAuthRequest.getAcsURL());
        assertEquals("2008-07-21T06:22:10Z", samlAuthRequest.getIssueInstant());
        assertEquals("google.com", samlAuthRequest.getProviderName());
        assertEquals("relay", samlAuthRequest.getRelayStateURL());
    }

    @Test(expected = SAMLException.class)
    public void parseIncompleteSAMLRequest() throws Exception
    {
        new SAMLAuthRequest("fVLLTsMwELwj8Q", "relay", SAMLAuthRequest.DEFLATE_ENCODING);
    }

    @Test(expected = SAMLException.class)
    public void testParseAuthRequestFailsWithNullsAndBlanks() throws SAMLException
    {
        new SAMLAuthRequest(null, "", SAMLAuthRequest.DEFLATE_ENCODING);
    }

    @Test(expected = SAMLException.class)
    public void testParseAuthRequestFailsWithBlanksAndNulls() throws SAMLException
    {
        new SAMLAuthRequest("", null, SAMLAuthRequest.DEFLATE_ENCODING);
    }

    @Test (expected = NullPointerException.class)
    public void requiresAnEncodingIdentifier() throws Exception
    {
        new SAMLAuthRequest(URLEncoder.encode(VALID_SAML_REQUEST_IN_BASE64, "UTF-8"), "relay", null);
    }

    @Test
    public void shouldFailIfEncodingIdentifierIsUnknown() throws Exception
    {
        expectedException.expect(SAMLException.class);
        expectedException.expectMessage(containsString("Unsupported SAML encoding"));

        new SAMLAuthRequest(URLEncoder.encode(VALID_SAML_REQUEST_IN_BASE64, "UTF-8"), "relay",
                            "unsupported encoding");
    }

    @Test
    public void shouldFailToParseSAMLRequestThatIsUrlEncoded() throws Exception
    {
        expectedException.expect(SAMLException.class);
        expectedException.expectMessage(containsString("SAMLRequest is not Base64 encoded"));

        new SAMLAuthRequest(URLEncoder.encode(VALID_SAML_REQUEST_IN_BASE64, "UTF-8"), "relay",
                            SAMLAuthRequest.DEFLATE_ENCODING);
    }

    @Test
    public void shouldFailToParseSAMLRequestThatHasANonBase64Suffix() throws Exception
    {
        expectedException.expect(SAMLException.class);
        expectedException.expectMessage(containsString("SAMLRequest is not Base64 encoded"));

        new SAMLAuthRequest(VALID_SAML_REQUEST_IN_BASE64 + "%suffix", "relay",
                            SAMLAuthRequest.DEFLATE_ENCODING);
    }

    @Test
    public void shouldFailToParseSAMLRequestThatCannotBeDecompressed() throws Exception
    {
        expectedException.expect(SAMLException.class);
        expectedException.expectMessage(containsString("Error decoding SAML Authentication Request"));

        new SAMLAuthRequest(new Base64().encodeAsString("corrupt-data".getBytes("UTF-8")), "relay",
                            SAMLAuthRequest.DEFLATE_ENCODING);
    }

    @Test
    public void shouldFailToParseSAMLRequestThatCannotBeParsedAsXml() throws Exception
    {
        expectedException.expect(SAMLException.class);
        expectedException.expectMessage(containsString("Error parsing AuthnRequest XML"));

        // this request expands to: <?xml version="1.0" encoding="UTF-8"?><fail>
        new SAMLAuthRequest("s7GvyM1RKEstKs7Mz7NVMtQzUFJIzUvOT8nMS7dVCg1x07VQsrfj5bJJS8zMsQMA", "relay",
                            SAMLAuthRequest.DEFLATE_ENCODING);
    }

    @Test
    public void shouldParseSAMLRequestThatDoesNotContainAttributes() throws Exception
    {
        // this request expands to: <?xml version="1.0" encoding="UTF-8"?><empty/>
        SAMLAuthRequest samlAuthRequest =
            new SAMLAuthRequest("s7GvyM1RKEstKs7Mz7NVMtQzUFJIzUvOT8nMS7dVCg1x07VQsrfj5bJJzS0oqdS3AwA=", "relay",
                                SAMLAuthRequest.DEFLATE_ENCODING);

        assertNull(samlAuthRequest.getAcsURL());
        assertNull(samlAuthRequest.getIssueInstant());
        assertNull(samlAuthRequest.getProviderName());
        assertNull(samlAuthRequest.getRequestID());
    }
}
