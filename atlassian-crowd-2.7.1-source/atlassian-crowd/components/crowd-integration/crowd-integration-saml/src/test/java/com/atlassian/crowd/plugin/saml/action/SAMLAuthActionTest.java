package com.atlassian.crowd.plugin.saml.action;

import com.atlassian.crowd.console.action.ActionHelper;
import com.atlassian.crowd.plugin.saml.SAMLMessageManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class SAMLAuthActionTest
{
    private SAMLAuthAction samlAuthAction;

    @Mock
    private SAMLMessageManager samlMessageManager;

    @Mock
    private ActionHelper actionHelper;

    @Before
    public void createObjectUnderTest()
    {
        samlAuthAction = new SAMLAuthAction() {
            // this method uses statics, so we override it
            @Override
            protected String getQueryString()
            {
                return "";
            }
        };
        samlAuthAction.setSamlMessageManager(samlMessageManager);
        samlAuthAction.setActionHelper(actionHelper);
    }

    @Test
    public void defaultEncodingIsUsedIfEncodingIsNotSpecified() throws Exception
    {
        samlAuthAction.setSAMLRequest("samlRequest");
        samlAuthAction.setRelayState("relayState");

        samlAuthAction.execute();

        verify(samlMessageManager).parseAuthRequest(eq("samlRequest"), eq("relayState"),
                                                    eq("urn:oasis:names:tc:SAML:2.0:bindings:URL-Encoding:DEFLATE"));
    }

    @Test
    public void specificEncodingIsUsedIfSpecified() throws Exception
    {
        samlAuthAction.setSAMLRequest("samlRequest");
        samlAuthAction.setRelayState("relayState");
        samlAuthAction.setSAMLEncoding("differentEncoding");

        samlAuthAction.execute();

        verify(samlMessageManager).parseAuthRequest(eq("samlRequest"), eq("relayState"), eq("differentEncoding"));
    }
}
