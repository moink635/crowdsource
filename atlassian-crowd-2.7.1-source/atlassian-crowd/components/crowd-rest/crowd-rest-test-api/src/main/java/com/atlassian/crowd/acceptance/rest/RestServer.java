package com.atlassian.crowd.acceptance.rest;

import java.net.URL;

import com.sun.jersey.api.client.Client;

/**
 * This interface represents a server which offers a Crowd REST service.
 * <p/>
 * Tests retrieve RestServer instance by calling com.atlassian.crowd.acceptance.tests.rest.RestServerImpl.INSTANCE
 */
public interface RestServer
{
    /**
     * This method is called before each test is run, and it should reset the server in a predictable state.
     *
     * @throws Exception if the server could not be restored
     */
    void before() throws Exception;

    /**
     * This method is called after each test is run, and it should free resources.
     */
    public void after();

    /**
     * @return the base URL of the application hosting the REST plugin.
     */
    URL getBaseUrl();

    /**
     * Perform custom processing on the Jersey client, such as adding any necessary headers for multitenant tests.
     *
     * @param client The client to decorate
     * @return The decorated client
     */
    Client decorateClient(Client client);
}
