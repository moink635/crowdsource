package com.atlassian.crowd.plugin.rest.service.resource;

import com.atlassian.crowd.plugin.rest.service.controller.ApplicationController;

import com.google.common.collect.ImmutableMap;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AliasResourceTest
{
    @Test
    public void allAliasesFormattedAsJson() throws Exception
    {
        ApplicationController applicationController = mock(ApplicationController.class);
        when(applicationController.getAliasesForUser("user")).thenReturn(
                ImmutableMap.of(1L, "alias", 2L, "alias2"));

        AliasResource alias = new AliasResource(applicationController);

        String json = alias.getAllAliasesForUserAsJson("user");

        JsonNode obj = new ObjectMapper().readTree(json);

        ObjectNode expected = new ObjectMapper().createObjectNode();
        expected.put("1", "alias");
        expected.put("2", "alias2");

        assertEquals(expected, obj);
    }
}
