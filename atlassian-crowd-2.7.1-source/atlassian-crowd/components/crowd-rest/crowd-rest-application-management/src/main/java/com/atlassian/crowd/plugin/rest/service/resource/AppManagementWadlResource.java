package com.atlassian.crowd.plugin.rest.service.resource;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.sun.jersey.server.impl.wadl.WadlResource;
import com.sun.jersey.server.wadl.WadlApplicationContext;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;


/**
 * Duplicated from com.sun.jersey.server.impl.wadl.WadlResource as atlassian-rest
 * filter blocks access to application.wadl resulting in a 401 unauthorized exception.
 * @author Paul.Sandoz@Sun.Com
 */
@Produces({"application/vnd.sun.wadl+xml", "application/xml"})
@Path("application.wadl")
@AnonymousAllowed
public class AppManagementWadlResource
{
    @GET
    public Response getWadl(@Context WadlApplicationContext wadlContext, @Context UriInfo uriInfo)
    {
        WadlResource wadlResource = new WadlResource(wadlContext);
        return wadlResource.getWadl(uriInfo);
    }
}
