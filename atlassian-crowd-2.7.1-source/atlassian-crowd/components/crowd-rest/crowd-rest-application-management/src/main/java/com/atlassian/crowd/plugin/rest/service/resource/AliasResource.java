package com.atlassian.crowd.plugin.rest.service.resource;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.AliasAlreadyInUseException;
import com.atlassian.crowd.plugin.rest.service.controller.ApplicationController;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;

@Path ("aliases")
@Consumes (MediaType.TEXT_PLAIN)
@Produces ("text/plain; charset=utf-8")
@AnonymousAllowed
public class AliasResource
{
    private final ApplicationController applicationController;

    public AliasResource(final ApplicationController applicationController)
    {
        this.applicationController = applicationController;
    }

    @GET
    @Path("{applicationId}/alias")
    public String getAlias(@PathParam ("applicationId") long applicationId, @QueryParam("user") String username)
        throws ApplicationNotFoundException
    {
        return applicationController.getAlias(applicationId, username);
    }

    @PUT
    @Path("{applicationId}/alias")
    public Response setAlias(@PathParam ("applicationId") long applicationId, @QueryParam("user") String username, String alias)
            throws ApplicationNotFoundException, AliasAlreadyInUseException
    {
        applicationController.setAlias(applicationId, username, alias);

        return Response.noContent().build();
    }

    @DELETE
    @Path("{applicationId}/alias")
    public Response deleteAlias(@PathParam ("applicationId") long applicationId, @QueryParam("user") String username)
            throws ApplicationNotFoundException, AliasAlreadyInUseException
    {
        applicationController.deleteAlias(applicationId, username);

        return Response.noContent().build();
    }

    @GET
    @Path("{applicationId}/username")
    public String getUsernameForAlias(@PathParam ("applicationId") long applicationId, @QueryParam("alias") String alias)
            throws ApplicationNotFoundException
    {
        return applicationController.getUsernameForAlias(applicationId, alias);
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON, "text/plain; charset=utf-8"})
    public String getAllAliasesForUserAsJson(@QueryParam("user") String username)
    {
        try
        {
            StringWriter sw = new StringWriter();

            JsonGenerator generator = new JsonFactory().createJsonGenerator(sw);

            generator.writeStartObject();

            for (Map.Entry<Long, String> e : applicationController.getAliasesForUser(username).entrySet())
            {
                generator.writeStringField(e.getKey().toString(), e.getValue());
            }

            generator.writeEndObject();
            generator.close();

            return sw.toString();
        }
        catch (IOException e)
        {
            throw new WebApplicationException(e);
        }
    }
}
