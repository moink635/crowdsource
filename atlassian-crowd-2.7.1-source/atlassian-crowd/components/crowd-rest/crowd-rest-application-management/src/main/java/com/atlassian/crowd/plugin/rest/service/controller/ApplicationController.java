package com.atlassian.crowd.plugin.rest.service.controller;


import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.manager.application.AliasAlreadyInUseException;
import com.atlassian.crowd.manager.application.AliasManager;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationManagerException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.proxy.TrustedProxyManager;
import com.atlassian.crowd.manager.validation.XForwardedForUtil;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.ImmutableApplication;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.plugin.rest.entity.ApplicationEntity;
import com.atlassian.crowd.plugin.rest.entity.ApplicationEntityList;
import com.atlassian.crowd.plugin.rest.entity.RemoteAddressEntity;
import com.atlassian.crowd.plugin.rest.entity.RemoteAddressEntitySet;
import com.atlassian.crowd.plugin.rest.util.ApplicationEntityTranslator;
import com.atlassian.crowd.plugin.rest.util.ApplicationLinkUriHelper;
import com.atlassian.plugins.rest.common.Link;

import com.google.common.collect.Sets;
import com.sun.jersey.api.NotFoundException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Controller for the Application resource.
 *
 * @since 2.2
 */
public class ApplicationController
{
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationController.class);

    private final ApplicationManager applicationManager;
    private final DirectoryManager directoryManager;
    private final TrustedProxyManager trustedProxyManager;
    private final AliasManager aliasManager;

    public ApplicationController(final ApplicationManager applicationManager, final DirectoryManager directoryManager,
            final TrustedProxyManager trustedProxyManager, AliasManager aliasManager)
    {
        this.applicationManager = applicationManager;
        this.directoryManager = directoryManager;
        this.trustedProxyManager = trustedProxyManager;
        this.aliasManager = aliasManager;
    }

    /**
     * Finds an application by name.
     *
     * @param name name of the application
     * @param baseUri base URI of the REST service
     * @return ApplicationEntity
     * @throws ApplicationNotFoundException if the application could not be found
     */
    public ApplicationEntity getApplicationByName(final String name, final URI baseUri) throws ApplicationNotFoundException
    {
        final Application application = applicationManager.findByName(name);
        final Link link = ApplicationLinkUriHelper.buildApplicationLink(baseUri, application.getId());
        return ApplicationEntityTranslator.toApplicationEntity(application, link);
    }

    /**
     * Finds an application by ID.
     *
     * @param id ID of the application
     * @param baseUri baseURI of the applications resource
     * @return ApplicationEntity
     * @throws ApplicationNotFoundException if the application could not be found
     */
    public ApplicationEntity getApplicationById(final long id, final URI baseUri) throws ApplicationNotFoundException
    {
        final Application application = applicationManager.findById(id);
        final Link link = ApplicationLinkUriHelper.buildApplicationLink(baseUri, application.getId());
        return ApplicationEntityTranslator.toApplicationEntity(application, link);
    }

    /**
     * Finds all applications.
     *
     * @param baseUri base URI of the REST service
     * @return ApplicationEntity
     * @throws ApplicationNotFoundException if the application could not be found
     */
    public ApplicationEntityList getAllApplications(final URI baseUri) throws ApplicationNotFoundException
    {
        final List<Application> applications = applicationManager.findAll();
        return ApplicationEntityTranslator.toApplicationEntities(applications, baseUri);
    }

    /**
     * Adds a new application with the request address.
     *
     * @param applicationEntity new application to add
     * @param request HTTP request
     * @param baseUri base URI of the REST service
     * @return link to the new application
     * @throws InvalidCredentialException if the given credentials are not valid
     * @throws DirectoryNotFoundException if the directory being mapped could not be found
     */
    public Link addApplicationWithRequestAddress(final ApplicationEntity applicationEntity, final HttpServletRequest request, final URI baseUri)
            throws InvalidCredentialException, DirectoryNotFoundException
    {
        Set<String> addresses = getRequestAddresses(request);
        if (!addresses.isEmpty())
        {
            if (applicationEntity.getRemoteAddresses() == null)
            {
                applicationEntity.setRemoteAddresses(new RemoteAddressEntitySet(Sets.<RemoteAddressEntity>newHashSet(), null));
            }

            for (String address : addresses)
            {
                applicationEntity.getRemoteAddresses().addRemoteAddress(new RemoteAddressEntity(address, null));
            }
        }
        return addApplication(applicationEntity, baseUri);
    }

    /**
     * Adds a new application.
     *
     * @param applicationEntity new application to add
     * @param baseUri base URI of the REST service
     * @return link to the new application
     * @throws InvalidCredentialException if the given credentials are not valid
     * @throws DirectoryNotFoundException if the directory being mapped could not be found
     */
    public Link addApplication(final ApplicationEntity applicationEntity, final URI baseUri)
            throws InvalidCredentialException, DirectoryNotFoundException
    {
        final Application applicationWithNoDirectoryMappings = ApplicationEntityTranslator.toApplicationWithNoDirectoryMappings(applicationEntity);
        final Application addedApplication = applicationManager.add(applicationWithNoDirectoryMappings);
        if (applicationEntity.getDirectoryMappings() != null && !applicationEntity.getDirectoryMappings().isEmpty())
        {
            final List<DirectoryMapping> directoryMappings = ApplicationEntityTranslator.toDirectoryMappings(applicationEntity.getDirectoryMappings(), addedApplication, directoryManager);
            final Application newApplication = ImmutableApplication.builder(addedApplication).setDirectoryMappings(directoryMappings).build();
            try
            {
                applicationManager.update(newApplication);
            }
            catch (ApplicationManagerException e)
            {
                throw new RuntimeException(e);
            }
            catch (ApplicationNotFoundException e)
            {
                throw new RuntimeException(e);
            }
        }
        return ApplicationLinkUriHelper.buildApplicationLink(baseUri, addedApplication.getId());
    }

    /**
     * Removes an application.
     *
     * @param applicationId ID of the application
     * @throws ApplicationManagerException if the remove operation is not permitted on the given application
     */
    public void removeApplication(final long applicationId)
            throws ApplicationManagerException
    {
        try
        {
            final Application application = applicationManager.findById(applicationId);
            applicationManager.remove(application);
        }
        catch (ApplicationNotFoundException e)
        {
            // do nothing
        }
    }

    /**
     * Updates an existing application.
     *
     * @param applicationEntity application entity with the new details
     * @throws ApplicationNotFoundException if the application could not be found
     * @throws ApplicationManagerException if there was an error updating the application
     * @throws DirectoryNotFoundException if a directory referenced by a directory mapping could not be found
     */
    public void updateApplication(final ApplicationEntity applicationEntity)
            throws ApplicationNotFoundException, ApplicationManagerException, DirectoryNotFoundException
    {
        final Application applicationWithNoDirectoryMappings = ApplicationEntityTranslator.toApplicationWithNoDirectoryMappings(applicationEntity);
        final Application newApplication;
        if (applicationEntity.getDirectoryMappings() != null && !applicationEntity.getDirectoryMappings().isEmpty())
        {
            final List<DirectoryMapping> directoryMappings = ApplicationEntityTranslator.toDirectoryMappings(applicationEntity.getDirectoryMappings(), applicationWithNoDirectoryMappings, directoryManager);
            newApplication = ImmutableApplication.builder(applicationWithNoDirectoryMappings).setDirectoryMappings(directoryMappings).build();
        }
        else
        {
            newApplication = applicationWithNoDirectoryMappings;
        }

        applicationManager.update(newApplication);
    }

    /**
     * Adds a remote address to the list of allowed addresses for the application.
     *
     * @param applicationId ID of the application
     * @param remoteAddressEntity remote address entity to add
     * @throws ApplicationNotFoundException if the application could not be found
     */
    public void addRemoteAddress(final long applicationId, final RemoteAddressEntity remoteAddressEntity)
            throws ApplicationNotFoundException
    {
        final Application application = applicationManager.findById(applicationId);
        applicationManager.addRemoteAddress(application, ApplicationEntityTranslator.toRemoteAddress(remoteAddressEntity));
    }

    /**
     * Removes a remote address from the list of allowed addresses for the application.
     *
     * @param applicationId ID of the application
     * @param remoteAddress remote address to remove
     * @throws ApplicationNotFoundException if the application could not be found
     */
    public void removeRemoteAddress(final long applicationId, final String remoteAddress)
            throws ApplicationNotFoundException
    {
        final Application application = applicationManager.findById(applicationId);
        applicationManager.removeRemoteAddress(application, new RemoteAddress(remoteAddress));
    }

    /**
     * Retrieves the list of request addresses.
     *
     * @param request HTTP request
     * @return list of request addresses
     */
    public Set<String> getRequestAddresses(final HttpServletRequest request)
    {
        Set<String> addresses = Sets.newHashSet();
        addresses.add(request.getRemoteAddr());
        InetAddress clientAddress = XForwardedForUtil.getTrustedAddress(trustedProxyManager, request);
        addresses.add(clientAddress.getHostAddress());

        try
        {
            for (InetAddress address : InetAddress.getAllByName(clientAddress.getHostName()))
            {
                addresses.add(address.getHostAddress());
            }
        }
        catch (UnknownHostException e)
        {
            LOGGER.warn(e.getMessage());
        }
        return addresses;
    }

    /**
     * Aliases are defined if they're not blank and are different from the username.
     *
     * @return if <code>alias</code> a defined alias for <code>username</code>
     */
    private static boolean isAliasFor(String alias, String username)
    {
        return StringUtils.isNotBlank(alias) && !alias.equals(username);
    }

    public String getAlias(long applicationId, String username)
        throws ApplicationNotFoundException, NotFoundException
    {
        Application application = applicationManager.findById(applicationId);

        String alias = aliasManager.findAliasByUsername(application, username);

        if (!isAliasFor(alias, username))
        {
            throw new NotFoundException("No alias for user " + username + " in application " + application.getName());
        }

        return alias;
    }

    public void setAlias(long applicationId, String username, String alias)
            throws ApplicationNotFoundException, AliasAlreadyInUseException
    {
        aliasManager.storeAlias(applicationManager.findById(applicationId), username, alias);
    }

    public void deleteAlias(long applicationId, String username) throws ApplicationNotFoundException, AliasAlreadyInUseException
    {
        aliasManager.removeAlias(applicationManager.findById(applicationId), username);
    }

    public String getUsernameForAlias(long applicationId, String alias) throws ApplicationNotFoundException
    {
        return aliasManager.findUsernameByAlias(applicationManager.findById(applicationId), alias);
    }

    public Map<Long, String> getAliasesForUser(String username)
    {
        Map<Long, String> aliases = new HashMap<Long, String>();

        for (Application application : applicationManager.findAll())
        {
            String alias = aliasManager.findAliasByUsername(application, username);
            if (isAliasFor(alias, username))
            {
                aliases.put(application.getId(), alias);
            }
        }

        return aliases;
    }
}
