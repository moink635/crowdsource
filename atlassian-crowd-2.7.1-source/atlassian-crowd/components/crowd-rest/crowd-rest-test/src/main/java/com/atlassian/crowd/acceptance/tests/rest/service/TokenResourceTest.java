package com.atlassian.crowd.acceptance.tests.rest.service;

import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.Response;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.plugin.rest.entity.AuthenticationContextEntity;
import com.atlassian.crowd.plugin.rest.entity.SessionEntity;
import com.atlassian.crowd.plugin.rest.entity.ValidationFactorEntity;
import com.atlassian.crowd.plugin.rest.entity.ValidationFactorEntityList;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import static org.junit.Assert.assertNotEquals;

public class TokenResourceTest extends RestCrowdServiceAcceptanceTestCase
{
    private static final String USERNAME = "admin";
    private static final String PASSWORD = "admin";
    private static final String BAD_PASSWORD = "bad_password";
    private static final ValidationFactorEntity restFactor1 = new ValidationFactorEntity(ValidationFactor.REMOTE_ADDRESS, "127.0.0.1");
    private static final ValidationFactorEntity restFactor2 = new ValidationFactorEntity(ValidationFactor.REMOTE_HOST, "blah");
    private static final ValidationFactorEntityList restFactors = new ValidationFactorEntityList(ImmutableList.of(restFactor1, restFactor2));

    /**
     * Constructs a test case with the given name.
     *
     * @param name the test name
     */
    public TokenResourceTest(String name)
    {
        super(name);
    }

    /**
     * Constructs a test case with the given name, using the given RestServer.
     *
     * @param name the test name
     * @param restServer the RestServer
     */
    public TokenResourceTest(String name, RestServer restServer)
    {
        super(name, restServer);
    }

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        // to ensure that tests are run in isolation, i.e., that the token created for one test is not
        // reused by another test, all existing sessions are killed before running each test
        deleteSessionsFor(USERNAME);
        deleteSessionsFor("eeeep");

        // Reset any alias changes
        setAliasForUsername("Alias2");
    }

    private void deleteSessionsFor(String username)
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD)
                .path(SESSION_RESOURCE)
                .queryParam("username", username);

        webResource.delete();
    }

    /**
     * Tests that a user can successfully authenticate with an application.
     */
    public void testUserAuthentication()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);

        AuthenticationContextEntity authContext = new AuthenticationContextEntity(USERNAME, PASSWORD, restFactors);

        ClientResponse response = webResource.entity(authContext, MT).post(ClientResponse.class);

        assertEquals(201, response.getStatus());
        SessionEntity session = response.getEntity(SessionEntity.class);
        assertNotNull("Session should contain a link", session.getLink());
        assertNotNull("Session should contain a token key", session.getToken());
        assertNotNull("Session creation date should be returned", session.getCreatedDate());
        assertNotNull("Session expiry date should be returned", session.getExpiryDate());
        assertUserEntityIsMinimal(session);
    }

    /**
     * Tests that a user can successfully authenticate with an application without password validation when the query
     * parameter "validate-password" is false.
     */
    public void testUserAuthentication_WithoutPasswordValidation()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE)
            .queryParam("validate-password", "false");

        AuthenticationContextEntity authContext = new AuthenticationContextEntity(USERNAME, null, restFactors);

        ClientResponse response = webResource.entity(authContext, MT).post(ClientResponse.class);

        assertEquals(201, response.getStatus());
        SessionEntity session = response.getEntity(SessionEntity.class);
        assertNotNull("Session should contain a link", session.getLink());
        assertNotNull("Session should contain a token key", session.getToken());
        assertNotNull("Session creation date should be returned", session.getCreatedDate());
        assertNotNull("Session expiry date should be returned", session.getExpiryDate());
        assertUserEntityIsMinimal(session);
    }

    /**
     * Tests that if a bad user credential is given, an error response is returned.
     */
    public void testInvalidUserAuthentication()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);

        AuthenticationContextEntity authContext = new AuthenticationContextEntity(USERNAME, BAD_PASSWORD, restFactors);

        try
        {
            webResource.entity(authContext, MT).post(SessionEntity.class);
            fail("Should have failed authentication and thrown a UniformInterfaceException.");
        }
        catch (UniformInterfaceException e)
        {
            // expected behaviour

            if (e.getResponse().getStatus() != Response.Status.BAD_REQUEST.getStatusCode())
            {
                fail("Should have returned a " + Response.Status.BAD_REQUEST.getStatusCode() + " status code.");
            }
        }
    }

    /**
     * Tests that getUserFromToken returns the correct user.
     */
    public void testGetUserFromToken()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);

        AuthenticationContextEntity authContext = new AuthenticationContextEntity(USERNAME, PASSWORD, restFactors);

        ClientResponse response = webResource.entity(authContext, MT).post(ClientResponse.class);

        assertEquals(201, response.getStatus());

        // attempt to get full user entity
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, response.getLocation());
        SessionEntity session = webResource.get(SessionEntity.class);
        assertNotNull("Session creation date should be returned", session.getCreatedDate());
        assertNotNull("Session expiry date should be returned", session.getExpiryDate());
        assertUserEntityIsExpanded(session);
    }

    /**
     * Tests that when the validation factors are correct, and the token is correct, no errors are thrown.
     */
    public void testValidateToken()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);

        AuthenticationContextEntity authContext = new AuthenticationContextEntity(USERNAME, PASSWORD, restFactors);

        ClientResponse response = webResource.entity(authContext, MT).post(ClientResponse.class);

        assertEquals(201, response.getStatus());

        // validate token
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, response.getLocation());
        ClientResponse validationResponse = webResource.entity(restFactors, MT).post(ClientResponse.class);
        assertEquals(200, validationResponse.getStatus());

        SessionEntity session = validationResponse.getEntity(SessionEntity.class);
        assertNotNull("Session creation date should be returned", session.getCreatedDate());
        assertNotNull("Session expiry date should be returned", session.getExpiryDate());
        assertUserEntityIsMinimal(session);
    }

    public void testShortLivedTokenExpiresImmediately()
    {
        WebResource authenticationWebResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD)
            .path(SESSION_RESOURCE).queryParam("duration", "0");

        AuthenticationContextEntity authContext = new AuthenticationContextEntity(USERNAME, PASSWORD, restFactors);

        ClientResponse response = authenticationWebResource.entity(authContext, MT).post(ClientResponse.class);

        assertEquals(201, response.getStatus());

        // validate token
        WebResource validationWebResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, response.getLocation());
        ClientResponse validationResponse = validationWebResource.entity(restFactors, MT).post(ClientResponse.class);
        assertEquals("Token should have expired", 404, validationResponse.getStatus());
    }

    /**
     * Tests the scenario in which different tokens are issued for the same
     * user with different privileges.
     */
    public void testMultipleTokensWithDifferentPrivilegesDoNotInterfereWithEachOther()
    {
        final ValidationFactorEntity privilegeFactor = new ValidationFactorEntity(ValidationFactor.PRIVILEGE_LEVEL, "WebSudo");
        final ValidationFactorEntityList validationFactors1 = new ValidationFactorEntityList(ImmutableList.of(restFactor1, restFactor2));
        final ValidationFactorEntityList validationFactors2 = new ValidationFactorEntityList(ImmutableList.of(restFactor1, restFactor2, privilegeFactor));

        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);

        // request first token
        AuthenticationContextEntity authContext1 = new AuthenticationContextEntity(USERNAME, PASSWORD, validationFactors1);
        ClientResponse createSessionResponse1 = webResource.entity(authContext1, MT).post(ClientResponse.class);
        assertEquals(201, createSessionResponse1.getStatus());

        // request a second token, presumably to get elevated privileges
        AuthenticationContextEntity authContext2 = new AuthenticationContextEntity(USERNAME, PASSWORD, validationFactors2);
        ClientResponse createSessionResponse2 = webResource.entity(authContext2, MT).post(ClientResponse.class);
        assertEquals(201, createSessionResponse2.getStatus());

        assertNotEquals(createSessionResponse1.getLocation(), createSessionResponse2.getLocation());

        // token1 is valid
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, createSessionResponse1.getLocation());
        webResource.entity(validationFactors1, MT).post();

        // token2 is also valid
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, createSessionResponse2.getLocation());
        webResource.entity(validationFactors2, MT).post();

        // token 1 is invalid with the validation factors of token 2
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, createSessionResponse1.getLocation());
        ClientResponse validationResponse1 = webResource.entity(validationFactors2, MT).post(ClientResponse.class);
        assertEquals(400, validationResponse1.getStatus());

        // token 2 is invalid with the validation factors of token 1
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, createSessionResponse2.getLocation());
        ClientResponse validationResponse2 = webResource.entity(validationFactors1, MT).post(ClientResponse.class);
        assertEquals(400, validationResponse2.getStatus());

        // invalidate token 2 (end of the privileged session)
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, createSessionResponse2.getLocation());
        ClientResponse deleteResponse1 = webResource.delete(ClientResponse.class);
        assertEquals(204, deleteResponse1.getStatus());

        // validate that the token 2 no longer exists
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, createSessionResponse2.getLocation());
        validationResponse2 = webResource.entity(validationFactors2, MT).post(ClientResponse.class);
        assertEquals(404, validationResponse2.getStatus());

        // but token 1 is still valid
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, createSessionResponse1.getLocation());
        webResource.entity(validationFactors1, MT).post();
    }

    public void testCreatedTokenUsesDirectoryCaseForUsername()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);

        String differentCaseUsername = USERNAME.toUpperCase();
        assertNotEquals(USERNAME, differentCaseUsername);

        /* Authenticate with a different case from what's in the directory */
        AuthenticationContextEntity authContext = new AuthenticationContextEntity(differentCaseUsername, PASSWORD, restFactors);

        ClientResponse response = webResource.entity(authContext, MT).post(ClientResponse.class);

        assertEquals(201, response.getStatus());

        // validate token
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, response.getLocation());
        SessionEntity session = webResource.entity(restFactors, MT).post(SessionEntity.class);
        assertEquals("The validated token username is in the directory case", USERNAME, session.getUser().getName());
    }

    /**
     * Tests that when the token is invalid, a 404 (Not Found) status is returned.
     */
    public void testValidateToken_InvalidToken()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE).path("invalidtoken");
        ClientResponse response = webResource.entity(restFactors, MT).post(ClientResponse.class);
        assertEquals(404, response.getStatus());
    }

    /**
     * Tests that when the validation factors are not correct, a 400 (Bad request) response is returned.
     */
    public void testValidateToken_InvalidValidationFactors()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);

        ValidationFactorEntity wrongFac = new ValidationFactorEntity(ValidationFactor.REMOTE_ADDRESS, "wrongFactor");
        ValidationFactorEntityList wrongFactors = new ValidationFactorEntityList(Arrays.asList(wrongFac));

        AuthenticationContextEntity authContext = new AuthenticationContextEntity(USERNAME, PASSWORD, restFactors);

        ClientResponse response = webResource.entity(authContext, MT).post(ClientResponse.class);

        assertEquals(201, response.getStatus());

        // validate token
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, response.getLocation());
        response = webResource.entity(wrongFactors, MT).post(ClientResponse.class);
        assertEquals(400, response.getStatus());
    }

    /**
     * Tests that the token really is invalidated.
     */
    public void testInvalidateToken()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);

        AuthenticationContextEntity authContext = new AuthenticationContextEntity(USERNAME, PASSWORD, restFactors);

        ClientResponse response = webResource.entity(authContext, MT).post(ClientResponse.class);
        final URI uri = response.getLocation();

        assertEquals(201, response.getStatus());

        // invalidate token
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);
        response = webResource.delete(ClientResponse.class);

        assertEquals(204, response.getStatus());

        // validate that the token no longer exists
        webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);
        response = webResource.entity(restFactors, MT).post(ClientResponse.class);

        assertEquals(404, response.getStatus());
    }

    /**
     * Tests that a token from a login against one application cannot be used with another application
     * where that user has no permission.
     */
    public void testGetUserFromTokenEnforcesApplicationAuthenticationChecks()
    {
        WebResource appAllowed = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);
        WebResource appNotAllowed = getRootWebResource("no-user-application", "password").path(SESSION_RESOURCE);

        AuthenticationContextEntity authContext = new AuthenticationContextEntity(USERNAME, PASSWORD, restFactors);

        ClientResponse r = appNotAllowed.queryParam("validate-password", "true").entity(authContext, MT).post(ClientResponse.class);
        assertEquals("User cannot log into appNotAllowed", Response.Status.FORBIDDEN.getStatusCode(), r.getStatus());

        /* Log into appAllowed */
        SessionEntity session = appAllowed.queryParam("validate-password", "true").entity(authContext, MT).post(SessionEntity.class);
        assertEquals(USERNAME, session.getUser().getName());

        String token = session.getToken();

        SessionEntity checkedSession = appAllowed.path(token).get(SessionEntity.class);
        assertEquals("The token is accepted for appAllowed", USERNAME, checkedSession.getUser().getName());

        ClientResponse r2 = appNotAllowed.path(token).get(ClientResponse.class);
        assertEquals("The token fails against appNotAllowed", Response.Status.FORBIDDEN.getStatusCode(), r2.getStatus());
    }

    private String createSession(String username, String password, ValidationFactorEntityList factors)
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);

        AuthenticationContextEntity authContext = new AuthenticationContextEntity(username, password, factors);

        SessionEntity response = webResource.entity(authContext, MT).post(SessionEntity.class);

        return response.getToken();
    }

    private int sessionStatus(String token, ValidationFactorEntityList factors)
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE).path(token);
        return webResource.entity(factors, MT).post(ClientResponse.class).getStatus();
    }

    private void assertActiveSession(String token, ValidationFactorEntityList factors)
    {
        assertEquals(200, sessionStatus(token, factors));
    }

    private void assertInactiveSession(String token, ValidationFactorEntityList factors)
    {
        assertEquals(404, sessionStatus(token, factors));
    }

    private Map<String, ValidationFactorEntityList> createAdminSessions(int count)
    {
        Map<String, ValidationFactorEntityList> adminSessions = new HashMap<String, ValidationFactorEntityList>();

        for (int i = 0; i < count; i++)
        {
            ValidationFactorEntity factor = new ValidationFactorEntity(ValidationFactor.REMOTE_ADDRESS, "127.0.0." + i);
            ValidationFactorEntityList restFactors = new ValidationFactorEntityList(ImmutableList.of(factor));

            String token = createSession(USERNAME, PASSWORD, restFactors);
            adminSessions.put(token, restFactors);
        }

        assertEquals(count, adminSessions.size());

        return adminSessions;
    }

    public void testAllSessionsForUserAreInvalidatedByDeleteWithUsernameSpecified()
    {
        Map<String, ValidationFactorEntityList> adminSessions = createAdminSessions(5);

        String otherAdminSession = createSession("secondadmin", "secondadmin", restFactors);

        for (Map.Entry<String, ValidationFactorEntityList> e : adminSessions.entrySet())
        {
            assertActiveSession(e.getKey(), e.getValue());
        }

        assertActiveSession(otherAdminSession, restFactors);


        WebResource sessionWebResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);

        WebResource webResource = sessionWebResource.queryParam("username", "admin");

        assertEquals(204, webResource.delete(ClientResponse.class).getStatus());


        for (Map.Entry<String, ValidationFactorEntityList> e : adminSessions.entrySet())
        {
            assertInactiveSession(e.getKey(), e.getValue());
        }

        assertActiveSession(otherAdminSession, restFactors);
    }

    public void testSpecifiedUserSessionIsExcludedFromDeletionByUsername()
    {
        Map<String, ValidationFactorEntityList> adminSessions = createAdminSessions(5);

        for (Map.Entry<String, ValidationFactorEntityList> e : adminSessions.entrySet())
        {
            assertActiveSession(e.getKey(), e.getValue());
        }

        String tokenToExclude = Iterables.getLast(adminSessions.keySet());

        WebResource sessionWebResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);

        WebResource webResource = sessionWebResource.queryParam("username", "admin").queryParam("exclude", tokenToExclude);

        assertEquals(204, webResource.delete(ClientResponse.class).getStatus());

        Map<String, Integer> expectedStatuses = new HashMap<String, Integer>();

        for (Map.Entry<String, ValidationFactorEntityList> e : adminSessions.entrySet())
        {
            expectedStatuses.put(e.getKey(), 404);
        }

        expectedStatuses.put(tokenToExclude, 200);


        Map<String, Integer> statuses = new HashMap<String, Integer>();

        for (Map.Entry<String, ValidationFactorEntityList> e : adminSessions.entrySet())
        {
            statuses.put(e.getKey(), sessionStatus(e.getKey(), e.getValue()));
        }

        assertEquals(expectedStatuses, statuses);
    }

    public void testStatusNotFoundWhenDeletingTokensForNonexistentUser()
    {
        WebResource sessionWebResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);

        WebResource webResource = sessionWebResource.queryParam("username", "no-such-user");

        assertEquals(404, webResource.delete(ClientResponse.class).getStatus());
    }

    private void assertUserEntityIsMinimal(SessionEntity session)
    {
        // the returned user entity should be minimal (just the user name)
        assertEquals(USERNAME, session.getUser().getName());
        assertNull("User entity should not contain a first name", session.getUser().getFirstName());
        assertNull("User entity should not contain a last name", session.getUser().getLastName());
        assertNull("User entity should not contain a display name", session.getUser().getDisplayName());
        assertNull("User entity should not contain an email ", session.getUser().getEmail());
        assertNull("User entity should not contain a password", session.getUser().getPassword());
    }

    private void assertUserEntityIsExpanded(SessionEntity session)
    {
        assertEquals(USERNAME, session.getUser().getName());
        assertEquals("bob the builder", session.getUser().getDisplayName());
        assertEquals("bob", session.getUser().getFirstName());
        assertEquals("the builder", session.getUser().getLastName());
        assertEquals("bob@example.net", session.getUser().getEmail());
        assertTrue("User should be active", session.getUser().isActive());
        assertNull("Password should not be visible", session.getUser().getPassword().getValue());
    }

    /**
     * For the session username alias tests, we use a pair of applications:
     *
     * <table>
     * <thead>
     * <tr><th>Application</th><th>Aliases enabled?</th><th>Username</th><th>Alias</th></tr>
     * </thead>
     * <tbody>
     * <tr><td>APPLICATION_NAME</td><td>Yes</td><td>eeeep</td><td>-</td></tr>
     * <tr><td>aliases</td><td>No</td><td>eeeep</td><td>Alias2</td></tr>
     * </tbody>
     * </table>
     */
    SessionEntity createSessionAsApplication(String appName, String appPass, String username, String password)
    {
        WebResource webResource = getRootWebResource(appName, appPass).path(SESSION_RESOURCE);
        AuthenticationContextEntity authContext = new AuthenticationContextEntity(username, password, restFactors);

        ClientResponse response = webResource.entity(authContext, MT).post(ClientResponse.class);
        assertEquals(201, response.getStatus());
        return response.getEntity(SessionEntity.class);
    }

    SessionEntity createSessionAsUnaliasedApplication(String username, String password)
    {
        return createSessionAsApplication(APPLICATION_NAME, APPLICATION_PASSWORD, username, password);
    }

    SessionEntity createSessionAsAliasedApplication(String username, String password)
    {
        return createSessionAsApplication("aliases", "aliases", username, password);
    }

    public void testSessionCreatedWithUnaliasedApplicationShowsAliasedNameWhenRetrievedWithAliasingApplication()
    {
        SessionEntity session = createSessionAsUnaliasedApplication("eeeep", "eep");

        assertEquals("eeeep", session.getUser().getName());

        WebResource wr2 = getWebResource("aliases", "aliases", session.getLink().getHref());

        SessionEntity aliasedSession = wr2.get(SessionEntity.class);

        assertEquals("alias2", aliasedSession.getUser().getName());
    }

    /**
     * The other alias tests GET the session. Ensure that aliases are also resolved for a POST validation.
     */
    public void testSessionCreatedWithUnaliasedApplicationShowsAliasedNameWhenValidatedWithAliasingApplication()
    {
        SessionEntity session = createSessionAsUnaliasedApplication("eeeep", "eep");

        assertEquals("eeeep", session.getUser().getName());

        WebResource webResource = getWebResource("aliases", "aliases", session.getLink().getHref());
        ClientResponse validationResponse = webResource.entity(restFactors, MT).post(ClientResponse.class);
        assertEquals(200, validationResponse.getStatus());

        assertEquals("alias2",
                validationResponse.getEntity(SessionEntity.class).getUser().getName());
    }

    public void testSessionCreatedThroughAliasedApplicationShowsOriginalNameWhenRetrievedWithNonAliasingApplication()
    {
        SessionEntity session = createSessionAsAliasedApplication("alias2", "eep");

        assertEquals("alias2", session.getUser().getName());

        WebResource wr2 = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, session.getLink().getHref());

        SessionEntity aliasedSession = wr2.get(SessionEntity.class);

        assertEquals("eeeep", aliasedSession.getUser().getName());
    }

    public void testSessionCreatedWithDifferentCaseReturnsCorrectCaseForUsername()
    {
        assertEquals("eeeep",
                createSessionAsUnaliasedApplication("EeeeP", "eep").getUser().getName());
    }

    public void testSessionCreatedThroughAliasWithDifferentCaseReturnsCorrectCase()
    {
        assertEquals("alias2",
                createSessionAsAliasedApplication("AliaS2", "eep").getUser().getName());
    }

    public void sessionCannotBeCreatedWithAliasThroughUnaliasedApplication()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(SESSION_RESOURCE);
        AuthenticationContextEntity authContext = new AuthenticationContextEntity("alias2", "eep", restFactors);

        ClientResponse response = webResource.entity(authContext, MT).post(ClientResponse.class);

        assertEquals(400, response.getStatus());
    }

    /**
     * Log in to an aliased application using the original username. This
     * succeeds but, arguably, shouldn't: that user is aliased so the
     * original username should be invisible.
     */
    public void testAliasedApplicationAlsoAcceptsUnaliasedUsername()
    {
        assertEquals("alias2",
                createSessionAsAliasedApplication("eeeep", "eep").getUser().getName());
    }

    protected void setAliasForUsername(String alias)
    {
        // 'aliases' application ID = 884737
        URI uri = getBaseUriBuilder("appmanagement").path("aliases").path("884737").path("alias").queryParam("user", "eeeep").build();

        WebResource aliasWebResource = getWebResource(AliasResourceTest.ADMIN_NAME, AliasResourceTest.ADMIN_PASSWORD, uri);

        aliasWebResource.put(alias);
    }

    public void testAliasChangesAreReflectedInSessionUsername()
    {
//        intendToModifyData(); // Cleaned up in setUp

        SessionEntity session = createSessionAsUnaliasedApplication("eeeep", "eep");

        assertEquals("eeeep", session.getUser().getName());

        WebResource webResource = getWebResource("aliases", "aliases", session.getLink().getHref());

        setAliasForUsername("new-alias");
        assertEquals("new-alias", webResource.get(SessionEntity.class).getUser().getName());

        setAliasForUsername("new-alias-2");
        assertEquals("new-alias-2", webResource.get(SessionEntity.class).getUser().getName());
    }

    public void testSessionForAliasedApplicationIncludesUnaliasedUsernameAsAnExtraField()
    {
        SessionEntity session = createSessionAsUnaliasedApplication("eeeep", "eep");

        assertEquals("eeeep", session.getUser().getName());
        assertNull(session.getUnaliasedUsername());

        WebResource wr2 = getWebResource("aliases", "aliases", session.getLink().getHref());

        SessionEntity aliasedSession = wr2.get(SessionEntity.class);

        assertEquals("alias2", aliasedSession.getUser().getName());
        assertEquals("eeeep", aliasedSession.getUnaliasedUsername());
    }
}
