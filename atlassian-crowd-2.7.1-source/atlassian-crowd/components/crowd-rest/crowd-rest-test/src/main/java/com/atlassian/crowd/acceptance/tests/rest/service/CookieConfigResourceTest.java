package com.atlassian.crowd.acceptance.tests.rest.service;

import java.net.URI;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.integration.rest.entity.CookieConfigEntity;

import com.sun.jersey.api.client.WebResource;

public class CookieConfigResourceTest extends RestCrowdServiceAcceptanceTestCase
{
    private static final String CONFIG_COOKIE_RESOURCE = "config/cookie";

    public CookieConfigResourceTest(String name)
    {
        super(name);
    }

    public CookieConfigResourceTest(String name, RestServer restServer)
    {
        super(name, restServer);
    }

    public void testGetConfig() throws Exception
    {
        final URI uri = getBaseUriBuilder().path(CONFIG_COOKIE_RESOURCE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final CookieConfigEntity config = webResource.get(CookieConfigEntity.class);

        assertEquals("", config.getDomain());
        assertEquals(getExpectedCookieName(), config.getName());
        assertFalse(config.isSecure());
    }

    protected String getExpectedCookieName()
    {
        return "crowd.token_key"; // specified in crowd.properties
    }
}
