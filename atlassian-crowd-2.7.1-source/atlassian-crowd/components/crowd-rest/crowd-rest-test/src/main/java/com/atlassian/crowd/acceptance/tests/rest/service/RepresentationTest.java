package com.atlassian.crowd.acceptance.tests.rest.service;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.atlassian.crowd.acceptance.rest.RestServer;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.WebResource.Builder;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.hamcrest.Matchers;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import static org.junit.Assert.assertThat;

/**
 * Test the textual representations of entities used by the REST resources.
 */
public abstract class RepresentationTest extends RestCrowdServiceAcceptanceTestCase
{
    private final MediaType mediaType;
    private final Map<String, String> expectedRepresentationForResource;

    RepresentationTest(String name, MediaType mediaType) throws IOException
    {
        super(name);
        this.mediaType = mediaType;
        expectedRepresentationForResource = loadExpectedRepresentations();
    }

    RepresentationTest(String name, MediaType mediaType, RestServer restServer) throws IOException
    {
        super(name, restServer);
        this.mediaType = mediaType;
        expectedRepresentationForResource = loadExpectedRepresentations();
    }

    /**
     * Parse a fragile file format for expected representations. identifier on one line,
     * zero or more non-blank lines of content, one blank terminal line.
     */
    Map<String, String> loadExpectedRepresentations() throws IOException
    {
        ImmutableMap.Builder<String, String> mb = ImmutableMap.builder();

        assertEquals("application", mediaType.getType());
        String type = mediaType.getSubtype();

        URL url = RepresentationTest.class.getResource("RepresentationTest-samples-" + type + ".txt");
        assertNotNull(url);

        List<String> lines = IOUtils.readLines(url.openStream());

        String id = null;
        StringBuilder value = new StringBuilder();

        for (String l : lines)
        {
            if (id == null)
            {
                assertThat(l, CoreMatchers.not(""));
                id = l;
                value.setLength(0);
            }
            else
            {
                if (l.equals(""))
                {
                    mb.put(id, value.toString());
                    id = null;
                }
                else
                {
                    value.append(l);
                }
            }
        }

        assertNull(id);

        return mb.build();
    }

    private void assertGetIsAsExpected(String resource) throws Exception
    {
        assertGetIsAsExpected(resource, 200);
    }

    private boolean isJson()
    {
        if (mediaType.equals(MediaType.APPLICATION_XML_TYPE))
        {
            return false;
        }
        else if (mediaType.equals(MediaType.APPLICATION_JSON_TYPE))
        {
            return true;
        }
        else
        {
            throw new IllegalStateException("Unexpected media type for test: " + mediaType);
        }
    }

    private Object parse(String s) throws Exception
    {
        if (isJson())
        {
            return new ObjectMapper().readTree(s);
        }
        else
        {
            /* Normalise to ignore differences in attribute ordering between JDKs */
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(s)));
            StringWriter sw = new StringWriter();
            TransformerFactory.newInstance().newTransformer().transform(new DOMSource(doc), new StreamResult(sw));

            return sw.toString();
        }
    }

    private String get(String resource, int statusCode) throws Exception
    {
        assertThat(resource, Matchers.startsWith("/"));

        URI ref = URI.create(resource.substring(1));

        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).uri(ref);

        Builder builder = webResource.accept(mediaType);

        ClientResponse response = builder.get(ClientResponse.class);

        assertEquals(statusCode, response.getStatus());

        return response.getEntity(String.class);
    }

    private void assertGetIsAsExpected(String resource, int statusCode) throws Exception
    {
        String result = get(resource, statusCode);

        assertParsedEquals(expectedRepresentationForResource.get(resource), result);
    }

    private String postAndRetrieve(String resource, String body, int statusCode)
    {
        Preconditions.checkNotNull(body);

        assertThat(resource, Matchers.startsWith("/"));

        URI ref = URI.create(resource.substring(1));

        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).uri(ref);

        Builder builder = webResource.accept(mediaType);

        ClientResponse response = builder.entity(body, mediaType).post(ClientResponse.class);

        assertEquals(statusCode, response.getStatus());

        String result = response.getEntity(String.class);

        return result;
    }

    /**
     * Tests for:
     * <ul>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.CookieConfigEntity}</li>
     * </ul>
     */
    public void testCookieConfig() throws Exception
    {
        assertGetIsAsExpected("/config/cookie");
    }

    /**
     * Tests for:
     * <ul>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.UserEntity}</li>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.MultiValuedAttributeEntityList}</li>
     * </ul>
     */
    public void testUser() throws Exception
    {
        String response = get("/user?username=admin", 200);

        assertParsedEquals(
            removeRandomnessFromUserRepresentation(expectedRepresentationForResource.get("/user?username=admin")),
            removeRandomnessFromUserRepresentation(response));
    }

    /**
     * Remove anything ephemeral or random.
     */
    private String removeRandomnessFromUserRepresentation(String user)
    {
        if (isJson())
        {
            return user.replaceAll("\"key\"\\s*:\\s*\"[^\"]+\"", "\"key\":\"MASKED-RANDOM-VALUE\"");
        }
        else
        {
            return user.replaceAll("<key>[^<]+</key>", "<key>MASKED-RANDOM-VALUE</key>");
        }
    }

    /**
     * Tests for:
     * <ul>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.MultiValuedAttributeEntityList}</li>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.MultiValuedAttributeEntity}</li>
     * </ul>
     */
    public void testUserAttributes() throws Exception
    {
        assertGetIsAsExpected("/user/attribute?username=dir1user");
    }

    /**
     * Tests for:
     * <ul>
     * <li>{@link {@link com.atlassian.crowd.plugin.rest.entity.GroupEntity}</li>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.MultiValuedAttributeEntityList}</li>
     * </ul>
     */
    public void testGroup() throws Exception
    {
        assertGetIsAsExpected("/group?groupname=crowd-administrators");
    }

    /**
     * Tests for:
     * <ul>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.UserEntityList}</li>
     * </ul>
     */
    public void testUserList() throws Exception
    {
        assertGetIsAsExpected("/search?entity-type=user&restriction=name%3Dadmin");
    }

    /**
     * Tests for:
     * <ul>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.GroupEntityList}</li>
     * </ul>
     */
    public void testGroupList() throws Exception
    {
        assertGetIsAsExpected("/search?entity-type=group&restriction=name%3Dcrowd-administrators");
    }

    /**
     * Tests for {@link com.atlassian.crowd.plugin.rest.entity.ErrorEntity}.
     */
    public void testGetErrorEntity() throws Exception
    {
        assertGetIsAsExpected("/search?entity-type=no-such-entity-type", 400);
    }

    /**
     * Remove anything ephemeral.
     */
    String sanitiseSession(String session)
    {
        String withTokenBlanked;

        if (isJson())
        {
            withTokenBlanked = session.replaceAll("\"token\"\\s*:\\s*\"[^\"]{24}\"", "\"token\":\"RANDOM-TOKEN\"");
        }
        else
        {
            withTokenBlanked = session.replaceAll("<token>[^<]{24}</token>", "<token>RANDOM-TOKEN</token>");
        }

        String tokenBlankedInSelfLink = withTokenBlanked.replaceAll(
                "/usermanagement/1/session/\\w{24}\"",
                "/usermanagement/1/session/RANDOM-TOKEN\"");

        String datesRemoved;

        if (isJson())
        {
            datesRemoved = tokenBlankedInSelfLink.replaceAll("-date\"\\s*:\\s*\\d+", "-date\":1234567890");
        }
        else
        {
            datesRemoved = tokenBlankedInSelfLink.replaceAll("-date>....-..-..T..:..:..(\\....)?(...:..|Z)<",
                "-date>DATE<");
        }

        return datesRemoved;
    }

    private void assertParsedEquals(String expected, String actual) throws Exception
    {
        Object e = parse(expected);
        Object a = parse(actual);

        if (e instanceof String && a instanceof String)
        {
            /* This gives a better description */
            assertEquals((String) e, (String) a);
        }
        else
        {
            assertEquals(e, a);
        }
    }

    /**
     * Tests for:
     * <ul>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.AuthenticationContextEntity}</li>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.ValidationFactorEntityList}</li>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.ValidationFactorEntity}</li>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.SessionEntity}</li>
     * </ul>
     * @throws IOException
     * @throws JsonProcessingException
     */
    public void testPostAuthenticationContextEntity() throws Exception
    {
        String payload = expectedRepresentationForResource.get("/session request");

        String response = postAndRetrieve("/session", payload, 201);

        assertParsedEquals(
                sanitiseSession(expectedRepresentationForResource.get("/session response")),
                sanitiseSession(response));
    }

    /**
     * Tests for {@link com.atlassian.crowd.plugin.rest.entity.PasswordEntity}.
     */
    public void testPasswordEntity() throws Exception
    {
        String payload = expectedRepresentationForResource.get("/authentication?username=admin request");

        String response = postAndRetrieve("/authentication?username=admin", payload, 200);

        assertNotNull(parse(response));
    }

    /**
     * Tests for:
     * <ul>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.BooleanRestrictionEntity}</li>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.PropertyRestrictionEntity}</li>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.PropertyEntity}</li>
     * </ul>
     */
    public void testSearchRestrictionEntity() throws Exception
    {
        String payload = expectedRepresentationForResource.get("/search request");

        String response = postAndRetrieve("/search?entity-type=user", payload, 200);

        assertNotNull(parse(response));
    }

    private String sanitiseEvents(String events)
    {
        if (isJson())
        {
            return events.replaceAll("\"newEventToken\"\\s*:\\s*\"[^\"]*\"", "\"newEventToken\":\"TOKEN\"");
        }
        else
        {
            return events.replaceAll("newEventToken=\"[^\"]*\"", "newEventToken=\"TOKEN\"");
        }
    }

    /**
     * Tests for:
     * <ul>
     * <li>{@link com.atlassian.crowd.plugin.rest.entity.EventEntityList}</li>
     * </ul>
     */
    public void testEventEntityList() throws Exception
    {
        String response = get("/event", 200);

        assertParsedEquals(
                sanitiseEvents(expectedRepresentationForResource.get("/event")),
                sanitiseEvents(response));
    }
}
