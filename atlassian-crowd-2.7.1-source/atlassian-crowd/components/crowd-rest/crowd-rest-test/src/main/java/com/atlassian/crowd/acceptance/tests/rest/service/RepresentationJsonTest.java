package com.atlassian.crowd.acceptance.tests.rest.service;

import java.io.IOException;

import javax.ws.rs.core.MediaType;

import com.atlassian.crowd.acceptance.rest.RestServer;

public class RepresentationJsonTest extends RepresentationTest
{
    public RepresentationJsonTest(String name) throws IOException
    {
        super(name, MediaType.APPLICATION_JSON_TYPE);
    }

    public RepresentationJsonTest(String name, RestServer restServer) throws IOException
    {
        super(name, MediaType.APPLICATION_JSON_TYPE, restServer);
    }

    @Override
    public void testSearchRestrictionEntity() throws Exception
    {
        // Skip; not supported in JSON
    }
}
