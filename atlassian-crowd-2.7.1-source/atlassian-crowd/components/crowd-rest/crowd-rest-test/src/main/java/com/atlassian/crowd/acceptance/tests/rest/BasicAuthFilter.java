package com.atlassian.crowd.acceptance.tests.rest;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.ClientFilter;
import org.apache.commons.codec.binary.Base64;

import javax.ws.rs.core.HttpHeaders;
import java.io.UnsupportedEncodingException;

/**
 * This exists because the HTTPBasicAuthFilter shipped with Jersey 1.0.3 adds "\u0000" padding before encoding, which
 * confuses the Spring Basic Auth filter.
 */
public class BasicAuthFilter extends ClientFilter    
{
    private final String authentication;

    /**
     * Creates a new HTTP Basic Authentication filter with the provided username and password.
     *
     * @param username name of the user
     * @param password password
     */
    public BasicAuthFilter(String username, String password) {
        String token = username + ":" + password;
        String encodedToken = null;
        try
        {
            byte[] rawToken = token.getBytes("UTF-8");
            encodedToken = new String(Base64.encodeBase64(rawToken), "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            // the day that "UTF-8" fails is a bad day.
        }
        authentication = "Basic " + encodedToken;
    }

    @Override
    public ClientResponse handle(final ClientRequest cr) throws ClientHandlerException
    {

        if (!cr.getMetadata().containsKey(HttpHeaders.AUTHORIZATION)) {
            cr.getMetadata().add(HttpHeaders.AUTHORIZATION, authentication);
        }
        return getNext().handle(cr);
    }
}
