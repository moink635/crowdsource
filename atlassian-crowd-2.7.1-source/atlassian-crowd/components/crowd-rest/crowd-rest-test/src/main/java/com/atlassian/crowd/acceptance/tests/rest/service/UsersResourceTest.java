package com.atlassian.crowd.acceptance.tests.rest.service;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.plugin.rest.entity.MultiValuedAttributeEntity;
import com.atlassian.crowd.plugin.rest.entity.MultiValuedAttributeEntityList;
import com.atlassian.crowd.plugin.rest.entity.ErrorEntity;
import com.atlassian.crowd.plugin.rest.entity.GroupEntity;
import com.atlassian.crowd.plugin.rest.entity.GroupEntityList;
import com.atlassian.crowd.plugin.rest.entity.PasswordEntity;
import com.atlassian.crowd.plugin.rest.entity.UserEntity;
import com.atlassian.crowd.plugin.rest.util.EntityTranslator;
import com.atlassian.plugins.rest.common.Link;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Tests UsersResource.
 *
 * Note that this test class is reused in JIRA via inheritance. Please be mindful of that when making changes to this
 * class.
 */
public class UsersResourceTest extends RestCrowdServiceAcceptanceTestCase
{
    private static final String USERNAME_PARAM = "username";
    private static final String ATTRIBUTE_NAME_PARAM = "attributename";

    /**
     * Constructs a test case with the given name.
     *
     * @param name the test name
     */
    public UsersResourceTest(String name)
    {
        super(name);
    }

    /**
     * Constructs a test case with the given name, using the given RestServer.
     *
     * @param name the test name
     * @param restServer the RestServer
     */
    public UsersResourceTest(String name, RestServer restServer)
    {
        super(name, restServer);
    }

    /**
     * Tests retrieving a valid user. Checks that all the user details are correct.
     */
    public void testGetUser()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build("admin");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final UserEntity user = webResource.get(UserEntity.class);

        assertEquals("admin", user.getName());
        assertEquals("bob", user.getFirstName());
        assertEquals("the builder", user.getLastName());
        assertEquals("bob the builder", user.getDisplayName());
        assertEquals("bob@example.net", user.getEmail());
        assertTrue(user.isActive().booleanValue());
        assertEquals(webResource.toString(), user.getLink().getHref().toString());
    }

    /**
     * Tests that attempting to access the UserResource with an InvalidPassword returns a 401 (Unauthorized) response.
     */
    public void testGetUser_InvalidApplicationCredentials()
    {
        final String INVALID_PASSWORD = "InvalidPassword";

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build("admin");
        final WebResource webResource = getWebResource(APPLICATION_NAME, INVALID_PASSWORD, uri);

        ClientResponse clientResponse = webResource.get(ClientResponse.class);
        assertEquals(ClientResponse.Status.UNAUTHORIZED, clientResponse.getClientResponseStatus());
    }

    /**
     * Tests that retrieving a non-existent user returns a 404 (Not Found) response.
     */
    public void testGetUser_NonExistent()
    {
        final String NON_EXISTENT_USER = "nonexistent";

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build(NON_EXISTENT_USER);
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        ClientResponse clientResponse = webResource.get(ClientResponse.class);
        assertEquals(ClientResponse.Status.NOT_FOUND, clientResponse.getClientResponseStatus());
    }

    /**
     * Tests retrieving a user with attributes.
     */
    public void testGetUserWithAttributes()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).queryParam(USERNAME_PARAM, "{username}").queryParam("expand", "attributes").build("admin");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        UserEntity user = webResource.get(UserEntity.class);

        assertEquals("admin", user.getName());
        assertEquals("bob", user.getFirstName());
        assertEquals("the builder", user.getLastName());
        assertEquals("bob the builder", user.getDisplayName());
        assertEquals("bob@example.net", user.getEmail());
        assertTrue(user.isActive().booleanValue());
        URI expectedUri = webResource.getUriBuilder().replaceQueryParam("expand", (Object[]) null).build();
        assertEquals(expectedUri.toString(), user.getLink().getHref().toString());

        final MultiValuedAttributeEntityList attributeList = user.getAttributes();
        assertNotNull(attributeList);

        final Set<String> attributeNames = Sets.newHashSet();
        for (MultiValuedAttributeEntity attribute : attributeList)
        {
            attributeNames.add(attribute.getName());
        }

        final Set<String> expectedAttributeNames = Sets.newHashSet(
                                                        UserConstants.PASSWORD_LASTCHANGED,
                                                        UserConstants.INVALID_PASSWORD_ATTEMPTS,
                                                        UserConstants.LAST_AUTHENTICATED,
                                                        UserConstants.REQUIRES_PASSWORD_CHANGE);
        assertEquals(expectedAttributeNames, attributeNames);
    }

    /**
     * Tests adding a new user.
     */
    public void testAddUser() throws Exception
    {
        intendToModifyData();

        final String NEW_USERNAME = "newuser";
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        UserTemplate userTemplate = new UserTemplate(NEW_USERNAME);
        userTemplate.setActive(true);
        userTemplate.setDisplayName("New User via REST");
        userTemplate.setFirstName("New");
        userTemplate.setLastName("User");
        userTemplate.setEmailAddress("newuser@example.com");

        UserEntity userEntity = EntityTranslator.toUserEntity(userTemplate, Link.self(URI.create("link_to_user")));
        PasswordEntity passwordEntity = new PasswordEntity("secret", null);
        userEntity.setPassword(passwordEntity);

        webResource.entity(userEntity, MT).post();

        final URI userUri = getBaseUriBuilder().path(USERS_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build(NEW_USERNAME);
        final WebResource userWebResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, userUri);
        final UserEntity newUserEntity = userWebResource.get(UserEntity.class);
        assertEquals(NEW_USERNAME, newUserEntity.getName());

        assertNotNull("An identifier should be generated for the user", newUserEntity.getKey());
    }

    /**
     * Tests that adding a user without providing a password will return a 400 (Bad Request) response.
     */
    public void testAddUser_NoPasswordProvided()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        UserTemplate userTemplate = new UserTemplate("newuser");
        userTemplate.setActive(true);
        userTemplate.setDisplayName("New User via REST");
        userTemplate.setFirstName("New");
        userTemplate.setLastName("User");
        userTemplate.setEmailAddress("newuser@example.com");

        UserEntity userEntity = EntityTranslator.toUserEntity(userTemplate, Link.self(URI.create("link_to_user")));

        ClientResponse clientResponse = webResource.entity(userEntity, MT).post(ClientResponse.class);
        assertEquals(ClientResponse.Status.BAD_REQUEST, clientResponse.getClientResponseStatus());
    }

    /**
     * Tests that a user is successfully updated.
     */
    public void testUpdateUser()
    {
        intendToModifyData();

        final String NEW_EMAIL = "eeeep@example.net";

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build("eeeep");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        UserEntity user = webResource.get(UserEntity.class);
        user.setEmail(NEW_EMAIL);
        user.setActive(false);

        webResource.entity(user, MT).put();

        UserEntity updatedUser = webResource.get(UserEntity.class);
        assertEquals(NEW_EMAIL, updatedUser.getEmail());
        assertFalse(updatedUser.isActive());
    }

    /**
     * Tests that sending an updated user with a wrong user URI (i.e. a URI for another user) will return a
     * 400 (Bad Request) response.
     */
    public void testUpdateUser_WrongUserUri()
    {
        final String USERNAME = "eeeep";
        final String ANOTHER_USERNAME = "meeep";

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build(USERNAME);
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final UserEntity user = webResource.get(UserEntity.class);
        user.setName(ANOTHER_USERNAME);

        ClientResponse clientResponse = webResource.entity(user, MT).put(ClientResponse.class);
        assertEquals(ClientResponse.Status.BAD_REQUEST, clientResponse.getClientResponseStatus());
    }

    public void testUpdateUserWithDifferentCaseInUrlIsAccepted()
    {
        intendToModifyData();

        final String NEW_EMAIL = "eeeep@example.net";

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build("Eeeep");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        UserEntity user = webResource.get(UserEntity.class);
        user.setEmail(NEW_EMAIL);

        webResource.entity(user, MT).put();

        UserEntity updatedUser = webResource.get(UserEntity.class);
        assertEquals(NEW_EMAIL, updatedUser.getEmail());
    }

    public void testUpdateUserWithDifferentCaseInEntityIsAccepted()
    {
        intendToModifyData();

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build("eeeep");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        UserEntity user = webResource.get(UserEntity.class);
        user.setName("Eeeep");

        webResource.entity(user, MT).put();

        UserEntity updatedUser = webResource.get(UserEntity.class);
        assertEquals("eeeep", updatedUser.getName());
    }

    /**
     * Tests updating a user password.
     */
    public void testUpdateUserPassword()
    {
        intendToModifyData();

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path("password").queryParam(USERNAME_PARAM, "{username}").build("secondadmin");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        webResource.put(new PasswordEntity("newpassword", null));

        // Assert we can login with the updated credentials
        assertNotNull(authenticateUser("secondadmin", "newpassword"));
    }

    /**
     * Test store user attributes.
     */
    public void testStoreUserAttributes()
    {
        intendToModifyData();

        final String ATTRIB1_NAME = "favourite-colour";
        final String ATTRIB1_VALUE = "green";
        final String ATTRIB2_NAME = "favourite-animals";
        final String ATTRIB2_VALUE1 = "Tapirus terrestris";
        final String ATTRIB2_VALUE2 = "Suricata suricatta";

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path(ATTRIBUTES_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build("secondadmin");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final MultiValuedAttributeEntityList attributeListBefore = webResource.get(MultiValuedAttributeEntityList.class);
        final Map<String, Set<String>> expectedAttributes = toAttributesMap(attributeListBefore);
        expectedAttributes.put(ATTRIB1_NAME, Sets.newHashSet(ATTRIB1_VALUE));
        expectedAttributes.put(ATTRIB2_NAME, Sets.newHashSet(ATTRIB2_VALUE1, ATTRIB2_VALUE2));

        final MultiValuedAttributeEntity attrib1 = new MultiValuedAttributeEntity(ATTRIB1_NAME, Sets.newHashSet(ATTRIB1_VALUE), null);
        final MultiValuedAttributeEntity attrib2 = new MultiValuedAttributeEntity(ATTRIB2_NAME, Sets.newHashSet(ATTRIB2_VALUE1, ATTRIB2_VALUE2), null);
        final MultiValuedAttributeEntityList attributes = new MultiValuedAttributeEntityList(Arrays.asList(attrib1, attrib2), null);

        webResource.entity(attributes, MT).post();

        final MultiValuedAttributeEntityList userAttributes = webResource.get(MultiValuedAttributeEntityList.class);
        final Map<String, Set<String>> actualAttributes = toAttributesMap(userAttributes);

        assertEquals(expectedAttributes, actualAttributes);
    }

    /**
     * Tests deleting a user attribute.
     */
    public void testDeleteUserAttribute()
    {
        intendToModifyData();

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path(ATTRIBUTES_RESOURCE).queryParam(USERNAME_PARAM, "{username}").queryParam(ATTRIBUTE_NAME_PARAM, "{attributename}").build("secondadmin", UserConstants.PASSWORD_LASTCHANGED);
        final WebResource passwordLastChangedWR = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);
        passwordLastChangedWR.delete();

        final URI attributesUri = getBaseUriBuilder().path(USERS_RESOURCE).path(ATTRIBUTES_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build("secondadmin");
        final WebResource attributesWebResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, attributesUri);

        MultiValuedAttributeEntityList attributes = attributesWebResource.get(MultiValuedAttributeEntityList.class);
        for (MultiValuedAttributeEntity attribute : attributes)
        {
            if (UserConstants.PASSWORD_LASTCHANGED.equals(attribute.getName()))
            {
                fail("Attribute " + UserConstants.PASSWORD_LASTCHANGED + " not deleted.");
            }
        }
    }

    /**
     * Tests deleting a user.
     */
    public void testDeleteUser()
    {
        intendToModifyData();

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build("dir1user");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final UserEntity userEntity = webResource.get(UserEntity.class);
        assertNotNull(userEntity);
        webResource.delete();

        ClientResponse clientResponse = webResource.get(ClientResponse.class);
        assertEquals(ClientResponse.Status.NOT_FOUND, clientResponse.getClientResponseStatus());
        ErrorEntity errorEntity = clientResponse.getEntity(ErrorEntity.class);
        assertEquals(ErrorEntity.ErrorReason.USER_NOT_FOUND, errorEntity.getReason());
    }

    /**
     * Tests adding a user to a group.
     */
    public void testAddUserToGroup()
    {
        intendToModifyData();

        final String GROUP_NAME = "crowd-administrators";
        final URI groupMembershipUri = getBaseUriBuilder().path(USERS_RESOURCE).path("group").path("direct").queryParam(USERNAME_PARAM, "{username}").queryParam("groupname", "{groupname}").build("eeeep", GROUP_NAME);
        final WebResource groupMembershipWebResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, groupMembershipUri);

        // Assert the membership does not exist
        ClientResponse clientResponse = groupMembershipWebResource.get(ClientResponse.class);
        assertEquals(ClientResponse.Status.NOT_FOUND, clientResponse.getClientResponseStatus());
        ErrorEntity errorEntity = clientResponse.getEntity(ErrorEntity.class);
        assertEquals(ErrorEntity.ErrorReason.MEMBERSHIP_NOT_FOUND, errorEntity.getReason());

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path("group").path("direct").queryParam(USERNAME_PARAM, "{username}").build("eeeep");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);
        GroupEntity groupEntity = GroupEntity.newMinimalGroupEntity(GROUP_NAME, APPLICATION_NAME, URI.create("random"));

        webResource.entity(groupEntity, MT).post();

        GroupEntity newGroupEntity = groupMembershipWebResource.get(GroupEntity.class);
        assertEquals(GROUP_NAME, newGroupEntity.getName());
    }

    /**
     * Tests that adding a user to a non-existent group returns a 400 (Bad Request) response.
     */
    public void testAddUserToGroup_NonExistentGroup()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path("group").path("direct").queryParam(USERNAME_PARAM, "{username}").build("secondadmin");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        GroupEntity groupEntity = GroupEntity.newMinimalGroupEntity("non-existent-group", APPLICATION_NAME, URI.create("random"));

        ClientResponse clientResponse = webResource.entity(groupEntity, MT).post(ClientResponse.class);
        assertEquals(ClientResponse.Status.BAD_REQUEST, clientResponse.getClientResponseStatus());
        ErrorEntity errorEntity = clientResponse.getEntity(ErrorEntity.class);
        assertEquals(ErrorEntity.ErrorReason.GROUP_NOT_FOUND, errorEntity.getReason());
    }

    /**
     * Tests that adding a non-existent user to a group returns a 404 (Not Found) response.
     */
    public void testAddUserToGroup_NonExistentUser()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path("group").path("direct").queryParam(USERNAME_PARAM, "{username}").build("non-existent-user");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        GroupEntity groupEntity = GroupEntity.newMinimalGroupEntity("crowd-administrators", APPLICATION_NAME, URI.create("random"));

        ClientResponse clientResponse = webResource.entity(groupEntity, MT).post(ClientResponse.class);
        assertEquals(ClientResponse.Status.NOT_FOUND, clientResponse.getClientResponseStatus());
    }

    public void testAddUserToGroup_MembershipAlreadyExists()
    {
        final String GROUP_NAME = "badgers";

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path("group").path("direct").queryParam(USERNAME_PARAM, "{username}").build("eeeep");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);
        GroupEntity groupEntity = GroupEntity.newMinimalGroupEntity(GROUP_NAME, APPLICATION_NAME, URI.create("random"));

        ClientResponse clientResponse = webResource.entity(groupEntity, MT).post(ClientResponse.class);
        assertEquals(ClientResponse.Status.CONFLICT, clientResponse.getClientResponseStatus());
    }

    /**
     * Tests removing a user from a group.
     */
    public void testRemoveUserFromGroup()
    {
        intendToModifyData();

        final String USER_NAME = "eeeep";
        final String GROUP_NAME = "crowd-administrators";

        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path("group").path("direct").queryParam(USERNAME_PARAM, "{username}").build(USER_NAME);
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        GroupEntity groupEntity = GroupEntity.newMinimalGroupEntity(GROUP_NAME, APPLICATION_NAME, URI.create("random"));
        webResource.entity(groupEntity, MT).post();

        final URI groupMemberUri = getBaseUriBuilder().path(USERS_RESOURCE).path("group").path("direct").queryParam(USERNAME_PARAM, "{username}").queryParam("groupname", "{groupname}").build(USER_NAME, GROUP_NAME);
        final WebResource groupMemberWebResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, groupMemberUri);

        GroupEntity newGroupEntity = groupMemberWebResource.get(GroupEntity.class);
        assertEquals(GROUP_NAME, newGroupEntity.getName());

        // Now delete
        groupMemberWebResource.delete();

        ClientResponse clientResponse = groupMemberWebResource.get(ClientResponse.class);
        assertEquals(ClientResponse.Status.NOT_FOUND, clientResponse.getClientResponseStatus());
        ErrorEntity errorEntity = clientResponse.getEntity(ErrorEntity.class);
        assertEquals(ErrorEntity.ErrorReason.MEMBERSHIP_NOT_FOUND, errorEntity.getReason());
    }

    /**
     * Tests retrieving a nested group membership.
     */
    public void testGetNestedGroup()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path("group").path("nested").queryParam(USERNAME_PARAM, "{username}").queryParam("groupname", "{groupname}").build("penny", "crowd-administrators");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        GroupEntity groupEntity = webResource.get(GroupEntity.class);
        assertEquals("crowd-administrators", groupEntity.getName());
    }

    /**
     * Tests getting the nested groups.
     */
    public void testGetNestedGroups()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path("group").path("nested").queryParam(USERNAME_PARAM, "{username}").build("admin");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        GroupEntityList groupEntityList = webResource.get(GroupEntityList.class);
        Set<String> groupNames = GroupsResourceTest.namesOf(groupEntityList);
        Set<String> expectedNames = Sets.newHashSet("badgers", "crowd-administrators", "crowd-testers", "crowd-users");
        assertEquals(expectedNames, groupNames);
    }

    public void testGetDirectGroup()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path("group").path("direct").queryParam(USERNAME_PARAM, "{username}").queryParam("groupname", "{groupname}").build("admin", "badgers");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        GroupEntity groupEntity = webResource.get(GroupEntity.class);
        assertEquals("badgers", groupEntity.getName());
    }

    public void testGetDirectGroups()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path("group").path("direct").queryParam(USERNAME_PARAM, "{username}").build("admin");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        GroupEntityList groupEntityList = webResource.get(GroupEntityList.class);
        Set<String> groupNames = GroupsResourceTest.namesOf(groupEntityList);
        Set<String> expectedNames = Sets.newHashSet("badgers", "crowd-administrators");
        assertEquals(expectedNames, groupNames);
    }

    /**
     * Tests nested group membership not found
     */
    public void testGetNestedGroup_NonExistentGroupMembership()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).path("group").path("nested").queryParam(USERNAME_PARAM, "{username}").queryParam("groupname", "{groupname}").build("penny", "crowd-users");
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        ClientResponse clientResponse = webResource.get(ClientResponse.class);
        assertEquals(ClientResponse.Status.NOT_FOUND, clientResponse.getClientResponseStatus());
    }

    /**
     * Creates an Attributes map, mapping attributeName -> [attributeValues].
     *
     * @param attributeEntityList list of attributes
     * @return attributes map.
     */
    private static Map<String, Set<String>> toAttributesMap(final MultiValuedAttributeEntityList attributeEntityList)
    {
        Map<String, Set<String>> result = Maps.newHashMap();
        for (MultiValuedAttributeEntity attributeEntity : attributeEntityList)
        {
            Set<String> attributeValues = new HashSet<String>(attributeEntity.getValues());
            result.put(attributeEntity.getName(), attributeValues);
        }
        return result;
    }
}
