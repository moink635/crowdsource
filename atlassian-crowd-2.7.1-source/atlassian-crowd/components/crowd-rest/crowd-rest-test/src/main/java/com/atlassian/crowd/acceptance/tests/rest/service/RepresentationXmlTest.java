package com.atlassian.crowd.acceptance.tests.rest.service;

import java.io.IOException;

import javax.ws.rs.core.MediaType;

import com.atlassian.crowd.acceptance.rest.RestServer;

public class RepresentationXmlTest extends RepresentationTest
{
    public RepresentationXmlTest(String name) throws IOException
    {
        super(name, MediaType.APPLICATION_XML_TYPE);
    }

    public RepresentationXmlTest(String name, RestServer restServer) throws IOException
    {
        super(name, MediaType.APPLICATION_XML_TYPE, restServer);
    }

    public void testDatesAreReplaced()
    {
        assertEquals("<created-date>DATE</created-date>",
                sanitiseSession("<created-date>2013-01-30T16:38:27.369+11:00</created-date>"));

        assertEquals("<created-date>DATE</created-date>",
                sanitiseSession("<created-date>2013-02-07T09:00:03.096Z</created-date>"));

        assertEquals("<created-date>DATE</created-date>",
                sanitiseSession("<created-date>2013-04-17T22:47:29-05:00</created-date>"));
    }
}
