package com.atlassian.crowd.acceptance.tests.rest.service;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.acceptance.tests.rest.RestServerImpl;

import com.google.common.collect.Iterators;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

public class AliasResourceTest extends RestCrowdServiceAcceptanceTestCase
{
    public static final String ADMIN_NAME = "admin",
            ADMIN_PASSWORD = "admin";

    public static final String APPLICATION_ID = "884737";
    public static final String MISSING_APPLICATION_ID = "0";

    public AliasResourceTest(String name)
    {
        this(name, RestServerImpl.INSTANCE);
    }

    public AliasResourceTest(String name, RestServer restServer)
    {
        super(name, restServer);
    }

    private final UriBuilder aliasesBase()
    {
        return getBaseUriBuilder("appmanagement").path("aliases");
    }

    private WebResource getAliasResource(String applicationId, String username)
    {
        final URI uri = aliasesBase().path(applicationId).path("alias").queryParam("user", username).build();

        return getWebResource(ADMIN_NAME, ADMIN_PASSWORD, uri);
    }

    private WebResource getUsernameFromAliasResource(String applicationId, String alias)
    {
        final URI uri = aliasesBase().path(applicationId).path("username").queryParam("alias", alias).build();

        return getWebResource(ADMIN_NAME, ADMIN_PASSWORD, uri);
    }

    public void testGetAliasForAliasedUserReturnsAlias()
    {
        final WebResource webResource = getAliasResource(APPLICATION_ID, "admin");

        assertEquals("Alias1", webResource.get(String.class));
    }

    int errorCodeFromGetting(WebResource resource)
    {
        try
        {
            resource.get(String.class);
            fail("Should have returned an error status");
            return 0;
        }
        catch (UniformInterfaceException e)
        {
            return e.getResponse().getStatus();
        }
    }

    public void testGetAliasForUnaliasedUserIsNotFound()
    {
        final WebResource webResource = getAliasResource(APPLICATION_ID, "dir1user");

        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), errorCodeFromGetting(webResource));
    }

    public void testGetAliasForUnknownApplicationIsNotFound()
    {
        final WebResource webResource = getAliasResource(MISSING_APPLICATION_ID, "dir1user");

        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), errorCodeFromGetting(webResource));
    }

    public void testSetAliasModifiesExistingAlias()
    {
        intendToModifyData();

        final WebResource webResource = getAliasResource(APPLICATION_ID, "admin");

        webResource.put("test-modified-alias");
        assertEquals("test-modified-alias", webResource.get(String.class));
    }

    public void testSetAliasAddsAlias()
    {
        intendToModifyData();

        final WebResource webResource = getAliasResource(APPLICATION_ID, "dir1user");

        webResource.put("test-modified-alias");
        assertEquals("test-modified-alias", webResource.get(String.class));
    }

    public void testDeleteRemovesAlias()
    {
        intendToModifyData();

        final WebResource webResource = getAliasResource(APPLICATION_ID, "admin");

        webResource.delete();

        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), errorCodeFromGetting(webResource));
    }

    public void testGetUsernameByAliasReturnsOriginalUser()
    {
        final WebResource webResource = getUsernameFromAliasResource(APPLICATION_ID, "Alias1");

        assertEquals("admin", webResource.get(String.class));
    }

    public void testGetUsernameByAliasReturnsSameUsernameWhenNoAliasDefined()
    {
        final WebResource webResource = getUsernameFromAliasResource(APPLICATION_ID, "no-user-with-this-alias");

        assertEquals("no-user-with-this-alias", webResource.get(String.class));
    }

    private WebResource getAllAliasesResource(String username)
    {
        final URI uri = aliasesBase().queryParam("user", username).build();

        return getWebResource(ADMIN_NAME, ADMIN_PASSWORD, uri);
    }

    public void testGetAllAliasesForUserWithoutAliasesIsEmpty()
    {
        assertEquals("{}", getAllAliasesResource("dir1user").get(String.class));
    }

    public void testGetAllAliasesForUserWithAliasesIncludesApplicationsWithAliases() throws Exception
    {
        JsonNode obj = new ObjectMapper().readTree(getAllAliasesResource("admin").accept(MediaType.APPLICATION_JSON).get(String.class));

        // {"APPLICATION_ID": "Alias1"}
        assertEquals(APPLICATION_ID, Iterators.getOnlyElement(obj.getFieldNames()));
        assertEquals("Alias1", obj.get(APPLICATION_ID).getTextValue());
    }
}
