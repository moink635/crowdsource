package com.atlassian.crowd.acceptance.harness;

import com.atlassian.crowd.acceptance.tests.rest.RestXmlParsingTest;
import com.atlassian.crowd.acceptance.tests.rest.service.ApplicationResourceTest;
import com.atlassian.crowd.acceptance.tests.rest.service.AuthenticationResourceTest;
import com.atlassian.crowd.acceptance.tests.rest.service.CookieConfigResourceTest;
import com.atlassian.crowd.acceptance.tests.rest.service.CrowdWadlResourceTest;
import com.atlassian.crowd.acceptance.tests.rest.service.EventsResourceTest;
import com.atlassian.crowd.acceptance.tests.rest.service.GroupsResourceTest;
import com.atlassian.crowd.acceptance.tests.rest.service.RepresentationJsonTest;
import com.atlassian.crowd.acceptance.tests.rest.service.RepresentationXmlTest;
import com.atlassian.crowd.acceptance.tests.rest.service.SearchResourceTest;
import com.atlassian.crowd.acceptance.tests.rest.service.TokenResourceTest;
import com.atlassian.crowd.acceptance.tests.rest.service.UsersResourceTest;
import com.atlassian.crowd.acceptance.tests.rest.service.WebhooksResourceTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Tests the Crowd REST API.
 */
@RunWith (Suite.class)
@Suite.SuiteClasses({
    TokenResourceTest.class,
    AuthenticationResourceTest.class,
    GroupsResourceTest.class,
    UsersResourceTest.class,
    SearchResourceTest.class,
    ApplicationResourceTest.class,
    WebhooksResourceTest.class,
    CookieConfigResourceTest.class,
    EventsResourceTest.class,
    RestXmlParsingTest.class,
    CrowdWadlResourceTest.class,
    RepresentationXmlTest.class,
    RepresentationJsonTest.class
    // if you are adding new tests here, please consider adding them to HordeRestAcceptanceTestHarness as well
})
public class RestAcceptanceTestHarness
{
}
