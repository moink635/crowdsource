package com.atlassian.crowd.acceptance.tests.rest.service;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.acceptance.tests.TestDataState;
import com.atlassian.crowd.acceptance.tests.rest.BasicAuthFilter;
import com.atlassian.crowd.acceptance.tests.rest.RestServerImpl;
import com.atlassian.crowd.plugin.rest.entity.PasswordEntity;
import com.atlassian.crowd.plugin.rest.entity.UserEntity;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;

import junit.framework.TestCase;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import java.net.URI;
import java.net.URISyntaxException;

/**
 * All REST acceptance tests should subclass this class.
 */
public abstract class RestCrowdServiceAcceptanceTestCase extends TestCase
{
    protected static final String APPLICATION_NAME = "crowd";
    protected static final String APPLICATION_PASSWORD = "qybhDMZh";
    protected static final String GROUPS_RESOURCE = "group";
    protected static final String ATTRIBUTES_RESOURCE = "attribute";
    protected static final String USERS_RESOURCE = "user";
    protected static final String AUTHENTICATION_RESOURCE = "authentication";
    protected static final String SESSION_RESOURCE = "session";
    protected static final String WEBHOOKS_RESOURCE = "webhook";
    protected static final String EVENTS_RESOURCE = "event";

    /**
     * Specify this type for any entities being sent to the server.
     */
    protected static final MediaType MT = MediaType.APPLICATION_XML_TYPE;

    protected static final String REST_SERVICE_NAME = "usermanagement";
    private final static String USERNAME_PARAM = "username";

    private RestServer restServer = RestServerImpl.INSTANCE;

    /**
     * Constructs a test case with the given name.
     *
     * @param name the test name
     */
    protected RestCrowdServiceAcceptanceTestCase(String name)
    {
        this(name, RestServerImpl.INSTANCE);
    }

    /**
     * Constructs a test case with the given name, using the given RestServer.
     *
     * @param name the test name
     * @param restServer the RestServer
     */
    protected RestCrowdServiceAcceptanceTestCase(String name, RestServer restServer)
    {
        super(name);

        if (restServer == null) { throw new NullPointerException("restServer"); }
        this.restServer = restServer;
    }

    @Override
    public void setUp() throws Exception
    {
        restServer.before();
    }

    void intendToModifyData()
    {
        TestDataState.INSTANCE.intendToModify(restServer.getBaseUrl());
    }

    @Override
    public void tearDown() throws Exception
    {
        restServer.after();
    }

    /**
     * Sets the RestServer instance to test against.
     *
     * @param restServer a RestServer
     */
    public final void setRestServer(RestServer restServer)
    {
        this.restServer = restServer;
    }

    /**
     * Returns the base URI of the REST service.
     *
     * @return UriBuilder
     */
    protected UriBuilder getBaseUriBuilder()
    {
        return getBaseUriBuilder(REST_SERVICE_NAME);
    }

    protected UriBuilder getBaseUriBuilder(String serviceName)
    {
        return UriBuilder.fromUri(restServer.getBaseUrl().toString()).path("rest").path(serviceName).path("1");
    }

    protected UriBuilder getBaseUriBuilder(String serviceName, String version)
    {
        return UriBuilder.fromUri(restServer.getBaseUrl().toString()).path("rest").path(serviceName).path(version);
    }

    /**
     * Returns the "root" WebResource. This is the resource that's at the / of the crowd-rest-plugin plugin namespace.
     *
     * @param applicationName name of the application
     * @param password password of the application
     * @return root WebResource
     */
    protected WebResource getRootWebResource(final String applicationName, final String password)
    {
        final URI baseUri = getBaseUriBuilder().build();
        return getWebResource(applicationName, password, baseUri);
    }

    /**
     * Returns the WebResource with the specified URI.
     *
     * @param applicationName name of the application
     * @param password password of the application
     * @param uri URI of the resource
     * @return WebResource
     */
    protected WebResource getWebResource(final String applicationName, final String password, final URI uri)
    {
        Client client = Client.create();
        client.addFilter(new BasicAuthFilter(applicationName, password));
        client = restServer.decorateClient(client);
        return client.resource(uri);
    }

    /**
     * Returns the WebResource with the specified URI, retrieved anonymously.
     *
     * @param applicationName name of the application
     * @return WebResource
     */
    protected WebResource getWebResource(final URI uri)
    {
        Client client = Client.create();
        client = restServer.decorateClient(client);
        return client.resource(uri);
    }

    /**
     * Nicely formats a Status.
     *
     * @param status Status
     * @return formatted string
     */
    protected static String statusToString(final Response.Status status)
    {
        return String.format("%d (%s)", status.getStatusCode(), status.toString());
    }

    /**
     * Authenticate a user with the specified username and password.
     *
     * @param username username
     * @param password password
     * @return UserEntity
     * @throws com.sun.jersey.api.client.UniformInterfaceException if the status of the HTTP response is greater than or equal to 300.
     */
    protected UserEntity authenticateUser(final String username, final String password)
    {
        final URI uri = getBaseUriBuilder().path(AUTHENTICATION_RESOURCE).queryParam(USERNAME_PARAM, "{username}").build(username);
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);
        return webResource.post(UserEntity.class, new PasswordEntity(password, null));
    }
}
