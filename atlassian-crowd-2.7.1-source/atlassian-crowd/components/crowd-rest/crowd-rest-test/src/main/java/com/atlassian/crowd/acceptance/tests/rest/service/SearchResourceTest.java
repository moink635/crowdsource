package com.atlassian.crowd.acceptance.tests.rest.service;

import java.net.URI;
import java.util.Arrays;
import java.util.Set;

import javax.ws.rs.core.Response;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.plugin.rest.entity.ErrorEntity;
import com.atlassian.crowd.plugin.rest.entity.GroupEntityList;
import com.atlassian.crowd.plugin.rest.entity.MultiValuedAttributeEntity;
import com.atlassian.crowd.plugin.rest.entity.MultiValuedAttributeEntityList;
import com.atlassian.crowd.plugin.rest.entity.NamedEntity;
import com.atlassian.crowd.plugin.rest.entity.NullRestrictionEntity;
import com.atlassian.crowd.plugin.rest.entity.SearchRestrictionEntity;
import com.atlassian.crowd.plugin.rest.entity.UserEntity;
import com.atlassian.crowd.plugin.rest.entity.UserEntityList;
import com.atlassian.crowd.plugin.rest.util.SearchRestrictionEntityTranslator;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.restriction.Property;
import com.atlassian.crowd.search.query.entity.restriction.PropertyImpl;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;

import org.hamcrest.collection.IsEmptyIterable;

import static org.junit.Assert.assertThat;

/**
 * Note that this test class is reused in JIRA via inheritance. Please be mindful of that when making changes to this
 * class.
 */
public class SearchResourceTest extends RestCrowdServiceAcceptanceTestCase
{
    private static final String APPLICATION_NAME = "crowd";
    private static final String SEARCH_RESOURCE = "search";
    private static final String START_INDEX_PARAM = "start-index";
    private static final String MAX_RESULT_PARAM = "max-results";
    private static final String ENTITY_TYPE_PARAM = "entity-type";
    private static final String USER_ENTITY_TYPE = "user";
    private static final String GROUP_ENTITY_TYPE = "group";

    private static final String EXPAND_PARAM = "expand";
    private static final String USER_PARAM_VALUE = "user";
    private static final String GROUP_PARAM_VALUE = "group";

    private static final String USERNAME1 = "admin";
    private static final String USERNAME2 = "eeeep";
    private static final String USERNAME3 = "penny";
    private static final String USERNAME4 = "regularuser";
    private static final String USERNAME5 = "secondadmin";
    private static final String USERNAME6 = "dir1user";

    private static final String GROUP1 = "animals";
    private static final String GROUP2 = "badgers";
    private static final String GROUP3 = "birds";
    private static final String GROUP4 = "cats";
    private static final String GROUP5 = "crowd-administrators";
    private static final String GROUP6 = "crowd-testers";
    private static final String GROUP7 = "crowd-users";
    private static final String GROUP8 = "dogs";

    private static final String EMAIL1 = "bob@example.net";

    private static final String NON_EXISTENT_EMAIL = "nosuchemail@nosuchemail.com";
    private static final int TOTAL_NUM_USERS;

    private static final Set<String> ALL_USERNAMES;
    private static final int TOTAL_NUM_GROUPS;
    private static final Set<String> ALL_GROUPNAMES;

    private static final String ALIAS_APPLICATION_NAME = "aliases";
    private static final String ALIAS_APPLICATION_PASSWORD = "aliases";

    static
    {
        ALL_USERNAMES = Sets.newHashSet(
                            USERNAME1,
                            USERNAME2,
                            USERNAME3,
                            USERNAME4,
                            USERNAME5,
                            USERNAME6);

        TOTAL_NUM_USERS = ALL_USERNAMES.size();

        ALL_GROUPNAMES = Sets.newHashSet(
                            GROUP1,
                            GROUP2,
                            GROUP3,
                            GROUP4,
                            GROUP5,
                            GROUP6,
                            GROUP7,
                            GROUP8);

        TOTAL_NUM_GROUPS = ALL_GROUPNAMES.size();
    }

    /**
     * Constructs a test case with the given name.
     *
     * @param name the test name
     */
    public SearchResourceTest(String name)
    {
        super(name);
    }

    /**
     * Constructs a test case with the given name, using the given RestServer.
     *
     * @param name the test name
     * @param restServer the RestServer
     */
    public SearchResourceTest(String name, RestServer restServer)
    {
        super(name, restServer);
    }

    /**
     * Tests that getting an unknown entity type results in a 400 (Bad Request) returned.
     */
    public void testGetUnknownEntityType() throws Exception
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, "UNKNOWN").build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final Response.Status expectedStatus = Response.Status.BAD_REQUEST;
        try
        {
            webResource.entity(NullRestrictionEntity.INSTANCE, MT).post(UserEntityList.class);
            fail(expectedStatus + " expected");
        }
        catch (UniformInterfaceException e)
        {
            ErrorEntity errorEntity = e.getResponse().getEntity(ErrorEntity.class);
            assertEquals(ErrorEntity.ErrorReason.ILLEGAL_ARGUMENT, errorEntity.getReason());
            assertEquals(expectedStatus.getStatusCode(), e.getResponse().getStatus());
        }
    }

    /**
     * Tests getting all available user names.
     */
    public void testGetUserNames()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final UserEntityList users = webResource.entity(NullRestrictionEntity.INSTANCE, MT).post(UserEntityList.class);
        final Set<String> userNames = getNames(users);

        assertEquals(ALL_USERNAMES, userNames);
    }

    /**
     * Tests getting user names with a max-result parameter.
     */
    public void testGetUserNames_MaxResult()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).queryParam(MAX_RESULT_PARAM, "3").build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final UserEntityList users = webResource.entity(NullRestrictionEntity.INSTANCE, MT).post(UserEntityList.class);
        final Set<String> userNames = getNames(users);

        assertEquals(3, userNames.size());
    }

    /**
     * Tests getting all user names from an offset value.
     */
    public void testGetUserNames_StartIndex()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).queryParam(START_INDEX_PARAM, "3").build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final UserEntityList users = webResource.entity(NullRestrictionEntity.INSTANCE, MT).post(UserEntityList.class);
        final Set<String> userNames = getNames(users);

        assertEquals(TOTAL_NUM_USERS - 3, userNames.size());
    }

    /**
     * Tests that users with matching aliases are returned in lower case.
     */
    public void testGetUserNames_Aliases()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).build();
        final WebResource webResource = getWebResource(ALIAS_APPLICATION_NAME, ALIAS_APPLICATION_PASSWORD, uri);

        final UserEntityList users = webResource
                .entity(SearchRestrictionEntityTranslator.toSearchRestrictionEntity(Restriction.on(UserTermKeys.USERNAME).containing("alias")), MT)
                .post(UserEntityList.class);
        final Set<String> userNames = getNames(users);

        assertEquals(Sets.newHashSet("alias1", "alias2", "alias3"), userNames);
    }

    /**
     * Tests that user with username 'admin' is not returned, because that user has an alias.
     */
    public void testGetUserNames_AliasesIgnoreUsernameWhenAliasExists()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).build();
        final WebResource webResource = getWebResource(ALIAS_APPLICATION_NAME, ALIAS_APPLICATION_PASSWORD, uri);

        final UserEntityList users = webResource
                .entity(SearchRestrictionEntityTranslator.toSearchRestrictionEntity(Restriction.on(UserTermKeys.USERNAME).containing("admin")), MT)
                .post(UserEntityList.class);
        final Set<String> userNames = getNames(users);

        assertEquals(Sets.newHashSet("secondadmin"), userNames);
    }

    /**
     * Tests that all users are matched and returned in lower case.
     */
    public void testGetUserNames_AliasesAll()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).build();
        final WebResource webResource = getWebResource(ALIAS_APPLICATION_NAME, ALIAS_APPLICATION_PASSWORD, uri);

        final UserEntityList users = webResource
                .entity(SearchRestrictionEntityTranslator.toSearchRestrictionEntity(Restriction.on(UserTermKeys.USERNAME).containing("a")), MT)
                .post(UserEntityList.class);
        final Set<String> userNames = getNames(users);

        assertEquals(Sets.newHashSet("alias1", "alias2", "alias3", "secondadmin"), userNames);
    }

    /**
     * Tests getting all user names from a negative offset value.  Should return a Bad Request (400) status.
     */
    public void testGetUserNames_NegativeStartIndex()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).queryParam(START_INDEX_PARAM, "-1").build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        try
        {
            webResource.entity(NullRestrictionEntity.INSTANCE, MT).post(UserEntityList.class);
            fail("Should have returned a Bad Request Error (400) status.");
        }
        catch (UniformInterfaceException e)
        {
            ClientResponse response = e.getResponse();
            assertEquals("Should have returned a Bad Request Error (400) status.", Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        }
    }

    /**
     * Tests getting all user names with the specified email.
     */
    public void testGetUserNames_EmailRestriction()
    {
        final PropertyRestriction<String> emailRestriction = Restriction.on(UserTermKeys.EMAIL).exactlyMatching(EMAIL1);
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final SearchRestrictionEntity restriction = SearchRestrictionEntityTranslator.toSearchRestrictionEntity(emailRestriction);
        UserEntityList users = webResource.entity(restriction, MT).post(UserEntityList.class);
        Set<String> usernames = getNames(users);

        Set<String> expectedUsernames = Sets.newHashSet(USERNAME1, USERNAME4);
        assertEquals(expectedUsernames, usernames);

        // test that a non-existent email should give an empty result
        final PropertyRestriction<String> nonExistentEmailRestriction = Restriction.on(UserTermKeys.EMAIL).exactlyMatching(NON_EXISTENT_EMAIL);
        final SearchRestrictionEntity restRestriction = SearchRestrictionEntityTranslator.toSearchRestrictionEntity(nonExistentEmailRestriction);
        users = webResource.entity(restRestriction, MT).post(UserEntityList.class);
        usernames = getNames(users);

        assertTrue(usernames.isEmpty());
    }

    /**
     * Tests getting all users.
     */
    public void testGetUsers()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).queryParam(EXPAND_PARAM, USER_PARAM_VALUE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final UserEntityList users = webResource.entity(NullRestrictionEntity.INSTANCE, MT).post(UserEntityList.class);

        assertEquals(TOTAL_NUM_USERS, users.size());
    }

    /**
     * Tests getting users with user name restriction.
     */
    public void testGetUsers_UserRestriction()
    {
        PropertyRestriction<String> usernameRestriction = Restriction.on(UserTermKeys.USERNAME).exactlyMatching(USERNAME2);
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).queryParam(EXPAND_PARAM, USER_PARAM_VALUE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        SearchRestrictionEntity restriction = SearchRestrictionEntityTranslator.toSearchRestrictionEntity(usernameRestriction);
        final UserEntityList users = webResource.entity(restriction, MT).post(UserEntityList.class);

        assertEquals(1, users.size());
        assertEquals(USERNAME2, users.get(0).getName());
        assertEquals("doflynn@atlassian.com", users.get(0).getEmail());
    }

    public void testGetUsersRestrictionOnUnusedCustomAttributeReturnsNoUsers()
    {
        Property<String> customAttribute = new PropertyImpl<String>("a-custom-property", String.class);
        PropertyRestriction<String> propRestriction = Restriction.on(customAttribute).exactlyMatching("custom-value");
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).queryParam(EXPAND_PARAM, USER_PARAM_VALUE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        SearchRestrictionEntity restriction = SearchRestrictionEntityTranslator.toSearchRestrictionEntity(propRestriction);
        final UserEntityList users = webResource.entity(restriction, MT).post(UserEntityList.class);

        assertThat(users, IsEmptyIterable.<UserEntity>emptyIterable());
    }

    public void testGetUsersRestrictionOnCustomAttributeReturnsUser()
    {
        intendToModifyData();

        /* Set a custom attribute */
        final URI userUri = getBaseUriBuilder().path(USERS_RESOURCE).path(ATTRIBUTES_RESOURCE).queryParam("username", "regularuser").build();

        final MultiValuedAttributeEntity attrib1 = new MultiValuedAttributeEntity("a-custom-property", ImmutableSet.of("custom-value"), null);
        final MultiValuedAttributeEntityList attributes = new MultiValuedAttributeEntityList(Arrays.asList(attrib1), null);

        getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, userUri).entity(attributes, MT).post();

        Property<String> customAttribute = new PropertyImpl<String>("a-custom-property", String.class);
        PropertyRestriction<String> propRestriction = Restriction.on(customAttribute).exactlyMatching("custom-value");
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).queryParam(EXPAND_PARAM, USER_PARAM_VALUE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        SearchRestrictionEntity restriction = SearchRestrictionEntityTranslator.toSearchRestrictionEntity(propRestriction);
        final UserEntityList users = webResource.entity(restriction, MT).post(UserEntityList.class);

        assertEquals(ImmutableSet.of("regularuser"), getNames(users));
    }

    /**
     * Tests getting all users from an offset value.
     */
    public void testGetUser_StartIndex()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, USER_ENTITY_TYPE).queryParam(EXPAND_PARAM, USER_PARAM_VALUE).queryParam(START_INDEX_PARAM, "3").build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final UserEntityList users = webResource.entity(NullRestrictionEntity.INSTANCE, MT).post(UserEntityList.class);

        assertEquals(TOTAL_NUM_USERS - 3, users.size());
    }

    /**
     * Tests getting all the group names
     */
    public void testGetGroupNames()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, GROUP_ENTITY_TYPE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final GroupEntityList groups = webResource.entity(NullRestrictionEntity.INSTANCE, MT).post(GroupEntityList.class);
        final Set<String> groupNames = getNames(groups);
        assertEquals(ALL_GROUPNAMES, groupNames);
    }

    /**
     * Tests getting a restricted number of group names (specifying a max result parameter).
     */
    public void testGetGroupNames_Restricted()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, GROUP_ENTITY_TYPE).queryParam(MAX_RESULT_PARAM, "3").build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final GroupEntityList groups = webResource.entity(NullRestrictionEntity.INSTANCE, MT).post(GroupEntityList.class);
        final Set<String> groupNames = getNames(groups);

        assertEquals(3, groupNames.size());
    }

    /**
     * Tests getting all group names from an offset value.
     */
    public void testGetGroupNames_StartIndex()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, GROUP_ENTITY_TYPE).queryParam(START_INDEX_PARAM, "3").build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        final GroupEntityList groups = webResource.entity(NullRestrictionEntity.INSTANCE, MT).post(GroupEntityList.class);
        final Set<String> groupNames = getNames(groups);

        assertEquals(TOTAL_NUM_GROUPS - 3, groupNames.size());
    }

    /**
     * Tests getting all group names with active and inactive groups.
     */
    public void testGetGroupNames_ActiveRestriction()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, GROUP_ENTITY_TYPE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        PropertyRestriction<Boolean> groupActiveRestriction = Restriction.on(GroupTermKeys.ACTIVE).exactlyMatching(true);
        SearchRestrictionEntity restriction = SearchRestrictionEntityTranslator.toSearchRestrictionEntity(groupActiveRestriction);

        GroupEntityList groups = webResource.entity(restriction, MT).post(GroupEntityList.class);
        Set<String> groupNames = getNames(groups);
        assertEquals(ALL_GROUPNAMES, groupNames);

        groupActiveRestriction = Restriction.on(GroupTermKeys.ACTIVE).exactlyMatching(false);
        restriction = SearchRestrictionEntityTranslator.toSearchRestrictionEntity(groupActiveRestriction);
        groups = webResource.entity(restriction, MT).post(GroupEntityList.class);
        groupNames = getNames(groups);
        assertTrue(groupNames.isEmpty());
    }

    /**
     * Tests getting all groups.
     */
    public void testGetGroups()
    {
        final URI uri = getBaseUriBuilder().path(SEARCH_RESOURCE).queryParam(ENTITY_TYPE_PARAM, GROUP_ENTITY_TYPE).queryParam(EXPAND_PARAM, GROUP_PARAM_VALUE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        GroupEntityList groups = webResource.entity(NullRestrictionEntity.INSTANCE, MT).post(GroupEntityList.class);

        assertEquals(TOTAL_NUM_GROUPS, groups.size());
    }

    /**
     * Returns a set of names from a list of entities. Order is not important so we use a set.
     *
     * @return set of user names
     */
    private static Set<String> getNames(Iterable<? extends NamedEntity> users)
    {
        final Set<String> usernames = Sets.newHashSet();
        for (NamedEntity user : users)
        {
            usernames.add(user.getName());
        }
        return usernames;
    }
}
