package com.atlassian.crowd.acceptance.tests.rest.service;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.acceptance.tests.TestDataState;
import com.atlassian.crowd.acceptance.tests.rest.BasicAuthFilter;
import com.atlassian.crowd.acceptance.tests.rest.RestServerImpl;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.ImmutableApplication;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.plugin.rest.entity.ApplicationEntity;
import com.atlassian.crowd.plugin.rest.entity.ApplicationErrorEntity;
import com.atlassian.crowd.plugin.rest.entity.RemoteAddressEntity;
import com.atlassian.crowd.plugin.rest.entity.RemoteAddressEntitySet;
import com.atlassian.crowd.plugin.rest.util.ApplicationEntityTranslator;
import com.atlassian.crowd.plugin.rest.util.ApplicationLinkUriHelper;
import com.atlassian.plugins.rest.common.Link;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import static com.atlassian.crowd.plugin.rest.util.ApplicationEntityTranslator.PasswordMode.INCLUDE_PASSWORD;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * Tests for the Application resource
 *
 * @since 2.2
 */
public class ApplicationResourceTest
{
    private static final MediaType MT = RestCrowdServiceAcceptanceTestCase.MT;
    private final static String X_FORWARDED_FOR_HEADER_NAME = "X-Forwarded-For";
    protected static final String USER_NAME = "admin";
    protected static final String USER_PASSWORD = "admin";
    protected static final String APPLICATION_RESOURCE = "application";
    protected static final long CROWD_APPLICATION_ID = 163841L;
    protected static final String CROWD_APPLICATION_NAME = "crowd";

    protected static final String REST_SERVICE_NAME = "appmanagement";

    private RestServer restServer = RestServerImpl.INSTANCE;

    @Before
    public void setUp() throws Exception
    {
        restServer.before();
    }

    @After
    public void tearDown() throws Exception
    {
        restServer.after();
    }

    void intendToModifyData()
    {
        TestDataState.INSTANCE.intendToModify(restServer.getBaseUrl());
    }

    /**
     * Returns the application ID.
     *
     * @return application ID
     */
    protected long getApplicationId()
    {
        return CROWD_APPLICATION_ID;
    }

    /**
     * Returns the application name.
     *
     * @return application name
     */
    protected String getApplicationName()
    {
        return CROWD_APPLICATION_NAME;
    }

    /**
     * Returns the application type.
     *
     * @return application type
     */
    protected ApplicationType getApplicationType()
    {
        return ApplicationType.CROWD;
    }

    /**
     * Returns the base URI of the REST service.
     *
     * @return UriBuilder
     */
    protected UriBuilder getBaseUriBuilder()
    {
        return UriBuilder.fromUri(restServer.getBaseUrl().toString()).path("rest").path(REST_SERVICE_NAME).path("1");
    }

    /**
     * Returns the "root" WebResource. This is the resource that's at the / of the crowd-rest-plugin plugin namespace.
     *
     * @param userName name of the user
     * @param password password of the user
     * @return root WebResource
     */
    protected WebResource getRootWebResource(final String userName, final String password)
    {
        final URI baseUri = getBaseUriBuilder().build();
        return getWebResource(userName, password, baseUri);
    }

    /**
     * Returns the WebResource with the specified URI.
     *
     * @param userName name of the user
     * @param password password of the user
     * @param uri URI of the resource
     * @return WebResource
     */
    protected WebResource getWebResource(final String userName, final String password, final URI uri)
    {
        Client client = Client.create();
        client.addFilter(new BasicAuthFilter(userName, password));
        client = restServer.decorateClient(client);
        return client.resource(uri);
    }

    /**
     * Tests that the REST service allows an application to be retrieved by name
     */
    @Test
    public void testGetApplicationByName() throws Exception
    {
        final URI uri = getBaseUriBuilder().path(APPLICATION_RESOURCE).queryParam("name", "{appname}").build(getApplicationName());
        final WebResource webResource = getWebResource(USER_NAME, USER_PASSWORD, uri);

        final ApplicationEntity applicationEntity = webResource.get(ApplicationEntity.class);

        assertEquals(getApplicationName(), applicationEntity.getName());
        assertEquals(getApplicationType().name(), applicationEntity.getType().toUpperCase());
        assertTrue(applicationEntity.isActive());
        assertNull(applicationEntity.getPassword());
    }

    /**
     * Tests that retrieving a non-existent application by name will return a 404 (Not Found) response with a reason of
     * Application Not Found.
     */
    @Test
    public void testGetApplicationByName_NotFound() throws Exception
    {
        final URI uri = getBaseUriBuilder().path(APPLICATION_RESOURCE).queryParam("name", "{appname}").build("non-existent");
        final WebResource webResource = getWebResource(USER_NAME, USER_PASSWORD, uri);

        final ClientResponse clientResponse = webResource.get(ClientResponse.class);
        final ApplicationErrorEntity applicationErrorEntity = clientResponse.getEntity(ApplicationErrorEntity.class);

        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), clientResponse.getStatus());
        assertEquals(ApplicationErrorEntity.ErrorReason.APPLICATION_NOT_FOUND, applicationErrorEntity.getReason());

    }

    /**
     * Tests that the REST service allows an application to be retrieved by ID
     */
    @Test
    public void testGetApplicationById() throws Exception
    {
        final URI uri = getBaseUriBuilder().path(APPLICATION_RESOURCE).path(String.valueOf(getApplicationId())).build();
        final WebResource webResource = getWebResource(USER_NAME, USER_PASSWORD, uri);

        final ApplicationEntity applicationEntity = webResource.get(ApplicationEntity.class);

        assertEquals(getApplicationName(), applicationEntity.getName());
        assertEquals(getApplicationType().name(), applicationEntity.getType().toUpperCase());
        assertTrue(applicationEntity.isActive());
        assertNull(applicationEntity.getPassword());
    }

    /**
     * Tests that retrieving a non-existent application by ID will return a 404 (Not Found) response with a reason of
     * Application Not Found.
     */
    @Test
    public void testGetApplicationById_NotFound() throws Exception
    {
        final URI uri = getBaseUriBuilder().path(APPLICATION_RESOURCE).path("-1").build();
        final WebResource webResource = getWebResource(USER_NAME, USER_PASSWORD, uri);

        final ClientResponse clientResponse = webResource.get(ClientResponse.class);
        final ApplicationErrorEntity applicationErrorEntity = clientResponse.getEntity(ApplicationErrorEntity.class);

        assertEquals(Response.Status.NOT_FOUND.getStatusCode(), clientResponse.getStatus());
        assertEquals(ApplicationErrorEntity.ErrorReason.APPLICATION_NOT_FOUND, applicationErrorEntity.getReason());

    }

    /**
     * Tests that the REST service does not allow a user to access the Application Management API if an incorrect user
     * password has been given.
     */
    @Test
    public void testWrongPassword() throws Exception
    {
        final URI uri = getBaseUriBuilder().path(APPLICATION_RESOURCE).path("1").build();
        final WebResource webResource = getWebResource(USER_NAME, "badpassword", uri);

        ClientResponse response = webResource.get(ClientResponse.class);
        assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
    }

    /**
     * Tests that adding an application is successful.
     */
    @Test
    public void testAddApplication() throws Exception
    {
        intendToModifyData();

        final Set<RemoteAddress> remoteAddresses = ImmutableSet.of(
            new RemoteAddress("::1"),
            new RemoteAddress("myhost")
        );
        final Application application = ImmutableApplication.builder("mynewapp", ApplicationType.BAMBOO)
                                            .setDescription("My Bamboo App")
                                            .setRemoteAddresses(remoteAddresses)
                                            .setPasswordCredential(PasswordCredential.unencrypted("myapppassword"))
                                            .setActive(true)
                                            .build();
        final ApplicationEntity applicationEntity = ApplicationEntityTranslator.toApplicationEntity(application, Link.self(URI.create("link_to_application")), INCLUDE_PASSWORD);
        final URI addApplicationUri = getBaseUriBuilder().path(APPLICATION_RESOURCE).build();
        final WebResource addApplicationWebResource = getWebResource(USER_NAME, USER_PASSWORD, addApplicationUri);

        final ClientResponse response = addApplicationWebResource.entity(applicationEntity, MT).post(ClientResponse.class);
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        // ensure that the link returned is correct and that the application has been added
        final String applicationLocation = response.getHeaders().getFirst("Location");
        assertNotNull(applicationLocation);
        final URI applicationUri = URI.create(applicationLocation);
        final WebResource applicationWebResource = getWebResource(USER_NAME, USER_PASSWORD, applicationUri);
        final ApplicationEntity createdApplication = applicationWebResource.get(ApplicationEntity.class);
        assertEquals("mynewapp", createdApplication.getName());
        assertEquals(ApplicationType.BAMBOO.name(), createdApplication.getType().toUpperCase());
        assertTrue(createdApplication.isActive());
        assertNull(createdApplication.getPassword());

        // ensure that the remote addresses are the same
        Set<String> expectedRemoteAddresses = Sets.newHashSet("::1", "myhost");
        Set<String> actualRemoteAddresses = Sets.newHashSet();
        for (RemoteAddressEntity remoteAddressEntity : createdApplication.getRemoteAddresses())
        {
            actualRemoteAddresses.add(remoteAddressEntity.getValue());
        }
        assertEquals(expectedRemoteAddresses, actualRemoteAddresses);
    }

    /**
     * Tests that adding an application with no password will fail with a 400 (Bad Request) with a reason of Invalid
     * Credential.
     */
    @Test
    public void testAddApplication_NoPassword() throws Exception
    {
        final Application application = ImmutableApplication.builder("mynewapp", ApplicationType.BAMBOO)
                                            .setDescription("My Bamboo App")
                                            .setActive(true)
                                            .build();
        final ApplicationEntity applicationEntity = ApplicationEntityTranslator.toApplicationEntity(application, Link.self(URI.create("link_to_application")), INCLUDE_PASSWORD);
        final URI addApplicationUri = getBaseUriBuilder().path(APPLICATION_RESOURCE).build();
        final WebResource addApplicationWebResource = getWebResource(USER_NAME, USER_PASSWORD, addApplicationUri);

        final ClientResponse response = addApplicationWebResource.entity(applicationEntity, MT).post(ClientResponse.class);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        final ApplicationErrorEntity applicationErrorEntity = response.getEntity(ApplicationErrorEntity.class);
        assertEquals(ApplicationErrorEntity.ErrorReason.INVALID_CREDENTIAL, applicationErrorEntity.getReason());
    }

    /**
     * Tests that adding an application with no name will fail with a 400 (Bad Request) with a reason of Illegal
     * Argument.
     */
    @Test
    public void testAddApplication_NoName() throws Exception
    {
        final Application application = ImmutableApplication.builder(null, ApplicationType.BAMBOO)
                                            .setDescription("My Bamboo App")
                                            .setPasswordCredential(PasswordCredential.unencrypted("myapppassword"))
                                            .setActive(true)
                                            .build();
        final ApplicationEntity applicationEntity = ApplicationEntityTranslator.toApplicationEntity(application, Link.self(URI.create("link_to_application")), INCLUDE_PASSWORD);
        final URI addApplicationUri = getBaseUriBuilder().path(APPLICATION_RESOURCE).build();
        final WebResource addApplicationWebResource = getWebResource(USER_NAME, USER_PASSWORD, addApplicationUri);

        final ClientResponse response = addApplicationWebResource.entity(applicationEntity, MT).post(ClientResponse.class);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        final ApplicationErrorEntity applicationErrorEntity = response.getEntity(ApplicationErrorEntity.class);
        assertEquals(ApplicationErrorEntity.ErrorReason.ILLEGAL_ARGUMENT, applicationErrorEntity.getReason());
    }

    /**
     * Tests that adding an application with no type will fail with a 400 (Bad Request) with a reason of Illegal
     * Argument.
     */
    @Test
    public void testAddApplication_NoType() throws Exception
    {
        final Application application = ImmutableApplication.builder("mynewapp", ApplicationType.BAMBOO)
                                            .setDescription("My Bamboo App")
                                            .setPasswordCredential(PasswordCredential.unencrypted("myapppassword"))
                                            .setActive(true)
                                            .build();
        final ApplicationEntity applicationEntity = ApplicationEntityTranslator.toApplicationEntity(application, Link.self(URI.create("link_to_application")), INCLUDE_PASSWORD);
        applicationEntity.setType(null);
        final URI addApplicationUri = getBaseUriBuilder().path(APPLICATION_RESOURCE).build();
        final WebResource addApplicationWebResource = getWebResource(USER_NAME, USER_PASSWORD, addApplicationUri);

        final ClientResponse response = addApplicationWebResource.entity(applicationEntity, MT).post(ClientResponse.class);
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        final ApplicationErrorEntity applicationErrorEntity = response.getEntity(ApplicationErrorEntity.class);
        assertEquals(ApplicationErrorEntity.ErrorReason.ILLEGAL_ARGUMENT, applicationErrorEntity.getReason());
    }

    /**
     * Tests that adding a remote address is successful.
     */
    @Test
    public void testAddRemoteAddress() throws Exception
    {
        intendToModifyData();

        final RemoteAddressEntity newAddressEntity = new RemoteAddressEntity("1.2.3.4", Link.self(URI.create("link_to_address")));
        final URI addRemoteAddressUri = getBaseUriBuilder().path(APPLICATION_RESOURCE).path(String.valueOf(getApplicationId())).path("remote_address").build();
        final WebResource addApplicationWebResource = getWebResource(USER_NAME, USER_PASSWORD, addRemoteAddressUri);

        final ClientResponse response = addApplicationWebResource.entity(newAddressEntity, MT).post(ClientResponse.class);
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        // ensure that the link returned is correct and that the application has been added
        final String remoteAddressLocation = response.getHeaders().getFirst("Location");
        assertNotNull(remoteAddressLocation);
        final URI applicationUri = ApplicationLinkUriHelper.buildApplicationUri(getBaseUriBuilder().build(), getApplicationId());
        final URI remoteAddressesUri = ApplicationLinkUriHelper.buildRemoteAddressesUri(applicationUri);
        final WebResource applicationWebResource = getWebResource(USER_NAME, USER_PASSWORD, remoteAddressesUri);
        final RemoteAddressEntitySet remoteAddressEntities = applicationWebResource.get(RemoteAddressEntitySet.class);

        // ensure that the remote addresses are the same
        Set<String> actualRemoteAddresses = Sets.newHashSet();
        for (RemoteAddressEntity remoteAddressEntity : remoteAddressEntities)
        {
            actualRemoteAddresses.add(remoteAddressEntity.getValue());
        }
        assertTrue(actualRemoteAddresses.contains("1.2.3.4"));
    }

    /**
     * Tests adding an application with the request address included. Checks that the application is able to connect to
     * the Crowd REST service after adding the application.
     */
    @Test
    public void testAddApplicationWithRequestAddress()
    {
        intendToModifyData();

        final String appName = "mynewapp";
        final String appPassword = "myapppassword";
        final Map<String, String> httpHeaders = Collections.emptyMap();

        addApplicationIncludingRequestAddress(appName, appPassword, httpHeaders);
        connectAsApplication(appName, appPassword, httpHeaders);
    }

    /**
     * Tests adding an application with the request address included, and simulating a proxy being involved (i.e. the
     * X-Forwarded-For header set). Checks that the application is able to connect to the Crowd REST service after
     * adding the application.
     */
    @Test
    public void testAddApplicationWithRequestAddress_WithProxy()
    {
        intendToModifyData();

        final String appName = "mynewapp";
        final String appPassword = "myapppassword";
        final String xffHeader = "127.0.0.1, 1.2.3.4";

        final Map<String, String> httpHeaders = Collections.singletonMap(X_FORWARDED_FOR_HEADER_NAME, xffHeader);

        addApplicationIncludingRequestAddress(appName, appPassword, httpHeaders);
        connectAsApplication(appName, appPassword, httpHeaders);
    }

    /**
     * Helper method to test adding an application including the request address.
     *
     * @param appName application name
     * @param appPassword application password
     * @param httpHeaders HTTP headers
     */
    private void addApplicationIncludingRequestAddress(final String appName, final String appPassword, final Map<String, String> httpHeaders)
    {
        intendToModifyData();

        final Application application = ImmutableApplication.builder(appName, ApplicationType.BAMBOO)
                                            .setDescription("My Bamboo App")
                                            .setPasswordCredential(PasswordCredential.unencrypted(appPassword))
                                            .setActive(true)
                                            .build();
        final ApplicationEntity applicationEntity = ApplicationEntityTranslator.toApplicationEntity(application, Link.self(URI.create("link_to_application")), INCLUDE_PASSWORD);
        final URI addApplicationUri = getBaseUriBuilder().path(APPLICATION_RESOURCE).queryParam("include-request-address", "true").build();
        final WebResource addApplicationWebResource = getWebResource(USER_NAME, USER_PASSWORD, addApplicationUri);
        for (Map.Entry<String, String> header : httpHeaders.entrySet())
        {
            addApplicationWebResource.header(header.getKey(), header.getValue());
        }

        final ClientResponse response = addApplicationWebResource.entity(applicationEntity, MT).post(ClientResponse.class);
        assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

        // ensure that the link returned is correct and that the application has been added
        final String applicationLocation = response.getHeaders().getFirst("Location");
        assertNotNull(applicationLocation);
        final URI applicationUri = URI.create(applicationLocation);
        final WebResource applicationWebResource = getWebResource(USER_NAME, USER_PASSWORD, applicationUri);
        final ApplicationEntity createdApplication = applicationWebResource.get(ApplicationEntity.class);
        assertEquals("mynewapp", createdApplication.getName());
        assertEquals(ApplicationType.BAMBOO.name(), createdApplication.getType().toUpperCase());
        assertTrue(createdApplication.isActive());
        assertNull(createdApplication.getPassword());
    }

    /**
     * Helper method to test connecting to the Crowd REST user management service as the application. The test connection
     * actually retrieves all the users.
     *
     * @param appName application name
     * @param appPassword application password
     * @param httpHeaders HTTP headers to set in the client request header
     */
    private void connectAsApplication(final String appName, final String appPassword, final Map<String, String> httpHeaders)
    {
        final URI baseUri = UriBuilder.fromUri(restServer.getBaseUrl().toString()).path("rest").path("usermanagement").path("1").build();
        final URI getAllUsersUri = UriBuilder.fromUri(baseUri).path("search").queryParam("entity-type", "user").build();
        final WebResource getAllUsersWebResource = getWebResource(appName, appPassword, getAllUsersUri);

        for (Map.Entry<String, String> header : httpHeaders.entrySet())
        {
            getAllUsersWebResource.header(header.getKey(), header.getValue());
        }

        final ClientResponse connectAppResponse = getAllUsersWebResource.get(ClientResponse.class);

        assertEquals(Response.Status.OK.getStatusCode(), connectAppResponse.getStatus());
        assertNotNull(connectAppResponse.getHeaders().get("X-Embedded-Crowd-Version"));
    }

    public RestServer getRestServer()
    {
        return restServer;
    }

    public void setRestServer(RestServer restServer)
    {
        this.restServer = restServer;
    }
}
