package com.atlassian.crowd.acceptance.tests.rest.service;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.plugin.rest.entity.ErrorEntity;
import com.atlassian.crowd.plugin.rest.entity.UserEntity;
import com.sun.jersey.api.client.UniformInterfaceException;

import javax.ws.rs.core.Response;

/**
 * Note that this test class is reused in JIRA via inheritance. Please be mindful of that when making changes to this
 * class.
 */
public class AuthenticationResourceTest extends RestCrowdServiceAcceptanceTestCase
{
    private final static String TEST_USERNAME = "admin";
    private final static String NON_EXISTENT_USERNAME = "non-existent";
    private final static String TEST_PASSWORD = "admin";

    /**
     * Constructs a test case with the given name.
     *
     * @param name the test name
     */
    public AuthenticationResourceTest(String name)
    {
        super(name);
    }

    /**
     * Constructs a test case with the given name, using the given RestServer.
     *
     * @param name the test name
     * @param restServer the RestServer
     */
    public AuthenticationResourceTest(String name, RestServer restServer)
    {
        super(name, restServer);
    }

    /**
     * Tests that the Authentication resource correctly authenticates a valid user.
     */
    public void testUserAuthentication()
    {
        UserEntity userEntity = authenticateUser(TEST_USERNAME, TEST_PASSWORD);

        assertNotNull(userEntity);
        assertEquals(TEST_USERNAME, userEntity.getName());
    }

    /**
     * Tests that the Authentication resource will return a 400 (Bad Request) status if the user authentication details
     * are incorrect.
     */
    public void testUserAuthentication_BadCredentials()
    {
        final Response.Status expectedStatus = Response.Status.BAD_REQUEST;
        try
        {
            authenticateUser(TEST_USERNAME, "I am a bad credential");
            fail(statusToString(expectedStatus) + " expected");
        }
        catch (UniformInterfaceException e)
        {
            assertEquals(expectedStatus.getStatusCode(), e.getResponse().getStatus());
        }
    }

    /**
     * Tests that the Authentication resource will return a 400 (Bad Request) status if there is no such user.
     */
    public void testUserAuthentication_NoUser()
    {
        final Response.Status expectedStatus = Response.Status.BAD_REQUEST;
        try
        {
            authenticateUser(NON_EXISTENT_USERNAME, TEST_PASSWORD);
            fail(statusToString(expectedStatus) + " expected");
        }
        catch (UniformInterfaceException e)
        {
            assertEquals(expectedStatus.getStatusCode(), e.getResponse().getStatus());
        }
    }

    /**
     * Tests that the Authentication resource will return a 400 (Bad Request) status if the authentication for the user
     * is correct but the user is not a member of an authorised group of the application.
     */
    public void testUserAuthentication_UnauthorisedGroupUser()
    {
        final String UNAUTHORISED_USERNAME = "dir1user";
        final String UNAUTHORISED_USER_PASSWORD = "dir1user";
        final Response.Status expectedStatus = Response.Status.BAD_REQUEST;
        try
        {
            authenticateUser(UNAUTHORISED_USERNAME, UNAUTHORISED_USER_PASSWORD);
            fail(statusToString(expectedStatus) + " expected");
        }
        catch (UniformInterfaceException e)
        {
            assertEquals(expectedStatus.getStatusCode(), e.getResponse().getStatus());
            final ErrorEntity errorEntity = e.getResponse().getEntity(ErrorEntity.class);
            assertNotNull(errorEntity);
            assertEquals(ErrorEntity.ErrorReason.INVALID_USER_AUTHENTICATION, errorEntity.getReason());
        }
    }

    /**
     * Tests that the Authentication resource will successfully authenticate a user that is not explicitly in an
     * authorised group of the application but is in a directory where all the users are allowed to authenticate with
     * the application.
     */
    public void testUserAuthentication_AllowAllToAuthenticate()
    {
        authenticateUser("regularuser", "regularuser");
    }

    /**
     * Tests that the Authentication resource will only authenticate the first user that it finds with the specified
     * username. i.e. even though there is another user in the second directory with a matching username and password,
     * it will only use the credentials of the first user.
     */
    public void testUserAuthentication_CommonUserUnauthorised()
    {
        authenticateUser("secondadmin", "secondadmin");
        final Response.Status expectedStatus = Response.Status.BAD_REQUEST;
        try
        {
            authenticateUser("secondadmin", "secondadmindir2");
            fail(statusToString(expectedStatus) + " expected");
        }
        catch (UniformInterfaceException e)
        {
            assertEquals(expectedStatus.getStatusCode(), e.getResponse().getStatus());
            final ErrorEntity errorEntity = e.getResponse().getEntity(ErrorEntity.class);
            assertNotNull(errorEntity);
            assertEquals(ErrorEntity.ErrorReason.INVALID_USER_AUTHENTICATION, errorEntity.getReason());
        }
    }

    /**
     * Tests that the Authentication resource will successfully authenticate a nested user.
     */
    public void testUserAuthentication_NestedUser()
    {
        authenticateUser("penny", "penny");
    }
}