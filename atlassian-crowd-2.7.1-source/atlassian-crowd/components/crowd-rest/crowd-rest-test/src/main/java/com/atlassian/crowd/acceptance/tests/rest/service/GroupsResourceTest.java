package com.atlassian.crowd.acceptance.tests.rest.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MediaType;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.plugin.rest.entity.ErrorEntity;
import com.atlassian.crowd.plugin.rest.entity.GroupEntity;
import com.atlassian.crowd.plugin.rest.entity.GroupEntityList;
import com.atlassian.crowd.plugin.rest.entity.MultiValuedAttributeEntity;
import com.atlassian.crowd.plugin.rest.entity.MultiValuedAttributeEntityList;
import com.atlassian.crowd.plugin.rest.entity.NamedEntity;
import com.atlassian.crowd.plugin.rest.entity.UserEntity;
import com.atlassian.crowd.plugin.rest.entity.UserEntityList;
import com.atlassian.crowd.plugin.rest.util.EntityTranslator;
import com.atlassian.crowd.plugin.rest.util.LinkUriHelper;
import com.atlassian.plugins.rest.common.Link;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertThat;

/**
 * Note that this test class is reused in JIRA via inheritance. Please be mindful of that when making changes to this
 * class.
 */
public class GroupsResourceTest extends RestCrowdServiceAcceptanceTestCase
{
    private static final String APPLICATION_NAME = "crowd";
    private static final String USER_PATH = "user";
    private static final String DIRECT_PATH = "direct";
    private static final String NESTED_PATH = "nested";
    private static final String MEMBERSHIP_PATH = "membership";
    private static final String GROUPNAME_PARAM = "groupname";
    private static final String CHILD_GROUPNAME_PARAM = "child-groupname";
    private static final String PARENT_GROUPNAME_PARAM = "parent-groupname";
    private static final String USERNAME_PARAM = "username";
    private static final String CHILD_GROUP_PATH = "child-group";
    private static final String PARENT_GROUP_PATH = "parent-group";
    private static final String START_INDEX_PARAM = "start-index";

    private static final String MAX_RESULT_PARAM = "max-results";
    private static final String USERNAME1 = "admin";
    private static final String USERNAME2 = "eeeep";
    private static final String USERNAME3 = "penny";
    private static final String GROUP1 = "badgers";
    private static final String GROUP2 = "crowd-administrators";
    private static final String GROUP3 = "crowd-testers";
    private static final String GROUP4 = "crowd-users";
    private static final String GROUP5 = "animals";
    private static final String GROUP6 = "birds";
    private static final String GROUP7 = "寿司";
    private static final String GROUP7_URLENCODED = "%E5%AF%BF%E5%8F%B8";

    private static final String EMAIL1 = "bob@example.net";
    private static final String EMAIL2 = "doflynn@atlassian.com";

    /**
     * Constructs a test case with the given name.
     *
     * @param name the test name
     */
    public GroupsResourceTest(String name)
    {
        super(name);
    }

    /**
     * Constructs a test case with the given name, using the given RestServer.
     *
     * @param name the test name
     * @param restServer the RestServer
     */
    public GroupsResourceTest(String name, RestServer restServer)
    {
        super(name, restServer);
    }

    public void testGetGroup()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).queryParam("groupname", "badgers");

        GroupEntity restGroup = webResource.get(GroupEntity.class);

        assertEquals("badgers", restGroup.getName());
        assertEquals("A group for badgers", restGroup.getDescription());
        assertEquals(true, restGroup.isActive());
        assertEquals(webResource.toString(), restGroup.getLink().getHref().toString());
    }

    public void testGetGroup_Expanded()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).queryParam("groupname", "badgers").queryParam("expand", "attributes");

        GroupEntity restGroup = webResource.get(GroupEntity.class);

        assertEquals("badgers", restGroup.getName());
        assertEquals("A group for badgers", restGroup.getDescription());
        assertEquals(true, restGroup.isActive());
        assertEquals(webResource.toString(), restGroup.getLink().getHref().toString() + "&expand=attributes");

        final Map<String, Set<String>> attributes = EntityTranslator.toAttributes(restGroup.getAttributes());
        assertEquals(1, attributes.size());
        assertEquals(Collections.singleton("hollow"), attributes.get("secret-location"));
    }

    public void testGetGroup_DoesNotExist()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).queryParam("groupname", "beavers");

        final ClientResponse response = webResource.get(ClientResponse.class);
        assertError(response, 404, ErrorEntity.ErrorReason.GROUP_NOT_FOUND);
    }

    public void testAddGroup()
    {
        intendToModifyData();

        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(GROUPS_RESOURCE);

        GroupTemplate groupTemplate = new GroupTemplate("beavers");
        groupTemplate.setDescription("A group for beavers");
        groupTemplate.setActive(false);

        webResource.entity(EntityTranslator.toGroupEntity(groupTemplate, getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI()), MT).post();

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(GROUPS_RESOURCE).queryParam("groupname", "beavers");

        GroupEntity restGroup = webResource.get(GroupEntity.class);
        assertEquals("beavers", restGroup.getName());
        assertEquals("A group for beavers", restGroup.getDescription());
        assertFalse(restGroup.isActive());
    }

    public void testAddGroup_Unicode()
    {
        intendToModifyData();

        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(GROUPS_RESOURCE);

        GroupTemplate groupTemplate = new GroupTemplate(GROUP7);
        groupTemplate.setDescription("A group with unicode characters in the name");
        groupTemplate.setActive(false);

        ClientResponse response = webResource
                .entity(EntityTranslator.toGroupEntity(groupTemplate, getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI()), MT)
                .post(ClientResponse.class);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).path(GROUPS_RESOURCE).queryParam("groupname", GROUP7);

        final String encodedURI = webResource.getURI().toASCIIString();
        assertTrue("URI should end with '" + GROUP7_URLENCODED + "' but was: " + encodedURI, encodedURI.endsWith(GROUP7_URLENCODED));
        assertEquals(webResource.getURI(), response.getLocation());

        GroupEntity restGroup = webResource.get(GroupEntity.class);
        assertEquals(GROUP7, restGroup.getName());
        assertEquals("A group with unicode characters in the name", restGroup.getDescription());
        assertFalse(restGroup.isActive());
    }

    public void testUpdateGroup()
    {
        intendToModifyData();

        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).queryParam("groupname", "badgers");

        GroupEntity restGroup = webResource.get(GroupEntity.class);

        GroupTemplate template = new GroupTemplate(EntityTranslator.toGroup(restGroup));
        template.setActive(false);
        template.setDescription("Badgers Rock!");

        webResource.entity(EntityTranslator.toGroupEntity(template, getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI()), MT).put();

        restGroup = webResource.get(GroupEntity.class);

        assertEquals("Badgers Rock!", restGroup.getDescription());
        assertFalse(restGroup.isActive());
    }

    public void testUpdateGroup_BadDetails()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).queryParam("groupname", "badgers");

        GroupEntity restGroup = webResource.get(GroupEntity.class);

        GroupTemplate template = new GroupTemplate(EntityTranslator.toGroup(restGroup));
        template.setName("cats");
        template.setActive(false);
        template.setDescription("Cats are cool!");

        final ClientResponse response = webResource
                .entity(EntityTranslator.toGroupEntity(template, getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI()), MT)
                .put(ClientResponse.class);
        assertError(response, 400, ErrorEntity.ErrorReason.INVALID_GROUP);
    }

    public void testDeleteGroup()
    {
        intendToModifyData();

        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).queryParam("groupname", "badgers");

        webResource.delete();

        final ClientResponse response = webResource.get(ClientResponse.class);
        assertError(response, 404, ErrorEntity.ErrorReason.GROUP_NOT_FOUND);
    }

    public void testStoreGroupAttributes()
    {
        intendToModifyData();

        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(ATTRIBUTES_RESOURCE).queryParam("groupname", "badgers");

        MultiValuedAttributeEntity attribute = new MultiValuedAttributeEntity("special-food-requirements", Sets.newHashSet("stoats", "voles", "marmots"), null);
        MultiValuedAttributeEntityList restAttributes = new MultiValuedAttributeEntityList(Collections.singletonList(attribute), null);

        webResource.entity(restAttributes, "application/XML").post();

        MultiValuedAttributeEntityList attributeEntityList = webResource.get(MultiValuedAttributeEntityList.class);

        final Map<String, Set<String>> attributes = EntityTranslator.toAttributes(attributeEntityList);
        assertEquals(2, attributes.size());
        assertEquals(Collections.singleton("hollow"), attributes.get("secret-location"));

        Set<String> foods = attributes.get("special-food-requirements");
        assertEquals(3, foods.size());
        assertTrue(foods.containsAll(Sets.newHashSet("stoats", "voles", "marmots")));
    }

    public void testDeleteGroupAttribute()
    {
        intendToModifyData();

        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(ATTRIBUTES_RESOURCE).queryParam("groupname", "badgers").queryParam("attributename", "secret-location");

        webResource.delete();

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(ATTRIBUTES_RESOURCE).queryParam("groupname", "badgers");

        MultiValuedAttributeEntityList attributeEntityList = webResource.get(MultiValuedAttributeEntityList.class);

        final Map<String, Set<String>> attributes = EntityTranslator.toAttributes(attributeEntityList);
        assertTrue(attributes.isEmpty());
        assertNull(attributes.get("secret-location"));
    }

    /**
     * Tests get usernames of a group.
     */
    public void testGetDirectUsers()
    {
        //group/user/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP1);

        UserEntityList users = webResource.get(UserEntityList.class);

        assertEquals(2, users.size());
        assertContainsName(users, USERNAME1);
        assertContainsName(users, USERNAME2);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP4);

        users = webResource.get(UserEntityList.class);

        assertEquals(0, users.size());
    }

    /**
     * Tests get user members of a group.
     */
    public void testGetDirectUsers_Limit1()
    {
        //group/user/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP1).queryParam(START_INDEX_PARAM, "0").queryParam(MAX_RESULT_PARAM, "1");

        UserEntityList users = webResource.get(UserEntityList.class);

        assertEquals(1, users.size());
        assertContainsName(users, USERNAME1);
    }

    /**
     * Tests get user members of a group.
     */
    public void testGetDirectUsers_Limit2()
    {
        //group/user/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP1).queryParam(START_INDEX_PARAM, "1").queryParam(MAX_RESULT_PARAM, "1");

        UserEntityList users = webResource.get(UserEntityList.class);

        assertEquals(1, users.size());
        assertContainsName(users, USERNAME2);
    }

    /**
     * Tests retrieving the users that are direct members of the specified group.
     */
    public void testGetDirectUsers_Expanded()
    {
        //group/user/direct?groupname=<groupname>&expand=user
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP1).queryParam("expand", "user");

        UserEntityList users = webResource.get(UserEntityList.class);

        assertEquals(2, users.size());
        assertContainsName(users, USERNAME1);
        assertContainsEmail(users, EMAIL1);
        assertContainsName(users, USERNAME2);
        assertContainsEmail(users, EMAIL2);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP4);

        users = webResource.get(UserEntityList.class);

        assertEquals(0, users.size());
    }

    public void testAddDirectUser()
    {
        intendToModifyData();

        //group/user/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP2);

        final Link link = LinkUriHelper.buildUserLink(getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI(), USERNAME2);
        UserEntity user = UserEntity.newMinimalUserEntity(USERNAME2, null, link);

        ClientResponse response = webResource.entity(user, MT).post(ClientResponse.class);
        assertEquals(201, response.getStatus());
        assertEquals(webResource.queryParam(USERNAME_PARAM, USERNAME2).getURI(), response.getLocation());
    }

    public void testAddDirectUser_NoUser()
    {
        //group/user/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP2);

        final Link link = LinkUriHelper.buildUserLink(getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI(), USERNAME2);
        UserEntity user = UserEntity.newMinimalUserEntity("non-existent-user", null, link);

        ClientResponse response = webResource.entity(user, MT).post(ClientResponse.class);
        assertError(response, 400, ErrorEntity.ErrorReason.USER_NOT_FOUND);
    }

    public void testAddDirectUser_NoGroup()
    {
        //group/user/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, "non-existant-group");

        final Link link = LinkUriHelper.buildUserLink(getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI(), USERNAME2);
        UserEntity user = UserEntity.newMinimalUserEntity(USERNAME2, null, link);

        ClientResponse response = webResource.entity(user, MT).post(ClientResponse.class);
        assertError(response, 404, ErrorEntity.ErrorReason.GROUP_NOT_FOUND);
    }

    /**
     * Tests get user members of a group.
     */
    public void testGetDirectUsers_User()
    {
        //group/user/direct?groupname=<groupname>&username=<username>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP1).queryParam(USERNAME_PARAM, USERNAME1);

        UserEntity user = webResource.get(UserEntity.class);

        assertEquals(USERNAME1, user.getName());

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP4);


        webResource.get(UserEntityList.class);
    }

    public void testDeleteDirectUser()
    {
        intendToModifyData();

        //group/user/direct?groupname=<groupname>&username=<username>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP2).queryParam(USERNAME_PARAM, USERNAME1);

        ClientResponse response = webResource.get(ClientResponse.class);
        assertEquals(200, response.getStatus());

        response = webResource.delete(ClientResponse.class);
        assertEquals(204, response.getStatus());

        response = webResource.get(ClientResponse.class);
        assertError(response, 404, ErrorEntity.ErrorReason.MEMBERSHIP_NOT_FOUND);
    }

    /**
     * Tests get user members of a group.
     */
    public void testGetNestedUsers()
    {
        //group/user/nested?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP1);

        UserEntityList users = webResource.get(UserEntityList.class);

        assertEquals(2, users.size());
        assertContainsName(users, USERNAME1);
        assertContainsName(users, USERNAME2);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP4);

        users = webResource.get(UserEntityList.class);
        assertEquals(2, users.size());
        assertContainsName(users, USERNAME1);
        assertContainsName(users, USERNAME2);
    }

    /**
     * Tests get user members of a group.
     */
    public void testGetNestedUsers_Expanded()
    {
        //group/user/nested?groupname=<groupname>&expand=user
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP1).queryParam("expand", "user");

        UserEntityList users = webResource.get(UserEntityList.class);

        assertEquals(2, users.size());
        assertContainsName(users, USERNAME1);
        assertContainsEmail(users, EMAIL1);
        assertContainsName(users, USERNAME2);
        assertContainsEmail(users, EMAIL2);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP4).queryParam("expand", "user");

        users = webResource.get(UserEntityList.class);
        assertEquals(2, users.size());
        assertContainsName(users, USERNAME1);
        assertContainsEmail(users, EMAIL1);
        assertContainsName(users, USERNAME2);
        assertContainsEmail(users, EMAIL2);
    }

    public void testGetNestedUsers_User()
    {
        //group/user/nested?groupname=<groupname>&username=<username>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP1).queryParam(USERNAME_PARAM, USERNAME1);

        UserEntity user = webResource.get(UserEntity.class);

        assertEquals(USERNAME1, user.getName());
    }

    public void testGetNestedUsers_NoUser()
    {
        //group/user/nested?groupname=<groupname>&username=<username>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP1).queryParam(USERNAME_PARAM, USERNAME3);

        ClientResponse response = webResource.get(ClientResponse.class);
        assertError(response, 404, ErrorEntity.ErrorReason.MEMBERSHIP_NOT_FOUND);
    }

    public void testGetNestedUsers_EmptyUsernameFails()
    {
        //group/user/nested?groupname=<groupname>&username=
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(USER_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP1).queryParam(USERNAME_PARAM, "");

        ClientResponse response = webResource.get(ClientResponse.class);
        assertError(response, 400, ErrorEntity.ErrorReason.ILLEGAL_ARGUMENT);
    }

    /**
     * Tests get user group memberships.
     */
    public void testGetDirectParentGroups()
    {
        //group/parent-group/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP1);

        GroupEntityList groups = webResource.get(GroupEntityList.class);

        assertEquals(2, groups.size());
        assertContainsName(groups, GROUP3);
        assertContainsName(groups, GROUP4);
        assertNotExpanded(groups);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                        path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP3);

        groups = webResource.get(GroupEntityList.class);

        assertEquals(1, groups.size());
        assertContainsName(groups, GROUP2);
        assertNotExpanded(groups);

    }

    /**
     * Tests get user group memberships.
     */
    public void testGetDirectParentGroups_Expanded()
    {
        //group/parent-group/direct?groupname=<groupname>&expand=group
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP1).queryParam("expand", "group");

        GroupEntityList groups = webResource.get(GroupEntityList.class);

        assertEquals(2, groups.size());
        assertContainsName(groups, GROUP3);
        assertContainsName(groups, GROUP4);
        assertExpanded(groups);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                        path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP3).queryParam("expand", "group");

        groups = webResource.get(GroupEntityList.class);

        assertEquals(1, groups.size());
        assertContainsName(groups, GROUP2);
        assertExpanded(groups);
    }

    /**
     * Tests adding a direct parent group membership.
     */
    public void testAddDirectParentGroup()
    {
        intendToModifyData();

        //group/parent-group/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP1);

        GroupEntity group = GroupEntity.newMinimalGroupEntity(GROUP2, null, getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI());

        ClientResponse response = webResource.entity(group, MT).post(ClientResponse.class);

        assertEquals(201, response.getStatus());
        assertEquals(webResource.queryParam(PARENT_GROUPNAME_PARAM, GROUP2).getURI(), response.getLocation());
    }

    /**
     * Tests adding a direct parent group membership with non-existing group.
     */
    public void testAddDirectParentGroup_NoGroup()
    {
        //group/parent-group/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, "does-not-exist");

        GroupEntity group = GroupEntity.newMinimalGroupEntity(GROUP2, null, getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI());

        ClientResponse response = webResource.entity(group, MT).post(ClientResponse.class);
        assertError(response, 404, ErrorEntity.ErrorReason.GROUP_NOT_FOUND);
    }

    /**
     * Tests adding a direct parent group membership with non-existing parent group.
     */
    public void testAddDirectParentGroup_NoParent()
    {
        //group/parent-group/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP1);

        GroupEntity group = GroupEntity.newMinimalGroupEntity("does-not-exist", null, getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI());

        ClientResponse response = webResource.entity(group, MT).post(ClientResponse.class);
        assertError(response, 400, ErrorEntity.ErrorReason.GROUP_NOT_FOUND);
    }

    /**
     * Tests get user group memberships.
     */
    public void testGetNestedParentGroups()
    {
        //group/parent-group/nested?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP1);

        GroupEntityList groups = webResource.get(GroupEntityList.class);

        assertEquals(3, groups.size());
        assertContainsName(groups, GROUP2);
        assertContainsName(groups, GROUP3);
        assertContainsName(groups, GROUP4);
        assertNotExpanded(groups);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                        path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP3);

        groups = webResource.get(GroupEntityList.class);

        assertEquals(1, groups.size());
        assertContainsName(groups, GROUP2);
        assertNotExpanded(groups);
    }

    /**
     * Tests get user group memberships.
     */
    public void testGetNestedParentGroups_Expanded()
    {
        //group/parent-group/nested?groupname=<groupname>&expand=group
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP1).queryParam("expand", "group");

        GroupEntityList groups = webResource.get(GroupEntityList.class);

        assertEquals(3, groups.size());
        assertContainsName(groups, GROUP2);
        assertContainsName(groups, GROUP3);
        assertContainsName(groups, GROUP4);
        assertExpanded(groups);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                        path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP3).queryParam("expand", "group");

        groups = webResource.get(GroupEntityList.class);

        assertEquals(1, groups.size());
        assertContainsName(groups, GROUP2);
        assertExpanded(groups);
    }

    public void testGetNestedParentGroups_Group()
    {
        //group/parent-group/nested?groupname=<groupname>&parent-groupname=<parentgroupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP1).queryParam(PARENT_GROUPNAME_PARAM, GROUP2);

        GroupEntity group = webResource.get(GroupEntity.class);

        assertEquals(GROUP2, group.getName());
    }

    public void testGetNestedParentGroups_NoGroup()
    {
        //group/parent-group/nested?groupname=<groupname>&parent-groupname=<parentgroupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(PARENT_GROUP_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP4).queryParam(PARENT_GROUPNAME_PARAM, GROUP2);

        ClientResponse response = webResource.get(ClientResponse.class);
        assertError(response, 404, ErrorEntity.ErrorReason.MEMBERSHIP_NOT_FOUND);
    }

    /**
     * Tests get group members of a group.
     */
    public void testGetDirectChildGroups()
    {
        //group/child-group/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP2);

        GroupEntityList groups = webResource.get(GroupEntityList.class);

        assertEquals(1, groups.size());
        assertContainsName(groups, GROUP3);
        assertNotExpanded(groups);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP4);

        groups = webResource.get(GroupEntityList.class);

        assertEquals(1, groups.size());
        assertContainsName(groups, GROUP1);
        assertNotExpanded(groups);
    }

    /**
     * Tests get group members of a group.
     */
    public void testGetDirectChildGroups_Expanded()
    {
        //group/child-group/direct?groupname=<groupname>&expand=group
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP2).queryParam("expand", "group");

        GroupEntityList groups = webResource.get(GroupEntityList.class);

        assertEquals(1, groups.size());
        assertContainsName(groups, GROUP3);
        assertExpanded(groups);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP4).queryParam("expand", "group");

        groups = webResource.get(GroupEntityList.class);

        assertEquals(1, groups.size());
        assertContainsName(groups, GROUP1);
        assertExpanded(groups);
    }

    /**
     * Tests adding a direct child group membership.
     */
    public void testAddDirectChildGroup()
    {
        intendToModifyData();

        //group/child-group/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP2);

        GroupEntity group = GroupEntity.newMinimalGroupEntity(GROUP1, null, getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI());

        ClientResponse response = webResource.entity(group, MT).post(ClientResponse.class);

        assertEquals(201, response.getStatus());
        assertEquals(webResource.queryParam(CHILD_GROUPNAME_PARAM, GROUP1).getURI(), response.getLocation());
    }

    /**
     * Tests adding a direct child group membership with non-existing group.
     */
    public void testAddDirectChildGroup_NoGroup()
    {
        //group/child-group/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, "does-not-exist");

        GroupEntity group = GroupEntity.newMinimalGroupEntity(GROUP1, null, getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI());

        ClientResponse response = webResource.entity(group, MT).post(ClientResponse.class);
        assertError(response, 404, ErrorEntity.ErrorReason.GROUP_NOT_FOUND);
    }

    /**
     * Tests adding a direct child group membership with non-existing child group.
     */
    public void testAddDirectChildGroup_NoChild()
    {
        //group/child-group/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP2);

        GroupEntity group = GroupEntity.newMinimalGroupEntity("does-not-exist", null, getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI());

        ClientResponse response = webResource.entity(group, MT).post(ClientResponse.class);
        assertError(response, 400, ErrorEntity.ErrorReason.GROUP_NOT_FOUND);
    }

    /**
     * Tests adding a direct child group membership with nested groups disabled.
     */
    public void testAddDirectChildGroup_NoNestedGroups()
    {
        //group/child-group/direct?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP5);

        GroupEntity group = GroupEntity.newMinimalGroupEntity(GROUP6, null, getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).getURI());

        ClientResponse response = webResource.entity(group, MT).post(ClientResponse.class);
        // 403 Does not contain ErrorEntity
        assertEquals(403, response.getStatus());
    }

    /**
     * Tests retrieving the group that is a direct child of the specified group.
     */
    public void testGetDirectChildGroups_Group()
    {
        //group/child-group/direct?groupname=<groupname>&child-groupname=<childgroupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP2).queryParam(CHILD_GROUPNAME_PARAM, GROUP3);

        GroupEntity group = webResource.get(GroupEntity.class);

        assertEquals(GROUP3, group.getName());
    }

    /**
     * Tests deleting a child group membership.
     */
    public void testDeleteDirectChildGroup()
    {
        intendToModifyData();

        //group/child-group/direct?groupname=<groupname>&child-groupname=<childgroupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP2).queryParam(CHILD_GROUPNAME_PARAM, GROUP3);

        ClientResponse response = webResource.delete(ClientResponse.class);

        assertEquals(204, response.getStatus());
    }

    /**
     * Tests deleting a child group membership when the parent does not exist.
     */
    public void testDeleteDirectChildGroup_NoParent()
    {
        //group/child-group/direct?groupname=<groupname>&child-groupname=<childgroupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, "does-not-exist").queryParam(CHILD_GROUPNAME_PARAM, GROUP3);

        ClientResponse response = webResource.delete(ClientResponse.class);
        assertError(response, 404, ErrorEntity.ErrorReason.MEMBERSHIP_NOT_FOUND);
    }

    /**
     * Tests deleting a child group membership when the child does not exist.
     */
    public void testDeleteDirectChildGroup_NoChild()
    {
        //group/child-group/direct?groupname=<groupname>&child-groupname=<childgroupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(DIRECT_PATH).queryParam(GROUPNAME_PARAM, GROUP2).queryParam(CHILD_GROUPNAME_PARAM, "does-not-exist");

        ClientResponse response = webResource.delete(ClientResponse.class);
        assertError(response, 404, ErrorEntity.ErrorReason.MEMBERSHIP_NOT_FOUND);
    }

    /**
     * Tests get group members of a group.
     */
    public void testGetNestedChildGroups()
    {
        //group/child-group/nested?groupname=<groupname>
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP2);

        GroupEntityList groups = webResource.get(GroupEntityList.class);

        assertEquals(2, groups.size());
        assertContainsName(groups, GROUP3);
        assertContainsName(groups, GROUP1);
        assertNotExpanded(groups);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP4);

        groups = webResource.get(GroupEntityList.class);
        assertEquals(1, groups.size());
        assertContainsName(groups, GROUP1);
        assertNotExpanded(groups);
    }

    /**
     * Tests get group members of a group.
     */
    public void testGetNestedChildGroups_Expanded()
    {
        //group/child-group/nested?groupname=<groupname>&expand=groups
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP2).queryParam("expand", "group");

        GroupEntityList groups = webResource.get(GroupEntityList.class);

        assertEquals(2, groups.size());
        assertContainsName(groups, GROUP3);
        assertContainsName(groups, GROUP1);
        assertExpanded(groups);

        webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(CHILD_GROUP_PATH).path(NESTED_PATH).queryParam(GROUPNAME_PARAM, GROUP4).queryParam("expand", "group");

        groups = webResource.get(GroupEntityList.class);
        assertEquals(1, groups.size());
        assertContainsName(groups, GROUP1);
        assertExpanded(groups);
    }

    private static void assertContainsName(UserEntityList expectedList, String name)
    {
        assertThat(namesOf(expectedList), hasItem(name));
    }

    private static void assertContainsEmail(UserEntityList expectedList, String email)
    {
        for (UserEntity entity : expectedList)
        {
            if (email.equals(entity.getEmail()))
            {
                return;
            }
        }
        fail("expectedList does not contain: " + "'email'");
    }

    private static void assertContainsName(GroupEntityList expectedList, String name)
    {
        assertThat(namesOf(expectedList), hasItem(name));
    }

    private static void assertExpanded(GroupEntityList expectedList)
    {
        for (GroupEntity entity : expectedList)
        {
            if (entity.getType() == null && entity.getDescription() == null)
            {
                fail("Group " + entity.getName() + " is not expanded");
            }
        }
    }

    private static void assertNotExpanded(GroupEntityList expectedList)
    {
        for (GroupEntity entity : expectedList)
        {
            if (entity.getType() != null || entity.getDescription() != null)
            {
                fail("Group " + entity.getName() + " is expanded");
            }
        }
    }

    private void assertError(ClientResponse response, int statusCode, ErrorEntity.ErrorReason reason)
    {
        assertEquals(statusCode, response.getStatus());
        final ErrorEntity error = response.getEntity(ErrorEntity.class);
        assertEquals(reason, error.getReason());
    }

    static Set<String> namesOf(Iterable<? extends NamedEntity> list)
    {
        Set<String> names = new HashSet<String>();

        for (NamedEntity e : list)
        {
            names.add(e.getName());
        }

        return names;
    }

    public void testGetMembershipsReturnsExactlyExpectedMemberships()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(GROUPS_RESOURCE).path(MEMBERSHIP_PATH);

        MembershipsEntity mems = webResource.get(MembershipsEntity.class);

        assertNotNull(mems.getList());

        Map<String, Set<String>> users = new HashMap<String, Set<String>>();
        Map<String, Set<String>> childGroups = new HashMap<String, Set<String>>();

        for (MembershipEntity e : mems.getList())
        {
            users.put(e.getName(), namesOf(e.getUsers()));
            childGroups.put(e.getName(), namesOf(e.getGroups()));
        }

        Set<String> E = Collections.emptySet();

        Map<String, Set<String>> expectedUsers = new HashMap<String, Set<String>>();
        expectedUsers.put("animals", E);
        expectedUsers.put("badgers", ImmutableSet.of("admin", "eeeep"));
        expectedUsers.put("birds", E);
        expectedUsers.put("cats", E);
        expectedUsers.put("crowd-administrators", ImmutableSet.of("admin", "secondadmin"));
        expectedUsers.put("crowd-testers", ImmutableSet.of("penny"));
        expectedUsers.put("crowd-users", E);
        expectedUsers.put("dogs", E);

        Map<String, Set<String>> expectedChildGroups = new HashMap<String, Set<String>>();
        expectedChildGroups.put("animals", E);
        expectedChildGroups.put("badgers", E);
        expectedChildGroups.put("birds", E);
        expectedChildGroups.put("cats", E);
        expectedChildGroups.put("crowd-administrators", ImmutableSet.of("crowd-testers"));
        expectedChildGroups.put("crowd-testers", ImmutableSet.of("badgers"));
        expectedChildGroups.put("crowd-users", ImmutableSet.of("badgers"));
        expectedChildGroups.put("dogs", E);

        assertEquals(expectedUsers, users);
        assertEquals(expectedChildGroups, childGroups);
    }

    /**
     * This resource is optimised for XML (it streams its response), but does not currently support JSON. The
     * tests asserts that requesting JSON results in an explicit error. Eventually, this resource may support
     * JSON responses as well, and this test method won't be necessary anymore.
     */
    public void testGetMembershipsDoesNotReturnJson()
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
            path(GROUPS_RESOURCE).path(MEMBERSHIP_PATH);

        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON_TYPE).get(ClientResponse.class);

        assertEquals(ClientResponse.Status.NOT_ACCEPTABLE, response.getClientResponseStatus());
    }
}