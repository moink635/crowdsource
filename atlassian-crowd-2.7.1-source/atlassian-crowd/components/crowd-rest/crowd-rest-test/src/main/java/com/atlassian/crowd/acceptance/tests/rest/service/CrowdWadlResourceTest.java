package com.atlassian.crowd.acceptance.tests.rest.service;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.acceptance.tests.rest.RestServerImpl;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.ws.rs.core.UriBuilder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

public class CrowdWadlResourceTest extends RestCrowdServiceAcceptanceTestCase
{
    private static final String WADL_RESOURCE = "application.wadl";
    private static final String ADMIN_USER_NAME = "admin";
    private static final String ADMIN_PASSWORD = "admin";
    private RestServer restServer = RestServerImpl.INSTANCE;

    public CrowdWadlResourceTest(String name)
    {
        super(name);
    }

    public CrowdWadlResourceTest(String name, RestServer restServer)
    {
        super(name);
        this.restServer = restServer;
    }

    @Override
    public void setUp() throws Exception
    {
        restServer.before();
    }

    public void testShouldGetAWadlRepresentationForUserManagement() throws IOException, SAXException, ParserConfigurationException
    {
        WebResource webResource = getRootWebResource(APPLICATION_NAME, APPLICATION_PASSWORD).
                path(WADL_RESOURCE);

        ClientResponse response = webResource.get(ClientResponse.class);
        assertEquals(200, response.getStatus()); // before the fix, application.wadl was returning a 401 unauthorized exception.

        String wadlRepresentation = webResource.get(String.class);
        assertNotNull(wadlRepresentation);

        Document document = parseXMLDocument(wadlRepresentation);
        assertNotNull(document);

        Element documentElement = document.getDocumentElement();
        assertThat(documentElement.getNodeName(), containsString("application"));
    }

    public void testShouldGetAWadlRepresentationForAppManagement() throws IOException, SAXException, ParserConfigurationException
    {
        UriBuilder path = getBaseUriBuilder("appmanagement").path("application.wadl");
        WebResource webResource = getWebResource(ADMIN_USER_NAME, ADMIN_PASSWORD, path.build());

        ClientResponse response = webResource.get(ClientResponse.class);
        assertEquals(200, response.getStatus()); // before the fix, application.wadl was returning a 401 unauthorized exception.

        String wadlRepresentation = webResource.get(String.class);
        assertNotNull(wadlRepresentation);

        Document document = parseXMLDocument(wadlRepresentation);
        assertNotNull(document);

        Element documentElement = document.getDocumentElement();
        assertThat(documentElement.getNodeName(), containsString("application"));
    }


    private Document parseXMLDocument(String wadlRepresentation) throws IOException, SAXException, ParserConfigurationException
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
        return documentBuilder.parse(new InputSource(new StringReader(wadlRepresentation)));
    }
}
