package com.atlassian.crowd.acceptance.tests.rest.service;

import java.net.HttpURLConnection;
import java.net.URI;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.integration.rest.entity.EventEntityList;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

public class EventsResourceTest extends RestCrowdServiceAcceptanceTestCase
{
    public EventsResourceTest(String name)
    {
        super(name);
    }

    public EventsResourceTest(String name, RestServer restServer)
    {
        super(name, restServer);
    }

    public void testGetEventToken()
    {
        EventEntityList events = requestEventToken();

        assertThat(events.getNewEventToken(), notNullValue());
        assertThat(events.getEvents(), nullValue());
    }

    public void testGetEventsWithBadEventToken()
    {
        final URI uri = getBaseUriBuilder().path(EVENTS_RESOURCE).path("bad-event-token").build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        ClientResponse response = webResource.get(ClientResponse.class);

        assertThat(response.getStatus(), is(HttpURLConnection.HTTP_GONE));
    }

    public void testGetNoEventsWithGoodEventToken()
    {
        String firstEventToken = requestEventToken().getNewEventToken();

        EventEntityList events = requestEvents(firstEventToken);

        assertThat(events.getEvents(), nullValue()); // empty list is returned as null
        assertThat(events.getNewEventToken(), notNullValue());
        assertThat(events.getNewEventToken(), is(firstEventToken)); // no events, no token change
    }

    public void testGetEvents()
    {
        intendToModifyData();

        String firstEventToken = requestEventToken().getNewEventToken();

        // generate an event
        removeUser();

        EventEntityList events = requestEvents(firstEventToken);

        assertThat(events.getEvents(), hasSize(1));
        assertThat(events.getNewEventToken(), notNullValue());
        assertThat(events.getNewEventToken(), not(firstEventToken));
    }

    private void removeUser()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).queryParam("username", "eeeep").build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);
        webResource.delete();
    }

    private EventEntityList requestEventToken()
    {
        final URI uri = getBaseUriBuilder().path(EVENTS_RESOURCE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        return webResource.get(EventEntityList.class);
    }

    private EventEntityList requestEvents(String eventToken)
    {
        final URI uri = getBaseUriBuilder().path(EVENTS_RESOURCE).path(eventToken).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        return webResource.get(EventEntityList.class);
    }
}
