package com.atlassian.crowd.acceptance.tests.rest.service;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.integration.rest.entity.AbstractEventEntity;
import com.atlassian.crowd.integration.rest.entity.EventEntityList;
import com.atlassian.crowd.integration.rest.entity.UserEventEntity;
import com.atlassian.crowd.plugin.rest.entity.WebhookEntity;

import com.google.common.util.concurrent.SettableFuture;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import org.apache.commons.httpclient.HttpStatus;
import org.hamcrest.core.IsCollectionContaining;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.IsInstanceOf.instanceOf;
import static org.junit.Assert.assertThat;

public class WebhooksResourceTest extends RestCrowdServiceAcceptanceTestCase
{
    private static final Logger logger = LoggerFactory.getLogger(WebhooksResourceTest.class);

    public WebhooksResourceTest(String name)
    {
        super(name);
    }

    public WebhooksResourceTest(String name, RestServer restServer)
    {
        super(name, restServer);
    }

    public void testGetNonExistingWebhook() throws Exception
    {
        URI uri = getBaseUriBuilder().path(WEBHOOKS_RESOURCE).path("not-a-webhook-id").build();
        WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        try
        {
            webResource.get(WebhookEntity.class);
            fail("Exception expected");
        }
        catch (UniformInterfaceException e)
        {
            // good, check the status code
            assertEquals(404, e.getResponse().getStatus());
        }
    }

    public void testRegisterAndGetWebhook() throws Exception
    {
        intendToModifyData();

        ClientResponse registrationResponse = registerWebhook("http://example.test/", "secret");

        assertEquals(201, registrationResponse.getStatus());
        assertTrue(registrationResponse.getHeaders().containsKey("Location"));
        WebhookEntity registrationResponseEntity = registrationResponse.getEntity(WebhookEntity.class);
        assertNotNull(registrationResponseEntity.getId());
        assertEquals("http://example.test/", registrationResponseEntity.getEndpointUrl());

        URI webhookResourceUri = new URI(registrationResponse.getHeaders().getFirst("Location"));

        WebResource webhookResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, webhookResourceUri);

        WebhookEntity webhookEntity = webhookResource.get(WebhookEntity.class);

        assertTrue(webhookResourceUri.getPath().endsWith("/" + webhookEntity.getId()));
        assertEquals("http://example.test/", webhookEntity.getEndpointUrl());
    }

    public void testRegisterWebhookWithInvalidEndpointUrlShouldFail() throws Exception
    {
        ClientResponse registrationResponse = registerWebhook("ftp://example.test/", "secret");

        assertEquals(400, registrationResponse.getStatus()); // 400 = Bad request
    }

    public void testRegisterWebhookWithSimpleBody() throws Exception
    {
        intendToModifyData();

        final URI uri = getBaseUriBuilder().path(WEBHOOKS_RESOURCE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        ClientResponse registrationResponse =
            webResource.entity("http://example.test/", MediaType.TEXT_PLAIN_TYPE).post(ClientResponse.class);

        assertEquals(201, registrationResponse.getStatus());
        assertTrue(registrationResponse.getHeaders().containsKey("Location"));
    }

    public void testRegisterAndUnregisterWebhook() throws Exception
    {
        intendToModifyData();

        ClientResponse registrationResponse = registerWebhook("http://example.test/", "secret");

        assertTrue(registrationResponse.getHeaders().containsKey("Location"));

        URI webhookResourceUri = new URI(registrationResponse.getHeaders().getFirst("Location"));

        unregisterWebhook(webhookResourceUri);
    }

    public void testUnregisterNonExistingWebhook() throws Exception
    {
        URI uri = getBaseUriBuilder().path(WEBHOOKS_RESOURCE).path("not-a-webhook-id").build();

        try
        {
            unregisterWebhook(uri);
            fail("Exception expected");
        }
        catch (UniformInterfaceException e)
        {
            // good, check the status code
            assertEquals(404, e.getResponse().getStatus());
        }
    }

    public void testWebhookFullCycle() throws Exception
    {
        intendToModifyData();

        final SettableFuture<List<AbstractEventEntity>> futureEvents = SettableFuture.create();
        final String eventToken = requestEventToken().getNewEventToken();

        InetSocketAddress addr = new InetSocketAddress(0);
        HttpServer server = HttpServer.create(addr, 0);
        server.setExecutor(Executors.newCachedThreadPool());

        server.createContext("/myapp", new HttpHandler()
        {
            @Override
            public void handle(HttpExchange httpExchange) throws IOException
            {
                logger.info("Received Webhook callback!");
                assertThat(httpExchange.getRequestHeaders().get("Authorization"), hasItem("Basic secret"));
                try
                {
                    List<AbstractEventEntity> events = requestEvents(eventToken).getEvents();
                    futureEvents.set(events);
                    httpExchange.sendResponseHeaders(HttpStatus.SC_NO_CONTENT, 0);
                }
                catch (Exception e)
                {
                    futureEvents.cancel(false);
                    throw new RuntimeException(e);
                }
                finally
                {
                    httpExchange.close();
                }
            }
        });
        String webhookUrl = "http://localhost:" + server.getAddress().getPort() + "/myapp/mywebhook";

        try
        {
            server.start();
            ClientResponse registerWebhookResponse = registerWebhook(webhookUrl, "secret");

            removeUser(); // trigger an event

            unregisterWebhook(registerWebhookResponse.getLocation());

            List<AbstractEventEntity> events = futureEvents.get(10, TimeUnit.SECONDS);

            assertThat(events, IsCollectionContaining.<AbstractEventEntity>hasItem(instanceOf(UserEventEntity.class)));
        }
        finally
        {
            server.stop(0);
        }
    }

    private ClientResponse registerWebhook(String endpointUrl, String password)
    {
        final URI uri = getBaseUriBuilder().path(WEBHOOKS_RESOURCE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        WebhookEntity webhookTemplate = new WebhookEntity(endpointUrl, password);

        return webResource.entity(webhookTemplate, MT).post(ClientResponse.class);
    }

    private void unregisterWebhook(URI webhookResourceUri)
    {
        WebResource webhookResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, webhookResourceUri);

        webhookResource.delete();
    }

    private EventEntityList requestEventToken()
    {
        final URI uri = getBaseUriBuilder().path(EVENTS_RESOURCE).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        return webResource.get(EventEntityList.class);
    }

    private EventEntityList requestEvents(String eventToken)
    {
        final URI uri = getBaseUriBuilder().path(EVENTS_RESOURCE).path(eventToken).build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);

        return webResource.get(EventEntityList.class);
    }

    private void removeUser()
    {
        final URI uri = getBaseUriBuilder().path(USERS_RESOURCE).queryParam("username", "eeeep").build();
        final WebResource webResource = getWebResource(APPLICATION_NAME, APPLICATION_PASSWORD, uri);
        webResource.delete();
    }
}
