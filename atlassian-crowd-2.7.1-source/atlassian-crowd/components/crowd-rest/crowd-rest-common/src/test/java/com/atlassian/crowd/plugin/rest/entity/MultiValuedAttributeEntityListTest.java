package com.atlassian.crowd.plugin.rest.entity;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.net.URISyntaxException;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import com.atlassian.plugins.rest.common.json.DefaultJaxbJsonMarshaller;
import com.atlassian.plugins.rest.common.json.JacksonJsonProviderFactory;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class MultiValuedAttributeEntityListTest
{
    @Test
    public void xmlSyntaxIsAsExpected() throws URISyntaxException, IOException, JAXBException
    {
        MultiValuedAttributeEntity attr = new MultiValuedAttributeEntity("attr-name", ImmutableList.of("attr-value"), null);
        MultiValuedAttributeEntityList l = new MultiValuedAttributeEntityList(ImmutableList.of(attr), null);

        JAXBContext jc = JAXBContext.newInstance(MultiValuedAttributeEntityList.class);

        StringWriter sw = new StringWriter();

        jc.createMarshaller().marshal(l, sw);

        assertThat(sw.toString(), Matchers.endsWith("<attributes><attribute name=\"attr-name\"><values><value>attr-value</value></values></attribute></attributes>"));
    }


    @Test
    public void jsonSerialisationHasNoUnnecessaryNesting() throws URISyntaxException
    {
        MultiValuedAttributeEntity attr = new MultiValuedAttributeEntity("attr-name", ImmutableList.of("attr-value"), null);
        MultiValuedAttributeEntityList l = new MultiValuedAttributeEntityList(ImmutableList.of(attr), null);

        DefaultJaxbJsonMarshaller x = new DefaultJaxbJsonMarshaller();

        String m = x.marshal(l);

        assertEquals("{\"attributes\":[{\"name\":\"attr-name\",\"values\":[\"attr-value\"]}]}", m);
    }

    @Test
    public void attributeValuesCanBeParsedFromString() throws URISyntaxException, IOException
    {
        String entity = "{\"attributes\": [{\"name\": \"attr-name\", \"values\": [\"attr-value\"]}]}";

        InputStream entityStream = new ByteArrayInputStream(entity.getBytes(Charsets.US_ASCII));

        JacksonJsonProvider jpf = new JacksonJsonProviderFactory().create();

        Class<Object> type = (Class) MultiValuedAttributeEntityList.class;
        Type genericType = type;
        Annotation[] annotations = null;
        MediaType mediaType = MediaType.APPLICATION_JSON_TYPE;
        MultivaluedMap<String, String> httpHeaders = null;
        Object o = jpf.readFrom(type, genericType, annotations, mediaType, httpHeaders, entityStream);

        MultiValuedAttributeEntityList l = (MultiValuedAttributeEntityList) o;

        MultiValuedAttributeEntity attr = Iterables.getOnlyElement(l);

        assertEquals("attr-name", attr.getName());
        assertEquals("attr-value", attr.getValue());
    }
}
