package com.atlassian.crowd.plugin.rest.filter;

import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.sal.api.ApplicationProperties;

import com.google.common.collect.ImmutableList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RestServiceVersionFilterTest
{
    @Mock
    private FilterConfig filterConfig;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @Test
    public void canBeInitialisedFromApplicationProperties()
    {
        ApplicationProperties applicationProperties = mock(ApplicationProperties.class);
        when(applicationProperties.getDisplayName()).thenReturn("displayname");
        when(applicationProperties.getVersion()).thenReturn("version");

        RestServiceVersionFilter filter = new RestServiceVersionFilter(applicationProperties);

        assertEquals("displayname", filter.getDisplayName());
        assertEquals("version", filter.getVersion());
    }

    @Test
    public void canBeInitialisedDirectly()
    {
        RestServiceVersionFilter filter = new RestServiceVersionFilter("displayname", "version");

        assertEquals("displayname", filter.getDisplayName());
        assertEquals("version", filter.getVersion());
    }

    @Test
    public void addsEmbeddedCrowdVersionHeader() throws Exception
    {
        when(filterConfig.getInitParameterNames()).thenReturn(Collections.enumeration(ImmutableList.of()));

        RestServiceVersionFilter filter = new RestServiceVersionFilter("displayname", "version");

        filter.init(filterConfig);

        filter.doFilter(request, response, filterChain);

        verify(response).setHeader("X-Embedded-Crowd-Version", "displayname/version");
    }

    @Test
    public void addsExtraHeadersPassedAsParameters() throws Exception
    {
        when(filterConfig.getInitParameterNames()).thenReturn(Collections.enumeration(ImmutableList.of("name1")));
        when(filterConfig.getInitParameter("name1")).thenReturn("value1");

        RestServiceVersionFilter filter = new RestServiceVersionFilter("displayname", "version");

        filter.init(filterConfig);

        filter.doFilter(request, response, filterChain);

        verify(response).setHeader("name1", "value1");
    }
}
