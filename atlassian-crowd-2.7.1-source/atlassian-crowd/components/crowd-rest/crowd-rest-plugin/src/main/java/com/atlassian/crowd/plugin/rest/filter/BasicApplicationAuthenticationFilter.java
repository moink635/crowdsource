package com.atlassian.crowd.plugin.rest.filter;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.manager.validation.ClientValidationException;
import com.atlassian.crowd.manager.validation.ClientValidationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.authentication.ApplicationAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.plugin.rest.service.util.AuthenticatedApplicationUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

import javax.annotation.Nullable;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

/**
 * Implementation of HTTP Basic Authentication such that all invocations
 * to the filter must be authenticated with a valid application name
 * and corresponding password.
 *
 * As a performance enhancement the application name is saved in the
 * session after a successful authentication. Password check is waived
 * with consequent requests when the application name in the request
 * matches the application name in the session. Clients wishing to take
 * advantage of this feature must support cookies.
 */
public class BasicApplicationAuthenticationFilter extends AbstractBasicAuthenticationFilter
{
    private static final String APPLICATION_AUTHENTICATION_ERROR_MSG = "Application failed to authenticate";
    private static final Logger LOG = LoggerFactory.getLogger(BasicApplicationAuthenticationFilter.class);
    private final ApplicationManager applicationManager;
    private final ClientValidationManager clientValidationManager;
    private final TokenAuthenticationManager tokenAuthenticationManager;

    public BasicApplicationAuthenticationFilter(ApplicationManager applicationManager,
                                                ClientValidationManager clientValidationManager,
                                                TokenAuthenticationManager tokenAuthenticationManager)
    {
        this.applicationManager = applicationManager;
        this.clientValidationManager = clientValidationManager;
        this.tokenAuthenticationManager = tokenAuthenticationManager;
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException
    {
        ensureSeraphForwardsRequest(servletRequest);
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        final HttpServletResponse response = (HttpServletResponse) servletResponse;

        final Credentials credentials = getBasicAuthCredentials(request);

        if (credentials == null)
        {
            LOG.debug("No basic auth credentials found in request, responding with authentication challenge");
            respondWithChallenge(response);
        }
        else
        {
            try
            {
                Application application = applicationManager.findByName(credentials.getName());
                clientValidationManager.validate(application, request);
                if (isAuthenticated(request, credentials))
                {
                    LOG.debug("Application '{}' is already authenticated", credentials.getName());
                    chain.doFilter(request, servletResponse);
                }
                else
                {
                    Token token = authenticate(application, credentials.getPassword());
                    LOG.debug("Application '{}' authenticated successfully", credentials.getName());
                    setAuthenticatedEntity(request, credentials.getName());
                    if (token != null)
                    {
                        HttpSessionAwareToken tokenThatExpires = new HttpSessionAwareToken(token);
                        AuthenticatedApplicationUtil.setAuthenticatedApplicationToken(request, tokenThatExpires);
                    }
                    chain.doFilter(request, servletResponse);
                }
            }
            catch (ApplicationNotFoundException e)
            {
                LOG.info("Application '{}' failed authentication", credentials.getName());
                respondWithChallenge(response);
            }
            catch (InvalidAuthenticationException e)
            {
                LOG.info("Invalid authentication for application with name '{}'", credentials.getName());
                respondWithChallenge(response);
            }
            catch (ClientValidationException e)
            {
                response.sendError(HttpServletResponse.SC_FORBIDDEN, e.getMessage());
            }
        }
    }

    /**
     * Authenticates the application.
     *
     * @param application application to be authenticated
     * @param password password
     * @return a Token if the host application supports token-based authentication, or <code>null</code> if
     * the host application does not support token-based authentication, but the application can be authenticated
     * anyway
     * @throws InvalidAuthenticationException if the application fails to authenticate
     * @throws ApplicationNotFoundException if the application cannot be found
     * @throws ClientValidationException if there is some other problem
     */
    @Nullable
    private Token authenticate(Application application, String password)
        throws ClientValidationException, InvalidAuthenticationException, ApplicationNotFoundException
    {
        ApplicationAuthenticationContext authenticationContext = new ApplicationAuthenticationContext(
            application.getName(), PasswordCredential.unencrypted(password), new ValidationFactor[0]);
        Token token = tokenAuthenticationManager.authenticateApplication(authenticationContext);

        // host applications (JIRA) may not support token-based authentication. They have a No-op implementation
        // of tokenAuthenticationManager which returns a 'null' token. In that case, fallback to
        // password-based authentication
        if (token == null && !applicationManager.authenticate(application, PasswordCredential.unencrypted(password)))
        {
            throw InvalidAuthenticationException.newInstanceWithName(application.getName());
        }

        return token;
    }

    /**
     * Decorates the parent behaviour to check also the validity of the token
     *
     * @param request HTTP servlet request possibly containing a HttpSession
     * @param credentials credentials sent with the request
     * @return <code>true</code> is the application is authenticated
     */
    @Override
    protected boolean isAuthenticated(HttpServletRequest request, Credentials credentials)
    {
        Token token = AuthenticatedApplicationUtil.getAuthenticatedApplicationToken(request);

        if (token != null)
        {
            // validate the token only if it exists
            try
            {
                tokenAuthenticationManager.validateApplicationToken(token.getRandomHash(), new ValidationFactor[0]);
            }
            catch (InvalidTokenException e)
            {
                return false;
            }
        }

        return super.isAuthenticated(request, credentials);

    }

    /**
     * Returns the authenticated entity from the <code>request</code>, or <tt>null</tt> if there is no authenticated entity.
     *
     * @param request Request
     * @return authenticated entity from the <code>request</code>, or <tt>null</tt> if there is no authenticated entity.
     */
    @Override
    protected String getAuthenticatedEntity(final HttpServletRequest request)
    {
        return AuthenticatedApplicationUtil.getAuthenticatedApplication(request);
    }

    /**
     * Sets the authenticated entity.
     *
     * @param request Request
     * @param name the name of the authenticated entity
     */
    @Override
    protected void setAuthenticatedEntity(final HttpServletRequest request, final String name)
    {
        AuthenticatedApplicationUtil.setAuthenticatedApplication(request, name);
    }

    @Override
    protected String getEntityAttributeKey()
    {
        return AuthenticatedApplicationUtil.APPLICATION_NAME_ATTRIBUTE_KEY;
    }

    @Override
    protected String getAuthenticationErrorMessage()
    {
        return APPLICATION_AUTHENTICATION_ERROR_MSG;
    }

    @Override
    protected String getBasicRealm()
    {
        return "Crowd REST Service";
    }

    /**
     * Specialises {@link Token} to invalidate the Token when it is unbound from the
     * {@link javax.servlet.http.HttpSession} (typically because the HTTP session expires).
     */
    private class HttpSessionAwareToken extends Token implements HttpSessionBindingListener
    {
        public HttpSessionAwareToken(Token token)
        {
            super(token);
        }

        @Override
        public void valueBound(HttpSessionBindingEvent event)
        {
            // nothing to do
        }

        @Override
        public void valueUnbound(HttpSessionBindingEvent event)
        {
            LOG.debug("Invalidating application token due to REST HTTP session expiration: " + getRandomHash());
            tokenAuthenticationManager.invalidateToken(getRandomHash());
        }
    }
}
