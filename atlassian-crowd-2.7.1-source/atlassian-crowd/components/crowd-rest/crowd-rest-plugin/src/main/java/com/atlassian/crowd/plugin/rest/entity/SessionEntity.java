package com.atlassian.crowd.plugin.rest.entity;

import java.util.Date;

import com.atlassian.plugins.rest.common.Link;
import com.atlassian.plugins.rest.common.expand.Expandable;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.xml.bind.annotation.*;

/**
 * Represents a Session entity.
 *
 * @since v2.1
 */
@XmlRootElement (name = "session")
@XmlAccessorType(XmlAccessType.FIELD)
public class SessionEntity
{
    @SuppressWarnings("unused")
    @XmlAttribute
    private String expand;

    @XmlElement (name = "token")
    private String token;

    @Expandable
    @XmlElement(name = "user")
    private UserEntity user;

    @XmlElement (name = "link")
    private Link link;

    @XmlElement (name = "created-date")
    private Date createdDate;

    @XmlElement (name = "expiry-date")
    private Date expiryDate;

    @XmlElement (name = "unaliased-username")
    private String unaliasedUsername;

    /**
     * JAXB requires a no-arg constructor.
     */
    private SessionEntity()
    {
    }

    public SessionEntity(final String token, final UserEntity user, final Link link, final Date createdDate,
                final Date expiryDate)
    {
        this(token, user, link, createdDate, expiryDate, null);
    }

    public SessionEntity(final String token, final UserEntity user, final Link link, final Date createdDate,
                         final Date expiryDate, String unaliasedUsername)
    {
        this.token = token;
        this.user = user;
        this.link = link;
        this.createdDate = createdDate;
        this.expiryDate = expiryDate;
        this.unaliasedUsername = unaliasedUsername;
    }

    public String getToken()
    {
        return token;
    }

    public UserEntity getUser()
    {
        return user;
    }

    public Link getLink()
    {
        return link;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public Date getExpiryDate()
    {
        return expiryDate;
    }

    public String getUnaliasedUsername()
    {
        return unaliasedUsername;
    }

    public String toString()
    {
        return new ToStringBuilder(this).
                append("token", getToken()).
                append("user", getUser()).
                append("created-date", getCreatedDate()).
                append("expiry-date", getExpiryDate()).
                append("unaliased-username", unaliasedUsername).
                toString();
    }
}
