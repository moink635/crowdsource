package com.atlassian.crowd.plugin.rest.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.atlassian.crowd.manager.webhook.InvalidWebhookEndpointException;
import com.atlassian.crowd.plugin.rest.entity.ErrorEntity;

/**
 * @since v2.7
 */
@Provider
public class InvalidWebhookEndpointUrlExceptionMapper implements ExceptionMapper<InvalidWebhookEndpointException>
{
    @Override
    public Response toResponse(InvalidWebhookEndpointException exception)
    {
        return Response.status(Response.Status.BAD_REQUEST).entity(ErrorEntity.of(exception)).build();
    }
}
