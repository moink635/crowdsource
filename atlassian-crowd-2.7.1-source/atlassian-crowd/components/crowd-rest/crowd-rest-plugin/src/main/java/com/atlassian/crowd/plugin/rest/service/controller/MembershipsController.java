package com.atlassian.crowd.plugin.rest.service.controller;

import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

public class MembershipsController extends AbstractResourceController
{
    public MembershipsController(ApplicationService applicationService, ApplicationManager applicationManager)
    {
        super(applicationService, applicationManager);
    }

    public StreamingOutput searchGroups(final String applicationName)
    {
        return new OutputAsXml(applicationName);
    }

    /**
     * This method streams the output so the server doesn't need to hold all the memberships in
     * memory.
     * @throws FactoryConfigurationError
     * @throws XMLStreamException
     */
    void writeXmlToStream(String applicationName, OutputStream output) throws IOException, XMLStreamException
    {
        final Application application = getApplication(applicationName);

        XMLStreamWriter sw = XMLOutputFactory.newInstance().createXMLStreamWriter(output, "utf-8");

        sw.writeStartDocument("utf-8", "1.0");
        sw.writeCharacters("\n");

        sw.writeStartElement("memberships");
        sw.writeCharacters("\n");

        SearchRestriction searchRestriction = NullRestrictionImpl.INSTANCE;

        final EntityQuery<String> entityQuery = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).with(searchRestriction).startingAt(0).returningAtMost(EntityQuery.ALL_RESULTS);

        for (String groupName : applicationService.searchGroups(application, entityQuery))
        {
            sw.writeCharacters(" ");

            sw.writeStartElement("membership");
            sw.writeAttribute("group", groupName);
            sw.writeCharacters("\n");

            sw.writeCharacters("  ");
            sw.writeStartElement("users");
            sw.writeCharacters("\n");

            /* Direct user members */
            MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(groupName).startingAt(0).returningAtMost(EntityQuery.ALL_RESULTS);
            for (String user : applicationService.searchDirectGroupRelationships(application, query))
            {
                sw.writeCharacters("   ");
                sw.writeEmptyElement("user");
                sw.writeAttribute("name", user);
                sw.writeCharacters("\n");
            }

            sw.writeCharacters("  ");
            sw.writeEndElement();
            sw.writeCharacters("\n");

            sw.writeCharacters("  ");
            sw.writeStartElement("groups");
            sw.writeCharacters("\n");

            /* Child groups */
            MembershipQuery<String> query2 = QueryBuilder.createMembershipQuery(EntityQuery.ALL_RESULTS, 0, true, EntityDescriptor.group(), String.class, EntityDescriptor.group(), groupName);
            for (String childGroup : applicationService.searchDirectGroupRelationships(application, query2))
            {
                sw.writeCharacters("   ");
                sw.writeEmptyElement("group");
                sw.writeAttribute("name", childGroup);
                sw.writeCharacters("\n");
            }

            sw.writeCharacters("  ");
            sw.writeEndElement();
            sw.writeCharacters("\n");

            sw.writeCharacters(" ");
            sw.writeEndElement();
            sw.writeCharacters("\n");
        }

        sw.writeEndElement();
        sw.writeEndDocument();

        sw.close();
    }

    class OutputAsXml implements StreamingOutput
    {
        private final String applicationName;

        public OutputAsXml(String applicationName)
        {
            this.applicationName = applicationName;
        }

        public void write(OutputStream output) throws IOException, WebApplicationException
        {
            try
            {
                writeXmlToStream(applicationName, output);
            }
            catch (XMLStreamException e)
            {
                throw new WebApplicationException(e);
            }
        }
    }
}
