package com.atlassian.crowd.plugin.rest.service.util;

import org.apache.commons.lang3.Validate;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.atlassian.crowd.model.token.Token;

/**
 * Utility for setting and retrieving the application name and token from the {@link HttpServletRequest}.
 *
 * @since v2.1
 */
public class AuthenticatedApplicationUtil
{
    public static final String APPLICATION_NAME_ATTRIBUTE_KEY = "com.atlassian.crowd.authenticated.application.name";
    public static final String APPLICATION_TOKEN_ATTRIBUTE_KEY = "com.atlassian.crowd.authenticated.application.token";

    private AuthenticatedApplicationUtil() {}

    /**
     * Returns the application name from the {@link HttpSession}, or <tt>null</tt> if no application name was found.
     * 
     * @param request HTTP servlet request
     * @return name of the authenticated application, <tt>null</tt> if no application name was found
     */
    @Nullable
    public static String getAuthenticatedApplication(final HttpServletRequest request)
    {
        Validate.notNull(request);
        final HttpSession session = request.getSession(false);
        if (session == null)
        {
            return null;
        }
        final Object value = session.getAttribute(APPLICATION_NAME_ATTRIBUTE_KEY);
        return (value instanceof String ? (String) value : null);
    }

    /**
     * Sets the name of the authenticated application as an attribute of the {@link HttpSession}.
     *
     * @param request HTTP servlet request
     * @param applicationName name of the authenticated application
     */
    public static void setAuthenticatedApplication(final HttpServletRequest request, final String applicationName)
    {
        Validate.notNull(request);
        Validate.notNull(applicationName);
        request.getSession().setAttribute(APPLICATION_NAME_ATTRIBUTE_KEY, applicationName);
    }

    /**
     * Returns the application token from the {@link HttpSession}, or <code>null</code> if there is none
     *
     * @param request HTTP servlet request
     * @return application token of the authenticated application, or <code>null</code> if there is none
     */
    @Nullable
    public static Token getAuthenticatedApplicationToken(HttpServletRequest request)
    {
        Validate.notNull(request);
        final HttpSession session = request.getSession(false);
        if (session == null)
        {
            return null;
        }
        final Object value = session.getAttribute(APPLICATION_TOKEN_ATTRIBUTE_KEY);
        return (value instanceof Token ? (Token) value : null);
    }

    /**
     * Sets the application token of the authenticated application as an attribute of the {@link HttpSession}.
     *
     * @param request HTTP servlet request
     * @param token application token
     */
    public static void setAuthenticatedApplicationToken(HttpServletRequest request, Token token)
    {
        Validate.notNull(request);
        Validate.notNull(token);
        request.getSession().setAttribute(APPLICATION_TOKEN_ATTRIBUTE_KEY, token);
    }
}
