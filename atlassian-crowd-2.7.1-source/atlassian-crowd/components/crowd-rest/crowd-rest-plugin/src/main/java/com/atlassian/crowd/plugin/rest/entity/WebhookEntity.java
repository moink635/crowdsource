package com.atlassian.crowd.plugin.rest.entity;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.crowd.model.webhook.Webhook;

/**
 * REST version of a Webhook (server-side).
 *
 * @since 2.7
 */
@XmlRootElement(name = "webhook")
@XmlAccessorType(XmlAccessType.FIELD)
public class WebhookEntity
{
    @XmlElement(name = "id")
    private final Long id;

    @XmlElement(name = "endpointUrl")
    private String endpointUrl;

    @XmlElement(name = "token")
    private String token;

    public WebhookEntity()
    {
        this.id = null;
    }

    public WebhookEntity(long id, String endpointUrl)
    {
        this.id = id;
        this.endpointUrl = endpointUrl;
    }

    public WebhookEntity(Webhook webhook)
    {
        this(webhook.getId(), webhook.getEndpointUrl());
    }

    public WebhookEntity(String endpointUrl, @Nullable String token)
    {
        this.id = null;
        this.endpointUrl = endpointUrl;
        this.token = token;
    }

    public long getId()
    {
        return id;
    }

    public String getEndpointUrl()
    {
        return endpointUrl;
    }

    @Nullable
    public String getToken()
    {
        return token;
    }
}
