package com.atlassian.crowd.plugin.rest.exception.mapper;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.plugin.rest.entity.ErrorEntity;

@Provider
public class MembershipAlreadyExistsExceptionMapper implements ExceptionMapper<MembershipAlreadyExistsException>
{
    @Override
    public Response toResponse(MembershipAlreadyExistsException exception)
    {
        return Response.status(Response.Status.CONFLICT).entity(ErrorEntity.of(exception)).build();
    }
}
