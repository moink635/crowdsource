package com.atlassian.crowd.plugin.rest.service.controller;

import javax.annotation.Nullable;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.webhook.InvalidWebhookEndpointException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.webhook.Webhook;

/**
 * Webhooks controller
 *
 * @since v2.7
 */
public class WebhooksController extends AbstractResourceController
{
    public WebhooksController(ApplicationService applicationService, ApplicationManager applicationManager)
    {
        super(applicationService, applicationManager);
    }

    public Webhook findWebhookById(String applicationName, long webhookId)
        throws ApplicationPermissionException, WebhookNotFoundException
    {
        Application application = getApplication(applicationName);
        return applicationService.findWebhookById(application, webhookId);
    }

    public Webhook registerWebhook(String applicationName, String endpointUrl, @Nullable String token) throws InvalidWebhookEndpointException
    {
        Application application = getApplication(applicationName);
        return applicationService.registerWebhook(application, endpointUrl, token);
    }

    public void unregisterWebhook(String applicationName, long webhookId)
        throws ApplicationPermissionException, WebhookNotFoundException
    {
        Application application = getApplication(applicationName);
        applicationService.unregisterWebhook(application, webhookId);
    }
}
