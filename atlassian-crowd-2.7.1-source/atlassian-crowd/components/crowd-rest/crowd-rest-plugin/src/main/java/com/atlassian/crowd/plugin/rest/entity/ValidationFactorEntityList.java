package com.atlassian.crowd.plugin.rest.entity;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * List of ValidationFactors.
 */
@XmlRootElement (name = "validation-factors")
@XmlAccessorType (XmlAccessType.FIELD)
public class ValidationFactorEntityList
{
    @XmlElement (name="validation-factor", type= ValidationFactorEntity.class)
    @JsonProperty ("validationFactors")
    private final List<ValidationFactorEntity> validationFactors;

    private ValidationFactorEntityList()
    {
        validationFactors = new ArrayList<ValidationFactorEntity>();
    }

    public ValidationFactorEntityList(final List<ValidationFactorEntity> validationFactors)
    {
        this.validationFactors = validationFactors;
    }

    public List<ValidationFactorEntity> getValidationFactors()
    {
        return validationFactors;
    }
}
