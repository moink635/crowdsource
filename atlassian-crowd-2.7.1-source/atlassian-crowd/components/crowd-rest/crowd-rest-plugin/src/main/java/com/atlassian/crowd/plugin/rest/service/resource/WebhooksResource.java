package com.atlassian.crowd.plugin.rest.service.resource;

import java.net.URI;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.manager.webhook.InvalidWebhookEndpointException;
import com.atlassian.crowd.model.webhook.Webhook;
import com.atlassian.crowd.plugin.rest.entity.WebhookEntity;
import com.atlassian.crowd.plugin.rest.service.controller.WebhooksController;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_XML;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

/**
 * Webhook management resource
 *
 * @since v2.7
 */
@Path("webhook")
@AnonymousAllowed
public class WebhooksResource extends AbstractResource
{
    private final WebhooksController webhooksController;

    public WebhooksResource(WebhooksController webhooksController)
    {
        this.webhooksController = webhooksController;
    }

    @POST
    @Consumes({APPLICATION_XML, APPLICATION_JSON})
    @Produces({APPLICATION_XML, APPLICATION_JSON})
    public Response registerWebhook(WebhookEntity webhookEntity) throws InvalidWebhookEndpointException
    {
        checkArgument(webhookEntity != null, "missing Webhook entity");
        checkArgument(webhookEntity.getEndpointUrl() != null, "missing Webhook endpoint URL");

        String applicationName = getApplicationName();
        Webhook webhook = webhooksController.registerWebhook(applicationName, webhookEntity.getEndpointUrl(),
                                                             webhookEntity.getToken());

        URI webhookResourceUri = UriBuilder.fromPath(Long.toString(webhook.getId())).build();
        return Response.status(Response.Status.CREATED).entity(new WebhookEntity(webhook))
            .location(webhookResourceUri).build();
    }

    @POST
    @Consumes({TEXT_PLAIN})
    public Response registerWebhookWithSimpleBody(String endpointUrl) throws InvalidWebhookEndpointException
    {
        checkArgument(endpointUrl != null, "missing Webhook endpoint URL");

        String applicationName = getApplicationName();
        Webhook webhook = webhooksController.registerWebhook(applicationName, endpointUrl, null);

        URI webhookResourceUri = UriBuilder.fromPath(Long.toString(webhook.getId())).build();
        return Response.created(webhookResourceUri).build();
    }

    @GET
    @Produces({APPLICATION_XML, APPLICATION_JSON})
    @Path("{webhookId}")
    public Response getWebhook(@PathParam("webhookId") Long id)
        throws WebhookNotFoundException, ApplicationPermissionException
    {
        checkNotNull(id, "Webhook id");

        String applicationName = getApplicationName();
        Webhook webhook = webhooksController.findWebhookById(applicationName, id);
        return Response.ok(new WebhookEntity(webhook)).build();
    }

    @DELETE
    @Path("{webhookId}")
    public Response unregisterWebhook(@PathParam("webhookId") Long id)
        throws WebhookNotFoundException, ApplicationPermissionException
    {
        checkNotNull(id, "Webhook id");

        String applicationName = getApplicationName();
        webhooksController.unregisterWebhook(applicationName, id);
        return Response.noContent().build();
    }
}
