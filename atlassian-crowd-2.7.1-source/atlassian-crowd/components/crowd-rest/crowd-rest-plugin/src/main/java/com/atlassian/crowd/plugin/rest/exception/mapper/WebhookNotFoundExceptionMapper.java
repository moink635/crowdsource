package com.atlassian.crowd.plugin.rest.exception.mapper;

import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.plugin.rest.entity.ErrorEntity;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * @since v2.7
 */
@Provider
public class WebhookNotFoundExceptionMapper implements ExceptionMapper<WebhookNotFoundException>
{
    public Response toResponse(WebhookNotFoundException exception)
    {
        return Response.status(Response.Status.NOT_FOUND).entity(ErrorEntity.of(exception)).build();
    }
}
