package com.atlassian.crowd.plugin.rest.service.resource;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class GroupsResourceTest
{
    @Test
    public void nullsAreConsideredUnspecified()
    {
        assertTrue(GroupsResource.unspecified(null, ""));
    }

    @Test
    public void nonBlankStringsAreConsideredSpecified()
    {
        assertFalse(GroupsResource.unspecified("value", ""));
    }

    @Test(expected = IllegalArgumentException.class)
    public void blankStringsCauseFailure()
    {
        GroupsResource.unspecified("", "");
    }
}
