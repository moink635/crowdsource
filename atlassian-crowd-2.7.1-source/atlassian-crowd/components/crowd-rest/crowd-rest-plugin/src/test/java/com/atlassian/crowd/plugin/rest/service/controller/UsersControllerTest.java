package com.atlassian.crowd.plugin.rest.service.controller;

import java.net.URI;

import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.login.ForgottenLoginManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.plugin.rest.entity.UserEntity;
import com.atlassian.plugins.rest.common.Link;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UsersControllerTest
{
    private static final long DIRECTORY_ID = 1L;
    private static final String APPLICATION_NAME = "application";

    private UsersController userController;

    @Mock private ApplicationService applicationService;
    @Mock private ApplicationManager applicationManager;
    @Mock private ForgottenLoginManager forgottenLoginManager;
    @Mock private Application application;

    @Before
    public void createObjectUnderTest() throws Exception
    {
        userController = new UsersController(applicationService, applicationManager, forgottenLoginManager);
    }

    @Test
    public void findUserByNameShouldReturnNameFromDatabaseWhenExpandingAttributes() throws Exception
    {
        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        UserWithAttributes userWithAttributes = new UserTemplateWithAttributes("username-from-database", DIRECTORY_ID);
        when(applicationService.findUserWithAttributesByName(application, "USERNAME-FROM-REQUEST")).thenReturn(userWithAttributes);

        Link link = Link.link(URI.create("http://example.test/user-resource"), "");
        UserEntity userEntity = userController.findUserByName(APPLICATION_NAME, "USERNAME-FROM-REQUEST", link, true);

        assertEquals("username-from-database", userEntity.getName());
        assertEquals("http://example.test/user-resource?username=username-from-database",
                     userEntity.getLink().getHref().toString());
        assertEquals("http://example.test/user-resource/password?username=username-from-database",
                     userEntity.getPassword().getLink().getHref().toString());
        assertEquals("http://example.test/user-resource/attribute?username=username-from-database",
                     userEntity.getAttributes().getLink().getHref().toString());
    }

    @Test
    public void findUserByNameShouldReturnNameFromDatabaseWhenNotExpandingAttributes() throws Exception
    {
        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        User user = new UserTemplate("username-from-database", DIRECTORY_ID);
        when(applicationService.findUserByName(application, "USERNAME-FROM-REQUEST")).thenReturn(user);

        Link link = Link.link(URI.create("http://example.test/user-resource"), "");
        UserEntity userEntity = userController.findUserByName(APPLICATION_NAME, "USERNAME-FROM-REQUEST", link, false);

        assertEquals("username-from-database", userEntity.getName());
        assertEquals("http://example.test/user-resource?username=username-from-database",
                     userEntity.getLink().getHref().toString());
        assertEquals("http://example.test/user-resource/password?username=username-from-database",
                     userEntity.getPassword().getLink().getHref().toString());
        assertEquals("http://example.test/user-resource/attribute?username=username-from-database",
                     userEntity.getAttributes().getLink().getHref().toString());
    }
}
