package com.atlassian.crowd.plugin.rest.util;

import org.junit.Test;

import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;

public class LinkUriHelperTest
{

    @Test
    public void buildUserUriShouldPercentEncodeCurlyBrackets()
    {
        String href = "http://sample.test/";
        URI uri = LinkUriHelper.buildUserUri(URI.create(href), "{curlybracketuser}");
        assertEquals(href + "user?username=%7Bcurlybracketuser%7D", uri.toString());
    }

    @Test
    public void updateUserUriShouldPercentEncodeCurlyBrackets()
    {
        String href = "http://sample.test/user";
        URI uri = LinkUriHelper.updateUserUri(URI.create(href), "{curlybracketuser}");
        assertEquals(href + "?username=%7Bcurlybracketuser%7D", uri.toString());
    }

    @Test
    public void demonstrateThatJavaUriForcesCurlyBracketsToBePercentEncodedSoWeDoNotNeedToTestNonEncodedCurlyBrackets()
    {
        String href = "http://sample.test/user?username={nonencodedcurlybracketuser}";
        try
        {
            URI.create(href);
        }
        catch (IllegalArgumentException e)
        {
            URISyntaxException uriSyntaxException = (URISyntaxException) e.getCause();
            assertEquals("Illegal character in query at index 33: " + href, uriSyntaxException.getMessage());
        }
    }

    @Test
    public void buildEntityAttributeUriShouldHandleUsernameWithMultipleConsecutiveSpaces()
    {
        String href = "http://sample.test/user?username=mr+trailing+spaces++++";
        URI uri = LinkUriHelper.buildEntityAttributeUri(URI.create(href), "sampleAttribute");
        assertEquals(href + "&attributename=sampleAttribute", uri.toString());
    }

    @Test
    public void buildEntityAttributeUriShouldHandleUsernameWithPercentEncodedCurlyBrackets()
    {
        String href = "http://sample.test/user?username=%7Bcurlybracketuser%7D";
        URI uri = LinkUriHelper.buildEntityAttributeUri(URI.create(href), "sampleAttribute");
        assertEquals(href + "&attributename=sampleAttribute", uri.toString());
    }

    @Test
    public void buildEntityAttributeUriShouldHandleUsernameWithNameOfAttributeName()
    {
        String href = "http://sample.test/user?username=%7BattributeName%7D";
        URI uri = LinkUriHelper.buildEntityAttributeUri(URI.create(href), "sampleAttribute");
        assertEquals(href + "&attributename=sampleAttribute", uri.toString());
    }
}
