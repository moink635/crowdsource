package com.atlassian.crowd.plugin.rest.filter;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionBindingListener;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.manager.validation.ClientValidationException;
import com.atlassian.crowd.manager.validation.ClientValidationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.authentication.ApplicationAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.plugin.rest.service.util.AuthenticatedApplicationUtil;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BasicApplicationAuthenticationFilterTest
{
    private static final String APPLICATION_NAME = "appname";
    private static final String APPLICATION_TOKEN_HASH = "token-hash";

    @Mock
    private ApplicationManager applicationManager;

    @Mock
    private ClientValidationManager clientValidationManager;

    @Mock
    private TokenAuthenticationManager tokenAuthenticationManager;

    @Mock
    private Application application;

    @Mock
    private Token token;

    @Mock
    private Token invalidToken;

    @Mock
    private FilterChain chain;

    @InjectMocks
    private BasicApplicationAuthenticationFilter filter;

    private MockHttpServletRequest request = new MockHttpServletRequest();
    private MockHttpServletResponse response = new MockHttpServletResponse();

    @Test
    public void shouldRespondWithChallengeIfCredentialsAreNotProvided() throws Exception
    {
        filter.doFilter(request, response, chain);

        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, response.getStatus());

        assertSeraphIsDisabled(request);
        assertApplicationNameIsNotSavedInSession();
        assertApplicationTokenIsNotSavedInSession();

        verifyZeroInteractions(chain);
        verifyZeroInteractions(applicationManager);
        verifyZeroInteractions(tokenAuthenticationManager);
    }

    @Test
    public void shouldRespondWithChallengeIfApplicationNameIsUnknown() throws Exception
    {
        addCredentials(request);

        when(applicationManager.findByName(APPLICATION_NAME)).thenThrow(new ApplicationNotFoundException(""));

        filter.doFilter(request, response, chain);

        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, response.getStatus());

        assertSeraphIsDisabled(request);
        assertApplicationNameIsNotSavedInSession();
        assertApplicationTokenIsNotSavedInSession();

        verifyZeroInteractions(chain);
        verifyZeroInteractions(tokenAuthenticationManager);
    }

    @Test
    public void shouldRefuseToAttendInvalidClients() throws Exception
    {
        addCredentials(request);

        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        doThrow(new ClientValidationException()).when(clientValidationManager).validate(application, request);

        filter.doFilter(request, response, chain);

        assertEquals(HttpServletResponse.SC_FORBIDDEN, response.getStatus());

        assertSeraphIsDisabled(request);
        assertApplicationNameIsNotSavedInSession();
        assertApplicationTokenIsNotSavedInSession();

        verifyZeroInteractions(chain);
        verifyZeroInteractions(tokenAuthenticationManager);
        verify(applicationManager, never()).authenticate(any(Application.class), any(PasswordCredential.class));
    }

    @Test
    public void shouldRespondWithChallengeIfPasswordIsInvalidUsingTokenBasedAuthentication() throws Exception
    {
        addCredentials(request);

        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        when(tokenAuthenticationManager.authenticateApplication(any(ApplicationAuthenticationContext.class)))
            .thenThrow(new InvalidAuthenticationException(""));

        when(applicationManager.authenticate(application, PasswordCredential.unencrypted("secret")))
            .thenReturn(false);

        filter.doFilter(request, response, chain);

        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, response.getStatus());

        assertSeraphIsDisabled(request);
        assertApplicationNameIsNotSavedInSession();
        assertApplicationTokenIsNotSavedInSession();

        verifyZeroInteractions(chain);
        verify(tokenAuthenticationManager).authenticateApplication(any(ApplicationAuthenticationContext.class));
        verify(applicationManager, never()).authenticate(any(Application.class), any(PasswordCredential.class));
    }

    @Test
    public void shouldRespondWithChallengeIfPasswordIsInvalidUsingPasswordBasedAuthentication() throws Exception
    {
        addCredentials(request);

        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        when(tokenAuthenticationManager.authenticateApplication(any(ApplicationAuthenticationContext.class)))
            .thenReturn(null); // host application does not support token-based authentication

        filter.doFilter(request, response, chain);

        assertEquals(HttpServletResponse.SC_UNAUTHORIZED, response.getStatus());

        assertSeraphIsDisabled(request);
        assertApplicationNameIsNotSavedInSession();
        assertApplicationTokenIsNotSavedInSession();

        verifyZeroInteractions(chain);
        verify(tokenAuthenticationManager).authenticateApplication(any(ApplicationAuthenticationContext.class));
        verify(applicationManager).authenticate(application, PasswordCredential.unencrypted("secret"));
    }

    @Test
    public void shouldLetPassIfApplicationAuthenticatesSuccessfullyUsingTokenBasedAuthentication() throws Exception
    {
        addCredentials(request);

        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        when(tokenAuthenticationManager.authenticateApplication(any(ApplicationAuthenticationContext.class)))
            .thenReturn(token);

        when(token.getRandomHash()).thenReturn(APPLICATION_TOKEN_HASH);

        filter.doFilter(request, response, chain);

        assertSeraphIsDisabled(request);
        assertApplicationNameIsSavedInSession();
        assertApplicationTokenIsSavedInSession();

        verify(chain).doFilter(request, response);
        verify(tokenAuthenticationManager).authenticateApplication(any(ApplicationAuthenticationContext.class));
    }

    @Test
    public void shouldLetPassIfApplicationAuthenticatesSuccessfullyUsingPasswordBasedAuthentication() throws Exception
    {
        addCredentials(request);

        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        when(tokenAuthenticationManager.authenticateApplication(any(ApplicationAuthenticationContext.class)))
            .thenReturn(null); // host application does not support token-based authentication

        when(applicationManager.authenticate(application, PasswordCredential.unencrypted("secret")))
            .thenReturn(true);

        filter.doFilter(request, response, chain);

        assertSeraphIsDisabled(request);
        assertApplicationNameIsSavedInSession();
        assertApplicationTokenIsNotSavedInSession();

        verify(chain).doFilter(request, response);
        verify(tokenAuthenticationManager).authenticateApplication(any(ApplicationAuthenticationContext.class));
        verify(applicationManager).authenticate(application, PasswordCredential.unencrypted("secret"));
    }

    @Test
    public void shouldLetPassWithoutAuthenticationIfApplicationHasAlreadyInitiatedSessionUsingToken()
        throws Exception
    {
        addCredentials(request);

        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        request.getSession().setAttribute(AuthenticatedApplicationUtil.APPLICATION_NAME_ATTRIBUTE_KEY, APPLICATION_NAME);
        request.getSession().setAttribute(AuthenticatedApplicationUtil.APPLICATION_TOKEN_ATTRIBUTE_KEY, token);

        when(token.getRandomHash()).thenReturn(APPLICATION_TOKEN_HASH);

        filter.doFilter(request, response, chain);

        assertSeraphIsDisabled(request);
        assertApplicationNameIsSavedInSession();
        assertApplicationTokenIsSavedInSession();

        verify(chain).doFilter(request, response);
        verify(tokenAuthenticationManager).validateApplicationToken(APPLICATION_TOKEN_HASH, new ValidationFactor[0]);
        verify(tokenAuthenticationManager, never())
            .authenticateApplication(any(ApplicationAuthenticationContext.class)); // do not authenticate again
        verify(applicationManager, never()).authenticate(any(Application.class), any(PasswordCredential.class));
    }

    @Test
    public void shouldGenerateANewTokenIfIfStoredOneIsNotValid()
        throws Exception
    {
        addCredentials(request);

        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        when(tokenAuthenticationManager.validateApplicationToken("invalid-token", new ValidationFactor[0]))
            .thenThrow(new InvalidTokenException(""));

        when(tokenAuthenticationManager.authenticateApplication(any(ApplicationAuthenticationContext.class)))
            .thenReturn(token);

        when(token.getRandomHash()).thenReturn(APPLICATION_TOKEN_HASH);
        when(invalidToken.getRandomHash()).thenReturn("invalid-token");

        request.getSession().setAttribute(AuthenticatedApplicationUtil.APPLICATION_NAME_ATTRIBUTE_KEY, APPLICATION_NAME);
        request.getSession().setAttribute(AuthenticatedApplicationUtil.APPLICATION_TOKEN_ATTRIBUTE_KEY, invalidToken);

        filter.doFilter(request, response, chain);

        assertSeraphIsDisabled(request);
        assertApplicationNameIsSavedInSession();
        assertApplicationTokenIsSavedInSession();

        verify(chain).doFilter(request, response);
        verify(tokenAuthenticationManager).validateApplicationToken("invalid-token", new ValidationFactor[0]);
        verify(tokenAuthenticationManager)
            .authenticateApplication(any(ApplicationAuthenticationContext.class)); // authenticate again
        verify(applicationManager, never()).authenticate(any(Application.class), any(PasswordCredential.class));
    }

    @Test
    public void shouldLetPassWithoutAuthenticationIfApplicationHasAlreadyInitiatedSessionUsingPassword()
        throws Exception
    {
        addCredentials(request);

        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        request.getSession().setAttribute(AuthenticatedApplicationUtil.APPLICATION_NAME_ATTRIBUTE_KEY, APPLICATION_NAME);

        filter.doFilter(request, response, chain);

        assertSeraphIsDisabled(request);
        assertApplicationNameIsSavedInSession();

        verify(chain).doFilter(request, response);
        verifyZeroInteractions(tokenAuthenticationManager);
        verify(applicationManager, never()).authenticate(any(Application.class), any(PasswordCredential.class));
    }

    @Test
    public void shouldNotAnotherApplicationToReuseExistingSession() throws Exception
    {
        addCredentials(request);

        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        when(tokenAuthenticationManager.authenticateApplication(any(ApplicationAuthenticationContext.class)))
            .thenReturn(token);

        when(token.getRandomHash()).thenReturn(APPLICATION_TOKEN_HASH);

        request.getSession().setAttribute(AuthenticatedApplicationUtil.APPLICATION_NAME_ATTRIBUTE_KEY, "other-app");
        request.getSession().setAttribute(AuthenticatedApplicationUtil.APPLICATION_TOKEN_ATTRIBUTE_KEY, "other-token");

        filter.doFilter(request, response, chain);

        assertApplicationNameIsSavedInSession();
        assertApplicationTokenIsSavedInSession();

        assertSeraphIsDisabled(request);

        verify(chain).doFilter(request, response);
        verify(tokenAuthenticationManager).authenticateApplication(any(ApplicationAuthenticationContext.class));
    }

    @Test
    public void shouldInvalidateTheTokenWhenHttpSessionExpires() throws Exception
    {
        // first we initiate a session so the token is saved in the session

        addCredentials(request);

        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        when(tokenAuthenticationManager.authenticateApplication(any(ApplicationAuthenticationContext.class)))
            .thenReturn(token);

        when(token.getRandomHash()).thenReturn(APPLICATION_TOKEN_HASH);

        filter.doFilter(request, response, chain);

        assertApplicationTokenIsSavedInSession();

        // then we simulate that the HTTP session expires

        HttpSessionBindingListener savedToken = (HttpSessionBindingListener) request.getSession()
                .getAttribute(AuthenticatedApplicationUtil.APPLICATION_TOKEN_ATTRIBUTE_KEY);

        savedToken.valueUnbound(null);

        verify(tokenAuthenticationManager).invalidateToken(APPLICATION_TOKEN_HASH);
    }

    private void addCredentials(MockHttpServletRequest request)
    {
        request.addHeader("Authorization", "Basic YXBwbmFtZTpzZWNyZXQ="); // appname:secret
    }

    private void assertSeraphIsDisabled(HttpServletRequest request)
    {
        assertEquals(Boolean.TRUE, request.getAttribute("os_securityfilter_already_filtered"));
    }

    private void assertApplicationNameIsSavedInSession()
    {
        assertEquals(APPLICATION_NAME,
                     request.getSession().getAttribute(AuthenticatedApplicationUtil.APPLICATION_NAME_ATTRIBUTE_KEY));
    }

    private void assertApplicationNameIsNotSavedInSession()
    {
        assertNull(request.getSession().getAttribute(AuthenticatedApplicationUtil.APPLICATION_NAME_ATTRIBUTE_KEY));
    }

    private void assertApplicationTokenIsSavedInSession()
    {
        Token savedToken =
            (Token) request.getSession().getAttribute(AuthenticatedApplicationUtil.APPLICATION_TOKEN_ATTRIBUTE_KEY);
        assertNotNull(savedToken);
        assertEquals(APPLICATION_TOKEN_HASH, savedToken.getRandomHash());
    }

    private void assertApplicationTokenIsNotSavedInSession()
    {
        assertNull(request.getSession().getAttribute(AuthenticatedApplicationUtil.APPLICATION_TOKEN_ATTRIBUTE_KEY));
    }
}
