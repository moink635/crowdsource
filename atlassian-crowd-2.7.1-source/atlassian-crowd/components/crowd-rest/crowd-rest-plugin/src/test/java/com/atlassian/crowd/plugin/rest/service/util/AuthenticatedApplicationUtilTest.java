package com.atlassian.crowd.plugin.rest.service.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.atlassian.crowd.model.token.Token;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests {@link com.atlassian.crowd.plugin.rest.service.util.AuthenticatedApplicationUtil}.
 */
@RunWith(MockitoJUnitRunner.class)
public class AuthenticatedApplicationUtilTest
{
    private static final String APPLICATION_NAME = "applicationName";

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpSession session;

    @Mock
    private Token token;

    @Test
    public void testGetAuthenticatedApplication() throws Exception
    {
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute(AuthenticatedApplicationUtil.APPLICATION_NAME_ATTRIBUTE_KEY))
            .thenReturn(APPLICATION_NAME);

        assertEquals(APPLICATION_NAME, AuthenticatedApplicationUtil.getAuthenticatedApplication(request));
    }

    @Test
    public void testGetAuthenticatedApplication_NoSession() throws Exception
    {
        when(request.getSession(false)).thenReturn(null);

        assertNull(AuthenticatedApplicationUtil.getAuthenticatedApplication(request));
    }

    @Test
    public void testGetAuthenticatedApplication_NotName() throws Exception
    {
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute(AuthenticatedApplicationUtil.APPLICATION_NAME_ATTRIBUTE_KEY)).thenReturn(null);

        assertNull(AuthenticatedApplicationUtil.getAuthenticatedApplication(request));
    }

    @Test
    public void testSetAuthenticatedApplication() throws Exception
    {
        when(request.getSession()).thenReturn(session);

        AuthenticatedApplicationUtil.setAuthenticatedApplication(request, APPLICATION_NAME);

        verify(session).setAttribute(AuthenticatedApplicationUtil.APPLICATION_NAME_ATTRIBUTE_KEY, APPLICATION_NAME);
    }

    @Test
    public void testGetAuthenticatedApplicationToken() throws Exception
    {
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute(AuthenticatedApplicationUtil.APPLICATION_TOKEN_ATTRIBUTE_KEY))
            .thenReturn(token);

        assertEquals(token, AuthenticatedApplicationUtil.getAuthenticatedApplicationToken(request));
    }

    @Test
    public void testGetAuthenticatedApplicationToken_NoSession() throws Exception
    {
        when(request.getSession(false)).thenReturn(null);

        assertNull(AuthenticatedApplicationUtil.getAuthenticatedApplicationToken(request));
    }

    @Test
    public void testGetAuthenticatedApplicationToken_NoToken() throws Exception
    {
        when(request.getSession(false)).thenReturn(session);
        when(session.getAttribute(AuthenticatedApplicationUtil.APPLICATION_TOKEN_ATTRIBUTE_KEY)).thenReturn(null);

        assertNull(AuthenticatedApplicationUtil.getAuthenticatedApplicationToken(request));
    }

    @Test
    public void testSetAuthenticatedApplicationToken() throws Exception
    {
        when(request.getSession()).thenReturn(session);

        AuthenticatedApplicationUtil.setAuthenticatedApplicationToken(request, token);

        verify(session).setAttribute(AuthenticatedApplicationUtil.APPLICATION_TOKEN_ATTRIBUTE_KEY, token);
    }
}
