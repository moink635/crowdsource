package com.atlassian.crowd.plugin.rest.entity;

import java.net.URI;

import com.atlassian.plugins.rest.common.Link;
import com.atlassian.plugins.rest.common.json.DefaultJaxbJsonMarshaller;

import com.google.common.collect.ImmutableList;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class UserEntityListTest
{
    @Test
    public void serialisationAsJsonDoesNotIncludeExtraNesting() throws Exception
    {
        UserEntity user = new UserEntity("username", "firstName", "lastName", "Display Name", "email", null,
                true, Link.self(new URI("http://localhost/")), "1-this-is-external-id-abcdefg");

        UserEntityList l = new UserEntityList(ImmutableList.of(user));

        DefaultJaxbJsonMarshaller marshaller = new DefaultJaxbJsonMarshaller();

        String json = marshaller.marshal(l);

        JsonNode node = new ObjectMapper().readTree(json);
        ArrayNode users = (ArrayNode) node.get("users");
        assertEquals(1, users.size());

        JsonNode userNode = users.get(0);
        assertNull(userNode.get("UserEntity"));
        assertEquals("username", userNode.get("name").getTextValue());
    }
}
