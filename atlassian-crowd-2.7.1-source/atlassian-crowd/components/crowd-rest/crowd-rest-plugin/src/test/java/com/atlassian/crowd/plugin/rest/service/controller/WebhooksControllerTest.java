package com.atlassian.crowd.plugin.rest.service.controller;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.webhook.Webhook;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WebhooksControllerTest
{
    private static final String APP_NAME = "my-application";

    private WebhooksController controller;

    @Mock private ApplicationService applicationService;
    @Mock private ApplicationManager applicationManager;
    @Mock private Application application;

    @Before
    public void createObjectUnderTest() throws ApplicationNotFoundException
    {
        controller = new WebhooksController(applicationService, applicationManager);

        // the application is assumed to exist
        when(applicationManager.findByName(APP_NAME)).thenReturn(application);
    }

    @Test
    public void shouldFindWebhookById() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(applicationService.findWebhookById(application, 1L)).thenReturn(webhook);

        assertSame(webhook, controller.findWebhookById(APP_NAME, 1L));
    }

    @Test (expected = WebhookNotFoundException.class)
    public void shouldNotFindWebhookIfItDoesNotExist() throws Exception
    {
        when(applicationService.findWebhookById(application, 1L)).thenThrow(new WebhookNotFoundException(1L));

        controller.findWebhookById(APP_NAME, 1L);
    }

    @Test (expected = ApplicationPermissionException.class)
    public void shouldNotFindWebhookIfApplicationDoesNotOwnTheWebhook() throws Exception
    {
        when(applicationService.findWebhookById(application, 1L)).thenThrow(new ApplicationPermissionException());

        controller.findWebhookById(APP_NAME, 1L);
    }

    @Test
    public void shouldRegisterWebhook() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(applicationService.registerWebhook(application, "http://example.test/mywebhook", "secret"))
            .thenReturn(webhook);

        assertSame(webhook, controller.registerWebhook(APP_NAME, "http://example.test/mywebhook", "secret"));
    }

    @Test
    public void unregisterWebhookSuccessfully() throws Exception
    {
        controller.unregisterWebhook(APP_NAME, 1L);

        verify(applicationService).unregisterWebhook(application, 1L);
    }

    @Test(expected = ApplicationPermissionException.class)
    public void unregisterWebhookWhenApplicationDoesNotOwnTheWebhook() throws Exception
    {
        doThrow(new ApplicationPermissionException("failed")).when(applicationService).unregisterWebhook(application,
                                                                                                         1L);

        controller.unregisterWebhook(APP_NAME, 1L);
    }

    @Test(expected = WebhookNotFoundException.class)
    public void unregisterNonExistingWebhook() throws Exception
    {
        doThrow(new WebhookNotFoundException(1L)).when(applicationService).unregisterWebhook(application, 1L);

        controller.unregisterWebhook(APP_NAME, 1L);
    }
}
