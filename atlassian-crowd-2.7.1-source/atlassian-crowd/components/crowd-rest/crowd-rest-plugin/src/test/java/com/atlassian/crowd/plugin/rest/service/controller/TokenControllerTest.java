package com.atlassian.crowd.plugin.rest.service.controller;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.plugin.rest.entity.SessionEntity;
import com.atlassian.crowd.plugin.rest.entity.ValidationFactorEntity;
import com.atlassian.crowd.plugin.rest.util.LinkUriHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TokenControllerTest
{
    private static final String TOKEN_KEY = "token-key";
    private static final String APPLICATION_NAME = "application-name";
    private static final String USER_NAME = "user1";
    private static final String USER_DISPLAY_NAME = "User Display Name";
    private static final String USER_FIRST_NAME = "john";
    private static final String USER_LAST_NAME = "doe";
    private static final String USER_EMAIL = "john@example.com";
    private static final String USER_PASSWORD = "user-password";
    private static final Date TOKEN_CREATED_DATE = new Date(0);
    private static final Collection<ValidationFactorEntity> NO_VALIDATION_FACTORS = Collections.emptyList();
    private static final URI BASE_URI = URI.create("http://localhost/");

    private TokenController tokenController;

    @Mock private TokenAuthenticationManager tokenAuthenticationManager;
    @Mock private User user;
    private Token token;

    @Before
    public void setUp() throws Exception
    {
        this.tokenController = new TokenController(tokenAuthenticationManager);
        this.token = new Token.Builder(1L, USER_NAME, "identifier-hash", 1234L, TOKEN_KEY)
            .setCreatedDate(TOKEN_CREATED_DATE)
            .create();

        when(user.getName()).thenReturn(USER_NAME);
        when(user.getFirstName()).thenReturn(USER_FIRST_NAME);
        when(user.getLastName()).thenReturn(USER_LAST_NAME);
        when(user.getDisplayName()).thenReturn(USER_DISPLAY_NAME);
        when(user.getEmailAddress()).thenReturn(USER_EMAIL);
    }

    @Test
    public void authenticateUserReturnsMinimalUser() throws Exception
    {
        when(tokenAuthenticationManager.authenticateUser(any(UserAuthenticationContext.class), any(TokenLifetime.class)))
            .thenReturn(token);

        SessionEntity session = tokenController.authenticateUser(APPLICATION_NAME, USER_NAME, USER_PASSWORD,
                                                                 TokenLifetime.USE_DEFAULT, NO_VALIDATION_FACTORS,
                                                                 BASE_URI);

        assertBaseSessionEntity(session);
        assertUserEntityIsMinimal(session);
    }

    @Test
    public void authenticateUserWithoutValidatingPasswordReturnsMinimalUser() throws Exception
    {
        when(tokenAuthenticationManager.authenticateUserWithoutValidatingPassword(any(UserAuthenticationContext.class)))
            .thenReturn(token);

        SessionEntity session =
            tokenController.authenticateUserWithoutValidatingPassword(APPLICATION_NAME, USER_NAME,
                                                                      NO_VALIDATION_FACTORS, BASE_URI);

        assertBaseSessionEntity(session);
        assertUserEntityIsMinimal(session);
    }

    @Test
    public void validateTokenReturnsMinimalUser() throws Exception
    {
        when(tokenAuthenticationManager.validateUserToken(TOKEN_KEY, new ValidationFactor[0], APPLICATION_NAME))
             .thenReturn(token);

        SessionEntity session =
            tokenController.validateToken(APPLICATION_NAME, TOKEN_KEY, NO_VALIDATION_FACTORS, BASE_URI);

        assertBaseSessionEntity(session);
        assertUserEntityIsMinimal(session);
    }

    @Test
    public void getSessionFromTokenReturnsExpandedUser() throws Exception
    {
        when(tokenAuthenticationManager.findUserTokenByKey(TOKEN_KEY, APPLICATION_NAME)).thenReturn(token);
        when(tokenAuthenticationManager.findUserByToken(TOKEN_KEY, APPLICATION_NAME)).thenReturn(user);

        SessionEntity session = tokenController.getSessionFromToken(TOKEN_KEY, APPLICATION_NAME, BASE_URI);

        assertBaseSessionEntity(session);
        assertUserEntityIsExpanded(session);
    }

    private void assertBaseSessionEntity(SessionEntity session)
    {
        assertEquals(TOKEN_KEY, session.getToken());
        assertEquals(LinkUriHelper.buildSessionLink(BASE_URI, TOKEN_KEY), session.getLink());
        assertEquals(TOKEN_CREATED_DATE, session.getCreatedDate());
    }

    private void assertUserEntityIsExpanded(SessionEntity session)
    {
        assertEquals(USER_NAME, session.getUser().getName());
        assertEquals(USER_FIRST_NAME, session.getUser().getFirstName());
        assertEquals(USER_LAST_NAME, session.getUser().getLastName());
        assertEquals(USER_DISPLAY_NAME, session.getUser().getDisplayName());
        assertEquals(USER_EMAIL, session.getUser().getEmail());
        assertNull("User entity should not contain a password", session.getUser().getPassword().getValue());
    }

    private void assertUserEntityIsMinimal(SessionEntity session)
    {
        assertEquals(USER_NAME, session.getUser().getName());
        assertNull("User entity should not contain a first name", session.getUser().getFirstName());
        assertNull("User entity should not contain a last name", session.getUser().getLastName());
        assertNull("User entity should not contain a display name", session.getUser().getDisplayName());
        assertNull("User entity should not contain an email ", session.getUser().getEmail());
        assertNull("User entity should not contain a password", session.getUser().getPassword());
    }
}
