package com.atlassian.crowd.plugin.rest.service.resource;

import java.net.URI;

import javax.ws.rs.core.Response;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.manager.webhook.InvalidWebhookEndpointException;
import com.atlassian.crowd.model.webhook.Webhook;
import com.atlassian.crowd.plugin.rest.entity.WebhookEntity;
import com.atlassian.crowd.plugin.rest.service.controller.WebhooksController;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WebhooksResourceTest
{
    private static final String APP_NAME = "my-application";

    private WebhooksResource resource;

    @Mock private WebhooksController webhooksController;

    @Before
    public void setUp() throws Exception
    {
        resource = new WebhooksResource(webhooksController);
        resource.request = new MockHttpServletRequest();
        resource.setApplicationName(APP_NAME);
    }

    @Test
    public void shouldGetWebhookById() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getId()).thenReturn(1L);
        when(webhook.getEndpointUrl()).thenReturn("http://example.test/mywebhook");
        when(webhooksController.findWebhookById(APP_NAME, 1L)).thenReturn(webhook);

        Response response = resource.getWebhook(1L);

        assertEquals(200, response.getStatus());
        WebhookEntity webhookEntity = (WebhookEntity) response.getEntity();
        assertEquals(1L, webhookEntity.getId());
        assertEquals("http://example.test/mywebhook", webhookEntity.getEndpointUrl());
    }

    @Test (expected = WebhookNotFoundException.class)
    public void shouldNotGetWebhookByIdIfItDoesNotExist() throws Exception
    {
        when(webhooksController.findWebhookById(APP_NAME, 1L)).thenThrow(new WebhookNotFoundException(1L));

        resource.getWebhook(1L);
    }

    @Test
    public void registerWebhookSuccessfully() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getId()).thenReturn(1L);
        when(webhook.getEndpointUrl()).thenReturn("http://example.test/mywebhook");
        when(webhooksController.registerWebhook(APP_NAME, "http://example.test/mywebhook", "secret")).thenReturn(webhook);

        WebhookEntity webhookEntity = new WebhookEntity("http://example.test/mywebhook", "secret");
        Response response = resource.registerWebhook(webhookEntity);

        assertEquals(201, response.getStatus());
        assertEquals(URI.create("1"), response.getMetadata().getFirst("Location"));

        WebhookEntity returnedWebhookEntity = (WebhookEntity) response.getEntity();
        assertEquals(1L, returnedWebhookEntity.getId());
        assertEquals("http://example.test/mywebhook", returnedWebhookEntity.getEndpointUrl());
    }

    @Test (expected = InvalidWebhookEndpointException.class)
    public void registerWebhookShouldFailWhenUrlEndpointIsInvalid() throws Exception
    {
        when(webhooksController.registerWebhook(APP_NAME, "{", "secret"))
                .thenThrow(new InvalidWebhookEndpointException("{", "because curly brackets are not allowed"));

        WebhookEntity webhookEntity = new WebhookEntity("{", "secret");
        resource.registerWebhook(webhookEntity);
    }

    @Test
    public void registerWebhookWithSimpleBodySuccessfully() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getId()).thenReturn(1L);
        when(webhook.getEndpointUrl()).thenReturn("http://example.test/mywebhook");
        when(webhooksController.registerWebhook(APP_NAME, "http://example.test/mywebhook", null)).thenReturn(webhook);

        Response response = resource.registerWebhookWithSimpleBody("http://example.test/mywebhook");

        assertEquals(201, response.getStatus());
        assertEquals(URI.create("1"), response.getMetadata().getFirst("Location"));
    }

    @Test (expected = InvalidWebhookEndpointException.class)
    public void registerWebhookWithSimpleBodyShouldFailWhenUrlEndpointIsInvalid() throws Exception
    {
        when(webhooksController.registerWebhook(APP_NAME, "{", null))
                .thenThrow(new InvalidWebhookEndpointException("{", "because curly braces are not allowed"));

        resource.registerWebhookWithSimpleBody("{");
    }

    @Test
    public void unregisterWebhookSuccessfully() throws Exception
    {
        Response response = resource.unregisterWebhook(1L);

        assertEquals(204, response.getStatus());

        verify(webhooksController).unregisterWebhook(APP_NAME, 1L);
    }

    @Test (expected = ApplicationPermissionException.class)
    public void unregisterWebhookWhenApplicationDoesNotOwnTheWebhook() throws Exception
    {
        doThrow(new ApplicationPermissionException("failed")).when(webhooksController).unregisterWebhook(APP_NAME, 1L);

        resource.unregisterWebhook(1L);
    }

    @Test (expected = WebhookNotFoundException.class)
    public void unregisterNonExistentWebhook() throws Exception
    {
        doThrow(new WebhookNotFoundException(1L)).when(webhooksController).unregisterWebhook(APP_NAME, 1L);

        resource.unregisterWebhook(1L);
    }
}
