package com.atlassian.crowd.trusted;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.service.UserService;
import com.atlassian.security.auth.trustedapps.filter.AuthenticationController;

import com.google.common.base.Preconditions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementation of {@link com.atlassian.security.auth.trustedapps.filter.AuthenticationController} for TrustedApps.
 *
 * @since v2.7
 */
public class CrowdTrustedAppAuthenticationController implements AuthenticationController
{
    private static final Logger log = LoggerFactory.getLogger(CrowdTrustedAppAuthenticationController.class);

    private final UserService userService;
    private final ApplicationManager applicationManager;
    private final ApplicationService applicationService;

    public CrowdTrustedAppAuthenticationController(final UserService userService, final ApplicationManager applicationManager, final ApplicationService applicationService)
    {
        this.userService = userService;
        this.applicationManager = checkNotNull(applicationManager);
        this.applicationService = checkNotNull(applicationService);
    }

    @Override
    public boolean shouldAttemptAuthentication(final HttpServletRequest request)
    {
        return userService.getAuthenticatedUsername(request) == null;
    }

    @Override
    public boolean canLogin(final Principal principal, final HttpServletRequest request)
    {
        try
        {
            final Application application = applicationManager.findByName(ApplicationType.CROWD.getDisplayName());
            return applicationService.isUserAuthorised(application, principal.getName());

        }
        catch (ApplicationNotFoundException e)
        {
            log.error("Failed to retrieve the 'Crowd' application", e);
            return false;
        }
    }
}
