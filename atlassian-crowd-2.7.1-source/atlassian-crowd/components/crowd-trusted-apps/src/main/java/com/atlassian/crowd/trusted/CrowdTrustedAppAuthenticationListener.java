package com.atlassian.crowd.trusted;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.service.UserService;
import com.atlassian.security.auth.trustedapps.filter.AuthenticationListener;
import com.atlassian.security.auth.trustedapps.filter.Authenticator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementation of {@link com.atlassian.security.auth.trustedapps.filter.AuthenticationListener} for Trusted Apps.
 *
 * @since v2.7
 */
public class CrowdTrustedAppAuthenticationListener implements AuthenticationListener
{
    private static final Logger log = LoggerFactory.getLogger(CrowdTrustedAppAuthenticationListener.class);

    private final UserService userService;

    public CrowdTrustedAppAuthenticationListener(final UserService userService)
    {
        this.userService = checkNotNull(userService);
    }

    public void authenticationSuccess(final Authenticator.Result result, final HttpServletRequest request, final HttpServletResponse response)
    {
        final String username = result.getUser().getName();
        if (username != null)
        {
            if (!userService.setAuthenticatedUser(username))
            {
                log.warn("Error while authenticating user '{}'", username);
            }
        }
    }

    public void authenticationFailure(final Authenticator.Result result, final HttpServletRequest request, final HttpServletResponse response)
    {
    }

    public void authenticationError(final Authenticator.Result result, final HttpServletRequest request, final HttpServletResponse response)
    {
    }

    public void authenticationNotAttempted(final HttpServletRequest request, final HttpServletResponse response)
    {
    }
}
