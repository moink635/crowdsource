package com.atlassian.crowd.trusted;

import java.security.Principal;

import com.atlassian.crowd.service.UserService;
import com.atlassian.security.auth.trustedapps.ApplicationCertificate;
import com.atlassian.security.auth.trustedapps.UserResolver;

/**
 *
 * @since v2.7
 */
public class CrowdUserResolver implements UserResolver
{
    private final UserService userService;

    public CrowdUserResolver(final UserService userService)
    {
        this.userService = userService;
    }

    @Override
    public Principal resolve(final ApplicationCertificate certificate)
    {
        return userService.resolve(certificate.getUserName());
    }
}
