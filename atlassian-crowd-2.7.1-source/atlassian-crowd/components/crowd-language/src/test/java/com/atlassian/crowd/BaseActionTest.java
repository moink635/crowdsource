package com.atlassian.crowd;

import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class BaseActionTest
{
    static String[] propertiesResources = {
        "/com/atlassian/crowd/console/action/BaseAction.properties",
        "/com/atlassian/crowd/openid/client/action/BaseAction.properties",
        "/com/atlassian/crowd/openid/server/action/BaseAction.properties",
        "/com/atlassian/crowd/security/demo/action/BaseAction.properties",
    };

    @Test
    public void escapedProperties() throws Exception
    {
        for (String p : propertiesResources)
        {
            assertCorrectEscapingIn(p);
        }
    }

    /**
     * Only some properties are used as message formats. Check for ones that include curly braces
     * for parameters and verify that they escape at least some of their apostrophes.
     */
    public void assertCorrectEscapingIn(String propResource) throws IOException
    {
        for (Map.Entry<String, String> e : asMap(propResource).entrySet())
        {
            String fmt = e.getValue();

            if (fmt.contains("{")) // Probably a message format
            {
                String result = MessageFormat.format(fmt, new Object[0]);
                if (fmt.contains("'"))
                {
                    assertTrue("Apostrophes should be escaped for format: " + e.getKey(), result.contains("'"));
                }
            }
        }
    }

    @Test
    public void baseActionPropertiesCanBeLoadedAsBundle()
    {
        ResourceBundle i18n = ResourceBundle.getBundle("com.atlassian.crowd.console.action.BaseAction", Locale.US);

        assertEquals("Atlassian Crowd", i18n.getString("application.title"));
    }

    @Test
    public void parameterIdentifiersAreContiguous() throws Exception
    {
        Pattern p = Pattern.compile("\\{(\\d+)\\}");

        for (String propResource : propertiesResources)
        {
            for (Map.Entry<String, String> e : asMap(propResource).entrySet())
            {
                String fmt = e.getValue();

                Matcher m = p.matcher(fmt);

                Set<Integer> ids = new HashSet<Integer>();

                while (m.find())
                {
                    ids.add(Integer.parseInt(m.group(1)));
                }

                if (!ids.isEmpty())
                {
                    assertThat(ids, Matchers.hasItem(0));
                    assertThat(e.getKey().toString(), ids, Matchers.hasItem(ids.size() - 1));
                }
            }
        }
    }

    private Map<String, String> asMap(String propResource) throws IOException
    {
        InputStream in = getClass().getResourceAsStream(propResource);
        assertNotNull("Test requires resource " + propResource, in);

        Properties props = new Properties();
        props.load(in);

        Map<String, String> underlying = new HashMap<String, String>();

        Map m = Collections.checkedMap(underlying, String.class, String.class);
        m.putAll(props);

        return underlying;
    }
}
