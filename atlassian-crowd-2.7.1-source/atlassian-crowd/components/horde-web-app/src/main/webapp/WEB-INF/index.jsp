<%@ page pageEncoding="utf-8" %><!DOCTYPE html>
<html>
<head>
    <title>Atlassian Crowd Horde</title>
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/aui/5.2/css/aui.css" />
</head>

<body class="aui-page-fixed" id="aui-flatpack">
    <div id="page">
        <header id="header">
            <nav class="aui-header aui-dropdown2-trigger-group">
                <div class="aui-header-inner aui-header-primary">
                    <h1 class="aui-header-logo aui-header-logo-crowd" id="logo"><a href="."><span class=
                    "aui-header-logo-device">Crowd</span></a></h1>
                </div>
            </nav>
        </header>

        <section id="content">
            <header class="aui-page-header">
                <div class="aui-page-header-inner">
                    <div class="aui-page-header-main">
                        <h1>Atlassian Crowd Horde</h1>
                    </div>
                </div>
            </header>

            <div class="aui-page-panel">
                <div class="aui-page-panel-inner">
                    <section class="aui-page-panel-content">
						<p>Hello, world! Unleash the hordes!</p>
						
						<p>Health Check status:
							<span class="aui-lozenge aui-lozenge-<%= request.getAttribute("healthCheckLozenge") %>">
							     <%= request.getAttribute("healthCheckResult") %>
							</span>
							(<a href="rest/healthcheck/checkDetails">details</a>)
					    </p>
						
						<p>Horde implements</p>
						<ul>
						<li><a href="https://developer.atlassian.com/display/CROWDDEV/Crowd+REST+Resources">usermanagement</a> - /rest/usermanagement/1</li>
						</ul>
						
                    </section>
                </div>
            </div>
        </section>

        <footer id="footer">
            <section class="footer-body">
                <ul id="aui-footer-list">
                    <li>Atlassian Crowd Horde <%= com.atlassian.crowd.util.build.BuildUtils.getVersion() %></li>
                    <li>Copyright &copy; 2006-2013 Atlassian</li>
                </ul>
            </section>
        </footer>
    </div>


</body>
</html>
