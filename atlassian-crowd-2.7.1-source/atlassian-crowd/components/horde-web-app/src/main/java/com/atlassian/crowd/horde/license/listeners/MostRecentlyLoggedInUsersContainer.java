package com.atlassian.crowd.horde.license.listeners;

import java.util.Iterator;

import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.collect.Iterators;
import com.google.common.collect.MinMaxPriorityQueue;

/**
 * A data structure that contains up to SIZE_CAP users.
 * If the data structure is full and an attempt to add a new user is made, then the user with the earliest login time is kicked out (which could be the one that is being added).
 * This implementation relies on a
 * <a href="http://docs.guava-libraries.googlecode.com/git/javadoc/com/google/common/collect/MinMaxPriorityQueue.html">MinMaxPriorityQueue</a>.
 */
public class MostRecentlyLoggedInUsersContainer implements Iterable<String>
{
    /**
     * Cap of number of users held in memory by this data structure.
     */
    @VisibleForTesting
    static final int SIZE_CAP = 20;
    private final MinMaxPriorityQueue<UserWithScore> queue;

    private static class UserWithScore implements Comparable<UserWithScore>
    {
        private final String username;
        private final long score;

        UserWithScore(final String username, final long score)
        {
            this.username = username;
            this.score = score;
        }

        @Override
        public int compareTo(final UserWithScore that)
        {
            if (that.score == this.score)
            {
                return 0;
            }
            if (that.score > this.score)
            {
                return 1;
            }
            return -1;
        }
    }

    public MostRecentlyLoggedInUsersContainer(final int tentativeMaxSize)
    {
        int maximumSize = Math.min(tentativeMaxSize, SIZE_CAP);
        queue = MinMaxPriorityQueue.maximumSize(maximumSize).<UserWithScore>create();
    }

    public boolean addUser(final String username, final String lastLoginTime)
    {
        long score;
        if (lastLoginTime == null)
        {
            score = 0;
        }
        else
        {
            try
            {
                score = Long.parseLong(lastLoginTime);
            }
            catch (NumberFormatException e)
            {
                score = 0;
            }
        }

        return queue.add(new UserWithScore(username, score));
    }

    @Override
    public Iterator<String> iterator()
    {
        return Iterators.transform(queue.iterator(), new Function<UserWithScore, String>()
        {
            @Override
            public String apply(@Nullable final UserWithScore input)
            {
                return input.username;
            }
        });
    }
}
