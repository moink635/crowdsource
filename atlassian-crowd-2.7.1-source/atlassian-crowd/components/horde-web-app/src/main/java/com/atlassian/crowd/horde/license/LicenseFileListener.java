package com.atlassian.crowd.horde.license;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.vfs2.FileChangeEvent;
import org.apache.commons.vfs2.FileListener;
import org.apache.commons.vfs2.FileName;
import org.apache.commons.vfs2.FileSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.crowd.horde.license.LicenseableApplication.fromLicenseKey;
import static com.google.common.base.Preconditions.checkNotNull;

public class LicenseFileListener implements FileListener
{
    private static final Logger log = LoggerFactory.getLogger(LicenseFileListener.class);

    private volatile LicenseChangedEvent lastChangedEvent;
    private final LicenseMonitorService licenseMonitorService;

    public LicenseFileListener(LicenseMonitorServiceImpl licenseMonitorService)
    {
        this.licenseMonitorService = licenseMonitorService;
        this.lastChangedEvent = null;
    }

    public LicenseChangedEvent getLastChangedEvent()
    {
        return lastChangedEvent;
    }

    @Override
    public void fileCreated(FileChangeEvent fileChangeEvent) throws Exception
    {
        if (isDecodedLicenseFile(fileChangeEvent))
        {
            log.info(fileChangeEvent.getFile().getName() + " was created.");
            readAndNotifyFromEvent(fileChangeEvent);
        }
    }

    @Override
    public void fileDeleted(FileChangeEvent fileChangeEvent) throws Exception
    {
        if (isDecodedLicenseFile(fileChangeEvent))
        {
            final FileName fileName = fileChangeEvent.getFile().getName();
            if (lastChangedEvent == null)
            {
                log.error(
                    "{} was deleted. We have no idea what the license is supposed to be and nothing will be notified as a result.",
                    fileName);
            }
            else
            {
                log.warn("The file {} was deleted but we still retain the last read license in memory.", fileName);
            }
        }
    }

    @Override
    public void fileChanged(FileChangeEvent fileChangeEvent) throws Exception
    {
        if (isDecodedLicenseFile(fileChangeEvent))
        {
            log.info(fileChangeEvent.getFile().getName() + " was modified.");
            readAndNotifyFromEvent(fileChangeEvent);
        }
    }

    private static boolean isDecodedLicenseFile(FileChangeEvent fileChangeEvent)
    {
        return fileChangeEvent.getFile().getName().getBaseName().equals(LicenseMonitorServiceImpl.DECODED_LICENSE_FILE);
    }

    /**
     * This method must be synchronised because we need to be sure that every read and notification of a changed license
     * happens before any other read and notification event.
     *
     * @param inputStream The input stream that you should read the license details from.
     */
    @VisibleForTesting
    synchronized void readAndNotify(InputStream inputStream)
    {
        final Optional<LicenseChangedEvent> potentialLicense = readLicense(inputStream);
        if (potentialLicense.isPresent())
        {
            notifyOnChange(potentialLicense.get());
        }
    }

    private static Optional<LicenseChangedEvent> readLicense(InputStream inputStream)
    {
        BufferedReader bufferedReader = null;

        try
        {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            final ImmutableSet.Builder<LicenseableApplication> licenseableApplications = ImmutableSet.builder();
            String currentLine;
            while ((currentLine = bufferedReader.readLine()) != null)
            {
                currentLine = currentLine.trim();
                if (StringUtils.isNotBlank(currentLine))
                {
                    final Optional<LicenseableApplication> potentialApplication = fromLicenseKey(currentLine);

                    if (potentialApplication.isPresent())
                    {
                        licenseableApplications.add(potentialApplication.get());
                    }
                    else
                    {
                        log.error(
                            "Encountered an application string {} that was not a known Licenseable Application. Nothing will be alerted that this application is active.",
                            currentLine);
                    }
                }
            }
            return Optional.of(new LicenseChangedEvent(licenseableApplications.build()));
        }
        catch (IOException e)
        {
            log.error("Encountered an IO error when attempting to read the decoded license.", e);
        }
        finally
        {
            IOUtils.closeQuietly(bufferedReader);
        }

        return Optional.absent();
    }

    private synchronized void notifyOnChange(LicenseChangedEvent newLicense)
    {
        checkNotNull(newLicense);
        if (newLicense.equals(lastChangedEvent))
        {
            log.debug("The set of licensed applications remains unchanged. Notifying nobody.");
        }
        else
        {
            log.info("The set of licensed applications has changed. Notifying all listeners.");
            lastChangedEvent = newLicense;
            licenseMonitorService.forceNotification();
        }
    }

    private void readAndNotifyFromEvent(FileChangeEvent fileChangeEvent) throws FileSystemException
    {
        readAndNotify(fileChangeEvent.getFile().getContent().getInputStream());
    }

    public void readAndNotifyFromFile(File decodedLicenseFile)
    {
        try
        {
            readAndNotify(new FileInputStream(decodedLicenseFile));
        }
        catch (FileNotFoundException e)
        {
            log.error("Important, we could not read the license file: " + decodedLicenseFile.getAbsolutePath(), e);
        }
    }
}
