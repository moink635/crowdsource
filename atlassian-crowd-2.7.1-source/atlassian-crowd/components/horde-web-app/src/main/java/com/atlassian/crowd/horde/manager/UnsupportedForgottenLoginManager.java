package com.atlassian.crowd.horde.manager;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidEmailAddressException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.directory.DirectoryPermissionException;
import com.atlassian.crowd.manager.login.ForgottenLoginManager;
import com.atlassian.crowd.manager.login.exception.InvalidResetPasswordTokenException;
import com.atlassian.crowd.model.application.Application;

/**
 * Horde does not reset passwords.
 */
public class UnsupportedForgottenLoginManager implements ForgottenLoginManager
{
    @Override
    public void sendResetLink(Application application, String username)
        throws UserNotFoundException, InvalidEmailAddressException, ApplicationPermissionException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean sendUsernames(Application application, String email) throws InvalidEmailAddressException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void sendResetLink(long directoryId, String username)
        throws DirectoryNotFoundException, UserNotFoundException, InvalidEmailAddressException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isValidResetToken(long directoryId, String username, String token)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void resetUserCredential(long directoryId, String username, PasswordCredential credential, String token)
        throws DirectoryNotFoundException, UserNotFoundException, InvalidResetPasswordTokenException,
               OperationFailedException, InvalidCredentialException, DirectoryPermissionException
    {
        throw new UnsupportedOperationException("Not implemented");
    }
}
