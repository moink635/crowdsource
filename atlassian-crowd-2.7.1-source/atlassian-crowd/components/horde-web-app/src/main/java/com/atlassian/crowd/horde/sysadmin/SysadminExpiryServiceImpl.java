package com.atlassian.crowd.horde.sysadmin;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.api.UserWithAttributes;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.security.random.SecureTokenGenerator;

import com.google.common.annotations.VisibleForTesting;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SysadminExpiryServiceImpl implements SysadminExpiryService
{
    private static final Logger log = LoggerFactory.getLogger(SysadminExpiryServiceImpl.class);

    @VisibleForTesting
    static final String SYSADMIN_USERNAME_PROP_NAME = "horde.ondemand.sysadmin.username";
    @VisibleForTesting
    static final String SYSADMIN_EXPIRY_PERIOD_PROP_NAME = "horde.ondemand.sysadmin.expiryperiodmillis";
    @VisibleForTesting
    static final String SYSADMIN_EXPIRY_ENABLED_PROP_NAME = "horde.ondemand.sysadmin.expiry.enabled";

    @VisibleForTesting
    static final String DEFAULT_SYSADMIN_USERNAME = "sysadmin";
    @VisibleForTesting
    static final long DEFAULT_EXPIRY_PERIOD_MS = TimeUnit.HOURS.toMillis(12);

    private static final String SYSADMIN_USERNAME = System.getProperty(SYSADMIN_USERNAME_PROP_NAME,
        DEFAULT_SYSADMIN_USERNAME);

    private static final long EXPIRY_PERIOD_MS = Long.getLong(SYSADMIN_EXPIRY_PERIOD_PROP_NAME,
        DEFAULT_EXPIRY_PERIOD_MS);

    private static final boolean EXPIRY_ENABLED = Boolean.valueOf(System.getProperty(SYSADMIN_EXPIRY_ENABLED_PROP_NAME, "true"));

    /**
     * For testing purposes we need a date factory that can give us the current date and, in tests, can give us back
     * whichever time we please.
     */
    @VisibleForTesting
    static class DateFactory
    {
        public long getNow()
        {
            return System.currentTimeMillis();
        }
    }

    private final CrowdService crowdService;
    private final SecureTokenGenerator secureTokenGenerator;
    private final DateFactory dateFactory;
    private final AtomicLong cachedExpiryTime;
    private final AtomicBoolean databaseExpiryLoaded;

    public SysadminExpiryServiceImpl(CrowdService crowdService, SecureTokenGenerator secureTokenGenerator)
    {
        this(crowdService, secureTokenGenerator, new DateFactory());
    }

    @VisibleForTesting
    SysadminExpiryServiceImpl(CrowdService crowdService, SecureTokenGenerator secureTokenGenerator,
                              DateFactory dateFactory)
    {
        this.crowdService = crowdService;
        this.secureTokenGenerator = secureTokenGenerator;
        this.dateFactory = dateFactory;
        this.cachedExpiryTime = new AtomicLong(Long.MAX_VALUE);
        this.databaseExpiryLoaded = new AtomicBoolean(false);

        log.info("Sysadmin password expiry enabled: {} (expiry time: {}ms)", EXPIRY_ENABLED, EXPIRY_PERIOD_MS);
    }

    @VisibleForTesting
    Long getCachedExpiryTime()
    {
        return cachedExpiryTime.get();
    }

    @Override
    public void checkAndExpireSysadminPassword()
    {
        if (EXPIRY_ENABLED)
        {
            if (!databaseExpiryLoaded.getAndSet(true))
            {
                // Lazily reading the last time the password was changed from the DB so that we are guaranteed to be
                // inside a transaction.
                refreshExpiryTimeFromDB(crowdService, dateFactory);
            }

            final long now = dateFactory.getNow();
            if (passwordHasExpired(now))
            {
                try
                {
                    final Long exceptionOrNewExpiryTime = expireSysadminPassword(now);
                    log.info("Expired the current sysadmin password because the expiry time was met.");
                    cachedExpiryTime.set(exceptionOrNewExpiryTime);
                }
                catch (Exception e)
                {
                    log.error("Could not expire password.", e);
                }
            }
            else
            {
                log.debug("Did not expire the sysadmin password because the expiry time is: {}", cachedExpiryTime.get());
            }
        }
        else
        {
            log.debug("Sysadmin password expiry is disabled, not checking timestamps.");
        }
    }

    private boolean passwordHasExpired(long now) {
        return (cachedExpiryTime.get() < now && refreshExpiryTimeFromDB(crowdService, dateFactory) < now);
    }

    private long refreshExpiryTimeFromDB(CrowdService crowdService, DateFactory dateFactory)
    {
        cachedExpiryTime.set(readExpiryTimeFromDB(crowdService, dateFactory));
        log.debug("Refreshed expiry time of sysadmin password based on database value is {}", cachedExpiryTime.get());
        return cachedExpiryTime.get();
    }

    private static long readExpiryTimeFromDB(CrowdService crowdService, DateFactory dateFactory)
    {
        Long expiryTime = null;
        try
        {
            expiryTime = computeExpiryTimeFromPasswordLastChangedTime(getLastSysadminChange(crowdService));
        }
        catch (NumberFormatException e)
        {
            log.warn("The sysadmin expiry time could not be read, setting the expiry time to now.", e);
        }

        // If the values comes out of the previous processing null then set it to the current time
        if (expiryTime == null)
        {
            expiryTime = dateFactory.getNow();
            log.warn("The expiry time has been set to now.");
        }

        return expiryTime;
    }

    private static long computeExpiryTimeFromPasswordLastChangedTime(long oldExpiry)
    {
        return oldExpiry + EXPIRY_PERIOD_MS;
    }

    /**
     * Expire the sysadmin password
     * @param now
     * @return Either an Exception, or the expiry time for the new password
     */
    private Long expireSysadminPassword(long now) throws Exception
    {
        try
        {
            final String newPassword = secureTokenGenerator.generateToken();
            User sysadminUser = crowdService.getUser(SYSADMIN_USERNAME);
            crowdService.updateUserCredential(sysadminUser, newPassword);
            return computeExpiryTimeFromPasswordLastChangedTime(now);
        }
        catch (CrowdException e)
        {
            throw new Exception("Encountered a crowd problem trying to expire the sysadmin password.", e);
        }
    }

    private static long getLastSysadminChange(CrowdService crowdService)
    {
        final UserWithAttributes user = crowdService.getUserWithAttributes(SYSADMIN_USERNAME);
        final String lastchangedProperty = user.getValue(UserConstants.PASSWORD_LASTCHANGED);
        if (StringUtils.isBlank(lastchangedProperty))
        {
            log.error("The PASSWORD_LASTCHANGED property for the sysadmin was empty. Crowd is supposed to prevent this.");
            // Return the beginning of time so that we actually change the password and fix this up
            return 0;
        }

        return Long.parseLong(lastchangedProperty);
    }

}
