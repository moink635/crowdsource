package com.atlassian.crowd.horde.sysadmin;

/**
 * A service responsible for expiring the sysadmin password in ondemand.
 */
public interface SysadminExpiryService
{
    /**
     * This method should check to see if the sysadmin password has expired and, if that is the case, then it should
     * expire the sysadmin password such that the sysadmin user cannot be accessed without setting another sysadmin
     * password.
     */
    void checkAndExpireSysadminPassword();
}
