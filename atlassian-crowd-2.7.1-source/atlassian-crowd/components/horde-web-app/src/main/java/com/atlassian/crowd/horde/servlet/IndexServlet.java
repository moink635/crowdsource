package com.atlassian.crowd.horde.servlet;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.healthcheck.core.HealthCheckManager;
import com.atlassian.healthcheck.core.HealthStatus;

import com.google.common.base.Predicate;

import org.springframework.web.HttpRequestHandler;

import static com.google.common.collect.Iterables.all;
import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAVAILABLE;


public class IndexServlet implements HttpRequestHandler
{

    private final HealthCheckManager healthCheckManager;

    public IndexServlet(HealthCheckManager healthCheckManager)
    {
        this.healthCheckManager = healthCheckManager;
    }

    @Override
    public void handleRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        boolean skipHealthCheck = Boolean.valueOf(request.getParameter("skipHealthCheck"));

        if (skipHealthCheck)
        {
            request.setAttribute("healthCheckResult",  "skipped");
            request.setAttribute("healthCheckLozenge",  "default");
            response.setStatus(HTTP_OK);
        }
        else
        {
            boolean healthy = isHealthy();
            request.setAttribute("healthCheckResult",  healthy ? "pass" : "fail");
            request.setAttribute("healthCheckLozenge",  healthy ? "success" : "error");
            response.setStatus(healthy ? HTTP_OK : HTTP_UNAVAILABLE);
        }

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("WEB-INF/index.jsp");
        requestDispatcher.forward(request, response);
    }

    /**
     * @return true if all health checks are healthy or there are no registered health checks; false otherwise.
     */
    private boolean isHealthy()
    {
        return all(healthCheckManager.performChecks(), new Predicate<HealthStatus>() {
            @Override
            public boolean apply(HealthStatus input)
            {
                return input.isHealthy();
            }
        });
    }

}