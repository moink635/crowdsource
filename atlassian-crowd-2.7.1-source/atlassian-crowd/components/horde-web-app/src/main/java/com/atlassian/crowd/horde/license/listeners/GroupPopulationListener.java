package com.atlassian.crowd.horde.license.listeners;

import java.util.Set;
import java.util.concurrent.ExecutorService;

import com.atlassian.crowd.horde.license.LicenseChangeListener;
import com.atlassian.crowd.horde.license.LicenseChangedEvent;
import com.atlassian.crowd.horde.license.LicenseableApplication;
import com.atlassian.crowd.horde.util.DelayedLoggingExceptionHandler;
import com.atlassian.crowd.service.factory.CrowdClientFactory;
import com.atlassian.fugue.retry.ExceptionHandler;
import com.atlassian.fugue.retry.RetryFactory;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.crowd.horde.license.LicenseableApplication.CROWD;
import static com.atlassian.crowd.horde.license.LicenseableApplication.FECRU;

public class GroupPopulationListener implements LicenseChangeListener
{
    private static final Logger log = LoggerFactory.getLogger(GroupPopulationListener.class);
    private static final Set<LicenseableApplication> IGNORED_APPLICATIONS = ImmutableSet.of(CROWD, FECRU);

    private Optional<LicenseChangedEvent> lastLicenseEvent;

    private final CrowdClientFactory restCrowdClientFactory;
    private final ExecutorService executor;
    private final CommonListenerProperties listenerProperties;

    public GroupPopulationListener(CrowdClientFactory restCrowdClientFactory, ExecutorService executor)
    {
        this(restCrowdClientFactory, executor, CommonListenerPropertiesImpl.INSTANCE);
    }

    GroupPopulationListener(CrowdClientFactory restCrowdClientFactory, ExecutorService executor,
                            CommonListenerProperties listenerProperties)
    {
        this.restCrowdClientFactory = restCrowdClientFactory;
        this.executor = executor;
        this.listenerProperties = listenerProperties;
        lastLicenseEvent = Optional.absent();
    }

    @Override
    public synchronized void onUpdate(LicenseChangedEvent event)
    {
        // Work out which applications were modified
        final LicenseChangedEvent lastEvent = lastLicenseEvent.or(LicenseChangedEvent.NO_APPS);

        // If this is not the first update then we want to just see what has changed in the license.
        final ImmutableSet<LicenseableApplication> addedApplications = Sets.difference(event.getLicensedApps(),
            lastEvent.getLicensedApps()).immutableCopy();

        // Get the list of applications that should not be ignored.
        final Set<LicenseableApplication> appsToRegister = Sets.difference(addedApplications, IGNORED_APPLICATIONS);

        // Now send off registration events for all of those apps
        triggerGroupModifications(appsToRegister);

        lastLicenseEvent = Optional.of(event);
    }

    private void triggerGroupModifications(Set<LicenseableApplication> appsToActUpon)
    {
        final Runnable productRegister = new AdminsToGroupsRunnable(restCrowdClientFactory, appsToActUpon,
            listenerProperties);

        final ExceptionHandler loggingHandler = DelayedLoggingExceptionHandler.create(log,
            listenerProperties.getRetries());
        final Runnable registerRunner = RetryFactory.create(productRegister, listenerProperties.getRetries(),
            loggingHandler, listenerProperties.getRetryDelay());

        executor.execute(registerRunner);
    }
}
