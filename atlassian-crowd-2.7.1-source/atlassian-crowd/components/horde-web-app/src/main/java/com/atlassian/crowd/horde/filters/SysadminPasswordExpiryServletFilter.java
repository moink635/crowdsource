package com.atlassian.crowd.horde.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.atlassian.crowd.horde.sysadmin.SysadminExpiryService;

/**
 * This class is responsible for expiring the sysadmin credentials when they have outlived their period of use.
 */
public class SysadminPasswordExpiryServletFilter implements Filter
{
    private final SysadminExpiryService sysadminExpiryService;

    public SysadminPasswordExpiryServletFilter(SysadminExpiryService sysadminExpiryService)
    {
        this.sysadminExpiryService = sysadminExpiryService;
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public final void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException
    {
        // Attempt to expire the sysadmin password
        sysadminExpiryService.checkAndExpireSysadminPassword();

        // Always pass the request down the chain when you are done
        filterChain.doFilter(request, response);
    }
}
