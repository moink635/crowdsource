package com.atlassian.crowd.horde.rest.service.resource;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

/**
 * Provides a dummy equivalent of {@link com.atlassian.ondemand.license.rest.LicenseResource} that simply returns OK.
 * <p>
 * In Horde, the license is reloaded from the file system periodically, so pings to this URI are not required. The
 * resource exists only to prevent clients that expect this resource to exist (namely
 * {@link com.atlassian.ondemand.license.LicenseLoadMulticaster}) from erroring out.
 * <p>
 * This resource can be removed when ODC-95 is implemented.
 */
@AnonymousAllowed
@Path("license")
@Produces("text/plain")
public class DummyLicenseResource
{
    private static final Response OK_RESPONSE = Response.ok("This is a deprecated, dummy resource. Requesting it has no effect. "
                                                            + "In Horde, the license is reloaded from the file system periodically. See JSTDEV-2849.").build();

    /**
     * Only returns HTTP OK.
     */
    @GET
    @Path("load")
    public Response get()
    {
        return OK_RESPONSE;
    }

    /**
     * Only returns HTTP OK.
     */
    @PUT
    @Path("load")
    public Response put()
    {
        return OK_RESPONSE;
    }

    /**
     * Only returns HTTP OK.
     */
    @POST
    @Path("load")
    public Response post()
    {
        return OK_RESPONSE;
    }

}
