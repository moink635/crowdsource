package com.atlassian.crowd.horde.manager;

import com.atlassian.crowd.directory.SynchronisableDirectory;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.directory.DirectorySynchroniser;
import com.atlassian.crowd.manager.directory.SynchronisationMode;

public class NoopDirectorySynchroniser implements DirectorySynchroniser
{
    @Override
    public void synchronise(SynchronisableDirectory synchronisableDirectory, SynchronisationMode mode)
        throws DirectoryNotFoundException, OperationFailedException
    {
        // no op
    }

    @Override
    public boolean isSynchronising(long directoryId) throws DirectoryNotFoundException
    {
        return false;
    }
}
