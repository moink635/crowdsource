package com.atlassian.crowd.horde.license;

import com.google.common.base.Optional;

public enum LicenseableApplication
{
    CROWD("crowd.active", "crowd", "/crowd"),
    JIRA("jira.active", "jira", ""),
    CONFLUENCE("conf.active", "confluence", "/wiki"),
    BAMBOO("bamboo.active", "bamboo", "/builds"),
    FECRU("crucible.active", "crucible", null);

    private final String licenseKey, applicationName, context;

    private LicenseableApplication(String licenseKey, String applicationName, String context)
    {
        this.licenseKey = licenseKey;
        this.applicationName = applicationName;
        this.context = context;
    }

    public String getLicenseKey()
    {
        return licenseKey;
    }

    public String getApplicationName()
    {
        return applicationName;
    }

    public String getNotificationUri(String endpoint) throws IllegalStateException
    {
        if (context == null)
        {
            throw new IllegalStateException(
                "Should not be attempting to create Webhooks for product: " + getApplicationName());
        }
        return context + endpoint;
    }

    public static Optional<LicenseableApplication> fromLicenseKey(String licenseKey)
    {
        if (licenseKey.equals("fisheye.active"))
        {
            return Optional.of(FECRU);
        }

        for (LicenseableApplication application : LicenseableApplication.values())
        {
            if (application.getLicenseKey().equals(licenseKey))
            {
                return Optional.of(application);
            }
        }
        return Optional.absent();
    }
}
