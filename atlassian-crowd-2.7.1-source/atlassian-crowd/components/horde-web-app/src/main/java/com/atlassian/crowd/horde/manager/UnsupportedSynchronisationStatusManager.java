package com.atlassian.crowd.horde.manager;

import java.io.Serializable;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectorySynchronisationInformation;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;

/**
 * Horde does not synchronise remote directories, therefore it does not offer synchronisation status information.
 */
public class UnsupportedSynchronisationStatusManager implements SynchronisationStatusManager
{
    @Override
    public void syncStarted(Directory directory)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void syncStatus(long directoryId, String key, Serializable... parameters)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void syncFinished(long directoryId)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public DirectorySynchronisationInformation getDirectorySynchronisationInformation(Directory directory)
    {
        throw new UnsupportedOperationException("Not implemented");
    }
}
