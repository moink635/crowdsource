package com.atlassian.crowd.horde.license.listeners;

/**
 * Provides common properties that listeners may need to be aware of.
 */
public interface CommonListenerProperties
{
    /**
     * @return The domain name of this instance.
     */
    String getDomain();

    /**
     * @return The base URL of this instance.
     */
    String getBaseUrl();

    /**
     *
     */
    String getCrowdPublicUrl();

    /**
     * The application name to use with the application password.
     *
     * @return The name of the application that you should use for processisng, if present. Returns the default applications
     *         name otherwise.
     */
    String getMaintainerApplicationName();

    /**
     * @return The application password for the Crowd applications.
     */
    String getApplicationPassword();

    /**
     * @return The context Path of horde.
     */
    String getContextPath();

    /**
     * @return The standard crowd notification endpoint in the applications.
     */
    String getCrowdNotifyEndpoint();

    /**
     * @return How many times you should attempt to retry a HTTP call.
     */
    int getRetries();

    /**
     * @return The delay, in milliseconds, that you should wait between failed HTTP calls.
     */
    long getRetryDelay();
}