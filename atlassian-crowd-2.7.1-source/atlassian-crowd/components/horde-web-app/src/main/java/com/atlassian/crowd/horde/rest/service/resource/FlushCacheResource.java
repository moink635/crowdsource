package com.atlassian.crowd.horde.rest.service.resource;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.atlassian.crowd.horde.cache.FlushCacheService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import static javax.ws.rs.core.MediaType.TEXT_HTML;
import static javax.ws.rs.core.MediaType.TEXT_PLAIN;

@Path("flushCache")
@AnonymousAllowed
public class FlushCacheResource
{
    private static final String HTML_FORM =
        "<html><body><form action='' method='post'><input type='submit' value='Flush cache'></form></body></html>";
    private static final Response FORM_RESPONSE = Response.ok(HTML_FORM).build();
    private static final Response FLUSH_CACHE_RESPONSE = Response.ok("Cache flushed").build();

    private final FlushCacheService flushCacheService;

    public FlushCacheResource(FlushCacheService flushCacheService)
    {
        this.flushCacheService = flushCacheService;
    }

    @GET
    @Produces( { TEXT_HTML })
    public Response showForm()
    {
        return FORM_RESPONSE;
    }

    @POST
    @Produces( { TEXT_PLAIN })
    public Response flushCache()
    {
        flushCacheService.cleanData();
        return FLUSH_CACHE_RESPONSE;
    }
}
