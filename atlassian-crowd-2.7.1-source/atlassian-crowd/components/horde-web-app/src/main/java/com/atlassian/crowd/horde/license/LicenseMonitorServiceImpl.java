package com.atlassian.crowd.horde.license;

import java.io.File;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Optional;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.apache.commons.vfs2.impl.DefaultFileMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The purpose of this service is to monitor the License File in OnDemand to watch for changes.
 */
public class LicenseMonitorServiceImpl implements LicenseMonitorService
{
    private static final Logger log = LoggerFactory.getLogger(LicenseMonitorServiceImpl.class);

    // System Properties
    private final static String LICENSE_DIRECTORY_SYSPROP = "horde.license.directory";
    private final static String LICENSE_POLL_DELAY_SYSPROP = "horde.license.delay";

    // Default Values
    private final static String DEFAULT_LICENSE_DIRECTORY = "/data/jirastudio/home/";
    final static String DECODED_LICENSE_FILE = "studio.license.decoded";
    private final static long DEFAULT_LICENSE_DELAY = TimeUnit.MINUTES.toMillis(5);

    // Actual Values
    private final static String LICENSE_DIRECTORY = System.getProperty(LICENSE_DIRECTORY_SYSPROP,
        DEFAULT_LICENSE_DIRECTORY);
    private final static Long POLLING_DELAY_MS = Long.getLong(LICENSE_POLL_DELAY_SYSPROP, DEFAULT_LICENSE_DELAY);

    private final List<LicenseChangeListener> listeners;
    private final LicenseFileListener licenseFileListener;
    private final DefaultFileMonitor fileMonitor;

    public LicenseMonitorServiceImpl()
    {
        listeners = new CopyOnWriteArrayList<LicenseChangeListener>();
        this.licenseFileListener = new LicenseFileListener(this);
        this.fileMonitor = new DefaultFileMonitor(licenseFileListener);
    }

    public void startMonitoring()
    {
        log.info("Starting the monitoring of the license file.");

        ensureLicenseFileMonitored();
        fileMonitor.setDelay(POLLING_DELAY_MS);
        // workaround for https://issues.apache.org/jira/browse/VFS-486
        fileMonitor.setChecksPerRun(0);
        fileMonitor.setRecursive(true);
        fileMonitor.start();

        log.info("Reading the license file and kicking an initial license notification off.");
        licenseFileListener.readAndNotifyFromFile(new File(LICENSE_DIRECTORY, DECODED_LICENSE_FILE));
    }

    public void setListeners(List<LicenseChangeListener> listeners)
    {
        for (LicenseChangeListener listener : listeners)
        {
            registerListener(listener);
        }
    }

    private void ensureLicenseFileMonitored()
    {
        try
        {
            final FileSystemManager manager = VFS.getManager();
            final FileObject licenseFile = manager.resolveFile(LICENSE_DIRECTORY);
            fileMonitor.addFile(licenseFile);
        }
        catch (FileSystemException e)
        {
            log.error("Could not monitor the license.", e);
        }
    }

    @Override
    public Optional<LicenseChangedEvent> getLastChangedEvent()
    {
        return Optional.fromNullable(licenseFileListener.getLastChangedEvent());
    }

    @Override
    public synchronized void registerListener(LicenseChangeListener licenseChangeListener)
    {
        // IMPORTANT: The License Listeners may need to perform DB operations. This wrapping process ensures that they
        // actually have a DB underneath them ready to serve the hibernate requests.
        listeners.add(licenseChangeListener);

        final LicenseChangedEvent lastChangedEvent = licenseFileListener.getLastChangedEvent();
        if (lastChangedEvent != null)
        {
            licenseChangeListener.onUpdate(lastChangedEvent);
        }
        else
        {
            log.debug(
                "A listener was added but it was not notified about the license status (because we don't know what license is present).");
        }
    }

    @Override
    public synchronized void forceNotification()
    {
        final LicenseChangedEvent lastChangedEvent = licenseFileListener.getLastChangedEvent();
        if (lastChangedEvent != null)
        {
            for (LicenseChangeListener listener : listeners)
            {
                listener.onUpdate(lastChangedEvent);
            }
        }
        else
        {
            log.error(
                "There was no previous LicenseChangedEvent, nobody will be notified and this may cause problems.");
        }
    }

    @VisibleForTesting
    LicenseFileListener getLicenseFileListener()
    {
        return licenseFileListener;
    }
}
