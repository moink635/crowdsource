package com.atlassian.crowd.horde.license;

import java.util.Set;

import javax.annotation.concurrent.Immutable;

import com.google.common.collect.ImmutableSet;

import static com.google.common.base.Preconditions.checkNotNull;

@Immutable
public class LicenseChangedEvent
{
    public static final LicenseChangedEvent NO_APPS = new LicenseChangedEvent(
        ImmutableSet.<LicenseableApplication>of());

    private final Set<LicenseableApplication> licensedApps;

    public LicenseChangedEvent(Set<LicenseableApplication> licensedApps)
    {
        checkNotNull(licensedApps);
        this.licensedApps = ImmutableSet.copyOf(licensedApps);
    }

    public Set<LicenseableApplication> getLicensedApps()
    {
        return licensedApps;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o == null) return false;
        if (this == o) return true;
        if (!(o instanceof LicenseChangedEvent)) return false;

        LicenseChangedEvent that = (LicenseChangedEvent) o;

        return licensedApps.equals(that.licensedApps);
    }

    @Override
    public int hashCode()
    {
        return licensedApps.hashCode();
    }
}
