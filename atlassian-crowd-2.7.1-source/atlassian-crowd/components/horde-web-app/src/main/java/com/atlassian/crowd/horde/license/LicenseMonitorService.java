package com.atlassian.crowd.horde.license;

import com.google.common.base.Optional;

public interface LicenseMonitorService
{
    /**
     * Gets the last license changed event that was sent. It is possible that the license has never been successfully read by this
     * point in time so please take that into account.
     *
     * @return A license changed event that was the last read (or absent if there were problems reading the license).
     */
    Optional<LicenseChangedEvent> getLastChangedEvent();

    /**
     * Add a listener to be notified of changes to the license. <br />
     * <br />
     * Important: Upon being added to this service the listener will be informed of the very last successful {@link LicenseChangedEvent}. Your listener
     * should be able to handle that correctly.
     *
     * @param licenseChangeListener An implementation of the listener interface. It will be called upon license change though there may be a delay.
     */
    void registerListener(LicenseChangeListener licenseChangeListener);

    /**
     * Force all the listeners to be notified of the last license change event.
     */
    void forceNotification();
}
