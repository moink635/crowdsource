package com.atlassian.crowd.horde.license.listeners;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.UserWithAttributes;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.horde.license.LicenseableApplication;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.service.client.CrowdClient;
import com.atlassian.crowd.service.factory.CrowdClientFactory;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.crowd.horde.license.listeners.ClientPropertiesUtils.generateMaintainerClientProperties;
import static com.google.common.base.Predicates.in;
import static com.google.common.base.Predicates.not;
import static com.google.common.collect.Iterables.filter;

/**
 * The Admin to Groups Runnable is responsible for adding the most recently accessed administrators to another
 * _licensed-product group so that the new application has a bunch of registered administrators when it first starts.
 * However, to do this it will need to make a number of HTTP requests against Crowd and this is a rought description of
 * those HTTP requests:
 * <ul>
 * <li>1*products Request to see if there are more than two people in any of the groups already for each product.
 * (If there are, for every product, then the requests stop here)</li>
 * <li>1-2 Rest Calls to get all of the administrators on the instance.</li>
 * <li>1-150 (Ave ~20) One request per administrator to get last used user attribute. (This expensive step could be removed
 * if you could specify, in the previous step, that the users should be returned with their attributes).</li>
 * <li>(1-20)*products One rest call to add each administrator to each product that is provided.</li>
 * </ul>
 * <p/>
 * N.B. As you can see, care has been taken to not let the number of HTTP requests made scale poorly with respect to the
 * number of products that are being added.
 */
public class AdminsToGroupsRunnable implements Runnable
{
    private static final Logger log = LoggerFactory.getLogger(AdminsToGroupsRunnable.class);

    @VisibleForTesting
    static final String ADMINISTRATORS_GROUP = "administrators";
    @VisibleForTesting
    static final int MAX_ADMINS_TO_ADD = Integer.getInteger("horde.ondemand.maxAdminsAddedOnLicenseChange", 20);
    @VisibleForTesting
    static final int MAX_ADMINS_SEARCHABLE = 150;

    private static final int MAX_USERS_IN_NEW_GROUP = 2;
    private static final int MAX_INFLIGHT_REST_CALLS = 1;

    private final CrowdClientFactory restCrowdClientFactory;
    private final Set<LicenseableApplication> licenseableApplications;
    private final ExecutorService executorService;
    private final CommonListenerProperties listenerProperties;

    AdminsToGroupsRunnable(CrowdClientFactory restCrowdClientFactory,
                           Set<LicenseableApplication> licenseableApplications,
                           CommonListenerProperties listenerProperties)
    {
        this(restCrowdClientFactory, licenseableApplications, listenerProperties, Executors.newFixedThreadPool(
            MAX_INFLIGHT_REST_CALLS));
    }

    AdminsToGroupsRunnable(CrowdClientFactory restCrowdClientFactory,
                           Set<LicenseableApplication> licenseableApplications,
                           CommonListenerProperties listenerProperties, ExecutorService executorService)
    {
        this.restCrowdClientFactory = restCrowdClientFactory;
        this.licenseableApplications = licenseableApplications;
        this.executorService = executorService;
        this.listenerProperties = listenerProperties;
    }

    private static boolean isNewApp(Collection<String> users)
    {
        // in the case of a new app, members of the licensed group should be only sysadmin and "original" admin.
        return users.size() <= MAX_USERS_IN_NEW_GROUP;
    }

    private static class GetUserWithAttributesFromName implements Callable<UserWithAttributes>
    {
        private final CrowdClient crowdClient;
        private final String username;

        private GetUserWithAttributesFromName(CrowdClient crowdClient, String username)
        {
            this.crowdClient = crowdClient;
            this.username = username;
        }

        @Override
        public UserWithAttributes call() throws Exception
        {
            return crowdClient.getUserWithAttributes(username);
        }
    }

    private static <A> Iterable<Future<A>> submitAll(final ExecutorService executorService,
                                                     Iterable<? extends Callable<A>> callables)
    {
        return Iterables.transform(callables, new Function<Callable<A>, Future<A>>()
        {
            @Override
            public Future<A> apply(Callable<A> input)
            {
                return executorService.submit(input);
            }
        });
    }

    private Iterable<Future<UserWithAttributes>> getAdminsWithAttributes(final CrowdClient crowdClient,
                                                                         final Set<String> usersAlreadyRegistered)
        throws OperationFailedException, GroupNotFoundException, ApplicationPermissionException,
        InvalidAuthenticationException, InterruptedException
    {
        log.debug("Attempting to get administrators from the Crowd instance | Application: " + licenseableApplications);
        final List<String> administrators = crowdClient.getNamesOfUsersOfGroup(ADMINISTRATORS_GROUP, 0,
            MAX_ADMINS_SEARCHABLE);

        // Get only the active administrators
        final Iterable<String> activeAdministrators = filter(administrators, not(in(usersAlreadyRegistered)));

        return submitAll(executorService,
            Iterables.transform(activeAdministrators,
                new Function<String, Callable<UserWithAttributes>>()
                {
                    @Override
                    public Callable<UserWithAttributes> apply(final String activeAdminUsername)
                    {
                        return new GetUserWithAttributesFromName(crowdClient, activeAdminUsername);
                    }
                }));
    }

    private Iterable<String> filterAdminsOnRecency(
        Iterable<Future<UserWithAttributes>> adminsWithAttributes) throws ExecutionException, InterruptedException
    {
        final MostRecentlyLoggedInUsersContainer mostRecentlyLoggedInAdmins = new MostRecentlyLoggedInUsersContainer(
            MAX_ADMINS_TO_ADD);

        // Extract the valid admin users out of the futures
        for (Future<UserWithAttributes> adminDetailsFuture : adminsWithAttributes)
        {
            final UserWithAttributes adminWithAttributes = adminDetailsFuture.get();
            final String lastLoginTime = adminWithAttributes.getValue(UserConstants.LAST_AUTHENTICATED);
            mostRecentlyLoggedInAdmins.addUser(adminWithAttributes.getName(), lastLoginTime);
        }

        return mostRecentlyLoggedInAdmins;
    }

    private void assignAdminsToGroup(CrowdClient crowdClient, Iterable<String> recentlyLoggedInAdmins,
                                     Collection<String> groupsToAddTo)
        throws ApplicationPermissionException, GroupNotFoundException, OperationFailedException,
        InvalidAuthenticationException
    {
        final String stringGroups = Arrays.toString(groupsToAddTo.toArray());
        for (String adminUsername : recentlyLoggedInAdmins)
        {
            log.info("Adding user {} to groups {}", adminUsername, stringGroups);
            for (String group : groupsToAddTo)
            {
                try
                {
                    // There is no bulk add user to groups or groups to user function so we are stuck doing this
                    crowdClient.addUserToGroup(adminUsername, group);
                    log.debug("Added user {} to group {}", adminUsername, group);
                }
                catch (UserNotFoundException e)
                {
                    log.warn(
                        "{} has not been added to {} because apparently they do not exist anymore; even though we just saw them before.",
                        adminUsername, group);
                }
                catch (MembershipAlreadyExistsException e)
                {
                    log.error(
                        "Apparently {} is already a member of {} which suggests that this has been run before and should not have been run again.",
                        adminUsername, group);
                }
            }
        }
    }

    private static Collection<String> getGroupNamesForLicenseableApplications(final Set<LicenseableApplication> apps)
    {
        final Collection<String> groupsToAddTo = Maps.filterKeys(APP_TO_GROUP_MAPPING, Predicates.in(
            apps)).values();

        if (groupsToAddTo.size() != apps.size())
        {
            final String licenseableApps = Arrays.toString(apps.toArray());
            throw new RuntimeException("Could not get the group names for the applications: " + licenseableApps);
        }

        return groupsToAddTo;
    }

    /**
     * Given a crowd client and a group it returns the first three users that belong to a particular group.
     */
    private static class UsersForGroupCallable implements Callable<GroupWithUsers>
    {
        private final CrowdClient crowdClient;
        private final String group;

        private UsersForGroupCallable(CrowdClient crowdClient, String group)
        {
            this.crowdClient = crowdClient;
            this.group = group;
        }

        @Override
        public GroupWithUsers call() throws Exception
        {
            log.debug("About to get the names of the users of a group: {}", group);
            final Set<String> usersSet = ImmutableSet.copyOf(crowdClient.getNamesOfUsersOfGroup(group, 0,
                MAX_USERS_IN_NEW_GROUP + 1));
            log.debug("Got the names of a user of a group: {}", group);
            return new GroupWithUsers(group, usersSet);
        }
    }

    private static class GroupWithUsers
    {
        private final String group;
        private final Set<String> usernames;

        private GroupWithUsers(String group, Set<String> usernames)
        {
            this.group = group;
            this.usernames = usernames;
        }

        private String getGroup()
        {
            return group;
        }

        private Set<String> getUsernames()
        {
            return usernames;
        }
    }

    private static Collection<GroupWithUsers> retainGroupsThatAreNew(
        final ExecutorService executorService, final CrowdClient crowdClient, Collection<String> groups)
    {
        final Iterable<? extends Callable<GroupWithUsers>> usersForGroupCallables = Sets.newHashSet(Iterables.transform(
            groups,
            new Function<String, UsersForGroupCallable>()
            {
                @Override
                public UsersForGroupCallable apply(@Nullable String input)
                {
                    return new UsersForGroupCallable(crowdClient, input);
                }
            }));

        final Iterable<Future<GroupWithUsers>> userForGroupFutures = submitAll(executorService, usersForGroupCallables);

        final Iterable<GroupWithUsers> gatheredUserForGroups = Iterables.transform(userForGroupFutures,
            new Function<Future<GroupWithUsers>, GroupWithUsers>()
            {
                @Override
                public GroupWithUsers apply(Future<GroupWithUsers> input)
                {
                    try
                    {
                        final GroupWithUsers groupWithUsers = input.get();
                        return groupWithUsers;
                    }
                    catch (InterruptedException e)
                    {
                        throw new RuntimeException("We were interrupted while we attempted get users in a group", e);
                    }
                    catch (ExecutionException e)
                    {
                        throw new RuntimeException("Attempted to execute the users for a group but failed", e);
                    }
                }
            });

        return ImmutableSet.copyOf(filter(gatheredUserForGroups, new Predicate<GroupWithUsers>()
        {
            @Override
            public boolean apply(GroupWithUsers input)
            {
                return isNewApp(input.getUsernames());
            }
        }));
    }

    @Override
    public void run()
    {
        log.debug("Running the Admins to groups runnable.");
        if (licenseableApplications.isEmpty())
        {
            log.info("The license is in a state that does not require that any admin users be added to groups.");
            return;
        }

        final Collection<String> groupsToAddTo = getGroupNamesForLicenseableApplications(licenseableApplications);

        final CrowdClient crowdClient = restCrowdClientFactory.newInstance(generateMaintainerClientProperties(
            listenerProperties));
        try
        {
            final Collection<GroupWithUsers> filteredGroupsToAddTo = retainGroupsThatAreNew(executorService,
                crowdClient, groupsToAddTo);

            if (!filteredGroupsToAddTo.isEmpty())
            {
                // Pick one new application and see who is already registered to it and assume it is the same groups across
                // all new applications. For now this should be a safe assumption because it should only be the sysadmin
                // user and the instance admin
                final GroupWithUsers selectedGroupWithUsers = Iterables.get(filteredGroupsToAddTo, 0);
                log.debug(
                    "Pretending that the users in this group are representative of the users in all newly licensed products {}",
                    selectedGroupWithUsers.getGroup());

                // Get the most recently logged in administrators
                final Iterable<Future<UserWithAttributes>> collectedAdminFutures = getAdminsWithAttributes(crowdClient,
                    selectedGroupWithUsers.getUsernames());
                if (!Iterables.isEmpty(collectedAdminFutures))
                {
                    final Iterable<String> mostRecentlyLoggedInAdmins = filterAdminsOnRecency(
                        collectedAdminFutures);

                    // Add them to the groups that they need to be added to
                    assignAdminsToGroup(crowdClient, mostRecentlyLoggedInAdmins, groupsToAddTo);
                }
                else
                {
                    log.warn(
                        "There were no administrators to add to the following applications: {}; will be run again.",
                        licenseableApplications);
                }
            }
            else
            {
                log.info("There are no new applications. No users will be added to groups: {}",
                    licenseableApplications);
            }
        }
        catch (ApplicationPermissionException e)
        {
            log.error("We did not have permission to perform on: " + licenseableApplications, e);
        }
        catch (InvalidAuthenticationException e)
        {
            log.error("Could not authenticate: " + licenseableApplications, e);
        }
        catch (GroupNotFoundException e)
        {
            log.error("A group that should have existed did not.", e);
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    /**
     * Create the map in here every time because there is no need to have it lying around
     *
     * @return A Map of LicenseableApplications to their corresponding license groups.
     */
    private static final Map<LicenseableApplication, String> APP_TO_GROUP_MAPPING = ImmutableMap.of(
        LicenseableApplication.JIRA, "_licensed-jira",
        LicenseableApplication.CONFLUENCE, "_licensed-confluence",
        LicenseableApplication.BAMBOO, "_licensed-bamboo");
}
