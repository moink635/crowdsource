package com.atlassian.crowd.horde.healthcheck;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.healthcheck.core.Application;
import com.atlassian.healthcheck.core.DefaultHealthStatus;
import com.atlassian.healthcheck.core.HealthCheck;
import com.atlassian.healthcheck.core.HealthStatus;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.commons.lang3.StringUtils.join;

public class ConfigurationHealthCheck implements HealthCheck
{
    private static final Logger log = LoggerFactory.getLogger(ConfigurationHealthCheck.class);

    private final PropertyManager propertyManager;


    public ConfigurationHealthCheck(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    @Override
    public String getName()
    {
        return "Configuration Health Check";
    }

    @Override
    public String getDescription()
    {
        return "Checks that the SSO cookie domain is as expected, and that IP address is excluded from validation factors";
    }

    @Override
    public Application getApplication()
    {
        return Application.Crowd;
    }

    @Override
    public HealthStatus check()
    {
        log.debug("Starting ConfigurationHealthCheck.");

        List<String> failureReasons = new ArrayList<String>();

        try
        {
            String expectedDomain = expectedDomain();
            String domain = propertyManager.getDomain();
            if (!expectedDomain.equals(domain))
            {
                log.warn("Healthcheck failure: invalid SSO cookie domain, expected {}, was {}.", expectedDomain, domain);
                failureReasons.add("Expected SSO cookie domain to be " + expectedDomain + ", was " + domain);
            }
            else
            {
                log.debug("SSO cookie domain healthcheck OK.");
            }

            if (propertyManager.isIncludeIpAddressInValidationFactors())
            {
                log.warn("Healthcheck failure: IP address should NOT be included in validation factors");
                failureReasons.add("IP address should NOT be included in validation factors");
            }
            else
            {
                log.debug("Validation factors healthcheck OK.");
            }
        }
        catch (PropertyManagerException e)
        {
            log.error("Could not read properties for health check", e);
            failureReasons.add("Error accessing PropertyManager, see log for details");
        }

        return new DefaultHealthStatus(this, failureReasons.isEmpty(), join(failureReasons, "; "));
    }

    private static String expectedDomain()
    {
        try
        {
            return "." + InetAddress.getLocalHost().getHostName();
        }
        catch (UnknownHostException e)
        {
            // Shouldn't happen for localhost.
            throw new RuntimeException(e);
        }
    }


}
