package com.atlassian.crowd.horde.healthcheck;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.atlassian.fugue.Pair;
import com.atlassian.healthcheck.core.DefaultHealthStatus;
import com.atlassian.healthcheck.core.HealthCheck;
import com.atlassian.healthcheck.core.HealthCheckManager;
import com.atlassian.healthcheck.core.HealthStatus;
import com.atlassian.healthcheck.core.thread.HealthCheckCallable;
import com.atlassian.healthcheck.core.thread.HealthCheckThreadFactory;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.util.concurrent.Executors.newFixedThreadPool;

/**
 * Based on Atlassian Healthcheck's <code>DefaultHealthCheckManager</code>, adapted to:
 * <ul>
 *   <li>Source HealthChecks from an explicit list rather than from plugin modules</li>
 *   <li>Work around https://ecosystem.atlassian.net/browse/AHC-3 (checks that time out or throw uncaught exceptions do not register as failures) </li>
 * </ul>
 *
 * To be merged into AHC plugin.
 */
public class PluginlessHealthCheckManager implements HealthCheckManager
{
    private static final Logger log = LoggerFactory.getLogger(PluginlessHealthCheckManager.class);

    private static final int DEFAULT_TIMEOUT_MS = Integer.getInteger("atlassian.healthcheck.timeout-ms", 5000);
    private static final int DEFAULT_THREAD_COUNT = Integer.getInteger("atlassian.healthcheck.thread-count", 8);

    private final int timeoutMs;
    private final ExecutorService executorService;
    private final List<HealthCheck> healthChecks;

    public PluginlessHealthCheckManager(final List<HealthCheck> checks)
    {
        this(checks, DEFAULT_TIMEOUT_MS, DEFAULT_THREAD_COUNT);
    }

    @VisibleForTesting
    PluginlessHealthCheckManager(final List<HealthCheck> checks, final int timeoutMs, final int threadCount)
    {
        this.healthChecks = ImmutableList.copyOf(checks);
        this.timeoutMs = timeoutMs;
        this.executorService = newFixedThreadPool(threadCount, new HealthCheckThreadFactory());
    }

    @Override
    public Collection<? extends HealthStatus> performChecks()
    {
        Collection<Pair<Future<HealthStatus>, HealthCheck>> futurePairs = new ArrayList<Pair<Future<HealthStatus>, HealthCheck>>(healthChecks.size());
        for (HealthCheck check : healthChecks)
        {
            futurePairs.add(new Pair<Future<HealthStatus>, HealthCheck>(executorService.submit(new HealthCheckCallable(check)), check));
        }

        Collection<HealthStatus> statuses = new ArrayList<HealthStatus>(healthChecks.size());

        for (Pair<Future<HealthStatus>, HealthCheck> pair : futurePairs)
        {
            // Block for 'completion' of all of the checks
            try
            {
                statuses.add(pair.left().get(timeoutMs, TimeUnit.MILLISECONDS));
            }
            catch (InterruptedException e)
            {
                statuses.add(new DefaultHealthStatus(pair.right(), false, "Interrupted: " + e.getMessage()));
                log.info(e.getMessage(), e);
            }
            catch (ExecutionException e)
            {
                statuses.add(new DefaultHealthStatus(pair.right(), false, "ExecutionException: " + e.getMessage()));
                log.info(e.getMessage(), e);
            }
            catch (TimeoutException e)
            {
                if (pair.left().cancel(true))
                {
                    log.info(e.getMessage(), e);
                    statuses.add(new DefaultHealthStatus(pair.right(), false, "TimeoutException after " + timeoutMs + "ms: " + e.getMessage()));
                }
                else
                {
                    log.warn("A healthcheck timed out, but was not cancelled - perhaps it completed before the cancellation?", e);
                }
            }
        }

        return statuses;
    }

}
