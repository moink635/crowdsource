package com.atlassian.crowd.horde.rest.service.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path ("status")
@Produces ( { APPLICATION_JSON })
@AnonymousAllowed
public class StudioCrowdStatusResource
{
    private static final String STATUS_RESPONSE_BODY = "{\"ready\":true}";
    private static final Response STATUS_RESPONSE = Response.ok(STATUS_RESPONSE_BODY).build();

    /**
     * Always returns <pre>{"ready":true}</pre> in Horde.
     * Available at e.g. <pre>http://monitor.jira.com/crowd/rest/studio/latest/status</pre>
     */
    @GET
    public Response getStatus()
    {
        return STATUS_RESPONSE;
    }
}
