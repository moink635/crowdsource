package com.atlassian.crowd.horde;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.I18nHelperConfigurationImpl;

import static java.util.Arrays.asList;

public class SimpleI18nHelperImpl implements I18nHelper
{
    @Override
    public String getText(String key)
    {
        return getText(key, new Object[0]);
    }

    @Override
    public String getText(String key, String value1)
    {
        return getText(key, asList(value1));
    }

    @Override
    public String getText(String key, String value1, String value2)
    {
        return getText(key, asList(value1, value2));
    }

    @Override
    public String getText(String key, Object parameters)
    {
        final Object[] params;
        if (parameters instanceof List)
        {
            params = ((List<?>) parameters).toArray();
        }
        else if (parameters instanceof Object[])
        {
            params = (Object[]) parameters;
        }
        else
        {
            params = new Object[]{parameters};
        }
        return new MessageFormat(getUnescapedText(key)).format(params);
    }

    @Override
    public String getUnescapedText(String key)
    {
        return key; // the keys are not looked up anywhere
    }

    @Override
    public Map<String, String> getAllTranslationsForPrefix(String prefix)
    {
        return Collections.emptyMap(); // this helper doesn't know about any translation
    }

    @Override
    public String getText(Locale locale, String key, Serializable[] arguments)
    {
        // Not worrying  about locale in Horde
        return getText(key, arguments);
    }
}
