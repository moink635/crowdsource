package com.atlassian.crowd.horde.util;

import java.util.ArrayList;
import java.util.List;

import com.atlassian.fugue.retry.ExceptionHandler;

import org.slf4j.Logger;

/**
 * Accumulates a number of exceptions up to a threshold where it then attempt to write all of the exceptions to the
 * logger. The threshold shold be set at the point at which it is a mistake to get this many exceptions.
 * This Exception Handler is thread safe.
 * <p/>
 * <strong>Important:</strong> Please do not use this exception handler for multiple
 * {@link com.atlassian.fugue.retry.RetryFactory#create(com.google.common.base.Function, int, com.atlassian.fugue.retry.ExceptionHandler)}
 * calls.
 */
public class DelayedLoggingExceptionHandler implements ExceptionHandler
{
    private final Logger logger;
    private final int maxFailsBeforeLog;

    private final List<Exception> failures;

    private DelayedLoggingExceptionHandler(Logger logger, int maxFailsBeforeLog)
    {
        this.logger = logger;
        this.maxFailsBeforeLog = maxFailsBeforeLog;

        failures = new ArrayList<Exception>(maxFailsBeforeLog);
    }

    public static DelayedLoggingExceptionHandler create(Logger logger, int maxFailsBeforeLog)
    {
        return new DelayedLoggingExceptionHandler(logger, maxFailsBeforeLog);
    }

    @Override
    public synchronized void handle(RuntimeException exception)
    {
        failures.add(exception);

        if (failures.size() >= maxFailsBeforeLog)
        {
            logAllFailures(logger, failures);
            failures.clear();
        }
        else
        {
            logger.debug("Exception encountered: ", exception);
        }
    }

    private static void logAllFailures(final Logger logger, final Iterable<? extends Exception> failures)
    {
        int i = 1;
        for (Exception failure : failures)
        {
            logger.warn("Failure #" + i + ":", failure);
            i++;
        }
    }
}
