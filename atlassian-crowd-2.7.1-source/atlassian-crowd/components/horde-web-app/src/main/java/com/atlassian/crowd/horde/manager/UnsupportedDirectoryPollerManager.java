package com.atlassian.crowd.horde.manager;

import com.atlassian.crowd.directory.monitor.poller.DirectoryPoller;
import com.atlassian.crowd.manager.directory.SynchronisationMode;
import com.atlassian.crowd.manager.directory.monitor.DirectoryMonitorRegistrationException;
import com.atlassian.crowd.manager.directory.monitor.DirectoryMonitorUnregistrationException;
import com.atlassian.crowd.manager.directory.monitor.poller.DirectoryPollerManager;

/**
 * Horde does not poll remote directories.
 */
public class UnsupportedDirectoryPollerManager implements DirectoryPollerManager
{
    @Override
    public void addPoller(DirectoryPoller poller) throws DirectoryMonitorRegistrationException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean hasPoller(long directoryID)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void triggerPoll(long directoryID, SynchronisationMode synchronisationMode)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean removePoller(long directoryID) throws DirectoryMonitorUnregistrationException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeAllPollers()
    {
        throw new UnsupportedOperationException("Not implemented");
    }
}
