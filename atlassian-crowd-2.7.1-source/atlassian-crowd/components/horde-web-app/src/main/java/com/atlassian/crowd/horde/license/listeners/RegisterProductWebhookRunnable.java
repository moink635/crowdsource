package com.atlassian.crowd.horde.license.listeners;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.horde.license.LicenseableApplication;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.CrowdClient;
import com.atlassian.crowd.service.factory.CrowdClientFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.crowd.horde.license.listeners.ClientPropertiesUtils.generateClientPropertiesFor;

public class RegisterProductWebhookRunnable implements Runnable
{
    private static final Logger log = LoggerFactory.getLogger(RegisterProductWebhookRunnable.class);

    private final CrowdClientFactory restCrowdClientFactory;
    private final LicenseableApplication licenseableApplication;
    private final CommonListenerProperties listenerProperties;

    public RegisterProductWebhookRunnable(CrowdClientFactory restCrowdClientFactory,
                                          LicenseableApplication licenseableApplication,
                                          CommonListenerProperties listenerProperties)
    {
        this.restCrowdClientFactory = restCrowdClientFactory;
        this.licenseableApplication = licenseableApplication;
        this.listenerProperties = listenerProperties;
    }

    @Override
    public void run()
    {
        try
        {
            final ClientProperties clientProperties = generateClientPropertiesFor(licenseableApplication,
                listenerProperties);
            final CrowdClient crowdClient = restCrowdClientFactory.newInstance(clientProperties);

            final String notifyEndpoint = listenerProperties.getBaseUrl() + licenseableApplication.getNotificationUri(
                listenerProperties.getCrowdNotifyEndpoint());
            final long webhookID = crowdClient.registerWebhook(notifyEndpoint, null);


            log.info("A webhook was registered with the ID {} for Application {}", webhookID,
                licenseableApplication.getApplicationName());
        }
        catch (IllegalStateException e)
        {
            log.error("Encountered a bad state: not registering this webhook.", e);
        }
        catch (InvalidAuthenticationException e)
        {
            throw new RuntimeException(
                "Failed to authenticate with Crowd for application: " + licenseableApplication.getApplicationName(), e);
        }
        catch (OperationFailedException e)
        {
            throw new RuntimeException(
                "Webhook registration failed for application: " + licenseableApplication.getApplicationName(), e);
        }
        catch (ApplicationPermissionException e)
        {
            log.error(
                "Did not have permission to register the webhook. Incorrect application permissions for application: " +
                    licenseableApplication.getApplicationName(), e);
        }
    }
}
