package com.atlassian.crowd.horde.cache;

public interface FlushCacheService
{
    void cleanData();
}
