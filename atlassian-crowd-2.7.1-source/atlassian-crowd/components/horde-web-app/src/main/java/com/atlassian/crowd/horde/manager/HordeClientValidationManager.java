package com.atlassian.crowd.horde.manager;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.proxy.TrustedProxyManager;
import com.atlassian.crowd.manager.validation.ClientValidationManagerImpl;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.InetAddressCacheUtil;

import com.google.common.base.Function;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

/**
 * Horde allows connections from a set of whitelisted clients. The list is defined by the system property
 * -Dhorde.valid.clients, that contains a comma-separated list of {@link RemoteAddress} strings. For
 * instance, -Dhorde.valid.clients=localhost,192.168.0.0/16,10.0.0.1.
 *
 * Contrary to Crowd, client validation is application-independent.
 *
 */
public class HordeClientValidationManager extends ClientValidationManagerImpl
{
    public static final String VALID_CLIENTS_PROPERTY = "horde.valid.clients";
    private static final String DEFAULT_VALID_CLIENTS = "localhost";

    private final Iterable<RemoteAddress> allowedAddresses;

    public HordeClientValidationManager(InetAddressCacheUtil cacheUtil,
                                        PropertyManager propertyManager,
                                        TrustedProxyManager trustedProxyManager,
                                        I18nHelper i18nHelper)
    {
        super(cacheUtil, propertyManager, trustedProxyManager, i18nHelper);
        String validClients = System.getProperty(VALID_CLIENTS_PROPERTY, DEFAULT_VALID_CLIENTS);
        allowedAddresses = ImmutableList.copyOf(Iterables.transform(ADDRESS_SPLITTER.split(validClients),
                                                                    TO_REMOTE_ADDRESS));
    }

    @Override
    protected Iterable<RemoteAddress> getAllowedAddressesForApplication(Application application /* unused */)
    {
        return allowedAddresses;
    }

    private static final Function<String,RemoteAddress> TO_REMOTE_ADDRESS = new Function<String, RemoteAddress>()
    {
        @Override
        public RemoteAddress apply(String addressAsString)
        {
            return new RemoteAddress(addressAsString);
        }
    };

    private static final Splitter ADDRESS_SPLITTER = Splitter.on(",").trimResults().omitEmptyStrings();
}
