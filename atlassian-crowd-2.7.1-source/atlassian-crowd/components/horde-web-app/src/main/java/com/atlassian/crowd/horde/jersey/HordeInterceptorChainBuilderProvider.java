package com.atlassian.crowd.horde.jersey;

import javax.ws.rs.core.Context;
import javax.ws.rs.ext.Provider;

import com.atlassian.plugins.rest.common.expand.interceptor.ExpandInterceptor;
import com.atlassian.plugins.rest.common.interceptor.impl.InterceptorChainBuilder;

import com.sun.jersey.spi.inject.SingletonTypeInjectableProvider;

/**
 * Similar to {@link com.atlassian.plugins.rest.common.interceptor.impl.InterceptorChainBuilderProvider} from
 * atlassian-rest, but it includes the @Provider annotation and does not depend on AutowireCapablePlugin.
 *
 * @since 2.7
 */
@Provider
public class HordeInterceptorChainBuilderProvider extends SingletonTypeInjectableProvider<Context, InterceptorChainBuilder>
{
    public HordeInterceptorChainBuilderProvider(ExpandInterceptor expandInterceptor)
    {
        super(InterceptorChainBuilder.class, new InterceptorChainBuilder(null, expandInterceptor));
    }
}
