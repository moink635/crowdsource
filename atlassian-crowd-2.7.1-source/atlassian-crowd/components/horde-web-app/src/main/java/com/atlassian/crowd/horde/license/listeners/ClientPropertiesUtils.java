package com.atlassian.crowd.horde.license.listeners;

import java.util.Properties;

import com.atlassian.crowd.horde.license.LicenseableApplication;
import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;

public class ClientPropertiesUtils
{
    private static final String COOKIE_TOKENKEY = System.getProperty(Constants.PROPERTIES_FILE_COOKIE_TOKENKEY,
        "studio.crowd.tokenkey");

    public static ClientProperties generateClientPropertiesFor(LicenseableApplication licenseableApplication,
                                                               CommonListenerProperties commonListenerProperties)
    {
        final String applicationName = licenseableApplication.getApplicationName();

        final String crowdPublicUrl = commonListenerProperties.getBaseUrl() + commonListenerProperties.getContextPath();
        final String applicationPassword = commonListenerProperties.getApplicationPassword();

        final Properties properties = generateCommonProperties();
        properties.setProperty(Constants.PROPERTIES_FILE_BASE_URL, crowdPublicUrl);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_NAME, applicationName);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_PASSWORD, applicationPassword);

        return ClientPropertiesImpl.newInstanceFromProperties(properties);
    }

    public static ClientProperties generateMaintainerClientProperties(CommonListenerProperties commonListenerProperties)
    {
        final String applicationName = commonListenerProperties.getMaintainerApplicationName();

        final String crowdPublicUrl = commonListenerProperties.getBaseUrl() + commonListenerProperties.getContextPath();
        final String applicationPassword = commonListenerProperties.getApplicationPassword();

        final Properties properties = generateCommonProperties();
        properties.setProperty(Constants.PROPERTIES_FILE_BASE_URL, crowdPublicUrl);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_NAME, applicationName);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_PASSWORD, applicationPassword);

        return ClientPropertiesImpl.newInstanceFromProperties(properties);
    }

    private static Properties generateCommonProperties()
    {
        final Properties properties = new Properties();
        properties.setProperty(Constants.PROPERTIES_FILE_SESSIONKEY_LASTVALIDATION, "session.lastvalidation");
        properties.setProperty(Constants.PROPERTIES_FILE_SESSIONKEY_VALIDATIONINTERVAL, "10");
        properties.setProperty(Constants.PROPERTIES_FILE_COOKIE_TOKENKEY, COOKIE_TOKENKEY);
        return properties;
    }
}
