package com.atlassian.crowd.horde.license;

public interface LicenseChangeListener
{
    /**
     * This function will be called once when it is registered to a license monitor and then it will be called on every
     * subsequent update to the license from there.
     *
     * @param event An event representing the applications that are now active.
     */
    void onUpdate(LicenseChangedEvent event);
}
