package com.atlassian.crowd.horde.license.listeners;

import java.util.Collection;
import java.util.Set;
import java.util.concurrent.Executor;

import com.atlassian.crowd.horde.license.LicenseChangeListener;
import com.atlassian.crowd.horde.license.LicenseChangedEvent;
import com.atlassian.crowd.horde.license.LicenseableApplication;
import com.atlassian.crowd.horde.util.DelayedLoggingExceptionHandler;
import com.atlassian.crowd.service.factory.CrowdClientFactory;
import com.atlassian.fugue.retry.ExceptionHandler;
import com.atlassian.fugue.retry.RetryFactory;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.crowd.horde.license.LicenseableApplication.CROWD;
import static com.atlassian.crowd.horde.license.LicenseableApplication.FECRU;

public class OnDemandWebhooksMaintainerListener implements LicenseChangeListener
{
    private static final Logger log = LoggerFactory.getLogger(OnDemandWebhooksMaintainerListener.class);

    private static final Set<LicenseableApplication> IGNORED_APPLICATIONS = ImmutableSet.of(CROWD, FECRU);

    private Optional<LicenseChangedEvent> lastLicenseEvent;

    private final CrowdClientFactory restCrowdClientFactory;
    private final Executor executor;
    private final CommonListenerProperties listenerProperties;

    public OnDemandWebhooksMaintainerListener(CrowdClientFactory restCrowdClientFactory, Executor executor)
    {
        this(restCrowdClientFactory, executor, null, CommonListenerPropertiesImpl.INSTANCE);
    }

    @VisibleForTesting
    OnDemandWebhooksMaintainerListener(CrowdClientFactory restCrowdClientFactory, Executor executor,
                                       LicenseChangedEvent licenseChangedEvent,
                                       CommonListenerProperties commonListenerProperties)
    {
        this.restCrowdClientFactory = restCrowdClientFactory;
        this.executor = executor;
        this.listenerProperties = commonListenerProperties;
        lastLicenseEvent = Optional.fromNullable(licenseChangedEvent);
    }

    private void registerWebhooksFor(Collection<LicenseableApplication> appsToAdd)
    {
        for (LicenseableApplication licenseableApplication : appsToAdd)
        {
            final Runnable productRegister = new RegisterProductWebhookRunnable(restCrowdClientFactory,
                licenseableApplication, listenerProperties);

            final ExceptionHandler loggingHandler = DelayedLoggingExceptionHandler.create(log,
                listenerProperties.getRetries());
            final Runnable registerRunner = RetryFactory.create(productRegister, listenerProperties.getRetries(),
                loggingHandler, listenerProperties.getRetryDelay());

            executor.execute(registerRunner);
        }
    }

    @Override
    public void onUpdate(LicenseChangedEvent event)
    {
        // Work out which applications were modified
        final LicenseChangedEvent lastEvent = lastLicenseEvent.or(LicenseChangedEvent.NO_APPS);
        // If this is not the first update then we want to just see what has changed in the license.
        final ImmutableSet<LicenseableApplication> addedApplications = Sets.difference(event.getLicensedApps(),
            lastEvent.getLicensedApps()).immutableCopy();

        // Get the list of applications that should not be ignored.
        final Set<LicenseableApplication> appsToRegister = Sets.difference(addedApplications, IGNORED_APPLICATIONS);

        // Debug logging to always ensure that something is shown if a supporter wants to see what happened.
        if (appsToRegister.isEmpty())
        {
            log.debug("There are no applications to be added.");
        }
        else
        {
            log.info("Ensuring webhooks are present for products: {}", appsToRegister);
            registerWebhooksFor(appsToRegister);
        }

        lastLicenseEvent = Optional.of(event);
    }
}
