package com.atlassian.crowd.horde.healthcheck;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.horde.license.listeners.CommonListenerProperties;
import com.atlassian.crowd.horde.license.listeners.CommonListenerPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;
import com.atlassian.crowd.service.factory.CrowdClientFactory;
import com.atlassian.healthcheck.core.Application;
import com.atlassian.healthcheck.core.DefaultHealthStatus;
import com.atlassian.healthcheck.core.HealthCheck;
import com.atlassian.healthcheck.core.HealthStatus;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.crowd.horde.license.LicenseableApplication.JIRA;
import static com.atlassian.crowd.horde.license.listeners.ClientPropertiesUtils.generateClientPropertiesFor;
import static org.apache.commons.lang3.StringUtils.join;

/**
 * Confirms user "sysadmin" is the only member of groups system-administrators and confluence-administrators.
 */
public class SysadminGroupsHealthCheck implements HealthCheck
{
    private static final Logger log = LoggerFactory.getLogger(SysadminGroupsHealthCheck.class);

    private static final String SYSTEM_ADMINISTRATORS = "system-administrators";
    private static final String CONFLUENCE_ADMINISTRATORS = "confluence-administrators";
    static final int QUERY_MAX_RESULTS = 10;

    final CrowdClient crowdClient;

    public SysadminGroupsHealthCheck(final CrowdClientFactory restCrowdClientFactory)
    {
        this(restCrowdClientFactory, CommonListenerPropertiesImpl.INSTANCE);
    }

    @VisibleForTesting
    SysadminGroupsHealthCheck(final CrowdClientFactory restCrowdClientFactory, CommonListenerProperties commonListenerProperties)
    {
        this.crowdClient = restCrowdClientFactory.newInstance(generateClientPropertiesFor(JIRA, commonListenerProperties));
    }

    @Override
    public String getName()
    {
        return "Sysadmin Groups Health Check";
    }

    @Override
    public String getDescription()
    {
        return "Confirms user 'sysadmin' is the only member of groups system-administrators and confluence-administrators.";
    }

    @Override
    public Application getApplication()
    {
        return Application.Crowd;
    }

    @SuppressWarnings("unchecked")
    @Override
    public HealthStatus check()
    {
        log.debug("Starting SysadminGroupsHealthCheck...");

        List<String> allFailures = new ArrayList<String>();

        try
        {
            allFailures.addAll(auditGroupMembership(SYSTEM_ADMINISTRATORS, ImmutableSet.of("sysadmin")));
            allFailures.addAll(auditGroupMembership(CONFLUENCE_ADMINISTRATORS, ImmutableSet.of("sysadmin"), ImmutableSet.<String>of()));
        }
        catch (ApplicationPermissionException e)
        {
            log.warn("Failed to get group members", e);
            allFailures.add("Error retrieving group members, could not check them.");
        }
        catch (CrowdException e)
        {
            log.warn("Failed to get group members", e);
            allFailures.add("Error retrieving group members, could not check them.");
        }

        return new DefaultHealthStatus(this, allFailures.isEmpty(), join(allFailures, "; "));
    }

    /**
     * @return a list of failure reasons if the membership set of group <code>groupName</code> fails to match any <code>validMemberSets</code>; empty list otherwise.
     */
    private List<String> auditGroupMembership(String groupName, Set<String>... validMemberSets) throws ApplicationPermissionException, CrowdException
    {
        List<String> failureReasons;

        Set<String> members = membersOf(groupName);
        if (Arrays.asList(validMemberSets).contains(members))
        {
            log.debug("Verified group {} has expected members.", groupName);
            failureReasons = ImmutableList.of();
        }
        else
        {
            String message = "Membership of '" + groupName + "' is invalid. Expected: " + join(validMemberSets, " or ") + ", got: " + members;
            if (members.size() == QUERY_MAX_RESULTS)
            {
                message += " (list capped at first " + QUERY_MAX_RESULTS + " results)";
            }
            failureReasons = ImmutableList.of(message);
        }

        return failureReasons;
    }

    /**
     * @return set of members in group <code>groupName</code>, or empty set if the group does not exist.
     */
    private ImmutableSet<String> membersOf(String groupName) throws ApplicationPermissionException, CrowdException
    {
        try
        {
            return ImmutableSet.copyOf(crowdClient.getNamesOfUsersOfGroup(groupName, 0, QUERY_MAX_RESULTS));
        }
        catch (GroupNotFoundException e)
        {
            return ImmutableSet.of();
        }
    }

}
