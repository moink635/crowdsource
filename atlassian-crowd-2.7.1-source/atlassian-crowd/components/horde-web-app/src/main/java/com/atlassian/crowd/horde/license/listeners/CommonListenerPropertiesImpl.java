package com.atlassian.crowd.horde.license.listeners;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import static com.atlassian.crowd.horde.license.LicenseableApplication.JIRA;
import static org.apache.commons.lang3.StringUtils.removeEnd;

/**
 * The purpose of this class is to pass system properties through to the application. It is structured in this manner so
 * that we can override it for testing purposes.
 */
public class CommonListenerPropertiesImpl implements CommonListenerProperties
{
    public static final CommonListenerPropertiesImpl INSTANCE = new CommonListenerPropertiesImpl();

    private CommonListenerPropertiesImpl()
    {
    }

    private static final String HORDE_NOTIFY_ENDPOINT_PROPERTY = "horde.product.notify.url";
    private static final String HORDE_CONTEXT_PATH = "horde.catalina.context";

    private static final String HORDE_CLIENT_BASE_URL = "horde.client.base.url";
    private static final String HORDE_MAINTAINER_APPLICATION_NAME = "horde.maintainer.application.name";
    private static final String HORDE_APPLICATION_PASSWORD = "horde.product.application.password";

    private static final String MAINTAINER_APPLICATION_NAME = System.getProperty(HORDE_MAINTAINER_APPLICATION_NAME,
        JIRA.getApplicationName());
    private static final String APPLICATION_PASSWORD = System.getProperty(HORDE_APPLICATION_PASSWORD, "fake-password");
    private static final String CONTEXT_PATH = System.getProperty(HORDE_CONTEXT_PATH, "/crowd");

    private static final String NOTIFY_ENDPOINT = System.getProperty(HORDE_NOTIFY_ENDPOINT_PROPERTY,
        "/plugins/servlet/crowdnotify");
    private static final String DOMAIN, PUBLIC_BASE_URL, CROWD_PUBLIC_URL;

    private static final int RETRIES = Integer.getInteger("horde.client.retries", 6);
    private static final long RETRY_DELAY = Long.getLong("horde.client.retry-delay", TimeUnit.SECONDS.toMillis(3));

    static
    {
        final String potentialDomain;
        try
        {
            potentialDomain = InetAddress.getLocalHost().getHostName();
        }
        catch (UnknownHostException e)
        {
            throw new RuntimeException("Could not work out what the base url is; could not register webhooks.", e);
        }

        DOMAIN = potentialDomain;
        PUBLIC_BASE_URL = removeEnd(System.getProperty(HORDE_CLIENT_BASE_URL, "https://" + potentialDomain), "/");
        CROWD_PUBLIC_URL = PUBLIC_BASE_URL + CONTEXT_PATH;
    }

    @Override
    public String getDomain()
    {
        return DOMAIN;
    }

    @Override
    public String getBaseUrl()
    {
        return PUBLIC_BASE_URL;
    }

    @Override
    public String getCrowdPublicUrl()
    {
        return CROWD_PUBLIC_URL;
    }

    @Override
    public String getMaintainerApplicationName()
    {
        return MAINTAINER_APPLICATION_NAME;
    }

    @Override
    public String getApplicationPassword()
    {
        return APPLICATION_PASSWORD;
    }

    @Override
    public String getContextPath()
    {
        return CONTEXT_PATH;
    }

    @Override
    public String getCrowdNotifyEndpoint()
    {
        return NOTIFY_ENDPOINT;
    }

    @Override
    public int getRetries()
    {
        return RETRIES;
    }

    @Override
    public long getRetryDelay()
    {
        return RETRY_DELAY;
    }
}
