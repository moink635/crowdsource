package com.atlassian.crowd.horde;

import java.util.Properties;

import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.model.authentication.ApplicationAuthenticationContext;
import com.atlassian.crowd.service.client.ClientProperties;

/**
 * Horde only needs some client properties. It reads them from system properties and not from the crowd.properties file.
 *
 */
public class HordeClientProperties implements ClientProperties
{
    private static final String DEFAULT_COOKIE_TOKEN_KEY = "studio.crowd.tokenkey";

    @Override
    public void updateProperties(Properties properties)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getApplicationName()
    {
        // This is the application name used when performing operations via CrowdService (which is a wrapper around
        // ApplicationService, and uses AplicationFactory to get a default application to work with). There's no "horde"
        // application in our data sets, so we use "crowd", which will be available in any Horde/Crowd installation and
        // effectively represents the Horde/Crowd application itself.
        return "crowd";
    }

    @Override
    public String getApplicationPassword()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getApplicationAuthenticationURL()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getCookieTokenKey()
    {
        return getCookieTokenKey(DEFAULT_COOKIE_TOKEN_KEY);
    }

    @Override
    public String getCookieTokenKey(String defaultKey)
    {
        return System.getProperty(Constants.PROPERTIES_FILE_COOKIE_TOKENKEY, defaultKey);
    }

    @Override
    public String getSessionTokenKey()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getSessionLastValidation()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public long getSessionValidationInterval()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public ApplicationAuthenticationContext getApplicationAuthenticationContext()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getHttpProxyPort()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getHttpProxyHost()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getHttpProxyUsername()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getHttpProxyPassword()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getHttpMaxConnections()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getHttpTimeout()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getSocketTimeout()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getBaseURL()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getSSOCookieDomainName()
    {
        throw new UnsupportedOperationException("Not implemented");
    }
}
