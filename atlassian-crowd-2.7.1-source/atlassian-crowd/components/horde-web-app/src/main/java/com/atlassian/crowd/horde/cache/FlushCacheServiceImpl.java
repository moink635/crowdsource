package com.atlassian.crowd.horde.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sf.ehcache.CacheManager;

public class FlushCacheServiceImpl implements FlushCacheService
{
    private static final Logger logger = LoggerFactory.getLogger(FlushCacheServiceImpl.class);

    @Override
    public void cleanData()
    {
        // eventually we'll use atlassian-cache >= 2.0, and there will be an elegant way
        // to flush the cache. For the moment being...

        logger.info("Flushing cache");
        CacheManager.getInstance().clearAll();
        logger.info("Cache flushed");
    }
}
