package com.atlassian.crowd.horde.util.persistence.hibernate;

import com.atlassian.crowd.util.persistence.hibernate.EhCacheProvider;
import net.sf.ehcache.CacheManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;

import java.io.IOException;

/**
 * A cut down version of {@link com.atlassian.crowd.util.persistence.hibernate.ConfigurableLocalSessionFactoryBean} that
 * allows us to set the cache manager, ensuring we have only one ehcache manager in Horde.
 */
public class ConfigurableLocalSessionFactoryBean extends LocalSessionFactoryBean
{
    private CacheManager cacheManager;

    @Override
    public void afterPropertiesSet() throws IOException
    {
        if (cacheManager != null)
        {
            //Use the Map<Object, Object> underpinnings of Properties to store the cache manager under the key that
            //is expected by the Crowd EhCacheProvider
            getHibernateProperties().put(EhCacheProvider.PROP_CACHE_MANAGER, cacheManager);
        }
        super.afterPropertiesSet();
    }

    public void setCacheManager(CacheManager cacheManager)
    {
        this.cacheManager = cacheManager;
    }
}
