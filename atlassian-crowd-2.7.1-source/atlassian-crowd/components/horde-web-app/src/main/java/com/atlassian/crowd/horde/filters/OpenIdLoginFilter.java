package com.atlassian.crowd.horde.filters;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Properties;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.horde.license.LicenseableApplication;
import com.atlassian.crowd.horde.license.listeners.ClientPropertiesUtils;
import com.atlassian.crowd.horde.license.listeners.CommonListenerProperties;
import com.atlassian.crowd.horde.license.listeners.CommonListenerPropertiesImpl;
import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.integration.http.util.CrowdHttpTokenHelper;
import com.atlassian.crowd.integration.http.util.CrowdHttpTokenHelperImpl;
import com.atlassian.crowd.integration.http.util.CrowdHttpValidationFactorExtractorImpl;
import com.atlassian.crowd.manager.application.ApplicationAccessDeniedException;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.model.authentication.CookieConfiguration;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Optional;

import org.openid4java.OpenIDException;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.consumer.VerificationResult;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.discovery.Identifier;
import org.openid4java.message.AuthRequest;
import org.openid4java.message.ParameterList;
import org.openid4java.util.HttpClientFactory;
import org.openid4java.util.ProxyProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Does Sysadmin OpenID login
 */
public class OpenIdLoginFilter implements Filter
{
    private static final Logger log = LoggerFactory.getLogger(OpenIdLoginFilter.class);

    private static final String OPEN_ID_IDENTIFIER_PROPERTY = "horde.ondemand.openidIdentifierUrl";

    @VisibleForTesting
    static final String OPEN_ID_IDENTIFIER = System.getProperty(OPEN_ID_IDENTIFIER_PROPERTY);

    private static final String PROXY_HOST_PROPERTY = "http.proxyHost";
    private static final String PROXY_PORT_PROPERTY = "http.proxyPort";

    private final ConsumerManager manager;
    private final TokenAuthenticationManager tokenAuthenticationManager;
    private final CrowdHttpTokenHelper tokenHelper;
    private final OpenIdUrl openIdUrl;
    private final CommonListenerProperties listenerProperties;
    private final ClientProperties clientProperties;

    public OpenIdLoginFilter(TokenAuthenticationManager tokenAuthenticationManager)
    {

        this(tokenAuthenticationManager, createConsumerManager(), CrowdHttpTokenHelperImpl.getInstance(
            CrowdHttpValidationFactorExtractorImpl.getInstance()), OpenIdUrl.INSTANCE,
            CommonListenerPropertiesImpl.INSTANCE);
    }

    @VisibleForTesting
    OpenIdLoginFilter(TokenAuthenticationManager tokenAuthenticationManager,
                      ConsumerManager consumerManager, CrowdHttpTokenHelper tokenHelper, OpenIdUrl openIdUrl,
                      CommonListenerProperties commonListenerProperties)
    {
        this.tokenAuthenticationManager = tokenAuthenticationManager;
        this.manager = consumerManager;
        this.tokenHelper = tokenHelper;
        this.openIdUrl = openIdUrl;
        this.listenerProperties = commonListenerProperties;
        this.clientProperties = ClientPropertiesUtils.generateClientPropertiesFor(LicenseableApplication.CROWD,
            commonListenerProperties);
    }

    static class OpenIdUrl
    {
        private static final OpenIdUrl INSTANCE = new OpenIdUrl();

        public String get()
        {
            return checkNotNull(OPEN_ID_IDENTIFIER,
                "The OpenId identifier is required but was not found in the system properties.");
        }
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
        throws IOException, ServletException
    {
        if (servletRequest instanceof HttpServletRequest && servletResponse instanceof HttpServletResponse)
        {
            doHttpServletFilter((HttpServletRequest) servletRequest, (HttpServletResponse) servletResponse,
                filterChain);
            return;
        }
        else
        {
            log.debug("The ServletRequest or ServletResponse for the open id filter were not HTTP requests.");
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    private void doHttpServletFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
        throws IOException, ServletException
    {
        final String servletPath = request.getServletPath();
        if ("/oid".equals(servletPath))
        {
            authRequest(request, response);
        }
        else if ("/oid/verify".equals(servletPath))
        {
            createTokenAndRedirect(request, response);
        }
        else
        {
            filterChain.doFilter(request, response);
        }
    }

    private void createTokenAndRedirect(HttpServletRequest request, HttpServletResponse response)
        throws IOException, ServletException
    {
        final Optional<Identifier> optionalIdentifier = verifyResponse(request);
        if (!optionalIdentifier.isPresent())
        {
            throw new ServletException(
                "OpenID Verification failed because an identifier could not be found for the OpenID request");
        }

        if (!openIdUrl.get().equals(optionalIdentifier.get().getIdentifier()))
        {
            throw new ServletException("Unexpected OpenId Identifier (Aborting OpenID login): " +
                optionalIdentifier.get().getIdentifier());
        }

        // Log the user in as sysadmin
        UserAuthenticationContext ctx = tokenHelper.getUserAuthenticationContext(request, "sysadmin", "",
            clientProperties);
        try
        {
            final Token token = tokenAuthenticationManager.authenticateUserWithoutValidatingPassword(ctx);
            final CookieConfiguration cookieConfig = new CookieConfiguration(listenerProperties.getDomain(), false,
                clientProperties.getCookieTokenKey());
            tokenHelper.setCrowdToken(request, response, token.getRandomHash(), clientProperties, cookieConfig);
            response.sendRedirect(listenerProperties.getBaseUrl());
        }
        catch (CrowdException e)
        {
            throw new ServletException("Authentication failed", e);
        }
        catch (ApplicationAccessDeniedException e)
        {
            throw new ServletException("Access denied with application 'crowd'", e);
        }
    }

    /**
     * Place the authentication request.
     */
    private void authRequest(HttpServletRequest httpReq, HttpServletResponse httpResp)
        throws IOException, ServletException
    {
        try
        {
            // configure the return_to URL where your application will receive
            // the authentication responses from the OpenID provider
            String returnToUrl = listenerProperties.getCrowdPublicUrl() + "/oid/verify";

            // perform discovery on the user-supplied identifier
            List discoveries = manager.discover(openIdUrl.get());

            // attempt to associate with the OpenID provider
            // and retrieve one service endpoint for authentication
            DiscoveryInformation discovered = manager.associate(discoveries);

            // store the discovery information in the user's session
            httpReq.getSession().setAttribute("openid-disc", discovered);

            // obtain a AuthRequest message to be sent to the OpenID provider
            AuthRequest authReq = manager.authenticate(discovered, returnToUrl);
            authReq.setRealm(listenerProperties.getBaseUrl());

            // GET HTTP-redirect to the OpenID Provider endpoint
            // The only method supported in OpenID 1.x
            // redirect-URL usually limited ~2048 bytes
            httpResp.sendRedirect(authReq.getDestinationUrl(true));
        }
        catch (OpenIDException e)
        {
            throw new ServletException("The OpenID handshake failed", e);
        }
    }

    /**
     * Verifying that the authentication response is valid.
     *
     * @return Returns the Identifier if authenticated and validated; otherwise will return {@link com.google.common.base.Optional#absent()}.
     */
    private Optional<Identifier> verifyResponse(HttpServletRequest httpReq) throws ServletException
    {
        try
        {
            // extract the parameters from the authentication response
            // (which comes in as a HTTP request from the OpenID provider)
            ParameterList response = new ParameterList(httpReq.getParameterMap());

            // retrieve the previously stored discovery information
            DiscoveryInformation discovered = (DiscoveryInformation) httpReq.getSession().getAttribute("openid-disc");

            // extract the receiving URL from the HTTP request
            StringBuffer receivingURL = httpReq.getRequestURL();
            String queryString = httpReq.getQueryString();
            if (queryString != null && queryString.length() > 0)
            {
                receivingURL.append("?").append(httpReq.getQueryString());
            }

            // verify the response; ConsumerManager needs to be the same
            // (static) instance used to place the authentication request
            VerificationResult verification = manager.verify(receivingURL.toString(), response, discovered);

            // examine the verification result and extract the verified identifier
            Identifier verified = verification.getVerifiedId();
            return Optional.fromNullable(verified);
        }
        catch (OpenIDException e)
        {
            throw new ServletException("Error in OpenId login", e);
        }
    }

    /**
     * WARNING: order of proxy setup and {@link ConsumerManager} instantiation is important.
     *
     * @return A valid Consumer Manager whose proxy details are setup correctly.
     */
    private static ConsumerManager createConsumerManager()
    {
        final String proxyHostname = System.getProperty(PROXY_HOST_PROPERTY);
        if (proxyHostname != null)
        {
            log.debug("Setting proxy: " + proxyHostname);
            ProxyProperties proxyProps = new ProxyProperties();
            proxyProps.setProxyHostName(proxyHostname);
            proxyProps.setProxyPort(Integer.getInteger(PROXY_PORT_PROPERTY, 8080));
            HttpClientFactory.setProxyProperties(proxyProps);
        }
        else
        {
            log.debug("Not using proxy");
        }

        return new ConsumerManager();
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        // NO-OP
    }

    @Override
    public void destroy()
    {
        // NO-OP
    }
}

