package com.atlassian.crowd.horde.manager;

import javax.annotation.Nullable;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.proxy.TrustedProxyManager;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.InetAddressCacheUtil;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class HordeClientValidationManagerTest
{
    @Mock
    private InetAddressCacheUtil cacheUtil;

    @Mock
    private PropertyManager propertyManager;

    @Mock
    private TrustedProxyManager trustedProxyManager;

    @Mock
    private I18nHelper i18nHelper;

    @Test
    public void shouldDefaultToLocalhost()
    {
        HordeClientValidationManager clientValidationManager =
            new HordeClientValidationManager(cacheUtil, propertyManager, trustedProxyManager, i18nHelper);
        assertThat(clientValidationManager.getAllowedAddressesForApplication(null),
                   hasItem(new RemoteAddress("localhost")));
    }

    @Test
    public void shouldGetRemoteAddressesFromSystemProperty()
    {
        String oldValue = System.getProperty(HordeClientValidationManager.VALID_CLIENTS_PROPERTY);
        try
        {
            System.setProperty(HordeClientValidationManager.VALID_CLIENTS_PROPERTY, "example1.test,example2.test,192.168.0.0/16");
            HordeClientValidationManager clientValidationManager =
                new HordeClientValidationManager(cacheUtil, propertyManager, trustedProxyManager, i18nHelper);
            assertThat(clientValidationManager.getAllowedAddressesForApplication(null),
                       hasItems(new RemoteAddress("example1.test"),
                                new RemoteAddress("example2.test"),
                                new RemoteAddress("192.168.0.0/16")));
        }
        finally
        {
            restoreSystemProperty(HordeClientValidationManager.VALID_CLIENTS_PROPERTY, oldValue);
        }
    }

    private static void restoreSystemProperty(String key, @Nullable String oldValue)
    {
        if (oldValue == null)
        {
            System.clearProperty(key);
        }
        else
        {
            System.setProperty(key, oldValue);
        }
    }
}
