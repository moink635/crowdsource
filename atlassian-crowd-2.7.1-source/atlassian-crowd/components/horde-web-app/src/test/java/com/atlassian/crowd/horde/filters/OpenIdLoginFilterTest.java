package com.atlassian.crowd.horde.filters;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;

import com.atlassian.crowd.horde.license.listeners.CommonListenerProperties;
import com.atlassian.crowd.integration.http.util.CrowdHttpTokenHelper;
import com.atlassian.crowd.manager.application.ApplicationAccessDeniedException;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.model.authentication.CookieConfiguration;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.service.client.ClientProperties;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.consumer.VerificationResult;
import org.openid4java.discovery.DiscoveryException;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.discovery.Identifier;
import org.openid4java.message.AuthRequest;
import org.openid4java.message.ParameterList;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import static com.atlassian.crowd.horde.license.LicenseableApplication.JIRA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OpenIdLoginFilterTest
{
    private static String EXPECTED_RETURN_TO_URL = "http://test/";
    private static String EXPECTED_TOKEN_HASH = "random-hash";

    @Mock
    private ConsumerManager consumerManager;

    @Mock
    private TokenAuthenticationManager tokenAuthenticationManager;

    @Mock
    private CrowdHttpTokenHelper tokenHelper;

    @Mock
    private FilterChain filterChain;

    @Mock
    private AuthRequest authRequest;

    @Mock
    private VerificationResult verificationResult;

    @Mock
    private Identifier identifier;

    @Mock
    private Token token;

    @Mock
    private OpenIdLoginFilter.OpenIdUrl openIdUrl;

    @Mock
    private CommonListenerProperties commonListenerProperties;

    private static final String TESTING_OPENID_URL = "https://openid/url/for/testing";
    private static final String JIRA_BASE_URL = "https://someinstance.jira.com/";

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    OpenIdLoginFilter sut;


    @Before
    public void wireUpMock() throws Exception
    {
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();

        when(commonListenerProperties.getRetries()).thenReturn(6);
        when(commonListenerProperties.getRetryDelay()).thenReturn(10l);
        when(commonListenerProperties.getMaintainerApplicationName()).thenReturn(JIRA.getApplicationName());
        when(commonListenerProperties.getApplicationPassword()).thenReturn("password");
        when(commonListenerProperties.getCrowdNotifyEndpoint()).thenReturn("/plugins/servlet/crowdnotifyendpoint");
        when(commonListenerProperties.getBaseUrl()).thenReturn(JIRA_BASE_URL);

        when(consumerManager.authenticate(any(DiscoveryInformation.class), anyString())).thenReturn(authRequest);
        when(authRequest.getDestinationUrl(true)).thenReturn(EXPECTED_RETURN_TO_URL);

        when(consumerManager.verify(anyString(), any(ParameterList.class), any(DiscoveryInformation.class))).thenReturn(
            verificationResult);
        when(verificationResult.getVerifiedId()).thenReturn(identifier);
        when(identifier.getIdentifier()).thenReturn(TESTING_OPENID_URL);

        when(tokenAuthenticationManager.authenticateUserWithoutValidatingPassword(any(
            UserAuthenticationContext.class))).thenReturn(token);
        when(token.getRandomHash()).thenReturn(EXPECTED_TOKEN_HASH);

        when(openIdUrl.get()).thenReturn(TESTING_OPENID_URL);

        sut = new OpenIdLoginFilter(tokenAuthenticationManager, consumerManager, tokenHelper, openIdUrl,
            commonListenerProperties);
    }

    @Test
    public void shouldRedirectToDestinationUrlIfPathIsOid() throws Exception
    {
        request.setServletPath("/oid");

        sut.doFilter(request, response, filterChain);

        verifyZeroInteractions(filterChain);
        assertEquals(EXPECTED_RETURN_TO_URL, response.getRedirectedUrl());
    }

    @Test
    public void shouldRedirectWithTokenIfPathIsOidVerify() throws Exception
    {
        request.setServletPath("/oid/verify");

        sut.doFilter(request, response, filterChain);

        verify(tokenHelper).setCrowdToken(eq(request), eq(response), eq(EXPECTED_TOKEN_HASH), any(
            ClientProperties.class), any(
            CookieConfiguration.class));
        verifyZeroInteractions(filterChain);
        assertEquals(JIRA_BASE_URL, response.getRedirectedUrl());

    }

    @Test
    public void shouldJustForwardFilterChainIfPathIsUnrecognized() throws Exception
    {
        request.setServletPath("/something/else");

        sut.doFilter(request, response, filterChain);

        verifyZeroInteractions(tokenHelper);
        assertNull(response.getRedirectedUrl());
        verify(filterChain).doFilter(request, response);
        verifyNoMoreInteractions(filterChain);
    }

    @Test(expected = ServletException.class)
    public void shouldPropagateExecptionIfDiscoveryFails() throws Exception
    {
        request.setServletPath("/oid");
        when(consumerManager.discover(anyString())).thenThrow(new DiscoveryException("Forced failure for test"));

        sut.doFilter(request, response, filterChain);
    }

    @Test(expected = ServletException.class)
    public void shouldPropagateExceptionIfVerificationFails() throws Exception
    {
        request.setServletPath("/oid/verify");
        when(consumerManager.verify(anyString(), any(ParameterList.class), any(DiscoveryInformation.class))).thenThrow(
            new DiscoveryException("Forced failure for test"));

        sut.doFilter(request, response, filterChain);
    }

    @Test(expected = ServletException.class)
    public void shouldPropagateExceptionIfApplicationAuthenticationFails() throws Exception
    {
        request.setServletPath("/oid/verify");
        when(tokenAuthenticationManager.authenticateUserWithoutValidatingPassword(any(UserAuthenticationContext.class)))
            .thenThrow(new ApplicationAccessDeniedException("Forced failure for test."));

        sut.doFilter(request, response, filterChain);
    }


}
