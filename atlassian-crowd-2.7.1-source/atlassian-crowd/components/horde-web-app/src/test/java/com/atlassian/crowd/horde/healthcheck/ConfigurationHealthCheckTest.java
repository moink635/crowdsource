package com.atlassian.crowd.horde.healthcheck;

import java.net.InetAddress;
import java.net.UnknownHostException;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.healthcheck.core.HealthStatus;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.horde.healthcheck.HealthCheckTestHelpers.assertUnhealthyWithReasonContaining;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigurationHealthCheckTest
{

    private static final String EXPECTED_SSO_COOKIE_DOMAIN = "." + getLocalhostDomain();

    @Test
    public void shouldFailIfSsoDomainIsWrong() throws Exception
    {
        ConfigurationHealthCheck sut = createSutWithConfiguration("different-domain.com", false);

        HealthStatus status = sut.check();

        assertUnhealthyWithReasonContaining(status, "domain");
    }

    @Test
    public void shouldFailIfIPAdressIncludedInValidationFactors() throws Exception
    {
        ConfigurationHealthCheck sut = createSutWithConfiguration(EXPECTED_SSO_COOKIE_DOMAIN, true);

        HealthStatus status = sut.check();

        assertUnhealthyWithReasonContaining(status, "validation factors");
    }

    @Test
    public void shouldFailIfPropertyManagerErrorsOut() throws Exception
    {
        ConfigurationHealthCheck sut = createSutWithBrokenPropertyManager();

        HealthStatus status = sut.check();

        assertUnhealthyWithReasonContaining(status, "Error accessing PropertyManager");
    }

    @Test
    public void shouldPassIfConfigurationIsAsExpected() throws Exception
    {
        ConfigurationHealthCheck sut = createSutWithConfiguration(EXPECTED_SSO_COOKIE_DOMAIN, false);

        HealthStatus status = sut.check();

        assertTrue(status.isHealthy());
    }

    private ConfigurationHealthCheck createSutWithConfiguration(String ssoCookieDomain, boolean ipIncludedInValidationFactors) throws PropertyManagerException
    {
        PropertyManager propertyManager = mock(PropertyManager.class);
        when(propertyManager.getDomain()).thenReturn(ssoCookieDomain);
        when(propertyManager.isIncludeIpAddressInValidationFactors()).thenReturn(ipIncludedInValidationFactors);
        return new ConfigurationHealthCheck(propertyManager);
    }

    private ConfigurationHealthCheck createSutWithBrokenPropertyManager() throws PropertyManagerException
    {
        PropertyManager propertyManager = mock(PropertyManager.class);
        when(propertyManager.getDomain()).thenThrow(new PropertyManagerException("Forced exception for test."));
        return new ConfigurationHealthCheck(propertyManager);
    }

    private static String getLocalhostDomain()
    {
        try
        {
            return InetAddress.getLocalHost().getHostName();
        }
        catch (UnknownHostException e)
        {
            // Shouldn't happen for localhost.
            throw new RuntimeException(e);
        }
    }
}
