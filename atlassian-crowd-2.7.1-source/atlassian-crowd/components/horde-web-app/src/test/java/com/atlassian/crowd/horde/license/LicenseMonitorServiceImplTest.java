package com.atlassian.crowd.horde.license;

import java.io.InputStream;

import com.google.common.base.Optional;
import com.google.common.collect.Sets;

import org.apache.commons.vfs2.FileChangeEvent;
import org.apache.commons.vfs2.FileContent;
import org.apache.commons.vfs2.FileName;
import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.horde.license.LicenseableApplication.BAMBOO;
import static com.atlassian.crowd.horde.license.LicenseableApplication.CONFLUENCE;
import static com.atlassian.crowd.horde.license.LicenseableApplication.CROWD;
import static com.atlassian.crowd.horde.license.LicenseableApplication.FECRU;
import static com.atlassian.crowd.horde.license.LicenseableApplication.JIRA;
import static com.google.common.collect.Sets.complementOf;
import static org.apache.commons.io.IOUtils.toInputStream;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LicenseMonitorServiceImplTest
{
    private static final String ALL_APPS = "conf.active\n" +
        "crucible.active\n" +
        "jira.active\n" +
        "crowd.active\n" +
        "fisheye.active\n" +
        "bamboo.active\n" +
        "\n";

    private static final String JIRA_ONLY = "crowd.active\n" +
        "jira.active\n" +
        "\n";

    private static final String CONF_ONLY = "conf.active\n" +
        "crowd.active\n" +
        "\n";

    @Test
    public void testLicenseListenerCalledOnRegistrationIfLastChangedAvaliable()
    {
        final LicenseMonitorServiceImpl sut = new LicenseMonitorServiceImpl();
        sut.getLicenseFileListener().readAndNotify(toInputStream(JIRA_ONLY));
        final LicenseChangeListener mockListener = mock(LicenseChangeListener.class);
        sut.registerListener(mockListener);
        verify(mockListener).onUpdate(eq(sut.getLastChangedEvent().get()));
    }

    @Test
    public void testDecodedLicenseWithAllApplications()
    {
        final LicenseMonitorServiceImpl sut = new LicenseMonitorServiceImpl();
        sut.getLicenseFileListener().readAndNotify(toInputStream(ALL_APPS));
        final LicenseChangedEvent licenseChangedEvent = sut.getLastChangedEvent().get();

        assertThat("All products should be present.", licenseChangedEvent.getLicensedApps(),
            hasItems(JIRA, CONFLUENCE, CROWD, FECRU, BAMBOO));
    }

    @Test
    public void testDecodedLicenseWithConfluenceOnly()
    {
        final LicenseMonitorServiceImpl sut = new LicenseMonitorServiceImpl();
        sut.getLicenseFileListener().readAndNotify(toInputStream(CONF_ONLY));
        final LicenseChangedEvent licenseChangedEvent = sut.getLastChangedEvent().get();

        assertThat("Expected products are present.", licenseChangedEvent.getLicensedApps(), hasItems(CROWD,
            CONFLUENCE));
        assertThat("Unexpected items are not present.", complementOf(licenseChangedEvent.getLicensedApps()),
            hasItems(JIRA, FECRU, BAMBOO));
    }

    @Test
    public void testDecodedLicenseWithJIRAOnly()
    {
        final LicenseMonitorServiceImpl sut = new LicenseMonitorServiceImpl();
        sut.getLicenseFileListener().readAndNotify(toInputStream(JIRA_ONLY));
        final LicenseChangedEvent licenseChangedEvent = sut.getLastChangedEvent().get();

        assertThat("Expected products are present.", licenseChangedEvent.getLicensedApps(), hasItems(CROWD, JIRA));
        assertThat("Unexpected items are not present.", Sets.complementOf(licenseChangedEvent.getLicensedApps()),
            hasItems(CONFLUENCE, FECRU, BAMBOO));
    }

    @Test
    public void testGetLastChangedReturnsNothingOnStartup()
    {
        final LicenseMonitorServiceImpl sut = new LicenseMonitorServiceImpl();
        assertFalse("Should not have a 'last change' on creation.", sut.getLastChangedEvent().isPresent());
    }

    @Test
    public void testGetLastChangedReturnsSomethingAfterSuccessfulRead()
    {
        final LicenseMonitorServiceImpl sut = new LicenseMonitorServiceImpl();
        sut.getLicenseFileListener().readAndNotify(toInputStream(JIRA_ONLY));
        assertTrue("There should be last license changed event after reading a license.",
            sut.getLastChangedEvent().isPresent());
    }

    @Test
    public void testFileChangedEventTriggersNotificationOnLicenseChange() throws Exception
    {
        final LicenseMonitorServiceImpl sut = new LicenseMonitorServiceImpl();
        sut.getLicenseFileListener().readAndNotify(toInputStream(JIRA_ONLY));

        final LicenseChangeListener mockListener = mock(LicenseChangeListener.class);
        final Optional<LicenseChangedEvent> firstLastChange = sut.getLastChangedEvent();
        sut.registerListener(mockListener);

        sut.getLicenseFileListener().fileChanged(generateMockEvent(toInputStream(ALL_APPS)));

        verify(mockListener).onUpdate(eq(firstLastChange.get()));
        verify(mockListener).onUpdate(eq(sut.getLastChangedEvent().get()));
    }

    @Test
    public void testFileCreatedEventTriggersNotificationOnLicenseChange() throws Exception
    {
        final LicenseMonitorServiceImpl sut = new LicenseMonitorServiceImpl();
        sut.getLicenseFileListener().readAndNotify(toInputStream(JIRA_ONLY));

        final LicenseChangeListener mockListener = mock(LicenseChangeListener.class);
        final Optional<LicenseChangedEvent> firstLastChange = sut.getLastChangedEvent();
        sut.registerListener(mockListener);

        sut.getLicenseFileListener().fileCreated(generateMockEvent(toInputStream(ALL_APPS)));

        verify(mockListener).onUpdate(eq(firstLastChange.get()));
        verify(mockListener).onUpdate(eq(sut.getLastChangedEvent().get()));
    }

    @Test
    public void testFileDeletedEventTriggersNotificationOnLicenseChange() throws Exception
    {
        final LicenseMonitorServiceImpl sut = new LicenseMonitorServiceImpl();
        sut.getLicenseFileListener().readAndNotify(toInputStream(JIRA_ONLY));

        final LicenseChangeListener mockListener = mock(LicenseChangeListener.class);
        final Optional<LicenseChangedEvent> firstLastChange = sut.getLastChangedEvent();
        sut.registerListener(mockListener);

        sut.getLicenseFileListener().fileDeleted(generateMockEvent(toInputStream(ALL_APPS)));

        verify(mockListener).onUpdate(eq(firstLastChange.get()));
        assertEquals("Assert that the last changed event has not changed.", firstLastChange.get(),
            sut.getLastChangedEvent().get());
    }

    private static FileChangeEvent generateMockEvent(InputStream result) throws FileSystemException
    {
        final FileChangeEvent mockFCE = mock(FileChangeEvent.class);
        final FileObject mockFileObject = mock(FileObject.class);
        final FileContent mockFileContent = mock(FileContent.class);
        when(mockFCE.getFile()).thenReturn(mockFileObject);
        when(mockFileObject.getContent()).thenReturn(mockFileContent);
        when(mockFileContent.getInputStream()).thenReturn(result);

        final FileName mockFileName = mock(FileName.class);
        when(mockFileObject.getName()).thenReturn(mockFileName);
        when(mockFileName.getBaseName()).thenReturn(LicenseMonitorServiceImpl.DECODED_LICENSE_FILE);

        return mockFCE;
    }
}
