package com.atlassian.crowd.horde.license.listeners;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Set;

import com.atlassian.crowd.horde.license.LicenseChangedEvent;
import com.atlassian.crowd.horde.license.LicenseableApplication;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.CrowdClient;
import com.atlassian.crowd.service.factory.CrowdClientFactory;

import com.google.common.collect.ImmutableSet;
import com.google.common.util.concurrent.MoreExecutors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.horde.license.LicenseableApplication.BAMBOO;
import static com.atlassian.crowd.horde.license.LicenseableApplication.CONFLUENCE;
import static com.atlassian.crowd.horde.license.LicenseableApplication.CROWD;
import static com.atlassian.crowd.horde.license.LicenseableApplication.JIRA;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OnDemandWebhooksMaintainerListenerTest
{
    private static final String TEST_BASE_URL = "http://" + getLocalhostDomain();

    @Mock
    private CrowdClientFactory crowdClientFactory;

    @Mock
    private CrowdClient crowdClient;

    @Mock
    private CommonListenerProperties listenerProperties;

    private static final Set<LicenseableApplication> ALL_APPS = ImmutableSet.copyOf(LicenseableApplication.values());

    @Before
    public void setupTest() throws Exception
    {
        when(listenerProperties.getRetries()).thenReturn(6);
        when(listenerProperties.getRetryDelay()).thenReturn(10l);
        when(listenerProperties.getApplicationPassword()).thenReturn("password");
        when(listenerProperties.getBaseUrl()).thenReturn(TEST_BASE_URL);
        when(listenerProperties.getCrowdNotifyEndpoint()).thenReturn("/plugins/servlet/crowdnotifyendpoint");

        when(crowdClient.registerWebhook(anyString(), anyString())).thenReturn(1L);
        when(crowdClientFactory.newInstance(any(ClientProperties.class))).thenReturn(crowdClient);
    }

    @Test
    public void testAllApplicationsAreEnsuredOnFirstRun() throws Exception
    {
        final OnDemandWebhooksMaintainerListener sut = makeSut();
        sut.onUpdate(licensedChangedEventFor());

        // No registration should occur if no applications are provided
        verify(crowdClient, never()).registerWebhook(anyString(), isNull(String.class));
        verifyZeroInteractions(crowdClient);
    }

    @Test
    public void testAddingApplicationResultsInOnlyOneRegistration() throws Exception
    {
        final OnDemandWebhooksMaintainerListener sut = makeSut(CROWD, JIRA);

        sut.onUpdate(licensedChangedEventFor(CROWD, JIRA, CONFLUENCE));
        verify(crowdClient).registerWebhook(contains("/wiki"), isNull(String.class));
        verifyNoMoreInteractions(crowdClient);
    }

    @Test
    public void testSwappingApplicationResultsInOnlyOneRegistration() throws Exception
    {
        final OnDemandWebhooksMaintainerListener sut = makeSut(CROWD, JIRA, CONFLUENCE);

        sut.onUpdate(licensedChangedEventFor(CROWD, BAMBOO, CONFLUENCE));
        verify(crowdClient).registerWebhook(contains("/builds"), isNull(String.class));
        verifyNoMoreInteractions(crowdClient);
    }

    @Test
    public void testRemovingApplicationFromExistingSetRegistersNothing() throws Exception
    {
        final OnDemandWebhooksMaintainerListener sut = makeSut(CROWD, BAMBOO, CONFLUENCE);

        sut.onUpdate(licensedChangedEventFor(CROWD, BAMBOO));
        verify(crowdClient, never()).registerWebhook(anyString(), isNull(String.class));
        verifyZeroInteractions(crowdClient);
    }

    @Test
    public void testRemovingCrowdDoesNothing() throws Exception
    {
        final OnDemandWebhooksMaintainerListener sut = makeSut(JIRA, CROWD);

        sut.onUpdate(licensedChangedEventFor(JIRA));
        verify(crowdClient, never()).registerWebhook(anyString(), isNull(String.class));
        verifyZeroInteractions(crowdClient);
    }

    @Test
    public void testAddingCrowdDoesNothing() throws Exception
    {
        final OnDemandWebhooksMaintainerListener sut = makeSut(JIRA);

        sut.onUpdate(licensedChangedEventFor(JIRA, CROWD));
        verify(crowdClient, never()).registerWebhook(anyString(), isNull(String.class));
        verifyZeroInteractions(crowdClient);
    }

    @Test
    public void testAllApplicationsGenerateExpectedEndpoints() throws Exception
    {
        final OnDemandWebhooksMaintainerListener sut = makeSut();
        sut.onUpdate(new LicenseChangedEvent(ALL_APPS));

        // Only expected to run three times: one add for JIRA, Confluence and Bamboo
        verify(crowdClient).registerWebhook(eq(TEST_BASE_URL + "/plugins/servlet/crowdnotifyendpoint"),
            Matchers.<String>eq(null));
        verify(crowdClient).registerWebhook(eq(TEST_BASE_URL + "/wiki/plugins/servlet/crowdnotifyendpoint"),
            Matchers.<String>eq(null));
        verify(crowdClient).registerWebhook(eq(TEST_BASE_URL + "/builds/plugins/servlet/crowdnotifyendpoint"),
            Matchers.<String>eq(null));
        verifyNoMoreInteractions(crowdClient);
    }

    private OnDemandWebhooksMaintainerListener makeSut(LicenseableApplication... apps)
    {
        return new OnDemandWebhooksMaintainerListener(crowdClientFactory, MoreExecutors.sameThreadExecutor(),
            licensedChangedEventFor(apps), listenerProperties);
    }

    private static LicenseChangedEvent licensedChangedEventFor(LicenseableApplication... apps)
    {
        return new LicenseChangedEvent(ImmutableSet.copyOf(apps));
    }

    private static String getLocalhostDomain()
    {
        try
        {
            return InetAddress.getLocalHost().getHostName();
        }
        catch (UnknownHostException e)
        {
            // Shouldn't happen for localhost.
            throw new RuntimeException(e);
        }
    }
}
