package com.atlassian.crowd.horde.healthcheck;

import com.atlassian.healthcheck.core.HealthStatus;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

public class HealthCheckTestHelpers
{
    public static void assertUnhealthyWithReasonContaining(HealthStatus status, String... substrings)
    {
        assertFalse(status.isHealthy());
        for (String substring : substrings)
        {
            assertThat(status.failureReason(), containsString(substring));
        }
    }
}
