package com.atlassian.crowd.horde.license.listeners;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.atlassian.crowd.horde.license.LicenseableApplication;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.model.user.Users;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.CrowdClient;
import com.atlassian.crowd.service.factory.CrowdClientFactory;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.util.concurrent.MoreExecutors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.horde.license.LicenseableApplication.FECRU;
import static com.atlassian.crowd.horde.license.LicenseableApplication.JIRA;
import static com.atlassian.crowd.horde.license.listeners.AdminsToGroupsRunnable.ADMINISTRATORS_GROUP;
import static com.atlassian.crowd.horde.license.listeners.AdminsToGroupsRunnable.MAX_ADMINS_TO_ADD;
import static com.atlassian.crowd.horde.license.listeners.MostRecentlyLoggedInUsersContainer.SIZE_CAP;
import static java.lang.Math.min;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AdminsToGroupsRunnableTest
{
    private static final String TEST_BASE_URL = "http://testbase";
    private static final String LICENSED_JIRA_GROUP = "_licensed-jira";

    private AdminsToGroupsRunnable sut;

    @Mock
    private CrowdClientFactory crowdClientFactory;

    @Mock
    private CrowdClient crowdClient;

    @Mock
    private CommonListenerProperties listenerProperties;

    @Before
    public void setupTest()
    {
        when(listenerProperties.getRetries()).thenReturn(6);
        when(listenerProperties.getRetryDelay()).thenReturn(10l);
        when(listenerProperties.getMaintainerApplicationName()).thenReturn(JIRA.getApplicationName());
        when(listenerProperties.getApplicationPassword()).thenReturn("password");
        when(listenerProperties.getBaseUrl()).thenReturn(TEST_BASE_URL);
        when(listenerProperties.getCrowdNotifyEndpoint()).thenReturn("/plugins/servlet/crowdnotifyendpoint");

        when(crowdClientFactory.newInstance(any(ClientProperties.class))).thenReturn(crowdClient);
    }

    @Test
    public void testThatNoChangesResultsInNoOperations() throws Exception
    {
        sut = makeSut(ImmutableSet.<LicenseableApplication>of());

        registerUsersWithGroups(ImmutableSet.of(ADMINISTRATORS_GROUP), "costa", "greg", "george");

        sut.run();
        verifyZeroInteractions(crowdClient);
    }

    @Test
    public void testAllRecentlyDiscoveredAdminsAreAddedToGroups() throws Exception
    {
        sut = makeSut();

        registerUsersWithGroups(ImmutableSet.of(ADMINISTRATORS_GROUP), "costa", "greg", "george");

        sut.run();

        verify(crowdClient).addUserToGroup("costa", LICENSED_JIRA_GROUP);
        verify(crowdClient).addUserToGroup("greg", LICENSED_JIRA_GROUP);
        verify(crowdClient).addUserToGroup("george", LICENSED_JIRA_GROUP);
        expectAdminsAdded(3);
        expectUserDetailsRequested(3);

        verify(crowdClient).getNamesOfUsersOfGroup(eq(LICENSED_JIRA_GROUP), anyInt(), anyInt());
        verify(crowdClient).getNamesOfUsersOfGroup(eq(ADMINISTRATORS_GROUP), anyInt(), anyInt());
        verifyNoMoreInteractions(crowdClient);
    }

    @Test
    public void testThatNoMoreThanTheMaximumCapAreAdddedToGroups() throws Exception
    {
        sut = makeSut();

        final int startValue = 1;
        final int numberOfUsernames = 30;
        final LinkedList<String> usernames = new LinkedList<String>();
        for (int i = startValue; i < startValue + numberOfUsernames; ++i)
        {
            usernames.add(Integer.toString(i));
        }

        registerUsersWithGroups(ImmutableSet.of(ADMINISTRATORS_GROUP), Iterables.toArray(usernames, String.class));

        sut.run();

        expectAdminsAdded(min(SIZE_CAP, MAX_ADMINS_TO_ADD));
        expectUserDetailsRequested(numberOfUsernames);

        // Ensure that the correct administrators were added, the most recent ones.
        final int maxUser = startValue + numberOfUsernames - 1;
        for (int i = maxUser; i > maxUser - MAX_ADMINS_TO_ADD; --i)
        {
            final String username = Integer.toString(i);
            verify(crowdClient).addUserToGroup(eq(username), eq(LICENSED_JIRA_GROUP));
        }

        verify(crowdClient).getNamesOfUsersOfGroup(eq(LICENSED_JIRA_GROUP), anyInt(), anyInt());
        verify(crowdClient).getNamesOfUsersOfGroup(eq(ADMINISTRATORS_GROUP), anyInt(), anyInt());
        verifyNoMoreInteractions(crowdClient);
    }

    @Test(expected = RuntimeException.class)
    public void testErrorIfNoGroupFoundForApplication() throws Exception
    {
        sut = makeSut(ImmutableSet.of(FECRU));

        sut.run();
    }

    @Test
    public void testNothingHappensIfTooManyAdminsInGroupAlready() throws Exception
    {
        sut = makeSut();

        registerUsersWithGroups(ImmutableSet.of(ADMINISTRATORS_GROUP, LICENSED_JIRA_GROUP), "sysadmin", "normaladmin",
            "george");

        sut.run();

        expectAdminsAdded(0);
        expectUserDetailsRequested(0);
        verify(crowdClient).getNamesOfUsersOfGroup(eq(LICENSED_JIRA_GROUP), anyInt(), anyInt());
        verify(crowdClient, never()).getNamesOfUsersOfGroup(eq(ADMINISTRATORS_GROUP), anyInt(), anyInt());
        verifyNoMoreInteractions(crowdClient);

    }

    private AdminsToGroupsRunnable makeSut()
    {
        return new AdminsToGroupsRunnable(crowdClientFactory, ImmutableSet.of(JIRA),
            listenerProperties, MoreExecutors.sameThreadExecutor());
    }

    private AdminsToGroupsRunnable makeSut(Set<LicenseableApplication> licenseableApplication)
    {
        return new AdminsToGroupsRunnable(crowdClientFactory, licenseableApplication,
            listenerProperties, MoreExecutors.sameThreadExecutor());
    }

    private List<User> registerUsersWithGroups(Set<String> groups, String... usernames) throws Exception
    {
        int usersAdded = 0;
        final long currentTime = 123456l;

        final LinkedList<User> users = new LinkedList<User>();
        for (String username : usernames)
        {
            final User userMock = mock(User.class);
            when(userMock.getName()).thenReturn(username);
            when(userMock.isActive()).thenReturn(true);
            users.add(userMock);

            final UserWithAttributes userWithAttributes = mock(UserWithAttributes.class);
            when(userWithAttributes.getName()).thenReturn(username);
            when(userWithAttributes.getValue(UserConstants.LAST_AUTHENTICATED)).thenReturn(Long.toString(
                currentTime + usersAdded));
            when(crowdClient.getUserWithAttributes(eq(username))).thenReturn(userWithAttributes);

            usersAdded++;
        }

        final List<String> usersByName = ImmutableList.copyOf(Users.namesOf(users));

        for (String group : groups)
        {
            when(crowdClient.getUsersOfGroup(eq(group), eq(0), anyInt())).thenReturn(users);
            when(crowdClient.getNamesOfUsersOfGroup(eq(group), eq(0), anyInt())).thenReturn(
                usersByName);
        }

        return users;
    }

    private void expectAdminsAdded(int expected) throws Exception
    {
        verify(crowdClient, times(expected)).addUserToGroup(anyString(), eq(LICENSED_JIRA_GROUP));
    }

    private void expectUserDetailsRequested(int expected) throws Exception
    {
        verify(crowdClient, times(expected)).getUserWithAttributes(anyString());
    }
}
