package com.atlassian.crowd.horde.sysadmin;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.api.UserWithAttributes;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.security.random.SecureTokenGenerator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.atLeastOnce;

import static com.atlassian.crowd.horde.sysadmin.SysadminExpiryServiceImpl.DEFAULT_EXPIRY_PERIOD_MS;
import static com.atlassian.crowd.horde.sysadmin.SysadminExpiryServiceImpl.DEFAULT_SYSADMIN_USERNAME;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SysadminExpiryServiceImplTest
{
    private static final String TEST_GENERATED_PASSWORD = "generated-password";

    @Mock
    private CrowdService crowdService;

    @Mock
    private SecureTokenGenerator secureTokenGenerator;

    @Mock
    private SysadminExpiryServiceImpl.DateFactory dateFactory;

    @Mock
    private UserWithAttributes sysadminWithAttributes;

    @Mock
    private User sysadmin;

    private static final long NOW = DEFAULT_EXPIRY_PERIOD_MS * 6;
    private static final long BEFORE_IN_PERIOD = NOW - 1;
    private static final long BEFORE_OUTSIDE_PERIOD = NOW - DEFAULT_EXPIRY_PERIOD_MS * 2;
    private static final long AFTER_IN_PERIOD = NOW + 1;

    @Before
    public void setupTest() throws Exception
    {
        when(crowdService.getUser(DEFAULT_SYSADMIN_USERNAME)).thenReturn(sysadmin);
        when(crowdService.getUserWithAttributes(DEFAULT_SYSADMIN_USERNAME)).thenReturn(sysadminWithAttributes);
        when(dateFactory.getNow()).thenReturn(NOW);
        when(secureTokenGenerator.generateToken()).thenReturn(TEST_GENERATED_PASSWORD);
    }

    @Test
    public void shouldResetWhenDateDoesNotExistInTheDatabaseYet() throws Exception
    {
        when(sysadminWithAttributes.getValue(UserConstants.PASSWORD_LASTCHANGED)).thenReturn("");

        final SysadminExpiryServiceImpl sut = makeSut();
        sut.checkAndExpireSysadminPassword();

        verifyThatPasswordWasReset();
    }

    @Test
    public void shouldNotResetWhenPasswordLastChangedDateIsAfterNow() throws Exception
    {
        when(sysadminWithAttributes.getValue(UserConstants.PASSWORD_LASTCHANGED)).thenReturn(Long.toString(
            AFTER_IN_PERIOD));

        final SysadminExpiryServiceImpl sut = makeSut();
        sut.checkAndExpireSysadminPassword();

        verifyThatPasswordWasNotReset();
    }

    @Test
    public void shouldNotResetWhenPasswordLastChangedDateHasNotExpired() throws Exception
    {
        when(sysadminWithAttributes.getValue(UserConstants.PASSWORD_LASTCHANGED)).thenReturn(Long.toString(
            BEFORE_IN_PERIOD));

        final SysadminExpiryServiceImpl sut = makeSut();
        sut.checkAndExpireSysadminPassword();

        verifyThatPasswordWasNotReset();
    }

    @Test
    public void shouldResetWhenPasswordLastChangedDateHasExpired() throws Exception
    {
        when(sysadminWithAttributes.getValue(UserConstants.PASSWORD_LASTCHANGED)).thenReturn(Long.toString(
            BEFORE_OUTSIDE_PERIOD));

        final SysadminExpiryServiceImpl sut = makeSut();
        sut.checkAndExpireSysadminPassword();

        verifyThatPasswordWasReset();
    }


    @Test
    public void shouldNotExpirePasswordIfExpiryDateInDatabaseIsInTheFuture() throws Exception
    {
        when(sysadminWithAttributes.getValue(UserConstants.PASSWORD_LASTCHANGED))
                .thenReturn(Long.toString(BEFORE_OUTSIDE_PERIOD)) // According to the in-memory value, the password has expired...
                .thenReturn(Long.toString(AFTER_IN_PERIOD)); // ... but not according the value stored in the db

        final SysadminExpiryServiceImpl sut = makeSut();
        sut.checkAndExpireSysadminPassword();

        verifyThatPasswordWasNotReset();
    }

    @Test
    public void shouldUpdateInMemoryExpiryTimeIfExpiryDateInDatabaseIsInTheFuture() throws Exception
    {
        when(sysadminWithAttributes.getValue(UserConstants.PASSWORD_LASTCHANGED))
                .thenReturn(Long.toString(BEFORE_OUTSIDE_PERIOD)) // According to the in-memory value, the password has expired...
                .thenReturn(Long.toString(AFTER_IN_PERIOD)); // ... but not according the value stored in the db

        final SysadminExpiryServiceImpl sut = makeSut();

        sut.checkAndExpireSysadminPassword();

        assertEquals(Long.valueOf(AFTER_IN_PERIOD + DEFAULT_EXPIRY_PERIOD_MS), sut.getCachedExpiryTime());
    }

    private void verifyThatPasswordWasReset() throws Exception
    {
        verify(crowdService, atLeastOnce()).getUser(DEFAULT_SYSADMIN_USERNAME);
        verify(crowdService, atLeastOnce()).getUserWithAttributes(DEFAULT_SYSADMIN_USERNAME);
        verify(crowdService).updateUserCredential(sysadmin, TEST_GENERATED_PASSWORD);
        verifyNoMoreInteractions(crowdService);
    }

    private void verifyThatPasswordWasNotReset() throws Exception
    {
        verify(crowdService, never()).updateUserCredential(any(User.class), anyString());
    }

    private SysadminExpiryServiceImpl makeSut()
    {
        return new SysadminExpiryServiceImpl(crowdService, secureTokenGenerator, dateFactory);
    }
}
