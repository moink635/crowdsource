package com.atlassian.crowd.horde.servlet;

import java.util.List;

import com.atlassian.healthcheck.core.DefaultHealthStatus;
import com.atlassian.healthcheck.core.HealthCheck;
import com.atlassian.healthcheck.core.HealthCheckManager;

import com.google.common.collect.ImmutableList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAVAILABLE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IndexServletTest
{
    private final MockHttpServletRequest request = new MockHttpServletRequest();
    private final MockHttpServletResponse response = new MockHttpServletResponse();

    @Test
    public void shouldRespondOkWhenHealthCheckPasses() throws Exception
    {
        IndexServlet sut = sutWithHealth(true);

        sut.handleRequest(request, response);

        assertEquals(HTTP_OK, response.getStatus());
    }

    @Test
    public void shouldRespondUnavailableWhenHealthCheckFails() throws Exception
    {
        IndexServlet sut = sutWithHealth(false);

        sut.handleRequest(request, response);

        assertEquals(HTTP_UNAVAILABLE, response.getStatus());
    }

    @Test
    public void shouldRespondOkWhenHealthCheckSkipped() throws Exception
    {
        IndexServlet sut = sutWithHealth(false);
        request.setParameter("skipHealthCheck", "true");

        sut.handleRequest(request, response);

        assertEquals(HTTP_OK, response.getStatus());
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    private static IndexServlet sutWithHealth(boolean health)
    {
        HealthCheck healthCheck = mock(HealthCheck.class);
        HealthCheckManager healthCheckManager = mock(HealthCheckManager.class);
        when(healthCheckManager.performChecks()).thenReturn((List)ImmutableList.of(new DefaultHealthStatus(healthCheck, health, "")));
        return new IndexServlet(healthCheckManager);
    }

}