package com.atlassian.crowd.horde.healthcheck;

import java.util.List;

import com.atlassian.crowd.horde.license.listeners.CommonListenerProperties;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.CrowdClient;
import com.atlassian.crowd.service.factory.CrowdClientFactory;
import com.atlassian.healthcheck.core.HealthStatus;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.horde.healthcheck.HealthCheckTestHelpers.assertUnhealthyWithReasonContaining;
import static com.atlassian.crowd.horde.healthcheck.SysadminGroupsHealthCheck.QUERY_MAX_RESULTS;
import static com.google.common.collect.Iterables.limit;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SysadminGroupsHealthCheckTest
{

    @Mock
    private CrowdClient crowdClient;
    @Mock
    private CrowdClientFactory crowdClientFactory;
    @Mock
    private CommonListenerProperties commonListenerProperties;

    private SysadminGroupsHealthCheck sut;

    @Before
    public void wireUpMocks()
    {
        when(commonListenerProperties.getApplicationPassword()).thenReturn("password");
        when(commonListenerProperties.getBaseUrl()).thenReturn("http://test/");
        when(commonListenerProperties.getContextPath()).thenReturn("/crowd");
        when(commonListenerProperties.getCrowdNotifyEndpoint()).thenReturn("/plugins/servlet/crowdnotifyendpoint");

        when(crowdClientFactory.newInstance(any(ClientProperties.class))).thenReturn(crowdClient);
        sut = new SysadminGroupsHealthCheck(crowdClientFactory, commonListenerProperties);
    }

    @Test
    public void shouldFailIfSysadminIsMissingFromSystemAdministratorsGroup() throws Exception
    {
        setupMembers("system-administrators", ImmutableList.<String>of());
        HealthStatus status = sut.check();
        assertUnhealthyWithReasonContaining(status, "system-administrators");
    }

    @Test
    public void shouldFailIfMoreThanJustSysadminInSystemAdministratorsGroup() throws Exception
    {
        setupMembers("system-administrators", ImmutableList.of("sysadmin", "user"));
        HealthStatus status = sut.check();
        assertUnhealthyWithReasonContaining(status, "system-administrators");
    }

    @Test
    public void shouldFailIfOneNonSysadminInConfluenceAdministratorsGroup() throws Exception
    {
        setupMembers("confluence-administrators", ImmutableList.of("user"));
        HealthStatus status = sut.check();
        assertUnhealthyWithReasonContaining(status, "confluence-administrators");
    }

    @Test
    public void shouldFailIfMoreThanJustSysadminInConfluenceAdministratorsGroup() throws Exception
    {
        setupMembers("confluence-administrators", ImmutableList.of("sysadmin", "user"));
        HealthStatus status = sut.check();
        assertUnhealthyWithReasonContaining(status, "confluence-administrators");
    }

    @Test
    public void shouldPassIfOnlySysadminIsInSystemAdministratorsGroup() throws Exception
    {
        setupMembers("system-administrators", ImmutableList.of("sysadmin"));
        HealthStatus status = sut.check();
        assertTrue(status.isHealthy());
    }

    @Test
    public void shouldPassIfSysadminInSystemAdministratorsAndConfluenceAdministratorsGroups() throws Exception
    {
        setupMembers("system-administrators", ImmutableList.of("sysadmin"));
        setupMembers("confluence-administrators", ImmutableList.of("sysadmin"));
        HealthStatus status = sut.check();
        assertTrue(status.isHealthy());
    }

    @Test
    public void shouldReportUnexpectedUsersOnFailure() throws Exception
    {
        setupMembers("system-administrators", ImmutableList.of("user1"));
        HealthStatus status = sut.check();
        assertUnhealthyWithReasonContaining(status, "user1");
    }

    @Test
    public void shouldReportUnexpectedUsersFromBothGroupsIfBothAreProblematic() throws Exception
    {
        setupMembers("system-administrators", ImmutableList.of("user1"));
        setupMembers("confluence-administrators", ImmutableList.of("user2"));
        HealthStatus status = sut.check();
        assertUnhealthyWithReasonContaining(status, "system-administrators", "user1", "confluence-administrators", "user2");
    }

    @Test
    public void shouldIndicateInReportIfListIsTruncated() throws Exception
    {
        Builder<String> unexpectedSysadmins = ImmutableList.builder();
        for (int i=0; i<=QUERY_MAX_RESULTS; i++)
        {
            unexpectedSysadmins.add("user"+i);
        }

        setupMembers("system-administrators", unexpectedSysadmins.build());
        HealthStatus status = sut.check();
        assertUnhealthyWithReasonContaining(status, "list capped");
    }

    /**
     * Setup up <code>group</code> to contain <code>members</code>. Note that number of results to queries for members
     * via <code>crowdClient</code> will be limited by QUERY_MAX_RESULTS (so the member list supplied here may be
     * truncated on retrieval).
     */
    private void setupMembers(String group, List<String> members) throws Exception
    {
        when(crowdClient.getNamesOfUsersOfGroup(group, 0, QUERY_MAX_RESULTS))
            .thenReturn(ImmutableList.copyOf(limit(members, QUERY_MAX_RESULTS)));
    }

}
