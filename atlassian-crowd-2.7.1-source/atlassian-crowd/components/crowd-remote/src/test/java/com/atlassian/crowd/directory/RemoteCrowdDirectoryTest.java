package com.atlassian.crowd.directory;

import java.util.Collections;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.UnsupportedCrowdApiException;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.CrowdClient;
import com.atlassian.crowd.service.factory.CrowdClientFactory;
import com.atlassian.crowd.util.PasswordHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RemoteCrowdDirectoryTest
{
    public static final String USERNAME = "username";
    public static final String EXTERNAL_ID = "externalID";

    @Mock
    private CrowdClient client;
    @Mock
    private PasswordHelper passwordHelper;
    @Mock
    private CrowdClientFactory factory;

    @InjectMocks
    private RemoteCrowdDirectory rcd;

    @Before
    public void initObjectUnderTest()
    {
        when(factory.newInstance(any(ClientProperties.class))).thenReturn(client);

        // the object under test requires a call to setAttributes to initialise itself
        rcd.setAttributes(Collections.<String, String>emptyMap());
    }

    @Test
    public void getUserPropagatesExternalIdFromRemoteCrowdServer() throws Exception
    {
        UserWithAttributes remoteUser = mock(UserWithAttributes.class);
        when(remoteUser.getName()).thenReturn(USERNAME);
        when(remoteUser.getExternalId()).thenReturn(EXTERNAL_ID);
        when(client.getUser(USERNAME)).thenReturn(remoteUser);

        User user = rcd.findUserByName(USERNAME);
        assertEquals(USERNAME, user.getName());
        assertEquals(EXTERNAL_ID, user.getExternalId());
    }

    @Test
    public void getUserWithAttributesPropagatesExternalIdFromRemoteCrowdServer() throws Exception
    {
        UserWithAttributes remoteUser = mock(UserWithAttributes.class);
        when(remoteUser.getName()).thenReturn(USERNAME);
        when(remoteUser.getExternalId()).thenReturn(EXTERNAL_ID);
        when(client.getUserWithAttributes(USERNAME)).thenReturn(remoteUser);

        User user = rcd.findUserWithAttributesByName(USERNAME);
        assertEquals(USERNAME, user.getName());
        assertEquals(EXTERNAL_ID, user.getExternalId());
    }

    @Test
    public void getMembershipsUsesCrowdClientCall() throws Exception
    {
        Iterable<Membership> expected = Collections.emptyList();
        when(client.getMemberships()).thenReturn(expected);

        assertSame(expected, rcd.getMemberships());
        verify(client).getMemberships();
    }

    @Test
    public void getMembershipsFallsBackWhenApiNotAvailable() throws Exception
    {
        when(client.getMemberships()).thenThrow(new UnsupportedCrowdApiException("0", "testing"));

        Iterable<Membership> memberships = rcd.getMemberships();

        assertEquals(DirectoryMembershipsIterable.class, memberships.getClass());
    }

    @Test(expected = InvalidAuthenticationException.class)
    public void authenticateFailsWithEncryptedPasswordCredential() throws Exception
    {
        rcd.authenticate("name", new PasswordCredential("hashed", true));
    }

    @Test(expected = InvalidCredentialException.class)
    public void addingUserWithEncryptedCredentialThatIsNotPasswordCredentialShouldFail() throws Exception
    {
        rcd.addUser(new UserTemplate(USERNAME), PasswordCredential.encrypted("hashed-password"));
    }

    @Test
    public void addingUserWithPasswordCredentialNoneShouldUseRandomPassword() throws Exception
    {
        when(passwordHelper.generateRandomPassword()).thenReturn("random-password");

        when(client.getUser(USERNAME)).thenReturn(new UserTemplate(USERNAME));

        rcd.addUser(new UserTemplate(USERNAME), PasswordCredential.NONE);

        ArgumentCaptor<PasswordCredential> credentialCaptor = ArgumentCaptor.forClass(PasswordCredential.class);
        verify(client).addUser(any(User.class), credentialCaptor.capture());
        assertThat(credentialCaptor.getValue().getCredential(), equalTo("random-password"));
    }

    @Test
    public void addingUserWithUnencryptedCredentialShouldUseGivenCredential() throws Exception
    {
        when(client.getUser(USERNAME)).thenReturn(new UserTemplate(USERNAME));

        rcd.addUser(new UserTemplate(USERNAME), PasswordCredential.unencrypted("raw-password"));

        ArgumentCaptor<PasswordCredential> credentialCaptor = ArgumentCaptor.forClass(PasswordCredential.class);
        verify(client).addUser(any(User.class), credentialCaptor.capture());
        assertThat(credentialCaptor.getValue().getCredential(), equalTo("raw-password"));
    }

    @Test(expected = InvalidCredentialException.class)
    public void updateUserCredentialWithEncryptedCredentialThatIsNotPasswordCredentialShouldFail() throws Exception
    {
        rcd.updateUserCredential(USERNAME, PasswordCredential.encrypted("hashed-password"));
    }

    @Test
    public void updateUserCredentialWithPasswordCredentialNoneShouldUseRandomPassword() throws Exception
    {
        when(passwordHelper.generateRandomPassword()).thenReturn("random-password");

        rcd.updateUserCredential(USERNAME, PasswordCredential.NONE);

        verify(client).updateUserCredential(USERNAME, "random-password");
    }

    @Test
    public void updateUserCredentialWithUnencryptedCredentialShouldUseGivenCredential() throws Exception
    {
        rcd.updateUserCredential(USERNAME, PasswordCredential.unencrypted("raw-password"));

        verify(client).updateUserCredential(USERNAME, "raw-password");
    }
}
