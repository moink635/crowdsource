package com.atlassian.crowd.password.encoder;

import com.atlassian.crowd.embedded.api.PasswordCredential;

/**
 * A plaintext password encoder
 */
public class PlaintextPasswordEncoder extends org.springframework.security.authentication.encoding.PlaintextPasswordEncoder
        implements InternalPasswordEncoder, LdapPasswordEncoder
{
    @Override
    public String getKey()
    {
        return "plaintext";
    }

    @Override
    public boolean isPasswordValid(String encPass, String rawPass, Object salt)
    {
        return !rawPass.equals(PasswordCredential.NONE.getCredential()) &&
                super.isPasswordValid(encPass, rawPass, salt);
    }
}
