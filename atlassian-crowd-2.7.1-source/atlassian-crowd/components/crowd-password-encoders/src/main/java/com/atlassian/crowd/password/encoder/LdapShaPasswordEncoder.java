package com.atlassian.crowd.password.encoder;

/**
 * An LDAP based SHA encoder that extends {@link org.acegisecurity.providers.ldap.authenticator.LdapShaPasswordEncoder}
 */
public class LdapShaPasswordEncoder extends org.springframework.security.authentication.encoding.LdapShaPasswordEncoder
        implements LdapPasswordEncoder, InternalPasswordEncoder
{

    public LdapShaPasswordEncoder()
    {
        setForceLowerCasePrefix(false);
    }

    @Override
    public String getKey()
    {
        return "sha";
    }
}
