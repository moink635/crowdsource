package com.atlassian.crowd.password.encoder;

import com.atlassian.security.password.RandomSaltGenerator;
import com.atlassian.security.password.SaltGenerator;

/**
 * This class overrides the {@link org.springframework.security.providers.ldap.authenticator.LdapShaPasswordEncoder} to specifically add salt to the SSHA
 * if it has not been provided
 */
public class LdapSshaPasswordEncoder extends org.springframework.security.authentication.encoding.LdapShaPasswordEncoder implements LdapPasswordEncoder, InternalPasswordEncoder
{
    private static final SaltGenerator DEFAULT_SALT_GENERATOR = new RandomSaltGenerator();

    private final SaltGenerator saltGenerator;

    public LdapSshaPasswordEncoder()
    {
        this(DEFAULT_SALT_GENERATOR);
    }

    LdapSshaPasswordEncoder(SaltGenerator saltGenerator)
    {
        setForceLowerCasePrefix(false);
        this.saltGenerator = saltGenerator;
    }

    /**
     * This method delegates to {@link org.springframework.security.providers.ldap.authenticator.LdapShaPasswordEncoder#encodePassword}, but if the passed in salt is null
     * Crowd will use the propertyManager to find the salt used for Token's and pass that along to the underlying implementation
     *
     * @param rawPass the password to encode
     * @param salt    the salt needs to be of type byte[], if null a random salt value will be used
     * @return String the encoded password
     */
    @Override
    public String encodePassword(String rawPass, Object salt)
    {
        if (salt == null)
        {
            salt = saltGenerator.generateSalt(8);
        }

        return super.encodePassword(rawPass, salt);
    }

    @Override
    public String getKey()
    {
        return "ssha";
    }
}
