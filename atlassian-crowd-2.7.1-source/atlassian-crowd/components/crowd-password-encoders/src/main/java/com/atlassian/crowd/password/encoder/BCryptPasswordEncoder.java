package com.atlassian.crowd.password.encoder;

import java.security.SecureRandom;

import com.atlassian.crowd.exception.PasswordEncoderException;
import com.atlassian.security.random.SecureRandomFactory;


public class BCryptPasswordEncoder implements InternalPasswordEncoder
{
    private final org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder crypto;

    public BCryptPasswordEncoder()
    {
        this(10, SecureRandomFactory.newInstance());
    }

    public BCryptPasswordEncoder(int rounds, SecureRandom random)
    {
        crypto = new org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder(rounds, random);
    }

    @Override
    public String getKey()
    {
        return "bcrypt";
    }

    @Override
    public String encodePassword(String rawPass, Object salt) throws PasswordEncoderException
    {
        return crypto.encode(rawPass);
    }

    @Override
    public boolean isPasswordValid(String encPass, String rawPass, Object salt)
    {
        try
        {
            return crypto.matches(rawPass, encPass);
        }
        catch (IllegalArgumentException e)
        {
            // Unknown salt version
            return false;
        }
        catch (StringIndexOutOfBoundsException e)
        {
            // Badly formatted bcrypt string
            return false;
        }
    }
}
