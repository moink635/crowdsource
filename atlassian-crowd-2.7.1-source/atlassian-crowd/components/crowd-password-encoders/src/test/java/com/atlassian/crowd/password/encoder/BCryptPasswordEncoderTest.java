package com.atlassian.crowd.password.encoder;

import java.security.SecureRandom;
import java.util.Arrays;

import com.google.common.collect.ImmutableSet;

import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class BCryptPasswordEncoderTest
{
    @Test
    public void encryptMatchesTestVector()
    {
        /* The salt the test vector uses */
        final byte[] bytesToProvideAsRandom = {
                119, 18, -120, -16, -6, 36, 127, -3,
                -50, -61, -12, -34, -94, -122, 27, -123,
        };

        SecureRandom sr = new SecureRandom() {
            public synchronized void nextBytes(byte[] bytes)
            {
                assert bytes.length == bytesToProvideAsRandom.length;
                System.arraycopy(bytesToProvideAsRandom, 0, bytes, 0, bytes.length);
            };
        };

        PasswordEncoder encoder = new BCryptPasswordEncoder(5, sr);
        String encoded = encoder.encodePassword("password", null);

        assertEquals("$2a$05$bvIG6Nmid91Mu9RcmmWZfO5HJIMCT8riNW0hEp8f6/FuA2/mHZFpe", encoded);
    }

    @Test
    public void decryptionSucceeds()
    {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        assertTrue(encoder.isPasswordValid(
                "$2a$05$bvIG6Nmid91Mu9RcmmWZfO5HJIMCT8riNW0hEp8f6/FuA2/mHZFpe",
                "password",
                null));
    }

    @Test
    public void decryptionFailsForWrongPassword()
    {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        assertFalse(encoder.isPasswordValid(
                "$2a$05$bvIG6Nmid91Mu9RcmmWZfO5HJIMCT8riNW0hEp8f6/FuA2/mHZFpe",
                "not-password",
                null));
    }

    @Test
    public void decryptionFailsForWrongSalt()
    {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        assertFalse(encoder.isPasswordValid(
                "$2$05$bvIG6Nmid91Mu9RcmmWZfO5HJIMCT8riNW0hEp8f6/FuA2/mHZFpe",
                "password",
                null));
    }

    @Test
    public void decryptionFailsForUnknownSalt()
    {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        assertFalse(encoder.isPasswordValid(
                "$3$05$bvIG6Nmid91Mu9RcmmWZfO5HJIMCT8riNW0hEp8f6/FuA2/mHZFpe",
                "password",
                null));
    }

    @Test
    public void decryptionFailsForBadBCryptString()
    {
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        assertFalse(encoder.isPasswordValid(
                "",
                "password",
                null));
    }

    /**
     * Make sure jBCrypt's Unicode bug is fixed.
     */
    @Test
    public void charactersOutsideAsciiAreDistinct()
    {
        /* Deterministic salt for testing */
        SecureRandom sr = new SecureRandom() {
            public synchronized void nextBytes(byte[] bytes)
            {
                Arrays.fill(bytes, (byte) 0);
            };
        };

        PasswordEncoder encoder = new BCryptPasswordEncoder(10, sr);

        String enc1 = encoder.encodePassword("ππππππππ", null);
        String enc2 = encoder.encodePassword("????????", null);
        String enc3 = encoder.encodePassword("☃☃☃☃☃☃☃☃", null);

        assertThat(ImmutableSet.of(enc1, enc2, enc3),
                IsCollectionWithSize.hasSize(3));

        assertEquals("$2a$10$......................18vkaI3X2CE8XV.hSM7CRMDQ3oXSXwG", enc3);
    }
}
