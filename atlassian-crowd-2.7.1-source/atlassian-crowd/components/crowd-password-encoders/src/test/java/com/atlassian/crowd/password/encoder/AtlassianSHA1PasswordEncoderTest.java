package com.atlassian.crowd.password.encoder;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AtlassianSHA1PasswordEncoderTest
{
    @Test
    public void encodingIsDeterministic()
    {
        AtlassianSHA1PasswordEncoder encoder = new AtlassianSHA1PasswordEncoder();
        assertEquals(
                "uQieO/1CGMUIXXftw3ynrsaYLShI+GTcPS4LdUGWbIusFvHPfUzD7CZvms6yMMvA8I7FViHVEqr6Mj4pCLKAFQ==",
                encoder.encodePassword("sphere", null));
    }

    @Test
    public void validPasswordsAreMatched()
    {
        AtlassianSHA1PasswordEncoder encoder = new AtlassianSHA1PasswordEncoder();
        assertTrue(encoder.isPasswordValid(
                "uQieO/1CGMUIXXftw3ynrsaYLShI+GTcPS4LdUGWbIusFvHPfUzD7CZvms6yMMvA8I7FViHVEqr6Mj4pCLKAFQ==", "sphere", null));
    }


    @Test
    public void invalidEncodedPasswordDoesNotMatch()
    {
        AtlassianSHA1PasswordEncoder encoder = new AtlassianSHA1PasswordEncoder();
        assertFalse(encoder.isPasswordValid(
                "not-a-base64-encoded-password-hash", "password", null));
    }
}
