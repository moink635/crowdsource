package com.atlassian.crowd.password.encoder;

import com.atlassian.crowd.embedded.api.PasswordCredential;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PlaintextPasswordEncoderTest
{
    PasswordEncoder encoder = new PlaintextPasswordEncoder();

    @Test
    public void regularPasswordMatches()
    {
        assertTrue(encoder.isPasswordValid("password", "password", null));
    }

    @Test
    public void incorrectPasswordDoesNotMatch()
    {
        assertFalse(encoder.isPasswordValid("password", "wrong-password", null));
    }

    @Test
    public void unsetPasswordValueDoesNotMatch()
    {
        assertFalse(encoder.isPasswordValid(PasswordCredential.NONE.getCredential(), PasswordCredential.NONE.getCredential(), null));
    }
}
