package com.atlassian.crowd.password.encoder;

import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.security.password.SaltGenerator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * LdapSshaPasswordEncoder Tester.
 */
@RunWith(MockitoJUnitRunner.class)
public class LdapSshaPasswordEncoderTest
{
    LdapSshaPasswordEncoder passwordEncoder = null;

    @Before
    public void setUp() throws Exception
    {
        passwordEncoder = new LdapSshaPasswordEncoder();
    }

    @Test
    public void testEncodePasswordWithNullSalt()
            throws PropertyManagerException
    {
        String encodedPassword = passwordEncoder.encodePassword("secret", null);

        assertNotNull(encodedPassword);
        assertNotSame("secret", encodedPassword);
    }

    @Test
    public void testEncodePasswordWithSalt()
            throws PropertyManagerException
    {
        String encodedPassword = passwordEncoder.encodePassword("secret", "secret-salt".getBytes());

        assertNotNull(encodedPassword);
        assertNotSame("secret", encodedPassword);
    }

    @Test
    public void encodePasswordWithNullSaltUsesSaltGenerator()
    {
        SaltGenerator mockDefaultSaltGenerator = mock(SaltGenerator.class);
        LdapSshaPasswordEncoder passwordEncoder = new LdapSshaPasswordEncoder(mockDefaultSaltGenerator);
        assertNotNull(passwordEncoder.encodePassword("secret", null));
        verify(mockDefaultSaltGenerator).generateSalt(8);
    }

    @Test
    public void encodePasswordWithKnownSaltMatchesReferenceVectors()
    {
        /* For the purposes of matching the OpenLDAP sample, return less salt than requested. */
        byte[] salt = {
                0x1D, 0x51, (byte) 0xA7, 0x37
        };

        SaltGenerator saltGenerator = mock(SaltGenerator.class);
        when(saltGenerator.generateSalt(8)).thenReturn(salt);

        LdapSshaPasswordEncoder passwordEncoder = new LdapSshaPasswordEncoder(saltGenerator);
        String encoded = passwordEncoder.encodePassword("secret", null);

        assertEquals("{SSHA}DkMTwBl+a/3DQTxCYEApdUtNXGgdUac3", encoded);
    }
}
