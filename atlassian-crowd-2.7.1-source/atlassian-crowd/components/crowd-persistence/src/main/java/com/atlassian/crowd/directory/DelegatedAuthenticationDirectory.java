package com.atlassian.crowd.directory;

import java.util.Collection;
import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.event.group.AutoGroupCreatedEvent;
import com.atlassian.crowd.event.group.AutoGroupMembershipCreatedEvent;
import com.atlassian.crowd.event.group.AutoGroupMembershipDeletedEvent;
import com.atlassian.crowd.event.user.AutoUserCreatedEvent;
import com.atlassian.crowd.event.user.AutoUserUpdatedEvent;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.InvalidMembershipException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.OperationNotSupportedException;
import com.atlassian.crowd.exception.ReadOnlyGroupException;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.group.Groups;
import com.atlassian.crowd.model.group.InternalDirectoryGroup;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.model.user.TimestampedUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.event.api.EventPublisher;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This implementation of a {@link RemoteDirectory} provides delegated
 * authentication to an underlying remote LDAP implementation.
 * <p/>
 * In essence this means that a User's groups and roles are managed
 * internally to Crowd and only authentication is delegated to the
 * LDAP directory.
 * <p/>
 * Users, group and memberships exist in an internal directory and all
 * query and mutation operations execute on the internal directory.
 * <p/>
 * For a user to successfully authenticate, they must exist in LDAP
 * and must authenticate against LDAP. Passwords are not stored
 * internally.
 * <p/>
 * If the {@link #ATTRIBUTE_CREATE_USER_ON_AUTH} attribute is
 * enabled, the delegated authentication directory will automatically
 * create the user in the internal portion of this directory, once they
 * successfully authenticate against LDAP. The initial user details, in
 * this case, will be obtained from LDAP.
 * <p/>
 * If the {@link #ATTRIBUTE_UPDATE_USER_ON_AUTH} attribute is
 * enabled, the delegated authentication directory will also update
 * the user's details from LDAP automatically whenever they
 * authenticate. The same behaviour will happen if the attribute is not
 * enabled and the user is deleted internally and then re-authenticates.
 * <p/>
 * If the create-on-auth option is not enabled, then users must always
 * be manually created in this directory, before they can authenticate
 * against LDAP. In this scenario, the user details will never be retrieved
 * from LDAP. This is OSUser's default LDAP behaviour.
 */
public class DelegatedAuthenticationDirectory implements RemoteDirectory
{
    private static final Logger logger = LoggerFactory.getLogger(DelegatedAuthenticationDirectory.class);

    public static final String ATTRIBUTE_CREATE_USER_ON_AUTH = "crowd.delegated.directory.auto.create.user";
    public static final String ATTRIBUTE_UPDATE_USER_ON_AUTH = "crowd.delegated.directory.auto.update.user";
    public static final String ATTRIBUTE_LDAP_DIRECTORY_CLASS = "crowd.delegated.directory.type";
    public static final String ATTRIBUTE_KEY_IMPORT_GROUPS = "crowd.delegated.directory.importGroups";

    private final RemoteDirectory ldapDirectory;
    private final InternalRemoteDirectory internalDirectory;
    private final EventPublisher eventPublisher;
    private final DirectoryDao directoryDao;

    public DelegatedAuthenticationDirectory(RemoteDirectory ldapDirectory, InternalRemoteDirectory internalDirectory, EventPublisher eventPublisher, DirectoryDao directoryDao)
    {
        this.ldapDirectory = ldapDirectory;
        this.internalDirectory = internalDirectory;
        this.eventPublisher = eventPublisher;
        this.directoryDao = directoryDao;
    }

    public long getDirectoryId()
    {
        return internalDirectory.getDirectoryId();
    }

    public void setDirectoryId(long directoryId)
    {
        throw new UnsupportedOperationException("You cannot mutate the directoryID of " + this.getClass().getName());
    }

    public String getDescriptiveName()
    {
        return "Delegated Authentication Directory";
    }

    public void setAttributes(Map<String, String> attributes)
    {
        throw new UnsupportedOperationException("You cannot mutate the attributes of " + this.getClass().getName());
    }

    public User findUserByName(String name) throws UserNotFoundException, OperationFailedException
    {
        return internalDirectory.findUserByName(name);
    }

    public UserWithAttributes findUserWithAttributesByName(String name) throws UserNotFoundException, OperationFailedException
    {
        return internalDirectory.findUserWithAttributesByName(name);
    }

    @Override
    public User findUserByExternalId(String externalId) throws UserNotFoundException
    {
        return internalDirectory.findUserByExternalId(externalId);
    }

    /**
     * In addition to the normal authentication behaviour, following a successful
     * authentication the following may occur:
     * <ul>
     * <li>If the user does not exist in the internal directory and
     * {@link #ATTRIBUTE_CREATE_USER_ON_AUTH} is enabled, the user's details
     * will be added to the internal directory.</li>
     * <li>If the user exists in the internal directory and
     * {@link #ATTRIBUTE_UPDATE_USER_ON_AUTH} is enabled, the user's details
     * will be updated in the internal directory.</li>
     * <li>If the user exists in the internal directory and
     * {@link #ATTRIBUTE_UPDATE_USER_ON_AUTH} is enabled and
       the username was changed in remote directory, the user's name
     * will be updated in the internal directory.</li>
     * </ul>
     * A user marked as inactive locally will not be authenticated, retrieved,
     * renamed or updated from the LDAP server.
     *
     * @see RemoteDirectory#authenticate(String, PasswordCredential)
     *
     * @throws OperationFailedException when user rename is not possible
     */
    public User authenticate(String name, PasswordCredential credential) throws UserNotFoundException, InactiveAccountException, InvalidAuthenticationException, ExpiredCredentialException, OperationFailedException
    {
        if (isUserCreateOnAuthEnabled() || isUserUpdateOnAuthEnabled())
        {
            return authenticateAndUpdateOrCreate(name, credential);
        }
        else
        {
            // No update or create allowed - find the local user
            final User internalUser = findUserByName(name);

            // user must be active locally to be considered for remote authentication.
            if (internalUser.isActive())
            {
                // authenticate the user against LDAP
                User ldapUser = ldapDirectory.authenticate(name, credential);
                if (isImportGroupsEnabled())
                {
                    updateGroups(ldapUser, internalUser);
                }
                return internalUser;
            }
            else
            {
                throw new InactiveAccountException(internalUser.getName());
            }
        }

    }

    private User authenticateAndUpdateOrCreate(String name, PasswordCredential credential)
            throws InactiveAccountException, ExpiredCredentialException, OperationFailedException, InvalidAuthenticationException, UserNotFoundException
    {
        // authenticate the user against LDAP
        User ldapUser = ldapDirectory.authenticate(name, credential);
        // Try to find the corresponding local user
        User internalUser = findLocalUserByExternalId(ldapUser.getExternalId());
        if (internalUser != null)
        {
            if (!IdentifierUtils.equalsInLowerCase(internalUser.getName(), ldapUser.getName()))
            {
                // We want to rename the existing user
                if (isUserUpdateOnAuthEnabled())
                {
                    // push any existing user out of our way and rename
                    try
                    {
                        internalUser = internalDirectory.forceRenameUser(internalUser, ldapUser.getName());
                    }
                    catch (UserNotFoundException e)
                    {
                        // Pretty unlucky
                        throw new ConcurrentModificationException("Unable to rename '" + internalUser.getName() + "' to new name '" + ldapUser.getName() + "' during login.");
                    }
                }
                else
                {
                    // Not allowed to rename - fall through and check user by name
                    internalUser = null;
                }
            }
        }
        if (internalUser == null)
        {
            // Search by username
            try
            {
                internalUser = internalDirectory.findUserByName(name);
                if (StringUtils.isNotBlank(internalUser.getExternalId()) && !internalUser.getExternalId().equals(ldapUser.getExternalId()))
                {
                    // The local user has the wrong external ID and so probably wants to be renamed
                    if (isUserUpdateOnAuthEnabled() && isUserCreateOnAuthEnabled())
                    {
                        try
                        {
                            // try to find a user in the LDAP server with this External ID
                            User movedLdapUser = ldapDirectory.findUserByExternalId(internalUser.getExternalId());
                            internalDirectory.forceRenameUser(internalUser, movedLdapUser.getName());
                            return createLdapUserInLocalCache(name, ldapUser);
                        }
                        catch (UserNotFoundException ex)
                        {
                            // user deleted remotely - let this user take over the account
                        }
                    }
                }
            }
            catch (UserNotFoundException ex)
            {
                if (isUserCreateOnAuthEnabled())
                {
                    return createLdapUserInLocalCache(name, ldapUser);
                }
                else
                {
                    throw ex;
                }
            }
        }
        // We have found a corresponding user - they are not allowed to login if they are inactive locally
        if (!internalUser.isActive())
        {
            throw new InactiveAccountException(name);
        }
        if (isUserUpdateOnAuthEnabled())
        {
            internalUser = updateLocalUserDetails(ldapUser, internalUser);
        }
        if (isImportGroupsEnabled())
        {
            updateGroups(ldapUser, internalUser);
        }
        return internalUser;
    }

    private User createLdapUserInLocalCache(final String name, final User ldapUser)
            throws OperationFailedException, InvalidAuthenticationException, InactiveAccountException
    {
        try
        {
            return addLdapUser(ldapUser);
        }
        catch (InvalidUserException e1)
        {
            // couldn't add the user because it was declared invalid by the internal directory
            throw new InvalidAuthenticationException("Failed to clone LDAP user <" + name + "> to internal directory", e1);
        }
        catch (UserAlreadyExistsException e1)
        {
            // This just appears that somebody else has just created the user locally.
            // In this case, we can simply return the newly created user.
            logger.info("User " + name + " could not be found initially, but when cloning the user internally, user exists");

            final User user;
            try
            {
                user = findUserByName(name);
            }
            catch (UserNotFoundException e)
            {
                // This is just silly - the user was missing, then they appeared, then went we tried to get details they
                // had disappeared again :)
                throw new ConcurrentModificationException("User '" + name + "' no longer exists.");
            }

            // since the suddenly appearing user could have been added by either cloning from ldap or
            // simply by UI, there's a possibility that it's actually inactive. In which case,
            // we make the decision based on the best available information.
            if (!user.isActive())
            {
                throw new InactiveAccountException(user.getName());
            }

            // if the user is active then we can just return the newly created (by somebody else) user
            // since we have already authenticated the account against the remote LDAP.
            return user;
        }
    }

    private User findLocalUserByExternalId(final String externalId)
    {
        try
        {
            return (StringUtils.isNotBlank(externalId)) ? internalDirectory.findUserByExternalId(externalId) : null;
        }
        catch (UserNotFoundException unf)
        {
            return null;
        }
    }

    private void preventExternalIdDuplication(final User ldapUser, final User internalUser)
            throws OperationFailedException, InvalidUserException, DirectoryNotFoundException
    {
        if (StringUtils.isBlank(ldapUser.getExternalId())
                || ldapUser.getExternalId().equals(internalUser.getExternalId()))
        {
            return;
        }

        try
        {
            final TimestampedUser internalUserByExternalId = internalDirectory.findUserByExternalId(ldapUser.getExternalId());
            if (internalUserByExternalId != null)
            {
                removeExternalId(internalUserByExternalId);
                logger.warn("Possible user unique id duplication, removing unique id: " + internalUser.getExternalId() + " for user " + internalUser.getName());
            }
        }
        catch (UserNotFoundException unf)
        {
            //one of the user with the same external id, does not exist any more
        }
    }

    /**
     * Copies or updates a user in the internal directory from their counterpart in the LDAP directory.
     * Used by custom authenticators to ensure users exist when external authentication mechanisms
     * just provide us with just a username.
     *
     * @param name the username of the user to copy
     * @return the newly updated internal user
     * @throws UserNotFoundException    if no user with the given username exists in LDAP
     * @throws OperationFailedException if there was a problem communicating with the LDAP server or the user
     *                                  could not be cloned to the internal directory
     */
    public User addOrUpdateLdapUser(String name) throws UserNotFoundException, OperationFailedException
    {
        User ldapUser = ldapDirectory.findUserByName(name);

        try
        {
            User internalUser = internalDirectory.findUserByName(name);
            User updatedUser = updateLocalUserDetails(ldapUser, internalUser);
            if (isImportGroupsEnabled())
            {
                updateGroups(ldapUser, internalUser);
            }
            return updatedUser;
        } catch (UserNotFoundException e)
        {
            // user doesn't exist internally -- fall through to add them
        }

        try
        {
            return addLdapUser(ldapUser);
        } catch (UserAlreadyExistsException e)
        {
            logger.info("User was added during the internal cloning process. Returning found user.");

            return findUserByName(name);
        } catch (InvalidUserException e)
        {
            throw new OperationFailedException(name, e);
        }
    }

    private User addLdapUser(User user) throws OperationFailedException, InvalidUserException, UserAlreadyExistsException
    {
        try
        {
            final User createdUser = addUser(new UserTemplate(user), null);
            final Directory dir = directoryDao.findById(createdUser.getDirectoryId());
            eventPublisher.publish(new AutoUserCreatedEvent(this, dir, createdUser));

            if (isImportGroupsEnabled())
            {
                final List<String> ldapGroups = getGroups(user, ldapDirectory, String.class);
                importGroupsAndMemberships(user, dir, ldapGroups);
                if (supportsNestedGroups())
                {
                    importGroupHierarchy(ldapGroups, dir);
                }
            }

            return createdUser;
        } catch (InvalidCredentialException e)
        {
            throw new OperationFailedException("Could not create authenticated user <" + user.getName() + "> " +
                    "in underlying InternalDirectory: " + e.getMessage(), e);
        } catch (DirectoryNotFoundException e)
        {
            // directory not found
            throw new ConcurrentModificationException("Directory mapping was removed while cloning a user: " + e.getMessage());
        }
    }

    private void importGroupsAndMemberships(final User user, final Directory dir, final Iterable<String> groupNames) throws OperationFailedException
    {
        for (final String groupName : groupNames)
        {
            try
            {
                final InternalDirectoryGroup group = internalDirectory.findGroupByName(groupName);
                if (group.isLocal())
                {
                    logger.warn("Remote group \"" + groupName + "\" in directory \"" + getDescriptiveName() + "\" is shadowed by a local group of the same name and will not be imported.");
                }
                else
                {
                    logger.debug("Remote group \"" + groupName + "\" in directory \"" + getDescriptiveName() + "\" has already been imported.");
                    importMembership(user, groupName, dir);
                }
            }
            catch (final GroupNotFoundException exception)
            {
                importGroup(groupName, dir);
                importMembership(user, groupName, dir);
            }
        }
    }

    private void importGroup(String groupName, Directory dir)
    {
        try
        {
            final GroupTemplate groupTemplate = new GroupTemplate(groupName, internalDirectory.getDirectoryId());
            groupTemplate.setLocal(false);
            final Group createdGroup = internalDirectory.addGroup(groupTemplate);
            logger.info("Imported remote group \"" + groupName + "\" to directory \"" + getDescriptiveName() + "\".");
            eventPublisher.publish(new AutoGroupCreatedEvent(this, dir, createdGroup));
        }
        catch (Exception e)
        {
            logger.error("Could not import remote group \"" + groupName + "\" in directory \"" + getDescriptiveName() + "\".", e);
        }
    }

    private void importMembership(final User user, final String groupName, final Directory dir)
    {
        try
        {
            addUserToGroup(user.getName(), groupName);
            logger.info("Imported user \"" + user.getName() + "\"'s membership of remote group \"" + groupName + "\" to directory \"" + getDescriptiveName() + "\".");
            eventPublisher.publish(new AutoGroupMembershipCreatedEvent(this, dir, user.getName(), groupName, MembershipType.GROUP_USER));
        } catch (final Exception exception)
        {
            logger.error("Could not import user \"" + user.getName() + "\"'s membership of remote group \"" + groupName + "\" to directory \"" + getDescriptiveName() + "\".", exception);
        }
    }

    private void importGroupMembership(String childGroupName, String parentGroupName, Directory dir)
    {
        try
        {
            addGroupToGroup(childGroupName, parentGroupName);
            logger.info("Imported group \"" + childGroupName + "\"'s membership of remote group \"" + parentGroupName + "\" to directory \"" + getDescriptiveName() + "\".");
            eventPublisher.publish(new AutoGroupMembershipCreatedEvent(this, dir, childGroupName, parentGroupName, MembershipType.GROUP_GROUP));
        }
        catch (Exception exception)
        {
            logger.error("Could not import group \"" + childGroupName + "\"'s membership of remote group \"" + parentGroupName + "\" to directory \"" + getDescriptiveName() + "\".", exception);
        }
    }

    private void removeGroupMembership(String childGroupName, String parentGroupName, Directory dir)
    {
        try
        {
            removeGroupFromGroup(childGroupName, parentGroupName);
            logger.info("Removed group \"" + childGroupName + "\"'s membership of remote group \"" + parentGroupName + "\" in directory \"" + getDescriptiveName() + "\".");
            eventPublisher.publish(new AutoGroupMembershipDeletedEvent(this, dir, childGroupName, parentGroupName, MembershipType.GROUP_GROUP));
        }
        catch (Exception exception)
        {
            logger.error("Could not remove group \"" + childGroupName + "\"'s membership of remote group \"" + parentGroupName + "\" in directory \"" + getDescriptiveName() + "\".", exception);
        }
}

    private User updateLocalUserDetails(User ldapUser, User internalUser) throws OperationFailedException
    {
        try
        {
            final UserTemplate template = new UserTemplate(ldapUser);
            // maintain active/inactive state from internal directory
            template.setActive(internalUser.isActive());
            // This method will never be called on to do a "real" rename, but it may want to do a case-only change.
            if (!ldapUser.getName().equals(internalUser.getName()))
            {
                try
                {
                    renameUser(internalUser.getName(), ldapUser.getName());
                }
                catch (UserAlreadyExistsException e)
                {
                    //This should not happen, however, when it does, proceed without renaming
                    template.setName(internalUser.getName());
                    logger.warn("Remote username [ {} ] casing differs from local username [ {} ], but the username cannot be updated", ldapUser.getName(), internalUser.getName());
                }
            }

            // We want to update the externalId of this user, but there exists some corner cases where another user can have this external ID.
            // See https://extranet.atlassian.com/display/JIRADEV/LDAP+Delegated+Authentication+expected+behavior+discussion
            preventExternalIdDuplication(ldapUser, internalUser);

            final User updatedUser = updateUser(template);
            final Directory dir = directoryDao.findById(updatedUser.getDirectoryId());
            eventPublisher.publish(new AutoUserUpdatedEvent(this, dir, updatedUser));

            return updatedUser;
        } catch (UserNotFoundException e)
        {
            throw new ConcurrentModificationException("User was removed during cloning process: " + e.getMessage());
        } catch (DirectoryNotFoundException e)
        {
            // directory not found
            throw new ConcurrentModificationException("Directory mapping was removed while cloning a user: " + e.getMessage());
        } catch (InvalidUserException e)
        {
            throw new OperationFailedException("Invalid user: unable to update user: '" + ldapUser.getName() + "' with data from LDAP", e);
        }
    }

    private void removeExternalId(final User user)
            throws UserNotFoundException, InvalidUserException, OperationFailedException, DirectoryNotFoundException
    {
        UserTemplate userTemplate = new UserTemplate(user);
        userTemplate.setExternalId(null);
        final Directory dir = directoryDao.findById(user.getDirectoryId());

        updateUser(userTemplate);
        eventPublisher.publish(new AutoUserUpdatedEvent(this, dir, userTemplate));
    }

    private void updateGroups(final User ldapUser, final User internalUser)
    {
        try
        {
            final Directory dir = directoryDao.findById(ldapUser.getDirectoryId());
            final Set<String> ldapGroupNames = Sets.newHashSet(getGroups(ldapUser, ldapDirectory, String.class));
            final Map<String, Group> internalGroupsMap = Maps.uniqueIndex(getGroups(internalUser, internalDirectory, Group.class), Groups.NAME_FUNCTION);
            final Set<String> internalGroupNames = internalGroupsMap.keySet();
            for (final String groupName : Sets.difference(internalGroupNames, ldapGroupNames))
            {
                if (!((InternalDirectoryGroup) internalGroupsMap.get(groupName)).isLocal())
                {
                    try
                    {
                        removeUserFromGroup(internalUser.getName(), groupName);
                        eventPublisher.publish(new AutoGroupMembershipDeletedEvent(this, dir, internalUser.getName(), groupName, MembershipType.GROUP_USER));
                        logger.info("Deleted user \"" + internalUser.getName() + "\"'s imported membership of remote group \"" + groupName + "\" to directory \"" + getDescriptiveName() + "\".");
                    } catch (final Exception exception)
                    {
                        logger.error("Could not delete user \"" + internalUser.getName() + "\"'s imported membership of remote group \"" + groupName + "\" to directory \"" + getDescriptiveName() + "\".", exception);
                    }
                }
            }
            importGroupsAndMemberships(internalUser, dir, Sets.difference(ldapGroupNames, internalGroupNames));
            if (supportsNestedGroups())
            {
                importGroupHierarchy(ldapGroupNames, dir);
            }
        }
        catch (DirectoryNotFoundException e)
        {
            // directory not found
            throw new ConcurrentModificationException("Directory mapping was removed while updating the groups of a user " + e.getMessage());
        }
        catch (final Exception exception)
        {
            logger.error("Could not update remote group imported memberships of user \"" + internalUser.getName() + "\" in directory \"" + getDescriptiveName() + "\".", exception);
        }
    }

    private void importGroupHierarchy(Collection<String> ldapGroupNames, Directory dir) throws OperationFailedException
    {
        importGroupHierarchy(ldapGroupNames, dir, Collections.<String>emptySet());
    }

    private void importGroupHierarchy(Collection<String> ldapGroupNames, Directory dir, Set<String> alreadySyncGroups) throws OperationFailedException
    {
        for (String ldapGroupName : ldapGroupNames)
        {
            if (!alreadySyncGroups.contains(ldapGroupName))
            {
                // add this group to the set of visited groups
                Set<String> newAlreadySyncGroups = ImmutableSet.<String>builder()
                    .addAll(alreadySyncGroups)
                    .add(ldapGroupName)
                    .build();

                MembershipQuery<String> directParentGroupsQuery = getDirectParentGroupsQuery(ldapGroupName);
                Set<String> ldapDirectParentGroups =
                    ImmutableSet.copyOf(ldapDirectory.searchGroupRelationships(directParentGroupsQuery));
                Set<String> internalDirectParentGroups =
                    ImmutableSet.copyOf(internalDirectory.searchGroupRelationships(directParentGroupsQuery));

                for (String directParentGroupInLdapButNotInInternal : Sets.difference(ldapDirectParentGroups, internalDirectParentGroups))
                {
                    if (!newAlreadySyncGroups.contains(directParentGroupInLdapButNotInInternal))
                    {
                        try
                        {
                            InternalDirectoryGroup internalParentGroup = internalDirectory.findGroupByName(
                                directParentGroupInLdapButNotInInternal);
                            if (internalParentGroup.isLocal())
                            {
                                logger.warn("Remote group \"" + internalParentGroup.getName() + "\" in directory \"" + getDescriptiveName() + "\" is shadowed by a local group of the same name and will not be imported.");
                            }
                            else
                            {
                                logger.debug("Remote group \"" + internalParentGroup.getName() + "\" in directory \"" + getDescriptiveName() + "\" has already been imported.");
                                importGroupMembership(ldapGroupName, directParentGroupInLdapButNotInInternal, dir);
                            }
                        }
                        catch (GroupNotFoundException e)
                        {
                            importGroup(directParentGroupInLdapButNotInInternal, dir);
                            importGroupMembership(ldapGroupName, directParentGroupInLdapButNotInInternal, dir);
                        }
                        catch (Exception e)
                        {
                            logger.error("Could not import group \"" + ldapGroupName + "\"'s membership of remote group \"" + directParentGroupInLdapButNotInInternal
                                         + "\" to directory \"" + getDescriptiveName() + "\".", e);
                        }
                    }
                    else
                    {
                        logger.error("Importing remote group \"" + ldapGroupName + "\"'s membership of remote group \"" + directParentGroupInLdapButNotInInternal
                                     + "\" to directory \"" + getDescriptiveName() + "\" would introduce a loop in the group hierarchy.");
                    }
                }

                for (String directParentGroupInInternalButNotInLdap : Sets.difference(internalDirectParentGroups, ldapDirectParentGroups))
                {
                    removeGroupMembership(ldapGroupName, directParentGroupInInternalButNotInLdap, dir);
                }

                // recursive call
                importGroupHierarchy(ldapDirectParentGroups, dir, newAlreadySyncGroups);
            }
        }
    }

    private MembershipQuery<String> getDirectParentGroupsQuery(String ldapGroupName)
    {
        return QueryBuilder.queryFor(String.class, EntityDescriptor.group())
            .parentsOf(EntityDescriptor.group())
            .withName(ldapGroupName)
            .returningAtMost(EntityQuery.ALL_RESULTS);
    }

    private <T> List<T> getGroups(final User user, final RemoteDirectory directory, final Class<T> returnType) throws OperationFailedException
    {
        return directory.searchGroupRelationships(QueryBuilder.queryFor(returnType, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(user.getName()).returningAtMost(EntityQuery.ALL_RESULTS));
    }

    public User addUser(UserTemplate user, PasswordCredential credential)
            throws InvalidUserException, InvalidCredentialException, UserAlreadyExistsException, OperationFailedException
    {
        return internalDirectory.addUser(user, credential);
    }

    public User updateUser(UserTemplate user) throws InvalidUserException, UserNotFoundException, OperationFailedException
    {
        return internalDirectory.updateUser(user);
    }

    public void updateUserCredential(String username, PasswordCredential credential) throws UserNotFoundException, InvalidCredentialException, OperationFailedException
    {
        throw new OperationNotSupportedException("Passwords are stored in LDAP and are read-only for delegated authentication directory");
    }

    public User renameUser(String oldName, String newName)
            throws UserNotFoundException, InvalidUserException, UserAlreadyExistsException, OperationFailedException
    {
        return internalDirectory.renameUser(oldName, newName);
    }

    public void storeUserAttributes(String username, Map<String, Set<String>> attributes) throws UserNotFoundException, OperationFailedException
    {
        internalDirectory.storeUserAttributes(username, attributes);
    }

    public void removeUserAttributes(String username, String attributeName) throws UserNotFoundException, OperationFailedException
    {
        internalDirectory.removeUserAttributes(username, attributeName);
    }

    public void removeUser(String name) throws UserNotFoundException, OperationFailedException
    {
        internalDirectory.removeUser(name);
    }

    public <T> List<T> searchUsers(EntityQuery<T> query) throws OperationFailedException
    {
        return internalDirectory.searchUsers(query);
    }

    public Group findGroupByName(String name) throws GroupNotFoundException, OperationFailedException
    {
        return internalDirectory.findGroupByName(name);
    }

    public GroupWithAttributes findGroupWithAttributesByName(String name) throws GroupNotFoundException, OperationFailedException
    {
        return internalDirectory.findGroupWithAttributesByName(name);
    }

    public Group addGroup(GroupTemplate group)
            throws InvalidGroupException, OperationFailedException
    {
        group.setLocal(true);
        return internalDirectory.addGroup(group);
    }

    public Group updateGroup(GroupTemplate group)
            throws InvalidGroupException, GroupNotFoundException, OperationFailedException, ReadOnlyGroupException
    {
        return internalDirectory.updateGroup(group);
    }

    public Group renameGroup(String oldName, String newName) throws GroupNotFoundException, InvalidGroupException, OperationFailedException
    {
        return internalDirectory.renameGroup(oldName, newName);
    }

    public void storeGroupAttributes(String groupName, Map<String, Set<String>> attributes) throws GroupNotFoundException, OperationFailedException
    {
        internalDirectory.storeGroupAttributes(groupName, attributes);
    }

    public void removeGroupAttributes(String groupName, String attributeName) throws GroupNotFoundException, OperationFailedException
    {
        internalDirectory.removeGroupAttributes(groupName, attributeName);
    }

    public void removeGroup(String name) throws GroupNotFoundException, OperationFailedException, ReadOnlyGroupException
    {
        internalDirectory.removeGroup(name);
    }

    public <T> List<T> searchGroups(EntityQuery<T> query) throws OperationFailedException
    {
        return internalDirectory.searchGroups(query);
    }

    public boolean isUserDirectGroupMember(String username, String groupName) throws OperationFailedException
    {
        return internalDirectory.isUserDirectGroupMember(username, groupName);
    }

    public boolean isGroupDirectGroupMember(String childGroup, String parentGroup)
            throws OperationFailedException
    {
        return internalDirectory.isGroupDirectGroupMember(childGroup, parentGroup);
    }

    public void addUserToGroup(String username, String groupName)
        throws GroupNotFoundException, UserNotFoundException, OperationFailedException, ReadOnlyGroupException,
               MembershipAlreadyExistsException
    {
        internalDirectory.addUserToGroup(username, groupName);
    }

    public void addGroupToGroup(String childGroup, String parentGroup)
        throws GroupNotFoundException, InvalidMembershipException, OperationFailedException, ReadOnlyGroupException,
               MembershipAlreadyExistsException
    {
        internalDirectory.addGroupToGroup(childGroup, parentGroup);
    }

    public void removeUserFromGroup(String username, String groupName)
            throws GroupNotFoundException, UserNotFoundException, MembershipNotFoundException, OperationFailedException, ReadOnlyGroupException
    {
        internalDirectory.removeUserFromGroup(username, groupName);
    }

    public void removeGroupFromGroup(String childGroup, String parentGroup)
            throws GroupNotFoundException, InvalidMembershipException, MembershipNotFoundException, OperationFailedException, ReadOnlyGroupException
    {
        internalDirectory.removeGroupFromGroup(childGroup, parentGroup);
    }

    public <T> List<T> searchGroupRelationships(MembershipQuery<T> query) throws OperationFailedException
    {
        return internalDirectory.searchGroupRelationships(query);
    }

    public void testConnection() throws OperationFailedException
    {
        ldapDirectory.testConnection();
    }

    public boolean supportsInactiveAccounts()
    {
        return internalDirectory.supportsInactiveAccounts();
    }

    public boolean supportsNestedGroups()
    {
        return ldapDirectory.supportsNestedGroups();
    }

    public boolean isRolesDisabled()
    {
        return true;
    }

    public Set<String> getValues(String key)
    {
        return internalDirectory.getValues(key);
    }

    public String getValue(String key)
    {
        return internalDirectory.getValue(key);
    }

    public Set<String> getKeys()
    {
        return internalDirectory.getKeys();
    }

    public boolean isEmpty()
    {
        return internalDirectory.isEmpty();
    }

    @Override
    public RemoteDirectory getAuthoritativeDirectory()
    {
        return ldapDirectory;
    }

    private boolean isUserCreateOnAuthEnabled()
    {
        return Boolean.parseBoolean(getValue(ATTRIBUTE_CREATE_USER_ON_AUTH));
    }

    private boolean isUserUpdateOnAuthEnabled()
    {
        return Boolean.parseBoolean(getValue(ATTRIBUTE_UPDATE_USER_ON_AUTH));
    }

    private boolean isImportGroupsEnabled()
    {
        return Boolean.parseBoolean(getValue(ATTRIBUTE_KEY_IMPORT_GROUPS));
    }

    @Override
    public Iterable<Membership> getMemberships() throws OperationFailedException
    {
        return internalDirectory.getMemberships();
    }
}
