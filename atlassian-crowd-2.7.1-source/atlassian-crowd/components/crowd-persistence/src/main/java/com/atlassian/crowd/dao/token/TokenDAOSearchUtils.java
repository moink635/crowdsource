package com.atlassian.crowd.dao.token;

import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.NullRestriction;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.TokenTermKeys;

public class TokenDAOSearchUtils
{
    public static boolean tokenMatchesSearchRestriction(final Token token, final SearchRestriction searchRestriction)
    {
        if (searchRestriction instanceof NullRestriction)
        {
            return true;
        }
        else if (searchRestriction instanceof PropertyRestriction)
        {
            return tokenMatchesTermRestriction(token, (PropertyRestriction) searchRestriction);
        }
        else if (searchRestriction instanceof BooleanRestriction)
        {
            return tokenMatchesMultiTermRestriction(token, (BooleanRestriction) searchRestriction);
        }
        else
        {
            throw new IllegalArgumentException("SearchRestriction unsupported: " + searchRestriction.getClass());
        }
    }

    private static boolean tokenMatchesMultiTermRestriction(final Token token, final BooleanRestriction multiRestriction)
    {
        if (multiRestriction.getBooleanLogic() == BooleanRestriction.BooleanLogic.AND)
        {
            boolean match = true;

            for (SearchRestriction restriction : multiRestriction.getRestrictions())
            {
                if (!tokenMatchesSearchRestriction(token, restriction))
                {
                    match = false;
                    break;
                }
            }

            return match;
        }
        else if (multiRestriction.getBooleanLogic() == BooleanRestriction.BooleanLogic.OR)
        {
            boolean match = false;

            for (SearchRestriction restriction : multiRestriction.getRestrictions())
            {
                if (tokenMatchesSearchRestriction(token, restriction))
                {
                    match = true;
                    break;
                }
            }

            return match;
        }
        else
        {
            throw new IllegalArgumentException("BooleanLogic unsupported: " + multiRestriction.getBooleanLogic().getClass());
        }
    }

    private static boolean tokenMatchesTermRestriction(final Token token, final PropertyRestriction restriction)
    {
        if (restriction.getProperty().equals(TokenTermKeys.NAME))
        {
            String value = (String) restriction.getValue();

            switch (restriction.getMatchMode())
            {
                case STARTS_WITH:       return token.getName().startsWith(value);
                case CONTAINS:          return token.getName().contains(value);
                default:                return token.getName().equals(value);
            }
        }
        else if (restriction.getProperty().equals(TokenTermKeys.LAST_ACCESSED_TIME))
        {
            Long value = (Long) restriction.getValue();

            switch (restriction.getMatchMode())
            {
                case GREATER_THAN:      return token.getLastAccessedTime() > value;
                case LESS_THAN:         return token.getLastAccessedTime() < value;
                default:                return token.getLastAccessedTime() == value;
            }
        }
        else if (restriction.getProperty().equals(TokenTermKeys.DIRECTORY_ID))
        {
            Long value = (Long) restriction.getValue();

            switch (restriction.getMatchMode())
            {
                case GREATER_THAN:      return token.getDirectoryId() > value;
                case LESS_THAN:         return token.getDirectoryId() < value;
                default:                return token.getDirectoryId() == value;
            }
        }
        else if (restriction.getProperty().equals(TokenTermKeys.RANDOM_NUMBER))
        {
            Long value = (Long) restriction.getValue();

            switch (restriction.getMatchMode())
            {
                case GREATER_THAN:      return token.getRandomNumber() > value;
                case LESS_THAN:         return token.getRandomNumber() < value;
                default:                return token.getRandomNumber() == value;
            }
        }
        else
        {
            throw new IllegalArgumentException("ProperyRestriction unsupported: " + restriction.getClass());
        }
    }
}
