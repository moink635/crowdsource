package com.atlassian.crowd.directory.loader;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.directory.InternalDirectoryForDelegation;
import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.directory.InternalRemoteDirectory;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.event.api.EventPublisher;
import com.google.common.annotations.VisibleForTesting;

public class DelegatedAuthenticationDirectoryInstanceLoaderImpl extends CachingDirectoryInstanceLoader implements DelegatedAuthenticationDirectoryInstanceLoader
{
    private final DirectoryInstanceLoader ldapDirectoryInstanceLoader;
    private final InternalDirectoryInstanceLoader internalDirectoryInstanceLoader;
    private final DirectoryDao directoryDao;

    public DelegatedAuthenticationDirectoryInstanceLoaderImpl(LDAPDirectoryInstanceLoader ldapDirectoryInstanceLoader, InternalDirectoryInstanceLoader internalDirectoryInstanceLoader, EventPublisher eventPublisher, DirectoryDao directoryDao)
    {
        super(eventPublisher);
        this.ldapDirectoryInstanceLoader = ldapDirectoryInstanceLoader;
        this.internalDirectoryInstanceLoader = internalDirectoryInstanceLoader;
        this.directoryDao = directoryDao;
    }

    @Override
    protected RemoteDirectory getNewDirectory(Directory directory) throws DirectoryInstantiationException
    {
        RemoteDirectory ldapDirectory = ldapDirectoryInstanceLoader.getDirectory(getLdapVersionOfDirectory(directory));
        InternalRemoteDirectory internalDirectory = internalDirectoryInstanceLoader.getDirectory(getInternalVersionOfDirectory(directory, ldapDirectory));

        return new DelegatedAuthenticationDirectory(ldapDirectory, internalDirectory, getEventPublisher(), directoryDao);
    }

    private Directory getLdapVersionOfDirectory(Directory directory)
    {
        DirectoryImpl ldap = new DirectoryImpl(directory);

        String ldapClass = directory.getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS);
        ldap.setImplementationClass(ldapClass);

        return ldap;
    }

    @VisibleForTesting
    Directory getInternalVersionOfDirectory(Directory delegatedDirectory, RemoteDirectory ldapDirectory)
    {
        DirectoryImpl internal = new DirectoryImpl(delegatedDirectory);

        internal.setImplementationClass(InternalDirectoryForDelegation.class.getCanonicalName());

        // attributes is probably immutable - build a new Map
        final Map<String, String> newAttributes = new HashMap<String, String>(internal.getAttributes());

        // internal directory needs a password encoder (even if it's just to store blank passwords)
        newAttributes.put(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, PasswordEncoderFactory.SHA_ENCODER);

        // internal directories and LDAP directories use a different attribute key to indicate support for
        // nested groups. We set it to ensure both directories have consistent settings
        newAttributes.put(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS,
                Boolean.toString(ldapDirectory.supportsNestedGroups()));

        internal.setAttributes(newAttributes);

        return internal;
    }

    public RemoteDirectory getRawDirectory(Long id, String className, Map<String, String> attributes) throws DirectoryInstantiationException
    {
        String ldapClass = attributes.get(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS);

        RemoteDirectory ldapDirectory = ldapDirectoryInstanceLoader.getRawDirectory(id, ldapClass, attributes);
        InternalRemoteDirectory internalDirectory = internalDirectoryInstanceLoader.getRawDirectory(id, InternalDirectory.class.getCanonicalName(), attributes);

        return new DelegatedAuthenticationDirectory(ldapDirectory, internalDirectory, getEventPublisher(), directoryDao);
    }

    public boolean canLoad(String className)
    {
        try
        {
            Class clazz = Class.forName(className);
            return DelegatedAuthenticationDirectory.class.isAssignableFrom(clazz);
        }
        catch (ClassNotFoundException e)
        {
            return false;
        }
    }
}
