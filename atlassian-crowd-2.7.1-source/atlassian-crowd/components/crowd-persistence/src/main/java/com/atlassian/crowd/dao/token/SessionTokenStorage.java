package com.atlassian.crowd.dao.token;

import java.util.Date;

import com.atlassian.crowd.exception.ObjectAlreadyExistsException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.token.Token;

/**
 * A store of session tokens, that may be created, retrieved and deleted.
 */
public interface SessionTokenStorage
{
    /**
     * Finds token by random hash.
     *
     * @param randomHash Random hash.
     * @return Token.
     * @throws ObjectNotFoundException if the token identified by the random hash cannot be found.
     */
    Token findByRandomHash(String randomHash) throws ObjectNotFoundException;

    /**
     * Finds token by identifier hash.
     *
     * @param identifierHash Identifier hash.
     * @return Token.
     * @throws ObjectNotFoundException if the token identified by the identifier hash cannot be found.
     */
    Token findByIdentifierHash(String identifierHash) throws ObjectNotFoundException;

    /**
     * Persists a new token.
     *
     * @param token Token.
     * @return The persisted token.
     * @throws ObjectAlreadyExistsException if a token with the same identifier hash already exists.
     */
    Token add(Token token) throws ObjectAlreadyExistsException;

    /**
     * @param token token to update.
     * @return updates the last accessed date on the token (sets it to now).
     * @throws com.atlassian.crowd.exception.ObjectNotFoundException
     */
    Token update(Token token) throws ObjectNotFoundException;

    /**
     * Removes a token.
     *
     * @param token Token.
     */
    void remove(Token token);

    /**
     * Remove token.
     *
     * @param directoryId Directory id.
     * @param name User or application name.
     */
    void remove(long directoryId, String name);

    /**
     * Remove all tokens for the user <em>except</em> for the token
     * specified by <code>exclusionToken</code>.
     *
     * @param directoryId Directory id.
     * @param name User or application name.
     * @param exclusionToken the random hash of the token to retain, if present
     */
    void removeExcept(long directoryId, String name, String exclusionToken);

    /**
     * Remove all tokens associated with the given directory id.
     *
     * @param directoryId Directory id.
     */
    void removeAll(long directoryId);

    /**
     * Remove all tokens that have expired. For a store that performs expiry
     * asynchronously this may be a no-op.
     *
     * @param currentTime Current date
     * @param maxLifeSeconds Max lifespan for tokens, unless they specific a shorter one.
     */
    void removeExpiredTokens(Date currentTime, long maxLifeSeconds);

    /**
     * Wipes all tokens from the store.
     */
    void removeAll();
}
