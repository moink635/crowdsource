package com.atlassian.crowd.search.hibernate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

public class HQLQuery
{
    protected final StringBuilder select = new StringBuilder("");
    protected final StringBuilder from = new StringBuilder(" FROM ");
    protected final StringBuilder where = new StringBuilder(" WHERE ");
    protected final StringBuilder orderBy = new StringBuilder(" ORDER BY ");
    protected int aliasCounter = 0;
    protected boolean distinctRequired = false;
    protected boolean whereRequired = false;
    protected boolean orderByRequired = false;
    protected final List<Object> parameterValues = new ArrayList<Object>();

    public StringBuilder appendSelect(CharSequence hql)
    {
        select.append(hql);
        return this.select;
    }

    public StringBuilder appendFrom(CharSequence hql)
    {
        from.append(hql);
        return this.from;
    }

    public StringBuilder appendWhere(CharSequence hql)
    {
        whereRequired = true;
        where.append(hql);
        return this.where;
    }

    public StringBuilder appendOrderBy(CharSequence hql)
    {
        orderByRequired = true;
        orderBy.append(hql);
        return this.orderBy;
    }

    public String getNextAlias(String baseAliasName)
    {
        aliasCounter++;
        return baseAliasName + aliasCounter;
    }

    /**
     * Creates a variable placeholder for a parameter
     *
     * @param value actual value of the parameter, or <code>null</code> if the parameter is to be ignored
     * @return the JPA-style positional variable (?1, ?2, ...)
     */
    public String addParameterPlaceholder(@Nullable Object value)
    {
        parameterValues.add(value);
        return "?" + Integer.toString(parameterValues.size());
    }

    /**
     * @return a list of parameter values. May contain <code>null</code> elements for parameters that are
     * to be ignored
     */
    public List<Object> getParameterValues()
    {
        return Collections.unmodifiableList(parameterValues);
    }

    public void requireDistinct()
    {
        distinctRequired = true;
    }

    @Override
    public String toString()
    {
        StringBuilder hql = new StringBuilder("SELECT ");
        if (distinctRequired)
        {
            hql.append("DISTINCT ");
        }

        hql.append(select);
        hql.append(from);

        if (whereRequired)
        {
            hql.append(where);
        }
        if (orderByRequired)
        {
            hql.append(orderBy);
        }
        return hql.toString();
    }
}
