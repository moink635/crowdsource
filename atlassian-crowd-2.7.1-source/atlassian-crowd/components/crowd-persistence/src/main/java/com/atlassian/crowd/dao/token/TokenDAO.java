package com.atlassian.crowd.dao.token;

import java.util.Collection;

import com.atlassian.crowd.model.token.Token;

/**
 * Manages persistence of {@link Token}
 */
public interface TokenDAO extends SessionTokenStorage, SearchableTokenStorage
{
    /**
     * Used when switching implementations. Synchronisation is the caller's responsibility; don't allow calls to other
     * methods while this is in progress if you need to guarantee that the data are complete.
     * @return Collection<Token> of all active tokens.
     * @throws org.springframework.dao.DataAccessException If the tokens could not be retrieved.
     */
    Collection<Token> loadAll();

    /**
     * Used when switching implementations. Synchronization is the caller's reponsibility; don't allow calls to other
     * methods while this is in progress if you need to guarantee that the data are complete.
     * @param tokens all tokens to add.
     * @throws org.springframework.dao.DataAccessException
     */
    void saveAll(Collection<Token> tokens);
}
