package com.atlassian.crowd.dao;

/**
 * DAOs that implement this interface can be refreshed to flush their caches and ensure
 * they are up-to-date with the backend storage.
 */
public interface RefreshableDao
{
    /**
     * Flush all the caches, reload everything from the backend storage.
     */
    void refresh();
}
