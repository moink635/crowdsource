package com.atlassian.crowd.dao.webhook;

import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.webhook.Webhook;

/**
 * Persists {@link com.atlassian.crowd.model.webhook.Webhook}s.
 *
 * @since v2.7
 */
public interface WebhookDAO
{
    Webhook findById(Long webhookId) throws WebhookNotFoundException;

    Webhook findByApplicationAndEndpointUrl(Application application, String endpointUrl)
        throws WebhookNotFoundException;

    Webhook add(Webhook webhook);

    void remove(Webhook webhook) throws WebhookNotFoundException;

    Iterable<Webhook> findAll();

    Webhook update(Webhook webhook) throws WebhookNotFoundException;
}
