package com.atlassian.crowd.directory;

import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.GroupDao;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;

/**
 * This is the internal front-end of a delegating directory. It similar to {@link InternalDirectory},
 * but with externally managed and mutable externalIds.
 *
 * @since v2.7
 */
public class InternalDirectoryForDelegation extends InternalDirectory
{
    public InternalDirectoryForDelegation(InternalDirectoryUtils internalDirectoryUtils,
                                          PasswordEncoderFactory passwordEncoderFactory,
                                          DirectoryDao directoryDao, UserDao userDao,
                                          GroupDao groupDao, MembershipDao membershipDao)
    {
        super(internalDirectoryUtils, passwordEncoderFactory, directoryDao, userDao, groupDao, membershipDao);
    }

    @Override
    public boolean isUserExternalIdReadOnly()
    {
        // the external_id is externally managed, and is a copy of the externalId from the remote LDAP server
        return false;
    }
}
