package com.atlassian.crowd.dao.token;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.atlassian.crowd.exception.ObjectAlreadyExistsException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.manager.cache.CacheManager;
import com.atlassian.crowd.manager.cache.NotInCacheException;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.util.SearchResultsUtil;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.equalTo;
import static com.google.common.base.Predicates.not;

/**
 * An in-memory implementation of the TokenDAO. This will use the caching manager.
 *
 * This class is thread-safe.
 */
public class TokenDAOMemory implements TokenDAO
{
    public static final String RANDOM_HASH_CACHE = Token.class.getName() + ".random-hash-cache";
    public static final String IDENTIFIER_HASH_CACHE = Token.class.getName() + ".identifier-hash-cache";

    @Deprecated
    public static final String IDENTIFIER_HASH_CAHE = IDENTIFIER_HASH_CACHE;

    private static final Logger logger = LoggerFactory.getLogger(TokenDAOMemory.class);

    private final ReadWriteLock lock = new ReentrantReadWriteLock();
    private final CacheManager cacheManager;

    public TokenDAOMemory(CacheManager cacheManager)
    {
        this.cacheManager = checkNotNull(cacheManager);
    }

    @Override
    public Token findByRandomHash(String randomHash) throws ObjectNotFoundException
    {
        lock.readLock().lock();
        try
        {
            return internalFindByRandomHash(randomHash);
        }
        finally
        {
            lock.readLock().unlock();
        }
    }

    /**
     * This method finds a token by random hash without requesting a lock. Use only from methods
     * that manage their own locks.
     *
     * @param randomHash key of the token to find
     * @return the token with the given randomHash
     * @throws ObjectNotFoundException if the token cannot be found
     */
    private Token internalFindByRandomHash(String randomHash) throws ObjectNotFoundException
    {
        try
        {
            return (Token) cacheManager.get(RANDOM_HASH_CACHE, randomHash);
        }
        catch (NotInCacheException e)
        {
            // miss
            throw new ObjectNotFoundException(Token.class, randomHash);
        }
    }

    @Override
    public Token findByIdentifierHash(final String identifierHash) throws ObjectNotFoundException
    {
        lock.readLock().lock();
        try
        {
            return (Token) cacheManager.get(IDENTIFIER_HASH_CACHE, identifierHash);
        }
        catch (NotInCacheException e)
        {
            // miss
            throw new ObjectNotFoundException(Token.class, identifierHash);
        }
        finally
        {
            lock.readLock().unlock();
        }
    }

    @Override
    public Token add(Token token) throws ObjectAlreadyExistsException
    {
        lock.writeLock().lock();
        try
        {
            if (containsByIdentifierHash(token.getIdentifierHash()))
            {
                throw new ObjectAlreadyExistsException(token.getIdentifierHash());
            }

            cacheManager.put(RANDOM_HASH_CACHE, token.getRandomHash(), token);
            cacheManager.put(IDENTIFIER_HASH_CACHE, token.getIdentifierHash(), token);
        }
        finally
        {
            lock.writeLock().unlock();
        }
        return token;
    }

    private boolean containsByIdentifierHash(String identifierHash)
    {
        try
        {
            cacheManager.get(IDENTIFIER_HASH_CACHE, identifierHash);
            return true;
        }
        catch (NotInCacheException e)
        {
            return false;
        }
    }

    @Override
    public Token update(Token token)
    {
        lock.writeLock().lock();

        // add the new/updated one.
        try
        {
            token.setLastAccessedTime(System.currentTimeMillis());

            cacheManager.put(RANDOM_HASH_CACHE, token.getRandomHash(), token);
            cacheManager.put(IDENTIFIER_HASH_CACHE, token.getIdentifierHash(), token);
        }
        finally
        {
            lock.writeLock().unlock();
        }
        return token;
    }

    @Override
    public void remove(Token token)
    {
        lock.writeLock().lock();

        // remove the existing token.
        try
        {
            cacheManager.remove(RANDOM_HASH_CACHE, token.getRandomHash());
            cacheManager.remove(IDENTIFIER_HASH_CACHE, token.getIdentifierHash());
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    @Override
    public List<Token> search(final EntityQuery<? extends Token> query)
    {
        checkArgument(query.getEntityDescriptor().getEntityType() == Entity.TOKEN,
                "TokenDAO can only evaluate EntityQueries for Entity.TOKEN");

        ImmutableList.Builder<Token> tokens = ImmutableList.builder();

        lock.readLock().lock();
        try
        {
            for (String key : findRandomHashKeys())
            {
                try
                {
                    Token token = internalFindByRandomHash(key);

                    if (TokenDAOSearchUtils.tokenMatchesSearchRestriction(token, query.getSearchRestriction()))
                    {
                        tokens.add(token);
                    }
                }
                catch (ObjectNotFoundException e)
                {
                    logger.error(e.getMessage(), e);
                }
            }
        }
        finally
        {
            lock.readLock().unlock();
        }

        return SearchResultsUtil.constrainResults(tokens.build(), query.getStartIndex(), query.getMaxResults());
    }

    private List<String> findRandomHashKeys()
    {
        return cacheManager.getAllKeys(RANDOM_HASH_CACHE);
    }

    @Override
    public void remove(long directoryId, String name)
    {
        lock.writeLock().lock();
        try
        {
            List<String> keys = findRandomHashKeys();
            remove(keys, directoryId, name);
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    private void remove(Iterable<String> keys, long directoryId, String name)
    {
        for (String key : keys)
        {
            try
            {
                Token token = internalFindByRandomHash(key);
                if (token.getDirectoryId() == directoryId && token.getName().equals(name))
                {
                    remove(token);
                }
            }
            catch (ObjectNotFoundException e)
            {
                // key already removed
            }
        }
    }

    @Override
    public void removeExcept(long directoryId, String name, String exclusionToken)
    {
        lock.writeLock().lock();
        try
        {
            Iterable<String> keys = Iterables.filter(findRandomHashKeys(), not(equalTo(exclusionToken)));

            remove(keys, directoryId, name);
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    @Override
    public void removeAll(final long directoryId)
    {
        lock.writeLock().lock();
        try
        {
            for (String key : findRandomHashKeys())
            {
                try
                {
                    Token token = internalFindByRandomHash(key);
                    if (token.getDirectoryId() == directoryId)
                    {
                        remove(token);
                    }
                }
                catch (ObjectNotFoundException e)
                {
                    // this should not happen because we have the lock
                    throw new IllegalStateException("Key already removed: " + key);
                }
            }
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    @Override
    public void removeExpiredTokens(final Date currentTime, final long maxLifeInSeconds)
    {
        lock.writeLock().lock();
        try
        {
            for (String key : findRandomHashKeys())
            {
                try
                {
                    Token token = internalFindByRandomHash(key);
                    long effectiveTokenSessionTime = getEffectiveTokenSessionTime(token, maxLifeInSeconds);
                    Date expiryTime = new Date(token.getLastAccessedTime()
                                               + TimeUnit.SECONDS.toMillis(effectiveTokenSessionTime));
                    if (expiryTime.before(currentTime))
                    {
                        remove(token);
                    }
                }
                catch (ObjectNotFoundException e)
                {
                    // this should not happen because we have the lock
                    throw new IllegalStateException("Key already removed: " + key);
                }
            }
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    /**
     * @param token token which effective duration after the last update is to be calculated
     * @param defaultMaxLifeInSeconds default maximum session life in seconds if the token does not specify a shorter one
     * @return the default session time unless the token specifies a shorter duration, values in seconds
     */
    private static long getEffectiveTokenSessionTime(Token token, final long defaultMaxLifeInSeconds)
    {
        TokenLifetime tokenLifetime = token.getLifetime();
        if (tokenLifetime.isDefault())
        {
            return defaultMaxLifeInSeconds;
        }
        else
        {
            return Math.min(tokenLifetime.getSeconds(), defaultMaxLifeInSeconds);
        }
    }

    /**
     * {@see TokenDAOPersistence.loadAll()}
     */
    @Override
    public Collection<Token> loadAll()
    {
        lock.readLock().lock();
        try
        {
            ImmutableList.Builder<Token> tokens = ImmutableList.builder();

            for (String key : findRandomHashKeys())
            {
                try
                {
                    Token token = internalFindByRandomHash(key);
                    tokens.add(token);
                }
                catch (ObjectNotFoundException e)
                {
                    // this should not happen because we have the lock
                    throw new IllegalStateException("Key already removed: " + key);
                }
            }

            return tokens.build();
        }
        finally
        {
            lock.readLock().unlock();
        }
    }

    /**
     * {@see TokenDAOPersistence.saveAll()}
     */
    @Override
    public void saveAll(Collection<Token> tokens)
    {
        checkNotNull(tokens);

        lock.writeLock().lock();
        try
        {
            for (Token token : tokens)
            {
                cacheManager.put(RANDOM_HASH_CACHE, token.getRandomHash(), token);
                cacheManager.put(IDENTIFIER_HASH_CACHE, token.getIdentifierHash(), token);
            }
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }

    /**
     * {@see TokenDAOPersistence.removeAll()}
     */
    @Override
    public void removeAll()
    {
        lock.writeLock().lock();
        try
        {
            cacheManager.removeAll(RANDOM_HASH_CACHE);
            cacheManager.removeAll(IDENTIFIER_HASH_CACHE);
        }
        finally
        {
            lock.writeLock().unlock();
        }
    }
}
