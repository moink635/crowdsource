package com.atlassian.crowd.dao.webhook;

import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.webhook.Webhook;

import com.google.common.collect.ImmutableList;

/**
 * A NOP implementation of WebhookDAO. Not used by Crowd or Horde, but provided for JIRA. Returns an immutable
 * empty list of Webhooks.
 *
 * @since v2.7
 */
public class NoopWebhookDAOImpl implements WebhookDAO
{
    @Override
    public Webhook findById(Long webhookId) throws WebhookNotFoundException
    {
        throw new WebhookNotFoundException(webhookId);
    }

    @Override
    public Webhook findByApplicationAndEndpointUrl(Application application, String endpointUrl)
        throws WebhookNotFoundException
    {
        throw new WebhookNotFoundException(application.getId(), endpointUrl);
    }

    @Override
    public Webhook add(Webhook webhook)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void remove(Webhook webhook) throws WebhookNotFoundException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Iterable<Webhook> findAll()
    {
        return ImmutableList.of();
    }

    @Override
    public Webhook update(Webhook webhook) throws WebhookNotFoundException
    {
        throw new WebhookNotFoundException(webhook.getId());
    }
}
