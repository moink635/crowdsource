package com.atlassian.crowd.dao.token;

import java.util.List;

import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.search.query.entity.EntityQuery;

/**
 * A store that can accept arbitrary queries over the tokens.
 */
public interface SearchableTokenStorage
{
    /**
     * Searches for token based on criteria.
     *
     * @param query Query.
     * @return List of tokens which qualify for the criteria.
     */
    List<Token> search(EntityQuery<? extends Token> query);
}
