package com.atlassian.crowd.util.persistence.hibernate.batch;

/**
 * Hibernate specific batch operation abstraction, shared between hibernate 2 (Confluence) and 4 (Crowd).
 * @param <S> type of the hibernate session which implementations may require in order to be performed.
 */
public interface HibernateOperation<S>
{
    /**
     * Interface to logic that performs a single hibernate operation on a target object for
     * use within a batch.
     *
     * @param object perform the operation on this object
     */
    void performOperation(Object object, S session);
}
