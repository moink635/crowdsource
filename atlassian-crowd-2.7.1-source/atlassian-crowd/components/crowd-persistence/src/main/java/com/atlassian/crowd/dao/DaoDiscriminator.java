package com.atlassian.crowd.dao;

/**
 * Objects that implement this interface can dynamically tell whether a DAO is in a state that permits handling
 * data access operations. For instance, a file-backed DAO requires a file to operate; without the file, the DAO
 * instance may exist, but be inactive. This interface can be implemented by the DAO itself, or by another class.
 */
public interface DaoDiscriminator
{

    /**
     * @return true if the DAO is ready to handle data access operations.
     */
    boolean isActive();

}
