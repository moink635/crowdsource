package com.atlassian.crowd.directory;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.GroupDao;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.user.TimestampedUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.password.encoder.LdapShaPasswordEncoder;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.PasswordHelper;

import com.google.common.collect.ImmutableMap;

import org.hamcrest.collection.IsEmptyCollection;
import org.hamcrest.core.Is;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.test.matchers.CrowdMatchers.user;
import static com.atlassian.crowd.test.matchers.CrowdMatchers.userNamed;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InternalDirectoryForDelegationTest
{
    private static final String ENCRYPTION_ALGORITHM = "atlassian-security";
    private static final String PASSWORD_1 = "mypassword1";
    private static final String ENCRYPTED_PASSWORD_1 = "secret1";
    private static final String TEST_USERNAME = "test";
    private static final String ADMIN_USERNAME = "admin";
    private static final long DIRECTORY_ID = 1;

    // these cannot be static because PasswordCredential is mutable
    private final PasswordCredential encryptedCredential = PasswordCredential.encrypted(ENCRYPTED_PASSWORD_1);
    private final PasswordCredential unencryptedCredential = PasswordCredential.unencrypted(PASSWORD_1);

    @Mock private PasswordEncoderFactory passwordEncoderFactory;
    @Mock private UserDao userDao;
    @Mock private GroupDao groupDao;
    @Mock private MembershipDao membershipDao;
    @Mock private PasswordHelper passwordHelper;
    @Mock private DirectoryDao directoryDao;
    @Mock private LdapShaPasswordEncoder ldapShaPasswordEncoder;

    private UserTemplate testUser;
    private UserTemplate adminUser;
    private DirectoryImpl directoryConfiguration;

    private InternalDirectory internalDelegatingDirectory;

    // declaring this captor as an attribute is syntactically simpler than declaring it in the test method
    @Captor
    private ArgumentCaptor<Set<UserTemplateWithCredentialAndAttributes>> addedUsers;

    @Before
    public void setUp() throws Exception
    {
        adminUser = new UserTemplate(ADMIN_USERNAME, DIRECTORY_ID);
        adminUser.setFirstName("Admin");
        adminUser.setLastName("Istrator");
        adminUser.setEmailAddress("admin@test.com");
        adminUser.setDisplayName("Mr. Admin");
        adminUser.setActive(true);

        testUser = new UserTemplate(TEST_USERNAME, DIRECTORY_ID);
        testUser.setFirstName("Test");
        testUser.setLastName("User");
        testUser.setEmailAddress("test@test.com");
        testUser.setDisplayName("Super Tester");
        testUser.setActive(true);

        InternalEntityTemplate template = new InternalEntityTemplate();
        template.setId(DIRECTORY_ID);
        template.setName("Test Directory");
        directoryConfiguration = new DirectoryImpl(template);
        directoryConfiguration.setType(DirectoryType.INTERNAL);
        directoryConfiguration.setImplementationClass(InternalDirectory.class.getCanonicalName());
        directoryConfiguration.setAttribute(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, ENCRYPTION_ALGORITHM);

        when(passwordEncoderFactory.getInternalEncoder(ENCRYPTION_ALGORITHM)).thenReturn(ldapShaPasswordEncoder);

        when(ldapShaPasswordEncoder.encodePassword(PASSWORD_1, null)).thenReturn(ENCRYPTED_PASSWORD_1);

        I18nHelper i18nHelper = mock(I18nHelper.class);
        when(i18nHelper.getText(anyString())).thenReturn("Test Class Call");
        when(i18nHelper.getText(anyString(), anyObject())).thenReturn("Test Class Call");
        when(i18nHelper.getText(anyString(), anyString(), anyString())).thenReturn("Test Class Call");

        InternalDirectoryUtilsImpl internalDirectoryUtils = new InternalDirectoryUtilsImpl(passwordHelper);

        internalDelegatingDirectory =
                new InternalDirectoryForDelegation(internalDirectoryUtils, passwordEncoderFactory, directoryDao,
                        userDao, groupDao, membershipDao);
        internalDelegatingDirectory.setAttributes(ImmutableMap.of(
                InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, ENCRYPTION_ALGORITHM,
                InternalDirectory.ATTRIBUTE_PASSWORD_HISTORY_COUNT, "1",
                InternalDirectory.ATTRIBUTE_PASSWORD_MAX_CHANGE_TIME, "14" // password maximum age (14 days)
        ));
        internalDelegatingDirectory.setDirectoryId(DIRECTORY_ID);
    }

    /**
     * Required for 'Copy user on login' of Delegating Directories
     */
    @Test
    public void testAddUserWithExternalId() throws Exception
    {
        testUser.setExternalId("my-external-id");
        internalDelegatingDirectory.addUser(testUser, unencryptedCredential);

        verify(userDao).add(argThat(Is.is(userNamed(TEST_USERNAME).withExternalIdOf("my-external-id"))),
                eq(encryptedCredential));
    }

    @Test
    public void testAddAllUsers()
    {
        adminUser.setExternalId("admin-external-id");
        testUser.setExternalId(null);

        Set<UserTemplateWithCredentialAndAttributes> usersToAdd = new HashSet<UserTemplateWithCredentialAndAttributes>();
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(adminUser, encryptedCredential));
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(testUser, encryptedCredential));

        when(userDao.addAll((Set<UserTemplateWithCredentialAndAttributes>) anyObject())).thenReturn(new BatchResult<User>(0));

        Collection<User> failedUsers = internalDelegatingDirectory.addAllUsers(usersToAdd).getFailedEntities();

        // should add all users with no failed users
        verify(userDao, times(1)).addAll(addedUsers.capture());
        assertThat(addedUsers.getValue(), contains(
                user(UserTemplateWithCredentialAndAttributes.class)
                        .withNameOf(ADMIN_USERNAME)
                        .withExternalIdOf("admin-external-id"),
                user(UserTemplateWithCredentialAndAttributes.class)
                        .withNameOf(TEST_USERNAME)
                        .withExternalIdMatching(nullValue(String.class))));  // no externalId is generated

        assertThat(failedUsers, IsEmptyCollection.emptyCollectionOf(User.class));
    }

    @Test
    public void updateUserShouldAllowToChangeExternalId() throws Exception
    {
        TimestampedUser savedUser = mock(TimestampedUser.class);
        when(savedUser.getExternalId()).thenReturn("old-external-id");
        when(userDao.findByName(DIRECTORY_ID, TEST_USERNAME)).thenReturn(savedUser);

        UserTemplate userTemplate = new UserTemplate(TEST_USERNAME, DIRECTORY_ID);
        userTemplate.setExternalId("new-external-id");

        User updatedUser = mock(User.class);
        when(updatedUser.getExternalId()).thenReturn("new-external-id");
        when(userDao.update(any(User.class))).thenReturn(updatedUser);

        User returnedUser = internalDelegatingDirectory.updateUser(userTemplate);

        assertThat(returnedUser.getExternalId(), is("new-external-id"));

        verify(userDao).update(argThat(user(UserTemplate.class).withExternalIdOf("new-external-id")));
    }
}
