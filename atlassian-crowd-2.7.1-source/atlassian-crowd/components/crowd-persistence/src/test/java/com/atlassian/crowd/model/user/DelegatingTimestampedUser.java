package com.atlassian.crowd.model.user;

import java.util.Date;

public class DelegatingTimestampedUser implements TimestampedUser
{
    private final User user;

    public DelegatingTimestampedUser(User user)
    {
        this.user = user;
    }

    @Override
    public long getDirectoryId()
    {
        return user.getDirectoryId();
    }

    @Override
    public boolean isActive()
    {
        return user.isActive();
    }

    @Override
    public String getEmailAddress()
    {
        return user.getEmailAddress();
    }

    @Override
    public String getDisplayName()
    {
        return user.getDisplayName();
    }

    @Override
    public int compareTo(com.atlassian.crowd.embedded.api.User user)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getName()
    {
        return user.getName();
    }

    @Override
    public String getFirstName()
    {
        return user.getFirstName();
    }

    @Override
    public String getLastName()
    {
        return user.getLastName();
    }

    @Override
    public String getExternalId()
    {
        return user.getExternalId();
    }

    @Override
    public Date getCreatedDate()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Date getUpdatedDate()
    {
        throw new UnsupportedOperationException("Not implemented");
    }
}
