package com.atlassian.crowd.directory;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.GroupDao;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidMembershipException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.InternalUserCredentialRecord;
import com.atlassian.crowd.model.user.InternalUserWithAttributes;
import com.atlassian.crowd.model.user.TimestampedUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.password.encoder.LdapShaPasswordEncoder;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.PasswordHelper;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;

import org.hamcrest.Matcher;
import org.hamcrest.text.IsEmptyString;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.test.matchers.CrowdMatchers.user;
import static com.atlassian.crowd.test.matchers.CrowdMatchers.userNamed;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.anyMap;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * InternalDirectory Tester.
 */
@RunWith(MockitoJUnitRunner.class)
public class InternalDirectoryTest
{
    private static final String ENCRYPTION_ALGORITHM = "atlassian-security";
    private static final String PASSWORD_1 = "mypassword1";
    private static final String ENCRYPTED_PASSWORD_1 = "secret1";
    private static final String ADMIN_USERNAME = "admin";

    private static final String TEST_USERNAME = "test";

    private static final String MAIN_GROUP = "main group";
    private static final String MAIN_GROUP_DESCRIPTION = "Top of the world";

    private static final String CHILD_GROUP = "child group";
    private static final String CHILD_GROUP_DESCRIPTION = "Somewhere near the bottom";
    private static final long DIRECTORY_ID = 1;

    // these cannot be static because PasswordCredential is mutable
    private final PasswordCredential encryptedCredential = PasswordCredential.encrypted(ENCRYPTED_PASSWORD_1);
    private final PasswordCredential unencryptedCredential = PasswordCredential.unencrypted(PASSWORD_1);

    @Mock private LdapShaPasswordEncoder ldapShaPasswordEncoder;
    private UserTemplate adminUser;
    private UserTemplate testUser;
    private GroupTemplate mainGroup;
    private GroupTemplate childGroup;
    private InternalUserWithAttributes internalUserWithAttributes;
    @Mock private PasswordEncoderFactory passwordEncoderFactory;
    @Mock private UserDao userDao;
    @Mock private GroupDao groupDao;
    @Mock private MembershipDao membershipDao;
    @Mock private PasswordHelper passwordHelper;
    @Mock private DirectoryDao directoryDao;
    private DirectoryImpl directoryConfiguration;

    private InternalDirectory internalDirectory;

    // declaring these captor as fields is syntactically simpler than declaring it in the test methods
    @Captor
    private ArgumentCaptor<Set<UserTemplateWithCredentialAndAttributes>> addedUsers;
    @Captor
    private ArgumentCaptor<Map<String, Set<String>>> attributesCaptor;

    @Before
    public void setUp() throws Exception
    {
        adminUser = new UserTemplate(ADMIN_USERNAME, DIRECTORY_ID);
        adminUser.setFirstName("Admin");
        adminUser.setLastName("Istrator");
        adminUser.setEmailAddress("admin@test.com");
        adminUser.setDisplayName("Mr. Admin");
        adminUser.setActive(true);

        testUser = new UserTemplate(TEST_USERNAME, DIRECTORY_ID);
        testUser.setFirstName("Test");
        testUser.setLastName("User");
        testUser.setEmailAddress("test@test.com");
        testUser.setDisplayName("Super Tester");
        testUser.setActive(true);

        mainGroup = new GroupTemplate(MAIN_GROUP, DIRECTORY_ID, GroupType.GROUP);
        mainGroup.setDescription(MAIN_GROUP_DESCRIPTION);

        childGroup = new GroupTemplate(CHILD_GROUP, DIRECTORY_ID, GroupType.GROUP);
        childGroup.setDescription(CHILD_GROUP_DESCRIPTION);

        InternalEntityTemplate template = new InternalEntityTemplate();
        template.setId(DIRECTORY_ID);
        template.setName("Test Directory");
        directoryConfiguration = new DirectoryImpl(template);
        directoryConfiguration.setType(DirectoryType.INTERNAL);
        directoryConfiguration.setImplementationClass(InternalDirectory.class.getCanonicalName());
        directoryConfiguration.setAttribute(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, ENCRYPTION_ALGORITHM);

        when(passwordEncoderFactory.getInternalEncoder(ENCRYPTION_ALGORITHM)).thenReturn(ldapShaPasswordEncoder);

        when(ldapShaPasswordEncoder.encodePassword(PASSWORD_1, null)).thenReturn(ENCRYPTED_PASSWORD_1);

        I18nHelper i18nHelper = mock(I18nHelper.class);
        when(i18nHelper.getText(anyString())).thenReturn("Test Class Call");
        when(i18nHelper.getText(anyString(), anyObject())).thenReturn("Test Class Call");
        when(i18nHelper.getText(anyString(), anyString(), anyString())).thenReturn("Test Class Call");

        InternalDirectoryUtilsImpl internalDirectoryUtils = new InternalDirectoryUtilsImpl(passwordHelper);

        internalDirectory = new InternalDirectory(internalDirectoryUtils, passwordEncoderFactory, directoryDao, userDao, groupDao, membershipDao);
        internalDirectory.setAttributes(ImmutableMap.of(
            InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, ENCRYPTION_ALGORITHM,
            InternalDirectory.ATTRIBUTE_PASSWORD_HISTORY_COUNT, "1",
            InternalDirectory.ATTRIBUTE_PASSWORD_MAX_CHANGE_TIME, "14" // password maximum age (14 days)
        ));
        internalDirectory.setDirectoryId(DIRECTORY_ID);
    }

    @Test
    public void testSupportsNestedGroupsEnabled()
    {
        internalDirectory.setAttributes(ImmutableMap.of(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS, "true"));
        assertTrue("Directory should support nested groups", internalDirectory.supportsNestedGroups());
    }

    @Test
    public void testSupportsNestedGroupsDisabled()
    {
        internalDirectory.setAttributes(ImmutableMap.of(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS, "false"));
        assertFalse("Directory should not support nested groups", internalDirectory.supportsNestedGroups());
    }

    @Test
    public void testSupportsNestedGroupsIsDisabledByDefault()
    {
        assertFalse("Nested groups are disabled by default", internalDirectory.supportsNestedGroups());
    }

    /**
     * Test the happy path
     */
    @Test
    public void testAddUser() throws Exception
    {
        when(userDao.add(adminUser, encryptedCredential)).thenReturn(new InternalUser(adminUser, directoryConfiguration, encryptedCredential));

        final User user = internalDirectory.addUser(adminUser, unencryptedCredential);

        assertEquals(ADMIN_USERNAME, user.getName());

        Matcher<String> externalIdMatcher = not(IsEmptyString.isEmptyOrNullString()); // an externalId must be generated
        verify(userDao).add(argThat(is(userNamed(ADMIN_USERNAME).withExternalIdMatching(externalIdMatcher))),
                            eq(encryptedCredential));

        // Verify that the store attributes method was also called.
        verify(userDao, atMost(1)).storeAttributes(eq(adminUser), anyMap());
    }

    @Test
    public void testAddUserSetsPasswordRelatedAttributesIfCredentialIsProvided() throws Exception
    {
        UserTemplate userAddedByDao = new UserTemplate(testUser);
        when(userDao.add(any(User.class), any(PasswordCredential.class))).thenReturn(userAddedByDao);

        internalDirectory.addUser(new UserTemplate(testUser), encryptedCredential);

        verify(userDao).storeAttributes(eq(userAddedByDao), attributesCaptor.capture());
        assertThat(attributesCaptor.getValue().keySet(),
                containsInAnyOrder(UserConstants.PASSWORD_LASTCHANGED, UserConstants.REQUIRES_PASSWORD_CHANGE));
    }

    @Test
    public void testAddUserDoesNotSetPasswordRelatedAttributesIfCredentialIsNotProvided() throws Exception
    {
        UserTemplate userAddedByDao = new UserTemplate(testUser);
        when(userDao.add(any(User.class), any(PasswordCredential.class))).thenReturn(userAddedByDao);

        internalDirectory.addUser(new UserTemplate(testUser), null);

        verify(userDao).storeAttributes(eq(userAddedByDao), attributesCaptor.capture());
        assertThat(attributesCaptor.getValue().keySet(),
                not(containsInAnyOrder(UserConstants.PASSWORD_LASTCHANGED, UserConstants.REQUIRES_PASSWORD_CHANGE)));
    }

    @Test (expected = InvalidUserException.class)
    public void testAddUserFailsIfUserHasExternalId() throws Exception
    {
        adminUser.setExternalId("new-external-id");

        internalDirectory.addUser(adminUser, unencryptedCredential);
    }

    @Test
    public void testAddPrincipalWithInvalidCredential() throws Exception
    {
        try
        {
            internalDirectory.addUser(adminUser, new PasswordCredential(""));
            fail("Should not be able to add a user with a blank credential");
        } catch (InvalidCredentialException e)
        {
            // We should be here
        }

        internalDirectory.setAttributes(Collections.singletonMap(InternalDirectory.ATTRIBUTE_PASSWORD_REGEX, "[a-zA-Z]"));

        try
        {
            internalDirectory.addUser(adminUser, new PasswordCredential("123"));
            fail("Should not be able to add a user with a numercial credential");
        } catch (InvalidCredentialException e)
        {
            // We should be here
        }
    }

    @Test(expected = NullPointerException.class)
    public void testAddPrincipalMustHaveName() throws Exception
    {
        when(userDao.add(null, unencryptedCredential)).thenThrow(new IllegalArgumentException());

        internalDirectory.addUser(null, unencryptedCredential);
    }

    @Test
    public void testAuthentication() throws Exception
    {
        internalUserWithAttributes = new InternalUserWithAttributes(new InternalUser(adminUser, directoryConfiguration, encryptedCredential), new HashMap<String, Set<String>>());
        when(userDao.findByNameWithAttributes(internalDirectory.getDirectoryId(), ADMIN_USERNAME)).thenReturn(internalUserWithAttributes);
        when(userDao.getCredential(internalDirectory.getDirectoryId(), ADMIN_USERNAME)).thenReturn(encryptedCredential);

        when(ldapShaPasswordEncoder.isPasswordValid(ENCRYPTED_PASSWORD_1, PASSWORD_1, null)).thenReturn(true);

        // Call authenticate
        User user = internalDirectory.authenticate(ADMIN_USERNAME, unencryptedCredential);

        // Assert stuff on the principal
        Map<String, Set<String>> attribs = new HashMap<String, Set<String>>();
        attribs.put(UserConstants.INVALID_PASSWORD_ATTEMPTS, Collections.singleton("0"));
        attribs.put(UserConstants.REQUIRES_PASSWORD_CHANGE, Collections.singleton("false"));

        verify(userDao, times(1)).storeAttributes(eq(internalUserWithAttributes), argThat(attributesMatch(attribs)));
    }

    @Test
    public void testAuthenticationWithInActiveUser() throws Exception
    {
        adminUser.setActive(false);
        internalUserWithAttributes = new InternalUserWithAttributes(new InternalUser(adminUser, directoryConfiguration, encryptedCredential), new HashMap<String, Set<String>>());
        when(userDao.findByNameWithAttributes(internalDirectory.getDirectoryId(), ADMIN_USERNAME)).thenReturn(internalUserWithAttributes);

        // Call authenticate
        try
        {
            internalDirectory.authenticate(ADMIN_USERNAME, unencryptedCredential);
            fail("This user account is inactive");
        } catch (InactiveAccountException e)
        {
            // Yay!
        }

        verify(userDao, times(1)).findByNameWithAttributes(internalDirectory.getDirectoryId(), ADMIN_USERNAME);
        verify(userDao, never()).storeAttributes(eq(adminUser), anyMap());
    }

    @Test
    public void testAuthenticationPrincipalRequiresPasswordChange() throws Exception
    {
        when(userDao.getCredential(internalDirectory.getDirectoryId(), ADMIN_USERNAME)).thenReturn(encryptedCredential);
        when(ldapShaPasswordEncoder.isPasswordValid(ENCRYPTED_PASSWORD_1, PASSWORD_1, null)).thenReturn(true);

        // Set the last changed time on the principal 01/01/2000
        internalUserWithAttributes = new InternalUserWithAttributes(new InternalUser(adminUser, directoryConfiguration, encryptedCredential), Collections.singletonMap(UserConstants.PASSWORD_LASTCHANGED, Collections.singleton(Long.toString(949375890937L))));

        when(userDao.findByNameWithAttributes(internalDirectory.getDirectoryId(), internalUserWithAttributes.getName())).thenReturn(internalUserWithAttributes);

        // Call authenticate
        try
        {
            internalDirectory.authenticate(internalUserWithAttributes.getName(), unencryptedCredential);
            fail("Authentication should have failed");
        } catch (ExpiredCredentialException e)
        {
            // validate the REQUIRES_PASSWORD_CHANGE value has been set to 'true'
            Map<String, Set<String>> userAttributes = Maps.newHashMap();
            userAttributes.put(UserConstants.INVALID_PASSWORD_ATTEMPTS, Collections.singleton("0"));
            userAttributes.put(UserConstants.REQUIRES_PASSWORD_CHANGE, Collections.singleton("true"));
            verify(userDao, times(1)).storeAttributes(eq(internalUserWithAttributes), argThat(attributesMatch(userAttributes)));
        }
    }

    @Test
    public void testAuthenticationWithBadPassword() throws Exception
    {
        internalUserWithAttributes = new InternalUserWithAttributes(new InternalUser(adminUser, directoryConfiguration, encryptedCredential), Collections.<String, Set<String>>emptyMap());

        when(userDao.findByNameWithAttributes(internalDirectory.getDirectoryId(), internalUserWithAttributes.getName())).thenReturn(internalUserWithAttributes);
        when(userDao.getCredential(internalDirectory.getDirectoryId(), ADMIN_USERNAME)).thenReturn(encryptedCredential);

        // Call authenticate
        try
        {
            internalDirectory.authenticate(internalUserWithAttributes.getName(), new PasswordCredential("badpassword"));
            fail("Authentication should have failed");

        } catch (InvalidAuthenticationException e)
        {
            // Success!
        }

        Map<String, Set<String>> userAttributes = Maps.newHashMap();
        userAttributes.put(UserConstants.INVALID_PASSWORD_ATTEMPTS, Collections.singleton("1"));

        verify(userDao, times(1)).storeAttributes(eq(internalUserWithAttributes), argThat(attributesMatch(userAttributes)));
    }

    @Test
    public void testAuthenticationWithMaxPasswordAttempts() throws Exception
    {
        // set up the principal with max password attempts
        internalDirectory.setAttributes(Collections.singletonMap(InternalDirectory.ATTRIBUTE_PASSWORD_MAX_ATTEMPTS, "1"));

        internalUserWithAttributes = new InternalUserWithAttributes(new InternalUser(adminUser, directoryConfiguration, encryptedCredential), Collections.<String, Set<String>>singletonMap(UserConstants.INVALID_PASSWORD_ATTEMPTS, Collections.singleton("1")));

        when(userDao.findByNameWithAttributes(internalDirectory.getDirectoryId(), internalUserWithAttributes.getName())).thenReturn(internalUserWithAttributes);

        // Call authenticate
        try
        {
            internalDirectory.authenticate(internalUserWithAttributes.getName(), unencryptedCredential);
            fail();
        } catch (InvalidAuthenticationException e)
        {
            // success!
            assertEquals("Maximum allowed invalid password attempts has been reached", e.getMessage());
        }
    }

    @Test(expected = UserNotFoundException.class)
    public void testAuthenticationWithUnknownPrincipal() throws Exception
    {
        // Remove the mock since we will not be using it here.
        when(userDao.findByNameWithAttributes(internalDirectory.getDirectoryId(), "unknown-user")).thenThrow(new UserNotFoundException("unknown-user"));

        internalDirectory.authenticate("unknown-user", unencryptedCredential);
    }

    @Test
    public void testUpdateUserCredentials() throws Exception
    {
        // we will try and find the user
        InternalUser internalUser = new InternalUser(adminUser, directoryConfiguration, encryptedCredential);

        when(userDao.findByName(internalDirectory.getDirectoryId(), internalUser.getName())).thenReturn(internalUser);

        internalDirectory.updateUserCredential(internalUser.getName(), new PasswordCredential("newpassword"));

        Map<String, Set<String>> userAttributes = new HashMap<String, Set<String>>();
        userAttributes.put(UserConstants.REQUIRES_PASSWORD_CHANGE, Collections.singleton(Boolean.FALSE.toString()));
        userAttributes.put(UserConstants.INVALID_PASSWORD_ATTEMPTS, Collections.singleton(Long.toString(0)));

        verify(userDao).storeAttributes(eq(internalUser), argThat(attributesMatch(userAttributes)));
    }

    @Test
    public void testUpdatePrincipalCredentialsWithOldPassword() throws Exception
    {
        when(ldapShaPasswordEncoder.isPasswordValid(ENCRYPTED_PASSWORD_1, PASSWORD_1, null)).thenReturn(true);

        InternalUser internalUser = new InternalUser(adminUser, directoryConfiguration, encryptedCredential);

        when(userDao.getCredentialHistory(internalDirectory.getDirectoryId(), internalUser.getName())).thenReturn(Arrays.asList(encryptedCredential));

        internalUser.getCredentialRecords().add(new InternalUserCredentialRecord(internalUser, encryptedCredential.getCredential()));

        when(userDao.findByName(internalDirectory.getDirectoryId(), internalUser.getName())).thenReturn(internalUser);

        try
        {
            internalDirectory.updateUserCredential(internalUser.getName(), new PasswordCredential(PASSWORD_1));
            fail("Should have thrown InvalidCredentialException since password is already used");
        } catch (InvalidCredentialException e)
        {
            // Success!
        }
    }

    @Test
    public void updateUserShouldPreserveExternalIdIfNewOneIsNull() throws Exception
    {
        TimestampedUser savedUser = mock(TimestampedUser.class);
        when(savedUser.getExternalId()).thenReturn("old-external-id");
        when(userDao.findByName(DIRECTORY_ID, TEST_USERNAME)).thenReturn(savedUser);

        User updatedUser = mock(User.class);
        when(updatedUser.getExternalId()).thenReturn("old-external-id");
        when(userDao.update(any(User.class))).thenReturn(updatedUser);

        UserTemplate userTemplate = new UserTemplate(TEST_USERNAME, DIRECTORY_ID);
        userTemplate.setExternalId(null);

        User user = internalDirectory.updateUser(userTemplate);

        assertEquals("old-external-id", user.getExternalId());

        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userDao).update(userArgumentCaptor.capture());
        assertEquals("old-external-id", userArgumentCaptor.getValue().getExternalId());
    }

    @Test (expected = InvalidUserException.class)
    public void updateUserShouldFailIfExternalIdIsChanged() throws Exception
    {
        TimestampedUser savedUser = mock(TimestampedUser.class);
        when(savedUser.getExternalId()).thenReturn("old-external-id");
        when(userDao.findByName(DIRECTORY_ID, TEST_USERNAME)).thenReturn(savedUser);

        UserTemplate userTemplate = new UserTemplate(TEST_USERNAME, DIRECTORY_ID);
        userTemplate.setExternalId("new-external-id");

        internalDirectory.updateUser(userTemplate);
    }

    @Test
    public void testRequiresExistingPasswordChangeFlagReturnsTrue()
    {
        internalUserWithAttributes = new InternalUserWithAttributes(new InternalUser(adminUser, directoryConfiguration, encryptedCredential), Collections.<String, Set<String>>singletonMap(UserConstants.REQUIRES_PASSWORD_CHANGE, Collections.singleton(Boolean.TRUE.toString())));

        // this should return true right away because the requires password change is set to true on the principal
        assertTrue(internalDirectory.requiresPasswordChange(internalUserWithAttributes));
    }

    @Test
    public void testRequiresExistingPasswordChangeFlagReturnsFalse()
    {
        internalUserWithAttributes = new InternalUserWithAttributes(new InternalUser(adminUser, directoryConfiguration, encryptedCredential), Collections.<String, Set<String>>singletonMap(UserConstants.REQUIRES_PASSWORD_CHANGE, Collections.singleton(Boolean.FALSE.toString())));

        // this should return true right away because the requires password change is set to true on the principal
        assertFalse(internalDirectory.requiresPasswordChange(internalUserWithAttributes));
    }

    @Test
    public void testCurrentUserInvalidPasswordAttemptsWithNullAttribute()
    {
        internalUserWithAttributes = new InternalUserWithAttributes(new InternalUser(adminUser, directoryConfiguration, encryptedCredential), Collections.<String, Set<String>>singletonMap(UserConstants.INVALID_PASSWORD_ATTEMPTS, Collections.singleton(String.class.cast(null))));

        long attempts = internalDirectory.currentPrincipalInvalidPasswordAttempts(internalUserWithAttributes);

        assertEquals("Method should return 0 if no attempts are found", 0L, attempts);

    }

    @Test
    public void testCurrentUserInvalidPasswordAttemptsWithEmptyAttributes()
    {
        internalUserWithAttributes = new InternalUserWithAttributes(new InternalUser(adminUser, directoryConfiguration, encryptedCredential), Collections.<String, Set<String>>emptyMap());

        long attempts = internalDirectory.currentPrincipalInvalidPasswordAttempts(internalUserWithAttributes);

        assertEquals("Method should return 0 if no attempts are found", 0L, attempts);
    }

    @Test
    public void testCurrentPrincipalInvalidPasswordAttemptsWithNullAttibuteList()
    {
        internalUserWithAttributes = new InternalUserWithAttributes(new InternalUser(adminUser, directoryConfiguration, encryptedCredential), Collections.<String, Set<String>>singletonMap(UserConstants.INVALID_PASSWORD_ATTEMPTS, Collections.<String>emptySet()));

        long attempts = internalDirectory.currentPrincipalInvalidPasswordAttempts(internalUserWithAttributes);

        assertEquals("Method should return 0 if no attempts are found", 0L, attempts);
    }

    @Test
    public void testCurrentPrincipalInvalidPasswordAttemptsWithCorrectAttribute()
    {
        internalUserWithAttributes = new InternalUserWithAttributes(new InternalUser(adminUser, directoryConfiguration, encryptedCredential), Collections.<String, Set<String>>singletonMap(UserConstants.INVALID_PASSWORD_ATTEMPTS, Collections.singleton("5")));

        long attempts = internalDirectory.currentPrincipalInvalidPasswordAttempts(internalUserWithAttributes);

        assertEquals("Method should return 5", 5L, attempts);
    }


    @Test
    public void testAddGroup() throws Exception
    {
        when(groupDao.add(mainGroup)).thenReturn(new InternalGroup(mainGroup, directoryConfiguration));

        final Group group = internalDirectory.addGroup(mainGroup);

        assertEquals(MAIN_GROUP, group.getName());

        // Verify that the store attributes method was also called.
        verify(groupDao, times(1)).add(mainGroup);
    }

    @Test
    public void testAddGroupToGroup() throws Exception
    {
        // mock the find
        when(groupDao.findByName(DIRECTORY_ID, MAIN_GROUP)).thenReturn(new InternalGroup(mainGroup, directoryConfiguration));
        when(groupDao.findByName(DIRECTORY_ID, CHILD_GROUP)).thenReturn(new InternalGroup(childGroup, directoryConfiguration));

        internalDirectory.addGroupToGroup(childGroup.getName(), mainGroup.getName());

        // Verify the add membership was called
        verify(membershipDao, times(1)).addGroupToGroup(DIRECTORY_ID, CHILD_GROUP, MAIN_GROUP);
    }

    @Test
    public void testAddGroupToGroupWrongGroupType() throws Exception
    {
        // Change childGroup to be of type LEGACY_ROLE
        childGroup.setType(GroupType.LEGACY_ROLE);

        // mock the find
        when(groupDao.findByName(DIRECTORY_ID, MAIN_GROUP)).thenReturn(new InternalGroup(mainGroup, directoryConfiguration));
        when(groupDao.findByName(DIRECTORY_ID, CHILD_GROUP)).thenReturn(new InternalGroup(childGroup, directoryConfiguration));

        try
        {
            internalDirectory.addGroupToGroup(mainGroup.getName(), childGroup.getName());
            fail("InvalidMembershipException expected");
        } catch (InvalidMembershipException e)
        {
            // Expected
        }

        // Verify the add membership was NOT called
        verify(membershipDao, never()).addGroupToGroup(DIRECTORY_ID, CHILD_GROUP, MAIN_GROUP);
    }


    @Test
    public void testRemoveUserFromGroup() throws Exception
    {
        // pretend user is already in group
        when(membershipDao.isUserDirectMember(DIRECTORY_ID, ADMIN_USERNAME, MAIN_GROUP)).thenReturn(true);
        // mock the find
        when(userDao.findByName(DIRECTORY_ID, ADMIN_USERNAME)).thenReturn(new InternalUser(adminUser, directoryConfiguration, encryptedCredential));
        when(groupDao.findByName(DIRECTORY_ID, MAIN_GROUP)).thenReturn(new InternalGroup(mainGroup, directoryConfiguration));

        internalDirectory.removeUserFromGroup(ADMIN_USERNAME, MAIN_GROUP);

        // removal of membership should be successful
        verify(membershipDao, times(1)).removeUserFromGroup(DIRECTORY_ID, ADMIN_USERNAME, MAIN_GROUP);
    }

    @Test
    public void testRemoveUserFromGroupNotMembers() throws Exception
    {
        // user is not in group
        when(membershipDao.isUserDirectMember(DIRECTORY_ID, ADMIN_USERNAME, MAIN_GROUP)).thenReturn(false);
        // mock the find
        when(userDao.findByName(DIRECTORY_ID, ADMIN_USERNAME)).thenReturn(new InternalUser(adminUser, directoryConfiguration, encryptedCredential));
        when(groupDao.findByName(DIRECTORY_ID, MAIN_GROUP)).thenReturn(new InternalGroup(mainGroup, directoryConfiguration));

        try
        {
            internalDirectory.removeUserFromGroup(ADMIN_USERNAME, MAIN_GROUP);
            fail("MembershipNotFoundException expected");
        } catch (MembershipNotFoundException e)
        {
            // expected
        }

        // not a member, so should not call removeUserFromGroup
        verify(membershipDao, never()).removeUserFromGroup(DIRECTORY_ID, ADMIN_USERNAME, MAIN_GROUP);
    }

    @Test
    public void testRemoveUserFromGroupNoUser() throws Exception
    {
        // pretend user is already in group
        when(membershipDao.isUserDirectMember(DIRECTORY_ID, ADMIN_USERNAME, MAIN_GROUP)).thenReturn(true);
        // mock the find
        when(userDao.findByName(DIRECTORY_ID, ADMIN_USERNAME)).thenThrow(new UserNotFoundException(ADMIN_USERNAME));
        when(groupDao.findByName(DIRECTORY_ID, MAIN_GROUP)).thenReturn(new InternalGroup(mainGroup, directoryConfiguration));

        try
        {
            internalDirectory.removeUserFromGroup(ADMIN_USERNAME, MAIN_GROUP);
            fail("UserNotFoundException expected");
        } catch (UserNotFoundException e)
        {
            // expected
        }

        // user does not exist,  so should not call removeUserFromGroup
        verify(membershipDao, never()).removeUserFromGroup(DIRECTORY_ID, ADMIN_USERNAME, MAIN_GROUP);
    }

    @Test
    public void testRemoveUserFromGroupNoGroup() throws Exception
    {
        // pretend user is already in group
        when(membershipDao.isUserDirectMember(DIRECTORY_ID, ADMIN_USERNAME, MAIN_GROUP)).thenReturn(true);
        // mock the find
        when(userDao.findByName(DIRECTORY_ID, ADMIN_USERNAME)).thenReturn(new InternalUser(adminUser, directoryConfiguration, encryptedCredential));
        when(groupDao.findByName(DIRECTORY_ID, MAIN_GROUP)).thenThrow(new GroupNotFoundException(MAIN_GROUP));

        try
        {
            internalDirectory.removeUserFromGroup(ADMIN_USERNAME, MAIN_GROUP);
            fail("GroupNotFoundException expected");
        } catch (GroupNotFoundException e)
        {
            // expected
        }

        // group does not exist, so should not call removeUserFromGroup
        verify(membershipDao, never()).removeUserFromGroup(DIRECTORY_ID, ADMIN_USERNAME, MAIN_GROUP);
    }

    @Test
    public void testRemoveGroupFromGroup() throws Exception
    {
        when(membershipDao.isGroupDirectMember(DIRECTORY_ID, CHILD_GROUP, MAIN_GROUP)).thenReturn(true);
        when(groupDao.findByName(DIRECTORY_ID, MAIN_GROUP)).thenReturn(new InternalGroup(mainGroup, directoryConfiguration));
        when(groupDao.findByName(DIRECTORY_ID, CHILD_GROUP)).thenReturn(new InternalGroup(childGroup, directoryConfiguration));

        internalDirectory.removeGroupFromGroup(CHILD_GROUP, MAIN_GROUP);

        // membership removal should be called
        verify(membershipDao, times(1)).removeGroupFromGroup(DIRECTORY_ID, CHILD_GROUP, MAIN_GROUP);
    }

    @Test
    public void testRemoveGroupFromGroupNotMembers() throws Exception
    {
        when(membershipDao.isGroupDirectMember(DIRECTORY_ID, CHILD_GROUP, MAIN_GROUP)).thenReturn(false);
        when(groupDao.findByName(DIRECTORY_ID, MAIN_GROUP)).thenReturn(new InternalGroup(mainGroup, directoryConfiguration));
        when(groupDao.findByName(DIRECTORY_ID, CHILD_GROUP)).thenReturn(new InternalGroup(childGroup, directoryConfiguration));

        try
        {
            internalDirectory.removeGroupFromGroup(CHILD_GROUP, MAIN_GROUP);
            fail("MembershipNotFoundException expected");
        } catch (MembershipNotFoundException e)
        {
            // expected
        }
        // membership removal should NOT be called
        verify(membershipDao, never()).removeGroupFromGroup(DIRECTORY_ID, CHILD_GROUP, MAIN_GROUP);
    }


    @Test
    public void testRemoveGroupFromGroupIncorrectGroupType() throws Exception
    {
        // Change child to GroupType.LEGACY_ROLE
        childGroup.setType(GroupType.LEGACY_ROLE);

        when(membershipDao.isGroupDirectMember(DIRECTORY_ID, CHILD_GROUP, MAIN_GROUP)).thenReturn(true);
        when(groupDao.findByName(DIRECTORY_ID, MAIN_GROUP)).thenReturn(new InternalGroup(mainGroup, directoryConfiguration));
        when(groupDao.findByName(DIRECTORY_ID, CHILD_GROUP)).thenReturn(new InternalGroup(childGroup, directoryConfiguration));

        try
        {
            internalDirectory.removeGroupFromGroup(CHILD_GROUP, MAIN_GROUP);
            fail("InvalidMembershipException expected");
        } catch (InvalidMembershipException e)
        {
            // expected
        }
        // membership removal should NOT be called
        verify(membershipDao, never()).removeGroupFromGroup(DIRECTORY_ID, CHILD_GROUP, MAIN_GROUP);
    }

    @Test
    public void testAddAllUsers()
    {
        adminUser.setExternalId("admin-external-id");  // this will cause this user to fail
        testUser.setExternalId(null); // an external id will be generated for this user

        Set<UserTemplateWithCredentialAndAttributes> usersToAdd = new HashSet<UserTemplateWithCredentialAndAttributes>();
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(adminUser, encryptedCredential));
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(testUser, encryptedCredential));

        when(userDao.addAll((Set<UserTemplateWithCredentialAndAttributes>) anyObject())).thenReturn(new BatchResult<User>(0));

        Collection<User> failedUsers = internalDirectory.addAllUsers(usersToAdd).getFailedEntities();

        // should add testUser, but not adminUser
        verify(userDao, times(1)).addAll(addedUsers.capture());
        assertThat(addedUsers.getValue(), contains(
                user(UserTemplateWithCredentialAndAttributes.class)
                        .withNameOf(TEST_USERNAME)
                        .withExternalIdMatching(not(isEmptyOrNullString()))));
        assertThat(failedUsers, contains(userNamed(ADMIN_USERNAME)));
    }

    @Test
    public void testAddAllUsersNull()
    {
        try
        {
            internalDirectory.addAllUsers(null);
            fail();
        }
        catch (NullPointerException e)
        {
            // expected
        }

        verify(userDao, never()).addAll((Set<UserTemplateWithCredentialAndAttributes>) anyObject());
    }

    @Test
    public void testAddAllUsersInvalidUser()
    {
        // make testUser invalid - have upper case in name
        testUser.setName("Test User");
        testUser.setDirectoryId(200L);

        Set<UserTemplateWithCredentialAndAttributes> usersToAdd = new HashSet<UserTemplateWithCredentialAndAttributes>();
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(adminUser, encryptedCredential));
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(testUser, encryptedCredential));

        when(userDao.addAll((Set<UserTemplateWithCredentialAndAttributes>) anyObject())).thenReturn(new BatchResult<User>(0));


        Collection<User> failedUsers = internalDirectory.addAllUsers(usersToAdd).getFailedEntities();

        // should add users
        verify(userDao, times(1)).addAll((Set<UserTemplateWithCredentialAndAttributes>) anyObject());
        assertEquals(1, failedUsers.size());
        assertTrue(failedUsers.contains(testUser));
    }

    @Test
    public void testAddAllUsersThrowsNullPointerExceptionWhenNullIsPresent()
    {
        Set<UserTemplateWithCredentialAndAttributes> usersToAdd = new HashSet<UserTemplateWithCredentialAndAttributes>();
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(adminUser, encryptedCredential));
        usersToAdd.add(null);

        when(userDao.addAll((Set<UserTemplateWithCredentialAndAttributes>) anyObject())).thenReturn(new BatchResult<User>(0));

        try
        {
            internalDirectory.addAllUsers(usersToAdd);
            fail();
        } catch (NullPointerException e)
        {
            // expected
        }

        verify(userDao, never()).addAll((Set<UserTemplateWithCredentialAndAttributes>) anyObject());
    }

    @Test
    public void testAddAllUsersInvalidCredential()
    {
        // set password regex so credentials for testUser is invalid
        internalDirectory.setAttributes(Collections.singletonMap(InternalDirectory.ATTRIBUTE_PASSWORD_REGEX, "abc"));

        Set<UserTemplateWithCredentialAndAttributes> usersToAdd = new HashSet<UserTemplateWithCredentialAndAttributes>();
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(adminUser, encryptedCredential));
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(testUser, encryptedCredential));

        when(userDao.addAll((Set<UserTemplateWithCredentialAndAttributes>) anyObject())).thenReturn(new BatchResult<User>(0));

        Collection<User> failedUsers = internalDirectory.addAllUsers(usersToAdd).getFailedEntities();

        // try to add all users - but none of them will match the regex "abc" so fail both!
        verify(userDao, times(1)).addAll((Set<UserTemplateWithCredentialAndAttributes>) anyObject());
        assertEquals(2, failedUsers.size());
        assertTrue(failedUsers.contains(testUser));
        assertTrue(failedUsers.contains(adminUser));
    }

    @Test
    public void testAddAllUsersUnencryptedCredentialShouldGetEncryptedBeforeBeingStored()
    {
        UserTemplateWithCredentialAndAttributes user = new UserTemplateWithCredentialAndAttributes(testUser, unencryptedCredential);

        BatchResult<User> userDaoResult = new BatchResult<User>(1);
        userDaoResult.addSuccess(user);
        when(userDao.addAll(Mockito.<Set<UserTemplateWithCredentialAndAttributes>>anyObject())).thenReturn(userDaoResult);

        BatchResult<User> addResult = internalDirectory.addAllUsers(ImmutableSet.of(user));

        verify(userDao).addAll(addedUsers.capture());
        UserTemplateWithCredentialAndAttributes userGivenToDaoDao = Iterables.getOnlyElement(addedUsers.getValue());
        assertEquals(user, userGivenToDaoDao);
        assertTrue(userGivenToDaoDao.getCredential().isEncryptedCredential());
        assertEquals(1, addResult.getTotalSuccessful());
        assertEquals(user, Iterables.getOnlyElement(addResult.getSuccessfulEntities()));
    }

    @Test
    public void testAddAllUsersAlwaysSetsPasswordRelatedAttributes()
    {
        when(userDao.addAll(Mockito.<Set<UserTemplateWithCredentialAndAttributes>>anyObject())).thenReturn(new BatchResult<User>(0));

        internalDirectory.addAllUsers(ImmutableSet.of(new UserTemplateWithCredentialAndAttributes(testUser, encryptedCredential)));

        verify(userDao).addAll(addedUsers.capture());
        assertThat(Iterables.getOnlyElement(addedUsers.getValue()).getAttributes().keySet(),
                containsInAnyOrder(UserConstants.PASSWORD_LASTCHANGED, UserConstants.REQUIRES_PASSWORD_CHANGE));
    }

    @Test
    public void testAddAllGroups() throws Exception
    {
        Set<GroupTemplate> groupsToAdd = new HashSet<GroupTemplate>();
        groupsToAdd.add(new GroupTemplate(mainGroup));
        groupsToAdd.add(new GroupTemplate(childGroup));

        when(groupDao.addAll((Set<? extends Group>) anyObject())).thenReturn(new BatchResult<Group>(0));

        Collection<Group> failedGroups = internalDirectory.addAllGroups(groupsToAdd).getFailedEntities();

        // should add groups
        verify(groupDao, times(1)).addAll((Set<GroupTemplate>) anyObject());
        assertEquals(0, failedGroups.size());
    }

    @Test
    public void testAddAllGroupsNull() throws Exception
    {
        try
        {
            internalDirectory.addAllGroups(null);
            fail();
        }
        catch (NullPointerException e)
        {
            // expected
        }

        verify(groupDao, never()).addAll((Set<GroupTemplate>) anyObject());
    }

    @Test
    public void testAddAllGroupsInvalidGroup() throws Exception
    {
        // make childGroup invalid - have upper case in name
        childGroup.setName("Child Group");
        childGroup.setDirectoryId(200L);

        Set<GroupTemplate> groupsToAdd = new HashSet<GroupTemplate>();
        groupsToAdd.add(new GroupTemplate(mainGroup));
        groupsToAdd.add(new GroupTemplate(childGroup));

        when(groupDao.addAll((Set<GroupTemplate>) anyObject())).thenReturn(new BatchResult<Group>(0));

        Collection<Group> failedGroups = internalDirectory.addAllGroups(groupsToAdd).getFailedEntities();

        // should add groups
        verify(groupDao, times(1)).addAll((Set<GroupTemplate>) anyObject());
        assertEquals(1, failedGroups.size());
        assertTrue(failedGroups.contains(childGroup));
    }

    @Test
    public void testAddAllGroupsIllegalArgument() throws Exception
    {
        Set<GroupTemplate> groupsToAdd = new HashSet<GroupTemplate>();
        groupsToAdd.add(new GroupTemplate(mainGroup));
        groupsToAdd.add(null);

        when(groupDao.addAll((Set<GroupTemplate>) anyObject())).thenReturn(new BatchResult<Group>(0));

        try
        {
            Collection<Group> failedGroups = internalDirectory.addAllGroups(groupsToAdd).getFailedEntities();
            fail();
        }
        catch (NullPointerException e)
        {
            // expected
        }

        verify(groupDao, never()).addAll((Set<GroupTemplate>) anyObject());
    }

    @Test
    public void testRenameUser() throws Exception
    {
        InternalUser internalUser = new InternalUser(testUser, directoryConfiguration, encryptedCredential);
        when(userDao.findByName(DIRECTORY_ID, TEST_USERNAME)).thenReturn(internalUser);
        when(userDao.findByName(DIRECTORY_ID, "newname")).thenThrow(new UserNotFoundException("newname"));

        internalDirectory.renameUser(TEST_USERNAME, "newname");

        verify(userDao, times(1)).rename(internalUser, "newname");
    }

    @Test
    public void testRenameUserInvalid() throws Exception
    {
        when(userDao.findByName(DIRECTORY_ID, TEST_USERNAME)).thenReturn(new InternalUser(testUser, directoryConfiguration, encryptedCredential));
        when(userDao.findByName(DIRECTORY_ID, ADMIN_USERNAME)).thenReturn(new InternalUser(adminUser, directoryConfiguration, encryptedCredential));

        try
        {
            internalDirectory.renameUser(TEST_USERNAME, ADMIN_USERNAME);
            fail();
        } catch (UserAlreadyExistsException e)
        {
            // expected
        }

        verify(userDao, never()).rename(testUser, ADMIN_USERNAME);
    }

    @Test
    public void testRenameUserWhenOnlyCaseChanged() throws Exception
    {
        InternalUser internalUser = new InternalUser(testUser, directoryConfiguration, encryptedCredential);
        when(userDao.findByName(DIRECTORY_ID, TEST_USERNAME)).thenReturn(internalUser);

        internalDirectory.renameUser(TEST_USERNAME, "TeST");

        verify(userDao).findByName(DIRECTORY_ID, TEST_USERNAME);
        verify(userDao, never()).findByName(DIRECTORY_ID, "TeST");
        verify(userDao).rename(internalUser, "TeST");
    }

    public void testForceRenameUser() throws Exception
    {
        InternalUser internalUser = new InternalUser(testUser, directoryConfiguration, encryptedCredential);
        when(userDao.findByName(DIRECTORY_ID, TEST_USERNAME)).thenReturn(internalUser);
        when(userDao.findByName(DIRECTORY_ID, "newname")).thenThrow(new UserNotFoundException("newname"));

        internalDirectory.forceRenameUser(internalUser, "newname");

        verify(userDao, times(1)).rename(internalUser, "newname");
    }

    @Test
    public void testForceRenameUserWithExisting() throws Exception
    {
        InternalUser internalUser1 = new InternalUser(testUser, directoryConfiguration, encryptedCredential);
        InternalUser internalUser2 = new InternalUser(adminUser, directoryConfiguration, encryptedCredential);
        when(userDao.findByName(DIRECTORY_ID, TEST_USERNAME)).thenReturn(internalUser1);
        when(userDao.findByName(DIRECTORY_ID, ADMIN_USERNAME)).thenReturn(internalUser2);
        when(userDao.findByName(DIRECTORY_ID, "admin#1")).thenThrow(new UserNotFoundException("admin#1"));

        internalDirectory.forceRenameUser(internalUser1, "admin");

        verify(userDao, times(1)).rename(internalUser2, "admin#1");
        verify(userDao, times(1)).rename(internalUser1, "admin");
    }

    @Test
    public void testForceRenameUserWithExisting2() throws Exception
    {
        InternalUser internalUser1 = new InternalUser(testUser, directoryConfiguration, encryptedCredential);
        InternalUser internalUser2 = new InternalUser(adminUser, directoryConfiguration, encryptedCredential);
        when(userDao.findByName(DIRECTORY_ID, TEST_USERNAME)).thenReturn(internalUser1);
        when(userDao.findByName(DIRECTORY_ID, ADMIN_USERNAME)).thenReturn(internalUser2);
        when(userDao.findByName(DIRECTORY_ID, "admin#1")).thenReturn(new InternalUser(testUser, directoryConfiguration, encryptedCredential));
        when(userDao.findByName(DIRECTORY_ID, "admin#2")).thenThrow(new UserNotFoundException("admin#2"));

        internalDirectory.forceRenameUser(internalUser1, "admin");

        verify(userDao, times(1)).rename(internalUser2, "admin#2");
        verify(userDao, times(1)).rename(internalUser1, "admin");
    }

    private CheckDefinedAttributes attributesMatch(Map<String, Set<String>> attributes)
    {
        return new CheckDefinedAttributes(attributes);
    }


    private static class CheckDefinedAttributes extends ArgumentMatcher<Map<String, Set<String>>>
    {
        private final Map<String, Set<String>> attributes;

        public CheckDefinedAttributes(final Map<String, Set<String>> attributes)
        {
            this.attributes = attributes;
        }

        public boolean matches(Object attributeMap)
        {
            return ((Map) attributeMap).entrySet().containsAll(attributes.entrySet());
        }
    }
}
