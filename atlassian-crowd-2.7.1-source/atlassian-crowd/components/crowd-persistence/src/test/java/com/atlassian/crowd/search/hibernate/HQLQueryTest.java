package com.atlassian.crowd.search.hibernate;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class HQLQueryTest
{
    @Test
    public void positionalVariablesUseNaturalIndexes() throws Exception
    {
        HQLQuery hqlQuery = new HQLQuery();

        assertEquals("?1", hqlQuery.addParameterPlaceholder("valueA").toString()); // JPA-style indexes start at 1
        assertEquals("?2", hqlQuery.addParameterPlaceholder("valueB").toString());
        assertEquals("?3", hqlQuery.addParameterPlaceholder("valueC").toString());

        assertThat(hqlQuery.getParameterValues(), Matchers.<Object>hasItems("valueA", "valueB", "valueC"));
    }
}
