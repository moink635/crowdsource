package com.atlassian.crowd.directory.loader;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.directory.InternalDirectoryForDelegation;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.event.api.EventPublisher;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.test.matchers.CrowdMatchers.directory;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DelegatedAuthenticationDirectoryInstanceLoaderImplTest
{
    @Mock private LDAPDirectoryInstanceLoader ldapDirectoryInstanceLoader;
    @Mock private InternalDirectoryInstanceLoader internalDirectoryInstanceLoader;
    @Mock private DirectoryDao directoryDao;
    @Mock private EventPublisher eventPublisher;

    private DelegatedAuthenticationDirectoryInstanceLoaderImpl loader;

    @Before
    public void setUp() throws Exception
    {
        loader = new DelegatedAuthenticationDirectoryInstanceLoaderImpl(ldapDirectoryInstanceLoader,
                                                                        internalDirectoryInstanceLoader,
                                                                        eventPublisher, directoryDao);
    }

    @Test
    public void supportForNestedGroupsIsDeterminedByLdapDirectory() throws DirectoryInstantiationException
    {
        Directory delegatedDirectoryTemplate = new DirectoryImpl("name", DirectoryType.DELEGATING, "impl.Class");
        delegatedDirectoryTemplate.getAttributes().put(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS, "ldap.Class");

        RemoteDirectory ldapDirectory = mock(RemoteDirectory.class);
        when(ldapDirectory.supportsNestedGroups()).thenReturn(true);

        when(ldapDirectoryInstanceLoader.getDirectory(any(Directory.class))).thenReturn(ldapDirectory);

        RemoteDirectory delegatedDirectory = loader.getDirectory(delegatedDirectoryTemplate);

        assertTrue("Delegated directory should have nested groups enabled", delegatedDirectory.supportsNestedGroups());
    }

    @Test
    public void testInternalDirectoryHasNestedGroupsEnabled()
    {
        Directory delegatedDirectoryTemplate = new DirectoryImpl("name", DirectoryType.DELEGATING, "impl.Class");

        RemoteDirectory ldapDirectory = mock(RemoteDirectory.class);
        when(ldapDirectory.supportsNestedGroups()).thenReturn(true);

        Directory internalDirectory = loader.getInternalVersionOfDirectory(delegatedDirectoryTemplate, ldapDirectory);

        assertTrue("Internal directory should have nested groups enabled",
                Boolean.valueOf(internalDirectory.getAttributes().get(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS)));
    }

    @Test
    public void testInternalDirectoryHasNestedGroupsDisabled()
    {
        Directory delegatedDirectoryTemplate = new DirectoryImpl("name", DirectoryType.DELEGATING, "impl.Class");

        RemoteDirectory ldapDirectory = mock(RemoteDirectory.class);
        when(ldapDirectory.supportsNestedGroups()).thenReturn(false);

        Directory internalDirectory = loader.getInternalVersionOfDirectory(delegatedDirectoryTemplate, ldapDirectory);

        assertFalse("Internal directory should have nested groups disabled",
                Boolean.valueOf(internalDirectory.getAttributes().get(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS)));
    }

    @Test
    public void shouldInstantiateInternalDelegatingDirectory() throws Exception
    {
        Directory delegatedDirectoryTemplate = new DirectoryImpl("name", DirectoryType.DELEGATING, "impl.Class");
        delegatedDirectoryTemplate.getAttributes().put(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS,
                                                       "ldap.Class");

        RemoteDirectory ldapDirectory = mock(RemoteDirectory.class);
        when(ldapDirectory.supportsNestedGroups()).thenReturn(true);

        when(ldapDirectoryInstanceLoader.getDirectory(any(Directory.class))).thenReturn(ldapDirectory);

        RemoteDirectory delegatedDirectory = loader.getDirectory(delegatedDirectoryTemplate);

        verify(internalDirectoryInstanceLoader).getDirectory(
                argThat(directory().withImplementationClassOf(InternalDirectoryForDelegation.class)));
    }
}
