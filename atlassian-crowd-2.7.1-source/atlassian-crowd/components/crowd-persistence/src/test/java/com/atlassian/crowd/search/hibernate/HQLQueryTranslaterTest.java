package com.atlassian.crowd.search.hibernate;


import java.util.Date;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.GroupQuery;
import com.atlassian.crowd.search.query.entity.UserQuery;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.PropertyImpl;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.search.query.entity.restriction.PropertyUtils;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.AliasTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.TokenTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

import org.hamcrest.Matchers;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.crowd.search.Entity.DIRECTORY;
import static com.atlassian.crowd.search.Entity.GROUP;
import static com.atlassian.crowd.search.Entity.USER;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class HQLQueryTranslaterTest
{
    private static final long DIRECTORY_ID = 1L;

    private HQLQueryTranslater translater;
    private HQLQuery hql;

    @Before
    public void setUp()
    {
        translater = new HQLQueryTranslater();
        hql = new HQLQuery();
    }

    @After
    public void tearDown()
    {
        translater = null;
        hql = null;
    }

    @Test
    public void testSearchApplicationByName()
    {
        final EntityQuery<Application> query = QueryBuilder.queryFor(Application.class, EntityDescriptor.application()).with(Restriction.on(DirectoryTermKeys.NAME).startingWith("app")).returningAtMost(10);

        final HQLQuery hql = translater.asHQL(query);

        assertEquals("SELECT application FROM ApplicationImpl application WHERE application.lowerName LIKE ?1 ORDER BY application.lowerName", hql.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem("app%"));
    }

    @Test
    public void testSearchAliasByName()
    {
        final EntityQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.alias()).with(Restriction.on(AliasTermKeys.ALIAS).startingWith("just")).returningAtMost(10);

        final HQLQuery hql = translater.asHQL(query);

        assertEquals("SELECT alias.name, alias.lowerName FROM Alias alias WHERE alias.lowerAlias LIKE ?1 ORDER BY alias.lowerName", hql.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem("just%"));
    }

    @Test
    public void testSearchDirectoryByName()
    {
        final EntityQuery<Directory> query = QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(Restriction.on(DirectoryTermKeys.NAME).startingWith("dir")).returningAtMost(10);

        final HQLQuery hql = translater.asHQL(query);

        assertEquals("SELECT directory FROM DirectoryImpl directory WHERE directory.lowerName LIKE ?1 ORDER BY directory.lowerName", hql.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem("dir%"));
    }

    @Test
    public void testSearchTokensByName()
    {
        final EntityQuery<Token> query = QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).with(Restriction.on(TokenTermKeys.NAME).startingWith("crowd")).returningAtMost(10);

        final HQLQuery hql = translater.asHQL(query);

        assertEquals("SELECT token FROM Token token WHERE token.name LIKE ?1 ORDER BY token.name", hql.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem("crowd%"));
    }

    @Test
    public void testSearchMembershipNames()
    {
        final MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(
            EntityDescriptor.user()).withName("admin").returningAtMost(10);

        final HQLQuery hqlQuery = translater.asHQL(DIRECTORY_ID, query);

        assertEquals("SELECT mem.parentName, mem.lowerParentName FROM InternalMembership mem WHERE mem.lowerChildName = ?1 AND mem.membershipType = ?2 AND mem.directory.id = ?3 AND mem.groupType = ?4 ORDER BY mem.lowerParentName", hqlQuery.toString());

        assertThat(hqlQuery.getParameterValues(),
                   Matchers.<Object>hasItems("admin", MembershipType.GROUP_USER, 1L, GroupType.GROUP));
    }

    @Test
    public void testSearchUserNames()
    {
        final EntityQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.user()).returningAtMost(10);

        final HQLQuery hqlQuery = translater.asHQL(DIRECTORY_ID, query);

        assertEquals("SELECT usr.name, usr.lowerName FROM InternalUser usr WHERE usr.directory.id = ?1 ORDER BY usr.lowerName", hqlQuery.toString());

        assertThat(hqlQuery.getParameterValues(), Matchers.<Object>hasItem(1L));
    }

    @Test
    public void testSearchGroupMembershipsForUser()
    {
        final MembershipQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(
            EntityDescriptor.user()).withName("admin").returningAtMost(10);

        final HQLQuery hqlQuery = translater.asHQL(DIRECTORY_ID, query);

        assertEquals("SELECT grp FROM InternalGroup grp, InternalMembership mem WHERE grp.id = mem.parentId AND mem.lowerChildName = ?1 AND mem.membershipType = ?2 AND mem.directory.id = ?3 AND mem.groupType = ?4 ORDER BY mem.lowerChildName", hqlQuery.toString());

        assertThat(hqlQuery.getParameterValues(),
                   Matchers.<Object>hasItems("admin", MembershipType.GROUP_USER, 1L, GroupType.GROUP));
    }

    @Test
    public void testSearchGroupMembershipsForGroup()
    {
        final MembershipQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(
            EntityDescriptor.group()).withName("admin").returningAtMost(10);

        final HQLQuery hqlQuery = translater.asHQL(DIRECTORY_ID, query);

        assertEquals("SELECT grp FROM InternalGroup grp, InternalMembership mem WHERE grp.id = mem.parentId AND mem.lowerChildName = ?1 AND mem.membershipType = ?2 AND mem.directory.id = ?3 AND mem.groupType = ?4 ORDER BY mem.lowerChildName", hqlQuery.toString());

        assertThat(hqlQuery.getParameterValues(),
                   Matchers.<Object>hasItems("admin", MembershipType.GROUP_GROUP, 1L, GroupType.GROUP));
    }

    @Test
    public void testSearchGroupUserMembers()
    {
        MembershipQuery<User> query = QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(
            EntityDescriptor.group()).withName("admin").returningAtMost(10);

        final HQLQuery hqlQuery = translater.asHQL(DIRECTORY_ID, query);

        assertEquals("SELECT usr FROM InternalUser usr, InternalMembership mem WHERE usr.id = mem.childId AND mem.lowerParentName = ?1 AND mem.membershipType = ?2 AND mem.directory.id = ?3 AND mem.groupType = ?4 ORDER BY mem.lowerChildName", hqlQuery.toString());

        assertThat(hqlQuery.getParameterValues(),
                   Matchers.<Object>hasItems("admin", MembershipType.GROUP_USER, 1L, GroupType.GROUP));
    }

    @Test
    public void testSearchGroupGroupMembers()
    {
        final MembershipQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(
            "admin").returningAtMost(10);

        final HQLQuery hqlQuery = translater.asHQL(DIRECTORY_ID, query);

        assertEquals(
            "SELECT grp FROM InternalGroup grp, InternalMembership mem WHERE grp.id = mem.childId AND mem.lowerParentName = ?1 AND mem.membershipType = ?2 AND mem.directory.id = ?3 AND mem.groupType = ?4 ORDER BY mem.lowerChildName",
            hqlQuery.toString());

        assertThat(hqlQuery.getParameterValues(),
                   Matchers.<Object>hasItems("admin", MembershipType.GROUP_GROUP, 1L, GroupType.GROUP));
    }

    @Test
    public void testNestedMultiTermRestrictionAsHQLForGroup()
    {
        PropertyRestriction<String> nameRestriction = new TermRestriction<String>(GroupTermKeys.NAME, MatchMode.CONTAINS, "g");
        PropertyRestriction<String> colorRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "red");
        PropertyRestriction<Boolean> activeRestriction = new TermRestriction<Boolean>(GroupTermKeys.ACTIVE, true);
        PropertyRestriction<String> starSign = new TermRestriction<String>(PropertyUtils.ofTypeString("starSign"), MatchMode.EXACTLY_MATCHES, "cancer");

        BooleanRestriction conjunction1 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, nameRestriction, colorRestriction);
        BooleanRestriction conjunction2 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, activeRestriction, starSign);
        BooleanRestriction disjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, conjunction1, conjunction2);

        HQLQuery query = translater.asHQL(DIRECTORY_ID, new GroupQuery<Group>(Group.class, GroupType.GROUP, disjunction, 0, 100));

        assertEquals("SELECT DISTINCT grp FROM InternalGroup grp, InternalGroupAttribute attr1, InternalGroupAttribute attr2 WHERE grp.directory.id = ?1 AND grp.type = ?2 AND  ( (grp.lowerName LIKE ?3 AND grp.id = attr1.group.id AND attr1.name = ?4 AND attr1.lowerValue = ?5)  OR  (grp.active = ?6 AND grp.id = attr2.group.id AND attr2.name = ?7 AND attr2.lowerValue = ?8) )  ORDER BY grp.lowerName", query.toString());

        assertThat(query.getParameterValues(),
                   Matchers.<Object>hasItems(1L, GroupType.GROUP, "%g%", "color", "red", true, "starSign", "cancer"));
    }

    @Test
    public void testNestedMultiTermRestrictionAsHQLForUser()
    {
        PropertyRestriction<String> emailRestriction = new TermRestriction<String>(UserTermKeys.EMAIL, MatchMode.CONTAINS, "bob@bob.com");
        PropertyRestriction<String> colorRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "red");
        PropertyRestriction<String> firstNameRestriction = new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "bob");
        PropertyRestriction<String> starSign = new TermRestriction<String>(PropertyUtils.ofTypeString("starSign"), MatchMode.EXACTLY_MATCHES, "cancer");

        BooleanRestriction conjunction1 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, emailRestriction, colorRestriction);
        BooleanRestriction conjunction2 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, firstNameRestriction, starSign);
        BooleanRestriction disjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, conjunction1, conjunction2);

        HQLQuery query = translater.asHQL(DIRECTORY_ID, new UserQuery<User>(User.class, disjunction, 0, 100));

        assertEquals("SELECT DISTINCT usr FROM InternalUser usr, InternalUserAttribute attr1, InternalUserAttribute attr2 WHERE usr.directory.id = ?1 AND  ( (usr.lowerEmailAddress LIKE ?2 AND usr.id = attr1.user.id AND attr1.name = ?3 AND attr1.lowerValue = ?4)  OR  (usr.lowerDisplayName LIKE ?5 AND usr.id = attr2.user.id AND attr2.name = ?6 AND attr2.lowerValue = ?7) )  ORDER BY usr.lowerName", query.toString());

        assertThat(query.getParameterValues(),
                   Matchers.<Object>hasItems(1L, "%bob@bob.com%", "color", "red", "bob%", "starSign", "cancer"));
    }

    @Test
    public void testMultiTermHybridConjunctiveRestrictionAsHQLForGroup()
    {
        PropertyRestriction<Boolean> activeRestriction = new TermRestriction<Boolean>(GroupTermKeys.ACTIVE, true);
        PropertyRestriction<String> nameRestriction = new TermRestriction<String>(GroupTermKeys.NAME, MatchMode.STARTS_WITH, "bob");
        PropertyRestriction<String> colorRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "red");
        PropertyRestriction<String> starSign = new TermRestriction<String>(PropertyUtils.ofTypeString("starSign"), MatchMode.EXACTLY_MATCHES, "cancer");
        BooleanRestriction multiRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, activeRestriction, nameRestriction, colorRestriction, starSign);

        HQLQuery query = translater.asHQL(DIRECTORY_ID, new GroupQuery<Group>(Group.class, GroupType.GROUP, multiRestriction, 0, 100));

        assertEquals("SELECT DISTINCT grp FROM InternalGroup grp, InternalGroupAttribute attr1, InternalGroupAttribute attr2 WHERE grp.directory.id = ?1 AND grp.type = ?2 AND  (grp.active = ?3 AND grp.lowerName LIKE ?4 AND grp.id = attr1.group.id AND attr1.name = ?5 AND attr1.lowerValue = ?6 AND grp.id = attr2.group.id AND attr2.name = ?7 AND attr2.lowerValue = ?8)  ORDER BY grp.lowerName", query.toString());

        assertThat(query.getParameterValues(),
                   Matchers.<Object>hasItems(1L, GroupType.GROUP, true, "bob%", "color", "red", "starSign", "cancer"));
    }

    @Test
    public void testMultiTermHybridDisjunctiveRestrictionAsHQLForGroup()
    {
        PropertyRestriction<Boolean> activeRestriction = new TermRestriction<Boolean>(GroupTermKeys.ACTIVE, true);
        PropertyRestriction<String> nameRestriction = new TermRestriction<String>(GroupTermKeys.NAME, MatchMode.STARTS_WITH, "bob");
        PropertyRestriction<String> colorRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "red");
        PropertyRestriction<String> starSign = new TermRestriction<String>(PropertyUtils.ofTypeString("starSign"), MatchMode.EXACTLY_MATCHES, "cancer");
        BooleanRestriction multiRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, activeRestriction, nameRestriction, colorRestriction, starSign);

        HQLQuery query = translater.asHQL(DIRECTORY_ID, new GroupQuery<Group>(Group.class, GroupType.GROUP, multiRestriction, 0, 100));

        assertEquals("SELECT DISTINCT grp FROM InternalGroup grp, InternalGroupAttribute attr1 WHERE grp.directory.id = ?1 AND grp.type = ?2 AND  (grp.active = ?3 OR grp.lowerName LIKE ?4 OR grp.id = attr1.group.id AND attr1.name = ?5 AND attr1.lowerValue = ?6 OR grp.id = attr1.group.id AND attr1.name = ?7 AND attr1.lowerValue = ?8)  ORDER BY grp.lowerName", query.toString());

        assertThat(query.getParameterValues(),
                   Matchers.<Object>hasItems(1L, GroupType.GROUP, true, "bob%", "color", "red", "starSign", "cancer"));
    }

    @Test
    public void testMultiTermHybridConjunctiveRestrictionAsHQLForUser()
    {
        PropertyRestriction<String> emailRestriction = new TermRestriction<String>(UserTermKeys.EMAIL, MatchMode.CONTAINS, "bob@bob.com");
        PropertyRestriction<String> firstNameRestriction = new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "bob");
        PropertyRestriction<String> colorRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "red");
        PropertyRestriction<String> starSign = new TermRestriction<String>(PropertyUtils.ofTypeString("starSign"), MatchMode.EXACTLY_MATCHES, "cancer");
        BooleanRestriction multiRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, emailRestriction, firstNameRestriction, colorRestriction, starSign);

        HQLQuery query = translater.asHQL(DIRECTORY_ID, new UserQuery<User>(User.class, multiRestriction, 0, 100));

        assertEquals("SELECT DISTINCT usr FROM InternalUser usr, InternalUserAttribute attr1, InternalUserAttribute attr2 WHERE usr.directory.id = ?1 AND  (usr.lowerEmailAddress LIKE ?2 AND usr.lowerDisplayName LIKE ?3 AND usr.id = attr1.user.id AND attr1.name = ?4 AND attr1.lowerValue = ?5 AND usr.id = attr2.user.id AND attr2.name = ?6 AND attr2.lowerValue = ?7)  ORDER BY usr.lowerName", query.toString());

        assertThat(query.getParameterValues(),
                   Matchers.<Object>hasItems(1L, "%bob@bob.com%", "bob%", "color", "red", "starSign", "cancer"));
    }

    @Test
    public void testMultiTermHybridDisjunctiveRestrictionAsHQLForUser()
    {
        PropertyRestriction<String> emailRestriction = new TermRestriction<String>(UserTermKeys.EMAIL, MatchMode.CONTAINS, "bob@bob.com");
        PropertyRestriction<String> firstNameRestriction = new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "bob");
        PropertyRestriction<String> colorRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "red");
        PropertyRestriction<String> starSign = new TermRestriction<String>(PropertyUtils.ofTypeString("starSign"), MatchMode.EXACTLY_MATCHES, "cancer");
        BooleanRestriction multiRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, emailRestriction, firstNameRestriction, colorRestriction, starSign);

        HQLQuery query = translater.asHQL(DIRECTORY_ID, new UserQuery<User>(User.class, multiRestriction, 0, 100));

        assertEquals("SELECT DISTINCT usr FROM InternalUser usr, InternalUserAttribute attr1 WHERE usr.directory.id = ?1 AND  (usr.lowerEmailAddress LIKE ?2 OR usr.lowerDisplayName LIKE ?3 OR usr.id = attr1.user.id AND attr1.name = ?4 AND attr1.lowerValue = ?5 OR usr.id = attr1.user.id AND attr1.name = ?6 AND attr1.lowerValue = ?7)  ORDER BY usr.lowerName", query.toString());

        assertThat(query.getParameterValues(),
                   Matchers.<Object>hasItems(1L, "%bob@bob.com%", "bob%", "color", "red", "starSign", "cancer"));
    }

    @Test
    public void shouldNotShareAttributeAliasForConjunctiveRestriction()
    {
        HQLQuery hqlQuery = mock(HQLQuery.class);
        PropertyRestriction<String> colorRestriction =
            new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "red");
        BooleanRestriction booleanRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND,
                                                                           colorRestriction);

        assertThat(HQLQueryTranslater.getAttributeSharedAlias(hqlQuery, Entity.USER, booleanRestriction),
                   nullValue(String.class));

        verifyZeroInteractions(hqlQuery);
    }

    @Test
    public void shouldNotShareAttributeAliasIfThereIsNoSecondaryPropertyRestriction()
    {
        HQLQuery hqlQuery = mock(HQLQuery.class);
        PropertyRestriction<String> emailRestriction =
            new TermRestriction<String>(UserTermKeys.EMAIL, MatchMode.CONTAINS, "bob@bob.com");
        BooleanRestriction booleanRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR,
                                                                           emailRestriction);

        assertThat(HQLQueryTranslater.getAttributeSharedAlias(hqlQuery, Entity.USER, booleanRestriction),
                   nullValue(String.class));

        verifyZeroInteractions(hqlQuery);
    }

    @Test
    public void shouldShareAttributeAliasForDisjunctiveRestrictionOnUserSecondaryPropertyRestrictions()
    {
        HQLQuery hqlQuery = mock(HQLQuery.class);
        PropertyRestriction<String> emailRestriction =
            new TermRestriction<String>(UserTermKeys.EMAIL, MatchMode.CONTAINS, "bob@bob.com");
        PropertyRestriction<String> firstNameRestriction =
            new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "bob");
        PropertyRestriction<String> colorRestriction =
            new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "red");
        PropertyRestriction<String> starSign =
            new TermRestriction<String>(PropertyUtils.ofTypeString("starSign"), MatchMode.EXACTLY_MATCHES, "cancer");
        BooleanRestriction booleanRestriction =
            new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR,
                                       emailRestriction, firstNameRestriction, colorRestriction, starSign);

        when(hqlQuery.getNextAlias(anyString())).thenReturn("attr1");
        when(hqlQuery.appendFrom(anyString())).thenReturn(new StringBuilder());

        assertThat(HQLQueryTranslater.getAttributeSharedAlias(hqlQuery, Entity.USER, booleanRestriction),
                   is("attr1"));

        verify(hqlQuery).getNextAlias("attr");
        verify(hqlQuery).appendFrom(", InternalUserAttribute ");
        verifyNoMoreInteractions(hqlQuery);
    }

    @Test
    public void shouldShareAttributeAliasForDisjunctiveRestrictionOnGroupSecondaryPropertyRestrictions()
    {
        HQLQuery hqlQuery = mock(HQLQuery.class);
        PropertyRestriction<Boolean> activeRestriction =
            new TermRestriction<Boolean>(GroupTermKeys.ACTIVE, true);
        PropertyRestriction<String> nameRestriction =
            new TermRestriction<String>(GroupTermKeys.NAME, MatchMode.STARTS_WITH, "bob");
        PropertyRestriction<String> colorRestriction =
            new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "red");
        PropertyRestriction<String> starSign =
            new TermRestriction<String>(PropertyUtils.ofTypeString("starSign"), MatchMode.EXACTLY_MATCHES, "cancer");
        BooleanRestriction booleanRestriction =
            new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR,
                                       activeRestriction, nameRestriction, colorRestriction, starSign);

        when(hqlQuery.getNextAlias(anyString())).thenReturn("attr1");
        when(hqlQuery.appendFrom(anyString())).thenReturn(new StringBuilder());

        assertThat(HQLQueryTranslater.getAttributeSharedAlias(hqlQuery, Entity.GROUP, booleanRestriction),
                   is("attr1"));

        verify(hqlQuery).getNextAlias("attr");
        verify(hqlQuery).appendFrom(", InternalGroupAttribute ");
        verifyNoMoreInteractions(hqlQuery);
    }

    @Test
    public void isSecondaryUserPropertyRestriction()
    {
        TermRestriction<String> searchRestriction1 =
            new TermRestriction<String>(UserTermKeys.EMAIL, MatchMode.CONTAINS, "@example.test");
        TermRestriction<String> searchRestriction2 =
            new TermRestriction<String>(new PropertyImpl<String>("custom-attribute", String.class),
                                        MatchMode.CONTAINS, "value");

        assertFalse(HQLQueryTranslater.isSecondaryPropertyRestriction(UserTermKeys.ALL_USER_PROPERTIES)
                        .apply(searchRestriction1));
        assertTrue(HQLQueryTranslater.isSecondaryPropertyRestriction(UserTermKeys.ALL_USER_PROPERTIES)
                       .apply(searchRestriction2));
        assertFalse(HQLQueryTranslater.isSecondaryPropertyRestriction(UserTermKeys.ALL_USER_PROPERTIES)
                        .apply(NullRestrictionImpl.INSTANCE));
    }

    @Test
    public void isSecondaryGroupPropertyRestriction()
    {
        TermRestriction<String> searchRestriction1 =
            new TermRestriction<String>(GroupTermKeys.NAME, MatchMode.CONTAINS, "group-name");
        TermRestriction<String> searchRestriction2 =
            new TermRestriction<String>(new PropertyImpl<String>("custom-attribute", String.class),
                                        MatchMode.CONTAINS, "value");

        assertFalse(HQLQueryTranslater.isSecondaryPropertyRestriction(GroupTermKeys.ALL_GROUP_PROPERTIES)
                        .apply(searchRestriction1));
        assertTrue(HQLQueryTranslater.isSecondaryPropertyRestriction(GroupTermKeys.ALL_GROUP_PROPERTIES)
                       .apply(searchRestriction2));
        assertFalse(HQLQueryTranslater.isSecondaryPropertyRestriction(GroupTermKeys.ALL_GROUP_PROPERTIES)
                        .apply(NullRestrictionImpl.INSTANCE));
    }

    @Test
    public void testAppendTermRestrictionWithCustomAttribute()
    {
        translater.appendStringTermRestrictionAsHQL(hql, USER, new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.CONTAINS, "red"), null);

        assertEquals(" WHERE usr.id = attr1.user.id AND attr1.name = ?1 AND attr1.lowerValue LIKE ?2", hql.where.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItems("color", "%red%"));
    }

    @Test
    public void testAppendTermRestrictionWithNullMatchMode()
    {
        translater.appendIsNullTermRestrictionAsHSQL(hql, USER, new TermRestriction<String>(UserTermKeys.EMAIL, MatchMode.NULL, null), null);

        assertEquals(" WHERE usr.lowerEmailAddress IS NULL", hql.where.toString());

        assertThat(hql.getParameterValues(), IsEmptyCollection.empty());
    }

    @Test
    public void testAppendTermRestrictionWithNullMatchModeAndCustomAttribute()
    {
        translater.appendIsNullTermRestrictionAsHSQL(hql, USER, new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.NULL, null), null);

        assertEquals(" WHERE ?1 NOT IN (SELECT attr1.name FROM InternalUserAttribute attr1 WHERE usr.id = attr1.user.id) AND ?2 IS NULL", hql.where.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItems("color", null));
    }

    @Test
    public void testAppendTermRestrictionWithExactlyMatchesMatchMode()
    {
        translater.appendStringTermRestrictionAsHQL(hql, USER, new TermRestriction<String>(UserTermKeys.FIRST_NAME, MatchMode.EXACTLY_MATCHES, "bob"), null);

        assertEquals(" WHERE usr.lowerFirstName = ?1", hql.where.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem("bob"));
    }

    @Test
    public void testAppendTermRestrictionWithStartsWithMatchMode()
    {
        translater.appendStringTermRestrictionAsHQL(hql, USER, new TermRestriction<String>(UserTermKeys.LAST_NAME, MatchMode.STARTS_WITH, "bob"), null);

        assertEquals(" WHERE usr.lowerLastName LIKE ?1", hql.where.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem("bob%"));
    }

    @Test
    public void testAppendTermRestrictionWithContainsMatchMode()
    {
        translater.appendStringTermRestrictionAsHQL(hql,
                                                    USER,
                                                    new TermRestriction<String>(UserTermKeys.EMAIL,
                                                                                MatchMode.CONTAINS,
                                                                                "bob"),
                                                    null);

        assertEquals(" WHERE usr.lowerEmailAddress LIKE ?1", hql.where.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem("%bob%"));
    }

    @Test
    public void testAppendTermRestrictionBoolean()
    {
        translater.appendBooleanTermRestrictionAsHQL(hql,
                                                     USER,
                                                     new TermRestriction<Boolean>(UserTermKeys.ACTIVE, true),
                                                     null);

        assertEquals(" WHERE usr.active = ?1", hql.where.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem(true));
    }

    @Test
    public void testAppendEnumTermRestriction()
    {
        translater.appendEnumTermRestrictionAsHQL(hql,
                                                  DIRECTORY,
                                                  new TermRestriction<Enum>(DirectoryTermKeys.TYPE,
                                                                            DirectoryType.CONNECTOR),
                                                  null);

        assertEquals(" WHERE directory.type = ?1", hql.where.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem(DirectoryType.CONNECTOR));
    }

    @Test
    public void testAppendDateTermRestrictionWithGreaterThan()
    {
        Date date = new Date();

        translater.appendDateTermRestriction(hql,
                                             USER,
                                             new TermRestriction<Date>(UserTermKeys.CREATED_DATE,
                                                                       MatchMode.GREATER_THAN,
                                                                       date),
                                             null);

        assertEquals(" WHERE usr.createdDate > ?1", hql.where.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem(date));
    }

    @Test
    public void testAppendDateTermRestrictionWithLessThan()
    {
        Date date = new Date();

        translater.appendDateTermRestriction(hql,
                                             USER,
                                             new TermRestriction<Date>(UserTermKeys.CREATED_DATE,
                                                                       MatchMode.LESS_THAN,
                                                                       date),
                                             null);

        assertEquals(" WHERE usr.createdDate < ?1", hql.where.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem(date));
    }

    @Test
    public void testAppendGroupNameRestriction()
    {
        translater.appendStringTermRestrictionAsHQL(hql, GROUP,
                                                    new TermRestriction<String>(GroupTermKeys.NAME, "MiXeD"),
                                                    null);

        assertEquals(" WHERE grp.lowerName = ?1", hql.where.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem("mixed"));   // lowercased
    }

    @Test
    public void testAppendGroupDescriptionRestriction()
    {
        translater.appendStringTermRestrictionAsHQL(hql, GROUP,
                                                    new TermRestriction<String>(GroupTermKeys.DESCRIPTION, "MiXeD"),
                                                    null);

        assertEquals(" WHERE grp.description = ?1", hql.where.toString());

        assertThat(hql.getParameterValues(), Matchers.<Object>hasItem("MiXeD"));   // not lowercased
    }

    @Test
    public void propertiesThatHaveALowerCaseField()
    {
        assertTrue(HQLQueryTranslater.isLowerCaseProperty(UserTermKeys.USERNAME));
        assertTrue(HQLQueryTranslater.isLowerCaseProperty(UserTermKeys.DISPLAY_NAME));
        assertTrue(HQLQueryTranslater.isLowerCaseProperty(UserTermKeys.FIRST_NAME));
        assertTrue(HQLQueryTranslater.isLowerCaseProperty(UserTermKeys.LAST_NAME));
        assertTrue(HQLQueryTranslater.isLowerCaseProperty(UserTermKeys.EMAIL));

        assertTrue(HQLQueryTranslater.isLowerCaseProperty(GroupTermKeys.NAME));

        assertTrue(HQLQueryTranslater.isLowerCaseProperty(AliasTermKeys.ALIAS));

        // arbitrary attributes
        assertTrue(HQLQueryTranslater.isLowerCaseProperty(new PropertyImpl<String>("attributeName", String.class)));
    }

    @Test
    public void propertiesThatDoNotHaveALowerCaseField()
    {
        assertFalse(HQLQueryTranslater.isLowerCaseProperty(GroupTermKeys.DESCRIPTION));
    }

    @Test
    public void testSearchGroupsOfTypeGroup()
    {
        EntityQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP)).with(Restriction.on(GroupTermKeys.NAME).startingWith("g")).returningAtMost(100);

        HQLQuery hql = translater.asHQL(DIRECTORY_ID, query);

        assertEquals("SELECT grp FROM InternalGroup grp WHERE grp.directory.id = ?1 AND grp.type = ?2 AND grp.lowerName LIKE ?3 ORDER BY grp.lowerName", hql.toString());

        assertThat(hql.getParameterValues(),
                   Matchers.<Object>hasItems(1L, GroupType.GROUP, "g%"));
    }

    @Test
    public void testSearchGroupsOfTypeRole()
    {
        EntityQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.LEGACY_ROLE)).with(Restriction.on(GroupTermKeys.NAME).startingWith("g")).returningAtMost(100);

        HQLQuery hql = translater.asHQL(DIRECTORY_ID, query);

        assertEquals(
            "SELECT grp FROM InternalGroup grp WHERE grp.directory.id = ?1 AND grp.type = ?2 AND grp.lowerName LIKE ?3 ORDER BY grp.lowerName",
            hql.toString());

        assertThat(hql.getParameterValues(),
                   Matchers.<Object>hasItems(1L, GroupType.LEGACY_ROLE, "g%"));
    }

    @Test
    public void testSearchMembershipsOfAnyType()
    {
        MembershipQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("bob").returningAtMost(100);

        HQLQuery hql = translater.asHQL(DIRECTORY_ID, query);

        assertEquals(
            "SELECT grp FROM InternalGroup grp, InternalMembership mem WHERE grp.id = mem.parentId AND mem.lowerChildName = ?1 AND mem.membershipType = ?2 AND mem.directory.id = ?3 AND mem.groupType = ?4 ORDER BY mem.lowerChildName",
            hql.toString());

        assertThat(hql.getParameterValues(),
                   Matchers.<Object>hasItems("bob", MembershipType.GROUP_USER, 1L, GroupType.GROUP));
    }

    @Test
    public void testSearchMembershipsOfTypeRole()
    {
        MembershipQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.LEGACY_ROLE)).parentsOf(EntityDescriptor.group(GroupType.LEGACY_ROLE)).withName("bob").returningAtMost(100);

        HQLQuery hql = translater.asHQL(DIRECTORY_ID, query);

        assertEquals(
            "SELECT grp FROM InternalGroup grp, InternalMembership mem WHERE grp.id = mem.parentId AND mem.lowerChildName = ?1 AND mem.membershipType = ?2 AND mem.directory.id = ?3 AND mem.groupType = ?4 ORDER BY mem.lowerChildName",
            hql.toString());

        assertThat(hql.getParameterValues(),
                   Matchers.<Object>hasItems("bob", MembershipType.GROUP_GROUP, 1L, GroupType.LEGACY_ROLE));
    }

    @Test
    public void testSearchMembersOfTypeGroup()
    {
        MembershipQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP)).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName("bob").returningAtMost(100);

        HQLQuery hql = translater.asHQL(DIRECTORY_ID, query);

        assertEquals("SELECT grp FROM InternalGroup grp, InternalMembership mem WHERE grp.id = mem.childId AND mem.lowerParentName = ?1 AND mem.membershipType = ?2 AND mem.directory.id = ?3 AND mem.groupType = ?4 ORDER BY mem.lowerChildName", hql.toString());

        assertThat(hql.getParameterValues(),
                   Matchers.<Object>hasItems("bob", MembershipType.GROUP_GROUP, 1L, GroupType.GROUP));
    }

    @Test
    public void testSearchMembershipsOfTypeRoleAsNames()
    {
        final MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.LEGACY_ROLE)).parentsOf(EntityDescriptor.group(GroupType.LEGACY_ROLE)).withName("bob").returningAtMost(100);

        HQLQuery hql = translater.asHQL(DIRECTORY_ID, query);

        assertEquals(
            "SELECT mem.parentName, mem.lowerParentName FROM InternalMembership mem WHERE mem.lowerChildName = ?1 AND mem.membershipType = ?2 AND mem.directory.id = ?3 AND mem.groupType = ?4 ORDER BY mem.lowerParentName",
            hql.toString());

        assertThat(hql.getParameterValues(),
                   Matchers.<Object>hasItems("bob", MembershipType.GROUP_GROUP, 1L, GroupType.LEGACY_ROLE));
    }

    @Test
    public void testSearchMembersOfTypeGroupAsNames()
    {
        final MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP)).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName("bob").returningAtMost(100);

        HQLQuery hql = translater.asHQL(DIRECTORY_ID, query);

        assertEquals(
            "SELECT mem.childName, mem.lowerChildName FROM InternalMembership mem WHERE mem.lowerParentName = ?1 AND mem.membershipType = ?2 AND mem.directory.id = ?3 AND mem.groupType = ?4 ORDER BY mem.lowerChildName",
            hql.toString());

        assertThat(hql.getParameterValues(),
                   Matchers.<Object>hasItems("bob", MembershipType.GROUP_GROUP, 1L, GroupType.GROUP));
    }

    @Test
    public void localRestrictionBecomesQueryAgainstColumn()
    {
        final QueryBuilder.PartialEntityQuery<InternalGroup> partialQuery = QueryBuilder.queryFor(InternalGroup.class, EntityDescriptor.group());

        EntityQuery<InternalGroup> query;

        query = partialQuery.with(Restriction.on(GroupTermKeys.LOCAL).exactlyMatching(true)).returningAtMost(EntityQuery.ALL_RESULTS);

        HQLQuery hql = translater.asHQL(DIRECTORY_ID, query);

        assertEquals("SELECT grp FROM InternalGroup grp WHERE grp.directory.id = ?1 AND grp.type = ?2 AND grp.local = ?3 ORDER BY grp.lowerName",
                hql.toString());

        assertThat(hql.getParameterValues(),
                   Matchers.<Object>hasItems(DIRECTORY_ID, GroupType.GROUP, true));
    }
}
