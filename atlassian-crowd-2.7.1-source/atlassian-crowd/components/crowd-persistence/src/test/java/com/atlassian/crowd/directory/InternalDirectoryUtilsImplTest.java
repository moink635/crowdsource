package com.atlassian.crowd.directory;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.util.PasswordHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class InternalDirectoryUtilsImplTest
{
    private static final String EMPTY_REGEX = "";
    private static final String COMPLEXITY_MESSAGE = "Password should contain a digit";

    private InternalDirectoryUtilsImpl internalDirectoryUtils;

    @Mock private PasswordHelper passwordHelper;

    @Before
    public void createObjectUnderTest() throws Exception
    {
        internalDirectoryUtils = new InternalDirectoryUtilsImpl(passwordHelper);
    }

    @Test
    public void credentialIsValidWithoutRegex() throws Exception
    {
        internalDirectoryUtils.validateCredential(new PasswordCredential("password"), EMPTY_REGEX, COMPLEXITY_MESSAGE);
    }

    @Test (expected = NullPointerException.class)
    public void nullCredentialIsInvalid() throws Exception
    {
        internalDirectoryUtils.validateCredential(null, EMPTY_REGEX, COMPLEXITY_MESSAGE);
    }

    @Test (expected = InvalidCredentialException.class)
    public void blankCredentialIsInvalid() throws Exception
    {
        internalDirectoryUtils.validateCredential(new PasswordCredential("   "), EMPTY_REGEX, COMPLEXITY_MESSAGE);
    }

    @Test
    public void credentialMatchesRegex() throws Exception
    {
        PasswordCredential credential = new PasswordCredential("passw0rd");

        when(passwordHelper.validateRegex("[0-9]", credential)).thenReturn(true);

        internalDirectoryUtils.validateCredential(credential, "[0-9]", COMPLEXITY_MESSAGE);
    }

    @Test
    public void credentialDoesNotMatchRegexWithComplexityMessage() throws Exception
    {
        PasswordCredential credential = new PasswordCredential("password");

        when(passwordHelper.validateRegex("[0-9]", credential)).thenReturn(false);

        try
        {
            internalDirectoryUtils.validateCredential(credential, "[0-9]", COMPLEXITY_MESSAGE);
            fail("Credential should not be valid");
        }
        catch (InvalidCredentialException e)
        {
            assertEquals("The new password does not meet the directory complexity requirements: " + COMPLEXITY_MESSAGE,
                    e.getMessage());
            assertEquals(COMPLEXITY_MESSAGE, e.getPolicyDescription());
        }
    }

    @Test
    public void credentialDoesNotMatchRegexWithoutComplexityMessage() throws Exception
    {
        PasswordCredential credential = new PasswordCredential("password");

        when(passwordHelper.validateRegex("[0-9]", credential)).thenReturn(false);

        try
        {
            internalDirectoryUtils.validateCredential(credential, "[0-9]", "  ");
            fail("Credential should not be valid");
        }
        catch (InvalidCredentialException e)
        {
            assertEquals("The new password does not meet the directory complexity requirements", e.getMessage());
            assertNull(e.getPolicyDescription());
        }
    }

    @Test(expected = InvalidCredentialException.class)
    public void encryptedPasswordCannotBeCheckedForValidity() throws InvalidCredentialException
    {
        PasswordCredential credential = new PasswordCredential("hashed", true);

        internalDirectoryUtils.validateCredential(credential, ".*", COMPLEXITY_MESSAGE);
    }

    @Test
    public void encryptedPasswordsAreNotCheckedForEmptiness() throws InvalidCredentialException
    {
        PasswordCredential credential = new PasswordCredential("", true);

        internalDirectoryUtils.validateCredential(credential, "", null);
    }
}
