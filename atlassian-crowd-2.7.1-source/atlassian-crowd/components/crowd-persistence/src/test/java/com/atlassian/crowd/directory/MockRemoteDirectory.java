package com.atlassian.crowd.directory;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.InvalidMembershipException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.ReadOnlyGroupException;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MockRemoteDirectory implements RemoteDirectory
{
    private Map<String, String> attributeMap = new HashMap<String, String>();
    private Map<String, User> userByNameMap = new HashMap<String, User>();
    private Map<String, User> userByExternalIdMap = new HashMap<String, User>();

    @Override
    public long getDirectoryId()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void setDirectoryId(long directoryId)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getDescriptiveName()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void setAttributes(Map<String, String> attributes)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void setAttribute(String key, String value)
    {
        attributeMap.put(key, value);
    }

    @Override
    public User findUserByName(String name) throws UserNotFoundException
    {
        final User user = userByNameMap.get(name.toLowerCase());
        if (user == null)
        {
            throw new UserNotFoundException(name);
        }
        return user;
    }

    @Override
    public UserWithAttributes findUserWithAttributesByName(String name) throws UserNotFoundException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public User findUserByExternalId(String externalId) throws UserNotFoundException
    {
        final User user = userByExternalIdMap.get(externalId);
        if (user == null)
        {
            throw new UserNotFoundException(externalId);
        }
        return user;
    }

    @Override
    public User authenticate(String name, PasswordCredential credential) throws UserNotFoundException, InactiveAccountException, InvalidAuthenticationException, ExpiredCredentialException, OperationFailedException
    {
        return findUserByName(name);
    }

    @Override
    public User addUser(UserTemplate user, PasswordCredential credential)
    {
        userByNameMap.put(user.getName().toLowerCase(), user);
        if (user.getExternalId() != null)
        {
            userByExternalIdMap.put(user.getExternalId(), user);
        }
        return user;
    }

    @Override
    public User updateUser(UserTemplate user) throws UserNotFoundException
    {
        removeUser(user.getName());

        userByNameMap.put(user.getName().toLowerCase(), user);
        if (user.getExternalId() != null)
        {
            userByExternalIdMap.put(user.getExternalId(), user);
        }
        return user;
    }

    @Override
    public void updateUserCredential(String username, PasswordCredential credential) throws UserNotFoundException, InvalidCredentialException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public User renameUser(String oldName, String newName) throws UserNotFoundException, InvalidUserException, UserAlreadyExistsException
    {
        return rename(findUserByName(oldName), newName);
    }

    protected User rename(User oldUser, String newName) throws UserNotFoundException
    {
        removeUser(oldUser.getName());
        UserTemplate newUser = new UserTemplate(oldUser);
        newUser.setName(newName);
        return addUser(newUser, PasswordCredential.NONE);
    }

    @Override
    public void storeUserAttributes(String username, Map<String, Set<String>> attributes) throws UserNotFoundException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeUserAttributes(String username, String attributeName) throws UserNotFoundException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeUser(String name) throws UserNotFoundException
    {
        User existingUser = userByNameMap.remove(name.toLowerCase());
        if (existingUser == null)
        {
            throw new UserNotFoundException(name);
        }
        if (existingUser.getExternalId() != null)
        {
            userByExternalIdMap.remove(existingUser.getExternalId());
        }
    }

    @Override
    public <T> List<T> searchUsers(EntityQuery<T> query) throws OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Group findGroupByName(String name) throws GroupNotFoundException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public GroupWithAttributes findGroupWithAttributesByName(String name) throws GroupNotFoundException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Group addGroup(GroupTemplate group) throws InvalidGroupException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Group updateGroup(GroupTemplate group) throws InvalidGroupException, GroupNotFoundException, ReadOnlyGroupException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Group renameGroup(String oldName, String newName) throws GroupNotFoundException, InvalidGroupException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void storeGroupAttributes(String groupName, Map<String, Set<String>> attributes) throws GroupNotFoundException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeGroupAttributes(String groupName, String attributeName) throws GroupNotFoundException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeGroup(String name) throws GroupNotFoundException, ReadOnlyGroupException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public <T> List<T> searchGroups(EntityQuery<T> query) throws OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isUserDirectGroupMember(String username, String groupName) throws OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isGroupDirectGroupMember(String childGroup, String parentGroup) throws OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void addUserToGroup(String username, String groupName) throws GroupNotFoundException, UserNotFoundException, ReadOnlyGroupException, OperationFailedException, MembershipAlreadyExistsException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void addGroupToGroup(String childGroup, String parentGroup) throws GroupNotFoundException, InvalidMembershipException, ReadOnlyGroupException, OperationFailedException, MembershipAlreadyExistsException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeUserFromGroup(String username, String groupName) throws GroupNotFoundException, UserNotFoundException, MembershipNotFoundException, ReadOnlyGroupException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void removeGroupFromGroup(String childGroup, String parentGroup) throws GroupNotFoundException, InvalidMembershipException, MembershipNotFoundException, ReadOnlyGroupException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public <T> List<T> searchGroupRelationships(MembershipQuery<T> query) throws OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public void testConnection() throws OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean supportsInactiveAccounts()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean supportsNestedGroups()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isRolesDisabled()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Iterable<Membership> getMemberships() throws OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public RemoteDirectory getAuthoritativeDirectory()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public Set<String> getValues(String key)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public String getValue(String key)
    {
        return attributeMap.get(key);
    }

    @Override
    public Set<String> getKeys()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isEmpty()
    {
        throw new UnsupportedOperationException("Not implemented");
    }
}
