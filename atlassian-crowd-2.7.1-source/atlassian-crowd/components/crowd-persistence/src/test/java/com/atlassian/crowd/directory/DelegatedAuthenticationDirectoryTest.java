package com.atlassian.crowd.directory;

import java.util.Arrays;
import java.util.List;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.event.DirectoryEvent;
import com.atlassian.crowd.event.group.AutoGroupCreatedEvent;
import com.atlassian.crowd.event.group.AutoGroupMembershipCreatedEvent;
import com.atlassian.crowd.event.group.AutoGroupMembershipDeletedEvent;
import com.atlassian.crowd.event.group.GroupCreatedEvent;
import com.atlassian.crowd.event.group.GroupMembershipCreatedEvent;
import com.atlassian.crowd.event.group.GroupMembershipDeletedEvent;
import com.atlassian.crowd.event.user.AutoUserCreatedEvent;
import com.atlassian.crowd.event.user.AutoUserUpdatedEvent;
import com.atlassian.crowd.event.user.UserCreatedEvent;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.ReadOnlyGroupException;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.InternalDirectoryGroup;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.InternalEntityHelper;
import com.atlassian.event.api.EventPublisher;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatcher;
import org.mockito.Matchers;

import static com.atlassian.crowd.model.group.Groups.namesOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

/**
 * DelegatedAuthenticationDirectory Tester.
 */
public class DelegatedAuthenticationDirectoryTest
{
    private static final Long DIRECTORY_ID = 1L;
    private static final String TEST_PASSWORD = "password";
    private static final String TEST_USER_NAME = "testuser";
    private static final String TEST_USER_NAME_DIFFERENT_CASE = "TestUser";
    private static final String TEST_USER_NAME_TO_RENAME = "testusertorename";
    private static final String GROUP_NAME1 = "group1";
    private static final String GROUP_NAME2 = "group2";
    public static final String USER_EXTERNAL_ID = "in-cache-external-id";
    public static final String TEST_USER_NAME_BLANK_EXTERNAL_ID = "use-with-blank-external-id";

    private DelegatedAuthenticationDirectory directory;
    private final EventPublisher eventPublisher = mock(EventPublisher.class);
    private PasswordCredential passwordCredential;
    private InternalUser user;
    private InternalUser userDifferentCase;
    private InternalUser userToRename;
    private InternalUser internalUser;
    private InternalUser userToRemoveExternalId;
    private InternalUser userWithBlankExternalId;
    private InternalUser userWithBlankExternalId2;
    private InternalUser userWithDifferentExternalId;
    private final InternalRemoteDirectory internalDirectory = mock(InternalRemoteDirectory.class);
    private final RemoteDirectory ldapDirectory = mock(RemoteDirectory.class);
    private final Group remoteGroup1 = mock(Group.class);
    private final Group remoteGroup2 = mock(Group.class);
    private final InternalDirectoryGroup internalGroup1 = mock(InternalDirectoryGroup.class);
    private final InternalDirectoryGroup internalGroup2 = mock(InternalDirectoryGroup.class);
    private final InternalDirectoryGroup localGroup1 = mock(InternalDirectoryGroup.class);
    private final InternalDirectoryGroup localGroup2 = mock(InternalDirectoryGroup.class);
    private final Directory directoryAPI = mock(Directory.class);

    static class UserExternalIdNullMatcher extends ArgumentMatcher<UserTemplate>
    {
        @Override
        public boolean matches(Object argument)
        {
            return ((UserTemplate) argument).getExternalId() == null;
        }
    }

    @Before
    public void setUp() throws Exception
    {
        passwordCredential = new PasswordCredential(TEST_PASSWORD, true);
        final DirectoryDao directoryDao = mock(DirectoryDao.class);
        when(directoryDao.findById(DIRECTORY_ID)).thenReturn(directoryAPI);

        when(internalDirectory.getDirectoryId()).thenReturn(DIRECTORY_ID);

        directory = new DelegatedAuthenticationDirectory(ldapDirectory, internalDirectory, eventPublisher, directoryDao);

        user = InternalEntityHelper.createUser(DIRECTORY_ID, TEST_USER_NAME, USER_EXTERNAL_ID);
        userDifferentCase = InternalEntityHelper.createUser(DIRECTORY_ID, TEST_USER_NAME_DIFFERENT_CASE, USER_EXTERNAL_ID);
        userToRename = InternalEntityHelper.createUser(DIRECTORY_ID, TEST_USER_NAME_TO_RENAME, USER_EXTERNAL_ID);
        internalUser = InternalEntityHelper.createUser(DIRECTORY_ID, TEST_USER_NAME, false);

        userWithDifferentExternalId = InternalEntityHelper.createUser(DIRECTORY_ID, TEST_USER_NAME, "other-external-id");
        userWithBlankExternalId = InternalEntityHelper.createUser(DIRECTORY_ID, TEST_USER_NAME_BLANK_EXTERNAL_ID, null);
        userWithBlankExternalId2 = InternalEntityHelper.createUser(DIRECTORY_ID, TEST_USER_NAME, null);
        userToRemoveExternalId = InternalEntityHelper.createUser(DIRECTORY_ID, "other-user-name", USER_EXTERNAL_ID);

        when(remoteGroup1.getName()).thenReturn(GROUP_NAME1);
        when(remoteGroup2.getName()).thenReturn(GROUP_NAME2);
        when(internalGroup1.getName()).thenReturn(GROUP_NAME1);
        when(internalGroup2.getName()).thenReturn(GROUP_NAME2);
        when(localGroup1.getName()).thenReturn(GROUP_NAME1);
        when(localGroup2.getName()).thenReturn(GROUP_NAME2);
        when(internalGroup1.isLocal()).thenReturn(false);
        when(internalGroup2.isLocal()).thenReturn(false);
        when(localGroup1.isLocal()).thenReturn(true);
        when(localGroup2.isLocal()).thenReturn(true);
    }

    @Test
    public void nestedGroupsEnabledDependsOnLdapDirectory()
    {
        configurationOf(ldapDirectory).setSupportsNestedGroups(true);

        assertTrue("Nested groups should be enabled", directory.supportsNestedGroups());

        verify(ldapDirectory).supportsNestedGroups();
        verifyZeroInteractions(internalDirectory);
    }

    @Test
    public void nestedGroupsDisabledDependsOnLdapDirectory()
    {
        configurationOf(ldapDirectory).setSupportsNestedGroups(false);

        assertFalse("Nested groups should be disabled", directory.supportsNestedGroups());

        verify(ldapDirectory).supportsNestedGroups();
        verifyZeroInteractions(internalDirectory);
    }

    @Test
    public void testAuthentication() throws Exception
    {
        configurationOf(internalDirectory).containsUser(user);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(ldapDirectory).authenticate(TEST_USER_NAME, passwordCredential);
        assertEquals(user, remotePrincipal);
    }

    @Test
    public void testAuthenticateWithInvalidCredentials() throws Exception
    {
        configurationOf(internalDirectory).containsUser(user);
        //noinspection ThrowableResultOfMethodCallIgnored
        configurationOf(ldapDirectory)
            .doesNotAuthenticateUser(user, passwordCredential);

        try
        {
            directory.authenticate(TEST_USER_NAME, passwordCredential);
            fail("InvalidAuthenticationException expected");
        } catch (InvalidAuthenticationException e)
        {
            // expected
        }
    }

    @Test
    public void testAuthenticateWithInactiveUser() throws Exception
    {
        configurationOf(internalDirectory).containsUser(internalUser);

        try
        {
            directory.authenticate(internalUser.getName(), passwordCredential);
            fail("InactiveAccountException expected");
        } catch (InactiveAccountException e)
        {
            // expected
        }
    }

    @Test
    public void testAuthenticateWhenUserDoesNotExistInternallyWithAutoCreate() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .doesNotContainUser(user);

        configurationOf(ldapDirectory)
            .authenticates(user, passwordCredential);

        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(user);
        when(internalDirectory.findUserByExternalId(anyString())).thenThrow(UserNotFoundException.class);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, times(1)).publish(isA(AutoUserCreatedEvent.class));
        verify(internalDirectory).findUserByExternalId(USER_EXTERNAL_ID);
        assertEquals(user, remotePrincipal);
    }


    @Test
    public void testAuthenticateWhenUserExistInternallyButWasRenamedInLdapAndNotAllowedToUpdate() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(false)
             //user is in internal repo with old username
            .doesNotContainUser(user)
            .containsUser(userToRename);

        configurationOf(ldapDirectory)
            .authenticates(user, passwordCredential);

        when(internalDirectory.findUserByExternalId(user.getExternalId())).thenReturn(userToRename);
        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(user);

        final User resultUser = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, never()).publish(isA(AutoUserUpdatedEvent.class));
        verify(internalDirectory, never()).updateUser(new UserTemplate(user));
        verify(internalDirectory, times(1)).addUser(new UserTemplate(user), null);

        assertEquals(user, resultUser);
    }

    @Test
    public void testDoNotRemoveExternalIdBeforeUserUpdateWithoutReason() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(true)
            .containsUser(user);

        configurationOf(ldapDirectory)
            .authenticates(user, passwordCredential);

        when(internalDirectory.findUserByExternalId(user.getExternalId())).thenReturn(user);
        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        final User resultUser = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(internalDirectory, never()).updateUser(new UserTemplate(userToRemoveExternalId));
        verify(internalDirectory, times(1)).updateUser(new UserTemplate(user));

        assertEquals(user, resultUser);
    }

    @Test
    public void testDoNotRemoveExternalIdIfItISEqualToExternalIdFromLdap() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(true)
            .containsUser(user)
            .containsUser(userToRemoveExternalId);

        configurationOf(ldapDirectory)
            .authenticates(user, passwordCredential);

        when(internalDirectory.findUserByExternalId(user.getExternalId())).thenReturn(userToRemoveExternalId);
        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        final User resultUser = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(internalDirectory, never()).updateUser(new UserTemplate(userToRemoveExternalId));
        verify(internalDirectory, times(1)).updateUser(new UserTemplate(user));

        assertEquals(user, resultUser);
    }

    @Test
    public void testRemoveExternalIdBeforeUserUpdateToPreventDuplicationOfExternalIds() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(true)
            .containsUser(userToRemoveExternalId)
            .containsUser(userWithDifferentExternalId);

        configurationOf(ldapDirectory)
            .authenticates(user, passwordCredential);

        when(internalDirectory.findUserByExternalId(user.getExternalId())).thenReturn(userToRemoveExternalId);
        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);
        when(ldapDirectory.findUserByExternalId(userWithDifferentExternalId.getExternalId())).thenThrow(new UserNotFoundException(user.getExternalId()));

        final User resultUser = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, times(2)).publish(isA(AutoUserUpdatedEvent.class));
        verify(internalDirectory, times(1)).updateUser(argThat(new UserExternalIdNullMatcher()));
        verify(internalDirectory, times(1)).updateUser(new UserTemplate(user));

        assertEquals(user, resultUser);
    }

    @Test
    public void testDoNotRemoveExternalIdWhenInternalUserDoesNotHaveOne() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(true)
            .containsUser(userWithBlankExternalId2);

        configurationOf(ldapDirectory)
            .authenticates(user, passwordCredential);

        when(internalDirectory.findUserByExternalId(user.getExternalId())).thenReturn(null);
        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        final User resultUser = directory.authenticate(TEST_USER_NAME, passwordCredential);

        class UserExternalIdNullMatcher<T> extends ArgumentMatcher<T>
        {
            @Override
            public boolean matches(Object argument)
            {
                return ((User) argument).getExternalId() == null;
            }
        }

        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(internalDirectory, never()).updateUser(argThat(new UserExternalIdNullMatcher<UserTemplate>()));
        verify(internalDirectory, times(1)).updateUser(new UserTemplate(user));

        assertEquals(user, resultUser);
    }

    @Test
    public void testDoNotRemoveExternalIdWhenLdapUserExternalIdIsBlank() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(true)
            .containsUser(userWithBlankExternalId)
            .containsUser(userToRemoveExternalId);

        configurationOf(ldapDirectory)
            .authenticates(userWithBlankExternalId, passwordCredential);

        when(internalDirectory.updateUser(new UserTemplate(userWithBlankExternalId))).thenReturn(userWithBlankExternalId);

        final User resultUser = directory.authenticate(TEST_USER_NAME_BLANK_EXTERNAL_ID, passwordCredential);

        verify(internalDirectory, never()).updateUser(new UserTemplate(userToRemoveExternalId));
        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(internalDirectory, times(1)).updateUser(new UserTemplate(userWithBlankExternalId));

        assertEquals(userWithBlankExternalId, resultUser);
    }


    @Test
    public void testAuthenticateWhenUserExistInternallyButWasRenamedAndUpdatedInLdap() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(true)
            //user is in internal repo with old username
            .doesNotContainUser(user)
            .containsUser(userToRename);

        configurationOf(ldapDirectory)
            .authenticates(user, passwordCredential);

        when(internalDirectory.findUserByExternalId(user.getExternalId())).thenReturn(userToRename);
        when(internalDirectory.forceRenameUser(userToRename, user.getName())).thenReturn(user);
        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        final User resultUser = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(internalDirectory).updateUser(new UserTemplate(user));
        verify(internalDirectory, times(1)).forceRenameUser(userToRename, user.getName());

        assertEquals(user, resultUser);
    }

    @Test
    public void testAuthenticateAndDoRegularRenameWhenUserExistInternallyButNameInLdapDiffersInCaseOnly() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(true)
            .containsUser(userDifferentCase);

        configurationOf(ldapDirectory)
            .authenticates(user, passwordCredential);

        when(internalDirectory.findUserByExternalId(user.getExternalId())).thenReturn(userDifferentCase);
        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        final User resultUser = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher).publish(isA(AutoUserUpdatedEvent.class));
        verify(internalDirectory).updateUser(new UserTemplate(user));
        verify(internalDirectory, never()).forceRenameUser(userDifferentCase, user.getName());
        verify(internalDirectory).renameUser(userDifferentCase.getName(), user.getName());

        assertEquals(user, resultUser);
    }

    @Test
    public void testAuthenticateWhenUserExistInternallyButWasRenamedAndGroupsChangedInLdap() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(true)
            .setImportGroups(true)
            .doesNotContainUser(user)
            .containsGroup(internalGroup2)
            .userHasDirectParentGroups(user, internalGroup1); // There are 2 memberships in the LDAP and only 1 membership in the local directory

        configurationOf(ldapDirectory)
                .authenticates(user, passwordCredential)
                .userHasDirectParentGroupsAsString(user, remoteGroup1, remoteGroup2);

        when(internalDirectory.findUserByExternalId(user.getExternalId())).thenReturn(userToRename);
        when(internalDirectory.forceRenameUser(userToRename, user.getName())).thenReturn(user);
        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        final User resultUser = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoGroupMembershipCreatedEvent.class));
        verify(internalDirectory, times(1)).addUserToGroup(TEST_USER_NAME, GROUP_NAME2);
        verify(internalDirectory, times(1)).forceRenameUser(userToRename, user.getName());

        assertEquals(user, resultUser);
    }

    @Test(expected = InactiveAccountException.class)
    public void testAuthenticateButLocalUserIsInactive() throws Exception
    {
        final DirectoryDao directoryDao = mock(DirectoryDao.class);
        when(directoryDao.findById(2)).thenReturn(directoryAPI);
        MockRemoteDirectory mockRemoteDirectory = new MockRemoteDirectory();
        MockInternalRemoteDirectory mockInternalDirectory = new MockInternalRemoteDirectory();
        DelegatedAuthenticationDirectory delegatedAuthenticationDirectory = new DelegatedAuthenticationDirectory(mockRemoteDirectory, mockInternalDirectory, eventPublisher, directoryDao);
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, "true");
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, "true");

        // LDAP
        final UserTemplate ldapUserA = new UserTemplate("asmith", 2);
        ldapUserA.setExternalId("a1");
        ldapUserA.setActive(true);
        mockRemoteDirectory.addUser(ldapUserA, PasswordCredential.NONE);

        // Internal
        final UserTemplate internalUserA = new UserTemplate("asmith", 2);
        internalUserA.setExternalId(null);
        internalUserA.setActive(false);
        mockInternalDirectory.addUser(internalUserA, PasswordCredential.NONE);

        delegatedAuthenticationDirectory.authenticate("asmith", PasswordCredential.NONE);
    }

    @Test
    public void testAuthenticateAndUpdateExternalIdWhenExternalIdMissing() throws Exception
    {
        final DirectoryDao directoryDao = mock(DirectoryDao.class);
        when(directoryDao.findById(2)).thenReturn(directoryAPI);
        MockRemoteDirectory mockRemoteDirectory = new MockRemoteDirectory();
        MockInternalRemoteDirectory mockInternalDirectory = new MockInternalRemoteDirectory();
        DelegatedAuthenticationDirectory delegatedAuthenticationDirectory = new DelegatedAuthenticationDirectory(mockRemoteDirectory, mockInternalDirectory, eventPublisher, directoryDao);
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, "true");
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, "true");

        // LDAP
        final UserTemplate ldapUserA = new UserTemplate("asmith", 2);
        ldapUserA.setExternalId("a1");
        ldapUserA.setActive(true);
        mockRemoteDirectory.addUser(ldapUserA, PasswordCredential.NONE);

        // Internal
        final UserTemplate internalUserA = new UserTemplate("asmith", 2);
        internalUserA.setExternalId(null);
        internalUserA.setActive(true);
        mockInternalDirectory.addUser(internalUserA, PasswordCredential.NONE);


        assertEquals(null, mockInternalDirectory.findUserByName("asmith").getExternalId());
        User loggedInUser = delegatedAuthenticationDirectory.authenticate("asmith", PasswordCredential.NONE);
        assertEquals("asmith", loggedInUser.getName());
        assertEquals("a1", loggedInUser.getExternalId());
        assertEquals("a1", mockInternalDirectory.findUserByName("asmith").getExternalId());
    }

    @Test
    public void testAuthenticateAndUpdateUserNameCase() throws Exception
    {
        final DirectoryDao directoryDao = mock(DirectoryDao.class);
        when(directoryDao.findById(2)).thenReturn(directoryAPI);
        MockRemoteDirectory mockRemoteDirectory = new MockRemoteDirectory();
        MockInternalRemoteDirectory mockInternalDirectory = new MockInternalRemoteDirectory();
        DelegatedAuthenticationDirectory delegatedAuthenticationDirectory = new DelegatedAuthenticationDirectory(mockRemoteDirectory, mockInternalDirectory, eventPublisher, directoryDao);
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, "true");
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, "true");

        // LDAP
        final UserTemplate ldapUserA = new UserTemplate("ASmith", 2);
        ldapUserA.setExternalId("a1");
        ldapUserA.setActive(true);
        mockRemoteDirectory.addUser(ldapUserA, PasswordCredential.NONE);

        // Internal
        final UserTemplate internalUserA = new UserTemplate("asmith", 2);
        internalUserA.setExternalId(null);
        internalUserA.setActive(true);
        mockInternalDirectory.addUser(internalUserA, PasswordCredential.NONE);

        // Assert initial conditions
        assertEquals("asmith", mockInternalDirectory.findUserByName("asmith").getName());
        assertEquals(null, mockInternalDirectory.findUserByName("asmith").getExternalId());

        User loggedInUser = delegatedAuthenticationDirectory.authenticate("asmith", PasswordCredential.NONE);
        assertEquals("ASmith", loggedInUser.getName());
        assertEquals("a1", loggedInUser.getExternalId());
        assertEquals("ASmith", mockInternalDirectory.findUserByName("asmith").getName());
        assertEquals("a1", mockInternalDirectory.findUserByName("asmith").getExternalId());
    }

    @Test
    public void testAuthenticateAndUpdateExternalIdWhenLdapUserIsDeletedAndRecreated() throws Exception
    {
        final DirectoryDao directoryDao = mock(DirectoryDao.class);
        when(directoryDao.findById(2)).thenReturn(directoryAPI);
        MockRemoteDirectory mockRemoteDirectory = new MockRemoteDirectory();
        MockInternalRemoteDirectory mockInternalDirectory = new MockInternalRemoteDirectory();
        DelegatedAuthenticationDirectory delegatedAuthenticationDirectory = new DelegatedAuthenticationDirectory(mockRemoteDirectory, mockInternalDirectory, eventPublisher, directoryDao);
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, "true");
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, "true");

        // LDAP
        final UserTemplate ldapUserA = new UserTemplate("asmith", 2);
        ldapUserA.setExternalId("b2");
        ldapUserA.setActive(true);
        mockRemoteDirectory.addUser(ldapUserA, PasswordCredential.NONE);

        // Internal
        final UserTemplate internalUserA = new UserTemplate("asmith", 2);
        internalUserA.setExternalId("a1");
        internalUserA.setActive(true);
        mockInternalDirectory.addUser(internalUserA, PasswordCredential.NONE);

        assertEquals("a1", mockInternalDirectory.findUserByName("asmith").getExternalId());
        User loggedInUser = delegatedAuthenticationDirectory.authenticate("asmith", PasswordCredential.NONE);
        assertEquals("asmith", loggedInUser.getName());
        assertEquals("b2", loggedInUser.getExternalId());
        assertEquals("b2", mockInternalDirectory.findUserByName("asmith").getExternalId());
    }

    @Test
    public void testAuthenticateAsUser1WhenLdapUserRenameChain() throws Exception
    {
        final DirectoryDao directoryDao = mock(DirectoryDao.class);
        when(directoryDao.findById(2)).thenReturn(directoryAPI);
        MockRemoteDirectory mockRemoteDirectory = new MockRemoteDirectory();
        MockInternalRemoteDirectory mockInternalDirectory = new MockInternalRemoteDirectory();
        DelegatedAuthenticationDirectory delegatedAuthenticationDirectory = new DelegatedAuthenticationDirectory(mockRemoteDirectory, mockInternalDirectory, eventPublisher, directoryDao);
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, "true");
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, "true");

        // LDAP
        // asmith -> adam.smith     andrew -> asmith
        final UserTemplate ldapUserA = new UserTemplate("adam.smith", 2);
        ldapUserA.setExternalId("a1");
        ldapUserA.setActive(true);
        mockRemoteDirectory.addUser(ldapUserA, PasswordCredential.NONE);
        final UserTemplate ldapUserB = new UserTemplate("asmith", 2);
        ldapUserB.setExternalId("b2");
        ldapUserB.setActive(true);
        mockRemoteDirectory.addUser(ldapUserB, PasswordCredential.NONE);

        // Internal
        final UserTemplate internalUserA = new UserTemplate("asmith", 2);
        internalUserA.setExternalId("a1");
        internalUserA.setActive(true);
        mockInternalDirectory.addUser(internalUserA, PasswordCredential.NONE);
        final UserTemplate internalUserB = new UserTemplate("andrew", 2);
        internalUserB.setExternalId("b2");
        internalUserB.setActive(true);
        mockInternalDirectory.addUser(internalUserB, PasswordCredential.NONE);

        // Preconditions
        assertEquals("a1", mockInternalDirectory.findUserByName("asmith").getExternalId());
        assertEquals("b2", mockInternalDirectory.findUserByName("andrew").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("adam.smith"));

        // First log in as adam.smith - this is simple as there is no-one in the way for the rename
        User loggedInUser = delegatedAuthenticationDirectory.authenticate("adam.smith", PasswordCredential.NONE);
        assertEquals("adam.smith", loggedInUser.getName());
        assertEquals("a1", loggedInUser.getExternalId());
        // Post conditions
        assertEquals("b2", mockInternalDirectory.findUserByName("andrew").getExternalId());
        assertEquals("a1", mockInternalDirectory.findUserByName("adam.smith").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("asmith"));

        // Now log in as asmith
        loggedInUser = delegatedAuthenticationDirectory.authenticate("asmith", PasswordCredential.NONE);
        assertEquals("asmith", loggedInUser.getName());
        assertEquals("b2", loggedInUser.getExternalId());
        // Post conditions
        assertEquals("a1", mockInternalDirectory.findUserByName("adam.smith").getExternalId());
        assertEquals("b2", mockInternalDirectory.findUserByName("asmith").getExternalId());
    }

    @Test
    public void testAuthenticateAsUser2WhenLdapUserRenameChain() throws Exception
    {
        final DirectoryDao directoryDao = mock(DirectoryDao.class);
        when(directoryDao.findById(2)).thenReturn(directoryAPI);
        MockRemoteDirectory mockRemoteDirectory = new MockRemoteDirectory();
        MockInternalRemoteDirectory mockInternalDirectory = new MockInternalRemoteDirectory();
        DelegatedAuthenticationDirectory delegatedAuthenticationDirectory = new DelegatedAuthenticationDirectory(mockRemoteDirectory, mockInternalDirectory, eventPublisher, directoryDao);
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, "true");
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, "true");

        // LDAP
        // asmith -> adam.smith     andrew -> asmith
        final UserTemplate ldapUserA = new UserTemplate("adam.smith", 2);
        ldapUserA.setExternalId("a1");
        ldapUserA.setActive(true);
        mockRemoteDirectory.addUser(ldapUserA, PasswordCredential.NONE);
        final UserTemplate ldapUserB = new UserTemplate("asmith", 2);
        ldapUserB.setExternalId("b2");
        ldapUserB.setActive(true);
        mockRemoteDirectory.addUser(ldapUserB, PasswordCredential.NONE);

        // Internal
        final UserTemplate internalUserA = new UserTemplate("asmith", 2);
        internalUserA.setExternalId("a1");
        internalUserA.setActive(true);
        mockInternalDirectory.addUser(internalUserA, PasswordCredential.NONE);
        final UserTemplate internalUserB = new UserTemplate("andrew", 2);
        internalUserB.setExternalId("b2");
        internalUserB.setActive(true);
        mockInternalDirectory.addUser(internalUserB, PasswordCredential.NONE);

        // Preconditions
        assertEquals("a1", mockInternalDirectory.findUserByName("asmith").getExternalId());
        assertEquals("b2", mockInternalDirectory.findUserByName("andrew").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("adam.smith"));

        // First log in as asmith
        User loggedInUser = delegatedAuthenticationDirectory.authenticate("asmith", PasswordCredential.NONE);
        assertEquals("asmith", loggedInUser.getName());
        assertEquals("b2", loggedInUser.getExternalId());
        // Post conditions
        assertEquals("b2", mockInternalDirectory.findUserByName("asmith").getExternalId());
        // old asmith is bumped for now
        assertEquals("a1", mockInternalDirectory.findUserByName("asmith#1").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("andrew"));

        // Now log in as adam.smith
        loggedInUser = delegatedAuthenticationDirectory.authenticate("adam.smith", PasswordCredential.NONE);
        assertEquals("adam.smith", loggedInUser.getName());
        assertEquals("a1", loggedInUser.getExternalId());
        // Post conditions
        assertEquals("b2", mockInternalDirectory.findUserByName("asmith").getExternalId());
        // asmith#1 gets cleaned up
        assertEquals("a1", mockInternalDirectory.findUserByName("adam.smith").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("asmith#1"));
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("andrew"));
    }

    @Test
    public void testAuthenticateWithLdapUserCircularRename() throws Exception
    {
        final DirectoryDao directoryDao = mock(DirectoryDao.class);
        when(directoryDao.findById(2)).thenReturn(directoryAPI);
        MockRemoteDirectory mockRemoteDirectory = new MockRemoteDirectory();
        MockInternalRemoteDirectory mockInternalDirectory = new MockInternalRemoteDirectory();
        DelegatedAuthenticationDirectory delegatedAuthenticationDirectory = new DelegatedAuthenticationDirectory(mockRemoteDirectory, mockInternalDirectory, eventPublisher, directoryDao);
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, "true");
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, "true");

        // LDAP
        // asmith -> adam.smith     adam.smith -> asmith
        final UserTemplate ldapUserA = new UserTemplate("adam.smith", 2);
        ldapUserA.setExternalId("a1");
        ldapUserA.setActive(true);
        mockRemoteDirectory.addUser(ldapUserA, PasswordCredential.NONE);
        final UserTemplate ldapUserB = new UserTemplate("asmith", 2);
        ldapUserB.setExternalId("b2");
        ldapUserB.setActive(true);
        mockRemoteDirectory.addUser(ldapUserB, PasswordCredential.NONE);

        // Internal
        final UserTemplate internalUserA = new UserTemplate("asmith", 2);
        internalUserA.setExternalId("a1");
        internalUserA.setActive(true);
        mockInternalDirectory.addUser(internalUserA, PasswordCredential.NONE);
        final UserTemplate internalUserB = new UserTemplate("adam.smith", 2);
        internalUserB.setExternalId("b2");
        internalUserB.setActive(true);
        mockInternalDirectory.addUser(internalUserB, PasswordCredential.NONE);

        // Preconditions
        assertEquals("a1", mockInternalDirectory.findUserByName("asmith").getExternalId());
        assertEquals("b2", mockInternalDirectory.findUserByName("adam.smith").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("asmith#1"));

        // First log in as asmith
        User loggedInUser = delegatedAuthenticationDirectory.authenticate("asmith", PasswordCredential.NONE);
        assertEquals("asmith", loggedInUser.getName());
        assertEquals("b2", loggedInUser.getExternalId());
        // Post conditions
        assertEquals("b2", mockInternalDirectory.findUserByName("asmith").getExternalId());
        // old asmith is bumped for now
        assertEquals("a1", mockInternalDirectory.findUserByName("asmith#1").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("adam.smith"));

        // Now log in as adam.smith
        loggedInUser = delegatedAuthenticationDirectory.authenticate("adam.smith", PasswordCredential.NONE);
        assertEquals("adam.smith", loggedInUser.getName());
        assertEquals("a1", loggedInUser.getExternalId());
        // Post conditions
        assertEquals("b2", mockInternalDirectory.findUserByName("asmith").getExternalId());
        // asmith#1 gets cleaned up
        assertEquals("a1", mockInternalDirectory.findUserByName("adam.smith").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("asmith#1"));
    }

    @Test
    public void testAuthenticateAsOldUserAndRenameWhenLdapUserIsRenamedAndRecreated() throws Exception
    {
        final DirectoryDao directoryDao = mock(DirectoryDao.class);
        when(directoryDao.findById(2)).thenReturn(directoryAPI);
        MockRemoteDirectory mockRemoteDirectory = new MockRemoteDirectory();
        MockInternalRemoteDirectory mockInternalDirectory = new MockInternalRemoteDirectory();
        DelegatedAuthenticationDirectory delegatedAuthenticationDirectory = new DelegatedAuthenticationDirectory(mockRemoteDirectory, mockInternalDirectory, eventPublisher, directoryDao);
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, "true");
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, "true");

        // LDAP
        final UserTemplate ldapUserA = new UserTemplate("adam.smith", 2);
        ldapUserA.setExternalId("a1");
        ldapUserA.setActive(true);
        mockRemoteDirectory.addUser(ldapUserA, PasswordCredential.NONE);
        final UserTemplate ldapUserB = new UserTemplate("asmith", 2);
        ldapUserB.setExternalId("b2");
        ldapUserB.setActive(true);
        mockRemoteDirectory.addUser(ldapUserB, PasswordCredential.NONE);

        // Internal
        final UserTemplate internalUserA = new UserTemplate("asmith", 2);
        internalUserA.setExternalId("a1");
        internalUserA.setActive(true);
        mockInternalDirectory.addUser(internalUserA, PasswordCredential.NONE);

        assertEquals("a1", mockInternalDirectory.findUserByName("asmith").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("adam.smith"));

        User loggedInUser = delegatedAuthenticationDirectory.authenticate("adam.smith", PasswordCredential.NONE);
        assertEquals("adam.smith", loggedInUser.getName());
        assertEquals("a1", loggedInUser.getExternalId());
        assertEquals("a1", mockInternalDirectory.findUserByName("adam.smith").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("asmith"));
    }

    @Test
    public void testAuthenticateAsNewUserAndRenameWhenLdapUserIsRenamedAndRecreated() throws Exception
    {
        final DirectoryDao directoryDao = mock(DirectoryDao.class);
        when(directoryDao.findById(2)).thenReturn(directoryAPI);
        MockRemoteDirectory mockRemoteDirectory = new MockRemoteDirectory();
        MockInternalRemoteDirectory mockInternalDirectory = new MockInternalRemoteDirectory();
        DelegatedAuthenticationDirectory delegatedAuthenticationDirectory = new DelegatedAuthenticationDirectory(mockRemoteDirectory, mockInternalDirectory, eventPublisher, directoryDao);
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, "true");
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, "true");

        // LDAP
        final UserTemplate ldapUserA = new UserTemplate("adam.smith", 2);
        ldapUserA.setExternalId("a1");
        ldapUserA.setActive(true);
        mockRemoteDirectory.addUser(ldapUserA, PasswordCredential.NONE);
        final UserTemplate ldapUserB = new UserTemplate("asmith", 2);
        ldapUserB.setExternalId("b2");
        ldapUserB.setActive(true);
        mockRemoteDirectory.addUser(ldapUserB, PasswordCredential.NONE);

        // Internal
        final UserTemplate internalUserA = new UserTemplate("asmith", 2);
        internalUserA.setExternalId("a1");
        internalUserA.setActive(true);
        mockInternalDirectory.addUser(internalUserA, PasswordCredential.NONE);

        assertEquals("a1", mockInternalDirectory.findUserByName("asmith").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("adam.smith"));

        User loggedInUser = delegatedAuthenticationDirectory.authenticate("asmith", PasswordCredential.NONE);
        assertEquals("asmith", loggedInUser.getName());
        assertEquals("b2", loggedInUser.getExternalId());
        assertEquals("b2", mockInternalDirectory.findUserByName("asmith").getExternalId());
        // This is the cool bit - we logged in as asmith, but the internal user asmith was out of date and we went back
        // to LDAP and got them updated to the latest name
        assertEquals("a1", mockInternalDirectory.findUserByName("adam.smith").getExternalId());
    }


    @Test
    public void testAuthenticateWhenLdapUserIsDeletedAndRenamed() throws Exception
    {
        final DirectoryDao directoryDao = mock(DirectoryDao.class);
        when(directoryDao.findById(2)).thenReturn(directoryAPI);
        MockRemoteDirectory mockRemoteDirectory = new MockRemoteDirectory();
        MockInternalRemoteDirectory mockInternalDirectory = new MockInternalRemoteDirectory();
        DelegatedAuthenticationDirectory delegatedAuthenticationDirectory = new DelegatedAuthenticationDirectory(mockRemoteDirectory, mockInternalDirectory, eventPublisher, directoryDao);
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, "true");
        mockInternalDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, "true");

        // LDAP
        final UserTemplate ldapUserA = new UserTemplate("asmith", 2);
        ldapUserA.setExternalId("b2");
        ldapUserA.setActive(true);
        mockRemoteDirectory.addUser(ldapUserA, PasswordCredential.NONE);

        // Internal
        final UserTemplate internalUserA = new UserTemplate("asmith", 2);
        internalUserA.setExternalId("a1");
        internalUserA.setActive(true);
        mockInternalDirectory.addUser(internalUserA, PasswordCredential.NONE);
        final UserTemplate internalUserB = new UserTemplate("adam", 2);
        internalUserB.setExternalId("b2");
        internalUserB.setActive(true);
        mockInternalDirectory.addUser(internalUserB, PasswordCredential.NONE);

        // Assert initial conditions
        assertEquals("a1", mockInternalDirectory.findUserByName("asmith").getExternalId());
        assertEquals("b2", mockInternalDirectory.findUserByName("adam").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("asmith#1"));

        User loggedInUser = delegatedAuthenticationDirectory.authenticate("asmith", PasswordCredential.NONE);
        assertEquals("asmith", loggedInUser.getName());
        assertEquals("b2", loggedInUser.getExternalId());
        assertEquals("b2", mockInternalDirectory.findUserByName("asmith").getExternalId());
        assertEquals(null, mockInternalDirectory.findUserByNameOrNull("adam"));
        // The old asmith is bumped out of the way
        assertEquals("a1", mockInternalDirectory.findUserByName("asmith#1").getExternalId());
    }

    @Test
    public void testAuthenticateWhenUserExistInternallyButWasRenamedInLdapAndWeCantMatchHimByExternalId() throws Exception
    {
        configurationOf(internalDirectory)
                .setCreateUserOnAuth(true)
                .setUpdateUserOnAuth(true)
                .doesNotContainUser(user)
                .containsUser(userToRename);

        configurationOf(ldapDirectory)
                .authenticates(user, passwordCredential);

        user = InternalEntityHelper.createUser(DIRECTORY_ID, TEST_USER_NAME, "");
        InternalUser newUser = InternalEntityHelper.createUser(DIRECTORY_ID, "newuser", "");

        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(newUser);

        //if we do not have external id, we can't match users so we should create new user
        final User resultUser = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, times(1)).publish(isA(AutoUserCreatedEvent.class));
        verify(internalDirectory).addUser(new UserTemplate(user), null);
        verify(internalDirectory, never()).renameUser(anyString(), anyString());

        assertEquals(newUser, resultUser);
    }

    @Test
    public void testAuthenticateWhenUserDoesNotExistInternallyWithoutAutoCreate() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(false)
            .doesNotContainUser(user);

        try
        {
            directory.authenticate(TEST_USER_NAME, passwordCredential);

            fail("UserNotFoundException expected");
        } catch (UserNotFoundException e)
        {
            // expected
        }

        verifyZeroInteractions(ldapDirectory);
        verifyZeroInteractions(eventPublisher);
    }

    @Test
    public void testAuthenticateWhenUserDoesNotExistInternallyWithAutoCreateButSuddenlyTheUserAppearsDuringTheAutoCreation() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true);

        configurationOf(ldapDirectory)
            .authenticates(user, passwordCredential);

        when(internalDirectory.findUserByName(TEST_USER_NAME))
            .thenThrow(new UserNotFoundException(TEST_USER_NAME))
            .thenReturn(user);
        when(internalDirectory.addUser(new UserTemplate(user), null)).thenThrow(new UserAlreadyExistsException(
            DIRECTORY_ID,
            user.getName()));

        DirectoryImpl directoryPojo = InternalEntityHelper.createDirectory(DIRECTORY_ID, "");
        directoryPojo.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, Boolean.TRUE.toString());

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        // if the user has been created by somebody else, the event should not be fired.
        verify(eventPublisher, times(0)).publish(isA(UserCreatedEvent.class));
        assertEquals(user, remotePrincipal);
    }

    @Test
    public void testAuthenticateWhenUserDoesNotExistInternallyWithAutoCreateButSuddenlyTheUserAppearsDuringTheAutoCreationAndUserIsInactive() throws Exception
    {
        configurationOf(internalDirectory).setCreateUserOnAuth(true);

        configurationOf(ldapDirectory).authenticates(internalUser, passwordCredential);

        when(internalDirectory.findUserByName(internalUser.getName()))
            .thenThrow(new UserNotFoundException(internalUser.getName()))
            .thenReturn(internalUser);
        when(internalDirectory.addUser(new UserTemplate(internalUser), null))
            .thenThrow(new UserAlreadyExistsException(DIRECTORY_ID, internalUser.getName()));

        DirectoryImpl directoryPojo = InternalEntityHelper.createDirectory(DIRECTORY_ID, "");
        directoryPojo.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, Boolean.TRUE.toString());

        User remotePrincipal = null;
        try
        {
            remotePrincipal = directory.authenticate(internalUser.getName(), passwordCredential);
            fail("InactiveAccountException expected");
        }
        catch (InactiveAccountException iae)
        {
            // expected
        }

        // if the user has been created by somebody else, the event should not be fired.
        verify(eventPublisher, times(0)).publish(isA(DirectoryEvent.class));
        assertNull(remotePrincipal);
    }

    /**
     * When user exists locally, but has been removed from the remote server,
     * InvalidAuthenticationException should be thrown.
     *
     * @throws Exception hopefully every time
     */
    @Test(expected = InvalidAuthenticationException.class)
    public void testAuthenticateExistingUserHasBeenRemovedFromLDAP() throws Exception
    {
        configurationOf(internalDirectory)
            .containsUser(user);

        configurationOf(ldapDirectory)
            .doesNotAuthenticateUser(user, passwordCredential);

        directory.authenticate(TEST_USER_NAME, passwordCredential);
    }

    @Test
    public void testAuthenticateWhenUserDoesExistInternallyWithAutoUpdate() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(true)
            .containsUser(user);

        configurationOf(ldapDirectory)
            .authenticates(user, passwordCredential);

        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(internalDirectory).updateUser(new UserTemplate(user));
        assertEquals(user, remotePrincipal);
    }

    @Test
    public void testAuthenticateWithImportGroupsButNoAutoUpdate() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(false)
            .setUpdateUserOnAuth(false)
            .setImportGroups(true)
            .containsUser(user)
            .containsGroup(internalGroup2)
            .userHasDirectParentGroups(user, internalGroup1); // There are 2 memberships in the LDAP and only 1 membership in the local directory

        configurationOf(ldapDirectory)
            .authenticates(user, passwordCredential)
            .userHasDirectParentGroupsAsString(user, remoteGroup1, remoteGroup2);

        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, never()).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoGroupMembershipCreatedEvent.class));
        verify(internalDirectory, times(1)).addUserToGroup(TEST_USER_NAME, GROUP_NAME2);
        assertSame(user, remotePrincipal);
    }

    @Test
    public void testAuthenticateWhenNothingExistsInternallyWithImportGroupsWithGroupHierarchy() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(false)
            .setImportGroups(true)
            .doesNotContainUser(user)
            .doesNotContainGroup(internalGroup1)
            .doesNotContainGroup(internalGroup2);

        configurationOf(ldapDirectory)
            .setSupportsNestedGroups(true)
            .authenticates(user, passwordCredential)
            .userHasDirectParentGroupsAsString(user, remoteGroup1)
            .groupHasDirectParentGroupsAsString(remoteGroup1, remoteGroup2);

        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(user);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, times(1)).publish(isA(AutoUserCreatedEvent.class));
        verify(eventPublisher, times(2)).publish(isA(AutoGroupCreatedEvent.class));
        verify(eventPublisher, times(2)).publish(isA(AutoGroupMembershipCreatedEvent.class));
        verify(internalDirectory, times(1)).addGroupToGroup(GROUP_NAME1, GROUP_NAME2);
    }

    @Test
    public void testAuthenticateWhenNothingExistsInternallyWithImportGroupsWithGroupHierarchyAndNestedGroupsDisabled() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(false)
            .setImportGroups(true)
            .doesNotContainUser(user)
            .doesNotContainGroup(internalGroup1)
            .doesNotContainGroup(internalGroup2);

        configurationOf(ldapDirectory)
            .setSupportsNestedGroups(false)
            .authenticates(user, passwordCredential)
            .userHasDirectParentGroupsAsString(user, remoteGroup1)
            .groupHasDirectParentGroupsAsString(remoteGroup1, remoteGroup2);

        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(user);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, times(1)).publish(isA(AutoUserCreatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoGroupCreatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoGroupMembershipCreatedEvent.class));
    }

    @Test
    public void testAuthenticateWhenUserDoesNotExistInternallyWithImportGroupsWithGroupHierarchyInternalParentGroupMembershipExists() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(true)
            .setUpdateUserOnAuth(false)
            .setImportGroups(true)
            .doesNotContainUser(user)
            .containsGroup(internalGroup1)
            .groupHasDirectParentGroupsAsString(internalGroup1, internalGroup2);

        configurationOf(ldapDirectory)
            .setSupportsNestedGroups(true)
            .authenticates(user, passwordCredential)
            .userHasDirectParentGroupsAsString(user, remoteGroup1)
            .groupHasDirectParentGroupsAsString(remoteGroup1, remoteGroup2);

        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(user);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, times(1)).publish(isA(AutoUserCreatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoGroupMembershipCreatedEvent.class));
        verify(internalDirectory, never()).addGroupToGroup(any(String.class), any(String.class));
    }

    @Test
    public void testAuthenticateWithImportGroupsWithGroupHierarchyInternalParentGroupMembershipExists() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(false)
            .setUpdateUserOnAuth(false)
            .setImportGroups(true)
            .containsUser(user)
            .userHasDirectParentGroups(user, internalGroup1)
            .groupHasDirectParentGroupsAsString(internalGroup1, internalGroup2);

        configurationOf(ldapDirectory)
            .setSupportsNestedGroups(true)
            .authenticates(user, passwordCredential)
            .userHasDirectParentGroupsAsString(user, remoteGroup1)
            .groupHasDirectParentGroupsAsString(remoteGroup1, remoteGroup2);

        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, never()).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, never()).publish(isA(AutoGroupMembershipCreatedEvent.class));
        verify(internalDirectory, never()).addGroupToGroup(any(String.class), any(String.class));
    }

    @Test
    public void testAuthenticateWithImportGroupsWhenThereIsALoopInTheGroupHierarchy() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(false)
            .setUpdateUserOnAuth(false)
            .setImportGroups(true)
            .containsUser(user)
            .containsGroup(internalGroup2)
            .userHasDirectParentGroups(user, internalGroup1);

        configurationOf(ldapDirectory)
            .setSupportsNestedGroups(true)
            .authenticates(user, passwordCredential)
            .userHasDirectParentGroupsAsString(user, remoteGroup1)
            .groupHasDirectParentGroupsAsString(remoteGroup1, remoteGroup2)
            .groupHasDirectParentGroupsAsString(remoteGroup2, remoteGroup1); // loop!

        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, never()).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoGroupMembershipCreatedEvent.class));
        verify(internalDirectory).addGroupToGroup(GROUP_NAME1, GROUP_NAME2); // this one is fine
        verify(internalDirectory, never()).addGroupToGroup(GROUP_NAME2, GROUP_NAME1); // this would introduce a loop
    }

    @Test
    public void testAuthenticateWithImportGroupsWithGroupHierarchyInternalParentGroupMembershipDoesNotExist() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(false)
            .setUpdateUserOnAuth(false)
            .setImportGroups(true)
            .containsUser(user)
            .containsGroup(internalGroup2)
            .userHasDirectParentGroups(user, internalGroup1);

        configurationOf(ldapDirectory)
            .setSupportsNestedGroups(true)
            .authenticates(user, passwordCredential)
            .userHasDirectParentGroupsAsString(user, remoteGroup1)
            .groupHasDirectParentGroupsAsString(remoteGroup1, remoteGroup2);

        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, never()).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoGroupMembershipCreatedEvent.class));
        verify(internalDirectory, times(1)).addGroupToGroup(GROUP_NAME1, GROUP_NAME2);
        assertSame(user, remotePrincipal);
    }

    @Test
    public void testAuthenticateWithImportGroupsWithGroupHierarchyInternalParentGroupMembershipDoesNotExistAndParentGroupIsLocal() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(false)
            .setUpdateUserOnAuth(false)
            .setImportGroups(true)
            .containsUser(user)
            .containsGroup(localGroup2)  // shadows remoteGroup1
            .userHasDirectParentGroups(user, internalGroup1);

        configurationOf(ldapDirectory)
            .setSupportsNestedGroups(true)
            .authenticates(user, passwordCredential)
            .userHasDirectParentGroupsAsString(user, remoteGroup1)
            .groupHasDirectParentGroupsAsString(remoteGroup1, remoteGroup2);

        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, never()).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, never()).publish(isA(AutoGroupMembershipCreatedEvent.class));
        verify(internalDirectory, never()).addGroupToGroup(any(String.class), any(String.class));
    }
    @Test
    public void testAuthenticateWithImportGroupsWithGroupHierarchyInternalParentGroupDoesNotExist() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(false)
            .setUpdateUserOnAuth(false)
            .setImportGroups(true)
            .containsUser(user)
            .doesNotContainGroup(internalGroup2)
            .userHasDirectParentGroups(user, internalGroup1);

        configurationOf(ldapDirectory)
            .setSupportsNestedGroups(true)
            .authenticates(user, passwordCredential)
            .userHasDirectParentGroupsAsString(user, remoteGroup1)
            .groupHasDirectParentGroupsAsString(remoteGroup1, remoteGroup2);

        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, never()).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoGroupCreatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoGroupMembershipCreatedEvent.class));
        verify(internalDirectory, times(1)).addGroupToGroup(GROUP_NAME1, GROUP_NAME2);
    }

    @Test
    public void testAuthenticateWithImportGroupsWithGroupHierarchyInternalParentGroupMembershipShouldBeGone() throws Exception
    {
        configurationOf(internalDirectory)
            .setCreateUserOnAuth(false)
            .setUpdateUserOnAuth(false)
            .setImportGroups(true)
            .containsUser(user)
            .userHasDirectParentGroups(user, internalGroup1)
            .groupHasDirectParentGroupsAsString(internalGroup1, internalGroup2);

        configurationOf(ldapDirectory)
            .setSupportsNestedGroups(true)
            .authenticates(user, passwordCredential)
            .userHasDirectParentGroupsAsString(user, remoteGroup1); // but group1 is NOT a member of group2

        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        User remotePrincipal = directory.authenticate(TEST_USER_NAME, passwordCredential);

        verify(eventPublisher, never()).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, times(0)).publish(isA(AutoGroupCreatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoGroupMembershipDeletedEvent.class));
        verify(internalDirectory, times(1)).removeGroupFromGroup(GROUP_NAME1, GROUP_NAME2);
    }

    @Test
    public void testAddOrUpdateLdapUserWhenUserDoesNotExistInternally() throws Exception
    {
        configurationOf(internalDirectory).doesNotContainUser(user);

        configurationOf(ldapDirectory).containsUser(user);

        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(user);

        User remotePrincipal = directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verify(eventPublisher, times(1)).publish(isA(AutoUserCreatedEvent.class));
        verify(internalDirectory).addUser(new UserTemplate(user), null);
        assertEquals(user, remotePrincipal);
    }

    @Test
    public void testAddOrUpdateLdapUserWhenUserExistsInternally() throws Exception
    {
        configurationOf(internalDirectory).containsUser(user);

        configurationOf(ldapDirectory).containsUser(user);

        when(internalDirectory.updateUser(new UserTemplate(user))).thenReturn(user);

        User remotePrincipal = directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(internalDirectory).updateUser(new UserTemplate(user));
        assertEquals(user, remotePrincipal);
    }

    @Test
    public void testAddOrUpdateLdapUserWhenUserExistsInternallyButIsInactive() throws Exception
    {
        configurationOf(internalDirectory).containsUser(internalUser);

        configurationOf(ldapDirectory).containsUser(internalUser);

        when(internalDirectory.updateUser(any(UserTemplate.class))).thenReturn(internalUser);

        User remotePrincipal = directory.addOrUpdateLdapUser(internalUser.getName());

        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(internalDirectory).updateUser(new UserTemplate(user));
        ArgumentCaptor<UserTemplate> argument = ArgumentCaptor.forClass(UserTemplate.class);
        verify(internalDirectory).updateUser(argument.capture());
        assertFalse("should still not be active when updating", argument.getValue().isActive());
        assertEquals(user, remotePrincipal);
        assertFalse("should still not be active after updating", remotePrincipal.isActive());
    }

    @Test
    public void testAddLdapUserAndGroupAndGroupMembership() throws Exception
    {
        configurationOf(internalDirectory)
            .setImportGroups(true)
            .doesNotContainUser(user)
            .doesNotContainGroup(internalGroup1);

        configurationOf(ldapDirectory)
            .containsUser(user)
            .userHasDirectParentGroupsAsString(user, remoteGroup1);

        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(user);
        when(internalDirectory.addGroup(any(GroupTemplate.class))).thenReturn(internalGroup1);

        User remotePrincipal = directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verifyUserCreated(remotePrincipal);
        verifyGroupCreated(GROUP_NAME1);
        verifyGroupMembershipCreated(GROUP_NAME1);
    }

    @Test
    public void testAddLdapUserAndGroupMembershipToExistingGroup() throws Exception
    {
        configurationOf(internalDirectory)
            .setImportGroups(true)
            .doesNotContainUser(user)
            .containsGroup(internalGroup1);

        configurationOf(ldapDirectory)
            .containsUser(user)
            .userHasDirectParentGroupsAsString(user, remoteGroup1);

        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(user);

        User remotePrincipal = directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verifyUserCreated(remotePrincipal);
        verifyGroupNotCreated();
        verifyGroupMembershipCreated(GROUP_NAME1);
    }

    @Test
    public void testAddLdapUserAndGroupMembershipToExistingGroupWithGroupHierarchyInternalParentGroupMembershipDoesNotExist() throws Exception
    {
        configurationOf(internalDirectory)
            .setImportGroups(true)
            .doesNotContainUser(user)
            .containsGroup(internalGroup1)
            .doesNotContainGroup(internalGroup2);

        configurationOf(ldapDirectory)
            .setSupportsNestedGroups(true)
            .containsUser(user)
            .userHasDirectParentGroupsAsString(user, remoteGroup1)
            .groupHasDirectParentGroupsAsString(remoteGroup1, remoteGroup2);

        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(user);
        when(internalDirectory.addGroup(any(GroupTemplate.class))).thenReturn(internalGroup2);

        User remotePrincipal = directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verifyUserCreated(remotePrincipal);
        verifyGroupCreated(GROUP_NAME2);
        verifyGroupMembershipCreated(GROUP_NAME1);
        verify(internalDirectory).addGroupToGroup(GROUP_NAME1, GROUP_NAME2);
    }

    @Test
    public void testAddLdapUserAndGroupMembershipToExistingGroupWithGroupHierarchyInternalParentGroupMembershipDoesNotExistAndNestedGroupsDisabled() throws Exception
    {
        configurationOf(internalDirectory)
            .setImportGroups(true)
            .doesNotContainUser(user)
            .containsGroup(internalGroup1)
            .doesNotContainGroup(internalGroup2);

        configurationOf(ldapDirectory)
            .setSupportsNestedGroups(false)
            .containsUser(user)
            .userHasDirectParentGroupsAsString(user, remoteGroup1)
            .groupHasDirectParentGroupsAsString(remoteGroup1, remoteGroup2);

        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(user);
        when(internalDirectory.addGroup(any(GroupTemplate.class))).thenReturn(internalGroup2);

        User remotePrincipal = directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verifyUserCreated(remotePrincipal);
        verifyGroupNotCreated();
        verifyGroupMembershipCreated(GROUP_NAME1);
        verify(internalDirectory, never()).addGroupToGroup(GROUP_NAME1, GROUP_NAME2);
    }

    @Test
    public void testAddLdapUserButNotShadowedGroup() throws Exception
    {
        configurationOf(internalDirectory)
            .setImportGroups(true)
            .doesNotContainUser(user)
            .containsGroup(localGroup1);  // shadows remoteGroup1

        configurationOf(ldapDirectory)
            .containsUser(user)
            .userHasDirectParentGroupsAsString(user, remoteGroup1);

        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(user);

        User remotePrincipal = directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verifyUserCreated(remotePrincipal);
        verifyGroupNotCreated();
        verifyGroupMembershipNotCreated();
    }

    @Test
    public void testAddLdapUserWithGroupImportDisabled() throws Exception
    {
        configurationOf(internalDirectory)
            .setImportGroups(false)
            .doesNotContainUser(user)
            .doesNotContainGroup(internalGroup1);

        configurationOf(ldapDirectory)
            .containsUser(user)
            .userHasDirectParentGroupsAsString(user, remoteGroup1);

        when(internalDirectory.addUser(new UserTemplate(user), null)).thenReturn(user);
        when(internalDirectory.addGroup(any(GroupTemplate.class))).thenReturn(remoteGroup1);

        User remotePrincipal = directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verifyUserCreated(remotePrincipal);
        verifyGroupNotCreated();
        verifyGroupMembershipNotCreated();
    }

    @Test
    public void testUpdateLdapUserWithGroupAndMembershipAdded() throws Exception
    {
        configurationOf(internalDirectory)
            .setImportGroups(true)
            .containsUser(user)
            .doesNotContainGroup(internalGroup2)
            .userHasDirectParentGroups(user, internalGroup1);

        configurationOf(ldapDirectory)
            .containsUser(user)
            .userHasDirectParentGroupsAsString(user, remoteGroup1, remoteGroup2);

        when(internalDirectory.updateUser(Matchers.<UserTemplate>any())).thenReturn(user);
        when(internalDirectory.addGroup(any(GroupTemplate.class))).thenReturn(internalGroup2);

        directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verify(eventPublisher, never()).publish(isA(UserCreatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, never()).publish(isA(GroupMembershipDeletedEvent.class));
        verifyGroupCreated(GROUP_NAME2);
        verifyGroupMembershipCreated(GROUP_NAME2);
    }

    @Test
    public void testUpdateLdapUserWithMembershipAdded() throws Exception
    {
        configurationOf(internalDirectory)
            .setImportGroups(true)
            .containsUser(user)
            .containsGroup(internalGroup2)
            .userHasDirectParentGroups(user, internalGroup1);

        configurationOf(ldapDirectory)
            .containsUser(user)
            .userHasDirectParentGroupsAsString(user, remoteGroup1, remoteGroup2);

        when(internalDirectory.updateUser(Matchers.<UserTemplate>any())).thenReturn(user);
        when(internalDirectory.addGroup(any(GroupTemplate.class))).thenReturn(remoteGroup1);

        directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verify(eventPublisher, never()).publish(isA(AutoUserCreatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, never()).publish(isA(GroupMembershipDeletedEvent.class));
        verifyGroupNotCreated();
        verifyGroupMembershipCreated(GROUP_NAME2);
    }

    @Test
    public void testUpdateLdapUserWithMembershipRemoved() throws Exception
    {
        configurationOf(internalDirectory)
            .setImportGroups(true)
            .containsUser(user)
            .containsGroup(internalGroup2)
            .userHasDirectParentGroups(user, internalGroup1, internalGroup2);

        configurationOf(ldapDirectory)
            .containsUser(user)
            .userHasDirectParentGroupsAsString(user, remoteGroup1);

        when(internalDirectory.updateUser(Matchers.<UserTemplate>any())).thenReturn(user);

        directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verify(eventPublisher, never()).publish(isA(AutoUserCreatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoGroupMembershipDeletedEvent.class));
        verifyGroupNotCreated();
        verifyGroupMembershipNotCreated();
        verify(internalDirectory, times(1)).removeUserFromGroup(TEST_USER_NAME, GROUP_NAME2);
    }

    @Test
    public void testUpdateLdapUserWithMembershipUnchanged() throws Exception
    {
        configurationOf(internalDirectory)
            .setImportGroups(true)
            .containsUser(user)
            .userHasDirectParentGroups(user, internalGroup1);

        configurationOf(ldapDirectory)
            .containsUser(user)
            .userHasDirectParentGroupsAsString(user, remoteGroup1);

        when(internalDirectory.updateUser(Matchers.<UserTemplate>any())).thenReturn(user);

        directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verify(eventPublisher, never()).publish(isA(UserCreatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, never()).publish(isA(GroupMembershipDeletedEvent.class));
        verifyGroupNotCreated();
        verifyGroupMembershipNotCreated();
        verify(internalDirectory, never()).removeUserFromGroup(Matchers.<String>any(), Matchers.<String>any());
    }

    @Test
    public void testUpdateLdapUserWithGroupImportDisabled() throws Exception
    {
        configurationOf(internalDirectory)
            .setImportGroups(false)
            .containsUser(user)
            .doesNotContainGroup(internalGroup2)
            .userHasDirectParentGroups(user, internalGroup1);

        configurationOf(ldapDirectory)
            .containsUser(user)
            .userHasDirectParentGroupsAsString(user, remoteGroup1, remoteGroup2);

        when(internalDirectory.updateUser(Matchers.<UserTemplate>any())).thenReturn(user);
        when(internalDirectory.addGroup(any(GroupTemplate.class))).thenReturn(remoteGroup1);

        directory.addOrUpdateLdapUser(TEST_USER_NAME);

        verify(eventPublisher, never()).publish(isA(UserCreatedEvent.class));
        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
        verify(eventPublisher, never()).publish(isA(GroupMembershipDeletedEvent.class));
        verifyGroupNotCreated();
        verifyGroupMembershipNotCreated();
        verify(internalDirectory, never()).removeUserFromGroup(Matchers.<String>any(), Matchers.<String>any());
    }

    @Test
    public void testUpdateLdapUserRenamesUserWhenCaseDiffers() throws Exception
    {
        final String lowercaseUsername = "user";
        final String uppercaseUsername = "UseR";

        final InternalUser lowercaseUser = InternalEntityHelper.createUser(DIRECTORY_ID, lowercaseUsername);
        final InternalUser uppercaseUser = InternalEntityHelper.createUser(DIRECTORY_ID, uppercaseUsername);

        configurationOf(internalDirectory).setImportGroups(false);

        when(ldapDirectory.findUserByName(lowercaseUsername)).thenReturn(uppercaseUser);
        when(internalDirectory.findUserByName(lowercaseUsername)).thenReturn(lowercaseUser);
        when(internalDirectory.updateUser(Matchers.<UserTemplate>any())).thenReturn(lowercaseUser);

        directory.addOrUpdateLdapUser(lowercaseUsername);

        verify(internalDirectory, times(1)).renameUser(lowercaseUsername, uppercaseUsername);
        verify(eventPublisher, never()).publish(isA(UserCreatedEvent.class));
        verify(internalDirectory, times(1)).updateUser(argThat(new ArgumentMatcher<UserTemplate>()
        {
            @Override
            public boolean matches(Object argument)
            {
                return uppercaseUsername.equals(((UserTemplate) argument).getName());
            }
        }));
        verify(eventPublisher, times(1)).publish(isA(AutoUserUpdatedEvent.class));
    }

    private void verifyUserCreated(User remotePrincipal) throws InvalidUserException, InvalidCredentialException, UserAlreadyExistsException, OperationFailedException
    {
        assertEquals(user, remotePrincipal);
        verify(internalDirectory, times(1)).addUser(new UserTemplate(user), null);
        verify(eventPublisher, times(1)).publish(isA(AutoUserCreatedEvent.class));
    }

    private void verifyGroupCreated(final String name) throws InvalidGroupException, OperationFailedException
    {
        final ArgumentCaptor<GroupTemplate> groupTemplate = ArgumentCaptor.forClass(GroupTemplate.class);
        verify(internalDirectory, times(1)).addGroup(groupTemplate.capture());
        assertEquals(new GroupTemplate(name, DIRECTORY_ID), groupTemplate.getValue());
        assertFalse(groupTemplate.getValue().isLocal());
        verify(eventPublisher, times(1)).publish(argThat(new ArgumentMatcher<AutoGroupCreatedEvent>()
        {

            @Override
            public boolean matches(final Object o)
            {
                if (o instanceof AutoGroupCreatedEvent)
                {
                    final AutoGroupCreatedEvent event = (AutoGroupCreatedEvent) o;
                    return event.getDirectory() == directoryAPI && event.getGroup().getName().equals(name)
                           && event.getSource() == directory;
                }
                else
                {
                    return false;
                }
            }

        }));
    }

    private void verifyGroupNotCreated() throws InvalidGroupException, OperationFailedException
    {
        verify(internalDirectory, never()).addGroup(any(GroupTemplate.class));
        verify(eventPublisher, never()).publish(isA(GroupCreatedEvent.class));
    }

    private void verifyGroupMembershipCreated(final String groupName)
        throws GroupNotFoundException, UserNotFoundException, ReadOnlyGroupException, OperationFailedException,
               MembershipAlreadyExistsException
    {
        verify(internalDirectory, times(1)).addUserToGroup(TEST_USER_NAME, groupName);
        verify(eventPublisher, times(1)).publish(argThat(new ArgumentMatcher<AutoGroupMembershipCreatedEvent>()
        {

            @Override
            public boolean matches(final Object o)
            {
                if (o instanceof AutoGroupMembershipCreatedEvent)
                {
                    final AutoGroupMembershipCreatedEvent event = (AutoGroupMembershipCreatedEvent) o;
                    return event.getDirectory() == directoryAPI && event.getEntityName().equals(TEST_USER_NAME) && event
                        .getGroupName()
                        .equals(groupName) && event.getMembershipType().equals(MembershipType.GROUP_USER)
                           && event.getSource() == directory;
                }
                else
                {
                    return false;
                }
            }

        }));
    }

    private void verifyGroupMembershipNotCreated()
        throws GroupNotFoundException, UserNotFoundException, ReadOnlyGroupException, OperationFailedException,
               MembershipAlreadyExistsException
    {
        verify(internalDirectory, never()).addUserToGroup(anyString(), anyString());
        verify(eventPublisher, never()).publish(isA(GroupMembershipCreatedEvent.class));
    }

    /**
     * A helper class that introduces a clear syntax to configure a RemoteDirectory mock
     */
    private static class RemoteDirectoryMockConfiguration
    {

        private final RemoteDirectory remoteDirectory;

        RemoteDirectoryMockConfiguration(RemoteDirectory remoteDirectory)
        {
            this.remoteDirectory = remoteDirectory;
        }

        RemoteDirectoryMockConfiguration setCreateUserOnAuth(boolean createUserOnAuth)
        {
            when(remoteDirectory.getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH))
                .thenReturn(Boolean.valueOf(createUserOnAuth).toString());
            return this;
        }

        RemoteDirectoryMockConfiguration setUpdateUserOnAuth(boolean updateUserOnAuth)
        {
            when(remoteDirectory.getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH))
                .thenReturn(Boolean.valueOf(updateUserOnAuth).toString());
            return this;
        }

        RemoteDirectoryMockConfiguration setImportGroups(boolean importGroups)
        {
            when(remoteDirectory.getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_KEY_IMPORT_GROUPS))
                .thenReturn(Boolean.valueOf(importGroups).toString());
            return this;
        }

        RemoteDirectoryMockConfiguration setSupportsNestedGroups(boolean supportsNestedGroups)
        {
            when(remoteDirectory.supportsNestedGroups()).thenReturn(supportsNestedGroups);
            return this;
        }

        RemoteDirectoryMockConfiguration doesNotContainUser(User user)
            throws UserNotFoundException, OperationFailedException
        {
            when(remoteDirectory.findUserByName(user.getName()))
                .thenThrow(new UserNotFoundException(user.getName()));
            return this;
        }

        RemoteDirectoryMockConfiguration containsUser(User user) throws UserNotFoundException, OperationFailedException
        {
            String userName = user.getName();  // mockito does not like if we inline this
            when(remoteDirectory.findUserByName(userName)).thenReturn(user);
            return this;
        }

        RemoteDirectoryMockConfiguration doesNotContainGroup(Group group)
            throws GroupNotFoundException, OperationFailedException
        {
            String groupName = group.getName();  // mockito does not like if we inline this
            when(remoteDirectory.findGroupByName(groupName))
                .thenThrow(new GroupNotFoundException(groupName));
            return this;
        }

        RemoteDirectoryMockConfiguration containsGroup(Group group)
            throws GroupNotFoundException, OperationFailedException
        {
            String groupName = group.getName(); // mockito does not like if we inline this
            when(remoteDirectory.findGroupByName(groupName)).thenReturn(group);
            return this;
        }

        RemoteDirectoryMockConfiguration authenticates(User user, PasswordCredential passwordCredential)
            throws UserNotFoundException, ExpiredCredentialException, InvalidAuthenticationException,
                   InactiveAccountException, OperationFailedException
        {
            String userName = user.getName(); // mockito does not like if we inline this
            when(remoteDirectory.authenticate(userName, passwordCredential)).thenReturn(user);
            return this;
        }

        RemoteDirectoryMockConfiguration doesNotAuthenticateUser(User user, PasswordCredential passwordCredential)
            throws UserNotFoundException, ExpiredCredentialException, InvalidAuthenticationException,
                   InactiveAccountException, OperationFailedException
        {
            String userName = user.getName(); // mockito does not like if we inline this
            when(remoteDirectory.authenticate(userName, passwordCredential))
                .thenThrow(InvalidAuthenticationException.newInstanceWithName(userName));
            return this;
        }


        RemoteDirectoryMockConfiguration userHasDirectParentGroups(InternalUser user, Group... groups)
            throws OperationFailedException
        {
            when(remoteDirectory.searchGroupRelationships(getQueryForDirectParentGroupsOfUser(user, Group.class)))
                .thenReturn(Arrays.<Group>asList(groups));
            return this;
        }

        RemoteDirectoryMockConfiguration userHasDirectParentGroupsAsString(InternalUser user, Group... groups)
            throws OperationFailedException
        {
            List<String> groupNames = ImmutableList.copyOf(namesOf(Arrays.<Group>asList(groups)));
            when(remoteDirectory.searchGroupRelationships(getQueryForDirectParentGroupsOfUser(user, String.class)))
                .thenReturn(groupNames);
            return this;
        }

        RemoteDirectoryMockConfiguration groupHasDirectParentGroupsAsString(Group group, Group... parentGroups)
            throws OperationFailedException
        {
            List<String> parentGroupNames = ImmutableList.copyOf(namesOf(Arrays.<Group>asList(parentGroups)));
            when(remoteDirectory.searchGroupRelationships(getQueryForDirectParentGroupsOfGroup(group, String.class)))
                .thenReturn(parentGroupNames);
            return this;
        }

        static <T> MembershipQuery<T> getQueryForDirectParentGroupsOfUser(User user, Class<T> clazz)
        {
            return QueryBuilder.queryFor(clazz, EntityDescriptor.group())
                .parentsOf(EntityDescriptor.user())
                .withName(user.getName())
                .returningAtMost(EntityQuery.ALL_RESULTS);
        }

        static <T> MembershipQuery<T> getQueryForDirectParentGroupsOfGroup(Group group, Class<T> clazz)
        {
            return QueryBuilder.queryFor(clazz, EntityDescriptor.group())
                .parentsOf(EntityDescriptor.group())
                .withName(group.getName())
                .returningAtMost(EntityQuery.ALL_RESULTS);
        }

    }

    private static RemoteDirectoryMockConfiguration configurationOf(RemoteDirectory remoteDirectory)
    {
        return new RemoteDirectoryMockConfiguration(remoteDirectory);
    }

}
