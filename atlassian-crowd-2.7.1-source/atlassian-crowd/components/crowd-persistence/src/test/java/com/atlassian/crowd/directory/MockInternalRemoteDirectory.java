package com.atlassian.crowd.directory;

import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.InternalDirectoryGroup;
import com.atlassian.crowd.model.user.DelegatingTimestampedUser;
import com.atlassian.crowd.model.user.TimestampedUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.util.BatchResult;

import javax.annotation.Nonnull;
import java.util.Set;

public class MockInternalRemoteDirectory extends MockRemoteDirectory implements InternalRemoteDirectory
{
    @Override
    public Group addLocalGroup(GroupTemplate group) throws InvalidGroupException, OperationFailedException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public BatchResult<User> addAllUsers(Set<UserTemplateWithCredentialAndAttributes> users)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public BatchResult<Group> addAllGroups(Set<GroupTemplate> groups)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public BatchResult<String> addAllUsersToGroup(Set<String> userNames, String groupName) throws GroupNotFoundException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public BatchResult<String> removeAllUsers(Set<String> usernames)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public BatchResult<String> removeAllGroups(Set<String> groupNames)
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public boolean isLocalUserStatusEnabled()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public User forceRenameUser(@Nonnull User oldUser, @Nonnull String newName) throws UserNotFoundException
    {

        final User existingUser;
        // Check if this is a trivial rename of case only (eg 'bob' -> 'Bob')
        if (IdentifierUtils.equalsInLowerCase(oldUser.getName(), newName))
        {
            existingUser = null;
        }
        else
        {
            // Check if new name is already in use
            existingUser = findUserByNameOrNull(newName);
        }

        if (existingUser != null)
        {
            // New name is already taken - first we move the existing user to a vacant username
            try
            {
                rename(existingUser, newName + "#1");
            }
            catch (UserNotFoundException ex)
            {
                // Strange - we just found this user a few lines ago.
                // Anyway, if there is no user in the way, we can just try the desired rename
            }
        }
        return rename(oldUser, newName);
    }

    public User findUserByNameOrNull(String name)
    {
        try
        {
            return findUserByName(name);
        }
        catch (UserNotFoundException e)
        {
            return null;
        }
    }

    // Methods with stronger return types

    @Override
    public InternalDirectoryGroup findGroupByName(String name) throws GroupNotFoundException
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    @Override
    public TimestampedUser findUserByName(String name) throws UserNotFoundException
    {
        return new DelegatingTimestampedUser(super.findUserByName(name));
    }

    @Override
    public TimestampedUser findUserByExternalId(String externalId) throws UserNotFoundException
    {
        return new DelegatingTimestampedUser(super.findUserByExternalId(externalId));
    }
}
