package com.atlassian.crowd.model.user;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import org.apache.commons.lang3.StringUtils;
import org.hamcrest.collection.IsEmptyIterable;
import org.junit.Test;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class InternalUserTest
{
    private static final String SHORT_STRING = StringUtils.repeat("X", 255);
    private static final String LONG_STRING = StringUtils.repeat("X", 300);
    private static final String TRUNCATED_STRING = StringUtils.repeat("X", 252).concat("...");

    private static final Directory DIRECTORY = new DirectoryImpl(new InternalEntityTemplate(-1L, "directory", true, null, null));
    private static final PasswordCredential CREDENTIAL = new PasswordCredential("", true);

    @Test
    public void testInternalUser_ShortFields()
    {
        final UserTemplate userTemplate = new UserTemplate("user", SHORT_STRING, SHORT_STRING, SHORT_STRING);
        userTemplate.setEmailAddress(SHORT_STRING);
        final InternalUser user = new InternalUser(userTemplate, DIRECTORY, CREDENTIAL);

        assertEquals(SHORT_STRING, user.getFirstName());
        assertEquals(SHORT_STRING, user.getLastName());
        assertEquals(SHORT_STRING, user.getDisplayName());
        assertEquals(SHORT_STRING, user.getEmailAddress());

        assertEquals(toLowerCase(SHORT_STRING), user.getLowerFirstName());
        assertEquals(toLowerCase(SHORT_STRING), user.getLowerLastName());
        assertEquals(toLowerCase(SHORT_STRING), user.getLowerDisplayName());
        assertEquals(toLowerCase(SHORT_STRING), user.getLowerEmailAddress());
    }

    @Test
    public void testInternalUser_LongFields()
    {
        final UserTemplate userTemplate = new UserTemplate("user", LONG_STRING, LONG_STRING, LONG_STRING);
        userTemplate.setEmailAddress(LONG_STRING);
        final InternalUser user = new InternalUser(userTemplate, DIRECTORY, CREDENTIAL);

        assertEquals(TRUNCATED_STRING, user.getFirstName());
        assertEquals(TRUNCATED_STRING, user.getLastName());
        assertEquals(TRUNCATED_STRING, user.getDisplayName());
        assertEquals(TRUNCATED_STRING, user.getEmailAddress());

        assertEquals(toLowerCase(TRUNCATED_STRING), user.getLowerFirstName());
        assertEquals(toLowerCase(TRUNCATED_STRING), user.getLowerLastName());
        assertEquals(toLowerCase(TRUNCATED_STRING), user.getLowerDisplayName());
        assertEquals(toLowerCase(TRUNCATED_STRING), user.getLowerEmailAddress());
    }

    @Test(expected=IllegalArgumentException.class)
    public void testInternalUser_LongName()
    {
        final UserTemplate userTemplate = new UserTemplate(LONG_STRING);
        new InternalUser(userTemplate, DIRECTORY, CREDENTIAL);
    }

    @Test
    public void updateCredentialDoesNotAddCredentalToCredentialHistoryWhenHistorySizeIsZero()
    {
        UserTemplate userTemplate = new UserTemplate("user");
        InternalUser user = new InternalUser(userTemplate, DIRECTORY, CREDENTIAL);

        PasswordCredential newCredential = PasswordCredential.encrypted("secret");
        user.updateCredentialTo(newCredential, 0);

        assertThat(user.getCredentialHistory(), IsEmptyIterable.emptyIterable());
        assertThat(user.getCredentialRecords(), IsEmptyIterable.emptyIterable());
    }

    @Test
    public void updateCredentialAddsCredentialToCredentialRecord()
    {
        UserTemplate userTemplate = new UserTemplate("user");
        InternalUser user = new InternalUser(userTemplate, DIRECTORY, CREDENTIAL);

        user.updateCredentialTo(PasswordCredential.encrypted("secret1"), 2);

        user.updateCredentialTo(PasswordCredential.encrypted("secret2"), 2);

        assertThat(user.getCredentialHistory(), hasItems(PasswordCredential.encrypted("secret1"),
                                                         PasswordCredential.encrypted("secret2")));
    }

    @Test
    public void updateCredentialsRemovesOldCredentialsFromCredentialHistory()
    {
        UserTemplate userTemplate = new UserTemplate("user");
        InternalUser user = new InternalUser(userTemplate, DIRECTORY, CREDENTIAL);

        user.updateCredentialTo(PasswordCredential.encrypted("secret1"), 2);

        user.updateCredentialTo(PasswordCredential.encrypted("secret2"), 2);

        user.updateCredentialTo(PasswordCredential.encrypted("secret3"), 2);

        assertThat(user.getCredentialHistory(), hasItems(PasswordCredential.encrypted("secret2"),
                                                         PasswordCredential.encrypted("secret3")));

        assertThat(user.getCredentialHistory(), not(hasItem(PasswordCredential.encrypted("secret1"))));
    }

    @Test
    public void updateDetailsFromAcceptsChanges()
    {
        UserTemplate userTemplate = new UserTemplate("user");
        userTemplate.setEmailAddress("original-email");
        userTemplate.setFirstName("Original First");
        userTemplate.setLastName("Original Last");
        userTemplate.setDisplayName("Original Display");

        InternalUser user = new InternalUser(userTemplate, DIRECTORY, CREDENTIAL);

        userTemplate.setEmailAddress("modified-email");
        userTemplate.setFirstName("Modified First");
        userTemplate.setLastName("Modified Last");
        userTemplate.setDisplayName("Modified Display");

        InternalUser user2 = new InternalUser(userTemplate, DIRECTORY, CREDENTIAL);

        assertEquals("original-email", user.getEmailAddress());
        assertEquals("Original First", user.getFirstName());
        assertEquals("Original Last", user.getLastName());
        assertEquals("Original Display", user.getDisplayName());

        user.updateDetailsFrom(user2);

        assertEquals("modified-email", user.getEmailAddress());
        assertEquals("Modified First", user.getFirstName());
        assertEquals("Modified Last", user.getLastName());
        assertEquals("Modified Display", user.getDisplayName());
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateDetailsFromRejectsUserWithDifferentUsername()
    {
        UserTemplate userTemplate = new UserTemplate("user");
        InternalUser user = new InternalUser(userTemplate, DIRECTORY, CREDENTIAL);

        InternalUser userWithDifferentName = new InternalUser(new UserTemplate("another-user"), DIRECTORY, CREDENTIAL);

        user.updateDetailsFrom(userWithDifferentName);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateDetailsFromRejectsUserWithDifferentDirectory()
    {
        UserTemplate userTemplate = new UserTemplate("user");
        InternalUser user = new InternalUser(userTemplate, DIRECTORY, CREDENTIAL);

        DirectoryImpl d2 = new DirectoryImpl(new InternalEntityTemplate(2L, "directory", true, null, null));
        InternalUser userWithDifferentName = new InternalUser(userTemplate, d2, CREDENTIAL);

        user.updateDetailsFrom(userWithDifferentName);
    }

    @Test
    public void updateDetailsFromAcceptsChangesFromCaseInsensitiveUsernameMatch()
    {
        UserTemplate userTemplate = new UserTemplate("user");
        userTemplate.setEmailAddress("original-email");

        InternalUser user = new InternalUser(userTemplate, DIRECTORY, CREDENTIAL);

        assertEquals("user", user.getName());
        assertEquals("original-email", user.getEmailAddress());

        UserTemplate userTemplateMixedCase = new UserTemplate("User");
        userTemplateMixedCase.setEmailAddress("modified-email");

        InternalUser user2 = new InternalUser(userTemplateMixedCase, DIRECTORY, CREDENTIAL);

        assertEquals("User", user2.getName());
        assertEquals("modified-email", user2.getEmailAddress());

        user.updateDetailsFrom(user2);

        assertEquals("user", user.getName());
        assertEquals("modified-email", user.getEmailAddress());
    }
}
