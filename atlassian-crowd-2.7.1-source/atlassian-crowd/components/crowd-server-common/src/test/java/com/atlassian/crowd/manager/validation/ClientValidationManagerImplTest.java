package com.atlassian.crowd.manager.validation;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.manager.cache.NotInCacheException;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.proxy.TrustedProxyManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.InetAddressCacheUtil;

import com.google.common.collect.Sets;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyBoolean;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 */
public class ClientValidationManagerImplTest
{
    private static final String TEST_APPLICATION_NAME = "app";
    private static final String ALLOWED_ADDRESS = "1.2.3.4";
    private static final String UNTRUSTED_ADDRESS = "0.0.0.0";
    private static final String X_FORWARDED_FOR = "X-Forwarded-For";

    private InetAddressCacheUtil cacheUtil;
    private PropertyManager propertyManager;
    private TrustedProxyManager trustedProxyManager;
    private I18nHelper i18nHelper;

    private Application application;
    private HttpServletRequest request;

    private ClientValidationManager clientValidationManager;

    @Before
    public void setUp()
    {
        cacheUtil = mock(InetAddressCacheUtil.class);
        propertyManager = mock(PropertyManager.class);
        trustedProxyManager = mock(TrustedProxyManager.class);
        i18nHelper = mock(I18nHelper.class);

        application = mock(Application.class);
        request = mock(HttpServletRequest.class);

        clientValidationManager = new ClientValidationManagerImpl(cacheUtil, propertyManager, trustedProxyManager, i18nHelper);
    }

    @Test
    public void testValidate_FromCache() throws ClientValidationException, NotInCacheException, UnknownHostException
    {
        RemoteAddress address = new RemoteAddress(ALLOWED_ADDRESS);
        InetAddress inetAddress = InetAddress.getByName(ALLOWED_ADDRESS);
        when(application.getName()).thenReturn(TEST_APPLICATION_NAME);
        when(application.isActive()).thenReturn(true);
        when(application.getRemoteAddresses()).thenReturn(Sets.newHashSet(address));

        when(cacheUtil.getPermitted(application, inetAddress)).thenReturn(Boolean.TRUE);

        when(trustedProxyManager.isTrusted(anyString())).thenReturn(true);

        when(request.getHeader(X_FORWARDED_FOR)).thenReturn(ALLOWED_ADDRESS);
        when(request.getRemoteAddr()).thenReturn(ALLOWED_ADDRESS);
        when(request.getRemoteHost()).thenReturn(ALLOWED_ADDRESS);

        when(propertyManager.isCacheEnabled()).thenReturn(true);

        clientValidationManager.validate(application, request);
        verify(cacheUtil, never()).setPermitted(Mockito.<Application>any(), Mockito.<InetAddress>any(), anyBoolean());
    }

    @Test
    public void testValidate_NotInCache() throws ClientValidationException, NotInCacheException, UnknownHostException
    {
        RemoteAddress address = new RemoteAddress(ALLOWED_ADDRESS);
        InetAddress inetAddress = InetAddress.getByName(ALLOWED_ADDRESS);
        when(application.getName()).thenReturn(TEST_APPLICATION_NAME);
        when(application.isActive()).thenReturn(true);
        when(application.getRemoteAddresses()).thenReturn(Sets.newHashSet(address));

        when(cacheUtil.getPermitted(application, inetAddress)).thenThrow(new NotInCacheException());

        when(trustedProxyManager.isTrusted(anyString())).thenReturn(true);

        when(request.getHeader(X_FORWARDED_FOR)).thenReturn(ALLOWED_ADDRESS);
        when(request.getRemoteAddr()).thenReturn(ALLOWED_ADDRESS);
        when(request.getRemoteHost()).thenReturn(ALLOWED_ADDRESS);

        when(propertyManager.isCacheEnabled()).thenReturn(true);

        clientValidationManager.validate(application, request);
        verify(cacheUtil).setPermitted(application, inetAddress, true);
    }

    @Test
    public void testValidate_CacheDisabled() throws ClientValidationException, NotInCacheException
    {
        when(application.getName()).thenReturn(TEST_APPLICATION_NAME);
        when(application.isActive()).thenReturn(true);
        when(application.getRemoteAddresses()).thenReturn(Sets.newHashSet(new RemoteAddress(ALLOWED_ADDRESS)));

        when(trustedProxyManager.isTrusted(anyString())).thenReturn(true);

        when(request.getHeader(X_FORWARDED_FOR)).thenReturn(ALLOWED_ADDRESS);
        when(request.getRemoteAddr()).thenReturn(ALLOWED_ADDRESS);
        when(request.getRemoteHost()).thenReturn(ALLOWED_ADDRESS);

        when(propertyManager.isCacheEnabled()).thenReturn(false);

        clientValidationManager.validate(application, request);
        verify(cacheUtil, never()).getPermitted(Mockito.<Application>any(), Mockito.<InetAddress>any());
        verify(cacheUtil, never()).setPermitted(Mockito.<Application>any(), Mockito.<InetAddress>any(), anyBoolean());
    }

    /**
     * Tests that when an application is not active, a ClientValidationException is thrown.
     */
    @Test
    public void testValidate_ApplicationNotActive() throws ClientValidationException, NotInCacheException
    {
        when(application.getName()).thenReturn(TEST_APPLICATION_NAME);
        when(application.isActive()).thenReturn(false);

        try
        {
            clientValidationManager.validate(application, request);
            fail("Application is not active.  Should have thrown a ClientValidationException.");
        }
        catch (ClientValidationException e)
        {
            // correct behaviour
        }
    }

    /**
     * Test that an untrusted client address results in the address being added to the cache and a ClientValidationException being thrown.
     */
    @Test
    public void testValidate_UntrustedClient() throws ClientValidationException, NotInCacheException, UnknownHostException
    {
        RemoteAddress clientAddress = new RemoteAddress(UNTRUSTED_ADDRESS);
        InetAddress inetAddress = InetAddress.getByName(UNTRUSTED_ADDRESS);
        when(application.getName()).thenReturn(TEST_APPLICATION_NAME);
        when(application.isActive()).thenReturn(true);
        when(application.getRemoteAddresses()).thenReturn(Sets.newHashSet(new RemoteAddress(ALLOWED_ADDRESS)));

        when(cacheUtil.getPermitted(application, inetAddress)).thenThrow(new NotInCacheException());

        when(trustedProxyManager.isTrusted(anyString())).thenReturn(true);

        when(request.getHeader(X_FORWARDED_FOR)).thenReturn(UNTRUSTED_ADDRESS);
        when(request.getRemoteAddr()).thenReturn(UNTRUSTED_ADDRESS);
        when(request.getRemoteHost()).thenReturn(UNTRUSTED_ADDRESS);

        when(propertyManager.isCacheEnabled()).thenReturn(true);

        try
        {
            clientValidationManager.validate(application, request);
            fail("Client address is not a trusted address.  Should have thrown a ClientValidationException.");
        }
        catch (ClientValidationException e)
        {
            // correct behaviour
        }
        verify(cacheUtil).getPermitted(application, inetAddress);
        verify(cacheUtil).setPermitted(application, inetAddress, false);
    }

    /**
     * Test that an untrusted client address results in a ClientValidationException being thrown.
     */
    @Test
    public void testValidate_UntrustedClientFromCache() throws ClientValidationException, NotInCacheException, UnknownHostException
    {
        RemoteAddress address = new RemoteAddress(ALLOWED_ADDRESS);
        InetAddress inetAddress = InetAddress.getByName(ALLOWED_ADDRESS);
        when(application.getName()).thenReturn(TEST_APPLICATION_NAME);
        when(application.isActive()).thenReturn(true);
        when(application.getRemoteAddresses()).thenReturn(Sets.newHashSet(address));

        when(cacheUtil.getPermitted(application, inetAddress)).thenReturn(false);

        when(trustedProxyManager.isTrusted(anyString())).thenReturn(true);

        when(request.getHeader(X_FORWARDED_FOR)).thenReturn(UNTRUSTED_ADDRESS);
        when(request.getRemoteAddr()).thenReturn(UNTRUSTED_ADDRESS);
        when(request.getRemoteHost()).thenReturn(UNTRUSTED_ADDRESS);

        when(propertyManager.isCacheEnabled()).thenReturn(true);

        try
        {
            clientValidationManager.validate(application, request);
            fail("Client address is not a trusted address.  Should have thrown a ClientValidationException.");
        }
        catch (ClientValidationException e)
        {
            // correct behaviour
        }
        verify(cacheUtil, never()).setPermitted(Mockito.<Application>any(), Mockito.<InetAddress>any(), anyBoolean());
    }

    @Test
    public void exceptionMessageIncludesClientAddressAndApplicationName() throws Exception
    {
        InetAddress inetAddress = InetAddress.getByName(UNTRUSTED_ADDRESS);
        when(application.getName()).thenReturn(TEST_APPLICATION_NAME);
        when(application.isActive()).thenReturn(true);
        when(request.getRemoteAddr()).thenReturn(UNTRUSTED_ADDRESS);

        when(cacheUtil.getPermitted(application, inetAddress)).thenReturn(false);
        when(propertyManager.isCacheEnabled()).thenReturn(true);

        when(i18nHelper.getText("client.forbidden.exception", UNTRUSTED_ADDRESS, TEST_APPLICATION_NAME)).thenReturn("XXXX");

        try
        {
            clientValidationManager.validate(application, request);
            fail("Client address is not a trusted address.  Should have thrown a ClientValidationException.");
        }
        catch (ClientValidationException e)
        {
            assertEquals("XXXX", e.getMessage());
        }

        verify(i18nHelper).getText("client.forbidden.exception", UNTRUSTED_ADDRESS, TEST_APPLICATION_NAME);
    }
}
