package com.atlassian.crowd.util;

import java.util.Locale;
import java.util.ResourceBundle;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

/**
 * Simple implementation of {@link com.atlassian.crowd.util.ResourceBundleProvider} that looks in fixed set of locations.
 *
 * @since v2.7
 */
public class StaticResourceBundleProvider implements ResourceBundleProvider
{
    private final Iterable<String> locations;
    private final Locale locale;
    private final Function<String, ResourceBundle> toBundle;

    public StaticResourceBundleProvider(I18nHelperConfiguration configuration)
    {
        this.locations = ImmutableList.copyOf(configuration.getBundleLocations());
        this.locale = configuration.getLocale();
        toBundle = new Function<String, ResourceBundle>()
        {
            @Override
            public ResourceBundle apply(String bundleLocation)
            {
                return ResourceBundle.getBundle(bundleLocation, locale);
            }
        };
    }

    @Override
    public Iterable<ResourceBundle> getResourceBundles()
    {
        return Iterables.transform(locations, toBundle);
    }
}
