package com.atlassian.crowd.directory;

import com.atlassian.crowd.directory.hybrid.LocalGroupHandler;
import com.atlassian.crowd.directory.ldap.cache.*;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.directory.SynchronisationMode;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.model.user.TimestampedUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.TimedOperation;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * A {@link RemoteDirectory} that provides LDAP and Crowd integration plus local storage in an internal directory
 * for LDAP user and group attributes, and local groups for LDAP and Crowd users with local caching of remote data.
 * The implementation delegates to an Remote directory for the "source of truth" and an internal directory for caching
 * and some special local storage.
 * <p/>
 * All the attributes for the directory itself (e.g. base DN, other configuration options) are stored on
 * the directory instance.
 * <p/>
 * Terminology used in this class:
 * <dl>
 * <dt>Remote user</dt><dd>A user stored in the LDAP or Remote Crowd directory</dd>
 * <dt>Remote group</dt>
 * <dd>A group stored in the LDAP or Remote Crowd directory.</dd>
 * <dt>Local group</dt>
 * <dd>A group stored in the internal directory. There must
 * not be an Remote group with the same name for a local group to exist.
 * </dl>
 */
public class DbCachingRemoteDirectory implements RemoteDirectory, SynchronisableDirectory
{
    private static final Logger log = LoggerFactory.getLogger(DbCachingRemoteDirectory.class);
    public static final String INTERNAL_USER_PASSWORD = "nopass";

    // delegations
    private final RemoteDirectory remoteDirectory;

    // helpers
    private final LocalGroupHandler localGroupHandler;
    private final InternalRemoteDirectory internalDirectory;
    private final DirectoryCacheFactory directoryCacheFactory;
    private final CacheRefresher cacheRefresher;

    public DbCachingRemoteDirectory(RemoteDirectory remoteDirectory, InternalRemoteDirectory internalDirectory, DirectoryCacheFactory directoryCacheFactory)
    {
        this(remoteDirectory, internalDirectory, directoryCacheFactory, new LocalGroupHandler(internalDirectory),
             buildCacheRefresher(remoteDirectory));
    }

    DbCachingRemoteDirectory(RemoteDirectory remoteDirectory,
            InternalRemoteDirectory internalDirectory,
            DirectoryCacheFactory directoryCacheFactory,
            CacheRefresher cacheRefresher)
    {
        this(remoteDirectory, internalDirectory, directoryCacheFactory, new LocalGroupHandler(internalDirectory),
                cacheRefresher);
    }

    private DbCachingRemoteDirectory(RemoteDirectory remoteDirectory,
                                    InternalRemoteDirectory internalDirectory,
                                    DirectoryCacheFactory directoryCacheFactory,
                                    LocalGroupHandler localGroupHandler,
                                    CacheRefresher cacheRefresher)
    {
        this.remoteDirectory = remoteDirectory;
        this.internalDirectory = internalDirectory;
        this.directoryCacheFactory = directoryCacheFactory;
        this.localGroupHandler = localGroupHandler;
        this.cacheRefresher = cacheRefresher;

        log.debug("DBCached directory created for directory [ " + remoteDirectory.getDirectoryId() + " ]");
    }

    private static CacheRefresher buildCacheRefresher(RemoteDirectory remoteDirectory)
    {
        // If we are connected to AD, then we get a special CacheRefresher that uses the USNChanged value.
        if (remoteDirectory instanceof MicrosoftActiveDirectory)
            return new UsnChangedCacheRefresher((MicrosoftActiveDirectory) remoteDirectory);
        else if (remoteDirectory instanceof RemoteCrowdDirectory)
            return new EventTokenChangedCacheRefresher((RemoteCrowdDirectory) remoteDirectory);
        else
            return new RemoteDirectoryCacheRefresher(remoteDirectory);
    }

    @Override
    public long getDirectoryId()
    {
        return remoteDirectory.getDirectoryId();
    }

    @Override
    public void setDirectoryId(long directoryId)
    {
        throw new UnsupportedOperationException("You cannot mutate the directoryID of " + this.getClass().getName());
    }

    @Override
    public String getDescriptiveName()
    {
        return remoteDirectory.getDescriptiveName();
    }

    @Override
    public void setAttributes(Map<String, String> attributes)
    {
        throw new UnsupportedOperationException("You cannot mutate the attributes of " + this.getClass().getName());
    }

    @Override
    public User findUserByName(String name) throws UserNotFoundException, OperationFailedException
    {
        return internalDirectory.findUserByName(name);
    }

    @Override
    public UserWithAttributes findUserWithAttributesByName(String name) throws UserNotFoundException, OperationFailedException
    {
        return internalDirectory.findUserWithAttributesByName(name);
    }

    @Override
    public User findUserByExternalId(String externalId) throws UserNotFoundException, OperationFailedException
    {
        return internalDirectory.findUserByExternalId(externalId);
    }

    @Override
    public User authenticate(String name, PasswordCredential credential) throws UserNotFoundException, InactiveAccountException, InvalidAuthenticationException, ExpiredCredentialException, OperationFailedException
    {
        if (remoteDirectory instanceof RemoteCrowdDirectory)
        {
            // No need to do special processing when we know the remote directory will do it for us.
            return authenticateAndUpdateInternalUser(name, credential);
        } else
        {
            return performAuthenticationAndUpdateAttributes(name, credential);
        }
    }

    @Override
    public Iterable<Membership> getMemberships() throws OperationFailedException
    {
        return internalDirectory.getMemberships();
    }

    /**
     * Performs authentication and updates user authentication attributes while not allowing inactive users to log in.
     *
     * @param name       name of the user
     * @param credential credential of the user
     * @return authenticated user
     * @throws UserNotFoundException          if the user does not exist
     * @throws ExpiredCredentialException     if the password has expired and the user is required to change their password
     * @throws InactiveAccountException       if the user account is not active
     * @throws InvalidAuthenticationException if the user name/password combination is invalid
     * @throws OperationFailedException       if the operation failed for any other reason
     */
    private User performAuthenticationAndUpdateAttributes(String name, PasswordCredential credential)
            throws UserNotFoundException, ExpiredCredentialException, InactiveAccountException, OperationFailedException, InvalidAuthenticationException
    {
        final Map<String, Set<String>> attributesToUpdate = new HashMap<String, Set<String>>();
        try
        {
            final User authenticatedUser = authenticateAndUpdateInternalUser(name, credential);

            // If remote directory does not handle inactive accounts, do it manually using the internal directory.
            if (!remoteDirectory.supportsInactiveAccounts())
            {
                User internalUser = internalDirectory.findUserByName(name);
                if (!internalUser.isActive())
                {
                    throw new InactiveAccountException(name);
                }
            }

            // authentication worked fine, set the invalid attempts to 0
            attributesToUpdate.put(UserConstants.INVALID_PASSWORD_ATTEMPTS, Collections.singleton(Long.toString(0L)));

            // update the last password authentication
            attributesToUpdate.put(UserConstants.LAST_AUTHENTICATED, Collections.singleton(Long.toString(System.currentTimeMillis())));

            storeUserAttributes(name, attributesToUpdate);

            return authenticatedUser;
        } catch (InvalidAuthenticationException e)
        {
            final UserWithAttributes user = findUserWithAttributesByName(name);
            long currentInvalidAttempts = NumberUtils.toLong(user.getValue(UserConstants.INVALID_PASSWORD_ATTEMPTS), 0L);

            // The user has entered incorrect password details
            // increment the invalid password attempts
            currentInvalidAttempts++;

            // set this on the principal object
            attributesToUpdate.put(UserConstants.INVALID_PASSWORD_ATTEMPTS, Collections.singleton(Long.toString(currentInvalidAttempts)));

            storeUserAttributes(name, attributesToUpdate);

            throw e;
        }
    }

    /**
     * Authenticates user and ensures that internal directory contains the
     * authenticated user.
     * <p/>
     * Updates user data (including groups membership) if the user exists in the internal directory. Renames user if necessary.
     * If user does not exists tries to add the user and the user's memberships in the internal
     * directory.
     * <p/>
     * User might not exist in the internal directory yet because the user was
     * added after the latest synchronisation. This happens for example when
     * the remote directory uses delegated authentication, and creates users on
     * successful authentication.
     *
     * @param name       of the user
     * @param credential credential of the user
     * @return The populated user if the authentication is valid.
     * @throws UserNotFoundException          if the user does not exist
     * @throws ExpiredCredentialException     if the password has expired and the user is required to change their password
     * @throws InactiveAccountException       if the user account is not active
     * @throws InvalidAuthenticationException if the user name/password combination is invalid
     * @throws OperationFailedException       if the operation failed for any other reason
     */
    @VisibleForTesting
    protected User authenticateAndUpdateInternalUser(String name, PasswordCredential credential) throws UserNotFoundException, InactiveAccountException, InvalidAuthenticationException, ExpiredCredentialException, OperationFailedException
    {
        final User remoteUser = remoteDirectory.authenticate(name, credential);
        User internalUser = findLocalUserByExternalIdOrName(remoteUser);
        try
        {
            if (internalUser == null)
            {
                try
                {
                    addInternalUser(remoteUser);
                }
                catch (UserAlreadyExistsException ex)
                {
                    throw new ConcurrentModificationException("Error while trying to add a new user '" + remoteUser.getName() + "' during login.");
                }
            }
            else
            {
                // Check if we require a user rename
                if (!internalUser.getName().equals(remoteUser.getName()))
                {
                    internalUser = internalDirectory.forceRenameUser(internalUser, remoteUser.getName());
                }
                updateUserAndSetActiveFlag(remoteUser, internalUser);
            }
        }
        catch (InvalidUserException ex)
        {
            throw new OperationFailedException(ex);
        }

        updateGroupsMembershipOnLogin(remoteUser);

        return remoteUser;
    }

    private TimestampedUser findLocalUserByExternalIdOrName(User remoteUser)
    {
        TimestampedUser user = findLocalUserByExternalId(remoteUser.getExternalId());
        if (user != null)
        {
            return user;
        }
        try
        {
            return internalDirectory.findUserByName(remoteUser.getName());
        }
        catch (UserNotFoundException e)
        {
            return null;
        }
    }

    private TimestampedUser findLocalUserByExternalId(String externalId)
    {
        try
        {
            if (externalId != null && !externalId.isEmpty())
            {
                return internalDirectory.findUserByExternalId(externalId);
            }
            else
            {
                return null;
            }
        }
        catch (UserNotFoundException e)
        {
            return null;
        }
    }

    @VisibleForTesting
    protected User updateUserAndSetActiveFlag(User remoteUser, User internalUser)
            throws UserNotFoundException, InvalidUserException, OperationFailedException
    {
        preventExternalIdDuplication(remoteUser, internalUser);

        final UserTemplate userTemplate = new UserTemplate(remoteUser);
        if (!remoteDirectory.supportsInactiveAccounts() || internalDirectory.isLocalUserStatusEnabled())
        {
            userTemplate.setActive(internalUser.isActive());
        }
        return internalDirectory.updateUser(userTemplate);
    }

    @VisibleForTesting
    protected void updateGroupsMembershipOnLogin(final User user) throws OperationFailedException, UserNotFoundException
    {
        final MembershipQuery<String> query = QueryBuilder
                .queryFor(String.class, EntityDescriptor.group())
                .parentsOf(EntityDescriptor.user())
                .withName(user.getName())
                .returningAtMost(EntityQuery.ALL_RESULTS);

        final Set<String> userRemoteGroupNames = ImmutableSet.copyOf(remoteDirectory.searchGroupRelationships(query));
        final Set<String> userLocalGroupNames = ImmutableSet.copyOf(internalDirectory.searchGroupRelationships(query));

        final Set<String> groupsToAddUser = Sets.difference(userRemoteGroupNames, userLocalGroupNames);
        final Set<String> groupsToRemoveUser = Sets.difference(userLocalGroupNames, userRemoteGroupNames);

        for (String groupName : groupsToAddUser)
        {
            try
            {
                internalDirectory.addUserToGroup(user.getName(), groupName);
            }
            catch (GroupNotFoundException e1)
            {
                // Group does not exist yet in the internal directory. This will get fixed with the next sync.
            }
            catch (ReadOnlyGroupException e1)
            {
                // Internal directory should never throw ReadOnlyGroupException
                throw new RuntimeException("Failed to add user from internal directory as group " + groupName + " is read only ", e1);
            }
            catch (MembershipAlreadyExistsException e1)
            {
                if (log.isDebugEnabled())
                {
                    log.debug("User " + user.getName() + " is already member of the group " + groupName);
                }
            }
        }

        // We remove the user from groups *only if* the remote server returns a non-empty list of groups.
        // This is because some LDAP configurations are not well supported by Crowd and can produce different
        // results when querying all-users-of-a-group (as part of the synchronisation) and when querying
        // all-groups-of-user (which is the query that we do in this method). In particular, this happens
        // when the LDAP directory does not publish the 'member' attribute on the group entity,
        // only the 'memberOf' attribute on the user entity.
        // This extra condition makes sure we do not remove any group if the LDAP server fails to fetch
        // all-groups-of-this-user. However, it also means that even when the LDAP server is correctly configured,
        // sometimes we don't remove groups on authentication, but we consider this is a minor problem that will
        // correct itself on the next sync. See CWD-3696 for details.
        if (!groupsToRemoveUser.isEmpty() && !userRemoteGroupNames.isEmpty())
        {
            final ImmutableSet<String> localGroups = findAllLocalGroups();
            for (String groupName : groupsToRemoveUser)
            {
                try
                {
                    if (!localGroups.contains(groupName))
                    {
                        internalDirectory.removeUserFromGroup(user.getName(), groupName);
                    }
                }
                catch (GroupNotFoundException e1)
                {
                    // Group does not exist yet in the internal directory. This will get fixed with the next sync.
                }
                catch (ReadOnlyGroupException e1)
                {
                    // Internal directory should never throw ReadOnlyGroupException
                    throw new RuntimeException("Failed to remove user from internal directory as group " + groupName + " is read only ", e1);
                }
                catch (MembershipNotFoundException e)
                {
                    log.debug("User " + user.getName() + " is no longer member of the group " + groupName);
                }
            }
        }
    }

    private void preventExternalIdDuplication(final User remoteUser, final User internalUser)
            throws OperationFailedException, InvalidUserException
    {
        if (StringUtils.isBlank(remoteUser.getExternalId())
                || remoteUser.getExternalId().equals(internalUser.getExternalId()))
        {
            return;
        }

        try
        {
            final TimestampedUser internalUserByExternalId = internalDirectory.findUserByExternalId(remoteUser.getExternalId());
            if (internalUserByExternalId != null)
            {
                removeExternalId(internalUserByExternalId);
                log.warn("Possible user unique id duplication, removing unique id: " + internalUser.getExternalId() + " for user " + internalUser.getName());
            }
        }
        catch (UserNotFoundException unf)
        {
            //one of the user with the same external id, does not exist any more
        }
    }

    private void removeExternalId(final User user)
            throws UserNotFoundException, InvalidUserException, OperationFailedException
    {
        UserTemplate userTemplate = new UserTemplate(user);
        userTemplate.setExternalId(null);
        internalDirectory.updateUser(userTemplate);
    }

    private ImmutableSet<String> findAllLocalGroups() throws OperationFailedException
    {
        //if local groups are disabled there is no chance they will be removed on login
        if (!localGroupHandler.isLocalGroupsEnabled())
        {
            return ImmutableSet.of();
        }

        return ImmutableSet.copyOf(internalDirectory.searchGroups(QueryBuilder
                .queryFor(String.class, EntityDescriptor.group())
                .with(Restriction.on(GroupTermKeys.LOCAL).exactlyMatching(true))
                .returningAtMost(EntityQuery.ALL_RESULTS)));

    }

    @Override
    public User addUser(UserTemplate user, PasswordCredential credential)
            throws InvalidUserException, InvalidCredentialException, UserAlreadyExistsException, OperationFailedException
    {
        User addedUser = remoteDirectory.addUser(user, credential);

        // if successful
        return addInternalUser(addedUser);
    }

    private User addInternalUser(User user)
            throws InvalidUserException, UserAlreadyExistsException, OperationFailedException
    {
        try
        {
            return internalDirectory.addUser(new UserTemplate(user), PasswordCredential.encrypted(INTERNAL_USER_PASSWORD));
        }
        catch (InvalidCredentialException ex)
        {
            // unexpected because we hard-code it
            throw new RuntimeException("Unexpected Credential Exception", ex);
        }
    }

    @Override
    public User updateUser(UserTemplate user) throws InvalidUserException, UserNotFoundException, OperationFailedException
    {
        UserTemplate remoteUserTemplate = new UserTemplate(user);
        if (remoteDirectory.supportsInactiveAccounts() && isLocalUserStatusEnabled())
        {
            User existingRemoteUser = remoteDirectory.findUserByName(user.getName());
            remoteUserTemplate.setActive(existingRemoteUser.isActive());
        }
        User updatedUser = remoteDirectory.updateUser(remoteUserTemplate);

        // if successful

        final UserTemplate updatedUserTemplate = new UserTemplate(updatedUser);
        if (!remoteDirectory.supportsInactiveAccounts() || isLocalUserStatusEnabled())
        {
            updatedUserTemplate.setActive(user.isActive()); // override remote active status with local status
        }

        return internalDirectory.updateUser(updatedUserTemplate);
    }

    private boolean isLocalUserStatusEnabled()
    {
        return internalDirectory.isLocalUserStatusEnabled();
    }

    @Override
    public void updateUserCredential(String username, PasswordCredential credential) throws UserNotFoundException, InvalidCredentialException, OperationFailedException
    {
        remoteDirectory.updateUserCredential(username, credential);
    }

    @Override
    public User renameUser(String oldName, String newName)
            throws UserNotFoundException, InvalidUserException, OperationFailedException, UserAlreadyExistsException
    {
        remoteDirectory.renameUser(oldName, newName);

        // if successful, do the rename in the internal directory as well
        return internalDirectory.renameUser(oldName, newName);
    }

    @Override
    public void storeUserAttributes(String username, Map<String, Set<String>> attributes) throws UserNotFoundException, OperationFailedException
    {
        internalDirectory.storeUserAttributes(username, attributes);
    }

    @Override
    public void removeUserAttributes(String username, String attributeName) throws UserNotFoundException, OperationFailedException
    {
        internalDirectory.removeUserAttributes(username, attributeName);
    }

    @Override
    public void removeUser(String name) throws UserNotFoundException, OperationFailedException
    {
        try
        {
            remoteDirectory.removeUser(name);
        } catch (UserNotFoundException ex)
        {
            // Looks like some one else did it on the server already - remove from cache
            internalDirectory.removeUser(name);
            throw ex;
        }
        internalDirectory.removeUser(name);
    }

    @Override
    public <T> List<T> searchUsers(EntityQuery<T> query) throws OperationFailedException
    {
        return internalDirectory.searchUsers(query);
    }

    @Override
    public Group findGroupByName(String name) throws GroupNotFoundException, OperationFailedException
    {
        return internalDirectory.findGroupByName(name);
    }

    @Override
    public GroupWithAttributes findGroupWithAttributesByName(String name) throws GroupNotFoundException, OperationFailedException
    {
        return internalDirectory.findGroupWithAttributesByName(name);
    }

    @Override
    public Group addGroup(GroupTemplate group)
            throws InvalidGroupException, OperationFailedException
    {
        if (localGroupHandler.isLocalGroupsEnabled())
        {
            if (isRemoteGroup(group.getName()))
            {
                throw new InvalidGroupException(group, "Group already exists in the Remote Directory");
            }

            try
            {
                return localGroupHandler.createLocalGroup(makeGroupTemplate(group));
            } catch (DirectoryNotFoundException e)
            {
                throw new OperationFailedException(e);
            }
        } else
        {
            // Add to the server
            Group addedGroup = remoteDirectory.addGroup(group);

            // now update the cache
            return internalDirectory.addGroup(new GroupTemplate(addedGroup));
        }
    }

    @Override
    public Group updateGroup(GroupTemplate group)
            throws InvalidGroupException, GroupNotFoundException, OperationFailedException, ReadOnlyGroupException
    {
        if (localGroupHandler.isLocalGroupsEnabled())
        {
            if (isRemoteGroup(group.getName()))
            {
                throw new ReadOnlyGroupException(group.getName());
            }

            return localGroupHandler.updateLocalGroup(makeGroupTemplate(group));
        } else
        {
            // Update on server
            Group updatedGroup = remoteDirectory.updateGroup(group);
            // now update the cache
            return internalDirectory.updateGroup(new GroupTemplate(updatedGroup));
        }
    }

    @Override
    public Group renameGroup(String oldName, String newName) throws GroupNotFoundException, InvalidGroupException
    {
        // don't even support it for local groups until we can properly support it in LDAP / Crowd too
        throw new UnsupportedOperationException("Renaming groups is not supported");
    }

    @Override
    public void storeGroupAttributes(String groupName, Map<String, Set<String>> attributes) throws GroupNotFoundException, OperationFailedException
    {
        internalDirectory.storeGroupAttributes(groupName, attributes);
    }

    @Override
    public void removeGroupAttributes(String groupName, String attributeName) throws GroupNotFoundException, OperationFailedException
    {
        internalDirectory.removeGroupAttributes(groupName, attributeName);
    }

    @Override
    public void removeGroup(String name) throws GroupNotFoundException, OperationFailedException, ReadOnlyGroupException
    {
        if (localGroupHandler.isLocalGroupsEnabled())
        {
            if (isRemoteGroup(name))
            {
                throw new ReadOnlyGroupException(name);
            }
            internalDirectory.removeGroup(name);
        } else
        {
            try
            {
                remoteDirectory.removeGroup(name);
            } catch (GroupNotFoundException e)
            {
                // Clear our cache anyway.
                internalDirectory.removeGroup(name);
                throw e;
            }
            internalDirectory.removeGroup(name);
        }
    }

    /**
     * This method avoids using exception handling for the case where you don't actually need a reference to
     * the resulting group. It is an expensive lookup however, so only use it where you're replacing a call to
     * findGroupByName in a try-catch block.
     *
     * @param groupName name of the group.
     * @return true if the group exists in the Remote directory, false if the group doesn't exist in the Remote directory (it may exist elsewhere).
     * @throws com.atlassian.crowd.exception.OperationFailedException
     *          badness.
     */
    private boolean isRemoteGroup(String groupName) throws OperationFailedException
    {
        try
        {
            remoteDirectory.findGroupByName(groupName);
            return true;
        } catch (GroupNotFoundException e)
        {
            return false;
        }
    }

    @Override
    public <T> List<T> searchGroups(EntityQuery<T> query) throws OperationFailedException
    {
        return internalDirectory.searchGroups(query);
    }

    @Override
    public boolean isUserDirectGroupMember(String username, String groupName) throws OperationFailedException
    {
        return internalDirectory.isUserDirectGroupMember(username, groupName);
    }

    @Override
    public boolean isGroupDirectGroupMember(String childGroup, String parentGroup) throws OperationFailedException
    {
        return internalDirectory.isGroupDirectGroupMember(childGroup, parentGroup);
    }

    @Override
    public void addUserToGroup(String username, String groupName)
        throws GroupNotFoundException, UserNotFoundException, OperationFailedException, ReadOnlyGroupException,
               MembershipAlreadyExistsException
    {
        if (localGroupHandler.isLocalGroupsEnabled())
        {
            if (isRemoteGroup(groupName))
            {
                throw new ReadOnlyGroupException(groupName);
            }

            localGroupHandler.addUserToLocalGroup(username, groupName);
        } else
        {
            try
            {
                // add membership on server
                remoteDirectory.addUserToGroup(username, groupName);
            }
            catch (MembershipAlreadyExistsException e)
            {
                // update the cache anyway
                internalDirectory.addUserToGroup(username, groupName);
                throw e;
            }
            // update the cache
            internalDirectory.addUserToGroup(username, groupName);
        }
    }

    @Override
    public void addGroupToGroup(String childGroup, String parentGroup)
        throws GroupNotFoundException, InvalidMembershipException, OperationFailedException, ReadOnlyGroupException,
               MembershipAlreadyExistsException
    {
        if (localGroupHandler.isLocalGroupsEnabled())
        {
            if (isRemoteGroup(parentGroup))
            {
                throw new ReadOnlyGroupException(parentGroup);
            }

            internalDirectory.addGroupToGroup(childGroup, parentGroup);
        }
        else
        {
            try
            {
                // add membership on server
                remoteDirectory.addGroupToGroup(childGroup, parentGroup);
            }
            catch (MembershipAlreadyExistsException e)
            {
                // update the cache anyway
                internalDirectory.addGroupToGroup(childGroup, parentGroup);
                throw e;
            }
            // update the cache
            internalDirectory.addGroupToGroup(childGroup, parentGroup);
        }
    }

    @Override
    public void removeUserFromGroup(String username, String groupName)
            throws GroupNotFoundException, UserNotFoundException, MembershipNotFoundException, OperationFailedException, ReadOnlyGroupException
    {
        if (localGroupHandler.isLocalGroupsEnabled())
        {
            if (isRemoteGroup(groupName))
            {
                throw new ReadOnlyGroupException(groupName);
            }

            localGroupHandler.removeUserFromLocalGroup(username, groupName);
        }
        else
        {
            try
            {
                // remove membership on server
                remoteDirectory.removeUserFromGroup(username, groupName);
            }
            catch (UserNotFoundException exceptionFromRemoteDirectory)
            {
                // the cache is probably out of date, so remove the membership from the cache anyway
                silentlyRemoveUserFromGroupInTheCache(username, groupName);
                throw exceptionFromRemoteDirectory;
            }
            catch (GroupNotFoundException exceptionFromRemoteDirectory)
            {
                // the cache is probably out of date, so remove the membership from the cache anyway
                silentlyRemoveUserFromGroupInTheCache(username, groupName);
                throw exceptionFromRemoteDirectory;
            }
            catch (MembershipNotFoundException exceptionFromRemoteDirectory)
            {
                // the cache is probably out of date, so remove the membership from the cache anyway
                silentlyRemoveUserFromGroupInTheCache(username, groupName);
                throw exceptionFromRemoteDirectory;
            }

            // update the cache
            internalDirectory.removeUserFromGroup(username, groupName);
        }
    }

    /**
     * Removes a user from a group in the cache, without propagating any checked exception. This should only
     * be used when the operation has failed in the remote directory, but we suspect that the cache is out of
     * date and it makes sense to perform the operation in the cache anyway.
     *
     * @param username
     * @param groupName
     */
    private void silentlyRemoveUserFromGroupInTheCache(String username, String groupName)
    {
        try
        {
            internalDirectory.removeUserFromGroup(username, groupName);
        }
        catch (ObjectNotFoundException e)
        {
            log.debug("Ignoring exception when removing user from group in cache", e);
        }
        catch (ReadOnlyGroupException e)
        {
            log.debug("Ignoring exception when removing user from group in cache", e);
        }
        catch (OperationFailedException e)
        {
            log.debug("Ignoring exception when removing user from group in cache", e);
        }
    }

    @Override
    public void removeGroupFromGroup(String childGroup, String parentGroup)
            throws GroupNotFoundException, InvalidMembershipException, MembershipNotFoundException, OperationFailedException, ReadOnlyGroupException
    {
        if (localGroupHandler.isLocalGroupsEnabled())
        {
            if (isRemoteGroup(parentGroup))
            {
                throw new ReadOnlyGroupException(parentGroup);
            }

            internalDirectory.removeGroupFromGroup(childGroup, parentGroup);
        }
        else
        {
            try
            {
                // remove membership on server
                remoteDirectory.removeGroupFromGroup(childGroup, parentGroup);
            }
            catch (GroupNotFoundException exceptionFromRemoteDirectory)
            {
                // the cache is probably out of date, so remove the membership from the cache anyway
                silentlyRemoveGroupFromGroupInTheCache(childGroup, parentGroup);
                throw exceptionFromRemoteDirectory;
            }
            catch (MembershipNotFoundException exceptionFromRemoteDirectory)
            {
                // the cache is probably out of date, so remove the membership from the cache anyway
                silentlyRemoveGroupFromGroupInTheCache(childGroup, parentGroup);
                throw exceptionFromRemoteDirectory;
            }

            // update the cache
            internalDirectory.removeGroupFromGroup(childGroup, parentGroup);
        }
    }

    /**
     * Removes a group from a group in the cache, without propagating any checked exception. This should only
     * be used when the operation has failed in the remote directory, but we suspect that the cache is out of
     * date and it makes sense to perform the operation in the cache anyway.
     *
     * @param childGroup
     * @param parentGroup
     */
    private void silentlyRemoveGroupFromGroupInTheCache(String childGroup, String parentGroup)
    {
        try
        {
            internalDirectory.removeGroupFromGroup(childGroup, parentGroup);
        }
        catch (ObjectNotFoundException e)
        {
            log.debug("Ignoring exception when removing group from group in cache", e);
        }
        catch (OperationFailedException e)
        {
            log.debug("Ignoring exception when removing group from group in cache", e);
        }
        catch (ReadOnlyGroupException e)
        {
            log.debug("Ignoring exception when removing group from group in cache", e);
        }
        catch (InvalidMembershipException e)
        {
            log.debug("Ignoring exception when removing group from group in cache", e);
        }
    }

    @Override
    public <T> List<T> searchGroupRelationships(MembershipQuery<T> query) throws OperationFailedException
    {
        return internalDirectory.searchGroupRelationships(query);
    }

    @Override
    public void testConnection() throws OperationFailedException
    {
        remoteDirectory.testConnection();
    }

    /**
     * This implementation will store the active flag locally in the internal directory if local user status is enabled
     * and if the active flag cannot be persisted on the underlying remote directory.
     *
     * @return true if the internal directory supports inactive accounts (which it should always do).
     */
    @Override
    public boolean supportsInactiveAccounts()
    {
        return remoteDirectory.supportsInactiveAccounts() || internalDirectory.isLocalUserStatusEnabled();
    }

    @Override
    public boolean supportsNestedGroups()
    {
        return remoteDirectory.supportsNestedGroups();
    }

    @Override
    public boolean isRolesDisabled()
    {
        return true;
    }

    // -----------------------------------------------------------------------------------------------------------------
    // Implementation of Attributes
    // -----------------------------------------------------------------------------------------------------------------

    @Override
    public Set<String> getValues(final String name)
    {
        return remoteDirectory.getValues(name);
    }

    @Override
    public String getValue(final String name)
    {
        return remoteDirectory.getValue(name);
    }

    @Override
    public boolean isEmpty()
    {
        return remoteDirectory.isEmpty();
    }

    @Override
    public Set<String> getKeys()
    {
        return remoteDirectory.getKeys();
    }

    @Override
    public void synchroniseCache(SynchronisationMode mode, SynchronisationStatusManager synchronisationStatusManager) throws OperationFailedException
    {
        final long directoryId = getDirectoryId();
        SynchronisationMode synchronisedMode = null;
        TimedOperation operation = new TimedOperation();
        try
        {
            log.info("synchronisation for directory [ " + directoryId + " ] starting");
            DirectoryCache directoryCache = directoryCacheFactory.createDirectoryCache(remoteDirectory, internalDirectory);
            if (mode == SynchronisationMode.INCREMENTAL)
            {
                // Only sync the delta
                synchronisationStatusManager.syncStatus(directoryId, "directory.caching.sync.incremental");
                try
                {
                    if (cacheRefresher.synchroniseChanges(directoryCache))
                    {
                        synchronisedMode = SynchronisationMode.INCREMENTAL;
                    }
                }
                catch (RuntimeException e)
                {
                    log.error("Incremental synchronisation was unexpectedly interrupted, falling back to a full synchronisation", e);
                }
            }

            if (synchronisedMode == null)
            {
                // Full sync
                synchronisationStatusManager.syncStatus(directoryId, "directory.caching.sync.full");
                cacheRefresher.synchroniseAll(directoryCache);
                synchronisedMode = SynchronisationMode.FULL;
            }
        } finally
        {
            String description = " synchronisation complete for directory [ " + directoryId + " ]";
            if (synchronisedMode != null)
            {
                log.info(operation.complete(synchronisedMode + description));
                synchronisationStatusManager.syncStatus(directoryId, "directory.caching.sync.completed." + synchronisedMode);
            }
            else
            {
                log.info(operation.complete("failed" + description));
                synchronisationStatusManager.syncStatus(directoryId, "directory.caching.sync.completed.error");
            }
        }
    }

    @Override
    public RemoteDirectory getAuthoritativeDirectory()
    {
        return remoteDirectory;
    }

    private static GroupTemplate makeGroupTemplate(Group group)
    {
        GroupTemplate template = new GroupTemplate(group);
        template.setDescription(group.getDescription());
        return template;
    }
}
