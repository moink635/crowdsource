package com.atlassian.crowd.manager.webhook;

import java.io.IOException;

import com.atlassian.crowd.model.webhook.Webhook;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Synchronously ping a Webhook.
 *
 * @since v2.7
 */
public class WebhookPinger
{
    private static final Logger logger = LoggerFactory.getLogger(WebhookPinger.class);

    private final HttpClient httpClient;

    /**
     * Constructs a pinger using an HTTP client with the default configuration. This constructor
     * exists to simplify the wiring of this object in host applications.
     */
    public WebhookPinger()
    {
        this(new HttpClient());
    }

    /**
     * Constructs a pinger using the provided, already configured HTTP client.
     *
     * @param httpClient a configured HTTP client which will be used to ping the Webhook callbacks.
     */
    public WebhookPinger(HttpClient httpClient)
    {
        this.httpClient = httpClient;
    }

    /**
     * Synchronously ping a Webhook and throw IOException if the ping cannot be delivered.
     *
     * @param webhook webhook to be pinged.
     * @throws IOException if the ping cannot be delivered.
     */
    public void ping(Webhook webhook) throws IOException
    {
        logger.debug("Pinging Webhook {} at endpoint {}", webhook.getId(), webhook.getEndpointUrl());
        HttpMethod method;
        try
        {
            method = new PostMethod(webhook.getEndpointUrl());
        }
        catch (IllegalArgumentException e)
        {
            throw new IOException("Failed to parse webhook endpoint url for ping (endpoint url might be invalid): "
                    + webhook.getEndpointUrl(), e);
        }
        if (webhook.getToken() != null)
        {
            method.setRequestHeader("Authorization", "Basic " + webhook.getToken());
        }

        int statusCode = httpClient.executeMethod(method);

        if (isSuccessfulStatusCode(statusCode))
        {
            logger.debug("Webhook {} successfully pinged at endpoint {}", webhook.getId(), webhook.getEndpointUrl());
        }
        else
        {
            throw new IOException("Webhook endpoint returned status code " + statusCode);
        }
    }

    /**
     * @param statusCode an HTTP status code
     * @return <code>true</code> if the status code is in the HTTP 2xx range
     */
    private static boolean isSuccessfulStatusCode(int statusCode)
    {
        return statusCode / 100 == 2;
    }
}
