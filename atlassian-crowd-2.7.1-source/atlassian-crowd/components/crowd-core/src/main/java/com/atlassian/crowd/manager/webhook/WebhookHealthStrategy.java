package com.atlassian.crowd.manager.webhook;

import com.atlassian.crowd.model.webhook.Webhook;

/**
 * Strategy to decide the health of Webhooks.
 *
 * @since v2.7
 */
public interface WebhookHealthStrategy
{
    /**
     * Register a successful ping delivery to a Webhook.
     *
     * @param webhook Webhook that has just been successfully pinged
     * @return a Webhook template to update the Webhook
     */
    Webhook registerSuccess(Webhook webhook);

    /**
     * Register a failed ping delivery to a Webhook. It simply registers the failure, but it does not take any
     * further action, i.e., it does not remove the Webhook.
     *
     * @param webhook Webhook that has just been unsuccessfully pinged
     * @return a Webhook template to update the Webhook
     */
    Webhook registerFailure(Webhook webhook);

    /**
     * Decides whether the Webhook is in good standing. Applications may use this information to deactivate a
     * Webhook that is no longer behaving. This check is made after each successful or failed Webhook ping.
     *
     * @param webhook Webhook which status is examined.
     * @return true if the Webhook is in good standing.
     */
    boolean isInGoodStanding(Webhook webhook);
}
