package com.atlassian.crowd.manager.application;

import com.atlassian.crowd.cache.UserAuthorisationCache;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.user.User;

import org.springframework.transaction.annotation.Transactional;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementation of ApplicationService which caches the result of ApplicationService methods.
 *
 * @since v2.2
 */
@Transactional
public class CachingApplicationService extends AbstractDelegatingApplicationService
{
    private final UserAuthorisationCache userAuthorisationCache;

    public CachingApplicationService(final ApplicationService applicationService, final UserAuthorisationCache userAuthorisationCache)
    {
        super(applicationService);
        this.userAuthorisationCache = checkNotNull(userAuthorisationCache);
    }

    public boolean isUserAuthorised(final Application application, final String username)
    {
        Boolean allowedToAuthenticate = userAuthorisationCache.isPermitted(username, application.getName());
        if (allowedToAuthenticate != null)
        {
            return allowedToAuthenticate;
        }
        else
        {
            boolean permitted = getApplicationService().isUserAuthorised(application, username);
            // only cache positive results
            if (permitted)
            {
                userAuthorisationCache.setPermitted(username, application.getName(), permitted);
            }
            return permitted;
        }
    }

    public User renameUser(Application application, String oldUserName, String newUsername) throws UserNotFoundException, OperationFailedException, ApplicationPermissionException, InvalidUserException
    {
        try
        {
            return getApplicationService().renameUser(application, oldUserName, newUsername);
        }
        finally
        {
            userAuthorisationCache.clear(oldUserName, application.getName());
            userAuthorisationCache.clear(newUsername, application.getName());
        }
    }

    public void removeUser(final Application application, final String user)
            throws OperationFailedException, UserNotFoundException, ApplicationPermissionException
    {
        try
        {
            getApplicationService().removeUser(application, user);
        }
        finally
        {
            userAuthorisationCache.clear(user, application.getName());
        }
    }
}
