package com.atlassian.crowd.manager.lock;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;

/**
 * Manages locks for synchronisation purposes, one per SynchronisableDirectory. Applies a standard naming strategy
 * for a key and delegates to a {@link LockFactory} to construct the particular locks.
 *
 * @see com.atlassian.crowd.directory.SynchronisableDirectory
 */
public class DirectoryLockManager
{
    private static final String KEY_PREFIX = DirectoryLockManager.class.getName() + ".directory-";

    private final LockFactory lockFactory;
    private final Map<String, Lock> locks;

    /**
     * Constructs an instance that delegates to a {@link ReentrantLockFactory} for lock construction.
     */
    public DirectoryLockManager()
    {
        this(new ReentrantLockFactory());
    }

    /**
     * Constructs an instance that delegates to the provided {@link LockFactory} for lock construction.
     *
     * @param lockFactory used to construct locks as required for each directory
     */
    public DirectoryLockManager(final LockFactory lockFactory)
    {
        this.lockFactory = lockFactory;
        locks = new HashMap<String, Lock>();
    }

    /**
     * Returns the lock for the directory with the given ID. The lock has not been acquired when it is returned,
     * clients still need to do this as normal. For example:
     * <p/>
     * <pre><code>
     *     Lock lock = directoryLockManager.getLock(directory.getId());
     *     lock.lock();
     *     try {
     *         // access the resource protected by this lock
     *     } finally {
     *         lock.unlock();
     *     }
     * </code></pre>
     *
     * @param directoryId the ID of the directory to lock
     * @return the lock for the provided directory ID
     */
    public Lock getLock(long directoryId)
    {
        String key = getLockKey(directoryId);

        synchronized (locks)
        {
            Lock lock = locks.get(key);
            if (lock == null)
            {
                lock = lockFactory.getLock(key);
                locks.put(key, lock);
            }
            return lock;
        }
    }

    private static String getLockKey(long directoryId)
    {
        return KEY_PREFIX + directoryId;
    }
}
