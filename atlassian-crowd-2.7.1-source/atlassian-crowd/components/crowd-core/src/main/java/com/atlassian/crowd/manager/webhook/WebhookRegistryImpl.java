package com.atlassian.crowd.manager.webhook;

import com.atlassian.crowd.dao.webhook.WebhookDAO;
import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.model.webhook.Webhook;

import org.springframework.transaction.annotation.Transactional;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A manager for Webhooks
 *
 * @since v2.7
 */
@Transactional
public class WebhookRegistryImpl implements WebhookRegistry
{
    private final WebhookDAO webhookDAO;

    public WebhookRegistryImpl(WebhookDAO webhookDAO)
    {
        this.webhookDAO = webhookDAO;
    }

    @Override
    public Webhook add(Webhook webhook)
    {
        try
        {
            return webhookDAO.findByApplicationAndEndpointUrl(webhook.getApplication(), webhook.getEndpointUrl());
        }
        catch (WebhookNotFoundException e)
        {
            return webhookDAO.add(webhook);
        }
    }

    @Override
    public void remove(Webhook webhook) throws WebhookNotFoundException
    {
        webhookDAO.remove(webhook);
    }

    @Override
    public Webhook findById(long webhookId) throws WebhookNotFoundException
    {
        return webhookDAO.findById(webhookId);
    }

    @Override
    public Iterable<Webhook> findAll()
    {
        return webhookDAO.findAll();
    }

    @Override
    public Webhook update(Webhook webhook) throws WebhookNotFoundException
    {
        checkNotNull(webhook.getId());
        return webhookDAO.update(webhook);
    }
}
