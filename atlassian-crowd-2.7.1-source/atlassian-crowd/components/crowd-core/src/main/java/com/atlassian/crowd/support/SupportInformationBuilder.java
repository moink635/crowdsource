package com.atlassian.crowd.support;

import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.google.common.collect.Ordering;

import java.util.Map;

/**
 * Builds a support information output string from a bunch of fields and headings.
 */
final class SupportInformationBuilder
{
    private final StringBuilder builder = new StringBuilder(1000);

    public void addHeading(String heading)
    {
        builder.append("=== ").append(heading).append(" ===\n");
    }

    public void addField(String name, Object value)
    {
        builder.append(name).append(": ").append(value).append("\n");
    }

    public void newLine()
    {
        builder.append("\n");
    }

    public void addAttributes(String name, Map<String, String> attributes)
    {
        builder.append(name).append(": ").append("\n");
        for (String key : Ordering.natural().sortedCopy(attributes.keySet()))
        {
            String value = DirectoryImpl.PASSWORD_ATTRIBUTES.contains(key) ?
                DirectoryImpl.SANITISED_PASSWORD : "\"" + attributes.get(key) + "\"";
            builder.append("    \"").append(key).append("\": ").append(value).append("\n");
        }
    }

    public String build()
    {
        return builder.toString();
    }
}
