package com.atlassian.crowd.util;

import java.util.ResourceBundle;

/**
 * Interface for providers of {@link ResourceBundle}s.
 *
 * @since v2.7
 */
public interface ResourceBundleProvider
{
    Iterable<ResourceBundle> getResourceBundles();
}
