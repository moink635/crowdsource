package com.atlassian.crowd.manager.webhook;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.atlassian.crowd.model.webhook.Webhook;
import com.atlassian.crowd.model.webhook.WebhookTemplate;

import com.google.common.annotations.VisibleForTesting;

/**
 * A Webhook health strategy that imposes a limit to the number of consecutive failures for at least some time.
 *
 * @since v2.7
 */
public class NoLongTermFailureStrategy implements WebhookHealthStrategy
{
    public static final long DEFAULT_MIN_FAILURES = 50;
    public static final long DEFAULT_MIN_INTERVAL_MILLIS = TimeUnit.HOURS.toMillis(6);
    private final long minFailures;
    private final long minIntervalMillis;
    private final Clock clock;

    //this constructor is needed by JIRA Pico container
    public NoLongTermFailureStrategy()
    {
        this(DEFAULT_MIN_FAILURES, DEFAULT_MIN_INTERVAL_MILLIS);
    }

    public NoLongTermFailureStrategy(long minFailures, long minIntervalMillis)
    {
        this(minFailures, minIntervalMillis, new Clock());
    }

    @VisibleForTesting
    NoLongTermFailureStrategy(long minFailures, long minIntervalMillis, Clock clock)
    {
        this.minFailures = minFailures;
        this.minIntervalMillis = minIntervalMillis;
        this.clock = clock;
    }

    @Override
    public Webhook registerSuccess(Webhook webhook)
    {
        WebhookTemplate webhookTemplate = new WebhookTemplate(webhook);
        webhookTemplate.resetFailuresSinceLastSuccess();
        webhookTemplate.resetOldestFailureDate();
        return webhookTemplate;
    }

    @Override
    public Webhook registerFailure(Webhook webhook)
    {
        WebhookTemplate webhookTemplate = new WebhookTemplate(webhook);
        webhookTemplate.setFailuresSinceLastSuccess(webhook.getFailuresSinceLastSuccess() + 1); // might overflow
        if (webhook.getOldestFailureDate() == null)
        {
            webhookTemplate.setOldestFailureDate(clock.getCurrentDate());
        }
        return webhookTemplate;
    }

    @Override
    public boolean isInGoodStanding(Webhook webhook)
    {
        return webhook.getOldestFailureDate() == null  // it is not failing
            || webhook.getFailuresSinceLastSuccess() < getMinFailures()  // it has been failing, but not enough times
            || clock.getCurrentDate().getTime() - webhook.getOldestFailureDate().getTime() < getMinIntervalMillis();  // it has been failing, but only recently
    }

    /**
     * @return the minimum number of times that a Webhook has to fail before it can be considered in bad standing
     */
    public long getMinFailures()
    {
        return minFailures;
    }

    /**
     * @return the minimum number of millis that a Webhook has to be failing before it can be considered in bad standing
     */
    public long getMinIntervalMillis()
    {
        return minIntervalMillis;
    }

    /**
     * This class exists to simplify unit testing by providing a mockable clock.
     */
    static class Clock
    {
        Date getCurrentDate()
        {
            return new Date();
        }
    }
}
