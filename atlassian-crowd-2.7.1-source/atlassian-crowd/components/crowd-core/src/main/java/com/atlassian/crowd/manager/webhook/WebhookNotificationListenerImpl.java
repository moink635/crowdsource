package com.atlassian.crowd.manager.webhook;

import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.model.webhook.Webhook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Reacts to the outcome of Webhook notifications by registering this outcome, updating the Webhook and,
 * if decided by the WebhookHealthStrategy, removing the Webhook.
 *
 * @since v2.7
 */
@Transactional
public class WebhookNotificationListenerImpl implements WebhookNotificationListener
{
    private static final Logger logger = LoggerFactory.getLogger(WebhookNotificationListenerImpl.class);

    private final WebhookRegistry webhookRegistry;
    private final WebhookHealthStrategy webhookHealthStrategy;

    public WebhookNotificationListenerImpl(WebhookRegistry webhookRegistry, WebhookHealthStrategy webhookHealthStrategy)
    {
        this.webhookRegistry = checkNotNull(webhookRegistry);
        this.webhookHealthStrategy = checkNotNull(webhookHealthStrategy);
    }

    @Override
    public void onPingSuccess(long webhookId) throws WebhookNotFoundException
    {
        Webhook webhookTemplate = webhookHealthStrategy.registerSuccess(webhookRegistry.findById(webhookId));
        if (webhookHealthStrategy.isInGoodStanding(webhookTemplate))
        {
            webhookRegistry.update(webhookTemplate);
        }
        else
        {
            logger.info("Webhook {} is in bad standing and will be removed", webhookTemplate);
            webhookRegistry.remove(webhookTemplate);
        }
    }

    @Override
    public void onPingFailure(long webhookId) throws WebhookNotFoundException
    {
        Webhook webhookTemplate = webhookHealthStrategy.registerFailure(webhookRegistry.findById(webhookId));
        if (webhookHealthStrategy.isInGoodStanding(webhookTemplate))
        {
            webhookRegistry.update(webhookTemplate);
        }
        else
        {
            logger.info("Webhook {} is in bad standing and will be removed", webhookTemplate);
            webhookRegistry.remove(webhookTemplate);
        }
    }
}
