package com.atlassian.crowd.manager.webhook;

import java.io.IOException;

import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.model.webhook.Webhook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A runnable that pings a Webhook and reports success or failure
 *
 * @since v2.7
 */
public class WebhookNotifierRunnable implements Runnable
{
    private static final Logger logger = LoggerFactory.getLogger(WebhookNotifierRunnable.class);

    private final Webhook webhook;
    private final WebhookPinger webhookPinger;
    private final WebhookNotificationListener webhookNotificationListener;

    public WebhookNotifierRunnable(Webhook webhook,
                                   WebhookPinger webhookPinger,
                                   WebhookNotificationListener webhookNotificationListener)
    {
        this.webhook = checkNotNull(webhook);
        this.webhookPinger = checkNotNull(webhookPinger);
        this.webhookNotificationListener = checkNotNull(webhookNotificationListener);
        checkArgument(webhook.getId() != null, "Webhook must be registered and have an ID");
    }

    /**
     * This runs in a separate thread, therefore the webhook instance is detached. All database operations must
     * be performed through transactional services. Do not try to access lazy members of the webhook.
     */
    @Override
    public void run()
    {
        try
        {
            if (pingWebhook(webhook))
            {
                webhookNotificationListener.onPingSuccess(webhook.getId());
            }
            else
            {
                webhookNotificationListener.onPingFailure(webhook.getId());
            }
        }
        catch (WebhookNotFoundException e)
        {
            logger.debug("Webhook " + webhook.getId() + " was deleted while it was being pinged", e);
        }
    }

    /**
     * Uses the WebhookPinger to ping a Webhook.
     *
     * @param webhook webhook to be pinged
     * @return <code>true</code> if the ping was successful, <code>false</code> otherwise
     */
    private boolean pingWebhook(Webhook webhook)
    {
        try
        {
            webhookPinger.ping(webhook);
            return true;
        }
        catch (IOException e)
        {
            logger.debug("Failed to notify Webhook " + webhook.getId(), e);
            return false;
        }
    }
}
