package com.atlassian.crowd.manager.webhook;

import com.atlassian.crowd.model.webhook.Webhook;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An implementation of WebhookService
 *
 * @since v2.7
 */
@Transactional
public class WebhookServiceImpl implements WebhookService
{
    private static final Logger logger = LoggerFactory.getLogger(WebhookServiceImpl.class);

    private final WebhookRegistry webhookRegistry;
    private final WebhookPinger webhookPinger;
    private final KeyedExecutor<Long> executor;
    private final WebhookNotificationListener webhookNotificationListener;

    public WebhookServiceImpl(WebhookRegistry webhookRegistry, WebhookPinger webhookPinger,
                              KeyedExecutor<Long> executor, WebhookNotificationListener webhookNotificationListener)
    {
        this.webhookRegistry = checkNotNull(webhookRegistry);
        this.webhookPinger = checkNotNull(webhookPinger);
        this.executor = checkNotNull(executor);
        this.webhookNotificationListener = checkNotNull(webhookNotificationListener);
    }

    @Override
    public void notifyWebhooks()
    {
        logger.debug("New events are available, notifying Webhooks");
        for (Webhook webhook : webhookRegistry.findAll())
        {
            WebhookNotifierRunnable runnable =
                new WebhookNotifierRunnable(webhook, webhookPinger, webhookNotificationListener);
            executor.execute(runnable, webhook.getId());
        }
    }
}
