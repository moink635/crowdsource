package com.atlassian.crowd.manager.application;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.event.EventTokenExpiredException;
import com.atlassian.crowd.event.Events;
import com.atlassian.crowd.event.IncrementalSynchronisationNotAvailableException;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.BulkAddFailedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidEmailAddressException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.InvalidMembershipException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.manager.webhook.InvalidWebhookEndpointException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.model.webhook.Webhook;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * An implementation of ApplicationService that delegates all methods to another ApplicationService. Subclasses
 * can override specific methods to change the behaviour.
 *
 * @since v2.7
 */
public abstract class AbstractDelegatingApplicationService implements ApplicationService
{
    private ApplicationService applicationService;

    public AbstractDelegatingApplicationService(ApplicationService applicationService)
    {
        this.applicationService = checkNotNull(applicationService);
    }

    protected ApplicationService getApplicationService()
    {
        return applicationService;
    }

    @Override
    public User authenticateUser(Application application,
                                 String username,
                                 PasswordCredential passwordCredential)
        throws OperationFailedException, InactiveAccountException, InvalidAuthenticationException,
               ExpiredCredentialException, UserNotFoundException
    {
        return applicationService.authenticateUser(application, username, passwordCredential);
    }

    @Override
    public boolean isUserAuthorised(Application application,
                                    String username)
    {
        return applicationService.isUserAuthorised(application, username);
    }

    @Override
    public void addAllUsers(Application application,
                            Collection<UserTemplateWithCredentialAndAttributes> users)
        throws ApplicationPermissionException, OperationFailedException, BulkAddFailedException
    {
        applicationService.addAllUsers(application, users);
    }

    @Override
    public User findUserByName(Application application, String name) throws UserNotFoundException
    {
        return applicationService.findUserByName(application, name);
    }

    @Override
    public UserWithAttributes findUserWithAttributesByName(Application application,
                                                           String name) throws UserNotFoundException
    {
        return applicationService.findUserWithAttributesByName(application, name);
    }

    @Override
    public User addUser(Application application,
                        UserTemplate user,
                        PasswordCredential credential)
        throws InvalidUserException, OperationFailedException, InvalidCredentialException,
               ApplicationPermissionException
    {
        return applicationService.addUser(application, user, credential);
    }

    @Override
    public User updateUser(Application application,
                           UserTemplate user)
        throws InvalidUserException, OperationFailedException, ApplicationPermissionException, UserNotFoundException
    {
        return applicationService.updateUser(application, user);
    }

    @Override
    public User renameUser(Application application,
                           String oldUserName, String newUsername)
        throws UserNotFoundException, OperationFailedException, ApplicationPermissionException, InvalidUserException
    {
        return applicationService.renameUser(application, oldUserName, newUsername);
    }

    @Override
    public void updateUserCredential(Application application,
                                     String username,
                                     PasswordCredential credential)
        throws OperationFailedException, UserNotFoundException, InvalidCredentialException,
               ApplicationPermissionException
    {
        applicationService.updateUserCredential(application, username, credential);
    }

    @Override
    public void resetUserCredential(Application application,
                                    String username)
        throws OperationFailedException, UserNotFoundException, InvalidCredentialException,
               ApplicationPermissionException, InvalidEmailAddressException
    {
        applicationService.resetUserCredential(application, username);
    }

    @Override
    public void storeUserAttributes(Application application,
                                    String username,
                                    Map<String, Set<String>> attributes)
        throws OperationFailedException, ApplicationPermissionException, UserNotFoundException
    {
        applicationService.storeUserAttributes(application, username, attributes);
    }

    @Override
    public void removeUserAttributes(Application application,
                                     String username, String attributeName)
        throws OperationFailedException, ApplicationPermissionException, UserNotFoundException
    {
        applicationService.removeUserAttributes(application, username, attributeName);
    }

    @Override
    public void removeUser(Application application, String user)
        throws OperationFailedException, UserNotFoundException, ApplicationPermissionException
    {
        applicationService.removeUser(application, user);
    }

    @Override
    public <T> List<T> searchUsers(Application application,
                                   EntityQuery<T> query)
    {
        return applicationService.searchUsers(application, query);
    }

    @Override
    public List<User> searchUsersAllowingDuplicateNames(Application application,
                                                        EntityQuery<User> query)
    {
        return applicationService.searchUsersAllowingDuplicateNames(application, query);
    }

    @Override
    public Group findGroupByName(Application application,
                                 String name) throws GroupNotFoundException
    {
        return applicationService.findGroupByName(application, name);
    }

    @Override
    public GroupWithAttributes findGroupWithAttributesByName(Application application,
                                                             String name) throws GroupNotFoundException
    {
        return applicationService.findGroupWithAttributesByName(application, name);
    }

    @Override
    public Group addGroup(Application application,
                          GroupTemplate group)
        throws InvalidGroupException, OperationFailedException, ApplicationPermissionException
    {
        return applicationService.addGroup(application, group);
    }

    @Override
    public Group updateGroup(Application application,
                             GroupTemplate group)
        throws InvalidGroupException, OperationFailedException, ApplicationPermissionException, GroupNotFoundException
    {
        return applicationService.updateGroup(application, group);
    }

    @Override
    public void storeGroupAttributes(Application application,
                                     String groupname,
                                     Map<String, Set<String>> attributes)
        throws OperationFailedException, ApplicationPermissionException, GroupNotFoundException
    {
        applicationService.storeGroupAttributes(application, groupname, attributes);
    }

    @Override
    public void removeGroupAttributes(Application application,
                                      String groupname, String attributeName)
        throws OperationFailedException, ApplicationPermissionException, GroupNotFoundException
    {
        applicationService.removeGroupAttributes(application, groupname, attributeName);
    }

    @Override
    public void removeGroup(Application application, String group)
        throws OperationFailedException, GroupNotFoundException, ApplicationPermissionException
    {
        applicationService.removeGroup(application, group);
    }

    @Override
    public <T> List<T> searchGroups(Application application,
                                    EntityQuery<T> query)
    {
        return applicationService.searchGroups(application, query);
    }

    @Override
    public void addUserToGroup(Application application,
                               String username, String groupName)
        throws OperationFailedException, UserNotFoundException, GroupNotFoundException, ApplicationPermissionException,
               MembershipAlreadyExistsException
    {
        applicationService.addUserToGroup(application, username, groupName);
    }

    @Override
    public void addGroupToGroup(Application application,
                                String childGroupName, String parentGroupName)
        throws OperationFailedException, GroupNotFoundException, ApplicationPermissionException,
               InvalidMembershipException, MembershipAlreadyExistsException
    {
        applicationService.addGroupToGroup(application, childGroupName, parentGroupName);
    }

    @Override
    public void removeUserFromGroup(Application application,
                                    String username, String groupName)
        throws OperationFailedException, GroupNotFoundException, UserNotFoundException, ApplicationPermissionException,
               MembershipNotFoundException
    {
        applicationService.removeUserFromGroup(application, username, groupName);
    }

    @Override
    public void removeGroupFromGroup(Application application,
                                     String childGroup, String parentGroup)
        throws OperationFailedException, GroupNotFoundException, ApplicationPermissionException,
               MembershipNotFoundException
    {
        applicationService.removeGroupFromGroup(application, childGroup, parentGroup);
    }

    @Override
    public boolean isUserDirectGroupMember(Application application,
                                           String username, String groupName)
    {
        return applicationService.isUserDirectGroupMember(application, username, groupName);
    }

    @Override
    public boolean isGroupDirectGroupMember(Application application,
                                            String childGroup, String parentGroup)
    {
        return applicationService.isGroupDirectGroupMember(application, childGroup, parentGroup);
    }

    @Override
    public boolean isUserNestedGroupMember(Application application,
                                           String username, String groupName)
    {
        return applicationService.isUserNestedGroupMember(application, username, groupName);
    }

    @Override
    public boolean isGroupNestedGroupMember(Application application,
                                            String childGroup, String parentGroup)
    {
        return applicationService.isGroupNestedGroupMember(application, childGroup, parentGroup);
    }

    @Override
    public <T> List<T> searchDirectGroupRelationships(Application application,
                                                      MembershipQuery<T> query)
    {
        return applicationService.searchDirectGroupRelationships(application, query);
    }

    @Override
    public <T> List<T> searchNestedGroupRelationships(Application application,
                                                      MembershipQuery<T> query)
    {
        return applicationService.searchNestedGroupRelationships(application, query);
    }

    @Override
    public String getCurrentEventToken(Application application) throws IncrementalSynchronisationNotAvailableException
    {
        return applicationService.getCurrentEventToken(application);
    }

    @Override
    public Events getNewEvents(Application application,
                               String eventToken) throws EventTokenExpiredException, OperationFailedException
    {
        return applicationService.getNewEvents(application, eventToken);
    }

    @Override
    public Webhook findWebhookById(Application application, long webhookId)
        throws WebhookNotFoundException, ApplicationPermissionException
    {
        return applicationService.findWebhookById(application, webhookId);
    }

    @Override
    public Webhook registerWebhook(Application application, String endpointUrl, @Nullable String token) throws InvalidWebhookEndpointException
    {
        return applicationService.registerWebhook(application, endpointUrl, token);
    }

    @Override
    public void unregisterWebhook(Application application, long webhookId)
        throws ApplicationPermissionException, WebhookNotFoundException
    {
        applicationService.unregisterWebhook(application, webhookId);
    }
}
