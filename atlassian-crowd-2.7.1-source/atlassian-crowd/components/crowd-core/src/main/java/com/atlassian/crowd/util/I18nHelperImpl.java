package com.atlassian.crowd.util;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.Arrays.asList;

public class I18nHelperImpl implements I18nHelper
{
    private static final Logger LOG = LoggerFactory.getLogger(I18nHelperImpl.class);

    private final Iterable<ResourceBundleProvider> resourceBundleProviders;

    public I18nHelperImpl(List<ResourceBundleProvider> providers)
    {
        checkNotNull(providers);
        this.resourceBundleProviders = ImmutableList.copyOf(providers);
    }

    @Override
    public String getText(String key)
    {
        return getText(key, new Object[0]);
    }

    @Override
    public String getText(String key, String value1)
    {
        return getText(key, asList(value1));
    }

    @Override
    public String getText(String key, String value1, String value2)
    {
        return getText(key, asList(value1, value2));
    }

    @Override
    public String getText(String key, Object parameters)
    {
        final Object[] params;
        if (parameters instanceof List)
        {
            params = ((List<?>) parameters).toArray();
        }
        else if (parameters instanceof Object[])
        {
            params = (Object[]) parameters;
        }
        else
        {
            params = new Object[]{parameters};
        }
        return new MessageFormat(getUnescapedText(key)).format(params);
    }

    @Override
    public String getText(Locale locale, String key, Serializable... arguments)
    {
        return getText(key, arguments);
    }

    /**
     * Get the raw property value, complete with {0}'s.
     * @param key Non-null key to look up
     * @return Unescaped property value for the key, or the key itself if no property with the specified key is found
     */
    @Override
    public String getUnescapedText(String key)
    {
        for (ResourceBundleProvider resourceBundleProvider : resourceBundleProviders)
        {
            for (ResourceBundle i18nBundle : resourceBundleProvider.getResourceBundles())
            {
                if (key.startsWith("'") && key.endsWith("'"))
                {
                    key = key.substring(1, key.length() - 1);
                }

                if (i18nBundle.containsKey(key))
                {
                    try
                    {
                        return i18nBundle.getString(key);
                    }
                    catch (MissingResourceException e)
                    {
                        LOG.debug("Key <{}> not present in bundle", key);
                    }
                }
            }
        }

        return key;
    }

    @Override
    public Map<String, String> getAllTranslationsForPrefix(String prefix)
    {
        checkNotNull(prefix);

        final Map<String, String> translations = Maps.newHashMap();
        for (ResourceBundleProvider resourceBundleProvider : resourceBundleProviders)
        {
            for (ResourceBundle resourceBundle : resourceBundleProvider.getResourceBundles())
            {
                for (String key : resourceBundle.keySet())
                {
                    if (key.startsWith(prefix))
                    {
                        translations.put(key, resourceBundle.getString(key));
                    }
                }
            }
        }

        return ImmutableMap.copyOf(translations);
    }
}
