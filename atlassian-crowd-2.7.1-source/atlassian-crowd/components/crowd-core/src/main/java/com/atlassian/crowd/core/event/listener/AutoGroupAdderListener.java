package com.atlassian.crowd.core.event.listener;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.event.user.UserAuthenticatedEvent;
import com.atlassian.crowd.event.user.UserAuthenticationSucceededEvent;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.ReadOnlyGroupException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.directory.DirectoryPermissionException;
import com.atlassian.crowd.model.EntityComparator;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.event.api.EventListener;

import com.google.common.collect.ImmutableMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

public class AutoGroupAdderListener
{
    protected static final String AUTO_GROUPS_ADDED = "autoGroupsAdded";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final DirectoryManager directoryManager;

    public AutoGroupAdderListener(DirectoryManager directoryManager)
    {
        this.directoryManager = checkNotNull(directoryManager, "directoryManager");
    }

    @EventListener
    public void handleEvent(final UserAuthenticatedEvent event)
    {
        final Directory directory = event.getDirectory();
        final User user = event.getUser();

        handleEvent(directory, user);
    }

    @EventListener
    public void handleEvent(final UserAuthenticationSucceededEvent event)
    {
        User user = event.getRemotePrincipal();

        try
        {
            handleEvent(directoryManager.findDirectoryById(user.getDirectoryId()), user);
        }
        catch (DirectoryNotFoundException e)
        {
            logger.error("Could not find directory " + user.getDirectoryId(), e);
        }
    }

    private void handleEvent(Directory directory, User user)
    {
        final String concatenatedGroupNames = directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_AUTO_ADD_GROUPS);

        if (StringUtils.isNotBlank(concatenatedGroupNames))
        {
            final String[] groups = StringUtils.split(concatenatedGroupNames, DirectoryImpl.AUTO_ADD_GROUPS_SEPARATOR);
            try
            {
                // Do not proceed if the user has authenticated successfully before
                final UserWithAttributes userWithAttributes =
                        directoryManager.findUserWithAttributesByName(directory.getId(), user.getName());
                if (Boolean.parseBoolean(userWithAttributes.getValue(AUTO_GROUPS_ADDED)))
                {
                    return;
                }

                final Set<String> currentMemberships = new TreeSet<String>(EntityComparator.of(String.class));
                currentMemberships.addAll(searchDirectGroupMemberships(directory.getId(), user));

                for (String groupName : groups)
                {
                    try
                    {
                        if (!currentMemberships.contains(groupName))
                        {
                            directoryManager.addUserToGroup(directory.getId(), user.getName(), groupName);
                        }
                    }
                    catch (GroupNotFoundException e)
                    {
                        logger.error("Could not auto add user to group: " + e.getMessage(), e);
                    }
                    catch (OperationFailedException e)
                    {
                        logger.error("Could not access directory: " + e.getMessage(), e);
                    }
                    catch (UserNotFoundException e)
                    {
                        logger.error("Could not auto add user to group: " + e.getMessage(), e);
                    }
                    catch (ReadOnlyGroupException e)
                    {
                        logger.error("Could not auto add user to group: " + e.getMessage(), e);
                    }
                    catch (DirectoryNotFoundException e)
                    {
                        logger.error("Could not find directory " + directory.getId(), e);
                    }
                    catch (DirectoryPermissionException e)
                    {
                        logger.error("You have group <" + groupName + "> to be auto-added for the user <" + user.getName() + ">, but the directory does not have permission for Group updates.");
                    }
                    catch (MembershipAlreadyExistsException e)
                    {
                        // This should only happen if they were added since we checked memberships a few lines beforehand
                        logger.error("Could not auto add user to group because membership already exists", e);
                    }
                }

                directoryManager.storeUserAttributes(directory.getId(), user.getName(),
                        ImmutableMap.of(AUTO_GROUPS_ADDED, Collections.singleton(Boolean.TRUE.toString())));
            }
            catch (DirectoryInstantiationException e)
            {
                logger.error("Could not instantiate directory: " + e.getMessage(), e);
            }
            catch (OperationFailedException e)
            {
                logger.error("Could not access directory: " + e.getMessage(), e);
            }
            catch (UserNotFoundException e)
            {
                logger.error("Could not access user: " + e.getMessage(), e);
            }
            catch (DirectoryNotFoundException e)
            {
                logger.error("Could not find directory " + directory.getId(), e);
            }
            catch (DirectoryPermissionException e)
            {
                logger.error("Could not store auto-added groups attributes for user <" + user.getName() + ">  because directory does not have permission for User updates.");
            }
        }
    }

    /**
     * Searches for user's direct group names.
     *
     * @param directoryId directory to use for searching direct group memberships
     * @param user user to search the memberships for
     * @return set of user's direct group names
     * @throws OperationFailedException if the memberships could not be retrieved
     * @throws DirectoryNotFoundException if the directory could not be found
     */
    private List<String> searchDirectGroupMemberships(long directoryId, User user)
            throws OperationFailedException, DirectoryNotFoundException
    {
        return directoryManager.searchDirectGroupRelationships(directoryId,
                QueryBuilder.createMembershipQuery(EntityQuery.ALL_RESULTS, 0, false,
                        EntityDescriptor.group(), String.class,
                        EntityDescriptor.user(), user.getName()));
    }
}
