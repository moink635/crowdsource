package com.atlassian.crowd.manager.webhook;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A task executor that discards tasks if there is another existing task with the same key queued for execution.
 * Keys must be valid Map keys, i.e., they must be immutable and have equals() and hashMap().
 *
 * @since v2.7
 */
public class KeyedExecutor<K>
{
    private static final Logger logger = LoggerFactory.getLogger(KeyedExecutor.class);

    private final Executor delegateExecutor;
    private final Set<K> keysInQueue = Collections.newSetFromMap(new ConcurrentHashMap<K, Boolean>());

    public KeyedExecutor(Executor delegateExecutor)
    {
        this.delegateExecutor = checkNotNull(delegateExecutor);
    }

    /**
     * Adds a Runnable to the executor queue. If there is another Runnable with the same key that is awaiting
     * its execution, then the new Runnable is discarded.
     *
     * @param runnable the runnable task.
     * @param key task key. Must be immutable and have equals() and hashMap().
     */
    public void execute(final Runnable runnable, final K key)
    {
        if (keysInQueue.add(key))
        {
            delegateExecutor.execute(new Runnable()
            {
                @Override
                public void run()
                {
                    if (keysInQueue.remove(key))
                    {
                        runnable.run();
                    }
                    else
                    {
                        logger.debug("Not running runnable {} because it was removed from the queue", runnable);
                    }
                }
            });
        }
        else
        {
            logger.debug("Discarding runnable {} because its key is already in the queue", runnable);
        }

    }
}
