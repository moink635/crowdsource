package com.atlassian.crowd.manager.cache;

import java.io.Serializable;
import java.util.List;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;


public class CacheManagerEhcache implements CacheManager
{
    private final net.sf.ehcache.CacheManager cacheManager;

    public CacheManagerEhcache(net.sf.ehcache.CacheManager cacheManager)
    {
        this.cacheManager = cacheManager;
    }

    /**
     * Creates a cache if it doesn't already exist - if the cache has been defined in the configuration xml, then it
     * will not be recreated.
     *
     * @param cacheName Name of the cache to ensure exists
     */
    private Ehcache configureCache(String cacheName)
    {
        return cacheManager.addCacheIfAbsent(cacheName);
    }

    private Ehcache getCache(String cacheName)
    {
        return configureCache(cacheName);
    }

    @Override
    public void put(String cacheName, Serializable key, Serializable obj) throws CacheManagerException
    {
        Element element = new Element(key, obj);

        try
        {
            getCache(cacheName).put(element);
        }
        catch (CacheException e)
        {
            throw new CacheManagerException(e);
        }
    }

    @Override
    public Object get(String cacheName, Serializable key) throws CacheManagerException, NotInCacheException
    {
        try
        {
            Element element = getCache(cacheName).get(key);

            if (element == null)
            {
                throw new NotInCacheException();
            }

            return element.getObjectValue();
        }
        catch (CacheException e)
        {
            throw new CacheManagerException(e);
        }
    }

    @Override
    public boolean remove(String cacheName, Serializable key) throws CacheManagerException
    {
        return getCache(cacheName).remove(key);
    }

    @Override
    public void removeAll(String cacheName) throws CacheManagerException
    {
        try
        {
            getCache(cacheName).removeAll();
        }
        // left in place because version 1.1 of the library throws IOException
        catch (Exception e)
        {
            throw new CacheManagerException(e);
        }
    }

    @Override
    public List getAllKeys(String cacheName) throws CacheManagerException
    {
        try
        {
            return getCache(cacheName).getKeys();
        }
        catch (CacheException e)
        {
            throw new CacheManagerException("Failed to load keys from cache", e);
        }
    }
}
