package com.atlassian.crowd.support;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import javax.annotation.Nullable;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Produces a String containing information about some aspects of the Crowd configuration.
 * This class requires a {@link DirectoryManager}, and optionally, an {@link ApplicationManager}.
 * The latter has been made optional because not all products have multiple applications.
 *
 * @since 2.6.2
 */
public class SupportInformationServiceImpl implements SupportInformationService
{
    private final DirectoryManager directoryManager;
    @Nullable private ApplicationManager applicationManager;

    public SupportInformationServiceImpl(DirectoryManager directoryManager)
    {
        this.directoryManager = checkNotNull(directoryManager);
    }

    public String getSupportInformation(@Nullable User currentUser)
    {
        SupportInformationBuilder builder = new SupportInformationBuilder();

        addCurrentUserInformation(builder, currentUser);
        addDirectoryConfiguration(builder);
        addApplicationConfiguration(builder);

        return builder.build();
    }

    private void addCurrentUserInformation(SupportInformationBuilder builder, @Nullable User currentUser)
    {
        if (currentUser != null)
        {
            builder.addHeading("Current user");
            builder.addField("Directory ID", currentUser.getDirectoryId());
            builder.addField("Username", currentUser.getName());
            builder.addField("Display name", currentUser.getDisplayName());
            builder.addField("Email address", currentUser.getEmailAddress());
            builder.newLine();
        }
    }

    private void addDirectoryConfiguration(SupportInformationBuilder builder)
    {
        List<Directory> directories = directoryManager.findAllDirectories();
        if (directories != null)
        {
            builder.addHeading("Directories configured");
            for (Directory directory : directories)
            {
                builder.addField("Directory ID", directory.getId());
                builder.addField("Name", directory.getName());
                builder.addField("Active", directory.isActive());
                builder.addField("Type", directory.getType());
                builder.addField("Created date", directory.getCreatedDate());
                builder.addField("Updated date", directory.getUpdatedDate());
                builder.addField("Allowed operations", directory.getAllowedOperations());
                builder.addField("Implementation class", directory.getImplementationClass());
                builder.addField("Encryption type", directory.getEncryptionType());
                builder.addAttributes("Attributes", directory.getAttributes());
                builder.newLine();
            }
        }
    }

    private void addApplicationConfiguration(SupportInformationBuilder builder)
    {
        if (applicationManager != null)
        {
            builder.addHeading("Applications configured");
            for (Application application : applicationManager.findAll())
            {
                builder.addField("Application ID", application.getId());
                builder.addField("Name", application.getName());
                builder.addField("Active", application.isActive());
                builder.addField("Type", application.getType());
                builder.addField("Is lowercase output", application.isLowerCaseOutput());
                builder.addField("Is aliasing enabled", application.isAliasingEnabled());
                builder.addField("Remote addresses",
                        Iterables.transform(application.getRemoteAddresses(), EXTRACT_REMOTE_ADDRESS));
                builder.addField("Created date", application.getCreatedDate());
                builder.addField("Updated date", application.getUpdatedDate());
                builder.addAttributes("Attributes", application.getAttributes());
                for (DirectoryMapping directoryMapping : application.getDirectoryMappings())
                {
                    builder.addField("Mapped to directory ID", directoryMapping.getDirectory().getId());
                    builder.addField("    Allow all to authenticate", directoryMapping.isAllowAllToAuthenticate());
                    builder.addField("    Mapped groups",
                            Iterables.transform(directoryMapping.getAuthorisedGroups(), EXTRACT_GROUP_NAME));
                    builder.addField("    Allowed operations", directoryMapping.getAllowedOperations());
                }
                builder.newLine();
            }
        }
    }

    public void setApplicationManager(ApplicationManager applicationManager)
    {
        this.applicationManager = applicationManager;
    }

    private static final Function<RemoteAddress,String> EXTRACT_REMOTE_ADDRESS = new Function<RemoteAddress, String>()
    {
        @Override
        public String apply(@Nullable RemoteAddress remoteAddress)
        {
            return remoteAddress.getAddress();
        }
    };

    private static final Function<GroupMapping,String> EXTRACT_GROUP_NAME = new Function<GroupMapping, String>()
    {
        @Override
        public String apply(@Nullable GroupMapping groupMapping)
        {
            return groupMapping.getGroupName();
        }
    };
}
