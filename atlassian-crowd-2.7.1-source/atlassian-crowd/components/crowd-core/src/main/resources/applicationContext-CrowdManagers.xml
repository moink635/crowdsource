<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:plugin="http://atlassian.com/schema/spring/plugin"
       xmlns:tx="http://www.springframework.org/schema/tx"
       xsi:schemaLocation="
       http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-2.5.xsd
       http://atlassian.com/schema/spring/plugin http://atlassian.com/schema/spring/plugin.xsd
       http://www.springframework.org/schema/tx http://www.springframework.org/schema/tx/spring-tx-2.5.xsd">

    <bean id="eventPublisher" class="com.atlassian.crowd.util.CrowdEventPublisherFactory" factory-method="createEventPublisher" plugin:available='true'/>

    <bean id="xWorkChangeListener" class="com.atlassian.crowd.event.listener.XWorkChangeListener" init-method="registerListener">
        <constructor-arg ref="eventPublisher"/>
    </bean>

    <bean id="profilingInterceptor" class="com.atlassian.spring.interceptors.SpringProfilingInterceptor"/>

    <tx:annotation-driven transaction-manager="transactionManager"/>

    <bean id="profilingProxy" class="org.springframework.aop.framework.ProxyFactoryBean" abstract="true">
        <property name="interceptorNames">
            <list>
                <value>profilingInterceptor</value>
            </list>
        </property>
    </bean>

    <!-- Managers -->
    <bean id="cacheManager" class="com.atlassian.crowd.manager.cache.CacheManagerEhcache">
        <constructor-arg ref="ehcacheManager"/>
    </bean>

    <bean id="applicationManager" plugin:available="true" parent="profilingProxy">
        <property name="target">
            <bean class="com.atlassian.crowd.manager.application.ApplicationManagerGeneric">
                <constructor-arg index="0" ref="applicationDao"/>
                <constructor-arg index="1" ref="passwordEncoderFactory"/>
                <constructor-arg index="2" ref="eventPublisher"/>
            </bean>
        </property>
    </bean>

    <bean id="userAuthorisationCache" class="com.atlassian.crowd.cache.UserAuthorisationCacheImpl">
        <constructor-arg ref="cacheManager"/>
    </bean>

    <bean id="applicationService" plugin:available="true" parent="profilingProxy">
        <property name="target">
            <bean class="com.atlassian.crowd.manager.application.TranslatingApplicationService">
                <constructor-arg index="0" ref="applicationServiceCaching"/>
                <constructor-arg index="1" ref="aliasManager"/>
            </bean>
        </property>
    </bean>

    <bean id="applicationServiceCaching" autowire-candidate="false" parent="profilingProxy">
        <property name="target">
            <bean class="com.atlassian.crowd.manager.application.CachingApplicationService">
                <constructor-arg index="0" ref="applicationServiceInternal"/>
                <constructor-arg index="1" ref="userAuthorisationCache"/>
            </bean>
        </property>
    </bean>

    <bean id="applicationServiceInternal" autowire-candidate="false" parent="profilingProxy">
        <property name="target">
            <bean class="com.atlassian.crowd.manager.application.ApplicationServiceGeneric">
                <constructor-arg index="0" ref="directoryManager"/>
                <constructor-arg index="1" ref="permissionManager"/>
                <constructor-arg index="2" ref="directoryInstanceLoader"/>
                <constructor-arg index="3" ref="eventPublisher"/>
                <constructor-arg index="4" ref="eventStore"/>
                <constructor-arg index="5" ref="webhookRegistry"/>
                <property name="allowShadowedUsersInMembershipQueryResults" value="true"/>
            </bean>
        </property>
    </bean>

    <bean id="storingEventListener" class="com.atlassian.crowd.event.StoringEventListener">
        <constructor-arg index="0" ref="eventStore"/>
        <constructor-arg index="1" ref="eventPublisher"/>
        <constructor-arg index="2" ref="webhookService"/>
        <constructor-arg index="3" value="true"/>
    </bean>

    <bean id="eventStore" class="com.atlassian.crowd.event.EventStoreGeneric">
        <constructor-arg index="0" value="10000"/>
    </bean>

    <bean id="propertyManager" plugin:available="true" parent="profilingProxy">
        <property name="target">
            <bean class="com.atlassian.crowd.manager.property.PropertyManagerGeneric">
                <constructor-arg ref="propertyDao"/>
            </bean>
        </property>
    </bean>

    <bean name="directoryDatabaseFileLocator"
          class="com.atlassian.crowd.dao.util.DatabaseFileLocatorImpl">
        <constructor-arg ref="homeLocator"/>
        <constructor-arg value="directories.properties"/>
    </bean>

    <bean name="directoryPropertiesMapper"
          class="com.atlassian.crowd.dao.directory.IntegrityConstraintAwareDirectoryPropertiesMapper">
        <constructor-arg ref="directoryDao"/>
    </bean>

    <bean name="fileBasedDirectoryDao"
          class="com.atlassian.crowd.dao.directory.DirectoryDAOFile"
          autowire-candidate="false">
        <constructor-arg ref="directoryDatabaseFileLocator"/>
        <constructor-arg ref="directoryPropertiesMapper"/>
    </bean>

    <bean id="directoryManager" plugin:available="true" parent="profilingProxy">
        <property name="target">
            <bean class="com.atlassian.crowd.manager.directory.DirectoryManagerGeneric">
                <constructor-arg index="0">
                    <bean class="com.atlassian.crowd.dao.directory.FailoverDirectoryDAO">
                        <constructor-arg index="0" ref="fileBasedDirectoryDao"/>
                        <constructor-arg index="1" ref="directoryDao"/>
                        <constructor-arg index="2" ref="fileBasedDirectoryDao"/>
                    </bean>
                </constructor-arg>
                <constructor-arg index="1" ref="applicationDao"/>
                <constructor-arg index="2" ref="eventPublisher"/>
                <constructor-arg index="3" ref="permissionManager"/>
                <constructor-arg index="4" ref="passwordHelper"/>
                <constructor-arg index="5" ref="directoryInstanceLoader"/>
                <constructor-arg index="6" ref="directorySynchroniser"/>
                <constructor-arg index="7" ref="directoryPollerManager"/>
                <constructor-arg index="8" ref="directoryLockManager"/>
                <constructor-arg index="9" ref="synchronisationStatusManager"/>
            </bean>
        </property>
    </bean>

    <bean id="directoryPollerManager"
          class="com.atlassian.crowd.manager.directory.monitor.poller.SpringQuartzDirectoryPollerManager"/>

    <bean id="directoryLockManager" class="com.atlassian.crowd.manager.lock.DirectoryLockManager"/>

    <bean id="synchronisationStatusManager" class="com.atlassian.crowd.manager.directory.SynchronisationStatusManagerImpl">
        <constructor-arg>
            <bean class="com.atlassian.crowd.manager.directory.DirectorySynchronisationInformationStoreImpl"/>
        </constructor-arg>
        <constructor-arg ref="eventPublisher"/>
    </bean>

    <bean id="directoryCacheFactory" class="com.atlassian.crowd.directory.TransactionalDirectoryCacheFactory">
        <constructor-arg index="0" ref="directoryDao"/>
        <constructor-arg index="1" ref="synchronisationStatusManager"/>
        <constructor-arg index="2" ref="eventPublisher"/>
        <constructor-arg index="3">
            <bean id="transactionInterceptor" class="org.springframework.transaction.interceptor.TransactionInterceptor">
                <property name="transactionManager" ref="transactionManager"/>
                <property name="transactionAttributes">
                    <props>
                        <prop key="*">
                            PROPAGATION_REQUIRES_NEW
                        </prop>
                    </props>
                </property>
            </bean>
        </constructor-arg>
    </bean>

    <bean id="directorySynchroniser" class="com.atlassian.crowd.manager.directory.DirectorySynchroniserImpl">
        <constructor-arg ref="directoryLockManager"/>
        <constructor-arg ref="directorySynchroniserHelper"/>
        <constructor-arg ref="synchronisationStatusManager"/>
        <constructor-arg ref="eventPublisher"/>
    </bean>

    <bean id="directorySynchroniserHelper" parent="profilingProxy" plugin:available="true">
        <property name="target">
            <bean class="com.atlassian.crowd.manager.directory.DirectorySynchroniserHelperImpl">
                <constructor-arg ref="directoryDao"/>
                <constructor-arg ref="eventPublisher"/>
            </bean>
        </property>
    </bean>

    <bean id="permissionManager" parent="profilingProxy" plugin:available="true">
        <property name="target">
            <bean class="com.atlassian.crowd.manager.permission.PermissionManagerImpl">
                <constructor-arg index="0" ref="applicationDao"/>
                <constructor-arg index="1" ref="directoryDao"/>
            </bean>
        </property>
    </bean>

    <bean id="aliasManager" plugin:available="true" parent="profilingProxy">
        <property name="target">
            <bean class="com.atlassian.crowd.manager.application.AliasManagerImpl">
                <constructor-arg ref="aliasDao"/>
                <constructor-arg ref="applicationServiceInternal"/>
                <constructor-arg ref="eventPublisher"/>
            </bean>
        </property>
    </bean>

    <bean id="webhookRegistry" plugin:available="true" parent="profilingProxy">
        <property name="target">
            <bean class="com.atlassian.crowd.manager.webhook.WebhookRegistryImpl">
                <constructor-arg ref="webhookDao"/>
            </bean>
        </property>
    </bean>

    <bean id="webhookService" class="com.atlassian.crowd.manager.webhook.WebhookServiceImpl">
        <constructor-arg ref="webhookRegistry"/>
        <constructor-arg ref="webhookPinger"/>
        <constructor-arg>
            <bean class="com.atlassian.crowd.manager.webhook.WebhookNotificationListenerImpl">
                <constructor-arg ref="webhookRegistry"/>
                <constructor-arg>
                    <bean class="com.atlassian.crowd.manager.webhook.NoLongTermFailureStrategy">
                        <constructor-arg index="0" value="50"/>
                        <constructor-arg index="1" value="21600000" /> <!-- 6 hours in millis -->
                    </bean>
                </constructor-arg>
            </bean>
        </constructor-arg>
        <constructor-arg>
            <bean class="com.atlassian.crowd.manager.webhook.KeyedExecutor">  <!-- type erasure in action -->
                <constructor-arg>
                    <bean class="org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor">
                        <property name="maxPoolSize" value="8"/>
                        <property name="threadNamePrefix" value="webhookPinger"/>
                    </bean>
                </constructor-arg>
            </bean>
        </constructor-arg>
    </bean>

    <bean id="webhookPinger" class="com.atlassian.crowd.manager.webhook.WebhookPinger">
        <constructor-arg>
            <bean class="org.apache.commons.httpclient.HttpClient"/>
        </constructor-arg>
    </bean>

    <bean id="hostApplicationControl" class="com.atlassian.crowd.DefaultHostApplicationControl"/>
</beans>
