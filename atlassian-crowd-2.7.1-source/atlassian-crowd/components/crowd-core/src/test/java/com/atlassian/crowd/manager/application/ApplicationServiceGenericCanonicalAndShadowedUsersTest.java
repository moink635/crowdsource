package com.atlassian.crowd.manager.application;

import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.user.User;

import org.junit.Before;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

/**
 * Responsible for testing the canonical and shadowed user detection algorithms
 * in the {@link ApplicationServiceGeneric} class.
 *
 */
public class ApplicationServiceGenericCanonicalAndShadowedUsersTest extends ApplicationServiceTestCase
{
    private ApplicationServiceGeneric applicationService;

    @Before
    public void setUp()
    {
        super.setUp();
        applicationService = new ApplicationServiceGeneric(
                                directoryManager, permissionManager, directoryInstanceLoader, mockEventPublisher,
                                mockEventStore, webhookRegistry);
    }

    /**
     * Users can be {@code null} when, for instance, the passed in user is &quot;anonymous&quot;
     */
    @Test
    public void nullUsersShouldAlwaysBeCanonical()
    {
        assertTrue(applicationService.isCanonical(application, null));
    }

    @Test
    public void aUserIsShadowedIfThereExistsAnotherUserWithTheSameUserNameInADirectoryOfHigherOrder() throws Exception
    {
        final User canonicalUser = mockUser("test-user", DIRECTORY1_ID);
        final User shadowedUser = mockUser("test-user", DIRECTORY2_ID);

        when(application.getDirectoryMappings()).thenReturn(asList(directoryMapping1, directoryMapping2, directoryMapping3));
        when(directoryManager.findUserByName(DIRECTORY1_ID, "test-user")).thenReturn(canonicalUser);
        when(directoryManager.findUserByName(DIRECTORY2_ID, "test-user")).thenReturn(shadowedUser);
        when(directoryManager.findUserByName(DIRECTORY3_ID, "unique")).thenThrow(UserNotFoundException.class);

        assertFalse(applicationService.isCanonical(application, shadowedUser));
        assertTrue(applicationService.isCanonical(application, canonicalUser));
    }

    @Test
    public void aUserIsAlwaysCanonicalIfItsUserNameIsDefinedOnlyInOnlyOneOfTheDirectories() throws Exception
    {
        final User uniqueAcrossDirectories = mockUser("unique", DIRECTORY2_ID);

        when(application.getDirectoryMappings()).thenReturn(asList(directoryMapping1, directoryMapping2, directoryMapping3));

        when(directoryManager.findUserByName(DIRECTORY1_ID, "unique")).thenThrow(UserNotFoundException.class);
        when(directoryManager.findUserByName(DIRECTORY2_ID, "unique")).thenReturn(uniqueAcrossDirectories);
        when(directoryManager.findUserByName(DIRECTORY3_ID, "unique")).thenThrow(UserNotFoundException.class);

        assertTrue(applicationService.isCanonical(application, uniqueAcrossDirectories));
    }

    @Test
    public void usersThatCanNotBeFoundInAnyDirectoryAreNeverCanonical() throws Exception
    {
        final User ghost = mockUser("ghost-user", DIRECTORY2_ID);

        when(application.getDirectoryMappings()).thenReturn(asList(directoryMapping1, directoryMapping2, directoryMapping3));

        when(directoryManager.findUserByName(DIRECTORY1_ID, "ghost-user")).thenThrow(UserNotFoundException.class);
        when(directoryManager.findUserByName(DIRECTORY2_ID, "ghost-user")).thenThrow(UserNotFoundException.class);
        when(directoryManager.findUserByName(DIRECTORY3_ID, "ghost-user")).thenThrow(UserNotFoundException.class);

        assertFalse(applicationService.isCanonical(application, ghost));
    }
}
