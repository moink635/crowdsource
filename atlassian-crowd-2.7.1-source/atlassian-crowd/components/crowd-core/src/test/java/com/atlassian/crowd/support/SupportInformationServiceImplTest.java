package com.atlassian.crowd.support;

import java.util.Date;

import com.atlassian.crowd.dao.directory.ImmutableDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.ImmutableApplication;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SupportInformationServiceImplTest
{
    private SupportInformationServiceImpl supportInformationService;

    @Mock private DirectoryManager directoryManager;

    @Before
    public void createObjectUnderTest() throws Exception
    {
        supportInformationService = new SupportInformationServiceImpl(directoryManager);
    }

    @Test
    public void supportInformationShouldContainCurrentUserInformation() throws Exception
    {
        User user = new UserTemplate("username", "firstName", "lastName", "displayName");

        String supportInformation = supportInformationService.getSupportInformation(user);

        assertThat(supportInformation, containsString("=== Current user ==="));
        assertThat(supportInformation, containsString("Username: username"));
    }

    @Test
    public void supportInformationShouldContainDirectoryConfiguration() throws Exception
    {
        Directory directory = new ImmutableDirectory(1L, "My Directory", true, "description", "encryptionType",
                                                     DirectoryType.CUSTOM, "implementation.Class", new Date(),
                                                     new Date(), ImmutableSet.<OperationType>of(),
                                                     ImmutableMap.<String, String>of());
        when(directoryManager.findAllDirectories()).thenReturn(ImmutableList.of(directory));

        String supportInformation = supportInformationService.getSupportInformation(null);

        assertThat(supportInformation, containsString("=== Directories configured ==="));
        assertThat(supportInformation, containsString("Name: My Directory"));
    }

    @Test
    public void supportInformationShouldContainApplicationConfiguration() throws Exception
    {
        Directory directory = new ImmutableDirectory(1L, "My Directory", true, "description", "encryptionType",
                DirectoryType.CUSTOM, "implementation.Class", new Date(),
                new Date(), ImmutableSet.<OperationType>of(),
                ImmutableMap.<String, String>of());

        DirectoryMapping directoryMapping = new DirectoryMapping(3L, null, directory, true);
        directoryMapping.addGroupMapping("my group");

        Application application = ImmutableApplication.builder("My Application", ApplicationType.GENERIC_APPLICATION)
                                                      .setDescription("description")
                                                      .setPasswordCredential(PasswordCredential.unencrypted("secret"))
                                                      .setDirectoryMappings(ImmutableList.of(directoryMapping))
                                                      .setRemoteAddresses(ImmutableSet.of(new RemoteAddress("an.ip.address")))
                                                      .build();

        ApplicationManager applicationManager = mock(ApplicationManager.class);
        when(applicationManager.findAll()).thenReturn(ImmutableList.of(application));
        supportInformationService.setApplicationManager(applicationManager);

        String supportInformation = supportInformationService.getSupportInformation(null);

        assertThat(supportInformation, containsString("=== Applications configured ==="));
        assertThat(supportInformation, containsString("Name: My Application"));
        assertThat(supportInformation, containsString("Remote addresses: [an.ip.address]"));
        assertThat(supportInformation, not(containsString("secret")));

        // directory mappings
        assertThat(supportInformation, containsString("Mapped to directory ID: 1"));
        assertThat(supportInformation, containsString("Mapped groups: [my group]"));
    }

    private static final String USER_FULL_NAME = "user-full-name";

    @Test
    public void testDisplaysAllDirectoriesAndCurrentUser()
    {
        User user = mock(User.class);
        when(user.getDisplayName()).thenReturn(USER_FULL_NAME);

        Directory directory1 = mock(Directory.class);
        when(directory1.getName()).thenReturn("Directory 1");
        Directory directory2 = mock(Directory.class);
        when(directory2.getName()).thenReturn("Directory 2");
        when(directoryManager.findAllDirectories()).thenReturn(Lists.newArrayList(directory1, directory2));

        String result = supportInformationService.getSupportInformation(user);

        assertThat(result, containsString(USER_FULL_NAME));
        assertThat(result, containsString("Directory 1"));
        assertThat(result, containsString("Directory 2"));
    }

    @Test
    public void testNullDirectoryAndUserDoNotThrowAnException()
    {
        String result = supportInformationService.getSupportInformation(null);
        assertThat(result, equalTo("=== Directories configured ===\n"));
    }
}
