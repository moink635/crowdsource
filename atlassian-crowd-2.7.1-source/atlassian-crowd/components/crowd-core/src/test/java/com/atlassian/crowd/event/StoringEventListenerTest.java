package com.atlassian.crowd.event;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.event.application.ApplicationDirectoryAddedEvent;
import com.atlassian.crowd.event.application.ApplicationDirectoryOrderUpdatedEvent;
import com.atlassian.crowd.event.application.ApplicationDirectoryRemovedEvent;
import com.atlassian.crowd.event.application.ApplicationUpdatedEvent;
import com.atlassian.crowd.event.directory.DirectoryDeletedEvent;
import com.atlassian.crowd.event.directory.DirectoryUpdatedEvent;
import com.atlassian.crowd.event.group.GroupAttributeDeletedEvent;
import com.atlassian.crowd.event.group.GroupAttributeStoredEvent;
import com.atlassian.crowd.event.group.GroupCreatedEvent;
import com.atlassian.crowd.event.group.GroupDeletedEvent;
import com.atlassian.crowd.event.group.GroupMembershipCreatedEvent;
import com.atlassian.crowd.event.group.GroupMembershipDeletedEvent;
import com.atlassian.crowd.event.group.GroupUpdatedEvent;
import com.atlassian.crowd.event.migration.XMLRestoreFinishedEvent;
import com.atlassian.crowd.event.user.UserAttributeDeletedEvent;
import com.atlassian.crowd.event.user.UserAttributeStoredEvent;
import com.atlassian.crowd.event.user.UserCreatedEvent;
import com.atlassian.crowd.event.user.UserDeletedEvent;
import com.atlassian.crowd.event.user.UserRenamedEvent;
import com.atlassian.crowd.event.user.UserUpdatedEvent;
import com.atlassian.crowd.manager.webhook.WebhookService;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.event.AliasEvent;
import com.atlassian.crowd.model.event.GroupEvent;
import com.atlassian.crowd.model.event.GroupMembershipEvent;
import com.atlassian.crowd.model.event.Operation;
import com.atlassian.crowd.model.event.OperationEvent;
import com.atlassian.crowd.model.event.UserEvent;
import com.atlassian.crowd.model.event.UserMembershipEvent;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.event.api.EventPublisher;

import com.google.common.base.Objects;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.event.StoringEventListenerTest.GroupEventMatcher.isGroupEvent;
import static com.atlassian.crowd.event.StoringEventListenerTest.GroupMembershipEventMatcher.isGroupMembershipEvent;
import static com.atlassian.crowd.event.StoringEventListenerTest.UserEventMatcher.isUserEvent;
import static com.atlassian.crowd.event.StoringEventListenerTest.UserMembershipEventMatcher.isUserMembershipEvent;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StoringEventListenerTest
{
    private static final Long DIRECTORY_ID = 1L;

    @Mock private EventStore eventStore;
    @Mock private WebhookService webhookService;
    @Mock private EventPublisher eventPublisher;
    @Mock private Directory directory;
    @Mock private Application application;
    private User user;
    private Group group;

    @Before
    public void createAuxiliaryObjects()
    {
        user = new UserTemplate("username", DIRECTORY_ID);
        group = new GroupTemplate("groupname", DIRECTORY_ID);
    }

    @Test
    public void shouldRegisterItselfWithTheEventPublisher() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        verify(eventPublisher).register(storingEventListener);
    }

    // USER EVENTS

    @Test
    public void testHandleUserCreatedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new UserCreatedEvent(null, directory, user));

        verify(eventStore).storeEvent(argThat(isUserEvent(Operation.CREATED, directory, user)));
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleUserUpdatedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new UserUpdatedEvent(null, directory, user));

        verify(eventStore).storeEvent(argThat(isUserEvent(Operation.UPDATED, directory, user)));
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleUserAttributeStoredEventWithoutIgnoringAttributes() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        Map<String, Set<String>> newAttributes =
                ImmutableMap.<String, Set<String>>of("key", ImmutableSet.of("value1", "value2"));
        storingEventListener.handleEvent(new UserAttributeStoredEvent(null, directory, user, newAttributes));

        // the StoringEventListener is configured to store the changes in attributes
        verify(eventStore).storeEvent(argThat(isUserEvent(Operation.UPDATED, directory, user, newAttributes, null)));
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleUserAttributeStoredEventIgnoringAttributes() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, true);

        Map<String, Set<String>> newAttributes =
                ImmutableMap.<String, Set<String>>of("key", ImmutableSet.of("value1", "value2"));
        storingEventListener.handleEvent(new UserAttributeStoredEvent(null, directory, user, newAttributes));

        // the StoringEventListener is configured to ignore the changes in attributes
        verifyZeroInteractions(eventStore);
        verifyZeroInteractions(webhookService);
    }

    @Test
    public void testHandleUserAttributeDeletedEventWithoutIgnoringAttributes() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new UserAttributeDeletedEvent(null, directory, user, "key"));

        // the StoringEventListener is configured to store the changes in attributes
        verify(eventStore).storeEvent(argThat(isUserEvent(Operation.UPDATED, directory, user, null, ImmutableSet.of("key"))));
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleUserAttributeDeletedEventIgnoringAttributes() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, true);

        storingEventListener.handleEvent(new UserAttributeDeletedEvent(null, directory, user, "key"));

        // the StoringEventListener is configured to ignore the changes in attributes
        verifyZeroInteractions(eventStore);
        verifyZeroInteractions(webhookService);
    }

    @Test
    public void testHandleUserDeletedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        when(directory.getId()).thenReturn(DIRECTORY_ID);

        storingEventListener.handleEvent(new UserDeletedEvent(null, directory, user.getName()));

        verify(eventStore).storeEvent(argThat(isUserEvent(Operation.DELETED, directory, user)));
        verify(webhookService).notifyWebhooks();
    }

    // GROUP EVENTS

    @Test
    public void testHandleGroupCreatedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new GroupCreatedEvent(null, directory, group));

        verify(eventStore).storeEvent(argThat(isGroupEvent(Operation.CREATED, directory, group)));
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleGroupUpdatedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new GroupUpdatedEvent(null, directory, group));

        verify(eventStore).storeEvent(argThat(isGroupEvent(Operation.UPDATED, directory, group)));
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleGroupAttributeStoredEventWithoutIgnoringAttributes() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        Map<String, Set<String>> newAttributes =
                ImmutableMap.<String, Set<String>>of("key", ImmutableSet.of("value1", "value2"));
        storingEventListener.handleEvent(new GroupAttributeStoredEvent(null, directory, group, newAttributes));

        verify(eventStore).storeEvent(argThat(isGroupEvent(Operation.UPDATED, directory, group, newAttributes, null)));
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleGroupAttributeStoredEventIgnoringAttributes() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, true);

        Map<String, Set<String>> newAttributes =
                ImmutableMap.<String, Set<String>>of("key", ImmutableSet.of("value1", "value2"));
        storingEventListener.handleEvent(new GroupAttributeStoredEvent(null, directory, group, newAttributes));

        verifyZeroInteractions(eventStore);
        verifyZeroInteractions(webhookService);
    }

    @Test
    public void testHandleGroupAttributeDeletedEventWithoutIgnoringAttributes() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new GroupAttributeDeletedEvent(null, directory, group, "key"));

        verify(eventStore).storeEvent(argThat(isGroupEvent(Operation.UPDATED, directory, group, null, ImmutableSet.of("key"))));
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleGroupAttributeDeletedEventIgnoringAttributes() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, true);

        storingEventListener.handleEvent(new GroupAttributeDeletedEvent(null, directory, group, "key"));

        verifyZeroInteractions(eventStore);
        verifyZeroInteractions(webhookService);
    }

    @Test
    public void testHandleGroupDeletedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        when(directory.getId()).thenReturn(DIRECTORY_ID);

        storingEventListener.handleEvent(new GroupDeletedEvent(null, directory, group.getName()));

        verify(eventStore).storeEvent(argThat(isGroupEvent(Operation.DELETED, directory, group)));
        verify(webhookService).notifyWebhooks();
    }

    // MEMBERSHIP EVENTS

    @Test
    public void testHandleUserGroupMembershipCreatedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new GroupMembershipCreatedEvent(null, directory, "user", "group", MembershipType.GROUP_USER));

        verify(eventStore).storeEvent(argThat(isUserMembershipEvent(Operation.CREATED, directory, "user", "group")));
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleGroupGroupMembershipCreatedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new GroupMembershipCreatedEvent(null, directory, "group1", "group2", MembershipType.GROUP_GROUP));

        verify(eventStore).storeEvent(argThat(isGroupMembershipEvent(Operation.CREATED, directory, "group1", "group2")));
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleUserGroupMembershipDeletedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new GroupMembershipDeletedEvent(null, directory, "user", "group", MembershipType.GROUP_USER));

        verify(eventStore).storeEvent(argThat(isUserMembershipEvent(Operation.DELETED, directory, "user", "group")));
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleGroupGroupMembershipDeletedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new GroupMembershipDeletedEvent(null, directory, "group1", "group2", MembershipType.GROUP_GROUP));

        verify(eventStore).storeEvent(argThat(isGroupMembershipEvent(Operation.DELETED, directory, "group1", "group2")));
        verify(webhookService).notifyWebhooks();
    }

    // EVENTS THAT INVALIDATE THE EVENT STORE

    /**
     * This behaviour will change when CWD-3384 is implemented for incremental syncs, but for the moment,
     * the right thing to do is to fallback to full sync.
     *
     * @throws Exception
     */
    @Test
    public void shouldInvalidateEventStoreWhenUserIsRenamed() throws Exception
    {
        StoringEventListener storingEventListener =
            new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new UserRenamedEvent(null, directory, user, "oldname"));

        verify(eventStore).invalidateEvents();
        verifyNoMoreInteractions(eventStore);
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleDirectoryUpdatedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new DirectoryUpdatedEvent(null, directory));

        verify(eventStore).invalidateEvents();
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleDirectoryDeletedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new DirectoryDeletedEvent(null, directory));

        verify(eventStore).invalidateEvents();
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleXMLRestoreFinishedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new XMLRestoreFinishedEvent(null));

        verify(eventStore).invalidateEvents();
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleApplicationDirectoryAddedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new ApplicationDirectoryAddedEvent(application, directory));

        verify(eventStore).invalidateEvents();
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleApplicationDirectoryRemovedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new ApplicationDirectoryRemovedEvent(application, directory));

        verify(eventStore).invalidateEvents();
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void testHandleApplicationDirectoryOrderUpdatedEvent() throws Exception
    {
        StoringEventListener storingEventListener =
                new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new ApplicationDirectoryOrderUpdatedEvent(application, directory));

        verify(eventStore).invalidateEvents();
        verify(webhookService).notifyWebhooks();
    }

    @Test
    public void aliasEventShouldBeStored() throws Exception
    {
        StoringEventListener storingEventListener =
            new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        AliasEvent event = AliasEvent.created(mock(Application.class), "user", "alias");

        storingEventListener.handleEvent(event);

        verify(eventStore).storeEvent(event);
    }

    @Test
    public void applicationUpdatedEventShouldInvalidateStore() throws Exception
    {
        StoringEventListener storingEventListener =
            new StoringEventListener(eventStore, eventPublisher, webhookService, false);

        storingEventListener.handleEvent(new ApplicationUpdatedEvent(mock(Application.class)));

        verify(eventStore).invalidateEvents();
    }

    // MATCHERS

    static class UserEventMatcher extends TypeSafeMatcher<UserEvent>
    {
        private final Operation operation;
        private final Directory directory;
        private final User user;
        private final Map<String, Set<String>> storedAttributes;
        private final Set<String> deletedAttributes;

        UserEventMatcher(Operation operation, Directory directory, User user,
                         Map<String, Set<String>> storedAttributes, Set<String> deletedAttributes)
        {
            this.operation = operation;
            this.directory = directory;
            this.user = user;
            this.storedAttributes = storedAttributes;
            this.deletedAttributes = deletedAttributes;
        }

        @Factory
        public static Matcher<? extends OperationEvent> isUserEvent(Operation operation, Directory directory, User user)
        {
            return new UserEventMatcher(operation, directory, user, null, null);
        }

        @Factory
        public static Matcher<? extends OperationEvent> isUserEvent(Operation operation, Directory directory, User user,
                                                                    Map<String,Set<String>> storedAttribures,
                                                                    Set<String> deletedAttributes)
        {
            return new UserEventMatcher(operation, directory, user, storedAttribures, deletedAttributes);
        }

        @Override
        public void describeTo(Description description)
        {
            description.appendText("user " + user + " " + operation + " in directory " + directory
                + " with stored attributes " + storedAttributes + " and deleted attributes " + deletedAttributes);
        }

        @Override
        protected boolean matchesSafely(UserEvent userEvent)
        {
            return operation.equals(userEvent.getOperation())
                    && directory.equals(userEvent.getDirectory())
                    && user.equals(userEvent.getUser())
                    && Objects.equal(storedAttributes, userEvent.getStoredAttributes())
                    && Objects.equal(deletedAttributes, userEvent.getDeletedAttributes());
        }
    }

    static class GroupEventMatcher extends TypeSafeMatcher<GroupEvent>
    {
        private final Operation operation;
        private final Directory directory;
        private final Group group;
        private final Map<String, Set<String>> storedAttributes;
        private final Set<String> deletedAttributes;

        GroupEventMatcher(Operation operation, Directory directory, Group group,
                         Map<String, Set<String>> storedAttributes, Set<String> deletedAttributes)
        {
            this.operation = operation;
            this.directory = directory;
            this.group = group;
            this.storedAttributes = storedAttributes;
            this.deletedAttributes = deletedAttributes;
        }

        @Factory
        public static Matcher<? extends OperationEvent> isGroupEvent(Operation operation, Directory directory, Group group)
        {
            return new GroupEventMatcher(operation, directory, group, null, null);
        }

        @Factory
        public static Matcher<? extends OperationEvent> isGroupEvent(Operation operation, Directory directory, Group group,
                                                                     Map<String,Set<String>> storedAttribures,
                                                                     Set<String> deletedAttributes)
        {
            return new GroupEventMatcher(operation, directory, group, storedAttribures, deletedAttributes);
        }

        @Override
        public void describeTo(Description description)
        {
            description.appendText("group " + group + " " + operation + " in directory " + directory
                    + " with stored attributes " + storedAttributes + " and deleted attributes " + deletedAttributes);
        }

        @Override
        protected boolean matchesSafely(GroupEvent groupEvent)
        {
            return operation.equals(groupEvent.getOperation())
                    && directory.equals(groupEvent.getDirectory())
                    && group.equals(groupEvent.getGroup())
                    && Objects.equal(storedAttributes, groupEvent.getStoredAttributes())
                    && Objects.equal(deletedAttributes, groupEvent.getDeletedAttributes());
        }
    }

    static class UserMembershipEventMatcher extends TypeSafeMatcher<UserMembershipEvent>
    {
        private final Operation operation;
        private final Directory directory;
        private final String childEntity;
        private final String parentEntity;

        UserMembershipEventMatcher(Operation operation, Directory directory, String childEntity, String parentEntity)
        {
            this.operation = operation;
            this.directory = directory;
            this.childEntity = childEntity;
            this.parentEntity = parentEntity;
        }

        @Factory
        public static Matcher<? extends OperationEvent> isUserMembershipEvent(Operation operation, Directory directory,
                                                                              String childEntity, String parentEntity)
        {
            return new UserMembershipEventMatcher(operation, directory, childEntity, parentEntity);
        }

        @Override
        public void describeTo(Description description)
        {
            description.appendText(operation + " membership of user " + childEntity + " to group " + parentEntity
                    + " in directory " + directory);
        }

        @Override
        protected boolean matchesSafely(UserMembershipEvent membershipEvent)
        {
            return operation.equals(membershipEvent.getOperation())
                    && directory.equals(membershipEvent.getDirectory())
                    && childEntity.equals(membershipEvent.getChildUsername())
                    && Collections.singleton(parentEntity).equals(membershipEvent.getParentGroupNames());
        }
    }

    static class GroupMembershipEventMatcher extends TypeSafeMatcher<GroupMembershipEvent>
    {
        private final Operation operation;
        private final Directory directory;
        private final String childEntity;
        private final String parentEntity;

        GroupMembershipEventMatcher(Operation operation, Directory directory, String childEntity, String parentEntity)
        {
            this.operation = operation;
            this.directory = directory;
            this.childEntity = childEntity;
            this.parentEntity = parentEntity;
        }

        @Factory
        public static Matcher<? extends OperationEvent> isGroupMembershipEvent(Operation operation, Directory directory,
                                                                               String childEntity, String parentEntity)
        {
            return new GroupMembershipEventMatcher(operation, directory, childEntity, parentEntity);
        }

        @Override
        public void describeTo(Description description)
        {
            description.appendText(operation + " membership of group " + childEntity + " to group " + parentEntity
                    + " in directory " + directory);
        }

        @Override
        protected boolean matchesSafely(GroupMembershipEvent membershipEvent)
        {
            return operation.equals(membershipEvent.getOperation())
                    && directory.equals(membershipEvent.getDirectory())
                    && childEntity.equals(membershipEvent.getGroupName())
                    && Collections.singleton(parentEntity).equals(membershipEvent.getParentGroupNames());
        }
    }
}
