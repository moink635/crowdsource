package com.atlassian.crowd.directory;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.TreeSet;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.InvalidUserException;

import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.user.TimestampedUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsEmptyCollection;

import static com.atlassian.crowd.test.matchers.UserMatcher.userNamed;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class DbCachingRemoteChangeOperationsTest
{
    private DbCachingRemoteChangeOperations dbCachingRemoteChangeOperations;

    @Mock
    private InternalRemoteDirectory internalDirectory;
    @Mock
    private SynchronisationStatusManager synchronisationStatusManager;
    @Mock
    private DirectoryDao directoryDao;
    @Mock
    private  Directory directory;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private MockRemoteDirectory mockRemoteDirectory;

    @Before
    public void createObjectUnderTest()
    {
        mockRemoteDirectory = new MockRemoteDirectory();
        dbCachingRemoteChangeOperations = new DbCachingRemoteChangeOperations(directoryDao, mockRemoteDirectory, internalDirectory, synchronisationStatusManager, null);
    }

    @Test
    public void testFindUserMembershipForGroupChanges() throws Exception
    {
        when(internalDirectory.searchGroupRelationships(any(MembershipQuery.class)))
                .thenReturn(Arrays.asList("jack", "jill", "fred"));

        DirectoryCacheChangeOperations.AddRemoveSets<String> changes = dbCachingRemoteChangeOperations
                .findUserMembershipForGroupChanges(new GroupTemplate("group"), Arrays.asList("jack", "Jill", "mary"));

        assertEquals(Collections.singleton("mary"), changes.toAdd);
        assertEquals(Collections.singleton("fred"), changes.toRemove);
    }

    @Test
    public void testFindGroupMembershipForGroupChanges() throws Exception
    {
        when(internalDirectory.searchGroupRelationships(any(MembershipQuery.class)))
                .thenReturn(Arrays.asList("child1", "child2", "child3"));

        DirectoryCacheChangeOperations.AddRemoveSets<String> changes = dbCachingRemoteChangeOperations
                .findGroupMembershipForGroupChanges(new GroupTemplate("parent"),
                        Arrays.asList("child1", "Child2", "child4"));

        assertEquals(Collections.singleton("child4"), changes.toAdd);
        assertEquals(Collections.singleton("child3"), changes.toRemove);
    }

    @Test (expected = OperationFailedException.class)
    public void addUserToGroupFailsWhenMembershipAlreadyExists() throws Exception
    {
        doThrow(new MembershipAlreadyExistsException("user", "group"))
                .when(internalDirectory).addUserToGroup("user", "group");

        dbCachingRemoteChangeOperations.addUserToGroup("user", "group");
    }

    @Test (expected = OperationFailedException.class)
    public void addGroupToGroupFailsWhenMembershipAlreadyExists() throws Exception
    {
        doThrow(new MembershipAlreadyExistsException("child", "parent"))
                .when(internalDirectory).addGroupToGroup("child", "parent");

        dbCachingRemoteChangeOperations.addGroupToGroup("child", "parent");
    }

    @Test
    public void ldapRenameTriggersRenameWhenNameChanged()
            throws OperationFailedException, DirectoryNotFoundException, UserNotFoundException, InvalidUserException,
            UserAlreadyExistsException
    {
        Directory dir = mock(Directory.class);
        UserTemplate user = mock(UserTemplate.class);
        Set<UserTemplate> users = ImmutableSet.of(user);

        when(user.getName()).thenReturn("NewName");
        when(user.getDirectoryId()).thenReturn(Long.valueOf(1));
        when(user.getExternalId()).thenReturn("eid");

        when(directoryDao.findById(1)).thenReturn(dir);

        TimestampedUser oldUser = mock(TimestampedUser.class);
        when(oldUser.getName()).thenReturn("OldName");

        when(internalDirectory.findUserByExternalId("eid")).thenReturn(oldUser);

        dbCachingRemoteChangeOperations.updateUsers(users);

        verify(internalDirectory).forceRenameUser(oldUser, "NewName");
    }

    @Test
    public void ldapRenameDoesntTriggerInternalDirectoryRename() throws Exception
    {
        Directory dir = mock(Directory.class);
        UserTemplate user = mock(UserTemplate.class);
        Set<UserTemplate> users = ImmutableSet.of(user);

        final String unchangedName = "UnchangedName";
        when(user.getName()).thenReturn(unchangedName);
        when(user.getDirectoryId()).thenReturn(Long.valueOf(1));
        when(user.getExternalId()).thenReturn("eid");

        when(directoryDao.findById(1)).thenReturn(dir);

        TimestampedUser oldUser = mock(TimestampedUser.class);
        when(oldUser.getName()).thenReturn(unchangedName);

        when(internalDirectory.findUserByExternalId("eid")).thenReturn(oldUser);

        dbCachingRemoteChangeOperations.updateUsers(users);

        verify(internalDirectory, never()).renameUser(anyString(), eq(unchangedName));
        verify(internalDirectory, never()).forceRenameUser(any(User.class), eq(unchangedName));
    }

    // when bob is deleted and john is renamed to bob, synchronization should work
    @Test
    public void deleteAndRenameShouldWork()
            throws DirectoryNotFoundException, UserNotFoundException, OperationFailedException,
            UserAlreadyExistsException, InvalidUserException
    {
        Directory dir = mock(Directory.class);
        UserTemplate user = mock(UserTemplate.class);
        Set<UserTemplate> users = ImmutableSet.of(user);

        when(user.getName()).thenReturn("NewName");
        when(user.getDirectoryId()).thenReturn(Long.valueOf(1));
        when(user.getExternalId()).thenReturn("eid");

        when(directoryDao.findById(1)).thenReturn(dir);

        TimestampedUser oldUser = mock(TimestampedUser.class);
        when(oldUser.getName()).thenReturn("OldName");

        when(internalDirectory.findUserByExternalId("eid")).thenReturn(oldUser);
        TimestampedUser existingUser = mock(TimestampedUser.class);
        when(existingUser.getName()).thenReturn("NewName");
        when(existingUser.getDirectoryId()).thenReturn(Long.valueOf(1));
        when(existingUser.getExternalId()).thenReturn("a1");
        when(internalDirectory.findUserByName("NewName")).thenReturn(existingUser);

        dbCachingRemoteChangeOperations.updateUsers(users);
        verify(internalDirectory).forceRenameUser(oldUser, "NewName");
        verify(internalDirectory).updateUser(user);
    }

    // when john was renamed to bob and at the same time bob was renamed to john, synchronization should work
    @Test
    public void ldapDoubleRenameShouldWork()
            throws DirectoryNotFoundException, UserNotFoundException, OperationFailedException,
            UserAlreadyExistsException, InvalidUserException
    {

        UserTemplate user1 = new UserTemplate("john", 1);
        user1.setExternalId("a1");
        UserTemplate user2 = new UserTemplate("bob", 1);
        user2.setExternalId("b2");
        // Guarantee the order of the objects in the set so the test is not flakey - Mockito and other mock frameworks
        // are brittle with ordering of operations.
        Set<UserTemplate> users = new TreeSet<UserTemplate>();
        users.add(user1);
        users.add(user2);

        when(directoryDao.findById(1)).thenReturn(directory);

        TimestampedUser oldUser1 = mock(TimestampedUser.class);
        when(oldUser1.getName()).thenReturn("bob");
        when(internalDirectory.findUserByExternalId("a1")).thenReturn(oldUser1);

        TimestampedUser oldUser2 = mock(TimestampedUser.class);
        when(oldUser2.getName()).thenReturn("john");
        when(internalDirectory.findUserByExternalId("b2")).thenReturn(oldUser2);

        dbCachingRemoteChangeOperations.updateUsers(users);
        verify(internalDirectory).forceRenameUser(oldUser1, "john");
        verify(internalDirectory).updateUser(user1);
        verify(internalDirectory).forceRenameUser(oldUser2, "bob");
        verify(internalDirectory).updateUser(user2);
    }

    @Test
    public void ldapRenameShouldNotBreakWhenExternalIdWasNotMatched()
            throws DirectoryNotFoundException, UserNotFoundException, OperationFailedException,
            UserAlreadyExistsException, InvalidUserException
    {
        Directory dir = mock(Directory.class);
        UserTemplate user = mock(UserTemplate.class);
        Set<UserTemplate> users = ImmutableSet.of(user);

        when(user.getName()).thenReturn("NewName");
        when(user.getDirectoryId()).thenReturn(Long.valueOf(1));
        when(user.getExternalId()).thenReturn("eid");

        when(directoryDao.findById(1)).thenReturn(dir);

        TimestampedUser oldUser = mock(TimestampedUser.class);
        when(oldUser.getName()).thenReturn("OldName");

        when(internalDirectory.findUserByExternalId("eid")).thenThrow(UserNotFoundException.class);

        dbCachingRemoteChangeOperations.updateUsers(users);

        verify(internalDirectory).updateUser(user);
    }

    @Test
    public void userShouldBeAddedToDirectoryIfItDoesntExist() throws OperationFailedException
    {
        UserTemplate user = mock(UserTemplate.class);
        Set<UserTemplate> users = ImmutableSet.of(user);

        when(user.getName()).thenReturn("NewName");
        when(user.getDirectoryId()).thenReturn(Long.valueOf(1));
        when(user.getExternalId()).thenReturn("eid");

        when(internalDirectory.searchUsers(org.mockito.Matchers.<EntityQuery<Object>>any())).thenReturn(
                Lists.<Object>newArrayList());

        DirectoryCacheChangeOperations.AddUpdateSets<UserTemplateWithCredentialAndAttributes, UserTemplate> addAndUpdate = dbCachingRemoteChangeOperations
                .getUsersToAddAndUpdate(users, null);

        assertThat(addAndUpdate.getToAddSet(), Matchers.contains(userNamed("NewName")));
        assertThat(addAndUpdate.getToUpdateSet(), Matchers.empty());
    }

    @Test
    public void userShouldBeUpdatedInTheDirectory() throws OperationFailedException
    {
        UserTemplate user = mock(UserTemplate.class);
        Set<UserTemplate> users = ImmutableSet.of(user);

        when(user.getName()).thenReturn("NewName");
        when(user.getDirectoryId()).thenReturn(Long.valueOf(1));
        when(user.getExternalId()).thenReturn("eid");

        TimestampedUser timestampedUser = mock(TimestampedUser.class);
        when(timestampedUser.getName()).thenReturn("NewName");

        when(internalDirectory.searchUsers(org.mockito.Matchers.<EntityQuery<Object>>any())).thenReturn(
                Lists.<Object>newArrayList(timestampedUser));

        DirectoryCacheChangeOperations.AddUpdateSets<UserTemplateWithCredentialAndAttributes, UserTemplate> addAndUpdate = dbCachingRemoteChangeOperations
                .getUsersToAddAndUpdate(users, null);

        assertThat(addAndUpdate.getToAddSet(), Matchers.empty());
        assertThat(addAndUpdate.getToUpdateSet(), Matchers.contains(userNamed("NewName")));
    }

    @Test
    public void userShouldNotBeUpdatedBecauseItsAlreadyInTheCache() throws OperationFailedException
    {
        UserTemplate user = mock(UserTemplate.class);
        Set<UserTemplate> users = ImmutableSet.of(user);

        when(user.getName()).thenReturn("NewName");
        when(user.getDirectoryId()).thenReturn(Long.valueOf(1));
        when(user.getExternalId()).thenReturn("eid");

        TimestampedUser timestampedUser = mock(TimestampedUser.class);
        when(timestampedUser.getName()).thenReturn("NewName");
        when(timestampedUser.getDirectoryId()).thenReturn(Long.valueOf(1));
        when(timestampedUser.getExternalId()).thenReturn("eid");

        when(internalDirectory.searchUsers(org.mockito.Matchers.<EntityQuery<Object>>any())).thenReturn(
                Lists.<Object>newArrayList(timestampedUser));

        DirectoryCacheChangeOperations.AddUpdateSets<UserTemplateWithCredentialAndAttributes, UserTemplate> addAndUpdate = dbCachingRemoteChangeOperations
                .getUsersToAddAndUpdate(users, null);

        assertThat(addAndUpdate.getToAddSet(), IsEmptyCollection.empty());
        assertThat(addAndUpdate.getToUpdateSet(), IsEmptyCollection.empty());
    }

    @Test
    public void testHasChanged() throws OperationFailedException
    {
        UserTemplate remoteUser = new UserTemplate("remoteUser");
        UserTemplate internalUser = new UserTemplate("internalUser");

        assertFalse(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser)); // name triggers change only when externalId is set for both

        remoteUser.setExternalId("eid");
        assertTrue(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser)); // external id are different

        internalUser.setExternalId("eid");
        assertTrue(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser)); // name triggers change

        internalUser.setName("remoteUser");
        assertFalse(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser));

        internalUser.setFirstName("First");
        assertTrue(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser));

        remoteUser.setFirstName("First");
        assertFalse(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser));

        internalUser.setLastName("Last");
        assertTrue(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser));

        remoteUser.setLastName("Last");
        assertFalse(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser));

        internalUser.setDisplayName("Display name");
        assertTrue(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser));

        remoteUser.setDisplayName("Display name");
        assertFalse(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser));

        internalUser.setEmailAddress("pniewiadomski@atlassian.com");
        assertTrue(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser));

        remoteUser.setEmailAddress("pniewiadomski@atlassian.com");
        assertFalse(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser));

        mockRemoteDirectory.setSupportsInactiveAccounts(false);
        internalUser.setActive(true);
        assertFalse(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser));

        mockRemoteDirectory.setSupportsInactiveAccounts(true);
        assertTrue(dbCachingRemoteChangeOperations.hasChanged(remoteUser, internalUser));
    }

    @Test
    public void shouldNotUpdateDisabledUserIfRemoteDirectoryDoesNotSupportInactiveUsers()
        throws Exception
    {
        UserTemplate remoteUser = mock(UserTemplate.class);
        when(remoteUser.getName()).thenReturn("user");
        when(remoteUser.isActive()).thenReturn(true);

        TimestampedUser internalUser = mock(TimestampedUser.class);
        when(internalUser.getName()).thenReturn("user");
        when(internalUser.isActive()).thenReturn(false);

        when(internalDirectory.searchUsers(any(EntityQuery.class))).thenReturn(ImmutableList.of(internalUser));

        mockRemoteDirectory.setSupportsInactiveAccounts(false);

        DirectoryCacheChangeOperations.AddUpdateSets<UserTemplateWithCredentialAndAttributes,UserTemplate>
            addAndUpdate =
            dbCachingRemoteChangeOperations.getUsersToAddAndUpdate(ImmutableList.of(remoteUser), null);

        assertThat(addAndUpdate.getToAddSet(), Matchers.empty());
        assertThat(addAndUpdate.getToUpdateSet(), Matchers.empty());
    }

    @Test
    public void shouldUpdateUserButPreserveLocallyDisabledUserIfRemoteDirectoryDoesNotSupportInactiveUsers()
        throws Exception
    {
        UserTemplate remoteUser = mock(UserTemplate.class);
        when(remoteUser.getName()).thenReturn("user");
        when(remoteUser.getEmailAddress()).thenReturn("email@example.test");
        when(remoteUser.isActive()).thenReturn(true);

        TimestampedUser internalUser = mock(TimestampedUser.class);
        when(internalUser.getName()).thenReturn("user");
        when(internalUser.isActive()).thenReturn(false);

        when(internalDirectory.searchUsers(any(EntityQuery.class))).thenReturn(ImmutableList.of(internalUser));

        mockRemoteDirectory.setSupportsInactiveAccounts(false);

        DirectoryCacheChangeOperations.AddUpdateSets<UserTemplateWithCredentialAndAttributes,UserTemplate>
            addAndUpdate =
            dbCachingRemoteChangeOperations.getUsersToAddAndUpdate(ImmutableList.of(remoteUser), null);

        assertThat(addAndUpdate.getToAddSet(), Matchers.empty());
        assertThat(addAndUpdate.getToUpdateSet(), Matchers.hasSize(1));
        UserTemplate userToUpdate = Iterables.getOnlyElement(addAndUpdate.getToUpdateSet());
        assertEquals("user", userToUpdate.getName());
        assertEquals("email@example.test", userToUpdate.getEmailAddress());
        assertFalse("Should preserve disabled user in internal directory", userToUpdate.isActive());
    }

    @Test
    public void shouldDisableUserIfRemoteDirectorySupportsItAndLocalActiveUserManagementIsDisabled()
        throws Exception
    {
        UserTemplate remoteUser = mock(UserTemplate.class);
        when(remoteUser.getName()).thenReturn("user");
        when(remoteUser.isActive()).thenReturn(false);

        TimestampedUser internalUser = mock(TimestampedUser.class);
        when(internalUser.getName()).thenReturn("user");
        when(internalUser.isActive()).thenReturn(true);

        when(internalDirectory.searchUsers(any(EntityQuery.class))).thenReturn(ImmutableList.of(internalUser));

        mockRemoteDirectory.setSupportsInactiveAccounts(true);
        when(internalDirectory.isLocalUserStatusEnabled()).thenReturn(false);

        DirectoryCacheChangeOperations.AddUpdateSets<UserTemplateWithCredentialAndAttributes,UserTemplate>
            addAndUpdate =
            dbCachingRemoteChangeOperations.getUsersToAddAndUpdate(ImmutableList.of(remoteUser), null);

        assertThat(addAndUpdate.getToAddSet(), Matchers.empty());
        assertThat("Should disable user in internal directory",
                addAndUpdate.getToUpdateSet(), Matchers.contains(userNamed("user").withActive(false)));
    }

    @Test
    public void shouldNotDisableUserIfRemoteDirectorySupportsItAndLocalActiveUserManagementIsEnabled()
            throws Exception
    {
        UserTemplate remoteUser = mock(UserTemplate.class);
        when(remoteUser.getName()).thenReturn("user");
        when(remoteUser.isActive()).thenReturn(false);

        TimestampedUser internalUser = mock(TimestampedUser.class);
        when(internalUser.getName()).thenReturn("user");
        when(internalUser.isActive()).thenReturn(true);

        when(internalDirectory.searchUsers(any(EntityQuery.class))).thenReturn(ImmutableList.of(internalUser));

        mockRemoteDirectory.setSupportsInactiveAccounts(true);
        when(internalDirectory.isLocalUserStatusEnabled()).thenReturn(true);

        DirectoryCacheChangeOperations.AddUpdateSets<UserTemplateWithCredentialAndAttributes,UserTemplate>
                addAndUpdate =
                dbCachingRemoteChangeOperations.getUsersToAddAndUpdate(ImmutableList.of(remoteUser), null);

        assertThat(addAndUpdate.getToAddSet(), Matchers.empty());
        assertThat("Should not propagate user disabled state from remote server to internal directory",
                addAndUpdate.getToUpdateSet(), Matchers.contains(userNamed("user").withActive(true)));
    }

    @Test
    public void shouldEnableUserIfRemoteDirectorySupportsItAndLocalActiveUserManagementIsDisabled()
        throws Exception
    {
        UserTemplate remoteUser = mock(UserTemplate.class);
        when(remoteUser.getName()).thenReturn("user");
        when(remoteUser.isActive()).thenReturn(true);

        TimestampedUser internalUser = mock(TimestampedUser.class);
        when(internalUser.getName()).thenReturn("user");
        when(internalUser.isActive()).thenReturn(false);

        when(internalDirectory.searchUsers(any(EntityQuery.class))).thenReturn(ImmutableList.of(internalUser));

        mockRemoteDirectory.setSupportsInactiveAccounts(true);
        when(internalDirectory.isLocalUserStatusEnabled()).thenReturn(false);

        DirectoryCacheChangeOperations.AddUpdateSets<UserTemplateWithCredentialAndAttributes,UserTemplate>
            addAndUpdate =
            dbCachingRemoteChangeOperations.getUsersToAddAndUpdate(ImmutableList.of(remoteUser), null);

        assertThat(addAndUpdate.getToAddSet(), Matchers.empty());
        assertThat("Should enable user in internal directory",
                addAndUpdate.getToUpdateSet(), Matchers.contains(userNamed("user").withActive(true)));
    }

    @Test
    public void shouldNotEnableUserIfRemoteDirectorySupportsItAndLocalActiveUserManagementIsEnabled()
        throws Exception
    {
        UserTemplate remoteUser = mock(UserTemplate.class);
        when(remoteUser.getName()).thenReturn("user");
        when(remoteUser.isActive()).thenReturn(true);

        TimestampedUser internalUser = mock(TimestampedUser.class);
        when(internalUser.getName()).thenReturn("user");
        when(internalUser.isActive()).thenReturn(false);

        when(internalDirectory.searchUsers(any(EntityQuery.class))).thenReturn(ImmutableList.of(internalUser));

        mockRemoteDirectory.setSupportsInactiveAccounts(true);
        when(internalDirectory.isLocalUserStatusEnabled()).thenReturn(true);

        DirectoryCacheChangeOperations.AddUpdateSets<UserTemplateWithCredentialAndAttributes,UserTemplate>
            addAndUpdate =
            dbCachingRemoteChangeOperations.getUsersToAddAndUpdate(ImmutableList.of(remoteUser), null);

        assertThat(addAndUpdate.getToAddSet(), Matchers.empty());
        assertThat("Should not propagate user enabled state from remote server to internal directory",
                addAndUpdate.getToUpdateSet(), Matchers.contains(userNamed("user").withActive(false)));
    }

    @Test
    public void shouldNotAddNorUpdateUserIfUpdatedSinceSyncStarted() throws Exception
    {
        Date syncStartedDate = new Date(0);

        UserTemplate remoteUserRequiringUpdate = mock(UserTemplate.class);
        when(remoteUserRequiringUpdate.getName()).thenReturn("user1");
        when(remoteUserRequiringUpdate.getEmailAddress()).thenReturn("email1@example.test");
        when(remoteUserRequiringUpdate.isActive()).thenReturn(true);

        UserTemplate remoteUserAlreadyUpdated = mock(UserTemplate.class);
        when(remoteUserAlreadyUpdated.getName()).thenReturn("user2");
        when(remoteUserAlreadyUpdated.getEmailAddress()).thenReturn("email2@example.test");
        when(remoteUserAlreadyUpdated.isActive()).thenReturn(true);

        TimestampedUser internalUserRequiringUpdate = mock(TimestampedUser.class);
        when(internalUserRequiringUpdate.getName()).thenReturn("user1");
        when(internalUserRequiringUpdate.isActive()).thenReturn(true);
        when(internalUserRequiringUpdate.getUpdatedDate()).thenReturn(new Date(-1));

        TimestampedUser internalUserAlreadyUpdated = mock(TimestampedUser.class);
        when(internalUserAlreadyUpdated.getName()).thenReturn("user2");
        when(internalUserAlreadyUpdated.isActive()).thenReturn(true);
        when(internalUserAlreadyUpdated.getUpdatedDate()).thenReturn(new Date(syncStartedDate.getTime()));

        when(internalDirectory.searchUsers(any(EntityQuery.class))).thenReturn(ImmutableList.of(internalUserRequiringUpdate, internalUserAlreadyUpdated));

        mockRemoteDirectory.setSupportsInactiveAccounts(true);
        when(internalDirectory.isLocalUserStatusEnabled()).thenReturn(true);

        DirectoryCacheChangeOperations.AddUpdateSets<UserTemplateWithCredentialAndAttributes,UserTemplate>
                addAndUpdate =
                dbCachingRemoteChangeOperations.getUsersToAddAndUpdate(ImmutableList.of(remoteUserAlreadyUpdated, remoteUserRequiringUpdate), syncStartedDate);

        assertThat(addAndUpdate.getToAddSet(), Matchers.empty());
        assertThat(addAndUpdate.getToUpdateSet(), Matchers.contains(userNamed("user1")));
    }

    @Test
    public void doNotRemoveUserIfIsPresentInRemoteUsersByName() throws OperationFailedException
    {
        UserTemplate remoteUser = mock(UserTemplate.class);
        when(remoteUser.getName()).thenReturn("user");
        when(remoteUser.isActive()).thenReturn(true);

        TimestampedUser internalUser = mock(TimestampedUser.class);
        when(internalUser.getName()).thenReturn("user");
        when(internalUser.isActive()).thenReturn(true);

        when(internalDirectory.searchUsers(org.mockito.Matchers.<EntityQuery<Object>>any())).thenReturn(ImmutableList.<Object>of(internalUser));

        dbCachingRemoteChangeOperations.deleteCachedUsersNotIn(ImmutableList.of(remoteUser), new Date());

        verify(internalDirectory, never()).removeAllUsers(org.mockito.Matchers.<Set<String>>any());
    }

    @Test
    public void doNotRemoveUserIfIsPresentInRemoteUsersByExternalId()
            throws OperationFailedException, DirectoryNotFoundException
    {
        UserTemplate remoteUser = mock(UserTemplate.class);
        when(remoteUser.getName()).thenReturn("user");
        when(remoteUser.isActive()).thenReturn(true);
        when(remoteUser.getExternalId()).thenReturn("x100");

        UserTemplate remoteUser2 = mock(UserTemplate.class);
        when(remoteUser2.getName()).thenReturn("user2");
        when(remoteUser2.isActive()).thenReturn(true);
        when(remoteUser2.getExternalId()).thenReturn("x101");

        TimestampedUser internalUser = mock(TimestampedUser.class);
        when(internalUser.getName()).thenReturn("other-user");
        when(internalUser.isActive()).thenReturn(true);
        when(internalUser.getExternalId()).thenReturn("x100");

        TimestampedUser internalUser2 = mock(TimestampedUser.class);
        when(internalUser2.getName()).thenReturn("other-user2");
        when(internalUser2.isActive()).thenReturn(true);
        when(internalUser2.getExternalId()).thenReturn("x101");

        when(internalDirectory.searchUsers(org.mockito.Matchers.<EntityQuery<Object>>any())).thenReturn(ImmutableList.<Object>of(internalUser,internalUser2));
        when(directoryDao.findById(mockRemoteDirectory.getDirectoryId())).thenReturn(directory);

        dbCachingRemoteChangeOperations.deleteCachedUsersNotIn(ImmutableList.of(remoteUser, remoteUser2), new Date());
        verify(internalDirectory, never()).removeAllUsers(org.mockito.Matchers.<Set<String>>any());
    }


    @Test
    public void removeUserIfIsNotPresentInRemoteUsersByName()
            throws OperationFailedException, DirectoryNotFoundException
    {
        UserTemplate remoteUser = mock(UserTemplate.class);
        when(remoteUser.getName()).thenReturn("user");
        when(remoteUser.isActive()).thenReturn(true);

        UserTemplate remoteUser2 = mock(UserTemplate.class);
        when(remoteUser2.getName()).thenReturn("user2");
        when(remoteUser2.isActive()).thenReturn(true);

        TimestampedUser internalUser = mock(TimestampedUser.class);
        when(internalUser.getName()).thenReturn("other-user");
        when(internalUser.isActive()).thenReturn(true);

        TimestampedUser internalUser2 = mock(TimestampedUser.class);
        when(internalUser2.getName()).thenReturn("user2");
        when(internalUser2.isActive()).thenReturn(true);

        when(internalDirectory.searchUsers(org.mockito.Matchers.<EntityQuery<Object>>any())).thenReturn(ImmutableList.<Object>of(internalUser,internalUser2));
        when(directoryDao.findById(mockRemoteDirectory.getDirectoryId())).thenReturn(directory);

        dbCachingRemoteChangeOperations.deleteCachedUsersNotIn(ImmutableList.of(remoteUser, remoteUser2), new Date());
        verify(internalDirectory, times(1)).removeAllUsers(org.mockito.Matchers.eq(ImmutableSet.of("other-user")));
    }

    @Test
    public void removeUserIfIsNotPresentInRemoteUsersByExternalId()
            throws OperationFailedException, DirectoryNotFoundException
    {
        UserTemplate remoteUser = mock(UserTemplate.class);
        when(remoteUser.getName()).thenReturn("user");
        when(remoteUser.isActive()).thenReturn(true);
        when(remoteUser.getExternalId()).thenReturn("x100");

        UserTemplate remoteUser2 = mock(UserTemplate.class);
        when(remoteUser2.getName()).thenReturn("user2");
        when(remoteUser2.isActive()).thenReturn(true);
        when(remoteUser2.getExternalId()).thenReturn("x101");

        TimestampedUser internalUser = mock(TimestampedUser.class);
        when(internalUser.getName()).thenReturn("other-user");
        when(internalUser.isActive()).thenReturn(true);
        when(internalUser.getExternalId()).thenReturn("x102");

        TimestampedUser internalUser2 = mock(TimestampedUser.class);
        when(internalUser2.getName()).thenReturn("other-user2");
        when(internalUser2.isActive()).thenReturn(true);
        when(internalUser2.getExternalId()).thenReturn("x101");

        when(internalDirectory.searchUsers(org.mockito.Matchers.<EntityQuery<Object>>any())).thenReturn(ImmutableList.<Object>of(internalUser,internalUser2));
        when(directoryDao.findById(mockRemoteDirectory.getDirectoryId())).thenReturn(directory);

        dbCachingRemoteChangeOperations.deleteCachedUsersNotIn(ImmutableList.of(remoteUser, remoteUser2), new Date());
        verify(internalDirectory, times(1)).removeAllUsers(org.mockito.Matchers.eq(ImmutableSet.of("other-user")));

    }

}
