package com.atlassian.crowd.manager.webhook;

import java.io.IOException;

import com.atlassian.crowd.model.webhook.Webhook;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.PostMethod;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WebhookPingerTest
{
    private WebhookPinger webhookPinger;

    @Mock private HttpClient httpClient;
    @Mock private Webhook webhook;

    @Before
    public void createObjectUnderTest() throws Exception
    {
        webhookPinger = new WebhookPinger(httpClient);
    }

    @Test
    public void shouldIncludeTokenInRequest() throws Exception
    {
        when(webhook.getEndpointUrl()).thenReturn("http://example.test/mywebhook");
        when(webhook.getToken()).thenReturn("secret");

        when(httpClient.executeMethod(any(PostMethod.class))).thenReturn(HttpStatus.SC_NO_CONTENT);

        webhookPinger.ping(webhook);

        ArgumentCaptor<PostMethod> methodCaptor = ArgumentCaptor.forClass(PostMethod.class);
        verify(httpClient).executeMethod(methodCaptor.capture());
        // secret is not hashed
        assertEquals("Basic secret", methodCaptor.getValue().getRequestHeader("Authorization").getValue());
    }

    @Test
    public void testPingReturnsValidStatusCode() throws Exception
    {
        when(webhook.getEndpointUrl()).thenReturn("http://example.test/mywebhook");

        when(httpClient.executeMethod(any(PostMethod.class))).thenReturn(HttpStatus.SC_NO_CONTENT);

        webhookPinger.ping(webhook);

        ArgumentCaptor<PostMethod> methodCaptor = ArgumentCaptor.forClass(PostMethod.class);
        verify(httpClient).executeMethod(methodCaptor.capture());
        assertEquals("http://example.test/mywebhook", methodCaptor.getValue().getURI().toString());
    }

    @Test(expected = IOException.class)
    public void testPingShouldFailWhenEndpointUrlIsInvalid() throws Exception
    {
        when(webhook.getEndpointUrl()).thenReturn("{");

        webhookPinger.ping(webhook);
    }

    @Test(expected = IOException.class)
    public void testPingShouldFailWhenEndpointUrlIsEmpty() throws Exception
    {
        when(webhook.getEndpointUrl()).thenReturn("");

        webhookPinger.ping(webhook);
    }

    @Test (expected = IOException.class)
    public void testPingReturnsInvalidStatusCode() throws Exception
    {
        when(webhook.getEndpointUrl()).thenReturn("http://example.test/this-resource-does-not-exist");

        when(httpClient.executeMethod(any(PostMethod.class))).thenReturn(HttpStatus.SC_NOT_FOUND);

        webhookPinger.ping(webhook);
    }

    @Test (expected = IOException.class)
    public void testPingFails() throws Exception
    {
        when(webhook.getEndpointUrl()).thenReturn("http://invalid-url.test/");

        when(httpClient.executeMethod(any(PostMethod.class))).thenThrow(new IOException("network error"));

        webhookPinger.ping(webhook);
    }
}
