package com.atlassian.crowd.manager.webhook;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;

import com.google.common.util.concurrent.MoreExecutors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class KeyedExecutorTest
{
    @Mock
    private Executor doNothingDelegatedExecutor;

    @Test
    public void queueingRunnablesWithTheSameKeyOnlyExecutesOne() throws Exception
    {
        KeyedExecutor<String> executor = new KeyedExecutor<String>(doNothingDelegatedExecutor);
        executor.execute(mock(Runnable.class), "A");
        executor.execute(mock(Runnable.class), "A");
        executor.execute(mock(Runnable.class), "A");

        verify(doNothingDelegatedExecutor, times(1)).execute(any(Runnable.class));
    }

    @Test
    public void queueingRunnablesWithTheDifferentKeysExecutesAll() throws Exception
    {
        KeyedExecutor<String> executor = new KeyedExecutor<String>(doNothingDelegatedExecutor);
        executor.execute(mock(Runnable.class), "A");
        executor.execute(mock(Runnable.class), "B");
        executor.execute(mock(Runnable.class), "C");

        verify(doNothingDelegatedExecutor, times(3)).execute(any(Runnable.class));
    }

    @Test
    public void shouldQueueRunnableIfPreviousRunnableWithSameKeyHasAlreadyStarted() throws Exception
    {
        final AtomicInteger countA = new AtomicInteger();
        KeyedExecutor<String> executor = new KeyedExecutor<String>(MoreExecutors.sameThreadExecutor());
        for (int i = 0 ; i < 3 ; i++)
        {
            executor.execute(new Runnable()
            {
                @Override
                public void run()
                {
                    countA.incrementAndGet();
                }
            }, "A");
        }

        assertEquals(3, countA.get());
    }

}
