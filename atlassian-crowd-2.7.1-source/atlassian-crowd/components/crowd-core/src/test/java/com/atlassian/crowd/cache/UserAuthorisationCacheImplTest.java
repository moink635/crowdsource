package com.atlassian.crowd.cache;

import com.atlassian.crowd.manager.cache.CacheManager;
import com.google.common.collect.ImmutableList;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class UserAuthorisationCacheImplTest
{
    private static final String CACHE_NAME = "com.atlassian.crowd.userauthorisationcache";
    private static final String APPLICATION_NAME = "Application";
    private static final String USERNAME = "User";

    @Mock private CacheManager cacheManager;

    private UserAuthorisationCache userAuthorisationCache;

    @Before
    public void setUp() throws Exception
    {
        userAuthorisationCache = new UserAuthorisationCacheImpl(cacheManager);
    }

    @Test
    public void setPermittedCachesResult() throws Exception
    {
        userAuthorisationCache.setPermitted(USERNAME, APPLICATION_NAME, true);

        verify(cacheManager).put(CACHE_NAME, ImmutableList.of(USERNAME, APPLICATION_NAME), true);
    }

    @Test
    public void clearByUserInvalidatesUserFromCache() throws Exception
    {
        userAuthorisationCache.clear(USERNAME, APPLICATION_NAME);

        verify(cacheManager).remove(CACHE_NAME, ImmutableList.of(USERNAME, APPLICATION_NAME));
    }
}
