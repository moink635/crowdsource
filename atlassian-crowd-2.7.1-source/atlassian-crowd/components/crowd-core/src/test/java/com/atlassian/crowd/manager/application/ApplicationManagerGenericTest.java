package com.atlassian.crowd.manager.application;

import java.io.Serializable;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.event.application.ApplicationDirectoryRemovedEvent;
import com.atlassian.crowd.event.application.ApplicationUpdatedEvent;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.event.api.EventPublisher;

import com.google.common.collect.ImmutableMap;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ApplicationManagerGenericTest
{
    protected ApplicationManagerGeneric applicationManager = null;
    protected ApplicationImpl application;
    protected DirectoryMapping directoryMapping1;
    protected DirectoryMapping directoryMapping2;
    protected Group group1;
    protected RemoteDirectory mockRemoteDirectory1;
    protected RemoteDirectory mockRemoteDirectory2;
    protected DirectoryImpl directory1;
    protected DirectoryImpl directory2;
    protected ApplicationDAO mockApplicationDAO;
    protected EventPublisher mockEventPublisher;

    protected final String applicationName = "My Test App";

    @Before
    public void setUp() throws Exception
    {
        mockApplicationDAO = mock(ApplicationDAO.class);
        mockEventPublisher = mock(EventPublisher.class);

        applicationManager = new ApplicationManagerGeneric(mockApplicationDAO, null, mockEventPublisher);

        // Objects used by all tests
        application = new ApplicationImpl(new InternalEntityTemplate(1L, applicationName, true, new Date(), new Date()));
        application.setCredential(PasswordCredential.unencrypted("secret"));

        mockRemoteDirectory1 = mock(RemoteDirectory.class);
        directory1 = new DirectoryImpl()
        {
            public RemoteDirectory getImplementation() throws DirectoryInstantiationException
            {
                return mockRemoteDirectory1;
            }

            @Override
            public Long getId()
            {
                return 1L;
            }
        };
        directory1.setName("Test Directory 1");
        directoryMapping1 = new DirectoryMapping(application, directory1, Boolean.FALSE);

        mockRemoteDirectory2 = mock(RemoteDirectory.class);
        directory2 = new DirectoryImpl()
        {
            public RemoteDirectory getImplementation() throws DirectoryInstantiationException
            {
                return mockRemoteDirectory2;
            }

            @Override
            public Long getId()
            {
                return 2L;
            }
        };
        directory2.setName("Test Directory 2");
        directoryMapping2 = new DirectoryMapping(application, directory2, Boolean.FALSE);

        application.addDirectoryMapping(directoryMapping1.getDirectory(), directoryMapping1.isAllowAllToAuthenticate(), directoryMapping1.getAllowedOperations().toArray(new OperationType[directoryMapping1.getAllowedOperations().size()]));
        application.addDirectoryMapping(directoryMapping2.getDirectory(), directoryMapping2.isAllowAllToAuthenticate(), directoryMapping2.getAllowedOperations().toArray(new OperationType[directoryMapping2.getAllowedOperations().size()]));


        group1 = new GroupTemplate("Remote Group", directory1.getId(), GroupType.GROUP);

//        groupMapping1 = new OldGroupMapping(true, directory1, group1.getName());

//        Mock mockDirectoryDao = new Mock(DirectoryDAO.class);
//        mockDirectoryDao.expects(new InvokeAtMostOnceMatcher()).method("findByID").withAnyArguments().will(new ReturnStub(directory1));
//        applicationManager.setDirectoryDAO((DirectoryDAO) mockDirectoryDao.proxy());

//        application.setGroupMappings(EasyList.build(new OldGroupMapping(true, directory1, "test-group")));
        application.addDirectoryMapping(directory1, Boolean.TRUE);
    }

    @Test(expected = ApplicationManagerException.class)
    public void testRemoveCrowdApplicationFails() throws ApplicationManagerException
    {
        Application app = ApplicationImpl.newInstance("test", ApplicationType.CROWD);

        applicationManager.remove(app);
    }

    @Test
    public void testUpdateApplicationSucceeds() throws Exception
    {
        Application app = ApplicationImpl.newInstanceWithIdAndCredential("new", ApplicationType.GENERIC_APPLICATION,
                PasswordCredential.unencrypted("new password"), 1L);
        Application oldApp = ApplicationImpl.newInstanceWithIdAndCredential("old", ApplicationType.GENERIC_APPLICATION,
                PasswordCredential.unencrypted("old password"), 1L);

        when(mockApplicationDAO.findByName("new")).thenThrow(new ApplicationNotFoundException("new"));

        when(mockApplicationDAO.findById(oldApp.getId())).thenReturn(oldApp);

        when(mockApplicationDAO.update(app)).thenReturn(app);

        applicationManager.update(app);

        verify(mockApplicationDAO).update(app);

        verify(mockEventPublisher).publish(new ApplicationUpdatedEvent(app));
    }

    @Test(expected = ApplicationManagerException.class)
    public void testRenamePermanentApplicationFails() throws Exception
    {
        ApplicationImpl app = ApplicationImpl.newInstance("new", ApplicationType.CROWD);
        Application oldApp = ApplicationImpl.newInstance("old", ApplicationType.CROWD);

        when(mockApplicationDAO.findById(oldApp.getId())).thenReturn(oldApp);

        applicationManager.update(app);
    }

    @Test(expected = ApplicationManagerException.class)
    public void testDeactivateCrowdApplicationFails() throws Exception
    {
        ApplicationImpl app = ApplicationImpl.newInstance("new", ApplicationType.CROWD);
        app.setActive(false);

        applicationManager.update(app);
    }

    @Test(expected = NullPointerException.class)
    public void testRemoveDirectoryFromApplicationWithInvalidArgumentsNullDirectoryFails() throws ApplicationManagerException
    {
        applicationManager.removeDirectoryFromApplication(null, application);
    }

    @Test(expected = NullPointerException.class)
    public void testRemoveDirectoryFromApplicationWithInvalidArgumentsNullApplicationFails() throws ApplicationManagerException
    {
        applicationManager.removeDirectoryFromApplication(directory1, null);
    }

    @Test
    public void testRemoveDirectoryFromApplicationThatAllMappingsAreRemoved() throws Exception
    {
        applicationManager.removeDirectoryFromApplication(directory1, application);

        verify(mockApplicationDAO).removeDirectoryMapping(application.getId(), directory1.getId());
        verify(mockEventPublisher).publish(Mockito.any(ApplicationDirectoryRemovedEvent.class));
    }

    @Test
    public void testRemoveDirectoryFromApplicationThatNotAllMappingsAreRemoved() throws Exception
    {
        // Add two extra mappings
        DirectoryMapping directoryMapping = new DirectoryMapping(application, directory2, Boolean.TRUE);
        application.getDirectoryMappings().add(directoryMapping);

        applicationManager.removeDirectoryFromApplication(directory1, application);

        verify(mockApplicationDAO).removeDirectoryMapping(application.getId(), directory1.getId());
        verify(mockEventPublisher).publish(Mockito.any(ApplicationDirectoryRemovedEvent.class));

        assertFalse(application.getDirectoryMappings().isEmpty());
        assertTrue(application.getDirectoryMappings().contains(directoryMapping));
    }
}
