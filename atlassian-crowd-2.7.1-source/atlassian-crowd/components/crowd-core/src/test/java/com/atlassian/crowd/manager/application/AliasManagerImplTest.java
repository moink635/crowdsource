package com.atlassian.crowd.manager.application;

import com.atlassian.crowd.dao.alias.AliasDAO;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.event.AliasEvent;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.event.api.EventPublisher;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AliasManagerImplTest
{
    private AliasManager aliasManager;
    private ApplicationImpl application;
    @Mock private AliasDAO aliasDAO;
    @Mock private ApplicationService applicationService;
    @Mock private EventPublisher eventPublisher;

    @Before
    public void setUp()
    {
        application = ApplicationImpl.newInstanceWithPassword("test application", ApplicationType.GENERIC_APPLICATION, "secret");

        aliasManager = new AliasManagerImpl(aliasDAO, applicationService, eventPublisher);
    }

    @Test(expected = NullPointerException.class)
    public void testFindUsernameByAliasWithNullApplicationFails()
    {
        aliasManager.findUsernameByAlias(null, "");
    }

    @Test(expected = NullPointerException.class)
    public void testFindUsernameByAliasWithNullUsernameFails()
    {
        aliasManager.findUsernameByAlias(application, null);
    }

    @Test
    public void testFindUsernameByAliasWhereApplicationIsNotAliasingEnabled()
    {
        application.setAliasingEnabled(false);

        final String alias = aliasManager.findUsernameByAlias(application, "authenticating-user");

        assertEquals("authenticating-user", alias);

        verify(aliasDAO, never()).findUsernameByAlias(application, "authenticating-user");
    }

    @Test
    public void testFindUsernameByAliasWhereAliasDoesNotExist()
    {
        application.setAliasingEnabled(true);
        when(aliasDAO.findUsernameByAlias(application, "authenticating-user")).thenReturn(null); // no alias exists

        final String alias = aliasManager.findUsernameByAlias(application, "authenticating-user");

        assertEquals("authenticating-user", alias);

        verify(aliasDAO, times(1)).findUsernameByAlias(application, "authenticating-user");
    }

    @Test
    public void testFindUsernameByAliasWhereAliasDoesExist()
    {
        application.setAliasingEnabled(true);
        when(aliasDAO.findUsernameByAlias(application, "authenticating-user")).thenReturn("real-username"); // alias exists

        final String alias = aliasManager.findUsernameByAlias(application, "authenticating-user");

        assertEquals("real-username", alias);

        verify(aliasDAO, times(1)).findUsernameByAlias(application, "authenticating-user");
    }

    @Test(expected = NullPointerException.class)
    public void testFindAliasByUsernameWithNullApplicationFails()
    {
        aliasManager.findAliasByUsername(null, "");
    }

    @Test(expected = NullPointerException.class)
    public void testFindAliasByUsernameWithNullUsernameFails()
    {
        aliasManager.findAliasByUsername(application, null);
    }

    @Test
    public void testFindAliasByUsernameWhereApplicationIsNotAliasingEnabled()
    {
        application.setAliasingEnabled(false);

        final String alias = aliasManager.findAliasByUsername(application, "real-username");

        assertEquals("real-username", alias);

        verify(aliasDAO, never()).findAliasByUsername(application, "real-username");
    }

    @Test
    public void testFindAliasByUsernameWhereAliasDoesNotExist()
    {
        application.setAliasingEnabled(true);
        when(aliasDAO.findAliasByUsername(application, "real-username")).thenReturn(null); // no alias exists

        final String alias = aliasManager.findAliasByUsername(application, "real-username");

        assertEquals("real-username", alias);

        verify(aliasDAO, times(1)).findAliasByUsername(application, "real-username");
    }

    @Test
    public void testFindAliasByUsernameWhereAliasDoesExist()
    {
        application.setAliasingEnabled(true);

        when(aliasDAO.findAliasByUsername(application, "real-username")).thenReturn("aliased-username"); // alias exists

        final String alias = aliasManager.findAliasByUsername(application, "real-username");

        assertEquals("aliased-username", alias);

        verify(aliasDAO, times(1)).findAliasByUsername(application, "real-username");
    }

    @Test
    public void testStoreNewAliasSuccess() throws Exception
    {
        String username = "user";
        String alias = "alias";

        when(aliasDAO.findUsernameByAlias(application, alias)).thenReturn(null);
        when(aliasDAO.findAliasByUsername(application, username)).thenReturn(null);
        when(applicationService.findUserByName(application, alias)).thenThrow(new UserNotFoundException(username));

        aliasManager.storeAlias(application, username, alias);

        verify(aliasDAO).storeAlias(application, username, alias);

        verify(eventPublisher).publish(AliasEvent.created(application, username, alias));
    }

    @Test
    public void testStoreUpdatedAliasSuccess() throws Exception
    {
        String username = "user";
        String alias = "alias";

        when(aliasDAO.findUsernameByAlias(application, alias)).thenReturn(null);
        when(aliasDAO.findAliasByUsername(application, username)).thenReturn("old alias");
        when(applicationService.findUserByName(application, alias)).thenThrow(new UserNotFoundException(username));

        aliasManager.storeAlias(application, username, alias);

        verify(aliasDAO).storeAlias(application, username, alias);

        verify(eventPublisher).publish(AliasEvent.updated(application, username, alias));
    }

    @Test (expected = AliasAlreadyInUseException.class)
    public void testStoreAliasAlreadyInUseAsAlias() throws AliasAlreadyInUseException
    {
        String username = "user";
        String alias = "alias";

        when(aliasDAO.findUsernameByAlias(application, alias)).thenReturn("someone");

        aliasManager.storeAlias(application, username, alias);
    }

    @Test (expected = AliasAlreadyInUseException.class)
    public void testStoreAliasAlreadyInUseAsUnaliasedUser() throws Exception
    {
        String username = "user";
        String alias = "alias";

        UserTemplate user = new UserTemplate(alias, 1L);

        when(aliasDAO.findUsernameByAlias(application, alias)).thenReturn(null);
        when(applicationService.findUserByName(application, alias)).thenReturn(user);
        when(aliasDAO.findAliasByUsername(application, alias)).thenReturn(null);

        aliasManager.storeAlias(application, username, alias);
    }

    @Test
    public void testStoreAliasSuccessBecauseUserWithAliasAsUsernameHasAnAliasToo() throws Exception
    {
        String username = "user";
        String alias = "alias";

        UserTemplate user = new UserTemplate(alias, 1L);

        when(aliasDAO.findUsernameByAlias(application, alias)).thenReturn(null);
        when(applicationService.findUserByName(application, alias)).thenReturn(user);
        when(aliasDAO.findAliasByUsername(application, alias)).thenReturn("i have alias too");

        aliasManager.storeAlias(application, username, alias);

        verify(aliasDAO).storeAlias(application, username, alias);

        verify(eventPublisher).publish(AliasEvent.created(application, username, alias));
    }

    @Test
    public void testStoreAliasSuccessAllowAliasingToTheSameAlias() throws AliasAlreadyInUseException
    {
        String username = "user";
        String alias = "alias";

        when(aliasDAO.findUsernameByAlias(application, alias)).thenReturn(username);

        aliasManager.storeAlias(application, username, alias);

        verify(aliasDAO, never()).storeAlias(any(Application.class), anyString(), anyString());

        verifyZeroInteractions(eventPublisher);
    }

    @Test
    public void removeAliasSuccess() throws Exception
    {
        when(aliasDAO.findUsernameByAlias(application, "user")).thenReturn(null);  // no alias is 'user'

        aliasManager.removeAlias(application, "user");

        verify(aliasDAO).removeAlias(application, "user");

        verify(eventPublisher).publish(AliasEvent.deleted(application, "user"));
    }

    @Test (expected = AliasAlreadyInUseException.class)
    public void removeAliasFailsWhenThereIsAnotherUserWithAnAliasEqualToTheUsername() throws Exception
    {
        when(aliasDAO.findUsernameByAlias(application, "user")).thenReturn("i have alias too");

        aliasManager.removeAlias(application, "user");
    }
}
