package com.atlassian.crowd.manager.property;

import com.atlassian.crowd.dao.property.PropertyDAO;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.mail.SMTPServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.security.Key;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Unit tests for the property manager
 */
public class PropertyManagerGenericTest
{
    private PropertyManager propertyManager;
    private PropertyDAO propertyDAO;
    private static final String LOCALHOST = "localhost";
    private static final String PASSWORD = "secret";
    private static final String MAIL_PREFIX = "[Crowd]";
    private static final String SENDER_ADDRESS = "test@atlassian.com";
    private static final String MAIL_USERNAME = "bob";
    private static final int MAIL_PORT = 433;
    private static final String MAIL_JNDI_LOCATION = "java:comp/env/mail/CrowdMailServer";
    private static final boolean USE_SSL = false;

    @Before
    public void setUp()
    {
        propertyDAO = mock(PropertyDAO.class);
        propertyManager = new PropertyManagerGeneric(propertyDAO);
    }

    @After
    public void tearDown()
    {
        propertyManager = null;
        propertyDAO = null;
    }

    private void mockProperty(String name, String value) throws ObjectNotFoundException
    {
        when(propertyDAO.find(Property.CROWD_PROPERTY_KEY, name)).thenReturn(new Property(Property.CROWD_PROPERTY_KEY, name, value));
    }

    private void mockPropertyMissing(String name) throws ObjectNotFoundException
    {
        when(propertyDAO.find(Property.CROWD_PROPERTY_KEY, name)).thenThrow(new ObjectNotFoundException());
    }

    @Test
    public void testGetAuthenticatingSMTPServer() throws Exception
    {
        // Expect SMTP server
        mockProperty(Property.MAILSERVER_HOST, LOCALHOST);
        mockProperty(Property.MAILSERVER_USERNAME, MAIL_USERNAME);
        mockProperty(Property.MAILSERVER_PASSWORD, PASSWORD);
        mockProperty(Property.MAILSERVER_PORT, Integer.toString(MAIL_PORT));
        mockProperty(Property.MAILSERVER_PREFIX, MAIL_PREFIX);
        mockProperty(Property.MAILSERVER_SENDER, SENDER_ADDRESS);
        mockProperty(Property.MAILSERVER_USE_SSL, Boolean.toString(USE_SSL));

        // Not a JNDI server
        mockPropertyMissing(Property.MAILSERVER_JNDI_LOCATION);

        SMTPServer server = propertyManager.getSMTPServer();

        assertFalse(server.isJndiMailActive());
        assertNull(server.getJndiLocation());
        assertEquals(LOCALHOST, server.getHost());
        assertEquals(MAIL_USERNAME, server.getUsername());
        assertEquals(PASSWORD, server.getPassword());
        assertEquals(MAIL_PREFIX, server.getPrefix());
        assertEquals(SENDER_ADDRESS, server.getFrom().toString());
        assertEquals(MAIL_PORT, server.getPort());
    }

    @Test
    public void testGetAnonymousSMTPServerWithDefaultPort() throws Exception
    {
        // Expect SMTP server
        mockProperty(Property.MAILSERVER_HOST, LOCALHOST);
        mockProperty(Property.MAILSERVER_PREFIX, MAIL_PREFIX);
        mockProperty(Property.MAILSERVER_SENDER, SENDER_ADDRESS);
        mockProperty(Property.MAILSERVER_USE_SSL, Boolean.toString(USE_SSL));

        // Anonymous
        mockPropertyMissing(Property.MAILSERVER_USERNAME);
        mockPropertyMissing(Property.MAILSERVER_PASSWORD);

        // Default port set
        mockPropertyMissing(Property.MAILSERVER_PORT);

        // Not a JNDI server
        mockPropertyMissing(Property.MAILSERVER_JNDI_LOCATION);

        SMTPServer server = propertyManager.getSMTPServer();

        assertFalse(server.isJndiMailActive());
        assertNull(server.getJndiLocation());
        assertEquals(LOCALHOST, server.getHost());
        assertNull(MAIL_USERNAME, server.getUsername());
        assertNull(PASSWORD, server.getPassword());
        assertEquals(MAIL_PREFIX, server.getPrefix());
        assertEquals(SENDER_ADDRESS, server.getFrom().toString());
        assertEquals(SMTPServer.DEFAULT_MAIL_PORT, server.getPort());
    }

    @Test
    public void testGetAnonymousSMTPServer() throws Exception
    {
        // Expect SMTP server
        mockProperty(Property.MAILSERVER_HOST, LOCALHOST);
        mockProperty(Property.MAILSERVER_PREFIX, MAIL_PREFIX);
        mockProperty(Property.MAILSERVER_SENDER, SENDER_ADDRESS);
        mockProperty(Property.MAILSERVER_PORT, Integer.toString(MAIL_PORT));
        mockProperty(Property.MAILSERVER_USE_SSL, Boolean.toString(USE_SSL));

        // Anonymous
        mockPropertyMissing(Property.MAILSERVER_USERNAME);
        mockPropertyMissing(Property.MAILSERVER_PASSWORD);

        // Not a JNDI server
        mockPropertyMissing(Property.MAILSERVER_JNDI_LOCATION);

        SMTPServer server = propertyManager.getSMTPServer();

        assertFalse(server.isJndiMailActive());
        assertNull(server.getJndiLocation());
        assertEquals(LOCALHOST, server.getHost());
        assertNull(MAIL_USERNAME, server.getUsername());
        assertNull(PASSWORD, server.getPassword());
        assertEquals(MAIL_PREFIX, server.getPrefix());
        assertEquals(SENDER_ADDRESS, server.getFrom().toString());
        assertEquals(MAIL_PORT, server.getPort());
    }

    @Test
    public void testGetJNDIServer() throws Exception
    {
        // Expect JNDI server
        mockProperty(Property.MAILSERVER_JNDI_LOCATION, MAIL_JNDI_LOCATION);
        mockProperty(Property.MAILSERVER_SENDER, SENDER_ADDRESS);
        mockProperty(Property.MAILSERVER_PREFIX, MAIL_PREFIX);

        // Not a SMTP server
        mockProperty(Property.MAILSERVER_HOST, LOCALHOST);
        mockProperty(Property.MAILSERVER_PORT, Integer.toString(MAIL_PORT));
        mockPropertyMissing(Property.MAILSERVER_USERNAME);
        mockPropertyMissing(Property.MAILSERVER_PASSWORD);
        mockProperty(Property.MAILSERVER_USE_SSL, Boolean.toString(USE_SSL));


        SMTPServer server = propertyManager.getSMTPServer();

        assertTrue(server.isJndiMailActive());

        assertEquals(MAIL_JNDI_LOCATION, server.getJndiLocation());
        assertEquals(MAIL_PREFIX, server.getPrefix());
        assertEquals(SENDER_ADDRESS, server.getFrom().toString());

        assertNull(server.getHost());
        assertNull(server.getUsername());
        assertNull(server.getPassword());
        assertEquals(0, server.getPort());
    }

    @Test
    public void testGetJNDIServerWithNoEmailPrefixSet() throws Exception
    {
        // Expect JNDI server
        mockProperty(Property.MAILSERVER_JNDI_LOCATION, MAIL_JNDI_LOCATION);
        mockProperty(Property.MAILSERVER_SENDER, SENDER_ADDRESS);

        // No prefix set
        mockPropertyMissing(Property.MAILSERVER_PREFIX);

        SMTPServer server = propertyManager.getSMTPServer();

        assertNull(server.getPrefix());
    }

    @Test
    public void testGetAnonymousSMTPServerWithNoEmailPrefixSet() throws Exception
    {
        // Expect SMTP server
        mockProperty(Property.MAILSERVER_HOST, LOCALHOST);
        mockProperty(Property.MAILSERVER_SENDER, SENDER_ADDRESS);
        mockProperty(Property.MAILSERVER_PORT, Integer.toString(MAIL_PORT));
        mockProperty(Property.MAILSERVER_USE_SSL, Boolean.toString(USE_SSL));

        // No email prefix set
        mockPropertyMissing(Property.MAILSERVER_PREFIX);

        // Anonymous
        mockPropertyMissing(Property.MAILSERVER_USERNAME);
        mockPropertyMissing(Property.MAILSERVER_PASSWORD);

        // Not a JNDI server
        mockPropertyMissing(Property.MAILSERVER_JNDI_LOCATION);

        SMTPServer server = propertyManager.getSMTPServer();

        assertNull(server.getPrefix());
    }

    @Test (expected = PropertyManagerException.class)
    public void testGetSMTPServerWhenItIsNotConfiguredAtAll() throws Exception
    {
        mockPropertyMissing(Property.MAILSERVER_SENDER);
        mockPropertyMissing(Property.MAILSERVER_PREFIX);

        // not a SMTP server
        mockPropertyMissing(Property.MAILSERVER_HOST);

        // not a JNDI server
        mockPropertyMissing(Property.MAILSERVER_JNDI_LOCATION);

        propertyManager.getSMTPServer();
    }

    @Test (expected = PropertyManagerException.class)
    public void testGetSMTPServerWhenItIsNotConfiguredButHasEmailAddressAndPrefix() throws Exception
    {
        mockProperty(Property.MAILSERVER_SENDER, SENDER_ADDRESS);
        mockProperty(Property.MAILSERVER_PREFIX, MAIL_PREFIX);

        // not a SMTP server
        mockPropertyMissing(Property.MAILSERVER_HOST);

        // not a JNDI server
        mockPropertyMissing(Property.MAILSERVER_JNDI_LOCATION);

        propertyManager.getSMTPServer();
    }

    @Test
    public void testSessionTimeConversion() throws Exception
    {
        mockProperty(Property.SESSION_TIME, "119999");
        assertEquals(1, propertyManager.getSessionTime());

        mockProperty(Property.SESSION_TIME, "60000");
        assertEquals(1, propertyManager.getSessionTime());

        mockProperty(Property.SESSION_TIME, "0");
        assertEquals(0, propertyManager.getSessionTime());

        mockProperty(Property.SESSION_TIME, "");
        assertEquals("An invalid string uses the default",
                5, propertyManager.getSessionTime());
    }

    @Test
    public void includeIpAddressInValidationFactorsDefaultsToTrue() throws Exception
    {
        mockPropertyMissing(Property.INCLUDE_IP_ADDRESS_IN_VALIDATION_FACTORS);
        assertTrue(propertyManager.isIncludeIpAddressInValidationFactors());
    }

    @Test
    public void includeIpAddressInValidationFactorsCanBeSetToFalse() throws Exception
    {
        mockProperty(Property.INCLUDE_IP_ADDRESS_IN_VALIDATION_FACTORS, "");
        assertFalse(propertyManager.isIncludeIpAddressInValidationFactors());
    }

    @Test
    public void getDesEncryptionKeyDecodesBase64() throws ObjectNotFoundException, PropertyManagerException
    {
        mockProperty(Property.DES_ENCRYPTION_KEY, "0Dfy7CXfkR8=");
        Key key = propertyManager.getDesEncryptionKey();

        byte[] expected = {
                (byte) 0xd0, (byte) 0x37, (byte) 0xf2, (byte) 0xec,
                (byte) 0x25, (byte) 0xdf, (byte) 0x91, (byte) 0x1f
        };

        assertArrayEquals(expected, key.getEncoded());
    }

    @Test
    public void getStringShouldReturnStoredValue() throws Exception
    {
        mockProperty("FOO", "someValue");
        final String string = propertyManager.getString("FOO", "default");
        assertThat(string, is("someValue"));
    }

    @Test
    public void getStringWithMissingPropertyShouldReturnDefaultValue() throws Exception
    {
        mockPropertyMissing("FOO");
        final String string = propertyManager.getString("FOO", "default");
        assertThat(string, is("default"));
    }

    @Test
    public void getBooleanShouldReturnStoredValueWhenTrue() throws Exception
    {
        mockProperty("FOO", "true");
        final boolean value = propertyManager.getBoolean("FOO", false);
        assertThat(value, is(true));
    }

    @Test
    public void getBooleanShouldReturnStoredValueWhenFalse() throws Exception
    {
        mockProperty("FOO", "false");
        final boolean value = propertyManager.getBoolean("FOO", true);
        assertThat(value, is(false));
    }

    @Test
    public void getGetBooleanWithMissingPropertyShouldReturnDefaultValue() throws Exception
    {
        mockPropertyMissing("FOO");
        final boolean value = propertyManager.getBoolean("FOO", true);
        assertThat(value, is(true));
    }

    @Test
    public void getIntShouldReturnStoredValue() throws Exception
    {
        mockProperty("FOO", "12");
        final int value = propertyManager.getInt("FOO", 5);
        assertThat(value, is(12));
    }

    @Test
    public void getIntWithMissingPropertyShouldReturnDefaultValue() throws Exception
    {
        mockPropertyMissing("FOO");
        final int value = propertyManager.getInt("FOO", 5);
        assertThat(value, is(5));
    }

    @Test
    public void getIntWithCorruptedPropertyShouldReturnDefaultValue() throws Exception
    {
        mockProperty("FOO", "NaN");
        final int value = propertyManager.getInt("FOO", 5);
        assertThat(value, is(5));
    }
}
