package com.atlassian.crowd.util;

import java.math.BigInteger;

import com.atlassian.crowd.embedded.api.PasswordCredential;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class PasswordHelperImplTest
{
    private PasswordHelper passwordHelper;

    @Before
    public void setUp()
    {
        passwordHelper = new PasswordHelperImpl();
    }

    @Test
    public void shouldValidatePasswordIfPasswordContainsASubstringOfARegexPattern()
    {
        assertTrue(passwordHelper.validateRegex("[a-zA-Z]", new PasswordCredential("secret")));
        assertTrue(passwordHelper.validateRegex("[0-9]", new PasswordCredential("123456789")));
    }

    @Test
    public void shouldFailIfItDoesNotContainASubstringThatMatchesTheRegex()
    {
        assertFalse(passwordHelper.validateRegex("[a-zA-Z]", new PasswordCredential("123345")));
        assertFalse(passwordHelper.validateRegex("[0-9]", new PasswordCredential("abc")));
    }

    @Test
    public void shouldFailWhenPasswordCredentialIsNull()
    {
        assertFalse(passwordHelper.validateRegex("[0-9]", new PasswordCredential(null, false)));
    }

    @Test
    public void shouldPassIfAnchoredToEndWithACharacter()
    {
        assertTrue(passwordHelper.validateRegex("[a-z]$", new PasswordCredential("1235a")));
    }

    @Test
    public void shouldFailIfAnchoredToEndWithACharacterAndPasswordDoesNotMatch()
    {
        assertFalse(passwordHelper.validateRegex("[a-z]$", new PasswordCredential("1235")));
    }

    @Test
    public void shouldPassIfAnchoredToStartWithANumber()
    {
        assertTrue(passwordHelper.validateRegex("^[a-z]", new PasswordCredential("a1234")));
    }

    @Test
    public void shouldFailIfAnchoredToStartWithANumberAndPasswordDoesNotMatch()
    {
        assertFalse(passwordHelper.validateRegex("^[a-z]", new PasswordCredential("1234")));
    }

    @Test
    public void shouldFailIfAnchoredBothAtBeginningAndEndEvenIfThereIsASubstringMatch()
    {
        assertFalse(passwordHelper.validateRegex("^[a-z][0-9]$", new PasswordCredential("abcd1234")));
        assertFalse(passwordHelper.validateRegex("^[a-z]$", new PasswordCredential("a1234b")));
        assertFalse(passwordHelper.validateRegex("^[a-z]$", new PasswordCredential("a1234")));
        assertFalse(passwordHelper.validateRegex("^[a-z]$", new PasswordCredential("213a123")));
    }

    @Test
    public void randomPasswordsHaveAtLeast128BitsOfEntropy()
    {
        String password = passwordHelper.generateRandomPassword();

        BigInteger entropy = BigInteger.valueOf(62).pow(password.length());

        assertThat(entropy, Matchers.greaterThan(BigInteger.valueOf(2).pow(128)));
    }

    @Test
    public void shouldFailIfUsedToValidateAnEncryptedPassword()
    {
        assertFalse(passwordHelper.validateRegex(".*", PasswordCredential.encrypted("password-hash")));
    }
}
