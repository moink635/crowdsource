package com.atlassian.crowd.manager.application;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.event.EventStore;
import com.atlassian.crowd.event.Events;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.permission.PermissionManager;
import com.atlassian.crowd.manager.webhook.InvalidWebhookEndpointException;
import com.atlassian.crowd.manager.webhook.WebhookRegistry;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.event.AliasEvent;
import com.atlassian.crowd.model.event.Operation;
import com.atlassian.crowd.model.event.OperationEvent;
import com.atlassian.crowd.model.event.UserEvent;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.webhook.Webhook;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.UserQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.event.api.EventPublisher;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.manager.application.OperationEventMatcher.creationEvent;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.collection.IsEmptyIterable.emptyIterable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationServiceGenericTest
{
    private static final Long APPLICATION_ID = 10L;
    private static final Long WEBHOOK_ID = 11L;

    private ApplicationServiceGeneric appSvc;

    @Mock
    DirectoryManager directoryManager;

    @Mock
    PermissionManager permissionManager;

    @Mock
    DirectoryInstanceLoader directoryInstanceLoader;

    @Mock
    EventPublisher eventPublisher;

    @Mock
    EventStore eventStore;

    @Mock
    Application application;

    @Mock
    WebhookRegistry webhookRegistry;

    @Before
    public void createObjectUnderTest()
    {
        appSvc = new ApplicationServiceGeneric(
            directoryManager, permissionManager, directoryInstanceLoader, eventPublisher, eventStore, webhookRegistry);
    }

    @Before
    public void initialiseMocks()
    {
        when(application.getId()).thenReturn(APPLICATION_ID);
    }

    /**
     * These two strings are the same when lowercased in EN but different when lowercased in TR.
     */
    private static final Collection<String> namesWithTurkishCharacters = ImmutableList.of("i", "I"); // "ı", "İ"

    private ApplicationService serviceWithUsers(List<User> users) throws DirectoryNotFoundException, OperationFailedException
    {
        when(directoryManager.searchUsers(Mockito.anyLong(), Mockito.<EntityQuery<User>>anyObject())).thenReturn(users);

        Directory directory = mock(Directory.class);
        when(directory.isActive()).thenReturn(true);

        DirectoryMapping mapping = new DirectoryMapping(application, directory, true);
        List<DirectoryMapping> dirMappings = Collections.singletonList(mapping);
        when(application.getDirectoryMappings()).thenReturn(dirMappings);

        return appSvc;
    }

    private UserQuery<User> allUsersQuery()
    {
        return new UserQuery<User>(User.class, NullRestrictionImpl.INSTANCE, 0, -1);
    }

    private static void withCrowdIdentifierLanguage(String lang, Callable<Void> r) throws Exception
    {
        String before = System.getProperty("crowd.identifier.language");
        try
        {
            System.setProperty("crowd.identifier.language", lang);
            IdentifierUtils.prepareIdentifierCompareLocale();
            r.call();
        }
        finally
        {
            if (before != null)
            {
                System.setProperty("crowd.identifier.language", before);
            }
            else
            {
                System.clearProperty("crowd.identifier.language");
            }
            IdentifierUtils.prepareIdentifierCompareLocale();
        }
    }

    private static List<User> withNames(Iterable<String> names)
    {
        final List<User> value = new ArrayList<User>();

        for (String s : names)
        {
            value.add(new UserTemplate(s));
        }

        return value;
    }

    @Test
    public void turkishLettersIdenticalInEnglishLocale() throws Exception
    {
        final List<User> value = withNames(namesWithTurkishCharacters);

        final List<User> users = new ArrayList<User>();

        withCrowdIdentifierLanguage("en", new Callable<Void>() {
            @Override
            public Void call() throws Exception
            {
                users.addAll(serviceWithUsers(value).searchUsers(application, allUsersQuery()));
                return null;
            }
        });

        assertEquals(1, users.size());
    }

    @Test
    public void turkishLettersDistinctInTurkishLocale() throws Exception
    {
        final List<User> value = withNames(namesWithTurkishCharacters);

        final List<User> users = new ArrayList<User>();

        withCrowdIdentifierLanguage("tr", new Callable<Void>() {
            @Override
            public Void call() throws Exception
            {
                users.addAll(serviceWithUsers(value).searchUsers(application, allUsersQuery()));
                return null;
            }
        });

        assertEquals(2, users.size());
    }

    @Test
    public void usersComeBackInLexicalOrder() throws Exception
    {
        List<String> names = new ArrayList<String>();

        for (int i = 0; i < 100; i++)
        {
            names.add(String.format("user%03d", i));
        }

        List<String> namesInRandomOrder = new ArrayList<String>(names);
        Collections.shuffle(namesInRandomOrder);

        assertFalse(names.equals(namesInRandomOrder));

        List<String> namesAsReturned = new ArrayList<String>();

        for (User u : serviceWithUsers(withNames(namesInRandomOrder)).searchUsers(application, allUsersQuery()))
        {
            namesAsReturned.add(u.getName());
        }

        assertEquals(names, namesAsReturned);
    }

    @Test
    public void searchResultsIncludeDuplicateUsersFromDifferentDirectoriesWhenRequested() throws Exception
    {
        List<User> users = new ArrayList<User>();

        User u1 = mock(User.class);
        when(u1.getName()).thenReturn("user");
        when(u1.getDirectoryId()).thenReturn(1L);
        users.add(u1);

        User u2 = mock(User.class);
        when(u2.getName()).thenReturn("user");
        when(u2.getDirectoryId()).thenReturn(2L);
        users.add(u2);

        List<User> results = serviceWithUsers(users).searchUsersAllowingDuplicateNames(application, allUsersQuery());

        assertEquals(2, results.size());
    }

    /**
     * This is a non-functional test. It should pass, but its purpose is for benchmarking as the number
     * of users increases.
     */
    @Test
    public void queryForLargeNumberOfUsers() throws DirectoryNotFoundException, OperationFailedException
    {
        List<String> names = new ArrayList<String>();

        for (int i = 0; i < 10000; i++)
        {
            names.add("user" + i);
        }

        List<User> users = serviceWithUsers(withNames(names)).searchUsers(application, allUsersQuery());

        assertEquals(names.size(), users.size());
    }

    @Test
    public void searchNestedGroupRelationshipsForMembersGetsAllResultsInUnderlyingQuery() throws DirectoryNotFoundException, OperationFailedException
    {
        Directory directory = mock(Directory.class);
        when(directory.isActive()).thenReturn(true);
        when(directory.getId()).thenReturn(1L);

        DirectoryMapping mapping = new DirectoryMapping(application, directory, true);
        List<DirectoryMapping> dirMappings = Collections.singletonList(mapping);
        when(application.getDirectoryMappings()).thenReturn(dirMappings);

        ApplicationServiceGeneric appSvc = new ApplicationServiceGeneric(
                directoryManager, permissionManager, directoryInstanceLoader, eventPublisher, eventStore,
                webhookRegistry);

        MembershipQuery<User> query = new MembershipQuery<User>(User.class, true,
                EntityDescriptor.group(), "group", EntityDescriptor.user(), 0, 5);

        List<User> underlyingResults = new ArrayList<User>();
        for (int i = 0; i < 10; i++)
        {
            User u = mock(User.class);
            when(u.getName()).thenReturn("user" + i);
            underlyingResults.add(u);
        }

        when(directoryManager.searchNestedGroupRelationships(Mockito.eq(1L), Mockito.any(MembershipQuery.class))).thenReturn(underlyingResults);

        List<User> results = appSvc.searchNestedGroupRelationships(application, query);
        assertThat("Only the requested page is returned", results, Matchers.hasSize(5));

        ArgumentCaptor<MembershipQuery<User>> qc = (ArgumentCaptor) ArgumentCaptor.forClass(MembershipQuery.class);

        verify(directoryManager).searchNestedGroupRelationships(Mockito.eq(1L), qc.capture());

        MembershipQuery<User> mq = qc.getValue();
        assertEquals("All results were requested from the underlying directory", 0, mq.getStartIndex());
        assertEquals("All results were requested from the underlying directory", EntityQuery.ALL_RESULTS, mq.getMaxResults());
    }

    @Test(expected = InvalidUserException.class)
    public void creationOfUserWithLeadingSpacesFails() throws Exception
    {
        UserTemplate user = new UserTemplate(" user");
        appSvc.addUser(null, user, null);
    }

    @Test(expected = InvalidUserException.class)
    public void creationOfUserWithTrailingSpacesFails() throws Exception
    {
        UserTemplate user = new UserTemplate("user ");
        appSvc.addUser(null, user, null);
    }

    @Test(expected = InvalidGroupException.class)
    public void creationOfGroupWithLeadingSpacesFails() throws Exception
    {
        GroupTemplate group = new GroupTemplate(" group");
        appSvc.addGroup(null, group);
    }

    @Test(expected = InvalidGroupException.class)
    public void creationOfGroupWithTrailingSpacesFails() throws Exception
    {
        GroupTemplate group = new GroupTemplate("group ");
        appSvc.addGroup(null, group);
    }

    @Test
    public void shouldFindWebhookById() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(webhookRegistry.findById(1L)).thenReturn(webhook);

        when(webhook.getApplication()).thenReturn(application);

        assertSame(webhook, appSvc.findWebhookById(application, 1L));
    }

    @Test (expected = ApplicationPermissionException.class)
    public void shouldNotFindWebhookByIdWhenApplicationDoesNotOwnWebhook() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(webhookRegistry.findById(1L)).thenReturn(webhook);

        Application differentApplication = mock(Application.class);
        when(differentApplication.getId()).thenReturn(APPLICATION_ID + 1);  // a different application
        when(webhook.getApplication()).thenReturn(differentApplication);

        appSvc.findWebhookById(application, 1L);
    }

    @Test (expected = WebhookNotFoundException.class)
    public void shouldNotFindWebhookByIdWhenWebhookDoesNotExist() throws Exception
    {
        when(webhookRegistry.findById(1L)).thenThrow(new WebhookNotFoundException(1L));

        appSvc.findWebhookById(application, 1L);
    }

    @Test
    public void registerWebhookSuccessfully() throws Exception
    {
        registerWebhookAndAssertSuccessForWebHookWithEndpointUrlAndTokenOf("http://example.test/myendpoint", "secret");
    }

    @Test
    public void registerWebhookSuccessfullyForHttpsEndpointUrl() throws Exception
    {
        registerWebhookAndAssertSuccessForWebHookWithEndpointUrlAndTokenOf("https://example.test/myendpoint", "secret");
    }

    @Test
    public void registerWebhookSuccessfullyWithoutToken() throws Exception
    {
        registerWebhookAndAssertSuccessForWebHookWithEndpointUrlAndTokenOf("http://example.test/myendpoint", null);
    }

    private void registerWebhookAndAssertSuccessForWebHookWithEndpointUrlAndTokenOf(String endpointUrl, String token) throws InvalidWebhookEndpointException
    {
        Webhook savedWebhook = mock(Webhook.class);
        when(savedWebhook.getId()).thenReturn(1L);

        when(webhookRegistry.add(any(Webhook.class))).thenReturn(savedWebhook);

        Webhook returnedWebhook = appSvc.registerWebhook(application, endpointUrl, token);

        assertSame(savedWebhook, returnedWebhook);

        ArgumentCaptor<Webhook> webhookArgumentCaptor = ArgumentCaptor.forClass(Webhook.class);
        verify(webhookRegistry).add(webhookArgumentCaptor.capture());

        assertEquals(endpointUrl, webhookArgumentCaptor.getValue().getEndpointUrl());
        assertEquals(token, webhookArgumentCaptor.getValue().getToken());
    }

    @Test(expected = InvalidWebhookEndpointException.class)
    public void registerWebhookShouldFailIfEndpointUrlIsAnInvalidUrl() throws Exception
    {
        appSvc.registerWebhook(application, "{", "secret");
    }

    @Test(expected = InvalidWebhookEndpointException.class)
    public void registerWebhookShouldFailIfEndpointUrlIsAnEmptyUrl() throws Exception
    {
        appSvc.registerWebhook(application, "", "secret");
    }

    @Test(expected = InvalidWebhookEndpointException.class)
    public void registerWebhookShouldFailIfEndpointUrlSchemeIsNotHttpOrHttps() throws Exception
    {
        appSvc.registerWebhook(application, "ftp://example.test/myendpoint", "secret");
    }

    @Test(expected = InvalidWebhookEndpointException.class)
    public void registerWebhookShouldFailIfEndpointUrlIsRelative() throws Exception
    {
        appSvc.registerWebhook(application, "a", "secret"); // a url is considered relative if it's missing a scheme
    }

    @Test
    public void unregisterWebhookSuccessfully() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getApplication()).thenReturn(application);

        when(webhookRegistry.findById(WEBHOOK_ID)).thenReturn(webhook);

        appSvc.unregisterWebhook(application, WEBHOOK_ID);

        verify(webhookRegistry).remove(webhook);
    }

    @Test(expected = WebhookNotFoundException.class)
    public void unregisterWebhookThatDoesNotExist() throws Exception
    {
        when(webhookRegistry.findById(WEBHOOK_ID)).thenThrow(new WebhookNotFoundException(WEBHOOK_ID));

        appSvc.unregisterWebhook(application, WEBHOOK_ID);
    }

    @Test(expected = ApplicationPermissionException.class)
    public void unregisterWebhookRegisteredByAnotherApplication() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        Application differentApplication = mock(Application.class);
        when(webhook.getApplication()).thenReturn(differentApplication);
        when(differentApplication.getId()).thenReturn(APPLICATION_ID + 1); // a different application

        when(webhookRegistry.findById(WEBHOOK_ID)).thenReturn(webhook);

        appSvc.unregisterWebhook(application, WEBHOOK_ID);
    }

    @Test
    public void getNewEventsReturnsNewEventToken() throws Exception
    {
        String eventToken = "event-token";

        ApplicationServiceGeneric appSvc = new ApplicationServiceGeneric(
            directoryManager, permissionManager, directoryInstanceLoader, eventPublisher, eventStore, webhookRegistry);

        when(eventStore.getNewEvents(eventToken))
            .thenReturn(new Events(ImmutableList.<OperationEvent>of(), "new-event-token"));

        Events events = appSvc.getNewEvents(application, eventToken);

        assertThat(events.getEvents(), emptyIterable());
        assertEquals("new-event-token", events.getNewEventToken());
    }

    @Test
    public void getNewEventsReturnsEventsFromActiveDirectories() throws Exception
    {
        String eventToken = "event-token";

        ApplicationServiceGeneric appSvc = new ApplicationServiceGeneric(
            directoryManager, permissionManager, directoryInstanceLoader, eventPublisher, eventStore, webhookRegistry);

        Directory directory = mock(Directory.class);
        when(directory.isActive()).thenReturn(true);
        when(directory.getId()).thenReturn(1L);

        when(application.getDirectoryMappings())
            .thenReturn(ImmutableList.<DirectoryMapping>of(new DirectoryMapping(application, directory, true)));

        User user = mock(User.class);
        Directory eventDirectory = mock(Directory.class);
        when(eventDirectory.getId()).thenReturn(1L);
        UserEvent event = new UserEvent(Operation.CREATED, eventDirectory, user,
                                                         ImmutableMap.<String, Set<String>>of(),
                                                         ImmutableSet.<String>of());

        when(eventStore.getNewEvents(eventToken))
            .thenReturn(new Events(ImmutableList.<OperationEvent>of(event), "new-event-token"));

        Events events = appSvc.getNewEvents(application, eventToken);

        assertThat(Iterables.getOnlyElement(events.getEvents()), creationEvent(is(eventDirectory)));
    }

    @Test
    public void getNewEventsDiscardsEventsFromInactiveDirectories() throws Exception
    {
        String eventToken = "event-token";

        ApplicationServiceGeneric appSvc = new ApplicationServiceGeneric(
            directoryManager, permissionManager, directoryInstanceLoader, eventPublisher, eventStore, webhookRegistry);

        Directory activeDirectory = mock(Directory.class);
        when(activeDirectory.isActive()).thenReturn(true);
        when(activeDirectory.getId()).thenReturn(1L);

        Directory inactiveDirectory = mock(Directory.class);
        when(inactiveDirectory.isActive()).thenReturn(false);
        when(inactiveDirectory.getId()).thenReturn(2L);

        when(application.getDirectoryMappings())
            .thenReturn(ImmutableList.<DirectoryMapping>of(new DirectoryMapping(application, activeDirectory, true)));

        User user = mock(User.class);
        UserEvent event = new UserEvent(Operation.CREATED, inactiveDirectory, user,
                                                           ImmutableMap.<String, Set<String>>of(),
                                                           ImmutableSet.<String>of());

        when(eventStore.getNewEvents(eventToken))
            .thenReturn(new Events(ImmutableList.<OperationEvent>of(event),
                                   "new-event-token"));

        Events events = appSvc.getNewEvents(application, eventToken);

        assertThat(events.getEvents(), emptyIterable());
    }

    @Test
    public void getNewEventsKeepsEventsNotAssociatedWithAnyDirectory() throws Exception
    {
        String eventToken = "event-token";

        ApplicationServiceGeneric appSvc = new ApplicationServiceGeneric(
            directoryManager, permissionManager, directoryInstanceLoader, eventPublisher, eventStore, webhookRegistry);

        OperationEvent eventNotAssociatedToAnyDirectory = AliasEvent.created(application, "username", "alias");

        when(eventStore.getNewEvents(eventToken))
            .thenReturn(new Events(ImmutableList.<OperationEvent>of(eventNotAssociatedToAnyDirectory),
                                   "new-event-token"));

        Events events = appSvc.getNewEvents(application, eventToken);

        assertThat(Iterables.getOnlyElement(events.getEvents()), creationEvent(nullValue(Directory.class)));
    }
}

class OperationEventMatcher extends TypeSafeMatcher<OperationEvent>
{
    private final Operation operation;
    private final Matcher<Directory> directory;

    public OperationEventMatcher(Operation operation, Matcher<Directory> directory)
    {
        this.operation = operation;
        this.directory = directory;
    }

    @Override
    public boolean matchesSafely(OperationEvent item)
    {
        return operation.equals(item.getOperation())
               && directory.matches(item.getDirectory());
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("a " + operation + " event in directory " + directory);
    }

    @Factory
    public static Matcher<OperationEvent> creationEvent(Matcher<Directory> directory)
    {
        return new OperationEventMatcher(Operation.CREATED, directory);
    }
}
