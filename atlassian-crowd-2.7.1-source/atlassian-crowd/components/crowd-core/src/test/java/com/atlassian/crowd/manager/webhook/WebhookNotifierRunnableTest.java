package com.atlassian.crowd.manager.webhook;

import java.io.IOException;

import com.atlassian.crowd.model.webhook.Webhook;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WebhookNotifierRunnableTest
{
    private static final long WEBHOOK_ID = 1L;

    // note that the constructor works because by default getId() on the Webhook mock returns 0 (not null)
    @InjectMocks
    private WebhookNotifierRunnable runnable;

    @Mock
    private Webhook webhook;

    @Mock
    private WebhookPinger webhookPinger;

    @Mock
    private WebhookNotificationListener webhookNotificationListener;

    @Test
    public void shouldPingTheWebhook() throws Exception
    {
        runnable.run();

        verify(webhookPinger).ping(webhook);
    }

    @Test
    public void shouldNotifyIfPingIsSuccessful() throws Exception
    {
        when(webhook.getId()).thenReturn(WEBHOOK_ID);

        doNothing().when(webhookPinger).ping(webhook);

        runnable.run();

        verify(webhookPinger).ping(webhook);
        verify(webhookNotificationListener).onPingSuccess(WEBHOOK_ID);
        verifyNoMoreInteractions(webhookNotificationListener);
    }

    @Test
    public void shouldNotifyIfPingFails() throws Exception
    {
        when(webhook.getId()).thenReturn(WEBHOOK_ID);

        doThrow(new IOException("IO error")).when(webhookPinger).ping(webhook);

        runnable.run();

        verify(webhookPinger).ping(webhook);
        verify(webhookNotificationListener).onPingFailure(WEBHOOK_ID);
        verifyNoMoreInteractions(webhookNotificationListener);
    }
}
