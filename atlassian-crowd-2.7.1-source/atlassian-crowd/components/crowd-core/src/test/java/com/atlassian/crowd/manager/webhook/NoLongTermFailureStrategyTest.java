package com.atlassian.crowd.manager.webhook;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.atlassian.crowd.model.webhook.Webhook;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NoLongTermFailureStrategyTest
{
    private static final Date NOW = new Date(1375000000000L); // some moment in 2013
    private static final Date A_FEW_MOMENTS_AGO = new Date(NOW.getTime() - 1000L);
    private static final Date LONG_TIME_AGO = new Date(0L);

    private static final long MIN_FAILURES = 50L;
    private static final long MIN_INTERVAL_MILLIS = TimeUnit.DAYS.toMillis(7);

    @Mock NoLongTermFailureStrategy.Clock clock;

    private NoLongTermFailureStrategy strategy;

    @Before
    public void setUp()
    {
        strategy = new NoLongTermFailureStrategy(MIN_FAILURES, MIN_INTERVAL_MILLIS, clock);

        when(clock.getCurrentDate()).thenReturn(NOW);
    }

    @Test
    public void registerSuccessResetsWebhookToGoodStanding() throws Exception
    {
        // a webhook that has been failing for a while
        Webhook webhook = mock(Webhook.class);
        when(webhook.getOldestFailureDate()).thenReturn(LONG_TIME_AGO);
        when(webhook.getFailuresSinceLastSuccess()).thenReturn(5L);

        Webhook returnedWebhook = strategy.registerSuccess(webhook);

        assertThat("Oldest failure date is reset", returnedWebhook.getOldestFailureDate(), nullValue(Date.class));
        assertThat("Failure count is reset", returnedWebhook.getFailuresSinceLastSuccess(), is(0L));

        verifyZeroInteractions(clock);
    }

    @Test
    public void registerFailureWhenTheWebhookFailsForTheFirstTime() throws Exception
    {
        // a webhook that was working fine (until now)
        Webhook webhook = mock(Webhook.class);
        when(webhook.getOldestFailureDate()).thenReturn(null);
        when(webhook.getFailuresSinceLastSuccess()).thenReturn(0L);

        Webhook returnedWebhook = strategy.registerFailure(webhook);

        assertThat("Oldest failure date is set", returnedWebhook.getOldestFailureDate(), is(NOW));
        assertThat("Failure count is incremented", returnedWebhook.getFailuresSinceLastSuccess(), is(0L + 1));

        verify(clock).getCurrentDate();
    }

    @Test
    public void registerFailureWhenTheWebhookFailsAgain() throws Exception
    {
        // a webhook that has been failing for a while
        Webhook webhook = mock(Webhook.class);
        when(webhook.getOldestFailureDate()).thenReturn(LONG_TIME_AGO);
        when(webhook.getFailuresSinceLastSuccess()).thenReturn(5L);

        Webhook returnedWebhook = strategy.registerFailure(webhook);

        assertThat("Oldest failure date is preserved", returnedWebhook.getOldestFailureDate(), is(LONG_TIME_AGO));
        assertThat("Failure count is incremented", returnedWebhook.getFailuresSinceLastSuccess(), is(5L + 1));

        verifyZeroInteractions(clock);
    }

    @Test
    public void aWebhookThatIsNotFailingIsInGoodStanding() throws Exception
    {
        // a webhook that was working fine
        Webhook webhook = mock(Webhook.class);
        when(webhook.getOldestFailureDate()).thenReturn(null);
        when(webhook.getFailuresSinceLastSuccess()).thenReturn(0L);

        assertTrue(strategy.isInGoodStanding(webhook));

        verifyZeroInteractions(clock);
    }

    @Test
    public void aWebhookThatHasFailedALotRecentlyIsStillInGoodStanding() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getOldestFailureDate()).thenReturn(A_FEW_MOMENTS_AGO);
        when(webhook.getFailuresSinceLastSuccess()).thenReturn(MIN_FAILURES + 1); // many failures in a short period

        assertTrue(strategy.isInGoodStanding(webhook));

        verify(clock).getCurrentDate();
    }

    @Test
    public void aWebhookThatHasFailedInfrequentlyIsStillInGoodStanding() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getOldestFailureDate()).thenReturn(LONG_TIME_AGO);
        when(webhook.getFailuresSinceLastSuccess()).thenReturn(2L); // few failures in a long period

        assertTrue(strategy.isInGoodStanding(webhook));

        verifyZeroInteractions(clock);
    }

    @Test
    public void aWebhookThatHasFailedManyTimesForAWhileIsNotInGoodStanding() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getOldestFailureDate()).thenReturn(LONG_TIME_AGO);
        when(webhook.getFailuresSinceLastSuccess()).thenReturn(MIN_FAILURES + 1); // many failures in a long period

        assertFalse(strategy.isInGoodStanding(webhook));

        verify(clock).getCurrentDate();
    }
}
