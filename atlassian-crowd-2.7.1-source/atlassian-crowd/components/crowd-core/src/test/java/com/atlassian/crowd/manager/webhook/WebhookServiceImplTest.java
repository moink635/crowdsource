package com.atlassian.crowd.manager.webhook;

import com.atlassian.crowd.model.webhook.Webhook;

import com.google.common.collect.ImmutableList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WebhookServiceImplTest
{
    private static final long WEBHOOK_ID = 1L;

    @InjectMocks
    private WebhookServiceImpl webhookService;

    @Mock private WebhookRegistry webhookRegistry;
    @Mock private WebhookPinger webhookPinger;
    @Mock private KeyedExecutor<Long> keyedExecutor;
    @Mock private WebhookNotificationListener webhookNotificationListener;

    @Test
    public void shouldNotifyAllWebhooks() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getId()).thenReturn(WEBHOOK_ID);
        when(webhookRegistry.findAll()).thenReturn(ImmutableList.of(webhook));

        webhookService.notifyWebhooks();

        verify(keyedExecutor).execute(any(WebhookNotifierRunnable.class), eq(WEBHOOK_ID));
    }
}
