package com.atlassian.crowd.util;

import java.util.Collections;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestI18nHelperImpl
{
    private I18nHelperImpl i18nHelper = null;
    private static final String LICENSE_RESOURCE_LIMIT_TEXT = "license.resource.limit.text";

    @Before
    public void setUp() throws Exception
    {
        final ResourceBundleProvider provider = mock(ResourceBundleProvider.class);
        when(provider.getResourceBundles()).thenReturn(ImmutableList.of(ResourceBundle.getBundle(TestI18nHelperImpl.class.getName(), Locale.getDefault())));

        i18nHelper = new I18nHelperImpl(ImmutableList.of(provider));
    }

    @Test
    public void testGetUnescapedText() throws Exception
    {
        String text = i18nHelper.getUnescapedText(LICENSE_RESOURCE_LIMIT_TEXT);
        assertNotNull(text);
        assertTrue(text.length() > 0);
        assertEquals(text, "Some random test text {0}");
    }

    @Test
    public void testTextWithParam() throws Exception
    {
        String text = i18nHelper.getText(LICENSE_RESOURCE_LIMIT_TEXT, "Crowd");
        assertNotNull(text);
        assertTrue(text.length() > 0);
        assertEquals(text, "Some random test text Crowd");
    }

    @Test(expected = NullPointerException.class)
    public void getAllTranslationsForPrefixWithNullKeyShouldThrow() throws Exception
    {
        i18nHelper.getAllTranslationsForPrefix(null);
    }

    @Test
    public void getAllTranslationsForPrefixWithNonMatchingKeyShouldReturnEmpty() throws Exception
    {
        final Map<String,String> translations = i18nHelper.getAllTranslationsForPrefix("somethingthatdoesntexist");
        assertThat(translations, is(Collections.<String, String>emptyMap()));
    }

    @Test
    public void getAllTranslationsForPrefixWithMatchingKey() throws Exception
    {
        final Map<String,String> translations = i18nHelper.getAllTranslationsForPrefix("foo");
        assertThat(translations.size(), is(2));
        assertThat(translations, hasEntry("foo.something", "blah"));
        assertThat(translations, hasEntry("foofoo.someotherthing", "blahblah"));
    }
}
