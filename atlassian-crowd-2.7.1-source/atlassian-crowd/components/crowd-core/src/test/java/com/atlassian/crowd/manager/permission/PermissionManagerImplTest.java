package com.atlassian.crowd.manager.permission;

import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.directory.DirectoryImpl;

import com.google.common.collect.Sets;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

/**
 * PermissionManagerImpl Tester.
 */
public class PermissionManagerImplTest
{
    private PermissionManager permissionManager;
    private DirectoryImpl directory = null;
    private ApplicationImpl application = null;

    @Before
    public void setUp() throws Exception
    {
        permissionManager = new PermissionManagerImpl(mock(ApplicationDAO.class), mock(DirectoryDao.class));

        directory = new DirectoryImpl();

        application = ApplicationImpl.newInstanceWithPassword("Test Application", ApplicationType.GENERIC_APPLICATION, "secret");

        directory.setAllowedOperations(Sets.newHashSet(OperationType.CREATE_USER, OperationType.UPDATE_USER));
    }

    @After
    public void tearDown() throws Exception
    {
        permissionManager = null;
    }

    @Test
    public void testDirectoryHasPermission()
    {
        boolean allowed = permissionManager.hasPermission(directory, OperationType.CREATE_USER);

        assertTrue(allowed);
    }

    @Test
    public void testDirectoryDoesNotHavePermission()
    {
        boolean allowed = permissionManager.hasPermission(directory, OperationType.DELETE_USER);

        assertFalse(allowed);
    }

    @Test(expected = NullPointerException.class)
    public void testHasPermissionIllegalArgumentNullDirectoryFails()
    {
        permissionManager.hasPermission(null, OperationType.DELETE_USER);
    }

    @Test(expected = NullPointerException.class)
    public void testHasPermissionIllegalArgumentNullOperationTypeFails()
    {
        permissionManager.hasPermission(directory, null);
    }

    @Test(expected = NullPointerException.class)
    public void testAddPermissionToApplicationDirectoryMappingNullApplicationFails() throws Exception
    {
        permissionManager.addPermission(null, directory, OperationType.CREATE_GROUP);
    }

    @Test(expected = NullPointerException.class)
    public void testAddPermissionToApplicationDirectoryMappingNullDirectoryFails() throws Exception
    {
        permissionManager.addPermission(application, null, OperationType.CREATE_GROUP);
    }

    @Test(expected = NullPointerException.class)
    public void testAddPermissionToApplicationDirectoryMappingNullOperationTypeFails() throws Exception
    {
        permissionManager.addPermission(application, directory, null);
    }

    @Test(expected = NullPointerException.class)
    public void testHasPermissionForApplicationDirectoryMappingNullApplicationFails()
    {
        permissionManager.hasPermission(null, directory, OperationType.CREATE_GROUP);
    }

    @Test(expected = NullPointerException.class)
    public void testHasPermissionForApplicationDirectoryMappingNullDirectoryFails()
    {
        permissionManager.hasPermission(application, null, OperationType.CREATE_GROUP);
    }

    @Test(expected = NullPointerException.class)
    public void testHasPermissionForApplicationDirectoryMappingNullOperationTypeFails()
    {
        permissionManager.hasPermission(application, directory, null);
    }

    @Test(expected = NullPointerException.class)
    public void testRemovePermissionNullApplicationFails() throws Exception
    {
        permissionManager.removePermission(null, directory, OperationType.CREATE_GROUP);
    }

    @Test(expected = NullPointerException.class)
    public void testRemovePermissionNullDirectoryFails() throws Exception
    {
        permissionManager.removePermission(application, null, OperationType.CREATE_GROUP);
    }

    @Test(expected = NullPointerException.class)
    public void testRemovePermissionNullOperationTypeFails() throws Exception
    {
        permissionManager.removePermission(application, directory, null);
    }
}
