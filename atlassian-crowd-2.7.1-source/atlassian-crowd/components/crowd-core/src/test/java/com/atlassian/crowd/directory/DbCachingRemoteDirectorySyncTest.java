package com.atlassian.crowd.directory;

import com.atlassian.crowd.directory.ldap.cache.CacheRefresher;
import com.atlassian.crowd.directory.ldap.cache.DirectoryCache;
import com.atlassian.crowd.directory.ldap.cache.DirectoryCacheFactory;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.directory.SynchronisationMode;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit tests for {@link DbCachingRemoteDirectory#synchroniseCache(com.atlassian.crowd.manager.directory.SynchronisationMode, com.atlassian.crowd.manager.directory.SynchronisationStatusManager)}
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class DbCachingRemoteDirectorySyncTest
{
    private static final long DIRECTORY_ID = 1L;

    private DbCachingRemoteDirectory dbCachingRemoteDirectory;

    @Mock private RemoteDirectory remoteDirectory;
    @Mock private InternalRemoteDirectory internalDirectory;
    @Mock private DirectoryCacheFactory directoryCacheFactory;
    @Mock private SynchronisationStatusManager synchronisationStatusManager;
    @Mock private DirectoryCache directoryCache;
    @Mock private CacheRefresher cacheRefresher;

    @Before
    public void createObjectUnderTest()
    {
        dbCachingRemoteDirectory = new DbCachingRemoteDirectory(remoteDirectory, internalDirectory, directoryCacheFactory, cacheRefresher);
    }

    @Before
    public void initCollaborators()
    {
        when(remoteDirectory.getDirectoryId()).thenReturn(DIRECTORY_ID);

        when(directoryCacheFactory.createDirectoryCache(remoteDirectory, internalDirectory)).thenReturn(directoryCache);
    }

    @Test
    public void fullSynchronisationSucceeds() throws Exception
    {
        dbCachingRemoteDirectory.synchroniseCache(SynchronisationMode.FULL, synchronisationStatusManager);

        verify(cacheRefresher).synchroniseAll(directoryCache);

        verify(cacheRefresher, never()).synchroniseChanges(directoryCache);

        verify(synchronisationStatusManager).syncStatus(DIRECTORY_ID, "directory.caching.sync.full");
        verify(synchronisationStatusManager).syncStatus(DIRECTORY_ID, "directory.caching.sync.completed.FULL");
    }

    @Test
    public void fullSynchronisationFails() throws Exception
    {
        doThrow(new OperationFailedException("sync failed")).when(cacheRefresher).synchroniseAll(directoryCache);

        try
        {
            dbCachingRemoteDirectory.synchroniseCache(SynchronisationMode.FULL, synchronisationStatusManager);
            fail("OperationFailedException expected");
        }
        catch (OperationFailedException e)
        {
            // exception expected, now verify the mocks

            verify(cacheRefresher, never()).synchroniseChanges(directoryCache);

            verify(synchronisationStatusManager).syncStatus(DIRECTORY_ID, "directory.caching.sync.full");
            verify(synchronisationStatusManager).syncStatus(DIRECTORY_ID, "directory.caching.sync.completed.error");
        }
    }

    @Test
    public void incrementalSynchronisationSucceeds() throws Exception
    {
        when(cacheRefresher.synchroniseChanges(directoryCache)).thenReturn(true); // incremental sync succeeds

        dbCachingRemoteDirectory.synchroniseCache(SynchronisationMode.INCREMENTAL, synchronisationStatusManager);

        verify(cacheRefresher).synchroniseChanges(directoryCache);

        verify(cacheRefresher, never()).synchroniseAll(directoryCache);

        verify(synchronisationStatusManager).syncStatus(DIRECTORY_ID, "directory.caching.sync.incremental");
        verify(synchronisationStatusManager).syncStatus(DIRECTORY_ID, "directory.caching.sync.completed.INCREMENTAL");
    }

    @Test
    public void whenIncrementalSynchronisationReturnsFalseThenFullSyncInRun() throws Exception
    {
        when(cacheRefresher.synchroniseChanges(directoryCache)).thenReturn(false); // incremental sync cannot complete

        dbCachingRemoteDirectory.synchroniseCache(SynchronisationMode.INCREMENTAL, synchronisationStatusManager);

        verify(cacheRefresher).synchroniseChanges(directoryCache);

        verify(cacheRefresher).synchroniseAll(directoryCache);

        verify(synchronisationStatusManager).syncStatus(DIRECTORY_ID, "directory.caching.sync.incremental");
        verify(synchronisationStatusManager).syncStatus(DIRECTORY_ID, "directory.caching.sync.full");
        verify(synchronisationStatusManager).syncStatus(DIRECTORY_ID, "directory.caching.sync.completed.FULL");
    }

    @Test
    public void whenIncrementalSynchronisationUnexpectedlyFailsThenFullSyncIsRun() throws Exception
    {
        when(cacheRefresher.synchroniseChanges(directoryCache))
            .thenThrow(new RuntimeException("something went awfully bad with incremental sync"));

        dbCachingRemoteDirectory.synchroniseCache(SynchronisationMode.INCREMENTAL, synchronisationStatusManager);

        verify(cacheRefresher).synchroniseChanges(directoryCache);

        verify(cacheRefresher).synchroniseAll(directoryCache);

        verify(synchronisationStatusManager).syncStatus(DIRECTORY_ID, "directory.caching.sync.incremental");
        verify(synchronisationStatusManager).syncStatus(DIRECTORY_ID, "directory.caching.sync.full");
        verify(synchronisationStatusManager).syncStatus(DIRECTORY_ID, "directory.caching.sync.completed.FULL");
    }
}
