package com.atlassian.crowd.core.event.listener;

import java.util.Arrays;
import java.util.Collections;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.event.user.UserAuthenticatedEvent;
import com.atlassian.crowd.event.user.UserAuthenticationSucceededEvent;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

import com.google.common.collect.ImmutableMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AutoGroupAdderListenerTest
{
    private static final String USER_NAME = "user";
    private static final User USER = new UserTemplate(USER_NAME);
    private static final long DIRECTORY_ID = 1L;

    @Mock private Directory directory;
    @Mock private DirectoryManager directoryManager;
    @Mock private Application application;
    @Mock private UserWithAttributes userWithAttributes;
    private UserAuthenticatedEvent event;

    private AutoGroupAdderListener autoGroupAdderListener;

    @Before
    public void setUp() throws Exception
    {
        autoGroupAdderListener = new AutoGroupAdderListener(directoryManager);

        event = new UserAuthenticatedEvent(this, directory, application, USER);

        when(directory.getId()).thenReturn(DIRECTORY_ID);
    }

    private static MembershipQuery<String> groupsOfUser()
    {
        return QueryBuilder.createMembershipQuery(EntityQuery.ALL_RESULTS, 0, false,
                                                  EntityDescriptor.group(), String.class,
                                                  EntityDescriptor.user(), USER_NAME);
    }

    @Test
    public void testHandleEventForDirectoryWithAutoAdd() throws Exception
    {
        when(directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_AUTO_ADD_GROUPS)).thenReturn("group1|group2|group3");
        when(directoryManager.findUserWithAttributesByName(DIRECTORY_ID, USER_NAME)).thenReturn(userWithAttributes);
        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, groupsOfUser())).thenReturn(Collections.<String>emptyList());

        autoGroupAdderListener.handleEvent(event);

        verify(directoryManager).findUserWithAttributesByName(DIRECTORY_ID, USER_NAME);
        verify(directoryManager).searchDirectGroupRelationships(DIRECTORY_ID, groupsOfUser());
        verify(directoryManager).addUserToGroup(DIRECTORY_ID, USER_NAME, "group1");
        verify(directoryManager).addUserToGroup(DIRECTORY_ID, USER_NAME, "group2");
        verify(directoryManager).addUserToGroup(DIRECTORY_ID, USER_NAME, "group3");
        verify(directoryManager).storeUserAttributes(DIRECTORY_ID, USER_NAME, ImmutableMap.of(AutoGroupAdderListener.AUTO_GROUPS_ADDED, Collections.singleton("true")));

        verifyNoMoreInteractions(directoryManager);
    }

    @Test
    public void testHandleEventForDirectoryWithAutoAddWhenOneGroupDoesNotExist() throws Exception
    {
        when(directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_AUTO_ADD_GROUPS)).thenReturn("group1|group2|group3");
        when(directoryManager.findUserWithAttributesByName(DIRECTORY_ID, USER_NAME)).thenReturn(userWithAttributes);
        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, groupsOfUser())).thenReturn(Collections.<String>emptyList());
        doThrow(new GroupNotFoundException("group2")).when(directoryManager).addUserToGroup(DIRECTORY_ID, USER_NAME, "group2");

        autoGroupAdderListener.handleEvent(event);

        verify(directoryManager).findUserWithAttributesByName(DIRECTORY_ID, USER_NAME);
        verify(directoryManager).searchDirectGroupRelationships(DIRECTORY_ID, groupsOfUser());
        verify(directoryManager).addUserToGroup(DIRECTORY_ID, USER_NAME, "group1");
        verify(directoryManager).addUserToGroup(DIRECTORY_ID, USER_NAME, "group2");
        verify(directoryManager).addUserToGroup(DIRECTORY_ID, USER_NAME, "group3"); // still executes
        verify(directoryManager).storeUserAttributes(DIRECTORY_ID, USER_NAME, ImmutableMap.of(AutoGroupAdderListener.AUTO_GROUPS_ADDED, Collections.singleton("true")));

        verifyNoMoreInteractions(directoryManager);
    }

    @Test
    public void testHandleEventForUserWithSomeExistingMemberships() throws Exception
    {
        when(directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_AUTO_ADD_GROUPS)).thenReturn("group1|group2|group3");
        when(directoryManager.findUserWithAttributesByName(DIRECTORY_ID, USER_NAME)).thenReturn(userWithAttributes);
        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, groupsOfUser())).thenReturn(Collections.singletonList("group2"));

        autoGroupAdderListener.handleEvent(event);

        verify(directoryManager).findUserWithAttributesByName(DIRECTORY_ID, USER_NAME);
        verify(directoryManager).searchDirectGroupRelationships(DIRECTORY_ID, groupsOfUser());
        verify(directoryManager).addUserToGroup(DIRECTORY_ID, USER_NAME, "group1");
        verify(directoryManager).addUserToGroup(DIRECTORY_ID, USER_NAME, "group3");
        verify(directoryManager).storeUserAttributes(DIRECTORY_ID, USER_NAME, ImmutableMap.of(AutoGroupAdderListener.AUTO_GROUPS_ADDED, Collections.singleton("true")));

        verifyNoMoreInteractions(directoryManager);
    }

    @Test
    public void testHandleEventForUserWithAllExistingMemberships() throws Exception
    {
        when(directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_AUTO_ADD_GROUPS)).thenReturn("group1|group2|group3");
        when(directoryManager.findUserWithAttributesByName(DIRECTORY_ID, USER_NAME)).thenReturn(userWithAttributes);
        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, groupsOfUser())).thenReturn(Arrays.asList("group1", "group2", "group3"));

        autoGroupAdderListener.handleEvent(event);

        verify(directoryManager).findUserWithAttributesByName(DIRECTORY_ID, USER_NAME);
        verify(directoryManager).searchDirectGroupRelationships(DIRECTORY_ID, groupsOfUser());
        verify(directoryManager).storeUserAttributes(DIRECTORY_ID, USER_NAME, ImmutableMap.of(AutoGroupAdderListener.AUTO_GROUPS_ADDED, Collections.singleton("true")));

        verifyNoMoreInteractions(directoryManager);
    }

    @Test
    public void testHandleEventForUserWithMixedCaseExistingMemberships() throws Exception
    {
        when(directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_AUTO_ADD_GROUPS)).thenReturn("group1|group2|group3");
        when(directoryManager.findUserWithAttributesByName(DIRECTORY_ID, USER_NAME)).thenReturn(userWithAttributes);
        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, groupsOfUser())).thenReturn(Arrays.asList("Group1", "GROUP2", "group3"));

        autoGroupAdderListener.handleEvent(event);

        verify(directoryManager).findUserWithAttributesByName(DIRECTORY_ID, USER_NAME);
        verify(directoryManager).searchDirectGroupRelationships(DIRECTORY_ID, groupsOfUser());
        verify(directoryManager).storeUserAttributes(DIRECTORY_ID, USER_NAME, ImmutableMap.of(AutoGroupAdderListener.AUTO_GROUPS_ADDED, Collections.singleton("true")));

        verifyNoMoreInteractions(directoryManager);
    }

    @Test
    public void testHandleEventForDirectoryWithNoPermission()
    {
        when(directory.getAllowedOperations()).thenReturn(Collections.<OperationType>emptySet());

        autoGroupAdderListener.handleEvent(event);

        verifyNoMoreInteractions(directoryManager);
    }

    @Test
    public void testHandleEventForDirectoryWithoutAutoAdd() throws Exception
    {
        when(directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_AUTO_ADD_GROUPS)).thenReturn(null);

        autoGroupAdderListener.handleEvent(event);

        verifyNoMoreInteractions(directoryManager);
    }

    @Test
    public void testHandleEventWithPreviouslyAuthenticateUser() throws Exception
    {
        when(directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_AUTO_ADD_GROUPS)).thenReturn("group1|group2|group3");
        when(userWithAttributes.getValue(AutoGroupAdderListener.AUTO_GROUPS_ADDED)).thenReturn("true");
        when(directoryManager.findUserWithAttributesByName(DIRECTORY_ID, USER_NAME)).thenReturn(userWithAttributes);

        autoGroupAdderListener.handleEvent(event);

        verify(directoryManager).findUserWithAttributesByName(DIRECTORY_ID, USER_NAME);

        verifyNoMoreInteractions(directoryManager);
    }

    @Test
    public void handleEventAcceptsUserAuthenticationSucceededEvent() throws Exception
    {
        UserTemplate user = new UserTemplate(USER_NAME, DIRECTORY_ID);

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        Token token = new Token.Builder(DIRECTORY_ID, USER_NAME, "identifier-hash", 1234L, "random-hash").create();
        UserAuthenticationSucceededEvent event = new UserAuthenticationSucceededEvent(this, user, application, token);

        when(directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_AUTO_ADD_GROUPS)).thenReturn("group1|group2|group3");
        when(directoryManager.findUserWithAttributesByName(DIRECTORY_ID, USER_NAME)).thenReturn(userWithAttributes);
        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, groupsOfUser())).thenReturn(Collections.<String>emptyList());

        autoGroupAdderListener.handleEvent(event);

        verify(directoryManager).findUserWithAttributesByName(DIRECTORY_ID, USER_NAME);
        verify(directoryManager).searchDirectGroupRelationships(DIRECTORY_ID, groupsOfUser());
        verify(directoryManager).addUserToGroup(DIRECTORY_ID, USER_NAME, "group1");
        verify(directoryManager).addUserToGroup(DIRECTORY_ID, USER_NAME, "group2");
        verify(directoryManager).addUserToGroup(DIRECTORY_ID, USER_NAME, "group3");
        verify(directoryManager).storeUserAttributes(DIRECTORY_ID, USER_NAME, ImmutableMap.of(AutoGroupAdderListener.AUTO_GROUPS_ADDED, Collections.singleton("true")));
        verify(directoryManager).findDirectoryById(DIRECTORY_ID);

        verifyNoMoreInteractions(directoryManager);
    }
}
