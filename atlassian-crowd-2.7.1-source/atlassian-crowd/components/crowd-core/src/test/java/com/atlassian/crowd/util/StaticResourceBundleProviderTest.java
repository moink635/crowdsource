package com.atlassian.crowd.util;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 *
 * @since v2.7
 */
public class StaticResourceBundleProviderTest
{
    @Test
    public void noLocationShouldReturnNoBundle() throws Exception
    {
        final Iterable<ResourceBundle> bundles = provider(Locale.FRENCH, ImmutableList.<String>of()).getResourceBundles();

        assertThat(bundles, emptyIterable());
    }

    @Test
    public void oneLocationWithTranslatedBundleShouldReturnOneBundle() throws Exception
    {
        final Iterable<ResourceBundle> bundles = provider(Locale.FRENCH, ImmutableList.of("testBundle1")).getResourceBundles();

        assertThat(bundles, Matchers.<ResourceBundle>iterableWithSize(1));
        final ResourceBundle bundle = Iterables.getOnlyElement(bundles);

        assertThat(bundle.getString("key1"), is("foo"));
        assertThat(bundle.getString("key2"), is("la barre"));
    }

    @Test
    public void oneLocationWithoutTranslatedBundleShouldReturnOneBundle() throws Exception
    {
        final Iterable<ResourceBundle> bundles = provider(Locale.GERMAN, ImmutableList.of("testBundle1")).getResourceBundles();

        assertThat(bundles, Matchers.<ResourceBundle>iterableWithSize(1));
        final ResourceBundle bundle = Iterables.getOnlyElement(bundles);
        assertThat(bundle.getString("key1"), is("foo"));
        assertThat(bundle.getString("key2"), is("bar"));
    }

    @Test
    public void twoLocationsShouldReturnTwoBundles() throws Exception
    {
        final Iterable<ResourceBundle> bundles = provider(Locale.GERMAN, ImmutableList.of("testBundle1", "testBundle2")).getResourceBundles();

        assertThat(bundles, Matchers.<ResourceBundle>iterableWithSize(2));
        final Iterator<ResourceBundle> iterator = bundles.iterator();
        final ResourceBundle bundle1 = iterator.next();
        assertThat(bundle1.getString("key1"), is("foo"));
        assertThat(bundle1.getString("key2"), is("bar"));

        final ResourceBundle bundle2 = iterator.next();
        assertThat(bundle2.getString("key3"), is("wizz"));
        assertThat(bundle2.getString("key4"), is("bang"));
    }

    private StaticResourceBundleProvider provider(final Locale locale, final List<String> locations)
    {
        return new StaticResourceBundleProvider(new I18nHelperConfigurationImpl(locale, locations));
    }
}
