package com.atlassian.crowd.manager.webhook;

import com.atlassian.crowd.model.webhook.Webhook;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WebhookNotificationListenerImplTest
{
    private static final long WEBHOOK_ID = 1L;

    @InjectMocks
    private WebhookNotificationListenerImpl webhookService;

    @Mock
    private WebhookRegistry webhookRegistry;

    @Mock
    private WebhookHealthStrategy webhookHealthStrategy;

    @Test
    public void shouldUpdateWebhookIfItIsInGoodStandingAfterSuccessfulPing() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        Webhook webhookTemplate = mock(Webhook.class);

        when(webhookRegistry.findById(WEBHOOK_ID)).thenReturn(webhook);

        when(webhookHealthStrategy.registerSuccess(webhook)).thenReturn(webhookTemplate);

        when(webhookHealthStrategy.isInGoodStanding(webhookTemplate)).thenReturn(true);

        webhookService.onPingSuccess(WEBHOOK_ID);

        verify(webhookHealthStrategy).registerSuccess(webhook);
        verify(webhookRegistry).update(webhookTemplate);
        verify(webhookRegistry, never()).remove(any(Webhook.class));
    }

    @Test
    public void shouldRemoveWebhookIfItIsInBadStandingAfterSuccessfulPing() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        Webhook webhookTemplate = mock(Webhook.class);

        when(webhookRegistry.findById(WEBHOOK_ID)).thenReturn(webhook);

        when(webhookHealthStrategy.registerSuccess(webhook)).thenReturn(webhookTemplate);

        when(webhookHealthStrategy.isInGoodStanding(webhookTemplate)).thenReturn(false);

        webhookService.onPingSuccess(WEBHOOK_ID);

        verify(webhookHealthStrategy).registerSuccess(webhook);
        verify(webhookRegistry).remove(webhookTemplate);
        verify(webhookRegistry, never()).update(any(Webhook.class));
    }

    @Test
    public void shouldUpdateWebhookIfItIsInGoodStandingAfterFailedPing() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        Webhook webhookTemplate = mock(Webhook.class);

        when(webhookRegistry.findById(WEBHOOK_ID)).thenReturn(webhook);

        when(webhookHealthStrategy.registerFailure(webhook)).thenReturn(webhookTemplate);

        when(webhookHealthStrategy.isInGoodStanding(webhookTemplate)).thenReturn(true);

        webhookService.onPingFailure(WEBHOOK_ID);

        verify(webhookHealthStrategy).registerFailure(webhook);
        verify(webhookRegistry).update(webhookTemplate);
        verify(webhookRegistry, never()).remove(any(Webhook.class));

    }

    @Test
    public void shouldRemoveWebhookIfItIsInBadStandingAfterFailedPing() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        Webhook webhookTemplate = mock(Webhook.class);

        when(webhookRegistry.findById(WEBHOOK_ID)).thenReturn(webhook);

        when(webhookHealthStrategy.registerFailure(webhook)).thenReturn(webhookTemplate);

        when(webhookHealthStrategy.isInGoodStanding(webhookTemplate)).thenReturn(false);

        webhookService.onPingFailure(WEBHOOK_ID);

        verify(webhookHealthStrategy).registerFailure(webhook);
        verify(webhookRegistry).remove(webhookTemplate);
        verify(webhookRegistry, never()).update(any(Webhook.class));
    }
}
