package com.atlassian.crowd.manager.webhook;

import com.atlassian.crowd.dao.webhook.WebhookDAO;
import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.webhook.Webhook;
import com.atlassian.crowd.model.webhook.WebhookTemplate;

import com.google.common.collect.ImmutableList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.hasItems;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WebhookRegistryImplTest
{
    private static final long APPLICATION_ID = 1L;
    private static final long WEBHOOK_ID = 2L;
    private static final String ENDPOINT_URL = "http://example.test/";

    @InjectMocks private WebhookRegistryImpl registry;

    @Mock private WebhookDAO webhookDAO;

    @Test
    public void testAddNewWebhook() throws Exception
    {
        Application application = mock(Application.class);
        when(application.getId()).thenReturn(APPLICATION_ID);
        Webhook webhookTemplate = new WebhookTemplate(application, ENDPOINT_URL, null);
        Webhook savedWebhook = mock(Webhook.class);

        when(webhookDAO.findByApplicationAndEndpointUrl(application, ENDPOINT_URL))
            .thenThrow(new WebhookNotFoundException(APPLICATION_ID, ENDPOINT_URL));

        when(webhookDAO.add(webhookTemplate)).thenReturn(savedWebhook);

        assertSame(savedWebhook, registry.add(webhookTemplate));
    }

    @Test
    public void testAddWebhookAlreadyRegistered() throws Exception
    {
        Application application = mock(Application.class);
        when(application.getId()).thenReturn(APPLICATION_ID);
        Webhook webhookTemplate = new WebhookTemplate(application, ENDPOINT_URL, null);
        Webhook existingWebhook = mock(Webhook.class);

        when(webhookDAO.findByApplicationAndEndpointUrl(application, ENDPOINT_URL))
            .thenReturn(existingWebhook);

        assertSame(existingWebhook, registry.add(webhookTemplate));

        verify(webhookDAO, never()).add(any(Webhook.class));
    }

    @Test
    public void testRemoveSuccessfully() throws Exception
    {
        Webhook webhook = mock(Webhook.class);

        registry.remove(webhook);

        verify(webhookDAO).remove(webhook);
    }

    @Test (expected = WebhookNotFoundException.class)
    public void testRemoveThrowsExceptionWhenWebhookIsNotFound() throws Exception
    {
        Webhook webhook = mock(Webhook.class);

        doThrow(new WebhookNotFoundException(WEBHOOK_ID)).when(webhookDAO).remove(webhook);

        registry.remove(webhook);
    }

    @Test
    public void findByIdReturnsWebhookInstance() throws Exception
    {
        Webhook webhook = mock(Webhook.class);

        when(webhookDAO.findById(WEBHOOK_ID)).thenReturn(webhook);

        assertSame(webhook, registry.findById(WEBHOOK_ID));
    }

    @Test(expected = WebhookNotFoundException.class)
    public void findByIdThrowsExceptionWhenWebhookIsNotFound() throws Exception
    {
        when(webhookDAO.findById(WEBHOOK_ID)).thenThrow(new WebhookNotFoundException(WEBHOOK_ID));

        registry.findById(WEBHOOK_ID);
    }

    @Test
    public void testFindAll()
    {
        Webhook webhook = mock(Webhook.class);

        when(webhookDAO.findAll()).thenReturn(ImmutableList.of(webhook));

        assertThat(registry.findAll(), hasItems(webhook));
    }

    @Test
    public void testUpdate() throws Exception
    {
        Webhook webhookTemplate = mock(Webhook.class);
        Webhook updatedWebhook = mock(Webhook.class);

        when(webhookDAO.update(webhookTemplate)).thenReturn(updatedWebhook);

        assertSame(updatedWebhook, registry.update(webhookTemplate));
    }
}
