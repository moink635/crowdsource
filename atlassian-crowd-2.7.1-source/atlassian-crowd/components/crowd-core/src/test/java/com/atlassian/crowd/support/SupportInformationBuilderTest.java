package com.atlassian.crowd.support;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;

public class SupportInformationBuilderTest
{
    @Test
    public void sensibleAttributesShouldBeMasked() throws Exception
    {
        SupportInformationBuilder builder = new SupportInformationBuilder();

        builder.addAttributes("directory name", ImmutableMap.of(
                "ldap.password", "secret",
                "password_regex", "[a-z]"
        ));

        String result = builder.build();
        assertThat(result, containsString("ldap.password"));
        assertThat(result, not(containsString("secret")));
        assertThat(result, containsString("[a-z]"));
    }
}
