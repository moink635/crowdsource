package com.atlassian.crowd.xwork;

import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.annotation.Retention;

/**
 * Marks a class as being safe for use as a complex form parameter. By marking a class with this
 * interceptor you are guaranteeing that there aren't any dangerous setters or getters that may be exposed
 * to the XWork parameters interceptor
 *
 * @see com.atlassian.crowd.xwork.interceptors.SafeParametersInterceptor
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
public @interface ParameterSafe
{

}
