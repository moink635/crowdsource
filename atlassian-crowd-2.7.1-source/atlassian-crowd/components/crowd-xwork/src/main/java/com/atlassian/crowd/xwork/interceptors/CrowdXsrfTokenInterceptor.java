package com.atlassian.crowd.xwork.interceptors;

import com.atlassian.crowd.xwork.interceptors.XsrfTokenInterceptor;
import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.TextProvider;

/**
 * Crowd specific implementation of XsrfTokenInterceptor.
 *
 * @since v2.3.2
 */
public class CrowdXsrfTokenInterceptor extends XsrfTokenInterceptor
{
    @Override
    protected String internationaliseErrorMessage(Action action, String messageKey)
    {
        if (action instanceof TextProvider)
        {
            TextProvider textProvider = (TextProvider) action;
            return textProvider.getText(messageKey);
        }

        return super.internationaliseErrorMessage(action, messageKey);
    }
}
