package com.atlassian.crowd.xwork;

import com.atlassian.security.random.DefaultSecureTokenGenerator;
import com.atlassian.crowd.xwork.interceptors.XsrfTokenInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Simple implementation of XsrfTokenGenerator that stores a unique value in the session. The session ID
 * itself isn't used because we don't want to risk compromising the entire session in case we don't protect
 * the XSRF token diligently enough.
 *
 * <p>Tokens are chosen to be reasonably unique (60 bits) with reasonably short representations (base64 encoded).
 */
public class SimpleXsrfTokenGenerator implements XsrfTokenGenerator
{
    public static final String TOKEN_SESSION_KEY = "atlassian.xsrf.token";

    public String getToken(HttpServletRequest request, boolean create)
    {
        HttpSession session = request.getSession();
        String token = (String) session.getAttribute(TOKEN_SESSION_KEY);

        if (create && token == null)
        {
            token = createToken();
            session.setAttribute(TOKEN_SESSION_KEY, token);
        }

        return token;
    }

    public String generateToken(HttpServletRequest request)
    {
        return getToken(request, true);
    }

    public String getXsrfTokenName()
    {
        return XsrfTokenInterceptor.REQUEST_PARAM_NAME;
    }

    public boolean validateToken(HttpServletRequest request, String token)
    {
        return token != null && token.equals(request.getSession(true).getAttribute(TOKEN_SESSION_KEY));
    }

    private String createToken()
    {
        return DefaultSecureTokenGenerator.getInstance().generateToken();
    }
}
