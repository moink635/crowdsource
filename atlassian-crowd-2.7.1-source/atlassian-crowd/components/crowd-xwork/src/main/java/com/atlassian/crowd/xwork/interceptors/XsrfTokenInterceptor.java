package com.atlassian.crowd.xwork.interceptors;

import com.opensymphony.xwork.interceptor.Interceptor;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.ActionSupport;
import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.ValidationAware;
import com.opensymphony.webwork.ServletActionContext;
import com.atlassian.crowd.xwork.RequireSecurityToken;
import com.atlassian.crowd.xwork.SimpleXsrfTokenGenerator;
import com.atlassian.crowd.xwork.XsrfTokenGenerator;
import com.atlassian.crowd.xwork.XWorkVersionSupport;

import java.lang.reflect.Method;

/**
 * Interceptor to add XSRF token protection to XWork actions. Configuring XSRF protection happens at the method
 * level, and can be done either by adding a @RequireSecurityToken annotation to the method, or by adding a
 * &lt;param name="RequireSecurityToken">[true|false]&lt;/param> parameter to the action configuration in
 * <code>xwork.xml</code>.
 *
 * <p>Configuration in xwork.xml will override any annotation-based configuration. Behaviour when a method is
 * not configured at all depends on the SecurityLevel seeting
 *
 * <p>Requests containing the HTTP header <code>X-Atlassian-Token: no-check</code> will bypass the check and always
 * succeed.
 *
 * @see SecurityLevel
 * @see #getSecurityLevel()
 *
 * TODO: Make this work with the RestrictHttpMethodInterceptor so get-only methods are not protected?
 */
public class XsrfTokenInterceptor implements Interceptor
{
    public static final String REQUEST_PARAM_NAME = "atl_token";
    public static final String CONFIG_PARAM_NAME = "RequireSecurityToken";
    public static final String VALIDATION_FAILED_ERROR_KEY = "atlassian.xwork.xsrf.badtoken";
    public static final String SECURITY_TOKEN_REQUIRED_ERROR_KEY = "atlassian.xwork.xsrf.notoken";
    public static final String OVERRIDE_HEADER_NAME = "X-Atlassian-Token";
    public static final String OVERRIDE_HEADER_VALUE = "no-check";

    public static enum SecurityLevel
    {
        /** Methods without any configuration are not protected by default */
        OPT_IN(false),
        /** Methods without any configuration are protected by default */
        OPT_OUT(true);

        private final boolean defaultProtection;

        SecurityLevel(boolean defaultProtection)
        {
            this.defaultProtection = defaultProtection;
        }

        public boolean getDefaultProtection()
        {
            return defaultProtection;
        }
    }

    private final XsrfTokenGenerator tokenGenerator;
    private final XWorkVersionSupport versionSupport = new XWorkVersionSupport();

    public XsrfTokenInterceptor()
    {
        this(new SimpleXsrfTokenGenerator());
    }

    public XsrfTokenInterceptor(XsrfTokenGenerator tokenGenerator)
    {
        this.tokenGenerator = tokenGenerator;
    }

    public String intercept(ActionInvocation invocation) throws Exception
    {
        Method invocationMethod = versionSupport.extractMethod(invocation);
        String configParam = (String) invocation.getProxy().getConfig().getParams().get(CONFIG_PARAM_NAME);
        RequireSecurityToken annotation = invocationMethod.getAnnotation(RequireSecurityToken.class);

        boolean isProtected = methodRequiresProtection(configParam, annotation);
        String token = ServletActionContext.getRequest().getParameter(REQUEST_PARAM_NAME);
        boolean validToken = tokenGenerator.validateToken(ServletActionContext.getRequest(), token);

        if (isProtected && !validToken)
        {
            if (token == null)
            {
                addInvalidTokenError(versionSupport.extractAction(invocation), SECURITY_TOKEN_REQUIRED_ERROR_KEY);
            }
            else
            {
                addInvalidTokenError(versionSupport.extractAction(invocation), VALIDATION_FAILED_ERROR_KEY);
            }
            ServletActionContext.getResponse().setStatus(403);
            return ActionSupport.INPUT;
        }

        return invocation.invoke();
    }

    private boolean methodRequiresProtection(String configParam, RequireSecurityToken annotation)
    {
        if (isOverrideHeaderPresent())
            return false;
        if (configParam != null)
            return Boolean.valueOf(configParam);
        else if (annotation != null)
            return annotation.value();
        else
            return getSecurityLevel().getDefaultProtection();
    }

    /**
     * Add error to action in cases where token is required, but is missing or invalid. Implementations may
     * wish to override this method, but most should be able to get away with just overriding
     * {@link #internationaliseErrorMessage}
     *
     * @param action the action to add the error message to
     * @param errorMessageKey the error message key that will be used to internationalise the message
     */
    protected void addInvalidTokenError(Action action, String errorMessageKey)
    {
        if (action instanceof ValidationAware)
            ((ValidationAware)action).addActionError(internationaliseErrorMessage(action, errorMessageKey));
    }

    /**
     * Convert an error message key into the correct message for the current user's locale. The default implementation
     * is only useful for testing. Implementations should override this method to provide the appropriate
     * internationalised implementation.
     *
     * @param action the current action being executed
     * @param messageKey the message key that needs internationalising
     * @return the appropriate internationalised message for the current user
     */
    protected String internationaliseErrorMessage(Action action, String messageKey)
    {
        return messageKey;
    }

    private boolean isOverrideHeaderPresent()
    {
        return OVERRIDE_HEADER_VALUE.equals(ServletActionContext.getRequest().getHeader(OVERRIDE_HEADER_NAME));
    }

    ///CLOVER:OFF

    public void destroy()
    {
    }

    public void init()
    {
    }

    /**
     * Gets the current security level. See {@link SecurityLevel} for more information on the meanings of the different
     * level. Default implementation returns <code>OPT_IN</code>. Implementations should override this method if they
     * want more control over the security level setting.
     *
     * @return the security level to apply to this interceptor.
     */
    protected SecurityLevel getSecurityLevel()
    {
        return SecurityLevel.OPT_IN;
    }
}
