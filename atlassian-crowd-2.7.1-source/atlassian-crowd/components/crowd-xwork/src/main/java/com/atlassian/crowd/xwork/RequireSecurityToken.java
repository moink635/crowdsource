package com.atlassian.crowd.xwork;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

/**
 * Method level annotation for XWork actions to mark whether a particular action method invocation needs to be
 * protected by an XSRF token.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RequireSecurityToken
{
    boolean value();
}
