package com.atlassian.crowd.xwork.interceptors;

import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.interceptor.Interceptor;
import com.opensymphony.xwork.interceptor.PreResultListener;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * Manages two separate database transactions around the action execution and result execution
 * in XWork, using the Spring PlatformTransactionManager.
 * <p/>
 * The first transaction begins as soon as this interceptor executes. It is committed in a {@link
 * PreResultListener} which runs just before the result executes. The second transaction is opened
 * in the same listener. The second transaction is committed when control returns to the interceptor
 * after the action and result executions are both complete.
 * <p/>
 * The active transaction will be rolled back if an exception is caught by this interceptor during
 * the execution of either the action or the result.
 * <p/>
 * Implementations should override {@link #getTransactionManager()} to provide the Spring transaction
 * manager, and {@link #shouldIntercept(ActionInvocation)} to configure when transactions should be
 * applied to actions.
 */
public abstract class XWorkTransactionInterceptor implements Interceptor
{
    /**
     * Template method for retrieving the transaction manager for the current application.
     *
     * @return the transaction manager for this application
     */
    public abstract PlatformTransactionManager getTransactionManager();

    /**
     * Determine if a certain action should be wrapped in a transaction. Applications should override this method
     * if they want to prevent specific actions from being intercepted, or want to prevent transactions from being
     * created before the system is fully set up.
     *
     * @param invocation the action being invoked
     * @return true if the action should be wrapped in a transaction
     */
    protected abstract boolean shouldIntercept(ActionInvocation invocation);

    public void destroy()
    {
    }

    public void init()
    {
    }

    public String intercept(final ActionInvocation invocation) throws Exception
    {
        if (shouldIntercept(invocation))
        {
            return new TransactionalInvocation(getTransactionManager()).invokeInTransaction(invocation);
        }
        else
        {
            return invocation.invoke();
        }
    }
}
