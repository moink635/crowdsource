package com.atlassian.crowd.xwork.interceptors;

import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.ActionProxy;
import com.opensymphony.xwork.interceptor.PreResultListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAttribute;

/**
 * Invoke an XWork ActionInvocation within a transaction. If the invocation throws an unchecked exception, roll
 * back the txn.
 */
class TransactionalInvocation
{
    private static final Logger log = LoggerFactory.getLogger(TransactionalInvocation.class);

    private final TransactionAttribute transactionAttribute = new DefaultTransactionAttribute(TransactionAttribute.PROPAGATION_REQUIRED);
    private final PlatformTransactionManager transactionManager;

    private TransactionStatus transactionStatus;

    public TransactionalInvocation(PlatformTransactionManager transactionManager)
    {
        this.transactionManager = transactionManager;
    }

    public String invokeInTransaction(final ActionInvocation invocation) throws Exception
    {
        log.debug("Creating transaction for action invocation: {}", getDetails(invocation));

        this.transactionStatus = transactionManager.getTransaction(transactionAttribute);

        // Add listener to commit transaction between action and result. If we do not do this, we run the risk
        // that if the result is a redirect, the user will follow the redirect before the transaction has been
        // committed, and see stale data.
        invocation.addPreResultListener(new PreResultListener()
        {
            public void beforeResult(ActionInvocation actionInvocation, String s)
            {
                commitOrRollbackTransaction(invocation, false);

                log.debug("Creating transaction for action result: {}", getDetails(invocation));

                TransactionalInvocation.this.transactionStatus = transactionManager.getTransaction(transactionAttribute);
            }
        });

        boolean swallowCommitErrors = true;
        try
        {
            String result = invokeAndHandleExceptions(invocation);
            swallowCommitErrors = false;
            return result;
        }
        finally
        {
            commitOrRollbackTransaction(invocation, swallowCommitErrors);
        }
    }

    private String invokeAndHandleExceptions(ActionInvocation invocation) throws Exception
    {
        try
        {
            return invocation.invoke();
        }
        catch (Exception ex)
        {
            handleInvocationException(invocation, transactionAttribute, transactionStatus, ex);
            throw ex;
        }
    }

    private void commitOrRollbackTransaction(ActionInvocation actionInvocation, boolean swallowCommitErrors)
    {
        try
        {
            // If you try to commit a transaction that is completed or marked for rollback,
            // you'll get an UnexpectedRollbackException
            if (transactionStatus.isCompleted())
            {
                log.error("Action {} is already completed and can not be committed again.", getDetails(actionInvocation));
            }
            else if (transactionStatus.isRollbackOnly())
            {
                log.debug("Transaction status for action {} set to rollback only. Invoking rollback()", getDetails(actionInvocation));

                transactionManager.rollback(transactionStatus);
            }
            else
            {
                log.debug("Committing transaction for action {}", getDetails(actionInvocation));

                transactionManager.commit(transactionStatus);
            }
        }
        catch (RuntimeException e)
        {
            if (swallowCommitErrors)
            {
                log.error("Commit/Rollback exception occurred but was swallowed", e);
            }
            else
            {
                throw e;
            }
        }
    }

    private void handleInvocationException(ActionInvocation invocation, TransactionAttribute txAtt, TransactionStatus status, Throwable ex)
    {
        if (status == null)
        {
            return;
        }

        if (txAtt.rollbackOn(ex))
        {
            log.info("Invoking rollback for transaction on action '{}' due to throwable: {}", getDetails(invocation), ex, ex);
            status.setRollbackOnly();
        }
        else
        {
            log.debug("Action {} threw exception {} but did not trigger a rollback.", getDetails(invocation), ex);
        }
    }

    private String getDetails(ActionInvocation invocation)
    {
        ActionProxy proxy = invocation.getProxy();
        String methodName = proxy.getConfig().getMethodName();

        if (methodName == null)
        {
            methodName = "execute";
        }

        String actionClazz = getJustClassName(proxy.getConfig().getClassName());

        return proxy.getNamespace() + "/" + proxy.getActionName() + ".action (" + actionClazz + "." + methodName + "())";
    }

    private static String getJustClassName(String name)
    {
        int lastDotPos = name.lastIndexOf('.');
        if (lastDotPos != -1)
        {
            return name.substring(lastDotPos + 1);
        }

        return name;
    }
}
