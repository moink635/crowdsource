package com.atlassian.crowd.xwork;

import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.Action;

import java.lang.reflect.Method;

/**
 * Utility interface for abstracting binary or API-incompatible aspects of different XWork versions.
 */
public class XWorkVersionSupport
{
    /**
     * Works around binary incompatibility of ActionInvocation#getAction between XWork 1.0 and 1.2.
     *
     * @param invocation the action invocation
     * @return the associated action
     */
    public Action extractAction(ActionInvocation invocation)
    {
        return (Action) invocation.getAction();
    }

    /**
     * Works around missing ActionConfig#getMethod in Xwork 1.2.
     *
     * @param invocation the action invocation
     * @return the associated action
     * @throws NoSuchMethodException if a method could not be found for the action
     */
    public Method extractMethod(ActionInvocation invocation) throws NoSuchMethodException
    {
        final Class<?> actionClass = invocation.getAction().getClass();
        final String methodName = invocation.getProxy().getMethod();

        try
        {
            return actionClass.getMethod(methodName);
        }
        catch (NoSuchMethodException e)
        {
            try
            {
                String altMethodName = "do" + methodName.substring(0, 1).toUpperCase() + methodName.substring(1);
                return actionClass.getMethod(altMethodName);
            }
            catch (NoSuchMethodException e1) {
                // throw the original one
                throw e;
            }
        }
    }
}
