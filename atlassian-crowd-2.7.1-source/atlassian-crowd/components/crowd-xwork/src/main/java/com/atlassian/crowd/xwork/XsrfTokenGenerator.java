package com.atlassian.crowd.xwork;

import javax.servlet.http.HttpServletRequest;

/**
 * Interface for generating anti-XSRF tokens for web forms. The default implementation
 * {@link com.atlassian.crowd.xwork.SimpleXsrfTokenGenerator} should be good enough for anyone, but
 * this interface is provided just in case anyone wants to implement their own token generation
 * strategy.
 */
public interface XsrfTokenGenerator
{
    /**
     * Retrieves the token from the request. Returns null if there is no request and create is false.
     * If create is true, a new token is generated and returned.
     * @param request the request the token is retrieved from
     * @param create if true, a token will be created if it doesn't already exist
     * @return a valid XSRF form token, null if there is none in the request and create of false.
     * @since 1.12
     */
    String getToken(HttpServletRequest request, boolean create);

    /**
     * Generate a new form token for the current request.
     *
     * @param request the request the token is being generated for
     * @return a valid XSRF form token
     */
    String generateToken(HttpServletRequest request);

    /**
     * Convenience method which will return the name to be used for a supplied XsrfToken in a request.
     *
     * @return the name in the request for the Xsrf token.
     */
    String getXsrfTokenName();

    /**
     * Validate a form token received as part of a web request
     *
     * @param request the request the token was received in
     * @param token the token
     * @return true iff the token is valid
     */
    boolean validateToken(HttpServletRequest request, String token);
}
