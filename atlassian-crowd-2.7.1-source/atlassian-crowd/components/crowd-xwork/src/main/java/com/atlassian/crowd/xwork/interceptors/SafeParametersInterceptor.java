package com.atlassian.crowd.xwork.interceptors;

import com.atlassian.crowd.xwork.ParameterSafe;
import com.atlassian.crowd.xwork.XWorkVersionSupport;
import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.ActionContext;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.interceptor.Interceptor;
import com.opensymphony.xwork.interceptor.NoParameters;
import com.opensymphony.xwork.util.InstantiatingNullHandler;
import com.opensymphony.xwork.util.OgnlValueStack;
import com.opensymphony.xwork.util.XWorkConverter;
import com.opensymphony.xwork.util.XWorkMethodAccessor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.beans.BeanInfo;
import java.beans.IntrospectionException;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Injects submitted form parameters into action properties. This implementation performs white-list based
 * sanity checks on incoming parameters before allowing OGNL to perform any potentially dangerous operations on
 * an action, closing off an entire category of parameter injection attacks.
 * <p/>
 * Parameters that set a value on an action directly will be allowed as will index-based setters for collections
 * of values. However:
 * <ol>
 * <li> To defend against possible OGNL vulnerabilities (especially Unicode attacks), parameter names will be
 * filtered so only ascii alphanumeric characters (plus the underscore, square brackets and apostrophes) are permitted
 * <li> If the dot-notation is used to access some property on an action (i.e. a parameter called "search.query")
 * the type returned from the getter (getSearch()) MUST have the @ParameterSafe annotation for the parameter
 * to be accepted, <i>or</i> the getter method must have the @ParameterSafe annotation
 * <li> If the map-notation is used to access some property on an action (i.e. a parameter called "map['key']")
 * the getter method must have the @ParameterSafe annotation
 * </ol>
 * <p/>
 * These last two checks (@ParameterSafe checks for dot- and map-notation) can be skipped by setting
 * disableAnnotationChecks. When disabled this interceptor still prevents Unicode-attacks (amoungst other things)
 * but allows dot/map traversal of any POJO retrievable from an action. To disable, use a param e.g.
 * <pre>
 *  &lt;interceptor name="params" class="com.atlassian.crowd.xwork.interceptors.SafeParametersInterceptor">
 *     &lt;param name="disableAnnotationChecks">true&lt;/param>
 *  &lt;/interceptor>
 * </pre>
 * <p/>
 * Portions of this class are copied from XWork under the Apache license, Copyright (c) 2002-2003 by OpenSymphony
 */
public class SafeParametersInterceptor implements Interceptor
{
    public static final Logger log = LoggerFactory.getLogger(SafeParametersInterceptor.class);

    private static final Pattern SAFE_PARAMETER_NAME_PATTERN = Pattern.compile("[a-zA-Z0-9\\.\\]\\[_']+");
    private static final Pattern MAP_PARAMETER_PATTERN = Pattern.compile(".*\\['[a-zA-Z0-9_]+'\\]");
    private final XWorkVersionSupport versionSupport = new XWorkVersionSupport();
    private boolean disableAnnotationChecks = false;

    @Override
    public void init()
    {
    }

    @Override
    public void destroy()
    {
    }

    @Override
    public String intercept(ActionInvocation invocation) throws Exception
    {
        before(invocation);
        return invocation.invoke();
    }

    public void setDisableAnnotationChecks(boolean disableAnnotationChecks)
    {
        this.disableAnnotationChecks = disableAnnotationChecks;
    }

    /**
     * The implementation of this method should evalutate if the passed in actionInvocation.getAction()
     * is of a type {@link com.opensymphony.xwork.interceptor.NoParameters} if it is, we should not bother
     * intercepting.
     * <p/>
     * The reason for this abstract class is so we are compatible with both 1.0.3 and 1.2.3 of XWork.
     *
     * @param actionInvocation the action invocation being intercepted
     * @return true if we are not of type {@link com.opensymphony.xwork.interceptor.NoParameters}
     */
    protected boolean shouldNotIntercept(ActionInvocation actionInvocation)
    {
        return versionSupport.extractAction(actionInvocation) instanceof NoParameters;
    }

    protected void before(ActionInvocation invocation) throws Exception
    {

        if (shouldNotIntercept(invocation))
        {
            return;
        }

        Action action = versionSupport.extractAction(invocation);

        //noinspection unchecked
        final Map<String, Object> parameters = filterSafeParameters(ActionContext.getContext().getParameters(), action);

        // Copied from the XWork parameters interceptor:

        if (log.isDebugEnabled())
        {
            log.debug("Setting params " + parameters);
        }

        ActionContext invocationContext = invocation.getInvocationContext();


        try
        {
            invocationContext.put(InstantiatingNullHandler.CREATE_NULL_OBJECTS, Boolean.TRUE);
            invocationContext.put(XWorkMethodAccessor.DENY_METHOD_EXECUTION, Boolean.TRUE);
            invocationContext.put(XWorkConverter.REPORT_CONVERSION_ERRORS, Boolean.TRUE);

            if (parameters != null)
            {
                final OgnlValueStack stack = ActionContext.getContext().getValueStack();

                for (Map.Entry<String, Object> entry : parameters.entrySet())
                {
                    String name = entry.getKey();

                    stack.setValue(name, entry.getValue());
                }
            }
        }
        finally
        {
            invocationContext.put(InstantiatingNullHandler.CREATE_NULL_OBJECTS, Boolean.FALSE);
            invocationContext.put(XWorkMethodAccessor.DENY_METHOD_EXECUTION, Boolean.FALSE);
            invocationContext.put(XWorkConverter.REPORT_CONVERSION_ERRORS, Boolean.FALSE);
        }
    }

    private Map<String, Object> filterSafeParameters(Map<String, ?> parameters, Action action)
    {
        Map<String, Object> safeParameters = new HashMap<String, Object>();

        for (Map.Entry<String, ?> entry : parameters.entrySet())
        {
            if (isSafeParameterName(entry.getKey(), action, disableAnnotationChecks))
            {
                safeParameters.put(entry.getKey(), entry.getValue());
            }
        }

        return safeParameters;
    }

    static boolean isSafeParameterName(String key, Action action)
    {
        return isSafeParameterName(key, action, true);
    }

    static boolean isSafeParameterName(String key, Action action, boolean disableAnnotationChecks)
    {
        if (!SAFE_PARAMETER_NAME_PATTERN.matcher(key).matches())
        {
            return false;
        }

        if (!disableAnnotationChecks && (key.contains(".") || MAP_PARAMETER_PATTERN.matcher(key).matches())) {
            return isSafeComplexParameterName(key, action);
        }

        return true;
    }

    private static boolean isSafeComplexParameterName(String key, Action action)
    {
        try
        {
            String initialParameterName = extractInitialParameterName(key);
            BeanInfo info = Introspector.getBeanInfo(action.getClass());
            PropertyDescriptor[] descs = info.getPropertyDescriptors();

            for (PropertyDescriptor desc : descs)
            {
                if (desc.getName().equals(initialParameterName))
                {
                    if (isSafeMethod(desc.getReadMethod()))
                    {
                        return true;
                    }
                    else
                    {
                        log.info("Attempt to call unsafe property setter " + key + " on " + action);
                        return false;
                    }
                }
            }
        }
        catch (IntrospectionException e)
        {
            log.warn("Error introspecting action parameter " + key + " for action " + action + ": " + e.getMessage(), e);
        }

        return false;
    }

    private static String extractInitialParameterName(String key)
    {
        if (!key.contains("[") || (key.indexOf(".") > 0 && key.indexOf("[") > key.indexOf(".")))
        {
            return key.substring(0, key.indexOf("."));
        }
        else
        {
            return key.substring(0, key.indexOf("["));
        }
    }

    private static boolean isSafeMethod(Method writeMethod)
    {
        return writeMethod.getAnnotation(ParameterSafe.class) != null ||
                writeMethod.getReturnType().getAnnotation(ParameterSafe.class) != null;
    }
}
