package com.atlassian.crowd.xwork.interceptors;

import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.ActionProxy;
import com.opensymphony.xwork.config.entities.ActionConfig;
import junit.framework.TestCase;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;

import java.util.Collections;

/**
 * Simple test for {@link TransactionalInvocation}.
 */
public class TestTransactionalInvocation extends MockObjectTestCase
{
    private Mock mockActionInvocation;
    private Mock mockTransactionManager;
    private Mock mockTransactionStatus;
    private TransactionalInvocation transactionalInvocation;

    public void setUp() throws Exception
    {
        mockActionInvocation = mock(ActionInvocation.class);
        mockTransactionManager = mock(PlatformTransactionManager.class);
        mockTransactionStatus = mock(TransactionStatus.class);
        mockTransactionManager.expects(once()).method("getTransaction").will(returnValue(mockTransactionStatus.proxy()));
        mockActionInvocation.expects(once()).method("addPreResultListener");
        stubOutGetDetails();

        transactionalInvocation = new TransactionalInvocation((PlatformTransactionManager) mockTransactionManager.proxy());
    }

    private void expectActionSuccess()
    {
        mockActionInvocation.expects(once()).method("invoke").will(returnValue("SUCCESS"));
    }

    /** This info will be used by any of the debugging calls to getDetails() */
    private void stubOutGetDetails()
    {
        Mock mockActionProxy = mock(ActionProxy.class);
        mockActionInvocation.stubs().method("getProxy").will(returnValue(mockActionProxy.proxy()));
        ActionConfig config = new ActionConfig("toString", Object.class, Collections.emptyMap(), Collections.emptyMap(), Collections.emptyList());
        mockActionProxy.stubs().method("getConfig").will(returnValue(config));
        mockActionProxy.stubs().method("getNamespace").will(returnValue(""));
        mockActionProxy.stubs().method("getActionName").will(returnValue(""));
    }

    public void tearDown() throws Exception
    {
        mockActionInvocation = null;
        mockTransactionManager = null;
        mockTransactionStatus = null;
        transactionalInvocation = null;
    }

    private void verifyMocks()
    {
        mockTransactionManager.verify();
        mockTransactionStatus.verify();
        mockActionInvocation.verify();
    }

    public void testNormalInvocation() throws Exception
    {
        expectActionSuccess();

        mockTransactionStatus.expects(once()).method("isCompleted").will(returnValue(false));
        mockTransactionStatus.expects(once()).method("isRollbackOnly").will(returnValue(false));

        mockTransactionManager.expects(once()).method("commit");

        transactionalInvocation.invokeInTransaction((ActionInvocation) mockActionInvocation.proxy());
        verifyMocks();
    }

    public void testAlreadyCompletedOrRolledBackInvocation() throws Exception
    {
        expectActionSuccess();

        mockTransactionStatus.expects(once()).method("isCompleted").will(returnValue(true));

        transactionalInvocation.invokeInTransaction((ActionInvocation) mockActionInvocation.proxy());
        verifyMocks();
    }

    public void testRequiresRollbackInvocation() throws Exception
    {
        expectActionSuccess();

        mockTransactionStatus.expects(once()).method("isCompleted").will(returnValue(false));
        mockTransactionStatus.expects(once()).method("isRollbackOnly").will(returnValue(true));

        mockTransactionManager.expects(once()).method("rollback");

        transactionalInvocation.invokeInTransaction((ActionInvocation) mockActionInvocation.proxy());

        verifyMocks();
    }

    public void testRollbackAfterExceptionThrownByInvocation() throws Exception
    {
        mockTransactionStatus.expects(once()).method("isCompleted").will(returnValue(false));
        mockTransactionStatus.expects(once()).method("setRollbackOnly");
        mockTransactionStatus.expects(once()).method("isRollbackOnly").will(returnValue(true));

        mockTransactionManager.expects(once()).method("rollback");

        RuntimeException runtimeException = new RuntimeException();
        mockActionInvocation.expects(once()).method("invoke").will(throwException(runtimeException));

        try
        {
            transactionalInvocation.invokeInTransaction((ActionInvocation) mockActionInvocation.proxy());
            fail();
        }
        catch (RuntimeException e)
        {
            assertSame(runtimeException, e);
        }

        verifyMocks();
    }

    public void testOriginalExceptionThrownIfRollbackFails() throws Exception
    {
        mockTransactionStatus.expects(once()).method("isCompleted").will(returnValue(false));
        mockTransactionStatus.expects(once()).method("setRollbackOnly");
        mockTransactionStatus.expects(once()).method("isRollbackOnly").will(returnValue(true));

        RuntimeException rollbackException = new RuntimeException("rollback failed");
        mockTransactionManager.expects(once()).method("rollback").will(throwException(rollbackException));

        RuntimeException actionException = new RuntimeException("action failed");
        mockActionInvocation.expects(once()).method("invoke").will(throwException(actionException));

        try
        {
            transactionalInvocation.invokeInTransaction((ActionInvocation) mockActionInvocation.proxy());
            fail();
        }
        catch (RuntimeException e)
        {
            assertSame(actionException, e);
        }

        verifyMocks();
    }
}
