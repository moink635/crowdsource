package com.atlassian.crowd.xwork.interceptors;

import com.opensymphony.xwork.ActionSupport;
import com.atlassian.crowd.xwork.ParameterSafe;
import junit.framework.TestCase;

import java.util.List;
import java.util.Collections;
import java.util.Map;

/**
 *
 */
public class TestSafeParametersInterceptor extends TestCase
{
    @ParameterSafe
    public static class SafeReturnType {}

    public static class UnsafeReturnType {}

    public static class MyAction extends ActionSupport
    {
        public void setFoo(String foo) {}

        public void setBar(String bar) {}

        public void setFoo_bar(String foo_bar) {}

        public List getBaz() { return Collections.EMPTY_LIST; }

        @ParameterSafe
        public List getQuux() { return Collections.EMPTY_LIST; }

        public SafeReturnType getSafeReturnType() { return new SafeReturnType(); }

        public UnsafeReturnType getUnsafeReturnType() { return new UnsafeReturnType(); }

        public String getFoo() { return "foo"; }

        @ParameterSafe
        public String getBar() { return "bar"; }

        @ParameterSafe
        public Map getBarMap() { return Collections.emptyMap(); }

        @ParameterSafe
        public Map getBar_Map() { return Collections.emptyMap(); }

        public Map getFooMap() { return Collections.emptyMap(); }

        public Map getFoo_Map() { return Collections.emptyMap(); }
    }

    public void testSimpleSetters()
    {
        assertTrue(isSafe("foo"));
        assertTrue(isSafe("bar"));
        assertTrue(isSafe("foo_bar"));
    }

    public void testIndexedSetters()
    {
        assertTrue(isSafe("baz[0]"));
        assertTrue(isSafe("baz[1]"));
        assertTrue(isSafe("baz[2]"));
    }

    public void testTraverseIndexedSetters()
    {
        assertFalse(isSafe("baz[1].foo"));
        assertTrue(isSafe("baz[1].foo", true));
        assertTrue(isSafe("quux[1].foo"));
    }

    public void testMultipleTraversing()
    {
        assertFalse(isSafe("foo.bar.baz"));
        assertTrue(isSafe("foo.bar.baz", true));
        assertFalse(isSafe("unsafeReturnType.foo"));
        assertTrue(isSafe("unsafeReturnType.foo", true));
        assertTrue(isSafe("bar.bar.baz"));
        assertTrue(isSafe("safeReturnType.foo"));
    }

    public void testMixedTraversingAndIndexing()
    {
        assertFalse(isSafe("foo.bar[1]"));
        assertTrue(isSafe("foo.bar[1]", true));
        assertTrue(isSafe("bar.foo[1]"));
    }

    public void testUnsafeCharacters()
    {
        assertFalse(isSafe("foo#bar"));
        assertFalse(isSafe("foo#bar", true));
        // nasty unicode
        assertFalse(isSafe("foo\u0023bar"));
        assertFalse(isSafe("foo\u0023bar", true));
        // escaped nasty unicode
        assertFalse(isSafe("foo\\u0023bar"));
        assertFalse(isSafe("foo\\u0023bar", true));
    }

    public void testDotNotation()
    {
        assertTrue(isSafe("bar.property"));
        assertTrue(isSafe("safeReturnType.property"));
        assertFalse(isSafe("foo.property"));
        assertTrue(isSafe("foo.property", true));
        assertFalse(isSafe("unsafeReturnType.property"));
        assertTrue(isSafe("unsafeReturnType.property", true));
    }

    public void testMapNotation()
    {
        assertTrue(isSafe("barMap['key']"));
        assertTrue(isSafe("bar_Map['key']"));
        assertTrue(isSafe("bar_Map['key_key']"));
        assertFalse(isSafe("fooMap['key']"));
        assertTrue(isSafe("fooMap['key']", true));
        assertFalse(isSafe("foo_Map['key']"));
        assertTrue(isSafe("foo_Map['key']", true));
        assertFalse(isSafe("foo_Map['key_key']"));
        assertTrue(isSafe("foo_Map['key_key']", true));
    }

    public void testMapNotationWithBadCharacters()
    {
        assertFalse(isSafe("barMap['key#G']"));
        assertFalse(isSafe("barMap['key#G']", true));
        assertFalse(isSafe("barMap['key!G']"));
        assertFalse(isSafe("barMap['key!G']", true));
        assertFalse(isSafe("barMap['key\u0023G']"));
        assertFalse(isSafe("barMap['key\u0023G']", true));
    }

    private boolean isSafe(String parameter)
    {
        return isSafe(parameter, false);
    }

    private boolean isSafe(String parameter, boolean disableAnnotationChecks)
    {
        return SafeParametersInterceptor.isSafeParameterName(parameter, new MyAction(), disableAnnotationChecks);
    }
}
