package com.atlassian.crowd.xwork;

import javax.servlet.http.HttpServletRequest;

import com.google.common.collect.ImmutableMap;
import com.opensymphony.webwork.dispatcher.mapper.ActionMapper;
import com.opensymphony.webwork.dispatcher.mapper.ActionMapping;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CrowdActionMapperTest
{
    @Test
    public void redirectParametersAreIgnored()
    {
        ActionMapper mapper = new CrowdActionMapper();

        HttpServletRequest req = mock(HttpServletRequest.class);

        when(req.getAttribute("javax.servlet.include.servlet_path")).thenReturn("/test.action");
        when(req.getParameterMap()).thenReturn(ImmutableMap.of("redirect:url", ""));

        ActionMapping mapping = mapper.getMapping(req);

        assertEquals("test", mapping.getName());
        assertNull(mapping.getMethod());
        assertEquals("/", mapping.getNamespace());

        assertNull(mapping.getResult());
    }
}
