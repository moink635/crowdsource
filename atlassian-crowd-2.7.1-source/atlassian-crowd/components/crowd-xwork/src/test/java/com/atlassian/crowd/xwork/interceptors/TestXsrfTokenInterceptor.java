package com.atlassian.crowd.xwork.interceptors;

import com.atlassian.crowd.xwork.RequireSecurityToken;
import com.atlassian.crowd.xwork.XsrfTokenGenerator;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.WebWorkStatics;
import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.ActionProxy;
import com.opensymphony.xwork.ActionProxyFactory;
import com.opensymphony.xwork.ActionSupport;
import com.opensymphony.xwork.ValidationAware;
import com.opensymphony.xwork.config.Configuration;
import com.opensymphony.xwork.config.ConfigurationException;
import com.opensymphony.xwork.config.ConfigurationManager;
import com.opensymphony.xwork.config.ConfigurationProvider;
import com.opensymphony.xwork.config.entities.ActionConfig;
import com.opensymphony.xwork.config.entities.InterceptorMapping;
import com.opensymphony.xwork.config.entities.PackageConfig;
import com.opensymphony.xwork.mock.MockActionInvocation;
import com.opensymphony.xwork.mock.MockActionProxy;

import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class TestXsrfTokenInterceptor extends MockObjectTestCase
{
    private static final String VALID_TOKEN = "abcdef";

    private XsrfTokenInterceptor.SecurityLevel securityLevel;
    private Mock mockServletRequest;
    private Mock mockServletResponce;

    public static class DummyAction implements Action
    {
        @RequireSecurityToken(true)
        public String withProtection() throws Exception
        {
            return SUCCESS;
        }

        @RequireSecurityToken(false)
        public String noProtection() throws Exception
        {
            return SUCCESS;

        }

        public String execute() throws Exception
        {
            return SUCCESS;
        }
    }

    public static class ValidateableDummyAction extends ActionSupport
    {
        @RequireSecurityToken(true)
        public String execute() throws Exception
        {
            return SUCCESS;
        }
    }

    private static class ConstantTokenGenerator implements XsrfTokenGenerator
    {
        public String getToken(HttpServletRequest request, boolean create)
        {
            return VALID_TOKEN;
        }

        public String generateToken(HttpServletRequest request)
        {
            return VALID_TOKEN;
        }

        public boolean validateToken(HttpServletRequest request, String token)
        {
            return VALID_TOKEN.equals(token);
        }

        public String getXsrfTokenName()
        {
            return XsrfTokenInterceptor.REQUEST_PARAM_NAME;
        }
    }

    private class ConfigurableInterceptor extends XsrfTokenInterceptor
    {
        public ConfigurableInterceptor()
        {
            super(new ConstantTokenGenerator());
        }

        protected SecurityLevel getSecurityLevel()
        {
            return securityLevel;
        }
    }
    private class CustomConfigurationProvider implements ConfigurationProvider
    {
        private List<InterceptorMapping> interceptors;

        public void destroy()
        {
        }

        public void init(Configuration configuration) throws ConfigurationException
        {
            PackageConfig packageConfig = new PackageConfig();
            interceptors = new ArrayList<InterceptorMapping>();
            interceptors.add(new InterceptorMapping("", new ConfigurableInterceptor()));

            // Note: XWork mapps a null method name to execute(), but config#getMethodName() still returns null so we
            // still test it separately just in case.

            packageConfig.addActionConfig("annotated-protected", makeActionConfig(DummyAction.class, "withProtection"));
            packageConfig.addActionConfig("annotated-notprotected", makeActionConfig(DummyAction.class, "noProtection"));
            packageConfig.addActionConfig("unannotated", makeActionConfig(DummyAction.class, "execute"));

            packageConfig.addActionConfig("configured-protected", makeActionConfig(DummyAction.class, "execute", "true"));
            packageConfig.addActionConfig("configured-notprotected", makeActionConfig(DummyAction.class, "execute", "false"));

            packageConfig.addActionConfig("override-annotation-notprotected", makeActionConfig(DummyAction.class, "withProtection", "false"));
            packageConfig.addActionConfig("override-annotation-protected", makeActionConfig(DummyAction.class, "noProtection", "true"));

            packageConfig.addActionConfig("validation-aware", makeActionConfig(ValidateableDummyAction.class, "execute"));

            configuration.addPackageConfig("defaultPackage", packageConfig);
        }

        private ActionConfig makeActionConfig(Class<? extends Action> actionClass, String methodName, String permittedMethodsParameter)
        {
            return makeActionConfig(actionClass, methodName, Collections.singletonMap(XsrfTokenInterceptor.CONFIG_PARAM_NAME, permittedMethodsParameter));
        }

        private ActionConfig makeActionConfig(Class<? extends Action> actionClass, String methodName, Map<?,?> parameterMap)
        {
            return new ActionConfig(methodName, actionClass, parameterMap, new HashMap<String,Object>(), interceptors);
        }

        private ActionConfig makeActionConfig(Class<? extends Action> actionClass, String methodName)
        {
            return makeActionConfig(actionClass, methodName, new HashMap<String,Object>());
        }

        public boolean needsReload()
        {
            return false;
        }
    }
    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        ConfigurationManager.addConfigurationProvider(new CustomConfigurationProvider());
        ConfigurationManager.getConfiguration().reload();

        mockServletRequest = new Mock(HttpServletRequest.class);
        mockServletResponce = new Mock(HttpServletResponse.class);
        mockServletResponce.stubs().method("setStatus").with(eq(403));
        ServletActionContext.setResponse((HttpServletResponse) mockServletResponce.proxy());
        mockServletRequest.stubs().method("getHeader").with(eq(XsrfTokenInterceptor.OVERRIDE_HEADER_NAME)).will(returnValue(null));
        ServletActionContext.setRequest((HttpServletRequest) mockServletRequest.proxy());
    }

    @Override
    protected void tearDown() throws Exception
    {
        ServletActionContext.setRequest(null);
        ConfigurationManager.destroyConfiguration();
        ConfigurationManager.clearConfigurationProviders();

        mockServletRequest = null;
        mockServletResponce = null;
        super.tearDown();
    }

    public void testWithValidation() throws Exception
    {
        securityLevel = XsrfTokenInterceptor.SecurityLevel.OPT_IN;
        testInterceptor("validation-aware", "cheese", Action.INPUT, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
    }

    public void testNoTokenOptOut() throws Exception
    {
        securityLevel = XsrfTokenInterceptor.SecurityLevel.OPT_OUT;
        testOnlyUnprotectedActionsSucceedWithToken(null);
        testInterceptor("unannotated", null, ActionSupport.INPUT, XsrfTokenInterceptor.SECURITY_TOKEN_REQUIRED_ERROR_KEY);
    }

    public void testNoTokenOptIn() throws Exception
    {
        securityLevel = XsrfTokenInterceptor.SecurityLevel.OPT_IN;
        testOnlyUnprotectedActionsSucceedWithToken(null);
        testInterceptor("unannotated", null, Action.SUCCESS, XsrfTokenInterceptor.SECURITY_TOKEN_REQUIRED_ERROR_KEY);
    }

    public void testCorrectTokenOptIn() throws Exception
    {
        securityLevel = XsrfTokenInterceptor.SecurityLevel.OPT_IN;
        testAllActionsSucceedWithToken(VALID_TOKEN);
        testInterceptor("unannotated", VALID_TOKEN, Action.SUCCESS, null);
    }

    public void testCorrectTokenOptOut() throws Exception
    {
        securityLevel = XsrfTokenInterceptor.SecurityLevel.OPT_OUT;
        testAllActionsSucceedWithToken(VALID_TOKEN);
        testInterceptor("unannotated", VALID_TOKEN, Action.SUCCESS, null);
    }

    public void testIncorrectTokenOptIn() throws Exception
    {
        securityLevel = XsrfTokenInterceptor.SecurityLevel.OPT_IN;
        testOnlyUnprotectedActionsSucceedWithToken("cheese");
        testInterceptor("unannotated", VALID_TOKEN, Action.SUCCESS, null);
    }

    public void testIncorrectTokenOptOut() throws Exception
    {
        securityLevel = XsrfTokenInterceptor.SecurityLevel.OPT_OUT;
        testOnlyUnprotectedActionsSucceedWithToken("cheese");
        testInterceptor("unannotated", "cheese", Action.INPUT, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
    }

    public void testCorrectTokenWithOverrideHeader() throws Exception
    {
        mockServletRequest.stubs().method("getHeader").with(eq(XsrfTokenInterceptor.OVERRIDE_HEADER_NAME)).will(returnValue(XsrfTokenInterceptor.OVERRIDE_HEADER_VALUE));
        // Should succeed even though token is bad.
        testAllActionsSucceedWithToken("badtoken");
    }

    private void testAllActionsSucceedWithToken(String token) throws Exception
    {
        testInterceptor("annotated-protected", token, Action.SUCCESS, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
        testInterceptor("annotated-notprotected", token, Action.SUCCESS, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
        testInterceptor("configured-protected", token, Action.SUCCESS, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
        testInterceptor("configured-notprotected", token, Action.SUCCESS, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
        testInterceptor("override-annotation-notprotected", token, Action.SUCCESS, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
        testInterceptor("override-annotation-protected", token, Action.SUCCESS, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
    }

    private void testOnlyUnprotectedActionsSucceedWithToken(String token) throws Exception
    {
        testInterceptor("annotated-protected", token, Action.INPUT, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
        testInterceptor("annotated-notprotected", token, Action.SUCCESS, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
        testInterceptor("configured-protected", token, Action.INPUT, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
        testInterceptor("configured-notprotected", token, Action.SUCCESS, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
        testInterceptor("override-annotation-notprotected", token, Action.SUCCESS, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
        testInterceptor("override-annotation-protected", token, Action.INPUT, XsrfTokenInterceptor.VALIDATION_FAILED_ERROR_KEY);
    }

    private void testInterceptor(String actionAlias, String token, String expectedResult, String messageErrorKey) throws Exception
    {
        ActionProxy proxy = ActionProxyFactory.getFactory().createActionProxy("", actionAlias, makeContext(), false, false);
        mockServletRequest.stubs().method("getParameter").with(eq(XsrfTokenInterceptor.REQUEST_PARAM_NAME)).will(returnValue(token));
        String result = proxy.execute();
        assertEquals("Testing " + actionAlias + " with token " + token, expectedResult, result);

        if (Action.INPUT.equals(result) && proxy.getAction() instanceof ValidationAware)
        {
            ValidationAware validateable = (ValidationAware) proxy.getAction();
            assertTrue(validateable.hasActionErrors());
            assertEquals(messageErrorKey, validateable.getActionErrors().toArray()[0]);
        }
    }

    private HashMap<String,Object> makeContext()
    {
        HashMap<String, Object> context = new HashMap<String, Object>();
        context.put(WebWorkStatics.HTTP_REQUEST, mockServletRequest.proxy());
        context.put(WebWorkStatics.HTTP_RESPONSE, mockServletResponce.proxy());
        return context;
    }

    public void testInterceptInvokesNonProtectedAction() throws Exception
    {
        Mock mockServletRequest;
        XsrfTokenInterceptor interceptor;

        mockServletRequest = mock(HttpServletRequest.class);
        mockServletRequest.stubs().method("getHeader").with(eq(XsrfTokenInterceptor.OVERRIDE_HEADER_NAME)).will(returnValue(null));
        mockServletRequest.stubs().method("getParameter").with(eq(XsrfTokenInterceptor.REQUEST_PARAM_NAME)).will(returnValue(null));
        ServletActionContext.setRequest((HttpServletRequest) mockServletRequest.proxy());

        interceptor = new XsrfTokenInterceptor();

        final MockActionProxy actionProxy = new MockActionProxy();
        actionProxy.setMethod("toString");
        actionProxy.setConfig(new ActionConfig());

        final MockActionInvocation actionInvocation = new MockActionInvocation();
        actionInvocation.setProxy(actionProxy);
        actionInvocation.setAction(new Object());
        actionInvocation.setResultCode("passed");

        assertEquals("passed", interceptor.intercept(actionInvocation));
    }
}
