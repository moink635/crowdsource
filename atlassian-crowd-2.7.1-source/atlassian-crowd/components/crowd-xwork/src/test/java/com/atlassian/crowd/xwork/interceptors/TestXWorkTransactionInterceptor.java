package com.atlassian.crowd.xwork.interceptors;

import com.opensymphony.xwork.*;
import com.opensymphony.xwork.config.Configuration;
import com.opensymphony.xwork.config.ConfigurationManager;
import com.opensymphony.xwork.config.entities.ActionConfig;
import com.opensymphony.xwork.config.entities.PackageConfig;
import com.opensymphony.xwork.config.entities.ResultConfig;
import com.opensymphony.xwork.config.impl.DefaultConfiguration;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.interceptor.DefaultTransactionAttribute;
import org.springframework.transaction.interceptor.TransactionAttribute;
import org.springframework.transaction.support.SimpleTransactionStatus;

import java.util.Collections;

public class TestXWorkTransactionInterceptor extends MockObjectTestCase
{
    private Mock transactionManager;
    private XWorkTransactionInterceptor interceptor;

    protected void setUp() throws Exception
    {
        super.setUp();

        transactionManager = mock(PlatformTransactionManager.class);
        interceptor = new XWorkTransactionInterceptor() {
            public PlatformTransactionManager getTransactionManager()
            {
                return (PlatformTransactionManager) transactionManager.proxy();
            }

            protected boolean shouldIntercept(ActionInvocation invocation)
            {
                return true;
            }
        };
    }

    public void testSuccessCommitsTwice() throws Exception
    {
        ConfigurationManager.setConfiguration(buildXWorkConfiguration("test", ActionSupport.class, "success", DoNothingResult.class));
        ActionInvocation actionInvocation = buildActionInvocation("test");

        DefaultTransactionAttribute expectedAttributes = new DefaultTransactionAttribute(TransactionAttribute.PROPAGATION_REQUIRED);
        TransactionStatus transaction = new SimpleTransactionStatus();
        transactionManager.expects(exactly(2)).method("getTransaction").with(eq(expectedAttributes)).will(returnValue(transaction));
        transactionManager.expects(exactly(2)).method("commit").with(same(transaction));

        interceptor.intercept(actionInvocation);
    }

    public void testRuntimeExceptionInActionRollsBack() throws Exception
    {
        ConfigurationManager.setConfiguration(buildXWorkConfiguration("test", FailingAction.class, "success", DoNothingResult.class));
        ActionInvocation actionInvocation = buildActionInvocation("test");

        DefaultTransactionAttribute expectedAttributes = new DefaultTransactionAttribute(TransactionAttribute.PROPAGATION_REQUIRED);
        TransactionStatus transaction = new SimpleTransactionStatus();
        transactionManager.expects(exactly(1)).method("getTransaction").with(eq(expectedAttributes)).will(returnValue(transaction));
        transactionManager.expects(exactly(1)).method("rollback").with(same(transaction));

        try
        {
            interceptor.intercept(actionInvocation);
            fail("Expected RuntimeException not thrown");
        }
        catch (RuntimeException expected) {}
    }

    public void testRollBackInActionDoesNotRollBackAgain() throws Exception
    {
        ConfigurationManager.setConfiguration(buildXWorkConfiguration("test", RollBackAction.class, "success", DoNothingResult.class));
        ActionInvocation actionInvocation = buildActionInvocation("test");

        DefaultTransactionAttribute expectedAttributes = new DefaultTransactionAttribute(TransactionAttribute.PROPAGATION_REQUIRED);
        TransactionStatus controllerTransaction = new SimpleTransactionStatus();

        RollBackAction.transactionStatus = controllerTransaction;
        transactionManager.expects(once()).method("getTransaction").with(eq(expectedAttributes)).will(returnValue(controllerTransaction));
        transactionManager.expects(once()).method("rollback").with(same(controllerTransaction));

        TransactionStatus viewTransaction = new SimpleTransactionStatus();
        transactionManager.expects(once()).method("getTransaction").with(eq(expectedAttributes)).will(returnValue(viewTransaction));
        transactionManager.expects(exactly(1)).method("commit").with(same(viewTransaction));

        interceptor.intercept(actionInvocation);
    }

    /**
     * Creates an action invocation for an action with the given name. The action is passed no parameters.
     */
    private ActionInvocation buildActionInvocation(String actionName) throws Exception
    {
        ActionProxyFactory factory = DefaultActionProxyFactory.getFactory();
        ActionProxy actionProxy = factory.createActionProxy("", actionName, Collections.EMPTY_MAP);
        return factory.createActionInvocation(actionProxy);
    }

    /**
     * Creates an XWork configuration with a single action and a single result for that action.
     */
    private Configuration buildXWorkConfiguration(String actionName, Class actionClass, String result, Class resultClass)
    {
        // static configuration -- not nice :-(
        ActionConfig actionConfig = new ActionConfig(null, actionClass, Collections.EMPTY_MAP,
            Collections.singletonMap(result, new ResultConfig("result", resultClass.getName())), Collections.EMPTY_LIST);
        PackageConfig packageConfig = new PackageConfig();
        packageConfig.addActionConfig(actionName, actionConfig);
        DefaultConfiguration configuration = new DefaultConfiguration();
        configuration.addPackageConfig("default", packageConfig);
        configuration.rebuildRuntimeConfiguration(); // nothing works without this line
        return configuration;
    }

    public static class FailingAction implements Action
    {
        public String execute() throws Exception
        {
            throw new RuntimeException("Expected exception thrown by FailingAction");
        }
    }

    public static class RollBackAction implements Action
    {
        public static TransactionStatus transactionStatus;

        public String execute() throws Exception
        {
            transactionStatus.setRollbackOnly();
            return SUCCESS;
        }
    }

    public static class DoNothingResult implements Result
    {
        public void execute(ActionInvocation invocation) throws Exception
        {
        }
    }
}
