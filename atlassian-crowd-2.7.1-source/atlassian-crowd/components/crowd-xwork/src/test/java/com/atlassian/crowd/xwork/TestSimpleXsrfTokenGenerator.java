package com.atlassian.crowd.xwork;

import org.jmock.MockObjectTestCase;
import org.jmock.Mock;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import javax.servlet.ServletContext;
import java.util.Map;
import java.util.HashMap;
import java.util.Enumeration;

/**
 *
 */
public class TestSimpleXsrfTokenGenerator extends MockObjectTestCase
{
    private Mock mockHttpServletRequest;
    private Map<String, Object> sessionContents;
    private HttpSession httpSession = new HttpSession() {
        public long getCreationTime()
        {
            return 0;
        }

        public String getId()
        {
            return null;
        }

        public long getLastAccessedTime()
        {
            return 0;
        }

        public ServletContext getServletContext()
        {
            return null;
        }

        public void setMaxInactiveInterval(int interval)
        {
        }

        public int getMaxInactiveInterval()
        {
            return 0;
        }

        public HttpSessionContext getSessionContext()
        {
            return null;
        }

        public Object getAttribute(String name)
        {
            return sessionContents.get(name);
        }

        public Object getValue(String name)
        {
            return null;
        }

        public Enumeration getAttributeNames()
        {
            return null;
        }

        public String[] getValueNames()
        {
            return new String[0];
        }

        public void setAttribute(String name, Object value)
        {
            sessionContents.put(name, value);
        }

        public void putValue(String name, Object value)
        {
        }

        public void removeAttribute(String name)
        {
        }

        public void removeValue(String name)
        {
        }

        public void invalidate()
        {
        }

        public boolean isNew()
        {
            return false;
        }
    };

    private HttpServletRequest request;
    private XsrfTokenGenerator generator;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        generator = new SimpleXsrfTokenGenerator();
        sessionContents = new HashMap<String, Object>();
        mockHttpServletRequest = new Mock(HttpServletRequest.class);
        mockHttpServletRequest.stubs().method("getSession").withAnyArguments().will(returnValue(httpSession));
        request = (HttpServletRequest) mockHttpServletRequest.proxy();
    }

    @Override
    protected void tearDown() throws Exception
    {
        request = null;
        mockHttpServletRequest = null;
        sessionContents = null;
        generator = null;
        super.tearDown();
    }

    public void testTokenAlreadyInSession()
    {
        sessionContents.put(SimpleXsrfTokenGenerator.TOKEN_SESSION_KEY, "cheese");
        assertEquals("cheese", generator.generateToken(request));
        assertTrue(generator.validateToken(request, "cheese"));
    }

    public void testNoTokenCreated()
    {
        String token = generator.getToken(request, false);
        assertNull(token);
        assertNull(sessionContents.get(SimpleXsrfTokenGenerator.TOKEN_SESSION_KEY));
    }

    public void testInvalidToken()
    {
        sessionContents.put(SimpleXsrfTokenGenerator.TOKEN_SESSION_KEY, "cheese");
        assertFalse(generator.validateToken(request, "milk"));
    }

    public void testGenerateNewToken()
    {
        String token = generator.generateToken(request);
        assertEquals(token, sessionContents.get(SimpleXsrfTokenGenerator.TOKEN_SESSION_KEY));
    }
}
