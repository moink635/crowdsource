package com.atlassian.crowd.xwork.interceptors;

import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.TextProvider;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CrowdXsrfTokenInterceptorTest
{
    @Test
    public void keyIsUsedWhenNoTextProviderAvailable()
    {
        assertEquals("message.key",
                new CrowdXsrfTokenInterceptor().internationaliseErrorMessage(null, "message.key"));
    }

    @Test
    public void keyIsResolvedWhenTextProviderIsAvailable()
    {
        Action action = mock(Action.class, Mockito.withSettings().extraInterfaces(TextProvider.class));

        when(((TextProvider) action).getText("message.key")).thenReturn("Message Text");

        assertEquals("Message Text",
                new CrowdXsrfTokenInterceptor().internationaliseErrorMessage(action, "message.key"));
    }
}
