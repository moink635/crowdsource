-- This file was generated with Crowd 2.1 running embedded HSQLDB, and restoring the data from remotecrowddirectory.xml
-- Some massaging was necessary to reorder the SQL statements
-- Some handmade SQL INSERTs were appended to make the dataset more interesting for testing

DROP SCHEMA PUBLIC CASCADE
-- CREATE SCHEMA PUBLIC AUTHORIZATION DBA

CREATE MEMORY TABLE CWD_APP_DIR_GROUP_MAPPING(ID BIGINT NOT NULL PRIMARY KEY,APP_DIR_MAPPING_ID BIGINT NOT NULL,APPLICATION_ID BIGINT NOT NULL,DIRECTORY_ID BIGINT NOT NULL,GROUP_NAME VARCHAR(255) NOT NULL,CONSTRAINT SYS_CT_145 UNIQUE(APP_DIR_MAPPING_ID,GROUP_NAME))
CREATE INDEX IDX_APP_DIR_GROUP_GROUP_DIR ON CWD_APP_DIR_GROUP_MAPPING(DIRECTORY_ID,GROUP_NAME)
CREATE MEMORY TABLE CWD_APP_DIR_MAPPING(ID BIGINT NOT NULL PRIMARY KEY,APPLICATION_ID BIGINT NOT NULL,DIRECTORY_ID BIGINT NOT NULL,ALLOW_ALL CHAR(1) NOT NULL,LIST_INDEX INTEGER,CONSTRAINT SYS_CT_149 UNIQUE(APPLICATION_ID,DIRECTORY_ID))
CREATE MEMORY TABLE CWD_APP_DIR_OPERATION(APP_DIR_MAPPING_ID BIGINT NOT NULL,OPERATION_TYPE VARCHAR(32) NOT NULL,PRIMARY KEY(APP_DIR_MAPPING_ID,OPERATION_TYPE),CONSTRAINT FK_APP_DIR_MAPPING FOREIGN KEY(APP_DIR_MAPPING_ID) REFERENCES CWD_APP_DIR_MAPPING(ID))
CREATE MEMORY TABLE CWD_APPLICATION(ID BIGINT NOT NULL PRIMARY KEY,APPLICATION_NAME VARCHAR(255) NOT NULL,LOWER_APPLICATION_NAME VARCHAR(255) NOT NULL,CREATED_DATE TIMESTAMP NOT NULL,UPDATED_DATE TIMESTAMP NOT NULL,ACTIVE CHAR(1) NOT NULL,DESCRIPTION VARCHAR(255),APPLICATION_TYPE VARCHAR(32) NOT NULL,CREDENTIAL VARCHAR(255) NOT NULL,CONSTRAINT SYS_CT_155 UNIQUE(LOWER_APPLICATION_NAME))
CREATE INDEX IDX_APP_ACTIVE ON CWD_APPLICATION(ACTIVE)
CREATE INDEX IDX_APP_TYPE ON CWD_APPLICATION(APPLICATION_TYPE)
CREATE MEMORY TABLE CWD_APPLICATION_ADDRESS(APPLICATION_ID BIGINT NOT NULL,REMOTE_ADDRESS VARCHAR(255) NOT NULL,REMOTE_ADDRESS_BINARY VARCHAR(255),REMOTE_ADDRESS_MASK INTEGER DEFAULT 0 NOT NULL,PRIMARY KEY(APPLICATION_ID,REMOTE_ADDRESS,REMOTE_ADDRESS_MASK),CONSTRAINT FK_APPLICATION_ADDRESS FOREIGN KEY(APPLICATION_ID) REFERENCES CWD_APPLICATION(ID))
CREATE MEMORY TABLE CWD_APPLICATION_ALIAS(ID BIGINT NOT NULL PRIMARY KEY,APPLICATION_ID BIGINT NOT NULL,USER_NAME VARCHAR(255) NOT NULL,LOWER_USER_NAME VARCHAR(255) NOT NULL,ALIAS_NAME VARCHAR(255) NOT NULL,LOWER_ALIAS_NAME VARCHAR(255) NOT NULL,CONSTRAINT SYS_CT_161 UNIQUE(APPLICATION_ID,LOWER_USER_NAME),CONSTRAINT SYS_CT_162 UNIQUE(APPLICATION_ID,LOWER_ALIAS_NAME),CONSTRAINT FK_ALIAS_APP_ID FOREIGN KEY(APPLICATION_ID) REFERENCES CWD_APPLICATION(ID))
CREATE MEMORY TABLE CWD_APPLICATION_ATTRIBUTE(APPLICATION_ID BIGINT NOT NULL,ATTRIBUTE_VALUE VARCHAR(4000),ATTRIBUTE_NAME VARCHAR(255) NOT NULL,PRIMARY KEY(APPLICATION_ID,ATTRIBUTE_NAME),CONSTRAINT FK_APPLICATION_ATTRIBUTE FOREIGN KEY(APPLICATION_ID) REFERENCES CWD_APPLICATION(ID))
CREATE MEMORY TABLE CWD_DIRECTORY(ID BIGINT NOT NULL PRIMARY KEY,DIRECTORY_NAME VARCHAR(255) NOT NULL,LOWER_DIRECTORY_NAME VARCHAR(255) NOT NULL,CREATED_DATE TIMESTAMP NOT NULL,UPDATED_DATE TIMESTAMP NOT NULL,ACTIVE CHAR(1) NOT NULL,DESCRIPTION VARCHAR(255),IMPL_CLASS VARCHAR(255) NOT NULL,LOWER_IMPL_CLASS VARCHAR(255) NOT NULL,DIRECTORY_TYPE VARCHAR(32) NOT NULL,CONSTRAINT SYS_CT_169 UNIQUE(LOWER_DIRECTORY_NAME))
CREATE INDEX IDX_DIR_TYPE ON CWD_DIRECTORY(DIRECTORY_TYPE)
CREATE INDEX IDX_DIR_ACTIVE ON CWD_DIRECTORY(ACTIVE)
CREATE INDEX IDX_DIR_L_IMPL_CLASS ON CWD_DIRECTORY(LOWER_IMPL_CLASS)
CREATE MEMORY TABLE CWD_DIRECTORY_ATTRIBUTE(DIRECTORY_ID BIGINT NOT NULL,ATTRIBUTE_VALUE VARCHAR(4000),ATTRIBUTE_NAME VARCHAR(255) NOT NULL,PRIMARY KEY(DIRECTORY_ID,ATTRIBUTE_NAME),CONSTRAINT FK_DIRECTORY_ATTRIBUTE FOREIGN KEY(DIRECTORY_ID) REFERENCES CWD_DIRECTORY(ID))
CREATE MEMORY TABLE CWD_DIRECTORY_OPERATION(DIRECTORY_ID BIGINT NOT NULL,OPERATION_TYPE VARCHAR(32) NOT NULL,PRIMARY KEY(DIRECTORY_ID,OPERATION_TYPE),CONSTRAINT FK_DIRECTORY_OPERATION FOREIGN KEY(DIRECTORY_ID) REFERENCES CWD_DIRECTORY(ID))
CREATE MEMORY TABLE CWD_GROUP(ID BIGINT NOT NULL PRIMARY KEY,GROUP_NAME VARCHAR(255) NOT NULL,LOWER_GROUP_NAME VARCHAR(255) NOT NULL,ACTIVE CHAR(1) NOT NULL,IS_LOCAL CHAR(1) NOT NULL,CREATED_DATE TIMESTAMP NOT NULL,UPDATED_DATE TIMESTAMP NOT NULL,DESCRIPTION VARCHAR(255),GROUP_TYPE VARCHAR(32) NOT NULL,DIRECTORY_ID BIGINT NOT NULL,CONSTRAINT SYS_CT_177 UNIQUE(LOWER_GROUP_NAME,DIRECTORY_ID),CONSTRAINT FK_DIRECTORY_ID FOREIGN KEY(DIRECTORY_ID) REFERENCES CWD_DIRECTORY(ID))
CREATE INDEX IDX_GROUP_ACTIVE ON CWD_GROUP(ACTIVE,DIRECTORY_ID)
CREATE INDEX IDX_GROUP_DIR_ID ON CWD_GROUP(DIRECTORY_ID)
CREATE MEMORY TABLE CWD_GROUP_ATTRIBUTE(ID BIGINT NOT NULL PRIMARY KEY,GROUP_ID BIGINT NOT NULL,DIRECTORY_ID BIGINT NOT NULL,ATTRIBUTE_NAME VARCHAR(255) NOT NULL,ATTRIBUTE_VALUE VARCHAR(255),ATTRIBUTE_LOWER_VALUE VARCHAR(255),CONSTRAINT SYS_CT_181 UNIQUE(GROUP_ID,ATTRIBUTE_NAME,ATTRIBUTE_LOWER_VALUE),CONSTRAINT FK_GROUP_ATTR_DIR_ID FOREIGN KEY(DIRECTORY_ID) REFERENCES CWD_DIRECTORY(ID),CONSTRAINT FK_GROUP_ATTR_ID_GROUP_ID FOREIGN KEY(GROUP_ID) REFERENCES CWD_GROUP(ID))
CREATE INDEX IDX_GROUP_ATTR_DIR_NAME_LVAL ON CWD_GROUP_ATTRIBUTE(DIRECTORY_ID,ATTRIBUTE_NAME,ATTRIBUTE_LOWER_VALUE)
CREATE INDEX IDX_GROUP_ATTR_GROUP_ID ON CWD_GROUP_ATTRIBUTE(GROUP_ID)
CREATE MEMORY TABLE CWD_MEMBERSHIP(ID BIGINT NOT NULL PRIMARY KEY,PARENT_ID BIGINT,CHILD_ID BIGINT,MEMBERSHIP_TYPE VARCHAR(32),GROUP_TYPE VARCHAR(32) NOT NULL,PARENT_NAME VARCHAR(255) NOT NULL,LOWER_PARENT_NAME VARCHAR(255) NOT NULL,CHILD_NAME VARCHAR(255) NOT NULL,LOWER_CHILD_NAME VARCHAR(255) NOT NULL,DIRECTORY_ID BIGINT NOT NULL,CONSTRAINT SYS_CT_185 UNIQUE(PARENT_ID,CHILD_ID,MEMBERSHIP_TYPE),CONSTRAINT FK_MEMBERSHIP_DIR FOREIGN KEY(DIRECTORY_ID) REFERENCES CWD_DIRECTORY(ID))
CREATE INDEX IDX_MEM_DIR_CHILD ON CWD_MEMBERSHIP(MEMBERSHIP_TYPE,LOWER_CHILD_NAME,DIRECTORY_ID)
CREATE INDEX IDX_MEM_DIR_PARENT ON CWD_MEMBERSHIP(MEMBERSHIP_TYPE,LOWER_PARENT_NAME,DIRECTORY_ID)
CREATE INDEX IDX_MEM_DIR_PARENT_CHILD ON CWD_MEMBERSHIP(MEMBERSHIP_TYPE,LOWER_PARENT_NAME,LOWER_CHILD_NAME,DIRECTORY_ID)
CREATE MEMORY TABLE CWD_PROPERTY(PROPERTY_KEY VARCHAR(255) NOT NULL,PROPERTY_NAME VARCHAR(255) NOT NULL,PROPERTY_VALUE VARCHAR(4000),PRIMARY KEY(PROPERTY_KEY,PROPERTY_NAME))
CREATE MEMORY TABLE CWD_TOKEN(ID BIGINT NOT NULL PRIMARY KEY,DIRECTORY_ID BIGINT NOT NULL,ENTITY_NAME VARCHAR(255) NOT NULL,RANDOM_NUMBER BIGINT NOT NULL,IDENTIFIER_HASH VARCHAR(255) NOT NULL,RANDOM_HASH VARCHAR(255) NOT NULL,CREATED_DATE TIMESTAMP NOT NULL,LAST_ACCESSED_DATE TIMESTAMP NOT NULL,CONSTRAINT SYS_CT_191 UNIQUE(IDENTIFIER_HASH))
CREATE INDEX IDX_TOKEN_KEY ON CWD_TOKEN(RANDOM_HASH)
CREATE INDEX IDX_TOKEN_DIR_ID ON CWD_TOKEN(DIRECTORY_ID)
CREATE INDEX IDX_TOKEN_NAME_DIR_ID ON CWD_TOKEN(DIRECTORY_ID,ENTITY_NAME)
CREATE INDEX IDX_TOKEN_LAST_ACCESS ON CWD_TOKEN(LAST_ACCESSED_DATE)
CREATE MEMORY TABLE CWD_USER(ID BIGINT NOT NULL PRIMARY KEY,USER_NAME VARCHAR(255) NOT NULL,LOWER_USER_NAME VARCHAR(255) NOT NULL,ACTIVE CHAR(1) NOT NULL,CREATED_DATE TIMESTAMP NOT NULL,UPDATED_DATE TIMESTAMP NOT NULL,FIRST_NAME VARCHAR(255),LOWER_FIRST_NAME VARCHAR(255),LAST_NAME VARCHAR(255),LOWER_LAST_NAME VARCHAR(255),DISPLAY_NAME VARCHAR(255),LOWER_DISPLAY_NAME VARCHAR(255),EMAIL_ADDRESS VARCHAR(255),LOWER_EMAIL_ADDRESS VARCHAR(255),DIRECTORY_ID BIGINT NOT NULL,CREDENTIAL VARCHAR(255),CONSTRAINT SYS_CT_195 UNIQUE(LOWER_USER_NAME,DIRECTORY_ID),CONSTRAINT FK_USER_DIR_ID FOREIGN KEY(DIRECTORY_ID) REFERENCES CWD_DIRECTORY(ID))
CREATE INDEX IDX_USER_LOWER_DISPLAY_NAME ON CWD_USER(LOWER_DISPLAY_NAME,DIRECTORY_ID)
CREATE INDEX IDX_USER_LOWER_LAST_NAME ON CWD_USER(LOWER_LAST_NAME,DIRECTORY_ID)
CREATE INDEX IDX_USER_ACTIVE ON CWD_USER(ACTIVE,DIRECTORY_ID)
CREATE INDEX IDX_USER_NAME_DIR_ID ON CWD_USER(DIRECTORY_ID)
CREATE INDEX IDX_USER_LOWER_FIRST_NAME ON CWD_USER(LOWER_FIRST_NAME,DIRECTORY_ID)
CREATE INDEX IDX_USER_LOWER_EMAIL_ADDRESS ON CWD_USER(LOWER_EMAIL_ADDRESS,DIRECTORY_ID)
CREATE MEMORY TABLE CWD_USER_ATTRIBUTE(ID BIGINT NOT NULL PRIMARY KEY,USER_ID BIGINT NOT NULL,DIRECTORY_ID BIGINT NOT NULL,ATTRIBUTE_NAME VARCHAR(255) NOT NULL,ATTRIBUTE_VALUE VARCHAR(255),ATTRIBUTE_LOWER_VALUE VARCHAR(255),CONSTRAINT SYS_CT_199 UNIQUE(USER_ID,ATTRIBUTE_NAME,ATTRIBUTE_LOWER_VALUE),CONSTRAINT FK_USER_ATTR_DIR_ID FOREIGN KEY(DIRECTORY_ID) REFERENCES CWD_DIRECTORY(ID),CONSTRAINT FK_USER_ATTRIBUTE_ID_USER_ID FOREIGN KEY(USER_ID) REFERENCES CWD_USER(ID))
CREATE INDEX IDX_USER_ATTR_DIR_NAME_LVAL ON CWD_USER_ATTRIBUTE(DIRECTORY_ID,ATTRIBUTE_NAME,ATTRIBUTE_LOWER_VALUE)
CREATE INDEX IDX_USER_ATTR_USER_ID ON CWD_USER_ATTRIBUTE(USER_ID)
CREATE MEMORY TABLE CWD_USER_CREDENTIAL_RECORD(ID BIGINT NOT NULL PRIMARY KEY,USER_ID BIGINT NOT NULL,PASSWORD_HASH VARCHAR(255) NOT NULL,LIST_INDEX INTEGER,CONSTRAINT FK_USER_CRED_USER FOREIGN KEY(USER_ID) REFERENCES CWD_USER(ID))
CREATE MEMORY TABLE HIBERNATE_UNIQUE_KEY(NEXT_HI INTEGER)
ALTER TABLE CWD_APP_DIR_GROUP_MAPPING ADD CONSTRAINT FK_APP_DIR_GROUP_DIR FOREIGN KEY(DIRECTORY_ID) REFERENCES CWD_DIRECTORY(ID)
ALTER TABLE CWD_APP_DIR_GROUP_MAPPING ADD CONSTRAINT FK_APP_DIR_GROUP_APP FOREIGN KEY(APPLICATION_ID) REFERENCES CWD_APPLICATION(ID)
ALTER TABLE CWD_APP_DIR_GROUP_MAPPING ADD CONSTRAINT FK_APP_DIR_GROUP_MAPPING FOREIGN KEY(APP_DIR_MAPPING_ID) REFERENCES CWD_APP_DIR_MAPPING(ID)
ALTER TABLE CWD_APP_DIR_MAPPING ADD CONSTRAINT FK_APP_DIR_DIR FOREIGN KEY(DIRECTORY_ID) REFERENCES CWD_DIRECTORY(ID)
ALTER TABLE CWD_APP_DIR_MAPPING ADD CONSTRAINT FK_APP_DIR_APP FOREIGN KEY(APPLICATION_ID) REFERENCES CWD_APPLICATION(ID)
-- CREATE USER SA PASSWORD ""
-- GRANT DBA TO SA
-- SET WRITE_DELAY 10
-- SET SCHEMA PUBLIC
INSERT INTO CWD_DIRECTORY VALUES(1,'Directory Two','directory two','2009-04-23 10:59:35.000000000','2012-12-31 13:41:49.383000000','T','Directory Without Modification Permissions','com.atlassian.crowd.directory.InternalDirectory','com.atlassian.crowd.directory.internaldirectory','INTERNAL')
INSERT INTO CWD_DIRECTORY VALUES(2,'Directory One','directory one','2009-04-03 11:50:40.000000000','2012-12-31 13:41:49.383000000','T','','com.atlassian.crowd.directory.InternalDirectory','com.atlassian.crowd.directory.internaldirectory','INTERNAL')
INSERT INTO CWD_DIRECTORY_ATTRIBUTE VALUES(1,'0','password_history_count')
INSERT INTO CWD_DIRECTORY_ATTRIBUTE VALUES(1,'0','password_max_attempts')
INSERT INTO CWD_DIRECTORY_ATTRIBUTE VALUES(1,'0','password_max_change_time')
INSERT INTO CWD_DIRECTORY_ATTRIBUTE VALUES(1,'atlassian-security','user_encryption_method')
INSERT INTO CWD_DIRECTORY_ATTRIBUTE VALUES(2,'0','password_history_count')
INSERT INTO CWD_DIRECTORY_ATTRIBUTE VALUES(2,'0','password_max_attempts')
INSERT INTO CWD_DIRECTORY_ATTRIBUTE VALUES(2,'0','password_max_change_time')
INSERT INTO CWD_DIRECTORY_ATTRIBUTE VALUES(2,'','password_regex')
INSERT INTO CWD_DIRECTORY_ATTRIBUTE VALUES(2,'true','useNestedGroups')
INSERT INTO CWD_DIRECTORY_ATTRIBUTE VALUES(2,'atlassian-security','user_encryption_method')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(1,'CREATE_GROUP')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(1,'CREATE_USER')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(1,'DELETE_GROUP')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(1,'DELETE_USER')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(1,'UPDATE_GROUP')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(1,'UPDATE_USER')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(2,'CREATE_GROUP')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(2,'CREATE_ROLE')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(2,'CREATE_USER')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(2,'DELETE_GROUP')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(2,'DELETE_ROLE')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(2,'DELETE_USER')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(2,'UPDATE_GROUP')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(2,'UPDATE_ROLE')
INSERT INTO CWD_DIRECTORY_OPERATION VALUES(2,'UPDATE_USER')
INSERT INTO CWD_APPLICATION VALUES(163841,'crowd','crowd','2009-04-03 11:50:50.000000000','2012-12-31 13:41:49.393000000','T','Crowd Console','CROWD','{PKCS5S2}M7DR6dm+f9yXhABBhkq/YPJndfmkaPJz1ziPi6ER0NoDAlS+GZ395rAUhVhRzDz/')
INSERT INTO CWD_APPLICATION VALUES(163842,'google-apps','google-apps','2009-04-03 11:50:11.000000000','2012-12-31 13:41:49.397000000','T','Google Applications Connector','PLUGIN','/iPwhVwwDOlh0cQqlpvtrYdwBLQ78c81Ld7yHhljWuoPT6RURvH1ZoSDYi2lQ9LfoPSyImrqQ0InFcv99NPSRg==')
INSERT INTO CWD_APPLICATION_ADDRESS VALUES(163841,'127.0.0.1','fwAAAQ==',0)
INSERT INTO CWD_APPLICATION_ADDRESS VALUES(163841,'192.168.3.242','wKgD8g==',0)
INSERT INTO CWD_APPLICATION_ADDRESS VALUES(163841,'badger-badger.local',NULL,0)
INSERT INTO CWD_APPLICATION_ADDRESS VALUES(163841,'localhost',NULL,0)
INSERT INTO CWD_APPLICATION_ATTRIBUTE VALUES(163841,'CROWD','applicationType')
INSERT INTO CWD_APPLICATION_ATTRIBUTE VALUES(163841,'true','atlassian_sha1_applied')
INSERT INTO CWD_APPLICATION_ATTRIBUTE VALUES(163842,'PLUGIN','applicationType')
INSERT INTO CWD_APPLICATION_ATTRIBUTE VALUES(163842,'true','atlassian_sha1_applied')
INSERT INTO CWD_APP_DIR_MAPPING VALUES(196609,163841,2,'F',0)
INSERT INTO CWD_APP_DIR_MAPPING VALUES(589825,163841,1,'F',1)
INSERT INTO CWD_APP_DIR_GROUP_MAPPING VALUES(229377,196609,163841,2,'crowd-administrators')
INSERT INTO CWD_APP_DIR_GROUP_MAPPING VALUES(884737,196609,163841,2,'badgers')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(196609,'CREATE_GROUP')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(196609,'CREATE_ROLE')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(196609,'CREATE_USER')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(196609,'DELETE_GROUP')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(196609,'DELETE_ROLE')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(196609,'DELETE_USER')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(196609,'UPDATE_GROUP')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(196609,'UPDATE_ROLE')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(196609,'UPDATE_USER')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(589825,'CREATE_GROUP')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(589825,'CREATE_ROLE')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(589825,'CREATE_USER')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(589825,'DELETE_GROUP')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(589825,'DELETE_ROLE')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(589825,'DELETE_USER')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(589825,'UPDATE_GROUP')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(589825,'UPDATE_ROLE')
INSERT INTO CWD_APP_DIR_OPERATION VALUES(589825,'UPDATE_USER')
INSERT INTO CWD_GROUP VALUES(98305,'crowd-administrators','crowd-administrators','T','F','2009-04-03 11:50:50.000000000','2009-04-03 11:50:50.000000000','','GROUP',2)
INSERT INTO CWD_GROUP VALUES(98307,'badgers','badgers','T','F','2009-04-03 11:50:50.000000000','2009-04-03 11:50:50.000000000','A group for badgers','GROUP',2)
INSERT INTO CWD_GROUP VALUES(524289,'crowd-users','crowd-users','T','F','2010-06-09 13:59:25.000000000','2010-06-09 13:59:25.000000000','','GROUP',2)
INSERT INTO CWD_GROUP VALUES(524290,'animals','animals','T','F','2010-06-09 14:01:20.000000000','2010-06-09 14:01:20.000000000','','GROUP',1)
INSERT INTO CWD_GROUP VALUES(524291,'birds','birds','T','F','2010-06-09 14:01:31.000000000','2010-06-09 14:01:31.000000000','','GROUP',1)
INSERT INTO CWD_GROUP VALUES(524292,'dogs','dogs','T','F','2010-06-09 14:01:39.000000000','2010-06-09 14:01:39.000000000','','GROUP',1)
INSERT INTO CWD_GROUP VALUES(524293,'cats','cats','T','F','2010-06-09 14:01:46.000000000','2010-06-09 14:01:46.000000000','','GROUP',1)
INSERT INTO CWD_GROUP VALUES(720897,'crowd-testers','crowd-testers','T','F','2010-06-10 11:47:08.000000000','2010-06-10 11:47:08.000000000','','GROUP',2)
INSERT INTO CWD_GROUP_ATTRIBUTE VALUES(458753,98307,2,'secret-location','hollow','hollow')
INSERT INTO CWD_PROPERTY VALUES('crowd','build.number','431')
INSERT INTO CWD_PROPERTY VALUES('crowd','cache.enabled','true')
INSERT INTO CWD_PROPERTY VALUES('crowd','com.sun.jndi.ldap.connect.pool.authentication','simple')
INSERT INTO CWD_PROPERTY VALUES('crowd','com.sun.jndi.ldap.connect.pool.initsize','1')
INSERT INTO CWD_PROPERTY VALUES('crowd','com.sun.jndi.ldap.connect.pool.maxsize','0')
INSERT INTO CWD_PROPERTY VALUES('crowd','com.sun.jndi.ldap.connect.pool.prefsize','10')
INSERT INTO CWD_PROPERTY VALUES('crowd','com.sun.jndi.ldap.connect.pool.protocol','plain ssl')
INSERT INTO CWD_PROPERTY VALUES('crowd','com.sun.jndi.ldap.connect.pool.timeout','30000')
INSERT INTO CWD_PROPERTY VALUES('crowd','current.license.resource.total','0')
INSERT INTO CWD_PROPERTY VALUES('crowd','database.token.storage.enabled','true')
INSERT INTO CWD_PROPERTY VALUES('crowd','deployment.title','Crowd Rest Testing')
INSERT INTO CWD_PROPERTY VALUES('crowd','des.encryption.key','mNrjiZI3j4Y=')
INSERT INTO CWD_PROPERTY VALUES('crowd','domain','')
INSERT INTO CWD_PROPERTY VALUES('crowd','email.template.forgotten.username','Hello $firstname $lastname, You have requested the username for your email: $email. Your username is: $username If you think it was sent incorrectly, please contact one of the administrators at: $admincontact $deploymenttitle Administrator')
INSERT INTO CWD_PROPERTY VALUES('crowd','gzip.enabled','true')
INSERT INTO CWD_PROPERTY VALUES('crowd','mailserver.host','mail.atlassian.com')
INSERT INTO CWD_PROPERTY VALUES('crowd','mailserver.jndi','')
INSERT INTO CWD_PROPERTY VALUES('crowd','mailserver.message.template','Hello $firstname $lastname, Your password has been reset by a $deploymenttitle administrator at $date. Your new password is: $password $deploymenttitle Administrator')
INSERT INTO CWD_PROPERTY VALUES('crowd','mailserver.password','yee9Pai1')
INSERT INTO CWD_PROPERTY VALUES('crowd','mailserver.port','25')
INSERT INTO CWD_PROPERTY VALUES('crowd','mailserver.prefix','[dof - Atlassian Crowd]')
INSERT INTO CWD_PROPERTY VALUES('crowd','mailserver.sender','doflynn@atlassian.com')
INSERT INTO CWD_PROPERTY VALUES('crowd','mailserver.username','doflynn')
INSERT INTO CWD_PROPERTY VALUES('crowd','notification.email','doflynn@atlassian.com')
INSERT INTO CWD_PROPERTY VALUES('crowd','secure.cookie','false')
INSERT INTO CWD_PROPERTY VALUES('crowd','session.time','1800000')
INSERT INTO CWD_PROPERTY VALUES('crowd','token.seed','Bm7CXmTp')
INSERT INTO CWD_USER VALUES(32769,'regularuser','regularuser','T','2009-04-23 11:00:31.000000000','2009-04-23 11:00:31.000000000','Reg','reg','User','user','Reg User','reg user','bob@example.net','bob@example.net',1,'SWdlwK77rqPjq4AYrGebTt9ZOYrmmbl6jXW5v46HfMN7MYLU/k/u6hufsacpf1ofxGw/0+BlcOEmMjLEjbe6Rw==')
INSERT INTO CWD_USER VALUES(32770,'admin','admin','T','2009-04-03 11:50:50.000000000','2012-12-31 13:41:57.471000000','bob','bob','the builder','the builder','bob the builder','bob the builder','bob@example.net','bob@example.net',2,'{PKCS5S2}Vbh2LMKcKYfh+SZyAC7JlqmITByp9tzAUtb6GJ8YVV25suslI/cloPujmpgN3d8y')
INSERT INTO CWD_USER VALUES(32771,'eeeep','eeeep','T','2009-04-17 16:06:09.000000000','2010-06-23 15:46:24.000000000','Zeee','zeee','Pop!','pop!','Zeee Pop!','zeee pop!','doflynn@atlassian.com','doflynn@atlassian.com',2,'xwtd2ev7b1HQnUEytxcMnSB1CnhS8AaA9lZY8DEOgQBW5nY8NMmgCw6UAHb1RJXBafwjAszrMSA5JxxDRpUH3A==')
INSERT INTO CWD_USER VALUES(327681,'secondadmin','secondadmin','T','2010-06-07 13:02:41.000000000','2010-06-07 13:02:41.000000000','Second','second','Admin','admin','Second Admin','second admin','secondadmin@example.net','secondadmin@example.net',2,'t6iJiuqtgmVgg4CmRfIfs4Uga4sbZ6Y6qNUK3u+R5FjiB9RD3oEtQgoQBMmpVVoUC5DhyeaN5V2JwdxjeRLE4Q==')
INSERT INTO CWD_USER VALUES(753665,'penny','penny','T','2010-06-10 11:47:31.000000000','2010-06-10 11:47:31.000000000','Penny','penny','Small','small','Penny Small','penny small','penny@example.com','penny@example.com',2,'0cJppBstdKTAbWDnKYpC6boOrITbCj4sgXLe3vcqdkcpgRH+gto9ChJzm1NrDlgvihi3vDzY0vwSRQWBs+hYvw==')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(65537,32769,1,'requiresPasswordChange','false','false')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(65538,32769,1,'passwordLastChanged','1240448431958','1240448431958')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(65539,32770,2,'passwordLastChanged','1238719850103','1238719850103')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(65540,32770,2,'invalidPasswordAttempts','0','0')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(65541,32770,2,'lastAuthenticated','1356921717472','1356921717472')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(65542,32770,2,'requiresPasswordChange','false','false')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(65543,32771,2,'passwordLastChanged','1277271984971','1277271984971')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(65544,32771,2,'requiresPasswordChange','false','false')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(360449,327681,2,'requiresPasswordChange','false','false')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(360450,327681,2,'passwordLastChanged','1275879761824','1275879761824')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(786433,753665,2,'requiresPasswordChange','false','false')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(786434,753665,2,'passwordLastChanged','1276134451307','1276134451307')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(917505,32771,2,'invalidPasswordAttempts','0','0')
INSERT INTO CWD_USER_ATTRIBUTE VALUES(917506,32771,2,'lastAuthenticated','1277271994445','1277271994445')
INSERT INTO CWD_MEMBERSHIP VALUES(131074,98305,32770,'GROUP_USER','GROUP','crowd-administrators','crowd-administrators','admin','admin',2)
INSERT INTO CWD_MEMBERSHIP VALUES(131075,98307,32770,'GROUP_USER','GROUP','badgers','badgers','admin','admin',2)
INSERT INTO CWD_MEMBERSHIP VALUES(393217,98305,327681,'GROUP_USER','GROUP','crowd-administrators','crowd-administrators','secondadmin','secondadmin',2)
INSERT INTO CWD_MEMBERSHIP VALUES(655361,524289,98307,'GROUP_GROUP','GROUP','crowd-users','crowd-users','badgers','badgers',2)
INSERT INTO CWD_MEMBERSHIP VALUES(655362,98307,32771,'GROUP_USER','GROUP','badgers','badgers','eeeep','eeeep',2)
INSERT INTO CWD_MEMBERSHIP VALUES(819201,720897,753665,'GROUP_USER','GROUP','crowd-testers','crowd-testers','penny','penny',2)
INSERT INTO CWD_MEMBERSHIP VALUES(819202,98305,720897,'GROUP_GROUP','GROUP','crowd-administrators','crowd-administrators','crowd-testers','crowd-testers',2)
INSERT INTO CWD_MEMBERSHIP VALUES(819203,720897,98307,'GROUP_GROUP','GROUP','crowd-testers','crowd-testers','badgers','badgers',2)
INSERT INTO CWD_TOKEN VALUES(950273,-1,'crowd',4612218007304958494,'u489Iq0t0xJ3DKsvkNdMcQ00','zDqmVmq2sVrcFcoJBCpYYg00','2012-12-31 13:41:49.302000000','2012-12-31 13:41:49.514000000')
INSERT INTO CWD_TOKEN VALUES(950274,-1,'crowd',4347516695877284923,'HynTzREIXxOhhDEp3DsKxg00','mnM09P0nA1cN6hKpTYRyPg00','2012-12-31 13:41:54.136000000','2012-12-31 13:41:57.735000000')
INSERT INTO CWD_TOKEN VALUES(950275,2,'admin',7488198466994087702,'2ydhQtejZLYmwMI2gtMVvQ00','dWKDF0WijBqA5qGMu15rAQ00','2012-12-31 13:41:57.477000000','2012-12-31 13:41:57.701000000')

-- handmade
INSERT INTO CWD_APPLICATION_ALIAS VALUES(55500, 163841, 'penny', 'penny', 'pen', 'pen')

INSERT INTO HIBERNATE_UNIQUE_KEY VALUES(30)
