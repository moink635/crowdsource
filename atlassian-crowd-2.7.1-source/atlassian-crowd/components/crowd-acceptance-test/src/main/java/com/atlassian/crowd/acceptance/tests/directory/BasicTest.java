package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.DbCachingRemoteDirectory;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.SpringLDAPConnector;
import com.atlassian.crowd.directory.SpringLDAPConnectorAccessor;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.ReadOnlyGroupException;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.directory.SynchronisationMode;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.LDAPUserWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.SearchContext;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.util.UserUtils;
import com.atlassian.spring.container.ContainerManager;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.ldap.LdapName;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import com.google.common.collect.Iterables;


public abstract class BasicTest extends BaseTest
{
    private static final Logger logger = LoggerFactory.getLogger(BaseTest.class);

    private final String autoTestUserName = "auto-test-user";
    private final String autoTestUserName2 = "auto-2-test-user";

    private final String autoTestFirstName = "Auto";                    //TODO: add odd characters to this
    private final String autoTestFirstName2 = "Sir Auto";
    private final String autoTestLastName = "Test";                    //TODO: add odd characters to this

    private final String autoTestPassword = "My-test-password1";        //TODO: add odd characters to this
    private final String autoTestPassword2 = "My-test-password1-again";  //TODO: add odd characters to this
    private final String autoTestPassword3 = "My-test-password1€";

    private final String autoTestValidEmail = "test.emailextension@test-ad-example.com";
    private final String autoTestValidEmail2 = "test.2.emailextension@test-ad-example.com";
    private final String autoTestInvalidEmail = "test_borkborkbork";

    private final String autoTestDisplayName = "My Test User";
    private final String autoTestDisplayName2 = "My Test User, Again";
    private final String autoTestDisplayName3 = "My Test User, Yet Again";

    private final String autoTestGroupName = "auto-test-group";
    private final String autoTestGroupName2 = "auto-2-test-group";

    private final String autoTestGroupDescription = "My First Group";
    private final String autoTestGroupDescription2 = "My First Group With A Better Description";

    private final String autoTestRoleName = "auto-test-role-name";
    private final String autoTestRoleName2 = "auto-2-test-role-name";


    private final String autoTestRoleDescription = "My First Role";
    private final String autoTestRoleDescription2 = "My First Role With A Description of Awesomeness!";

    protected BasicTest()
    {
    }

    protected BasicTest(String name)
    {
        super(name);
    }

    public static RemoteDirectory getImplementation(Directory directory) throws DirectoryInstantiationException
    {
        return ((DirectoryInstanceLoader) ContainerManager.getComponent("directoryInstanceLoader")).getDirectory(directory);
    }

    static RemoteDirectory getRawImplementation(Directory directory) throws DirectoryInstantiationException
    {
        return ((DirectoryInstanceLoader) ContainerManager.getComponent("directoryInstanceLoader")).getRawDirectory(
                directory.getId(), directory.getImplementationClass(), directory.getAttributes());
    }

    @Override
    protected void configureDirectory(Properties directorySettings)
    {
        super.configureDirectory(directorySettings);
        directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.TRUE.toString());
    }

    private User addUserAndAssertCreation(String name, String displayName, String firstName, String lastName, String email, String password, boolean active)
            throws InvalidUserException, RemoteException, InvalidCredentialException, UserNotFoundException, OperationFailedException, UserAlreadyExistsException, DirectoryNotFoundException
    {
        UserTemplate user = new UserTemplate(name, directory.getId());
        user.setActive(true);
        user.setName(name);
        user.setDisplayName(displayName);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmailAddress(email);

        PasswordCredential credential = null;
        if (password != null)
        {
            credential = new PasswordCredential(password);
        }

        User addedUser = getImplementation(directory).addUser(user, credential);

        assertNotNull(addedUser);

        assertEquals(name, addedUser.getName());

        User populatedUser = UserUtils.populateNames(user);
        assertEquals(populatedUser.getDisplayName(), addedUser.getDisplayName());
        assertEquals(populatedUser.getFirstName(), addedUser.getFirstName());
        assertEquals(populatedUser.getLastName(), addedUser.getLastName());

        if (StringUtils.isNotBlank(email))
        {
            assertEquals(email, addedUser.getEmailAddress());
        }


        return addedUser;
        // active state does not get persisted for LDAP directories
        //assertEquals(active, addedUser.isActive());
    }

    private void addUserWithDefaults()
            throws InvalidUserException, RemoteException, InvalidCredentialException, UserNotFoundException, OperationFailedException, UserAlreadyExistsException, DirectoryNotFoundException
    {
        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword(), true);
    }

    private void authenticate(String password) throws  RemoteException, InvalidUserException, InactiveAccountException, InvalidAuthenticationException, UserNotFoundException, ExpiredCredentialException, OperationFailedException
    {
        getImplementation(directory).authenticate(getAutoTestUserName(), new PasswordCredential(password));  // throws if unsuccessful
    }

    private Group addGroup()
            throws InvalidGroupException, RemoteException, OperationFailedException, DirectoryNotFoundException, GroupNotFoundException
    {
        return addGroupWithDescription(getAutoTestGroupName(), "Default Description");
    }

    private Group addGroupWithDescription(String groupName, String description)
            throws InvalidGroupException, OperationFailedException, DirectoryNotFoundException, GroupNotFoundException
    {
        GroupTemplate group = new GroupTemplate(groupName, directory.getId(), GroupType.GROUP);
        group.setActive(true);
        group.setName(groupName);
        group.setDescription(description);

        Group addedGroup = getImplementation(directory).addGroup(group);

        assertEquals(getImplementation(directory).getDirectoryId(), addedGroup.getDirectoryId());
        assertEquals(group.getName(), addedGroup.getName());
        assertEquals(description, addedGroup.getDescription());
        return addedGroup;
    }

    /**
     * Returns the total number of users (active or inactive)
     *
     * @return
     * @throws DirectoryInstantiationException
     *
     * @throws RemoteException
     */
    private int getTotalUserCount() throws OperationFailedException
    {
        List<User> users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).returningAtMost(EntityQuery.ALL_RESULTS));
        return users.size();
    }

    private void addUserToGroup()
        throws RemoteException, InvalidUserException, InvalidCredentialException, InvalidGroupException,
               OperationFailedException, UserAlreadyExistsException, DirectoryNotFoundException, UserNotFoundException,
               GroupNotFoundException, ReadOnlyGroupException, MembershipAlreadyExistsException
    {
        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword(), true);
        addGroup();

        getImplementation(directory).addUserToGroup(getAutoTestUserName(), getAutoTestGroupName());
    }

    protected void loadTestData()
    {
        // not needed for this test suite :-)
    }

    /**
     * Override - called before & after every test to remove test data from directory
     */
    protected void removeTestData() throws DirectoryInstantiationException
    {
        DirectoryTestHelper.silentlyRemoveUser(getAutoTestUserName(), getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser(getAutoTestUserName2(), getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup(getAutoTestGroupName(), getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup(getAutoTestGroupName2(), getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup(getAutoTestRoleName(), getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup(getAutoTestRoleName2(), getRemoteDirectory());
    }

    /**
     * CWD-584 - users created through JIRA work this way.
     */
    public void testAddUserWithNullPassword()
            throws InvalidUserException, RemoteException, InvalidCredentialException, OperationFailedException, UserAlreadyExistsException, DirectoryNotFoundException, UserNotFoundException
    {
        logger.info("Running testAddUserWithNullPassword");

        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), null, true);
    }

    public void testAddUserExisting()
            throws InvalidUserException, RemoteException, InvalidCredentialException, OperationFailedException, UserAlreadyExistsException, DirectoryNotFoundException, UserNotFoundException
    {
        logger.info("Running testAddUserExisting");

        addUserWithDefaults();
        try
        {
            addUserWithDefaults();

            fail("Created same user (" + getAutoTestUserName() + ") twice.");
        }
        catch (InvalidUserException e)
        {
        }
    }

    public void testAddUserNull() throws Exception
    {
        logger.info("Running testAddUserNull");

        try
        {
            getImplementation(directory).addUser(null, null);

            fail("Passing in a null user should have thrown a NullPointerException.");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testAddUserInactive() throws Exception
    {
        logger.info("Running testAddUserInactive");

        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword(), false);
    }

    public void testAddUserWithFirstnameNoPassword() throws Exception
    {
        logger.info("Running testAddUserWithFirstnameNoPassword");

        UserTemplate user = new UserTemplate(getAutoTestUserName());
        user.setFirstName(getAutoTestFirstName());

        addUserAndAssertCreation(getAutoTestUserName(), null, getAutoTestFirstName(), null, null, null, true);
    }

    public void testAddUserWithFirstname() throws Exception
    {
        logger.info("Running testAddUserWithFirstname");

        UserTemplate user = new UserTemplate(getAutoTestUserName());
        user.setFirstName(getAutoTestFirstName());

        addUserAndAssertCreation(getAutoTestUserName(), null, getAutoTestFirstName(), null, null, getAutoTestPassword(), true);
    }

    public void testAddUserWithLastnameNoPassword() throws Exception
    {
        logger.info("Running testAddUserWithLastnameNoPassword");

        addUserAndAssertCreation(getAutoTestUserName(), null, null, getAutoTestLastName(), null, null, true);
    }

    public void testAddUserWithLastnameAndPassword() throws Exception
    {
        logger.info("Running testAddUserWithLastnameAndPassword");

        addUserAndAssertCreation(getAutoTestUserName(), null, null, getAutoTestLastName(), null, getAutoTestPassword(), true);
    }

    public void testAddUserWithEmailAndPassword() throws Exception
    {
        logger.info("Running testAddUserWithEmail");

        addUserAndAssertCreation(getAutoTestUserName(), null, null, null, getAutoTestValidEmail(), getAutoTestPassword(), true);
    }

    public void testAddUserWithDisplayName() throws Exception
    {

        logger.info("Running testAddUserWithDisplayName");

        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), null, null, null, getAutoTestPassword(), true);
    }

    public void testAddUserWithFirstnameLastnameEmailPassword() throws Exception
    {
        logger.info("Running testAddUserWithFirstnameLastnameEmailPassword");

        addUserAndAssertCreation(getAutoTestUserName(), null, getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword(), true);
        authenticate(getAutoTestPassword());
    }


    public void testAddUserWithFirstnameLastnameEmailDisplayNamePassword() throws Exception
    {
        logger.info("Running testAddUserWithFirstnameLastnameEmailDisplayNamePassword");

        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword(), true);
        authenticate(getAutoTestPassword());
    }

    public void testAddUserWithEmptyPassword() throws Exception
    {
        logger.info("Running testAddUserWithEmptyPassword");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestInvalidEmail, "", true);
    }


    /*
    * RemoteDirectory.updateUserCredential() & RemoteDirectory.authenticate()
    *
    */
    public void testAuthenticateEmptyPassword() throws Exception
    {
        logger.info("Running testAuthenticateEmptyPassword");

        DirectoryTestHelper.addUser(autoTestUserName, directory.getId(), "", getRemoteDirectory());
        try
        {
            authenticate("");

            fail("Should not be able to authenticate with a blank password");
        }
        catch (InvalidAuthenticationException e)
        {
            // can't authenticate with a blank password
        }
    }

    public void testUpdateUserCredentialAfterNullCreation() throws Exception
    {
        logger.info("Running testUpdateUserCredentialAfterNullCreation");

        DirectoryTestHelper.addUser(autoTestUserName, directory.getId(), null, getRemoteDirectory());

        PasswordCredential credential = new PasswordCredential(getAutoTestPassword());
        getImplementation(directory).updateUserCredential(getAutoTestUserName(), credential);
    }

    public void testAuthenticateAfterPasswordAdd() throws Exception
    {
        logger.info("Running testAuthenticateAfterPasswordAdd");

        DirectoryTestHelper.addUser(autoTestUserName, directory.getId(), null, getRemoteDirectory());

        PasswordCredential credential = new PasswordCredential(getAutoTestPassword());
        getImplementation(directory).updateUserCredential(getAutoTestUserName(), credential);

        authenticate(getAutoTestPassword());
    }

    public void testUpdateUserAfterPasswordAdded() throws Exception
    {
        logger.info("Running testUpdateUserAfterPasswordAdded");

        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword(), true);

        PasswordCredential credential = new PasswordCredential(getAutoTestPassword2());
        getImplementation(directory).updateUserCredential(getAutoTestUserName(), credential);
    }


    public void testAuthenticateAfterPasswordUpdate() throws Exception
    {
        logger.info("Running testAuthenticateAfterPasswordUpdate");

        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword(), true);

        authenticate(getAutoTestPassword());

        PasswordCredential credential = new PasswordCredential(getAutoTestPassword2());
        getImplementation(directory).updateUserCredential(getAutoTestUserName(), credential);

        authenticate(getAutoTestPassword2());
    }

    public void testAuthenticateAfterUnicodePasswordUpdate() throws Exception
    {
        logger.info("Running testAuthenticateAfterUnicodePasswordUpdate");

        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword(), true);

        authenticate(getAutoTestPassword());

        PasswordCredential credential = new PasswordCredential(getAutoTestPassword3());
        getImplementation(directory).updateUserCredential(getAutoTestUserName(), credential);

        authenticate(getAutoTestPassword3());
    }

    public void testAddUserWithPassword() throws Exception
    {
        logger.info("Running testAddUserWithPassword");

        UserTemplate user = new UserTemplate(getAutoTestUserName(), directory.getId());
        user.setActive(true);
        user.setName(getAutoTestUserName());
        user.setLastName(getAutoTestUserName());

        getImplementation(directory).addUser(user, new PasswordCredential(getAutoTestPassword()));
    }


    public void testAuthenticateAfterUserAdd() throws Exception
    {
        logger.info("Running testAuthenticateAfterUserAdd");

        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword(), true);
        authenticate(getAutoTestPassword());
    }

    public void testAuthenticateAfterUserAddWithUnicodePassword() throws Exception
    {
        logger.info("Running testAuthenticateAfterUserAddWithUnicodePassword");

        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword3(), true);
        authenticate(getAutoTestPassword3());
    }

    public void testAuthenticateWithNullPasswordArray() throws Exception
    {
        logger.info("Running testAuthenticateWithNullPasswordArray");

        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword(), true);

        try
        {
            getImplementation(directory).authenticate(getAutoTestUserName(), null);

            fail("Should have thrown InvalidAuthenticationException");
        }
        catch (InvalidAuthenticationException e)
        {
        }
    }

    public void testAuthenticateWithNullPasswordCredentialInArray() throws Exception
    {
        logger.info("Running testAuthenticateWithNullPasswordCredentialInArray");

        addUserWithDefaults();

        try
        {
            getImplementation(directory).authenticate(getAutoTestUserName(), null);

            fail("Should have thrown InvalidAuthenticationException");
        }
        catch (InvalidAuthenticationException e)
        {
        }
    }

    public void testAuthenticateWithNullPassword() throws Exception
    {
        logger.info("Running testAuthenticateWithNullPassword");

        addUserWithDefaults();

        try
        {
            getImplementation(directory).authenticate(getAutoTestUserName(), new PasswordCredential());  // throws if unsuccessful

            fail("Should have thrown InvalidAuthenticationException");
        }
        catch (InvalidAuthenticationException e)
        {
        }
    }

    public void testAuthenticateWithEmptyPassword() throws Exception
    {
        logger.info("Running testAuthenticateWithEmptyPassword");

        addUserWithDefaults();

        try
        {
            getImplementation(directory).authenticate(getAutoTestUserName(), new PasswordCredential(""));  // throws if unsuccessful

            fail("Should have thrown InvalidAuthenticationException");
        }
        catch (InvalidAuthenticationException e)
        {
        }
    }

    public void testAuthenticateWithIncorrectPassword() throws Exception
    {
        logger.info("Running testAuthenticateWithIncorrectPassword");

        addUserWithDefaults();

        try
        {
            getImplementation(directory).authenticate(getAutoTestUserName(), new PasswordCredential("boop-boop-be-doop"));  // throws if unsuccessful

            fail("Should have thrown InvalidAuthenticationException");
        }
        catch (InvalidAuthenticationException e)
        {
        }
    }

    public void testAuthenticateWithInvalidUser() throws Exception
    {
        logger.info("Running testAuthenticateWithInvalidUser");

        addUserWithDefaults();
        try
        {
            getImplementation(directory).authenticate("my-made-up-user-name", new PasswordCredential("this-shouldnt-matter"));  // throws if unsuccessful

            fail("Should have thrown UserNotFoundException");
        }
        catch (UserNotFoundException e)
        {
            // yay!
        }
    }


/*
 * RemoteDirectory.removeUser()
 */

    public void testRemoveUserNull() throws Exception
    {
        logger.info("Running testRemoveUserNull");

        addUserWithDefaults();
        try
        {
            getImplementation(directory).removeUser(null);

            fail("Should have thrown NullPointerException");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testRemoveUserEmptyString() throws Exception
    {
        logger.info("Running testRemoveUserEmptyString");

        addUserWithDefaults();
        try
        {
            getImplementation(directory).removeUser("");

            fail("Should have thrown IllegalArgumentException");
        }
        catch (IllegalArgumentException e)
        {
        }
    }

    public void testRemoveUserInvalid() throws Exception
    {
        logger.info("Running testRemoveUserInvalid");

        addUserWithDefaults();
        try
        {
            getImplementation(directory).removeUser("i-wanna-be-a-small-furry-animal");

            fail("Should have thrown UserNotFoundException");
        }
        catch (UserNotFoundException e)
        {
        }
    }

    public void testRemoveUser() throws Exception
    {
        logger.info("Running testRemoveUser");

        addUserWithDefaults();
        getImplementation(directory).removeUser(getAutoTestUserName());
    }


/*
 * RemoteDirectory.updateUser()
 *
 */

    public void testUpdateUserNull() throws Exception
    {
        logger.info("Running testUpdateUserNull");

        try
        {
            getImplementation(directory).updateUser(null);

            fail("Should have thrown NullPointerException");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testUpdateUserInvalidName() throws Exception
    {
        logger.info("Running testUpdateUserInvalidName");

        addUserWithDefaults();

        UserTemplate user = new UserTemplate("mice-dont-live-in-ldap-directories", directory.getId());
        try
        {
            getImplementation(directory).updateUser(user);

            fail("Should have thrown UserNotFoundException");
        }
        catch (UserNotFoundException e)
        {
        }
    }

    public void testUpdateUserEmail() throws Exception
    {
        logger.info("Running testUpdateUserEmail");

        User addedUser = addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        UserTemplate modifiedUser = new UserTemplate(addedUser);
        modifiedUser.setEmailAddress(getAutoTestValidEmail2());

        User updatedUser = getImplementation(directory).updateUser(modifiedUser);

        assertEquals(modifiedUser.getName(), updatedUser.getName());
        assertEquals(modifiedUser.getEmailAddress(), updatedUser.getEmailAddress());
    }

    public void testUpdateUserEmailExplicitCheck() throws Exception
    {
        logger.info("Running testUpdateUserEmailExplicitCheck");

        User addedUser = addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        UserTemplate updatingUser = new UserTemplate(addedUser);
        updatingUser.setEmailAddress(getAutoTestValidEmail());

        getImplementation(directory).updateUser(updatingUser);

        // Separate check because we don't trust the RemoteDirectory to tell the truth about the update - it could
        //  have created the updated user, then failed to update the RemoteDirectory appropriately.
        User updatedUser = getImplementation(directory).findUserByName(getAutoTestUserName());

        assertEquals(updatingUser.getName(), updatedUser.getName());  // only checks some fields (as of time of writing: name & active flag)
        assertEquals(updatingUser.getEmailAddress(), updatedUser.getEmailAddress());
    }

    public void testUpdateUserEmailInvalid() throws Exception
    {
        logger.info("Running testUpdateUserEmailInvalid");

        User addedUser = addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        UserTemplate updatingUser = new UserTemplate(addedUser);
        updatingUser.setEmailAddress(getAutoTestInvalidEmail());

        User updatedUser = getImplementation(directory).updateUser(updatingUser);

        assertEquals(updatingUser.getName(), updatedUser.getName());  // only checks some fields (as of time of writing: name & active flag)
        assertEquals(updatingUser.getEmailAddress(), updatedUser.getEmailAddress());
    }


/*
 * RemoteDirectory.findUserByName()
 *
 */

    public void testFindUserByNameNull() throws Exception
    {
        logger.info("Running testFindUserByNameNull");

        try
        {
            getImplementation(directory).findUserByName(null);

            fail("Should not have found null user");

        }
        catch (NullPointerException e)
        {
        }
    }

    public void testFindUserByNameEmpty() throws RemoteException, OperationFailedException
    {
        logger.info("Running testFindUserByNameEmpty");

        try
        {
            getImplementation(directory).findUserByName("");

            fail("Should not have found empty user");
        }
        catch (UserNotFoundException e)
        {
        }
    }

    public void testFindUserByNameNonExistent() throws Exception
    {
        logger.info("Running testFindUserByNameNonExistent");

        addUserWithDefaults();
        try
        {
            getImplementation(directory).findUserByName("I've-been-to-paradise-but-I've-never-been-to-me");

            fail("Should not have found user \"I've-been-to-paradise-but-I've-never-been-to-me\"");
        }
        catch (UserNotFoundException e)
        {
        }
    }

    public void testFindUserByName() throws Exception
    {
        logger.info("Running testFindUserByName");

        addUserWithDefaults();

        User user = getImplementation(directory).findUserByName(getAutoTestUserName());
        assertEquals(getAutoTestUserName(), user.getName());
    }

    public void testFindUserByNameDisplayNamePopulatedInLdap() throws Exception
    {
        logger.info("Running testFindUserByName_displayNamePopulated");

        addUserWithDefaults();

        final SpringLDAPConnectorAccessor accessor = new SpringLDAPConnectorAccessor((SpringLDAPConnector) getRawImplementation(directory));
        LDAPUserWithAttributes currentUser = (LDAPUserWithAttributes) getRawImplementation(directory).findUserByName(getAutoTestUserName());
        String displayNameAttributeKey = accessor.getLdapPropertiesMapper().getUserDisplayNameAttribute();
        ModificationItem[] items = {new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(displayNameAttributeKey, " "))};
        accessor.getLdapTemplate().modifyAttributes(new LdapName(currentUser.getDn()), items);

        User user = getRawImplementation(directory).findUserByName(getAutoTestUserName());
        assertEquals(getAutoTestUserName(), user.getName());
        assertEquals(getAutoTestFirstName() + " " + getAutoTestLastName(), user.getDisplayName());
    }

    /*
    * RemoteDirectory.addGroup()
    *
    */
    public void testAddGroupNull() throws Exception
    {
        logger.info("Running testAddGroupNull");

        try
        {
            getImplementation(directory).addGroup(null);

            fail("Should not have created null group");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testAddGroup() throws Exception
    {
        logger.info("Running testAddGroup");

        Group addedGroup = addGroup();
        Group foundGroup = getImplementation(directory).findGroupByName(getAutoTestGroupName());
        assertEquals(addedGroup.getName(), foundGroup.getName());
        assertEquals(addedGroup.getType(), foundGroup.getType());
        assertEquals(addedGroup.getDirectoryId(), foundGroup.getDirectoryId());
        assertEquals(addedGroup.getDescription(), foundGroup.getDescription());
    }

    public void testAddGroupExisting() throws Exception
    {
        logger.info("Running testAddGroupExisting");

        addGroup();
        try
        {
            addGroup();

            fail("Created same group twice");
        }
        catch (InvalidGroupException e)
        {
        }
    }
/*
    // Not implemented since in-active groups currently is not fully implemented
    public void testAddGroupInactive() throws  RemoteException, InvalidGroupException
    {
        Group group = new Group();
        group.setActive(false);
        group.setName(autoTestGroupName);

        Group addedGroup = getImplementation(directory).addGroup(group);
        Group foundGroup = getImplementation(directory).findGroupByName(autoTestGroupName);

        assertEquals(group, addedGroup);
        assertEquals(group, foundGroup);

        assertEquals(group.isActive(), addedGroup.isActive());
        assertEquals(group.isActive(), foundGroup.isActive());
    }
*/

    public void testAddGroupWithDescription() throws Exception
    {
        logger.info("Running testAddGroupWithDescription");

        GroupTemplate group = new GroupTemplate(getAutoTestGroupName(), directory.getId(), GroupType.GROUP);
        group.setDescription(getAutoTestGroupDescription());

        Group addedGroup = getImplementation(directory).addGroup(group);
        Group foundGroup = getImplementation(directory).findGroupByName(getAutoTestGroupName());

        assertEquals(getImplementation(directory).getDirectoryId(), addedGroup.getDirectoryId());
        assertEquals(group.getName(), addedGroup.getName());

        assertEquals(getImplementation(directory).getDirectoryId(), foundGroup.getDirectoryId());
        assertEquals(group.getName(), foundGroup.getName());

        assertEquals(group.getDescription(), addedGroup.getDescription());
        assertEquals(group.getDescription(), foundGroup.getDescription());
    }

/*
 * RemoteDirectory.isGroupMember()
 *
 */

    public void testIsGroupMemberBothNull() throws Exception
    {
        logger.info("Running testIsGroupMemberBothNull");

        try
        {
            getImplementation(directory).isUserDirectGroupMember(null, null);
            fail("Should have thrown NullPointerException");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testIsGroupMemberBothEmpty() throws Exception
    {
        logger.info("Running testIsGroupMemberBothEmpty");

        try
        {
            getImplementation(directory).isUserDirectGroupMember("", "");
            fail("should have thrown IllegalArgumentException");
        }
        catch (IllegalArgumentException e)
        {
        }
    }

    public void testIsGroupMemberNonExistentUser() throws Exception
    {
        logger.info("Running testIsGroupMemberNonExistentUser");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addGroup();

        assertFalse(getImplementation(directory).isUserDirectGroupMember("meeep-meep-moop", getAutoTestGroupName()));
    }

    public void testIsGroupMemberNonExistentGroup() throws Exception
    {
        logger.info("Running testIsGroupMemberNonExistentGroup");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addGroup();

        assertFalse(getImplementation(directory).isUserDirectGroupMember(getAutoTestUserName(), "meeep-meep-moop"));

    }

    public void testIsGroupMember() throws Exception
    {
        logger.info("Running testIsGroupMember");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addGroup();
        getImplementation(directory).addUserToGroup(getAutoTestUserName(), getAutoTestGroupName());

        assertTrue(getImplementation(directory).isUserDirectGroupMember(getAutoTestUserName(), getAutoTestGroupName()));
    }


/*
* RemoteDirectory.searchGroups()
*
*/

    public void testSearchGroupsEmptyContext() throws Exception
    {
        logger.info("Running testSearchGroupsEmptyContext");

        addGroup();

        List groups = getImplementation(directory).searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP)).returningAtMost(EntityQuery.ALL_RESULTS));
        assertTrue(groups.size() >= 1); // should have at least the group we've added.
    }

    public void testSearchGroupsNameEmptyString() throws Exception
    {
        logger.info("Running testSearchGroupsNameEmptyString");

        addGroup();

        List groups = getImplementation(directory).searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).with(Restriction.on(GroupTermKeys.NAME).containing("")).returningAtMost(EntityQuery.ALL_RESULTS));
        assertTrue(groups.size() != 0);
    }

    public void testSearchGroupsNameNonExistent() throws Exception
    {
        logger.info("Running testSearchGroupsNameNonExistent");

        addGroup();

        List groups = getImplementation(directory).searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).with(Restriction.on(GroupTermKeys.NAME).containing("wibble-wobble-up-and-down")).returningAtMost(EntityQuery.ALL_RESULTS));
        assertTrue(groups.size() == 0);
    }

    public void testSearchGroupsNameSingleMatch() throws Exception
    {
        logger.info("Running testSearchGroupsNameSingleMatch");

        addGroup();

        List groups = getImplementation(directory).searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).with(Restriction.on(GroupTermKeys.NAME).containing(getAutoTestGroupName())).returningAtMost(EntityQuery.ALL_RESULTS));
        assertTrue(groups.size() == 1);
    }

    public void testSearchGroupsNameWildcardSingleMatch() throws Exception
    {
        logger.info("Running testSearchGroupsNameWildcardSingleMatch");

        addGroupWithDescription(getAutoTestGroupName(), null);
        addGroupWithDescription(getAutoTestGroupName2(), null);

        List groups = getImplementation(directory).searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).with(Restriction.on(GroupTermKeys.NAME).containing("auto-t")).returningAtMost(EntityQuery.ALL_RESULTS));
        assertEquals(1, groups.size());
    }

    public void testSearchGroupsNameWildcardDualMatch() throws Exception
    {
        logger.info("Running testSearchGroupsNameWildcardDualMatch");

        addGroupWithDescription(getAutoTestGroupName(), null);
        addGroupWithDescription(getAutoTestGroupName2(), null);

        List groups = getImplementation(directory).searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).with(Restriction.on(GroupTermKeys.NAME).containing("auto")).returningAtMost(EntityQuery.ALL_RESULTS));
        assertEquals(2, groups.size());
    }

    public void testSearchGroupsContainingNull() throws Exception
    {
        logger.info("Running testSearchGroupsContainingNull");

        addGroup();

        try
        {
            getImplementation(directory).searchGroupRelationships(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(null).returningAtMost(EntityQuery.ALL_RESULTS));

            fail("Should have thrown NullPointerException");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testSearchGroupsContainingEmptyString() throws Exception
    {
        logger.info("Running testSearchGroupsContainingEmptyString");

        addGroup();

        List<Group> groups = getImplementation(directory).searchGroupRelationships(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("").returningAtMost(EntityQuery.ALL_RESULTS));
        assertTrue(groups.isEmpty());
    }

    public void testSearchGroupsContainingNonExistentUser() throws Exception
    {
        logger.info("Running testSearchGroupsContainingNonExistentUser");

        addGroupWithDescription(getAutoTestGroupName(), null);
        addGroupWithDescription(getAutoTestGroupName2(), null);

        List<Group> groups = getImplementation(directory).searchGroupRelationships(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("I'm-a-hippy-and-I-wanna-get...").returningAtMost(EntityQuery.ALL_RESULTS));
        assertTrue(groups.isEmpty());
    }

    public void testSearchGroupsNotContainingUser() throws Exception
    {
        logger.info("Running testSearchGroupsNotContainingUser");

        addUserWithDefaults();
        addGroupWithDescription(getAutoTestGroupName(), null);
        addGroupWithDescription(getAutoTestGroupName2(), null);

        List<Group> groups = getImplementation(directory).searchGroupRelationships(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(getAutoTestUserName()).returningAtMost(EntityQuery.ALL_RESULTS));
        assertNotNull(groups);
        assertTrue("There should be no groups containing this user", groups.size() == 0);
    }


    public void testSearchGroupsContainingUser() throws Exception
    {
        logger.info("Running testSearchGroupsContainingUser");

        addUserWithDefaults();
        addGroupWithDescription(getAutoTestGroupName(), null);
        addGroupWithDescription(getAutoTestGroupName2(), null);
        getImplementation(directory).addUserToGroup(getAutoTestUserName(), getAutoTestGroupName());
        getImplementation(directory).addUserToGroup(getAutoTestUserName(), getAutoTestGroupName2());

        List<Group> groups = getImplementation(directory).searchGroupRelationships(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(getAutoTestUserName()).returningAtMost(EntityQuery.ALL_RESULTS));

        assertNotNull(groups);
        assertTrue("There should be two groups containing this user", groups.size() == 2);
        assertTrue("One of groups should be " + getAutoTestGroupName(), ((Group) groups.get(0)).getName().equals(getAutoTestGroupName()) || ((Group) groups.get(1)).getName().equals(getAutoTestGroupName()));
        assertTrue("One of groups should be " + getAutoTestGroupName2(), ((Group) groups.get(1)).getName().equals(getAutoTestGroupName2()) || ((Group) groups.get(0)).getName().equals(getAutoTestGroupName2()));
    }

    public void testSearchGroupsByDescription() throws Exception
    {
        logger.info("Running testSearchGroupsByDescription");

        addGroupWithDescription(getAutoTestGroupName(), getAutoTestGroupDescription());
        addGroupWithDescription(getAutoTestGroupName2(), getAutoTestGroupDescription2());

        TermRestriction<String> descriptionRestriction =
            new TermRestriction<String>(GroupTermKeys.DESCRIPTION, MatchMode.EXACTLY_MATCHES,
                                        getAutoTestGroupDescription2());

        EntityQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group(),
                                                         descriptionRestriction, 0, EntityQuery.ALL_RESULTS);

        List<Group> groups = getImplementation(directory).searchGroups(query);

        assertTrue("There should be exactly one group containing this user", groups.size() == 1);
        assertEquals(getAutoTestGroupName2(), Iterables.getOnlyElement(groups).getName());
    }

/*
 * RemoteDirectory.findGroupByName()
 *
 */

    public void testFindGroupByNameNull() throws Exception
    {
        logger.info("Running testFindGroupByNameNull");

        try
        {
            getImplementation(directory).findGroupByName(null);

            fail("Should have thrown NullPointerException");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testFindGroupByNameEmptyString() throws  RemoteException, OperationFailedException
    {
        logger.info("Running testFindGroupByNameEmptyString");

        try
        {
            getImplementation(directory).findGroupByName("");

            fail("Should have thrown GroupNotFoundException");
        }
        catch (GroupNotFoundException e)
        {
        }
    }

    public void testFindGroupByNameNonExistent() throws  RemoteException, OperationFailedException
    {
        logger.info("Running testFindGroupByNameNonExistent");

        try
        {
            getImplementation(directory).findGroupByName("Ill-fix-ya-with-a-drumstick-and-Ill-do-it-for-free");

            fail("Should have thrown GroupNotFoundException");
        }
        catch (GroupNotFoundException e)
        {
        }
    }

    public void testFindGroupByName() throws Exception
    {
        logger.info("Running testFindGroupByName");

        addGroup();
        Group group = getImplementation(directory).findGroupByName(getAutoTestGroupName());
        assertNotNull(group);
        assertEquals(getAutoTestGroupName(), group.getName());
    }


/*
 * RemoteDirectory.updateGroup()
 *
 */

    public void testUpdateGroupNull() throws Exception
    {
        logger.info("Running testUpdateGroupNull");

        try
        {
            getImplementation(directory).updateGroup(null);

            fail("Should have thrown NullPointerException");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testUpdateGroupNonExistent() throws Exception
    {
        logger.info("Running testUpdateGroupNonExistent");

        GroupTemplate group = new GroupTemplate("i-bet-we-dont-have-a-badger-appreciation-society-group", directory.getId(), GroupType.GROUP);

        try
        {
            getImplementation(directory).updateGroup(group);

            fail("Should have thrown GroupNotFoundException");
        }
        catch (GroupNotFoundException e)
        {
        }
        catch (UnsupportedOperationException e)
        {
        }
    }

    public void testUpdateGroupSetDescriptionFromNull() throws Exception
    {
        logger.info("Running testUpdateGroupSetDescriptionFromNull");

        Group addedGroup = addGroup();

        GroupTemplate group = new GroupTemplate(addedGroup);
        group.setActive(true);
        group.setName(getAutoTestGroupName());
        group.setDescription(getAutoTestGroupDescription());

        Group updatedGroup = getImplementation(directory).updateGroup(group);
        Group foundGroup = getImplementation(directory).findGroupByName(getAutoTestGroupName());

        assertEquals(getImplementation(directory).getDirectoryId(), updatedGroup.getDirectoryId());
        assertEquals(group.getName(), updatedGroup.getName());

        assertEquals(getImplementation(directory).getDirectoryId(), foundGroup.getDirectoryId());
        assertEquals(group.getName(), foundGroup.getName());

        assertEquals(group.getDescription(), updatedGroup.getDescription());
        assertEquals(group.getDescription(), foundGroup.getDescription());
    }

    public void testUpdateGroupSetDescriptionToNull() throws Exception
    {
        logger.info("Running testUpdateGroupSetDescriptionToNull");

        Group addedGroup = addGroupWithDescription(getAutoTestGroupName(), getAutoTestGroupDescription());

        GroupTemplate group = new GroupTemplate(addedGroup);
        group.setActive(true);
        group.setName(getAutoTestGroupName());

        Group updatedGroup = getImplementation(directory).updateGroup(group);
        Group foundGroup = getImplementation(directory).findGroupByName(getAutoTestGroupName());

        assertEquals(getImplementation(directory).getDirectoryId(), updatedGroup.getDirectoryId());
        assertEquals(group.getName(), updatedGroup.getName());

        assertEquals(getImplementation(directory).getDirectoryId(), foundGroup.getDirectoryId());
        assertEquals(group.getName(), foundGroup.getName());

        assertEquals(group.getDescription(), updatedGroup.getDescription());
        assertEquals(group.getDescription(), foundGroup.getDescription());
    }


    public void testUpdateGroupUpdateDescription() throws Exception
    {
        logger.info("Running testUpdateGroupUpdateDescription");

        Group addedGroup = addGroupWithDescription(getAutoTestGroupName(), getAutoTestGroupDescription());

        GroupTemplate group = new GroupTemplate(addedGroup);
        group.setActive(true);
        group.setName(getAutoTestGroupName());
        group.setDescription(getAutoTestGroupDescription2());

        Group updatedGroup = getImplementation(directory).updateGroup(group);
        Group foundGroup = getImplementation(directory).findGroupByName(getAutoTestGroupName());

        assertEquals(getImplementation(directory).getDirectoryId(), updatedGroup.getDirectoryId());
        assertEquals(group.getName(), updatedGroup.getName());

        assertEquals(getImplementation(directory).getDirectoryId(), foundGroup.getDirectoryId());
        assertEquals(group.getName(), foundGroup.getName());

        assertEquals(group.getDescription(), updatedGroup.getDescription());
        assertEquals(group.getDescription(), foundGroup.getDescription());
    }

/*
 * RemoteDirectory.searchGroups()
 *
 */

/*
 * RemoteDirectory.removeGroup()
 *
 */

    public void testRemoveGroupNull() throws Exception
    {
        logger.info("Running testRemoveGroupNull");

        addGroup();

        try
        {
            getImplementation(directory).removeGroup(null);

            fail("Should have thrown NullPointerException");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testRemoveGroupEmptyString() throws Exception
    {
        logger.info("Running testRemoveGroupEmptyString");

        addGroup();

        try
        {
            getImplementation(directory).removeGroup("");
            fail("should have thrown IllegalArgumentException");
        }
        catch (IllegalArgumentException e)
        {
        }
    }

    public void testRemoveGroupNonExistent() throws Exception
    {
        logger.info("Running testRemoveGroupNonExistent");

        addGroup();

        try
        {
            getImplementation(directory).removeGroup("badger-badger-badger-SNAKE!-SNAAKE!-Oh-it's-a-snaaaaake");
        }
        catch (GroupNotFoundException e)
        {
        }
    }

    public void testRemoveGroup() throws Exception
    {
        logger.info("Running testRemoveGroup");

        addGroup();

        getImplementation(directory).removeGroup(getAutoTestGroupName());
    }

    /*
    * RemoteDirectory.searchUsers()
    *
    */
    public void testSearchUsersNull() throws Exception
    {
        logger.info("Running testSearchUsersNull");

        try
        {
            getImplementation(directory).searchUsers(null);

            fail("Should have thrown NullPointerException");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testSearchUsersEmptyContext() throws Exception
    {
        logger.info("Running testSearchUsersEmptyContext");

        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), getAutoTestFirstName(), getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword(), true);

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(String.class, EntityDescriptor.user()).returningAtMost(10));
        assertTrue(users.size() >= 1); // should have at least the user we've added.
    }

    public void testSearchUsersNameEmptyString() throws Exception
    {
        logger.info("Running testSearchUsersNameEmptyString");

        addUserAndAssertCreation(getAutoTestUserName(), getAutoTestDisplayName(), " ", getAutoTestLastName(), getAutoTestValidEmail(), getAutoTestPassword(), true);

        SearchContext searchContext = new SearchContext();
        searchContext.put(SearchContext.PRINCIPAL_NAME, " ");

        // user will get added with a generated first name (empty) as it's blank
        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(String.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.FIRST_NAME).exactlyMatching(" ")).returningAtMost(10));
        assertEquals(0, users.size());
    }

    public void testSearchUsersNameNonExistent() throws Exception
    {
        logger.info("Running testSearchUsersNameNonExistent");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.USERNAME).exactlyMatching("wibble-wobble-up-and-down")).returningAtMost(10));
        assertEquals(0, users.size());
    }

    public void testSearchUsersNameSingleMatch() throws Exception
    {
        logger.info("Running testSearchUsersNameSingleMatch");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.USERNAME).exactlyMatching(autoTestUserName)).returningAtMost(10));
        assertEquals(1, users.size());
    }

    public void testSearchUsersNameWildcardSingleMatch() throws Exception
    {
        logger.info("Running testSearchUsersNameWildcardSingleMatch");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addUserAndAssertCreation(autoTestUserName2, autoTestDisplayName, autoTestFirstName2, autoTestLastName, autoTestValidEmail2, autoTestPassword2, true);

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.USERNAME).startingWith("auto-t")).returningAtMost(10));
        assertEquals(1, users.size());
    }

    public void testSearchUsersNameWildcardDualMatch() throws Exception
    {
        logger.info("Running testSearchUsersNameWildcardDualMatch");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addUserAndAssertCreation(autoTestUserName2, autoTestDisplayName, autoTestFirstName2, autoTestLastName, autoTestValidEmail2, autoTestPassword2, true);

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.USERNAME).containing("auto")).returningAtMost(10));
        assertEquals(2, users.size());
    }

    // THIS DOES NOT WORK ON APACHEDS 1.5.4 AS BLANK EMAIL ADDRESSES ARE STORED AS NULL IN APACHEDS
//    public void testSearchUsersEmailEmptyString() throws  RemoteException, InvalidUserException, InvalidCredentialException, OperationFailedException
//    {
//        logger.info("Running testSearchUsersEmailEmptyString");
//
//        addUserAndAssertCreation(autoTestUserName, autoTestFirstName, autoTestLastName, " ", autoTestPassword, true);
//
//        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.EMAIL).exactlyMatching(" ")).returningAtMost(10));
//        assertEquals(1, users.size());
//    }

    public void testSearchUsersEmailNonExistent() throws Exception
    {
        logger.info("Running testSearchUsersEmailNonExistent");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.EMAIL).exactlyMatching("if-anyone-has.this_email@Ill_be_very.surprised.com")).returningAtMost(10));
        assertEquals(0, users.size());
    }

    public void testSearchUsersEmailSingleMatch() throws Exception
    {
        logger.info("Running testSearchUsersEmailSingleMatch");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.EMAIL).exactlyMatching(autoTestValidEmail)).returningAtMost(10));
        assertEquals(1, users.size());
    }

    public void testSearchUsersEmailWildcardSingleMatch() throws Exception
    {
        logger.info("Running testSearchUsersEmailWildcardSingleMatch");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addUserAndAssertCreation(autoTestUserName2, autoTestDisplayName, autoTestFirstName2, autoTestLastName, autoTestValidEmail2, autoTestPassword2, true);

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.EMAIL).startingWith("test.email")).returningAtMost(10));
        assertEquals(1, users.size());
    }

    public void testSearchUsersEmailWildcardDualMatch() throws Exception
    {
        logger.info("Running testSearchUsersEmailWildcardDualMatch");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addUserAndAssertCreation(autoTestUserName2, autoTestDisplayName, autoTestFirstName2, autoTestLastName, autoTestValidEmail2, autoTestPassword2, true);

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.EMAIL).containing("test-ad-example.com")).returningAtMost(10));
        assertEquals(2, users.size());
    }

    public void testSearchUsersFullnameEmptyString() throws Exception
    {
        addUserAndAssertCreation(autoTestUserName, null, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.DISPLAY_NAME).exactlyMatching("")).returningAtMost(10));
        assertEquals("No users should have been returned", 0, users.size());
    }

    public void testSearchUsersFullnameNonExistent() throws Exception
    {
        addUserAndAssertCreation(autoTestUserName, null, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.DISPLAY_NAME).exactlyMatching("My Auntie Married A Badger")).returningAtMost(10));
        assertEquals("No users should have been returned", 0, users.size());
    }

    public void testSearchUsersFullnameSingleMatch() throws Exception
    {
        User firstUser = addUserAndAssertCreation(autoTestUserName, null, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addUserAndAssertCreation(autoTestUserName2, null, autoTestFirstName2, autoTestLastName, autoTestValidEmail2, autoTestPassword2, true);

        List<String> users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(String.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.DISPLAY_NAME).exactlyMatching(firstUser.getDisplayName())).returningAtMost(10));
        assertEquals("One user should have been returned", 1, users.size());
        assertEquals("One user should have been returned", firstUser.getName(), users.get(0));
    }

    public void testSearchUsersFullnameWildcardSingleMatch() throws Exception
    {
        addUserAndAssertCreation(autoTestUserName, null, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addUserAndAssertCreation(autoTestUserName2, null, autoTestFirstName2, autoTestLastName, autoTestValidEmail2, autoTestPassword2, true);

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.DISPLAY_NAME).startingWith("Auto")).returningAtMost(10));
        assertEquals("One user should have been returned", 1, users.size());
    }

    public void testSearchUsersFullnameWildcardDualMatch() throws Exception
    {
        addUserAndAssertCreation(autoTestUserName, null, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addUserAndAssertCreation(autoTestUserName2, null, autoTestFirstName2, autoTestLastName, autoTestValidEmail2, autoTestPassword2, true);

        SearchContext searchContext = new SearchContext();
        searchContext.put(SearchContext.PRINCIPAL_FULLNAME, "*Auto*");

        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.DISPLAY_NAME).containing("Auto")).returningAtMost(10));
        assertEquals("Two users should have been returned", 2, users.size());
    }

    // Commented out due to us not supporting the 'active' 'in-active' concept ... yet
//    public void testSearchUsersActive() throws  RemoteException, InvalidUserException, InvalidCredentialException, OperationFailedException
//    {
//        addUserAndAssertCreation(autoTestUserName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
//        addUserAndAssertCreation(autoTestUserName2, autoTestFirstName2, autoTestLastName, autoTestValidEmail2, autoTestPassword2, false);
//
//        int totalUsers = getTotalUserCount();
//
//        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(EntityDescriptor.user()).with(Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(Boolean.TRUE)).returningAtMost(10));
//        assertTrue("Some active users should have been returned", users.size() > 0);
//        assertTrue("The number of active users (" + users.size() + ") should be less than the total number of users (" + totalUsers + ")", totalUsers > users.size());
//    }

    // Commented out due to us not supporting the 'active' 'in-active' concept ... yet
//    public void testSearchUsersInactive() throws  RemoteException, InvalidUserException, InvalidCredentialException, OperationFailedException
//    {
//        addUserAndAssertCreation(autoTestUserName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, false);
//        addUserAndAssertCreation(autoTestUserName2, autoTestFirstName2, autoTestLastName, autoTestValidEmail2, autoTestPassword2, true);
//
//        int totalUsers = getTotalUserCount();
//
//        List users = getImplementation(directory).searchUsers(QueryBuilder.queryFor(EntityDescriptor.user()).with(Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(Boolean.FALSE)).returningAtMost(10));
//        assertTrue("At least one inactive user should have been returned", users.size() > 0);
//        assertTrue("The number of inactive users (" + users.size() + ") should be less than the total number of users (" + totalUsers + ")", totalUsers > users.size());
//    }

    /*
     * RemoteDirectory.addUserToGroup()
     *
     */

    public void testAddUserToGroupBothNull() throws Exception
    {
        logger.info("Running testAddUserToGroupBothNull");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        addGroup();

        try
        {
            getImplementation(directory).addUserToGroup(null, null);

            fail("Should have thrown NullPointerException");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testAddUserToGroupNullAndEmpty1() throws Exception
    {
        logger.info("Running testAddUserToGroupNullAndEmpty1");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addGroup();

        try
        {
            getImplementation(directory).addUserToGroup(null, "");

            fail("Should have thrown NullPointerException");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testAddUserToGroupWithEmptyUsernameFails() throws Exception
    {
        logger.info("Running testAddUserToGroupNullAndEmpty2");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addGroup();
        try
        {
            getImplementation(directory).addUserToGroup("", "valid-group");

            fail("Should have thrown IllegalArgumentException");
        }
        catch (IllegalArgumentException e)
        {
        }
    }

    public void testAddUserToGroupBothEmpty() throws Exception
    {
        logger.info("Running testAddUserToGroupBothEmpty");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        addGroup();

        try
        {
            getImplementation(directory).addUserToGroup("", "");

            fail("Should have thrown IllegalArgumentException");
        }
        catch (IllegalArgumentException e)
        {
        }
    }

    public void testAddUserToGroupNonExistentUser() throws Exception
    {
        logger.info("Running testAddUserToGroupNonExistentUser");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        addGroup();

        try
        {
            getImplementation(directory).addUserToGroup("meeep-meep-moop", getAutoTestGroupName());

            fail("Should have thrown UserNotFoundException");
        }
        catch (UserNotFoundException e)
        {
        }
    }

    public void testAddUserToGroupNonExistentGroup() throws Exception
    {
        logger.info("Running testAddUserToGroupNonExistentGroup");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        addGroup();

        try
        {
            getImplementation(directory).addUserToGroup(getAutoTestUserName(), "meeep-meep-moop");

            fail("Should have thrown GroupNotFoundException");
        }
        catch (GroupNotFoundException e)
        {
        }
    }

    public void testAddUserToGroup() throws Exception
    {
        logger.info("Running testAddUserToGroup");

        addUserToGroup();

        List<String> groupList = getImplementation(directory).searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(getAutoTestUserName()).returningAtMost(10));


        assertNotNull("Group list should not be null", groupList);
        assertEquals("User should only belong to one group", 1, groupList.size());
        assertEquals("User should belong to the group " + getAutoTestGroupName(), getAutoTestGroupName(), groupList.get(0));
    }

    public void testAddUserToGroupTwice() throws Exception
    {
        logger.info("Running testAddUserToGroup");

        addUserToGroup();

        assertTrue(getImplementation(directory).isUserDirectGroupMember(getAutoTestUserName(), getAutoTestGroupName()));

        try
        {
            // again
            getImplementation(directory).addUserToGroup(getAutoTestUserName(), getAutoTestGroupName());
            fail("MembershipAlreadyExistsException expected");
        }
        catch (MembershipAlreadyExistsException e)
        {
            // expected
        }
    }

    public void testAddUserToTwoGroups() throws Exception
    {
        logger.info("Running testAddUserToTwoGroups");

        addUserToGroup();
        DirectoryTestHelper.addGroup(getAutoTestGroupName2(), directory.getId(), getRemoteDirectory());

        getImplementation(directory).addUserToGroup(getAutoTestUserName(), getAutoTestGroupName2());

        List<String> groupList = getImplementation(directory).searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(getAutoTestUserName()).returningAtMost(10));

        assertNotNull("Group list should not be null", groupList);
        assertEquals("User should belong to two groups", 2, groupList.size());
        assertTrue("User should belong to the group " + getAutoTestGroupName(), groupList.contains(getAutoTestGroupName()));
        assertTrue("User should belong to the group " + getAutoTestGroupName2(), groupList.contains(getAutoTestGroupName2()));
    }

    public void testAddGroupToGroupFailsWithEmptyChildGroupName() throws Exception
    {
        try
        {
            getImplementation(directory).addGroupToGroup("", "parent-group");
            fail();
        }
        catch (IllegalArgumentException e)
        {
            // Expected
        }
    }

    public void testAddGroupToGroupFailsWithEmptyParentGroupName() throws Exception
    {
        try
        {
            getImplementation(directory).addGroupToGroup("child-group", "");
            fail();
        }
        catch (IllegalArgumentException e)
        {
            // Expected
        }
    }

    public void testRemoveGroupToGroupFailsWithEmptyChildGroupName() throws Exception
    {
        try
        {
            getImplementation(directory).removeGroupFromGroup("", "parent-group");
            fail();
        }
        catch (IllegalArgumentException e)
        {
            // Expected
        }
    }

    public void testRemoveGroupToGroupFailsWithEmptyParentGroupName() throws Exception
    {
        try
        {
            getImplementation(directory).removeGroupFromGroup("child-group", "");
            fail();
        }
        catch (IllegalArgumentException e)
        {
            // Expected
        }
    }

/*
 * RemoteDirectory.removeUserFromGroup()
 *
 */

    public void testRemoveUserFromGroupBothNull() throws Exception
    {
        logger.info("Running testRemoveUserFromGroupBothNull");

        addUserToGroup();

        try
        {
            getImplementation(directory).removeUserFromGroup(null, null);

            fail("Should have thrown NullPointerException");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testRemoveUserFromGroupNullAndEmpty1() throws Exception
    {
        logger.info("Running testRemoveUserFromGroupNullAndEmpty1");

        addUserToGroup();

        try
        {
            getImplementation(directory).removeUserFromGroup(null, "");

            fail("Should have thrown NullPointerException");
        }
        catch (NullPointerException e)
        {
        }
    }

    public void testRemoveUserFromGroupNullAndEmpty2() throws Exception
    {
        logger.info("Running testRemoveUserFromGroupNullAndEmpty2");

        addUserToGroup();

        try
        {
            getImplementation(directory).removeUserFromGroup("", null);

            fail("Should have thrown IllegalArgumentException");
        }
        catch (IllegalArgumentException e)
        {
        }
    }

    public void testRemoveUserFromGroupBothEmpty() throws Exception
    {
        logger.info("Running testRemoveUserFromGroupBothEmpty");

        addUserToGroup();

        try
        {
            getImplementation(directory).removeUserFromGroup("", "");

            fail("Should have thrown IllegalArgumentException");
        }
        catch (IllegalArgumentException e)
        {
        }
    }

    public void testRemoveUserFromGroupNonExistentUser() throws Exception
    {
        logger.info("Running testRemoveUserFromGroupNonExistentUser");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addGroup();

        try
        {
            getImplementation(directory).removeUserFromGroup("meeep-meep-moop", getAutoTestGroupName());

            fail("Should have thrown UserNotFoundException");
        }
        catch (UserNotFoundException e)
        {
        }
    }

    public void testRemoveUserFromGroupNonExistentGroup() throws Exception
    {
        logger.info("Running testRemoveUserFromGroupNonExistentGroup");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addGroup();

        try
        {
            getImplementation(directory).removeUserFromGroup(getAutoTestUserName(), "meeep-meep-moop");

            fail("Should have thrown GroupNotFoundException");
        }
        catch (GroupNotFoundException e)
        {
        }
    }

    public void testRemoveUserFromGroupWhereThereIsNoMembership() throws Exception
    {
        logger.info("Running testRemoveUserFromGroup");

        DirectoryTestHelper.addGroup(getAutoTestGroupName2(), directory.getId(), getRemoteDirectory());

        addUserToGroup();

        try
        {
            getImplementation(directory).removeUserFromGroup(getAutoTestUserName(), getAutoTestGroupName2());
            fail("We should not have a membership and this call should have failed");
        }
        catch (MembershipNotFoundException e)
        {
            // success
        }

    }

    public void testRemoveUserFromGroup() throws Exception
    {
        logger.info("Running testRemoveUserFromGroup");

        addUserToGroup();

        getImplementation(directory).removeUserFromGroup(getAutoTestUserName(), getAutoTestGroupName());

        List<String> groupList = getImplementation(directory).searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(getAutoTestUserName()).returningAtMost(10));

        assertEquals("Group list should be empty", 0, groupList.size());
    }

/*
 * RemoteDirectory.findUserToGroupMemberships()
 *
 */

    public void testFindGroupMembershipsNull() throws Exception
    {
        logger.info("Running testFindGroupMembershipsNull");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        try
        {
            getImplementation(directory).searchGroupRelationships(null);

            fail("Should not have found memberships for null user");

        }
        catch (NullPointerException e)
        {
        }
    }


    public void testFindGroupMembershipsNoGroups() throws Exception
    {
        logger.info("Running testFindGroupMembershipsNoGroups");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);

        List<String> groups = getImplementation(directory).searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(getAutoTestUserName()).returningAtMost(10));
        assertNotNull(groups);
        assertEquals("User should not be a member of any groups", 0, groups.size());
    }

    public void testFindGroupMemberships() throws Exception
    {
        logger.info("Running testFindGroupMemberships");

        addUserAndAssertCreation(autoTestUserName, autoTestDisplayName, autoTestFirstName, autoTestLastName, autoTestValidEmail, autoTestPassword, true);
        addGroupWithDescription(getAutoTestGroupName(), null);
        addGroupWithDescription(getAutoTestGroupName2(), null);
        getImplementation(directory).addUserToGroup(getAutoTestUserName(), getAutoTestGroupName());
        getImplementation(directory).addUserToGroup(getAutoTestUserName(), getAutoTestGroupName2());


        List<String> groups = getImplementation(directory).searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(getAutoTestUserName()).returningAtMost(10));
        assertNotNull(groups);
        assertEquals("User should be a member of two groups", 2, groups.size());
        assertTrue("Groups should contain " + getAutoTestGroupName() + " and " + getAutoTestGroupName2(), groups.containsAll(Arrays.asList(getAutoTestGroupName(), getAutoTestGroupName2())));
    }

    public void testUpdateUserWithNullDisplayName() throws Exception
    {
        logger.info("Running testUpdateUserWithNullFirstNameAndLastName");

        User addedUser = addUserAndAssertCreation("auto-test-user", null, "Auto", "Test", "test.emailextension@test-ad-example.com", "My-test-password1", true);
        UserTemplate modifiedUser = new UserTemplate(addedUser);

        modifiedUser.setDisplayName(null);
        modifiedUser.setFirstName("Auto");
        modifiedUser.setLastName("Test");

        User updatedUser = getImplementation(directory).updateUser(modifiedUser);
        assertEquals("calculated displayName", "Auto Test", updatedUser.getDisplayName());
        assertEquals("specified first name", "Auto", updatedUser.getFirstName());
        assertEquals("specified last name", "Test", updatedUser.getLastName());
    }

    public void testUpdateUserWithNullFirstNameAndLastName() throws Exception
    {
        logger.info("Running testUpdateUserWithNullFirstNameAndLastName");

        User addedUser = addUserAndAssertCreation("auto-test-user", null, "Auto", "Test", "test.emailextension@test-ad-example.com", "My-test-password1", true);
        UserTemplate modifiedUser = new UserTemplate(addedUser);

        modifiedUser.setDisplayName("Auto Test"); // same as calculated display name
        modifiedUser.setFirstName(null);
        modifiedUser.setLastName(null);

        User updatedUser = getImplementation(directory).updateUser(modifiedUser);
        assertEquals("newly defined displayName", "Auto Test", updatedUser.getDisplayName());
        assertEquals("calculated first name", "Auto", updatedUser.getFirstName());
        assertEquals("calculated last name", "Test", updatedUser.getLastName());
    }

    public void testSynchroniseUserWithNoDisplayName() throws Exception
    {
        final SpringLDAPConnector ldapDirectory = (SpringLDAPConnector) getRawImplementation(directory);
        final SpringLDAPConnectorAccessor accessor = new SpringLDAPConnectorAccessor(ldapDirectory);

        // Add user in LDAP
        final UserTemplate userTemplate = new UserTemplate(getAutoTestUserName(), -1L);
        userTemplate.setActive(true);
        userTemplate.setDisplayName(getAutoTestDisplayName());
        userTemplate.setFirstName(getAutoTestFirstName());
        userTemplate.setLastName(getAutoTestLastName());
        userTemplate.setEmailAddress(getAutoTestValidEmail());
        ldapDirectory.addUser(userTemplate, null);

        // ...and change the display name to empty
        final LDAPUserWithAttributes currentUser = (LDAPUserWithAttributes) getRawImplementation(directory).findUserByName(getAutoTestUserName());
        final String displayNameAttributeKey = accessor.getLdapPropertiesMapper().getUserDisplayNameAttribute();
        final ModificationItem[] items = {new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(displayNameAttributeKey, " "))};
        accessor.getLdapTemplate().modifyAttributes(new LdapName(currentUser.getDn()), items);

        // synchronise the updated user in the internal directory
        final DbCachingRemoteDirectory internalDirectory = (DbCachingRemoteDirectory) getImplementation(directory);
        internalDirectory.synchroniseCache(SynchronisationMode.FULL, new MockSynchronisationStatusManager());

        // ...and check that display name has been constructed
        final User user = internalDirectory.findUserByName(getAutoTestUserName());
        assertEquals("Display name should have been constructed", getAutoTestFirstName() + " " + getAutoTestLastName(), user.getDisplayName());

        // synchronise the user again in the internal directory
        internalDirectory.synchroniseCache(SynchronisationMode.FULL, new MockSynchronisationStatusManager());

        // ...and check that nothing has been done
        final User refreshedUser = internalDirectory.findUserByName(getAutoTestUserName());
        assertTrue("User should not be updated between syncs", user == refreshedUser);
    }

    public void testSynchroniseUserWithNoLastName() throws Exception
    {
        final SpringLDAPConnector ldapDirectory = (SpringLDAPConnector) getRawImplementation(directory);
        final SpringLDAPConnectorAccessor accessor = new SpringLDAPConnectorAccessor(ldapDirectory);

        // Add user in LDAP
        final UserTemplate userTemplate = new UserTemplate(getAutoTestUserName(), -1L);
        userTemplate.setActive(true);
        userTemplate.setDisplayName("Bob Smith");
        userTemplate.setFirstName(getAutoTestFirstName());
        userTemplate.setLastName(getAutoTestLastName());
        userTemplate.setEmailAddress(getAutoTestValidEmail());
        ldapDirectory.addUser(userTemplate, null);

        // ...and change the last name to empty
        final LDAPUserWithAttributes currentUser = (LDAPUserWithAttributes) getRawImplementation(directory).findUserByName(getAutoTestUserName());
        final String lastNameAttributeKey = accessor.getLdapPropertiesMapper().getUserLastNameAttribute();
        final ModificationItem[] items = {new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(lastNameAttributeKey, " "))};
        accessor.getLdapTemplate().modifyAttributes(new LdapName(currentUser.getDn()), items);

        // synchronise the updated user in the internal directory
        final DbCachingRemoteDirectory internalDirectory = (DbCachingRemoteDirectory) getImplementation(directory);
        internalDirectory.synchroniseCache(SynchronisationMode.FULL, new MockSynchronisationStatusManager());

        // ...and check that last name has been constructed
        final User user = internalDirectory.findUserByName(getAutoTestUserName());
        assertEquals("Last name should have been constructed", "Smith", user.getLastName());

        // synchronise the user again in the internal directory
        internalDirectory.synchroniseCache(SynchronisationMode.FULL, new MockSynchronisationStatusManager());

        // ...and check that nothing has been done
        final User refreshedUser = internalDirectory.findUserByName(getAutoTestUserName());
        assertTrue("User should not be updated between syncs", user == refreshedUser);
    }

    public void testSynchroniseUserWithNoFirstName() throws Exception
    {
        final SpringLDAPConnector ldapDirectory = (SpringLDAPConnector) getRawImplementation(directory);
        final SpringLDAPConnectorAccessor accessor = new SpringLDAPConnectorAccessor(ldapDirectory);

        // Add user in LDAP
        final UserTemplate userTemplate = new UserTemplate(getAutoTestUserName(), -1L);
        userTemplate.setActive(true);
        userTemplate.setDisplayName("Bob Smith");
        userTemplate.setFirstName(getAutoTestFirstName());
        userTemplate.setLastName(getAutoTestLastName());
        userTemplate.setEmailAddress(getAutoTestValidEmail());
        ldapDirectory.addUser(userTemplate, null);

        // ...and change the first name to empty
        final LDAPUserWithAttributes currentUser = (LDAPUserWithAttributes) getRawImplementation(directory).findUserByName(getAutoTestUserName());
        final String firstNameAttributeKey = accessor.getLdapPropertiesMapper().getUserFirstNameAttribute();
        final ModificationItem[] items = {new ModificationItem(DirContext.REPLACE_ATTRIBUTE, new BasicAttribute(firstNameAttributeKey, " "))};
        accessor.getLdapTemplate().modifyAttributes(new LdapName(currentUser.getDn()), items);

        // synchronise the updated user in the internal directory
        final DbCachingRemoteDirectory internalDirectory = (DbCachingRemoteDirectory) getImplementation(directory);
        internalDirectory.synchroniseCache(SynchronisationMode.FULL, new MockSynchronisationStatusManager());

        // ...and check that first name has been constructed
        final User user = internalDirectory.findUserByName(getAutoTestUserName());
        assertEquals("First name should have been constructed", "Bob", user.getFirstName());

        // synchronise the user again in the internal directory
        internalDirectory.synchroniseCache(SynchronisationMode.FULL, new MockSynchronisationStatusManager());

        // ...and check that nothing has been done
        final User refreshedUser = internalDirectory.findUserByName(getAutoTestUserName());
        assertTrue("User should not be updated between syncs", user == refreshedUser);
    }

    public void testFindRoleMembershipsNoRoles() throws Exception
    {
        logger.info("Running testFindRoleMembershipsNoRoles");

        addUserWithDefaults();

        List<String> roles = getImplementation(directory).searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.LEGACY_ROLE)).parentsOf(EntityDescriptor.user()).withName(getAutoTestUserName()).returningAtMost(EntityQuery.ALL_RESULTS));
        assertNotNull(roles);
        assertEquals("User should not be a member of any roles", 0, roles.size());
    }

    protected String getAutoTestUserName()
    {
        return autoTestUserName;
    }

    protected String getAutoTestUserName2()
    {
        return autoTestUserName2;
    }

    protected String getAutoTestFirstName()
    {
        return autoTestFirstName;
    }

    protected String getAutoTestFirstName2()
    {
        return autoTestFirstName2;
    }

    protected String getAutoTestLastName()
    {
        return autoTestLastName;
    }

    protected String getAutoTestPassword()
    {
        return autoTestPassword;
    }

    protected String getAutoTestPassword2()
    {
        return autoTestPassword2;
    }

    protected String getAutoTestPassword3()
    {
        return autoTestPassword3;
    }

    protected String getAutoTestValidEmail()
    {
        return autoTestValidEmail;
    }

    protected String getAutoTestValidEmail2()
    {
        return autoTestValidEmail2;
    }

    protected String getAutoTestInvalidEmail()
    {
        return autoTestInvalidEmail;
    }

    protected String getAutoTestDisplayName()
    {
        return autoTestDisplayName;
    }

    protected String getAutoTestDisplayName2()
    {
        return autoTestDisplayName2;
    }

    public String getAutoTestDisplayName3()
    {
        return autoTestDisplayName3;
    }

    public String getAutoTestGroupName()
    {
        return autoTestGroupName;
    }

    public String getAutoTestGroupName2()
    {
        return autoTestGroupName2;
    }

    public String getAutoTestGroupDescription()
    {
        return autoTestGroupDescription;
    }

    public String getAutoTestGroupDescription2()
    {
        return autoTestGroupDescription2;
    }

    public String getAutoTestRoleName()
    {
        return autoTestRoleName;
    }

    public String getAutoTestRoleName2()
    {
        return autoTestRoleName2;
    }

    public String getAutoTestRoleDescription()
    {
        return autoTestRoleDescription;
    }

    public String getAutoTestRoleDescription2()
    {
        return autoTestRoleDescription2;
    }
}
