package com.atlassian.crowd.acceptance.tests.applications.crowd.legacy;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.directory.ApacheDS;
import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.security.password.DefaultPasswordEncoder;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.util.Arrays;

public abstract class BaseLegacyXmlRestoreTest extends CrowdAcceptanceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreCrowdFromXML("legacy/" + getLegacyXmlFileName());
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public abstract String getLegacyXmlFileName();

    public void testConsoleAdminLoginSuccessful()
    {
        log("Running testConsoleAdminLoginSuccessful");

        // force a logoff
        gotoPage("/console/logoff.action");

        // now try and login
        gotoPage("/console/login.action");

        setTextField("j_username", ADMIN_USER);
        setTextField("j_password", ADMIN_PW);
        submit();

        // assert that we are on the console page
        assertKeyPresent("console.welcome");
        assertTextPresent("Log Out");
    }

    public void testBrowseDirectories()
    {
        log("Running testBrowseDirectories");

        gotoBrowseDirectories();

        assertTextPresent("ApacheDS");
        assertTextPresent(ApacheDS.getStaticDirectoryType());

        assertTextPresent("Crowd Internal");
        assertTextPresent(InternalDirectory.DESCRIPTIVE_NAME);
    }

    public void testViewInternalDirectory()
    {
        log("Running testViewInternalDirectory");

        gotoBrowseDirectories();
        clickLinkWithExactText("Crowd Internal");

        assertKeyPresent("menu.viewdirectory.label", Arrays.asList("Crowd Internal"));
        assertTextFieldEquals("name", "Crowd Internal");
        assertTextFieldEquals("directoryDescription", "");
        assertCheckboxSelected("active");

        clickLink("internal-configuration");
        assertTextFieldEquals("passwordRegex", "");
        assertTextFieldEquals("passwordMaxAttempts", "0");
        assertTextFieldEquals("passwordMaxChangeTime", "0");
        assertTextFieldEquals("passwordHistoryCount", "0");
        assertTextFieldEquals("passwordRegex", "");

        clickLink("internal-permissions");
        assertCheckboxSelected("permissionPrincipalAdd");
        assertCheckboxSelected("permissionPrincipalModify");
        assertCheckboxSelected("permissionPrincipalRemove");
        assertCheckboxSelected("permissionGroupAdd");
        assertCheckboxSelected("permissionGroupModify");
        assertCheckboxSelected("permissionGroupRemove");

        gotoBrowseDirectories();
        viewDirectoryAsCustom("Crowd Internal");
        assertTextPresent("com.atlassian.crowd.directory.InternalDirectory");
    }

    private void viewDirectoryAsCustom(String directory)
    {
        int id = extractIdParameter(tester.getElementAttributeByXPath("//a[contains(text(), '" + directory + "')]", "href"));
        gotoPage("/console/secure/directory/viewcustom.action?ID=" + id);
    }

    private int extractIdParameter(String url)
    {
        final String[] queryParams = url.substring(url.indexOf('?') + 1).split("&");
        for (String queryParam : queryParams)
        {
            if (queryParam.startsWith("ID="))
            {
                return Integer.valueOf(queryParam.substring(3));
            }
        }
        fail("Should have found an ID in the query params");
        return 0;
    }

    public void testViewLDAPDirectory()
    {
        log("Running testViewLDAPDirectory");

        gotoBrowseDirectories();
        clickLinkWithExactText("ApacheDS");

        assertKeyPresent("menu.viewdirectory.label", Arrays.asList("ApacheDS"));
        assertTextFieldEquals("name", "ApacheDS");
        assertTextFieldEquals("directoryDescription", "");
        assertCheckboxSelected("active");

        clickLink("connector-connectiondetails");
        assertTextFieldEquals("URL", "ldap://localhost:11389/");
        assertCheckboxNotSelected("secure");
        assertCheckboxNotSelected("referral");
        assertCheckboxNotSelected("useNestedGroups");
        assertCheckboxNotSelected("useUserMembershipAttribute");
        assertCheckboxNotSelected("pagedResults");
        assertTextFieldEquals("pagedResultsSize", "");
        assertTextFieldEquals("baseDN", "dc=example,dc=com");
        assertTextFieldEquals("userDN", "uid=admin,ou=system");
        assertTextFieldEquals("ldapPassword", "");

        clickLink("connector-configuration");
        assertTextFieldEquals("groupDNaddition", "");
        assertTextFieldEquals("groupObjectClass", "groupOfUniqueNames");
        assertTextFieldEquals("groupObjectFilter", "(objectclass=groupOfUniqueNames)");
        assertTextFieldEquals("groupNameAttr", "cn");
        assertTextFieldEquals("groupDescriptionAttr", "description");
        assertTextFieldEquals("groupMemberAttr", "uniqueMember");
        assertCheckboxNotPresent("rolesDisabled");
        assertElementNotPresent("roleDNaddition");
        assertElementNotPresent("roleObjectClass");
        assertElementNotPresent("roleObjectFilter");
        assertElementNotPresent("roleNameAttr");
        assertElementNotPresent("roleDescriptionAttr");
        assertElementNotPresent("roleMemberAttr");
        assertTextFieldEquals("userDNaddition", "");
        assertTextFieldEquals("userObjectClass", "inetorgperson");
        assertTextFieldEquals("userObjectFilter", "(objectclass=inetorgperson)");
        assertTextFieldEquals("userNameAttr", "cn");
        assertTextFieldEquals("userNameRdnAttr", "cn");
        assertTextFieldEquals("userFirstnameAttr", "givenname");
        assertTextFieldEquals("userLastnameAttr", "sn");
        assertTextFieldEquals("userDisplayNameAttr", "displayName");
        assertTextFieldEquals("userMailAttr", "mail");
        assertTextFieldEquals("userGroupMemberAttr", "memberOf");
        assertTextFieldEquals("userPasswordAttr", "userpassword");


        clickLink("connector-permissions");
        assertCheckboxSelected("permissionPrincipalAdd");
        assertCheckboxSelected("permissionPrincipalModify");
        assertCheckboxSelected("permissionPrincipalRemove");
        assertCheckboxSelected("permissionGroupAdd");
        assertCheckboxSelected("permissionGroupModify");
        assertCheckboxSelected("permissionGroupRemove");

        gotoBrowseDirectories();
        viewDirectoryAsCustom("ApacheDS");
        assertTextPresent("com.atlassian.crowd.directory.ApacheDS");
    }

    public void testBrowseApplications()
    {
        log("Running testBrowseApplications");

        gotoBrowseApplications();

        assertTextInTable("application-table", new String[]{"crowd", "Crowd Console", "View"});
        assertTextInTable("application-table", new String[]{"demo", "Crowd Demo Application", "View"});
    }

    public void testViewCrowdApplication()
    {
        log("Running testViewCrowdApplication");

        gotoBrowseApplications();

        clickLinkWithExactText("crowd");

        assertTextInElement("name", "crowd");
        assertTextFieldEquals("applicationDescription", "Crowd Console");
        assertCheckboxSelected("active");

        clickLink("application-directories");
        assertTextInTable("directoriesTable", new String[]{"Crowd Internal"});
        setWorkingForm("directoriesForm");
        assertSelectedOptionEquals("directory1-allowAll", "False");

        clickLink("application-groups");
        assertTextInElement("groupsTable", "Crowd Internal - crowd-administrators");
        assertTextPresent("crowd-administrators");

        clickLink("application-permissions");
        assertKeyPresent("application.permission.text");
        setWorkingForm("permissionForm");
        String[] selectOptions = getDialog().getSelectOptionValues("directory-select");
        selectOptionByValue("directory-select", selectOptions[0]);
        setWorkingForm("permissionForm");
        assertCheckboxSelected("CREATE_GROUP");
        assertCheckboxSelected("CREATE_USER");
        assertCheckboxNotPresent("CREATE_ROLE");
        assertCheckboxSelected("UPDATE_GROUP");
        assertCheckboxSelected("UPDATE_USER");
        assertCheckboxNotPresent("UPDATE_ROLE");
        assertCheckboxSelected("DELETE_GROUP");
        assertCheckboxSelected("DELETE_USER");
        assertCheckboxNotPresent("DELETE_ROLE");

        clickLink("application-remoteaddress");
        assertTextInTable("addressesTable", new String[]{"localhost"});
        assertTextInTable("addressesTable", new String[]{"127.0.0.1"});

        clickLink("application-options");
        setWorkingForm("applicationOptions");
        assertCheckboxNotSelected("lowerCaseOutput");
    }

    public void testViewDemoApplication()
    {
        log("Running testViewDemoApplication");

        gotoBrowseApplications();

        clickLinkWithExactText("demo");

        assertTextInElement("name", "demo");
        assertTextFieldEquals("applicationDescription", "Crowd Demo Application");
        assertCheckboxSelected("active");

        clickLink("application-directories");
        assertTextInTable("directoriesTable", new String[]{"Crowd Internal"});
        setWorkingForm("directoriesForm");
        assertSelectedOptionEquals("directory1-allowAll", "True");

        clickLink("application-groups");
        assertTextInElement("groupsTable", "Crowd Internal - crowd-administrators");

        clickLink("application-permissions");
        assertKeyPresent("application.permission.text");
        setWorkingForm("permissionForm");
        String[] selectOptions = getDialog().getSelectOptionValues("directory-select");
        selectOptionByValue("directory-select", selectOptions[0]);
        setWorkingForm("permissionForm");
        assertCheckboxSelected("CREATE_GROUP");
        assertCheckboxSelected("CREATE_USER");
        assertCheckboxNotPresent("CREATE_ROLE");
        assertCheckboxSelected("UPDATE_GROUP");
        assertCheckboxSelected("UPDATE_USER");
        assertCheckboxNotPresent("UPDATE_ROLE");
        assertCheckboxSelected("DELETE_GROUP");
        assertCheckboxSelected("DELETE_USER");
        assertCheckboxNotPresent("DELETE_ROLE");

        clickLink("application-remoteaddress");
        assertTextInTable("addressesTable", new String[]{"localhost"});
        assertTextInTable("addressesTable", new String[]{"127.0.0.1"});

        clickLink("application-options");
        setWorkingForm("applicationOptions");
        assertCheckboxNotSelected("lowerCaseOutput");
    }

    public void testBrowseUsers()
    {
        log("Running testBrowseUsers");

        gotoBrowsePrincipals();

        setWorkingForm("searchusers");
        selectOption("directoryID", "Crowd Internal");

        submit();

        assertUserInTable("admin", "Super Admin", "admin@example.com");
        assertUserInTable("user", "Super User", "user@example.com");
    }

    public void testViewUser()
    {
        log("Running testViewUser");

        gotoViewPrincipal("admin", "Crowd Internal");

        assertTextFieldEquals("email", "admin@example.com");
        assertCheckboxSelected("active");
        assertTextFieldEquals("firstname", "Super");
        assertTextFieldEquals("lastname", "Admin");

        // Go to Attributes Tab
        clickLink("user-attributes-tab");
        assertTextInTable("attributesTable", new String[]{"passwordLastChanged"});
        assertTextInTable("attributesTable", new String[]{"invalidPasswordAttempts"});
        assertTextInTable("attributesTable", new String[]{"lastAuthenticated"});
        assertTextInTable("attributesTable", new String[]{"requiresPasswordChange"});
        setWorkingForm("attributesForm");
        assertTextFieldEquals("invalidPasswordAttempts1", "0");
        assertTextFieldEquals("requiresPasswordChange1", "false");

        // Go to Groups Tab
        clickLink("user-groups-tab");
        assertTextInTable("groupsTable", new String[]{"crowd-administrators"});
    }

    public void testBrowseGroups()
    {
        log("Running testBrowseGroups");

        gotoBrowseGroups();

        setWorkingForm("browsegroups");
        selectOption("directoryID", "Crowd Internal");

        submit();

        assertTextInTable("group-details", new String[]{"crowd-administrators", "true", "View"});
    }

    public void testViewGroup()
    {
        log("Running testViewGroup");

        gotoViewGroup("crowd-administrators", "Crowd Internal");

        setWorkingForm("groupForm");
        assertTextFieldEquals("description", "");
        assertCheckboxSelected("active");

        clickLink("view-group-users");

        assertTextInTable("view-group-users", new String[]{"admin", "admin@example.com", "true"});
    }

    public void testGeneralOptions()
    {
        log("Running testGeneralOptions");

        gotoGeneral();
        setWorkingForm("general");
        assertTextFieldEquals("title", "Atlassian Crowd");
        assertTextFieldEquals("domain", "");
        assertCheckboxNotSelected("secureCookie");
        assertCheckboxSelected("cachingEnabled");
        assertCheckboxSelected("gzip");
    }

    public void testLicenseScreen()
    {
        log("Running testLicenseScreen");

        gotoLicensing();

        // Only assert the SID since this is the only piece of data that is exported/imported
        assertTextPresent("ARVO-O3PS-SFJ8-ZDP9");
    }

    public void testMailConfiguration()
    {
        log("Running testMailConfiguration");

        gotoMailServer();

        setWorkingForm("mailserver");
        assertTextFieldEquals("notificationEmail", "admin@example.com");
        assertTextFieldEquals("from", "admin@example.com");
        assertTextFieldEquals("prefix", "[Atlassian Crowd - Atlassian Crowd]");
        assertRadioOptionSelected("jndiMailActive", "false");
        assertTextFieldEquals("host", "localhost");
        assertTextFieldEquals("port", "25");
        assertTextFieldEquals("username", "");
        assertTextFieldEquals("password", "");
    }

    public void testMailTemplate()
    {
        log("Running testMailTemplate");

        gotoMailTemplate();

        setWorkingForm("mailtemplate");
        assertTextFieldEquals("forgottenPasswordTemplate", "Hello $firstname $lastname, Your password has been reset by a $deploymenttitle administrator at $date. Your new password is: $password $deploymenttitle Administrator");
    }

    public void testSessionConfig()
    {
        log("Running testSessionConfig");

        gotoSessionConfig();

        setWorkingForm("session");
        assertTextFieldEquals("sessionTime", "30");
        assertRadioOptionSelected("storageType", "database");
    }

    /**
     * Ensures that display names are not blank after an upgrade.
     *
     * @throws Exception
     */
    public void testDisplayNameUpdated() throws Exception
    {
        log("Running testDisplayNameUpdated");

        String displayName = backupAndRetrieveXPath("//user[name='blank']/displayName/text()");

        assertEquals("DisplayName has not been updated from empty to username", "blank", displayName);
    }

    /**
     * Ensures that last names are not blank after an upgrade.
     *
     * @throws Exception
     */
    public void testLastNameUpdated() throws Exception
    {
        log("Running testLastNameUpdated");

        String lastName = backupAndRetrieveXPath("//user[name='blank']/lastName/text()");

        assertEquals("Last name has not been updated from empty to username", "blank", lastName);
    }

    /**
     * Ensures that passwords are lazily updated to the Atlassian Security form after an upgrade.
     *
     * @throws Exception
     */
    public void testPasswordUpgraded() throws Exception
    {
        log("Running testPasswordUpgraded");

        // We have already logged in as admin, so the password for admin user should have been re-encoded.
        String encPass = backupAndRetrieveXPath("//user[name='admin']/credential/text()");

        // Ensure password has been re-encoded with the DefaultPasswordEncoder
        assertTrue("SHA1 encoded password has not been updated", DefaultPasswordEncoder.getDefaultInstance().canDecodePassword(encPass));
    }

    protected String backupAndRetrieveXPath(String expr) throws Exception
    {
        String backupFileName = "BaseLegacyXmlRestoreTest_TempBackupFile.xml";
        String backupFilePath = getCrowdHome() + File.separator + "backups" + File.separator + backupFileName;

        gotoBackup();

        setTextField("exportFileName", backupFileName);

        submit();

        // can't use keys since can't assert the time taken
        assertTextPresent("Crowd has successfully backed up to " + backupFilePath);

        final File fileLocation = new File(backupFilePath);
        try
        {
            DocumentBuilderFactory domFactory =
            DocumentBuilderFactory.newInstance();
                  domFactory.setNamespaceAware(true);
            DocumentBuilder builder = domFactory.newDocumentBuilder();
            Document doc = builder.parse(fileLocation);

            XPathFactory factory = XPathFactory.newInstance();
            XPath xpath = factory.newXPath();

            return xpath.evaluate(expr, doc);
        }
        finally
        {
            //delete the file
            if (!fileLocation.delete())
            {
                fileLocation.deleteOnExit();
            }
        }
    }
}
