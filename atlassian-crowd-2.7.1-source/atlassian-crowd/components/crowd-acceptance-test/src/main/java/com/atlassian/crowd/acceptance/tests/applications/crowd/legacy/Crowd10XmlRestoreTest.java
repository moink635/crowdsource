package com.atlassian.crowd.acceptance.tests.applications.crowd.legacy;

public class Crowd10XmlRestoreTest extends BaseLegacyXmlRestoreTest
{
    public String getLegacyXmlFileName()
    {
        return "1_0_7_111.xml";
    }

    @Override
    public void testPasswordUpgraded() throws Exception
    {
        // Crowd 1.0 uses DES passwords, which are not upgraded.
    }
}

