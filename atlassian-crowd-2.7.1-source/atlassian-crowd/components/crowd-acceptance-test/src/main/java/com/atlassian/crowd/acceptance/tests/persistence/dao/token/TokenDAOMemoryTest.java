package com.atlassian.crowd.acceptance.tests.persistence.dao.token;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import com.atlassian.crowd.dao.token.TokenDAO;
import com.atlassian.crowd.dao.token.TokenDAOMemory;
import com.atlassian.crowd.manager.cache.CacheManagerEhcache;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;

import com.google.common.collect.ImmutableList;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import net.sf.ehcache.CacheManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml",
    "classpath:/applicationContext-CrowdEncryption.xml"
})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class TokenDAOMemoryTest extends TokenDAOTester
{
    private TokenDAOMemory tokenDAO; // not injected, constructed by this class
    @Inject private CacheManager cacheManager;

    public static Iterable<Token> getTestTokens() throws ParseException
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Token token1 = new Token.Builder(1L, "admin", "blah", 1234567890L, "oFpXcZ2lfvtc0YQBgE1EeA00")
            .setCreatedDate(sdf.parse("25-01-2009 11:00:30"))
            .setLastAccessedTime(1L)
            .create();
        Token token2 = new Token.Builder(1L, "user", "bleh", 0L, "oFpScZ2lfvtc0YQBgE1EeA00")
            .setCreatedDate(sdf.parse("25-01-2009 11:22:30"))
            .setLastAccessedTime(TimeUnit.MINUTES.toMillis(30))
            .create();
        long directoryId = -1L;
        TokenLifetime validFor15Minutes = TokenLifetime.inSeconds(TimeUnit.MINUTES.toSeconds(15));
        Token token3 = new Token.Builder(directoryId, "crowd", "bloh", 123456789012345L, "dHFiDosttLVTLlTKc08BEw00")
            .setCreatedDate(sdf.parse("25-01-2009 11:50:30"))
            .setLastAccessedTime(TimeUnit.MINUTES.toMillis(30))
            .setLifetime(validFor15Minutes)
            .create();

        return ImmutableList.of(token1, token2, token3);
    }

    @Before
    public void createObjectUnderTest() throws Exception
    {
        // Set up an internal cache
        CacheManagerEhcache cacheManagerEhcache = new CacheManagerEhcache(cacheManager);
        for (Token t : getTestTokens())
        {
            cacheManagerEhcache.put(TokenDAOMemory.RANDOM_HASH_CACHE, t.getRandomHash(), t);
            cacheManagerEhcache.put(TokenDAOMemory.IDENTIFIER_HASH_CACHE, t.getIdentifierHash(), t);
        }

        tokenDAO = new TokenDAOMemory(cacheManagerEhcache);
    }

    @After
    public void clearCache() throws Exception
    {
        // clean up the cache
        cacheManager.clearAll();
    }

    @Override
    protected TokenDAO getTokenDAO()
    {
        return tokenDAO;
    }
}
