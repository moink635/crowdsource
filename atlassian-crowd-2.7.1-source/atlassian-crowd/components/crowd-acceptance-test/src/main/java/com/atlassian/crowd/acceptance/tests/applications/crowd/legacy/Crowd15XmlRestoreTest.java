package com.atlassian.crowd.acceptance.tests.applications.crowd.legacy;

public class Crowd15XmlRestoreTest extends BaseLegacyXmlRestoreTest
{
    public String getLegacyXmlFileName()
    {
        return "1_5_2_342.xml";
    }
}
