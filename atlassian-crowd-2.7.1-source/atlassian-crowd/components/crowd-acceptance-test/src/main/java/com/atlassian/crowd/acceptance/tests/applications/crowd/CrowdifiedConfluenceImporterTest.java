package com.atlassian.crowd.acceptance.tests.applications.crowd;

public class CrowdifiedConfluenceImporterTest extends AppImporterTestBase
{
    @Override
    String getHsqlFileName()
    {
        return "confluencedb-3.5-m4";
    }

    public void testImportConfluenceUsers()
    {
        runImportTest("Confluence", 1, 3, 2);
    }
}
