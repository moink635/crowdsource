package com.atlassian.crowd.acceptance.tests.persistence.dao.group;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.embedded.spi.GroupDao;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.group.InternalDirectoryGroup;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.atlassian.crowd.model.group.GroupType.GROUP;
import static com.atlassian.crowd.model.group.GroupType.LEGACY_ROLE;
import static com.atlassian.crowd.model.group.Groups.namesOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Integration tests for the CRUD operations of {@link GroupDao}. For convenience, the tests
 * of this interface have been split into multiple test classes.
 *
 * @see GroupDaoSearchTest
 * @see GroupDAOHibernateTest
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class GroupDaoCRUDTest
{
    @Inject private GroupDao groupDAO;
    @Inject private MembershipDao membershipDAO;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;
    @Inject private DataSource dataSource;

    private static final long DIRECTORY_ID = 1L;
    private static final String EXISTING_GROUP_1 = "administratorS";
    private static final String EXISTING_GROUP_1_DESC = "The Administrator Group";
    private static final String EXISTING_GROUP_2 = "users";
    private static final String NON_EXISTING_GROUP = "NewGroup";
    private static final String COUNTRY_ATTRIBUTE = "country";
    private static final String LANGUAGE_ATTRIBUTE = "language";
    private static final String NON_EXISTING_GROUP_DESC = "New Group";
    private static final String UPDATED_DESCRIPTION = "Updated Description";
    private static final String COUNTRY_VALUE_NZ = "New Zealand";
    private static final String COUNTRY_VALUE_AU = "Australia";
    private static final String LANGUAGE_ATTRIBUTE_VALUE = "Sheepish";

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Test
    public void testFindByName() throws Exception
    {
        InternalDirectoryGroup group = groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_1);

        assertEquals(EXISTING_GROUP_1, group.getName());
        assertEquals(EXISTING_GROUP_1_DESC, group.getDescription());
        assertEquals(GROUP, group.getType());
        assertTrue(group.isActive());
        assertTrue(group.getCreatedDate().before(new Date()));
        assertTrue(group.getUpdatedDate().before(new Date()));
        assertEquals(DIRECTORY_ID, group.getDirectoryId());
    }

    @Test
    public void testFindByNameMixedCase() throws Exception
    {
        InternalDirectoryGroup group = groupDAO.findByName(2L, "teSterS");

        assertEquals("Testers", group.getName());
        assertEquals("The Crowd Tester Crew", group.getDescription());
        assertEquals(GROUP, group.getType());
        assertTrue(group.isActive());
        assertTrue(group.getCreatedDate().before(new Date()));
        assertTrue(group.getUpdatedDate().before(new Date()));
        assertEquals(2L, group.getDirectoryId());
    }

    @Test (expected = GroupNotFoundException.class)
    public void testFindByNameDoesNotExist() throws Exception
    {
        groupDAO.findByName(DIRECTORY_ID, NON_EXISTING_GROUP);
    }

    @Test
    public void testFindByNameWithAttributes() throws Exception
    {
        GroupWithAttributes group = groupDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_GROUP_1);

        assertEquals(EXISTING_GROUP_1, group.getName());
        assertTrue(group.isActive());
        assertEquals(EXISTING_GROUP_1_DESC, group.getDescription());
        assertEquals(DIRECTORY_ID, group.getDirectoryId());

        assertEquals(2, group.getKeys().size());
        assertTrue(group.getKeys().contains(COUNTRY_ATTRIBUTE));
        assertTrue(group.getKeys().contains(LANGUAGE_ATTRIBUTE));

        assertEquals(2, group.getValues(COUNTRY_ATTRIBUTE).size());
        assertTrue(group.getValues(COUNTRY_ATTRIBUTE).contains(COUNTRY_VALUE_NZ));
        assertTrue(group.getValues(COUNTRY_ATTRIBUTE).contains(COUNTRY_VALUE_AU));

        assertEquals(1, group.getValues(LANGUAGE_ATTRIBUTE).size());
        assertEquals(LANGUAGE_ATTRIBUTE_VALUE, group.getValue(LANGUAGE_ATTRIBUTE));
    }

    @Test
    public void testFindByMixedCaseNameWithAttributes() throws Exception
    {
        GroupWithAttributes group = groupDAO.findByNameWithAttributes(2L, "TestErs");

        assertEquals("Testers", group.getName());
        assertTrue(group.isActive());
        assertEquals("The Crowd Tester Crew", group.getDescription());
        assertEquals(2L, group.getDirectoryId());

        assertEquals(1, group.getKeys().size());
        assertTrue(group.getKeys().contains("city"));
        assertEquals(1, group.getValues("city").size());
        assertTrue(group.getValues("city").contains("Perth"));
    }

    @Test (expected = GroupNotFoundException.class)
    public void testFindByNameWithAttributesWhenGroupsDoesNotExist() throws Exception
    {
        groupDAO.findByNameWithAttributes(DIRECTORY_ID, NON_EXISTING_GROUP);
    }

    @Test
    public void testAdd() throws Exception
    {
        Group createdGroup = createNewGroup(NON_EXISTING_GROUP);

        assertNotNull(createdGroup);

        // find
        Group foundGroup = groupDAO.findByName(DIRECTORY_ID, NON_EXISTING_GROUP.toUpperCase());
        assertEquals(NON_EXISTING_GROUP, foundGroup.getName());
        assertEquals(true, foundGroup.isActive());
        assertEquals(NON_EXISTING_GROUP_DESC, foundGroup.getDescription());
        assertEquals(GROUP, foundGroup.getType());
    }

    @Test (expected = InvalidGroupException.class)
    public void testAddWhenGroupAlreadyExists() throws Exception
    {
        createNewGroup(EXISTING_GROUP_1);
    }

    @Test (expected = InvalidGroupException.class)
    public void testAddWhenGroupAlreadyExistsWithMixedCase() throws Exception
    {
        createNewGroup("AdministratorS");
    }

    @Test
    public void testAddLocal() throws Exception
    {
        Group createdGroup = createNewLocalGroup(NON_EXISTING_GROUP);

        assertNotNull(createdGroup);

        // find
        Group foundGroup = groupDAO.findByName(DIRECTORY_ID, NON_EXISTING_GROUP.toUpperCase());
        assertEquals(NON_EXISTING_GROUP, foundGroup.getName());
        assertEquals(true, foundGroup.isActive());
        assertEquals(NON_EXISTING_GROUP_DESC, foundGroup.getDescription());
        assertEquals(GROUP, foundGroup.getType());
    }

    @Test (expected = InvalidGroupException.class)
    public void testAddLocalWhenGroupAlreadyExists() throws Exception
    {
        createNewLocalGroup(EXISTING_GROUP_1);
    }

    @Test (expected = InvalidGroupException.class)
    public void testAddLocalWhenGroupAlreadyExistsWithMixedCase() throws Exception
    {
        createNewLocalGroup("AdministratorS");
    }

    @Test
    public void testUpdate() throws Exception
    {
        Group group = groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_1);

        GroupTemplate groupTemplate = new GroupTemplate(group);
        groupTemplate.setActive(false);
        groupTemplate.setDescription(UPDATED_DESCRIPTION);

        groupDAO.update(groupTemplate);

        // find
        Group foundGroup = groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_1);
        assertEquals(EXISTING_GROUP_1, foundGroup.getName());
        assertEquals(false, foundGroup.isActive());
        assertEquals(UPDATED_DESCRIPTION, foundGroup.getDescription());
        assertEquals(GROUP, foundGroup.getType());
    }

    @Test
    public void testUpdateTypeChangeFail() throws Exception
    {
        Group group = groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_1);

        GroupTemplate groupTemplate = new GroupTemplate(group);
        groupTemplate.setActive(false);
        groupTemplate.setDescription(UPDATED_DESCRIPTION);
        groupTemplate.setType(LEGACY_ROLE);

        try
        {
            groupDAO.update(groupTemplate);
            fail("UnsupportedOperationException expected");
        }
        catch (UnsupportedOperationException e)
        {
            // expected
        }
    }

    @Test
    public void testUpdateWhenGroupDoesNotExist() throws Exception
    {
        // find an existing group
        Group group = groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_1);

        // then remove it
        groupDAO.remove(group);

        try
        {
            groupDAO.update(group);
            fail("GroupNotFoundException expected");
        }
        catch (GroupNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testRename() throws Exception
    {
        Group group = groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_1);
        groupDAO.rename(group, NON_EXISTING_GROUP);

        try
        {
            groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_1);
        }
        catch (GroupNotFoundException e)
        {
            // expected
        }

        Group renamedGroup = groupDAO.findByName(DIRECTORY_ID, NON_EXISTING_GROUP);
        assertEquals(NON_EXISTING_GROUP, renamedGroup.getName());

        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(EXISTING_GROUP_1).returningAtMost(10)).size());
        assertEquals(1, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(NON_EXISTING_GROUP.toUpperCase()).returningAtMost(10)).size());

        List<Group> groups = membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("admin").returningAtMost(10));
        assertThat(namesOf(groups), hasItem(NON_EXISTING_GROUP));

        List<String> groupNames = membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("admin").returningAtMost(10));
        assertThat(groupNames, hasItem(NON_EXISTING_GROUP));
    }

    @Test
    public void testRenameWhenGroupDoesNotExist() throws Exception
    {
        // find an existing group
        Group group = groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_1);

        // then remove it
        groupDAO.remove(group);

        try
        {
            groupDAO.rename(group, "new name");
            fail("GroupNotFoundException expected");
        }
        catch (GroupNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testRenameWhenThereIsANameClash() throws Exception
    {
        // find an existing group
        Group group = groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_1);

        try
        {
            groupDAO.rename(group, EXISTING_GROUP_2);
            fail("InvalidGroupException expected");
        }
        catch (InvalidGroupException e)
        {
            // expected
        }
    }

    @Test
    public void testRemove() throws Exception
    {
        Group group = groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_2);

        groupDAO.remove(group);

        try
        {
            groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_2);
        }
        catch (GroupNotFoundException e)
        {
            // expected
        }

        assertTrue(membershipDAO.search(DIRECTORY_ID,
                                        QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                                            .childrenOf(EntityDescriptor.group())
                                            .withName(EXISTING_GROUP_2)
                                            .returningAtMost(10)).isEmpty());
        assertTrue(membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(EXISTING_GROUP_2).returningAtMost(10)).isEmpty());
        assertTrue(membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.group()).withName(EXISTING_GROUP_2).returningAtMost(10)).isEmpty());
    }

    @Test
    public void testRemoveWhenGroupDoesNotExist() throws Exception
    {
        // find existing group
        GroupWithAttributes group = groupDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_GROUP_1);

        groupDAO.remove(group);  // should succeed

        try
        {
            groupDAO.remove(group);
            fail("GroupNotFoundException expected");
        }
        catch (GroupNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testStoreAttributesWithUpdateAndInsertForExistingGroup() throws Exception
    {
        // find existing group with attributes
        GroupWithAttributes group = groupDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_GROUP_1);

        assertEquals(2, group.getKeys().size());
        assertTrue(group.getKeys().contains(COUNTRY_ATTRIBUTE));
        assertTrue(group.getKeys().contains(LANGUAGE_ATTRIBUTE));

        assertEquals(2, group.getValues(COUNTRY_ATTRIBUTE).size());
        assertTrue(group.getValues(COUNTRY_ATTRIBUTE).contains(COUNTRY_VALUE_NZ));
        assertTrue(group.getValues(COUNTRY_ATTRIBUTE).contains(COUNTRY_VALUE_AU));

        assertEquals(1, group.getValues(LANGUAGE_ATTRIBUTE).size());
        assertEquals(LANGUAGE_ATTRIBUTE_VALUE, group.getValue(LANGUAGE_ATTRIBUTE));

        // add/update attributes (update country and add phone)
        Map<String, Set<String>> attributes = new HashMap<String, Set<String>>();
        attributes.put(COUNTRY_ATTRIBUTE, Sets.newHashSet(COUNTRY_VALUE_NZ, "UK", "U.S.A"));
        attributes.put("phone", Sets.newHashSet("131111"));
        groupDAO.storeAttributes(group, attributes);

        // retrieve and verify
        GroupWithAttributes returnedGroup = groupDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_GROUP_1.toUpperCase());
        assertEquals(3, returnedGroup.getKeys().size());
        assertEquals(3, returnedGroup.getValues(COUNTRY_ATTRIBUTE).size());
        assertTrue(returnedGroup.getValues(COUNTRY_ATTRIBUTE).contains(COUNTRY_VALUE_NZ));
        assertTrue(returnedGroup.getValues(COUNTRY_ATTRIBUTE).contains("UK"));
        assertTrue(returnedGroup.getValues(COUNTRY_ATTRIBUTE).contains("U.S.A"));
        assertEquals(1, returnedGroup.getValues("phone").size());
        assertTrue(returnedGroup.getValues("phone").contains("131111"));
        assertEquals(1, group.getValues(LANGUAGE_ATTRIBUTE).size());
        assertEquals(LANGUAGE_ATTRIBUTE_VALUE, group.getValue(LANGUAGE_ATTRIBUTE));
    }

    @Test
    public void testStoreAttributesWhenGroupDoesNotExist() throws Exception
    {
        // find existing group
        GroupWithAttributes group = groupDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_GROUP_1);

        // then remove it
        groupDAO.remove(group);

        try
        {
            groupDAO.storeAttributes(group, ImmutableMap.<String,Set<String>>of());
            fail("GroupNotFoundException expected");
        }
        catch (GroupNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testRemoveAttribute() throws Exception
    {
        // find existing group with attributes
        GroupWithAttributes group = groupDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_GROUP_1);
        assertEquals(2, group.getKeys().size());

        // remove attribute (country)
        groupDAO.removeAttribute(group, COUNTRY_ATTRIBUTE);

        // find again
        GroupWithAttributes updatedGroup = groupDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_GROUP_1.toUpperCase());
        assertEquals(1, updatedGroup.getKeys().size());
        assertEquals(1, updatedGroup.getValues(LANGUAGE_ATTRIBUTE).size());
        assertEquals(LANGUAGE_ATTRIBUTE_VALUE, updatedGroup.getValue(LANGUAGE_ATTRIBUTE));
    }

    @Test
    public void testRemoveAttributeWhenGroupDoesNotExist() throws Exception
    {
        // find existing group
        GroupWithAttributes group = groupDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_GROUP_1);

        // then remove it
        groupDAO.remove(group);

        try
        {
            groupDAO.removeAttribute(group, "attribute name");
            fail("GroupNotFoundException expected");
        }
        catch (GroupNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testRemoteAttributeWhenAttributeDoesNotExist() throws Exception
    {
        // find existing group
        GroupWithAttributes group = groupDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_GROUP_1);

        groupDAO.removeAttribute(group, "this attribute name does not exist");   // should do nothing
    }

    @Test
    public void testAddAll() throws Exception
    {
        Set<GroupTemplate> groups = new HashSet<GroupTemplate>();

        for (int i = 0; i < 50; i++)
        {
            GroupTemplate group = new GroupTemplate("Group" + i, DIRECTORY_ID, GroupType.LEGACY_ROLE);
            group.setDescription("description");
            groups.add(group);
        }

        BatchResult<Group> result = groupDAO.addAll(groups);

        assertEquals(50, result.getTotalSuccessful());
        assertEquals(50, result.getTotalAttempted());
        assertTrue(result.getFailedEntities().isEmpty());

        assertEquals(50,
                     groupDAO.search(DIRECTORY_ID,
                                     QueryBuilder.queryFor(String.class, EntityDescriptor.role())
                                         .with(Restriction.on(GroupTermKeys.NAME).startingWith("group"))
                                         .returningAtMost(100)).size());
    }

    @Test
    public void testAddAllWithErrorsAndDuplicates() throws Exception
    {
        // all groups before test
        int groupCount = groupDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.role()).returningAtMost(100)).size();

        Set<GroupTemplate> groups = new HashSet<GroupTemplate>();

        // new group
        GroupTemplate group = new GroupTemplate("group", DIRECTORY_ID, GroupType.LEGACY_ROLE);
        group.setDescription("description");
        groups.add(group);

        // update existing group
        GroupTemplate existinggroup = new GroupTemplate(groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_1));
        group.setDescription("something different");
        groups.add(existinggroup);

        // new group for different directory
        GroupTemplate differentDirectoryGroup = new GroupTemplate("group3", DIRECTORY_ID + 1, GroupType.LEGACY_ROLE);
        group.setDescription("description");
        groups.add(differentDirectoryGroup);

        BatchResult<Group> result = groupDAO.addAll(groups);

        assertEquals(2, result.getTotalSuccessful());
        assertEquals(groups.size(), result.getTotalAttempted());
        assertThat(namesOf(result.getFailedEntities()), containsInAnyOrder(EXISTING_GROUP_1));

        // two groups should have been added
        assertEquals(groupCount + 1, groupDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.role()).returningAtMost(100)).size());
    }

    private Group createNewGroup(String groupName)
        throws DirectoryNotFoundException, GroupNotFoundException, InvalidGroupException
    {
        GroupTemplate group = new GroupTemplate(groupName, DIRECTORY_ID, GROUP);
        group.setActive(true);
        group.setDescription(NON_EXISTING_GROUP_DESC);
        return groupDAO.add(group);
    }

    private Group createNewLocalGroup(String groupName)
        throws DirectoryNotFoundException, GroupNotFoundException, InvalidGroupException
    {
        GroupTemplate group = new GroupTemplate(groupName, DIRECTORY_ID, GROUP);
        group.setActive(true);
        group.setDescription(NON_EXISTING_GROUP_DESC);
        return groupDAO.addLocal(group);
    }
}
