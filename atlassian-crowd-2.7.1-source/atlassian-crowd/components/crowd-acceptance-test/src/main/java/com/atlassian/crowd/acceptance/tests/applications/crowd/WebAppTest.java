package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.io.IOException;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.atlassian.crowd.acceptance.tests.BaseUrlFromProperties;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;

import org.apache.commons.io.IOUtils;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.fail;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.text.IsEqualIgnoringCase.equalToIgnoringCase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Tests of how the webapp is deployed in its container.
 */
public class WebAppTest
{
    @Test
    public void classpathResourceCannotBeFetched() throws IOException
    {
        String baseUrl = BaseUrlFromProperties.withLocalTestProperties().baseUrlFor("crowd");

        String resourceUrl = baseUrl + "/s///X/_/WEB-INF/web.xml";

        HttpURLConnection conn = (HttpURLConnection) new URL(resourceUrl).openConnection();

        assertEquals(404, conn.getResponseCode());
        String content = IOUtils.toString(conn.getErrorStream(), "utf-8");
        assertThat(content, not(containsString("<web-app")));
    }

    @Test
    public void classpathResourceCannotBeFetchedWithParentTraversal() throws IOException
    {
        String baseUrl = BaseUrlFromProperties.withLocalTestProperties().baseUrlFor("crowd");

        String resourceUrl = baseUrl + "/s///X/_/download/../WEB-INF/web.xml";

        HttpURLConnection conn = (HttpURLConnection) new URL(resourceUrl).openConnection();

        assertEquals(404, conn.getResponseCode());
        String content = IOUtils.toString(conn.getErrorStream(), "utf-8");
        assertThat(content, not(containsString("<web-app")));
    }

    @Test
    public void classpathResourceCannotBeFetchedWithPercentEncodedParentTraversal() throws IOException
    {
        String baseUrl = BaseUrlFromProperties.withLocalTestProperties().baseUrlFor("crowd");

        String resourceUrl = baseUrl + "/s///X/_/download/%2E./WEB-INF/web.xml";

        HttpURLConnection conn = (HttpURLConnection) new URL(resourceUrl).openConnection();

        assertEquals(404, conn.getResponseCode());
        String content = IOUtils.toString(conn.getErrorStream(), "utf-8");
        assertThat(content, not(containsString("<web-app")));
    }

    @Test
    public void staticResourceCanBeFetchedAndHasExpirySet() throws IOException
    {
        String baseUrl = BaseUrlFromProperties.withLocalTestProperties().baseUrlFor("crowd");

        String resourceUrl = baseUrl + "/s///X/_/download/batch/com.atlassian.auiplugin:jquery-lib/com.atlassian.auiplugin:jquery-lib.js";

        HttpURLConnection conn = (HttpURLConnection) new URL(resourceUrl).openConnection();

        assertEquals(200, conn.getResponseCode());

        assertThat(conn.getContentType(), Matchers.<String>either(startsWith("text/javascript")).or(startsWith("application/javascript")));

        long exp = conn.getExpiration();
        assertNotEquals(exp, 0);
        assertThat(new Date(exp), greaterThan(new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(30))));

        String content = IOUtils.toString(conn.getInputStream(), "utf-8");
        assertThat(content, containsString("jquery.org/license"));
    }

    @Test
    public void tomcatJSessionIdIsHttpOnlyCookie() throws Exception
    {
        String baseUrl = BaseUrlFromProperties.withLocalTestProperties().baseUrlFor("crowd");

        HttpURLConnection conn = (HttpURLConnection) new URL(baseUrl + "/").openConnection();
        conn.setInstanceFollowRedirects(false);
        conn.connect();

        assertEquals("Should be redirected", 302, conn.getResponseCode());

        String setCookie = conn.getHeaderField("Set-Cookie");
        assertNotNull("Should be provided with a session cookie", setCookie);

        try
        {
            assertEquals("JSESSIONID", HttpCookie.parse(setCookie).get(0).getName());
        }
        catch (IllegalArgumentException e)
        {
            fail(e.toString() + ": " + setCookie);
        }

        List<String> parts = ImmutableList.copyOf(Splitter.on(';').trimResults().split(setCookie));

        assertThat(parts, Matchers.<String>hasItem(equalToIgnoringCase("HttpOnly")));
    }
}
