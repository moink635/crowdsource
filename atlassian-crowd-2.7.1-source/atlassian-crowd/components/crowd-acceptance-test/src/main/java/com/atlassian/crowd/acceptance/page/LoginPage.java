package com.atlassian.crowd.acceptance.page;

import net.sourceforge.jwebunit.api.ITestingEngine;

public class LoginPage
{
    public LoginPage(ITestingEngine engine)
    {
        if (!engine.hasForm("login"))
        {
            throw new IllegalPageStateException(engine, "Not a login page");
        }
    }
}
