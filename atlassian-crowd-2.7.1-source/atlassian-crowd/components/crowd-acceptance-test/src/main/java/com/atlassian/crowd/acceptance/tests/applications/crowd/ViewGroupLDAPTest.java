package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.crowd.acceptance.tests.directory.BaseTest;
import com.atlassian.crowd.acceptance.utils.DbCachingTestHelper;
import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.UserTemplate;

import java.util.Properties;

/**
 * Test class for testing the updating of a group in an external (LDAP) directory
 */
public class ViewGroupLDAPTest extends CrowdAcceptanceTestCase
{
    private static final String DIRECTORY_NAME = "ApacheDS154";

    private static final long MAX_WAIT_FOR_SYNC_MS = 10000L; // 10 seconds

    private static final String USER_NAME = "myUser";
    private static final String GROUP_NAME = "myGroup";
    private static final String SUB_GROUP_NAME = "mySubGroup";
    private static final String SUB_USER_NAME = "mySubUser";

    LDAPLoader loader;

    /**
     * Allows us to add users to our ApacheDS instance.
     */
    class LDAPLoader extends BaseTest
    {
        LDAPLoader()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
        }

        @Override
        protected void configureDirectory(Properties directorySettings)
        {
            super.configureDirectory(directorySettings);
            directory.setAttribute(LDAPPropertiesMapper.LDAP_BASEDN_KEY, directorySettings.getProperty("test.integration.basedn"));
            directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.FALSE.toString());
        }

        @Override
        protected void loadTestData() throws Exception
        {
            UserTemplate user1 = new UserTemplate(USER_NAME, directory.getId());
            user1.setFirstName("Bob");
            user1.setLastName("Smith");
            user1.setDisplayName("Bob Smith");
            user1.setEmailAddress("bob@example.com");
            getRemoteDirectory().addUser(user1, new PasswordCredential("password"));

            UserTemplate user2 = new UserTemplate(SUB_USER_NAME, directory.getId());
            user2.setFirstName("Jane");
            user2.setLastName("Doe");
            user2.setDisplayName("Jane Doe");
            user2.setEmailAddress("jane@example.com");
            getRemoteDirectory().addUser(user2, new PasswordCredential("password"));

            GroupTemplate group = new GroupTemplate(GROUP_NAME, directory.getId(), GroupType.GROUP);     // members: USER_NAME, SUB_GROUP_NAME
            getRemoteDirectory().addGroup(group);

            GroupTemplate subGroup = new GroupTemplate(SUB_GROUP_NAME, directory.getId(), GroupType.GROUP);     // members: SUB_USER_NAME
            getRemoteDirectory().addGroup(subGroup);

            getRemoteDirectory().addUserToGroup(USER_NAME, GROUP_NAME);
            getRemoteDirectory().addGroupToGroup(SUB_GROUP_NAME, GROUP_NAME);

            getRemoteDirectory().addUserToGroup(SUB_USER_NAME, SUB_GROUP_NAME);
        }

        @Override
        protected void removeTestData() throws DirectoryInstantiationException
        {
            DirectoryTestHelper.silentlyRemoveUserFromGroup(USER_NAME, GROUP_NAME, getRemoteDirectory());
            DirectoryTestHelper.silentlyRemoveGroupFromGroup(SUB_GROUP_NAME, GROUP_NAME, getRemoteDirectory());
            DirectoryTestHelper.silentlyRemoveUserFromGroup(SUB_USER_NAME, SUB_GROUP_NAME, getRemoteDirectory());

            DirectoryTestHelper.silentlyRemoveUser(USER_NAME, getRemoteDirectory());
            DirectoryTestHelper.silentlyRemoveUser(SUB_USER_NAME, getRemoteDirectory());

            DirectoryTestHelper.silentlyRemoveGroup(GROUP_NAME, getRemoteDirectory());
            DirectoryTestHelper.silentlyRemoveGroup(SUB_GROUP_NAME, getRemoteDirectory());

            sessionFactory.getCurrentSession().flush();
        }

        public void removeGroupFromGroup() throws Exception
        {
            getRemoteDirectory().removeUserFromGroup(USER_NAME, GROUP_NAME);
        }

        public void accessibleSetUp() throws Exception
        {
            super.setUp();
        }

        public void accessibleTearDown() throws Exception
        {
            super.tearDown();
        }

    }

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        loader = new LDAPLoader();
        loader.accessibleSetUp();
        restoreCrowdFromXML("viewgrouptest.xml");
    }

    @Override
    public void tearDown() throws Exception
    {
        loader.accessibleTearDown();
        loader = null;
        super.tearDown();
    }

    public void testViewNestedGroupMembers()
    {
        intendToModifyData();

        log("Running: testViewNestedGroupMembers");
        synchroniseDirectory();

        gotoViewGroup(GROUP_NAME, DIRECTORY_NAME);

        clickLink("view-group-users");

        assertTextInTable("view-group-users", USER_NAME);
        assertTextNotInTable("view-group-users", SUB_USER_NAME);
        assertTextInTable("view-group-groups", SUB_GROUP_NAME);

        // Make sure the "All Users" tab is showing - it's used for nested groups
        assertKeyPresent("group.nestedmembers.label");
    }

    private void synchroniseDirectory()
    {
        DbCachingTestHelper.synchroniseDirectory(tester, DIRECTORY_NAME, MAX_WAIT_FOR_SYNC_MS);
    }

    public void testViewNestedGroupAllUsers()
    {
        intendToModifyData();

        log("Running: testViewNestedGroupAllUsers");

        synchroniseDirectory();
        gotoViewGroup(GROUP_NAME, DIRECTORY_NAME);

        clickLink("view-group-nested-principals");

        assertTextInTable("view-group-nested-principals", USER_NAME);
        assertTextInTable("view-group-nested-principals", SUB_USER_NAME);
        assertTableNotPresent("view-group-groups");
        assertTextNotInTable("view-group-nested-principals", SUB_GROUP_NAME);
    }

    public void testViewNestedGroupMembersOnlyGroupsAssigned() throws Exception
    {
        intendToModifyData();

        log("Running: testViewNestedGroupMembersOnlyGroupsAssigned");
        loader.removeGroupFromGroup();  // make sure that GROUP_NAME has no user members; just SUB_GROUP

        synchroniseDirectory();
        gotoViewGroup(GROUP_NAME, DIRECTORY_NAME);

        clickLink("view-group-users");

//        assertKeyPresent("viewprincipals.group.noprincipals.assigned", EasyList.build(GROUP_NAME));

        assertTextInTable("view-group-groups", SUB_GROUP_NAME);

        // Make sure the "All Users" tab is showing - it's used for nested groups
        assertKeyPresent("group.nestedmembers.label");
    }

    public void testViewNestedGroupAllMembersOnlyGroupsAssigned() throws Exception
    {
        intendToModifyData();

        log("Running: testViewNestedGroupAllMembersOnlyGroupsAssigned");
        loader.removeGroupFromGroup();  // make sure that GROUP_NAME has no user members; just SUB_GROUP

        synchroniseDirectory();
        gotoViewGroup(GROUP_NAME, DIRECTORY_NAME);

        clickLink("view-group-nested-principals");

        assertTextInTable("view-group-nested-principals", SUB_USER_NAME);
        assertTableNotPresent("view-group-groups");
        assertTextNotInTable("view-group-nested-principals", SUB_GROUP_NAME);
    }

    public void testViewGroupWithOneNonExistantMember()
    {
        log("Running: testViewGroupWithOneNonExistantMember");

        // groupA has a member DN with cn=ldapuserz, that does not exist
        // this test should show that even if one member is a ghost, the other members are still retrieved

        restoreCrowdFromXML("apachedsgrouptest.xml");

        DbCachingTestHelper.synchroniseDirectory(tester, "apacheds", MAX_WAIT_FOR_SYNC_MS);
        gotoViewGroup("groupA", "apacheds");

        clickLink("view-group-users");

        assertTextInTable("view-group-users", new String[]{"ldapusera", "ldapusera@testingarea.org", "true"});
        assertTextInTable("view-group-users", new String[]{"ldapuserb", "ldapuserb@testingarea.org", "true"});
        assertTextInTable("view-group-users", new String[]{"ldapuserc", "ldapuserc@testingarea.org", "true"});
        assertTextInTable("view-group-users", new String[]{"ldapuserd", "ldapuserd@testingarea.org", "true"});
        assertTextInTable("view-group-users", new String[]{"ldapusere", "ldapusere@testingarea.org", "true"});
        assertTextInTable("view-group-users", new String[]{"ldapuserf", "ldapuserf@testingarea.org", "true"});
        assertTextInTable("view-group-users", new String[]{"ldapuserg", "ldapuserg@testingarea.org", "true"});
        assertTextInTable("view-group-users", new String[]{"ldapuserh", "ldapuserh@testingarea.org", "true"});
    }
}
