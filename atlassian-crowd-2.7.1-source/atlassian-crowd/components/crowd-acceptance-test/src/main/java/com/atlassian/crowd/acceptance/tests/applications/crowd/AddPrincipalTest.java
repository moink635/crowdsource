package com.atlassian.crowd.acceptance.tests.applications.crowd;

/**
 * Web acceptance test for the adding of a Principal
 */
public class AddPrincipalTest extends CrowdAcceptanceTestCase
{
    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        _loginAdminUser();
        restoreCrowdFromXML("searchdirectories.xml");
    }

    public void testAddPrincipalWithNoDetails()
    {
        log("Running testAddPrincipalWithNoDetails");

        gotoAddPrincipal();

        setTextField("email", "");
        setTextField("name", "");
        setTextField("password", "");
        setTextField("passwordConfirm", "");
        setTextField("firstname", "");
        setTextField("lastname", "");
        selectOptionByValue("directoryID", "-1");
        submit();

        assertKeyPresent("principal.name.invalid");
        assertKeyPresent("principal.password.invalid");   // empty passwords are not allowed
        assertKeyPresent("principal.email.invalid");
        assertKeyPresent("principal.firstname.invalid");
        assertKeyPresent("principal.lastname.invalid");
        assertKeyPresent("principal.directory.invalid");
    }

    public void testInvalidEmailAddress()
    {
        log("Running testInvalidEmailAddress");

        gotoAddPrincipal();

        setTextField("email", "testuserATtest.com");

        submit();

        assertKeyPresent("principal.email.invalid");
    }

    public void testPasswordsMatchError()
    {
        log("Running testPasswordsMatchError");

        gotoAddPrincipal();

        setTextField("password", "password1");
        setTextField("passwordConfirm", "password2");

        submit();

        assertKeyPresent("principal.passwordconfirm.nomatch");
    }

    public void testUserAlreadyExistsError()
    {
        // Try to add the admin user, since this user should be created during the setup test
        log("Running testUserAlreadyExistsError");

        gotoAddPrincipal();

        setTextField("email", "admin@atlassian.com");
        setTextField("name", "admin");
        setTextField("password", "admin");
        setTextField("passwordConfirm", "admin");
        setTextField("firstname", "Super");
        setTextField("lastname", "User");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertKeyPresent("invalid.namealreadyexist");
    }

    public void testUserWithTrailingWhitespaceFailsWithSpecificError()
    {
        gotoAddPrincipal();

        setTextField("email", "admin@atlassian.com");
        setTextField("name", "admin ");
        setTextField("password", "admin");
        setTextField("passwordConfirm", "admin");
        setTextField("firstname", "Super");
        setTextField("lastname", "User");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertKeyPresent("invalid.whitespace");
    }

    public void testEmailAddressWithTrailingSpacesFailsWithSpecificError()
    {
        log("Running testEmailAddressWithTrailingSpacesFailsWithSpecificError");

        gotoAddPrincipal();

        setTextField("email", " john@example.test ");

        submit();

        assertKeyPresent("principal.email.whitespace");
    }

    public void testAddPrincipalToInternalDirectory()
    {
        intendToModifyData();

        log("Running testAddPrincipalToInternalDirectory");

        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("testuser");
    }

    public void testPasswordDoesNotMatchInternalDirectoryComplexityRequirements()
    {
        intendToModifyData();

        log("Running testPasswordDoesNotMatchInternalDirectoryRequirements");

        updateDirectoryWithPasswordRegex("Test Internal Directory", "[a-z]",
                                         "Passwords <b>must</b> contain at least one lowercase character");

        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "testuser");
        setTextField("password", "ALL-UPPERCASE-PASSWORD");
        setTextField("passwordConfirm", "ALL-UPPERCASE-PASSWORD");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertTextPresent("Passwords <b>must</b> contain at least one lowercase character");   // escaped
    }

    public void testAddModifyRemoveLDAPPrincipal()
    {
        intendToModifyData();
        intendToModifyLdapData();

        log("Running testAddModifyRemoveLDAPPrincipal");

        gotoAddPrincipal();

        String username = "testuser-LDAP";

        // add principal
        setTextField("email", "testuser@atlassian.com");
        setTextField("name", username);
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "Apache DS");

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent(username);
        assertTextFieldEquals("email", "testuser@atlassian.com");
        assertTextFieldEquals("firstname", "Test");
        assertTextFieldEquals("lastname", "User");
        assertCheckboxSelected("active");

        // modify attributes
        setTextField("email", "changed@example.com");
        setTextField("firstname", "Changed");
        setTextField("lastname", "Changed Too");
        submit();
        assertKeyPresent("updatesuccessful.label");
        assertTextPresent(username);
        assertTextFieldEquals("email", "changed@example.com");
        assertTextFieldEquals("firstname", "Changed");
        assertTextFieldEquals("lastname", "Changed Too");

        // remove the principal
        clickLink("remove-principal");
        submit();

        assertTextNotPresent(username);
    }

    public void testAddPrincipalToInternalDirectoryWithMixedName()
    {
        intendToModifyData();

        log("Running testAddPrincipalToInternalDirectoryWithInvalidName");

        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "TestUser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("TestUser");
    }

    /**
     * Test to assert that the validation error messages that occur when all input fields are left empty
     * are displayed only once when supplied to the AddPrincipal class.
     */
    public void testKeyPresentOnce()
    {
        log("Running testKeyPresentOnce");

        gotoAddPrincipal();

        selectOptionByValue("directoryID", "-1");

        submit();

        assertKeyPresentOnce("principal.name.invalid");
        assertKeyPresentOnce("principal.password.invalid");
        assertKeyPresentOnce("principal.email.invalid");
        assertKeyPresentOnce("principal.firstname.invalid");
        assertKeyPresentOnce("principal.lastname.invalid");
        assertKeyPresentOnce("principal.directory.invalid");
    }

    public void testNoDirectoryIsSelectedByDefault()
    {
        gotoAddPrincipal();
        assertSelectedOptionEquals("directoryID" , getText("selectdirectory.label"));
    }

    public void testDirectorySelectionIsSaved()
    {
        intendToModifyData();

        // Save directory selection
        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "Second Directory");

        submit();

        // Check that the directory is selected
        gotoAddPrincipal();
        assertSelectedOptionEquals("directoryID" , "Second Directory");
        assertSelectOptionNotPresent("directoryID" , getText("selectdirectory.label"));
    }

    public void testNonExistingDirectorySelection()
    {
        intendToModifyData();

        // Save directory selection
        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "Second Directory");

        submit();

        // Remove selected directory
        gotoBrowseDirectories();
        clickLinkWithText("Second Directory");
        clickLink("remove-directory");
        submit();

        // Check that no directory is selected
        gotoAddPrincipal();
        assertSelectedOptionEquals("directoryID" , getText("selectdirectory.label"));
        assertWarningAndErrorNotPresent();
    }

    public void testOnlyDirectoryIsSelected()
    {
        intendToModifyData();

        gotoBrowseDirectories();

        clickLinkWithText("Apache DS");
        clickLink("remove-directory");
        submit();

        clickLinkWithText("Apache DS 1.5.4");
        clickLink("remove-directory");
        submit();

        clickLinkWithText("Second Directory");
        clickLink("remove-directory");
        submit();

        gotoAddPrincipal();

        assertSelectedOptionEquals("directoryID" , "Test Internal Directory");
    }

    private void updateDirectoryWithPasswordRegex(String directoryName, String passwordRegex, String passwordComplexityMessage)
    {
        gotoBrowseDirectories();

        clickLinkWithExactText(directoryName);

        clickLink("internal-configuration");

        setTextField("passwordRegex", passwordRegex);
        setTextField("passwordComplexityMessage", passwordComplexityMessage);

        submit();
        assertTextFieldEquals("passwordComplexityMessage", passwordComplexityMessage);
    }
}
