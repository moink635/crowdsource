package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nullable;
import java.util.List;

import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;

public class BrowseDirectoriesTest extends CrowdAcceptanceTestCase
{

    public static final String DIRECTORY_TABLE_ID = "directory-table";
    public static final ImmutableList<String> DIRECTORY_TABLE_HEADERS = ImmutableList.of("Name", "Active", "Type", "Action");
    public static final Function<List<String>,String> MAP_DIRECTORY_NAME = new Function<List<String>, String>()
    {
        @Override
        public String apply(@Nullable List<String> input)
        {
            return input.get(0);
        }
    };

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("viewdirectory.xml");
    }

    public void testBrowseAllDirectories()
    {
        log("Running: testBrowseAllDirectories");

        gotoBrowseDirectories();

        List<String> directories = scrapeTable(DIRECTORY_TABLE_ID, DIRECTORY_TABLE_HEADERS, MAP_DIRECTORY_NAME);
        assertThat(directories, contains(
                "ActiveDirectory",
                "ApacheDS Caching Directory",
                "ApacheDS Delegated Directory",
                "ApacheDS Directory",
                "Custom Directory",
                "Generic Delegated Directory",
                "GenericLDAP Directory",
                "Remote Crowd Directory",
                "Second Directory",
                "Test Internal Directory"));
    }

    public void testFilterDirectoriesByName()
    {
        log("Running: testFilterDirectoriesByName");

        gotoBrowseDirectories();

        setTextField("name", "Delegated");
        submit();

        List<String> directories = scrapeTable(DIRECTORY_TABLE_ID, DIRECTORY_TABLE_HEADERS, MAP_DIRECTORY_NAME);
        assertThat(directories, contains(
                "ApacheDS Delegated Directory",
                "Generic Delegated Directory"));
    }

}
