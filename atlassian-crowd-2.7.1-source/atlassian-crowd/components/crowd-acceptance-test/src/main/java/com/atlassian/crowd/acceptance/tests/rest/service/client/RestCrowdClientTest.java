package com.atlassian.crowd.acceptance.tests.rest.service.client;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.integration.rest.service.RestCrowdClient;
import com.atlassian.crowd.model.authentication.Session;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import org.hamcrest.number.OrderingComparison;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;

/**
 * Tests the {@link RestCrowdClient}.
 */
public class RestCrowdClientTest extends CrowdAcceptanceTestCase
{
    private static final String APPLICATION_NAME = "crowd";
    private static final String APPLICATION_PASSWORD = "qybhDMZh";
    private static final ValidationFactor[] NO_VALIDATION_FACTORS = new ValidationFactor[] {};
    private static final String NON_EXISTENT_TOKEN = "this-token-key-does-not-exist";
    private static final long NON_EXISTENT_WEBHOOKID = 999L;

    private CrowdClient crowdClient;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        final Properties properties = new Properties();
        properties.setProperty(Constants.PROPERTIES_FILE_BASE_URL, HOST_PATH);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_NAME, APPLICATION_NAME);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_PASSWORD, APPLICATION_PASSWORD);

        final ClientProperties clientProperties = ClientPropertiesImpl.newInstanceFromProperties(properties);

        crowdClient = new RestCrowdClient(clientProperties);
        restoreCrowdFromXML("remotecrowddirectory.xml");
    }

    public void testGetUser() throws Exception
    {
        final User user = crowdClient.getUser("admin");
        assertEquals("admin", user.getName());
        assertEquals("bob", user.getFirstName());
        assertEquals("the builder", user.getLastName());
        assertEquals("bob@example.net", user.getEmailAddress());
        assertTrue(user.isActive());
    }

    /**
     * Tests that {@link CrowdClient#getNamesOfParentGroupsForGroup(String, int, int)} retrieves the correct parent
     * group names.
     */
    public void testGetGroupNameMembershipsForGroup() throws Exception
    {
        List<String> expectedGroupNames = Arrays.asList("crowd-testers", "crowd-users");
        List<String> groupNames = crowdClient.getNamesOfParentGroupsForGroup("badgers", 0, 50);
        assertNotNull(groupNames);
        assertEquals(expectedGroupNames.size(), groupNames.size());
        assertTrue(groupNames.containsAll(expectedGroupNames));
    }

    /**
     * Tests that {@link CrowdClient#getNamesOfParentGroupsForGroup(String, int, int)} with an invalid start index will
     * throw an OperationFailedException.
     */
    public void testGetGroupNameMembershipsForGroup_InvalidStartIndex() throws Exception
    {
        try
        {
            crowdClient.getNamesOfParentGroupsForGroup("badgers", -1, 50);
            fail("Expected OperationFailedException");
        }
        catch (OperationFailedException e)
        {
            // expected behaviour
        }
    }

    /**
     * Tests that {@link CrowdClient#getNamesOfParentGroupsForGroup(String, int, int)} for a non-existent group returns an empty list.
     */
    public void testGetGroupNameMembershipsForGroup_NonExistentGroup() throws Exception
    {
        List<String> groupNames = crowdClient.getNamesOfParentGroupsForGroup("non-existent", 0, 50);
        assertNotNull(groupNames);
        assertTrue(groupNames.isEmpty());
    }

    /**
     * Tests that {@link CrowdClient#getNamesOfNestedUsersOfGroup(String, int, int)} returns the correct user names
     * of nested members of a group.
     */
    public void testGetNestedUserNameMembersOfGroup() throws Exception
    {
        List<String> expectedUsernames = Arrays.asList("secondadmin", "penny", "admin", "eeeep");
        List<String> usernames = crowdClient.getNamesOfNestedUsersOfGroup("crowd-administrators", 0, 50);
        assertNotNull(usernames);
        assertEquals(expectedUsernames.size(), usernames.size());
        assertTrue(usernames.containsAll(expectedUsernames));
    }

    /**
     * Tests that {@link CrowdClient#getNamesOfNestedUsersOfGroup(String, int, int)} returns an empty list for a
     * non-existent group.
     */
    public void testGetNestedUserNameMembersOfGroup_NonExistentGroup() throws Exception
    {
        List<String> usernames = crowdClient.getNamesOfNestedUsersOfGroup("non-existent", 0, 50);
        assertNotNull(usernames);
        assertTrue(usernames.isEmpty());
    }

    /**
     * Tests that {@link CrowdClient#getNestedChildGroupsOfGroup(String, int, int)} returns the nested group members
     * of group.
     */
    public void testGetNestedGroupMembersOfGroup() throws Exception
    {
        List<String> expectedGroups = Arrays.asList("crowd-testers", "badgers");
        List<Group> groups = crowdClient.getNestedChildGroupsOfGroup("crowd-administrators", 0, 50);
        assertNotNull(groups);
        List<String> groupNames = new ArrayList<String>(groups.size());
        for (Group group : groups)
        {
            groupNames.add(group.getName());
        }
        assertEquals(expectedGroups.size(), groupNames.size());
        assertTrue(groupNames.containsAll(expectedGroups));
    }

    /**
     * Tests that {@link CrowdClient#getNestedChildGroupsOfGroup(String, int, int)} returns an empty list for a
     * non-existent group.
     */
    public void testGetNestedGroupMembersOfGroup_NonExistentGroup() throws Exception
    {
        List<Group> groups = crowdClient.getNestedChildGroupsOfGroup("non-existent", 0, 50);
        assertNotNull(groups);
        assertTrue(groups.isEmpty());
    }

    /**
     * Tests that {@link CrowdClient#getNamesOfNestedChildGroupsOfGroup(String, int, int)} returns the group names of
     * nested group members.
     */
    public void testGetNestedGroupNameMembersOfGroup() throws Exception
    {
        List<String> expectedGroups = Arrays.asList("crowd-testers", "badgers");
        List<String> groups = crowdClient.getNamesOfNestedChildGroupsOfGroup("crowd-administrators", 0, 50);
        assertNotNull(groups);
        assertEquals(expectedGroups.size(), groups.size());
        assertTrue(groups.containsAll(expectedGroups));
    }

    /**
     * Tests that {@link CrowdClient#getNamesOfNestedChildGroupsOfGroup(String, int, int)} returns an empty list for a
     * non-existent group.
     */
    public void testGetNestedGroupNameMembersOfGroup_NonExistent() throws Exception
    {
        List<String> expectedGroups = Arrays.asList("crowd-testers", "badgers");
        List<String> groups = crowdClient.getNamesOfNestedChildGroupsOfGroup("crowd-administrators", 0, 50);
        assertNotNull(groups);
        assertEquals(expectedGroups.size(), groups.size());
        assertTrue(groups.containsAll(expectedGroups));
    }

    /**
     * Tests that {@link CrowdClient#getGroupsForNestedUser(String, int, int)} returns the correct nested
     * group memberships for a user.
     */
    public void testGetNestedGroupMembershipsForUser() throws Exception
    {
        List<String> expectedGroups = Arrays.asList("crowd-administrators", "crowd-testers", "badgers", "crowd-users");
        List<Group> groups = crowdClient.getGroupsForNestedUser("admin", 0, 50);
        assertNotNull(groups);
        List<String> groupNames = new ArrayList<String>();
        for (Group group : groups)
        {
            groupNames.add(group.getName());
        }
        assertEquals(expectedGroups.size(), groupNames.size());
        assertTrue(groupNames.containsAll(expectedGroups));
    }

    /**
     * Tests that {@link CrowdClient#getGroupsForNestedUser(String, int, int)} returns an empty list for a
     * non-existent username.
     */
    public void testGetNestedGroupMembershipsForUser_NonExistent() throws Exception
    {
        List<Group> groups = crowdClient.getGroupsForNestedUser("non-existent", 0, 50);
        assertNotNull(groups);
        assertTrue(groups.isEmpty());
    }

    /**
     * Tests that {@link CrowdClient#getNamesOfGroupsForNestedUser(String, int, int)} returns the correct group
     * names for a nested user member
     */
    public void testGetNestedGroupNameMembershipsForUser() throws Exception
    {
        List<String> expectedGroups = Arrays.asList("crowd-administrators", "crowd-testers", "badgers", "crowd-users");
        List<String> groups = crowdClient.getNamesOfGroupsForNestedUser("admin", 0, 50);
        assertNotNull(groups);
        assertTrue(groups.containsAll(expectedGroups));
    }

    /**
     * Tests that {@link CrowdClient#getNamesOfGroupsForNestedUser(String, int, int)} returns an empty list
     * for a non-existent username.
     */
    public void testGetNestedGroupNameMembershipsForUser_NonExistent() throws Exception
    {
        List<String> groups = crowdClient.getNamesOfGroupsForNestedUser("non-existent", 0, 50);
        assertNotNull(groups);
        assertTrue(groups.isEmpty());
    }

    /**
     * Tests that {@link CrowdClient#getParentGroupsForNestedGroup(String, int, int)} returns the correct nested
     * group memberships for a group.
     */
    public void testGetNestedGroupMembershipsForGroup() throws Exception
    {
        List<String> expectedGroups = Arrays.asList("crowd-administrators", "crowd-testers", "crowd-users");
        List<Group> groups = crowdClient.getParentGroupsForNestedGroup("badgers", 0, 50);
        assertNotNull(groups);
        List<String> groupNames = new ArrayList<String>();
        for (Group group : groups)
        {
            groupNames.add(group.getName());
        }
        assertEquals(expectedGroups.size(), groupNames.size());
        assertTrue(groupNames.containsAll(expectedGroups));
    }

    /**
     * Tests that {@link CrowdClient#getParentGroupsForNestedGroup(String, int, int)} returns an empty list for a
     * non-existent group.
     */
    public void testGetNestedGroupMembershipsForGroup_NonExistent() throws Exception
    {
        List<Group> groups = crowdClient.getParentGroupsForNestedGroup("non-existent", 0, 50);
        assertNotNull(groups);
        assertTrue(groups.isEmpty());
    }

    /**
     * Tests that {@link CrowdClient#getNamesOfParentGroupsForNestedGroup(String, int, int)} returns the correct nested
     * group name memberships of a group.
     */
    public void testGetNestedGroupNameMembershipsForGroup() throws Exception
    {
        List<String> expectedGroups = Arrays.asList("crowd-administrators", "crowd-testers", "crowd-users");
        List<String> groups = crowdClient.getNamesOfParentGroupsForNestedGroup("badgers", 0, 50);
        assertNotNull(groups);
        assertEquals(expectedGroups.size(), groups.size());
        assertTrue(groups.containsAll(expectedGroups));
    }

    /**
     * Tests that {@link CrowdClient#getNamesOfParentGroupsForNestedGroup(String, int, int)} returns an empty list for
     * a non-existent group.
     */
    public void testGetNestedGroupNameMembershipsForGroup_NonExistent() throws Exception
    {
        List<String> groups = crowdClient.getNamesOfParentGroupsForNestedGroup("non-existent", 0, 50);
        assertNotNull(groups);
        assertTrue(groups.isEmpty());
    }

    public void testAddUser() throws Exception
    {
        intendToModifyData();

        UserTemplate userTemplate = new UserTemplate("newuser", "fname", "lname", "dname");
        crowdClient.addUser(userTemplate, PasswordCredential.unencrypted("newpassword"));

        // finds the new user
        User foundUser = crowdClient.getUser("newuser");
        assertNotNull("An identifier should be generated for the user", foundUser.getExternalId());
    }

    public void testUpdateUserCredential() throws Exception
    {
        intendToModifyData();

        final String user = "eeeep";
        final String oldPassword = "abc123";
        final String newPassword = "newpassword";

        // checks that the old password is working
        crowdClient.authenticateUser(user, oldPassword);

        crowdClient.updateUserCredential(user, newPassword);

        try
        {
            crowdClient.authenticateUser(user, oldPassword);
            fail("User should no longer be able to authenticate with the old password");
        }
        catch (InvalidAuthenticationException e)
        {
            // good
        }

        crowdClient.authenticateUser(user, newPassword);
    }

    public void testUpdateUserCredentialPasswordDoesNotMatchDirectoryComplexityRequirement() throws Exception
    {
        intendToModifyData();

        final String user = "eeeep";
        final String oldPassword = "abc123";

        // checks that the old password is working
        crowdClient.authenticateUser(user, oldPassword);

        try
        {
            crowdClient.updateUserCredential(user, "PASSWORD-WITHOUT-LOWERCASE");
            fail("The password shouldn't be changed if it does not match the directory complexity requirements");
        }
        catch (InvalidCredentialException e)
        {
            // good
            assertThat(e.getMessage(), containsString("Passwords must contain at least one lowercase character"));
        }

        // checks that the old password is still working
        crowdClient.authenticateUser(user, oldPassword);
    }

    /**
     * Tests that {@link RestCrowdClient} throws an <tt>OperationFailedException</tt> if the RestCrowdClient is not
     * communicating with a valid Crowd REST service.
     */
    public void testConnectToInvalidRestService() throws Exception
    {
        final Properties properties = new Properties();
        properties.setProperty(Constants.PROPERTIES_FILE_BASE_URL, HOST_PATH + "/badservice");
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_NAME, APPLICATION_NAME);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_PASSWORD, APPLICATION_PASSWORD);

        final ClientProperties clientProperties = ClientPropertiesImpl.newInstanceFromProperties(properties);

        CrowdClient badCrowdClient = new RestCrowdClient(clientProperties);
        try
        {
            badCrowdClient.testConnection();
            fail("OperationFailedException expected.");
        }
        catch (com.atlassian.crowd.exception.OperationFailedException e)
        {
            // expected
        }
    }

    /**
     * Tests that {@link RestCrowdClient} throws an <tt>InvalidAuthenticationException</tt> if the application password
     * is invalid.
     */
    public void testConnectToRestService_WrongPassword() throws Exception
    {
        final String APPLICATION_AUTHENTICATION_ERROR_MSG = "Application failed to authenticate";
        final Properties properties = new Properties();
        properties.setProperty(Constants.PROPERTIES_FILE_BASE_URL, HOST_PATH);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_NAME, APPLICATION_NAME);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_PASSWORD, "blah");

        final ClientProperties clientProperties = ClientPropertiesImpl.newInstanceFromProperties(properties);

        CrowdClient badCrowdClient = new RestCrowdClient(clientProperties);
        try
        {
            badCrowdClient.testConnection();
            fail("OperationFailedException expected.");
        }
        catch (com.atlassian.crowd.exception.InvalidAuthenticationException e)
        {
            assertEquals(APPLICATION_AUTHENTICATION_ERROR_MSG, e.getMessage());
        }
    }

    /**
     * Tests that {@link RestCrowdClient#getMemberships()} returns successfully. The content of the response
     * is tested by <code>GroupsResourceTest#testGetMembershipsReturnsExactlyExpectedMemberships</code> in crowd-rest-test.
     */
    public void testGetMemberships() throws Exception
    {
        Iterable<Membership> groups = crowdClient.getMemberships();

        assertNotNull(groups);

        Iterator<Membership> i = groups.iterator();

        assertTrue(i.hasNext());
        assertNotNull(i.next());
    }

    public void testQueryForNullFirstNames() throws Exception
    {
        PropertyRestriction<String> restriction = Restriction.on(UserTermKeys.FIRST_NAME).isNull();
        List<User> users = crowdClient.searchUsers(restriction, 0, EntityQuery.ALL_RESULTS);

        assertEquals(Collections.emptyList(), users);
    }

    public void testMailUsernamesFailsWithUnknownAddress() throws Exception
    {
        try
        {
            crowdClient.requestUsernames("no-user-with@this-email-address.invalid");
            fail("Request for usernames should fail for unused email address.");
        }
        catch (OperationFailedException ofe)
        {
            assertEquals("Not Found", ofe.getMessage());
        }

        // A positive test would send email; ignore for now
    }

    public void testUpdateGroupWithAttributes() throws Exception
    {
        intendToModifyData();

        crowdClient.storeGroupAttributes("badgers", ImmutableMap.<String, Set<String>>of("favourite-colour", ImmutableSet.of("green")));

        GroupWithAttributes badgers = crowdClient.getGroupWithAttributes("badgers");

        assertEquals("green", badgers.getValue("favourite-colour"));
    }

    public void testSSOUserWithDefaultTokenDuration() throws Exception
    {
        intendToModifyData();

        _invalidateExistingSessions(ADMIN_USER);

        UserAuthenticationContext userAuthenticationContext =
            new UserAuthenticationContext(ADMIN_USER, new PasswordCredential(ADMIN_PW),
                                          NO_VALIDATION_FACTORS, APPLICATION_NAME);
        String token = crowdClient.authenticateSSOUser(userAuthenticationContext);

        // the token should be valid
        crowdClient.validateSSOAuthentication(token, Arrays.asList(NO_VALIDATION_FACTORS));

        // get the session details
        Session session =
            crowdClient.validateSSOAuthenticationAndGetSession(token, Arrays.asList(NO_VALIDATION_FACTORS));
        assertNotNull("Session should have a token key", session.getToken());
        assertNotNull("Session should have a creation date", session.getCreatedDate());
        assertNotNull("Session should have a expiry date", session.getExpiryDate());
        assertThat("Session should have some duration",
                   session.getCreatedDate(), OrderingComparison.lessThan(session.getExpiryDate()));

        // invalidate the token
        crowdClient.invalidateSSOToken(token);

        // the token should no longer be valid
        try
        {
            crowdClient.validateSSOAuthentication(token, Arrays.asList(NO_VALIDATION_FACTORS));
            fail("Token should no longer be valid");
        }
        catch (InvalidTokenException e)
        {
            // ok
        }
    }

    public void testSSOUserWithZeroLivedToken() throws Exception
    {
        intendToModifyData();

        _invalidateExistingSessions(ADMIN_USER);

        UserAuthenticationContext userAuthenticationContext =
            new UserAuthenticationContext(ADMIN_USER, new PasswordCredential(ADMIN_PW),
                                          NO_VALIDATION_FACTORS, APPLICATION_NAME);
        String token = crowdClient.authenticateSSOUser(userAuthenticationContext, 0L);
        try
        {
            crowdClient.validateSSOAuthentication(token, Arrays.asList(NO_VALIDATION_FACTORS));
            fail("Token should have expired already");
        }
        catch (InvalidTokenException e)
        {
            // ok
        }
    }

    public void testInvalidateAllSSOTokensForUser() throws Exception
    {
        intendToModifyData();

        // the tested operation is actually a pre-requisite to run this test
        _invalidateExistingSessions(ADMIN_USER);

        UserAuthenticationContext userAuthenticationContext =
            new UserAuthenticationContext(ADMIN_USER, new PasswordCredential(ADMIN_PW),
                                          NO_VALIDATION_FACTORS, APPLICATION_NAME);
        String token = crowdClient.authenticateSSOUser(userAuthenticationContext);

        // the token should be valid
        crowdClient.validateSSOAuthentication(token, Arrays.asList(NO_VALIDATION_FACTORS));

        // invalidate the token
        crowdClient.invalidateSSOTokensForUser(ADMIN_USER);

        // the token should no longer be valid
        try
        {
            crowdClient.validateSSOAuthentication(token, Arrays.asList(NO_VALIDATION_FACTORS));
            fail("Token should no longer be valid");
        }
        catch (InvalidTokenException e)
        {
            // ok
        }
    }

    public void testInvalidateAllOtherSSOTokensForUser() throws Exception
    {
        intendToModifyData();

        _invalidateExistingSessions(ADMIN_USER);

        UserAuthenticationContext userAuthenticationContext =
            new UserAuthenticationContext(ADMIN_USER, new PasswordCredential(ADMIN_PW),
                                          NO_VALIDATION_FACTORS, APPLICATION_NAME);
        String token = crowdClient.authenticateSSOUser(userAuthenticationContext);

        // the token should be valid
        crowdClient.validateSSOAuthentication(token, Arrays.asList(NO_VALIDATION_FACTORS));

        // invalidate the tokens, except for this one
        crowdClient.invalidateSSOTokensForUser(ADMIN_USER, token);

        // the token should be still valid
        crowdClient.validateSSOAuthentication(token, Arrays.asList(NO_VALIDATION_FACTORS));
    }

    public void testFindUserFromValidSSOToken() throws Exception
    {
        intendToModifyData();

        _invalidateExistingSessions(ADMIN_USER);

        UserAuthenticationContext userAuthenticationContext =
            new UserAuthenticationContext(ADMIN_USER, new PasswordCredential(ADMIN_PW),
                                          NO_VALIDATION_FACTORS, APPLICATION_NAME);
        String token = crowdClient.authenticateSSOUser(userAuthenticationContext);

        User user = crowdClient.findUserFromSSOToken(token);
        assertEquals(ADMIN_USER, user.getName());
        assertEquals("bob the builder", user.getDisplayName());
        assertEquals("bob", user.getFirstName());
        assertEquals("the builder", user.getLastName());
        assertEquals("bob@example.net", user.getEmailAddress());
        assertTrue("User should be active", user.isActive());
    }

    public void testFindUserFromInvalidSSOToken() throws Exception
    {
        intendToModifyData();

        _invalidateExistingSessions(ADMIN_USER);

        UserAuthenticationContext userAuthenticationContext =
            new UserAuthenticationContext(ADMIN_USER, new PasswordCredential(ADMIN_PW),
                                          NO_VALIDATION_FACTORS, APPLICATION_NAME);
        String token = crowdClient.authenticateSSOUser(userAuthenticationContext);

        // kill this session
        crowdClient.invalidateSSOToken(token);

        try
        {
            crowdClient.findUserFromSSOToken(token);
            fail("Should fail for an invalid token");
        }
        catch (InvalidTokenException e)
        {
            // ok
        }
    }

    public void testFindUserFromNonExistentSSOToken() throws Exception
    {
        try
        {
            crowdClient.findUserFromSSOToken(NON_EXISTENT_TOKEN);
            fail("Should fail for a non existent token");
        }
        catch (InvalidTokenException e)
        {
            // ok
        }
    }

    public void testGetNonExistingWebhook() throws Exception
    {
        try
        {
            crowdClient.getWebhook(NON_EXISTENT_WEBHOOKID);
            fail("Should fail for a non existent Webhook");
        }
        catch (WebhookNotFoundException e)
        {
            // ok
            assertThat(e.getMessage(), containsString("Webhook <" + NON_EXISTENT_WEBHOOKID + "> not found"));
        }
    }

    public void testUnregisterNonExistingWebhook() throws Exception
    {
        try
        {
            crowdClient.unregisterWebhook(NON_EXISTENT_WEBHOOKID);
            fail("Should fail for a non existent Webhook");
        }
        catch (WebhookNotFoundException e)
        {
            // ok
            assertThat(e.getMessage(), containsString("Webhook <" + NON_EXISTENT_WEBHOOKID + "> not found"));
        }
    }

    public void testAddUserToGroup() throws Exception
    {
        assertFalse(crowdClient.isUserDirectGroupMember("eeeep", "crowd-users"));

        crowdClient.addUserToGroup("eeeep", "crowd-users");

        assertTrue(crowdClient.isUserDirectGroupMember("eeeep", "crowd-users"));
    }

    public void testAddUserToGroupWhenMembershipAlreadyExists() throws Exception
    {
        assertTrue(crowdClient.isUserDirectGroupMember("eeeep", "badgers"));

        try
        {
            crowdClient.addUserToGroup("eeeep", "badgers");
            fail("Should fail for already existing membership");
        }
        catch (MembershipAlreadyExistsException e)
        {
            // ok
        }
    }

    public void testAddGroupToGroup() throws Exception
    {
        assertFalse(crowdClient.isGroupDirectGroupMember("badgers", "crowd-administrators"));

        crowdClient.addGroupToGroup("badgers", "crowd-administrators");

        assertTrue(crowdClient.isGroupDirectGroupMember("badgers", "crowd-administrators"));
    }

    public void testAddGroupToGroupWhenMembershipAlreadyExists() throws Exception
    {
        assertTrue(crowdClient.isGroupDirectGroupMember("badgers", "crowd-users"));

        try
        {
            crowdClient.addGroupToGroup("badgers", "crowd-users");
            fail("Should fail for already existing membership");
        }
        catch (MembershipAlreadyExistsException e)
        {
            // ok
        }
    }

    private void _invalidateExistingSessions(String username)
        throws OperationFailedException, ApplicationPermissionException, InvalidAuthenticationException
    {
        // in order to ensure that tests are run in isolation, sometimes we need to
        // invalidated all existing sessions. Otherwise, we may be re-using existing
        // tokens instead of creating new ones
        crowdClient.invalidateSSOTokensForUser(username);
    }
}
