package com.atlassian.crowd.acceptance.utils;

import org.dom4j.Document;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.XMLWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Helps collect and write stats in Jmeter format.
 */
public class LoadStatWriter
{
    private String filename;
    private Document doc;
    private Element root;

    public LoadStatWriter(String filename)
    {
        this.filename = filename;
        doc = DocumentFactory.getInstance().createDocument();
        root = doc.addElement("testResults");
        root.addAttribute("version", "1.2");

    }

    public void addPoint(String statName, long result)
    {
        Element point = root.addElement("httpSample");
        point.addAttribute("lb", statName);
        point.addAttribute("t", Long.toString(result));
        point.addAttribute("s", "true");
        point.addAttribute("ts", "1");
        point.addAttribute("tn", "0");
    }

    public void close() throws IOException
    {
        (new File("target/loadteststats")).mkdir();

        XMLWriter writer = new XMLWriter(new FileWriter("target/loadteststats/" + filename + ".jtl"));

        try
        {
            writer.write(doc);
        }
        finally
        {
            writer.close();
        }
    }
}