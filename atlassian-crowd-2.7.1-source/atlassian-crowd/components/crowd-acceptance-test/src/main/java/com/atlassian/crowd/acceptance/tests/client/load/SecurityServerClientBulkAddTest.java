package com.atlassian.crowd.acceptance.tests.client.load;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.BulkAddFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.soap.SOAPPrincipalWithCredential;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.service.soap.client.SecurityServerClientImpl;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

public class SecurityServerClientBulkAddTest extends CrowdAcceptanceTestCase
{
    private SecurityServerClientImpl securityServerClient;
    private UserAuthenticationContext userAuthenticationContext;

    public void setUp() throws Exception
    {
        super.setUp();

        restoreCrowdFromXML("tokenauthenticationtest.xml");
        intendToModifyData();

        final Properties sscProperties = AcceptanceTestHelper.loadProperties("localtest.crowd.properties");
        SoapClientProperties cProperties = SoapClientPropertiesImpl.newInstanceFromProperties(sscProperties);
        securityServerClient = new SecurityServerClientImpl(cProperties);

        userAuthenticationContext = new UserAuthenticationContext();
        userAuthenticationContext.setApplication("integrationtest");
    }

    public static void setFullName(String fullname, SOAPPrincipal principal)
    {
        setProperty(UserConstants.DISPLAYNAME, fullname, principal);
    }

    public static void setEmail(String email, SOAPPrincipal principal)
    {
        setProperty(UserConstants.EMAIL, email, principal);
    }

    public static void setProperty(String property, String value, SOAPPrincipal principal)
    {
        Collection<SOAPAttribute> newAttributes = new ArrayList<SOAPAttribute>();

        // copy existing attributes into collection.
        SOAPAttribute[] existingAttributes = principal.getAttributes();
        if (existingAttributes != null)
        {
            newAttributes.addAll(Arrays.asList(existingAttributes));
        }

        // Some specific logic for firstname and last name here!
        // We need to set three seperate attibutes (first name, last name and display name)
        if (UserConstants.DISPLAYNAME.equals(property))
        {
            addOrReplaceAttribute(newAttributes, new SOAPAttribute(UserConstants.DISPLAYNAME, value));
            // first name and last name will be constructed from the display name
            addOrReplaceAttribute(newAttributes, new SOAPAttribute(UserConstants.FIRSTNAME, " "));
            addOrReplaceAttribute(newAttributes, new SOAPAttribute(UserConstants.LASTNAME, " "));
        }
        else
        {
            addOrReplaceAttribute(newAttributes, new SOAPAttribute(property, value));
        }

        principal.setAttributes(newAttributes.toArray(new SOAPAttribute[newAttributes.size()]));
    }

    /**
     * Given an attribute name, will add the value to the attributes list. If an attribute already exists, it will be
     * replaced.
     *
     * @param attributes The existing attribute collection
     * @param newAttribute The attribute to add or replace in the collection
     */
    private static void addOrReplaceAttribute(Collection<SOAPAttribute> attributes, SOAPAttribute newAttribute)
    {
        assertNotNull(attributes);
        assertNotNull(newAttribute);
        assertNotNull(newAttribute.getName());
        assertNotNull(newAttribute.getValues());

        boolean replaced = false;
        for (Iterator<SOAPAttribute> attrIt = attributes.iterator(); attrIt.hasNext();)
        {
            SOAPAttribute existingAttribute = attrIt.next();

            if (newAttribute.getName().equals(existingAttribute.getName()))
            {
                // replace
                existingAttribute.setValues(new String[] { newAttribute.getValues()[0] });
            }
        }
        if (!replaced)
        {
            // add
            attributes.add(newAttribute);
        }
    }

    public void testBulkAddToTwoDirectories() throws Exception
    {
        Collection<SOAPPrincipalWithCredential> principalWithCredentials = new ArrayList<SOAPPrincipalWithCredential>();
        for (int i = 0; i < 1000; i++)
        {
            final SOAPPrincipal principal = new SOAPPrincipal("user" + i);
            setFullName("user" + i + "test", principal);
            setEmail("user" + i + "@localhost", principal);
            SOAPPrincipalWithCredential principalWithCredential = new SOAPPrincipalWithCredential(principal, com.atlassian.crowd.integration.authentication.PasswordCredential.unencrypted("user" + i));
            principalWithCredentials.add(principalWithCredential);
        }
        setApplication("dir3_dir4_app ", "admin");
        securityServerClient.addAllPrincipals(principalWithCredentials);
        // All the users should have been added to "dir3_app"
        setApplication("dir3_app", "admin");
        for (SOAPPrincipalWithCredential principal : principalWithCredentials)
        {
            final SOAPPrincipal soapPrincipal = securityServerClient.findPrincipalByName(principal.getPrincipal().getName());
            assertEquals(principal.getPrincipal().getName(), soapPrincipal.getName());
        }
        // The users should have ONLY been added to "dir3_app", not "dir4_app"
        setApplication("dir4_app", "admin");
        for (SOAPPrincipalWithCredential principal : principalWithCredentials)
        {
            try
            {
                securityServerClient.findPrincipalByName(principal.getPrincipal().getName());
                fail("UserNotFoundException expected");
            }
            catch (UserNotFoundException ex)
            {
                assertEquals("User <" + principal.getPrincipal().getName() + "> does not exist", ex.getMessage());
            }
        }
    }

    public void testBulkUserCreation() throws Exception
    {
        Collection<SOAPPrincipalWithCredential> principalWithCredentials = new ArrayList<SOAPPrincipalWithCredential>();
        for (int i = 0; i < 1000; i++)
        {
            final SOAPPrincipal principal = new SOAPPrincipal("usertest" + i);
            setFullName("user" + i + "test", principal);
            setEmail("user" + i + "@localhost", principal);
            SOAPPrincipalWithCredential principalWithCredential = new SOAPPrincipalWithCredential(principal, com.atlassian.crowd.integration.authentication.PasswordCredential.unencrypted("user" + i));
            principalWithCredentials.add(principalWithCredential);
        }
        securityServerClient.addAllPrincipals(principalWithCredentials);

        principalWithCredentials = new ArrayList<SOAPPrincipalWithCredential>();
        for (int i = 1000; i < 2000; i++)
        {
            final SOAPPrincipal principal = new SOAPPrincipal("usertest" + i);
            setFullName("user" + i + "test", principal);
            setEmail("user" + i + "@localhost", principal);
            SOAPPrincipalWithCredential principalWithCredential = new SOAPPrincipalWithCredential(principal, com.atlassian.crowd.integration.authentication.PasswordCredential.unencrypted("user" + i));
            principalWithCredentials.add(principalWithCredential);
        }
        securityServerClient.addAllPrincipals(principalWithCredentials);
    }

    public void testBulkUserCreationFailedUsersExist() throws Exception
    {
        Collection<SOAPPrincipalWithCredential> principalWithCredentials = new ArrayList<SOAPPrincipalWithCredential>();
        for (int i = 0; i < 900; i++)
        {
            final SOAPPrincipal principal = new SOAPPrincipal("usertest" + i);
            setFullName("user" + i + "test", principal);
            setEmail("user" + i + "@localhost", principal);
            SOAPPrincipalWithCredential principalWithCredential = new SOAPPrincipalWithCredential(principal, com.atlassian.crowd.integration.authentication.PasswordCredential.unencrypted("user" + i));
            principalWithCredentials.add(principalWithCredential);
        }
        securityServerClient.addAllPrincipals(principalWithCredentials);

        principalWithCredentials = new ArrayList<SOAPPrincipalWithCredential>();
        for (int i = 0; i < 1000; i++)
        {
            final SOAPPrincipal principal = new SOAPPrincipal("usertest" + i);
            setFullName("user" + i + "test", principal);
            setEmail("user" + i + "@localhost", principal);
            SOAPPrincipalWithCredential principalWithCredential = new SOAPPrincipalWithCredential(principal, com.atlassian.crowd.integration.authentication.PasswordCredential.unencrypted("user" + i));
            principalWithCredentials.add(principalWithCredential);
        }
        try
        {
            securityServerClient.addAllPrincipals(principalWithCredentials);
            fail("BulkAddFailedException expected!");
        }
        catch (BulkAddFailedException ex)
        {
            assertEquals(900, ex.getExistingUsers().size());
            assertEquals(0, ex.getFailedUsers().size());
        }
        for (int i = 900; i < 1000; i++)
        {
            final String username = "usertest" + i;
            final SOAPPrincipal principal = securityServerClient.findPrincipalByName(username);
            assertEquals(username, principal.getName());
        }
    }

    public void testBulkUserCreationFailedNoPermission() throws Exception
    {
        setApplication("bulk-create-app", "admin");

        Collection<SOAPPrincipalWithCredential> principalWithCredentials = new ArrayList<SOAPPrincipalWithCredential>();
        for (int i = 0; i < 1000; i++)
        {
            final SOAPPrincipal principal = new SOAPPrincipal("usertest" + i);
            setFullName("user" + i + "test", principal);
            setEmail("user" + i + "@localhost", principal);
            SOAPPrincipalWithCredential principalWithCredential = new SOAPPrincipalWithCredential(principal, com.atlassian.crowd.integration.authentication.PasswordCredential.unencrypted("user" + i));
            principalWithCredentials.add(principalWithCredential);
        }

        try
        {
            securityServerClient.addAllPrincipals(principalWithCredentials);
            fail("ApplicationPermissionException expected!");
        }
        catch (ApplicationPermissionException e)
        {
            assertEquals("Application 'bulk-create-app' has no directories that allow adding of users.", e.getMessage());
        }
    }

    private void setApplication(final String applicationName, final String password)
    {
        Properties sscProperties = AcceptanceTestHelper.loadProperties("localtest.crowd.properties");
        sscProperties.setProperty("application.password", password);
        sscProperties.setProperty("application.name", applicationName);
        SoapClientProperties cProperties = SoapClientPropertiesImpl.newInstanceFromProperties(sscProperties);
        securityServerClient = new SecurityServerClientImpl(cProperties);
        userAuthenticationContext = new UserAuthenticationContext();
        userAuthenticationContext.setApplication("integrationtest");
    }
}
