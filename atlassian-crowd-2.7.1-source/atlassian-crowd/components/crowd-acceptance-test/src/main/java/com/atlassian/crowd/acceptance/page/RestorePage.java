package com.atlassian.crowd.acceptance.page;

import net.sourceforge.jwebunit.junit.WebTester;

public class RestorePage extends SecurePage
{
    public RestorePage(WebTester tester, String baseUrl, String displayName, String username, String password)
    {
        super(tester, baseUrl, "/console/secure/admin/restore.action", displayName, username, password);

        if (!engine.hasForm("import"))
        {
            throw new IllegalPageStateException(engine, "This is not the restore page");
        }
    }

    public LoginPage restore(String path)
    {
        engine.setWorkingForm("import", 0);
        engine.setTextField("importFilePath", path);
        engine.submit();

        return new LoginPage(engine);
    }
}
