package com.atlassian.crowd.acceptance.tests.directory;

import java.util.List;

import javax.annotation.Nullable;

import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.ReadOnlyGroupException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

/**
 * Acceptance tests for Primary Groups in Active Directory
 *
 * @since v2.7
 */
public class PrimaryGroupTest extends BaseTest
{
    protected final String userNameA = "user-A";

    protected final String password = "secret-password";
    private static final String PRIMARY_GROUP_NAME = "Domain Users";

    @Override
    protected void loadTestData() throws Exception
    {
        DirectoryTestHelper.addUser(userNameA, directory.getId(), password, getRemoteDirectory());
    }

    @Override
    protected void removeTestData() throws DirectoryInstantiationException
    {
        DirectoryTestHelper.silentlyRemoveUser(userNameA, getRemoteDirectory());
    }

    public void testCannotRemovePrimaryGroup() throws Exception
    {
        try
        {
            directoryManager.removeGroup(directory.getId(), PRIMARY_GROUP_NAME);
            fail("OperationFailedException expected");
        }
        catch (OperationFailedException e)
        {
            // good, now assert that we get an informative exception message
            assertThat(e.getMessage(), containsString("Cannot remove group '" + PRIMARY_GROUP_NAME
                                                      + "' because it is the primary group of some user(s)"));
        }
    }

    public void testNewUserHasPrimaryGroupAttributeAndObjectSid() throws Exception
    {
        UserWithAttributes user =
            directoryManager.findUserWithAttributesByName(directory.getId(), userNameA);
        assertNotNull(user.getValue("primaryGroupId"));
        assertNotNull(user.getValue("objectSid"));
    }

    public void testFindPrimaryGroupByName() throws Exception
    {
        Group group = directoryManager.findGroupByName(directory.getId(), PRIMARY_GROUP_NAME);
        assertThat(group.getName(), is(PRIMARY_GROUP_NAME));
        assertThat(group.getDescription(), is("All domain users"));
    }

    public void testFindPrimaryGroupHasObjectSidAttribute() throws Exception
    {
        GroupWithAttributes group =
            directoryManager.findGroupWithAttributesByName(directory.getId(), PRIMARY_GROUP_NAME);
        assertThat(group.getName(), is(PRIMARY_GROUP_NAME));
        assertThat(group.getDescription(), is("All domain users"));
        assertNotNull(group.getValue("objectSid"));
    }

    public void testSearchAllGroupsReturnsPrimaryGroup() throws Exception
    {
        List<String> groups = directoryManager.searchGroups(directory.getId(),
                                                            QueryBuilder.queryFor(String.class,
                                                                                  EntityDescriptor.group())
                                                                .returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(groups, hasItem(PRIMARY_GROUP_NAME));
    }

    public void testNewUserIsDirectMemberOfPrimaryGroup() throws Exception
    {
        assertTrue(directoryManager.isUserDirectGroupMember(directory.getId(), userNameA, PRIMARY_GROUP_NAME));
    }

    public void testPrimaryGroupMembersContainsNewUser() throws Exception
    {
        List<String> members = directoryManager.searchDirectGroupRelationships(directory.getId(),
                                                                               QueryBuilder.queryFor(String.class,
                                                                                                     EntityDescriptor.user())
                                                                                   .childrenOf(EntityDescriptor.group())
                                                                                   .withName(PRIMARY_GROUP_NAME)
                                                                                   .returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(members, hasItem(userNameA));
    }

    public void testNewUserGroupsContainsPrimaryGroup() throws Exception
    {
        List<String> groups = directoryManager.searchDirectGroupRelationships(directory.getId(),
                                                                              QueryBuilder.queryFor(String.class,
                                                                                                    EntityDescriptor.group())
                                                                                  .parentsOf(EntityDescriptor.user())
                                                                                  .withName(userNameA)
                                                                                  .returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(groups, hasItem(PRIMARY_GROUP_NAME));
    }

    public void testCannotAddUserToPrimaryGroup() throws Exception
    {
        try
        {
            directoryManager.addUserToGroup(directory.getId(), userNameA, PRIMARY_GROUP_NAME);
            fail("MembershipAlreadyExists expected");
        }
        catch (MembershipAlreadyExistsException e)
        {
            // good
        }
    }

    public void testCannotRemoveUserFromPrimaryGroup() throws Exception
    {
        try
        {
            directoryManager.removeUserFromGroup(directory.getId(), userNameA, PRIMARY_GROUP_NAME);
            fail("OperationFailedException expected");
        }
        catch (OperationFailedException e)
        {
            // good, now assert that we get an informative exception message
            assertEquals("Cannot remove user '" + userNameA + "' from group '" + PRIMARY_GROUP_NAME
                         + "' because it is the primary group of the user",
                         e.getMessage());
        }
    }

    public void testGetMemberships() throws Exception
    {
        Iterable<Membership> allMemberships = getRemoteDirectory().getMemberships();

        Iterable<Membership> membershipsOfPrimaryGroup = Iterables.filter(allMemberships, new Predicate<Membership>()
        {
            @Override
            public boolean apply(@Nullable Membership input)
            {
                return input.getGroupName().equals(PRIMARY_GROUP_NAME);
            }
        });

        assertThat(Iterables.getOnlyElement(membershipsOfPrimaryGroup).getUserNames(), hasItem(userNameA));
    }
}
