package com.atlassian.crowd.acceptance.utils;

import org.springframework.beans.BeansException;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Factory bean for EhCacheManagers that name {@link net.sf.ehcache.CacheManager}s after
 * the Id of the application context in which they are instantiated.
 * This factory makes it possible to re-use the same Spring bean files to instantiate multiple concurrent
 * application contexts without sharing the instance of {@link net.sf.ehcache.CacheManager}.
 * Calling {@link #setCacheManagerName(String)} or setting {@link #setShared(boolean)} to true defeat
 * the purpose of this factory bean.
 *
 */
public class ContextAwareEhCacheManagerFactoryBean extends EhCacheManagerFactoryBean implements ApplicationContextAware
{
    @Override
    public void setApplicationContext(final ApplicationContext applicationContext) throws BeansException
    {
        setCacheManagerName(applicationContext.getId());
    }
}
