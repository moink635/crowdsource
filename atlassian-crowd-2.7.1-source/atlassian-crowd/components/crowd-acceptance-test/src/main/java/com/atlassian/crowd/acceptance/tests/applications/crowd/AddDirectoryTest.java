package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.crowd.directory.ApacheDS;
import com.atlassian.crowd.directory.ApacheDS15;
import com.atlassian.crowd.directory.AppleOpenDirectory;
import com.atlassian.crowd.directory.FedoraDS;
import com.atlassian.crowd.directory.GenericLDAP;
import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.directory.MicrosoftActiveDirectory;
import com.atlassian.crowd.directory.OpenLDAP;
import com.atlassian.crowd.directory.OpenLDAPRfc2307;
import com.atlassian.crowd.directory.SunONE;

import com.google.common.collect.ImmutableList;
import net.sourceforge.jwebunit.api.IElement;

/**
 * Test to look at adding multiple directory types
 * Roles are disabled by default
 */
public class AddDirectoryTest extends CrowdAcceptanceTestCase
{
    private static final String INTERNAL_DIRECTORY_NAME = "Second Internal Directory";
    private static final String INTERNAL_DIRECTORY_DESCRIPTION = "Second Internal Directory Description";
    private static final String CROWD_DIRECTORY_NAME = "Test Remote Crowd Directory";
    private static final String CROWD_DIRECTORY_DESCRIPTION = "Test Remote Crowd Directory Description";
    private static final String CONNECTOR_DIRECTORY_NAME = "Test Connector Directory";
    private static final String CONNECTOR_DIRECTORY_DESCRIPTION = "Test Connector Directory Description";
    private static final String CUSTOM_DIRECTORY_NAME = "Test Custom Remote Directory";
    private static final String DELEGATED_DIRECTORY_NAME = "Test Delegated Remote Directory";
    private static final String DELEGATED_DIRECTORY_DESCRIPTION = "Test Delegated Remote Directory Description";
    private static final String BAD_CLASS_NAME = "com.atlassian.crowd.integration.connector.ApacheDS";
    private static final String INTERNAL_DIRECTORY_PASSWORD_COMPLEXITY_DESCRIPTION = "Test Password Complexity Description";
    private static final String INTERNAL_DIRECTORY_PASSWORD_REGEX="[a-z]";

    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreBaseSetup();
    }

    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testAddInternalDirectory()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-internal");

        assertKeyPresent("directoryinternalcreate.title");

        setWorkingForm("directoryinternal");

        setTextField("name", INTERNAL_DIRECTORY_NAME);
        setTextField("description", INTERNAL_DIRECTORY_DESCRIPTION);
        setTextField("passwordRegex", INTERNAL_DIRECTORY_PASSWORD_REGEX);
        setTextField("passwordComplexityMessage", INTERNAL_DIRECTORY_PASSWORD_COMPLEXITY_DESCRIPTION);

        submit();

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(INTERNAL_DIRECTORY_NAME));

        assertTextFieldEquals("name", INTERNAL_DIRECTORY_NAME);

        assertTextFieldEquals("directoryDescription", INTERNAL_DIRECTORY_DESCRIPTION);

        assertTextPresent(InternalDirectory.DESCRIPTIVE_NAME);
    }

    public void testAddInternalDirectoryHasAtlassianSecurityAsDefaultEncryption()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-internal");

        assertKeyPresent("directoryinternalcreate.title");

        setWorkingForm("directoryinternal");

        assertSelectedOptionEquals("userEncryptionMethod", "ATLASSIAN-SECURITY");
        setTextField("name", INTERNAL_DIRECTORY_NAME);

        submit();

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(INTERNAL_DIRECTORY_NAME));

        clickLinkWithExactText("Configuration");

        assertTextPresent("ATLASSIAN-SECURITY");
    }

    public void testAddRemoteCrowdDirectory_ShouldTestConnectionBeforeSaving()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-crowd");

        assertKeyPresent("directorycrowdcreate.title");

        setWorkingForm("directorycrowd");

        // Details Tab
        setTextField("name", CROWD_DIRECTORY_NAME);
        setTextField("description", CROWD_DIRECTORY_DESCRIPTION);

        // Connector Tab
        clickLinkWithExactText("Connection");
        setTextField("url", getBaseUrl());
        setTextField("applicationName", "bad-application-name");
        setTextField("applicationPassword", "password");
        setTextField("pollingIntervalInMin", "60");

        submit();

        assertKeyPresent("directorycrowd.testconnection.invalid");

        setTextField("applicationName", "demo"); // good application name

        submit(); // all the other fields, including password, should be retained

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(CROWD_DIRECTORY_NAME));

        setWorkingForm("updateGeneral");

        assertTextFieldEquals("name", CROWD_DIRECTORY_NAME);
        assertTextFieldEquals("directoryDescription", CROWD_DIRECTORY_DESCRIPTION);

        clickLink("crowd-connectiondetails");

        assertTextFieldEquals("url", getBaseUrl());
        assertTextFieldEquals("applicationName", "demo");
        assertTextFieldEquals("pollingIntervalInMin", "60");
    }

    public void testAddRemoteCrowdDirectory_ManualConnectionTestShouldRetainPassword()
    {
        gotoCreateDirectory();

        clickButton("create-crowd");

        assertKeyPresent("directorycrowdcreate.title");

        setWorkingForm("directorycrowd");

        // Details Tab
        setTextField("name", CROWD_DIRECTORY_NAME);
        setTextField("description", CROWD_DIRECTORY_DESCRIPTION);

        // Connector Tab
        clickLinkWithExactText("Connection");
        setTextField("url", getBaseUrl());
        setTextField("applicationName", "bad-application-name");
        setTextField("applicationPassword", "password");
        setTextField("pollingIntervalInMin", "60");

        clickButton("test-connection");

        assertKeyPresent("directorycrowd.testconnection.invalid");

        setTextField("applicationName", "demo"); // good application name

        clickButton("test-connection"); // all the other fields, including password, should be retained

        assertKeyPresent("directorycrowd.testconnection.success");
    }

    /**
     * Tests that adding an inactive directory will not show any information about the synchronisation including the
     * "Synchronise now" button.
     */
    public void testAddInactiveRemoteCrowdDirectory()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-crowd");

        assertKeyPresent("directorycrowdcreate.title");

        setWorkingForm("directorycrowd");

        // Details Tab
        setTextField("name", CROWD_DIRECTORY_NAME);
        setTextField("description", CROWD_DIRECTORY_DESCRIPTION);
        uncheckCheckbox("active");

        // Connector Tab
        clickLinkWithExactText("Connection");
        setTextField("url", getBaseUrl());
        setTextField("applicationName", "demo");
        setTextField("applicationPassword", "password");
        setTextField("pollingIntervalInMin", "60");

        submit();

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(CROWD_DIRECTORY_NAME));

        setWorkingForm("updateGeneral");

        assertElementNotPresent("lastsyncinfo");
        assertButtonNotPresent("synchroniseDirectoryButton");
    }

    public void testAddRemoteCrowdDirectoryWithVariousOptions()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-crowd");

        assertKeyPresent("directorycrowdcreate.title");

        setWorkingForm("directorycrowd");

        // Details Tab
        setTextField("name", CROWD_DIRECTORY_NAME);
        setTextField("description", CROWD_DIRECTORY_DESCRIPTION);
        // Disabled by default
        assertCheckboxNotSelected("useNestedGroups");
        checkCheckbox("useNestedGroups");

        // Connector Tab
        clickLinkWithExactText("Connection");
        setTextField("url", getBaseUrl());
        setTextField("applicationName", "demo");
        setTextField("applicationPassword", "password");
        setTextField("pollingIntervalInMin", "60");
        setTextField("httpTimeout", "12");
        setTextField("httpMaxConnections", "123");
        setTextField("httpProxyPort", "5432");
        setTextField("httpProxyUsername", "proxyuser");

        submit();

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(CROWD_DIRECTORY_NAME));
        assertCheckboxSelected("useNestedGroups");

        clickLinkWithExactText("Connection");
        assertTextFieldEquals("httpTimeout", "12");
        assertTextFieldEquals("httpMaxConnections", "123");
        assertTextFieldEquals("httpProxyPort", "5432");
        assertTextFieldEquals("httpProxyUsername", "proxyuser");
    }

    private void setConnectorFieldsForLocalLdapDirectory()
    {
        selectOption("connector", ApacheDS.getStaticDirectoryType());
        setTextField("URL", "ldap://localhost:11389/");
        setTextField("baseDN", "dc=example,dc=com");
        setTextField("userDN", "uid=admin,ou=system");
        setTextField("ldapPassword", "secret");
    }

    public void testAddConnectorSetDefaultValuesForConfigurationProperties()
    {
        gotoCreateDirectory();

        clickButton("create-connector");

        clickLinkWithExactText("Connector");
        selectOption("connector", MicrosoftActiveDirectory.getStaticDirectoryType());

        clickLinkWithExactText("Configuration");

        // user attributes (from microsoftactivedirectory.properties)
        assertTextFieldEquals("userFirstnameAttr", "givenName");
        assertTextFieldEquals("userGroupMemberAttr", "memberOf");
        assertTextFieldEquals("userMailAttr", "mail");
        assertTextFieldEquals("userNameAttr", "sAMAccountName");
        assertTextFieldEquals("userNameRdnAttr", "cn");
        assertTextFieldEquals("userLastnameAttr", "sn");
        assertTextFieldEquals("userDisplayNameAttr", "displayName");
        assertTextFieldEquals("userObjectClass", "user");
        assertTextFieldEquals("userPasswordAttr", "unicodePwd");
        assertTextFieldEquals("userObjectFilter", "(&(objectCategory=Person)(sAMAccountName=*))");
        assertTextFieldEquals("userExternalIdAttr", "objectGUID");

        // group attributes (from microsoftactivedirectory.properties)
        assertTextFieldEquals("groupDescriptionAttr", "description");
        assertTextFieldEquals("groupMemberAttr", "member");
        assertTextFieldEquals("groupNameAttr", "cn");
        assertTextFieldEquals("groupObjectClass", "group");
        assertTextFieldEquals("groupObjectFilter", "(objectCategory=Group)");
    }

    public void testAddConnectorDirectoryWithUsernameAndPassword()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        setWorkingForm("directoryconnector");

        // Details Tab
        setTextField("name", CONNECTOR_DIRECTORY_NAME);
        setTextField("description", CONNECTOR_DIRECTORY_DESCRIPTION);

        // Connector Tab
        clickLinkWithExactText("Connector");
        setConnectorFieldsForLocalLdapDirectory();
        setTextField("readTimeoutInSec", "42000");
        checkCheckbox("localGroupsEnabled");

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("groupDNaddition", "ou=groups");
        setTextField("userDNaddition", "ou=users");
        submit();

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(CONNECTOR_DIRECTORY_NAME));

        setWorkingForm("updateGeneral");

        assertTextFieldEquals("name", CONNECTOR_DIRECTORY_NAME);

        assertTextFieldEquals("directoryDescription", CONNECTOR_DIRECTORY_DESCRIPTION);

        assertTextPresent(ApacheDS.getStaticDirectoryType());

        clickLink("connector-connectiondetails");

        assertTextFieldEquals("URL", "ldap://localhost:11389/");

        assertTextFieldEquals("baseDN", "dc=example,dc=com");

        assertTextFieldEquals("userDN", "uid=admin,ou=system");

        assertCheckboxSelected("localGroupsEnabled");

        assertTextFieldEquals("readTimeoutInSec", "42000");

        assertTextFieldEquals("searchTimeoutInSec", "60");

        assertTextFieldEquals("connectionTimeoutInSec", "10");

        clickLink("connector-configuration");

        assertTextFieldEquals("groupDNaddition", "ou=groups");

        assertTextFieldEquals("userDNaddition", "ou=users");

        assertCheckboxNotPresent("rolesDisabled");
    }

    public void testAddConnectorDirectoryWithAnonymousAccess()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        setWorkingForm("directoryconnector");

        // Details Tab
        setTextField("name", CONNECTOR_DIRECTORY_NAME);
        setTextField("description", CONNECTOR_DIRECTORY_DESCRIPTION);

        // Connector Tab
        clickLinkWithExactText("Connector");
        selectOption("connector", ApacheDS.getStaticDirectoryType());
        setTextField("URL", "ldap://localhost:11389/");
        setTextField("baseDN", "dc=example,dc=com");
        // userDN and ldapPassword are left empty

        submit();

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(CONNECTOR_DIRECTORY_NAME));

        setWorkingForm("updateGeneral");

        assertTextFieldEquals("name", CONNECTOR_DIRECTORY_NAME);

        assertTextFieldEquals("directoryDescription", CONNECTOR_DIRECTORY_DESCRIPTION);

        assertTextPresent(ApacheDS.getStaticDirectoryType());

        clickLink("connector-connectiondetails");

        assertTextFieldEquals("URL", "ldap://localhost:11389/");

        assertTextFieldEquals("baseDN", "dc=example,dc=com");

        assertTextFieldEquals("userDN", "");
    }

    public void testTestConfigurationShouldPersistPassword()
    {
        intendToModifyData();

        gotoCreateDirectory();
        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        setTextField("name", CONNECTOR_DIRECTORY_NAME);

        // Goto the connection configuration tab
        clickLinkWithExactText("Connector");
        setWorkingForm("directoryconnector");

        //configure the connector with basic settings
        selectOption("connector", OpenLDAP.getStaticDirectoryType());
        setTextField("URL", "ldap://localhost:11389/");
        // Select Box showed only if the connector is for OpenLDAP or Generic
        selectOption("userEncryptionMethod", "MD5");
        setTextField("baseDN", "dc=example,dc=com");
        setTextField("userDN", "uid=admin,ou=system");
        setTextField("ldapPassword", "secret");

        //test connection
        clickButton("test-connection");
        assertKeyPresent("directoryconnector.testconnection.success");

        //recheck the connection to see if the password is persisted.
        clickButton("test-connection");
        assertKeyPresent("directoryconnector.testconnection.success");
    }
    /**
     * Tests that adding an inactive directory will not show any information about the synchronisation including the
     * "Synchronise now" button.
     */
    public void testAddInactiveConnectorDirectory()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        setWorkingForm("directoryconnector");

        // Details Tab
        setTextField("name", CONNECTOR_DIRECTORY_NAME);
        setTextField("description", CONNECTOR_DIRECTORY_DESCRIPTION);
        uncheckCheckbox("active");

        // Connector Tab
        clickLinkWithExactText("Connector");
        setConnectorFieldsForLocalLdapDirectory();
        setTextField("readTimeoutInSec", "42000");

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("groupDNaddition", "ou=groups");
        setTextField("userDNaddition", "ou=users");
        submit();

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(CONNECTOR_DIRECTORY_NAME));

        setWorkingForm("updateGeneral");

        assertElementNotPresent("lastsyncinfo");
        assertButtonNotPresent("synchroniseDirectoryButton");
    }

    public void testAddConnectorDirectoryWithPwdEncryption()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        setWorkingForm("directoryconnector");

        // Details Tab
        setTextField("name", CONNECTOR_DIRECTORY_NAME);
        setTextField("description", CONNECTOR_DIRECTORY_DESCRIPTION);

        // Connector Tab
        clickLinkWithExactText("Connector");
        // The Generic connector will allow to use ApacheDS to test the Password Encryption Select Box
        selectOption("connector", GenericLDAP.getStaticDirectoryType());
        setTextField("URL", "ldap://localhost:11389/");
        // Select Box showed only if the connector is for OpenLDAP or Generic
        selectOption("userEncryptionMethod", "MD5");
        setTextField("baseDN", "dc=example,dc=com");
        setTextField("userDN", "uid=admin,ou=system");
        setTextField("ldapPassword", "secret");

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("groupDNaddition", "ou=groups");
        setTextField("userDNaddition", "ou=users");

        submit();

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(CONNECTOR_DIRECTORY_NAME));

        setWorkingForm("updateGeneral");

        assertTextFieldEquals("name", CONNECTOR_DIRECTORY_NAME);

        assertTextFieldEquals("directoryDescription", CONNECTOR_DIRECTORY_DESCRIPTION);

        assertTextPresent(GenericLDAP.getStaticDirectoryType());

        clickLink("connector-connectiondetails");

        assertTextFieldEquals("URL", "ldap://localhost:11389/");

        assertSelectedOptionEquals("userEncryptionMethod", "MD5");

        assertTextFieldEquals("baseDN", "dc=example,dc=com");

        assertTextFieldEquals("userDN", "uid=admin,ou=system");

        clickLink("connector-configuration");

        assertTextFieldEquals("groupDNaddition", "ou=groups");

        assertTextFieldEquals("userDNaddition", "ou=users");

        assertCheckboxNotPresent("rolesDisabled");
    }

    public void testAddInternalDirectoryWithoutName()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-internal");

        assertKeyPresent("directoryinternalcreate.title");

        setWorkingForm("directoryinternal");

        setTextField("name", "");
        setTextField("description", INTERNAL_DIRECTORY_DESCRIPTION);

        submit();

        assertKeyPresent("directoryinternal.name.invalid");
    }

    public void testAddConnectorDirectoryWithoutAttributes()
    {
        gotoCreateDirectory();

        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        setWorkingForm("directoryconnector");

        // Details Tab
        setTextField("name", "");
        setTextField("description", "");

        // Connector Tab
        clickLinkWithExactText("Connector");
        selectOption("connector", ApacheDS.getStaticDirectoryType());
        setTextField("URL", "");
        setTextField("baseDN", "");
        setTextField("userDN", "");
        setTextField("ldapPassword", "");

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("groupDNaddition", "");
        setTextField("groupObjectClass", "");
        setTextField("groupObjectFilter", "");
        setTextField("groupNameAttr", "");
        setTextField("groupDescriptionAttr", "");
        setTextField("groupMemberAttr", "");

        setTextField("userDNaddition", "");
        setTextField("userObjectClass", "");
        setTextField("userObjectFilter", "");
        setTextField("userNameAttr", "");
        setTextField("userFirstnameAttr", "");
        setTextField("userLastnameAttr", "");
        setTextField("userMailAttr", "");
        setTextField("userGroupMemberAttr", "");
        setTextField("userPasswordAttr", "");
        setTextField("userExternalIdAttr", "");

        submit();

        // Details Tab
        assertKeyPresent("directoryinternal.name.invalid");

        // Connector Tab
        clickLinkWithExactText("Connector");
        assertKeyPresent("directoryconnector.url.invalid");
        assertKeyPresent("directoryconnector.basedn.invalid.blank");

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        assertKeyPresent("directoryconnector.groupobjectclass.invalid");
        assertKeyPresent("directoryconnector.groupobjectfilter.invalid");
        assertKeyPresent("directoryconnector.groupname.invalid");
        assertKeyPresent("directoryconnector.groupmember.invalid");
        assertKeyPresent("directoryconnector.groupdescription.invalid");

        assertCheckboxNotPresent("rolesDisabled");

        assertKeyPresent("directoryconnector.userobjectclass.invalid");
        assertKeyPresent("directoryconnector.userobjectfilter.invalid");
        assertKeyPresent("directoryconnector.usernameattribute.invalid");
        assertKeyPresent("directoryconnector.userfirstnameattribute.invalid");
        assertKeyPresent("directoryconnector.userlastnameattribute.invalid");
        assertKeyPresent("directoryconnector.usermailattribute.invalid");
        assertKeyPresent("directoryconnector.usermemberofattribute.invalid");
        assertKeyPresent("directoryconnector.userpassword.invalid");
    }

    public void testAddConnectorDirectoryWithBadDnShowsSpecificError()
    {
        gotoCreateDirectory();

        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        clickLinkWithExactText("Connector");
        selectOption("connector", ApacheDS.getStaticDirectoryType());
        setTextField("baseDN", "invalid");
        submit();

        clickLinkWithExactText("Connector");
        assertKeyPresent("directoryconnector.basedn.invalid");
    }

    public void testAddConnectorDirectoryWithUseUserMembershipAttribute()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        setWorkingForm("directoryconnector");

        // Details Tab
        setTextField("name", "1");
        setTextField("description", "2");

        // Connector Tab
        clickLinkWithExactText("Connector");
        setConnectorFieldsForLocalLdapDirectory();

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        checkCheckbox("useUserMembershipAttribute");   // using "memberOf"-type attribute

        setTextField("groupDNaddition", "7");
        setTextField("groupObjectClass", "8");
        setTextField("groupObjectFilter", "9");
        setTextField("groupNameAttr", "10");
        setTextField("groupDescriptionAttr", "11");
        setTextField("groupMemberAttr", "12");

        setTextField("userDNaddition", "19");
        setTextField("userObjectClass", "20");
        setTextField("userObjectFilter", "21");
        setTextField("userNameAttr", "22");
        setTextField("userFirstnameAttr", "23");
        setTextField("userLastnameAttr", "24");
        setTextField("userMailAttr", "25");
        setTextField("userGroupMemberAttr", "26");
        setTextField("userPasswordAttr", "27");

        submit();

        assertWarningNotPresent();
        assertErrorNotPresent();

        // The View Directory structure, while looking similar to Add Directory, is actually a separate page for each
        //  tab.
        assertTextFieldEquals("name", "1");
        assertTextFieldEquals("directoryDescription", "2");

        clickLink("connector-connectiondetails");

        assertTextFieldEquals("URL", "ldap://localhost:11389/");
        assertTextFieldEquals("baseDN", "dc=example,dc=com");
        assertTextFieldEquals("userDN", "uid=admin,ou=system");
        //   assertTextFieldEquals("ldapPassword", "6");   <-- will not be shown - it's a password! :-)

        assertCheckboxSelected("useUserMembershipAttribute");

        clickLink("connector-configuration");

        assertTextFieldEquals("groupDNaddition", "7");
        assertTextFieldEquals("groupObjectClass", "8");
        assertTextFieldEquals("groupObjectFilter", "9");
        assertTextFieldEquals("groupNameAttr", "10");
        assertTextFieldEquals("groupDescriptionAttr", "11");
        assertTextFieldEquals("groupMemberAttr", "12");

        assertTextFieldEquals("userDNaddition", "19");
        assertTextFieldEquals("userObjectClass", "20");
        assertTextFieldEquals("userObjectFilter", "21");
        assertTextFieldEquals("userNameAttr", "22");
        assertTextFieldEquals("userFirstnameAttr", "23");
        assertTextFieldEquals("userLastnameAttr", "24");
        assertTextFieldEquals("userMailAttr", "25");
        assertTextFieldEquals("userGroupMemberAttr", "26");
        assertTextFieldEquals("userPasswordAttr", "27");


        assertCheckboxNotPresent("rolesDisabled"); // Roles disabled by default
    }

    public void testAddConnectorDirectoryWithUseUserMembershipAttributeForGroupMembership()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        setWorkingForm("directoryconnector");

        // Details Tab
        setTextField("name", "1");
        setTextField("description", "2");

        // Connector Tab
        clickLinkWithExactText("Connector");
        setConnectorFieldsForLocalLdapDirectory();

        checkCheckbox("useUserMembershipAttributeForGroupMembership");   // using "memberOf"-type attribute for AD only

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("groupDNaddition", "7");
        setTextField("groupObjectClass", "8");
        setTextField("groupObjectFilter", "9");
        setTextField("groupNameAttr", "10");
        setTextField("groupDescriptionAttr", "11");
        setTextField("groupMemberAttr", "12");

        setTextField("userDNaddition", "19");
        setTextField("userObjectClass", "20");
        setTextField("userObjectFilter", "21");
        setTextField("userNameAttr", "22");
        setTextField("userFirstnameAttr", "23");
        setTextField("userLastnameAttr", "24");
        setTextField("userMailAttr", "25");
        setTextField("userGroupMemberAttr", "26");
        setTextField("userPasswordAttr", "27");

        submit();

        assertWarningNotPresent();
        assertErrorNotPresent();

        // The View Directory structure, while looking similar to Add Directory, is actually a separate page for each
        //  tab.
        assertTextFieldEquals("name", "1");
        assertTextFieldEquals("directoryDescription", "2");

        clickLink("connector-connectiondetails");

        assertTextFieldEquals("URL", "ldap://localhost:11389/");
        assertTextFieldEquals("baseDN", "dc=example,dc=com");
        assertTextFieldEquals("userDN", "uid=admin,ou=system");
        //   assertTextFieldEquals("ldapPassword", "6");   <-- will not be shown - it's a password! :-)

        assertCheckboxNotSelected("useUserMembershipAttribute");

        clickLink("connector-configuration");

        assertTextFieldEquals("groupDNaddition", "7");
        assertTextFieldEquals("groupObjectClass", "8");
        assertTextFieldEquals("groupObjectFilter", "9");
        assertTextFieldEquals("groupNameAttr", "10");
        assertTextFieldEquals("groupDescriptionAttr", "11");
        assertTextFieldEquals("groupMemberAttr", "12");

        assertTextFieldEquals("userDNaddition", "19");
        assertTextFieldEquals("userObjectClass", "20");
        assertTextFieldEquals("userObjectFilter", "21");
        assertTextFieldEquals("userNameAttr", "22");
        assertTextFieldEquals("userFirstnameAttr", "23");
        assertTextFieldEquals("userLastnameAttr", "24");
        assertTextFieldEquals("userMailAttr", "25");
        assertTextFieldEquals("userGroupMemberAttr", "26");
        assertTextFieldEquals("userPasswordAttr", "27");


        assertCheckboxNotPresent("rolesDisabled"); // Roles disabled by default
    }

    public void testAddConnectorDirectoryTestSearch()
    {
        gotoCreateDirectory();

        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        clickLinkWithExactText("Connector");

        setWorkingForm("directoryconnector");

        // Connector Tab
        clickLinkWithExactText("Connector");
        setConnectorFieldsForLocalLdapDirectory();

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("groupDNaddition", "ou=groups");
        setTextField("groupObjectClass", "groupOfNames");
        setTextField("groupObjectFilter", "(objectclass=groupOfNames)");

        clickButton("test-search-group");

        assertKeyPresent("directoryconnector.testsearch.success");
    }

    public void testAddConnectorDirectoryTestSearchInvalid()
    {
        gotoCreateDirectory();

        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        clickLinkWithExactText("Connector");

        setWorkingForm("directoryconnector");

        // Connector Tab
        clickLinkWithExactText("Connector");
        setConnectorFieldsForLocalLdapDirectory();

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("groupDNaddition", "ou=nonexistent");
        clickButton("test-search-group");

        assertKeyPresent("directoryconnector.testsearch.invalid");
    }

    public void testAddCustomDirectoryWithoutAttributes()
    {
        gotoCreateDirectory();

        clickButton("create-custom");

        assertKeyPresent("directorycustomcreate.title");

        setWorkingForm("directorycustom");

        setTextField("name", "");
        setTextField("implementationClass", "");

        submit();

        assertKeyPresent("directoryinternal.name.invalid");
        assertKeyPresent("directorycustom.implementationclass.invalid");
    }

    public void testAddCustomDirectoryWithBadImplementationClass()
    {
        gotoCreateDirectory();

        clickButton("create-custom");

        assertKeyPresent("directorycustomcreate.title");

        setWorkingForm("directorycustom");

        setTextField("name", CUSTOM_DIRECTORY_NAME);
        setTextField("implementationClass", BAD_CLASS_NAME);

        submit();

        assertErrorPresent();
        assertTextPresent("Could not find a directory instance loader for directory <" + BAD_CLASS_NAME + ">");

        assertKeyPresent("directorycustom.implementationclass.invalid");
    }

    public void testAddCustomDirectory()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-custom");

        assertKeyPresent("directorycustomcreate.title");

        setWorkingForm("directorycustom");

        setTextField("name", CUSTOM_DIRECTORY_NAME);
        setTextField("implementationClass", ApacheDS.class.getCanonicalName());

        submit();

        assertTextPresent(ApacheDS.getStaticDirectoryType());
        assertTextFieldEquals("name", CUSTOM_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(CUSTOM_DIRECTORY_NAME));
    }

    public void testAddDelegatedDirectory()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-delegating");

        assertKeyPresent("directory.delegated.create.title");

        setWorkingForm("directorydelegated");

        // Details Tab
        setTextField("name", DELEGATED_DIRECTORY_NAME);
        setTextField("description", DELEGATED_DIRECTORY_DESCRIPTION);
        checkCheckbox("active");

        // Connector Tab
        clickLinkWithExactText("Connector");
        setConnectorFieldsForLocalLdapDirectory();

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("userDNaddition", "ou=users");
        submit();

        assertTextFieldEquals("name", DELEGATED_DIRECTORY_NAME);

        assertTextFieldEquals("directoryDescription", DELEGATED_DIRECTORY_DESCRIPTION);

        assertKeyPresent("directory.delegating.type.name");

        assertCheckboxSelected("active");

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(DELEGATED_DIRECTORY_NAME));

        clickLink("delegated-connectiondetails");

        assertTextPresent(ApacheDS.getStaticDirectoryType());

        assertTextFieldEquals("URL", "ldap://localhost:11389/");

        assertTextFieldEquals("baseDN", "dc=example,dc=com");

        assertTextFieldEquals("userDN", "uid=admin,ou=system");

        clickLink("delegated-configuration");

        assertTextFieldEquals("userDNaddition", "ou=users");
    }

    public void testTestConfigurationShouldPersistPasswordForDelegatedDirectory()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-delegating");

        assertKeyPresent("directory.delegated.create.title");

        setWorkingForm("directorydelegated");

        // Details Tab
        setTextField("name", DELEGATED_DIRECTORY_NAME);
        setTextField("description", DELEGATED_DIRECTORY_DESCRIPTION);
        checkCheckbox("active");

        // Connector Tab
        clickLinkWithExactText("Connector");
        // The Generic connector will allow to use ApacheDS to test the Password Encryption Select Box
        selectOption("connector", GenericLDAP.getStaticDirectoryType());
        setTextField("URL", "ldap://localhost:11389/");
        // Select Box showed only if the connector is for OpenLDAP or Generic
        selectOption("userEncryptionMethod", "MD5");
        setTextField("baseDN", "dc=example,dc=com");
        setTextField("userDN", "uid=admin,ou=system");
        setTextField("ldapPassword", "secret");

        //test connection
        clickButtonWithText("Test Connection");
        assertKeyPresent("directoryconnector.testconnection.success");

        //recheck the connection to see if the password is persisted.
        clickButtonWithText("Test Connection");
        assertKeyPresent("directoryconnector.testconnection.success");
    }

    public void testAddDelegatedDirectoryWithPwdEncryption()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-delegating");

        assertKeyPresent("directory.delegated.create.title");

        setWorkingForm("directorydelegated");

        // Details Tab
        setTextField("name", DELEGATED_DIRECTORY_NAME);
        setTextField("description", DELEGATED_DIRECTORY_DESCRIPTION);
        checkCheckbox("active");

        // Connector Tab
        clickLinkWithExactText("Connector");
        // The Generic connector will allow to use ApacheDS to test the Password Encryption Select Box
        selectOption("connector", GenericLDAP.getStaticDirectoryType());
        setTextField("URL", "ldap://localhost:11389/");
        // Select Box showed only if the connector is for OpenLDAP or Generic
        selectOption("userEncryptionMethod", "MD5");
        setTextField("baseDN", "dc=example,dc=com");
        setTextField("userDN", "uid=admin,ou=system");
        setTextField("ldapPassword", "secret");

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("userDNaddition", "ou=users");
        submit();

        assertTextFieldEquals("name", DELEGATED_DIRECTORY_NAME);

        assertTextFieldEquals("directoryDescription", DELEGATED_DIRECTORY_DESCRIPTION);

        assertKeyPresent("directory.delegating.type.name");

        assertCheckboxSelected("active");

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(DELEGATED_DIRECTORY_NAME));

        clickLink("delegated-connectiondetails");

        assertTextPresent(GenericLDAP.getStaticDirectoryType());

        assertTextFieldEquals("URL", "ldap://localhost:11389/");

        assertSelectedOptionEquals("userEncryptionMethod", "MD5");

        assertTextFieldEquals("baseDN", "dc=example,dc=com");

        assertTextFieldEquals("userDN", "uid=admin,ou=system");

        clickLink("delegated-configuration");

        assertTextFieldEquals("userDNaddition", "ou=users");
    }

    public void testAddDelegatingDirectoryWithoutAttributes()
    {
        gotoCreateDirectory();

        clickButton("create-delegating");

        assertKeyPresent("directory.delegated.create.title");

        setWorkingForm("directorydelegated");

        // Details Tab
        setTextField("name", "");
        setTextField("description", "");

        // Connector Tab
        clickLinkWithExactText("Connector");
        selectOption("connector", ApacheDS.getStaticDirectoryType());
        setTextField("URL", "");
        setTextField("baseDN", "");
        setTextField("userDN", "");
        setTextField("ldapPassword", "");

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("userDNaddition", "");
        setTextField("userObjectClass", "");
        setTextField("userObjectFilter", "");
        setTextField("userNameAttr", "");
        setTextField("userNameRdnAttr", "");
        setTextField("userDisplayNameAttr", "");
        setTextField("userFirstnameAttr", "");
        setTextField("userLastnameAttr", "");
        setTextField("userMailAttr", "");
        setTextField("userGroupMemberAttr", "");
        setTextField("userExternalIdAttr", "");

        submit();

        // Details Tab
        assertKeyPresent("directoryinternal.name.invalid");

        // Connector Tab
        clickLinkWithExactText("Connector");
        assertKeyPresent("directoryconnector.url.invalid");
        assertKeyNotPresent("directoryconnector.basedn.invalid");
        assertKeyPresent("directoryconnector.basedn.invalid.blank");

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        assertKeyPresent("directoryconnector.userobjectclass.invalid");
        assertKeyPresent("directoryconnector.userobjectfilter.invalid");
        assertKeyPresent("directoryconnector.usernameattribute.invalid");
        assertKeyPresent("directoryconnector.usernamerdnattribute.invalid");
        assertKeyPresent("directoryconnector.userfirstnameattribute.invalid");
        assertKeyPresent("directoryconnector.userlastnameattribute.invalid");
        assertKeyPresent("directoryconnector.usermailattribute.invalid");
        assertKeyPresent("directoryconnector.usermemberofattribute.invalid");
        assertKeyPresent("directoryconnector.userdisplaynameattribute.invalid");
    }

    public void testAddDelegatedDirectoryTestSearch()
    {
        gotoCreateDirectory();
        clickButton("create-delegating");

        assertKeyPresent("directory.delegated.create.title");

        setWorkingForm("directorydelegated");

        // Connector Tab
        clickLinkWithExactText("Connector");
        setConnectorFieldsForLocalLdapDirectory();

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("userDNaddition", "ou=users");

        clickButton("test-search-principal");

        assertKeyPresent("directoryconnector.testsearch.success");
    }

    public void testAddDelegatedDirectoryTestSearchInvalid()
    {
        gotoCreateDirectory();

        clickButton("create-delegating");

        assertKeyPresent("directory.delegated.create.title");

        setWorkingForm("directorydelegated");

        // Connector Tab
        clickLinkWithExactText("Connector");
        setConnectorFieldsForLocalLdapDirectory();

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("userDNaddition", "ou=nonexistent");
        clickButton("test-search-principal");

        assertKeyPresent("directoryconnector.testsearch.invalid");
    }

    public void testAddDelegatedDirectoryPageResultsEnabledForActiveDirectory()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-delegating");

        /* Configure a directory */
        setTextField("name", DELEGATED_DIRECTORY_NAME);
        setTextField("baseDN", "dc=example,dc=com");

        clickLinkWithExactText("Connector");

        assertCheckboxSelected("pagedResults");

        selectOption("connector", OpenLDAP.getStaticDirectoryType());
        assertCheckboxNotSelected("pagedResults");

        selectOption("connector", MicrosoftActiveDirectory.getStaticDirectoryType());
        assertCheckboxSelected("pagedResults");
    }

    public void testAddDelegatedDirectoryWithPagedResultsSpecified()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-delegating");

        /* Configure a directory */
        setTextField("name", DELEGATED_DIRECTORY_NAME);
        setTextField("baseDN", "dc=example,dc=com");

        clickLinkWithExactText("Connector");

        /* Check initial defaults */
        assertCheckboxSelected("pagedResults");
        assertTextFieldEquals("pagedResultsSize", "999");

        setConnectorFieldsForLocalLdapDirectory();

        checkCheckbox("pagedResults");
        setTextField("pagedResultsSize", "998");

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("userDNaddition", "ou=users");
        submit();

        /* Check the stored results */
        assertTextPresent("View Directory - " + DELEGATED_DIRECTORY_NAME);
        assertTextFieldEquals("name", DELEGATED_DIRECTORY_NAME);
        clickLinkWithExactText("Connector");
        assertCheckboxSelected("pagedResults");
        assertTextFieldEquals("pagedResultsSize", "998");
    }

    public void testAddDelegatedDirectoryWithPagedResultsNotSpecified()
    {
        intendToModifyData();

        gotoCreateDirectory();

        clickButton("create-delegating");

        /* Configure a directory */
        setTextField("name", DELEGATED_DIRECTORY_NAME);
        setTextField("baseDN", "dc=example,dc=com");

        clickLinkWithExactText("Connector");

        /* Check initial defaults */
        assertCheckboxSelected("pagedResults");
        assertTextFieldEquals("pagedResultsSize", "999");

        setConnectorFieldsForLocalLdapDirectory();

        uncheckCheckbox("pagedResults");
        setTextField("pagedResultsSize", "998");

        // Configuration Tab
        clickLinkWithExactText("Configuration");
        setTextField("userDNaddition", "ou=users");
        submit();

        /* Check the stored results */
        assertTextPresent("View Directory - " + DELEGATED_DIRECTORY_NAME);
        assertTextFieldEquals("name", DELEGATED_DIRECTORY_NAME);
        clickLinkWithExactText("Connector");
        assertCheckboxNotSelected("pagedResults");
        assertTextFieldEquals("pagedResultsSize", "");
    }

    public void testBaseDNMandatoryForAllDirectoriesExceptGenericLDAP()
    {
        gotoCreateDirectory();

        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        setWorkingForm("directoryconnector");

        // Details Tab
        setTextField("name", "Name");

        // Connector Tab
        clickLinkWithExactText("Connector");
        setTextField("URL", "");
        setTextField("baseDN", "");
        setTextField("userDN", "");
        setTextField("ldapPassword", "");

        assertBaseDnMandatoryForAllExceptGenericLDAP();
    }

    public void testBaseDnMandatoryForAllDirectoriesExceptGenericLDAPForDelegatedAuthentication()
    {
        gotoCreateDirectory();

        clickButton("create-delegating");

        assertKeyPresent("directory.delegated.create.title");

        setWorkingForm("directorydelegated");

        // Details Tab
        setTextField("name", "Name");

        // Connector Tab
        clickLinkWithExactText("Connector");
        setTextField("URL", "");
        setTextField("baseDN", "");
        setTextField("userDN", "");
        setTextField("ldapPassword", "");

        assertBaseDnMandatoryForAllExceptGenericLDAP();
    }

    private void assertBaseDnMandatoryForAllExceptGenericLDAP()
    {
        assertBaseDNMandatory(ApacheDS.getStaticDirectoryType());
        assertBaseDNMandatory(ApacheDS15.getStaticDirectoryType());
        assertBaseDNMandatory(OpenLDAP.getStaticDirectoryType());
        assertBaseDNMandatory(MicrosoftActiveDirectory.getStaticDirectoryType());
        assertBaseDNMandatory(SunONE.getStaticDirectoryType());
        assertBaseDNMandatory(FedoraDS.getStaticDirectoryType());
        assertBaseDNMandatory(OpenLDAPRfc2307.getStaticDirectoryType());
        assertBaseDNMandatory(AppleOpenDirectory.getStaticDirectoryType());
    }

    private void assertBaseDNMandatory(String connector)
    {
        selectOption("connector", connector);
        submit();
        assertUnescapedKeyPresent("directoryconnector.basedn.invalid.blank");
    }

    public void testBaseDNOptionalForGenericLDAP()
    {
        gotoCreateDirectory();

        clickButton("create-connector");

        assertKeyPresent("directoryconnectorcreate.title");

        setWorkingForm("directoryconnector");

        // Details Tab
        setTextField("name", "Name");

        // Connector Tab
        clickLinkWithExactText("Connector");
        setTextField("URL", "");
        setTextField("baseDN", "");
        setTextField("userDN", "");
        setTextField("ldapPassword", "");

        selectOption("connector", GenericLDAP.getStaticDirectoryType());
        submit();
        assertUnescapedKeyNotPresent("directoryconnector.basedn.invalid.blank");
        assertUnescapedKeyNotPresent("directoryconnector.basedn.invalid");
    }

    public void testBaseDNOptionalForGenericLDAPForDelegatedAuthentication()
    {
        gotoCreateDirectory();

        clickButton("create-delegating");

        assertKeyPresent("directory.delegated.create.title");

        setWorkingForm("directorydelegated");

        // Details Tab
        setTextField("name", "Name");

        // Connector Tab
        clickLinkWithExactText("Connector");
        setTextField("URL", "");
        setTextField("baseDN", "");
        setTextField("userDN", "");
        setTextField("ldapPassword", "");

        selectOption("connector", GenericLDAP.getStaticDirectoryType());
        submit();
        assertUnescapedKeyNotPresent("directoryconnector.basedn.invalid.blank");
        assertUnescapedKeyNotPresent("directoryconnector.basedn.invalid");
    }

    public void testOnlyActiveDirectoryHasLocalUserStatusOption()
    {
        gotoCreateDirectory();

        clickButton("create-connector");

        clickLinkWithExactText("Connector");

        IElement localUserStatusDiv = getElementById("local_user_status");

        selectOption("connector", OpenLDAP.getStaticDirectoryType());
        assertEquals("display: none;", localUserStatusDiv.getAttribute("style"));

        selectOption("connector", MicrosoftActiveDirectory.getStaticDirectoryType());
        assertEquals("display: block;", localUserStatusDiv.getAttribute("style"));
    }
}
