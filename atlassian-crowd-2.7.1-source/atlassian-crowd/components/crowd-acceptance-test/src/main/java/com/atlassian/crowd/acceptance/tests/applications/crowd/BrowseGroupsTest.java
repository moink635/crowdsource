package com.atlassian.crowd.acceptance.tests.applications.crowd;

/**
 * Test the searching of groups
 */
public class BrowseGroupsTest extends CrowdAcceptanceTestCase
{
    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("searchdirectories.xml");
    }

    public void testNoDirectoryIsSelectedByDefault()
    {
        gotoBrowseGroups();
        assertSelectedOptionEquals("directoryID" , getText("selectdirectory.label"));
    }

    public void testDirectorySelectionIsSaved()
    {
        // Save directory selection
        gotoBrowseGroups();
        selectOption("directoryID", "Second Directory");
        submit();

        // Check that the directory is selected
        gotoBrowseGroups();
        assertSelectedOptionEquals("directoryID" , "Second Directory");
        assertSelectOptionNotPresent("directoryID" , getText("selectdirectory.label"));
    }

    public void testNonExistingDirectorySelection()
    {
        // Save directory selection
        gotoBrowseGroups();
        selectOption("directoryID", "Second Directory");
        submit();

        // Remove selected directory
        gotoBrowseDirectories();
        clickLinkWithText("Second Directory");
        clickLink("remove-directory");
        submit();

        // Check that no directory is selected
        gotoBrowseGroups();
        assertSelectedOptionEquals("directoryID" , getText("selectdirectory.label"));
        assertWarningAndErrorNotPresent();
    }

    public void testOnlyDirectoryIsSelected()
    {
        gotoBrowseDirectories();

        clickLinkWithText("Apache DS");
        clickLink("remove-directory");
        submit();

        clickLinkWithText("Apache DS 1.5.4");
        clickLink("remove-directory");
        submit();

        clickLinkWithText("Second Directory");
        clickLink("remove-directory");
        submit();

        gotoBrowseGroups();

        assertSelectedOptionEquals("directoryID" , "Test Internal Directory");
    }
}
