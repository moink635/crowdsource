package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.crowd.acceptance.utils.DbCachingTestHelper;
import com.google.common.collect.Lists;

/**
 * Tests to look at updating Crowd directories
 */
public class ViewDirectoryTest extends CrowdAcceptanceTestCase
{
    private static final String INTERNAL_DIRECTORY_NAME = "Second Directory";

    private static final String CROWD_DIRECTORY_NAME = "Remote Crowd Directory";

    private static final String CONNECTOR_DIRECTORY_NAME_APACHEDS = "ApacheDS Directory";

    private static final String CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING = "ApacheDS Caching Directory";

    private static final String CONNECTOR_DIRECTORY_NAME_GENERIC = "GenericLDAP Directory";

    private static final String CUSTOM_DIRECTORY_NAME = "Custom Directory";

    private static final String DELEGATED_DIRECTORY_NAME_APACHEDS = "ApacheDS Delegated Directory";

    private static final String DELEGATED_DIRECTORY_NAME_GENERIC = "Generic Delegated Directory";

    private static final String CONNECTOR_DIRECTORY_NAME_ACTIVE_DIRECTORY = "ActiveDirectory";

    private static final long ONE_MINUTE_IN_MILLIS = 60000L;

    private static final String INTERNAL_DIRECTORY_PASSWORD_COMPLEXITY_DESCRIPTION = "Test Complexity Description";

    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreCrowdFromXML("viewdirectory.xml");
    }

    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    // =========== INTERNAL DIRECTORIES ==========================================================

    public void testUpdateInternalDirectoryGeneralScreen()
    {
        intendToModifyData();

        log("Running: testUpdateInternalDirectoryGeneralScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(INTERNAL_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", INTERNAL_DIRECTORY_NAME);

        // Update General Tab
        setWorkingForm("updateGeneral");

        setTextField("name", "Update Second Directory");
        setTextField("directoryDescription", "Update Second Directory Description");

        submit();

        assertTextFieldEquals("name", "Update Second Directory");
        assertTextFieldEquals("directoryDescription", "Update Second Directory Description");
    }

    public void testUpdateInternalDirectoryConfiguration()
    {
        intendToModifyData();

        log("Running: testUpdateInternalDirectoryGeneralScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(INTERNAL_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", INTERNAL_DIRECTORY_NAME);

        clickLink("internal-configuration");

        // Update Configuration Tab
        setWorkingForm("updateConfiguration");

        setTextField("passwordRegex", ".*");
        setTextField("passwordComplexityMessage","AwesomeRegex");
        setTextField("passwordMaxAttempts", "10");
        setTextField("passwordMaxChangeTime", "15");
        setTextField("passwordHistoryCount", "20");

        submit();

        assertTextFieldEquals("passwordRegex", ".*");
        assertTextFieldEquals("passwordComplexityMessage","AwesomeRegex");
        assertTextFieldEquals("passwordMaxAttempts", "10");
        assertTextFieldEquals("passwordMaxChangeTime", "15");
        assertTextFieldEquals("passwordHistoryCount", "20");
    }

    public void testUpdateInternalDirectoryPermissions()
    {
        intendToModifyData();

        log("Running: testUpdateInternalDirectoryGeneralScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(INTERNAL_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", INTERNAL_DIRECTORY_NAME);

        clickLink("internal-permissions");

        // Update Permissions Tab
        setWorkingForm("permissionForm");

        uncheckCheckbox("permissionGroupAdd");
        uncheckCheckbox("permissionPrincipalAdd");

        submit();

        setWorkingForm("permissionForm");
        assertCheckboxNotSelected("permissionGroupAdd");
        assertCheckboxNotSelected("permissionPrincipalAdd");

        // Update Permissions Tab again
        setWorkingForm("permissionForm");

        checkCheckbox("permissionGroupAdd");
        checkCheckbox("permissionPrincipalAdd");

        submit();

        setWorkingForm("permissionForm");
        assertCheckboxSelected("permissionGroupAdd");
        assertCheckboxSelected("permissionPrincipalAdd");
    }

    public void testUpdateInternalDirectoryGeneralScreenError()
    {
        log("Running: testUpdateInternalDirectory");

        gotoBrowseDirectories();

        clickLinkWithExactText(INTERNAL_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", INTERNAL_DIRECTORY_NAME);

        setTextField("name", "");

        submit();

        assertKeyPresent("directoryinternal.name.invalid");
    }

    public void testUpdateInternalDirectoryGeneralScreenWithDuplicateDirectoryName()
    {
        log("Running: testUpdateInternalDirectoryGeneralScreenWithDuplicateDirectoryName");

        gotoBrowseDirectories();

        clickLinkWithExactText(INTERNAL_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", INTERNAL_DIRECTORY_NAME);

        setTextField("name", DELEGATED_DIRECTORY_NAME_APACHEDS);

        submit();

        assertKeyPresent("directory.name.nonunique.invalid", DELEGATED_DIRECTORY_NAME_APACHEDS);
    }

    public void testUpdateInternalDirectoryConfigurationScreenErrors()
    {
        log("Running: testUpdateInternalDirectory");

        gotoBrowseDirectories();

        clickLinkWithExactText(INTERNAL_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", INTERNAL_DIRECTORY_NAME);

        clickLink("internal-configuration");

        // Update Configuration Tab
        setWorkingForm("updateConfiguration");

        setTextField("passwordRegex", "[]");
        setTextField("passwordMaxAttempts", "-1");
        setTextField("passwordMaxChangeTime", "-2");
        setTextField("passwordHistoryCount", "-3");

        submit();

        assertTextPresent("Unclosed character class near index 1 [] ^");
        assertKeyPresent("directoryinternal.passwordmaxchangetime.invalid");
        assertKeyPresent("directoryinternal.passwordmaxattempts.invalid");
        assertKeyPresent("directoryinternal.passwordhistorycount.invalid");
    }

    // =========== REMOTE CROWD DIRECTORIES ==========================================================

    public void testUpdateCrowdDirectoryGeneralScreen()
    {
        intendToModifyData();

        log("Running: testUpdateCrowdDirectoryGeneralScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(CROWD_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CROWD_DIRECTORY_NAME);

        // Update General Tab
        setWorkingForm("updateGeneral");

        setTextField("name", "Update Second Directory");
        setTextField("directoryDescription", "Update Second Directory Description");

        submit();

        assertTextFieldEquals("name", "Update Second Directory");
        assertTextFieldEquals("directoryDescription", "Update Second Directory Description");

    }

    public void testViewCrowdDirectorySynchroniseCache() throws Exception
    {
        intendToModifyData();

        log("running testViewCrowdDirectorySynchroniseCache");

        gotoBrowseDirectories();

        clickLinkWithExactText(CROWD_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CROWD_DIRECTORY_NAME);

        // will go back and the cache should be initialised but w/o anything in it.
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);

        assertKeyPresent("directory.caching.sync.never.label");

        DbCachingTestHelper.synchroniseDirectory(tester, CROWD_DIRECTORY_NAME, ONE_MINUTE_IN_MILLIS);
    }

    public void testUpdateCrowdDirectoryGeneralScreenError()
    {
        log("Running: testUpdateCrowdDirectoryGeneralScreenError");

        gotoBrowseDirectories();

        clickLinkWithExactText(CROWD_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CROWD_DIRECTORY_NAME);

        setTextField("name", "");

        submit();

        assertKeyPresent("directoryinternal.name.invalid");
    }

    public void testUpdateCrowdDirectoryGeneralScreenWithDuplicateDirectoryName()
    {
        log("Running: testUpdateCrowdDirectoryGeneralScreenWithDuplicateDirectoryName");

        gotoBrowseDirectories();

        clickLinkWithExactText(CROWD_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CROWD_DIRECTORY_NAME);

        setTextField("name", DELEGATED_DIRECTORY_NAME_APACHEDS);

        submit();

        assertKeyPresent("directory.name.nonunique.invalid", DELEGATED_DIRECTORY_NAME_APACHEDS);
    }

    public void testUpdateCrowdDirectoryConnectionScreen()
    {
        log("Running: testUpdateCrowdDirectoryConnectionScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(CROWD_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CROWD_DIRECTORY_NAME);

        clickLink("crowd-connectiondetails");

        setTextField("url", getBaseUrl() + "2");
        setTextField("applicationName", "jira2");
        setTextField("pollingIntervalInMin", "100");
        setTextField("httpTimeout", "12");
        setTextField("httpMaxConnections", "123");
        setTextField("httpProxyPort", "5432");
        setTextField("httpProxyUsername", "proxyuser");
        submit();

        assertTextFieldEquals("url", getBaseUrl() + "2");
        assertTextFieldEquals("applicationName", "jira2");
        assertTextFieldEquals("pollingIntervalInMin", "100");
        assertTextFieldEquals("httpTimeout", "12");
        assertTextFieldEquals("httpMaxConnections", "123");
        assertTextFieldEquals("httpProxyPort", "5432");
        assertTextFieldEquals("httpProxyUsername", "proxyuser");
    }

    public void testUpdateCrowdTestConnectionWithCredentials()
    {
        log("Running: testUpdateCrowdTestConnectionWithCredentials");

        gotoBrowseDirectories();

        clickLinkWithExactText(CROWD_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CROWD_DIRECTORY_NAME);

        clickLink("crowd-connectiondetails");

        // With credentials
        setTextField("url", getBaseUrl());
        setTextField("applicationName", "jira");
        setTextField("applicationPassword", "password");
        clickButton("test-connection");

        assertKeyPresent("directorycrowd.testconnection.success");
    }

    public void testUpdateCrowdTestConnectionInvalidNoPassword()
    {
        log("Running: testUpdateCrowdTestConnectionInvalidNoPassword");

        gotoBrowseDirectories();

        clickLinkWithExactText(CROWD_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CROWD_DIRECTORY_NAME);

        clickLink("crowd-connectiondetails");

        clickButton("test-connection");

        assertKeyPresent("directorycrowd.testconnection.invalid");
    }


    public void testUpdateCrowdTestConnectionInvalidURL()
    {
        log("Running: testUpdateCrowdTestConnectionInvalidURL");

        gotoBrowseDirectories();

        clickLinkWithExactText(CROWD_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CROWD_DIRECTORY_NAME);

        clickLink("crowd-connectiondetails");
        setTextField("url", getBaseUrl() + "/invalidUrl");

        clickButton("test-connection");

        assertKeyPresent("directorycrowd.testconnection.invalid");

    }

    public void testUpdateCrowdDirectoryConnectionScreenError()
    {
        log("Running: testUpdateCrowdDirectoryConnectionScreenError");

        gotoBrowseDirectories();

        clickLinkWithExactText(CROWD_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CROWD_DIRECTORY_NAME);

        clickLink("crowd-connectiondetails");

        setTextField("url", "");
        setTextField("applicationName", "");

        submit();

        assertKeyPresent("directorycrowd.url.invalid");
        assertKeyPresent("directorycrowd.applicationname.invalid");
    }

    public void testUpdateCrowdDirectoryPermissionScreen()
    {
        intendToModifyData();

        log("Running: testUpdateCrowdDirectoryPermissionScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(CROWD_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CROWD_DIRECTORY_NAME);

        clickLink("crowd-permissions");

        // Update Permissions Tab

        uncheckCheckbox("permissionGroupAdd");
        uncheckCheckbox("permissionPrincipalAdd");

        submit();

        assertCheckboxNotSelected("permissionGroupAdd");
        assertCheckboxNotSelected("permissionPrincipalAdd");
    }

    // =========== CONNECTOR DIRECTORIES ==========================================================

    public void testUpdateConnectorDirectory_LocalUserStatusVisibleForActiveDirectory()
    {
        log("Running: testUpdateConnectorDirectory_LocalUserStatusVisibleForActiveDirectory");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_ACTIVE_DIRECTORY);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_ACTIVE_DIRECTORY);

        clickLink("connector-connectiondetails");

        assertCheckboxPresent("localUserStatusEnabled");
    }

    public void testUpdateConnectorDirectory_LocalUserStatusNotVisibleForDirectoriesOtherThanActiveDirectory()
    {
        log("Running: testUpdateConnectorDirectory_LocalUserStatusNotVisibleForDirectoriesOtherThanActiveDirectory");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        assertCheckboxNotPresent("localUserStatusEnabled");
    }

    public void testUpdateConnectorDirectory_GeneralScreen()
    {
        intendToModifyData();

        log("Running: testUpdateConnectorDirectory_GeneralScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        // Update General Tab
        setWorkingForm("updateGeneral");

        setTextField("name", "Update Second Directory");
        setTextField("directoryDescription", "Update Second Directory Description");

        submit();

        assertTextFieldEquals("name", "Update Second Directory");
        assertTextFieldEquals("directoryDescription", "Update Second Directory Description");
    }

    public void testViewConnectorDirectory_SynchroniseCache() throws Exception
    {
        intendToModifyData();

        log("running testViewConnectorDirectory_SynchroniseCache");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);

        // will go back and the cache should be initialised but w/o anything in it.
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);

        assertKeyPresent("directory.caching.sync.never.label");

        DbCachingTestHelper.synchroniseDirectory(tester, CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING, ONE_MINUTE_IN_MILLIS);
    }

    public void testUpdateConnectorDirectory_GeneralScreenError()
    {
        log("Running: testUpdateConnectorDirectory_GeneralScreenError");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        setTextField("name", "");

        submit();

        assertKeyPresent("directoryinternal.name.invalid");
    }

    public void testUpdateConnectorDirectory_ExistingPasswordIsPreservedUnlessExplicitlyModified()
    {
        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        // just submit the form without changing anything
        submit();

        // test connection to assert that the password hasn't been changed
        clickButton("test-connection");
        assertKeyPresent("directoryconnector.testconnection.success");
    }

    public void testUpdateConnectorDirectory_GeneralScreenWithDuplicateDirectoryName()
    {
        log("Running: testUpdateConnectorDirectory_GeneralScreenWithDuplicateDirectoryName");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        setTextField("name", DELEGATED_DIRECTORY_NAME_APACHEDS);

        submit();

        assertKeyPresent("directory.name.nonunique.invalid", DELEGATED_DIRECTORY_NAME_APACHEDS);
    }

    public void testUpdateConnectorDirectory_ConnectionScreen()
    {
        intendToModifyData();

        log("Running: testUpdateConnectorDirectory_ConnectionScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        setTextField("URL", "ldap://localhost:389/");
        setTextField("baseDN", "dc=example,dc=com");
        setTextField("userDN", "uid=admin,ou=system");
        checkCheckbox("secure");
        checkCheckbox("referral");
        checkCheckbox("pagedResults");

        submit();

        assertTextFieldEquals("URL", "ldap://localhost:389/");
        assertTextFieldEquals("baseDN", "dc=example,dc=com");
        assertTextFieldEquals("userDN", "uid=admin,ou=system");
        assertCheckboxSelected("secure");
        assertCheckboxSelected("referral");
        assertCheckboxSelected("pagedResults");
    }

    public void testUpdateConnectorDirectory_ExistingPasswordIsNotRevealed()
    {
        log("Running: testUpdateConnectorDirectory_ExistingPasswordIsNotRevealed");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        assertTextFieldEquals("ldapPassword", "");
    }

    public void testUpdateConnectorDirectory_NewPasswordIsRetainedAfterConnectionTest()
    {
        intendToModifyData();

        log("Running: testUpdateConnectorDirectory_NewPasswordIsRetainedAfterConnectionTest");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        setTextField("ldapPassword", "invalid password");
        clickButton("test-connection");

        //Test connection will be invalid due to wrong credentials.
        assertWarningPresent();
        assertTextPresent("LDAP: error code 49 - Bind failed");

        //Try saving the configuration and ensure that we get a
        //invalid credentials exception which confirms that the
        //password was retained on test configuration
        submit();
        assertWarningPresent();
        assertTextPresent("LDAP: error code 49 - Bind failed");
    }

    public void testUpdateConnectorDirectory_TestConnectionWithCredentials()
    {
        intendToModifyData();

        log("Running: testUpdateConnectorDirectory_TestConnectionWithCredentials");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        // With credentials
        setTextField("userDN", "uid=admin,ou=system");
        setTextField("ldapPassword", "secret");
        clickButton("test-connection");

        assertKeyPresent("directoryconnector.testconnection.success");
    }

    public void testUpdateConnectorDirectory_TestConnectionWithAnonymousBind()
    {
        log("Running: testUpdateConnectorDirectory_TestConnectionWithAnonymousBind");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        setTextField("userDN", "");

        clickButton("test-connection");
        assertKeyPresent("directoryconnector.testconnection.success");
    }

    public void testUpdateConnectorDirectory_TestConnectionWithNoPasswordShouldUseTheSavedPassword()
    {
        intendToModifyData();

        log("Running: testUpdateConnectorDirectory_TestConnectionWithNoPasswordShouldUseTheSavedPassword");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        //Set the userDN with a valid dn & password to blank.
        setTextField("userDN","uid=admin,ou=system");
        setTextField("ldapPassword","");
        clickButton("test-connection");

        //expecting the test to pass since it reuses the existing valid password.
        assertErrorNotPresent();
        assertKeyPresent("directoryconnector.testconnection.success");
    }

    public void testUpdateConnectorDirectory_TestConnectionWithoutAnyChanges()
    {
        log("Running: testUpdateConnectorDirectory_TestConnectionWithoutAnyChanges");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        clickButton("test-connection");

        assertKeyPresent("directoryconnector.testconnection.success");
    }

    public void testUpdateConnectorDirectory_TestConnectionInvalidURL()
    {
        log("Running: testUpdateConnectorDirectory_TestConnectionInvalidURL");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");
        setTextField("URL", "ldap://localhost:389/");

        clickButton("test-connection");

        assertKeyPresent("directoryconnector.testconnection.invalid");

    }

    public void testUpdateConnectorDirectory_TestConnectionInvalidUserDN()
    {
        log("Running: testUpdateConnectorDirectory_TestConnectionInvalidUserDN");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        setTextField("userDN", "invalid user DN");

        clickButton("test-connection");
        assertKeyPresent("directoryconnector.testconnection.invalid");
    }

    public void testUpdateConnectorDirectory_TestConnectionInvalidPassword()
    {
        log("Running: testUpdateConnectorDirectory_TestConnectionInvalidPassword");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        setTextField("ldapPassword", "invalid password");

        clickButton("test-connection");
        assertKeyPresent("directoryconnector.testconnection.invalid");
    }

    public void testUpdateConnectorDirectory_PasswordIsForgottenWhenSwitchingToAnonymousBind()
    {
        log("Running: testUpdateConnectorDirectory_PasswordIsForgottenWhenSwitchingToAnonymousBind");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        // prerequisite: a working the directory connection
        clickButton("test-connection");
        assertKeyPresent("directoryconnector.testconnection.success");

        // switch to anonymous bind
        setTextField("userDN", "");

        // the connection still works with anonymous bind
        clickButton("test-connection"); // old password should be lost at this point
        assertKeyPresent("directoryconnector.testconnection.success");

        // set the valid user DN, but not the password
        setTextField("userDN", "uid=admin,ou=system");

        // the connection doesn't work because the password has been forgotten
        clickButton("test-connection");
        assertKeyPresent("directoryconnector.testconnection.invalid");

        // set also the valid password
        setTextField("ldapPassword", "secret");

        // connection is working again
        clickButton("test-connection");
        assertKeyPresent("directoryconnector.testconnection.success");
    }

    public void testUpdateConnectorDirectory_ConnectionScreenWithPwdEncryption()
    {
        intendToModifyData();

        log("Running: testUpdateConnectorDirectory_ConnectionScreenWithPwdEncryption");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_GENERIC);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_GENERIC);

        clickLink("connector-connectiondetails");

        setTextField("URL", "ldap://localhost:389/");
        selectOption("userEncryptionMethod", "DES");
        setTextField("baseDN", "dc=example,dc=com");
        setTextField("userDN", "uid=admin,ou=system");
        checkCheckbox("secure");
        checkCheckbox("referral");
        checkCheckbox("useUserMembershipAttribute");

        submit();

        assertTextFieldEquals("URL", "ldap://localhost:389/");
        assertSelectedOptionEquals("userEncryptionMethod", "DES");
        assertTextFieldEquals("baseDN", "dc=example,dc=com");
        assertTextFieldEquals("userDN", "uid=admin,ou=system");
        assertCheckboxSelected("secure");
        assertCheckboxSelected("referral");
        assertCheckboxSelected("pagedResults");
        assertCheckboxSelected("useUserMembershipAttribute");
    }

    public void testUpdateConnectorDirectory_ConnectionScreenError()
    {
        log("Running: testUpdateConnectorDirectory_ConnectionScreenError");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        setTextField("URL", "");
        setTextField("baseDN", "");

        submit();

        assertKeyPresent("directoryconnector.url.invalid");
        assertKeyPresent("directoryconnector.basedn.invalid.blank");
    }

    public void testUpdateConnectorDirectory_ConnectionScreenErrorWithBadDn()
    {
        log("Running: testUpdateConnectorDirectory_ConnectionScreenErrorWithBadDn");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        setTextField("URL", "");
        setTextField("baseDN", "invalid");

        submit();

        assertKeyPresent("directoryconnector.url.invalid");
        assertKeyPresent("directoryconnector.basedn.invalid");
    }

    public void testUpdateConnectorDirectory_ConfigurationScreen()
    {
        intendToModifyData();

        log("Running: testUpdateConnectorDirectory_ConfigurationScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-configuration");

        setTextField("groupDNaddition", "1");
        setTextField("groupObjectClass", "2");
        setTextField("groupObjectFilter", "3");
        setTextField("groupNameAttr", "4");
        setTextField("groupDescriptionAttr", "5");
        setTextField("groupMemberAttr", "6");

        assertCheckboxNotPresent("rolesDisabled");

        setTextField("userDNaddition", "13");
        setTextField("userObjectClass", "14");
        setTextField("userObjectFilter", "15");
        setTextField("userNameAttr", "16");
        setTextField("userNameRdnAttr", "17");
        setTextField("userFirstnameAttr", "18");
        setTextField("userLastnameAttr", "19");
        setTextField("userMailAttr", "20");
        setTextField("userGroupMemberAttr", "21");
        setTextField("userPasswordAttr", "22");
        setTextField("userDisplayNameAttr", "23");

        submit();

        assertTextFieldEquals("groupDNaddition", "1");
        assertTextFieldEquals("groupObjectClass", "2");
        assertTextFieldEquals("groupObjectFilter", "3");
        assertTextFieldEquals("groupNameAttr", "4");
        assertTextFieldEquals("groupDescriptionAttr", "5");
        assertTextFieldEquals("groupMemberAttr", "6");

        assertCheckboxNotPresent("rolesDisabled");

        assertTextFieldEquals("userDNaddition", "13");
        assertTextFieldEquals("userObjectClass", "14");
        assertTextFieldEquals("userObjectFilter", "15");
        assertTextFieldEquals("userNameAttr", "16");
        assertTextFieldEquals("userNameRdnAttr", "17");
        assertTextFieldEquals("userFirstnameAttr", "18");
        assertTextFieldEquals("userLastnameAttr", "19");
        assertTextFieldEquals("userMailAttr", "20");
        assertTextFieldEquals("userGroupMemberAttr", "21");
        assertTextFieldEquals("userPasswordAttr", "22");
        assertTextFieldEquals("userDisplayNameAttr", "23");
    }

    public void testUpdateConnectorDirectory_ConfigurationScreenError()
    {
        log("Running: testUpdateConnectorDirectory_ConfigurationScreenError");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-configuration");

        setTextField("groupDNaddition", "");
        setTextField("groupObjectClass", "");
        setTextField("groupObjectFilter", "");
        setTextField("groupNameAttr", "");
        setTextField("groupDescriptionAttr", "");
        setTextField("groupMemberAttr", "");

        setTextField("userDNaddition", "");
        setTextField("userObjectClass", "");
        setTextField("userObjectFilter", "");
        setTextField("userNameAttr", "");
        setTextField("userNameRdnAttr", "");
        setTextField("userFirstnameAttr", "");
        setTextField("userLastnameAttr", "");
        setTextField("userDisplayNameAttr", "");
        setTextField("userMailAttr", "");
        setTextField("userGroupMemberAttr", "");
        setTextField("userPasswordAttr", "");

        submit();

        assertKeyPresent("directoryconnector.groupobjectclass.invalid");
        assertKeyPresent("directoryconnector.groupobjectfilter.invalid");
        assertKeyPresent("directoryconnector.groupname.invalid");
        assertKeyPresent("directoryconnector.groupmember.invalid");
        assertKeyPresent("directoryconnector.groupdescription.invalid");

        assertKeyPresent("directoryconnector.userobjectclass.invalid");
        assertKeyPresent("directoryconnector.userobjectfilter.invalid");
        assertKeyPresent("directoryconnector.usernameattribute.invalid");
        assertKeyPresent("directoryconnector.usernamerdnattribute.invalid");
        assertKeyPresent("directoryconnector.userfirstnameattribute.invalid");
        assertKeyPresent("directoryconnector.userlastnameattribute.invalid");
        assertKeyPresent("directoryconnector.userdisplaynameattribute.invalid");
        assertKeyPresent("directoryconnector.usermailattribute.invalid");
        assertKeyPresent("directoryconnector.usermemberofattribute.invalid");
        assertKeyPresent("directoryconnector.userpassword.invalid");
    }

    public void testUpdateConnectorDirectory_ConfigurationSearchGroup()
    {
        intendToModifyData();

        log("Running: testUpdateConnectorDirectory_ConfigurationSearchGroup");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-configuration");

        setTextField("groupObjectClass", "groupOfNames");
        setTextField("groupObjectFilter", "(objectclass=groupOfNames)");

        setTextField("groupDNaddition", "");

        clickButton("test-search-group");

        assertKeyNotPresent("directoryconnector.testsearch.invalid");
    }

    public void testUpdateConnectorDirectory_SearchPrincipal()
    {
        intendToModifyData();

        log("Running: testUpdateConnectorDirectory_SearchPrincipal");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-configuration");
        setTextField("userDNaddition", "");

        clickButton("test-search-principal");

        assertKeyNotPresent("directoryconnector.testsearch.invalid");
    }

    public void testUpdateConnectorDirectory_SearchGroupInvalid()
    {
        log("Running: testUpdateConnectorDirectory_SearchGroupInvalid");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-configuration");

        setTextField("groupDNaddition", "ou=nonexistentgroup");

        clickButton("test-search-group");

        assertKeyPresent("directoryconnector.testsearch.invalid");
    }

    public void testUpdateConnectorDirectory_SearchPrincipalInvalid()
    {
        log("Running: testUpdateConnectorDirectory_SearchPrincipalInvalid");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-configuration");
        setTextField("userDNaddition", "ou=nonexistentprincipal");

        clickButton("test-search-principal");

        assertKeyPresent("directoryconnector.testsearch.invalid");
    }


    public void testUpdateConnectorDirectory_PermissionScreen()
    {
        intendToModifyData();

        log("Running: testUpdateConnectorDirectory_PermissionScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-permissions");

        // Update Permissions Tab

        uncheckCheckbox("permissionGroupAdd");
        uncheckCheckbox("permissionPrincipalAdd");

        submit();

        assertCheckboxNotSelected("permissionGroupAdd");
        assertCheckboxNotSelected("permissionPrincipalAdd");
    }

    // =========== DELEGATED DIRECTORIES ==========================================================

    public void testUpdateDelegatedDirectory_GeneralScreen()
    {
        intendToModifyData();

        log("Running: testUpdateDelegatedDirectory_GeneralScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        // Update General Tab
        setTextField("name", "Update Delegated Directory");
        setTextField("directoryDescription", "Update Delegated Directory Description");

        submit();

        assertTextFieldEquals("name", "Update Delegated Directory");
        assertTextFieldEquals("directoryDescription", "Update Delegated Directory Description");

    }

    public void testUpdateDelegatedDirectory_GeneralScreenError()
    {
        log("Running: testUpdateDelegatedDirectory_GeneralScreenError");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        setTextField("name", "");

        submit();

        assertKeyPresent("directoryinternal.name.invalid");
    }

    public void testUpdateDelegatedDirectory_GeneralScreenWithDuplicateDirectoryName()
    {
        log("Running: testUpdateDelegatedDirectory_GeneralScreenWithDuplicateDirectoryName");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        setTextField("name", INTERNAL_DIRECTORY_NAME);

        submit();

        assertKeyPresent("directory.name.nonunique.invalid", INTERNAL_DIRECTORY_NAME);
    }

    public void testUpdateDelegatedDirectory_ConnectionScreen()
    {
        intendToModifyData();

        log("Running: testUpdateDelegatedDirectory_ConnectionScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-connectiondetails");

        assertTextFieldEquals("URL", "ldap://localhost:11389/");
        assertTextFieldEquals("baseDN", "dc=example,dc=com");
        assertTextFieldEquals("userDN", "uid=admin,ou=system");
        assertTextFieldEquals("ldapPassword", ""); // do not reveal current password
        assertCheckboxNotSelected("secure");
        assertCheckboxNotSelected("referral");
        assertCheckboxNotSelected("updateUsers");
        assertCheckboxNotSelected("importGroups");

        setTextField("URL", "ldap://localhost:389/");
        setTextField("baseDN", "dc=example,dc=com");
        setTextField("userDN", "uid=admin,ou=system");
        checkCheckbox("secure");
        checkCheckbox("referral");
        checkCheckbox("updateUsers");
        checkCheckbox("importGroups");

        submit();

        assertTextFieldEquals("URL", "ldap://localhost:389/");
        assertTextFieldEquals("baseDN", "dc=example,dc=com");
        assertTextFieldEquals("userDN", "uid=admin,ou=system");
        assertCheckboxSelected("secure");
        assertCheckboxSelected("referral");
        assertCheckboxSelected("updateUsers");
        assertCheckboxSelected("importGroups");
    }

    public void testUpdateDelegatedDirectory_NestedGroupsCanBeEnabledAndDisabled()
    {
        intendToModifyData();

        log("Running: testUpdateDelegatedDirectory_NestedGroupsCanBeEnabledAndDisabled");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-connectiondetails");

        assertCheckboxNotSelected("useNestedGroups");

        checkCheckbox("useNestedGroups");
        submit();

        assertWarningAndErrorNotPresent();
        assertCheckboxSelected("useNestedGroups");

        uncheckCheckbox("useNestedGroups");
        submit();

        assertWarningAndErrorNotPresent();
        assertCheckboxNotSelected("useNestedGroups");
    }

    public void testUpdateDelegatedDirectory_ConnectionScreenWithPwdEncryption()
    {
        intendToModifyData();

        log("Running: testUpdateDelegatedDirectory_ConnectionScreenWithPwdEncryption");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_GENERIC);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_GENERIC);

        clickLink("delegated-connectiondetails");
        setTextField("URL", "ldap://localhost:389/");
        selectOption("userEncryptionMethod", "DES");
        setTextField("baseDN", "dc=example,dc=com");
        setTextField("userDN", "uid=admin,ou=system");
        checkCheckbox("secure");
        checkCheckbox("referral");

        submit();

        assertTextFieldEquals("URL", "ldap://localhost:389/");
        assertSelectedOptionEquals("userEncryptionMethod", "DES");
        assertTextFieldEquals("baseDN", "dc=example,dc=com");
        assertTextFieldEquals("userDN", "uid=admin,ou=system");
        assertCheckboxSelected("secure");
        assertCheckboxSelected("referral");
    }

    public void testUpdateDelegatedDirectory_ConnectionScreenError()
    {
        log("Running: testUpdateDelegatedDirectory_ConnectionScreenError");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-connectiondetails");

        setTextField("URL", "");
        setTextField("baseDN", "");

        submit();

        assertKeyPresent("directoryconnector.url.invalid");
        assertKeyPresent("directoryconnector.basedn.invalid.blank");
    }

    public void testUpdateDelegatedDirectory_ConnectionScreenInvalidPassword()
    {
        log("Running: testUpdateDelegatedDirectory_ConnectionScreenInvalidPassword");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-connectiondetails");

        setTextField("ldapPassword", "invalid");

        submit();

        assertWarningPresent();
    }

    public void testUpdateDelegatedDirectory_TestConnectionWithCredentials()
    {
        intendToModifyData();

        log("Running: testUpdateDelegatedDirectory_TestConnectionWithCredentials");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-connectiondetails");

        // With credentials
        setTextField("userDN", "uid=admin,ou=system");
        setTextField("ldapPassword", "secret");

        clickButton("test-connection");

        assertKeyPresent("directoryconnector.testconnection.success");
    }

    public void testUpdateDelegatedDirectory_TestConnectionWithAnonymousBind()
    {
        intendToModifyData();

        log("Running: testUpdateDelegatedDirectory_TestConnectionWithAnonymousBind");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-connectiondetails");

        // Clear credentials
        setTextField("userDN", "");

        clickButton("test-connection");

        assertKeyPresent("directoryconnector.testconnection.success");
    }

    public void testUpdateDelegatedDirectory_TestConnectionWithNoPasswordShouldUseTheSavedPassword()
    {
        log("Running: testUpdateDelegatedDirectory_TestConnectionWithNoPasswordShouldUseTheSavedPassword");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-connectiondetails");

        // With userDN only
        setTextField("userDN", "uid=admin,ou=system");

        clickButton("test-connection");

        assertKeyPresent("directoryconnector.testconnection.success");
    }

    public void testUpdateDelegatedDirectory_TestConnectionWithoutAnyChanges()
    {
        log("Running: testUpdateDelegatedDirectory_TestConnectionWithoutAnyChanges");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-connectiondetails");

        clickButton("test-connection");

        assertKeyPresent("directoryconnector.testconnection.success");
    }

    public void testUpdateDelegatedDirectory_TestConnectionInvalidURL()
    {
        log("Running: testUpdateDelegatedDirectory_TestConnectionInvalidURL");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-connectiondetails");

        // Invalid URL
        setTextField("URL", "ldap://localhost:123/");

        clickButton("test-connection");

        assertKeyPresent("directoryconnector.testconnection.invalid");
    }

    public void testUpdateDelegatedDirectory_SearchPrincipal()
    {
        intendToModifyData();

        log("Running: testUpdateDelegatedDirectory_SearchPrincipal");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-configuration");
        setTextField("userDNaddition", "");

        clickButton("test-search-principal");

        assertKeyNotPresent("directoryconnector.testsearch.invalid");
    }

    public void testUpdateDelegatedDirectory_SearchPrincipalInvalid()
    {
        log("Running: testUpdateDelegatedDirectory_SearchPrincipalInvalid");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-configuration");
        setTextField("userDNaddition", "ou=nonexistentprincipal");

        clickButton("test-search-principal");

        assertKeyPresent("directoryconnector.testsearch.invalid");
    }

    public void testUpdateDelegatedDirectory_ConfigurationScreen()
    {
        intendToModifyData();

        log("Running: testUpdateDelegatedDirectory_ConfigurationScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-configuration");

        setTextField("userDNaddition", "13");
        setTextField("userObjectClass", "14");
        setTextField("userObjectFilter", "15");
        setTextField("userNameAttr", "16");
        setTextField("userNameRdnAttr", "17");
        setTextField("userFirstnameAttr", "18");
        setTextField("userLastnameAttr", "19");
        setTextField("userMailAttr", "20");
        setTextField("userGroupMemberAttr", "21");
        setTextField("userDisplayNameAttr", "23");

        submit();

        assertTextFieldEquals("userDNaddition", "13");
        assertTextFieldEquals("userObjectClass", "14");
        assertTextFieldEquals("userObjectFilter", "15");
        assertTextFieldEquals("userNameAttr", "16");
        assertTextFieldEquals("userNameRdnAttr", "17");
        assertTextFieldEquals("userFirstnameAttr", "18");
        assertTextFieldEquals("userLastnameAttr", "19");
        assertTextFieldEquals("userMailAttr", "20");
        assertTextFieldEquals("userGroupMemberAttr", "21");
        assertTextFieldEquals("userDisplayNameAttr", "23");
    }

    public void testUpdateDelegatedDirectory_ConfigurationScreenError()
    {
        log("Running: testUpdateDelegatedDirectory_ConfigurationScreenError");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-configuration");

        setTextField("userDNaddition", "");
        setTextField("userObjectClass", "");
        setTextField("userObjectFilter", "");
        setTextField("userNameAttr", "");
        setTextField("userNameRdnAttr", "");
        setTextField("userFirstnameAttr", "");
        setTextField("userLastnameAttr", "");
        setTextField("userDisplayNameAttr", "");
        setTextField("userMailAttr", "");
        setTextField("userGroupMemberAttr", "");

        submit();
        assertKeyPresent("directoryconnector.userobjectclass.invalid");
        assertKeyPresent("directoryconnector.userobjectfilter.invalid");
        assertKeyPresent("directoryconnector.usernameattribute.invalid");
        assertKeyPresent("directoryconnector.usernamerdnattribute.invalid");
        assertKeyPresent("directoryconnector.userfirstnameattribute.invalid");
        assertKeyPresent("directoryconnector.userlastnameattribute.invalid");
        assertKeyPresent("directoryconnector.userdisplaynameattribute.invalid");
        assertKeyPresent("directoryconnector.usermailattribute.invalid");
        assertKeyPresent("directoryconnector.usermemberofattribute.invalid");
    }

    public void testUpdateDelegatedDirectory_ConfigurationScreenWithoutGroupConfigurationNoErrors()
    {
        intendToModifyData();

        log("Running: testUpdateDelegatedDirectory_ConfigurationScreenWithoutGroupConfigurationNoErrors");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-configuration");

        submit();

        assertKeyNotPresent("directoryconnector.update.invalid");
    }

    public void testUpdateDelegatedDirectory_PermissionScreen()
    {
        intendToModifyData();

        log("Running: testUpdateDelegatedDirectory_PermissionScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", DELEGATED_DIRECTORY_NAME_APACHEDS);

        clickLink("delegated-permissions");

        // Update Permissions Tab

        uncheckCheckbox("permissionGroupAdd");
        uncheckCheckbox("permissionPrincipalAdd");

        submit();

        assertCheckboxNotSelected("permissionGroupAdd");
        assertCheckboxNotSelected("permissionPrincipalAdd");
    }

    // ========== CUSTOM DIRECTORIES =============================================================

    public void testUpdateCustomDirectory_GeneralScreen()
    {
        intendToModifyData();

        log("Running: testUpdateCustomDirectory_GeneralScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(CUSTOM_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CUSTOM_DIRECTORY_NAME);

        // Update General Tab

        setTextField("name", "Update Custom Directory");
        setTextField("directoryDescription", "Update Custom Directory Description");
        uncheckCheckbox("active");

        submit();

        assertTextFieldEquals("name", "Update Custom Directory");
        assertTextFieldEquals("directoryDescription", "Update Custom Directory Description");
        assertCheckboxNotSelected("active");
    }

    public void testUpdateCustomDirectory_GeneralScreenError()
    {
        log("Running: testUpdateCustomDirectory_GeneralScreenError");

        gotoBrowseDirectories();

        clickLinkWithExactText(CUSTOM_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CUSTOM_DIRECTORY_NAME);

        setTextField("name", "");

        submit();

        assertKeyPresent("directoryinternal.name.invalid");
    }

    public void testUpdateCustomDirectory_GeneralScreenWithDuplicateDirectoryName()
    {
        log("Running: testUpdateCustomDirectory_GeneralScreenWithDuplicateDirectoryName");

        gotoBrowseDirectories();

        clickLinkWithExactText(CUSTOM_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CUSTOM_DIRECTORY_NAME);

        setTextField("name", INTERNAL_DIRECTORY_NAME);

        submit();

        assertKeyPresent("directory.name.nonunique.invalid", INTERNAL_DIRECTORY_NAME);
    }

    public void testUpdateCustomDirectory_AttributesAddNewAttribute()
    {
        intendToModifyData();

        log("Running: testUpdateCustomDirectory_AttributesAddNewAttribute");

        gotoBrowseDirectories();

        clickLinkWithExactText(CUSTOM_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CUSTOM_DIRECTORY_NAME);

        clickLink("custom-attributes");

        setTextField("attribute", "New Attribute");
        setTextField("value", "123");

        clickButton("add-attribute");

        assertTextInTable("attributesTable", new String[]{"New Attribute", "123"});
    }

    public void testUpdateCustomDirectory_AttributesWithError()
    {
        log("Running: testUpdateCustomDirectory_AttributesWithError");

        gotoBrowseDirectories();

        clickLinkWithExactText(CUSTOM_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CUSTOM_DIRECTORY_NAME);

        clickLink("custom-attributes");

        setTextField("attribute", "New Attribute");
        setTextField("value", "");

        clickButton("add-attribute");

        assertKeyPresent("directorycustom.attribute.invalid");
    }

    public void testUpdateCustomDirectory_AttributesRemovingAttribute()
    {
        intendToModifyData();

        log("Running: testUpdateCustomDirectory_AttributesRemovingAttribute");

        gotoBrowseDirectories();

        clickLinkWithExactText(CUSTOM_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CUSTOM_DIRECTORY_NAME);

        clickLink("custom-attributes");

        clickLink("remove-attribute-testattrib");

        assertTextNotInTable("attributesTable", new String[]{"testattrib"});
    }

    public void testUpdateCustomDirectory_PermissionScreen()
    {
        intendToModifyData();

        log("Running: testUpdateCustomDirectory_PermissionScreen");

        gotoBrowseDirectories();

        clickLinkWithExactText(CUSTOM_DIRECTORY_NAME);

        assertKeyPresent("menu.viewdirectory.label", CUSTOM_DIRECTORY_NAME);

        clickLink("custom-permissions");

        // Update Permissions Tab

        uncheckCheckbox("permissionGroupAdd");
        uncheckCheckbox("permissionPrincipalAdd");

        submit();

        assertCheckboxNotSelected("permissionGroupAdd");
        assertCheckboxNotSelected("permissionPrincipalAdd");
    }

    public void testUpdatePagedResultsOnConnectorDirectoryWithInvalidAmount()
    {
        log("Running: testUpdatePagedResultsOnConnectorDirectory");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        uncheckCheckbox("pagedResults");

        assertTextFieldEquals("pagedResultsSize", "");

        checkCheckbox("pagedResults");

        setTextField("pagedResultsSize", "99");

        submit();

        assertKeyPresent("directoryconnector.pagedresultscontrolsize.invalid");
    }

    public void testUpdatePagedResultsOnConnectorDirectoryWithValidAmount()
    {
        intendToModifyData();

        log("Running: testUpdatePagedResultsOnConnectorDirectory");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS);

        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS);

        clickLink("connector-connectiondetails");

        uncheckCheckbox("pagedResults");

        assertTextFieldEquals("pagedResultsSize", "");

        checkCheckbox("pagedResults");

        setTextField("pagedResultsSize", "100");

        submit();

        assertKeyNotPresent("directoryconnector.pagedresultscontrolsize.invalid");

        assertTextFieldEquals("pagedResultsSize", "100");
    }

    public void testViewDelegatingNoGroupRoleDnOverlapWarning()
    {
        log("Running: testViewDelegatingNoGroupRoleDnOverlapWarning");

        // Delegated directory so no error for overlapping role/group DN
        gotoBrowseDirectories();
        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME_APACHEDS);
        assertWarningNotPresent();
    }

    public void testBaseDNMandatoryForNonGenericDirectory()
    {
        log("Running: testBaseDNMandatoryForAllExceptGeneric");

        attemptConfigureDirectoryWithEmptyBaseDN(CONNECTOR_DIRECTORY_NAME_APACHEDS);
        assertUnescapedKeyPresent("directoryconnector.basedn.invalid.blank");
    }

    public void testBaseDNNotMandatoryForGenericDirectory()
    {
        attemptConfigureDirectoryWithEmptyBaseDN(DELEGATED_DIRECTORY_NAME_APACHEDS);
        assertUnescapedKeyPresent("directoryconnector.basedn.invalid.blank");
    }

    public void testBaseDNNotMandatoryForGeneric()
    {
        attemptConfigureDirectoryWithEmptyBaseDN(CONNECTOR_DIRECTORY_NAME_GENERIC);
        assertUnescapedKeyNotPresent("directoryconnector.basedn.invalid.blank");
    }

    public void testBaseDNNotMandatoryForDelegatedGeneric()
    {
        attemptConfigureDirectoryWithEmptyBaseDN(DELEGATED_DIRECTORY_NAME_GENERIC);
        assertUnescapedKeyNotPresent("directoryconnector.basedn.invalid.blank");
    }

    private void attemptConfigureDirectoryWithEmptyBaseDN(String directoryName)
    {
        gotoBrowseDirectories();

        clickLinkWithExactText(directoryName);

        assertKeyPresent("menu.viewdirectory.label", directoryName);

        clickLinkWithExactText("Connector");

        setTextField("baseDN", "");

        submit();
    }

    /**
     * Assert that a web resource's value is present.
     *
     * @param key  web resource name
     * @param args the arguments for the web resource
     */
    public void assertKeyPresent(String key, String... args)
    {
        assertKeyPresent(key, Lists.newArrayList(args));
    }
}
