package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.util.Properties;
import java.util.Set;

import com.atlassian.crowd.acceptance.utils.DbCachingTestHelper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.event.Events;
import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.integration.rest.service.RestCrowdClient;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.event.Operation;
import com.atlassian.crowd.model.event.OperationEvent;
import com.atlassian.crowd.model.event.UserMembershipEvent;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;

import com.google.common.collect.ImmutableSet;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static com.atlassian.crowd.acceptance.tests.applications.crowd.UserMembershipEventMatcher.userMembershipCreated;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class AutoGroupAdderTest extends CrowdAcceptanceTestCase
{
    private static final String CROWD_DIRECTORY_NAME = "remotecrowd";

    private static final long MAX_WAIT_TIME_MS = 10000L; // 10 seconds

    public void setUp() throws Exception
    {
        super.setUp();
        _loginAdminUser();
        restoreCrowdFromXML("autogroupaddertest.xml");
    }

    public void testAutoAddWithInternalDirectory()
    {
        intendToModifyData();

        log("Running testAutoAddWithInternalDirectory");

        gotoBrowseDirectories();

        clickLinkWithExactText("Crowd Local");

        clickLink("internal-options");

        assertTextPresent("group1");
        assertTextPresent("group2");
        assertTextPresent("group3");

        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "Crowd Local");

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("testuser");

        // Groups Tab
        clickLink("user-groups-tab");
        assertTableNotPresent("groupsTable");

        _loginAsUser("testuser", "password");

        gotoPage("/console/user/viewprofile.action");

        clickLink("view-groups");

        assertTextPresent(getText("user.console.groups.text"));
        assertTextPresent("group1");
        assertTextPresent("group2");
        assertTextPresent("group3");
    }

    public void testAutoAddWithDelegatedDirectory()
    {
        intendToModifyData();

        log("Running testAutoAddWithDelegatedDirectory");

        gotoBrowseDirectories();

        clickLinkWithExactText("delg");

        clickLink("delegated-options");

        assertTextPresent("test1");

        gotoAddPrincipal();

        setTextField("email", "jdoe@testingarea.org");
        setTextField("name", "jdoe");
        setTextField("password", "jdoe");
        setTextField("passwordConfirm", "jdoe");
        setTextField("firstname", "John");
        setTextField("lastname", "Doe");
        selectOption("directoryID", "delg");

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("jdoe");

        // Groups Tab
        clickLink("user-groups-tab");
        assertTableNotPresent("groupsTable");

        increaseDirectoryPriorityTo("crowd", "delg", 2);

        _loginAsUser("jdoe", "jdoe");

        gotoPage("/console/user/viewprofile.action");

        clickLink("view-groups");

        assertTextPresent(getText("user.console.groups.text"));
        assertTextPresent("test1");
    }

    public void testAutoAddWithLDAPDirectory()
    {
        intendToModifyData();

        log("Running testAutoAddWithLDAPDirectory");

        DbCachingTestHelper.synchroniseDirectory(tester, "ldap", MAX_WAIT_TIME_MS);
        gotoBrowseDirectories();

        clickLinkWithExactText("ldap");

        clickLink("connector-options");

        assertTextPresent("groupA");
        assertTextPresent("groupB");
        assertTextPresent("groupC");

        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "ldap");

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("testuser");

        // Groups Tab
        clickLink("user-groups-tab");
        assertTableNotPresent("groupsTable");

        increaseDirectoryPriorityTo("crowd", "ldap", 2);

        _loginAsUser("testuser", "password");

        gotoPage("/console/user/viewprofile.action");

        clickLink("view-groups");

        assertTextPresent(getText("user.console.groups.text"));
        assertTextPresent("groupA");
        assertTextPresent("groupB");
        assertTextPresent("groupC");

        // we need to clean this user up as he's gonna persist in our ApacheDS
        _loginAdminUser();
        gotoViewPrincipal("testuser", "ldap");
        clickLink("remove-principal");
        submit();

        assertTextNotPresent("testuser");
    }

    public void testAutoAddWithRemoteCrowdDirectory()
    {
        intendToModifyData();

        log("Running testAutoAddWithRemoteCrowdDirectory");

        updateRemoteCrowdUrl(CROWD_DIRECTORY_NAME);

        DbCachingTestHelper.synchroniseDirectory(tester, "remotecrowd", MAX_WAIT_TIME_MS);
        gotoBrowseDirectories();

        clickLinkWithExactText("remotecrowd");

        clickLink("crowd-options");

        assertTextPresent("group1");
        assertTextPresent("group2");
        assertTextPresent("group3");

        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "remotecrowd");

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("testuser");

        // Groups Tab
        clickLink("user-groups-tab");
        assertTableNotPresent("groupsTable");

        increaseDirectoryPriorityTo("crowd", "remotecrowd", 1);

        _loginAsUser("testuser", "password");

        gotoPage("/console/user/viewprofile.action");

        clickLink("view-groups");

        assertTextPresent(getText("user.console.groups.text"));
        assertTextPresent("group1");
        assertTextPresent("group2");
        assertTextPresent("group3");

        // we need to clean this user up as he's gonna persist in our ApacheDS
        _loginAdminUser();
        gotoViewPrincipal("testuser", "remotecrowd");
        clickLink("remove-principal");
        submit();

        assertTextNotPresent("testuser");
    }

    /**
     * Tests that group auto add works when remote crowd directory connects to
     * delegated authentication directory and the user is added on successful
     * auth.
     */
    public void testAutoAddWithRemoteCrowdDirectoryUsingDelegatedDirectory()
    {
        intendToModifyData();

        log("Running testAutoAddWithRemoteCrowdDirectoryUsingDelegatedDirectory");

        updateRemoteCrowdUrl("Remote Crowd Directory - Delegated");

        DbCachingTestHelper.synchroniseDirectory(tester, "Remote Crowd Directory - Delegated", MAX_WAIT_TIME_MS);

        increaseDirectoryPriorityTo("crowd", "Remote Crowd Directory - Delegated", 2);

        _loginAsUser("jdoe", "jdoe");

        gotoPage("/console/user/viewprofile.action");

        clickLink("view-groups");

        // jdoe should have been added as a member of test1 group
        assertTextPresent(getText("user.console.groups.text"));
        assertTextPresent("test1");
    }

    /**
     * Tests that group auto add does not add user to a group when remote crowd
     * directory connecting to delegated authentication directory does not
     * contain the group yet.
     */
    public void testAutoAddWithRemoteCrowdDirectoryUsingDelegatedDirectory_NoGroup()
    {
        intendToModifyData();

        log("Running testAutoAddWithRemoteCrowdDirectoryUsingDelegatedDirectory_NoGroup");

        updateRemoteCrowdUrl("Remote Crowd Directory - Delegated");

        increaseDirectoryPriorityTo("crowd", "Remote Crowd Directory - Delegated", 2);

        _loginAsUser("jdoe", "jdoe");

        gotoPage("/console/user/viewprofile.action");

        clickLink("view-groups");

        // Groups have not been synced to Remote Crowd Directory yet.
        assertTextPresent(getText("user.console.nogroups.text"));
    }

    /**
     * Tests that user will get groups added the first time they log in,
     * but no new directories will be added after that.
     *
     * @throws Exception
     */
    public void testAutoAddOnlyAddingGroupsOnce() throws Exception
    {
        intendToModifyData();

        log("Running testAutoAddOnlyAddingGroupsOnce");

        setScriptingEnabled(true);

        // Set group1 and group2 as auto-add-groups
        gotoBrowseDirectories();

        clickLinkWithExactText("Crowd Local");

        clickLink("internal-options");

        clickLinkWithText("remove", 2); // Remove group3

        assertTextPresent("group1");
        assertTextPresent("group2");
        assertTextNotPresent("group3");

        // Add a principal and check that groups are added automatically when authenticated
        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "Crowd Local");

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("testuser");

        // Groups Tab
        clickLink("user-groups-tab");
        assertTableNotPresent("groupsTable");

        _loginAsUser("testuser", "password");

        gotoPage("/console/user/viewprofile.action");

        clickLink("view-groups");

        assertTextPresent(getText("user.console.groups.text"));
        assertTextPresent("group1");
        assertTextPresent("group2");
        assertTextNotPresent("group3");

        // Add group3 to auto-add-groups
        _loginAdminUser();

        gotoBrowseDirectories();

        clickLinkWithExactText("Crowd Local");

        clickLink("internal-options");

        clickButton("addGroups");

        waitForText(getText("picker.addgroups.generic.message"));

        setTextField("searchString", "group3");

        submit("searchButton");

        waitForText("Description");

        setWorkingForm("selectentities");

        checkCheckbox("selectedEntityNames");

        clickButtonWithText(getText("picker.addselected.groups.label"));

        assertTextPresent("group3 (");

        // Check that group3 will not be assigned to user who has had directories set previously
        _loginAsUser("testuser", "password");

        gotoPage("/console/user/viewprofile.action");

        clickLink("view-groups");

        assertTextPresent(getText("user.console.groups.text"));
        assertTextPresent("group1");
        assertTextPresent("group2");
        assertTextNotPresent("group3");

        setScriptingEnabled(false);
    }

    private void increaseDirectoryPriorityTo(String application, String directoryName, int order)
    {
        gotoViewApplication(application);
        clickLink("application-directories");

        assertTextInTable("directoriesTable", directoryName);

        // While directory is not high enough, move it up
        while (!tester.getTestingEngine().hasElementByXPath("id('directoriesTable')/tbody/tr[" + (order + 1) + "][contains(.,'" + directoryName + "')]"))
        {
            clickElementByXPath("id('directoriesTable')/tbody/tr[contains(.,'" + directoryName + "')]/td/a[@name='Move Up']");
        }
    }

    private void updateRemoteCrowdUrl(String directoryName)
    {
        // Update remote Crowd url first
        gotoBrowseDirectories();

        clickLinkWithExactText(directoryName);

        assertKeyPresent("menu.viewdirectory.label", directoryName);

        clickLink("crowd-connectiondetails");

        setTextField("url", getBaseUrl());

        submit();
    }

    public void testAutoAddWithInternalDirectoryIsTriggeredBySessionCreationWithoutPassword() throws Exception
    {
        intendToModifyData();

        gotoBrowseDirectories();

        clickLinkWithExactText("Crowd Local");

        clickLink("internal-options");

        assertTextPresent("group1");
        assertTextPresent("group2");
        assertTextPresent("group3");

        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "Crowd Local");

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("testuser");

        // Groups Tab
        clickLink("user-groups-tab");
        assertTableNotPresent("groupsTable");

        UserAuthenticationContext userAuthenticationContext =
                new UserAuthenticationContext("testuser", null, new ValidationFactor[0], "crowd");
        createCrowdClient().authenticateSSOUserWithoutValidatingPassword(userAuthenticationContext);

        gotoViewPrincipal("testuser", "Crowd Local");
        clickLink("user-groups-tab");

        assertThat(namesOf(getGroupTableContents()), hasItems("group1", "group2", "group3"));
    }

    public void testAutoAddGroupMembershipsAreSynchronizedAfterLogin() throws Exception
    {
        intendToModifyData();

        log("Running testAutoAddGroupMembershipsAreSynchronizedAfterLogin");

        CrowdClient crowdClient = createCrowdClient();
        String eventToken = crowdClient.getCurrentEventToken();

        gotoBrowseDirectories();

        clickLinkWithExactText("Crowd Local");

        clickLink("internal-options");

        assertTextPresent("group1");
        assertTextPresent("group2");
        assertTextPresent("group3");

        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "Crowd Local");

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("testuser");

        Events eventsAfterUserCreation = crowdClient.getNewEvents(eventToken); // sync before login

        _loginAsUser("testuser", "password");

        Events eventsAfterUserFirstLogin =
            crowdClient.getNewEvents(eventsAfterUserCreation.getNewEventToken()); // sync after login

        // events are independently fired for each group. This may change in the future
        assertThat(eventsAfterUserFirstLogin.getEvents(),
                   CoreMatchers.<OperationEvent>hasItems(userMembershipCreated("testuser", "group1"),
                            userMembershipCreated("testuser", "group2"),
                            userMembershipCreated("testuser", "group3")));
    }

    private static final String APPLICATION_NAME = "crowd";
    private static final String APPLICATION_PASSWORD = "OJHhmMvi";

    public CrowdClient createCrowdClient()
    {
        final Properties properties = new Properties();
        properties.setProperty(Constants.PROPERTIES_FILE_BASE_URL, HOST_PATH);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_NAME, APPLICATION_NAME);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_PASSWORD, APPLICATION_PASSWORD);

        final ClientProperties clientProperties = ClientPropertiesImpl.newInstanceFromProperties(properties);

        return new RestCrowdClient(clientProperties);
    }
}

class UserMembershipEventMatcher extends TypeSafeMatcher<OperationEvent>
{

    private Operation operation;
    private Matcher<Directory> directory;
    private Matcher<String> childUsername;
    private Matcher<Set<String>> parentGroupNames;

    public UserMembershipEventMatcher(Operation operation, Matcher<Directory> directory,
                                      Matcher<String> childUsername, Matcher<Set<String>> parentGroupNames)
    {
        this.operation = operation;
        this.directory = directory;
        this.childUsername = childUsername;
        this.parentGroupNames = parentGroupNames;
    }

    @Override
    public boolean matchesSafely(OperationEvent item)
    {
        try
        {
            UserMembershipEvent userMembershipEvent = (UserMembershipEvent) item;
            return operation.equals(userMembershipEvent.getOperation())
                && directory.matches(userMembershipEvent.getDirectory())
                && childUsername.matches(userMembershipEvent.getChildUsername())
                && parentGroupNames.matches(userMembershipEvent.getParentGroupNames());
        }
        catch (ClassCastException e)
        {
            return false;
        }
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("a user membership " + operation + " event for user " + childUsername
                               + " and groups " + parentGroupNames + " in directory " + directory);
    }

    @Factory
    public static UserMembershipEventMatcher userMembershipCreated(String username, Set<String> groupNames)
    {
        return new UserMembershipEventMatcher(Operation.CREATED, nullValue(Directory.class), is(username), is(groupNames));
    }

    @Factory
    public static UserMembershipEventMatcher userMembershipCreated(String username, String groupName)
    {
        return userMembershipCreated(username, ImmutableSet.of(groupName));
    }
}