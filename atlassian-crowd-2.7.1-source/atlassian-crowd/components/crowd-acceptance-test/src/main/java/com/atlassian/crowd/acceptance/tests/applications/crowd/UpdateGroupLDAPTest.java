package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.util.Properties;

import com.atlassian.crowd.acceptance.tests.directory.BaseTest;
import com.atlassian.crowd.acceptance.utils.DbCachingTestHelper;
import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;

import static org.hamcrest.Matchers.isEmptyString;
import static org.junit.Assert.assertThat;

/**
 * Web acceptance test for the update of an LDAP Group
 */
public class UpdateGroupLDAPTest extends CrowdAcceptanceTestCase
{
    private static final String DIRECTORY_NAME = "ApacheDS154";

    private static final long MAX_WAIT_TIME_MS = 10000L;

    private static final String GROUP_NAME = "myGroup";

    private static final String SUB_GROUP_NAME = "mySubGroup";
    LDAPLoader loader;

    /**
     * Allows us to add users to our ApacheDS instance.
     */
    class LDAPLoader extends BaseTest
    {
        LDAPLoader()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
        }

        @Override
        protected void configureDirectory(Properties directorySettings)
        {
            super.configureDirectory(directorySettings);
            directory.setAttribute(LDAPPropertiesMapper.LDAP_BASEDN_KEY, directorySettings.getProperty("test.integration.basedn"));
            directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.FALSE.toString());
        }

        protected void loadTestData() throws Exception
        {
            GroupTemplate group = new GroupTemplate(GROUP_NAME, directory.getId(), GroupType.GROUP);     // members: SUB_USER_NAME
            getRemoteDirectory().addGroup(group);

            GroupTemplate subGroup = new GroupTemplate(SUB_GROUP_NAME, directory.getId(), GroupType.GROUP);     // members: SUB_USER_NAME

            getRemoteDirectory().addGroup(subGroup);

            // members: SUB_USER_NAME
            getRemoteDirectory().addGroupToGroup(SUB_GROUP_NAME, GROUP_NAME);
        }

        protected void removeTestData() throws DirectoryInstantiationException
        {
            DirectoryTestHelper.silentlyRemoveGroup(GROUP_NAME, getRemoteDirectory());
            DirectoryTestHelper.silentlyRemoveGroup(SUB_GROUP_NAME, getRemoteDirectory());
        }

        public void accessibleSetUp() throws Exception
        {
            super.setUp();
        }

        public void accessibleTearDown() throws Exception
        {
            super.tearDown();
        }

    }

    public UpdateGroupLDAPTest()
    {
        loader = new LDAPLoader();
    }

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        loader.accessibleSetUp();
        restoreCrowdFromXML("viewgrouptest.xml");
        synchroniseDirectory();
    }

    private void synchroniseDirectory()
    {
        DbCachingTestHelper.synchroniseDirectory(tester, DIRECTORY_NAME, MAX_WAIT_TIME_MS);
    }

    @Override
    public void tearDown() throws Exception
    {
        super.tearDown();
        loader.accessibleTearDown();
    }

    /**
     * Regression test for CWD-1140 (and also making sure that we can have empty descriptions)
     */
    public void testUpdateGroupTwiceWithEmptyDescription()
    {
        log("Running testUpdateGroupTwiceWithEmptyDescription");
        intendToModifyData();
        intendToModifyLdapData();

        gotoViewGroup(GROUP_NAME, DIRECTORY_NAME);
        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent(GROUP_NAME);

        setWorkingForm("groupForm");
        setTextField("description", ""); // description may already be empty but set it anyway to make test less fragile
        submit();

        assertWarningAndErrorNotPresent();
        assertKeyPresent("updatesuccessful.label");
        assertTextPresent(GROUP_NAME);

        assertThat(getElementTextById("description"), isEmptyString());
        submit();

        assertWarningAndErrorNotPresent();
        assertKeyPresent("updatesuccessful.label");
        assertTextPresent(GROUP_NAME);
        assertThat(getElementTextById("description"), isEmptyString());
    }

    public void testUpdateRemoteGroupWhenLocalGroupsAreDisabled()
    {
        log("Running testUpdateRemoteGroupWhenLocalGroupsAreDisabled");
        intendToModifyData();
        intendToModifyLdapData();
        disableLocalGroups();

        gotoViewGroup(GROUP_NAME, DIRECTORY_NAME);
        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent(GROUP_NAME);

        setWorkingForm("groupForm");
        setTextField("description", "New description 1");
        submit();

        assertWarningAndErrorNotPresent();
        assertKeyPresent("updatesuccessful.label");
        assertTextPresent(GROUP_NAME);
        assertTextPresent("New description 1");
    }

    public void testUpdateRemoteGroupWhenLocalGroupsAreEnabledShouldFail()
    {
        log("Running testUpdateRemoteGroupWhenLocalGroupsAreEnabledShouldFail");
        intendToModifyData();
        enableLocalGroups();

        gotoViewGroup(GROUP_NAME, DIRECTORY_NAME);
        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent(GROUP_NAME);

        setWorkingForm("groupForm");
        setTextField("description", "New description 2");
        submit();

        assertWarningPresent();
        assertTextPresent("Group <" + GROUP_NAME + "> is read-only and cannot be updated");
    }

    private void enableLocalGroups()
    {
        gotoBrowseDirectories();
        clickLinkWithExactText(DIRECTORY_NAME);
        clickLink("connector-connectiondetails");
        checkCheckbox("localGroupsEnabled");
        submit();

        assertWarningAndErrorNotPresent();
        assertCheckboxSelected("localGroupsEnabled");
    }

    private void disableLocalGroups()
    {
        gotoBrowseDirectories();
        clickLinkWithExactText(DIRECTORY_NAME);
        clickLink("connector-connectiondetails");
        uncheckCheckbox("localGroupsEnabled");
        submit();

        assertWarningAndErrorNotPresent();
        assertCheckboxNotSelected("localGroupsEnabled");
    }
}
