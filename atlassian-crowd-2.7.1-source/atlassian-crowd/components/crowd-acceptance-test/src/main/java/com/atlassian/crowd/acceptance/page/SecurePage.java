package com.atlassian.crowd.acceptance.page;

import net.sourceforge.jwebunit.api.ITestingEngine;
import net.sourceforge.jwebunit.junit.WebTester;

public abstract class SecurePage
{
    protected final ITestingEngine engine;

    protected SecurePage(ITestingEngine engine)
    {
        this.engine = engine;

        if (!engine.hasElement("userFullName"))
        {
            throw new IllegalStateException("User is not logged in");
        }
        else if (engine.getPageTitle().contains("Access Denied"))
        {
            throw new IllegalPageStateException(engine, "User has no access to this page");
        }
    }

    protected SecurePage(WebTester tester, String baseUrl, String pageUrl, String displayName, String username, String password)
    {
        this.engine = tester.getTestingEngine();

        // Navigate to page
        tester.gotoPage(baseUrl + pageUrl);

        // If necessary, logout and return to page
        if (engine.hasElement("userFullName") && !engine.isTextInElement("userFullName", displayName))
        {
            tester.gotoPage(baseUrl  + "/console/logoff.action");
            tester.gotoPage(baseUrl + pageUrl);
        }

        // Login if necessary
        if (!engine.hasElement("userFullName"))
        {
            loginAs(username, password);
        }

        if (engine.getPageTitle().contains("Access Denied"))
        {
            throw new IllegalPageStateException(engine, "User " + username + " has no access to this page");
        }
    }

    private void loginAs(String username, String password)
    {
        engine.setWorkingForm("login", 0);
        engine.setTextField("j_username", username);
        engine.setTextField("j_password", password);
        engine.submit();

        if (engine.hasForm("login"))
        {
            throw new IllegalStateException("Login failed");
        }
    }
}
