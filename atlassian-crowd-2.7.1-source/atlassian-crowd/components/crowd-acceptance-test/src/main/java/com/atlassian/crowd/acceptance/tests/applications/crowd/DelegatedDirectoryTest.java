package com.atlassian.crowd.acceptance.tests.applications.crowd;

public class DelegatedDirectoryTest extends CrowdAcceptanceTestCase
{
    private static final String DELEGATED_DIRECTORY_NAME = "ApacheDS Delegated Directory";

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("delegatedtest.xml");
    }

    public void testDelegatedDirectoryAuthenticateCreatesUserInUnderlyingInternalDirectory()
    {
        log("Running testDelegatedDirectoryAuthenticateCreatesUserInUnderlyingInternalDirectory");

        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", DELEGATED_DIRECTORY_NAME);
        submit();

        // no users present (just the table header row)
        assertTableRowCountEquals("user-details", 1);

        gotoViewApplication("crowd");
        clickLink("application-authtest");
        setWorkingForm("app-auth-test-form");
        setTextField("testUsername", "jdoe");
        setTextField("testPassword", "jdoe");
        submit();

        // authentication should have succeeded and created a copy of the LDAP user in the internal directory
        assertTextPresent("Successful verification");

        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", DELEGATED_DIRECTORY_NAME);
        submit();

        // one user present (incl header row)
        assertTableRowCountEquals("user-details", 2);
        assertUserInTable("jdoe", "John Doe", "jdoe@testingarea.org");
    }

    public void testDelegatedDirectoryImportLocalGroup()
    {
        log("Running testDelegatedDirectoryImportLocalGroup");

        gotoViewGroup("imported_group", DELEGATED_DIRECTORY_NAME);

        assertTextInElement("groupName", "imported_group");
        assertKeyInElement("groupLocation", "group.location.local");
        assertTextInElement("groupDirectory", DELEGATED_DIRECTORY_NAME);
        assertTextInElement("groupDirectory", "Delegated Authentication Directory");
    }

    public void testDelegatedDirectoryAddLocalGroup()
    {
        log("Running testDelegatedDirectoryAddLocalGroup");

        gotoAddGroup();
        setWorkingForm("addGroupForm");
        setTextField("name", "test_local_group");
        selectOption("directoryID", DELEGATED_DIRECTORY_NAME);
        submit();

        assertTextInElement("groupName", "test_local_group");
        assertKeyInElement("groupLocation", "group.location.local");
        assertTextInElement("groupDirectory", DELEGATED_DIRECTORY_NAME);
        assertTextInElement("groupDirectory", "Delegated Authentication Directory");
    }
}
