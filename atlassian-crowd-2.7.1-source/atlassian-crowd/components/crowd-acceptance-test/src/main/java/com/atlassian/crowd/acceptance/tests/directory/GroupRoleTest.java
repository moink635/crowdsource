package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import static com.atlassian.crowd.acceptance.tests.directory.BasicTest.getImplementation;

import java.util.List;
import java.util.Properties;

/**
 * Tests Groups and Roles function in LDAP in the same way that they
 * do in the internal directory.
 *
 * Requires directory server to have ou=roles and ou=groups defined.
 */
public abstract class GroupRoleTest extends BaseTest
{
    private static final String GROUP_NAME = "group-group";
    private static final String ROLE_NAME = "group-role";
    private static final String USER_NAME = "mysupercooluser";

    public GroupRoleTest()
    {
//        setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
    }

    @Override
    protected void configureDirectory(final Properties directorySettings)
    {
        super.configureDirectory(directorySettings);

        // make the role and group dn's distinct

        directory.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, "false");
        directory.setAttribute(LDAPPropertiesMapper.ROLE_DN_ADDITION, "ou=roles");
        directory.setAttribute(LDAPPropertiesMapper.GROUP_DN_ADDITION, "ou=groups");
    }

    protected void removeTestData() throws DirectoryInstantiationException
    {
        DirectoryTestHelper.silentlyRemoveGroup(GROUP_NAME, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup(ROLE_NAME, getRemoteDirectory());
    }

    protected void loadTestData() throws Exception
    {
        GroupTemplate group = new GroupTemplate(GROUP_NAME, directory.getId(), GroupType.GROUP);
        Group addedGroup = getImplementation(directory).addGroup(group);
        assertEquals(getImplementation(directory).getDirectoryId(), addedGroup.getDirectoryId());
        assertEquals(group.getName(), addedGroup.getName());
        assertEquals(group.getType(), addedGroup.getType());
    }

    public void testFindGroupOfAnyTypeFindsGroup() throws Exception
    {
        Group group = getImplementation(directory).findGroupByName(GROUP_NAME);
        assertEquals(getImplementation(directory).getDirectoryId(), group.getDirectoryId());
        assertEquals(GROUP_NAME, group.getName());
        assertEquals(GroupType.GROUP, group.getType());

        List<Group> containers = getImplementation(directory).searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group(null)).returningAtMost(EntityQuery.ALL_RESULTS));
        assertEquals(1, containers.size());
    }

    public void testFindGroupOfAnyTypeDoesNotFindRole() throws Exception
    {
        try
        {
            getImplementation(directory).findGroupByName(ROLE_NAME);
            fail();
        }
        catch (GroupNotFoundException e)
        {
            // Okay
        }
    }

    public void testFindGroupOfTypeRoleFindsNothing() throws OperationFailedException
    {
        List<Group> roles = getImplementation(directory).searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.LEGACY_ROLE)).returningAtMost(EntityQuery.ALL_RESULTS));

        assertEquals(0, roles.size());
    }

    public void testFindGroupOfTypeGroup() throws OperationFailedException
    {
        List<Group> groups = getImplementation(directory).searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP)).returningAtMost(EntityQuery.ALL_RESULTS));

        assertEquals(1, groups.size());

        Group group = groups.get(0);
        assertEquals(getImplementation(directory).getDirectoryId(), group.getDirectoryId());
        assertEquals(GROUP_NAME, group.getName());
        assertEquals(GroupType.GROUP, group.getType());
    }

    public void testAddGroupWithNoTypeFail() throws Exception
    {
        GroupTemplate group = new GroupTemplate("new group", directory.getId(), GroupType.GROUP);
        group.setType(null);
        try
        {
            getImplementation(directory).addGroup(group);
            fail("InvalidGroupException expected");
        }
        catch (InvalidGroupException e)
        {
            assertTrue(true);
        }
    }
}
