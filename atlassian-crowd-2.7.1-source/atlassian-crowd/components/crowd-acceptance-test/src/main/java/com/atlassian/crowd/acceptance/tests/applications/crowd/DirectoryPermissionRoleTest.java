package com.atlassian.crowd.acceptance.tests.applications.crowd;

public class DirectoryPermissionRoleTest extends CrowdAcceptanceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("directorypermissionroletest.xml");
    }

    public void testAbleToUpdateGroup() // this should make sure we have no cross talk between group and role permissions
    {
        gotoViewGroup("tester-group", "Test Internal Directory");

        setWorkingForm("groupForm");

        setTextField("description", "Crowd Acceptance Test Group Updated");

        submit();

        assertKeyPresent("updatesuccessful.label");

        setWorkingForm("groupForm");

        assertTextFieldEquals("description", "Crowd Acceptance Test Group Updated");

        assertTextNotPresent("Directory does not allow group modifications");
    }

    public void testAbleToRemoveGroup() // this should make sure we have no cross talk between group and role permissions
    {
        gotoViewGroup("tester-group", "Test Internal Directory");

        clickLink("remove-group");

        submit();

        assertKeyPresent("updatesuccessful.label");

        assertTextNotPresent("tester-group");
    }

    public void testAbleToAddGroup() // this should make sure we have no cross talk between group and role permissions
    {
        gotoAddGroup();

        setTextField("name", "test-group");
        setTextField("description", "Crowd Test Group");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent("test-group");
    }
}
