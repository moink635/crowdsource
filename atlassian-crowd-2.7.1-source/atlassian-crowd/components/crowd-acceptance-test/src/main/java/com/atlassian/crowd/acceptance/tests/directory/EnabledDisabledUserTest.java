package com.atlassian.crowd.acceptance.tests.directory;

import java.util.List;

import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;

import static com.atlassian.crowd.acceptance.tests.directory.BasicTest.getImplementation;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class EnabledDisabledUserTest extends BaseTest
{
    private final String userNameA = "user-A";
    private final String userNameB = "user-B";
    private final String firstName = "First";
    private final String surName = "Last";
    private final String displayName = "Display Name";
    private final String email = "user@example.test";
    private final String password = "secret-password";

    @Override
    protected void removeTestData() throws DirectoryInstantiationException
    {
        DirectoryTestHelper.silentlyRemoveUser(userNameA, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser(userNameB, getRemoteDirectory());
    }

    @Override
    protected void loadTestData() throws Exception
    {
        // an enabled user
        UserTemplate enabledUserTemplate = new UserTemplate(userNameA, firstName, surName, displayName);
        enabledUserTemplate.setActive(true);
        enabledUserTemplate.setEmailAddress(email);  // strictly not necessary, but reduces logging
        getRemoteDirectory().addUser(enabledUserTemplate, PasswordCredential.unencrypted(password));

        // a disabled user
        UserTemplate disabledUserTemplate = new UserTemplate(userNameB, firstName, surName, displayName);
        disabledUserTemplate.setActive(false);
        enabledUserTemplate.setEmailAddress(email);  // strictly not necessary, but reduces logging
        getRemoteDirectory().addUser(disabledUserTemplate, PasswordCredential.unencrypted(password));
    }

    public void testEnabledUserIsEnabled() throws Exception
    {
        User user = getImplementation(directory).findUserByName(userNameA);
        assertTrue("User should be enabled", user.isActive());
    }

    public void testDisabledUserIsDisabled() throws Exception
    {
        User user = getImplementation(directory).findUserByName(userNameB);
        assertFalse("User should be disabled", user.isActive());
    }

    public void testUserCanBeDisabledAndEnabledAgain() throws Exception
    {
        // initially the user is enabled
        User user = getImplementation(directory).findUserByName(userNameA);
        assertTrue("User should be enabled", user.isActive());

        UserTemplate disabledUserTemplate = new UserTemplate(user);
        disabledUserTemplate.setActive(false);
        getImplementation(directory).updateUser(disabledUserTemplate);

        // the user is now disabled
        user = getImplementation(directory).findUserByName(userNameA);
        assertFalse("User should be disabled", user.isActive());

        UserTemplate enabledUserTemplate = new UserTemplate(user);
        enabledUserTemplate.setActive(true);
        getImplementation(directory).updateUser(enabledUserTemplate);

        // the user is enabled again
        user = getImplementation(directory).findUserByName(userNameA);
        assertTrue("User should be enabled", user.isActive());
    }

    public void testQueryEnabledUsers() throws Exception
    {
        EntityQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.user(),
                                                          Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(true),
                                                          0, EntityQuery.ALL_RESULTS);
        List<String> users = getImplementation(directory).searchUsers(query);

        assertThat(users, hasItem(userNameA));
        assertThat(users, not(hasItem(userNameB)));
    }

    public void testQueryDisabledUsers() throws Exception
    {
        EntityQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.user(),
                                                          Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(false),
                                                          0, EntityQuery.ALL_RESULTS);
        List<String> users = getImplementation(directory).searchUsers(query);

        assertThat(users, hasItem(userNameB));
        assertThat(users, not(hasItem(userNameA)));
    }

    public void testEnabledAndDisabledUsersShouldBeReturnedWhenQueryingAllUsers() throws Exception
    {
        EntityQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.user(),
                                                          NullRestrictionImpl.INSTANCE,
                                                          0, EntityQuery.ALL_RESULTS);
        List<String> users = getImplementation(directory).searchUsers(query);

        assertThat(users, hasItem(userNameA));
        assertThat(users, hasItem(userNameB));

    }
}
