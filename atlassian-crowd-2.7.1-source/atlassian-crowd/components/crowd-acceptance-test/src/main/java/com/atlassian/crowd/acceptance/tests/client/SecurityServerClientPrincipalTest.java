package com.atlassian.crowd.acceptance.tests.client;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import static com.atlassian.crowd.acceptance.utils.ArrayAssertions.assertContainsElements;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl;
import com.atlassian.crowd.service.soap.client.SecurityServerClientImpl;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;

import java.util.Properties;

public class SecurityServerClientPrincipalTest extends CrowdAcceptanceTestCase
{
    private SecurityServerClientImpl securityServerClient;
    private UserAuthenticationContext userAuthenticationContext;
    private Properties sscProperties;

    private static final String USER_1 = "user";
    private static final String USER_2 = "user2";

    private static final String USER1_ATTRIBUTE1_NAME = "colour";
    private static final String USER1_ATTRIBUTE2_NAME = "numbers";
    private static final SOAPAttribute USER1_ATTRIBUTE1_VALUE = new SOAPAttribute(USER1_ATTRIBUTE1_NAME, "green");
    private static final SOAPAttribute USER1_ATTRIBUTE2_VALUE = new SOAPAttribute(USER1_ATTRIBUTE2_NAME,new String[]{"one", "two", "three"});

    private static final String USER2_ATTRIBUTE1_NAME = "animal";
    private static final String USER2_ATTRIBUTE2_NAME = "food";

    private static final String GIVEN_NAME = "givenName";
    private static final String LAST_NAME = "sn";
    private static final String DISPLAY_NAME = "displayName";
    private static final String MAIL = "mail";
    private static final String INVALID_PASSWORD_ATTEMPTS = "invalidPasswordAttempts";
    private static final String REQUIRES_PASSWORD_CHANGE = "requiresPasswordChange";
    private static final String PASSWORD_LAST_CHANGED = "passwordLastChanged";
    private static final String LAST_AUTHENTICATED = "lastAuthenticated";


    public void setUp() throws Exception
    {
        super.setUp();

        restoreCrowdFromXML("securityserverclienttest.xml");

        sscProperties = AcceptanceTestHelper.loadProperties("localtest.crowd.properties");
        SoapClientProperties cProperties = SoapClientPropertiesImpl.newInstanceFromProperties(sscProperties);
        securityServerClient = new SecurityServerClientImpl(cProperties);

        userAuthenticationContext = new UserAuthenticationContext();
        userAuthenticationContext.setApplication("integrationtest");
    }

    public void testFindPrincipalByName() throws Exception
    {
        final SOAPPrincipal soapPrincipal = securityServerClient.findPrincipalByName(USER_1);

        assertEquals(USER_1,soapPrincipal.getName());
        // User has the default 4 attributes - firstname, lastname, displayname, email
        // We don't care about the other attributes (invalidPasswordAttempts, lastAuthenticated, passwordLastChanged, requiresPasswordChange)
        //  we just want to get the user
        assertEquals(4, soapPrincipal.getAttributes().length);
        assertContainsElements(soapPrincipal.getAttribute(GIVEN_NAME).getValues(), "Normal");
        assertContainsElements(soapPrincipal.getAttribute(LAST_NAME).getValues(), "User");
        assertContainsElements(soapPrincipal.getAttribute(DISPLAY_NAME).getValues(), "Normal User");
        assertContainsElements(soapPrincipal.getAttribute(MAIL).getValues(), "user@user.com");
    }

    public void testFindPrincipalWithAttributesByName() throws Exception
    {
        final SOAPPrincipal soapPrincipal = securityServerClient.findPrincipalWithAttributesByName(USER_1);

        assertEquals(USER_1,soapPrincipal.getName());

        // User has the default 4 attributes - firstname, lastname, displayname, email
        // We care about the other attributes (invalidPasswordAttempts, lastAuthenticated, passwordLastChanged, requiresPasswordChange)
        //  - make sure we have these attributes
        assertEquals(8, soapPrincipal.getAttributes().length);
        assertContainsElements(soapPrincipal.getAttribute(INVALID_PASSWORD_ATTEMPTS).getValues(), "0");
        assertContainsElements(soapPrincipal.getAttribute(LAST_AUTHENTICATED).getValues(), "1241415635159");
        assertContainsElements(soapPrincipal.getAttribute(PASSWORD_LAST_CHANGED).getValues(), "1241415535781");
        assertContainsElements(soapPrincipal.getAttribute(REQUIRES_PASSWORD_CHANGE).getValues(), "false");
    }

    public void testFindPrincipalWithAttributesByNameCustomAttributes() throws Exception
    {
        intendToModifyData();

        securityServerClient.addAttributeToPrincipal(USER_1, USER1_ATTRIBUTE1_VALUE);
        securityServerClient.addAttributeToPrincipal(USER_1, USER1_ATTRIBUTE2_VALUE);

        final SOAPPrincipal soapPrincipal = securityServerClient.findPrincipalWithAttributesByName(USER_1);

        assertEquals(USER_1,soapPrincipal.getName());

        // Check all attributes are there
        assertEquals(10, soapPrincipal.getAttributes().length);
        assertContainsElements(soapPrincipal.getAttribute(USER1_ATTRIBUTE1_NAME).getValues(), "green");
        assertContainsElements(soapPrincipal.getAttribute(USER1_ATTRIBUTE2_NAME).getValues(), "one", "two", "three");
    }

    public void testAddRemovePrincipalCustomAttributes() throws Exception
    {
        intendToModifyData();

        SOAPPrincipal soapPrincipal = securityServerClient.findPrincipalWithAttributesByName(USER_1);
        assertEquals(USER_1,soapPrincipal.getName());

        // Check only have the primary attributes and default attributes
        assertEquals(8, soapPrincipal.getAttributes().length);

        // Add 2 new custom attributes and check
        securityServerClient.addAttributeToPrincipal(USER_1, USER1_ATTRIBUTE1_VALUE);
        securityServerClient.addAttributeToPrincipal(USER_1, USER1_ATTRIBUTE2_VALUE);
        soapPrincipal = securityServerClient.findPrincipalWithAttributesByName(USER_1);
        assertEquals(10, soapPrincipal.getAttributes().length);
        assertContainsElements(soapPrincipal.getAttribute(USER1_ATTRIBUTE1_NAME).getValues(), "green");
        assertContainsElements(soapPrincipal.getAttribute(USER1_ATTRIBUTE2_NAME).getValues(), "one", "two", "three");

        // Remove 1 custom attribute and check
        securityServerClient.removeAttributeFromPrincipal(USER_1, USER1_ATTRIBUTE1_NAME);
        soapPrincipal = securityServerClient.findPrincipalWithAttributesByName(USER_1);
        assertEquals(9, soapPrincipal.getAttributes().length);
        assertContainsElements(soapPrincipal.getAttribute(USER1_ATTRIBUTE2_NAME).getValues(), "one", "two", "three");
        assertNull(soapPrincipal.getAttribute(USER1_ATTRIBUTE1_NAME));
    }

    public void testFindPrincipalByNameWithExistingCustomAttributes() throws Exception
    {
        final SOAPPrincipal soapPrincipal = securityServerClient.findPrincipalByName(USER_2);

        assertEquals(USER_2,soapPrincipal.getName());
        // User has the default 4 attributes - firstname, lastname, displayname, email
        // We don't care about the other attributes (passwordLastChanged, requiresPasswordChange) + existing custom attributes
        //  we just want to get the user
        assertEquals(4, soapPrincipal.getAttributes().length);
        assertContainsElements(soapPrincipal.getAttribute(GIVEN_NAME).getValues(), "Normal");
        assertContainsElements(soapPrincipal.getAttribute(LAST_NAME).getValues(), "User2");
        assertContainsElements(soapPrincipal.getAttribute(DISPLAY_NAME).getValues(), "Normal User2");
        assertContainsElements(soapPrincipal.getAttribute(MAIL).getValues(), "user2@user.com");
    }

    public void testFindPrincipalWithAttributesByNameWithExistingCustomAttributes() throws Exception
    {
        final SOAPPrincipal soapPrincipal = securityServerClient.findPrincipalWithAttributesByName(USER_2);

        assertEquals(USER_2,soapPrincipal.getName());

        // User has the default 4 attributes - firstname, lastname, displayname, email
        // We care about the other attributes (passwordLastChanged, requiresPasswordChange)
        // and also the custom attributes that the user already has
        //  - make sure we have these attributes
        assertEquals(8, soapPrincipal.getAttributes().length);
        assertContainsElements(soapPrincipal.getAttribute(REQUIRES_PASSWORD_CHANGE).getValues(), "false");
        assertContainsElements(soapPrincipal.getAttribute(PASSWORD_LAST_CHANGED).getValues(), "1241415585824");
        assertContainsElements(soapPrincipal.getAttribute(USER2_ATTRIBUTE1_NAME).getValues(), "bear", "dog");
        assertContainsElements(soapPrincipal.getAttribute(USER2_ATTRIBUTE2_NAME).getValues(), "chocolate");
    }

    public void testAddRemovePrincipalCustomAttributesWithExistingCustomAttributes() throws Exception
    {
        intendToModifyData();

        SOAPPrincipal soapPrincipal = securityServerClient.findPrincipalWithAttributesByName(USER_2);
        assertEquals(USER_2,soapPrincipal.getName());

        // Check only have the primary attributes and default attributes
        assertEquals(8, soapPrincipal.getAttributes().length);

        // Add 2 new custom attributes and check
        securityServerClient.addAttributeToPrincipal(USER_2, USER1_ATTRIBUTE1_VALUE);
        securityServerClient.addAttributeToPrincipal(USER_2, USER1_ATTRIBUTE2_VALUE);
        soapPrincipal = securityServerClient.findPrincipalWithAttributesByName(USER_2);
        assertEquals(10, soapPrincipal.getAttributes().length);
        assertContainsElements(soapPrincipal.getAttribute(USER1_ATTRIBUTE1_NAME).getValues(), "green");
        assertContainsElements(soapPrincipal.getAttribute(USER1_ATTRIBUTE2_NAME).getValues(), "one", "two", "three");

        // Remove 1 custom attribute and check
        securityServerClient.removeAttributeFromPrincipal(USER_2, USER1_ATTRIBUTE1_NAME);
        soapPrincipal = securityServerClient.findPrincipalWithAttributesByName(USER_2);
        assertEquals(9, soapPrincipal.getAttributes().length);
        assertContainsElements(soapPrincipal.getAttribute(USER1_ATTRIBUTE2_NAME).getValues(), "one", "two", "three");
        assertNull(soapPrincipal.getAttribute(USER1_ATTRIBUTE1_NAME));

        // check all other attributes still exist
        assertContainsElements(soapPrincipal.getAttribute(GIVEN_NAME).getValues(), "Normal");
        assertContainsElements(soapPrincipal.getAttribute(LAST_NAME).getValues(), "User2");
        assertContainsElements(soapPrincipal.getAttribute(DISPLAY_NAME).getValues(), "Normal User2");
        assertContainsElements(soapPrincipal.getAttribute(MAIL).getValues(), "user2@user.com");
        assertContainsElements(soapPrincipal.getAttribute(REQUIRES_PASSWORD_CHANGE).getValues(), "false");
        assertContainsElements(soapPrincipal.getAttribute(PASSWORD_LAST_CHANGED).getValues(), "1241415585824");
        assertContainsElements(soapPrincipal.getAttribute(USER2_ATTRIBUTE1_NAME).getValues(), "bear", "dog");
        assertContainsElements(soapPrincipal.getAttribute(USER2_ATTRIBUTE2_NAME).getValues(), "chocolate");
    }
}
