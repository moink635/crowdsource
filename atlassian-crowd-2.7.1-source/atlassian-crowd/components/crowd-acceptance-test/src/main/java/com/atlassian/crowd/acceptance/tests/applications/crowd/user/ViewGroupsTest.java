package com.atlassian.crowd.acceptance.tests.applications.crowd.user;

public class ViewGroupsTest extends CrowdUserConsoleAcceptenceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        loadXmlOnSetUp("userconsoletest.xml");
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testViewGroupsForAdmin()
    {
        log("Running testViewGroupsForAdmin");

        _loginAdminUser();

        gotoPage("/console/user/viewgroups.action");

        assertKeyPresent("menu.user.console.viewgroups.label");
        assertKeyPresent("user.console.groups.text");
        assertTextPresent("crowd-administrators");
    }

    public void testViewGroupsForUser()
    {
        log("Running testViewGroupsForUser");

        _loginTestUser();

        gotoPage("/console/user/viewgroups.action");

        assertKeyPresent("menu.user.console.viewgroups.label");
        assertKeyPresent("user.console.groups.text");
        assertTextPresent("crowd-users");
    }

    public void testViewGroupsForUserWithoutAnyGroups()
    {
        log("Running testViewGroupsForUserWithoutAnyGroups");

        _loginImmutableUser();

        gotoPage("/console/user/viewgroups.action");

        assertKeyPresent("menu.user.console.viewgroups.label");
        assertKeyPresent("user.console.nogroups.text");
    }
}
