package com.atlassian.crowd.acceptance.tests;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import com.atlassian.core.util.PropertyUtils;
import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import com.atlassian.crowd.acceptance.utils.CrowdWebTestCase;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.rest.service.factory.RestCrowdClientFactory;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;

import org.hamcrest.CoreMatchers;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.hamcrest.text.IsEmptyString;

import junit.framework.AssertionFailedError;

import static org.junit.Assert.assertThat;

/**
 * Generic ApplicationAcceptanceTestCase base class for all
 * web acceptance tests.
 *
 * This class loads up the required properties from
 * <code>localtest.properties</code>.
 *
 */
public abstract class ApplicationAcceptanceTestCase extends CrowdWebTestCase
{
    // crowd admin user
    protected static final String CROWD_ADMIN_USER = "admin";
    protected static final String CROWD_ADMIN_EMAIL_ADDRESS = "admin@example.com";
    protected static final String CROWD_ADMIN_FULLNAME = "Super User";
    protected static final String ADMIN_PW = "admin";

    protected String baseUrl;
    protected Properties specProperties;

    // note that the same tester instance is shared between the crowd console
    // and other app. if you need access to the crowd console, call the useCrowd()
    // method prior to using this object and call the useApp() method to switch
    // back to the app.
    protected CrowdAcceptanceTestCase crowdConsole;

    protected String crowdBaseUrl;
    protected String crowdResourceBundle;
    protected static final String CROWD_ADMINISTRATORS_GROUP = "crowd-administrators";

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        // load specific properties if defined
        specProperties = AcceptanceTestHelper.loadProperties(getLocalTestPropertiesFileName());

        BaseUrlFromProperties bufp = new BaseUrlFromProperties(specProperties);


        baseUrl = bufp.baseUrlFor(getApplicationName());

        getTestContext().setBaseUrl(baseUrl);

        setScriptingEnabled(false);
        beginAt(baseUrl);

        // hook up crowd tester
        crowdBaseUrl = bufp.baseUrlFor("crowd");

        crowdConsole = new CrowdAcceptanceTestCase();
        crowdConsole.setTester(this.getTester());
        crowdConsole.setUp();
        crowdResourceBundle = "com.atlassian.crowd.console.action.BaseAction";

        // reset context to app
        useApp();

        log("Running: " + this.getClass().getSimpleName() + " - " + this.getName());
    }

    @Override
    protected void tearDown() throws Exception
    {
        crowdConsole.tearDown();
        crowdConsole = null;

        super.tearDown();
    }

    protected String getTestProperty(String propName)
    {
        if (System.getProperty("acceptance.test." + propName) != null)
        {
            return System.getProperty("acceptance.test." + propName);
        }
        else if (specProperties.getProperty(propName) != null)
        {
            return specProperties.getProperty(propName);
        }

        return "";
    }

    protected void useCrowd()
    {
        getTestContext().setBaseUrl(crowdBaseUrl);
        getTestContext().setResourceBundleName(crowdResourceBundle);
    }

    protected void useApp()
    {
        getTestContext().setBaseUrl(baseUrl);
        getTestContext().setResourceBundleName(getResourceBundleName());
    }

    public void restoreBaseSetup()
    {
        restoreCrowdFromXML("basesetup.xml");
    }

    public void intendToModifyData()
    {
        useCrowd();
        crowdConsole.intendToModifyData();
        useApp();
    }

    public void restoreCrowdFromXML(String xmlFilename)
    {
        useCrowd();
        crowdConsole.restoreCrowdFromXML(xmlFilename);
        useApp();
    }

    protected void logoutFromCrowd()
    {
        useCrowd();
        crowdConsole._logout();
        useApp();
    }

    protected void loginToCrowd()
    {
        useCrowd();
        crowdConsole._logout();
        crowdConsole.assertKeyPresent("login.title");
        crowdConsole.setTextField("j_username", CROWD_ADMIN_USER);
        crowdConsole.setTextField("j_password", ADMIN_PW);
        crowdConsole.submit();
        crowdConsole.assertLinkPresentWithKey("menu.logout.label");
        useApp();
    }

    public String getCrowdHome()
    {
        String crowdHome = System.getProperty("crowd.home");

        if (crowdHome == null)
        {
            // Lets just use java io tmp
            return System.getProperty("java.io.tmp");
        }

        return crowdHome;
    }

    protected abstract String getResourceBundleName();

    protected abstract String getApplicationName();

    protected abstract String getLocalTestPropertiesFileName();

    protected void verifyUserExistsInCrowd(String username, String firstname, String lastname, String email, String... groupNames)
        throws IOException, CrowdException, ApplicationPermissionException
    {
        CrowdClient client = getCrowdClient();

        User user = client.getUser(username);

        assertEquals(username, user.getName());
        assertEquals(firstname + " " + lastname, user.getDisplayName());
        assertEquals(email, user.getEmailAddress());

        assertTrue(user.isActive());

        List<String> groups = client.getNamesOfGroupsForUser(username, 0, EntityQuery.ALL_RESULTS);

        assertThat(groups, IsIterableContainingInAnyOrder.containsInAnyOrder(groupNames));
    }

    protected void verifyUserDoesNotExistInCrowd(String username, String firstname, String lastname, String email, String... groupNames)
            throws IOException, CrowdException, ApplicationPermissionException
    {
        CrowdClient client = getCrowdClient();

        try
        {
            client.getUser(username);
            fail("User '" + username + "' should not exist");
        }
        catch (UserNotFoundException e)
        {
            // Okay
        }
    }

    protected void verifyGroupExistsInCrowd(String groupName, String... memberNames)
            throws IOException, CrowdException, ApplicationPermissionException
    {
        CrowdClient client = getCrowdClient();

        Group group = client.getGroup(groupName);
        assertEquals(groupName, group.getName());
        assertThat(group.getDescription(), IsEmptyString.isEmptyOrNullString());
        assertTrue(group.isActive());

        assertThat(client.getNamesOfUsersOfGroup(groupName, 0, EntityQuery.ALL_RESULTS),
                IsIterableContainingInAnyOrder.containsInAnyOrder(memberNames));
    }

    protected void verifyGroupDoesNotExistInCrowd(String groupName)
            throws IOException, CrowdException, ApplicationPermissionException
    {
        CrowdClient client = getCrowdClient();

        try
        {
            client.getGroup(groupName);
            fail("Group '" + groupName + "' should not exist");
        }
        catch (GroupNotFoundException e)
        {
            // Okay
        }
    }

    /**
     * Either the group doesn't exist or its name has a different case.
     */
    protected void verifyGroupDoesNotExistInCrowdWithCaseSensitiveName(String groupName)
            throws IOException, CrowdException, ApplicationPermissionException
    {
        CrowdClient client = getCrowdClient();

        try
        {
            Group group = client.getGroup(groupName);
            assertThat(group.getName(), CoreMatchers.not(groupName));
        }
        catch (GroupNotFoundException e)
        {
            // Okay
        }
    }

    protected String getCurrentlyLoggedInCrowdUserFullName()
    {
        useCrowd();

        crowdConsole.gotoPage("/console");

        try
        {
            return crowdConsole.getElementTextById("userFullName");
        }
        catch (AssertionFailedError e)
        {
            return null;
        }
        finally
        {
            useApp();
        }
    }

    /**
     * Login to the Crowd Console.
     *
     * @param username Crowd Console username.
     * @param password Crowd Console password.
     * @return full name of the successfully logged in user or <code>null</code>
     *         if the authentication failed.
     */
    protected String loginToCrowd(String username, String password)
    {
        useCrowd();
        crowdConsole._logout();
        crowdConsole.assertKeyPresent("login.title");
        crowdConsole.setTextField("j_username", username);
        crowdConsole.setTextField("j_password", password);
        crowdConsole.submit();

        // quick verification
        try
        {
            crowdConsole.assertLinkPresentWithKey("menu.logout.label");
            return crowdConsole.getElementTextById("userFullName");
        }
        catch (AssertionFailedError e)
        {
            return null;
        }
        finally
        {
            useApp();
        }
    }

    CrowdClient getCrowdClient() throws IOException
    {
        String home = System.getProperty("crowd.home");

        assertNotNull(home);

        File propsFile = new File(home, "crowd.properties");

        return new RestCrowdClientFactory().newInstance(ClientPropertiesImpl.newInstanceFromProperties(PropertyUtils.getPropertiesFromFile(propsFile)));
    }

    public String getBaseUrl()
    {
        return baseUrl;
    }
}
