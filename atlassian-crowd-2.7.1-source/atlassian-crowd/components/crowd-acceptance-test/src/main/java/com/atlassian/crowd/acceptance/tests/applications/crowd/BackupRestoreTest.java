package com.atlassian.crowd.acceptance.tests.applications.crowd;

import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class BackupRestoreTest extends CrowdAcceptanceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        _loginAdminUser();
    }

    public void testRestoreWithNoFileName()
    {
        gotoRestore();
        submit();

        assertErrorPresentWithKey("administration.restore.filePath.error");
    }

    public void testRestoreUTF8BasedFile()
    {
        gotoRestore();

        restoreCrowdFromXML("utf8test.xml");

        _loginAdminUser();
    }

    public void testRestoreValidationFailWithDuplicateMixedCaseGroups()
    {
        gotoRestore();

        restoreCrowdFromXMLWithoutReloggingIn("restorevalidationfail.xml", false);

        assertTextPresent("XML file failed validation: Could not add role with name 'cRoWd-aDminisTraTorS' as it matches another group name in the same directory");
    }

    public void testRestoreInvalidData()
    {
        gotoRestore();

        getTester().setIgnoreFailingStatusCodes(true); // continue in spite of the HTTP 500

        restoreCrowdFromXMLWithoutReloggingIn("invalidimportdata.xml", false);

        getTester().setIgnoreFailingStatusCodes(false);

        assertKeyPresent("error.import.remove");

        clickLink("remove-errors");

        _loginAdminUser();
    }

    public void testRestoreMixedCaseData()
    {
        gotoRestore();

        restoreCrowdFromXML("mixedcase.xml");

        _loginAdminUser();

        gotoViewPrincipal("mixedcase", "Test Internal Directory");
        assertTextPresent("MixedCase");
        assertLinkPresent("viewgroup-MixedGroup-Test Internal Directory"); // we can go to the group ... table match failed

        gotoViewGroup("MixedGroup", "Test Internal Directory");
        assertTextPresent("MixedGroup");

        clickLink("view-group-users");
        assertTextInTable("view-group-users", new String[] { "MixedCase", "mixed@atlassian.com", "true" });
    }

    public void testBackupWithNoFileName()
    {
        gotoBackup();

        setTextField("exportFileName", "");

        submit();

        assertErrorPresentWithKey("administration.backup.fileName.error");
    }

    public void testBackupWithInvalidFileName()
    {
        gotoBackup();

        setTextField("exportFileName", "../blah.xml");

        submit();

        assertErrorPresentWithKey("administration.backup.fileName.error");

        gotoBackup();

        setTextField("exportFileName", "\\blah.xml");

        submit();

        assertErrorPresentWithKey("administration.backup.fileName.error");
    }

    public void testBackupPrepopulatesWithDefaultFileName()
    {
        gotoBackup();

        String backupFileName = getTester().getTestingEngine().getTextFieldValue("exportFileName");
        assertTrue(StringUtils.isNotBlank(backupFileName));
        assertTrue(backupFileName.startsWith("atlassian-crowd-"));
    }

    public void testBackupSuccessful()
    {
        String backupFileName = "BackupRestoreTest_TempBackupFile.xml";
        String backupFilePath = getCrowdHome() + File.separator + "backups" + File.separator + backupFileName;

        gotoBackup();

        setTextField("exportFileName", backupFileName);

        submit();

        // can't use keys since can't assert the time taken
        assertTextPresent("Crowd has successfully backed up to " + backupFilePath);

        // Check the file really exists
        File fileLocation = new File(backupFilePath);
        assertTrue(fileLocation.exists());
        //delete the file
        assertTrue(fileLocation.delete());
    }
}
