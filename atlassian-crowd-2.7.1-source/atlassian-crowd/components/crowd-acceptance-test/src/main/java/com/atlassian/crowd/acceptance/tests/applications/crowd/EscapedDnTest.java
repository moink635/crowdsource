package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;

import com.atlassian.crowd.acceptance.utils.DbCachingTestHelper;
import com.atlassian.crowd.embedded.api.User;

import com.google.common.collect.ImmutableList;

import org.hamcrest.collection.IsIterableContainingInAnyOrder;

import static org.junit.Assert.assertThat;

/**
 * This test modifies the data in the ApacheDS server. For now, that means it must be the last
 * test run.
 */
public class EscapedDnTest extends CrowdAcceptanceTestCase
{
    private static final String CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING = "ApacheDS Caching Directory";
    private static final long MAX_SYNC_WAIT_TIME_MS = 60000L; // 1 minute

    private static boolean ldifSetup = false;

    private DbCachingTestHelper dbCachingTestHelper;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        _loginAdminUser();
        restoreCrowdFromXML("viewdirectory.xml");

        if (!ldifSetup)
        {
            InputStream in = getClass().getResourceAsStream("EscapedDnTest-entries.ldif");

            assertNotNull(in);
            new LdifLoaderForTesting(getBaseUrl()).setLdif(in);
            ldifSetup = true;
        }

        dbCachingTestHelper = new DbCachingTestHelper(tester);
    }

    private void searchAllPrincipals()
    {
        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        submit("submit-search");
    }

    private void synchroniseDirectory()
    {
        dbCachingTestHelper.synchroniseDirectory(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING, MAX_SYNC_WAIT_TIME_MS);
    }

    public void testSynchronisationPicksUpUsersAndGroupsWithDnsThatRequireEscaping()
    {
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        clickLinkWithExactText("Configuration");
        setWorkingForm("configuration_details");
        setTextField("userDNaddition", ""); // remove "users" restriction"
        submit();

        synchroniseDirectory();

        // now check the users can be found
        searchAllPrincipals();

        Collection<org.hamcrest.Matcher<? super User>> expectedUsers = ImmutableList.<org.hamcrest.Matcher<? super User>>of(
                userWithDetails("User \"with\" Quotes", "user-with-quotes"),
                userWithDetails("User with Spaces", "user-with-spaces"),
                userWithDetails("User,with,Commas", "user-with-commas"),
                userWithDetails("User/with/Slashes", "u/s/e/r"),
                userWithDetails("User\\with\\Backslashes", "u\\s\\e\\r")
        );

        List<User> users = getUserDetailsTableContents();

        assertThat(users, IsIterableContainingInAnyOrder.containsInAnyOrder(expectedUsers));
    }

    public void testEmptyOUWithSlashesInDnWarnsOnlyAboutMissingEntries() throws InterruptedException
    {
        // Go back to the directory and change the config
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING));
        // Config Tab
        clickLinkWithExactText("Connector");
        setWorkingForm("connectordetails");
        setTextField("baseDN", "ou=escaping,dc=example,dc=com");
        submit();
        clickLinkWithExactText("Configuration");
        setWorkingForm("configuration_details");
        setTextField("userDNaddition", "ou=unit/with/slashes");
        submit();

        clickButton("test-search-principal");

        assertKeyPresent("directoryconnector.testsearch.invalid");

        String warning = getElementTextByXPath("//p[@class='warningBox']").trim();
        assertEquals("The warning should not include any exception",
                getMessage("directoryconnector.testsearch.invalid"), warning);
    }
}
