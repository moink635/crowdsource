package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.integration.rest.service.RestCrowdClient;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.util.Properties;
import java.util.Set;

/**
 * Tests the directory amalgamation logic.
 */
public class DirectoryAmalgamationTest extends CrowdAcceptanceTestCase
{
    private static final String APPLICATION_NAME = "crowd";
    private static final String APPLICATION_PASSWORD = "qybhDMZh";

    private CrowdClient crowdClient;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        final Properties properties = new Properties();
        properties.setProperty(Constants.PROPERTIES_FILE_BASE_URL, HOST_PATH);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_NAME, APPLICATION_NAME);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_PASSWORD, APPLICATION_PASSWORD);

        final ClientProperties clientProperties = ClientPropertiesImpl.newInstanceFromProperties(properties);

        crowdClient = new RestCrowdClient(clientProperties);
        restoreCrowdFromXML("directoryamalgamation.xml");
    }

    /**
     * Tests that retrieving the direct memberships of user occurs only in the directory the user is found in (i.e. one
     * directory only).
     */
    public void testDirectGroupMembershipAmalgamation() throws Exception
    {
        final Set<String> expectedGroupNames = ImmutableSet.of("dir1group1");
        final Set<String> groupNames = Sets.newHashSet(crowdClient.getNamesOfGroupsForUser("user1", 0, EntityQuery.ALL_RESULTS));
        assertEquals(expectedGroupNames, groupNames);
    }

    /**
     * Tests that retrieving the nested memberships of user occurs only in the directory the user is found in (i.e. one
     * directory only).
     */
    public void testNestedGroupMembershipAmalgamation() throws Exception
    {
        final Set<String> expectedGroupNames = ImmutableSet.of("dir2childgroup", "dir2parentgroup");
        final Set<String> groupNames = Sets.newHashSet(crowdClient.getNamesOfGroupsForNestedUser("nesteduser", 0, EntityQuery.ALL_RESULTS));
        assertEquals(expectedGroupNames, groupNames);
    }

    /**
     * Tests that retrieving the direct user members of a group amalgamates across all the directories (not just one).
     */
    public void testDirectUserMembersAmalgamation() throws Exception
    {
        final Set<String> expectedUserNames = ImmutableSet.of("dir1user", "dir2user");
        final Set<String> userNames = Sets.newHashSet(crowdClient.getNamesOfUsersOfGroup("commongroup", 0, EntityQuery.ALL_RESULTS));
        assertEquals(expectedUserNames, userNames);
    }

    /**
     * Tests that retrieving the nested user members of a group amalgamates across all the directories (not just one).
     * directory only).
     */
    public void testNestedUserMembersAmalgamation() throws Exception
    {
        final Set<String> expectedUserNames = ImmutableSet.of("dir1user", "dir2user");
        final Set<String> userNames = Sets.newHashSet(crowdClient.getNamesOfNestedUsersOfGroup("commonparentgroup", 0, EntityQuery.ALL_RESULTS));
        assertEquals(expectedUserNames, userNames);
    }

    /**
     * Tests that determining the direct group membership of a user is only performed in the directory the user is first
     * found in.
     */
    public void testIsUserDirectGroupMember() throws Exception
    {
        assertTrue(crowdClient.isUserDirectGroupMember("user1", "dir1group1"));
        assertFalse(crowdClient.isUserDirectGroupMember("user1", "dir2group1"));
    }

    /**
     * Tests that determining the nested group membership of a user is only performed in the directory the user is first
     * found in.
     */
    public void testIsUserNestedGroupMember() throws Exception
    {
        assertTrue(crowdClient.isUserNestedGroupMember("nesteduser", "dir2parentgroup"));
        assertTrue(crowdClient.isUserNestedGroupMember("nesteduser", "dir2childgroup"));
        assertFalse(crowdClient.isUserNestedGroupMember("nesteduser", "dir3parentgroup"));
        assertFalse(crowdClient.isUserNestedGroupMember("nesteduser", "dir3childgroup"));
    }
}
