package com.atlassian.crowd.acceptance.tests.persistence.dao.directory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.atlassian.crowd.embedded.api.Directories.namesOf;
import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.springframework.test.jdbc.JdbcTestUtils.deleteFromTables;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class DirectoryDAOHibernateTest
{
    @Inject private DirectoryDAOHibernate directoryDAO;
    @Inject private SessionFactory sessionFactory;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    private static final long DIRECTORY_ID = 1;

    private static final String DIRECTORY_NAME_1 = "directory1";
    private static final String DIRECTORY_NAME_2 = "Directory Too";
    private static final String DIRECTORY_NAME_3 = "Third One";

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        // Fix up the hi-lo stuff so we can insert attributes etc with the correct primary key
        List errors = new ArrayList();
        resetableHiLoGeneratorHelper.setNextHiValue(errors);
        assertTrue(errors.isEmpty());
    }

    @Test
    public void testAddDirectory()
    {
        DirectoryImpl directory = new DirectoryImpl();
        directory.setActive(true);
        directory.setName("New Directory");
        directory.setImplementationClass("com.atlassian.crowd.TestClass");
        directory.setType(DirectoryType.INTERNAL);
        directory.setAttribute("key", "value");

        DirectoryImpl returnedDirectory = directoryDAO.add(directory);

        assertNotNull(returnedDirectory);
        assertNotNull(returnedDirectory.getId());
        assertTrue(returnedDirectory.isActive());
        assertEquals(DirectoryType.INTERNAL, returnedDirectory.getType());
        assertEquals("New Directory", returnedDirectory.getName());
        assertEquals(toLowerCase("New Directory"), returnedDirectory.getLowerName());
        assertNotNull(returnedDirectory.getCreatedDate());
        assertNotNull(returnedDirectory.getUpdatedDate());
        assertEquals("com.atlassian.crowd.TestClass", returnedDirectory.getImplementationClass());
        assertEquals("com.atlassian.crowd.TestClass".toLowerCase(Locale.ENGLISH), returnedDirectory.getLowerImplementationClass());
        assertNull(returnedDirectory.getDescription());
        assertFalse(returnedDirectory.getAttributes().isEmpty());
        assertTrue(returnedDirectory.getAllowedOperations().isEmpty());
        assertEquals("value", returnedDirectory.getValue("key"));
    }

    @Test (expected = ConstraintViolationException.class)
    public void testAddDirectoryDuplicate()
    {
        DirectoryImpl directory = new DirectoryImpl();
        directory.setActive(false);
        directory.setName(DIRECTORY_NAME_1);
        directory.setImplementationClass("com.atlassian.crowd.TestClassic");
        directory.setType(DirectoryType.CONNECTOR);

        directoryDAO.add(directory);

        // the contract of DirectoryDAO.add() does not require implementations to throw an exception. Therefore,
        // the exception may come later, when the session is flushed
        sessionFactory.getCurrentSession().flush();
    }

    @Test (expected = NullPointerException.class)
    public void testAddDirectoryIncomplete()
    {
        DirectoryImpl directory = new DirectoryImpl();
        directory.setActive(true);
        directory.setName("New Directory");

        // directory type is not set

        directoryDAO.add(directory);
    }

    @Test
    public void testFindById() throws Exception
    {
        DirectoryImpl directory = directoryDAO.findById(DIRECTORY_ID);

        assertNotNull(directory.getId());
        assertEquals(DIRECTORY_NAME_1, directory.getName());
        assertEquals(toLowerCase(DIRECTORY_NAME_1), directory.getLowerName());
        assertEquals("Simple Directory", directory.getDescription());
        assertEquals("com.atlassian.crowd.directory.InternalDirectory", directory.getImplementationClass());
        assertEquals("com.atlassian.crowd.directory.InternalDirectory".toLowerCase(Locale.ENGLISH),
                     directory.getLowerImplementationClass());
        assertEquals(DirectoryType.INTERNAL, directory.getType());
        assertTrue(directory.isActive());
        assertEquals("Ten", directory.getValue("Number"));
        assertEquals("Red", directory.getValue("color"));
        assertEquals(4, directory.getKeys().size());
        assertEquals(6, directory.getAllowedOperations().size());
        assertTrue(directory.getAllowedOperations().contains(OperationType.CREATE_GROUP));
        assertTrue(directory.getAllowedOperations().contains(OperationType.CREATE_USER));
        assertTrue(directory.getAllowedOperations().contains(OperationType.UPDATE_GROUP));
        assertTrue(directory.getAllowedOperations().contains(OperationType.UPDATE_USER));
        assertTrue(directory.getAllowedOperations().contains(OperationType.DELETE_GROUP));
        assertTrue(directory.getAllowedOperations().contains(OperationType.DELETE_USER));
    }

    @Test (expected = DirectoryNotFoundException.class)
    public void testFindByIdNotFound() throws Exception
    {
        directoryDAO.findById(-1L);
    }

    @Test
    public void testFindByName() throws Exception
    {
        DirectoryImpl directory = directoryDAO.findByName(DIRECTORY_NAME_2.toUpperCase());

        assertNotNull(directory.getId());
        assertEquals(DIRECTORY_NAME_2, directory.getName());
        assertEquals(toLowerCase(DIRECTORY_NAME_2), directory.getLowerName());
        assertEquals("Another Simple Directory", directory.getDescription());
        assertEquals("com.atlassian.crowd.directory.MyDirectory", directory.getImplementationClass());
        assertEquals("com.atlassian.crowd.directory.MyDirectory".toLowerCase(Locale.ENGLISH), directory.getLowerImplementationClass());
        assertEquals(DirectoryType.CUSTOM, directory.getType());
        assertFalse(directory.isActive());
    }

    @Test (expected = DirectoryNotFoundException.class)
    public void testFindByNameNotFound() throws Exception
    {
        directoryDAO.findByName("bogus");
    }

    @Test
    public void testUpdateDirectory() throws Exception
    {
        DirectoryImpl directory = directoryDAO.findById(DIRECTORY_ID);

        directory.setActive(false);
        directory.setName("New Directory");
        directory.setImplementationClass("com.atlassian.crowd.TestClassic");
        directory.setType(DirectoryType.CUSTOM);
        directory.setDescription(null);

        directoryDAO.update(directory);

        DirectoryImpl updatedDirectory = directoryDAO.findById(DIRECTORY_ID);
        assertEquals("New Directory", updatedDirectory.getName());
        assertEquals(toLowerCase("New Directory"), updatedDirectory.getLowerName());
        assertNull(updatedDirectory.getDescription());
        assertEquals("com.atlassian.crowd.TestClassic", updatedDirectory.getImplementationClass());
        assertEquals("com.atlassian.crowd.TestClassic".toLowerCase(Locale.ENGLISH), directory.getLowerImplementationClass());
        assertEquals(DirectoryType.CUSTOM, updatedDirectory.getType());
        assertFalse(updatedDirectory.isActive());
        assertEquals("Ten", directory.getValue("Number"));
        assertEquals("Red", directory.getValue("color"));
        assertEquals(4, directory.getKeys().size());
        assertEquals(6, directory.getAllowedOperations().size());
    }

    @Test
    public void testAddDirectoryWithBellsAndWhistles() throws Exception
    {
        DirectoryImpl directory = new DirectoryImpl();
        directory.setActive(true);
        directory.setName("New Directory");
        directory.setImplementationClass("com.atlassian.crowd.TestClass");
        directory.setType(DirectoryType.INTERNAL);
        directory.setAttribute("One", "Two");
        directory.setAttribute("Three", "Four");
        Set<OperationType> ops = new HashSet<OperationType>(Arrays.asList(OperationType.CREATE_GROUP, OperationType.CREATE_USER));
        directory.setAllowedOperations(ops);

        directoryDAO.add(directory);

        DirectoryImpl addedDirectory = directoryDAO.findByName(directory.getName().toUpperCase());

        assertTrue(directory.isActive());
        assertEquals("New Directory", addedDirectory.getName());
        assertEquals(toLowerCase("New Directory"), addedDirectory.getLowerName());
        assertEquals("com.atlassian.crowd.TestClass", addedDirectory.getImplementationClass());
        assertEquals("com.atlassian.crowd.TestClass".toLowerCase(Locale.ENGLISH), addedDirectory.getLowerImplementationClass());
        assertEquals(DirectoryType.INTERNAL, addedDirectory.getType());
        assertEquals("Two", directory.getValue("One"));
        assertEquals("Four", directory.getValue("Three"));
        assertEquals(2, directory.getKeys().size());
        assertEquals(ops, directory.getAllowedOperations());
    }

    @Test
    public void testUpdateDirectoryWithBellsAndWhistles() throws Exception
    {
        DirectoryImpl directory = directoryDAO.findById(DIRECTORY_ID);

        directory.setActive(false);
        directory.setName("New Directory");
        directory.setImplementationClass("com.atlassian.crowd.TestClassic");
        directory.setType(DirectoryType.CUSTOM);
        directory.setDescription(null);
        directory.setAttribute("Number", "one");
        directory.getAllowedOperations().remove(OperationType.DELETE_GROUP);
        directory.getAllowedOperations().remove(OperationType.DELETE_USER);

        directoryDAO.update(directory);

        DirectoryImpl updatedDirectory = directoryDAO.findById(DIRECTORY_ID);
        assertEquals("New Directory", updatedDirectory.getName());
        assertEquals(toLowerCase("New Directory"), updatedDirectory.getLowerName());
        assertNull(updatedDirectory.getDescription());
        assertEquals("com.atlassian.crowd.TestClassic", updatedDirectory.getImplementationClass());
        assertEquals("com.atlassian.crowd.TestClassic".toLowerCase(Locale.ENGLISH), updatedDirectory.getLowerImplementationClass());
        assertEquals(DirectoryType.CUSTOM, updatedDirectory.getType());
        assertFalse(updatedDirectory.isActive());
        assertEquals("one", directory.getValue("Number"));
        assertEquals("Red", directory.getValue("color"));
        assertEquals(4, directory.getKeys().size());
        assertEquals(4, directory.getAllowedOperations().size());
        assertTrue(directory.getAllowedOperations().contains(OperationType.CREATE_GROUP));
        assertTrue(directory.getAllowedOperations().contains(OperationType.CREATE_USER));
        assertTrue(directory.getAllowedOperations().contains(OperationType.UPDATE_GROUP));
        assertTrue(directory.getAllowedOperations().contains(OperationType.UPDATE_USER));
    }

    private static final String[] TABLES_TO_DELETE = {
        "cwd_app_dir_group_mapping",
        "cwd_app_dir_operation",
        "cwd_app_dir_mapping"
    };

    @Test
    public void testRemove() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        // pre-delete things that can cause FK violations (these are removed at the manager-level when deleting a directory)
        deleteFromTables(jdbcTemplate, TABLES_TO_DELETE);

        DirectoryImpl directory = directoryDAO.findById(DIRECTORY_ID);

        directoryDAO.remove(directory);

        try
        {
            directoryDAO.findById(DIRECTORY_ID);
            fail("DirectoryNotFoundException expected");
        }
        catch (DirectoryNotFoundException e)
        {
            // expected
        }

        // the following asserts depend on the delete-on-cascade behavior of the database, therefore we must flush
        sessionFactory.getCurrentSession().flush();

        assertThat(jdbcTemplate.queryForObject("select count(*) from cwd_directory_attribute where directory_id = ?",
                                               Integer.class, DIRECTORY_ID), is(0));
        assertThat(jdbcTemplate.queryForObject("select count(*) from cwd_directory_operation where directory_id = ?",
                                               Integer.class, DIRECTORY_ID), is(0));

        assertThat(jdbcTemplate.queryForObject("select count(*) from cwd_user where directory_id = ?",
                                               Integer.class, DIRECTORY_ID), is(0));
        assertThat(jdbcTemplate.queryForObject("select count(*) from cwd_user_attribute where directory_id = ?",
                                               Integer.class, DIRECTORY_ID), is(0));
        assertThat(jdbcTemplate.queryForObject("select count(*) from cwd_user_credential_record",
                                               Integer.class), is(0)); // this assumes only DIRECTORY_ID has user credentials and no other directory

        assertThat(jdbcTemplate.queryForObject("select count(*) from cwd_group where directory_id = ?",
                                               Integer.class, DIRECTORY_ID), is(0));
        assertThat(jdbcTemplate.queryForObject("select count(*) from cwd_group_attribute where directory_id = ?",
                                               Integer.class, DIRECTORY_ID), is(0));

        assertThat(jdbcTemplate.queryForObject("select count(*) from cwd_membership where directory_id = ?",
                                               Integer.class, DIRECTORY_ID), is(0));
    }

    @Test
    public void testSearchAll()
    {
        List<Directory> directories = directoryDAO.search(QueryBuilder.queryFor(Directory.class,
                                                                                EntityDescriptor.directory()).returningAtMost(10));

        assertThat(namesOf(directories), containsInAnyOrder(DIRECTORY_NAME_1, DIRECTORY_NAME_2, DIRECTORY_NAME_3));
    }

    @Test
    public void testSearchByNameStartingWith()
    {
        List<Directory> directories = directoryDAO.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(Restriction.on(DirectoryTermKeys.NAME).startingWith(
            "directory")).returningAtMost(10));

        assertThat(namesOf(directories), containsInAnyOrder(DIRECTORY_NAME_1, DIRECTORY_NAME_2));
    }

    @Test
    public void testSearchByNameContaining()
    {
        List<Directory> direcotries = directoryDAO.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(Restriction.on(DirectoryTermKeys.NAME).containing(
            "oo")).returningAtMost(10));

        assertThat(namesOf(direcotries), containsInAnyOrder(DIRECTORY_NAME_2));
    }

    @Test
    public void testSearchByActive()
    {
        List<Directory> directories = directoryDAO.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(Restriction.on(DirectoryTermKeys.ACTIVE).exactlyMatching(true)).returningAtMost(10));

        assertThat(namesOf(directories), contains(DIRECTORY_NAME_1));
    }

    @Test
    public void testSearchByType()
    {
        List<Directory> directories = directoryDAO.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(
            DirectoryType.INTERNAL)).returningAtMost(10));

        assertThat(namesOf(directories), contains(DIRECTORY_NAME_1));
    }

    @Test
    public void testSearchByImplementationClass()
    {
        List<Directory> directories = directoryDAO.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(Restriction.on(DirectoryTermKeys.IMPLEMENTATION_CLASS).containing(
            "InternalDirectory")).returningAtMost(10));

        assertThat(namesOf(directories), contains(DIRECTORY_NAME_1));
    }

    @Test
    public void testSearchNestedQuery()
    {
        List<Directory> directories = directoryDAO.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(Combine.anyOf(Combine.allOf(Restriction.on(DirectoryTermKeys.NAME).startingWith("dir"), Restriction.on(DirectoryTermKeys.ACTIVE).exactlyMatching(true)), Combine.allOf(Restriction.on(DirectoryTermKeys.IMPLEMENTATION_CLASS).containing("my"), Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.CUSTOM)))).returningAtMost(10));

        assertThat(namesOf(directories), containsInAnyOrder(DIRECTORY_NAME_1, DIRECTORY_NAME_2));
    }
}
