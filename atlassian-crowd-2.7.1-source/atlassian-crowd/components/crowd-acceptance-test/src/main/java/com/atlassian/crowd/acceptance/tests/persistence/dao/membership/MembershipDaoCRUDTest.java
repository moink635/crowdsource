package com.atlassian.crowd.acceptance.tests.persistence.dao.membership;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.dao.membership.MembershipDAOHibernate;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import com.google.common.collect.ImmutableList;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.atlassian.crowd.search.EntityDescriptor.group;
import static com.atlassian.crowd.search.EntityDescriptor.role;
import static com.atlassian.crowd.search.EntityDescriptor.user;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Integration tests for the CRUD operations of {@link MembershipDao}. For convenience, the tests
 * of this interface have been split into multiple test classes.
 *
 * @see MembershipDaoSearchTest
 * @see MembershipDAOHibernateTest
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class MembershipDaoCRUDTest
{
    private static final long DIRECTORY_ID = 1L;
    private static final String NON_EXISTING_GROUP = "fakers";

    @Inject private MembershipDao membershipDAO;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    private static final String ADMINISTRATORS_GROUP_MIXED = "Administrators";
    private static final String ADMINISTRATORS_GROUP_ACTUAL = "administratorS";
    private static final String USERS_ROLE = "users";
    private static final String DEVELOPERS_GROUP = "developers";
    private static final String ADMIN_USER = "aDmin";
    private static final String PRODUCT_MANAGERS_ROLE = "Product Managers";

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Test
    public void testIsUserDirectMemberTrueMixedCase()
    {
        assertTrue(membershipDAO.isUserDirectMember(2L, "JohnSmith", "Testers"));
        assertTrue(membershipDAO.isUserDirectMember(2L, "JohnSmith".toUpperCase(), "Testers"));
        assertTrue(membershipDAO.isUserDirectMember(2L, "JohnSmith", "Testers".toUpperCase()));
    }

    @Test
    public void testIsUserDirectMemberTrue()
    {
        assertTrue(membershipDAO.isUserDirectMember(DIRECTORY_ID, "bob", DEVELOPERS_GROUP));
    }

    @Test
    public void testIsUserDirectMemberFalse()
    {
        assertFalse(membershipDAO.isUserDirectMember(DIRECTORY_ID, "bob", USERS_ROLE));
    }

    @Test
    public void testIsGroupDirectMemberTrue()
    {
        assertTrue(membershipDAO.isGroupDirectMember(DIRECTORY_ID, PRODUCT_MANAGERS_ROLE, USERS_ROLE));
    }

    @Test
    public void testIsGroupDirectMemberFalse()
    {
        assertFalse(membershipDAO.isGroupDirectMember(DIRECTORY_ID, DEVELOPERS_GROUP, ADMINISTRATORS_GROUP_ACTUAL));
    }

    @Test
    public void testAddUserToGroup() throws Exception
    {
        membershipDAO.addUserToGroup(DIRECTORY_ID, "jane", USERS_ROLE);

        assertTrue(membershipDAO.isUserDirectMember(DIRECTORY_ID, "jane", USERS_ROLE));
    }

    @Test (expected = MembershipAlreadyExistsException.class)
    public void testAddUserToGroupWhereMembershipExists() throws Exception
    {
        membershipDAO.addUserToGroup(2L, "johnSmith", "testers");
    }

    @Test
    public void testAddUserToGroupWhereGroupDoesNotExist() throws Exception
    {
        try
        {
            membershipDAO.addUserToGroup(DIRECTORY_ID, "jane", NON_EXISTING_GROUP);
            fail("GroupNotFoundException expected");
        }
        catch (GroupNotFoundException e)
        {
            //true!
        }

        assertEquals(0, membershipDAO.search(DIRECTORY_ID, groupsOfUser("jane")).size());

        assertFalse(membershipDAO.isUserDirectMember(DIRECTORY_ID, "jane", NON_EXISTING_GROUP));
    }

    @Test
    public void testAddUserToGroupWhereUserDoesNotExist() throws Exception
    {
        try
        {
            membershipDAO.addUserToGroup(DIRECTORY_ID, "billy", USERS_ROLE);
            fail("UserNotFoundException expected");
        }
        catch (UserNotFoundException e)
        {
            //true!
        }

        assertEquals(1,
                     membershipDAO.search(DIRECTORY_ID,
                                          QueryBuilder.queryFor(User.class, user())
                                              .childrenOf(role())
                                              .withName(USERS_ROLE)
                                              .returningAtMost(10)).size());

        assertFalse(membershipDAO.isUserDirectMember(DIRECTORY_ID, "billy", USERS_ROLE));
    }

    @Test
    public void testAddUserToGroupWhereUserDoesNotExistPreservesCase() throws Exception
    {
        try
        {
            membershipDAO.addUserToGroup(DIRECTORY_ID, "Billy", USERS_ROLE);
            fail("UserNotFoundException expected");
        }
        catch (UserNotFoundException e)
        {
            //true!
        }

        assertEquals(1,
                     membershipDAO.search(DIRECTORY_ID,
                                          QueryBuilder.queryFor(User.class, user())
                                              .childrenOf(role())
                                              .withName(USERS_ROLE)
                                              .returningAtMost(10)).size());

        assertFalse(membershipDAO.isUserDirectMember(DIRECTORY_ID, "Billy", USERS_ROLE));
    }

    @Test
    public void testAddAllUsersToGroup() throws Exception
    {
        assertFalse(membershipDAO.isUserDirectMember(DIRECTORY_ID, "bob", ADMINISTRATORS_GROUP_ACTUAL));

        final BatchResult<String> result =
            membershipDAO.addAllUsersToGroup(DIRECTORY_ID, ImmutableList.of("bob"), ADMINISTRATORS_GROUP_ACTUAL);

        assertFalse("There should not be failures", result.hasFailures());
        assertThat(result.getSuccessfulEntities(), hasItem("bob"));

        assertEquals(2, membershipDAO.search(DIRECTORY_ID,
                                             usersOfGroup(ADMINISTRATORS_GROUP_ACTUAL)).size());

        assertTrue(membershipDAO.isUserDirectMember(DIRECTORY_ID, "bob", ADMINISTRATORS_GROUP_ACTUAL));
    }

    @Test
    public void testAddAllUsersToGroupIsCaseInsensitive() throws Exception
    {
        assertFalse(membershipDAO.isUserDirectMember(DIRECTORY_ID, "bob", ADMINISTRATORS_GROUP_ACTUAL));

        final BatchResult<String> result =
            membershipDAO.addAllUsersToGroup(DIRECTORY_ID, ImmutableList.of("Bob"), ADMINISTRATORS_GROUP_ACTUAL);

        assertFalse("There should not be failures", result.hasFailures());
        assertThat(result.getSuccessfulEntities(), hasItem("bob"));  // returns the actual username

        assertEquals(2, membershipDAO.search(DIRECTORY_ID,
                                             usersOfGroup(ADMINISTRATORS_GROUP_ACTUAL)).size());

        assertTrue(membershipDAO.isUserDirectMember(DIRECTORY_ID, "bob", ADMINISTRATORS_GROUP_ACTUAL));
    }

    @Test (expected = GroupNotFoundException.class)
    public void testAddAllUsersToGroupWhenGroupDoesNotExist() throws Exception
    {
        membershipDAO.addAllUsersToGroup(DIRECTORY_ID, ImmutableList.of("jane"), NON_EXISTING_GROUP);
    }

    @Test
    public void testAddAllUsersToGroupWhenSomeUsersDoNotExist() throws Exception
    {
        assertFalse(membershipDAO.isUserDirectMember(DIRECTORY_ID, "bob", ADMINISTRATORS_GROUP_ACTUAL)); // precondition

        final BatchResult<String> result =
            membershipDAO.addAllUsersToGroup(DIRECTORY_ID, ImmutableList.of("not a user", "bob"), ADMINISTRATORS_GROUP_ACTUAL);

        assertTrue(membershipDAO.isUserDirectMember(DIRECTORY_ID, "bob", ADMINISTRATORS_GROUP_ACTUAL));

        assertThat(result.getSuccessfulEntities(), hasItem("bob"));

        assertTrue("Some entities should have failed", result.hasFailures());
        assertThat(result.getFailedEntities(), hasItem("not a user"));
    }

    @Test
    public void testAddAllUsersToGroupWhenSomeMembershipsAlreadyExist() throws Exception
    {
        assertFalse(membershipDAO.isUserDirectMember(DIRECTORY_ID, "bob", ADMINISTRATORS_GROUP_ACTUAL)); // precondition
        assertTrue(membershipDAO.isUserDirectMember(DIRECTORY_ID, "admin", ADMINISTRATORS_GROUP_ACTUAL)); // precondition

        final BatchResult<String> result =
            membershipDAO.addAllUsersToGroup(DIRECTORY_ID, ImmutableList.of("admin", "bob"), ADMINISTRATORS_GROUP_ACTUAL);

        assertTrue(membershipDAO.isUserDirectMember(DIRECTORY_ID, "bob", ADMINISTRATORS_GROUP_ACTUAL));
        assertTrue(membershipDAO.isUserDirectMember(DIRECTORY_ID, "admin", ADMINISTRATORS_GROUP_ACTUAL));

        assertThat(result.getSuccessfulEntities(), hasItem("bob"));

        assertTrue("Some entities should have failed", result.hasFailures());
        assertThat(result.getFailedEntities(), hasItem("admin"));
    }

    @Test
    public void testAddAllUsersToGroupWhenMembershipAlreadyExistIsCaseInsensitive() throws Exception
    {
        assertTrue(membershipDAO.isUserDirectMember(DIRECTORY_ID, "admin", ADMINISTRATORS_GROUP_ACTUAL)); // precondition

        final BatchResult<String> result =
            membershipDAO.addAllUsersToGroup(DIRECTORY_ID, ImmutableList.of("Admin"), ADMINISTRATORS_GROUP_ACTUAL);

        assertTrue(membershipDAO.isUserDirectMember(DIRECTORY_ID, "admin", ADMINISTRATORS_GROUP_ACTUAL));

        assertThat(result.getSuccessfulEntities(), Matchers.emptyCollectionOf(String.class));

        assertTrue("Some entities should have failed", result.hasFailures());
        assertThat(result.getFailedEntities(), hasItem("admin"));  // returns the actual username
    }

    @Test
    public void testAddGroupToGroup() throws Exception
    {
        membershipDAO.addGroupToGroup(DIRECTORY_ID, ADMINISTRATORS_GROUP_MIXED, DEVELOPERS_GROUP);


        assertEquals(1, membershipDAO.search(DIRECTORY_ID, parentGroupsOfGroup(ADMINISTRATORS_GROUP_MIXED)).size());

        assertEquals(1, membershipDAO.search(DIRECTORY_ID, childGroupsOfGroup(DEVELOPERS_GROUP)).size());

        assertTrue(membershipDAO.isGroupDirectMember(DIRECTORY_ID, ADMINISTRATORS_GROUP_MIXED, DEVELOPERS_GROUP));
    }

    @Test
    public void testAddGroupToGroupWhereMembershipExists() throws Exception
    {
        membershipDAO.addGroupToGroup(DIRECTORY_ID, ADMINISTRATORS_GROUP_MIXED, DEVELOPERS_GROUP);

        try
        {
            membershipDAO.addGroupToGroup(DIRECTORY_ID, ADMINISTRATORS_GROUP_MIXED, DEVELOPERS_GROUP);
            fail("MembershipAlreadyExistsException expected");
        }
        catch (MembershipAlreadyExistsException e)
        {
            // expected
        }
    }

    @Test
    public void testAddGroupToGroupWhereChildGroupDoesNotExist() throws Exception
    {
        try
        {
            membershipDAO.addGroupToGroup(DIRECTORY_ID, NON_EXISTING_GROUP, DEVELOPERS_GROUP);
            fail("GroupNotFoundException expected");
        }
        catch (GroupNotFoundException e)
        {
            // Win!
        }

        assertFalse(membershipDAO.isGroupDirectMember(DIRECTORY_ID, NON_EXISTING_GROUP, DEVELOPERS_GROUP));
    }

    @Test
    public void testAddGroupToGroupWhereParentGroupDoesNotExist() throws Exception
    {
        try
        {
            membershipDAO.addGroupToGroup(DIRECTORY_ID, DEVELOPERS_GROUP, NON_EXISTING_GROUP);
            fail("GroupNotFoundException expected");
        }
        catch (GroupNotFoundException e)
        {
            // Win!
        }

        assertFalse(membershipDAO.isGroupDirectMember(DIRECTORY_ID, DEVELOPERS_GROUP, NON_EXISTING_GROUP));
    }

    @Test
    public void testRemoveUserFromGroup() throws Exception
    {
        membershipDAO.removeUserFromGroup(DIRECTORY_ID, ADMIN_USER, ADMINISTRATORS_GROUP_MIXED);


        assertEquals(1, membershipDAO.search(DIRECTORY_ID, groupsOfUser(ADMIN_USER)).size());
        assertEquals(1,
                     membershipDAO.search(DIRECTORY_ID,
                                          QueryBuilder.queryFor(Group.class, role()).parentsOf(user()).withName(
                                              ADMIN_USER).returningAtMost(10)).size());

        assertEquals(0, membershipDAO.search(DIRECTORY_ID, usersOfGroup(ADMINISTRATORS_GROUP_MIXED)).size());

        assertFalse(membershipDAO.isUserDirectMember(DIRECTORY_ID, ADMIN_USER, ADMINISTRATORS_GROUP_MIXED));
    }

    @Test
    public void testRemoveUserFromGroupWhereGroupDoesNotExist() throws Exception
    {
        try
        {
            membershipDAO.removeUserFromGroup(DIRECTORY_ID, ADMIN_USER, "faker");
            fail("GroupNotFoundException expected");
        }
        catch (GroupNotFoundException e)
        {
            // Yay!
        }
        catch (MembershipNotFoundException e)
        {
            // Yay!
        }

        assertFalse(membershipDAO.isUserDirectMember(DIRECTORY_ID, ADMIN_USER, "faker"));
    }

    @Test
    public void testRemoveUserFromGroupWhereUserDoesNotExist() throws Exception
    {
        try
        {
            membershipDAO.removeUserFromGroup(DIRECTORY_ID, "billy", ADMINISTRATORS_GROUP_MIXED);
            fail("UserNotFoundException expected");
        }
        catch (UserNotFoundException e)
        {
            // Yay!
        }
        catch (MembershipNotFoundException e)
        {
            // Yay!
        }

        assertFalse(membershipDAO.isUserDirectMember(DIRECTORY_ID, "billy", ADMINISTRATORS_GROUP_MIXED));
    }

    @Test
    public void testRemoveGroupFromGroup() throws Exception
    {
        membershipDAO.removeGroupFromGroup(DIRECTORY_ID, PRODUCT_MANAGERS_ROLE, USERS_ROLE);

        assertEquals(0, membershipDAO.search(DIRECTORY_ID, parentGroupsOfGroup(DEVELOPERS_GROUP)).size());

        assertEquals(0, membershipDAO.search(DIRECTORY_ID, childGroupsOfGroup(USERS_ROLE)).size());

        assertFalse(membershipDAO.isGroupDirectMember(DIRECTORY_ID, DEVELOPERS_GROUP, USERS_ROLE));
    }

    @Test
    public void testRemoveGroupFromGroupWhereParentGroupDoesNotExist() throws Exception
    {
        try
        {
            membershipDAO.removeGroupFromGroup(DIRECTORY_ID, DEVELOPERS_GROUP, "faker");
            fail("GroupNotFoundException expected");
        }
        catch (MembershipNotFoundException e)
        {
            // Yay!
        }

        assertFalse(membershipDAO.isGroupDirectMember(DIRECTORY_ID, DEVELOPERS_GROUP, "faker"));
    }

    @Test
    public void testRemoveGroupFromGroupWhereChildGroupDoesNotExist() throws Exception
    {
        try
        {
            membershipDAO.removeGroupFromGroup(DIRECTORY_ID, "faker", DEVELOPERS_GROUP);
            fail("GroupNotFoundException expected");
        }
        catch (MembershipNotFoundException e)
        {
            // Yay!
        }

        assertFalse(membershipDAO.isGroupDirectMember(DIRECTORY_ID, "faker", DEVELOPERS_GROUP));
    }

    private static MembershipQuery<Group> groupsOfUser(String username)
    {
        return QueryBuilder.queryFor(Group.class, group()).parentsOf(user()).withName(username).returningAtMost(10);
    }

    private static MembershipQuery<User> usersOfGroup(String groupName)
    {
        return QueryBuilder.queryFor(User.class, user()).childrenOf(group()).withName(groupName).returningAtMost(10);
    }

    private MembershipQuery<Group> childGroupsOfGroup(String parentGroup)
    {
        return QueryBuilder.queryFor(Group.class, group()).childrenOf(group()).withName(parentGroup).returningAtMost(10);
    }

    private static MembershipQuery<Group> parentGroupsOfGroup(String childGroup)
    {
        return QueryBuilder.queryFor(Group.class, group()).parentsOf(group()).withName(childGroup).returningAtMost(10);
    }
}
