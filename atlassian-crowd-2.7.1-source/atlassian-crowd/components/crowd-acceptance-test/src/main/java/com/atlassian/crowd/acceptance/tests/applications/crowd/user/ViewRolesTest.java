package com.atlassian.crowd.acceptance.tests.applications.crowd.user;

import org.apache.commons.httpclient.HttpStatus;

public class ViewRolesTest extends CrowdUserConsoleAcceptenceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        loadXmlOnSetUp("userconsoletest.xml");
    }

    public void testViewRolesNoActionMapped()
    {
        log("Running testViewRolesForAdmin");

        _loginAdminUser();

        try
        {
            tester.setIgnoreFailingStatusCodes(true);
            gotoPage("/console/user/viewroles.action");
        }
        finally
        {
            tester.setIgnoreFailingStatusCodes(false);
        }

        // No action mapped
        tester.assertResponseCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }
}
