package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.util.Arrays;

/**
 * Tests updates to an application
 */
public class ViewApplicationTest extends CrowdAcceptanceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreBaseSetup();
    }

    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testUpdateApplicationDetails()
    {
        intendToModifyData();

        log("Running: testUpdateApplicationDetails");

        gotoViewApplication("demo");

        assertTextInElement("application-name", "demo");

        setWorkingForm("applicationDetails");

        setTextField("name", "demo-updated");
        setTextField("applicationDescription", "Crowd Demo Application Updated");
        uncheckCheckbox("active");

        submit();

        assertTextInElement("application-name", "demo");

        assertTextFieldEquals("name", "demo-updated");
        assertTextFieldEquals("applicationDescription", "Crowd Demo Application Updated");
        assertCheckboxNotSelected("active");
    }

    public void testUpdateDetailsForPermanentApplication()
    {
        intendToModifyData();

        log("Running: testUpdateDetailsForPermanentApplication");

        gotoViewApplication("crowd");

        assertTextInElement("application-name", "crowd");

        // Check that the name field is set to disabled
        String disabled = tester.getElementAttributByXPath("//*[@id=\"name\"]", "disabled");

        assertEquals("disabled", disabled);
    }

    public void testUpdateApplicationWithMismatchedPassword()
    {
        log("Running: testUpdateApplicationWithMismatchedPassword");

        gotoViewApplication("demo");

        assertTextInElement("application-name", "demo");

        setWorkingForm("applicationDetails");

        setTextField("password", "value1");
        setTextField("passwordConfirm", "value2");

        submit();

        assertTextInElement("application-name", "demo");

        assertKeyPresent("invalid.passwordmismatch");
    }

    public void testUpdateApplicationWithExistingApplicationName()
    {
        log("Running: testUpdateApplicationWithExistingApplicationName");

        gotoViewApplication("demo");

        assertTextInElement("application-name", "demo");

        setWorkingForm("applicationDetails");

        setTextField("name", "crowd");

        submit();

        assertTextInElement("application-name", "demo");

        assertKeyPresent("invalid.namealreadyexist");
    }

    public void testUpdateApplicationWithNoName()
    {
        log("Running: testUpdateApplicationWithNoName");

        gotoViewApplication("demo");

        assertTextInElement("application-name", "demo");

        setWorkingForm("applicationDetails");

        setTextField("name", "");

        submit();

        assertTextInElement("application-name", "demo");

        assertKeyPresent("application.name.invalid");
    }

    public void testAddSecondDirectoryToApplication()
    {
        intendToModifyData();

        log("Running: testAddSecondDirectoryToApplication");

        restoreCrowdFromXML("viewapplication_addseconddirectory.xml");

        gotoViewApplication("demo");

        assertTextInElement("application-name", "demo");

        clickLink("application-directories");

        clickButton("add-directory");

        assertTextInTable("directoriesTable", "Second Internal Directory");
    }

    public void testAddRemoteAddressToApplication()
    {
        intendToModifyData();

        log("Running: testAddRemoteAddressToApplication");

        gotoViewApplication("demo");

        assertTextInElement("application-name", "demo");

        clickLink("application-remoteaddress");

        setTextField("address", "192.168.0.0");

        clickButton("add-address");

        assertTextInTable("addressesTable", "192.168.0.0");
    }

    public void testSearchUsersInApplication()
    {
        gotoViewApplication("demo");

        assertTextInElement("application-name", "demo");

        clickLink("application-users");

        setTextField("search", "min");

        submit();

        assertMatchInTable("user-details", "admin");
    }

    public void testUpdateDirectoryOrdering()
    {
        intendToModifyData();

        log("Running: testUpdateDirectoryOrdering");

        restoreCrowdFromXML("directoryordering.xml");

        gotoViewApplication("demo");
        assertTextInElement("application-name", "demo");

        clickLink("application-directories");

        // assert current order
        assertLinkPresent("down-16023553");
        assertLinkPresent("up-16023554");
        assertLinkPresent("down-16023554");
        assertLinkPresent("up-16023555");

        // click down on first
        clickLink("down-16023553");

        // click up on last
        clickLink("up-16023555");

        // assert new order
        assertLinkPresent("down-16023554");
        assertLinkPresent("up-16023555");
        assertLinkPresent("down-16023555");
        assertLinkPresent("up-16023553");
    }

    public void testUpdateApplicationOptions()
    {
        intendToModifyData();

        gotoViewApplication("demo");

        assertTextInElement("application-name", "demo");

        clickLink("application-options");

        checkCheckbox("lowerCaseOutput");
        checkCheckbox("aliasingEnabled");

        submit();

        assertCheckboxSelected("lowerCaseOutput");
        assertCheckboxSelected("aliasingEnabled");

        uncheckCheckbox("lowerCaseOutput");
        uncheckCheckbox("aliasingEnabled");

        submit();

        assertCheckboxNotSelected("lowerCaseOutput");
        assertCheckboxNotSelected("aliasingEnabled");
    }

    public void testUpdateApplicationDirectoryPermissions()
    {
        intendToModifyData();

        log("Running: testUpdateApplicationDirectoryPermissions");

        restoreCrowdFromXML("directoryordering.xml");

        gotoViewApplication("demo");

        assertTextInElement("application-name", "demo");

        clickLink("application-permissions");

        selectOptionByValue("directory-select", "16023553");

        assertCheckboxSelected("CREATE_GROUP");
        assertCheckboxSelected("CREATE_USER");
        assertCheckboxNotPresent("CREATE_ROLE");
        assertCheckboxSelected("UPDATE_GROUP");
        assertCheckboxSelected("UPDATE_USER");
        assertCheckboxNotPresent("UPDATE_ROLE");
        assertCheckboxSelected("DELETE_GROUP");
        assertCheckboxSelected("DELETE_USER");
        assertCheckboxNotPresent("DELETE_ROLE");

        uncheckCheckbox("CREATE_GROUP");
        uncheckCheckbox("CREATE_USER");
        uncheckCheckbox("UPDATE_GROUP");
        uncheckCheckbox("UPDATE_USER");
        uncheckCheckbox("DELETE_GROUP");
        uncheckCheckbox("DELETE_USER");

        submit();

        assertCheckboxNotSelected("CREATE_GROUP");
        assertCheckboxNotSelected("CREATE_USER");
        assertCheckboxNotPresent("CREATE_ROLE");
        assertCheckboxNotSelected("UPDATE_GROUP");
        assertCheckboxNotSelected("UPDATE_USER");
        assertCheckboxNotPresent("UPDATE_ROLE");
        assertCheckboxNotSelected("DELETE_GROUP");
        assertCheckboxNotSelected("DELETE_USER");
        assertCheckboxNotPresent("DELETE_ROLE");

        checkCheckbox("CREATE_GROUP");
        checkCheckbox("CREATE_USER");
        checkCheckbox("UPDATE_GROUP");
        checkCheckbox("UPDATE_USER");
        checkCheckbox("DELETE_GROUP");
        checkCheckbox("DELETE_USER");

        submit();

        assertCheckboxSelected("CREATE_GROUP");
        assertCheckboxSelected("CREATE_USER");
        assertCheckboxNotPresent("CREATE_ROLE");
        assertCheckboxSelected("UPDATE_GROUP");
        assertCheckboxSelected("UPDATE_USER");
        assertCheckboxNotPresent("UPDATE_ROLE");
        assertCheckboxSelected("DELETE_GROUP");
        assertCheckboxSelected("DELETE_USER");
        assertCheckboxNotPresent("DELETE_ROLE");
    }

    public void testRemoveLastDirectoryFromCrowdConsole()
    {
        intendToModifyData();

        log("Running: testRemoveLastDirectoryFromCrowdConsole");


        gotoViewApplication("crowd");

        assertTextInElement("application-name", "crowd");

        clickLink("application-directories");

        clickLinkWithExactText("Remove");

        // Check removal is prevented
        assertKeyPresent("preventlockout.unassigndirectory.label", Arrays.asList("Test Internal Directory"));
        assertTextInTable("directoriesTable", "Test Internal Directory");
    }

    public void testRemoveLastDirectoryFromDemoApp()
    {
        intendToModifyData();

        log("Running: testRemoveLastDirectoryFromDemoApp");


        gotoViewApplication("demo");

        assertTextInElement("application-name", "demo");

        clickLink("application-directories");

        clickLinkWithExactText("Remove");

        // Check removal is successful
        assertKeyPresent("updatesuccessful.label");
        assertTextNotInTable("directoriesTable", "Test Internal Directory");
    }
}
