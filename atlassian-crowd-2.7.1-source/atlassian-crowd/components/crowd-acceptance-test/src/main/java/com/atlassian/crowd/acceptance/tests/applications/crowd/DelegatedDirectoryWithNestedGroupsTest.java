package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.io.InputStream;
import java.util.List;

import com.atlassian.crowd.directory.ApacheDS15;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import org.hamcrest.collection.IsEmptyIterable;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;

import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;

/**
 * Extra tests on top of {@link DelegatedDirectoryTest} to cover LDAP
 * directories with nested groups.
 */
public class DelegatedDirectoryWithNestedGroupsTest extends CrowdAcceptanceTestCase
{
    private static final String DELEGATED_DIRECTORY_NAME = "Test Delegated Directory";

    // Duplicated logic from EscapedDnTest.
    private static boolean ldifSetup = false;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("basesetup.xml");

        if (!ldifSetup)
        {
            InputStream in = getClass().getResourceAsStream("DelegatedDirectoryWithNestedGroupsTest-entries.ldif");

            assertNotNull(in);
            new LdifLoaderForTesting(getBaseUrl()).setLdif(in);
            ldifSetup = true;
        }
    }

    void createDelegatingDirectory(boolean synchroniseGroupMemberships, boolean useNestedGroups)
    {
        gotoCreateDirectory();

        clickButton("create-delegating");

        assertKeyPresent("directory.delegated.create.title");

        setWorkingForm("directorydelegated");

        // Details Tab
        setTextField("name", DELEGATED_DIRECTORY_NAME);

        // Connector Tab
        clickLinkWithExactText("Connector");
        selectOption("connector", ApacheDS15.getStaticDirectoryType());
        setTextField("URL", "ldap://localhost:12389/");
        setTextField("baseDN", "dc=example,dc=com");
        setTextField("userDN", "uid=admin,ou=system");
        setTextField("ldapPassword", "secret");
        setTextField("readTimeoutInSec", "42000");

        assertCheckboxNotSelected("useNestedGroups");

        if (synchroniseGroupMemberships)
        {
            checkCheckbox("importGroups");
        }

        if (useNestedGroups)
        {
            checkCheckbox("useNestedGroups");
        }

        submit();

        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(DELEGATED_DIRECTORY_NAME));
    }

    void addDirectoryToCrowdApplication()
    {
        gotoBrowseApplications();

        setScriptingEnabled(true);

        try
        {
            clickLinkWithExactText("crowd");
            clickLink("application-directories");

            setWorkingForm("directoriesForm");
            selectOption("unsubscribedDirectoriesID", DELEGATED_DIRECTORY_NAME);
            clickButton("add-directory");
        }
        finally
        {
            setScriptingEnabled(false);
        }

        selectOption("directory2-allowAll", "True");
        submit();
    }

    void assertNoUsersPresentInTestDirectory()
    {
        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", DELEGATED_DIRECTORY_NAME);
        submit();

        assertThat(getUserDetailsTableContents(), IsEmptyIterable.<User>emptyIterable());
    }

    void attemptAuthentication(String username, String password)
    {
        gotoViewApplication("crowd");
        clickLink("application-authtest");
        setWorkingForm("app-auth-test-form");
        setTextField("testUsername", username);
        setTextField("testPassword", password);
        submit();

        assertTextPresent("Successful verification");
    }

    List<Group> getGroupDetailsTableContents()
    {
        return scrapeTable("group-details",
                ImmutableList.of("Name", "Active", "Action"),
                new Function<List<String>, Group>()
                {
                    @Override
                    public Group apply(List<String> input)
                    {
                        String groupName = input.get(0);
                        String active = input.get(1);

                        GroupTemplate group = new GroupTemplate(groupName, -1);
                        group.setActive(Boolean.parseBoolean(active));

                        return group;
                    }
                });
    }

    List<String> getGroupMemberUserNames()
    {
        return scrapeTable("view-group-users",
                ImmutableList.of("Username", "Email Address", "Active"),
                new Function<List<String>, String>()
                {
                    @Override
                    public String apply(List<String> input)
                    {
                        return input.get(0);
                    }
                });
    }

    List<String> getGroupMemberGroupNames()
    {
        return scrapeTable("view-group-groups",
                ImmutableList.of("Group Name", "Description", "Active"),
                new Function<List<String>, String>()
                {
                    @Override
                    public String apply(List<String> input)
                    {
                        return input.get(0);
                    }
                });
    }

    void browseTestDirectoryGroups()
    {
        gotoBrowseGroups();
        selectOption("directoryID", DELEGATED_DIRECTORY_NAME);
        submit();
    }

    public void testDelegatedDirectoryAuthenticateCreatesUserInUnderlyingInternalDirectory()
    {
        intendToModifyData();

        createDelegatingDirectory(false, false);

        assertNoUsersPresentInTestDirectory();

        addDirectoryToCrowdApplication();

        attemptAuthentication("user1", "password");

        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", DELEGATED_DIRECTORY_NAME);
        submit();

        assertThat(getUserDetailsTableContents(),
                IsIterableContainingInAnyOrder.containsInAnyOrder(userWithDetails("user1", "User 1")));

        browseTestDirectoryGroups();
        assertThat(getGroupDetailsTableContents(), IsEmptyIterable.<Group>emptyIterable());
    }

    void gotoViewGroupDirectMembers(String groupName, String directoryName)
    {
        gotoViewGroup(groupName, directoryName);
        clickLink("view-group-users");
    }

    public void testDelegatedDirectoryAuthenticateAddsUsersToGroupsInUnderlyingInternalDirectory()
    {
        intendToModifyData();

        createDelegatingDirectory(true, false);

        assertNoUsersPresentInTestDirectory();

        addDirectoryToCrowdApplication();

        /* First user */
        attemptAuthentication("user1", "password");

        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", DELEGATED_DIRECTORY_NAME);
        submit();

        assertThat(getUserDetailsTableContents(),
                IsIterableContainingInAnyOrder.containsInAnyOrder(userWithDetails("user1", "User 1")));

        browseTestDirectoryGroups();
        assertThat(namesOf(getGroupDetailsTableContents()), containsInAnyOrder("users-dept"));

        gotoViewGroupDirectMembers("users-dept", DELEGATED_DIRECTORY_NAME);
        assertTableNotPresent("view-group-groups");
        assertThat(getGroupMemberUserNames(), containsInAnyOrder("user1"));

        /* Second user */
        attemptAuthentication("user2", "password");

        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", DELEGATED_DIRECTORY_NAME);
        submit();

        assertThat(getUserDetailsTableContents(),
                IsIterableContainingInAnyOrder.containsInAnyOrder(userWithDetails("user1", "User 1"), userWithDetails("user2", "User 2")));

        /* New group is created */
        browseTestDirectoryGroups();
        assertThat(namesOf(getGroupDetailsTableContents()), containsInAnyOrder("users-dept", "users-all"));

        /* First group unaffected */
        gotoViewGroupDirectMembers("users-dept", DELEGATED_DIRECTORY_NAME);
        assertTableNotPresent("view-group-groups");
        assertThat(getGroupMemberUserNames(), containsInAnyOrder("user1"));

        /* New group contains new user */
        gotoViewGroupDirectMembers("users-all", DELEGATED_DIRECTORY_NAME);
        assertTableNotPresent("view-group-groups");
        assertThat(getGroupMemberUserNames(), containsInAnyOrder("user2"));
    }

    // CWD-2719
    public void testDelegatedDirectoryAuthenticateAddsUsersToNestedGroupsInUnderlyingInternalDirectory()
    {
        intendToModifyData();

        createDelegatingDirectory(true, true);

        assertNoUsersPresentInTestDirectory();

        addDirectoryToCrowdApplication();

        /* First user */
        attemptAuthentication("user1", "password");

        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", DELEGATED_DIRECTORY_NAME);
        submit();

        assertThat(getUserDetailsTableContents(),
                IsIterableContainingInAnyOrder.containsInAnyOrder(userWithDetails("user1", "User 1")));

        browseTestDirectoryGroups();
        assertThat(namesOf(getGroupDetailsTableContents()), containsInAnyOrder("users-dept", "users-all"));

        gotoViewGroupDirectMembers("users-dept", DELEGATED_DIRECTORY_NAME);
        assertTableNotPresent("view-group-groups");
        assertThat(getGroupMemberUserNames(), containsInAnyOrder("user1"));

        gotoViewGroupDirectMembers("users-all", DELEGATED_DIRECTORY_NAME);
        assertThat(getGroupMemberGroupNames(), containsInAnyOrder("users-dept"));
        assertTableNotPresent("view-group-users");

        /* Second user */
        attemptAuthentication("user2", "password");

        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", DELEGATED_DIRECTORY_NAME);
        submit();

        assertThat(getUserDetailsTableContents(),
                IsIterableContainingInAnyOrder.containsInAnyOrder(userWithDetails("user1", "User 1"), userWithDetails("user2", "User 2")));

        /* No new group is created */
        browseTestDirectoryGroups();
        assertThat(namesOf(getGroupDetailsTableContents()), containsInAnyOrder("users-dept", "users-all"));

        /* First group unaffected */
        gotoViewGroupDirectMembers("users-dept", DELEGATED_DIRECTORY_NAME);
        assertTableNotPresent("view-group-groups");
        assertThat(getGroupMemberUserNames(), containsInAnyOrder("user1"));

        /* All users group contains new user */
        gotoViewGroupDirectMembers("users-all", DELEGATED_DIRECTORY_NAME);
        assertThat(getGroupMemberGroupNames(), containsInAnyOrder("users-dept"));
        assertThat(getGroupMemberUserNames(), containsInAnyOrder("user2"));
    }

    // CWD-3136
    public void testDelegatedDirectoryAuthenticateAddUsersToNestedGroupsWhenNestedGroupsAreEnabledAfterCreation()
    {
        intendToModifyData();

        // initially create the directory without support for nested groups
        createDelegatingDirectory(true, false);

        // then enable support for nested groups
        clickLinkWithExactText("Connector");
        assertCheckboxNotSelected("useNestedGroups");
        checkCheckbox("useNestedGroups");
        submit();
        assertCheckboxSelected("useNestedGroups");

        assertNoUsersPresentInTestDirectory();

        addDirectoryToCrowdApplication();

        attemptAuthentication("user1", "password");

        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", DELEGATED_DIRECTORY_NAME);
        submit();

        assertThat(getUserDetailsTableContents(),
                IsIterableContainingInAnyOrder.containsInAnyOrder(userWithDetails("user1", "User 1")));

        browseTestDirectoryGroups();
        assertThat(namesOf(getGroupDetailsTableContents()), containsInAnyOrder("users-dept", "users-all"));

        gotoViewGroupDirectMembers("users-dept", DELEGATED_DIRECTORY_NAME);
        assertTableNotPresent("view-group-groups");
        assertThat(getGroupMemberUserNames(), containsInAnyOrder("user1"));

        gotoViewGroupDirectMembers("users-all", DELEGATED_DIRECTORY_NAME);
        assertThat(getGroupMemberGroupNames(), containsInAnyOrder("users-dept"));
        assertTableNotPresent("view-group-users");
    }
}
