package com.atlassian.crowd.acceptance.tests.applications.crowd.plugin;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import junit.framework.AssertionFailedError;

public class ApplicationPluginPermissioningTest extends CrowdAcceptanceTestCase
{
    private String DUAL_USER = "admin";
    private String CROWD_USER = "user";
    private String GOOGLE_USER = "google";

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        _loginAdminUser();
        restoreCrowdFromXML("googletest.xml");

        // we don't want to test google auth, just permissioning
        ensureNoKeysPresent();

        _logout();
    }

    protected void ensureNoKeysPresent()
    {
        gotoSAMLConfig();

        try
        {
            assertButtonPresent("keydelButton"); // this won't log the page if the button doesn't exist
            clickButton("keydelButton");
        }
        catch (AssertionFailedError e)
        {
            // delete button doesn't exist
        }

        assertButtonNotPresent("keydelButton");
        assertKeyPresent("saml.key.none");
    }

    protected void gotoGoogleApplication()
    {
        gotoPage("/console/plugin/secure/saml/samlauth.action");
    }

    protected void gotoCrowdApplication()
    {
        gotoPage("/console");
    }

    protected void assertLoggedInToGoogle()
    {
        gotoGoogleApplication();
        assertTextPresent("Invalid SAML authentication request");
    }

    protected void assertLoggedInToCrowd()
    {
        gotoCrowdApplication();
        assertKeyPresent("menu.profile.label");
    }

    protected void assertNotLoggedInToCrowd()
    {
        gotoCrowdApplication();
        assertAtLoginScreen();
    }

    protected void assertNotLoggedInToGoogle()
    {
        gotoGoogleApplication();
        assertAtLoginScreen();
    }

    protected void assertAtLoginScreen()
    {
        assertKeyPresent("login.title");
    }

    protected void login(String user)
    {
        setTextField("j_username", user);
        setTextField("j_password", user);
        submit();
    }

    public void testCrowdToPluginSSO()
    {
        log("Running testCrowdToPluginSSO");

        gotoCrowdApplication();
        login(DUAL_USER);

        assertLoggedInToCrowd();
        assertLoggedInToGoogle();
    }

    public void testPluginToCrowdSSO()
    {
        log("Running testPluginToCrowdSSO");

        gotoGoogleApplication();
        login(DUAL_USER);

        assertLoggedInToGoogle();
        assertLoggedInToCrowd();
    }

    public void testCrowdToPluginSSOFail()
    {
        log("Running testCrowdToPluginSSOFail");

        gotoCrowdApplication();
        login(CROWD_USER);

        assertLoggedInToCrowd();
        assertNotLoggedInToGoogle();
    }

    public void testPluginToCrowdSSOFail()
    {
        log("Running testPluginToCrowdSSOFail");

        gotoGoogleApplication();
        login(GOOGLE_USER);

        assertLoggedInToGoogle();
        assertNotLoggedInToCrowd();
    }

    public void testPluginToCrowdDoesNotAuthorisedBasedOnPreviouslyAttemptedPage()
    {
        gotoGoogleApplication();
        login(GOOGLE_USER);

        assertLoggedInToGoogle();
        gotoPage("/console/user/viewprofile.action");
        gotoGoogleApplication();
        gotoPage("/console/user/viewprofile.action");

        assertKeyNotPresent("menu.profile.label");
        assertAtLoginScreen();
    }

    public void testPluginToCrowdAndBackStillShowsExpectedPluginPage()
    {
        gotoGoogleApplication();
        login(GOOGLE_USER);

        assertLoggedInToGoogle();
        gotoPage("/console/user/viewprofile.action");
        assertLoggedInToGoogle();
    }

    public void testPluginDirectFail()
    {
        log("Running testPluginDirectFail");

        gotoGoogleApplication();
        login(CROWD_USER);

        assertAtLoginScreen();
        assertNotLoggedInToGoogle();
        assertNotLoggedInToCrowd();
    }

    public void testCrowdDirectFail()
    {
        log("Running testCrowdDirectFail");

        gotoCrowdApplication();
        login(GOOGLE_USER);

        assertAtLoginScreen();
        assertNotLoggedInToGoogle();
        assertNotLoggedInToCrowd();
    }
}
