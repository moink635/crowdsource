package com.atlassian.crowd.acceptance.tests.applications.crowd;

/**
 * Test the removing of an application
 */
public class RemoveApplicationTest extends CrowdAcceptanceTestCase
{
    private static final String APPLICATION_NAME = "test-application";

    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("removeapplication.xml");
    }

    public void testRemoveApplicationFromConsole()
    {
        intendToModifyData();

        log("Running: testRemoveApplicationFromConsole");

        gotoBrowseApplications();

        clickLinkWithExactText(APPLICATION_NAME);

        assertTextPresent(APPLICATION_NAME);

        clickLink("remove-application");

        assertKeyPresent("application.remove.text", APPLICATION_NAME);

        // Submit the form
        submit();

        assertKeyPresent("updatesuccessful.label");

        assertTextNotInTable("application-table", APPLICATION_NAME);
    }

    public void testRemoveApplicationFromConsole_Cancel()
    {
        log("Running: testRemoveApplicationFromConsole_Cancel");

        gotoBrowseApplications();

        clickLinkWithExactText(APPLICATION_NAME);

        assertTextPresent(APPLICATION_NAME);

        clickLink("remove-application");

        assertKeyPresent("application.remove.text", APPLICATION_NAME);

        setScriptingEnabled(true);

        clickButtonWithText("Cancel");

        setScriptingEnabled(false);

        // Application details should be displayed
        assertTextInElement("name", APPLICATION_NAME);
    }
}
