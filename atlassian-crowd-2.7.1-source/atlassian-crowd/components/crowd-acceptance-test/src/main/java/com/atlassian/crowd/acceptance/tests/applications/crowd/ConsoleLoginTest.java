package com.atlassian.crowd.acceptance.tests.applications.crowd;

import org.apache.commons.httpclient.HttpStatus;

/**
 * Authentication tests for the Crowd Console.
 */
public class ConsoleLoginTest extends CrowdAcceptanceTestCase
{
    private static final String MORE_CROWD_ADMINISTRATORS_GROUP = "more-crowd-administrators";
    private static final String APP_CROWD = "crowd";

    private String user = "user";
    private String anotherAdmin = "anotherAdmin";
    private String noauth = "demo";
    private String nonexistent = "nonexistent";
    private String inactive = "inactive";
    private String password = "password";
    private String bogusPassword = "bogus";

    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("authenticationtest.xml");

        // force a logoff
        gotoPage("/console/logoff.action");
    }

    public void testConsoleAdminLoginSuccessfull()
    {
        log("Running testConsoleAdminLoginSuccessfull");

        // now try and login
        gotoPage("/console/login.action");

        setTextField("j_username", ADMIN_USER);
        setTextField("j_password", ADMIN_PW);
        submit();

        // assert that we are on the console page
        assertKeyPresent("console.welcome");
        assertTextPresent("Log Out");
    }

    /**
     * This test specifically asserts the non-documented behavior of the Crowd console regarding
     * who is granted administrative. Members of groups that are mapped to the Crowd application
     * receive admin rights. Group names are not significant.
     * Verify that access rights are immediately revoked when group mappings are changed.
     */
    public void testConsoleAdminAccessRevokedByRemovingGroupMapping()
    {
        log("Running testConsoleAdminAccessRevokedByRemovingGroupMapping");
        intendToModifyData();

        // first attempt to login as another admin should be successful
        gotoPage("/console/login.action");
        setTextField("j_username", anotherAdmin);
        setTextField("j_password", password);
        submit();

        // assert that we are on the console page
        assertKeyPresent("console.welcome");
        assertTextPresent("Log Out");

        // logout and login again as the main admin
        gotoPage("/console/logoff.action");
        gotoPage("/console/login.action");
        setTextField("j_username", ADMIN_USER);
        setTextField("j_password", ADMIN_PW);
        submit();

        // assert that we are on the console page
        assertKeyPresent("console.welcome");
        assertTextPresent("Log Out");

        // remove more-crowd-administrators from the list of privileged groups
        gotoViewApplication(APP_CROWD);
        assertTextInElement("application-name", APP_CROWD);
        clickLink("application-groups");
        clickLink("remove-" + MORE_CROWD_ADMINISTRATORS_GROUP);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable", MORE_CROWD_ADMINISTRATORS_GROUP);

        // logout and try to login again as the another admin
        gotoPage("/console/logoff.action");
        gotoPage("/console/login.action");
        setTextField("j_username", anotherAdmin);
        setTextField("j_password", password);
        submit();

        // assert that we are on the profile page (we no longer have admin rights)
        assertTextPresent("Log Out");
        assertKeyPresent("menu.user.console.editprofile.label");
        assertTextFieldEquals("firstname", "Another");
        assertTextFieldEquals("lastname", "Administrator");
    }

    public void testConsoleUserLoginSuccessful()
    {
        log("Running testConsoleUserLoginSuccessful");

        // now try and login
        gotoPage("/console/login.action");

        setTextField("j_username", user);
        setTextField("j_password", password);
        submit();

        // assert that we are on the profile page
        assertTextPresent("Log Out");
        assertKeyPresent("menu.user.console.editprofile.label");
        assertTextFieldEquals("firstname", "Test");
        assertTextFieldEquals("lastname", "User");
        assertTextFieldEquals("email", "user@example.com");
    }

    public void testConsoleLoginWithBadCredentials()
    {
        log("Running testConsoleLoginWithBadCredentials");

        // now try and login
        gotoPage("/console/login.action");

        setTextField("j_username", ADMIN_USER);
        setTextField("j_password", bogusPassword);
        submit();

        // assert that we got an invalid login error
        assertKeyPresent("login.failed.label");
    }

    public void testConsoleLoginWithNonExistentUser()
    {
        log("Running testConsoleLoginWithNonExistentUser");

        // now try and login
        gotoPage("/console/login.action");

        setTextField("j_username", nonexistent);
        setTextField("j_password", password);
        submit();

        // assert that we got an invalid login error
        assertKeyPresent("login.failed.label");
        assertTextNotPresent("Log Out");
    }

    public void testConsoleLoginWithNoAuthenticationAccess()
    {
        log("Running testConsoleLoginWithNoAuthenticationAccess");

        // now try and login
        gotoPage("/console/login.action");

        setTextField("j_username", noauth);
        setTextField("j_password", password);
        submit();

        // assert that we got an invalid login error
        assertKeyPresent("login.failed.label");
        assertTextNotPresent("Log Out");
    }

    public void testConsoleLoginWithInactiveUser()
    {
        log("Running testConsoleLoginWithInactiveUser");

        // now try and login
        gotoPage("/console/login.action");

        setTextField("j_username", inactive);
        setTextField("j_password", password);
        submit();

        // assert that we got an invalid login error
        assertKeyPresent("login.failed.label");
        assertTextNotPresent("Log Out");
    }

    public void testConsoleLoginWhenAlreadyLoggedIn()
    {
        log("Running testConsoleLoginWhenAlreadyLoggedIn");

        // now try and login
        gotoPage("/console/login.action");

        setTextField("j_username", user);
        setTextField("j_password", password);
        submit();

        // assert that we are on the profile page
        assertTextPresent("Log Out");
        assertKeyPresent("menu.user.console.editprofile.label");
        assertTextFieldEquals("firstname", "Test");
        assertTextFieldEquals("lastname", "User");
        assertTextFieldEquals("email", "user@example.com");

        // now try go to login
        gotoPage("/console/login.action");

        // assert that we are on the profile page
        assertTextPresent("Log Out");
        assertKeyPresent("menu.user.console.editprofile.label");
        assertTextFieldEquals("firstname", "Test");
        assertTextFieldEquals("lastname", "User");
        assertTextFieldEquals("email", "user@example.com");
    }

    public void testConsoleLogoutSuccessful()
    {
        log("Running testConsoleLogoutSuccessful");

        // now try and login
        gotoPage("/console/login.action");

        setTextField("j_username", ADMIN_USER);
        setTextField("j_password", ADMIN_PW);
        submit();

        // assert that we are on the console page
        assertKeyPresent("console.welcome");
        assertTextPresent("Log Out");

        // logout
        gotoPage("/console/logoff.action");

        assertKeyPresent("login.title");
        assertTextNotPresent("Log Out");
    }

    public void testConsoleLogoutWhenNotLoggedIn()
    {
        log("Running testConsoleLogoutWhenNotLoggedIn");

        assertKeyPresent("login.title");
        assertTextNotPresent("Log Out");

        // force another logoff
        gotoPage("/console/logoff.action");

        assertKeyPresent("login.title");
        assertTextNotPresent("Log Out");
    }

    public void testConsoleLoginInterceptor()
    {
        log("Running testConsoleLoginInterceptor");

        assertKeyPresent("login.title");
        assertTextNotPresent("Log Out");

        // invoke interceptor
        gotoPage("/console/secure/admin/systeminfo.action");

        assertKeyPresent("login.title");
        assertTextNotPresent("Log Out");

        // log in
        setTextField("j_username", ADMIN_USER);
        setTextField("j_password", ADMIN_PW);
        submit();

        // assert at saved url
        assertKeyPresent("systeminfo.title");
    }

    public void testConsoleAccessDenied()
    {
        log("Running testConsoleAccessDenied");

        assertKeyPresent("login.title");
        assertTextNotPresent("Log Out");

        // invoke interceptor
        gotoPage("/console/secure/admin/systeminfo.action");

        assertKeyPresent("login.title");
        assertTextNotPresent("Log Out");

        // log in
        setTextField("j_username", user);
        setTextField("j_password", password);
        try
        {
            tester.setIgnoreFailingStatusCodes(true);
            submit();
        }
        finally
        {
            tester.setIgnoreFailingStatusCodes(false);
        }
        tester.assertResponseCode(HttpStatus.SC_FORBIDDEN);

        // assert at saved url (no access for non-admins)
        assertKeyPresent("accessdenied.text");
    }
}
