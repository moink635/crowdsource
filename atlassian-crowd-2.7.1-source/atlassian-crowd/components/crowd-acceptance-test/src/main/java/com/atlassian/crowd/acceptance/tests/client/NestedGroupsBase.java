package com.atlassian.crowd.acceptance.tests.client;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.tests.directory.BaseTest;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.service.cache.CachingManagerFactory;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.service.soap.client.SecurityServerClientImpl;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl;

import java.util.Collection;
import java.util.Properties;


public abstract class NestedGroupsBase extends CrowdAcceptanceTestCase
{
    protected SecurityServerClient securityServerClient;
    LDAPLoader loader;

    private static final String USER_NAME = "myUser";
    private static final String GROUP_NAME = "myGroup";
    private static final String SUB_GROUP_NAME = "mySubGroup";
    private static final String SUB_USER_NAME = "mySubUser";
    private static final String ANOTHER_USER_NAME = "myOtherUser";


    /**
     * Allows us to add users to our ApacheDS instance.
     */
    class LDAPLoader extends BaseTest
    {
        LDAPLoader()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS102ConfigFileName());
        }

        @Override
        protected void configureDirectory(final Properties directorySettings)
        {
            super.configureDirectory(directorySettings);
            directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.FALSE.toString());
        }

        @Override
        protected void loadTestData() throws Exception
        {
            Long directoryId = directory.getId();
            DirectoryTestHelper.addUser(USER_NAME, directoryId, "secret", getRemoteDirectory());
            DirectoryTestHelper.addUser(SUB_USER_NAME, directoryId, "secret", getRemoteDirectory());
            DirectoryTestHelper.addUser(ANOTHER_USER_NAME, directoryId, "secret", getRemoteDirectory());

            // members: USER_NAME, SUB_GROUP_NAME
            DirectoryTestHelper.addGroup(GROUP_NAME, directoryId, getRemoteDirectory());

            // members: SUB_USER_NAME
            DirectoryTestHelper.addGroup(SUB_GROUP_NAME, directoryId, getRemoteDirectory());

            getRemoteDirectory().addUserToGroup(USER_NAME, GROUP_NAME);
            getRemoteDirectory().addGroupToGroup(SUB_GROUP_NAME, GROUP_NAME);

            getRemoteDirectory().addUserToGroup(SUB_USER_NAME, SUB_GROUP_NAME);
        }

        @Override
        protected void removeTestData() throws DirectoryInstantiationException
        {
            DirectoryTestHelper.silentlyRemoveUser(USER_NAME, getRemoteDirectory());
            DirectoryTestHelper.silentlyRemoveUser(SUB_USER_NAME, getRemoteDirectory());
            DirectoryTestHelper.silentlyRemoveUser(ANOTHER_USER_NAME, getRemoteDirectory());

            DirectoryTestHelper.silentlyRemoveGroup(GROUP_NAME, getRemoteDirectory());
            DirectoryTestHelper.silentlyRemoveGroup(SUB_GROUP_NAME, getRemoteDirectory());
        }

        public void removeGroupFromGroup() throws Exception
        {
            getRemoteDirectory().removeUserFromGroup(USER_NAME, GROUP_NAME);
        }

        public void accessibleSetUp() throws Exception
        {
            super.setUp();
        }

        public void accessibleTearDown() throws Exception
        {
            super.tearDown();
        }

    }

    public NestedGroupsBase()
    {
        loader = new LDAPLoader();
    }


    /**
     * Sets the connection details & auths the app.
     *
     * @throws Exception
     */
    protected void setupSecurityServer() throws Exception
    {
        Properties sscProperties = AcceptanceTestHelper.loadProperties("localtest.crowd.properties");
        SoapClientProperties cProperties = SoapClientPropertiesImpl.newInstanceFromProperties(sscProperties);
        securityServerClient = new SecurityServerClientImpl(cProperties);
    }

    /**
     * Overridden to set up the interface object(s).
     */
    protected abstract void setupClientLibrary() throws Exception;


    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("clientlib.xml");
        setupSecurityServer();
        setupClientLibrary();
        loader.accessibleSetUp();
    }

    public void tearDown() throws Exception
    {
        loader.accessibleTearDown();
        CachingManagerFactory.getCache().flush();
        super.tearDown();
    }

    /**
     * In OSUser: Checks that the data returned from com.opensymphony.user.provider.listGroupsContainingUser() and
     * com.opensymphony.user.providerlistUsersInGroup() is consistent.
     * <p/>
     * In atlassian-user: the same, but for getMembers() and getGroupMemberships().
     */
    public void testUsersByGroupMatchesGroupsByUser() throws Exception
    {
//        getAllGroupNames(); // temp

        // GROUP_NAME contains USER_NAME and SUB_USER_NAME
        Collection usersInGroup = getUsersInGroup(GROUP_NAME);
        assertNotNull(usersInGroup);
        assertEquals("There should be two users in GROUP_NAME", 2, usersInGroup.size());
        assertTrue("USER_NAME should be in GROUP_NAME via getUsersInGroup", isUserInList(usersInGroup, USER_NAME));
        assertTrue("SUB_USER_NAME should be in GROUP_NAME via getUsersInGroup", isUserInList(usersInGroup, SUB_USER_NAME));

        // SUB_GROUP_NAME contains only SUB_USER_NAME
        Collection usersInGroup2 = getUsersInGroup(SUB_GROUP_NAME);
        assertNotNull(usersInGroup2);
        assertEquals("There should be one user in SUB_GROUP_NAME", 1, usersInGroup2.size());
        assertTrue("SUB_USER_NAME should be in SUB_GROUP_NAME via getUsersInGroup", isUserInList(usersInGroup, SUB_USER_NAME));

        // SUB_USER_NAME is in SUB_GROUP_NAME directly. We make sure to return true for GROUP_NAME as well, to keep both
        //  calls consistent.
        Collection groupsForUser = getGroupsForUser(SUB_USER_NAME);
        assertNotNull(groupsForUser);
        assertEquals("There should be two groups for SUB_USER_NAME", 2, groupsForUser.size());
        assertTrue("SUB_USER_NAME should be in GROUP_NAME via getGroupsForUser", isGroupInList(groupsForUser, GROUP_NAME));
        assertTrue("SUB_GROUP_NAME should be a group of SUB_USER_NAME", isGroupInList(groupsForUser, SUB_GROUP_NAME));

        // USER_NAME is in GROUP_NAME directly, but *not* in SUB_GROUP_NAME
        Collection groupsForUser2 = getGroupsForUser(USER_NAME);
        assertNotNull(groupsForUser2);
        assertEquals("There should be one group for USER_NAME", 1, groupsForUser2.size());
        assertTrue("GROUP_NAME should be a group of USER_NAME", isGroupInList(groupsForUser2, GROUP_NAME));
    }

    /**
     * Removing SUB_USER_NAME from GROUP_NAME should fail as the user is not a direct member.
     */
    public void testRemoveFromGroup_MemberOfSubGroup() throws Exception
    {
        assertTrue("Sub user should be reported as being member of parent group", isInGroup(SUB_USER_NAME, GROUP_NAME));
        assertFalse("Sub-user removal from parent group should fail", removeUserFromGroup(SUB_USER_NAME, GROUP_NAME));
        assertTrue("Sub user should be reported as being member of parent group", isInGroup(SUB_USER_NAME, GROUP_NAME));
    }

    public void testRemoveFromGroup_MemberOfGroup() throws Exception
    {
        intendToModifyData();

//        getAllGroupNames(); // temp

        assertTrue("USER_NAME should be a member of GROUP_NAME", isInGroup(USER_NAME, GROUP_NAME));
        assertTrue("Removal of USER_NAME from GROUP_NAME should succeed", removeUserFromGroup(USER_NAME, GROUP_NAME));

//        getAllGroupNames(); // temp

        // check all three methods to ensure caches are cleared properly.
        assertFalse("USER_NAME should not be a member of GROUP_NAME", isInGroup(USER_NAME, GROUP_NAME));
        assertFalse("USER_NAME should not be a member of GROUP_NAME using getGroupsForUser call", isGroupInList(getGroupsForUser(USER_NAME), GROUP_NAME));
        assertFalse("USER_NAME should not be in member list of GROUP_NAME", isUserInList(getUsersInGroup(GROUP_NAME), USER_NAME));

        // ensure that the memberships of SUB_USER_NAME are not affected
        assertTrue("SUB_USER_NAME should be in SUB_GROUP_NAME", isInGroup(SUB_USER_NAME, SUB_GROUP_NAME));
        assertTrue("SUB_USER_NAME should be in SUB_GROUP_NAME using getGroupsForUser call", isGroupInList(getGroupsForUser(SUB_USER_NAME), SUB_GROUP_NAME));
        assertTrue("SUB_USER_NAME should be in GROUP_NAME using getGroupsForUser call", isGroupInList(getGroupsForUser(SUB_USER_NAME), GROUP_NAME));
        assertTrue("SUB_USER_NAME should be in SUB_GROUP_NAME using getUsersForGroup call", isUserInList(getUsersInGroup(SUB_GROUP_NAME), SUB_USER_NAME));
        assertTrue("SUB_USER_NAME should be in GROUP_NAME using getUsersForGroup call", isUserInList(getUsersInGroup(GROUP_NAME), SUB_USER_NAME));
    }

    public void testRemoveFromGroup_SubMemberOfSubGroup() throws Exception
    {
        intendToModifyData();

        assertTrue(isInGroup(SUB_USER_NAME, SUB_GROUP_NAME));
        assertTrue(removeUserFromGroup(SUB_USER_NAME, SUB_GROUP_NAME));

        // check all three methods to ensure caches are cleared properly.
        assertFalse(isInGroup(SUB_USER_NAME, SUB_GROUP_NAME));
        assertFalse(isGroupInList(getGroupsForUser(SUB_USER_NAME), SUB_GROUP_NAME));
        assertFalse(isUserInList(getUsersInGroup(SUB_GROUP_NAME), SUB_USER_NAME));
    }

    public void testInGroup() throws Exception
    {
        assertTrue(isInGroup(USER_NAME, GROUP_NAME));
    }

    public void testInGroup_UserInSubGroup() throws Exception
    {
        assertTrue(isInGroup(SUB_USER_NAME, GROUP_NAME));
    }

    public void testInGroup_UserInParentGroup() throws Exception
    {
        assertFalse(isInGroup(USER_NAME, SUB_GROUP_NAME));
    }

    public void testInGroup_UserNotInGroup() throws Exception
    {
        assertFalse(isInGroup(ANOTHER_USER_NAME, GROUP_NAME));
    }


    public abstract Collection getAllGroupNames() throws Exception;

    public abstract Collection getUsersInGroup(String groupName) throws Exception;

    public abstract Collection getGroupsForUser(String userName) throws Exception;

    public abstract boolean isInGroup(String userName, String groupName) throws Exception;

    public abstract boolean removeUserFromGroup(String userName, String groupName) throws Exception;

    /**
     * Checks if a group specified by <code>groupName</code> is in <code>groupList</code>. Not all interfaces return
     * a Collection of Strings, so we may need interface-specific comparison ops. For example, atlassian-user returns
     * a <code>List<DefaultGroup></code>
     *
     * @param groupList A list of groups as returned to the application using the interface
     * @param groupName The name of a group
     * @return
     * @throws Exception
     */
    public abstract boolean isGroupInList(Collection groupList, String groupName) throws Exception;

    public abstract boolean isUserInList(Collection userList, String userName) throws Exception;
}
