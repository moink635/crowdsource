package com.atlassian.crowd.acceptance.tests.applications.crowdid.client;

import com.atlassian.crowd.acceptance.tests.ApplicationAcceptanceTestCase;

public class CrowdIDClientAcceptanceTestCase extends ApplicationAcceptanceTestCase
{
    protected String getResourceBundleName()
    {
        return "com.atlassian.crowd.openid.client.action.BaseAction";
    }

    protected String getApplicationName()
    {
        return "crowdidclient";
    }

    protected String getLocalTestPropertiesFileName()
    {
        return "localtest.properties";
    }
}
