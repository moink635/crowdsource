package com.atlassian.crowd.acceptance.tests.applications.demo;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;

public class DemoOGNLInjectionTest extends DemoAcceptanceTestCase
{
    /**
     * CWD-3385
     */
    public void testOGNLCodeInjection()
    {
        // inject OGNL
        gotoPage("/console/login.action?redirect:${@java.lang.System@exit(0)}");

        // make sure the server is still running
        gotoPage("/console/login.action");
        assertKeyPresent("login.title");
    }

    /**
     * CWD-3291
     *
     * @throws Exception
     */
    public void testOGNLArbitraryRedirect() throws Exception
    {
        HttpMethod method = new GetMethod(getBaseUrl() + "/console/login.action?redirect:http://example.test/");
        method.setFollowRedirects(false);
        HttpClient httpClient = new HttpClient();
        httpClient.executeMethod(method);

        assertEquals("HTTP status code should be a 2xx, not a 3xx", 2, method.getStatusCode() / 100);
        assertNull("Should not have a Location header", method.getResponseHeader("Location"));
    }
}
