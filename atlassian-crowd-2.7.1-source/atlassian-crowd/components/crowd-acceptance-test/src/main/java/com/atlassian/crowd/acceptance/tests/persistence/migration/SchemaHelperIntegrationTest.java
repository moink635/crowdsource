package com.atlassian.crowd.acceptance.tests.persistence.migration;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.directory.ImmutableUser;
import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.dao.token.TokenDAOHibernate;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.GroupDao;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.ImmutableApplication;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.persistence.hibernate.SchemaHelper;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.jdbc.SimpleJdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * This test is HSQLDB-specific because it uses a SQL script with the HSQLDB dialect to
 * restore the database to a known state.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext-schemahelper-config.xml",
                                   "classpath:/applicationContext-CrowdDAO.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class})
@TransactionConfiguration(defaultRollback = true)
public class SchemaHelperIntegrationTest
{

    private static final Logger logger = LoggerFactory.getLogger(SchemaHelperIntegrationTest.class);

    private static final String REFERENCE_DATABASE_SCRIPT = "database-crowd-2.1.sql";

    private static final long EXISTING_APPLICATION_ID = 163841L;
    private static final long EXISTING_DIRECTORY_ID = 2;
    private static final String EXISTING_GROUP_NAME = "badgers";
    private static final String EXISTING_USER_NAME = "penny";

    @Inject private SchemaHelper schemaHelper;
    @Inject private DataSource dataSource;
    @Inject private TokenDAOHibernate tokenDao;
    @Inject private ApplicationDAO applicationDao;
    @Inject private UserDao userDao;
    @Inject private GroupDao groupDao;
    @Inject private MembershipDao membershipDao;

    /**
     * The database needs to be restored and upgraded before a transaction is started, because
     * otherwise Hibernate's SessionFactory will initialise it with the new schema.
     * @throws Exception
     */
    @BeforeTransaction
    public void restoreDatabaseAndUpgradeSchema() throws Exception
    {
        logger.info("Restoring database");
        assertNotNull("Data source must have been injected", dataSource);
        SimpleJdbcTemplate template = new SimpleJdbcTemplate(dataSource);
        ClassPathResource resource = new ClassPathResource(REFERENCE_DATABASE_SCRIPT);
        SimpleJdbcTestUtils.executeSqlScript(template, resource, false);
        logger.info("Database restored");

        logger.info("Updating schema");
        schemaHelper.updateSchemaIfNeeded(true);
        logger.info("Schema updated");
    }

    /**
     * DAOs must be run within a transaction.
     * @throws Exception
     */
    @Test
    @Transactional
    public void testDaosAreFunctionalAfterUpgrade() throws Exception
    {
        _testApplicationDao();
        _testTokenDao();
        _testUserDao();
        _testGroupDao();
        _testMembershipDao();
        // test other DAOs here, if their tables are affected by some schema change
    }

    private void _testApplicationDao() throws Exception
    {
        RemoteAddress remoteAddress = new RemoteAddress("127.0.0.99");
        PasswordCredential passwordCredential = new PasswordCredential("password", true);
        EntityQuery<Application> allApplicationsQuery = QueryBuilder.queryFor(Application.class,
                                                                              EntityDescriptor.application())
            .returningAtMost(EntityQuery.ALL_RESULTS);

        // verify applications exist
        List<Application> applicationsBeforeInsertion = applicationDao.search(allApplicationsQuery);
        assertThat(applicationsBeforeInsertion, hasSize(2));

        // verify directory mappings exist
        assertNotNull("There should exist a directory mapping for existing application",
                      applicationDao.findById(EXISTING_APPLICATION_ID).getDirectoryMapping(1));

        // verify group mappings exist
        assertThat(applicationDao.findById(EXISTING_APPLICATION_ID).getRemoteAddresses(), hasSize(4));

        // add a new application
        Application newApplication =
            ImmutableApplication.builder("new application", ApplicationType.GENERIC_APPLICATION)
                                .setDescription("new description")
                                .setPasswordCredential(passwordCredential)
                                .setAttributes(ImmutableMap.of("name1", "value1"))
                                .setRemoteAddresses(ImmutableSet.of(remoteAddress))
                                .build();
        newApplication = applicationDao.add(newApplication,  passwordCredential);
        List<Application> applicationsAfterInsertion = applicationDao.search(allApplicationsQuery);
        assertContainsExistingPlusNewOne(applicationsAfterInsertion, applicationsBeforeInsertion, newApplication);

        // add a new remote address
        applicationDao.addRemoteAddress(newApplication.getId(), remoteAddress);
        assertThat(applicationDao.findById(newApplication.getId()).getRemoteAddresses(), hasItem(remoteAddress));

        // add a new directory mapping with a group mapping
        applicationDao.addDirectoryMapping(newApplication.getId(),
                                           EXISTING_DIRECTORY_ID, false, OperationType.CREATE_USER);
        applicationDao.addGroupMapping(newApplication.getId(), EXISTING_DIRECTORY_ID, "my group");
        assertThat(applicationDao.findById(newApplication.getId()).getDirectoryMapping(EXISTING_DIRECTORY_ID).getAuthorisedGroups(), hasSize(1));
    }

    private void _testTokenDao() throws Exception
    {
        long tokenRandomNumber = 12982189L;
        EntityQuery<Token> allTokensQuery = QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
            .returningAtMost(EntityQuery.ALL_RESULTS);

        // verify existing tokens (in the future, upgrades may clean the table, but this is not the case yet)
        List<Token> tokensBeforeInsertion = tokenDao.search(allTokensQuery);
        assertThat(tokensBeforeInsertion, hasSize(3));

        // add a new token
        Token newToken = tokenDao.add(new Token.Builder(EXISTING_DIRECTORY_ID, "tokenName", "tokenHash",
                tokenRandomNumber, "randomHash").create());

        List<Token> tokensAfterInsertion = tokenDao.search(allTokensQuery);
        assertContainsExistingPlusNewOne(tokensAfterInsertion, tokensBeforeInsertion, newToken);
    }

    private void _testUserDao() throws Exception
    {
        EntityQuery<User> allUsersQuery = QueryBuilder.queryFor(User.class, EntityDescriptor.user())
            .returningAtMost(EntityQuery.ALL_RESULTS);

        // verify existing users
        List<User> usersBeforeInsertion = userDao.search(EXISTING_DIRECTORY_ID, allUsersQuery);
        assertThat(usersBeforeInsertion, hasSize(4));

        // verify user attributes
        assertThat(userDao.findByNameWithAttributes(EXISTING_DIRECTORY_ID, "eeeep").getKeys(), hasSize(4));

        // add a new user
        PasswordCredential passwordCredential = new PasswordCredential("password", true);
        User newUser = new ImmutableUser(EXISTING_DIRECTORY_ID, "user name", "user 1st", "user last", "display name", "email");
        newUser = userDao.add(newUser, passwordCredential);
        List<User> usersAfterInsertion = userDao.search(EXISTING_DIRECTORY_ID, allUsersQuery);
        assertContainsExistingPlusNewOne(usersAfterInsertion, usersBeforeInsertion, newUser);

        // store attributes
        userDao.storeAttributes(newUser, ImmutableMap.<String,Set<String>>of("name1", ImmutableSet.of("value1")));
        assertThat(userDao.findByNameWithAttributes(EXISTING_DIRECTORY_ID, "user name").getValue("name1"), is("value1"));
    }

    private void _testGroupDao() throws Exception
    {
        EntityQuery<Group> allGroupsQuery = QueryBuilder.queryFor(Group.class, EntityDescriptor.group())
            .returningAtMost(EntityQuery.ALL_RESULTS);

        // verify existing groups
        List<Group> groupsBeforeInsertion = groupDao.search(EXISTING_DIRECTORY_ID, allGroupsQuery);
        assertThat(groupsBeforeInsertion, hasSize(4));

        // add a new group
        Group groupTemplate = new GroupTemplate("group name", EXISTING_DIRECTORY_ID, GroupType.GROUP);
        Group newGroup = groupDao.add(groupTemplate);
        List<Group> groupsAfterInsertion = groupDao.search(EXISTING_DIRECTORY_ID, allGroupsQuery);
        assertContainsExistingPlusNewOne(groupsAfterInsertion, groupsBeforeInsertion, newGroup);
    }

    private void _testMembershipDao() throws Exception
    {
        MembershipQuery<User> allMembershipsQuery =
            QueryBuilder.createMembershipQuery(EntityQuery.MAX_MAX_RESULTS, 0, true,
                                               EntityDescriptor.user(), User.class,
                                               EntityDescriptor.group(), EXISTING_GROUP_NAME);

        // verify existing memberships
        List<User> membershipsBeforeInsertion = membershipDao.search(EXISTING_DIRECTORY_ID, allMembershipsQuery);
        assertThat(membershipsBeforeInsertion, hasSize(2));

        // add a new membership
        membershipDao.addUserToGroup(EXISTING_DIRECTORY_ID, EXISTING_USER_NAME, EXISTING_GROUP_NAME);
        List<User> membershipsAfterInsertion = membershipDao.search(EXISTING_DIRECTORY_ID, allMembershipsQuery);
        User newUser = userDao.findByName(EXISTING_DIRECTORY_ID, EXISTING_USER_NAME);
        assertContainsExistingPlusNewOne(membershipsAfterInsertion, membershipsBeforeInsertion, newUser);
    }

    private static <T> void assertContainsExistingPlusNewOne(List<T> elementsAfterInsertion,
                                                             List<T> elementsBeforeInsertion,
                                                             T insertedElement)
    {
        ImmutableSet<T> expectedElementsAfterInsertion = ImmutableSet.<T>builder()
            .addAll(elementsBeforeInsertion).add(insertedElement).build();
        assertEquals("After insertion, the elements should contain the previous ones plus the new one",
                     expectedElementsAfterInsertion, ImmutableSet.copyOf(elementsAfterInsertion));
    }

}
