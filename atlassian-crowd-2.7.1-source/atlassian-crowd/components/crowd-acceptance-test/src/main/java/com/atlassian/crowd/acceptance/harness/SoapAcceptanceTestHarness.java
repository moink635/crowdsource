package com.atlassian.crowd.acceptance.harness;

import com.atlassian.crowd.acceptance.tests.client.AuthenticationTokenTest;
import com.atlassian.crowd.acceptance.tests.client.SecurityServerClientGroupTest;
import com.atlassian.crowd.acceptance.tests.client.SecurityServerClientPrincipalTest;
import com.atlassian.crowd.acceptance.tests.client.SecurityServerClientTest;
import com.atlassian.crowd.acceptance.tests.client.XFireGzipTest;
import com.atlassian.crowd.acceptance.tests.soap.SoapXmlParsingTest;
import com.atlassian.crowd.acceptance.tests.soap.WsdlTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * SOAP test suite.
 *
 * @since v2.1
 */
@RunWith (Suite.class)
@Suite.SuiteClasses({
    WsdlTest.class,
    AuthenticationTokenTest.class,
    XFireGzipTest.class,
    SecurityServerClientTest.class,
    SecurityServerClientGroupTest.class,
    SecurityServerClientPrincipalTest.class,
    SoapXmlParsingTest.class
})
public class SoapAcceptanceTestHarness
{
}
