package com.atlassian.crowd.acceptance.tests.applications.crowd;

import javax.mail.internet.MimeMessage;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class ResetPrincipalPasswordTest extends CrowdAcceptanceTestCaseWithEmailServer
{
    private static final String EMAIL_TEMPLATE_JAPANESE = "こんにちは";
    private static final String EMAIL_TEMPLATE_GREEK = "αβγδες";
    private static final String USERNAME = "TestUser";
    private static final String USER_FIRSTNAME = "Øring";
    private static final String USER_LASTNAME = "Núñez";
    private static final String DIRECTORY_NAME = "Test Internal Directory";

    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        restoreCrowdFromXML("authenticationtest.xml");

        _loginAdminUser();

        _configureMailServer();
    }

    public void testResetEmailWithNonAsciiBody() throws Exception
    {
        log("Running testResetEmailWithNonAsciiBody");

        // add a user to test with
        gotoAddPrincipal();
        setTextField("email", getEmailAddress());
        setTextField("name", USERNAME);
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", USER_FIRSTNAME);
        setTextField("lastname", USER_LASTNAME);
        selectOption("directoryID", DIRECTORY_NAME);
        submit();

        // set the forgotten password email template to a string with non Ascii characters
        gotoMailTemplate();
        setTextField("forgottenPasswordTemplate", EMAIL_TEMPLATE_JAPANESE + EMAIL_TEMPLATE_GREEK);
        submit();

        // enter the test user's profile and reset the password, which will sent an email
        gotoViewPrincipal(USERNAME, DIRECTORY_NAME);
        clickLink("reset-password-principal");
        submit();

        MimeMessage message = waitForExactlyOneMessage();

        // assert that the only received email's body is the same as the template
        assertEquals("Reset password email contains incorrect content",
                EMAIL_TEMPLATE_JAPANESE + EMAIL_TEMPLATE_GREEK, message.getContent().toString());
    }

    public void testResetEmailWithNonAsciiUsername() throws Exception
    {
        log("Running testResetEmailWithNonAsciiUsername");

        // add a user to test with
        gotoAddPrincipal();
        setTextField("email", getEmailAddress());
        setTextField("name", USERNAME);
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", USER_FIRSTNAME);
        setTextField("lastname", USER_LASTNAME);
        selectOption("directoryID", DIRECTORY_NAME);
        submit();

        // set the forgotten password email template
        gotoMailTemplate();
        setTextField("forgottenPasswordTemplate", "$firstname $lastname");
        submit();

        // enter the test user's profile and reset the password, which will sent an email
        gotoViewPrincipal(USERNAME, DIRECTORY_NAME);
        clickLink("reset-password-principal");
        submit();

        MimeMessage message = waitForExactlyOneMessage();

        // assert that the only received email's body contains the user full name
        assertThat(message.getContent().toString(), containsString(USER_FIRSTNAME + " " + USER_LASTNAME));
    }

    public void testResetEmailForInactiveStatusMessage() throws Exception
    {
        log("Running testResetEmailForInactiveStatusMessage");

        gotoMailTemplate();
        setTextField("forgottenPasswordTemplate", getText("mailtemplate.template.forgotten.password.text"));
        submit();

        _logout();
        clickLink("forgottenlogindetails");
        clickRadioOption("forgottenDetail", "password");
        setTextField("username", "inactive");
        submit();

        MimeMessage message = waitForExactlyOneMessage();

        // assert that the only received email's body contains the inactive user message
        assertTrue("Reset password email does not acknowledge inactive user message",
                   message.getContent().toString().contains(getText("mailtemplate.template.user.inactive")));

    }

    public void testResetEmailForActiveStatusMessage() throws Exception
    {
        log("Running testResetEmailForActiveStatusMessage");

        gotoMailTemplate();
        setTextField("forgottenPasswordTemplate", getText("mailtemplate.template.forgotten.password.text"));
        submit();

        _logout();
        clickLink("forgottenlogindetails");
        clickRadioOption("forgottenDetail", "password");
        setTextField("username", "user");
        submit();

        MimeMessage message = waitForExactlyOneMessage();

        // assert that the only received email's body contains the active user message
        assertTrue("Reset password email does not acknowledge active user message",
                   message.getContent().toString().contains(getText("mailtemplate.template.user.active")));
    }
}
