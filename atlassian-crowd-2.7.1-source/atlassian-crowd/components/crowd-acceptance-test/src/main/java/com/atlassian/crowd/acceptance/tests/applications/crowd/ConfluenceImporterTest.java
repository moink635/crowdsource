package com.atlassian.crowd.acceptance.tests.applications.crowd;

public class ConfluenceImporterTest extends AppImporterTestBase
{
    @Override
    String getHsqlFileName()
    {
        return "confluencedb-3.4.6";
    }

    public void testImportConfluenceUsers()
    {
        runImportTest("Confluence", 1, 2, 2);
    }
}
