package com.atlassian.crowd.acceptance.tests.persistence.dao.group;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.group.InternalGroupWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import org.hamcrest.Matchers;
import org.hibernate.proxy.HibernateProxy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static com.atlassian.crowd.model.group.GroupType.GROUP;
import static com.atlassian.crowd.model.group.Groups.namesOf;
import static com.atlassian.crowd.test.matchers.CrowdMatchers.groupNamed;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * This class tests GroupDAOHibernate beyond the implementation of the {@link com.atlassian.crowd.embedded.spi.GroupDao}
 * interface. This includes tests for methods with a more concrete return type (e.g.,
 * {@link com.atlassian.crowd.model.group.InternalGroup} for the return of
 * {@link GroupDAOHibernate#findByName(long, String)}, and also methods that do not
 * exist in the interface, such as {@link com.atlassian.crowd.dao.group.GroupDAOHibernate#removeAll(long)}.
 * Therefore, the tests in this class are implementation specific, and cannot be run against
 * other implementations of the interface.
 * The reason to have tests for the implementation (as opposed to just testing the interface)
 * is that some legacy code is coupled to the Hibernate implementation of the interface.
 *
 * @see GroupDaoCRUDTest
 * @see GroupDaoSearchTest
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class GroupDAOHibernateTest
{
    @Inject private GroupDAOHibernate groupDAO;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    private static final long DIRECTORY_ID = 1L;
    private static final String EXISTING_GROUP_1 = "administratorS";
    private static final String EXISTING_ROLE_2 = "users";
    private static final String EXISTING_GROUP_3 = "developers";
    private static final String EXISTING_GROUP_4 = "Managers";
    private static final String NON_EXISTING_GROUP = "NewGroup";
    private static final String NON_EXISTING_GROUP_DESC = "New Group";

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Test
    public void testFindByNameReturnsGroupWithLazyLoadedDirectory() throws Exception
    {
        InternalGroup group = groupDAO.findByName(DIRECTORY_ID, EXISTING_GROUP_1);

        // lazy-load check
        assertTrue(((HibernateProxy) group.getDirectory()).getHibernateLazyInitializer().isUninitialized());
    }

    @Test
    public void testFindByNameMixedCase() throws Exception
    {
        InternalGroup group = groupDAO.findByName(2L, "teSterS");

        assertEquals(toLowerCase("Testers"), group.getLowerName());
    }

    @Test
    public void testFindByMixedCaseNameWithAttributes() throws Exception
    {
        InternalGroupWithAttributes group = groupDAO.findByNameWithAttributes(2L, "TestErs");

        assertEquals("testers", group.getInternalGroup().getLowerName());
    }

    @Test
    public void testAddAssignsAnId() throws Exception
    {
        InternalGroup createdGroup = createNewGroup(NON_EXISTING_GROUP);

        assertTrue(createdGroup.getId() > 0);
    }

    @Test
    public void testAddLocalAssignsAnId() throws Exception
    {
        InternalGroup createdGroup = createNewLocalGroup(NON_EXISTING_GROUP);

        assertTrue(createdGroup.getId() > 0);
    }

    @Test
    public void testRemoveAll()
    {
        groupDAO.removeAll(DIRECTORY_ID);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertTrue(groupDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).returningAtMost(10)).isEmpty());
        assertThat(jdbcTemplate.queryForObject("select count(*) from cwd_membership where directory_id = ?",
                                               Integer.class, DIRECTORY_ID), is(0));
    }

    @Test
    public void testRemoveAllGroups()
    {
        groupDAO.removeAllGroups(DIRECTORY_ID, ImmutableSet.of(EXISTING_GROUP_1, EXISTING_GROUP_3));

        List<Group> groups = groupDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).returningAtMost(EntityQuery.ALL_RESULTS));

        assertThat(groups, not(Matchers.<Group>hasItem(groupNamed(EXISTING_GROUP_1))));
        assertThat(groups, not(Matchers.<Group>hasItem(groupNamed(EXISTING_GROUP_3))));
        assertThat(groups, Matchers.<Group>hasItem(groupNamed(EXISTING_GROUP_4)));
    }

    @Test
    public void testAddAllUsers() throws Exception
    {
        groupDAO.addAll(ImmutableSet.of(
                new GroupTemplate("new group 1", DIRECTORY_ID, GroupType.GROUP),
                new GroupTemplate("new group 2", DIRECTORY_ID, GroupType.GROUP)));

        List<Group> groups = groupDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).returningAtMost(EntityQuery.ALL_RESULTS));

        assertThat(groups, Matchers.<Group>hasItem(groupNamed("new group 1")));
        assertThat(groups, Matchers.<Group>hasItem(groupNamed("new group 2")));
    }

    @Test
    public void testStoreAttributesWithInsertForNewGroup() throws Exception
    {
        // add group
        Group createdGroup = createNewGroup(NON_EXISTING_GROUP);

        // add attributes
        Map<String, Set<String>> attributes = new HashMap<String, Set<String>>();
        attributes.put("color", Sets.newHashSet("Blue", "Green"));
        attributes.put("phone", Sets.newHashSet("131111"));
        groupDAO.storeAttributes(createdGroup, attributes);

        // retrieve and verify
        InternalGroupWithAttributes returnedGroup = groupDAO.findByNameWithAttributes(DIRECTORY_ID, NON_EXISTING_GROUP.toUpperCase());
        assertEquals(2, returnedGroup.getKeys().size());
        assertEquals(2, returnedGroup.getValues("color").size());
        assertTrue(returnedGroup.getValues("color").contains("Blue"));
        assertTrue(returnedGroup.getValues("color").contains("Green"));
        assertEquals(1, returnedGroup.getValues("phone").size());
        assertTrue(returnedGroup.getValues("phone").contains("131111"));

        // test lower case values were stored
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet("select attribute_lower_value from cwd_group_attribute where group_id = " + returnedGroup.getInternalGroup().getId());

        Set<String> lowerCaseAttributeValues = new HashSet<String>();
        while (sqlRowSet.next())
        {
            lowerCaseAttributeValues.add(sqlRowSet.getString("attribute_lower_value"));
        }

        assertEquals(3, lowerCaseAttributeValues.size());
        assertTrue(lowerCaseAttributeValues.contains(toLowerCase("Blue")));
        assertTrue(lowerCaseAttributeValues.contains(toLowerCase("Green")));
        assertTrue(lowerCaseAttributeValues.contains(toLowerCase("131111")));
    }

    @Test
    public void testFindByNames()
    {
        List<Group> groups = new ArrayList<Group>(groupDAO.findByNames(DIRECTORY_ID, Arrays.asList(EXISTING_GROUP_1,
                                                                                                   "bogus",
                                                                                                   EXISTING_GROUP_3)));

        assertThat(namesOf(groups), containsInAnyOrder(EXISTING_GROUP_1, EXISTING_GROUP_3));
    }

    private InternalGroup createNewGroup(String groupName)
        throws DirectoryNotFoundException, GroupNotFoundException, InvalidGroupException
    {
        GroupTemplate group = new GroupTemplate(groupName, DIRECTORY_ID, GROUP);
        group.setActive(true);
        group.setDescription(NON_EXISTING_GROUP_DESC);
        return groupDAO.add(group);
    }

    private InternalGroup createNewLocalGroup(String groupName)
        throws DirectoryNotFoundException, GroupNotFoundException, InvalidGroupException
    {
        GroupTemplate group = new GroupTemplate(groupName, DIRECTORY_ID, GROUP);
        group.setActive(true);
        group.setDescription(NON_EXISTING_GROUP_DESC);
        return groupDAO.addLocal(group);
    }
}
