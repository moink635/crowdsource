package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.google.common.collect.ImmutableList;

/**
 * Test class for testing the updating of a group
 */
public class ViewGroupTest extends CrowdAcceptanceTestCase
{
    private static final String CROWD_ADMINISTRATORS_GROUP = "crowd-administrators";
    private static final String CROWD_DEVELOPERS_GROUP = "crowd-developers";
    private static final String TEST_INTERNAL_DIRECTORY = "Test Internal Directory";

    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("viewgrouptest.xml");
    }

    public void testUpdateGroup()
    {
        log("Running: testUpdateGroup");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, TEST_INTERNAL_DIRECTORY);
        setWorkingForm("groupForm");
        setTextField("description", "test-description");

        uncheckCheckbox("active");

        // Submit these updates
        submit();
        setWorkingForm("groupForm");
        // assert that we have updated the group
        assertKeyPresent("updatesuccessful.label");

        assertTextFieldEquals("description", "test-description");

        assertCheckboxNotSelected("active");
    }

    public void testViewGroupMembers()
    {
        log("Running: testViewGroupMembers");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, TEST_INTERNAL_DIRECTORY);

        clickLink("view-group-users");

        assertKeyNotPresent("group.modify.disabled");
        assertButtonPresent("addUsers");
        assertButtonPresent("removeUsers");

        assertTextInTable("view-group-users", new String[]{"admin", "admin@example.com", "true"});
    }

    /**
     * CWD-716
     */
    public void testViewGroupMembersThenRemoveGroup()
    {
        log("Running: testViewGroupMembersThenRemoveGroup");

        gotoViewGroup(CROWD_DEVELOPERS_GROUP, TEST_INTERNAL_DIRECTORY);

        clickLink("view-group-users");

        clickLinkWithKey("menu.removegroup.label");
        assertWarningNotPresent();      // pre-CWD-716 fix version would fail here

        submit();
        assertKeyPresent("updatesuccessful.label");
    }

    public void testViewGroupMembersNoneAssigned()
    {
        log("Running: testViewGroupMembersNoneAssigned");

        gotoViewGroup(CROWD_DEVELOPERS_GROUP, TEST_INTERNAL_DIRECTORY);

        clickLink("view-group-users");

        assertKeyPresent("viewprincipals.group.noprincipals.assigned", ImmutableList.of(CROWD_DEVELOPERS_GROUP));
    }

    public void testViewGroupMembersNoModifyPermissions()
    {
        intendToModifyData();

        log("Running: testViewGroupNoModifyPermissions");

        // Disable 'Modify Group' permission for the directory
        gotoBrowseDirectories();
        clickLinkWithExactText(TEST_INTERNAL_DIRECTORY);
        clickLink("internal-permissions");
        setWorkingForm("permissionForm");
        uncheckCheckbox("permissionGroupModify");
        submit();

        // Check adding members to group is disabled
        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, TEST_INTERNAL_DIRECTORY);
        clickLink("view-group-users");
        assertKeyPresent("group.modify.disabled");
        assertButtonNotPresent("addUsers");
        assertButtonNotPresent("removeUsers");
        assertTextInTable("view-group-users", new String[]{"admin", "admin@example.com", "true"});
    }
}
