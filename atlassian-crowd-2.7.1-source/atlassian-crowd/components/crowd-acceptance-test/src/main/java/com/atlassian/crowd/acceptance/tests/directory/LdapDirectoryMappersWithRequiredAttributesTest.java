package com.atlassian.crowd.acceptance.tests.directory;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.ApacheDS15;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.AttributeMapper;
import com.atlassian.crowd.embedded.api.UserWithAttributes;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.search.ldap.LDAPQueryTranslater;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.event.api.EventPublisher;

import org.springframework.ldap.core.DirContextAdapter;

public class LdapDirectoryMappersWithRequiredAttributesTest extends BaseTest
{
    public LdapDirectoryMappersWithRequiredAttributesTest()
    {
        setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
    }

    @Override
    protected void loadTestData() throws Exception
    {
    }

    @Override
    protected void removeTestData()
    {
    }

    // Assume we're configured with LDAP
    RemoteDirectory getReconfiguredDirectory(String implementation) throws DirectoryInstantiationException
    {
        directory.setImplementationClass(implementation);
        directory.setAttribute(LDAPPropertiesMapper.LDAP_BASEDN_KEY, "ou=Users,dc=example,dc=com");
        directory.setAttribute(LDAPPropertiesMapper.USER_USERNAME_KEY, "uid");
        return getRemoteDirectory();
    }

    public void testCustomAttributeIsNotPresentByDefault() throws Exception
    {
        RemoteDirectory rd = getReconfiguredDirectory(ApacheDS15.class.getName());

        UserWithAttributes user = rd.findUserWithAttributesByName("aeinstein");
        assertNotNull(user);
        assertNull("Custom attributes are not returned with the default RemoteDirectory",
                user.getValue("test.phone"));
    }

    public void testRequiredAttributesArePresentForCustomAttributeMapper() throws Exception
    {
        RemoteDirectory rd = getReconfiguredDirectory(DirectoryWithCustomAttributeMapper.class.getName());

        UserWithAttributes user = rd.findUserWithAttributesByName("aeinstein");
        assertNotNull(user);
        assertEquals("Custom attributes are returned when a custom AttributeMapper requests them",
                "+1 904 982 6882", user.getValue("test.phone"));
    }

    public void testAttributesAreNotPresentWhenCustomAttributeMapperDoesNotDeclareThem() throws Exception
    {
        RemoteDirectory rd = getReconfiguredDirectory(DirectoryWithCustomAttributeMapperWithoutRequiredFields.class.getName());

        UserWithAttributes user = rd.findUserWithAttributesByName("aeinstein");
        assertNotNull(user);
        assertEquals("Custom attributes are not available when a custom AttributeMapper claims not to need them",
                "(none specified)", user.getValue("test.phone"));
    }

    public void testRequiredAttributesArePresentForCustomAttributeMapperWhenAllAttributesAreRequested() throws Exception
    {
        RemoteDirectory rd = getReconfiguredDirectory(DirectoryWithCustomAttributeMapperThatRequestsAllFields.class.getName());

        UserWithAttributes user = rd.findUserWithAttributesByName("aeinstein");
        assertNotNull(user);
        assertEquals("Custom attributes are available when a custom AttributeMapper claims to need all attributes",
                "+1 904 982 6882", user.getValue("test.phone"));
    }

    static abstract class PhoneNumberAttributeMapper implements AttributeMapper
    {
        @Override
        public Set<String> getValues(DirContextAdapter ctx) throws Exception
        {
            String value = ctx.getStringAttribute("telephonenumber");
            if (value == null)
            {
                value = "(none specified)";
            }

            return Collections.singleton(value);
        }

        @Override
        public String getKey()
        {
            return "test.phone";
        }
    }

    public static class DirectoryWithCustomAttributeMapper extends ApacheDS15
    {
        public DirectoryWithCustomAttributeMapper(LDAPQueryTranslater ldapQueryTranslater,
                EventPublisher eventPublisher, InstanceFactory instanceFactory,
                PasswordEncoderFactory passwordEncoderFactory)
        {
            super(ldapQueryTranslater, eventPublisher, instanceFactory, passwordEncoderFactory);
        }

        @Override
        protected List<AttributeMapper> getCustomUserAttributeMappers()
        {
            return Collections.<AttributeMapper>singletonList(new PhoneNumberAttributeMapper(){
                @Override
                public Set<String> getRequiredLdapAttributes()
                {
                    return Collections.singleton("telephonenumber");
                }
            });
        }
    }

    public static class DirectoryWithCustomAttributeMapperWithoutRequiredFields extends ApacheDS15
    {
        public DirectoryWithCustomAttributeMapperWithoutRequiredFields(LDAPQueryTranslater ldapQueryTranslater,
                EventPublisher eventPublisher, InstanceFactory instanceFactory,
                PasswordEncoderFactory passwordEncoderFactory)
        {
            super(ldapQueryTranslater, eventPublisher, instanceFactory, passwordEncoderFactory);
        }

        @Override
        protected List<AttributeMapper> getCustomUserAttributeMappers()
        {
            return Collections.<AttributeMapper>singletonList(new PhoneNumberAttributeMapper(){
                @Override
                public Set<String> getRequiredLdapAttributes()
                {
                    return Collections.emptySet();
                }
            });
        }
    }

    public static class DirectoryWithCustomAttributeMapperThatRequestsAllFields extends ApacheDS15
    {
        public DirectoryWithCustomAttributeMapperThatRequestsAllFields(LDAPQueryTranslater ldapQueryTranslater,
                EventPublisher eventPublisher, InstanceFactory instanceFactory,
                PasswordEncoderFactory passwordEncoderFactory)
        {
            super(ldapQueryTranslater, eventPublisher, instanceFactory, passwordEncoderFactory);
        }

        @Override
        protected List<AttributeMapper> getCustomUserAttributeMappers()
        {
            return Collections.<AttributeMapper>singletonList(new PhoneNumberAttributeMapper(){
                @Override
                public Set<String> getRequiredLdapAttributes()
                {
                    return null;
                }
            });
        }
    }
}
