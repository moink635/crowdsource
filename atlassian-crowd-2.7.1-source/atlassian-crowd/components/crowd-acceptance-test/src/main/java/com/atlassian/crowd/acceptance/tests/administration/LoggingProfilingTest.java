package com.atlassian.crowd.acceptance.tests.administration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;

import static org.hamcrest.CoreMatchers.everyItem;

import static org.hamcrest.CoreMatchers.hasItems;

import net.sourceforge.jwebunit.html.Cell;
import net.sourceforge.jwebunit.html.Row;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;

public class LoggingProfilingTest extends CrowdAcceptanceTestCase
{
    public void testRevertToDefaultReturnsLogLevelsToKnownState()
    {
        restoreBaseSetup();
        intendToModifyData();

        _loginAdminUser();

        gotoLoggingProfiling();

        clickButton("revertToDefault");

        Map<String, String> initialLevels = getCurrentLevels();

        assertThat(initialLevels.keySet(),
                hasItems(
                        "root",
                        "com.atlassian.crowd",
                        "com.atlassian.crowd.startup",
                        "com.atlassian.crowd.license"));

        assertThat("No default levels are FATAL",
                initialLevels.values(), everyItem(not(equalTo("FATAL"))));

        setWorkingForm("logging");

        for (int i = 0; i < initialLevels.size(); i++)
        {
            selectOption("levelNames", i, "FATAL");
        }

        submit("updateLogging");

        Map<String, String> levels = getCurrentLevels();

        assertEquals(initialLevels.keySet(), levels.keySet());
        assertThat(levels.values(), everyItem(equalTo("FATAL")));

        clickButton("revertToDefault");

        assertEquals("Levels after Revert to Default match initial levels",
                initialLevels, getCurrentLevels());
    }

    Map<String, String> getCurrentLevels()
    {
        Map<String, String> levels = new HashMap<String, String>();

        ArrayList<Row> rows = getTable("loglevelTable").getRows();

        Row header = rows.get(0);
        assertEquals(3, header.getCells().size());
        assertEquals("Class/Package Name", ((Cell) header.getCells().get(0)).getValue());
        assertEquals("Current Level", ((Cell) header.getCells().get(1)).getValue());
        assertEquals("New Level", ((Cell) header.getCells().get(2)).getValue());

        for (Row r : (Iterable<Row>) rows.subList(1, rows.size()))
        {
            List<Cell> cells = r.getCells();

            assertNull(levels.put(cells.get(0).getValue(), cells.get(1).getValue()));
        }

        return levels;
    }
}
