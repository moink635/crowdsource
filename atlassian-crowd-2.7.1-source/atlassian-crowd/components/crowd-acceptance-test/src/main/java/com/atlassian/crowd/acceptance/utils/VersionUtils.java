package com.atlassian.crowd.acceptance.utils;

public class VersionUtils
{
    private VersionUtils()
    {
        // Not to be instantiated.
    }

    /**
     * Compares two version numbers.
     *
     * Only numbers and dots are allowed in the actual release number (e.g. "1.4.5").
     * Anything after possible '-' is ignored.
     *
     * @param aVersion version number to compare
     * @param bVersion version number to compare
     * @return positive integer if a is bigger than b, negative integer if b is bigger than a, or 0 if they are equal
     */
    public static int compareVersions(String aVersion, String bVersion)
    {
        final String[] aComponents = aVersion.split("-")[0].split("\\.");
        final String[] bComponents = bVersion.split("-")[0].split("\\.");
        for (int i = 0; i < aComponents.length && i < bComponents.length; ++i)
        {
            final int aComponent = Integer.parseInt(aComponents[i]);
            final int bComponent = Integer.parseInt(bComponents[i]);
            if (aComponent != bComponent)
            {
                return aComponent - bComponent;
            }
        }
        for (int i = bComponents.length; i < aComponents.length; ++i)
        {
            final int aComponent = Integer.parseInt(aComponents[i]);
            if (aComponent != 0)
            {
                return 1;
            }
        }
        for (int i = aComponents.length; i < bComponents.length; ++i)
        {
            final int bComponent = Integer.parseInt(bComponents[i]);
            if (bComponent != 0)
            {
                return -1;
            }
        }
        return 0;
    }
}
