package com.atlassian.crowd.directory;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.LdapTemplateWithClassLoaderWrapper;

/**
 * This class is used for gaining access to non-public {@link SpringLDAPConnector} fields and methods .
 */
public class SpringLDAPConnectorAccessor
{
    private final SpringLDAPConnector connector;

    public SpringLDAPConnectorAccessor(SpringLDAPConnector connector)
    {
        this.connector = connector;
    }

    public LdapTemplateWithClassLoaderWrapper getLdapTemplate()
    {
        return connector.ldapTemplate;
    }

    public LDAPPropertiesMapper getLdapPropertiesMapper()
    {
        return connector.getLdapPropertiesMapper();
    }
}
