package com.atlassian.crowd.acceptance.tests.persistence.directory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.TimestampedUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests up to JDBC layer.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml",
    "classpath:/applicationContext-CrowdUtils.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class InternalDirectoryIntegrationTest
{
    @Inject private DirectoryDao directoryDao;
    @Inject private DirectoryInstanceLoader directoryInstanceLoader;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    private InternalDirectory internalDirectory;
    private PasswordCredential passwordCredential;

    // Values correspond to data from sample-data-v2.xml
    private static final long DIRECTORY_ID = 1;
    private static final String DIRECTORY_NAME = "directory1";
    // Group stuff
    private static final String GROUP1_NAME = "administratorS";
//    private static final String GROUP2_NAME = "users";

    private static final String GROUP_DESCRIPTION = "The Administrator Group";
    // User stuff
    private static final String USER_NAME = "admin";

    private static final String USER_DISPLAYNAME = "Super Man";

    // Values not from sample-data-v2.xml (used for testAdd*)
    private static final long INVALID_DIRECTORY_ID = 4;

    private static final String NEW_GROUP_NAME = "brand-New-group";
    private static final String NEW_GROUP_DESCRIPTION = "New Test Group";
    private GroupTemplate newGroup;

    private static final String NEW_GROUP_NAME2 = "brand-new-group2";
    private static final String NEW_GROUP_DESCRIPTION2 = "New Test Group2";
    private GroupTemplate newGroup2;

    private static final String NEW_GROUP_NAME3 = "brand-new-group3";
    private static final String NEW_GROUP_DESCRIPTION3 = "New Test Group3";
    private GroupTemplate newGroup3;


    private static final String NEW_USER_NAME = "New-User";
    private static final String NEW_USER_DISPLAY_NAME = "New Person";
    private UserTemplate newUser;


    private static final String NEW_USER_NAME2 = "new-user2";
    private static final String NEW_USER_DISPLAY_NAME2 = "New Person2";
    private UserTemplate newUser2;


    private static final String NEW_USER_NAME3 = "new-user3";
    private static final String NEW_USER_DISPLAY_NAME3 = "New Person3";
    private UserTemplate newUser3;

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Before
    public void onSetUp() throws Exception
    {
        internalDirectory = getInternalDirectory();

        Map<String, String> attributes = new HashMap<String, String>();
        attributes.put(InternalDirectory.ATTRIBUTE_PASSWORD_REGEX, "");
        attributes.put(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, "");
        internalDirectory.setAttributes(attributes);

        newGroup = makeGroup(NEW_GROUP_NAME, DIRECTORY_ID, NEW_GROUP_DESCRIPTION);
        newGroup2 = makeGroup(NEW_GROUP_NAME2, DIRECTORY_ID, NEW_GROUP_DESCRIPTION2);
        newGroup3 = makeGroup(NEW_GROUP_NAME3, DIRECTORY_ID, NEW_GROUP_DESCRIPTION3);

        newUser = makeUser(NEW_USER_NAME, DIRECTORY_ID, NEW_USER_DISPLAY_NAME);
        newUser2 = makeUser(NEW_USER_NAME2, DIRECTORY_ID, NEW_USER_DISPLAY_NAME2);
        newUser3 = makeUser(NEW_USER_NAME3, DIRECTORY_ID, NEW_USER_DISPLAY_NAME3);

        passwordCredential = PasswordCredential.NONE;
    }

    private GroupTemplate makeGroup(String name, long directoryID, String description)
    {
        GroupTemplate group = new GroupTemplate(name, directoryID, GroupType.GROUP);
        group.setDescription(description);
        return group;
    }

    private UserTemplate makeUser(String name, long directoryID, String displayName)
    {
        UserTemplate user = new UserTemplate(name, directoryID);
        user.setDisplayName(displayName);
        user.setFirstName("first");
        user.setLastName("last");
        user.setEmailAddress(name + "@blah.com");
        return user;
    }


    public InternalDirectory getInternalDirectory() throws Exception
    {
        Directory directory = directoryDao.findByName(DIRECTORY_NAME);
        return (InternalDirectory) directoryInstanceLoader.getDirectory(directory);
    }

    @Test
    public void testAddUser() throws Exception
    {
        UserTemplate user = makeUser("new", DIRECTORY_ID, "New Guy");
        internalDirectory.addUser(user, PasswordCredential.encrypted("secret"));

        // verify
        User foundUser = internalDirectory.findUserByName("new");
        assertEquals("new", foundUser.getName());
        assertEquals("New Guy", foundUser.getDisplayName());
        assertEquals("first", foundUser.getFirstName());
        assertEquals("last", foundUser.getLastName());
        assertEquals("new@blah.com", foundUser.getEmailAddress());
        assertNotNull("Should assign a unique, immutable identifier", foundUser.getExternalId());
    }

    @Test
    public void testAddUser_displayNameCalculated() throws Exception
    {
        UserTemplate user = makeUser("new", DIRECTORY_ID, null);
        internalDirectory.addUser(user, PasswordCredential.encrypted("secret"));

        // verify
        User foundUser = internalDirectory.findUserByName("new");
        assertEquals("new", foundUser.getName());
        assertEquals("first last", foundUser.getDisplayName());
        assertEquals("first", foundUser.getFirstName());
        assertEquals("last", foundUser.getLastName());
        assertEquals("new@blah.com", foundUser.getEmailAddress());
    }

    @Test
    public void testAddUser_firstNameLastNameCalculated() throws Exception
    {
        UserTemplate user = makeUser("new", DIRECTORY_ID, "new guy");
        user.setFirstName(null);
        user.setLastName(null);
        internalDirectory.addUser(user, PasswordCredential.encrypted("secret"));

        // verify
        User foundUser = internalDirectory.findUserByName("new");
        assertEquals("new", foundUser.getName());
        assertEquals("new guy", foundUser.getDisplayName());
        assertEquals("new", foundUser.getFirstName());
        assertEquals("guy", foundUser.getLastName());
        assertEquals("new@blah.com", foundUser.getEmailAddress());
    }

    @Test
    public void testAddUser_AllCalculated() throws Exception
    {
        UserTemplate user = makeUser("new", DIRECTORY_ID, null);
        user.setFirstName(null);
        user.setLastName(null);
        internalDirectory.addUser(user, PasswordCredential.encrypted("secret"));

        // verify
        User foundUser = internalDirectory.findUserByName("new");
        assertEquals("new", foundUser.getName());
        assertEquals("new", foundUser.getDisplayName());
        assertEquals("", foundUser.getFirstName());
        assertEquals("new", foundUser.getLastName());
        assertEquals("new@blah.com", foundUser.getEmailAddress());
    }

    @Test
    public void testUpdateUser_ShouldPreserveExternalId() throws Exception
    {
        User user = internalDirectory.findUserByName(USER_NAME);
        UserTemplate template = new UserTemplate(user);
        template.setExternalId(null);

        User updatedUser = internalDirectory.updateUser(template);
        assertEquals("external-id-2", updatedUser.getExternalId());

        User foundUser = internalDirectory.findUserByName(USER_NAME);
        assertEquals(USER_NAME, foundUser.getName());
        assertEquals("external-id-2", foundUser.getExternalId());
    }

    @Test
    public void testUpdateUser_ShouldFailOnAttemptToChangeExternalId() throws Exception
    {
        User user = internalDirectory.findUserByName(USER_NAME);
        UserTemplate template = new UserTemplate(user);
        template.setExternalId("new-external-id");

        try
        {
            internalDirectory.updateUser(template);
            fail("InvalidUserException expected");
        }
        catch (InvalidUserException e)
        {
            // ok
        }
    }

    @Test
    public void testUpdateUser_DisplayNameCalculated() throws Exception
    {
        User user = internalDirectory.findUserByName(USER_NAME);
        UserTemplate template = new UserTemplate(user);
        template.setDisplayName("");
        template.setFirstName("first");
        template.setLastName("last");

        internalDirectory.updateUser(template);

        // verify
        User foundUser = internalDirectory.findUserByName(USER_NAME);
        assertEquals(USER_NAME, foundUser.getName());
        assertEquals("first last", foundUser.getDisplayName());
        assertEquals("first", foundUser.getFirstName());
        assertEquals("last", foundUser.getLastName());
    }

    @Test
    public void testUpdateUser_firstNameLastNameCalculated() throws Exception
    {
        User user = internalDirectory.findUserByName(USER_NAME);
        UserTemplate template = new UserTemplate(user);
        template.setDisplayName("first last");
        template.setFirstName(null);
        template.setLastName(null);

        internalDirectory.updateUser(template);

        // verify
        User foundUser = internalDirectory.findUserByName(USER_NAME);
        assertEquals(USER_NAME, foundUser.getName());
        assertEquals("first last", foundUser.getDisplayName());
        assertEquals("first", foundUser.getFirstName());
        assertEquals("last", foundUser.getLastName());
    }

    @Test
    public void testUpdateUser_AllCalculated() throws Exception
    {
        User user = internalDirectory.findUserByName(USER_NAME);
        UserTemplate template = new UserTemplate(user);
        template.setDisplayName(null);
        template.setFirstName(null);
        template.setLastName(null);

        internalDirectory.updateUser(template);

        // verify
        User foundUser = internalDirectory.findUserByName(USER_NAME);
        assertEquals(USER_NAME, foundUser.getName());
        assertEquals(USER_NAME, foundUser.getDisplayName());
        assertEquals("", foundUser.getFirstName());
        assertEquals(USER_NAME, foundUser.getLastName());
    }

    @Test
    public void testUpdateCredentialMaintainsCrendentialHistory() throws Exception
    {
        internalDirectory.setAttributes(ImmutableMap.of(InternalDirectory.ATTRIBUTE_PASSWORD_HISTORY_COUNT, "2",
                                                        InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, "plaintext"));

        internalDirectory.updateUserCredential(USER_NAME, PasswordCredential.unencrypted("password1"));
        internalDirectory.updateUserCredential(USER_NAME, PasswordCredential.unencrypted("password2"));
        internalDirectory.updateUserCredential(USER_NAME, PasswordCredential.unencrypted("password3"));
        // at this point, credential history contains password2 and password3, but not password1

        try
        {
            // back to password2 should not be possible
            internalDirectory.updateUserCredential(USER_NAME, PasswordCredential.unencrypted("password2"));
            fail("InvalidCredentialException expected");
        }
        catch (InvalidCredentialException e)
        {
            // ok
        }

        // changing to password1 should be OK
        internalDirectory.updateUserCredential(USER_NAME, PasswordCredential.unencrypted("password1"));
    }

    // GROUP tests
    @Test
    public void testAddGroup() throws Exception
    {
        // make sure group does not already exist
        try
        {
            internalDirectory.findGroupByName(NEW_GROUP_NAME);
            fail("GroupNotFoundException exepcted since Group should not exist");
        }
        catch (GroupNotFoundException e)
        {
            // group has been deleted
        }

        // check it does not exist in database
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertEquals(0,
                     jdbcTemplate.queryForInt("select count(*) from cwd_group where group_name = ?",
                                              new Object[] { NEW_GROUP_NAME }));

        // add group
        internalDirectory.addGroup(newGroup);

        // check group can be found
        Group group = internalDirectory.findGroupByName(NEW_GROUP_NAME);
        assertEquals(NEW_GROUP_DESCRIPTION, group.getDescription());


        // check group can be found in database
        assertEquals(1,
                     jdbcTemplate.queryForInt("select count(*) from cwd_group where group_name = ?",
                                              new Object[] { NEW_GROUP_NAME }));
    }

    @Test
    public void testAddGroupInvalidDirectoryId() throws Exception
    {
        GroupTemplate invalidGroup = new GroupTemplate(NEW_GROUP_NAME, INVALID_DIRECTORY_ID, GroupType.GROUP);

        // make sure group does not already exist
        try
        {
            internalDirectory.findGroupByName(NEW_GROUP_NAME);
            fail("GroupNotFoundException exepcted since Group should not exist");
        }
        catch (GroupNotFoundException e)
        {
            // group has been deleted
        }

        // check it does not exist in database
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertEquals(0, jdbcTemplate.queryForInt("select count(*) from cwd_group where group_name = ?",
                                                 new Object[] { NEW_GROUP_NAME }));

        // add group
        try
        {
            internalDirectory.addGroup(invalidGroup);
            fail("IllegalArgumentException expected - group has invalid directory id");
        }
        catch (IllegalArgumentException e)
        {
            // expected
        }

        // check it does not exist in database
        assertEquals(jdbcTemplate.queryForInt("select count(*) from cwd_group where group_name = ?",
                                              new Object[] { NEW_GROUP_NAME }), 0);
    }

    @Test
    public void testUpdateGroup() throws Exception
    {
        // create a new dummy group with same name/directory id as original group. just diff description
        GroupTemplate groupDiffDescription = makeGroup(GROUP1_NAME, DIRECTORY_ID, NEW_GROUP_DESCRIPTION);

        // check the group can be found (ie. it exists)
        Group group = internalDirectory.findGroupByName(GROUP1_NAME);
        assertEquals(GROUP_DESCRIPTION, group.getDescription());

        // make sure join table has at least one member (otherwise test is useless)
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertEquals(1, jdbcTemplate.queryForInt("select count(*) from cwd_group where group_name = ?",
                                                 new Object[] { GROUP1_NAME }));

        //update the group
        internalDirectory.updateGroup(groupDiffDescription);

        // check the group can be found from new updated details
        Group updatedGroup = internalDirectory.findGroupByName(groupDiffDescription.getName());
        Assert.assertEquals(1, jdbcTemplate.queryForInt("select count(*) from cwd_group where group_name = ?",
                                                        new Object[] { groupDiffDescription.getName() }));

        // check directoryID and name are same. while description has been updated.
        assertEquals(group.getDirectoryId(), updatedGroup.getDirectoryId());
        assertEquals(group.getName(), updatedGroup.getName());
        assertEquals(NEW_GROUP_DESCRIPTION, updatedGroup.getDescription());
    }

    @Test
    public void testRemoveGroup() throws Exception
    {
        // check the group can be found (ie. it exists)
        Group group = internalDirectory.findGroupByName(GROUP1_NAME);
        assertEquals(GROUP_DESCRIPTION, group.getDescription());

        // make sure join table has at least one member (otherwise test is useless)
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertTrue(jdbcTemplate.queryForInt("select count(*) from cwd_membership where parent_name = ?",
                                            new Object[] { GROUP1_NAME }) > 0);

        // try removing the group (failure to remove produces an exception)
        internalDirectory.removeGroup(GROUP1_NAME);

        // make sure group cannot be found
        try
        {
            internalDirectory.findGroupByName(GROUP1_NAME);
            fail("GroupNotFoundException exepcted since Group should have been deleted");
        }
        catch (GroupNotFoundException e)
        {
            // group has been deleted
        }

        // make sure the join table for group memberships has nothing (the hardcore test)
        assertEquals(0, jdbcTemplate.queryForInt("select count(*) from cwd_membership where parent_name = ?",
                                                 new Object[] { GROUP1_NAME }));
    }


    // USER tests

    @Test
    public void testRemoveUser() throws Exception
    {
        // check user can be found
        User user = internalDirectory.findUserByName(USER_NAME);
        assertEquals(USER_DISPLAYNAME, user.getDisplayName());

        // check user belongs in group
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertEquals(1,
                     jdbcTemplate.queryForInt(
                         "select count(*) from cwd_membership where parent_name = ? and child_name = ?",
                         new Object[] { GROUP1_NAME, USER_NAME }));

        // remove user
        internalDirectory.removeUser(USER_NAME);

        // make sure user cannot be found
        try
        {
            internalDirectory.findUserByName(USER_NAME);
            fail("UserNotFoundException exepcted since User should have been deleted");
        }
        catch (UserNotFoundException e)
        {
            // expected - user has been deleted
        }

        // make sure the group/user membership no longer exists
        assertEquals(0,
                     jdbcTemplate.queryForInt(
                         "select count(*) from cwd_membership where parent_name = ? and child_name = ?",
                         new Object[] { GROUP1_NAME, USER_NAME }));
        assertEquals(0, jdbcTemplate.queryForInt("select count(*) from cwd_membership where child_name = ?",
                                                 new Object[] { USER_NAME }));
    }

    @Test
    public void testAddRemoveGroupToGroup() throws Exception
    {
        // first add the group to directory
        internalDirectory.addGroup(newGroup);

        // group not yet a member
        assertFalse(internalDirectory.isGroupDirectGroupMember(newGroup.getName(), GROUP1_NAME));

        // add newGroup as childgroup of existing group
        internalDirectory.addGroupToGroup(newGroup.getName(), GROUP1_NAME);

        // group is now a member
        assertTrue(internalDirectory.isGroupDirectGroupMember(newGroup.getName(), GROUP1_NAME));

        // double check
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertEquals(1,
                     jdbcTemplate.queryForInt(
                         "select count(*) from cwd_membership where parent_name = ? and child_name = ?",
                         new Object[] { GROUP1_NAME, newGroup.getName() }));

        // adding was successful, now test removeGroupFromGroup
        internalDirectory.removeGroupFromGroup(newGroup.getName(), GROUP1_NAME);

        // group should no longer be a member
        assertFalse(internalDirectory.isGroupDirectGroupMember(newGroup.getName(), GROUP1_NAME));

        // double check
        assertEquals(0,
                     jdbcTemplate.queryForInt(
                         "select count(*) from cwd_membership where parent_name = ? and child_name = ?",
                         new Object[] { GROUP1_NAME, newGroup.getName() }));

    }

    @Test
    public void testRemoveGroupFromGroupNotMember() throws Exception
    {
        // first add the group to directory
        internalDirectory.addGroup(newGroup);

        try
        {
            internalDirectory.removeGroupFromGroup(newGroup.getName(), GROUP1_NAME);
            fail("MembershipNotFoundException expected");
        }
        catch (MembershipNotFoundException e)
        {
            // expected
        }

    }

    @Test
    public void testAddUserToGroup() throws Exception
    {
        // first add the group to directory
        internalDirectory.addUser(newUser, passwordCredential);

        // group not yet a member
        assertFalse(internalDirectory.isUserDirectGroupMember(newUser.getName(), GROUP1_NAME));

        // add newGroup as childgroup of existing group
        internalDirectory.addUserToGroup(newUser.getName(), GROUP1_NAME);

        // group is now a member
        assertTrue(internalDirectory.isUserDirectGroupMember(newUser.getName(), GROUP1_NAME));

        // double check
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertEquals(1,
                     jdbcTemplate.queryForInt(
                         "select count(*) from cwd_membership where parent_name = ? and child_name = ?",
                         new Object[] { GROUP1_NAME, newUser.getName() }));

        // adding was successful, now test removeGroupFromGroup
        internalDirectory.removeUserFromGroup(newUser.getName(), GROUP1_NAME);

        // group should no longer be a member
        assertFalse(internalDirectory.isUserDirectGroupMember(newUser.getName(), GROUP1_NAME));

        // double check
        assertEquals(0,
                     jdbcTemplate.queryForInt(
                         "select count(*) from cwd_membership where parent_name = ? and child_name = ?",
                         new Object[] { GROUP1_NAME, newUser.getName() }));
    }

    @Test
    public void testRemoveUserFromGroup() throws Exception
    {
        // first add the group to directory
        internalDirectory.addUser(newUser, passwordCredential);

        try
        {
            internalDirectory.removeUserFromGroup(newUser.getName(), GROUP1_NAME);
            fail("MembershipNotFoundException expected");
        }
        catch (MembershipNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testAddAllUsers() throws Exception
    {
        // create Set of user to add
        Set<UserTemplateWithCredentialAndAttributes> usersToAdd = new HashSet<UserTemplateWithCredentialAndAttributes>();

        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(newUser, passwordCredential));
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(newUser2, passwordCredential));
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(newUser3, passwordCredential));

        // check users are not in directory
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertEquals(0,
                     jdbcTemplate.queryForInt("select count(*) from cwd_user where user_name = ?",
                                              new Object[] { NEW_USER_NAME }));
        assertEquals(0,
                     jdbcTemplate.queryForInt("select count(*) from cwd_user where user_name = ?",
                                              new Object[] { NEW_USER_NAME2 }));
        assertEquals(0,
                     jdbcTemplate.queryForInt("select count(*) from cwd_user where user_name = ?",
                                              new Object[] { NEW_USER_NAME3 }));

        // add users
        internalDirectory.addAllUsers(usersToAdd);

        // check users can be found
        TimestampedUser newUserFound1 = internalDirectory.findUserByName(newUser.getName());
        TimestampedUser newUserFound2 = internalDirectory.findUserByName(newUser2.getName());
        TimestampedUser newUserFound3 = internalDirectory.findUserByName(newUser3.getName());

        assertEquals("Identifiers should be unique", 3, ImmutableSet.of(newUserFound1.getExternalId(),
                                                                        newUserFound2.getExternalId(),
                                                                        newUserFound3.getExternalId()).size());

        // double check
        assertEquals(1, jdbcTemplate.queryForInt("select count(*) from cwd_user where user_name = ?",
                                                 new Object[] { NEW_USER_NAME }));
        assertEquals(1, jdbcTemplate.queryForInt("select count(*) from cwd_user where user_name = ?",
                                                 new Object[] { NEW_USER_NAME2 }));
        assertEquals(1, jdbcTemplate.queryForInt("select count(*) from cwd_user where user_name = ?",
                                                 new Object[] { NEW_USER_NAME3 }));
    }

    @Test
    public void testAddAllUsers_displayNameCalculated() throws Exception
    {
        UserTemplate user1 = makeUser("user1", DIRECTORY_ID, null);
        user1.setFirstName("first1");
        user1.setLastName("last1");

        UserTemplate user2 = makeUser("user2", DIRECTORY_ID, null);
        user2.setFirstName("first2");
        user2.setLastName("last2");

        // create Set of user to add
        Set<UserTemplateWithCredentialAndAttributes> usersToAdd = new HashSet<UserTemplateWithCredentialAndAttributes>();

        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(user1, passwordCredential));
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(user2, passwordCredential));

        // check users are not in directory
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertEquals(0, jdbcTemplate.queryForInt("select count(*) from cwd_user where user_name = ?",
                                                 new Object[] { "user1" }));
        assertEquals(0, jdbcTemplate.queryForInt("select count(*) from cwd_user where user_name = ?",
                                                 new Object[] { "user2" }));

        // add users
        internalDirectory.addAllUsers(usersToAdd);

        // check users can be found
        User foundUser1 = internalDirectory.findUserByName(user1.getName());
        User foundUser2 = internalDirectory.findUserByName(user2.getName());

        assertEquals("user1", foundUser1.getName());
        assertEquals("first1 last1", foundUser1.getDisplayName());
        assertEquals("first1", foundUser1.getFirstName());
        assertEquals("last1", foundUser1.getLastName());
        assertEquals("user1@blah.com", foundUser1.getEmailAddress());

        assertEquals("user2", foundUser2.getName());
        assertEquals("first2 last2", foundUser2.getDisplayName());
        assertEquals("first2", foundUser2.getFirstName());
        assertEquals("last2", foundUser2.getLastName());
        assertEquals("user2@blah.com", foundUser2.getEmailAddress());
    }

    @Test
    public void testAddAllUsers_firstNameLastNameCalculated() throws Exception
    {
        UserTemplate user1 = makeUser("user1", DIRECTORY_ID, "first1 last1");
        user1.setFirstName(null);
        user1.setLastName(null);

        UserTemplate user2 = makeUser("user2", DIRECTORY_ID, "first2 last2");
        user2.setFirstName(null);
        user2.setLastName(null);

        // create Set of user to add
        Set<UserTemplateWithCredentialAndAttributes> usersToAdd = new HashSet<UserTemplateWithCredentialAndAttributes>();

        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(user1, passwordCredential));
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(user2, passwordCredential));

        // check users are not in directory
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertEquals(Integer.valueOf(0),
                     jdbcTemplate.queryForObject("select count(*) from cwd_user where user_name = ?", Integer.class,
                                                 "user1"));
        assertEquals(Integer.valueOf(0),
                     jdbcTemplate.queryForObject("select count(*) from cwd_user where user_name = ?", Integer.class,
                                                 "user2"));

        // add users
        internalDirectory.addAllUsers(usersToAdd);

        // check users can be found
        User foundUser1 = internalDirectory.findUserByName(user1.getName());
        User foundUser2 = internalDirectory.findUserByName(user2.getName());

        assertEquals("user1", foundUser1.getName());
        assertEquals("first1 last1", foundUser1.getDisplayName());
        assertEquals("first1", foundUser1.getFirstName());
        assertEquals("last1", foundUser1.getLastName());
        assertEquals("user1@blah.com", foundUser1.getEmailAddress());

        assertEquals("user2", foundUser2.getName());
        assertEquals("first2 last2", foundUser2.getDisplayName());
        assertEquals("first2", foundUser2.getFirstName());
        assertEquals("last2", foundUser2.getLastName());
        assertEquals("user2@blah.com", foundUser2.getEmailAddress());
    }

    @Test
    public void testAddAllUsers_AllCalculated() throws Exception
    {
        UserTemplate user1 = makeUser("user1", DIRECTORY_ID, null);
        user1.setFirstName(null);
        user1.setLastName(null);

        UserTemplate user2 = makeUser("user2", DIRECTORY_ID, null);
        user2.setFirstName(null);
        user2.setLastName(null);

        // create Set of user to add
        Set<UserTemplateWithCredentialAndAttributes> usersToAdd = new HashSet<UserTemplateWithCredentialAndAttributes>();

        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(user1, passwordCredential));
        usersToAdd.add(new UserTemplateWithCredentialAndAttributes(user2, passwordCredential));

        // check users are not in directory
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertEquals(0, jdbcTemplate.queryForInt("select count(*) from cwd_user where user_name = ?",
                                                 new Object[] { "user1" }));
        assertEquals(0, jdbcTemplate.queryForInt("select count(*) from cwd_user where user_name = ?",
                                                 new Object[] { "user2" }));

        // add users
        internalDirectory.addAllUsers(usersToAdd);

        // check users can be found
        User foundUser1 = internalDirectory.findUserByName(user1.getName());
        User foundUser2 = internalDirectory.findUserByName(user2.getName());

        assertEquals("user1", foundUser1.getName());
        assertEquals("user1", foundUser1.getDisplayName());
        assertTrue(StringUtils.isBlank(foundUser1.getFirstName()));
        assertEquals("user1", foundUser1.getLastName());
        assertEquals("user1@blah.com", foundUser1.getEmailAddress());

        assertEquals("user2", foundUser2.getName());
        assertEquals("user2", foundUser2.getDisplayName());
        assertTrue(StringUtils.isBlank(foundUser2.getFirstName()));
        assertEquals("user2", foundUser2.getLastName());
        assertEquals("user2@blah.com", foundUser2.getEmailAddress());
    }

    @Test
    public void testAddAllUsersToGroup() throws Exception
    {
        Set<String> usersToAdd = ImmutableSet.of("admin", "bob", "jane");

        BatchResult<String> batchResult = internalDirectory.addAllUsersToGroup(usersToAdd, GROUP1_NAME);

        assertThat(batchResult.getFailedEntities(), contains("admin")); // he's already a member
        assertThat(batchResult.getSuccessfulEntities(), containsInAnyOrder("bob", "jane"));

        // double check - group should have at least 3 members
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertThat(jdbcTemplate.queryForInt("select count(*) from cwd_membership where parent_name = ? ",
                                            new Object[] { GROUP1_NAME }),
                   Matchers.greaterThanOrEqualTo(3));

    }

    @Test
    public void testAddAllGroups() throws Exception
    {
        // create Set of groups to add
        Set<GroupTemplate> groupsToAdd = new HashSet<GroupTemplate>();
        groupsToAdd.add(newGroup);
        groupsToAdd.add(newGroup2);
        groupsToAdd.add(newGroup3);

        // check Groups are not in directory
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertEquals(0, jdbcTemplate.queryForInt("select count(*) from cwd_group where group_name = ?",
                                                 new Object[] { NEW_GROUP_NAME }));
        assertEquals(0, jdbcTemplate.queryForInt("select count(*) from cwd_group where group_name = ?",
                                                 new Object[] { NEW_GROUP_NAME2 }));
        assertEquals(0,jdbcTemplate.queryForInt("select count(*) from cwd_group where group_name = ?",
                                                 new Object[] { NEW_GROUP_NAME3 }));

        // add groups
        internalDirectory.addAllGroups(groupsToAdd);

        // check groups can be found
        internalDirectory.findGroupByName(newGroup.getName());
        internalDirectory.findGroupByName(newGroup2.getName());
        internalDirectory.findGroupByName(newGroup3.getName());

        // check Groups are in directory
        assertEquals(1, jdbcTemplate.queryForInt("select count(*) from cwd_group where group_name = ?",
                                                 new Object[] { NEW_GROUP_NAME }));
        assertEquals(1, jdbcTemplate.queryForInt("select count(*) from cwd_group where group_name = ?",
                                                 new Object[] { NEW_GROUP_NAME2 }));
        assertEquals(1, jdbcTemplate.queryForInt("select count(*) from cwd_group where group_name = ?",
                                                 new Object[] { NEW_GROUP_NAME3 }));
    }

    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }

    public void setDirectoryInstanceLoader(DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }

    public void setDataSource(DataSource dataSource)
    {
        this.dataSource = dataSource;
    }

    public void setResetableHiLoGeneratorHelper(ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper)
    {
        this.resetableHiLoGeneratorHelper = resetableHiLoGeneratorHelper;
    }
}
