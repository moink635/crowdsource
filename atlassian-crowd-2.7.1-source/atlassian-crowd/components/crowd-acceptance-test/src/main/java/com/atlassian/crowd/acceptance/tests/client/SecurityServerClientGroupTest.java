package com.atlassian.crowd.acceptance.tests.client;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import static com.atlassian.crowd.acceptance.utils.ArrayAssertions.assertContainsElements;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl;
import com.atlassian.crowd.service.soap.client.SecurityServerClientImpl;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;

import java.util.Properties;

public class SecurityServerClientGroupTest extends CrowdAcceptanceTestCase
{
    private SecurityServerClientImpl securityServerClient;
    private UserAuthenticationContext userAuthenticationContext;
    private Properties sscProperties;

    private static final String GENERAL_GROUP = "general-group";

    private static final String SINGLE_ATTRIBUTE_NAME = "colour";
    private static final String LIST_ATTRIBUTE_NAME = "numbers";

    private static final SOAPAttribute SINGLE_VALUE_ATTRIBUTE = new SOAPAttribute(SINGLE_ATTRIBUTE_NAME, "green");
    private static final SOAPAttribute LIST_ATTRIBUTE = new SOAPAttribute(LIST_ATTRIBUTE_NAME,new String[]{"one", "two", "three"});


    public void setUp() throws Exception
    {
        super.setUp();

        restoreCrowdFromXML("securityserverclienttest.xml");

        sscProperties = AcceptanceTestHelper.loadProperties("localtest.crowd.properties");
        SoapClientProperties cProperties = SoapClientPropertiesImpl.newInstanceFromProperties(sscProperties);
        securityServerClient = new SecurityServerClientImpl(cProperties);

        userAuthenticationContext = new UserAuthenticationContext();
        userAuthenticationContext.setApplication("integrationtest");
    }

    public void testFindGroupByName() throws Exception
    {
        final SOAPGroup soapGroup = securityServerClient.findGroupByName(GENERAL_GROUP);

        assertEquals(GENERAL_GROUP,soapGroup.getName());
        // Group by default has 0 attributes
        assertEquals(0, soapGroup.getAttributes().length);
    }

    public void testFindGroupWithAttributesByName() throws Exception
    {
        final SOAPGroup soapGroup = securityServerClient.findGroupWithAttributesByName(GENERAL_GROUP);

        assertEquals(GENERAL_GROUP,soapGroup.getName());

        // Group by default still has 0 attributes even if we specifically ask for them
        assertEquals(0, soapGroup.getAttributes().length);
    }

    public void testFindGroupWithAttributesByNameCustomAttributes() throws Exception
    {
        intendToModifyData();

        securityServerClient.addAttributeToGroup(GENERAL_GROUP, SINGLE_VALUE_ATTRIBUTE);
        securityServerClient.addAttributeToGroup(GENERAL_GROUP, LIST_ATTRIBUTE);

        final SOAPGroup soapGroup = securityServerClient.findGroupWithAttributesByName(GENERAL_GROUP);
        assertEquals(GENERAL_GROUP, soapGroup.getName());

        assertEquals(2, soapGroup.getAttributes().length);
        assertContainsElements(soapGroup.getAttribute(SINGLE_ATTRIBUTE_NAME).getValues(), "green");
        assertContainsElements(soapGroup.getAttribute(LIST_ATTRIBUTE_NAME).getValues(), "one", "two", "three");
    }


    public void testAddRemoveGroupCustomAttributes() throws Exception
    {
        intendToModifyData();

        SOAPGroup soapGroup = securityServerClient.findGroupWithAttributesByName(GENERAL_GROUP);
        assertEquals(GENERAL_GROUP,soapGroup.getName());

        // Check there are no attributes to start off with
        assertEquals(0, soapGroup.getAttributes().length);

        // Add 2 new custom attributes and check
        securityServerClient.addAttributeToGroup(GENERAL_GROUP, SINGLE_VALUE_ATTRIBUTE);
        securityServerClient.addAttributeToGroup(GENERAL_GROUP, LIST_ATTRIBUTE);
        soapGroup = securityServerClient.findGroupWithAttributesByName(GENERAL_GROUP);
        assertEquals(2, soapGroup.getAttributes().length);
        assertContainsElements(soapGroup.getAttribute(SINGLE_ATTRIBUTE_NAME).getValues(), "green");
        assertContainsElements(soapGroup.getAttribute(LIST_ATTRIBUTE_NAME).getValues(), "one", "two", "three");

        // Remove 1 custom attribute and check
        securityServerClient.removeAttributeFromGroup(GENERAL_GROUP, SINGLE_ATTRIBUTE_NAME);
        soapGroup = securityServerClient.findGroupWithAttributesByName(GENERAL_GROUP);
        assertEquals(1, soapGroup.getAttributes().length);
        assertContainsElements(soapGroup.getAttribute(LIST_ATTRIBUTE_NAME).getValues(), "one", "two", "three");
        assertNull(soapGroup.getAttribute(SINGLE_ATTRIBUTE_NAME));
    }
}
