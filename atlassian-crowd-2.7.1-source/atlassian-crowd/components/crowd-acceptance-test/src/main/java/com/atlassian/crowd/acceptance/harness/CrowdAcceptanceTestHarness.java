package com.atlassian.crowd.acceptance.harness;

import java.io.InputStream;

import com.atlassian.crowd.acceptance.tests.administration.BackupTest;
import com.atlassian.crowd.acceptance.tests.administration.LoggingProfilingTest;
import com.atlassian.crowd.acceptance.tests.administration.SessionConfigTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.AddApplicationTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.AddDirectoryTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.AddGroupLDAPTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.AddGroupTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.AddPrincipalTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.AliasTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.AutoGroupAdderTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.BackupRestoreTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.BrowseDirectoriesTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.BrowsePrincipalSessionsTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.BrowsePrincipalsTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.ChangeExpiredPasswordTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.ConsoleLoginTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.tests.applications.crowd.CsvImporterTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.DelegatedDirectoryTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.DelegatedDirectoryWithNestedGroupsTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.DirectoryAmalgamationTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.DirectoryImporterTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.DirectoryPermissionGroupTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.DirectoryPermissionRoleTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.DirectoryPermissionUserTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.EscapedDnTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.ExpireSessionTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.GzipFilterOptionTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.JiraImporterTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.LdifLoaderForTesting;
import com.atlassian.crowd.acceptance.tests.applications.crowd.LicenseMaintenanceTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.ModifyApplicationPermissionsTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.OGNLInjectionTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.OGNLDoubleEvaluationTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.RecalculateLicenseTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.RemoveApplicationTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.RemoveDirectoryTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.RemoveGroupLDAPTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.ResetPrincipalPasswordTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.SynchroniseCrowdDirectoryFullTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.SynchroniseCrowdDirectoryTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.SynchroniseDirectoryTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.UpdateApplicationDirectoryGroupTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.UpdateConnectionPoolTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.UpdateGroupLDAPTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.ViewApplicationTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.ViewDirectoryTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.ViewGroupLDAPTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.ViewGroupTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.ViewOptionsTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.ViewPrincipalTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.ViewRoleTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.WebAppTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.legacy.Crowd10XmlRestoreTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.legacy.Crowd11XmlRestoreTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.legacy.Crowd12XmlRestoreTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.legacy.Crowd13XmlRestoreTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.legacy.Crowd14XmlRestoreTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.legacy.Crowd15XmlRestoreTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.legacy.Crowd16XmlRestoreTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.plugin.ApplicationPluginPermissioningTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.plugin.ViewApplicationPluginTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.plugin.saml.SAMLAuthTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.plugin.saml.UpdateSAMLConfigurationTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.user.ChangePasswordTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.user.EditProfileTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.user.ResetPasswordTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.user.ViewApplicationsTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.user.ViewGroupsTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.user.ViewRolesTest;
import com.atlassian.crowd.acceptance.tests.rest.service.client.RestCrowdClientTest;
import com.atlassian.crowd.acceptance.tests.administration.ErrorPageTest;
import com.atlassian.crowd.acceptance.tests.administration.GeneralAdministrationTest;

import junit.framework.JUnit4TestAdapter;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * The main testing acceptance harness that will be used to test the Crowd Console and general Crowd tests.
 * <p/>
 * POM will ensure that SetupCrowdTest is run prior to the suite.
 */
public class CrowdAcceptanceTestHarness extends TestCase
{
    public static Test suite()
    {
        InputStream in = CrowdDirectoryTestHarness.class.getResourceAsStream("/com/atlassian/crowd/acceptance/tests/default-entries.ldif");
        assertNotNull(in);

        TestSuite suite = new TestSuite();
        suite.addTest(new LdifLoaderForTesting(CrowdAcceptanceTestCase.HOST_PATH).createRestoreTest(in));

        // Container tests
        suite.addTest(new JUnit4TestAdapter(WebAppTest.class));

        // Main Console Tests
        suite.addTestSuite(ConsoleLoginTest.class);
        suite.addTestSuite(AddApplicationTest.class);
        suite.addTestSuite(AddDirectoryTest.class);
        suite.addTestSuite(AddGroupTest.class);
        suite.addTestSuite(AutoGroupAdderTest.class);
        suite.addTestSuite(DelegatedDirectoryTest.class);
        suite.addTestSuite(AddGroupLDAPTest.class);
        suite.addTestSuite(UpdateGroupLDAPTest.class);
        suite.addTestSuite(AddPrincipalTest.class);
        suite.addTestSuite(BrowsePrincipalsTest.class);
        suite.addTestSuite(BrowseDirectoriesTest.class);
        suite.addTestSuite(CsvImporterTest.class);
        suite.addTestSuite(GzipFilterOptionTest.class);
        suite.addTestSuite(ModifyApplicationPermissionsTest.class);
        suite.addTestSuite(RecalculateLicenseTest.class);
        suite.addTestSuite(RemoveApplicationTest.class);
        suite.addTestSuite(RemoveDirectoryTest.class);
        suite.addTestSuite(ViewGroupTest.class);
        suite.addTestSuite(ViewGroupLDAPTest.class);
        suite.addTestSuite(ViewOptionsTest.class);
        suite.addTestSuite(ViewPrincipalTest.class);
        suite.addTestSuite(ViewRoleTest.class);
        suite.addTestSuite(ViewDirectoryTest.class);
        suite.addTestSuite(DirectoryImporterTest.class);
        suite.addTestSuite(RemoveGroupLDAPTest.class);
        suite.addTestSuite(ExpireSessionTest.class);
        suite.addTestSuite(JiraImporterTest.class);
        suite.addTestSuite(BackupRestoreTest.class);
        suite.addTestSuite(BrowsePrincipalSessionsTest.class);
        suite.addTestSuite(AliasTest.class);
        suite.addTestSuite(DirectoryPermissionRoleTest.class);
        suite.addTestSuite(DirectoryPermissionGroupTest.class);
        suite.addTestSuite(DirectoryPermissionUserTest.class);
        suite.addTestSuite(UpdateApplicationDirectoryGroupTest.class);
        suite.addTestSuite(ChangeExpiredPasswordTest.class);
        suite.addTestSuite(UpdateConnectionPoolTest.class);
        suite.addTestSuite(SynchroniseDirectoryTest.class);
        suite.addTestSuite(SynchroniseCrowdDirectoryTest.class);
        suite.addTestSuite(SynchroniseCrowdDirectoryFullTest.class);
        suite.addTestSuite(SessionConfigTest.class);
        suite.addTestSuite(LoggingProfilingTest.class);
        suite.addTestSuite(OGNLInjectionTest.class);
        suite.addTestSuite(OGNLDoubleEvaluationTest.class);

        // User Console Tests
        suite.addTestSuite(ViewApplicationTest.class);
        suite.addTestSuite(EditProfileTest.class);
        suite.addTestSuite(ChangePasswordTest.class);
        suite.addTestSuite(ViewGroupsTest.class);
        suite.addTestSuite(ViewRolesTest.class);
        suite.addTestSuite(ViewApplicationsTest.class);
        suite.addTestSuite(ResetPasswordTest.class);
        suite.addTestSuite(ResetPrincipalPasswordTest.class);

        // Maintenance tests
        suite.addTestSuite(LicenseMaintenanceTest.class);

        // Plugin Tests
        suite.addTestSuite(UpdateSAMLConfigurationTest.class);
        suite.addTestSuite(SAMLAuthTest.class);
        suite.addTestSuite(ApplicationPluginPermissioningTest.class);
        suite.addTestSuite(ViewApplicationPluginTest.class);

        // Legacy tests
        suite.addTestSuite(Crowd10XmlRestoreTest.class);
        suite.addTestSuite(Crowd11XmlRestoreTest.class);
        suite.addTestSuite(Crowd12XmlRestoreTest.class);
        suite.addTestSuite(Crowd13XmlRestoreTest.class);
        suite.addTestSuite(Crowd14XmlRestoreTest.class);
        suite.addTestSuite(Crowd15XmlRestoreTest.class);
        suite.addTestSuite(Crowd16XmlRestoreTest.class);

        // directory amalgamation tests
        suite.addTestSuite(DirectoryAmalgamationTest.class);

        suite.addTestSuite(RestCrowdClientTest.class);

        // Other LDAP tests
        suite.addTestSuite(EscapedDnTest.class);
        suite.addTestSuite(DelegatedDirectoryWithNestedGroupsTest.class);

        //Error page tests
        suite.addTestSuite(ErrorPageTest.class);
        //Console Administration tests
        suite.addTestSuite(GeneralAdministrationTest.class);
        suite.addTestSuite(BackupTest.class);

        return suite;
    }
}
