package com.atlassian.crowd.acceptance.tests.soap;

import java.io.IOException;
import java.io.InputStream;

import javax.ws.rs.core.MediaType;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.hamcrest.CoreMatchers;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SoapXmlParsingTest extends InformationLeakingTestBase
{
    PostMethod postToSoapEndpoint(String content, String pathSuffix) throws HttpException, IOException
    {
        String endpoint = getBaseUrl() + "/services" + pathSuffix;

        HttpClient client = new HttpClient();

        PostMethod m = new PostMethod(endpoint);

        m.setRequestEntity(new StringRequestEntity(content, "application/soap+xml", "us-ascii"));

        client.executeMethod(m);

        return m;
    }

    PostMethod postToSoapEndpoint(String content) throws HttpException, IOException
    {
        return postToSoapEndpoint(content, "");
    }

    public void testEntityExpansionDoesNotIncludeFileContents() throws IOException
    {
        runEntityExpansionTestWithPathSuffix("");
    }

    private void runEntityExpansionTestWithPathSuffix(String pathSuffix) throws IOException
    {
        InputStream in = getClass().getResourceAsStream("SoapXmlParsingTest-soap-include-external-entity.xml");
        assertNotNull(in);

        String contents = IOUtils.toString(in, "us-ascii");

        contents = contents.replace("/etc/passwd", createSecretFile().toURI().toString());

        PostMethod m = postToSoapEndpoint(contents, pathSuffix);

        MediaType mt = MediaType.valueOf(m.getResponseHeader("content-type").getValue());

        assertThat("The response should be XML",
                mt.getType() + '/' + mt.getSubtype(),
                CoreMatchers.anyOf(is("text/xml"), is("application/xml"), is("application/soap+xml")));

        String resp = IOUtils.toString(m.getResponseBodyAsStream(), "us-ascii");

        assertThat(resp, CoreMatchers.not(CoreMatchers.containsString(secret)));
    }

    public void testValidEntitiesAreExpanded() throws IOException
    {
        InputStream in = getClass().getResourceAsStream("SoapXmlParsingTest-soap-with-amp-entity.xml");
        assertNotNull(in);

        String contents = IOUtils.toString(in, "us-ascii");

        PostMethod m = postToSoapEndpoint(contents);

        MediaType mt = MediaType.valueOf(m.getResponseHeader("content-type").getValue());

        assertThat("The response should be XML",
                mt.getType() + '/' + mt.getSubtype(),
                CoreMatchers.anyOf(is("text/xml"), is("application/xml"), is("application/soap+xml")));

        String resp = IOUtils.toString(m.getResponseBodyAsStream(), "us-ascii");

        assertThat(resp, CoreMatchers.containsString("<faultstring>&amp;</faultstring>"));
    }

    public void testEntityExpansionDoesNotCauseDenialOfService() throws IOException
    {
        InputStream in = getClass().getResourceAsStream("SoapXmlParsingTest-soap-billion-laughs.xml");
        assertNotNull(in);

        String contents = IOUtils.toString(in, "us-ascii");

        PostMethod m = postToSoapEndpoint(contents);

        String resp = IOUtils.toString(m.getResponseBodyAsStream(), "us-ascii");

        assertThat("The response should not indicate a server memory error",
                resp, CoreMatchers.not(CoreMatchers.containsString("java.lang.OutOfMemoryError")));

        MediaType mt = MediaType.valueOf(m.getResponseHeader("content-type").getValue());

        assertThat("The response should be XML, not HTML",
                mt.getType() + '/' + mt.getSubtype(),
                CoreMatchers.anyOf(is("text/xml"), is("application/xml"), is("application/soap+xml")));
    }

    public void testVersionedEndpointsDoNotAllowExpansion() throws IOException
    {
        String[] versions = {"1", "2", "latest"};

        for (String v : versions)
        {
            runEntityExpansionTestWithPathSuffix("/" + v + "/");
        }
    }
}
