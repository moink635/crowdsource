package com.atlassian.crowd.acceptance.tests.applications.crowd;

/**
 * Test Case that modifies the application-permission for directory mappings
 */
public class ModifyApplicationPermissionsTest extends CrowdAcceptanceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreBaseSetup();
        _loginAdminUser();
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testAddingAndRemovingApplicationPermissions()
    {
        log("Running testAddingAndRemovingApplicationPermissions");

        gotoPage("/console/secure/application/browse.action");

        clickLinkWithText("crowd-openid-server");

        clickLinkWithText("Permissions");

        assertKeyPresent("application.permission.text");

        setWorkingForm("permissionForm");

        String[] selectOptions = getDialog().getSelectOptionValues("directory-select");

        selectOptionByValue("directory-select", selectOptions[0]);

        // Adds
        // Looks like we have to set the form again, being dodgy with the dialog above resets the form
        setWorkingForm("permissionForm");
        checkCheckbox("CREATE_GROUP");
        uncheckCheckbox("CREATE_USER");

        //Modifies
        checkCheckbox("UPDATE_GROUP");
        uncheckCheckbox("UPDATE_USER");

        // Removes
        checkCheckbox("DELETE_GROUP");
        uncheckCheckbox("DELETE_USER");

        submit("update-permissions");

        // Validate the above changes
        setWorkingForm("permissionForm");

        assertCheckboxSelected("CREATE_GROUP");
        assertCheckboxNotSelected("CREATE_USER");

        assertCheckboxSelected("UPDATE_GROUP");
        assertCheckboxNotSelected("UPDATE_USER");

        assertCheckboxSelected("DELETE_GROUP");
        assertCheckboxNotSelected("DELETE_USER");
    }

    public void testSetGlobalPermissionReflectedInApplicationPermissions()
    {
        log("Running testSetGlobalPermissionReflectedInApplicationPermissions");

        gotoPage("/console/secure/directory/browse.action");

        clickLinkWithText("View");

        // Go to the Permissions tab
        clickLinkWithText("Permissions");

        setWorkingForm("permissionForm");

        // Deselect all permission options.
        uncheckCheckbox("permissionGroupAdd");
        uncheckCheckbox("permissionPrincipalAdd");

        uncheckCheckbox("permissionGroupModify");
        uncheckCheckbox("permissionPrincipalModify");

        uncheckCheckbox("permissionGroupRemove");
        uncheckCheckbox("permissionPrincipalRemove");

        submit("update-permissions");

        // Now go to the Application Permission form and check for Global permissions being set.

        gotoPage("/console/secure/application/browse.action");

        clickLinkWithText("crowd-openid-server");

        clickLinkWithText("Permissions");

        setWorkingForm("permissionForm");

        String[] selectOptions = getDialog().getSelectOptionValues("directory-select");

        selectOptionByValue("directory-select", selectOptions[0]);

        String setglobally = getMessage("application.permission.disabledglobally");

        String permissionType = getMessage("permission.addgroup.label");
        assertTextPresent(permissionType + " (" + setglobally + ")");

        permissionType = getMessage("permission.addprincipal.label");
        assertTextPresent(permissionType + " (" + setglobally + ")");

        permissionType = getMessage("permission.modifygroup.label");
        assertTextPresent(permissionType + " (" + setglobally + ")");

        permissionType = getMessage("permission.modifyprincipal.label");
        assertTextPresent(permissionType + " (" + setglobally + ")");

        permissionType = getMessage("permission.removegroup.label");
        assertTextPresent(permissionType + " (" + setglobally + ")");

        permissionType = getMessage("permission.removeprincipal.label");
        assertTextPresent(permissionType + " (" + setglobally + ")");
    }

    public void testPermissionsCancelButton()
    {
        log("Running testPermissionsCancelButton");

        gotoPage("/console/secure/application/browse.action");

        clickLinkWithKey("crowdid.application.name");

        clickLinkWithKey("menu.permissions.label");

        // assert that the current page is the Application Permissions page
        assertKeyPresent("application.permission.text");

        selectOption("directory-select", getText("application.permission.test.directory"));

        // assert the the 'UPDATE_GROUP_ATTRIBUTE' checkbox is checked by default
        assertCheckboxSelected("UPDATE_GROUP_ATTRIBUTE");

        // make a change that should be reversed when cancelled
        uncheckCheckbox("UPDATE_GROUP_ATTRIBUTE");

        clickButtonWithText(getText("cancel.label"));

        // assert that the cancel button returned the page to the default permissions page
        assertKeyPresent("application.permission.text");
        assertKeyPresent("application.permission.directory.none");

        selectOption("directory-select", getText("application.permission.test.directory"));

        // assert that the checkbox has reverted to it's default state and the cancel button worked
        assertCheckboxSelected("UPDATE_GROUP_ATTRIBUTE");
    }
}
