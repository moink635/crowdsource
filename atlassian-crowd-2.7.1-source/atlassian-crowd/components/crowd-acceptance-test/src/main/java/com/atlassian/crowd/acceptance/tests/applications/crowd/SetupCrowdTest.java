package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.util.Arrays;
import java.util.Collections;

/**
 * This test will run in a specific order to make sure we get through the setup process
 * in once piece.
 */
public class SetupCrowdTest extends CrowdAcceptanceTestCase
{
    private static final String USE_DEVELOPER_LICENSE_PROPERTY_KEY = "crowd.developer.license";

    public void testSetup() throws Exception
    {
        setScriptingEnabled(true);

        // about screen should be visible before setup initiated
        _testAboutScreen();

        // test license screen
        _testLicenseScreen();
        _testNoLicense();
        _testInvalidLicense();
        _testInvalidLicenseWithExpiredMaintence();
        _testLicenseSuccess();

        // test installation type screen
        _testInstallScreen();
        _testInstallNew();

        // test database screen
        _testDatabaseScreen();
        _testDatabaseBlank();
        _testDatabaseNothingSelected();
        _testDatabaseDriverClassNotFound();
        _testDatabaseDialectClassNotFound();
        _testDatabaseJavascriptSelection();
        _testDatabaseSelectEmbedded();

        // test options screen
        _testOptionsScreen();
        _testOptionsBlank();
        _testOptionsSessionTimeInvalid();
        _testOptionsBaseURLInvalid();
        _testOptionsBaseURLUnavailable();
        _testOptionsSuccess();

        // test mail server screen
        _testMailServerScreen();
        _testMailServerScreenSSLPort();
        _testMailServerBlank();
        _testMailServerInvalidEmail();
        _testMailServerSuccess();

        // test internal directory screen
        _testInternalDirectoryScreen();
        _testInternalDirectoryBlank();
        _testInternalDirectoryAllErrors();
        _testInternalDirectorySuccess();

        // test default admin screen
        _testDefaultAdminScreen();
        _testDefaultAdminBlank();
        _testDefaultAdminUsernameAndEmailWithTrailingWhitespace();
        _testDefaultAdminEmailAndPwConfirmErrors();
        _testDefaultAdminSuccess();

        // test configure demo app and setup success screens
        _testConfigureIntegratedApps();
        _testCompleteSetup();

        setScriptingEnabled(false);

        _testConfigureIntegratedAppsPresent();

        // about screen should be visible after setup completed
        _testAboutScreen();
    }

    private void _testAboutScreen()
    {
        gotoPage("/about.jsp");
        assertKeyPresent("about.title");
    }

    private void _testLicenseScreen()
    {
        gotoPage("/");
        log("Running _testLicenseScreen");
        assertKeyPresent("license.title");
        assertKeyPresent("license.serverid.label");
    }

    private void _testNoLicense()
    {
        log("Running _testNoLicense");
        submit();

        assertKeyPresent("license.title");
        assertKeyPresent("license.serverid.label");
        assertKeyPresent("license.key.error.required");
    }

    private void _testInvalidLicense()
    {
        log("Running _testInvalidLicense");
        setTextField("key", "fake_license_key");
        submit();

        assertKeyPresent("license.title");
        assertKeyPresent("license.serverid.label");
        assertKeyPresent("license.key.error.invalid");
    }

    private void _testInvalidLicenseWithExpiredMaintence()
    {
        log("Running _testInvalidLicenseWithExpiredMaintence");
        setTextField("key", EXPIRED_MAINTENANCE_KEY);
        submit();

        assertKeyPresent("license.title");
        assertKeyPresent("license.serverid.label");
        assertKeyPresent("license.key.error.maintenance.expired");
    }

    private void _testLicenseSuccess()
    {
        log("Running _testLicenseSuccess");

        if (useDeveloperLicense())
        {
            setTextField("key", DEVELOPER_LICENSE_KEY);
        }
        else
        {
            setTextField("key", UNLIMITED_LICENSE_KEY);
        }

        // Submit on the first page
        submit();

        assertKeyNotPresent("license.title");
    }

    public static boolean useDeveloperLicense()
    {
        return Boolean.parseBoolean(System.getProperty(USE_DEVELOPER_LICENSE_PROPERTY_KEY));
    }

    private void _testInstallScreen()
    {
        log("Running _testInstallScreen");

        assertKeyPresent("install.title");
        assertKeyPresent("install.new.label");
        assertKeyPresent("install.upgrade.xml.label");
        assertKeyNotPresent("install.upgrade.db.label"); // 2.0 doesn't allow this
    }

    private void _testInstallNew()
    {
        log("Running _testInstallNew");
        clickRadioOption("installOption", "install.new");
        submit();

        assertKeyNotPresent("install.title");
    }

    private void _testDatabaseScreen()
    {
        log("Running _testDatabaseScreen");

        assertKeyPresent("database.title");
        assertKeyPresent("database.embedded.label");
        assertKeyPresent("database.jdbc.label");
        assertKeyPresent("database.datasource.label");
    }

    private void _testDatabaseNothingSelected()
    {
        log("Running _testDatabaseNothingSelected");

        // JDBC Connection
        clickRadioOption("databaseOption", "db.jdbc");
        selectOptionByValue("jdbcDatabaseType", "other");

        submit();

        assertKeyPresent("database.title");
        assertKeyPresent("database.driver.blank");
        assertKeyPresent("database.jdbc.url.blank");
        assertKeyPresent("database.username.blank");
        assertKeyPresent("database.dialect.blank");

        // JNDI Datasource
        clickRadioOption("databaseOption", "db.datasource");
        selectOptionByValue("datasourceDatabaseType", "other");

        submit();


        assertKeyPresent("database.title");
        assertKeyPresent("database.jndi.blank");
        assertKeyPresent("database.dialect.blank");
    }

    private void _testDatabaseBlank()
    {
        log("Running _testDatabaseBlank");

        clickRadioOption("databaseOption", "db.jdbc");

        submit();

        assertKeyPresent("database.title");
        assertKeyPresent("database.select.blank");

        clickRadioOption("databaseOption", "db.datasource");

        submit();

        assertKeyPresent("database.title");
        assertKeyPresent("database.select.blank");
    }

    private void _testDatabaseDriverClassNotFound()
    {
        log("Running _testDatabaseDriverClassNotFound");
        clickRadioOption("databaseOption", "db.jdbc");
        setTextField("jdbcDriverClassName", "some.non.existent.Driver");
        submit();

        assertKeyPresent("database.title");
        assertKeyPresent("database.driver.notfound");
    }

    private void _testDatabaseDialectClassNotFound()
    {
        log("Running _testDatabaseDialectClassNotFound");
        clickRadioOption("databaseOption", "db.jdbc");
        setTextField("jdbcDriverClassName", "some.non.existent.Dialect");
        submit();

        assertKeyPresent("database.title");
        assertKeyPresent("database.dialect.notfound");

        clickRadioOption("databaseOption", "db.datasource");
        setTextField("jdbcHibernateDialect", "some.non.existent.Dialect");
        submit();

        assertKeyPresent("database.title");
        assertKeyNotPresent("database.dialect.notfound");

        clickRadioOption("databaseOption", "db.datasource");
        setTextField("datasourceHibernateDialect", "some.non.existent.Dialect");
        submit();

        assertKeyPresent("database.title");
        assertKeyPresent("database.dialect.notfound");
    }

    private void _testDatabaseJavascriptSelection()
    {
        log("Running _testDatabaseJavascriptSelection");
        clickRadioOption("databaseOption", "db.jdbc");
        selectOptionByValue("jdbcDatabaseType", "postgresql");
        submit();

        assertKeyPresent("database.title");
        assertTextFieldEquals("jdbcDriverClassName", "org.postgresql.Driver");
        assertTextFieldEquals("jdbcUrl", "jdbc:postgresql://localhost:5432/crowd");
        assertTextFieldEquals("jdbcHibernateDialect", "org.hibernate.dialect.PostgreSQLDialect");

        clickRadioOption("databaseOption", "db.datasource");
        selectOptionByValue("datasourceDatabaseType", "mysql");
        submit();

        assertKeyPresent("database.title");
        assertTextFieldEquals("datasourceHibernateDialect", "org.hibernate.dialect.MySQL5InnoDBDialect");
    }

    private void _testDatabaseSelectEmbedded()
    {
        log("Running _testDatabaseSelectEmbedded");
        clickRadioOption("databaseOption", "db.embedded");
        submit();

        assertKeyNotPresent("database.title");
    }


    private void _testOptionsScreen()
    {
        log("Running _testOptionsScreen");

        assertKeyPresent("options.title");
        assertKeyPresent("options.title.label");
        assertKeyPresent("session.sessiontime.label");
    }

    private void _testOptionsBlank()
    {
        log("Running _testOptionsBlank");
        setTextField("title", "");
        setTextField("sessionTime", "");
        setTextField("baseURL", "");
        submit();

        assertKeyPresent("options.title.invalid");
        assertKeyPresent("options.base.url.invalid", Collections.singletonList(HOST_PATH));
        assertKeyPresent("session.sessiontime.invalid");
    }

    private void _testOptionsSessionTimeInvalid()
    {
        log("Running _testOptionsSessionTimeInvalid");
        // negative session time
        setTextField("title", "Test Deployment");
        setTextField("sessionTime", "-1");
        submit();

        assertKeyNotPresent("options.title.invalid");
        assertKeyPresent("session.sessiontime.invalid");

        // non-numeric session time
        setTextField("title", "Test Deployment");
        setTextField("sessionTime", "-muhahaha!");
        submit();

        assertKeyNotPresent("options.title.invalid");
        assertKeyPresent("session.sessiontime.invalid");
    }

    private void _testOptionsBaseURLInvalid()
    {
        log("Running _testOptionsbaseURLInvalid");
        // negative session time
        setTextField("title", "Test Deployment");
        setTextField("sessionTime", "20");
        setTextField("baseURL", HOST_PATH + "\\\\");
        submit();

        assertKeyNotPresent("options.title.invalid");
        assertKeyNotPresent("session.sessiontime.invalid");
        assertKeyPresent("options.base.url.invalid", Collections.singletonList(HOST_PATH));

        // non-numeric session time
        setTextField("title", "Test Deployment");
        setTextField("sessionTime", "-muhahaha!");
        submit();

        assertKeyNotPresent("options.title.invalid");
        assertKeyPresent("session.sessiontime.invalid");
    }

    private void _testOptionsBaseURLUnavailable()
    {
        log("Running _testOptionsbaseURLInvalid");
        // negative session time
        setTextField("title", "Test Deployment");
        setTextField("sessionTime", "20");
        setTextField("baseURL", HOST_PATH+"/not-there");
        submit();

        assertKeyNotPresent("options.title.invalid");
        assertKeyNotPresent("session.sessiontime.invalid");
        assertKeyPresent("options.base.url.unavailable", Arrays.asList("404", HOST_PATH));

        // non-numeric session time
        setTextField("title", "Test Deployment");
        setTextField("sessionTime", "-muhahaha!");
        submit();

        assertKeyNotPresent("options.title.invalid");
        assertKeyPresent("session.sessiontime.invalid");
    }

    private void _testOptionsSuccess()
    {
        log("Running _testOptionsSuccess");
        setTextField("title", "Test Deployment");
        setTextField("sessionTime", "20");
        setTextField("baseURL", HOST_PATH);
        submit();

        assertKeyNotPresent("options.title");
    }

    private void _testMailServerScreen()
    {
        log("Running _testMailServerScreen");
        assertKeyPresent("mailserver.title");
        assertKeyPresent("mailserver.postpone.question.label");

        // by default, skip mail configuration
        assertKeyPresent("mailserver.postpone.during.setup1");
        assertKeyPresent("mailserver.postpone.during.setup2");
        assertKeyNotPresent("mailserver.notification.label");
        assertKeyNotPresent("mailserver.from.label");
        assertKeyNotPresent("mailserver.prefix.label");
        assertKeyNotPresent("mailserver.host.label");
        assertKeyNotPresent("mailserver.jndilocation.label");

        clickRadioOption("postponeConfiguration", "false");

        assertKeyNotPresent("mailserver.postpone.during.setup1");
        assertKeyNotPresent("mailserver.postpone.during.setup2");
        assertKeyPresent("mailserver.notification.label");
        assertKeyPresent("mailserver.from.label");
        assertKeyPresent("mailserver.prefix.label");
        assertKeyPresent("mailserver.host.label");
        assertKeyPresent("mailserver.username.label");
        assertKeyPresent("mailserver.password.label");
    }

    private void _testMailServerScreenSSLPort()
    {
        log("Running _testMailServerScreenSSLPort");

        checkCheckbox("useSSL");

        assertTextInElement("port", "465");

        uncheckCheckbox("useSSL");

        assertTextInElement("port", "25");
    }

    private void _testMailServerBlank()
    {
        log("Running _testMailServerBlank");

        submit();

        assertKeyPresent("mailserver.notification.invalid");
        assertKeyPresent("mailserver.from.invalid");
        assertKeyPresent("mailserver.host.invalid");

        clickRadioOption("jndiMailActive", "true");

        assertRadioOptionSelected("jndiMailActive", "true");

        submit();

        assertKeyPresent("mailserver.jndiLocation.invalid");
    }

    private void _testMailServerInvalidEmail()
    {
        log("Running _testMailServerInvalidEmail");

        setTextField("notificationEmail", "invalid");
        setTextField("host", "localhost");
        setTextField("from", "invalid");

        submit();

        assertKeyPresent("mailserver.notification.invalid");
        assertKeyPresent("mailserver.from.invalid");
        assertKeyNotPresent("mailserver.host.invalid");
    }

    private void _testMailServerSuccess()
    {
        log("Running _testMailServerSuccess");

        clickRadioOption("jndiMailActive", "false");

        setTextField("notificationEmail", "user@example.com");
        setTextField("host", "localhost");
        setTextField("from", "security@example.com");

        submit();

        assertKeyNotPresent("mailserver.notification.label");
    }

    private void _testInternalDirectoryScreen()
    {
        log("Running _testInternalDirectoryScreen");
        assertSelectedOptionEquals("userEncryptionMethod", "ATLASSIAN-SECURITY");
        assertKeyPresent("directoryinternal.title");
        assertKeyPresent("directoryinternal.name.label");
        assertKeyPresent("directoryinternal.description.label");
        assertKeyPresent("directoryinternal.passwordmaxchangetime.label");
        assertKeyPresent("directoryinternal.passwordmaxattempts.label");
        assertKeyPresent("directoryinternal.passwordhistorycount.label");
        assertKeyPresent("directoryinternal.passwordregex.label");
        assertKeyPresent("directoryinternal.passwordcomplexity.help.label");
        assertKeyPresent("directoryconnector.userencryptionmethod.label");
    }

    private void _testInternalDirectoryBlank()
    {
        log("Running _testInternalDirectoryBlank");
        setTextField("name", "");
        setTextField("description", "");
        setTextField("passwordRegex", "");
        setTextField("passwordComplexityMessage", "");
        setTextField("passwordMaxAttempts", "");
        setTextField("passwordMaxChangeTime", "");
        setTextField("passwordHistoryCount", "");
        submit();

        // Required Fields
        assertKeyPresent("directoryinternal.name.invalid");

        // Unrequired Fields
        assertKeyNotPresent("directoryinternal.passwordmaxchangetime.invalid");
        assertKeyNotPresent("directoryinternal.passwordmaxattempts.invalid");
        assertKeyNotPresent("directoryinternal.passwordhistorycount.invalid");
        assertKeyNotPresent("directoryinternal.passwordregex.invalid");
    }

    private void _testInternalDirectoryAllErrors()
    {
        log("Running _testInternalDirectoryAllErrors");
        setTextField("name", "");
        setTextField("description", "blah");
        setTextField("passwordRegex", "?");
        setTextField("passwordMaxAttempts", "-1");
        setTextField("passwordMaxChangeTime", "-1");
        setTextField("passwordHistoryCount", "-1");
        submit();

        assertKeyPresent("directoryinternal.name.invalid");
        assertKeyPresent("directoryinternal.passwordmaxchangetime.invalid");
        assertKeyPresent("directoryinternal.passwordmaxattempts.invalid");
        assertKeyPresent("directoryinternal.passwordhistorycount.invalid");
        assertTextPresent("Dangling meta"); // regex specific error
    }

    private void _testInternalDirectorySuccess()
    {
        log("Running _testInternalDirectorySuccess");
        setTextField("name", "Test Internal Directory");
        setTextField("description", "");
        setTextField("passwordRegex", "");
        setTextField("passwordComplexityMessage", "");
        setTextField("passwordMaxAttempts", "");
        setTextField("passwordMaxChangeTime", "");
        setTextField("passwordHistoryCount", "");
        selectOptionByValue("userEncryptionMethod", "atlassian-security");
        submit();

        assertKeyNotPresent("directoryinternal.title");
    }

    private void _testDefaultAdminScreen()
    {
        log("Running _testDefaultAdminScreen");
        assertKeyPresent("defaultadmin.title");
        assertKeyPresent("principal.name.label");
        assertKeyPresent("principal.password.label");
        assertKeyPresent("principal.passwordconfirm.label");
        assertKeyPresent("principal.firstname.label");
        assertKeyPresent("principal.lastname.label");
        assertKeyPresent("principal.email.label");
    }

    private void _testDefaultAdminBlank()
    {
        log("Running _testDefaultAdminBlank");
        setTextField("name", "");
        setTextField("email", "");
        setTextField("firstname", "");
        setTextField("lastname", "");
        submit();

        assertKeyPresent("principal.name.invalid");
        assertKeyPresent("principal.password.invalid");
        assertKeyPresent("principal.firstname.invalid");
        assertKeyPresent("principal.lastname.invalid");
        assertKeyPresent("principal.email.invalid");
    }

    private void _testDefaultAdminUsernameAndEmailWithTrailingWhitespace()
    {
        log("Running _testDefaultAdminUsernameAndEmailWithTrailingWhitespace");
        setTextField("email", " john@example.test ");
        setTextField("name", " admin ");
        submit();

        assertKeyPresent("invalid.whitespace");
        assertKeyPresent("principal.email.whitespace");
    }

    private void _testDefaultAdminEmailAndPwConfirmErrors()
    {
        log("Running _testDefaultAdminEmailAndPwConfirmErrors");
        setTextField("email", "fake");
        setTextField("name", "admin");
        setTextField("password", "admin");
        setTextField("passwordConfirm", "not same");
        setTextField("firstname", "Super");
        setTextField("lastname", "User");

        submit();

        assertKeyPresent("principal.passwordconfirm.nomatch");
        assertKeyPresent("principal.email.invalid");
    }

    private void _testDefaultAdminSuccess()
    {
        log("Running _testDefaultAdminSuccess");
        setTextField("email", "admin@example.com");
        setTextField("name", "admin");
        setTextField("password", "admin");
        setTextField("passwordConfirm", "admin");
        setTextField("firstname", "Super");
        setTextField("lastname", "User");

        submit();

        assertKeyNotPresent("defaultadmin.title");
    }

    private void _testConfigureIntegratedApps()
    {
        log("Running _testConfigureIntegratedApps");
        assertKeyPresent("integration.title");

        // disable the demo app, we will test later if it does not show up
        setRadioButton("configureOpenIDServer", Boolean.TRUE.toString());
        setRadioButton("configureDemoApp", Boolean.FALSE.toString());

        submit();
    }

    private void _testCompleteSetup()
    {
        log("Running _testCompleteSetup");
        assertKeyPresent("login.title");
        assertKeyPresent("setupcomplete.text", new String[] {"admin"});
    }

    private void _testConfigureIntegratedAppsPresent()
    {
        log("Running _testConfigureIntegratedAppsPresent");

        _loginAdminUser();

        gotoBrowseApplications();

        assertTextNotPresent("demo");
        assertTextPresent("crowd-openid-server");

        // also assert bundled plugin-applications present
        assertTextPresent("google-apps");
    }
}
