package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.crowd.acceptance.tests.applications.crowd.user.CrowdUserConsoleAcceptenceTestCase;

public class ChangeExpiredPasswordTest extends CrowdUserConsoleAcceptenceTestCase
{

    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        loadXmlOnSetUp("changepasswordfiltertest.xml");
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testViewChangePasswordWithPolicy()
    {
        log("Running testViewChangePasswordWithPolicy");

        _logout();

        _loginExpiredCredentialUser("joe", "joe");

        // We should be thrown to the changepassword page

        assertKeyPresent("menu.user.console.changepassword.label");
        assertKeyPresent("principal.password.complexity.policy");
        assertTextPresent("Passwords <b>must</b> contain at least one lowercase character");  // escaped

        assertKeyPresent("error.changepassword.required");
        assertWarningPresent(); // your password has expired!

        assertTextInElement("username", "joe");

        assertTextFieldEquals("originalPassword", "");
        assertTextFieldEquals("password", "");
        assertTextFieldEquals("confirmPassword", "");
    }

    public void testChangePasswordSuccessful()
    {
        intendToModifyData();

        log("Running testChangePasswordSuccessful");

        _logout();

        _loginExpiredCredentialUser("joe", "joe");

        // We should be thrown to the changepassword page

        assertKeyPresent("menu.user.console.changepassword.label");
        assertTextInElement("username", "joe");

        setTextField("originalPassword", "joe");
        setTextField("password", "joesmith");
        setTextField("confirmPassword", "joesmith");

        submit();

        // Assert the update was successful
        assertKeyPresent("passwordupdate.message");

        _loginTestUser("joe", "joesmith");

        assertKeyPresent("menu.profile.label");
    }

    public void testChangePasswordOldPasswordBlank()
    {
        log("Running testChangePasswordOldPasswordBlank");

        _logout();

        _loginExpiredCredentialUser("joe", "joe");

        // We should be thrown to the changepassword page

        assertKeyPresent("menu.user.console.changepassword.label");
        assertTextInElement("username", "joe");

        // original password is not entered
        setTextField("password", "joesmith");
        setTextField("confirmPassword", "joesmith");

        submit();

        // Assert the update was unsuccessful & on same page
        assertKeyPresent("password.invalid");
        assertKeyPresent("menu.user.console.changepassword.label");
    }

    public void testChangePasswordNewPasswordBlank()
    {
        log("Running testChangePasswordNewPasswordBlank");

        _logout();

        _loginExpiredCredentialUser("joe", "joe");

        // We should be thrown to the changepassword page

        assertKeyPresent("menu.user.console.changepassword.label");
        assertTextInElement("username", "joe");

        setTextField("originalPassword", "joe");
        // new passwords not entered

        submit();

        // Assert the update was unsuccessful & on same page
        assertKeyPresent("menu.user.console.changepassword.label");
        assertKeyPresent("passwordempty.invalid");
    }

    public void testChangePasswordNewPasswordDoesNotMatchDirectoryComplexityRequirements()
    {
        _logout();

        _loginExpiredCredentialUser("joe", "joe");

        // We should be thrown to the changepassword page

        assertKeyPresent("menu.user.console.changepassword.label");
        assertTextInElement("username", "joe");

        setTextField("originalPassword", "joe");
        setTextField("password", "PASSWORD-WITHOUT-LOWERCASE");
        setTextField("confirmPassword", "PASSWORD-WITHOUT-LOWERCASE");

        submit();

        // Assert the update was unsuccessful & on same page
        assertKeyPresent("menu.user.console.changepassword.label");
        assertKeyPresent("passwordupdate.policy.error.message");
        assertTextPresent("Passwords <b>must</b> contain at least one lowercase character");  // escaped
    }

    public void testChangePasswordNewPasswordMismatch()
    {
        log("Running testChangePasswordNewPasswordMismatch");

        _logout();

        _loginExpiredCredentialUser("joe", "joe");

        // We should be thrown to the changepassword page

        assertKeyPresent("menu.user.console.changepassword.label");
        assertTextInElement("username", "joe");

        setTextField("originalPassword", "joe");
        setTextField("password", "new");
        setTextField("confirmPassword", "somethingelse");
        submit();

        // Assert the update was unsuccessful & on same page
        assertKeyPresent("menu.user.console.changepassword.label");
        assertKeyPresent("passworddonotmatch.invalid");
    }

    public void testChangePasswordOldPasswordWrong()
    {
        log("Running testChangePasswordOldPasswordWrong");

        _logout();

        _loginExpiredCredentialUser("joe", "joe");

        // We should be thrown to the changepassword page

        assertKeyPresent("menu.user.console.changepassword.label");
        assertTextInElement("username", "joe");

        setTextField("originalPassword", "wrong");
        setTextField("password", "new");
        setTextField("confirmPassword", "new");
        submit();

        // Assert the update was unsuccessful & on same page
        assertKeyPresent("menu.user.console.changepassword.label");
        assertKeyPresent("password.invalid");
    }


    public void testChangePasswordAfterFailedAttempt()
    {
        intendToModifyData();

        log("Running testChangePasswordAfterFailedAttempt");

        _logout();

        _loginExpiredCredentialUser("joe", "joe");

        // We should be thrown to the changepassword page

        assertKeyPresent("menu.user.console.changepassword.label");
        assertTextInElement("username", "joe");

        setTextField("originalPassword", "wrong");
        setTextField("password", "new");
        setTextField("confirmPassword", "new");
        submit();

        // Assert the update was unsuccessful & on same page
        assertKeyPresent("menu.user.console.changepassword.label");
        assertKeyPresent("password.invalid");

        // now with the correct password
        setTextField("originalPassword", "joe");
        setTextField("password", "new");
        setTextField("confirmPassword", "new");

        submit();

        // Assert the update was successful
        assertKeyPresent("passwordupdate.message");

        _loginTestUser("joe", "new");

        assertKeyPresent("menu.profile.label");
    }
}
