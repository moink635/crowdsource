package com.atlassian.crowd.acceptance.tests.persistence.dao.group;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.embedded.spi.GroupDao;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.GroupQuery;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.search.query.entity.restriction.PropertyUtils;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.atlassian.crowd.model.group.Groups.namesOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Integration tests for the search operations of {@link GroupDao}. For convenience, the tests
 * of this interface have been split into multiple test classes.
 *
 * @see GroupDaoCRUDTest
 * @see GroupDAOHibernateTest
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class GroupDaoSearchTest
{
    private static final long DIRECTORY_ID = 1L;
    private static final String EXISTING_GROUP_1 = "administratorS";
    private static final String EXISTING_GROUP_2 = "users";
    private static final String EXISTING_GROUP_3 = "developers";
    private static final String EXISTING_GROUP_4 = "Managers";
    private static final String COUNTRY_ATTRIBUTE = "country";
    private static final String CITY_ATTRIBUTE = "city";
    private static final String LANGUAGE_ATTRIBUTE = "language";
    private static final String COUNTRY_VALUE_NZ = "New Zealand";
    private static final String COUNTRY_VALUE_AU = "Australia";
    private static final String COUNTRY_VALUE_CN = "China";
    private static final String CITY_VALUE_SYD = "Sydney";

    @Inject private GroupDao groupDAO;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    // needed because some tests modify the dataset
    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Test
    public void testSearchAllGroupNames()
    {
        EntityQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).returningAtMost(10);

        List<String> groups = groupDAO.search(DIRECTORY_ID, query);

        assertEquals(Arrays.asList(EXISTING_GROUP_1, EXISTING_GROUP_3, EXISTING_GROUP_4), groups);
    }

    @Test
    public void testSearchGroupNameStartsWith()
    {
        PropertyRestriction<String>
            groupNameRestriction = new TermRestriction<String>(GroupTermKeys.NAME, MatchMode.STARTS_WITH, "a");
        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, groupNameRestriction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), contains(EXISTING_GROUP_1));
    }

    @Test
    public void testSearchGroupTypeRole()
    {
        EntityQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.LEGACY_ROLE)).returningAtMost(EntityQuery.ALL_RESULTS);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), contains(EXISTING_GROUP_2));
    }

    @Test
    public void testSearchGroupTypeGroup()
    {
        EntityQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP)).returningAtMost(EntityQuery.ALL_RESULTS);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), containsInAnyOrder(EXISTING_GROUP_1, EXISTING_GROUP_3, EXISTING_GROUP_4));
    }

    @Test
    public void testSearchForActiveGroupsUsingBuilder()
    {
        PropertyRestriction<Boolean> booleanRestriction = Restriction.on(GroupTermKeys.ACTIVE).exactlyMatching(true);
        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, booleanRestriction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), contains(EXISTING_GROUP_1));
    }

    @Test
    public void testSearchForInactiveGroupsUsingBuilder()
    {
        PropertyRestriction<Boolean> booleanRestriction = Restriction.on(GroupTermKeys.ACTIVE).exactlyMatching(false);
        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, booleanRestriction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), containsInAnyOrder(EXISTING_GROUP_3, EXISTING_GROUP_4));
    }

    @Test
    public void testSearchCountryExact()
    {
        PropertyRestriction<String> groupNameRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString(
            COUNTRY_ATTRIBUTE), MatchMode.EXACTLY_MATCHES, COUNTRY_VALUE_AU);
        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, groupNameRestriction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), contains(EXISTING_GROUP_1));
    }

    @Test
    public void testSearchCountryNoResults()
    {
        PropertyRestriction<String> groupNameRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString(COUNTRY_ATTRIBUTE), MatchMode.EXACTLY_MATCHES, "canada");
        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, groupNameRestriction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertEquals(0, groups.size());
    }

    @Test
    public void testSearchNullAttribute()
    {
        PropertyRestriction<String> groupAttributeRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString(LANGUAGE_ATTRIBUTE), MatchMode.NULL, null);

        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, groupAttributeRestriction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertEquals(2, groups.size());
    }

    @Test
    public void testSearchFirstClassDisjunction()
    {
        PropertyRestriction<String> groupNameRestriction1 = new TermRestriction<String>(GroupTermKeys.NAME, MatchMode.STARTS_WITH, "a");
        PropertyRestriction<String> groupNameRestriction2 = new TermRestriction<String>(GroupTermKeys.NAME, MatchMode.STARTS_WITH, "u");
        BooleanRestriction groupNameDisjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, groupNameRestriction1, groupNameRestriction2);

        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, groupNameDisjunction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), contains(EXISTING_GROUP_1));
    }

    @Test
    public void testSearchFirstClassConjunction()
    {
        PropertyRestriction<String> groupNameRestriction = new TermRestriction<String>(GroupTermKeys.NAME, MatchMode.STARTS_WITH, "a");
        PropertyRestriction<Boolean> activeRestriction = new TermRestriction<Boolean>(GroupTermKeys.ACTIVE, true);
        BooleanRestriction groupNameDisjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, groupNameRestriction, activeRestriction);

        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, groupNameDisjunction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), contains(EXISTING_GROUP_1));
    }

    @Test
    public void testSearchSecondClassDisjunction()
    {
        PropertyRestriction<String> countryRestriction1 = new TermRestriction<String>(PropertyUtils.ofTypeString(COUNTRY_ATTRIBUTE), MatchMode.EXACTLY_MATCHES, COUNTRY_VALUE_AU);
        PropertyRestriction<String> countryRestriction2 = new TermRestriction<String>(PropertyUtils.ofTypeString(CITY_ATTRIBUTE), MatchMode.EXACTLY_MATCHES, CITY_VALUE_SYD);
        BooleanRestriction groupNameDisjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, countryRestriction1, countryRestriction2);

        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, groupNameDisjunction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), containsInAnyOrder(EXISTING_GROUP_1, EXISTING_GROUP_3));
    }

    @Test
    public void testSearchSecondClassConjunction()
    {
        PropertyRestriction<String> countryRestriction1 = new TermRestriction<String>(PropertyUtils.ofTypeString(COUNTRY_ATTRIBUTE), MatchMode.EXACTLY_MATCHES, COUNTRY_VALUE_AU);
        PropertyRestriction<String> countryRestriction2 = new TermRestriction<String>(PropertyUtils.ofTypeString(COUNTRY_ATTRIBUTE), MatchMode.EXACTLY_MATCHES, COUNTRY_VALUE_CN);
        BooleanRestriction groupNameDisjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, countryRestriction1, countryRestriction2);

        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.LEGACY_ROLE, groupNameDisjunction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), contains(EXISTING_GROUP_2));
    }

    @Test
    public void testSearchMixedClassDisjunction()
    {
        PropertyRestriction<String> countryRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString(COUNTRY_ATTRIBUTE), MatchMode.EXACTLY_MATCHES, COUNTRY_VALUE_AU);
        PropertyRestriction<Boolean> activeRestriction = new TermRestriction<Boolean>(GroupTermKeys.ACTIVE, false);
        BooleanRestriction groupNameDisjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, countryRestriction, activeRestriction);

        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, groupNameDisjunction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), containsInAnyOrder(EXISTING_GROUP_1, EXISTING_GROUP_3, EXISTING_GROUP_4));
    }

    @Test
    public void testSearchMixedClassConjunction()
    {
        PropertyRestriction<String> cityRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString(CITY_ATTRIBUTE), MatchMode.EXACTLY_MATCHES, CITY_VALUE_SYD);
        PropertyRestriction<Boolean> activeRestriction = new TermRestriction<Boolean>(GroupTermKeys.ACTIVE, true);
        BooleanRestriction groupNameConjuction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, cityRestriction, activeRestriction);

        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.LEGACY_ROLE, groupNameConjuction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), contains(EXISTING_GROUP_2));
    }

    @Test
    public void testSearchByDescription()
    {
        // description is case-sensitive
        PropertyRestriction<String> groupRestriction =
            new TermRestriction<String>(GroupTermKeys.DESCRIPTION, MatchMode.EXACTLY_MATCHES, "The Crowd Crew");

        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, groupRestriction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), contains(EXISTING_GROUP_3));
    }

    @Test
    public void testSearchByDescriptionWithNoMatches()
    {
        // description is case-sensitive
        PropertyRestriction<String> groupRestriction =
            new TermRestriction<String>(GroupTermKeys.DESCRIPTION, MatchMode.EXACTLY_MATCHES,
                                        "No group has this description");

        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, groupRestriction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(groups, IsEmptyCollection.empty());
    }

    @Test
    public void testSearchNested()
    {
        PropertyRestriction<String> cityRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString(CITY_ATTRIBUTE), MatchMode.EXACTLY_MATCHES, CITY_VALUE_SYD);
        PropertyRestriction<Boolean> activeRestriction = new TermRestriction<Boolean>(GroupTermKeys.ACTIVE, true);
        BooleanRestriction conjunction1 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, cityRestriction, activeRestriction);

        PropertyRestriction<String> countryRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString(COUNTRY_ATTRIBUTE), MatchMode.EXACTLY_MATCHES, COUNTRY_VALUE_NZ);
        PropertyRestriction<String> groupNameRestriction = new TermRestriction<String>(GroupTermKeys.NAME, MatchMode.STARTS_WITH, "a");
        BooleanRestriction conjunction2 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, countryRestriction, groupNameRestriction);

        BooleanRestriction nestedRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, conjunction1, conjunction2);

        GroupQuery<Group> query = new GroupQuery<Group>(Group.class, GroupType.GROUP, nestedRestriction, 0, 100);

        List<Group> groups = groupDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(groups), contains(EXISTING_GROUP_1));
    }

    @Test
    public void testSearchByMixedName()
    {
        List<Group> results = groupDAO.search(2L, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).with(Restriction.on(GroupTermKeys.NAME).startingWith("t")).returningAtMost(10));
        assertThat(namesOf(results), contains("Testers"));
    }

    @Test
    public void testSearchGroupsWithLocalTermDoesNotFail()
    {
        EntityQuery<InternalGroup> query =
            QueryBuilder.queryFor(InternalGroup.class, EntityDescriptor.group()).with(Restriction.on(GroupTermKeys.LOCAL).exactlyMatching(true)).returningAtMost(EntityQuery.ALL_RESULTS);

        List<InternalGroup> results = groupDAO.search(2L, query);
        assertEquals(Collections.emptyList(), results);
    }

    @Test
    public void testSearchGroupsWithLocalTermReturnsExpectedResults() throws Exception
    {
        groupDAO.addLocal(new GroupTemplate("local-group", DIRECTORY_ID));
        groupDAO.add(new GroupTemplate("not-local-group", DIRECTORY_ID));

        EntityQuery<InternalGroup> query;
        List<InternalGroup> results;

        /* Local */
        query = QueryBuilder.queryFor(InternalGroup.class, EntityDescriptor.group()).with(Restriction.on(GroupTermKeys.LOCAL).exactlyMatching(true)).returningAtMost(EntityQuery.ALL_RESULTS);
        results = groupDAO.search(DIRECTORY_ID, query);
        assertEquals(1, results.size());
        assertTrue(results.get(0).isLocal());
        assertEquals("local-group", results.get(0).getName());

        /* Not local */
        query = QueryBuilder.queryFor(InternalGroup.class, EntityDescriptor.group()).with(Restriction.on(GroupTermKeys.LOCAL).exactlyMatching(false)).returningAtMost(EntityQuery.ALL_RESULTS);
        results = groupDAO.search(DIRECTORY_ID, query);
        Assert.assertThat((Iterable<InternalGroup>) results,
                          CoreMatchers.<InternalGroup>hasItem(new TypeSafeMatcher<InternalGroup>()
                          {
                              @Override
                              public void describeTo(Description description)
                              {
                                  description.appendText("a non-local group called 'not-local-group'");
                              }

                              public boolean matchesSafely(InternalGroup item)
                              {
                                  return !item.isLocal() && item.getName().equals("not-local-group");
                              }
                          }));
    }
}
