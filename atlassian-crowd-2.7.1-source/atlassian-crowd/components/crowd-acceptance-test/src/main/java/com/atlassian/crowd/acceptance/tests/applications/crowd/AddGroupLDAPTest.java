package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.crowd.acceptance.tests.directory.BaseTest;
import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.ReadOnlyGroupException;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;

import java.util.Properties;

/**
 * Web acceptance test for the adding of a Group
 */
public class AddGroupLDAPTest extends CrowdAcceptanceTestCase
{
    private static final String DIRECTORY_NAME = "ApacheDS154";

    private static final String GROUP_NAME = "myGroup";
    private static final String SUB_GROUP_NAME = "mySubGroup";

    LDAPLoader loader;

    /**
     * Allows us to add users to our ApacheDS instance.
     */
    class LDAPLoader extends BaseTest
    {
        LDAPLoader()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
        }

        @Override
        protected void configureDirectory(Properties directorySettings)
        {
            super.configureDirectory(directorySettings);
            directory.setAttribute(LDAPPropertiesMapper.LDAP_BASEDN_KEY, directorySettings.getProperty("test.integration.basedn"));
        }

        @Override
        protected void loadTestData() throws Exception
        {
            GroupTemplate subGroup = new GroupTemplate(SUB_GROUP_NAME, directory.getId(), GroupType.GROUP);     // members: SUB_USER_NAME
            getRemoteDirectory().addGroup(subGroup);
        }

        @Override
        protected void removeTestData() throws DirectoryInstantiationException
        {
            DirectoryTestHelper.silentlyRemoveGroup(GROUP_NAME, getRemoteDirectory());
            DirectoryTestHelper.silentlyRemoveGroup(SUB_GROUP_NAME, getRemoteDirectory());
        }

        public void accessibleSetUp() throws Exception
        {
            super.setUp();
        }

        public void accessibleTearDown() throws Exception
        {
            super.tearDown();
        }

    }

    public AddGroupLDAPTest()
    {
        loader = new LDAPLoader();
    }

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        loader.accessibleSetUp();
        restoreCrowdFromXML("viewgrouptest.xml");
    }

    @Override
    public void tearDown() throws Exception
    {
        super.tearDown();
        loader.accessibleTearDown();
    }

    public void testAddGroupToLDAPDirectory()
    {
        log("Running testAddGroupToLDAPDirectory");
        gotoAddGroup();

        setTextField("name", GROUP_NAME);
        setTextField("description", "Crowd LDAP Test Group");
        selectOption("directoryID", DIRECTORY_NAME);

        submit();

        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent(GROUP_NAME);
    }

    public void testAddRemoteGroupTwice()
    {
        log("Running testAddRemoteGroupTwice");
        intendToModifyLdapData();

        // add the group
        final String groupName = "Test-RemoteGroup";
        gotoAddGroup();
        setTextField("name", groupName);
        selectOption("directoryID", DIRECTORY_NAME);
        submit();

        // first attempt succeeds
        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent(groupName);
        assertKeyPresent("group.location.remote");

        // try to add the group again
        gotoAddGroup();
        setTextField("name", groupName);
        selectOption("directoryID", DIRECTORY_NAME);
        submit();

        // second attempt must show an error
        assertErrorPresentWithKey("invalid.namealreadyexist");
        assertKeyPresent("menu.addgroup.label");
        assertKeyNotPresent("menu.viewgroup.label");

        // remove the remote group
        gotoBrowseGroups();
        clickLinkWithExactText(groupName);
        clickLink("remove-group");
        submit();
        assertKeyPresent("updatesuccessful.label");
        assertTextNotInTable("group-details", groupName);
    }

    public void testAddLocalGroupTwice()
    {
        log("Running testAddLocalGroupTwice");
        intendToModifyData();

        enableLocalGroups();

        // add the group
        gotoAddGroup();
        final String groupName = "Test-LocalGroup";
        setTextField("name", groupName);
        selectOption("directoryID", DIRECTORY_NAME);
        submit();

        // first attempt succeeds
        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent(groupName);
        assertKeyPresent("group.location.local");

        // try to add the group again
        gotoAddGroup();
        setTextField("name", groupName);
        selectOption("directoryID", DIRECTORY_NAME);
        submit();

        // second attempt must show an error
        assertErrorPresentWithKey("invalid.namealreadyexist");
        assertKeyPresent("menu.addgroup.label");
        assertKeyNotPresent("menu.viewgroup.label");
    }

    public void testAddLocalGroupWithTheSameNameAsRemoteGroup()
    {
        log("Running testAddLocalGroupTwice");
        intendToModifyData();
        intendToModifyLdapData();

        // add the remote group
        final String groupName = "Test-LocalAndRemoteGroup";
        gotoAddGroup();
        setTextField("name", groupName);
        selectOption("directoryID", DIRECTORY_NAME);
        submit();

        // the remote group can be added successfully
        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent(groupName);
        assertKeyPresent("group.location.remote");

        enableLocalGroups();

        // try to add a local group with the same name
        gotoAddGroup();
        setTextField("name", groupName);
        selectOption("directoryID", DIRECTORY_NAME);
        submit();

        // a local group cannot have the same name as a remote one
        assertErrorPresentWithKey("invalid.namealreadyexist");
        assertKeyPresent("menu.addgroup.label");
        assertKeyNotPresent("menu.viewgroup.label");

        // try to remove the remote group
        gotoBrowseGroups();
        clickLinkWithExactText(groupName);
        clickLink("remove-group");
        submit();

        // remote groups cannot be removed because local groups are enabled
        assertWarningPresent();
        assertTextPresent(new ReadOnlyGroupException(groupName).getMessage());

        disableLocalGroups();

        // try (again) to remove the remote group
        gotoBrowseGroups();
        clickLinkWithExactText(groupName);
        clickLink("remove-group");
        submit();

        // now it succeeds
        assertKeyPresent("updatesuccessful.label");
        assertTextNotInTable("group-details", groupName);
    }

    private void enableLocalGroups()
    {
        gotoBrowseDirectories();
        clickLinkWithExactText(DIRECTORY_NAME);
        clickLink("connector-connectiondetails");
        checkCheckbox("localGroupsEnabled");
        submit();

        assertWarningAndErrorNotPresent();
        assertCheckboxSelected("localGroupsEnabled");
    }

    private void disableLocalGroups()
    {
        gotoBrowseDirectories();
        clickLinkWithExactText(DIRECTORY_NAME);
        clickLink("connector-connectiondetails");
        uncheckCheckbox("localGroupsEnabled");
        submit();

        assertWarningAndErrorNotPresent();
        assertCheckboxNotSelected("localGroupsEnabled");
    }

    // can't test this because this is a group membership mutation

//    public void testAddSubGroupToLDAPDirectory()
//    {
//        log("Running testAddSubGroupToLDAPDirectory");
//
//        // Add the parent group; the child group is added directly by LDAPLoader
//        gotoAddGroup();
//
//        setTextField("name", GROUP_NAME);
//        setTextField("description", "Crowd LDAP Test Group");
//        selectOption("directoryID", DIRECTORY_NAME);
//
//        submit();
//
//        assertKeyPresent("menu.viewgroup.label");
//        assertTextPresent(GROUP_NAME);
//
//        // Add the child group to the parent group
//        clickLink("view-group-users");
//        selectOption("childGroup", SUB_GROUP_NAME);
//        clickButton("add-selected-group");
//
//        assertTextInTable("view-group-groups", SUB_GROUP_NAME);
//    }
}
