package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.directory.monitor.poller.DirectoryPoller;
import com.atlassian.crowd.manager.directory.monitor.DirectoryMonitorRegistrationException;
import com.atlassian.crowd.manager.directory.monitor.poller.SpringQuartzDirectoryPollerManager;
import org.quartz.JobDetail;
import org.quartz.Trigger;
import org.springframework.scheduling.quartz.SimpleTriggerBean;

import java.text.ParseException;

/**
 * A "real" SpringQuartzDirectoryPollerManager used for testing only. Start delay and repeat interval have been set to
 * a long time to ensure that the directory will only poll when it is manually triggered.
 */
public class SpringQuartzDirectoryPollerManagerForTesting extends SpringQuartzDirectoryPollerManager
{
    private static final int LONG_DELAY_IN_MILLIS = 3 * 60 * 60 * 1000; // 3 hours

    @Override
    protected Trigger buildTrigger(DirectoryPoller poller, JobDetail jobDetail) throws DirectoryMonitorRegistrationException
    {
        SimpleTriggerBean trigger = new SimpleTriggerBean();
        trigger.setName(getJobName(poller.getDirectoryID()));
        trigger.setGroup(DIRECTORY_POLLER_JOB_GROUP);

        // If the Job is Volatile, then the Trigger must be too (else you can get Exceptions).
        trigger.setVolatility(jobDetail.isVolatile());

        trigger.setJobDetail(jobDetail);
        // Set the delay and repeat interval to a long time to prevent the poller from automatically kicking off during tests
        trigger.setStartDelay(LONG_DELAY_IN_MILLIS);
        trigger.setRepeatInterval(LONG_DELAY_IN_MILLIS);
        try
        {
            trigger.afterPropertiesSet();
        }
        catch (ParseException e)
        {
            throw new DirectoryMonitorRegistrationException(e);
        }
        return trigger;
    }
}
