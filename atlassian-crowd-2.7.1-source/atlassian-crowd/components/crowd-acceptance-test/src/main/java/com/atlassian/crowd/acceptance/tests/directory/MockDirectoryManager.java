package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectorySynchronisationInformation;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryCurrentlySynchronisingException;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidEmailAddressException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.InvalidMembershipException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.NestedGroupsNotSupportedException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.ReadOnlyGroupException;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.directory.BulkAddResult;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.directory.DirectoryManagerGeneric;
import com.atlassian.crowd.manager.directory.DirectoryPermissionException;
import com.atlassian.crowd.manager.directory.DirectorySynchroniser;
import com.atlassian.crowd.manager.directory.SynchronisationMode;
import com.atlassian.crowd.manager.directory.monitor.poller.DirectoryPollerManager;
import com.atlassian.crowd.manager.lock.DirectoryLockManager;
import com.atlassian.crowd.manager.permission.PermissionManager;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.PasswordHelperImpl;
import com.atlassian.event.api.EventPublisher;

import static org.mockito.Mockito.mock;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MockDirectoryManager implements DirectoryManager
{
    private final DirectoryManager delegated;
    private Directory directory;

    public MockDirectoryManager(PermissionManager permissionManager,
                                EventPublisher eventPublisher,
                                DirectoryInstanceLoader directoryInstanceLoader,
                                DirectorySynchroniser directorySynchroniser,
                                DirectoryPollerManager directoryPollerManager,
                                DirectoryLockManager directoryLockManager)
    {
        // since we manually instantiate the manager, it will not use transactions
        delegated = new DirectoryManagerGeneric(mock(DirectoryDao.class),
                                                mock(ApplicationDAO.class),
                                                eventPublisher,
                                                permissionManager,
                                                new PasswordHelperImpl(),
                                                directoryInstanceLoader,
                                                directorySynchroniser,
                                                directoryPollerManager,
                                                directoryLockManager,
                                                new MockSynchronisationStatusManager())
        {
            /**
             * This is the only implemented method. Will always return <code>directory</code>.
             *
             * @param directoryID
             * @return
             */
            @Override
            public Directory findDirectoryById(long directoryID) throws DirectoryNotFoundException
            {
                return directory;
            }

            @Override
            public List<Directory> findAllDirectories()
            {
                return Arrays.asList(directory);
            }

            @Override
            public List<Directory> searchDirectories(final EntityQuery query)
            {
                return Arrays.asList(directory);
            }

            @Override
            public Directory findDirectoryByName(final String name) throws DirectoryNotFoundException
            {
                return directory;
            }

            @Override
            public Directory addDirectory(final Directory directory) throws DirectoryInstantiationException
            {
                throw new UnsupportedOperationException();
            }

            @Override
            public Directory updateDirectory(final Directory directory) throws DirectoryNotFoundException
            {
                throw new UnsupportedOperationException();
            }

            @Override
            public void removeDirectory(final Directory directory) throws DirectoryNotFoundException, DirectoryCurrentlySynchronisingException
            {
                throw new UnsupportedOperationException();
            }
        };
    }

    // Mock-specific methods

    public void setDirectory(final Directory directory)
    {
        this.directory = directory;
    }

    // Everything else is just delegated

    @Override
    public Directory addDirectory(Directory directory) throws DirectoryInstantiationException
    {
        return delegated.addDirectory(directory);
    }

    @Override
    public Directory findDirectoryById(long directoryID) throws DirectoryNotFoundException
    {
        return delegated.findDirectoryById(directoryID);
    }

    @Override
    public List<Directory> findAllDirectories()
    {
        return delegated.findAllDirectories();
    }

    @Override
    public List<Directory> searchDirectories(EntityQuery<Directory> query)
    {
        return delegated.searchDirectories(query);
    }

    @Override
    public Directory findDirectoryByName(String name) throws DirectoryNotFoundException
    {
        return delegated.findDirectoryByName(name);
    }

    @Override
    public Directory updateDirectory(Directory directory) throws DirectoryNotFoundException
    {
        return delegated.updateDirectory(directory);
    }

    @Override
    public void removeDirectory(Directory directory)
        throws DirectoryNotFoundException, DirectoryCurrentlySynchronisingException
    {
        delegated.removeDirectory(directory);
    }

    @Override
    public User authenticateUser(long directoryId, String username, PasswordCredential passwordCredential)
        throws OperationFailedException, InactiveAccountException, InvalidAuthenticationException,
               ExpiredCredentialException, DirectoryNotFoundException, UserNotFoundException
    {
        return delegated.authenticateUser(directoryId, username, passwordCredential);
    }

    @Override
    public User findUserByName(long directoryId, String username)
        throws DirectoryNotFoundException, UserNotFoundException, OperationFailedException
    {
        return delegated.findUserByName(directoryId, username);
    }

    @Override
    public UserWithAttributes findUserWithAttributesByName(long directoryId, String username)
        throws DirectoryNotFoundException, UserNotFoundException, OperationFailedException
    {
        return delegated.findUserWithAttributesByName(directoryId, username);
    }

    @Override
    public <T> List<T> searchUsers(long directoryId, EntityQuery<T> query)
        throws DirectoryNotFoundException, OperationFailedException
    {
        return delegated.searchUsers(directoryId, query);
    }

    @Override
    public User addUser(long directoryId, UserTemplate user, PasswordCredential credential)
        throws InvalidCredentialException, InvalidUserException, DirectoryPermissionException,
               DirectoryNotFoundException, OperationFailedException, UserAlreadyExistsException
    {
        return delegated.addUser(directoryId, user, credential);
    }

    @Override
    public User updateUser(long directoryId, UserTemplate user)
        throws DirectoryNotFoundException, UserNotFoundException, DirectoryPermissionException, InvalidUserException,
               OperationFailedException
    {
        return delegated.updateUser(directoryId, user);
    }

    @Override
    public User renameUser(long directoryId, String oldUsername, String newUsername)
        throws DirectoryNotFoundException, UserNotFoundException, OperationFailedException,
               DirectoryPermissionException, InvalidUserException, UserAlreadyExistsException
    {
        return delegated.renameUser(directoryId, oldUsername, newUsername);
    }

    @Override
    public void storeUserAttributes(long directoryId, String username, Map<String, Set<String>> attributes)
        throws DirectoryPermissionException, DirectoryNotFoundException, UserNotFoundException, OperationFailedException
    {
        delegated.storeUserAttributes(directoryId, username, attributes);
    }

    @Override
    public void removeUserAttributes(long directoryId, String username, String attributeName)
        throws DirectoryPermissionException, DirectoryNotFoundException, UserNotFoundException, OperationFailedException
    {
        delegated.removeUserAttributes(directoryId, username, attributeName);
    }

    @Override
    public void updateUserCredential(long directoryId, String username, PasswordCredential credential)
        throws DirectoryPermissionException, InvalidCredentialException, DirectoryNotFoundException,
               UserNotFoundException, OperationFailedException
    {
        delegated.updateUserCredential(directoryId, username, credential);
    }

    @Override
    public void resetPassword(long directoryId, String username)
        throws DirectoryNotFoundException, UserNotFoundException, InvalidEmailAddressException,
               DirectoryPermissionException, InvalidCredentialException, OperationFailedException
    {
        delegated.resetPassword(directoryId, username);
    }

    @Override
    public void removeUser(long directoryId, String username)
        throws DirectoryNotFoundException, UserNotFoundException, DirectoryPermissionException, OperationFailedException
    {
        delegated.removeUser(directoryId, username);
    }

    @Override
    public Group findGroupByName(long directoryId, String groupName)
        throws GroupNotFoundException, DirectoryNotFoundException, OperationFailedException
    {
        return delegated.findGroupByName(directoryId, groupName);
    }

    @Override
    public GroupWithAttributes findGroupWithAttributesByName(long directoryId, String groupName)
        throws GroupNotFoundException, DirectoryNotFoundException, OperationFailedException
    {
        return delegated.findGroupWithAttributesByName(directoryId, groupName);
    }

    @Override
    public <T> List<T> searchGroups(long directoryId, EntityQuery<T> query)
        throws DirectoryNotFoundException, OperationFailedException
    {
        return delegated.searchGroups(directoryId, query);
    }

    @Override
    public Group addGroup(long directoryId, GroupTemplate group)
        throws InvalidGroupException, DirectoryPermissionException, DirectoryNotFoundException, OperationFailedException
    {
        return delegated.addGroup(directoryId, group);
    }

    @Override
    public Group updateGroup(long directoryId, GroupTemplate group)
        throws GroupNotFoundException, DirectoryNotFoundException, DirectoryPermissionException, InvalidGroupException,
               OperationFailedException, ReadOnlyGroupException
    {
        return delegated.updateGroup(directoryId, group);
    }

    @Override
    public Group renameGroup(long directoryId, String oldGroupname, String newGroupname)
        throws GroupNotFoundException, DirectoryNotFoundException, DirectoryPermissionException, InvalidGroupException,
               OperationFailedException
    {
        return delegated.renameGroup(directoryId, oldGroupname, newGroupname);
    }

    @Override
    public void storeGroupAttributes(long directoryId, String groupName, Map<String, Set<String>> attributes)
        throws DirectoryPermissionException, GroupNotFoundException, DirectoryNotFoundException,
               OperationFailedException
    {
        delegated.storeGroupAttributes(directoryId, groupName, attributes);
    }

    @Override
    public void removeGroupAttributes(long directoryId, String groupName, String attributeName)
        throws DirectoryPermissionException, GroupNotFoundException, DirectoryNotFoundException,
               OperationFailedException
    {
        delegated.removeGroupAttributes(directoryId, groupName, attributeName);
    }

    @Override
    public void removeGroup(long directoryId, String groupName)
        throws GroupNotFoundException, DirectoryNotFoundException, DirectoryPermissionException,
               OperationFailedException, ReadOnlyGroupException
    {
        delegated.removeGroup(directoryId, groupName);
    }

    @Override
    public boolean isUserDirectGroupMember(long directoryId, String username, String groupName)
        throws DirectoryNotFoundException, OperationFailedException
    {
        return delegated.isUserDirectGroupMember(directoryId, username, groupName);
    }

    @Override
    public boolean isGroupDirectGroupMember(long directoryId, String childGroup, String parentGroup)
        throws DirectoryNotFoundException, OperationFailedException
    {
        return delegated.isGroupDirectGroupMember(directoryId, childGroup, parentGroup);
    }

    @Override
    public void addUserToGroup(long directoryId, String username, String groupName)
        throws DirectoryPermissionException, DirectoryNotFoundException, UserNotFoundException, GroupNotFoundException,
               OperationFailedException, ReadOnlyGroupException, MembershipAlreadyExistsException
    {
        delegated.addUserToGroup(directoryId, username, groupName);
    }

    @Override
    public void addGroupToGroup(long directoryId, String childGroup, String parentGroup)
        throws DirectoryPermissionException, DirectoryNotFoundException, GroupNotFoundException,
               InvalidMembershipException, NestedGroupsNotSupportedException, OperationFailedException,
               ReadOnlyGroupException, MembershipAlreadyExistsException
    {
        delegated.addGroupToGroup(directoryId, childGroup, parentGroup);
    }

    @Override
    public void removeUserFromGroup(long directoryId, String username, String groupName)
        throws DirectoryPermissionException, DirectoryNotFoundException, UserNotFoundException, GroupNotFoundException,
               MembershipNotFoundException, OperationFailedException, ReadOnlyGroupException
    {
        delegated.removeUserFromGroup(directoryId, username, groupName);
    }

    @Override
    public void removeGroupFromGroup(long directoryId, String childGroup, String parentGroup)
        throws DirectoryPermissionException, GroupNotFoundException, DirectoryNotFoundException,
               InvalidMembershipException, MembershipNotFoundException, OperationFailedException, ReadOnlyGroupException
    {
        delegated.removeGroupFromGroup(directoryId, childGroup, parentGroup);
    }

    @Override
    public <T> List<T> searchDirectGroupRelationships(long directoryId, MembershipQuery<T> query)
        throws DirectoryNotFoundException, OperationFailedException
    {
        return delegated.searchDirectGroupRelationships(directoryId, query);
    }

    @Override
    public boolean isUserNestedGroupMember(long directoryId, String username, String groupName)
        throws DirectoryNotFoundException, OperationFailedException
    {
        return delegated.isUserNestedGroupMember(directoryId, username, groupName);
    }

    @Override
    public boolean isGroupNestedGroupMember(long directoryId, String childGroup, String parentGroup)
        throws DirectoryNotFoundException, OperationFailedException
    {
        return delegated.isGroupNestedGroupMember(directoryId, childGroup, parentGroup);
    }

    @Override
    public <T> List<T> searchNestedGroupRelationships(long directoryId, MembershipQuery<T> query)
        throws DirectoryNotFoundException, OperationFailedException
    {
        return delegated.searchNestedGroupRelationships(directoryId, query);
    }

    @Override
    public BulkAddResult<User> addAllUsers(long directoryId,
                                           Collection<UserTemplateWithCredentialAndAttributes> users,
                                           boolean overwrite)
        throws DirectoryPermissionException, DirectoryNotFoundException, OperationFailedException
    {
        return delegated.addAllUsers(directoryId, users, overwrite);
    }

    @Override
    public BulkAddResult<Group> addAllGroups(long directoryId, Collection<GroupTemplate> groups, boolean overwrite)
        throws DirectoryPermissionException, DirectoryNotFoundException, OperationFailedException, InvalidGroupException
    {
        return delegated.addAllGroups(directoryId, groups, overwrite);
    }

    @Override
    public BulkAddResult<String> addAllUsersToGroup(long directoryID, Collection<String> userNames, String groupName)
        throws DirectoryPermissionException, DirectoryNotFoundException, GroupNotFoundException,
               OperationFailedException
    {
        return delegated.addAllUsersToGroup(directoryID, userNames, groupName);
    }

    @Override
    public boolean supportsNestedGroups(long directoryId)
        throws DirectoryInstantiationException, DirectoryNotFoundException
    {
        return delegated.supportsNestedGroups(directoryId);
    }

    @Override
    public boolean isSynchronisable(long directoryId) throws DirectoryInstantiationException, DirectoryNotFoundException
    {
        return delegated.isSynchronisable(directoryId);
    }

    @Override
    public void synchroniseCache(long directoryId, SynchronisationMode mode)
        throws OperationFailedException, DirectoryNotFoundException
    {
        delegated.synchroniseCache(directoryId, mode);
    }

    @Override
    public void synchroniseCache(long directoryId, SynchronisationMode mode, boolean runInBackground)
        throws OperationFailedException, DirectoryNotFoundException
    {
        delegated.synchroniseCache(directoryId, mode, runInBackground);
    }

    @Override
    public boolean isSynchronising(long directoryId) throws DirectoryInstantiationException, DirectoryNotFoundException
    {
        return delegated.isSynchronising(directoryId);
    }

    @Override
    public DirectorySynchronisationInformation getDirectorySynchronisationInformation(long directoryId)
        throws DirectoryInstantiationException, DirectoryNotFoundException
    {
        return delegated.getDirectorySynchronisationInformation(directoryId);
    }
}
