package com.atlassian.crowd.acceptance.tests.persistence.importer;

import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.exceptions.ImporterConfigurationException;
import com.atlassian.crowd.importer.factory.ImporterFactory;
import com.atlassian.crowd.importer.importers.Importer;
import com.atlassian.crowd.importer.importers.JiraImporter;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.plugin.web.WebInterfaceManager;
import org.quartz.Scheduler;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.test.AbstractDependencyInjectionSpringContextTests;

import static org.mockito.Mockito.mock;

import java.util.Set;

/**
 * ImporterFactoryImpl Tester.
 */
public class ImporterFactoryImplTest extends AbstractDependencyInjectionSpringContextTests
{
    private ImporterFactory importerFactory;
    private Configuration configuration;

    protected String[] getConfigLocations()
    {
        setAutowireMode(AUTOWIRE_BY_NAME);
        return new String[]{
                "classpath:/applicationContext-CrowdDAO.xml",
                "classpath:/applicationContext-CrowdManagers.xml",
                "classpath:/applicationContext-CrowdUtils.xml",
                "classpath:/applicationContext-CrowdEncryption.xml",
                "classpath:/applicationContext-CrowdImporter.xml",
                "classpath:/applicationContext-CrowdLDAP.xml",
                "classpath:/applicationContext-CrowdServer.xml",
                "classpath:/applicationContext-CrowdClient.xml",
                "classpath:/applicationContext-config.xml"
        };
    }

    @Override
    protected void customizeBeanFactory(DefaultListableBeanFactory beanFactory)
    {
        beanFactory.registerSingleton("scheduler", mock(Scheduler.class));
        beanFactory.registerSingleton("bootstrapManager", mock(CrowdBootstrapManager.class));
        beanFactory.registerSingleton("crowdUserDetailsService", mock(CrowdUserDetailsService.class));
        beanFactory.registerSingleton("webInterfaceManager", mock(WebInterfaceManager.class));
    }

    protected void onSetUp() throws Exception
    {
        super.onSetUp();
        configuration = new Configuration(1L, "jira", Boolean.TRUE, Boolean.TRUE);
    }

    public void testGetImporterDAO() throws Exception
    {
        Importer importerDAO = importerFactory.getImporterDAO(configuration);

        assertNotNull(importerDAO);

        assertTrue(importerDAO instanceof JiraImporter);
    }

    public void testGetImporterDAOWithNullConfiguration() throws ImporterConfigurationException
    {
        try
        {
            importerFactory.getImporterDAO(null);
            fail("We should have thrown an IllegalArgumentException");
        }
        catch (IllegalArgumentException e)
        {
            // We should be here
        }
    }

    public void testGetImporterDAOWithBadConfiguration()
    {
        try
        {
            configuration.setApplication(null);
            importerFactory.getImporterDAO(configuration);
            fail("We should have thrown an ImporterConfigurationException");
        }
        catch (ImporterConfigurationException e)
        {
            // We should be here
        }
    }

    public void testGetSupportedImporterApplications() throws Exception
    {
        Set<String> importers = importerFactory.getSupportedImporterApplications();
        assertNotNull(importers);
        assertFalse(importers.isEmpty());
    }

    public void testGetAtlassianSupportedImporterApplications() throws Exception
    {
        Set<String> importers = importerFactory.getAtlassianSupportedImporterApplications();
        assertNotNull(importers);
        assertFalse(importers.isEmpty());
    }

    public void testGetSupportedImporterApplicationsIsAnUnmodifiableSet()
    {
        Set<String> importers = importerFactory.getSupportedImporterApplications();
        try
        {
            importers.add("randomimporter");
            fail("We should have thrown a UnsupportedOperationException");
        }
        catch (UnsupportedOperationException e)
        {
            // We should be here
        }
    }

    public void testGetAtlassianSupportedImporterApplicationsIsAnUnmodifiableSet()
    {
        Set<String> importers = importerFactory.getAtlassianSupportedImporterApplications();
        try
        {
            importers.add("randomimporter");
            fail("We should have thrown a UnsupportedOperationException");
        }
        catch (UnsupportedOperationException e)
        {
            // We should be here
        }
    }

    public void setImporterFactory(ImporterFactory importerFactory)
    {
        this.importerFactory = importerFactory;
    }
}
