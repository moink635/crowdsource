package com.atlassian.crowd.acceptance.tests.applications.crowd;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

/**
 * Acceptance tests for the directory based importer.
 */
public class DirectoryImporterTest extends CrowdAcceptanceTestCase
{
    private static final String SOURCE_DIRECTORY_NAME = "Test Internal Directory";
    private static final String TARGET_DIRECTORY_NAME = "Blank Internal Directory";
    private static final String DIFFERENT_ENCRYPTION_DIRECTORY_NAME = "Third Directory";

    private static final String SOURCE_DIRECTORY_NAME_WITH_NESTED_GROUPS = "nested group";
    private static final String DESTINATION_DIRECTORY_NAME_WITH_NESTED_GROUPS = "another blank directory with nested group";
    private static final String DESTINATION_DIRECTORY_NAME_WITHOUT_NESTED_GROUPS = "another blank directory without nested group";


    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreCrowdFromXML("directoryimportertest.xml");

        _loginAdminUser();

        gotoImporters();

        clickButton("importdirectory");
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testBothDirectoriesCannotBeTheSame()
    {
        log("Running testBothDirectoriesCannotBeTheSame");

        setWorkingForm("dataimport");

        selectOption("sourceDirectoryID", TARGET_DIRECTORY_NAME);
        selectOption("targetDirectoryID", TARGET_DIRECTORY_NAME);

        // submit the form
        submit();

        // assert error
        assertKeyPresent("dataimport.directory.sourcetargetsame.error");
    }

    public void testUserEncryptionMustBeTheSame()
    {
        log("Running testUserEncryptionMustBeTheSame");

        setWorkingForm("dataimport");

        selectOption("sourceDirectoryID", TARGET_DIRECTORY_NAME);
        selectOption("targetDirectoryID", DIFFERENT_ENCRYPTION_DIRECTORY_NAME);

        // submit the form
        submit();

        // assert error
        assertKeyPresent("dataimport.directory.same.encryption.error");
    }

    public void testPerformDirectoryMigration()
    {
        log("Running testPerformDirectoryMigration");

        intendToModifyData();

        setWorkingForm("dataimport");

        selectOption("sourceDirectoryID", SOURCE_DIRECTORY_NAME);
        selectOption("targetDirectoryID", TARGET_DIRECTORY_NAME);

        // submit the form
        submit();

        assertKeyPresent("dataimport.directory.source.confirmation.directory.text", SOURCE_DIRECTORY_NAME);
        assertKeyPresent("dataimport.directory.target.confirmation.directory.text", TARGET_DIRECTORY_NAME);
        assertKeyPresent("dataimport.directory.overwrite.confirmation.directory.text", "False");

        // Submit on confirmation screen
        setWorkingForm("dataimport");
        submit();

        // Assert confirmation screeen
        assertKeyPresent("dataimport.atlassianimportsuccess.text");

        assertThat(getElementTextById("users-imported"), is("2"));
        assertThat(getElementTextById("groups-imported"), is("2"));
        assertThat(getElementTextById("memberships-imported"), is("3"));
    }

    public void testPerformDirectoryImportWithNestedGroups()
    {
        log("Running testPerformDirectoryImportWithNestedGroups");

        intendToModifyData();

        setWorkingForm("dataimport");

        selectOption("sourceDirectoryID", SOURCE_DIRECTORY_NAME_WITH_NESTED_GROUPS);
        selectOption("targetDirectoryID", DESTINATION_DIRECTORY_NAME_WITH_NESTED_GROUPS);

        // submit the form
        submit();

        // the UI must indicate that nested groups will be imported.
        assertKeyPresent("dataimport.importnestedgroup.text");

        // Submit on confirmation screen
        setWorkingForm("dataimport");
        submit();

        // Assert confirmation screeen
        assertKeyPresent("dataimport.atlassianimportsuccess.text");

        assertThat(getElementTextById("users-imported"), is("1"));
        assertThat(getElementTextById("groups-imported"), is("2"));
        assertThat(getElementTextById("memberships-imported"), is("2"));
    }

    public void testPerformDirectoryImportWithoutNestedGroups()
    {
        log("Running testPerformDirectoryImportWithoutNestedGroups");

        intendToModifyData();

        setWorkingForm("dataimport");

        selectOption("sourceDirectoryID", SOURCE_DIRECTORY_NAME_WITH_NESTED_GROUPS);
        selectOption("targetDirectoryID", DESTINATION_DIRECTORY_NAME_WITHOUT_NESTED_GROUPS);

        // submit the form
        submit();

        // the UI must indicate that nested groups will *NOT* be imported.
        assertKeyPresent("dataimport.importnestedgroup.not.text");

        // Submit on confirmation screen
        setWorkingForm("dataimport");
        submit();

        // Assert confirmation screeen
        assertKeyPresent("dataimport.atlassianimportsuccess.text");

        assertThat(getElementTextById("users-imported"), is("1"));
        assertThat(getElementTextById("groups-imported"), is("2"));
        // group relationships will not be included.
        assertThat(getElementTextById("memberships-imported"), is("1"));
    }

    public void testPerformDirectoryImportWithOverwrite()
    {
        log("Running testPerformDirectoryImportWithOverwrite");

        intendToModifyData();

        selectOption("sourceDirectoryID", SOURCE_DIRECTORY_NAME);
        selectOption("targetDirectoryID", TARGET_DIRECTORY_NAME);
        checkCheckbox("overwriteTarget");
        submit();

        assertTextPresent("Overwriting destination directory: True");
        submit();

        assertKeyPresent("dataimport.atlassianimportsuccess.text");

        assertThat(getElementTextById("users-imported"), is("2"));
        assertThat(getElementTextById("groups-imported"), is("2"));
        assertThat(getElementTextById("memberships-imported"), is("3"));
    }

    /** Regression test for CWD-3495 */
    public void testPerformDirectoryImportThenRepeatWithOverwrite()
    {
        log("Running testPerformDirectoryImportWithOverwrite");

        intendToModifyData();

        selectOption("sourceDirectoryID", SOURCE_DIRECTORY_NAME);
        selectOption("targetDirectoryID", TARGET_DIRECTORY_NAME);
        submit();

        assertTextNotPresent("Overwriting destination directory: True");
        submit();

        assertThat(getElementTextById("users-imported"), is("2"));
        assertThat(getElementTextById("groups-imported"), is("2"));
        assertThat(getElementTextById("memberships-imported"), is("3"));

        // now do it again with overwrite enabled and make sure it still succeeds
        gotoImporters();

        clickButton("importdirectory");

        selectOption("sourceDirectoryID", SOURCE_DIRECTORY_NAME);
        selectOption("targetDirectoryID", TARGET_DIRECTORY_NAME);
        checkCheckbox("overwriteTarget");
        submit();

        assertTextPresent("Overwriting destination directory: True");
        submit();

        assertKeyPresent("dataimport.atlassianimportsuccess.text");

        assertThat(getElementTextById("users-imported"), is("2"));
        assertThat(getElementTextById("groups-imported"), is("2"));
        assertThat(getElementTextById("memberships-imported"), is("3"));
    }
}
