package com.atlassian.crowd.acceptance.utils;

import com.atlassian.crowd.directory.SynchronisableDirectory;
import com.atlassian.crowd.manager.directory.DirectorySynchroniser;
import com.atlassian.crowd.manager.directory.SynchronisationMode;

/**
 */
public class NoopDirectorySynchroniser implements DirectorySynchroniser
{
    public void synchronise(SynchronisableDirectory synchronisableDirectory, SynchronisationMode mode)
    {
        // do nothing
    }

    public boolean isSynchronising(long directoryId)
    {
        return false;
    }
}
