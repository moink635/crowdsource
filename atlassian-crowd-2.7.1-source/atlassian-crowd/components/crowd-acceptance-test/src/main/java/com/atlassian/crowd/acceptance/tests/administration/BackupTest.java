package com.atlassian.crowd.acceptance.tests.administration;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;

public class BackupTest extends CrowdAcceptanceTestCase
{

    /**
     * This test restores the backup to a known state because there are other tests
     * downstream that will use the file(s) generated here.
     * @see com.atlassian.crowd.acceptance.harness.ConfigurationFromFileCrowdAcceptanceTestHarness
     * @throws Exception
     */
    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("remotecrowddirectory.xml");
    }

    public void testExportBackup()
    {
        gotoAdministrationPage();
        clickLink("backup-admin");

        submit("manualBackup");

        assertWarningAndErrorNotPresent();
        assertSuccessPresent();

        // scrape the backup file name
        String message = getElementTextByXPath("//p[@class='success']");
        Pattern pattern = Pattern.compile("Crowd has successfully backed up to (.+\\.xml)\\. Time taken");
        Matcher matcher = pattern.matcher(message);
        assertTrue("Message should match regex", matcher.find());
        String filename = matcher.group(1);

        File xmlBackupFile = new File(filename);
        assertTrue("XML backup file " + xmlBackupFile + " should exist", xmlBackupFile.exists());

        File directoriesPropertiesFile = new File(xmlBackupFile.getParentFile(), "directories.properties");
        assertTrue("File " + directoriesPropertiesFile + " should exist", directoriesPropertiesFile.exists());
    }

}
