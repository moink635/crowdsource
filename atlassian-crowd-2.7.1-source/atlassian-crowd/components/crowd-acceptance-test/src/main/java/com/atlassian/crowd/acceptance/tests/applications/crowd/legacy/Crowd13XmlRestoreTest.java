package com.atlassian.crowd.acceptance.tests.applications.crowd.legacy;

public class Crowd13XmlRestoreTest extends BaseLegacyXmlRestoreTest
{
    public String getLegacyXmlFileName()
    {
        return "1_3_3_217.xml";
    }
}
