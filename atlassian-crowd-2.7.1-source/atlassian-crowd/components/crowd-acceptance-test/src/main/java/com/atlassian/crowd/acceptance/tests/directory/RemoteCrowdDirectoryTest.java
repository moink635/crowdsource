package com.atlassian.crowd.acceptance.tests.directory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.directory.RemoteCrowdDirectory;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.rest.service.factory.RestCrowdClientFactory;
import com.atlassian.crowd.model.DirectoryEntity;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.service.client.AbstractClientProperties;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.factory.CrowdClientFactory;
import com.atlassian.crowd.util.PasswordHelperImpl;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;

import org.hamcrest.Matcher;
import org.hamcrest.collection.IsEmptyCollection;
import org.hamcrest.collection.IsEmptyIterable;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;

import static org.junit.Assert.assertThat;

public class RemoteCrowdDirectoryTest extends CrowdAcceptanceTestCase
{
    private static final String APPLICATION_NAME = "crowd";
    private static final String APPLICATION_PASSWORD = "qybhDMZh";
    private static final String DEFAULT_USERNAME = "admin";

    private static final String USERNAME1 = "admin";
    private static final String USERNAME2 = "eeeep";
    private static final String MR_TRAILING_SPACES = "mr trailing spaces    ";

    private static final String GROUP1 = "badgers";
    private static final String GROUP2 = "crowd-administrators";
    private static final String GROUP3 = "crowd-testers";
    private static final String GROUP4 = "crowd-users";

    private static final String TEST_USERNAME = "eeeep";
    private static final String TEST_USER_PASSWORD = "abc123";
    private static final String BAD_USER_PASSWORD = "badpassword";
    private static final String TEST_GROUP = "badgers";
    private static final String NON_EXISTENT_USERNAME = "errorName";
    private static final String NON_EXISTENT_GROUP = "errorGroup";

    private static final String NEW_PASSWORD = "newpassword";
    private static final String NEW_DISPLAY_NAME = "New Display Name";
    private static final String NEW_FIRST_NAME = "New First Name";
    private static final String NEW_LAST_NAME = "New Last Name";
    private static final String NEW_EMAIL = "newemail@example.com";
    private static final String NEW_GROUP = "newgroup";
    private static final String NEW_GROUP_DESCRIPTION = "New group description";

    private static final List<String> ALL_USER_NAMES = Arrays.asList(USERNAME1, USERNAME2, "penny", "regularuser", "secondadmin", MR_TRAILING_SPACES);
    private static final List<String> ALL_GROUP_NAMES= Arrays.asList("animals", "badgers", "birds", "cats", "crowd-administrators", "crowd-testers", "crowd-users", "dogs");

    private Map<String, String> attributes;
    private RemoteCrowdDirectory remoteDirectory;
    private CrowdClientFactory restClientFactory;

    private static class FixedClientProperties extends AbstractClientProperties
    {
        private FixedClientProperties(String url, String username, String password,
                                             String httpTimeOut, String httpMaxConnections,
                                             String proxyHost, String proxyPort, String proxyUsername, String proxyPassword)
        {
            this.baseURL = url;
            this.applicationName = username;
            this.applicationPassword = password;

            this.httpTimeout = httpTimeOut;
            this.httpMaxConnections = httpMaxConnections;

            this.httpProxyHost = proxyHost;
            this.httpProxyPort = proxyPort;
            this.httpProxyPassword = proxyUsername;
            this.httpProxyPassword = proxyPassword;
        }

        public void updateProperties(Properties properties)
        {
            throw new UnsupportedOperationException("not supported");
        }
    }

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("remotecrowddirectory.xml");

        restClientFactory = new RestCrowdClientFactory();
        remoteDirectory = newRemoteCrowdDirectoryForTest(restClientFactory);
        remoteDirectory.setDirectoryId(1);

        attributes = new HashMap<String, String>();
        attributes.put(RemoteCrowdDirectory.CROWD_SERVER_URL, HOST_PATH);
        attributes.put(RemoteCrowdDirectory.APPLICATION_NAME, APPLICATION_NAME);
        attributes.put(RemoteCrowdDirectory.APPLICATION_PASSWORD, APPLICATION_PASSWORD);

        remoteDirectory.setAttributes(attributes);
    }

    RemoteCrowdDirectory newRemoteCrowdDirectoryForTest(CrowdClientFactory restClientFactory)
    {
        return new RemoteCrowdDirectory(restClientFactory, new PasswordHelperImpl()) {
            @Override
            protected ClientProperties getClientProperties() {
                return new FixedClientProperties(getValue(CROWD_SERVER_URL), getValue(APPLICATION_NAME), getValue(APPLICATION_PASSWORD),
                        getValue(CROWD_HTTP_TIMEOUT),
                        getValue(CROWD_HTTP_MAX_CONNECTIONS),
                        getValue(CROWD_HTTP_PROXY_HOST), getValue(CROWD_HTTP_PROXY_PORT),
                        getValue(CROWD_HTTP_PROXY_USERNAME), getValue(CROWD_HTTP_PROXY_PASSWORD));
            };
        };
    }

    public void testBadClientParameters() throws UserNotFoundException, GroupNotFoundException
    {
        RemoteCrowdDirectory badRemoteDir = newRemoteCrowdDirectoryForTest(restClientFactory);
        badRemoteDir.setAttributes(ImmutableMap.<String, String>of());

        try
        {
            badRemoteDir.findUserByName("blah");
            fail("OperationFailedException expected");
        }
        catch (OperationFailedException e)
        {
            // good.
        }

        try
        {
            badRemoteDir.findGroupByName("hoho");
            fail("OperationFailedException expected");
        }
        catch (OperationFailedException e)
        {
            // good.
        }
    }

    // Connection
    public void testConnection() throws OperationFailedException
    {
        remoteDirectory.testConnection();
    }

    public void testInvalidConnections() throws OperationFailedException
    {
        RemoteDirectory badDirectory = newRemoteCrowdDirectoryForTest(restClientFactory);
        badDirectory.setDirectoryId(1);

        attributes = new HashMap<String, String>();
        attributes.put(RemoteCrowdDirectory.CROWD_SERVER_URL, "http://badhost.priv:2388/crowd");
        attributes.put(RemoteCrowdDirectory.APPLICATION_NAME, APPLICATION_NAME);
        attributes.put(RemoteCrowdDirectory.APPLICATION_PASSWORD, APPLICATION_PASSWORD);
        badDirectory.setAttributes(attributes);
        try
        {
            badDirectory.testConnection();
            fail("Should not get a good test of 'badhost.priv'");
        }
        catch (OperationFailedException e)
        {
            // Good result
        }
        finally
        {
//            System.setProperty("crowd.property.crowd.server.url", "http://badhost.priv:2388/crowd")
        }

        badDirectory = newRemoteCrowdDirectoryForTest(restClientFactory);
        badDirectory.setDirectoryId(1);

        attributes = new HashMap<String, String>();
        attributes.put(RemoteCrowdDirectory.CROWD_SERVER_URL, HOST_PATH);
        attributes.put(RemoteCrowdDirectory.APPLICATION_NAME, "badapplication");
        attributes.put(RemoteCrowdDirectory.APPLICATION_PASSWORD, APPLICATION_PASSWORD);
        badDirectory.setAttributes(attributes);
        try
        {
            badDirectory.testConnection();
            fail("Should not get a good test using 'badapplication'");
        }
        catch (OperationFailedException e)
        {
            // Good result
        }

        badDirectory = newRemoteCrowdDirectoryForTest(restClientFactory);
        badDirectory.setDirectoryId(1);


        attributes = new HashMap<String, String>();
        attributes.put(RemoteCrowdDirectory.CROWD_SERVER_URL, HOST_PATH);
        attributes.put(RemoteCrowdDirectory.APPLICATION_NAME, APPLICATION_NAME);
        attributes.put(RemoteCrowdDirectory.APPLICATION_PASSWORD, "badpass");
        badDirectory.setAttributes(attributes);
        try
        {
            badDirectory.testConnection();
            fail("Should not get a good test using 'badpass'");
        }
        catch (OperationFailedException e)
        {
            // Good result
        }
    }

    // USER tests
    public void testFindUserByName() throws Exception
    {
        User user = remoteDirectory.findUserByName(USERNAME1);
        assertNotNull(user);
        assertEquals(DEFAULT_USERNAME, user.getName());

    }

    public void testFindUserWithSpacesInName() throws Exception
    {
        UserWithAttributes user = remoteDirectory.findUserWithAttributesByName(MR_TRAILING_SPACES);
        assertNotNull(user);
        assertEquals(MR_TRAILING_SPACES, user.getName());
        assertEquals("user should have an attribute", "false", user.getValue("requiresPasswordChange"));
    }

    public void testFindUserByNameNotFound() throws OperationFailedException
    {
        try
        {
            remoteDirectory.findUserByName(NON_EXISTENT_USERNAME);
            fail("User '" + NON_EXISTENT_USERNAME + "' does not exist, but we found him anyway. That's bad");
        }
        catch (UserNotFoundException e)
        {
            // Good result
        }
    }

    public void testFindUserWithAttributesByName() throws Exception
    {
        UserWithAttributes user = remoteDirectory.findUserWithAttributesByName(USERNAME1);
        assertNotNull(user);
        assertEquals(DEFAULT_USERNAME, user.getName());
        assertNotNull(user.getValue("requiresPasswordChange"));
        assertEquals("false", user.getValue("requiresPasswordChange"));

    }

    public void testFindUserWithAttributesByNameNotFound() throws OperationFailedException
    {
        try
        {
            remoteDirectory.findUserWithAttributesByName(NON_EXISTENT_USERNAME);
            fail("User '" + NON_EXISTENT_USERNAME + "' does not exist, but we found him anyway. That's bad");
        }
        catch (UserNotFoundException e)
        {
            // Good result
        }
    }

    public void testAddActiveUser() throws Exception
    {
        intendToModifyData();

        UserTemplate userTemplate = new UserTemplate("newuser", 1);
        userTemplate.setActive(true);
        userTemplate.setDisplayName("New User via REST");
        userTemplate.setFirstName("New");
        userTemplate.setLastName("User");
        userTemplate.setEmailAddress("newuser@example.com");

        User user = remoteDirectory.addUser(userTemplate, new PasswordCredential("password"));
        assertNotNull(user);
        assertEquals("newuser", user.getName());

        // We should be able to retrieve the user

        UserWithAttributes addedUser = remoteDirectory.findUserWithAttributesByName("newuser");
        assertNotNull(addedUser);
        assertEquals("newuser", addedUser.getName());
        assertTrue(addedUser.isActive());
        assertEquals("New User via REST", addedUser.getDisplayName());
        assertEquals("New", addedUser.getFirstName());
        assertEquals("User", addedUser.getLastName());
        assertEquals("newuser@example.com", addedUser.getEmailAddress());
    }

    public void testAddInactiveUser() throws Exception
    {
        intendToModifyData();

        UserTemplate userTemplate = new UserTemplate("newuser", 1);
        userTemplate.setActive(false);
        userTemplate.setDisplayName("New User via REST");
        userTemplate.setFirstName("New");
        userTemplate.setLastName("User");
        userTemplate.setEmailAddress("newuser@example.com");

        User user = remoteDirectory.addUser(userTemplate, new PasswordCredential("password"));
        assertNotNull(user);
        assertEquals("newuser", user.getName());

        // We should be able to retrieve the user

        UserWithAttributes addedUser = remoteDirectory.findUserWithAttributesByName("newuser");
        assertNotNull(addedUser);
        assertEquals("newuser", addedUser.getName());
        assertFalse(addedUser.isActive());
        assertEquals("New User via REST", addedUser.getDisplayName());
        assertEquals("New", addedUser.getFirstName());
        assertEquals("User", addedUser.getLastName());
        assertEquals("newuser@example.com", addedUser.getEmailAddress());
    }

    public void testAddUserNoPassword() throws Exception
    {
        UserTemplate userTemplate = new UserTemplate("newuser", 1);
        userTemplate.setActive(true);
        userTemplate.setDisplayName("New User via REST");
        userTemplate.setFirstName("New");
        userTemplate.setLastName("User");
        userTemplate.setEmailAddress("newuser@example.com");

        try
        {
            remoteDirectory.addUser(userTemplate, new PasswordCredential(""));
            fail("Should not be able to create a user with an empty password");
        }
        catch (InvalidCredentialException e)
        {
            // Good result
        }
    }

    public void testAddUserDuplicate() throws Exception
    {
        intendToModifyData();

        UserTemplate userTemplate = new UserTemplate("newuser", 1);
        userTemplate.setActive(true);
        userTemplate.setDisplayName("New User via REST");
        userTemplate.setFirstName("New");
        userTemplate.setLastName("User");
        userTemplate.setEmailAddress("newuser@example.com");

        User user = remoteDirectory.addUser(userTemplate, new PasswordCredential("password"));
        assertNotNull(user);
        assertEquals("newuser", user.getName());

        try
        {
            user = remoteDirectory.addUser(userTemplate, new PasswordCredential("password"));
            fail("Should not be able to create a user with the same name as one that already exists");
        }
        catch (InvalidUserException e)
        {
            // Good result
        }
    }

    public void testUpdateUser() throws Exception
    {
        intendToModifyData();

        User user = remoteDirectory.findUserByName(TEST_USERNAME);
        assertNotNull(user);
        assertTrue(user.isActive());
        assertEquals(TEST_USERNAME, user.getName());

        UserTemplate userTemplate = new UserTemplate(user);
        userTemplate.setActive(false);
        userTemplate.setDisplayName(NEW_DISPLAY_NAME);
        userTemplate.setFirstName(NEW_FIRST_NAME);
        userTemplate.setLastName(NEW_LAST_NAME);
        userTemplate.setEmailAddress(NEW_EMAIL);

        User updatedUser = remoteDirectory.updateUser(userTemplate);

        // We should be able to retrieve the user

        updatedUser = remoteDirectory.findUserWithAttributesByName(TEST_USERNAME);
        assertNotNull(user);
        assertFalse(updatedUser.isActive());
        assertEquals(TEST_USERNAME, updatedUser.getName());
        assertEquals(NEW_DISPLAY_NAME, updatedUser.getDisplayName());
        assertEquals(NEW_FIRST_NAME, updatedUser.getFirstName());
        assertEquals(NEW_LAST_NAME, updatedUser.getLastName());
        assertEquals(NEW_EMAIL, updatedUser.getEmailAddress());
    }

    public void testUpdateUserNotFound() throws Exception
    {
        UserTemplate userTemplate = new UserTemplate(NON_EXISTENT_USERNAME, 1);
        userTemplate.setActive(true);
        userTemplate.setDisplayName(NEW_DISPLAY_NAME);
        userTemplate.setFirstName(NEW_FIRST_NAME);
        userTemplate.setLastName(NEW_LAST_NAME);
        userTemplate.setEmailAddress(NEW_EMAIL);

        try
        {
            remoteDirectory.updateUser(userTemplate);
            fail("User '" + NON_EXISTENT_USERNAME + "' does not exist, but we found him anyway. That's bad");
        }
        catch (UserNotFoundException e)
        {
            // Good result
        }
    }

    public void testRemoveUser() throws Exception
    {
        intendToModifyData();

        User user = remoteDirectory.findUserByName(TEST_USERNAME);
        assertNotNull(user);

        remoteDirectory.removeUser(TEST_USERNAME);

        // Really check he is gone.
        try
        {
            remoteDirectory.findUserWithAttributesByName(TEST_USERNAME);
            fail("User '" + TEST_USERNAME + "' was removed, but we found him anyway. That's bad");
        }
        catch (UserNotFoundException e)
        {
            // Good result
        }
    }

    public void testRemoveUserBadUser() throws Exception
    {
        try
        {
            remoteDirectory.removeUser(NON_EXISTENT_USERNAME);
            fail("User '" + NON_EXISTENT_USERNAME + "' does not exist, but we found him anyway. That's bad");
        }
        catch (UserNotFoundException e)
        {
            // Good result
        }
        UserWithAttributes userWithAttributes = remoteDirectory.findUserWithAttributesByName(TEST_USERNAME);
        assertEquals(TEST_USERNAME, userWithAttributes.getName());
    }

    public void testAuthenticate() throws Exception
    {
        User user = remoteDirectory.findUserByName(TEST_USERNAME);
        assertNotNull(user);
        assertEquals(TEST_USERNAME, user.getName());

        User authUser = remoteDirectory.authenticate(user.getName(), PasswordCredential.unencrypted(TEST_USER_PASSWORD));
        assertEquals(user, authUser);
    }

    public void testAuthenticateBadPassword() throws Exception
    {
        User user = remoteDirectory.findUserByName(TEST_USERNAME);
        assertNotNull(user);
        assertEquals(TEST_USERNAME, user.getName());

        try
        {
            remoteDirectory.authenticate(user.getName(), PasswordCredential.unencrypted(BAD_USER_PASSWORD));
            fail("Should not have authenticated user with a wrong password");
        }
        catch (InvalidAuthenticationException e)
        {
            // Correct behaviour
        }
    }

    public void testAuthenticateNoUser() throws Exception
    {
        try
        {
            remoteDirectory.authenticate(NON_EXISTENT_USERNAME, PasswordCredential.unencrypted(TEST_USER_PASSWORD));
            fail("Authenticating non-existent user should have resulted in an UserNotFoundException being thrown");
        }
        catch (UserNotFoundException e)
        {
            // Correct behaviour
        }
    }

    public void testUpdateUserPassword() throws Exception
    {
        intendToModifyData();

        User user = remoteDirectory.findUserByName(TEST_USERNAME);
        assertNotNull(user);
        assertEquals(TEST_USERNAME, user.getName());

        remoteDirectory.updateUserCredential(user.getName(), PasswordCredential.unencrypted(NEW_PASSWORD));

        // Assert we can authenticate with the updated credentials
        remoteDirectory.authenticate(user.getName(), PasswordCredential.unencrypted(NEW_PASSWORD));

        // Should not be able to authenticate with old credentials
        try
        {
            remoteDirectory.authenticate(user.getName(), PasswordCredential.unencrypted(TEST_USER_PASSWORD));
            fail("Should not be able to authenticate user with old password");
        }
        catch (InvalidAuthenticationException e)
        {
            // Correct behaviour
        }
    }

    public void testStoreUserAttributes() throws Exception
    {
        intendToModifyData();

        User user = remoteDirectory.findUserByName(TEST_USERNAME);
        assertNotNull(user);
        assertEquals(TEST_USERNAME, user.getName());

        HashMap<String, Set<String>> userAttributes = new HashMap<String, Set<String>>();
        userAttributes.put("colour", Sets.newHashSet("blue"));
        userAttributes.put("phone-number", Sets.newHashSet("555-555-555"));
        userAttributes.put("preferred-beers", Sets.newHashSet("vb", "xxxx", "crown"));

        remoteDirectory.storeUserAttributes(user.getName(), userAttributes);

        UserWithAttributes userWithAttributes = remoteDirectory.findUserWithAttributesByName(TEST_USERNAME);
        assertEquals("blue", userWithAttributes.getValue("colour"));
        assertEquals("555-555-555", userWithAttributes.getValue("phone-number"));
        Set<String> beers = userWithAttributes.getValues("preferred-beers");
        assertEquals(3, beers.size());
        assertTrue(beers.contains("vb"));
        assertTrue(beers.contains("xxxx"));
        assertTrue(beers.contains("crown"));
    }

    public void testStoreUserAttributesEmptySet() throws Exception
    {
        intendToModifyData();

        UserWithAttributes userWithAttributes = remoteDirectory.findUserWithAttributesByName(TEST_USERNAME);
        assertEquals("false", userWithAttributes.getValue("requiresPasswordChange"));

        HashMap<String, Set<String>> userAttributes = new HashMap<String, Set<String>>();

        remoteDirectory.storeUserAttributes(TEST_USERNAME, userAttributes);

        userWithAttributes = remoteDirectory.findUserWithAttributesByName(TEST_USERNAME);
        assertEquals("false", userWithAttributes.getValue("requiresPasswordChange"));
        assertNull(userWithAttributes.getValue("colour"));
        assertNull(userWithAttributes.getValue("phone-number"));
        assertNull(userWithAttributes.getValues("preferred-beers"));
    }

    public void testStoreUserAttributesBadUser() throws OperationFailedException
    {
        HashMap<String, Set<String>> userAttributes = new HashMap<String, Set<String>>();
        userAttributes.put("colour", Sets.newHashSet("blue"));
        userAttributes.put("phone-number", Sets.newHashSet("555-555-555"));
        userAttributes.put("preferred-beers", Sets.newHashSet("vb", "xxxx", "crown"));

        try
        {
            remoteDirectory.storeUserAttributes(NON_EXISTENT_USERNAME, userAttributes);
            fail("User '" + NON_EXISTENT_USERNAME + "' does not exist, but we found him anyway. That's bad");
        }
        catch (UserNotFoundException e)
        {
            // Good result
        }
    }

    public void testRemoveUserAttributes() throws Exception
    {
        intendToModifyData();

        UserWithAttributes userWithAttributes = remoteDirectory.findUserWithAttributesByName(TEST_USERNAME);
        assertEquals("false", userWithAttributes.getValue("requiresPasswordChange"));

        remoteDirectory.removeUserAttributes(TEST_USERNAME, "requiresPasswordChange");

        userWithAttributes = remoteDirectory.findUserWithAttributesByName(TEST_USERNAME);
        assertNull(userWithAttributes.getValue("requiresPasswordChange"));
    }

    public void testRemoveUserAttributesBadUser() throws Exception
    {
        try
        {
            remoteDirectory.removeUserAttributes(NON_EXISTENT_USERNAME, "requiresPasswordChange");
            fail("User '" + NON_EXISTENT_USERNAME + "' does not exist, but we found him anyway. That's bad");
        }
        catch (UserNotFoundException e)
        {
            // Good result
        }

        UserWithAttributes userWithAttributes = remoteDirectory.findUserWithAttributesByName(TEST_USERNAME);
        assertNotNull(userWithAttributes);
    }

    public void testRemoveUserAttributesBadAttribute() throws Exception
    {
        remoteDirectory.removeUserAttributes(TEST_USERNAME, "badattribute");
        UserWithAttributes userWithAttributes = remoteDirectory.findUserWithAttributesByName(TEST_USERNAME);
        assertNotNull(userWithAttributes);
    }

    // GROUP tests
    public void testFindGroupByName() throws Exception
    {
        Group group = remoteDirectory.findGroupByName(TEST_GROUP);
        assertNotNull(group);
        assertEquals(TEST_GROUP, group.getName());

    }

    public void testFindGroupByNameNotFound() throws OperationFailedException
    {
        try
        {
            remoteDirectory.findGroupByName(NON_EXISTENT_GROUP);
            fail("Group '" + NON_EXISTENT_GROUP + "' does not exist, but we found him anyway. That's bad");
        }
        catch (GroupNotFoundException e)
        {
            // Good result
        }
    }

    public void testFindGroupWithAttributesByName() throws Exception
    {
        GroupWithAttributes group = remoteDirectory.findGroupWithAttributesByName(TEST_GROUP);
        assertNotNull(group);
        assertEquals(TEST_GROUP, group.getName());
        assertNotNull(group.getValue("secret-location"));
        assertEquals("hollow", group.getValue("secret-location"));
    }

    public void testFindGroupWithAttributesByNameNotFound() throws OperationFailedException
    {
        try
        {
            remoteDirectory.findGroupWithAttributesByName(NON_EXISTENT_GROUP);
            fail("Group '" + NON_EXISTENT_GROUP + "' does not exist, but we found him anyway. That's bad");
        }
        catch (GroupNotFoundException e)
        {
            // Good result
        }
    }

    public void testAddGroup() throws Exception
    {
        intendToModifyData();

        GroupTemplate groupTemplate = new GroupTemplate(NEW_GROUP, 1);
        groupTemplate.setActive(true);
        groupTemplate.setDescription(NEW_GROUP_DESCRIPTION);
        groupTemplate.setType(GroupType.GROUP);

        Group group = remoteDirectory.addGroup(groupTemplate);
        assertNotNull(group);
        assertEquals(NEW_GROUP, group.getName());

        // We should be able to retrieve the group

        GroupWithAttributes addedGroup = remoteDirectory.findGroupWithAttributesByName(NEW_GROUP);
        assertNotNull(addedGroup);
        assertEquals(NEW_GROUP, addedGroup.getName());
        assertEquals(NEW_GROUP_DESCRIPTION, addedGroup.getDescription());
        assertEquals(GroupType.GROUP, addedGroup.getType());

    }

    public void testAddGroupDuplicate() throws Exception
    {
        GroupTemplate groupTemplate = new GroupTemplate(NEW_GROUP, 1);
        groupTemplate.setActive(true);
        groupTemplate.setDescription(NEW_GROUP_DESCRIPTION);
        groupTemplate.setType(GroupType.GROUP);

        Group group = remoteDirectory.addGroup(groupTemplate);
        assertNotNull(group);
        assertEquals(NEW_GROUP, group.getName());

        try
        {
            group = remoteDirectory.addGroup(groupTemplate);
            fail("Should not be able to create a group with the same name as one that already exists");
        }
        catch (InvalidGroupException e)
        {
            // Good result
        }
    }

    public void testUpdateGroup() throws Exception
    {
        intendToModifyData();

        Group group = remoteDirectory.findGroupByName(TEST_GROUP);
        assertNotNull(group);
        assertEquals(TEST_GROUP, group.getName());

        GroupTemplate groupTemplate = new GroupTemplate(group);
        groupTemplate.setDescription("Updated Group via REST");

        Group updatedGroup = remoteDirectory.updateGroup(groupTemplate);

        // We should be able to retrieve the group

        updatedGroup = remoteDirectory.findGroupWithAttributesByName(TEST_GROUP);
        assertNotNull(group);
        assertEquals(TEST_GROUP, updatedGroup.getName());
        assertEquals("Updated Group via REST", updatedGroup.getDescription());
    }

    public void testUpdateGroupNotFound() throws Exception
    {
        GroupTemplate groupTemplate = new GroupTemplate(NON_EXISTENT_GROUP, 1);
        groupTemplate.setDescription("Updated Group via REST");

        // We should be able to retrieve the group
        try
        {
            remoteDirectory.updateGroup(groupTemplate);
            fail("Group '" + NON_EXISTENT_GROUP + "' does not exist, but we found him anyway. That's bad");
        }
        catch (GroupNotFoundException e)
        {
            // Good result
        }
    }

    public void testRemoveGroup() throws Exception
    {
        intendToModifyData();

        Group group = remoteDirectory.findGroupByName(TEST_GROUP);
        assertEquals(TEST_GROUP, group.getName());

        remoteDirectory.removeGroup(TEST_GROUP);

        // Really check he is gone.
        try
        {
            group = remoteDirectory.findGroupWithAttributesByName(TEST_GROUP);
            fail("Group '" + TEST_GROUP + "' was removed, but we found him anyway. That's bad");
        }
        catch (GroupNotFoundException e)
        {
            // Good result
        }
    }

    public void testRemoveGroupBadGroup() throws Exception
    {
        try
        {
            remoteDirectory.removeGroup(NON_EXISTENT_GROUP);
            fail("Group '" + NON_EXISTENT_GROUP + "' does not exist, but we found him anyway. That's bad");
        }
        catch (GroupNotFoundException e)
        {
            // Good result
        }
        GroupWithAttributes groupWithAttributes = remoteDirectory.findGroupWithAttributesByName(TEST_GROUP);
        assertEquals(TEST_GROUP, groupWithAttributes.getName());
    }

    public void testStoreGroupAttributes() throws Exception
    {
        intendToModifyData();

        Group group = remoteDirectory.findGroupByName(TEST_GROUP);
        assertNotNull(group);
        assertEquals(TEST_GROUP, group.getName());

        HashMap<String, Set<String>> groupAttributes = new HashMap<String, Set<String>>();
        groupAttributes.put("colour", Sets.newHashSet("blue"));
        groupAttributes.put("phone-number", Sets.newHashSet("555-555-555"));
        groupAttributes.put("preferred-beers", Sets.newHashSet("vb", "xxxx", "crown"));

        remoteDirectory.storeGroupAttributes(group.getName(), groupAttributes);

        GroupWithAttributes groupWithAttributes = remoteDirectory.findGroupWithAttributesByName(TEST_GROUP);
        assertEquals("blue", groupWithAttributes.getValue("colour"));
        assertEquals("555-555-555", groupWithAttributes.getValue("phone-number"));
        Set<String> beers = groupWithAttributes.getValues("preferred-beers");
        assertEquals(3, beers.size());
        assertTrue(beers.contains("vb"));
        assertTrue(beers.contains("xxxx"));
        assertTrue(beers.contains("crown"));
    }

    public void testStoreGroupAttributesEmptySet() throws Exception
    {
        intendToModifyData();

        GroupWithAttributes groupWithAttributes = remoteDirectory.findGroupWithAttributesByName(TEST_GROUP);
        assertEquals("hollow", groupWithAttributes.getValue("secret-location"));

        HashMap<String, Set<String>> groupAttributes = new HashMap<String, Set<String>>();

        remoteDirectory.storeGroupAttributes(TEST_GROUP, groupAttributes);

        groupWithAttributes = remoteDirectory.findGroupWithAttributesByName(TEST_GROUP);
        assertEquals("hollow", groupWithAttributes.getValue("secret-location"));
        assertNull(groupWithAttributes.getValue("colour"));
        assertNull(groupWithAttributes.getValue("phone-number"));
        assertNull(groupWithAttributes.getValues("preferred-beers"));
    }

    public void testStoreGroupAttributesBadGroup() throws OperationFailedException
    {
        HashMap<String, Set<String>> groupAttributes = new HashMap<String, Set<String>>();
        groupAttributes.put("colour", Sets.newHashSet("blue"));
        groupAttributes.put("phone-number", Sets.newHashSet("555-555-555"));
        groupAttributes.put("preferred-beers", Sets.newHashSet("vb", "xxxx", "crown"));

        try
        {
            remoteDirectory.storeGroupAttributes(NON_EXISTENT_GROUP, groupAttributes);
            fail("Group '" + NON_EXISTENT_GROUP + "' does not exist, but we found him anyway. That's bad");
        }
        catch (GroupNotFoundException e)
        {
            // Good result
        }
    }

    public void testRemoveGroupAttributes() throws Exception
    {
        intendToModifyData();

        GroupWithAttributes groupWithAttributes = remoteDirectory.findGroupWithAttributesByName(TEST_GROUP);
        assertEquals("hollow", groupWithAttributes.getValue("secret-location"));

        remoteDirectory.removeGroupAttributes(TEST_GROUP, "secret-location");

        groupWithAttributes = remoteDirectory.findGroupWithAttributesByName(TEST_GROUP);
        assertNull(groupWithAttributes.getValue("secret-location"));
    }

    public void testRemoveGroupAttributesBadGroup() throws Exception
    {
        try
        {
            remoteDirectory.removeGroupAttributes(NON_EXISTENT_GROUP, "secret-location");
            fail("Group '" + NON_EXISTENT_GROUP + "' does not exist, but we found him anyway. That's bad");
        }
        catch (GroupNotFoundException e)
        {
            // Good result
        }
        GroupWithAttributes groupWithAttributes = remoteDirectory.findGroupWithAttributesByName(TEST_GROUP);
        assertEquals(TEST_GROUP, groupWithAttributes.getName());
    }

    public void testRemoveGroupAttributesBadAttribute() throws Exception
    {
        remoteDirectory.removeGroupAttributes(TEST_GROUP, "badattribute");
        GroupWithAttributes groupWithAttributes = remoteDirectory.findGroupWithAttributesByName(TEST_GROUP);
        assertEquals(TEST_GROUP, groupWithAttributes.getName());
    }

    // Memberships
    public void testIsUserDirectMemberOfGroup() throws OperationFailedException
    {
        boolean found = remoteDirectory.isUserDirectGroupMember(TEST_USERNAME, TEST_GROUP);

        assertTrue(found);
    }

    public void testAddUserToGroup() throws Exception
    {
        intendToModifyData();

        Group group = remoteDirectory.findGroupByName("crowd-administrators");
        assertNotNull(group);
        User user = remoteDirectory.findUserByName(TEST_USERNAME);
        assertNotNull(user);

        remoteDirectory.addUserToGroup(user.getName(), group.getName());

        // Check the membership exists
        assertTrue(remoteDirectory.isUserDirectGroupMember(TEST_USERNAME, "crowd-administrators"));
    }

    public void testAddUserToGroupDuplicate() throws Exception
    {
        intendToModifyData();

        Group group = remoteDirectory.findGroupByName("crowd-administrators");
        assertNotNull(group);
        User user = remoteDirectory.findUserByName(TEST_USERNAME);
        assertNotNull(user);

        remoteDirectory.addUserToGroup(user.getName(), group.getName());
        // Check the membership exists
        assertTrue(remoteDirectory.isUserDirectGroupMember(TEST_USERNAME, "crowd-administrators"));

        try
        {
            // Now again
            remoteDirectory.addUserToGroup(user.getName(), group.getName());
            fail("MembershipAlreadyExistsException expected");
        }
        catch (MembershipAlreadyExistsException e)
        {
            // expected
        }
        // Check the membership exists
        assertTrue(remoteDirectory.isUserDirectGroupMember(TEST_USERNAME, "crowd-administrators"));
    }

    public void testAddUserToGroupBadGroup() throws Exception
    {
        try
        {
            remoteDirectory.addUserToGroup(TEST_USERNAME, NON_EXISTENT_GROUP);
            fail("Group '" + NON_EXISTENT_GROUP + "' does not exist, but we added the membership anyway. That's bad");
        }
        catch (GroupNotFoundException e)
        {
            // Good result
        }
    }

    public void testAddUserToGroupBadUser() throws Exception
    {
        try
        {
            remoteDirectory.addUserToGroup(NON_EXISTENT_USERNAME, "crowd-administrators");
            fail("User '" + NON_EXISTENT_USERNAME + "' does not exist, but we added the membership anyway. That's bad");
        }
        catch (UserNotFoundException e)
        {
            // Good result
        }
    }

    public void testIsGroupDirectMemberOfGroup() throws OperationFailedException
    {
        boolean found = remoteDirectory.isGroupDirectGroupMember(TEST_GROUP, "crowd-users");

        assertTrue(found);
    }

    public void testAddGroupToGroup() throws Exception
    {
        intendToModifyData();

        Group parent = remoteDirectory.findGroupByName("crowd-administrators");
        assertNotNull(parent);
        Group child = remoteDirectory.findGroupByName(TEST_GROUP);
        assertNotNull(child);

        remoteDirectory.addGroupToGroup(child.getName(), parent.getName());

        // Check the membership exists
        assertTrue(remoteDirectory.isGroupDirectGroupMember(TEST_GROUP, "crowd-administrators"));
    }

    public void testAddGroupToGroupDuplicate() throws Exception
    {
        intendToModifyData();

        Group parent = remoteDirectory.findGroupByName("crowd-administrators");
        assertNotNull(parent);
        Group child = remoteDirectory.findGroupByName(TEST_GROUP);
        assertNotNull(child);

        remoteDirectory.addGroupToGroup(child.getName(), parent.getName());

        // Check the membership exists
        assertTrue(remoteDirectory.isGroupDirectGroupMember(TEST_GROUP, "crowd-administrators"));

        try
        {
            // Now again
            remoteDirectory.addGroupToGroup(child.getName(), parent.getName());
            fail("MembershipAlreadyExistsException expected");
        }
        catch (MembershipAlreadyExistsException e)
        {
            // expected
        }
        // Check the membership exists
        assertTrue(remoteDirectory.isGroupDirectGroupMember(TEST_GROUP, "crowd-administrators"));
    }

    public void testAddGroupToGroupBadParent() throws Exception
    {
        try
        {
            remoteDirectory.addGroupToGroup("crowd-administrators", NON_EXISTENT_GROUP);
            fail("Group '" + NON_EXISTENT_GROUP + "' does not exist, but we added the membership anyway. That's bad");
        }
        catch (GroupNotFoundException e)
        {
            // Good result
        }
    }

    public void testAddGroupToGroupBadChild() throws Exception
    {
        try
        {
            remoteDirectory.addGroupToGroup(NON_EXISTENT_GROUP, TEST_GROUP);
            fail("Group '" + NON_EXISTENT_GROUP + "' does not exist, but we added the membership anyway. That's bad");
        }
        catch (GroupNotFoundException e)
        {
            // Good result
        }
    }

    public void testRemoveUserFromGroup() throws Exception
    {
        intendToModifyData();

        assertTrue(remoteDirectory.isUserDirectGroupMember(TEST_USERNAME, TEST_GROUP));

        Group group = remoteDirectory.findGroupByName(TEST_GROUP);
        assertNotNull(group);
        User user = remoteDirectory.findUserByName(TEST_USERNAME);
        assertNotNull(user);

        remoteDirectory.removeUserFromGroup(user.getName(), group.getName());

        // Check the membership exists
        assertFalse(remoteDirectory.isUserDirectGroupMember(TEST_USERNAME, TEST_GROUP));
    }


    public void testRemoveUserFromGroupBadUser() throws Exception
    {
        try
        {
            remoteDirectory.removeUserFromGroup(NON_EXISTENT_USERNAME, TEST_GROUP);
            fail("User '" + NON_EXISTENT_USERNAME + "' does not exist, but we added the membership anyway. That's bad");
        }
        catch (UserNotFoundException e)
        {
            // Good result
        }
    }

    public void testRemoveUserFromGroupBadGroup() throws Exception
    {
        try
        {
            remoteDirectory.removeUserFromGroup(TEST_USERNAME, NON_EXISTENT_GROUP);
            fail("Group '" + NON_EXISTENT_GROUP + "' does not exist, but we added the membership anyway. That's bad");
        }
        catch (GroupNotFoundException e)
        {
            // Good result
        }
    }

    public void testRemoveGroupFromGroup() throws Exception
    {
        intendToModifyData();

        assertTrue(remoteDirectory.isGroupDirectGroupMember(TEST_GROUP, "crowd-users"));

        Group child = remoteDirectory.findGroupByName(TEST_GROUP);
        assertNotNull(child);
        Group parent = remoteDirectory.findGroupByName("crowd-users");
        assertNotNull(parent);

        remoteDirectory.removeGroupFromGroup(child.getName(), parent.getName());

        // Check the membership no longer exists
        assertFalse(remoteDirectory.isGroupDirectGroupMember(TEST_GROUP, "crowd-users"));
    }

    public void testRemoveGroupFromGroupBadParent() throws Exception
    {
        try
        {
            remoteDirectory.removeGroupFromGroup(TEST_GROUP, NON_EXISTENT_GROUP);
            fail("Group '" + NON_EXISTENT_GROUP + "' does not exist, but we could successfully remove an existing group from it. That's bad");
        }
        catch (MembershipNotFoundException e)
        {
            // Good result
        }
    }

    public void testRemoveGroupFromGroupBadChild() throws Exception
    {
        try
        {
            remoteDirectory.removeGroupFromGroup(NON_EXISTENT_GROUP, "crowd-users");
            fail("Group '" + NON_EXISTENT_GROUP + "' does not exist, but we could successfully remove it from an existing group. That's bad");
        }
        catch (MembershipNotFoundException e)
        {
            // Good result
        }
    }

    static Matcher<Iterable<? extends String>> isNames(Collection<String> names)
    {
        return IsIterableContainingInAnyOrder.containsInAnyOrder(names.toArray(new String[names.size()]));
    }

    public void testSearchAllUsers() throws OperationFailedException
    {
        List<User> users = remoteDirectory.searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).returningAtMost(EntityQuery.MAX_MAX_RESULTS));

        assertThat(namesOf(users), isNames(ALL_USER_NAMES));
    }

    public void testSearchAllUserNames() throws OperationFailedException
    {
        List<String> userNames = remoteDirectory.searchUsers(QueryBuilder.queryFor(String.class, EntityDescriptor.user()).returningAtMost(EntityQuery.MAX_MAX_RESULTS));

        assertThat(userNames, isNames(ALL_USER_NAMES));
    }

    public void testSearchUsers_EmailRestriction() throws OperationFailedException
    {
        List<User> users = remoteDirectory.searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(
            Restriction.on(UserTermKeys.EMAIL).exactlyMatching("bob@example.net")).returningAtMost(EntityQuery.MAX_MAX_RESULTS));

        assertThat(namesOf(users), IsIterableContainingInAnyOrder.containsInAnyOrder(USERNAME1, "regularuser"));
    }

    public void testSearchUsers_ActiveRestriction() throws OperationFailedException
    {
        List<User> users = remoteDirectory.searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                                                           .with(Restriction.on(UserTermKeys.ACTIVE)
                                                                     .exactlyMatching(true))
                                                           .returningAtMost(EntityQuery.MAX_MAX_RESULTS));

        assertThat(namesOf(users),
                   IsIterableContainingInAnyOrder.containsInAnyOrder(USERNAME1, USERNAME2, "penny", "regularuser",
                                                                     "secondadmin", MR_TRAILING_SPACES));
    }

    public void testSearchUsers_InactiveRestriction() throws OperationFailedException
    {
        List<User> users = remoteDirectory.searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                                                           .with(Restriction.on(UserTermKeys.ACTIVE)
                                                                     .exactlyMatching(false))
                                                           .returningAtMost(EntityQuery.MAX_MAX_RESULTS));

        assertThat(namesOf(users), IsEmptyIterable.emptyIterable());
    }

    public void testSearchAllGroups() throws OperationFailedException
    {
        List<Group> groups = remoteDirectory.searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).returningAtMost(EntityQuery.MAX_MAX_RESULTS));

        assertThat(namesOf(groups), isNames(ALL_GROUP_NAMES));
    }

    public void testSearchAllGroupNames() throws OperationFailedException
    {
        List<String> groupNames = remoteDirectory.searchGroups(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).returningAtMost(EntityQuery.MAX_MAX_RESULTS));

        assertThat(groupNames, isNames(ALL_GROUP_NAMES));
    }

    public void testSearchGroups_NameRestriction() throws OperationFailedException
    {
        List<Group> groups = remoteDirectory.searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).with(Restriction.on(GroupTermKeys.NAME).exactlyMatching(ALL_GROUP_NAMES.get(0))).returningAtMost(EntityQuery.MAX_MAX_RESULTS));

        assertThat(namesOf(groups), IsIterableContainingInAnyOrder.containsInAnyOrder(ALL_GROUP_NAMES.get(0)));
    }

    public void testGetUserMembersOfGroup() throws OperationFailedException
    {
        MembershipQuery<User> query = QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(GROUP1).returningAtMost(EntityQuery.ALL_RESULTS);
        List<User> users = remoteDirectory.searchGroupRelationships(query);

        assertThat(namesOf(users), IsIterableContainingInAnyOrder.containsInAnyOrder(USERNAME1, USERNAME2));

        query = QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(GROUP4).returningAtMost(EntityQuery.ALL_RESULTS);
        users = remoteDirectory.searchGroupRelationships(query);

        assertThat(users, IsEmptyCollection.empty());
    }

    public void testGetUserNameMembersOfGroup() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(GROUP1).returningAtMost(EntityQuery.ALL_RESULTS);
        List<String> users = remoteDirectory.searchGroupRelationships(query);

        assertThat(users, IsIterableContainingInAnyOrder.containsInAnyOrder(USERNAME1, USERNAME2));

        query = QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(GROUP4).returningAtMost(EntityQuery.ALL_RESULTS);
        users = remoteDirectory.searchGroupRelationships(query);

        assertThat(users, IsEmptyCollection.empty());
    }

    public void testGetGroupMembersOfGroup() throws OperationFailedException
    {
        MembershipQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(GROUP2).returningAtMost(EntityQuery.ALL_RESULTS);
        List<Group> groups = remoteDirectory.searchGroupRelationships(query);

        assertThat(namesOf(groups), IsIterableContainingInAnyOrder.containsInAnyOrder(GROUP3));

        query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(GROUP4).returningAtMost(EntityQuery.ALL_RESULTS);
        groups = remoteDirectory.searchGroupRelationships(query);

        assertThat(namesOf(groups), IsIterableContainingInAnyOrder.containsInAnyOrder(GROUP1));
    }

    public void testGetGroupNameMembersOfGroup() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(GROUP2).returningAtMost(EntityQuery.ALL_RESULTS);
        List<String> groups = remoteDirectory.searchGroupRelationships(query);

        assertThat(groups, IsIterableContainingInAnyOrder.containsInAnyOrder(GROUP3));

        query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(GROUP4).returningAtMost(EntityQuery.ALL_RESULTS);
        groups = remoteDirectory.searchGroupRelationships(query);

        assertThat(groups, IsIterableContainingInAnyOrder.containsInAnyOrder(GROUP1));
    }

    public void testGroupMembershipsForUser() throws OperationFailedException
    {
        MembershipQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(USERNAME1).returningAtMost(EntityQuery.ALL_RESULTS);
        List<Group> groups = remoteDirectory.searchGroupRelationships(query);

        assertThat(namesOf(groups), IsIterableContainingInAnyOrder.containsInAnyOrder(GROUP1, GROUP2));

        query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(USERNAME2).returningAtMost(EntityQuery.ALL_RESULTS);
        groups = remoteDirectory.searchGroupRelationships(query);

        assertThat(namesOf(groups), IsIterableContainingInAnyOrder.containsInAnyOrder(GROUP1));
    }

    public void testGroupNameMembershipsForUser() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(USERNAME1).returningAtMost(EntityQuery.ALL_RESULTS);
        List<String> groups = remoteDirectory.searchGroupRelationships(query);

        assertThat(groups, IsIterableContainingInAnyOrder.containsInAnyOrder(GROUP1, GROUP2));

        query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(USERNAME2).returningAtMost(EntityQuery.ALL_RESULTS);
        groups = remoteDirectory.searchGroupRelationships(query);

        assertThat(groups, IsIterableContainingInAnyOrder.containsInAnyOrder(GROUP1));
    }

    public void testGroupMembershipsForGroup() throws OperationFailedException
    {
        MembershipQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.group()).withName(GROUP1).returningAtMost(EntityQuery.ALL_RESULTS);
        List<Group> groups = remoteDirectory.searchGroupRelationships(query);

        assertThat(namesOf(groups), IsIterableContainingInAnyOrder.containsInAnyOrder(GROUP3, GROUP4));

        query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.group()).withName(GROUP3).returningAtMost(EntityQuery.ALL_RESULTS);
        groups = remoteDirectory.searchGroupRelationships(query);

        assertThat(namesOf(groups), IsIterableContainingInAnyOrder.containsInAnyOrder(GROUP2));
    }

    public void testGroupNameMembershipsForGroup() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.group()).withName(GROUP1).returningAtMost(EntityQuery.ALL_RESULTS);
        List<String> groups = remoteDirectory.searchGroupRelationships(query);

        assertThat(groups, IsIterableContainingInAnyOrder.containsInAnyOrder(GROUP3, GROUP4));

        query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.group()).withName(GROUP3).returningAtMost(EntityQuery.ALL_RESULTS);
        groups = remoteDirectory.searchGroupRelationships(query);

        assertThat(groups, IsIterableContainingInAnyOrder.containsInAnyOrder(GROUP2));
    }

    public void testNestedGroupSupport()
    {
        Map<String, String> originalMap = Collections.unmodifiableMap(getAttributeMap(remoteDirectory));

        try
        {
            Map<String, String> nonNestedGroupMap = new HashMap<String, String>(originalMap);
            nonNestedGroupMap.put(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS, Boolean.FALSE.toString());
            remoteDirectory.setAttributes(nonNestedGroupMap);
            assertFalse(remoteDirectory.supportsNestedGroups());

            Map<String, String> nestedGroupMap = new HashMap<String, String>(originalMap);
            nestedGroupMap.put(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS, Boolean.TRUE.toString());
            remoteDirectory.setAttributes(nestedGroupMap);
            assertTrue(remoteDirectory.supportsNestedGroups());
        }
        finally
        {
            remoteDirectory.setAttributes(originalMap);
        }
    }

    private static Map<String, String> getAttributeMap(RemoteCrowdDirectory directory)
    {
        final Map<String, String> map = new HashMap<String, String>();

        for(String key:directory.getKeys())
        {
            map.put(key, directory.getValue(key));
        }

        return map;
    }

    static Iterable<String> namesOf(Iterable<? extends DirectoryEntity> entities)
    {
        Collection<String> names = new ArrayList<String>();

        for (DirectoryEntity e : entities)
        {
            names.add(e.getName());
        }

        return names;
    }
 }
