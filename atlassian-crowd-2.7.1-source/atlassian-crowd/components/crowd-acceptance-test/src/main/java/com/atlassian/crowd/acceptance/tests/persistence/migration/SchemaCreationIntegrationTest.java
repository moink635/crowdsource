package com.atlassian.crowd.acceptance.tests.persistence.migration;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.dao.alias.AliasDAO;
import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.dao.property.PropertyDAO;
import com.atlassian.crowd.dao.token.TokenDAOHibernate;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.GroupDao;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.util.persistence.hibernate.SchemaHelper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;

/**
 * This test is HSQLDB-specific because it uses a HSQLDB-specific statement
 * to delete any existing previous schema.
 * The DirtiesContext annotation ensures that the Spring context created for this
 * test is not reused by any other test (such as {@link SchemaUpgradeIntegrationTest}).
 * Each test class requires a fresh instance of the HSQL database, a fresh instance
 * of the SessionFactory and a reset of the auto-increment ID generators.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext-schemahelper-config.xml",
                                   "classpath:/applicationContext-CrowdDAO.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class})
@TransactionConfiguration(defaultRollback = true)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class SchemaCreationIntegrationTest extends AbstractDaoIntegrationTest
{
    private static final Logger logger = LoggerFactory.getLogger(SchemaCreationIntegrationTest.class);

    @Inject private SchemaHelper schemaHelper;
    @Inject private DataSource dataSource;

    @Inject private TokenDAOHibernate tokenDao;
    @Inject private ApplicationDAO applicationDao;
    @Inject private UserDao userDao;
    @Inject private GroupDao groupDao;
    @Inject private MembershipDao membershipDao;
    @Inject private DirectoryDao directoryDao;
    @Inject private AliasDAO aliasDao;
    @Inject private PropertyDAO propertyDao;

    /**
     * The database schema must be created before a transaction is started, because
     * otherwise Hibernate's SessionFactory will initialise it for us (or
     * validate it, depending on how it is configured).
     * @throws Exception
     */
    @BeforeTransaction
    public void createSchema() throws Exception
    {
        logger.info("Initialising empty database");
        assertNotNull("Data source must have been injected", dataSource);
        SimpleJdbcTemplate template = new SimpleJdbcTemplate(dataSource);
        template.update("DROP SCHEMA PUBLIC CASCADE"); // HSQLDB-specific
        logger.info("Database initialised");

        logger.info("Creating schema");
        schemaHelper.createSchema();
        logger.info("Schema created");
    }

    /**
     * DAOs must be run within a transaction.
     * @throws Exception
     */
    @Test
    @Transactional
    public void testDaosCanInsertNewDataAfterSchemaCreation() throws Exception
    {
        // order is important due to referential integrity
        _testDirectoryDaoCanInsert();
        _testUserDaoCanInsert();
        _testGroupDaoCanInsert();
        _testMembershipDaoCanInsert();
        _testApplicationDaoCanInsert();
        _testAliasDaoCanInsert();
        _testTokenDaoCanInsert();
        _testPropertyDaoCanInsert();
    }

    @Override
    public TokenDAOHibernate getTokenDao()
    {
        return tokenDao;
    }

    @Override
    public ApplicationDAO getApplicationDao()
    {
        return applicationDao;
    }

    @Override
    public UserDao getUserDao()
    {
        return userDao;
    }

    @Override
    public GroupDao getGroupDao()
    {
        return groupDao;
    }

    @Override
    public MembershipDao getMembershipDao()
    {
        return membershipDao;
    }

    @Override
    public DirectoryDao getDirectoryDao()
    {
        return directoryDao;
    }

    @Override
    public AliasDAO getAliasDao()
    {
        return aliasDao;
    }

    @Override
    public PropertyDAO getPropertyDao()
    {
        return propertyDao;
    }
}
