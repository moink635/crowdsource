package com.atlassian.crowd.acceptance.tests.applications.crowd.plugin;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;

public class ViewApplicationPluginTest extends CrowdAcceptanceTestCase
{
    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        _loginAdminUser();
        restoreCrowdFromXML("googletest.xml");
    }

    public void testViewPluginApplication()
    {
        log("Running testViewPluginApplication");

        gotoViewApplication("google-apps");

        // top tab
        assertElementPresent("topnavBrowseApplication");

        // left tabs
        assertElementPresent("browse-application");
        assertElementPresent("add-application");
        assertElementNotPresent("remove-application"); // can't delete

        // we're on application-details => no link => no id.
        assertElementPresent("application-directories");
        assertElementPresent("application-groups");
        assertElementPresent("application-users");
        assertElementPresent("application-permissions");
        assertElementNotPresent("application-remoteaddresses"); // this is the remote addresses tab
        assertElementPresent("application-authtest");
        assertElementPresent("application-options");
        assertElementPresent("samlConfigTab"); // plugin tab
    }

    public void testViewPluginConfiguration()
    {
        log("Running testViewPluginConfiguration");

        gotoViewApplication("google-apps");
        clickLink("samlConfigTab");

        // top tab
        assertElementPresent("topnavBrowseApplication");

        // left tabs
        assertElementPresent("browse-application");
        assertElementPresent("add-application");
        assertElementNotPresent("remove-application"); // can't delete

        assertElementPresent("application-details");
        assertElementPresent("application-directories");
        assertElementPresent("application-groups");
        assertElementPresent("application-users");
        assertElementPresent("application-permissions");
        assertElementNotPresent("application-remoteaddresses"); // this is the remote addresses tab
        assertElementPresent("application-authtest");
        assertElementPresent("application-options");
        
        assertKeyPresent("saml.description.text");
    }

    public void testUpdateApplicationPlugin()
    {
        log("Running testUpdateApplicationPlugin");

        gotoViewApplication("google-apps");

        String description = "bogus";
        setTextField("applicationDescription", description);
        uncheckCheckbox("active");

        submit();

        assertTextFieldEquals("applicationDescription", description);
        assertCheckboxNotSelected("active");
        assertTextInElement("application-name", "google-apps");
    }
}
