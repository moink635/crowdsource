package com.atlassian.crowd.acceptance.tests.client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Properties;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.service.soap.client.SecurityServerClientImpl;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl;

import com.google.common.collect.ImmutableList;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Inspects the Crowd Client Headers for a single user-agent header.  This is to ensure the method we're
 * changing the default user-agent with doesn't break in future XFire libraries.
 * <p/>
 * We intercept and send off the exact headers to the Crowd Server and ensure that content-encoding header is gzip enabled.
 * <p/>
 * AuthenticateApplicationListener is the server socket that will always return a dummy SOAP message
 * with successful result to a Crowd Client's AuthenticateApplication request.
 * <p/>
 * The port for server socket that intercepts the headers is hard-coded in listenerPort.
 */
public class XFireGzipTest extends CrowdAcceptanceTestCase
{
    public class AuthenticateApplicationProxy implements Runnable
    {
        protected final ServerSocket connection;
        protected Socket clientSocket;
        protected final List<String> headers = Collections.synchronizedList(new ArrayList<String>());
        protected final String authAppSOAPResponse = "HTTP/1.1 200 OK\n" + "Content-Type: text/xml;charset=UTF-8\n" + "\n" +

                "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"><soap:Body><ns1:authenticateApplicationResponse xmlns:ns1=\"urn:SecurityServer\"><ns1:out><name xmlns=\"http://authentication.integration.crowd.atlassian.com\">bamboo-connect</name><token xmlns=\"http://authentication.integration.crowd.atlassian.com\">qkzPvczkQgyYdNwYSzk1jw00</token></ns1:out></ns1:authenticateApplicationResponse></soap:Body></soap:Envelope>";


        public AuthenticateApplicationProxy() throws IOException
        {
            clientSocket = new Socket();
            connection = new ServerSocket();
            connection.bind(null);
        }

        public int getListenerPort()
        {
            return connection.getLocalPort();
        }

        public List<String> getHeaders()
        {
            return ImmutableList.copyOf(headers);
        }

        public void run()
        {
            DataInputStream in = null;
            PrintStream out = null;
            BufferedReader inputReader = null;

            try
            {
                clientSocket = connection.accept();
                in = new DataInputStream(clientSocket.getInputStream());
                out = new PrintStream(clientSocket.getOutputStream());
                inputReader = new BufferedReader(new InputStreamReader(in));

                // consume the first line of the HTTP request
                String line = inputReader.readLine();

                // Capture the Crowd Client Request Headers
                while (StringUtils.isNotBlank(line = inputReader.readLine()))
                {
                    headers.add(line);
                }

                // Return a successful AuthenticateApplication message to Crowd Client so that it doesn't throw an Exception
                out.println(authAppSOAPResponse);
            }
            catch (IOException ioEx)
            {
                ioEx.printStackTrace();
                fail(ioEx.getMessage());
            }
            finally
            {
                IOUtils.closeQuietly(inputReader);
                IOUtils.closeQuietly(out);
                IOUtils.closeQuietly(in);

                IOUtils.closeQuietly(clientSocket);

                try
                {
                    connection.close();
                }
                catch (IOException e)
                {
                }
            }
        }
    }

    private AuthenticateApplicationProxy startUpFakeCrowd() throws IOException
    {
        AuthenticateApplicationProxy socketListener = new AuthenticateApplicationProxy();

        // Make a new thread for the serverSocket so it'll listen in the background
        Thread tSocketListener = new Thread(socketListener, "CrowdSocketListener");
        tSocketListener.start();

        return socketListener;
    }

    private SecurityServerClient createCrowdClient(String serverURL)
    {
        // We need to point the Crowd Client towards our ServerSocket port so that we can intercept the headers
        Properties properties = AcceptanceTestHelper.loadProperties("localtest.crowd.properties");
        properties.remove("crowd.server.url");
        properties.setProperty("crowd.server.url", serverURL);
        SoapClientProperties clientProperties = SoapClientPropertiesImpl.newInstanceFromPropertiesWithoutOverrides(properties);

        return new SecurityServerClientImpl(clientProperties);
    }

    /**
     * This will:
     * 1. Sets up socket listener to emulate Crowd Server
     * 2. Makes a call from Crowd Client to fake Crowd Server to obtain User-Agent header
     * 3. Verify only one User-Agent header exists
     * 4. Sends the same headers with a request to the WSDL to the real Crowd Server
     */
    public void testClientHeaders() throws Exception
    {
        log("Running testClientHeaders");

        AuthenticateApplicationProxy socketListener = startUpFakeCrowd();

        String url = "http://localhost:" + socketListener.getListenerPort() + "/crowd/services/";
        SecurityServerClient crowdClient = createCrowdClient(url);

        try
        {
            crowdClient.authenticate();
        }
        catch (InvalidAuthorizationTokenException tokenEx)
        {
            // Could happen if SOAP Response format changes in the future.
            // The headers should still be OK for inspecting GZIP is on.
        }
        catch (Exception e)
        {
            throw new RuntimeException("Client can't authenticate to our socket server at:" + url, e);
        }

        List<String> headerList = socketListener.getHeaders();

        assertFalse("Request should have been recorded", headerList.isEmpty());

        // We want to ensure user-agent is only set once and prepare concatedHeaders for our Server Response test.
        StringBuilder concatedHeadersBuffer = new StringBuilder();
        int countOfUserAgentString = 0;

        for (String headerline : headerList)
        {
            if (headerline.toLowerCase(Locale.ENGLISH).matches("^user-agent:.*"))
            {
                countOfUserAgentString++;
            }
            if (!headerline.toLowerCase(Locale.ENGLISH).matches("^expect:.*"))
            {
                // Remove expect from the headers as we don't want the HTTP Server expecting the HTTP body
                // during the second part of the test.
                concatedHeadersBuffer.append(headerline).append("\n");
            }
        }
        String concatedHeaders = concatedHeadersBuffer.toString();

        assertEquals("User-Agent must appear once only in Headers", 1, countOfUserAgentString);

        // We must now test that the headers against the real Crowd Server.  Pasting code from GzipFilterOptionTest:

        //We first turn on GZIP on the Crowd Server
        addRequestHeader("Accept-Encoding", "gzip");

        // Restart session so that request header will take effect
        closeBrowser();
        beginAt(URL_HOME);

        _loginAdminUser();
        gotoGeneral();
        checkCheckbox("gzip");
        submit();

        // first response will still contain the old setting so reload
        gotoGeneral();
        assertServerResponseContains("\nContent-Encoding: gzip\n");
        assertCheckboxSelected("gzip");


        Socket clientSocket = null;
        DataOutputStream out = null;
        DataInputStream in = null;
        BufferedReader serverResults = null;

        try
        {
            // connect to real Crowd server and delegate the headers with a request for the WSDL
            URL baseURL = getTestContext().getBaseUrl();
            clientSocket = new Socket(baseURL.getHost(), baseURL.getPort());
            out = new DataOutputStream(clientSocket.getOutputStream());
            in = new DataInputStream(clientSocket.getInputStream());
            concatedHeaders = "GET /crowd/services/SecurityServer?wsdl HTTP/1.1\n" + concatedHeaders;

            // send out headers we first intercepted in the "client header check" with a GET request to the WSDL page
            // The WSDL is a good test as the Content-Type is text/xml.
            // Tests for CWD-1398 and CWD-1404.
            log("Client headers generated (expect header stripped): \n" + concatedHeaders);
            out.writeBytes(concatedHeaders + "\n");
            serverResults = new BufferedReader(new InputStreamReader(in));

            boolean gzipEncodingOn = false;

            String serverResponse;
            StringBuilder serverResponseBuffer = new StringBuilder();

            while ((serverResponse = serverResults.readLine()) != null)
            {
                serverResponseBuffer.append(serverResponse).append("\n");
                if (serverResponse.toLowerCase(Locale.ENGLISH).equals("content-encoding: gzip"))
                {
                    gzipEncodingOn = true;
                }
                if (serverResponse.equals(""))
                // no need to test the rest of the body as headers should be received by now
                {
                    break;
                }
            }

            log("Crowd server reponse to headers: \n" + serverResponseBuffer.toString());
            assertEquals("Content Encoding is GZIP enabled", true, gzipEncodingOn);
        }
        finally
        {
            IOUtils.closeQuietly(in);
            IOUtils.closeQuietly(out);
            IOUtils.closeQuietly(serverResults);
            IOUtils.closeQuietly(clientSocket);
        }
    }
}
