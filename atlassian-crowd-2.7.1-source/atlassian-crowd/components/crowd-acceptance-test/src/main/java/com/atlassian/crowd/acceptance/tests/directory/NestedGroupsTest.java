package com.atlassian.crowd.acceptance.tests.directory;

import java.util.List;

import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import com.google.common.collect.ImmutableList;

import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertThat;


public abstract class NestedGroupsTest extends BaseTest
{
    protected final String groupName1 = "group-1";
    protected final String groupName2 = "group-2";
    protected final String groupName3 = "group-3";
    protected final String groupName4 = "group-4";
    protected final String groupName5 = "group-5";

    protected final String userNameA = "user-A";
    protected final String userNameB = "user-B";
    protected final String userNameC = "user-C";
    protected final String userNameD = "user-D";
    protected final String userNameE = "user-E";
    protected final String userNameF = "user-F";
    protected final String userNameG = "user-G";

    protected final String firstName = "First";
    protected final String surName = "Last";
    protected final String password = "secret-password";


    /**
     * Called as part of tearDown()
     */
    @Override
    protected void removeTestData() throws DirectoryInstantiationException
    {
        DirectoryTestHelper.silentlyRemoveGroup(groupName1, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup(groupName2, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup(groupName3, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup(groupName4, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup(groupName5, getRemoteDirectory());

        DirectoryTestHelper.silentlyRemoveUser(userNameA, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser(userNameB, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser(userNameC, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser(userNameD, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser(userNameE, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser(userNameF, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser(userNameG, getRemoteDirectory());
    }

    /**
     * Called as part of setUp()
     */
    @Override
    protected void loadTestData()
        throws Exception
    {
        GroupTemplate group1 = new GroupTemplate(groupName1, directory.getId(), GroupType.GROUP);     // members: userA, group2
        getRemoteDirectory().addGroup(group1);

        GroupTemplate group2 = new GroupTemplate(groupName2, directory.getId(), GroupType.GROUP);     // members: group3, userB
        getRemoteDirectory().addGroup(group2);

        GroupTemplate group3 = new GroupTemplate(groupName3, directory.getId(), GroupType.GROUP);     // members: userC, group1, userD, group4
        getRemoteDirectory().addGroup(group3);

        GroupTemplate group4 = new GroupTemplate(groupName4, directory.getId(), GroupType.GROUP);     // members: userE, group5
        getRemoteDirectory().addGroup(group4);

        GroupTemplate group5 = new GroupTemplate(groupName5, directory.getId(), GroupType.GROUP);     // members: [none]
        getRemoteDirectory().addGroup(group5);

        DirectoryTestHelper.addUser(userNameA, directory.getId(), password, getRemoteDirectory());
        DirectoryTestHelper.addUser(userNameB, directory.getId(), password, getRemoteDirectory());
        DirectoryTestHelper.addUser(userNameC, directory.getId(), password, getRemoteDirectory());
        DirectoryTestHelper.addUser(userNameD, directory.getId(), password, getRemoteDirectory());
        DirectoryTestHelper.addUser(userNameE, directory.getId(), password, getRemoteDirectory());
        DirectoryTestHelper.addUser(userNameF, directory.getId(), password, getRemoteDirectory());
        DirectoryTestHelper.addUser(userNameG, directory.getId(), password, getRemoteDirectory());  // not a member of anything

        getRemoteDirectory().addUserToGroup(userNameA, groupName1);
        getRemoteDirectory().addUserToGroup(userNameB, groupName2);
        getRemoteDirectory().addUserToGroup(userNameC, groupName3);
        getRemoteDirectory().addUserToGroup(userNameD, groupName3);
        getRemoteDirectory().addUserToGroup(userNameE, groupName4);

        getRemoteDirectory().addGroupToGroup(groupName2, groupName1);
        getRemoteDirectory().addGroupToGroup(groupName3, groupName2);
        getRemoteDirectory().addGroupToGroup(groupName1, groupName3);
        getRemoteDirectory().addGroupToGroup(groupName4, groupName3);
        getRemoteDirectory().addGroupToGroup(groupName5, groupName4);
    }

    public void testNestedGroups() throws Exception
    {
        logger.info("Running testNestedGroups");

        // groupName1 list should contain: userA, userB, userC, userD, userE
        List<String> users = directoryManager.searchNestedGroupRelationships(directory.getId(), QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(groupName1).returningAtMost(EntityQuery.ALL_RESULTS));

        assertNotNull(users);
        assertEquals(5 + getInitialGroupMemberCount(), users.size());
        assertThat(users, hasItems(userNameA, userNameB, userNameC, userNameD, userNameE));

        // group4 list should contain: userE
        users = directoryManager.searchNestedGroupRelationships(directory.getId(), QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(groupName4).returningAtMost(EntityQuery.ALL_RESULTS));
        assertNotNull(users);
        assertEquals(1 + getInitialGroupMemberCount(), users.size());
        assertThat(users, hasItems(userNameE));
    }

    public void testAddUserToNestedGroups() throws Exception
    {
        logger.info("Running testAddUserToNestedGroups");

        // should add the user to group1, and not any of the sub-groups of which it's a member.
        directoryManager.addUserToGroup(directory.getId(), userNameF, groupName1);

        // group1 list should contain: userA, userB, userC, userD, userE, userF


        List<String> users = directoryManager.searchNestedGroupRelationships(directory.getId(), QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(groupName1).returningAtMost(EntityQuery.ALL_RESULTS));
        assertNotNull(users);
        assertEquals(6 + getInitialGroupMemberCount(), users.size());
        assertThat(users, hasItems(userNameA, userNameB, userNameC, userNameD, userNameE, userNameF));

        // group4 list should contain: userE
        users = directoryManager.searchNestedGroupRelationships(directory.getId(), QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(groupName4).returningAtMost(EntityQuery.ALL_RESULTS));

        assertNotNull(users);
        assertEquals(1 + getInitialGroupMemberCount(), users.size());
        assertThat(users, hasItems(userNameE));
    }

    public void testRemoveUserFromNestedGroupsFail() throws Exception
    {
        logger.info("Running testRemoveUserFromNestedGroupsFail");

        // If a user is a member of a sub-group, but we're passed the parent group we should get cranky and refuse to
        //  remove the user
        try
        {
            directoryManager.removeUserFromGroup(directory.getId(), userNameB, groupName1);

            fail("Should have thrown exception");
        }
        catch (MembershipNotFoundException e)
        {
        }

        // group1 list should contain: userA, userB, userC, userD, userE
        List<String> users = directoryManager.searchNestedGroupRelationships(directory.getId(), QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(groupName1).returningAtMost(EntityQuery.ALL_RESULTS));
        assertNotNull(users);
        assertEquals(5 + getInitialGroupMemberCount(), users.size());
        assertThat(users, hasItems(userNameA, userNameB, userNameC, userNameD, userNameE));

        // group4 list should contain: userE
        users = directoryManager.searchNestedGroupRelationships(directory.getId(), QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(groupName4).returningAtMost(EntityQuery.ALL_RESULTS));
        assertNotNull(users);
        assertEquals(1 + getInitialGroupMemberCount(), users.size());
        assertThat(users, hasItems(userNameE));
    }

    public void testRemoveUserFromNestedGroupsOk() throws Exception
    {
        logger.info("Running testRemoveUserFromNestedGroupsOk");

        // The user is a member of the group, which also contains nested groups. Make sure we only remove the user in
        //  question.
        directoryManager.removeUserFromGroup(directory.getId(), userNameA, groupName1);


        // group1 list should contain: userB, userC, userD, userE
        List<String> users = directoryManager.searchNestedGroupRelationships(directory.getId(), QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(groupName1).returningAtMost(EntityQuery.ALL_RESULTS));
        assertNotNull(users);
        assertEquals(4 + getInitialGroupMemberCount(), users.size());
        assertThat(users, hasItems(userNameB, userNameC, userNameD, userNameE));

        // group4 list should contain: userE
        users = directoryManager.searchNestedGroupRelationships(directory.getId(), QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(groupName4).returningAtMost(EntityQuery.ALL_RESULTS));
        assertNotNull(users);
        assertEquals(1 + getInitialGroupMemberCount(), users.size());
        assertThat(users, hasItems(userNameE));
    }

    public void testNestedGroupsDirectFetch() throws Exception
    {
        logger.info("Running testNestedGroupsDirectFetch");

        // Make sure that when we just ask for the direct members, we get 'em.
        List<String> groups = directoryManager.searchDirectGroupRelationships(directory.getId(), QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP)).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(groupName1).returningAtMost(EntityQuery.ALL_RESULTS));

        // check group member
        assertEquals(ImmutableList.of(groupName2), groups);

        // check principal member
        List<String> users = directoryManager.searchDirectGroupRelationships(directory.getId(), QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(groupName1).returningAtMost(EntityQuery.ALL_RESULTS));

        assertNotNull(users);
        assertEquals(1 + getInitialGroupMemberCount(), users.size());
        assertThat(users, hasItems(userNameA));
    }


    public void testIsGroupMemberDirect() throws Exception
    {
        logger.info("Running testIsGroupMemberDirect");
        boolean isMember = directoryManager.isUserDirectGroupMember(directory.getId(), userNameA, groupName1);
        assertTrue(userNameA + " should be a member of " + groupName1, isMember);
    }

    public void testIsGroupMemberDirectNot() throws Exception
    {
        logger.info("Running testIsGroupMemberDirectNot");
        boolean isMember = directoryManager.isUserDirectGroupMember(directory.getId(), userNameG, groupName1);
        assertFalse(userNameG + " should NOT be a member of " + groupName1, isMember);
    }

    public void testIsGroupMemberDirectNonExistent() throws Exception
    {
        logger.info("Running testIsGroupMemberDirectNonExistent");
        String nonExistentUserName = "badgers-Ahoy!";
        boolean isMember = directoryManager.isUserDirectGroupMember(directory.getId(), nonExistentUserName, groupName1);
        assertFalse(nonExistentUserName + " should NOT be a member of " + groupName1, isMember);
    }

    public void testIsGroupMemberNested() throws Exception
    {
        logger.info("Running testIsGroupMemberNested");
        boolean isMember = directoryManager.isUserNestedGroupMember(directory.getId(), userNameB, groupName1);
        assertTrue(userNameB + " should be a nested member of " + groupName1, isMember);
    }

    protected int getInitialGroupMemberCount()
    {
        // by default, new groups have zero members (in most directories)
        return 0;
    }
}
