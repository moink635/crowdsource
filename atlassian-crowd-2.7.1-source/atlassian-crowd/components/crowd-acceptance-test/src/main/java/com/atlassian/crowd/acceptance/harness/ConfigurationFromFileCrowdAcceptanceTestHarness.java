package com.atlassian.crowd.acceptance.harness;

import com.atlassian.crowd.acceptance.tests.applications.crowd.DirectoryConfigurationReadFromFileTest;
import com.atlassian.crowd.acceptance.tests.rest.service.client.RestCrowdClientTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * This suite runs tests to assess the behaviour of Crowd when the configuration is
 * read from property files, and not from the database. Since the configuration is
 * read-only, the tests cannot restore it to a known state, therefore they rely on
 * another suite to produce the property files from a known database state.
 * However, in order to demonstrate that the configuration specified in the file
 * actually takes precedence over the contents of the database, the tests of this
 * suite assume that the property files have been patched (see src/main/patch).
 * @see com.atlassian.crowd.acceptance.harness.FreezeConfigurationCrowdAcceptanceTestHarness
 */
@RunWith(Suite.class)
@SuiteClasses({
    DirectoryConfigurationReadFromFileTest.class,
    ConfigurationFromFileCrowdAcceptanceTestHarness.RestCrowdClientTestNoRestore.class })
public class ConfigurationFromFileCrowdAcceptanceTestHarness
{
    public static class RestCrowdClientTestNoRestore extends RestCrowdClientTest
    {
        @Override
        public void restoreCrowdFromXML(String xmlfilename)
        {
            // do nothing, Crowd is assumed to be in a known state. Assert that this is
            // the state that the test is trying to restore:
            assertEquals("remotecrowddirectory.xml", xmlfilename);
        }
    }
}
