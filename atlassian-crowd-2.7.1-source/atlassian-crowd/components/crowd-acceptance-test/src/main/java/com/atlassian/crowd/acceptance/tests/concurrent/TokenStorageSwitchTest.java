package com.atlassian.crowd.acceptance.tests.concurrent;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.service.soap.client.SecurityServerClientImpl;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;

import java.util.Properties;
import java.util.concurrent.*;


/**
 * Makes a number of requests to the SecurityServerClient interface while switching the token storage location. Checks
 * that the stores remain consistent.
 */
public class TokenStorageSwitchTest extends CrowdAcceptanceTestCase
{
    private final String APPLICATION_NAME = "tokentest";         // must be same as in tokenstorage.xml and localtest.crowd.properties
    private final String USERNAME_PREFIX = "test-user-name";
    private final String PASSWORD_SUFFIX = "password";

    protected CyclicBarrier barrier = null;
    private static final ExecutorService pool = Executors.newCachedThreadPool();
    private volatile boolean endOfPhase = false;

    private SecurityServerClient ssc = null;


    /**
     * Sets the connection details & auths the app.
     * @throws Exception
     */
    protected void setupSecurityServer() throws Exception
    {
        Properties sscProperties = AcceptanceTestHelper.loadProperties("localtest.crowd.properties");
        SoapClientProperties cProperties = SoapClientPropertiesImpl.newInstanceFromProperties(sscProperties);
        ssc = new SecurityServerClientImpl(cProperties);
    }

    protected SecurityServerClient getSSC()
    {
        return ssc;
    }

    protected int getNumberOfCores()
    {
        return Runtime.getRuntime().availableProcessors();
    }

    protected String getUsername(int i)
    {
        return USERNAME_PREFIX + i;
    }

    protected String getPassword(String username)
    {
        return username + PASSWORD_SUFFIX;
    }

    protected PasswordCredential getNewPasswordCredential(String username)
    {
        return new PasswordCredential(getPassword(username));
    }

    protected ValidationFactor[] getValidationFactors(String username)
    {
        return new ValidationFactor[] {};
    }

    protected void addUser(String username) throws Exception
    {
        SOAPPrincipal soapPrincipal = new SOAPPrincipal();
        soapPrincipal.setActive(true);
        soapPrincipal.setName(username);
        getSSC().addPrincipal(soapPrincipal, getNewPasswordCredential(username));
    }

    protected boolean waitForBarrier()
    {
        boolean waitSuccess = false;

        try
        {
            barrier.await();
            waitSuccess = true;
        }
        catch (BrokenBarrierException e)
        {
            logAndFail("Broken barrier exception", e);
        }
        catch (InterruptedException e)
        {
            logAndFail("Thread interrupted exception", e);
        }
        return waitSuccess;
    }

    protected void logAndFail(String message, Exception e)
    {
        logger.error(message, e);
        fail(message);
    }

    /**
     * A worker thread that keeps checking a token's validity while the switch is going on. There will be many of these.
     */
    public class AuthenticateThread implements Runnable
    {
        private final int userNumber;

        public AuthenticateThread(int userNumber)
        {
            this.userNumber = userNumber;
        }

        protected void assertTokenIsValid(String token)
        {
            try
            {
                // assert the token is valid
                boolean isValidToken = getSSC().isValidToken(token, getValidationFactors(getUsername(userNumber)));
                assertTrue("Token was returned as invalid", isValidToken);
            }
            catch (Exception e)
            {
                logAndFail("Unable to check token validity", e);
            }
        }

        public void run()
        {
            String token = null;

            // wait for all threads to be ready before starting
            if (waitForBarrier())
            {
                try
                {
                    // auth principal
                    UserAuthenticationContext authContext = new UserAuthenticationContext();
                    authContext.setApplication(APPLICATION_NAME);
                    authContext.setName(getUsername(userNumber));
                    authContext.setCredential(getNewPasswordCredential(getUsername(userNumber)));
                    authContext.setValidationFactors(getValidationFactors(getUsername(userNumber)));
                    token = getSSC().authenticatePrincipal(authContext);
                    assertNotNull("Principal token should not have been null", token);
                }
                catch (Exception e) // bad practice, but 'tis a test case & all we need to know is that it failed.
                {
                    logAndFail("Unable to authenticate principal #" + userNumber + ": ", e);
                }

                // this loop runs before-and-during the switch
                while (!endOfPhase)
                {
                    assertTokenIsValid(token);
                }

                // assert the token is valid after the switch
                assertTokenIsValid(token);
            }

            // wait for all threads to be complete before ending.
            waitForBarrier();
        }
    }

    public void setUp() throws Exception
    {
        super.setUp();

        restoreCrowdFromXML("tokenstorage.xml");

        setupSecurityServer();

        // add (# of cores) users
        int numUsers = getNumberOfCores();
        for (int i=0; i < numUsers; i++)
        {
            addUser(getUsername(i));
        }

        // setup worker threads - more than the # of cores, to stimulate interleaving.
        int numThreads = numUsers * 2;
        barrier = new CyclicBarrier(numThreads + 1);    // +1 is the testcase thread (this)
        for (int i=0; i < numThreads; i++)
        {
            pool.execute(new AuthenticateThread(i % numUsers)); // won't start working until testcase thread calls barrier.await();
        }
    }

    public void tearDown() throws Exception
    {
        barrier = null;
        pool.shutdownNow();
        super.tearDown();
    }

    protected void doSwitchToMemory()
    {
        gotoSessionConfig();
        
        // Set all fields to a new value
        setRadioButton("storageType", "memory");   // set to "memory caching"

        submit();

        // assert that we have the correct values
        assertKeyPresent("updatesuccessful.label");
    }

    public void testTokenSwitchUnderLoad()
    {
        try
        {
            barrier.await();    // waits for all worker threads to be ready & kicks them off.
            barrier.reset();    // reset so it can be used to synchronize the ending of the threads too.

            // do the switch via the UI.
            doSwitchToMemory();

            endOfPhase = true;  // tells the threads to check the token one more time, then await() on the barrier.
            barrier.await(60, TimeUnit.SECONDS);    // waits for all worker threads to end.
        }
        catch (InterruptedException e)
        {
            logAndFail("Test failed due to interrupted exception", e);
        }
        catch (BrokenBarrierException e)
        {
            logAndFail("Test failed due to broken barrier exception", e);
        }
        catch (TimeoutException e)
        {
            logAndFail("Test failed due to timeout", e);
        }
    }
}
