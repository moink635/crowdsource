package com.atlassian.crowd.acceptance.tests.applications.crowd.legacy;

public class Crowd14XmlRestoreTest extends BaseLegacyXmlRestoreTest
{
    public String getLegacyXmlFileName()
    {
        return "1_4_7_306.xml";
    }
}
