package com.atlassian.crowd.acceptance.harness;

import com.atlassian.crowd.acceptance.tests.applications.crowd.performance.LargeCsvImporterTest;
import com.atlassian.crowd.acceptance.tests.client.load.PrincipalAuthenticationLoadTest;
import com.atlassian.crowd.acceptance.tests.client.load.SecurityServerClientBulkAddTest;
import com.atlassian.crowd.acceptance.tests.client.load.TokenValidationLoadTest;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * This suite embodies the Java-bases performance/load tests that are run against a running Crowd server
 * and do not require access to external remote directories such as LDAP.
 */
public class CrowdPerformanceTestHarness extends TestCase
{
    public static Test suite()
    {
        TestSuite suite = new TestSuite();

        suite.addTestSuite(LargeCsvImporterTest.class);
        suite.addTestSuite(PrincipalAuthenticationLoadTest.class);
        suite.addTestSuite(TokenValidationLoadTest.class);
        suite.addTestSuite(SecurityServerClientBulkAddTest.class);
//        suite.addTestSuite(VeryLargeCsvImporterTest.class); // TODO comment back in when fixed

        return suite;
    }
}
