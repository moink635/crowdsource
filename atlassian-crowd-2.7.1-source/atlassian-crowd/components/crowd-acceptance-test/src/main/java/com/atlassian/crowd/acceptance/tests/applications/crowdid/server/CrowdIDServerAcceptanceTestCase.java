package com.atlassian.crowd.acceptance.tests.applications.crowdid.server;

import com.atlassian.crowd.acceptance.tests.ApplicationAcceptanceTestCase;


public class CrowdIDServerAcceptanceTestCase extends ApplicationAcceptanceTestCase
{
    protected String getResourceBundleName()
    {
        return "com.atlassian.crowd.openid.server.action.BaseAction";
    }

    protected String getApplicationName()
    {
        return "crowdid";
    }

    protected String getLocalTestPropertiesFileName()
    {
        return "localtest.properties";
    }
}
