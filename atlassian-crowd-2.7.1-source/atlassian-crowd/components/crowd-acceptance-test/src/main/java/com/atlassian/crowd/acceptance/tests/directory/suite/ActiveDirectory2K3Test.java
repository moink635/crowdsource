package com.atlassian.crowd.acceptance.tests.directory.suite;

import java.util.Properties;

import com.atlassian.crowd.acceptance.tests.directory.BasicTest;
import com.atlassian.crowd.acceptance.tests.directory.DnRangeTest;
import com.atlassian.crowd.acceptance.tests.directory.EnabledDisabledUserTest;
import com.atlassian.crowd.acceptance.tests.directory.NestedGroupsTest;
import com.atlassian.crowd.acceptance.tests.directory.PageAndRangeTest;
import com.atlassian.crowd.acceptance.tests.directory.PrimaryGroupTest;
import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.model.directory.DirectoryImpl;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Tests for Active Directory 2003
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({ActiveDirectory2K3Test.AD2K3BasicTest.class,
                     ActiveDirectory2K3Test.AD2K3NestedGroupsTest.class,
                     ActiveDirectory2K3Test.AD2K3PrimaryGroupTest.class,
                     ActiveDirectory2K3Test.AD2K3EnabledDisabledUserTest.class,
                     ActiveDirectory2K3Test.AD2K3ForwardslashTest.class,
                     ActiveDirectory2K3Test.AD2K3BackslashTest.class,
                     ActiveDirectory2K3Test.AD2K3DnRangeTestCacheEnabled.class,
                     ActiveDirectory2K3Test.AD2K3DnRangeTestCacheDisabled.class,
                     ActiveDirectory2K3Test.AD2K3PageAndRangeTest.class })
public class ActiveDirectory2K3Test
{
    public static class AD2K3BasicTest extends BasicTest
    {
        public AD2K3BasicTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getAD2K3ConfigFileName());
        }
    }

    public static class AD2K3NestedGroupsTest extends NestedGroupsTest
    {
        public AD2K3NestedGroupsTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getAD2K3ConfigFileName());
        }
    }

    public static class AD2K3PrimaryGroupTest extends PrimaryGroupTest
    {
        public AD2K3PrimaryGroupTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getAD2K3ConfigFileName());
        }

        @Override
        protected void configureDirectory(Properties directorySettings)
        {
            super.configureDirectory(directorySettings);
            directory.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_USE_PRIMARY_GROUP, "true");
        }
    }

    public static class AD2K3EnabledDisabledUserTest extends EnabledDisabledUserTest
    {
        public AD2K3EnabledDisabledUserTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getAD2K3ConfigFileName());
        }
    }

    /**
     * Tests performance against AD using backslashes
     */
    public static class AD2K3BackslashTest extends BasicTest
    {
        public AD2K3BackslashTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getAD2K3ConfigFileName());
        }

        @Override
        public String getAutoTestFirstName()
        {
            return "First\\ Name";
        }

        @Override
        public String getAutoTestGroupName()
        {
            return "My \\ Group\\Name auto-t";
        }
    }

    /**
     * Tests performance against AD using forwardslashes.
     */
    public static class AD2K3ForwardslashTest extends BasicTest
    {
        public AD2K3ForwardslashTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getAD2K3ConfigFileName());
        }

        @Override
        public String getAutoTestFirstName()
        {
            return "user/name";
        }

        @Override
        public String getAutoTestGroupName()
        {
            return "My / Group //Name auto-t";
        }
    }

    public static class AD2K3PageAndRangeTest extends PageAndRangeTest
    {
        public AD2K3PageAndRangeTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getAD2K3ConfigFileName());
        }
    }

    /**
     * Tests for dnRanges performed against loadTesting10k with cache enabled
     */
    public static class AD2K3DnRangeTestCacheEnabled extends DnRangeTest
    {
        public AD2K3DnRangeTestCacheEnabled()
        {
            super(DirectoryTestHelper.getAD2K3ConfigFileName());
        }

        @Override
        protected DirectoryImpl configureDirectory(Properties directorySettings)
        {
            DirectoryImpl directory = super.configureDirectory(directorySettings);

            // Disable roles
            directory.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.TRUE.toString());
            directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.TRUE.toString());

            return directory;
        }
    }

    /**
     * Tests for dnRanges performed against loadTesting10k with cache disabled
     */

    public static class AD2K3DnRangeTestCacheDisabled extends DnRangeTest
    {
        public AD2K3DnRangeTestCacheDisabled()
        {
            super(DirectoryTestHelper.getAD2K3ConfigFileName());
        }

        @Override
        protected DirectoryImpl configureDirectory(Properties directorySettings)
        {
            DirectoryImpl directory = super.configureDirectory(directorySettings);

            // Disable roles
            directory.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.TRUE.toString());
            directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.FALSE.toString());

            return directory;
        }
    }

    /**
     * TODO: Run the normal tests, but mutating the user/group before the test and testing for the changed test.
     */
}
