package com.atlassian.crowd.acceptance.tests.administration;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

public class SystemInfoTest extends CrowdAcceptanceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        restoreBaseSetup();
    }

    public void testSupportInformationIsDisplayed()
    {
        gotoAdministrationPage();
        clickLink("system-info-admin");

        assertKeyPresent("systeminfo.supportinfo.title");
        String supportInformation = getElementTextByXPath("//textarea[@name='supportInformation']");

        // current user
        assertThat(supportInformation, containsString("Username: admin"));

        // directories
        assertThat(supportInformation, containsString("Name: Test Internal Directory"));

        // applications and directory mappings
        assertThat(supportInformation, containsString("Name: crowd-openid-server"));
        assertThat(supportInformation, containsString("Mapped to directory ID: 23"));
        assertThat(supportInformation, containsString("Mapped groups: [crowd-administrators]"));
    }
}
