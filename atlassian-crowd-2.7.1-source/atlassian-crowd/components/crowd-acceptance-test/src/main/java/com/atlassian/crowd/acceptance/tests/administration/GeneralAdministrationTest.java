package com.atlassian.crowd.acceptance.tests.administration;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import net.sourceforge.jwebunit.api.IElement;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GeneralAdministrationTest extends CrowdAcceptanceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        restoreBaseSetup();
    }

    public void testShouldNotShowEnableSecureSSOCookieWhenAccessedViaAnInsecureChannel()
    {
        gotoAdministrationPage();
        clickLinkWithKey("menu.general.label");
        assertCheckboxPresent("secureCookie");
        assertThat(isElementDisabled("secureCookie"),is(true));
        assertKeyPresent("options.securecookie.disabled.description");
        assertKeyPresent("options.securecookie.description");
    }

    private boolean isElementDisabled(String elementId)
    {
        IElement secureCookie = getElementById(elementId);
        return "disabled".equals(secureCookie.getAttribute("disabled"));
    }

    public void testTokenSeedIsNotShown()
    {
        gotoAdministrationPage();
        assertKeyNotPresent("options.seed.label");
        assertKeyNotPresent("options.seed.description");
        assertElementNotPresent("seed");

        assertKeyNotPresent("generate.label");
    }
}
