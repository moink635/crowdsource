package com.atlassian.crowd.acceptance.tests.applications.crowdid;

import com.atlassian.crowd.acceptance.tests.BaseUrlFromProperties;
import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CrowdIDAcceptanceTestCase extends CrowdAcceptanceTestCase
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    static
    {
        HOST_PATH = new BaseUrlFromProperties(specProperties).baseUrlFor("crowdid");
        URL_HOME = "/";
    }

    public void setUp() throws Exception
    {
        super.setUp();

        getTestContext().setResourceBundleName("com.atlassian.crowd.openid.server.action.BaseAction");

        beginAt(URL_HOME);
    }

    protected void log(String message)
    {
        logger.info(message);
    }
}
