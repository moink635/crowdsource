package com.atlassian.crowd.acceptance.tests.persistence.dao.token;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.dao.token.TokenDAO;
import com.atlassian.crowd.dao.token.TokenDAOHibernate;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class TokenDAOHibernateTest extends TokenDAOTester
{
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;
    @Inject private TokenDAOHibernate tokenDAO;

    // a @Rule ExpectedException expectedException is inherited. Do not shadow it!

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Test
    public void testFindByID() throws Exception
    {
        Token token = (tokenDAO).findByID(TOKEN_ID);
        assertNotNull(token);
        assertEquals(TOKEN_ID, token.getId());
    }

    @Test
    public void testTokenRemovalWhereTokenDoesNotExist() throws Exception
    {
        Token tokenToRemove = tokenDAO.findByRandomHash(TOKEN_KEY);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.update("DELETE FROM cwd_token WHERE random_hash = ?", new Object[] { TOKEN_KEY });

        tokenDAO.remove(tokenToRemove);

        expectedException.expect(ObjectNotFoundException.class);
        tokenToRemove = tokenDAO.findByRandomHash(TOKEN_KEY);
    }

    @Test
    public void testCallingTokenRemovalTwiceDoesNotThrowAnException() throws Exception
    {
        Token tokenToRemove = tokenDAO.findByRandomHash(TOKEN_KEY);

        tokenDAO.remove(tokenToRemove);

        tokenDAO.remove(tokenToRemove);
    }

    @Override
    protected TokenDAO getTokenDAO()
    {
        return tokenDAO;
    }
}
