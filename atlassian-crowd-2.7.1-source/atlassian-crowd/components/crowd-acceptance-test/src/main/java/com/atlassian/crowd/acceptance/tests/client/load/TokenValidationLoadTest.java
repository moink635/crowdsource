package com.atlassian.crowd.acceptance.tests.client.load;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import com.atlassian.crowd.acceptance.utils.Action;
import com.atlassian.crowd.acceptance.utils.ActionRunner;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl;
import com.atlassian.crowd.service.soap.client.SecurityServerClientImpl;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;

import java.rmi.RemoteException;
import java.util.Properties;

public class TokenValidationLoadTest extends CrowdAcceptanceTestCase
{
    private static final int THREADS = 5;
    private static final int ITERATIONS_PER_THREAD = 30;

    private SecurityServerClientImpl securityServerClient;
    private UserAuthenticationContext userAuthenticationContext;
    private Properties sscProperties;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        restoreCrowdFromXML("tokenauthenticationtest.xml"); // uses database tokens

        sscProperties = AcceptanceTestHelper.loadProperties("localtest.crowd.properties");
        SoapClientProperties cProperties = SoapClientPropertiesImpl.newInstanceFromProperties(sscProperties);
        securityServerClient = new SecurityServerClientImpl(cProperties);

        userAuthenticationContext = new UserAuthenticationContext();
        userAuthenticationContext.setApplication("integrationtest");
    }

    public void testHammerTokenValidation() throws InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException, RemoteException, InactiveAccountException, InterruptedException, ExpiredCredentialException
    {
        log("Running testHammerTokenValidation");

        ValidationFactor validationFactor = new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, "127.0.0.1");
        final ValidationFactor[] validationFactors = new ValidationFactor[] { validationFactor };

        userAuthenticationContext.setName("user");
        userAuthenticationContext.setCredential(new PasswordCredential("user"));
        userAuthenticationContext.setValidationFactors(validationFactors);

        final String token = securityServerClient.authenticatePrincipal(userAuthenticationContext);

        Action action = new Action()
        {
            public void execute() throws Exception
            {
                securityServerClient.isValidToken(token, validationFactors);
            }
        };

        ActionRunner runner = new ActionRunner(THREADS, ITERATIONS_PER_THREAD, action);

        runner.execute();

        if (runner.getFailures() > 0)
        {
            fail("" + runner.getFailures() + " of the " + THREADS * ITERATIONS_PER_THREAD + " iterations failed");
        }
    }
}