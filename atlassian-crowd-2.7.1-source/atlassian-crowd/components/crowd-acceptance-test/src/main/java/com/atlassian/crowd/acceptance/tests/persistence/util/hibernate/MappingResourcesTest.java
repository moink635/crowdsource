package com.atlassian.crowd.acceptance.tests.persistence.util.hibernate;

import java.util.Arrays;

import com.atlassian.crowd.util.persistence.hibernate.MappingResources;

import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;

public class MappingResourcesTest
{
    private MappingResources mappingResources;
    
    @Before
    public void setUp() throws Exception
    {
        mappingResources = new MappingResources();
    }

    @Test
    public void testGetMappings() throws Exception
    {
        mappingResources.setConfigLocation(new ClassPathResource("hibernate.cfg.xml"));

        mappingResources.afterPropertiesSet();

        assertThat(mappingResources.getMappings(),
                   containsInAnyOrder("com/atlassian/crowd/model/CustomTypes.hbm.xml",
                                      "com/atlassian/crowd/model/alias/Alias.hbm.xml",
                                      "com/atlassian/crowd/model/application/Application.hbm.xml",
                                      "com/atlassian/crowd/model/directory/Directory.hbm.xml",
                                      "com/atlassian/crowd/model/group/InternalGroup.hbm.xml",
                                      "com/atlassian/crowd/model/membership/InternalMembership.hbm.xml",
                                      "com/atlassian/crowd/model/property/Property.hbm.xml",
                                      "com/atlassian/crowd/model/token/Token.hbm.xml",
                                      "com/atlassian/crowd/model/user/InternalUser.hbm.xml",
                                      "com/atlassian/crowd/model/webhook/Webhook.hbm.xml"));
    }

    @Test
    public void testGetMappingsAsArray() throws Exception
    {
        mappingResources.setConfigLocation(new ClassPathResource("hibernate.cfg.xml"));

        mappingResources.afterPropertiesSet();

        assertThat(Arrays.asList(mappingResources.getMappingsAsArray()),
                   containsInAnyOrder("com/atlassian/crowd/model/CustomTypes.hbm.xml",
                                      "com/atlassian/crowd/model/alias/Alias.hbm.xml",
                                      "com/atlassian/crowd/model/application/Application.hbm.xml",
                                      "com/atlassian/crowd/model/directory/Directory.hbm.xml",
                                      "com/atlassian/crowd/model/group/InternalGroup.hbm.xml",
                                      "com/atlassian/crowd/model/membership/InternalMembership.hbm.xml",
                                      "com/atlassian/crowd/model/property/Property.hbm.xml",
                                      "com/atlassian/crowd/model/token/Token.hbm.xml",
                                      "com/atlassian/crowd/model/user/InternalUser.hbm.xml",
                                      "com/atlassian/crowd/model/webhook/Webhook.hbm.xml"));
    }

    @Test
    public void testGetTableNames() throws Exception
    {
        mappingResources.setConfigLocation(new ClassPathResource("hibernate.cfg.xml"));

        mappingResources.afterPropertiesSet();

        assertThat(mappingResources.getTableNames(),
                   containsInAnyOrder("cwd_app_dir_group_mapping",
                                      "cwd_app_dir_mapping",
                                      "cwd_app_dir_operation",
                                      "cwd_application",
                                      "cwd_application_address",
                                      "cwd_application_alias",
                                      "cwd_application_attribute",
                                      "cwd_directory",
                                      "cwd_directory_attribute",
                                      "cwd_directory_operation",
                                      "cwd_group",
                                      "cwd_group_attribute",
                                      "cwd_membership",
                                      "cwd_property",
                                      "cwd_token",
                                      "cwd_user",
                                      "cwd_user_attribute",
                                      "cwd_user_credential_record",
                                      "cwd_webhook"));
    }
}
