package com.atlassian.crowd.acceptance.tests.applications.crowd;

import javax.mail.internet.MimeMessage;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetup;

/**
 * Extends {@link CrowdAcceptanceTestCase} with an email server. Subclasses must call
 * {@link #_configureMailServer()} after restoring the database and logging in as an admin.
 */
public abstract class CrowdAcceptanceTestCaseWithEmailServer extends CrowdAcceptanceTestCase
{
    private static final String SMTP_FROM_ADDRESS = "crowd-tests@example.com";
    private static final String SMTP_SUBJECT_PREFIX = "[crowd-tests]";

    private GreenMail greenMail;
    private String smtpPort;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        // non-default port used by GreenMail for fake SMTP server
        smtpPort = System.getProperty("crowd.test.smtp.port", "3025");
        ServerSetup smtpServer = new ServerSetup(Integer.parseInt(smtpPort), null, ServerSetup.PROTOCOL_SMTP);

        // start up the fake SMTP server on a non-default test port to enable email
        greenMail = new GreenMail(smtpServer);
        greenMail.start(); // If this line throws an exception, check if smtpPort is available on testing server
    }

    @Override
    public void tearDown() throws Exception
    {
        if (null != greenMail)
        {
            greenMail.stop();
        }

        super.tearDown();
    }

    protected void _configureMailServer()
    {
        // configure Crowd's mail configuration
        gotoMailServer();
        setTextField("notificationEmail", SMTP_FROM_ADDRESS);
        setTextField("from", SMTP_FROM_ADDRESS);
        setTextField("prefix", SMTP_SUBJECT_PREFIX);
        setTextField("host", "localhost");
        setTextField("port", smtpPort);
        submit();
    }

    public String getEmailAddress()
    {
        return SMTP_FROM_ADDRESS;
    }

    protected MimeMessage waitForExactlyOneMessage() throws InterruptedException
    {
        // wait max 5 seconds to receive 1 email and assert that it arrives
        assertTrue("A reset password email should have arrived", greenMail.waitForIncomingEmail(5000, 1));

        // put each email in the inbox into an array and assert there is only one message present
        MimeMessage[] messages = greenMail.getReceivedMessages();
        assertEquals("Incorrect number of emails present", 1, messages.length);

        return messages[0];
    }
}
