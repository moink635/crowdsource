package com.atlassian.crowd.acceptance.tests.applications.crowd;

/**
 * A test class that looks at the license resource count for Crowd
 */
public class RecalculateLicenseTest extends CrowdAcceptanceTestCase
{

    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("recalculatelicensetest.xml");
    }

    public void testRecalculatingResourceTotal()
    {
        gotoLicensing();

        // Assert that we only have a license count of one
        assertTextInElement("license-count", "1");

        // Now recalculate this total
        clickLink("license-recalculate-total");

        // And assert that the count is now correct
        assertTextInElement("license-count", "3");

        assertKeyPresent("updatesuccessful.label");
    }

    public void testRecalculatingResourceTotalWithInactivePrincipal()
    {
        // set a principal to inactive
        gotoViewPrincipal("justin", "Test Internal Directory");

        setWorkingForm("updateprincipalForm");

        // Set them to inactive
        uncheckCheckbox("active");

        submit();

        assertCheckboxNotSelected("active");

        // Now go to the licensing page and re-calculate the user total
        gotoLicensing();

        // Now recalculate this total
        clickLink("license-recalculate-total");

        // And assert that the count is now correct
        assertTextInElement("license-count", "2");

        assertKeyPresent("updatesuccessful.label");
    }
}
