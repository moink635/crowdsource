package com.atlassian.crowd.acceptance.tests.persistence.dao.alias;

import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.dao.alias.AliasDAOHibernate;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.alias.Alias;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.AliasTermKeys;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class AliasDAOHibernateTest
{
    private static final Long APPLICATION1_ID = 1L;
    private static final Long APPLICATION2_ID = 2L;

    private static final String USER1 = "Robert";
    private static final String USER2 = "William";

    private static final String USER1_ALIAS1 = "bob";
    private static final String USER1_ALIAS2 = "Robbie";
    private static final String USER2_ALIAS = "BiLL";

    @Inject private AliasDAOHibernate aliasDAO;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    private Application application1;
    private Application application2;

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Before
    public void createApplications()
    {
        InternalEntityTemplate template1 = new InternalEntityTemplate();
        template1.setId(APPLICATION1_ID);
        template1.setName("");

        InternalEntityTemplate template2 = new InternalEntityTemplate();
        template2.setId(APPLICATION2_ID);
        template2.setName("");

        application1 = new ApplicationImpl(template1);
        application2 = new ApplicationImpl(template2);
    }

    @Test
    public void testFindAliasByUsername()
    {
        String alias = aliasDAO.findAliasByUsername(application1, USER1);
        assertEquals(USER1_ALIAS1, alias);

        alias = aliasDAO.findAliasByUsername(application2, USER1.toUpperCase());
        assertEquals(USER1_ALIAS2, alias);

        alias = aliasDAO.findAliasByUsername(application1, USER2);
        assertEquals(USER2_ALIAS, alias);
    }

    @Test
    public void testFindAliasByUsernameNotFound()
    {
        String alias = aliasDAO.findAliasByUsername(application1, "bleh");
        assertNull(alias);

        alias = aliasDAO.findAliasByUsername(application2, USER2);
        assertNull(alias);
    }

    @Test
    public void testFindUsernameByAlias()
    {
        String username = aliasDAO.findUsernameByAlias(application1, USER1_ALIAS1);
        assertEquals(USER1, username);

        username = aliasDAO.findUsernameByAlias(application2, USER1_ALIAS2.toUpperCase());
        assertEquals(USER1, username);

        username = aliasDAO.findUsernameByAlias(application1, USER2_ALIAS);
        assertEquals(USER2, username);
    }

    @Test
    public void testFindUsernameByAliasNotFound()
    {
        String username = aliasDAO.findUsernameByAlias(application1, "bleh");
        assertNull(username);

        username = aliasDAO.findUsernameByAlias(application2, USER2_ALIAS);
        assertNull(username);
    }

    @Test
    public void testStoreAliasNew()
    {
        String username = "Yahya";
        String alias = "John";

        assertNull(aliasDAO.findAliasByUsername(application1, username));
        assertNull(aliasDAO.findUsernameByAlias(application1, alias));

        aliasDAO.storeAlias(application1, username, alias);

        assertEquals(alias, aliasDAO.findAliasByUsername(application1, username.toUpperCase()));
        assertEquals(username, aliasDAO.findUsernameByAlias(application1, alias.toUpperCase()));
    }

    @Test
    public void testStoreAliasUpdate()
    {
        String username = USER1;
        String oldAlias = USER1_ALIAS1;
        String newAlias = "aWesome";

        assertEquals(oldAlias, aliasDAO.findAliasByUsername(application1, username.toUpperCase()));
        assertEquals(username, aliasDAO.findUsernameByAlias(application1, oldAlias.toUpperCase()));
        assertNull(aliasDAO.findUsernameByAlias(application1, newAlias.toUpperCase()));

        aliasDAO.storeAlias(application1, username, newAlias);

        assertEquals(newAlias, aliasDAO.findAliasByUsername(application1, username.toUpperCase()));
        assertEquals(username, aliasDAO.findUsernameByAlias(application1, newAlias.toUpperCase()));
        assertNull(aliasDAO.findUsernameByAlias(application1, oldAlias.toUpperCase()));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testStoreAliasInvalid()
    {
        aliasDAO.storeAlias(application1, USER1, "");
    }

    @Test
    public void testRemoveAlias()
    {
        assertEquals(USER1_ALIAS1, aliasDAO.findAliasByUsername(application1, USER1));
        assertEquals(USER1, aliasDAO.findUsernameByAlias(application1, USER1_ALIAS1));

        aliasDAO.removeAlias(application1, USER1);

        assertNull(aliasDAO.findAliasByUsername(application1, USER1));
        assertNull(aliasDAO.findUsernameByAlias(application1, USER1_ALIAS1));
    }

    @Test
    public void testRemoveAliasSilentlySucceed()
    {
        String username = "user3";

        assertNull(aliasDAO.findAliasByUsername(application1, username));

        aliasDAO.removeAlias(application1, username);

        assertNull(aliasDAO.findAliasByUsername(application1, username));
    }

    @Test
    public void testRemoveAliases()
    {
        final String countAliasQuery = "select count(*) from cwd_application_alias where application_id = ?";
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertThat(jdbcTemplate.queryForObject(countAliasQuery, Integer.class, APPLICATION1_ID), is(2));

        aliasDAO.removeAliases(application1);

        assertThat(jdbcTemplate.queryForObject(countAliasQuery, Integer.class, APPLICATION1_ID), is(0));
        assertThat(jdbcTemplate.queryForObject(countAliasQuery, Integer.class, APPLICATION2_ID), is(1));
    }

    @Test
    public void testFindAll()
    {
        List<Alias> aliases = aliasDAO.findAll();
        assertEquals(3, aliases.size());
    }

    @Test
    public void testSearchByNameContainingWithMixedCase()
    {
        List<String> matchingUsernames = aliasDAO.search(QueryBuilder.queryFor(String.class, EntityDescriptor.alias()).with(Restriction.on(AliasTermKeys.ALIAS).containing(
            "IL")).returningAtMost(10));

        assertThat(matchingUsernames, contains(USER2));
    }

    @Test
    public void testSearchByNameContainingAndApplication()
    {
        List<String> matchingUsernames = aliasDAO.search(QueryBuilder.queryFor(String.class, EntityDescriptor.alias()).with(Combine.allOf(
            Restriction.on(AliasTermKeys.ALIAS).containing("ob"),
            Restriction.on(AliasTermKeys.APPLICATION_ID).exactlyMatching(1L))).returningAtMost(10));

        assertThat(matchingUsernames, contains(USER1));
    }
}
