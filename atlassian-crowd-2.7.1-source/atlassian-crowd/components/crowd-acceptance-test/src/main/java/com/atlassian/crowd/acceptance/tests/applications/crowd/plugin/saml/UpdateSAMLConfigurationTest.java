package com.atlassian.crowd.acceptance.tests.applications.crowd.plugin.saml;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class UpdateSAMLConfigurationTest extends CrowdAcceptanceTestCase
{
    private static final String KEY_PATH = "/plugin-data/crowd-saml-plugin";
    private static final String PUBLIC_KEY = "DSAPublic.key";
    private static final String PRIVATE_KEY = "DSAPrivate.key";

    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        _loginAdminUser();
        restoreBaseSetup();
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    protected File getKeyPath()
    {
        return new File(getCrowdHome() + KEY_PATH);
    }

    protected boolean pluginDataExists()
    {
        File keyPath = getKeyPath();
        if (keyPath.exists() && keyPath.isDirectory() && keyPath.list() != null)
        {
            List files = Arrays.asList(keyPath.list());
            if (files.contains(PUBLIC_KEY) && files.contains(PRIVATE_KEY))
            {
                return true;
            }
        }

        return false;
    }

    protected void createPluginData() throws IOException
    {
        File keyPath = getKeyPath();
        File privateKey = new File(getKeyPath() + "/" + PRIVATE_KEY);
        File publicKey = new File(getKeyPath() + "/" + PUBLIC_KEY);
        String keyString = "some bogus key string";

        FileUtils.forceMkdir(keyPath);
        FileUtils.writeByteArrayToFile(privateKey, keyString.getBytes());
        FileUtils.writeByteArrayToFile(publicKey, keyString.getBytes());
    }

    protected void nukePluginData() throws IOException
    {
        File keyPath = getKeyPath();
        if (keyPath.exists())
        {
            FileUtils.forceDelete(keyPath);
        }
    }

    public void testViewSAMLConfig()
    {
        log("Running testViewSAMLConfig");

        gotoSAMLConfig();

        assertTitleMatch(getMessage("menu.viewapplication.label"));
        assertKeyPresent("saml.auth.url.label");
        assertKeyPresent("saml.signout.url.label");
        assertKeyPresent("saml.password.url.label");
        assertKeyPresent("saml.key.label");

        assertTextPresent(getBaseUrl() + "/console/plugin/secure/saml/samlauth.action");
        assertTextPresent(getBaseUrl() + "/console/logoff.action");
        assertTextPresent(getBaseUrl() + "/console/user/viewchangepassword.action");

        assertButtonPresent("keygenButton");
        if (pluginDataExists())
        {
            assertButtonPresent("keydelButton");
            assertTextPresent(getCrowdHome() + "/plugin-data/crowd-saml-plugin");
        }
        else
        {
            assertKeyPresent("saml.key.none");
        }
    }

    public void testGenerateKeys() throws IOException
    {
        log("Running testGenerateKeys");

        gotoSAMLConfig();

        // no keys
        nukePluginData();
        assertFalse(pluginDataExists());

        clickButton("keygenButton");

        // keys generated
        assertTrue(pluginDataExists());
        assertKeyPresent("saml.key.gen.success");
        assertButtonPresent("keydelButton");
        assertTextPresent(getCrowdHome() + "/plugin-data/crowd-saml-plugin");
    }

    public void testOverwriteKeys() throws IOException
    {
        log("Running testOverwriteKeys");

        gotoSAMLConfig();

        // has keys
        if (!pluginDataExists())
        {
            createPluginData();
            assertTrue(pluginDataExists());
        }

        clickButton("keygenButton");

        // keys generated
        assertTrue(pluginDataExists());
        assertKeyPresent("saml.key.gen.success");
        assertButtonPresent("keydelButton");
        assertTextPresent(getCrowdHome() + "/plugin-data/crowd-saml-plugin");
    }

    public void testDeleteKeys()
    {
        log("Running testDeleteKeys");

        gotoSAMLConfig();

        // generate keys
        clickButton("keygenButton");

        assertTrue(pluginDataExists());
        assertKeyPresent("saml.key.gen.success");
        assertButtonPresent("keydelButton");
        assertTextPresent(getCrowdHome() + "/plugin-data/crowd-saml-plugin");

        // delete keys
        clickButton("keydelButton");

        assertFalse(pluginDataExists());
        assertKeyPresent("saml.key.del.success");
        assertButtonNotPresent("keydelButton");
        assertKeyPresent("saml.key.none");
    }
}
