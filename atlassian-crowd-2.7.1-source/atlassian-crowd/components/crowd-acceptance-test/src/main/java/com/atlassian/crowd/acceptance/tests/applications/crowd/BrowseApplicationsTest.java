package com.atlassian.crowd.acceptance.tests.applications.crowd;

public class BrowseApplicationsTest extends CrowdAcceptanceTestCase
{
    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        _loginAdminUser();

        // this file has v1.5 data
        restoreCrowdFromXML("googletest.xml");
    }

    public void testBrowseApplications()
    {
        log("Running testBrowseApplications");

        gotoBrowseApplications();

        // check names
        assertTextInTable("application-table", new String[]{"crowd", "Crowd Console", "View"});
        assertTextInTable("application-table", new String[]{"crowd-openid-server", "CrowdID OpenID Provider", "View"});
        assertTextInTable("application-table", new String[]{"demo", "Crowd Demo Application", "View"});
        assertTextInTable("application-table", new String[]{"google-apps", "Google Applications Connector", "View"});
    }
}
