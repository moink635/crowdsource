package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.util.Arrays;

import com.atlassian.crowd.model.application.ApplicationType;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import net.sourceforge.jwebunit.api.IElement;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static org.junit.Assert.assertThat;

/**
 * Web acceptance test for the adding of an Application
 */
public class AddApplicationTest extends CrowdAcceptanceTestCase
{
    private static final String APPLICATION_NAME = "Acceptance Test Application";
    private static final String IP_ADDRESS = "127.0.0.1";
    private static final String CIDR_ADDRESS = "127.0.0.1/16";

    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        _loginAdminUser();
        restoreBaseSetup();
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testAddApplicationWithNoDetails()
    {
        log("Running testAddApplicationWithNoDetails");

        gotoAddApplication();

        setTextField("name", "");
        setTextField("description", "");
        setTextField("password", "");
        setTextField("passwordConfirmation", "");
        selectOptionByValue("applicationType", "");

        submit();

        // Assert all the errors
        assertKeyPresent("application.name.invalid");
        assertKeyPresent("invalid.mustsupplyapassword");
        assertKeyPresent("application.type.invalid");

    }

    public void testAddApplicationThatAlreadyExists()
    {
        log("Running testAddApplicationThatExists");

        gotoAddApplication();

        setTextField("name", "crowd");
        setTextField("description", "Duplicated Crowd web app");
        setTextField("password", "password");
        setTextField("passwordConfirmation", "password");
        selectOption("applicationType", ApplicationType.JIRA.getDisplayName());

        submit();

        // Assert the errors
        assertKeyPresent("invalid.namealreadyexist");
    }

    public void testPasswordMatchError()
    {
        log("Running testPasswordMatchError");

        gotoAddApplication();

        setTextField("name", "");
        setTextField("description", "");
        setTextField("password", "password1");
        setTextField("passwordConfirmation", "password2");
        selectOption("applicationType", ApplicationType.JIRA.getDisplayName());

        submit();

        // Assert the errors
        assertKeyPresent("invalid.passwordmismatch");
    }

    public void testAddApplicationIPNotation()
    {
        intendToModifyData();

        _testAddApplicationDetails();

        _testAddApplicationConnectionDetailsIPNotation();

        _testAddApplicationDirectoryDetailsShowDescriptiveNames();

        _testAddApplicationDirectoriesDetails();

        _testAddApplicationAuthorisationDetails();

        _testAddApplicationConfirmationDetails(IP_ADDRESS);
    }

    public void testAddApplicationCIDRNotation()
    {
        intendToModifyData();

        _testAddApplicationDetails();

        _testAddApplicationConnectionDetailsCIDRNotation();

        _testAddApplicationDirectoryDetailsShowDescriptiveNames();

        _testAddApplicationDirectoriesDetails();

        _testAddApplicationAuthorisationDetails();

        _testAddApplicationConfirmationDetails(CIDR_ADDRESS);
    }

    private void _testAddApplicationDetails()
    {
        log("Running testAddApplicationDetails");

        gotoAddApplication();

        setTextField("name", APPLICATION_NAME);
        setTextField("description", "Application for the Acceptance Test Harness");
        setTextField("password", "password");
        setTextField("passwordConfirmation", "password");
        selectOption("applicationType", ApplicationType.GENERIC_APPLICATION.getDisplayName());

        submit();

        assertTextPresent(toLowerCase(APPLICATION_NAME));
    }

    private void _testAddApplicationConnectionDetailsIPNotation()
    {
        log("Running testAddApplicationConnectionDetailsIPNotation");

        // Test for blank data
        setTextField("applicationURL", "");
        setTextField("remoteIPAddress", "");
        submit();

        assertKeyPresent("application.url.invalid");
        assertKeyPresent("application.remoteipaddress.invalid");

        // Validate invalid data
        setTextField("applicationURL", "this is not a URL");
        // 1234567895 is actually a valid IPv4 address
        // http://en.wikipedia.org/wiki/IPv4#Address_representations
        setTextField("remoteIPAddress", "1234567895z");
        submit();

        assertKeyPresent("application.remoteipaddress.invalid");
        assertKeyPresent("application.url.invalid");

        // Enter valid data
        setTextField("applicationURL", "http://crowd.atlassian.com");
        setTextField("remoteIPAddress", IP_ADDRESS);
        submit();
    }

    private void _testAddApplicationConnectionDetailsCIDRNotation()
    {
        log("Running testAddApplicationDetailsCIDRNotation");
        // Enter valid data
        setTextField("applicationURL", "http://crowd.atlassian.com");
        setTextField("remoteIPAddress", CIDR_ADDRESS);
        submit();
    }

    private void _testAddApplicationDirectoryDetailsShowDescriptiveNames()
    {
        Iterable<IElement> fieldDescriptions = getElementsByXPath("//input[@type = 'checkbox']/following-sibling::div");

        assertThat(fieldDescriptions,
                Matchers.contains(elementWithTrimmedText("Crowd Internal Directory")));
    }

    private static Matcher<IElement> elementWithTrimmedText(String s)
    {
        return new FeatureMatcher<IElement, String>(Matchers.equalTo(s), "IElement with trimmed text content", "trimmed text content")
        {
            @Override
            protected String featureValueOf(IElement actual)
            {
                return actual.getTextContent().trim();
            }
        };
    }

    private void _testAddApplicationDirectoriesDetails()
    {
        log("Running testAddApplicationDirectoriesDetails");

        // Do not select a directory
        submit();
        assertKeyPresent("application.directories.invalid", Arrays.asList(toLowerCase(APPLICATION_NAME)));

        // Select the directory
        checkCheckbox("selecteddirectories");
        submit();
    }

    private void _testAddApplicationAuthorisationDetails()
    {
        log("Running testAddApplicationAuthorisationDetails");

        // Select a group to add to the directory
        selectOption("addgrouptodirectory-23", "crowd-administrators");
        clickButton("add-group-23");

        submit();
    }

    private void _testAddApplicationConfirmationDetails(String remoteAddress)
    {
        log("Running testAddApplicationConfirmationDetails");
        // Assert values on the confirmation screen
        assertTextPresent(toLowerCase(APPLICATION_NAME));
        assertTextPresent(ApplicationType.GENERIC_APPLICATION.getDisplayName());
        assertTextPresent("http://crowd.atlassian.com");
        assertTextPresent(remoteAddress);
        assertTextPresent("crowd-administrators");

        submit();

        // We should now be on the View Application Page
        assertTitleMatch(getMessage("menu.viewapplication.label"));
        assertTextPresent(toLowerCase(APPLICATION_NAME));
    }
}
