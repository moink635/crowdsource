package com.atlassian.crowd.acceptance.tests.applications.crowdid.client;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.acceptance.tests.BaseUrlFromProperties;

import com.google.common.base.Functions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import org.apache.commons.lang3.StringUtils;
import org.hamcrest.Matcher;
import org.hamcrest.text.IsEqualIgnoringWhiteSpace;

import net.sourceforge.jwebunit.api.IElement;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.collection.IsMapContaining.hasEntry;
import static org.hamcrest.collection.IsMapContaining.hasKey;
import static org.junit.Assert.assertThat;

/**
 * This class contains tests which examine the authentication process
 * between the crowd-openid-client and crowd-openid-server. The openid
 * server retrieves user data from the crowd-console.
 *
 * It is a 3-webapp test that currently tests basic forms of OpenID
 * authentication + SREG exchange.
 */
public class OpenIDAuthenticationTest extends CrowdIDClientAcceptanceTestCase
{
    protected String OPENIDSERVER_LOGIN_URL_ADMIN;
    protected String OPENID_SERVER_URL;
    private String OPENID_CLIENT_REALM;
    protected final static String SERVER_NAME = "crowdid";

    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        loginToCrowd();
        restoreCrowdFromXML("openidauthtest.xml");
        logoutFromCrowd();

        // load specific properties if defined
        BaseUrlFromProperties props = BaseUrlFromProperties.withLocalTestProperties();

        // CrowdID Client details
        OPENID_CLIENT_REALM = props.baseUrlFor(getApplicationName());

        // CrowdID Server details
        OPENID_SERVER_URL = props.baseUrlFor(SERVER_NAME);
        OPENIDSERVER_LOGIN_URL_ADMIN = OPENID_SERVER_URL + "/users/" + CROWD_ADMIN_USER;

        tearDownApprovedSitesForAdmin();
    }

    /**
     * Remove any 'Allow Always' approvals.
     */
    public void tearDownApprovedSitesForAdmin()
    {
        gotoPage(OPENID_SERVER_URL + "/secure/interaction/editallowalways!default.action");
        assertTitleEquals("Atlassian CrowdID - Login");

        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        setScriptingEnabled(true);
        submit();

        assertTitleEquals("Atlassian CrowdID - Approved Sites");

        Collection<IElement> sites = getElementsByXPath("//img[@title='Remove approved site from profile.']");

        if (!sites.isEmpty())
        {
            for (IElement e : sites)
            {
                String id = e.getAttribute("id");

                clickElementByXPath("id('" + id + "')");
            }

            clickButtonWithText("Apply");

            assertThat(getElementTextByXPath("//p[@class='successBox']"), equalToIgnoringWhiteSpace("Update Successful."));
        }

        gotoPage(OPENID_SERVER_URL + "/logoff.action");
        assertTitleEquals("Atlassian CrowdID - Login");
    }

    protected void assertAtClientLoginPage()
    {
        assertKeyPresent("login.title");
        assertKeyNotPresent("menu.logout.label");
        assertKeyNotPresent("menu.profile.label");
    }

    protected void assertAtServerLoginPage()
    {
        assertTitleEquals("Atlassian CrowdID - Login");
    }

    Map<String, String> scrapeKeyValueTable(String id)
    {
        Builder<String, String> mb = ImmutableMap.builder();

        for (List<String> ls : scrapeTable(id, Functions.<List<String>>identity()))
        {
            assertEquals(2, ls.size());

            mb.put(ls.get(0), ls.get(1));
        }

        return mb.build();
    }

    /**
     * Override {@link IsEqualIgnoringWhiteSpace} and also ignore no-break space.
     */
    private static Matcher<String> equalToIgnoringWhiteSpace(String s)
    {
        return new IsEqualIgnoringWhiteSpace(s) {
            @Override
            public String stripSpace(String toBeStripped)
            {
                return toBeStripped.replaceAll("[\\s\u00A0]+", " ").trim();
            }
        };
    }

    protected void assertAtServerAllowDenyPage(String clientUrl, String serverUrl, String nickName, String fullName, String email)
    {
        assertTitleEquals("Atlassian CrowdID - OpenID Verification");
        assertThat(getElementTextById("requestingSite"), equalToIgnoringWhiteSpace(clientUrl));
        assertThat(getElementTextById("requestingIdentity"), equalToIgnoringWhiteSpace(serverUrl));
        assertThat(getElementTextById("requestingAttributes"),
                equalToIgnoringWhiteSpace("nickname   email   fullname   dob   gender   postcode   country   language   timezone"));

        Map<String, String> attributeTable = scrapeKeyValueTable("attributeTable");

        assertThat(attributeTable, hasEntry("Nickname", nickName));
        assertThat(attributeTable, hasEntry("Full Name", fullName));
        assertThat(attributeTable, hasEntry("Email", email));

        assertThat(attributeTable, hasKey("Country"));
        assertThat(attributeTable, hasKey("Language"));
    }

    protected void assertAtClientProfilePage(String serverUrl, String userName, String email, String userFullName)
    {
        assertKeyNotPresent("login.title");
        assertKeyPresent("menu.logout.label");
        assertKeyPresent("menu.profile.label");

        IElement link = getElementByXPath("id('identifierTable')/tbody/tr/td/a");

        assertEquals(serverUrl, link.getTextContent());
        assertEquals(serverUrl, link.getAttribute("href"));

        assertEquals(scrapeKeyValueTable("identifierTable"), ImmutableMap.of("OpenID Identifier (URL)", serverUrl));

        Map<String, String> attributesTable = scrapeKeyValueTable("attributesTable");
        assertThat(attributesTable, hasEntry("nickname", userName));
        assertThat(attributesTable, hasEntry("email", email));
        assertThat(attributesTable, hasEntry("fullname", userFullName));

        assertThat(attributesTable, hasKey("language"));
        assertThat(attributesTable, hasKey("country"));
    }

    private void submitAndAssertAtServerLoginPage()
    {
        setScriptingEnabled(true);
        submit();
        assertAtServerLoginPage();
        setScriptingEnabled(false);
    }

    public void testOpenIDAuthenticationAllow()
    {
        log("Running testOpenIDAuthenticationAllow");

        gotoPage("/");

        // login page (client)
        assertAtClientLoginPage();
        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        submitAndAssertAtServerLoginPage();

        // login page (server)
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        setScriptingEnabled(true);
        submit();

        // allow/deny page (server)
        assertAtServerAllowDenyPage(OPENID_CLIENT_REALM, OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "Super User", "admin@example.com");
        setScriptingEnabled(false);
        clickLink("allow");

        // profile page (client)
        assertAtClientProfilePage(OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "admin@example.com", "Super User");
    }

    public void testOpenIDAuthenticationDeny()
    {
        log("Running testOpenIDAuthenticationDeny");

        gotoPage("/");

        // login page (client)
        assertAtClientLoginPage();
        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        submitAndAssertAtServerLoginPage();

        // login page (server)
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        setScriptingEnabled(true);
        submit();

        // allow/deny page (server)
        assertAtServerAllowDenyPage(OPENID_CLIENT_REALM, OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "Super User", "admin@example.com");
        setScriptingEnabled(false);
        clickLink("deny");

        // login page with error (client)
        assertAtClientLoginPage();
        assertTextPresent("Authentication Failed: Your OpenID provider was unable to authenticate the OpenID URL. Log in to your OpenID provider and try again.");
    }

    public void testOpenIDAuthenticationFailsWhenUsernamePaddedBeforeFinalSlash()
    {
        String loginUrlNotTheAdmin = OPENID_SERVER_URL + "/users/" + "not/the/admin";

        log("Running testOpenIDAuthenticationDeny");

        gotoPage("/");

        // login page (client)
        assertAtClientLoginPage();
        setTextField("openid_identifier", loginUrlNotTheAdmin);
        submitAndAssertAtServerLoginPage();

        // login page (server)
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        assertTextPresent("A request has been made to authenticate an OpenID identifier, different from the one you are logged in as.");
    }

    public void testOpenIDAuthenticationStateless()
    {
        log("Running testOpenIDAuthenticationStateless");

        gotoPage("/");

        // login page (client)
        assertAtClientLoginPage();
        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        checkCheckbox("dummyMode");
        submitAndAssertAtServerLoginPage();

        // login page (server)
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        // allow/deny page (server)
        assertAtServerAllowDenyPage(OPENID_CLIENT_REALM, OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "Super User", "admin@example.com");
        clickLink("allow");

        // profile page (client)
        assertAtClientProfilePage(OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "admin@example.com", "Super User");
    }

    public void testOpenIDAuthenticationImmediate_fail()
    {
        log("Running testOpenIDAuthenticationImmediate_fail");

        gotoPage("/");

        // login page (client)
        assertAtClientLoginPage();
        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        checkCheckbox("checkImmediate");
        setScriptingEnabled(true);
        submit();

        // login page with error (client)
        assertAtClientLoginPage();
        setScriptingEnabled(false);
        assertTextPresent("Authentication Failed: Your OpenID provider was unable to authenticate the OpenID URL. Log in to your OpenID provider and try again.");
    }

    private void establishApproveAlwaysForAdmin()
    {
        /* Run through once and 'Allow Always' */
        gotoPage("/");

        // login page (client)
        assertAtClientLoginPage();
        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        submitAndAssertAtServerLoginPage();

        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        // allow/deny page (server)
        assertAtServerAllowDenyPage(OPENID_CLIENT_REALM, OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "Super User", "admin@example.com");
        clickLink("allowAlways");

        // profile page (client)
        assertAtClientProfilePage(OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "admin@example.com", "Super User");

        gotoPage("/logoff.action");
    }

    public void testOpenIDAuthenticationImmediateSucceedsAfterAllowAlwaysApproval()
    {
        establishApproveAlwaysForAdmin();

        /* Run through again with 'immediate' */
        gotoPage("/");

        // login page (client)
        assertAtClientLoginPage();
        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        checkCheckbox("checkImmediate");
        setScriptingEnabled(true);
        submit();

        assertAtClientProfilePage(OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "admin@example.com", "Super User");
        setScriptingEnabled(false);
    }

    public void testOpenIDClientShouldWorkWithUrlsWhichContainUTF8Characters() throws UnsupportedEncodingException
    {
        String serverUrl =  OPENID_SERVER_URL + "/users/" + "john.tøstinógé";
        assertIsAbleToAuthenticate("john.tøstinógé", "john", serverUrl,
                "john@example.com", "John Tøstinógé", "/users/john.t%C3%B8stin%C3%B3g%C3%A9");
    }

    public void testOpenIDClientShouldWorkWithUrlInEncodedFormat() throws UnsupportedEncodingException
    {
        String serverUrl =  OPENID_SERVER_URL + "/users/" + "john.t%C3%B8stin%C3%B3g%C3%A9";
        assertIsAbleToAuthenticate("john.tøstinógé", "john", serverUrl,
                "john@example.com", "John Tøstinógé", "/users/john.t%C3%B8stin%C3%B3g%C3%A9");
    }

    public void testOpenIDTrimsUrlsBeforeAuthenticatingRequest()
    {
        gotoPage("/");

        // Modify the Open ID url to add some blank spaces.
        String openidserver_login_url_admin = "  " + OPENIDSERVER_LOGIN_URL_ADMIN + "   ";

        setTextField("openid_identifier", openidserver_login_url_admin);
        submitAndAssertAtServerLoginPage();

        // login page (server)
        // This would fail if blank spaces were not trimmed properly.

        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        // allow/deny page (server)
        assertAtServerAllowDenyPage(OPENID_CLIENT_REALM, OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "Super User", "admin@example.com");
        clickLink("allow");

        // profile page (client)
        assertAtClientProfilePage(OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "admin@example.com", "Super User");
    }

    private void assertIsAbleToAuthenticate(String username, String password, String serverUrl,
            String email, String fullName, String relativeIdentifier) throws UnsupportedEncodingException
    {
        gotoPage("/");

        assertAtClientLoginPage();

        setTextField("openid_identifier", serverUrl);
        submitAndAssertAtServerLoginPage();

        // login page (server)
        setTextField("username", username);
        setTextField("password", password);
        submit();

        //The server url is always decoded regardless of the input.
        String openIdUrl = OPENID_SERVER_URL + relativeIdentifier;


        // allow/deny page (server)
        assertAtServerAllowDenyPage(OPENID_CLIENT_REALM, openIdUrl, username, fullName, email);
        clickLink("allow");

        // profile page (client)
        assertAtClientProfilePage(openIdUrl, username, email, fullName);
    }

    private String getOpEndpointUrl()
    {
        return OPENID_SERVER_URL + "/op";
    }

    public void testServerSelectsIdentityForLoggedInUserWhenOpEndpointIsProvided()
    {
        gotoPage("/");

        assertAtClientLoginPage();

        setTextField("openid_identifier", getOpEndpointUrl());
        setScriptingEnabled(true);
        submitAndAssertAtServerLoginPage();

        // login page (server)
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        // allow/deny page (server)
        assertAtServerAllowDenyPage(OPENID_CLIENT_REALM, OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "Super User",
                "admin@example.com");
        clickLink("allow");

        // profile page (client)
        assertAtClientProfilePage(OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "admin@example.com", "Super User");
    }

    public void testServerSelectsIdentityForLoggedInUserWhenOpEndpointIsProvidedWithNonAdminUser()
    {
        gotoPage("/");

        assertAtClientLoginPage();

        setTextField("openid_identifier", getOpEndpointUrl());
        setScriptingEnabled(true);
        submitAndAssertAtServerLoginPage();

        // login page (server)
        setTextField("username", "john.tøstinógé");
        setTextField("password", "john");
        submit();

        String identity = OPENID_SERVER_URL + "/users/john.t%C3%B8stin%C3%B3g%C3%A9";

        // allow/deny page (server)
        assertAtServerAllowDenyPage(OPENID_CLIENT_REALM, identity, "john.tøstinógé", "John Tøstinógé", "john@example.com");
        clickLink("allow");

        // profile page (client)
        assertAtClientProfilePage(identity, "john.tøstinógé", "john@example.com", "John Tøstinógé");
    }

    public void testServerSelectsIdentityForLoggedInUserWhenImmediateCheckIsMade()
    {
        establishApproveAlwaysForAdmin();

        /* Run through again with 'immediate' */
        gotoPage("/");

        // login page (client)
        assertAtClientLoginPage();
        setTextField("openid_identifier", getOpEndpointUrl());
        checkCheckbox("checkImmediate");
        setScriptingEnabled(true);
        submit();

        assertAtClientProfilePage(OPENIDSERVER_LOGIN_URL_ADMIN, "admin", "admin@example.com", "Super User");
        setScriptingEnabled(false);
    }

    public void testRequiredAttribsCorrectlyEscaped() throws UnsupportedEncodingException
    {
        String op = getOpEndpointUrl();
        String requiredAttribs = "<b>test</b>";

        String url = "/login!login.action?openid_identifier=" + URLEncoder.encode(op, "us-ascii") + "&requiredAttribs=" + requiredAttribs;

        /* Force use of a form, rather than redirection */
        String padding = "&requiredAttribs=" + StringUtils.repeat('x', 2048);

        tester.setScriptingEnabled(false);
        gotoPage(url + padding);

        assertThat(getPageSource(), not(containsString("<b>test</b>")));
        assertThat(getPageSource(), containsString("&lt;b&gt;test&lt;/b&gt;"));
    }

    public void testRequiredAttribsCorrectlyEscapedWhenRedirecting() throws MalformedURLException, IOException
    {
        String op = getOpEndpointUrl();
        String requiredAttribs = "<b>test</b>";

        String url = "/login!login.action?openid_identifier=" + URLEncoder.encode(op, "us-ascii") + "&requiredAttribs=" + requiredAttribs;

        HttpURLConnection connection = (HttpURLConnection) new URL(getBaseUrl() + url).openConnection();

        connection.setInstanceFollowRedirects(false);

        assertEquals(302, connection.getResponseCode());

        assertThat(connection.getHeaderField("Location"),
                containsString("openid.sreg.required=%3Cb%3Etest%3C%2Fb%3E"));
    }

    public void testAbleToAuthenticateWithPlusInUsername() throws UnsupportedEncodingException
    {
        String serverUrl =  OPENID_SERVER_URL + "/users/" + "user+tag";
        assertIsAbleToAuthenticate("user+tag", "user+tag", serverUrl,
                "user+tag@localhost", "User +Tag", "/users/user+tag");
    }
}
