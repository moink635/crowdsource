package com.atlassian.crowd.acceptance.tests.applications.crowd.plugin.saml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.hamcrest.CoreMatchers;

import junit.framework.AssertionFailedError;

import static org.junit.Assert.assertThat;

/**
 * This tests the SAML Authentication interaction but does
 * not rigorously examine the actual response message sent
 * back to Google.
 *
 * Please see the TestSAMLMessageManagerImpl for response
 * testing.
 *
 * In place of the Google server, we use the 500.jsp page as
 * our "mock Google" response recipient and use GET requests
 * as our "mock Google" request generator.
 *
 * Both SSO and sign-in-after-login workflows are tested.
 */
public class SAMLAuthTest extends CrowdAcceptanceTestCase
{
    private static final String SAMPLE_GOOGLE_REQUEST = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<samlp:AuthnRequest xmlns:samlp=\"urn:oasis:names:tc:SAML:2.0:protocol\" ID=\"cliofbcolgkmklnaoeafjjcakpnkblpccdjkpcla\" Version=\"2.0\" IssueInstant=\"2008-07-21T06:22:10Z\" ProtocolBinding=\"urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST\" ProviderName=\"google.com\" IsPassive=\"false\" AssertionConsumerServiceURL=\"https://www.google.com/a/thanksforcomingin.com/acs\"><saml:Issuer xmlns:saml=\"urn:oasis:names:tc:SAML:2.0:assertion\">google.com</saml:Issuer><samlp:NameIDPolicy AllowCreate=\"true\" Format=\"urn:oasis:names:tc:SAML:1.1:nameid-format:unspecified\" /></samlp:AuthnRequest>";
    private static final String SAMPLE_GOOGLE_RELAY = "https%3A%2F%2Fwww.google.com%2Fa%2Fthanksforcomingin.com%2FServiceLogin%3Fservice%3Dmail%26passive%3Dtrue%26rm%3Dfalse%26continue%3Dhttp%253A%252F%252Fmail.google.com%252Fa%252Fthanksforcomingin.com%252F%26bsv%3D1k96igf4806cy%26ltmpl%3Ddefault%26ltmplcache%3D2";
    private static final boolean WITHOUT_ZLIB_HEADER = true; // SAML spec uses RFC 1951, not RFC 1950

    public void setUp() throws Exception
    {
        super.setUp();
        _loginAdminUser();
        restoreCrowdFromXML("googletest.xml");
    }

    protected void forgeGoogleRequest() throws IOException
    {
        // this is similar to a SAML request Google would make to Crowd
        // when a user attempts to access Google Apps
        gotoPage("/console/plugin/secure/saml/samlauth.action?SAMLRequest=" + generateGoogleSAMLRequestParameter() + "&RelayState=" + SAMPLE_GOOGLE_RELAY);
    }

    protected void forgeInvalidGoogleRequest()
    {
        gotoPage("/console/plugin/secure/saml/samlauth.action?SAMLRequest=fVLJTsMKNd7FmhP1MDfkpfEBStNJlJ5G40mZnojjY5EePXNWfDqda7Nv8F%2Bs5Z5E4rosi6i4X5SDwEavwN0FdsZrxLqFWGG3sy8kkd4EuJItAWczInA%2BBLxAQ30HbgFuoxU8PtxkvPHekkiS7XYbf8skMvGNNGuq0IVjcNdmjyvi%2BTBkMfR0P6b7fwv5lYLn3z7T5IdU%2Fvl5u07zywJbrd7ZrG1xe%2BFA%2BlDIuz70uULXSf%2B32ygeDYheRdVAFb0hC0pXGlacJfne9feWhN35AA%3D%3D&RelayState=https%3A%2F%2Fwww.google.com%2Fa%2Fthanksforcomingin.com%2FServic%252Fmail.google.com%252Fa%252Fthanksforcomingin.com%252F%26bsv%3D1k96igf4806cy%26ltmpl%3Ddefault%26ltmplcache%3D2");
    }

    protected void ensureNoKeysPresent()
    {
        gotoSAMLConfig();

        try
        {
            assertButtonPresent("keydelButton"); // this won't log the page if the button doesn't exist
            clickButton("keydelButton");
        }
        catch (AssertionFailedError e)
        {
            // delete button doesn't exist
        }

        assertButtonNotPresent("keydelButton");
        assertKeyPresent("saml.key.none");
    }

    protected void ensureKeysPresent()
    {
        gotoSAMLConfig();

        try
        {
            clickButton("keygenButton");
        }
        catch (AssertionFailedError e)
        {
            // delete button doesn't exist
        }

        assertKeyPresent("saml.key.gen.success");
        assertButtonPresent("keydelButton");
    }

    public void testAuthWithNoKeysSetup() throws IOException
    {
        log("Running testAuthWithNoKeysSetup");

        ensureNoKeysPresent();

        forgeGoogleRequest();

        assertTextPresent("SAML Error: SAML signature keys have not been set up.");
    }

    public void testAuthWithGarbageRequest()
    {
        log("Running testAuthWithGarbageRequest");

        ensureKeysPresent();

        forgeInvalidGoogleRequest();

        assertTextPresent("SAML Error: Error decoding SAML Authentication Request");
    }

//  The following 2 tests have been commented out because they do not pass in Java 1.6.
//  Java 1.6 includes xml-security 1.3.1 in the JDK and we need 1.4.2.

    public void testAuthSuccessSSO() throws IOException
    {
        log("Running testAuthSuccessSSO");

        ensureKeysPresent();

        forgeGoogleRequest();

        verifySAMLResponse();
    }

    public void testAuthSuccessAfterLogin() throws IOException
    {
        log("Running testAuthSuccessAfterLogin");

        ensureKeysPresent();

        _logout();

        forgeGoogleRequest();

        // verify we are at the login login screen (cf. interceptor pattern)
        assertKeyPresent("login.title");

        // login
        setTextField("j_username", ADMIN_USER);
        setTextField("j_password", ADMIN_PW);
        submit();

        // assert that we are redirected to "google apps"
        verifySAMLResponse();
    }

    protected void verifySAMLResponse()
    {
        // should be redirected to a 500 page with headers nicely displayed
        assertTextPresent("Oops - an error has occurred");

        // verify that some of the key SAML XML data is present
        assertTextPresent("SAMLResponse : &lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot;?&gt;");
        assertTextPresent("&lt;samlp:Response xmlns:samlp=&quot;urn:oasis:names:tc:SAML:2.0:protocol&quot; xmlns=&quot;urn:oasis:names:tc:SAML:2.0:assertion&quot; xmlns:xenc=&quot;http://www.w3.org/2001/04/xmlenc#&quot; ID=&quot;");
        assertTextPresent("&lt;Signature xmlns=&quot;http://www.w3.org/2000/09/xmldsig#&quot;&gt;&lt;SignedInfo&gt;&lt;CanonicalizationMethod Algorithm=&quot;http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments&quot; /&gt;&lt;SignatureMethod Algorithm=&quot;http://www.w3.org/2000/09/xmldsig#dsa-sha1&quot; /&gt;&lt;Reference URI=&quot;&quot;&gt;&lt;Transforms&gt;&lt;Transform Algorithm=&quot;http://www.w3.org/2000/09/xmldsig#enveloped-signature&quot; /&gt;&lt;/Transforms&gt;&lt;DigestMethod Algorithm=&quot;http://www.w3.org/2000/09/xmldsig#sha1&quot; /&gt;&lt;DigestValue&gt;");
        assertTextPresent("&lt;samlp:StatusCode Value=&quot;urn:oasis:names:tc:SAML:2.0:status:Success&quot; /&gt;");
        assertTextPresent("&lt;samlp:StatusCode Value=&quot;urn:oasis:names:tc:SAML:2.0:status:Success&quot; /&gt;");
        assertTextPresent("&lt;NameID Format=&quot;urn:oasis:names:tc:SAML:1.1:nameid-format:emailAddress&quot;&gt;");
        assertTextPresent(ADMIN_USER);
    }

    public void assertTextPresent(String text)
    {
        // WebTester.assertTextPresent() does not handle XML
        // presence testing properly
        try
        {
            if (!tester.getPageSource().contains(text))
            {
                fail("Expected text not found in current page: [" + text + "]");
            }
        }
        catch (junit.framework.AssertionFailedError error)
        {
            System.err.println("Unable to find " + text + " in:");
            System.err.print(tester.getPageSource());
            throw error;
        }
    }

    /**
     * Self sanity check that we're generating properly encoded requests.
     */
    public void testGeneratedRequestParameterDecodes() throws Exception
    {
        String base64 = URLDecoder.decode(generateGoogleSAMLRequestParameter(), "us-ascii");

        byte[] bytes = new Base64().decode(base64);

        InputStream in = new InflaterInputStream(new ByteArrayInputStream(bytes), new Inflater(WITHOUT_ZLIB_HEADER));

        assertThat(new String(IOUtils.toByteArray(in), "utf-8"), CoreMatchers.containsString("<?xml"));
    }

    // this method does not appear to work correctly on bamboo (the deflate does not behave correctly)
    // thus we have generated the output locally and stored it in SAMPLE_GOOGLE_FULL_REQUEST
    private String generateGoogleSAMLRequestParameter() throws UnsupportedEncodingException, IOException
    {
        // the 500 page lists the request headers nicely for us
        // so we can "see" the response that would have been
        // send to google
        String acsURL = getBaseUrl() + "/console/500.jsp";

        String requestXML = SAMPLE_GOOGLE_REQUEST.replaceAll("https://www.google.com/a/thanksforcomingin.com/acs", acsURL);

        // deflate
        ByteArrayOutputStream bout = new ByteArrayOutputStream();

        OutputStream out = new DeflaterOutputStream(bout, new Deflater(9, WITHOUT_ZLIB_HEADER));
        out.write(requestXML.getBytes("utf-8"));
        out.close();

        // base64 encode
        String encodedXML = new Base64().encodeToString(bout.toByteArray());

        // url encode
        encodedXML = URLEncoder.encode(encodedXML, "us-ascii");

        return encodedXML;
    }
}
