package com.atlassian.crowd.acceptance.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

import com.atlassian.core.util.ClassLoaderUtils;

import com.google.common.base.Preconditions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: Justin
 * Date: 12/03/2007
 */
public class AcceptanceTestHelper
{
    private static final Logger log = LoggerFactory.getLogger(AcceptanceTestHelper.class);

    private static final String IT_TEST_RESOURCES = "target/it-classes";

    protected static File loadFileFromClasspath(String source) throws Exception
    {
        File testFile = getResource(source);

        if (!testFile.exists())
        {
            throw new Exception("Could not find an existing file on filesystem to match:" + source);
        }

        return testFile;
    }

    public static String getTestResourcesLocation()
    {
        return IT_TEST_RESOURCES;
    }

    public static InputStream getResourceAsStream(String fileName) throws Exception
    {
        final File resource = new File(getTestResourcesLocation(), fileName);

        if (resource.exists())
        {
            return new FileInputStream(resource);
        }
        else
        {
            // Attempt to find the file on the Classpath
            final URL classpathResource = ClassLoaderUtils.getResource(fileName, AcceptanceTestHelper.class);
            return classpathResource.openStream();
        }
    }

    public static File getResource(String fileName)
    {
        final File fileResource = new File(getTestResourcesLocation(), fileName);

        if (fileResource.exists())
        {
            return fileResource;
        }
        else
        {
            final URL classpathResource = ClassLoaderUtils.getResource(fileName, AcceptanceTestHelper.class);

            if (classpathResource != null)
            {
                try
                {
                    return new File(classpathResource.toURI());
                }
                catch (URISyntaxException e)
                {
                    throw new RuntimeException(e);
                }
            }
            throw new IllegalArgumentException("File does not exist: " + fileName);
        }
    }

    public static String getResourcePath(String fileName)
    {
        return getResource(fileName).getAbsolutePath();
    }

    public static Properties loadProperties(String fileName)
    {
        Preconditions.checkNotNull(fileName);

        Properties props = new Properties();

        InputStream inputStream = null;
        try
        {
            inputStream = ClassLoaderUtils.getResourceAsStream(fileName, AcceptanceTestHelper.class);
            if (inputStream == null)
            {
                throw new RuntimeException("Unable to load properties file: " + fileName);
            }
            props.load(inputStream);
        }
        catch (IOException e)
        {
            throw new RuntimeException("Unable to load properties file: " + fileName, e);
        }
        finally
        {
            if (inputStream != null)
            {
                try
                {
                    inputStream.close();
                }
                catch (IOException e)
                {
                    log.error(e.getMessage(), e);
                }
            }

        }

        return props;
    }
}
