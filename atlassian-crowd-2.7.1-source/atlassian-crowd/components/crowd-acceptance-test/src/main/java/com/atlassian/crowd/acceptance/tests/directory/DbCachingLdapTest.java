package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.acceptance.tests.PatternMatcher;
import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.SynchronisableDirectory;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelperImpl;
import com.atlassian.crowd.directory.loader.CacheableDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.InternalDirectoryInstanceLoader;
import com.atlassian.crowd.directory.loader.LDAPDirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.directory.SynchronisationMode;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.password.encoder.AtlassianSHA1PasswordEncoder;
import com.atlassian.crowd.password.encoder.LdapShaPasswordEncoder;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.inject.Inject;
import javax.sql.DataSource;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import static com.atlassian.crowd.acceptance.tests.directory.ModelAssertions.assertGroupsEqual;
import static com.atlassian.crowd.acceptance.tests.directory.ModelAssertions.assertUsersEqual;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Tests the caching functionality of the {@link com.atlassian.crowd.directory.DbCachingRemoteDirectory}.
 * This test shouldn't be running in a single transaction.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/dbCachingRemoteDirectoryApplicationContext.xml",
                      "classpath:/applicationContext-CrowdEncryption.xml",
                      "classpath:/applicationContext-CrowdUtils.xml",
                      "classpath:/applicationContext-CrowdDAO.xml",
                      "classpath:/applicationContext-config.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         DirtiesContextTestExecutionListener.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)   // because it modifies some mocks
public class DbCachingLdapTest
{
    private static final String LDAP_USER_NAME = "user1";
    private static final String LDAP_USER2_NAME = "user2";
    private static final String LDAP_USER3_NAME = "user3";

    private static final String LDAP_GROUP_NAME = "ldap-group";
    private static final String LDAP_GROUP2_NAME = "ldap-group2";
    private static final String LDAP_GROUP3_NAME = "ldap-group3";
    private static final String LOCAL_GROUP_NAME = "local-group";
    private static final String LOCAL_GROUP2_NAME = "local-group2";

    private static final String EXTERNAL_ID_PATTERN = "(\\w){8}-(\\w){4}-(\\w){4}-(\\w){4}-(\\w){12}"; // LDAP server specific

    private RemoteDirectory remoteDirectory;
    private RemoteDirectory ldapDirectory;
    private User ldapUser;
    private Group ldapGroup;
    private Session session;

    private DirectoryImpl directory;

    @Inject private DataSource dataSource;
    @Inject private PasswordEncoderFactory passwordEncoderFactory;
    @Inject private SessionFactory sessionFactory;
    @Inject private DirectoryDao directoryDao;
    @Inject private MockDirectoryManager directoryManager;
    @Inject private LDAPPropertiesHelperImpl ldapPropertiesHelperImpl;
    @Inject private CacheableDirectoryInstanceLoader directoryInstanceLoader;
    @Inject private LDAPDirectoryInstanceLoader ldapDirectoryInstanceLoader;
    @Inject private InternalDirectoryInstanceLoader internalDirectoryInstanceLoader;

    @Before
    public void setUp() throws Exception
    {
        // make sure Atlassian Sha1 password encoder is present for DBCachingRemoteDirectory
        passwordEncoderFactory.addEncoder(new AtlassianSHA1PasswordEncoder());

        // clean up the database before a test run
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        DirectoryTestHelper.deleteFromTables(DirectoryTestHelper.TABLE_NAMES, jdbcTemplate);

        Properties properties = DirectoryTestHelper.getConfigProperties(DirectoryTestHelper.getApacheDS154ConfigFileName());

        // build our Crowd directory/connection object
        DirectoryImpl directoryTemplate = DirectoryTestHelper.createDirectoryTemplate(properties,
                                                                                      ldapPropertiesHelperImpl, true);

        // this test is non-transactional, therefore a temporal session is manually managed for directoryDao
        session = sessionFactory.openSession();
        session.setFlushMode(FlushMode.ALWAYS);
        TransactionSynchronizationManager.unbindResourceIfPossible(sessionFactory); // clean up
        TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));

        directory = new DirectoryImpl(directoryDao.add(directoryTemplate));

        // Set the directoryId before adding directory to be available for mock directory manager
        InternalEntityTemplate template = new InternalEntityTemplate(directory.getId(), directory.getName(), true, new Date(), new Date());
        DirectoryImpl templateForMockDirectoryManager = new DirectoryImpl(template);
        templateForMockDirectoryManager.updateDetailsFrom(directory);

        // this modifies the context, therefore it is not safe to reuse the context outside of this class
        directoryManager.setDirectory(templateForMockDirectoryManager);

        // enable cache
        directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.TRUE.toString());

        remoteDirectory = directoryInstanceLoader.getDirectory(directory);
        ldapDirectory = ldapDirectoryInstanceLoader.getDirectory(directory);

        // no point running tests if we can't connect to server
        directoryInstanceLoader.getRawDirectory(directory.getId(),
                                                directory.getImplementationClass(),
                                                directory.getAttributes()).testConnection();

        passwordEncoderFactory.addEncoder(new LdapShaPasswordEncoder());

        // prepare the data in the LDAP server
        removeTestData();
        loadTestData();
    }

    @After
    public void tearDown()
    {
        removeTestData();

        TransactionSynchronizationManager.unbindResource(sessionFactory);
        session.close();
    }

    /**
     * Puts some data in the LDAP server
     */
    private void loadTestData() throws Exception
    {
        User user = new ImmutableUser(directory.getId(), LDAP_USER_NAME, "Bob", "Smith", "Bob Smith", "bsmith@example.com");
        User user2 = new ImmutableUser(directory.getId(), LDAP_USER2_NAME, "Bob", "Smith", "Bob Smith", "bsmith@example.com");
        User user3 = new ImmutableUser(directory.getId(), LDAP_USER3_NAME, "Bob", "Smith", "Bob Smith", "bsmith@example.com");

        ldapUser = ldapDirectory.addUser(new UserTemplate(user), new PasswordCredential("password"));
        ldapDirectory.addUser(new UserTemplate(user2), new PasswordCredential("password"));
        ldapDirectory.addUser(new UserTemplate(user3), new PasswordCredential("password"));

        final GroupTemplate groupTemplate1 = new GroupTemplate(LDAP_GROUP_NAME, directory.getId());
        groupTemplate1.setDescription("LDAP Group 1");
        ldapGroup = ldapDirectory.addGroup(groupTemplate1);
        final GroupTemplate groupTemplate2 = new GroupTemplate(LDAP_GROUP2_NAME, directory.getId());
        groupTemplate2.setDescription("LDAP Group 2");
        ldapDirectory.addGroup(groupTemplate2);

        // memberships
        ldapDirectory.addUserToGroup(LDAP_USER2_NAME, LDAP_GROUP2_NAME);
        ldapDirectory.addUserToGroup(LDAP_USER3_NAME, LDAP_GROUP2_NAME);
        ldapDirectory.addGroupToGroup(LDAP_GROUP_NAME, LDAP_GROUP2_NAME);

        synchroniseCache(remoteDirectory);
    }

    /**
     * Removes test data from the LDAP server
     */
    public void removeTestData()
    {
        DirectoryTestHelper.silentlyRemoveUser(LDAP_USER_NAME, remoteDirectory);
        DirectoryTestHelper.silentlyRemoveUser(LDAP_USER2_NAME, remoteDirectory);
        DirectoryTestHelper.silentlyRemoveUser(LDAP_USER3_NAME, remoteDirectory);

        DirectoryTestHelper.silentlyRemoveGroup(LOCAL_GROUP_NAME, remoteDirectory);
        DirectoryTestHelper.silentlyRemoveGroup(LOCAL_GROUP2_NAME, remoteDirectory);

        DirectoryTestHelper.silentlyRemoveGroup(LDAP_GROUP_NAME, remoteDirectory);
        DirectoryTestHelper.silentlyRemoveGroup(LDAP_GROUP2_NAME, remoteDirectory);
        DirectoryTestHelper.silentlyRemoveGroup(LDAP_GROUP3_NAME, remoteDirectory);
    }

    /**
     * Synchronise the cache
     * @param remoteDirectory Directory to synchronize.
     */
    private static void synchroniseCache(final RemoteDirectory remoteDirectory) throws OperationFailedException
    {
        ((SynchronisableDirectory) remoteDirectory).synchroniseCache(SynchronisationMode.FULL, new MockSynchronisationStatusManager());
    }

    /**
     * Test the basic search operations to make sure the work normally.
     * @throws Exception
     */
    @Test
    public void testSearchOperations() throws Exception
    {
        User user = remoteDirectory.findUserByName(LDAP_USER_NAME);
        assertUsersEqual(ldapUser, user);

        Group group = remoteDirectory.findGroupByName(LDAP_GROUP_NAME);
        assertGroupsEqual(ldapGroup, group);

        assertFalse(remoteDirectory.isUserDirectGroupMember(LDAP_USER_NAME, LDAP_GROUP_NAME));
        assertTrue(remoteDirectory.isUserDirectGroupMember(LDAP_USER2_NAME, LDAP_GROUP2_NAME));
        assertTrue(remoteDirectory.isGroupDirectGroupMember(LDAP_GROUP_NAME, LDAP_GROUP2_NAME));

        EntityQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.user()).returningAtMost(EntityQuery.ALL_RESULTS);
        List<String> users = remoteDirectory.searchUsers(query);
        assertEquals(3, users.size());
        assertTrue(users.contains(LDAP_USER_NAME));
        assertTrue(users.contains(LDAP_USER2_NAME));
        assertTrue(users.contains(LDAP_USER3_NAME));

        query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).returningAtMost(EntityQuery.ALL_RESULTS);
        List<String> groups = remoteDirectory.searchGroups(query);
        assertEquals(3, users.size());
        assertTrue(groups.contains(LDAP_GROUP_NAME));
        assertTrue(groups.contains(LDAP_GROUP2_NAME));

        MembershipQuery<String> membershipquery =
                QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP_NAME)
                        .returningAtMost(EntityQuery.ALL_RESULTS);
        List<String> members = remoteDirectory.searchGroupRelationships(membershipquery);
        assertEquals(0, members.size());

        membershipquery =
                QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME)
                        .returningAtMost(EntityQuery.ALL_RESULTS);
        members = remoteDirectory.searchGroupRelationships(membershipquery);
        assertEquals(2, members.size());
        assertTrue(members.contains(LDAP_USER2_NAME));
        assertTrue(members.contains(LDAP_USER3_NAME));
    }

    /**
     * Test adding a user.
     * This test directly updates the ldap directory and checks the correct results are seen after the sync is done, but not before.
     * @throws Exception
     */
    @Test
    public void testAddUserViaLdap() throws Exception
    {
        User newUser = new ImmutableUser(directory.getId(), "USER99", "Alice", "Jones", "Alice Jones", "ajones@example.com");

        // Add to LDAP directory
        ldapUser = ldapDirectory.addUser(new UserTemplate(newUser), new PasswordCredential("password"));
        try
        {
            // external id should be empty
            assertThat(ldapUser.getExternalId(), isEmptyOrNullString());

            // This should not be seen locally yet.
            User retrievedUser = null;
            try
            {
                retrievedUser = remoteDirectory.findUserByName("USER99");
                fail("User should not be seen locally yet");
            }
            catch (UserNotFoundException e)
            {
                // OK this is what we expect
            }

            // Should be seen after sync
            synchroniseCache(remoteDirectory);
            retrievedUser = remoteDirectory.findUserByName("USER99");
            assertUsersEqual(newUser, retrievedUser);

            // Should be picked up from the server
            assertThat(retrievedUser.getExternalId(), not(isEmptyOrNullString()));
        }
        finally
        {
            ldapDirectory.removeUser("USER99");
        }

    }

    /**
     * Test removing a user.
     * This test directly updates the ldap directory and checks the correct results are seen after the sync is done, but not before.
     * @throws Exception
     */
    @Test(expected = UserNotFoundException.class)
    public void testRemoveUserViaLdap() throws Exception
    {
        // Remove from LDAP directory
        ldapDirectory.removeUser(LDAP_USER_NAME);

        // This should still be seen locally until sync.
        User retrievedUser = remoteDirectory.findUserByName(LDAP_USER_NAME);
        assertNotNull(retrievedUser);
        assertUsersEqual(ldapUser, retrievedUser);

        // Should NOT be seen after sync
        synchroniseCache(remoteDirectory);
        remoteDirectory.findUserByName(LDAP_USER_NAME); // user should have been removed
    }


    /**
     * Test Update a user.
     * This test directly updates the ldap directory and checks the correct results are seen after the sync is done, but not before.
     */
    @Test
    public void testUpdateUserViaLdap() throws Exception
    {
        // Remove from LDAP directory
        User testUser = ldapDirectory.findUserByName(LDAP_USER_NAME);

        UserTemplate template = new UserTemplate(testUser);
        template.setDisplayName("Updated display name");
        template.setEmailAddress("updated@example.org");
        User updatedUser = ldapDirectory.updateUser(template);

        // The old values be seen locally until sync.
        User retrievedUser = remoteDirectory.findUserByName(LDAP_USER_NAME);
        assertNotNull(retrievedUser);
        assertEquals(ldapUser.getDisplayName(), retrievedUser.getDisplayName());
        assertEquals(ldapUser.getEmailAddress(), retrievedUser.getEmailAddress());

        // Should see changes  after sync
        synchroniseCache(remoteDirectory);
        retrievedUser = remoteDirectory.findUserByName(LDAP_USER_NAME);
        assertNotNull(retrievedUser);
        assertEquals("Updated display name", retrievedUser.getDisplayName());
        assertEquals("updated@example.org", retrievedUser.getEmailAddress());

        //this should not change
        assertThat(retrievedUser.getExternalId(), PatternMatcher.matchesPattern(EXTERNAL_ID_PATTERN));
        assertEquals(testUser.getExternalId(), retrievedUser.getExternalId());
    }

    /**
     * Test adding a group.
     * This test directly updates the ldap directory and checks the correct results are seen after the sync is done, but not before.
     */
    @Test
    public void testAddGroupViaLdap() throws Exception
    {
        // Add to LDAP directory
        ldapGroup = ldapDirectory.addGroup(new GroupTemplate(LDAP_GROUP3_NAME, directory.getId()));
        try
        {
            // This should not be seen locally yet.
            Group retrievedGroup = null;
            try
            {
                retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP3_NAME);
                fail("Group should not be seen locally yet");
            }
            catch (GroupNotFoundException e)
            {
                // OK this is what we expect
            }

            // Should be seen after sync
            synchroniseCache(remoteDirectory);
            retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP3_NAME);
            assertGroupsEqual(ldapGroup, retrievedGroup);
        }
        finally
        {
            ldapDirectory.removeGroup(LDAP_GROUP3_NAME);
        }

    }

    /**
     * Test removing a group.
     * This test directly updates the ldap directory and checks the correct results are seen after the sync is done, but not before.
     */
    @Test
    public void testRemoveGroupViaLdap() throws Exception
    {
        // Remove from LDAP directory
        ldapDirectory.removeGroup(LDAP_GROUP_NAME);

        // This should still be seen locally until sync.
        Group retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP_NAME);
        assertNotNull(retrievedGroup);
        assertGroupsEqual(ldapGroup, retrievedGroup);

        // Should NOT be seen after sync
        synchroniseCache(remoteDirectory);
        try
        {
            retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP_NAME);
            fail("Group should have been removed");
        }
        catch (GroupNotFoundException e)
        {
            // OK this is what we expect
        }
    }

    /**
     * Test Update a group.
     * This test directly updates the ldap directory and checks the correct results are seen after the sync is done, but not before.
     */
    @Test
    public void testUpdateGroupViaLdap() throws Exception
    {
        // Remove from LDAP directory
        Group testGroup = ldapDirectory.findGroupByName(LDAP_GROUP_NAME);

        GroupTemplate template = new GroupTemplate(testGroup);
        template.setDescription("Updated description");
        Group updatedGroup = ldapDirectory.updateGroup(template);

        // The old values be seen locally until sync.
        Group retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP_NAME);
        assertNotNull(retrievedGroup);
        assertEquals(ldapGroup.getDescription(), retrievedGroup.getDescription());

        // Should see changes  after sync
        synchroniseCache(remoteDirectory);
        retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP_NAME);
        assertNotNull(retrievedGroup);
        assertEquals("Updated description", retrievedGroup.getDescription());
    }


    /**
     * Test adding a membership.
     * This test directly updates the ldap directory and checks the correct results are seen after the sync is done, but not before.
     */
    @Test
    public void testAddMembershipViaLdap() throws Exception
    {
        try
        {
            // Add to LDAP directory
            ldapGroup = ldapDirectory.addGroup(new GroupTemplate(LDAP_GROUP3_NAME, directory.getId()));
            synchroniseCache(remoteDirectory);

            ldapDirectory.addUserToGroup(LDAP_USER2_NAME, LDAP_GROUP3_NAME);
            ldapDirectory.addUserToGroup(LDAP_USER3_NAME, LDAP_GROUP3_NAME);
            ldapDirectory.addGroupToGroup(LDAP_GROUP_NAME, LDAP_GROUP3_NAME);
            // This should not be seen locally yet.
            MembershipQuery<String> membershipQuery =
                    QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP3_NAME)
                            .returningAtMost(EntityQuery.ALL_RESULTS);
            List<String> members = remoteDirectory.searchGroupRelationships(membershipQuery);
            assertEquals(0, members.size());

            MembershipQuery<String> groupMembershipQuery =
                    QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP3_NAME)
                            .returningAtMost(EntityQuery.ALL_RESULTS);
            List<String> groupMembers = remoteDirectory.searchGroupRelationships(groupMembershipQuery);
            assertEquals(0, groupMembers.size());

            // Should be seen after sync
            synchroniseCache(remoteDirectory);
            membershipQuery =
                    QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP3_NAME)
                            .returningAtMost(EntityQuery.ALL_RESULTS);
            members = remoteDirectory.searchGroupRelationships(membershipQuery);
            assertEquals(2, members.size());
            assertTrue(members.contains(LDAP_USER2_NAME));
            assertTrue(members.contains(LDAP_USER3_NAME));

            groupMembershipQuery =
                    QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP3_NAME)
                            .returningAtMost(EntityQuery.ALL_RESULTS);
            groupMembers = remoteDirectory.searchGroupRelationships(groupMembershipQuery);
            assertEquals(1, groupMembers.size());
            assertTrue(groupMembers.contains(LDAP_GROUP_NAME));
        }
        finally
        {
            ldapDirectory.removeUserFromGroup(LDAP_USER2_NAME, LDAP_GROUP3_NAME);
            ldapDirectory.removeUserFromGroup(LDAP_USER3_NAME, LDAP_GROUP3_NAME);
            ldapDirectory.removeGroupFromGroup(LDAP_GROUP_NAME, LDAP_GROUP3_NAME);
            ldapDirectory.removeGroup(LDAP_GROUP3_NAME);
        }

    }

    /**
     * Test removing a membership.
     * This test directly updates the ldap directory and checks the correct results are seen after the sync is done, but not before.
     */
    @Test
    public void testRemoveMembershipViaLdap() throws Exception
    {
        // Remove from LDAP directory
        ldapDirectory.removeUserFromGroup(LDAP_USER2_NAME, LDAP_GROUP2_NAME);
        ldapDirectory.removeGroupFromGroup(LDAP_GROUP_NAME, LDAP_GROUP2_NAME);

        // This should still be seen locally until sync.
        MembershipQuery<String> membershipQuery =
                QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME)
                        .returningAtMost(EntityQuery.ALL_RESULTS);
        List<String> members = remoteDirectory.searchGroupRelationships(membershipQuery);
        assertEquals(2, members.size());
        assertTrue(members.contains(LDAP_USER2_NAME));
        assertTrue(members.contains(LDAP_USER3_NAME));

        MembershipQuery<String> groupMembershipQuery =
                QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME)
                        .returningAtMost(EntityQuery.ALL_RESULTS);
        List<String> groupMembers = remoteDirectory.searchGroupRelationships(groupMembershipQuery);
        assertEquals(1, groupMembers.size());
        assertTrue(groupMembers.contains(LDAP_GROUP_NAME));

        // Should NOT be seen after sync
        synchroniseCache(remoteDirectory);

        membershipQuery =
                QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME)
                        .returningAtMost(EntityQuery.ALL_RESULTS);
        members = remoteDirectory.searchGroupRelationships(membershipQuery);
        assertEquals(1, members.size());
        assertTrue(members.contains(LDAP_USER3_NAME));

        groupMembershipQuery =
                QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME)
                        .returningAtMost(EntityQuery.ALL_RESULTS);
        groupMembers = remoteDirectory.searchGroupRelationships(groupMembershipQuery);
        assertEquals(0, groupMembers.size());
    }

    /**
     * Test adding a user.
     * This test performs updates via the dbCachingDirectory and checks the correct results are seen both before and after the sync is done.
     * @throws Exception
     */
    @Test
    public void testAddUserViaCachingDirectory() throws Exception
    {
        User newUser = new ImmutableUser(directory.getId(), "USER99", "Alice", "Jones", "Alice Jones", "ajones@example.com");

        // Add to remote directory
        ldapUser = remoteDirectory.addUser(new UserTemplate(newUser), new PasswordCredential("password"));
        session.flush();
        try
        {
            // This should seen immediately.
            User retrievedUser = remoteDirectory.findUserByName("USER99");
            assertUsersEqual(newUser, retrievedUser);

            // Should be seen after sync
            synchroniseCache(remoteDirectory);
            retrievedUser = remoteDirectory.findUserByName("USER99");
            assertUsersEqual(newUser, retrievedUser);
            assertThat(retrievedUser.getExternalId(), not(isEmptyOrNullString()));
        }
        finally
        {
            remoteDirectory.removeUser("USER99");
        }

    }

    /**
     * Test removing a user.
     * This test performs updates via the dbCachingDirectory and checks the correct results are seen both before and after the sync is done.
     * @throws Exception
     */
    @Test
    public void testRemoveUserViaCachingDirectory() throws Exception
    {
        // Remove from remote directory
        remoteDirectory.removeUser(LDAP_USER_NAME);
        session.flush();

        // This should be gone immediately.
        try
        {
            User retrievedUser = remoteDirectory.findUserByName(LDAP_USER_NAME);
            fail("User should have been removed");
        }
        catch (UserNotFoundException e)
        {
            // OK this is what we expect
        }

        // Should NOT be seen after sync
        synchroniseCache(remoteDirectory);
        try
        {
            User retrievedUser = remoteDirectory.findUserByName(LDAP_USER_NAME);
            fail("User should have been removed");
        }
        catch (UserNotFoundException e)
        {
            // OK this is what we expect
        }
    }


    /**
     * Test Update a user.
     * This test performs updates via the dbCachingDirectory and checks the correct results are seen both before and after the sync is done.
     */
    @Test
    public void testUpdateUserViaCachingDirectory() throws Exception
    {
        // Update remote directory
        User testUser = remoteDirectory.findUserByName(LDAP_USER_NAME);
        assertThat(testUser.getExternalId(), PatternMatcher.matchesPattern(EXTERNAL_ID_PATTERN));

        UserTemplate template = new UserTemplate(testUser);
        template.setDisplayName("Updated display name");
        template.setEmailAddress("updated@example.org");
        User updatedUser = remoteDirectory.updateUser(template);

        // This should seen immediately.
        User retrievedUser = remoteDirectory.findUserByName(LDAP_USER_NAME);
        assertNotNull(retrievedUser);
        assertEquals("Updated display name", retrievedUser.getDisplayName());
        assertEquals("updated@example.org", retrievedUser.getEmailAddress());

        // Should see changes  after sync
        synchroniseCache(remoteDirectory);
        retrievedUser = remoteDirectory.findUserByName(LDAP_USER_NAME);
        assertNotNull(retrievedUser);
        assertEquals("Updated display name", retrievedUser.getDisplayName());
        assertEquals("updated@example.org", retrievedUser.getEmailAddress());

        // This should not change (and should be set)
        assertEquals(testUser.getExternalId(), retrievedUser.getExternalId());
    }

    /**
     * Test adding a group.
     */
    @Test
    public void testAddGroupViaCachingDirectory() throws Exception
    {
        // Add to remote directory
        ldapGroup = remoteDirectory.addGroup(new GroupTemplate(LDAP_GROUP3_NAME, directory.getId()));
        session.flush();

        try
        {
            // This should seen immediately.
            Group retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP3_NAME);
            assertGroupsEqual(ldapGroup, retrievedGroup);

            // Should be seen after sync
            synchroniseCache(remoteDirectory);
            retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP3_NAME);
            assertGroupsEqual(ldapGroup, retrievedGroup);
        }
        finally
        {
            remoteDirectory.removeGroup(LDAP_GROUP3_NAME);
        }

    }

    /**
     * Test removing a group.
     * This test performs updates via the dbCachingDirectory and checks the correct results are seen both before and after the sync is done.
     */
    @Test
    public void testRemoveGroupViaCachingDirectory() throws Exception
    {
        // Remove from remote directory
        remoteDirectory.removeGroup(LDAP_GROUP_NAME);
        session.flush();

        // This should be gone immediately.
        try
        {
            Group retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP_NAME);
            fail("Group should have been removed");
        }
        catch (GroupNotFoundException e)
        {
            // OK this is what we expect
        }

        // Should NOT be seen after sync
        synchroniseCache(remoteDirectory);
        try
        {
            Group retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP_NAME);
            fail("Group should have been removed");
        }
        catch (GroupNotFoundException e)
        {
            // OK this is what we expect
        }
    }

    /**
     * Test Update a group.
     * This test performs updates via the dbCachingDirectory and checks the correct results are seen both before and after the sync is done.
     */
    @Test
    public void testUpdateGroupViaCachingDirectory() throws Exception
    {
        // Remove from LDAP directory
        Group testGroup = remoteDirectory.findGroupByName(LDAP_GROUP_NAME);

        GroupTemplate template = new GroupTemplate(testGroup);
        template.setDescription("Updated description");
        Group updatedGroup = remoteDirectory.updateGroup(template);

        // This should seen immediately.
        Group retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP_NAME);
        assertNotNull(retrievedGroup);
        assertEquals("Updated description", retrievedGroup.getDescription());

        // Should see changes  after sync
        synchroniseCache(remoteDirectory);
        retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP_NAME);
        assertNotNull(retrievedGroup);
        assertEquals("Updated description", retrievedGroup.getDescription());
    }


    /**
     * Test adding a membership.
     * This test performs updates via the dbCachingDirectory and checks the correct results are seen both before and after the sync is done.
     */
    @Test
    public void testAddMembershipViaCachingDirectory() throws Exception
    {
        try
        {
            // Add to remote directory
            ldapGroup = remoteDirectory.addGroup(new GroupTemplate(LDAP_GROUP3_NAME, directory.getId()));
            synchroniseCache(remoteDirectory);

            remoteDirectory.addUserToGroup(LDAP_USER2_NAME, LDAP_GROUP3_NAME);
            remoteDirectory.addUserToGroup(LDAP_USER3_NAME, LDAP_GROUP3_NAME);
            remoteDirectory.addGroupToGroup(LDAP_GROUP_NAME, LDAP_GROUP3_NAME);
            session.flush();

            // This should seen immediately.
            MembershipQuery<String> membershipQuery =
                    QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP3_NAME)
                            .returningAtMost(EntityQuery.ALL_RESULTS);
            List<String> members = remoteDirectory.searchGroupRelationships(membershipQuery);
            assertEquals(2, members.size());
            assertTrue(members.contains(LDAP_USER2_NAME));
            assertTrue(members.contains(LDAP_USER3_NAME));

            MembershipQuery<String> groupMembershipQuery =
                    QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP3_NAME)
                            .returningAtMost(EntityQuery.ALL_RESULTS);
            List<String> groupMembers = remoteDirectory.searchGroupRelationships(groupMembershipQuery);
            assertEquals(1, groupMembers.size());
            assertTrue(groupMembers.contains(LDAP_GROUP_NAME));

            // Should be seen after sync
            synchroniseCache(remoteDirectory);
            membershipQuery =
                    QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP3_NAME)
                            .returningAtMost(EntityQuery.ALL_RESULTS);
            members = remoteDirectory.searchGroupRelationships(membershipQuery);
            assertEquals(2, members.size());
            assertTrue(members.contains(LDAP_USER2_NAME));
            assertTrue(members.contains(LDAP_USER3_NAME));

            groupMembershipQuery =
                    QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP3_NAME)
                            .returningAtMost(EntityQuery.ALL_RESULTS);
            groupMembers = remoteDirectory.searchGroupRelationships(groupMembershipQuery);
            assertEquals(1, groupMembers.size());
            assertTrue(groupMembers.contains(LDAP_GROUP_NAME));
        }
        finally
        {
            remoteDirectory.removeUserFromGroup(LDAP_USER2_NAME, LDAP_GROUP3_NAME);
            remoteDirectory.removeUserFromGroup(LDAP_USER3_NAME, LDAP_GROUP3_NAME);
            remoteDirectory.removeGroupFromGroup(LDAP_GROUP_NAME, LDAP_GROUP3_NAME);
            remoteDirectory.removeGroup(LDAP_GROUP3_NAME);
        }

    }

    /**
     * Test removing a membership.
     * This test performs updates via the dbCachingDirectory and checks the correct results are seen both before and after the sync is done.
     */
    @Test
    public void testRemoveMembershipViaCachingDirectory() throws Exception
    {
        // Remove from LDAP directory
        remoteDirectory.removeUserFromGroup(LDAP_USER2_NAME, LDAP_GROUP2_NAME);
        remoteDirectory.removeGroupFromGroup(LDAP_GROUP_NAME, LDAP_GROUP2_NAME);
        session.flush();

        // This should seen immediately.
        MembershipQuery<String> membershipQuery =
                QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME)
                        .returningAtMost(EntityQuery.ALL_RESULTS);
        List<String> members = remoteDirectory.searchGroupRelationships(membershipQuery);
        assertEquals(1, members.size());
        assertTrue(members.contains(LDAP_USER3_NAME));

        MembershipQuery<String> groupMembershipQuery =
                QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME)
                        .returningAtMost(EntityQuery.ALL_RESULTS);
        List<String> groupMembers = remoteDirectory.searchGroupRelationships(groupMembershipQuery);
        assertEquals(0, groupMembers.size());

        // Should NOT be seen after sync
        synchroniseCache(remoteDirectory);

        membershipQuery =
                QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME)
                        .returningAtMost(EntityQuery.ALL_RESULTS);
        members = remoteDirectory.searchGroupRelationships(membershipQuery);
        assertEquals(1, members.size());
        assertTrue(members.contains(LDAP_USER3_NAME));

        groupMembershipQuery =
                QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME)
                        .returningAtMost(EntityQuery.ALL_RESULTS);
        groupMembers = remoteDirectory.searchGroupRelationships(groupMembershipQuery);
        assertEquals(0, groupMembers.size());
    }

    public void setDataSource(DataSource dataSource)
    {
        this.dataSource = dataSource;
    }

    public void setLdapPropertiesHelperImpl(LDAPPropertiesHelperImpl ldapPropertiesHelperImpl)
    {
        this.ldapPropertiesHelperImpl = ldapPropertiesHelperImpl;
    }

    public void setPasswordEncoderFactory(PasswordEncoderFactory passwordEncoderFactory)
    {
        this.passwordEncoderFactory = passwordEncoderFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }

    public void setDirectoryManager(MockDirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }

    public void setDirectoryInstanceLoader(CacheableDirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }

    public void setLdapDirectoryInstanceLoader(LDAPDirectoryInstanceLoader ldapDirectoryInstanceLoader)
    {
        this.ldapDirectoryInstanceLoader = ldapDirectoryInstanceLoader;
    }

    public void setInternalDirectoryInstanceLoader(InternalDirectoryInstanceLoader internalDirectoryInstanceLoader)
    {
        this.internalDirectoryInstanceLoader = internalDirectoryInstanceLoader;
    }
}
