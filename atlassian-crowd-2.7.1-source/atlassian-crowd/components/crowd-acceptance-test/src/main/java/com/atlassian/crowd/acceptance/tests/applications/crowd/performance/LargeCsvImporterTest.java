package com.atlassian.crowd.acceptance.tests.applications.crowd.performance;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import net.sourceforge.jwebunit.html.Table;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;

/**
 * Test CSV import of 1000 users, 100 groups and 5000 members.
 */
public class LargeCsvImporterTest extends CrowdAcceptanceTestCase
{
    private String userFileLocation = null;
    private String groupmembershipFileLocation = null;

    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        _loginAdminUser();
        restoreBaseSetup();

        gotoImporters();

        clickButton("importcsv");

        userFileLocation = getCsvFileLocation("users.csv");
        groupmembershipFileLocation = getCsvFileLocation("groupmembers.csv");
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testCsvImportLargeDatasetOfUsersAndGroupMappings() throws IOException
    {
        intendToModifyData();

        log("Running testCsvImportLargeDatasetOfUsersAndGroupMappings");

        userFileLocation = getCsvFileLocation("1000users.csv");
        groupmembershipFileLocation = getCsvFileLocation("100groups5000members.csv");

        setTextField("delimiter", ",");
        setTextField("users", userFileLocation);
        setTextField("groupMemberships", groupmembershipFileLocation);

        submit();     // Successful

        assertKeyPresent("dataimport.csv.configuration.text");

        selectOptionByValue("user.0", "user.username");
        selectOptionByValue("user.1", "user.firstname");
        selectOptionByValue("user.2", "user.lastname");
        selectOptionByValue("user.3", "user.emailaddress");
        selectOptionByValue("user.4", "user.password");

        selectOptionByValue("group.0", "group.username");
        selectOptionByValue("group.1", "group.name");

        submit();   // Successful

        assertKeyPresent("dataimport.csv.configuration.confirmation.text");

        // Assert the directory we have chosen is present
        assertKeyPresent("dataimport.csv.configuration.passwordencrypted.label", getMessage("yes.label"));
        assertKeyPresent("dataimport.csv.configuration.userfile.label", userFileLocation);
        assertKeyPresent("dataimport.csv.configuration.groupmembershipfile.label", groupmembershipFileLocation);

        String[][] userMappingsTableData = {new String[]{getMessage("dataimport.csv.configuration.headerrow"), getMessage("dataimport.csv.configuration.mapping")}, new String[]{"User Name", getMessage("user.username")}, new String[]{"First Name", getMessage("user.firstname")}, new String[]{"Last Name", getMessage("user.lastname")}, new String[]{"Email Address", getMessage("user.emailaddress")}, new String[]{"Password", getMessage("user.password")}};

        assertTableEquals("usermappings", new Table(userMappingsTableData));

        String[][] groupMappingsTableData = {new String[]{getMessage("dataimport.csv.configuration.headerrow"), getMessage("dataimport.csv.configuration.mapping")}, new String[]{"User", getMessage("group.username")}, new String[]{"Group", getMessage("group.name")}};

        assertTableEquals("groupmappings", new Table(groupMappingsTableData));

        submit(); // We are all good, run the import

        assertKeyPresent("dataimport.csv.result.text");

        assertTextInElement("users-imported", "1000");
        assertTextInElement("groups-imported", "100");
        assertTextInElement("memberships-imported", "5000");

        log("Finished importing 1000 users, 100 groups and 5000 memberships");

        // perform verification of import

        // verify first batch of users
        gotoBrowsePrincipalsLotsOfResultsPerPage();
        assertUserInTable("admin", "Super User", "admin@example.com");
        DecimalFormat df = new DecimalFormat("000");
        for (int i = 0; i < 99; i++)
        {
            String userNumber = df.format(i);
            assertUserInTable("user" + userNumber, "First" + userNumber + " Last" + userNumber, "user" + userNumber + "@example.com");
        }

        // verify 1000th import
        assertUserInTable("user999", "First999 Last999", "user999@example.com");

        // verify page 1 of groups
        gotoBrowseGroups();
        assertTextInTable("group-details", new String[]{"crowd-administrators", "true"});
        for (int i = 0; i < 99; i++)
        {
            String groupNumber = df.format(i);
            assertTextInTable("group-details", new String[]{"group" + groupNumber, "true"});
        }

        // verify page 2 of groups
        gotoBrowseGroups(100);
        assertTextInTable("group-details", new String[]{"group099", "true"});

        // verify a single group's memberships
        gotoViewGroup("group004", "Test Internal Directory ");
        clickLink("view-group-users");
        for (int i = 0; i < 50; i++)
        {
            String userNumber = df.format(i);
            assertTextInTable("view-group-users", new String[]{"user" + userNumber, "user" + userNumber + "@example.com", "true"});
        }
    }

    public void gotoBrowsePrincipalsLotsOfResultsPerPage()
    {
        gotoPage("/console/secure/user/browse.action?resultsPerPage=4096");
        assertKeyPresent("browser.principal.title");
    }

    private String getCsvFileLocation(String fileName) throws IOException
    {
        // Locate the where the xmlfile is
        File fileLocation = AcceptanceTestHelper.getResource("com/atlassian/crowd/acceptance/tests/applications/crowd/" + fileName);

        if (!fileLocation.exists())
        {
            URL resource = ClassLoaderUtils.getResource("com/atlassian/crowd/acceptance/tests/applications/crowd/" + fileName, this.getClass());
            fileLocation = new File(resource.getFile());
        }

        if (!fileLocation.exists())
        {
            throw new IllegalArgumentException("File: " + fileName + " does not exist in the resources directory, under the 'com/atlassian/crowd/acceptance/tests/console/' directory");
        }

        return fileLocation.getCanonicalPath();
    }
}
