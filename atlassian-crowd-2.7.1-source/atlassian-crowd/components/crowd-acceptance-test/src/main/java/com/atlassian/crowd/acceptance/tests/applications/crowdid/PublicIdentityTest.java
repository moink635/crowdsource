package com.atlassian.crowd.acceptance.tests.applications.crowdid;

import java.util.List;

import com.google.inject.internal.Function;
import com.google.inject.internal.Iterables;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.openid4java.discovery.Discovery;
import org.openid4java.discovery.DiscoveryException;
import org.openid4java.discovery.DiscoveryInformation;

import net.sourceforge.jwebunit.api.IElement;

import static org.hamcrest.Matchers.endsWith;
import static org.junit.Assert.assertThat;

public class PublicIdentityTest extends CrowdIDAcceptanceTestCase
{
    private String getIdentity()
    {
        String identity = getElementTextByXPath("//div[@class='identity-bar']");
        assertNotNull(identity);
        return identity;
    }

    // view public indentity page
    public void testPublicIdentityPage()
    {
        log("Running testPublicIdentityPage");

        String username = "schmuck";
        gotoPage("/users/"+username);

        assertKeyPresent("identity.title");
        assertTextPresent(username);

        assertEquals(getBaseUrl() + "/users/schmuck", getIdentity());
    }

    void assertDiscoveryLeadsToCorrectIdentifierShown(String relativePath, String expectedRelativePathInIdentifier)
    {
        /* Correct identifier shown on the page */
        gotoPage(relativePath);
        assertEquals(getBaseUrl() + expectedRelativePathInIdentifier, getIdentity());
    }

    void assertDiscoveryLeadsToExpectedIdentifier(String relativePath, String expectedRelativePathInIdentifier)
        throws DiscoveryException
    {
        assertDiscoveryLeadsToCorrectIdentifierShown(relativePath, expectedRelativePathInIdentifier);

        /* OpenID discovery */
        Discovery discovery = new Discovery();

        String userIdentifier = getBaseUrl() + relativePath;

        List<DiscoveryInformation> discoveries = discovery.discover(userIdentifier);

        String expectedIdentifier = getBaseUrl() + expectedRelativePathInIdentifier;

        assertThat(discoveries, CoreMatchers.<DiscoveryInformation>hasItem(openIdWithVersionAndDelegateId(
                DiscoveryInformation.OPENID11,
                expectedIdentifier)));

        assertThat(discoveries, CoreMatchers.<DiscoveryInformation>hasItem(openIdWithVersionAndDelegateId(
                DiscoveryInformation.OPENID2,
                expectedIdentifier)));
    }

    public void testPublicIdentityEscapesAtSigns() throws Exception
    {
        assertDiscoveryLeadsToExpectedIdentifier(
                "/users/user@host",
                "/users/user@host");

        assertDiscoveryLeadsToExpectedIdentifier(
                "/users/user%40host",
                "/users/user@host");
    }

    public void testPublicIdentityAcceptsPlusSignsLiterally() throws Exception
    {
        assertDiscoveryLeadsToExpectedIdentifier(
                "/users/user+host",
            "/users/user+host");
    }

    public void testNonCanonicalUrlsShowToCanonicalForm() throws Exception
    {
        assertDiscoveryLeadsToExpectedIdentifier(
                "/users/user%40host",
                "/users/user@host");
    }

    public void testNonCanonicalUrlsWithUnnecessaryQueriesShowCanonicalFormWithout() throws Exception
    {
        assertDiscoveryLeadsToExpectedIdentifier(
                "/users/name?",
                "/users/name");
    }

    // Blocked by Tomcat in the default configuration
//    public void testUrlWithPercentSlashesIsAccepted() throws IOException
//    {
//        gotoPage("/users/not%2Fthe%2Fadmin");
//        assertEquals(getBaseUrl() + "/users/not%2Fthe%2Fadmin", getIdentity());
//    }

    public void testUrlWithUnencodedSlashesHasCanonicalFormEncoded() throws Exception
    {
        assertDiscoveryLeadsToExpectedIdentifier(
                "/users/not/the/admin",
                "/users/not%2Fthe%2Fadmin");
    }

    public void testUrlWithHtmlCharactersDiscoveredAsCorrectIdentifier() throws Exception
    {
        assertDiscoveryLeadsToCorrectIdentifierShown(
                "/users/<i>user",
                "/users/%3Ci%3Euser");

        assertDiscoveryLeadsToExpectedIdentifier(
                "/users/&amp;user",
                "/users/&amp%3Buser");

        assertDiscoveryLeadsToExpectedIdentifier(
                "/users/&amp",
                "/users/&amp");

        assertDiscoveryLeadsToExpectedIdentifier(
                "/users/'user",
                "/users/'user");
    }

    public void testUrlWithNoUserSpecifiedIsStillDiscoverable() throws Exception
    {
        assertDiscoveryLeadsToExpectedIdentifier(
                "/users/",
                "/users/");
    }

    // Blocked by Tomcat in the default configuration
//    public void testLeadingSlashesArePermittedInUsernames() throws Exception
//    {
//        assertDiscoveryLeadsToExpectedIdentifier(
//                "/users//user",
//                "/users/%2Fuser");
//
//        assertDiscoveryLeadsToExpectedIdentifier(
//                "/users/%2Fuser",
//                "/users/%2Fuser");
//    }

    public static Matcher<DiscoveryInformation> openIdWithVersionAndDelegateId(final String version, final String delegateId)
    {
        return new TypeSafeMatcher<DiscoveryInformation>(DiscoveryInformation.class)
        {
            @Override
            public void describeTo(Description desc)
            {
                desc.appendText("A version '" + version + "' identifier with delegated ID '" + delegateId + "'");
            }

            @Override
            protected boolean matchesSafely(DiscoveryInformation info)
            {
                return info.getVersion().equals(version) && info.getDelegateIdentifier().toString().equals(delegateId);
            }
        };
    }

    public void testEndpointUrlIsShown()
    {
        String opUrl =  getBaseUrl() + "/op";

        gotoPage("/users/username");

        List<IElement> identityInfo = getElementsByXPath("//div[@class='identity-info']");

        Iterable<String> identityInfoText = Iterables.transform(identityInfo, elementToTextContent);

        assertThat(identityInfoText, Matchers.<String>hasItem(endsWith(": " + opUrl)));
    }

    private static final Function<IElement, String> elementToTextContent = new Function<IElement, String>() {
        @Override
        public String apply(IElement from)
        {
            return from.getTextContent();
        }
    };
}
