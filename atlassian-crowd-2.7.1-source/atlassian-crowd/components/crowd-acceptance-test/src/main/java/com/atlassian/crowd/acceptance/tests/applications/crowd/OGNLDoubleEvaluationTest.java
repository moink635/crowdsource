package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class OGNLDoubleEvaluationTest extends CrowdAcceptanceTestCase
{
    /**
     * CWD-3486
     */
    public void testUsingAStringParameter()
    {
        restoreBaseSetup();

        _loginAdminUser();

        String token = findCsrfToken();

        // inject OGNL in the "name" parameter
        gotoPage("/console/secure/user/update.action?directoryID=32769&name=${\"foo\"%2B\"bar\"}&atl_token=" + token);

        // the OGNL expression is not evaluated
        assertTextPresent("${\"foo\"+\"bar\"}");
        assertTextNotPresent("foobar");
    }

    public void testUsingANonStringParameter()
    {
        restoreBaseSetup();

        _loginAdminUser();

        String token = findCsrfToken();

        // inject OGNL in the "directoryID" parameter
        gotoPage("/console/secure/user/update!updateGeneral.action?directoryID=${32769}&name=admin&atl_token="
                 + token);

        assertKeyPresent("menu.viewprincipal.label");
        // WW is unable to parse the parameter a as long, so it never calls the setter. The default value (-1) is used
        assertTextPresent("Directory <-1> does not exist");
    }

    public void testUsingANonSettableParameter()
    {
        gotoPage("/console/setup/selectsetupstep.action?currentStep=${@java.lang.System@exit(0)}");

        // Crowd is double-protected about this. Firstly, once the setup process is completed,
        // the action is not accessible anymore. Secondly, there is no setter for "currentStep".
        assertTextPresent("Your crowd installation is already setup");
    }

    private String findCsrfToken()
    {
        gotoViewPrincipal("admin", "Test Internal Directory");

//        String token = getElementTextByXPath("//input[@id='atl_token']/@value");

        Matcher m = Pattern.compile("name=\\\"atl_token\\\"(\\W)*value=\\\"(.*)\\\"").matcher(getPageSource());
        assertTrue(m.find());
        return m.group(2);
    }
}
