package com.atlassian.crowd.acceptance.tests.directory;

import junit.framework.TestCase;
import org.springframework.ldap.core.ContextExecutor;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.event.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Not included as a part of the integration test suite
 * but allows for easy diagnostics regarding change notification.
 */
public class ChangeNotificationTest extends TestCase
{
    private String connectionURL;
    private String username;
    private String password;
    private String userDN;

    private LdapTemplate ldapTemplate;

    private void setupApacheDS()
    {
        connectionURL = "ldap://localhost:10389";
        username = "uid=admin,ou=system";
        password = "secret";
        userDN = "ou=users,dc=example,dc=com";
    }

    private void setupOpenDS()
    {
        connectionURL = "ldap://crowd-opends:389";
        username = "cn=Directory Manager";
        password = "atlassian";
        userDN = "ou=People,dc=example,dc=com";
    }

    private void setupNovell()
    {
        connectionURL = "ldap://crowd-novell:389";
        username = "cn=Admin,o=null";
        password = "atlassian";
        userDN = "o=Atlassian";
    }

    @Override
    protected void setUp() throws Exception
    {
        //setupApacheDS();
        //setupOpenDS();
        setupNovell();

        LdapContextSource contextSource = new LdapContextSource();

        contextSource.setUrl(connectionURL);
        contextSource.setUserDn(username);
        contextSource.setPassword(password);

        Map env = new HashMap();
        contextSource.setBaseEnvironmentProperties(env);

        // let spring know of our connection attributes
        //contextSource.setBaseEnvironmentProperties(getBaseEnvironmentProperties());

        // create a pool for when doing multiple calls.
        contextSource.setPooled(true);

        // by default, all results are converted to a DirContextAdapter using the
        // dirObjectFactory property of the ContextSource (we need to disable that)
        contextSource.setDirObjectFactory(null);

        try
        {
            // we need to tell the context source to configure up our ldap server
            contextSource.afterPropertiesSet();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail(e.getMessage());
        }

        ldapTemplate = new LdapTemplate(contextSource);                
    }

    public void testPersistentSearch() throws Exception
    {
        final SearchControls searchControls = new SearchControls();
        searchControls.setSearchScope(SearchControls.SUBTREE_SCOPE);

        ContextExecutor contextExecutor = new ContextExecutor()
        {
            public Object executeWithContext(DirContext ctx) throws NamingException
            {
                EventDirContext ectx = (EventDirContext) ctx.lookup("");
                ectx.addNamingListener(userDN, "(objectclass=inetorgperson)", searchControls, new SimpleChangeListener("listener_1"));
                return null;
            }
        };
        ldapTemplate.executeReadOnly(contextExecutor);

        System.out.println("slumber..");

        Thread.sleep(120*1000);

        System.out.println("..party's over");
    }

    private static class SimpleChangeListener implements NamespaceChangeListener, ObjectChangeListener
    {
        private String id;

        public SimpleChangeListener(String id)
        {
            this.id = id;
        }

        public void objectAdded(NamingEvent evt)
        {
            System.out.println("\n\n" + id + ">>> object added event. Object Name: " + evt.getNewBinding().getName());
        }

        public void objectRemoved(NamingEvent evt)
        {
            System.out.println("\n\n" + id + ">>> object removed event. Object Name: " + evt.getOldBinding().getName());
        }

        public void objectRenamed(NamingEvent evt)
        {
            System.out.println("\n\n" + id + ">>> object renamed event. New name: " + evt.getNewBinding().getName()
                    + " Old name: " + evt.getOldBinding().getName());
        }

        public void objectChanged(NamingEvent evt)
        {
            System.out.println("\n\n" + id + ">>> object changed event. Object name: " + evt.getNewBinding().getName());
        }

        public void namingExceptionThrown(NamingExceptionEvent evt)
        {
            System.out.println("\n\n" + id + ">>> Listener received a naming exception");
            evt.getException().printStackTrace();
        }
    }
}
