package com.atlassian.crowd.acceptance.harness;

import com.atlassian.crowd.acceptance.tests.administration.BackupTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * This test suite assumes that Crowd configuration is persisted in a database, and it
 * produces a snapshot of the configuration as property files that are later used by
 * another test suite.
 * @see com.atlassian.crowd.acceptance.harness.ConfigurationFromFileCrowdAcceptanceTestHarness
 */
@RunWith(Suite.class)
@SuiteClasses({BackupTest.class})
public class FreezeConfigurationCrowdAcceptanceTestHarness
{
}
