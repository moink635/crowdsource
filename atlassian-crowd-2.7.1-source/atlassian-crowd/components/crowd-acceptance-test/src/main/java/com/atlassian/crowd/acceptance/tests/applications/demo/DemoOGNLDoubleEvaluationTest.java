package com.atlassian.crowd.acceptance.tests.applications.demo;

public class DemoOGNLDoubleEvaluationTest extends DemoAcceptanceTestCase
{
    public void testOGNLDoubleEvaluationUsingStringParameter()
    {
        restoreBaseSetup();

        _loginAdminUser();

        gotoPage("/secure/group/addgroup!update.action?name=${\"foo\"%2B\"bar\"}");

        // the OGNL expression is not evaluated
        assertTextPresent("${\"foo\"+\"bar\"}");
        assertTextNotPresent("foobar");
    }
}
