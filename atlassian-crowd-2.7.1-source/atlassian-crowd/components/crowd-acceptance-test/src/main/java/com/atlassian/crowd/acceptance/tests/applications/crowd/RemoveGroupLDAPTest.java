package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.crowd.acceptance.tests.directory.BaseTest;
import com.atlassian.crowd.acceptance.utils.DbCachingTestHelper;
import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;

import java.util.Properties;

/**
 * Web acceptance test for the removing of an LDAP Group
 */
public class RemoveGroupLDAPTest extends CrowdAcceptanceTestCase
{
    private static final String DIRECTORY_NAME = "ApacheDS154";

    private static final long MAX_WAIT_TIME_MS = 10000L;

    private static final String GROUP_NAME = "myGroup";

    private static final String SUB_GROUP_NAME = "mySubGroup";
    LDAPLoader loader;

    /**
     * Allows us to add users to our ApacheDS instance.
     */
    class LDAPLoader extends BaseTest
    {
        LDAPLoader()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
        }

        @Override
        protected void configureDirectory(Properties directorySettings)
        {
            super.configureDirectory(directorySettings);
            directory.setAttribute(LDAPPropertiesMapper.LDAP_BASEDN_KEY, directorySettings.getProperty("test.integration.basedn"));
            directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.FALSE.toString());
        }

        protected void loadTestData() throws Exception
        {
            GroupTemplate group = new GroupTemplate(GROUP_NAME, directory.getId(), GroupType.GROUP);     // members: SUB_USER_NAME
            getRemoteDirectory().addGroup(group);

            GroupTemplate subGroup = new GroupTemplate(SUB_GROUP_NAME, directory.getId(), GroupType.GROUP);     // members: SUB_USER_NAME

            getRemoteDirectory().addGroup(subGroup);

            // members: SUB_USER_NAME
            getRemoteDirectory().addGroupToGroup(SUB_GROUP_NAME, GROUP_NAME);
        }

        protected void removeTestData() throws DirectoryInstantiationException
        {
            DirectoryTestHelper.silentlyRemoveGroup(GROUP_NAME, getRemoteDirectory());
            DirectoryTestHelper.silentlyRemoveGroup(SUB_GROUP_NAME, getRemoteDirectory());
        }

        public void accessibleSetUp() throws Exception
        {
            super.setUp();
        }

        public void accessibleTearDown() throws Exception
        {
            super.tearDown();
        }

    }

    public RemoveGroupLDAPTest()
    {
        loader = new LDAPLoader();
    }

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        loader.accessibleSetUp();
        restoreCrowdFromXML("viewgrouptest.xml");
        synchroniseDirectory();
    }

    private void synchroniseDirectory()
    {
        DbCachingTestHelper.synchroniseDirectory(tester, DIRECTORY_NAME, MAX_WAIT_TIME_MS);
    }

    @Override
    public void tearDown() throws Exception
    {
        super.tearDown();
        loader.accessibleTearDown();
    }

    public void testRemoveRemoteGroupWhenLocalGroupsAreDisabled()
    {
        log("Running testRemoveRemoteGroupWhenLocalGroupsAreDisabled");
        intendToModifyData();
        intendToModifyLdapData();
        disableLocalGroups();

        gotoViewGroup(GROUP_NAME, DIRECTORY_NAME);

        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent(GROUP_NAME);

        clickLink("remove-group");
        assertKeyPresent("group.remove.text");
        assertTextPresent(GROUP_NAME);

        submit();

        assertKeyPresent("updatesuccessful.label");
        assertTextNotPresent(GROUP_NAME);
    }

    public void testRemoveRemoteGroupWhenLocalGroupsAreEnabledShouldFail()
    {
        log("Running testRemoveRemoteGroupWhenLocalGroupsAreEnabledShouldFail");
        intendToModifyData();
        enableLocalGroups();
        gotoViewGroup(GROUP_NAME, DIRECTORY_NAME);

        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent(GROUP_NAME);

        clickLink("remove-group");
        assertKeyPresent("group.remove.text");
        assertTextPresent(GROUP_NAME);

        submit();

        assertWarningPresent();
        assertTextPresent("Group <" + GROUP_NAME + "> is read-only and cannot be updated");

        // group is still there
        gotoViewGroup(GROUP_NAME, DIRECTORY_NAME);
        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent(GROUP_NAME);
    }

    private void enableLocalGroups()
    {
        gotoBrowseDirectories();
        clickLinkWithExactText(DIRECTORY_NAME);
        clickLink("connector-connectiondetails");
        checkCheckbox("localGroupsEnabled");
        submit();

        assertWarningAndErrorNotPresent();
        assertCheckboxSelected("localGroupsEnabled");
    }

    private void disableLocalGroups()
    {
        gotoBrowseDirectories();
        clickLinkWithExactText(DIRECTORY_NAME);
        clickLink("connector-connectiondetails");
        uncheckCheckbox("localGroupsEnabled");
        submit();

        assertWarningAndErrorNotPresent();
        assertCheckboxNotSelected("localGroupsEnabled");
    }

    // can't test this membership operation as it relies on dialog js

//    public void testRemoveSubGroup()
//    {
//        log("Running testRemoveSubGroup");
//
//        gotoViewGroup(GROUP_NAME, DIRECTORY_NAME);
//
//        assertKeyPresent("menu.viewgroup.label");
//        assertTextPresent(GROUP_NAME);
//
//        clickLink("view-group-users");
//        assertTextInTable("view-group-groups", SUB_GROUP_NAME);
//
//        clickLink("removegroup-" + SUB_GROUP_NAME + "-");
//        assertKeyPresent("updatesuccessful.label");
//    }
}
