package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.util.List;

import javax.annotation.Nullable;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

/**
 * Tests the functionality of the View Principal page
 */
public class ViewPrincipalTest extends CrowdAcceptanceTestCase
{
    private static final String ATLASSIAN_DIRECTORY_NAME = "Atlassian";

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreCrowdFromXML("viewprincipaltest.xml");
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testPrincipalDetailsShowDirectoryDescription()
    {
        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);

        assertTextPresent("Atlassian — Crowd Internal Directory");
    }

    public void testUpdateMainAttributes()
    {
        intendToModifyData();

        log("Running: testUpdateMainAttributes");

        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);

        setWorkingForm("updateprincipalForm");

        // Change First Name
        setTextField("firstname", "Billy");

        // Change Last name
        setTextField("lastname", "Bob");

        // Change Email
        setTextField("email", "billy@atlassian.com");

        // Set inactive
        uncheckCheckbox("active");

        // Submit changes
        submit();

        // assert that we have success
        assertKeyPresent("updatesuccessful.label");

        assertTextFieldEquals("firstname", "Billy");

        assertTextFieldEquals("lastname", "Bob");

        assertTextFieldEquals("email", "billy@atlassian.com");

        assertCheckboxNotSelected("active");
    }

    public void testRenameUser()
    {
        intendToModifyData();

        log("Running: testRenameUser");

        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);

        setWorkingForm("updateprincipalForm");

        setTextField("newName", "jkoke");

        submit();

        assertKeyPresent("updatesuccessful.label");

        assertTextFieldEquals("newName", "jkoke");
    }

    public void testRenameUserCausesNameClash()
    {
        intendToModifyData();

        log("Running: testRenameUser");

        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);

        setWorkingForm("updateprincipalForm");

        setTextField("newName", "admin"); // this name is already taken

        submit();

        assertKeyPresent("invalid.namealreadyexist");
    }

    public void testRenameUserIsNotSupportedForLDAPDirectories()
    {
        intendToModifyData();

        log("Running: testRenameUser");

        gotoViewPrincipal("jdoe", "Apache DS");

        setWorkingForm("updateprincipalForm");

        setTextField("newName", "new-name");

        submit();

        assertWarningPresent();
        assertTextPresent("User renaming is not supported for LDAP directories");
    }

    public void testChangePasswordDoesNotMatchComplexityRequirements()
    {
        intendToModifyData();

        log("Running: testChangePasswordDoesNotMatchComplexityRequirements");

        updateDirectoryWithPasswordRegex(ATLASSIAN_DIRECTORY_NAME, "[a-z]",
                                         "Passwords <b>must</b> contain at least one lowercase character");

        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);

        setWorkingForm("updateprincipalForm");

        setTextField("password", "ALL-UPPERCASE-PASSWORD");
        setTextField("passwordConfirm", "ALL-UPPERCASE-PASSWORD");
        submit();

        assertKeyNotPresent("updatesuccessful.label");
        assertTextPresent("Passwords <b>must</b> contain at least one lowercase character");   // escaped
    }

    public void testAddNewPrincipalAttribute()
    {
        intendToModifyData();

        log("Running: testAddNewPrincipalAttribute");

        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);

        // Go to Attributes Tab
        clickLink("user-attributes-tab");
        setWorkingForm("attributesForm");
        setTextField("attribute", "company");
        setTextField("value", "Atlassian");

        // submit the addition
        clickButton("add-attribute");

        // assert that we have success
        assertKeyPresent("updatesuccessful.label");

        assertTextInTable("attributesTable", new String[]{"company", "Atlassian" });
    }

    public void testShouldNotAllowToAddNewPrincipalEmptyAttribute()
    {
        log("Running: testShouldNotAllowToAddNewPrincipalEmptyAttribute");

        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);

        // Go to Attributes Tab
        clickLink("user-attributes-tab");
        setWorkingForm("attributesForm");
        setTextField("attribute", "");
        setTextField("value", "Atlassian");

        // submit the addition
        clickButton("add-attribute");

        // assert that the empty attribute has not been added
        assertKeyNotPresent("updatesuccessful.label");
        assertErrorPresentWithKey("principal.attributename.invalid");
        Function<List<String>, String> getAttributeNames = new Function<List<String>, String>()
        {
            @Override
            public String apply(@Nullable List<String> row)
            {
                return row.get(0);
            }
        };
        List<String> attributes = scrapeTable("attributesTable",
                                              ImmutableList.of("Attribute", "Values", "Action"),
                                              getAttributeNames);
        assertThat(attributes, not(hasItem("")));
    }

    public void testUpdatePrincipalAttributes()
    {
        intendToModifyData();

        log("Running: testUpdatePrincipalAttributes");

        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);

        // Go to Attributes Tab
        clickLink("user-attributes-tab");
        setWorkingForm("attributesForm");

        setTextField("phone1", "9999-5555");

        setTextField("country1", "Tibet");

        // submit the updates
        submit();

        // assert that we have success
        assertKeyPresent("updatesuccessful.label");

        // And that the updates worked
        assertTextInTable("attributesTable", new String[]{"9999-5555", "Tibet"});
    }

    public void testUpdatePrincipalWithTrailingWhitespaceFailsWithSpecificError()
    {
        log("Running: testUpdatePrincipalWithTrailingWhitespace");

        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);

        setWorkingForm("updateprincipalForm");

        setTextField("email", " foo@example.test ");

        // submit the updates
        submit();

        assertKeyPresent("principal.email.whitespace");
    }

    public void testRemovePrincipalAttribute()
    {
        intendToModifyData();

        log("Running: testRemovePrincipalAttribute");

        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);

        setWorkingForm("attributesForm");

        // Rmeove the mail attribute
        clickLink("remove-country");

        // assert that we have success
        assertKeyPresent("updatesuccessful.label");

        assertTextNotInTable("attributesTable", new String[]{"country", "Australia"});
    }

    public void testEditApacheDSPrincipal()
    {
        intendToModifyData();
        intendToModifyLdapData();

        gotoViewPrincipal("jdoe", "Apache DS");

        setWorkingForm("updateprincipalForm");

        setTextField("firstname", "John");
        setTextField("lastname", "Small");
        setTextField("email", "john@test.org");

        submit();

        assertTextFieldEquals("firstname", "John");
        assertTextFieldEquals("lastname", "Small");
        assertTextFieldEquals("email", "john@test.org");

    }

    public void testRemoveUserNotPossibleEvenViaUrlHack()
    {
        _loginAdminUser();

        gotoRemovePrincipal("admin", ATLASSIAN_DIRECTORY_NAME);

        assertFormNotPresent("remove-user-form");

        assertKeyPresent("principal.remove.curent.user");
    }


    public void testRemoveUserLinkNotPresentForLoggedInUser()
    {
        _loginAdminUser();

        gotoViewPrincipal("admin", ATLASSIAN_DIRECTORY_NAME);

        assertLinkNotPresent("remove-principal");
    }

    public void testRemoveUser()
    {
        intendToModifyData();

        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);

        clickLink("remove-principal");

        submit();

        assertKeyPresent("updatesuccessful.label");

        assertTextNotPresent("justin@atlassian.com");
    }

    public void testViewUserGroupsTabWithNoModifyGroupPermissions()
    {
        intendToModifyData();

        // Disable 'Modify Group' permission for the directory
        gotoBrowseDirectories();
        clickLinkWithExactText(ATLASSIAN_DIRECTORY_NAME);
        clickLink("internal-permissions");
        setWorkingForm("permissionForm");
        uncheckCheckbox("permissionGroupModify");
        submit();

        // Check adding group memberships is disabled
        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);
        clickLink("user-groups-tab");
        assertKeyPresent("group.modify.disabled");
        assertButtonNotPresent("addGroups");
        assertButtonNotPresent("removeGroups");

        assertTextInTable("groupsTable", new String[]{"crowd-administrators", "", "true"});
    }

    public void testViewUserGroupsTabWithModifyGroupPermissions()
    {
        // Check that it is possible to add group memberships
        gotoViewPrincipal("justin", ATLASSIAN_DIRECTORY_NAME);

        clickLink("user-groups-tab");
        assertKeyNotPresent("group.modify.disabled");
        assertButtonPresent("addGroups");
        assertButtonPresent("removeGroups");

        assertTextInTable("groupsTable", new String[]{"crowd-administrators", "", "true"});
    }

    public void testViewUserWithUserNameWhichContainsNonAsciiCharacters()
    {
        intendToModifyData();
        // Save directory selection
        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "john.tøstinógé");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "Second Directory");

        submit();

        assertTextPresent("john.tøstinógé");
    }

    private void updateDirectoryWithPasswordRegex(String directoryName, String passwordRegex, String passwordComplexityMessage)
    {
        gotoBrowseDirectories();

        clickLinkWithExactText(directoryName);

        clickLink("internal-configuration");

        setTextField("passwordRegex", passwordRegex);
        setTextField("passwordComplexityMessage", passwordComplexityMessage);

        submit();
        assertTextFieldEquals("passwordComplexityMessage", passwordComplexityMessage);
    }
}
