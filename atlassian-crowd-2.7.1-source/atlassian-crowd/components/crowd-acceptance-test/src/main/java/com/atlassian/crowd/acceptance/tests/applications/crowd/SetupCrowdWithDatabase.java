package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;

import org.apache.commons.lang3.StringUtils;
import org.hamcrest.text.IsEmptyString;

import junit.framework.AssertionFailedError;
import net.sourceforge.jwebunit.api.IElement;

import static org.junit.Assert.assertThat;

/**
 * Runs through the setup process for Crowd (based on SetupCrowdTest) with the option to specify a database to use (rather than using hsqldb)
 *
 * Note:
 *  1. Despite being based on SetupCrowdTest, there are some 'flaky' bits (most likely all to do with jwebunit)
 *  2. If you do not specify the database properties, the setup process will fall back to using hsqldb
 */
public class SetupCrowdWithDatabase extends CrowdAcceptanceTestCase
{
    // if the user hasn't specified a database type, the system property will hold this value if run via crowd-test-runner
    private static final String UNSPECIFIED_DATABASE_TYPE = "${database.type}";

    // Database properties that we require user input.
    // for values see *.properties files in database-defaults (eg. postgresql.properties)
    private static final String DATABASE_TYPE = System.getProperty("database.type", "");
    private static final String DATABASE_USERNAME = System.getProperty("database.username");
    private static final String DATABASE_PASSWORD = System.getProperty("database.password");
    private static final String DATABASE_DIALECT = System.getProperty("database.dialect");
    private static final String DATABASE_DRIVER = System.getProperty("database.driver");
    private static final String DATABASE_URL = System.getProperty("database.url");

    // HSQLDB properties
    private static final String HSQLDB_USERNAME = "sa";
    private static final String HSQLDB_DRIVER = "org.hsqldb.jdbcDriver";
    private static final String HSQLDB_DIALECT = "org.hibernate.dialect.HSQLDialect";
    private static final String HSQLDB_URL_PREFIX = "jdbc:hsqldb:";
    private static final String HSQLDB_URL_SUFFIX = "/database/defaultdb";

    public void testSetup() throws Exception
    {
        // license screen
        _testLicenseScreen();
        _testLicenseSuccess();

        // installation type screen
        _testInstallScreen();
        _testInstallNew();

        // database screen
        setScriptingEnabled(true);
        _testDatabaseScreen();
        _testDatabaseSelectJDBC();
        setScriptingEnabled(false);

        // options screen
        _testOptionsScreen();
        _testOptionsSuccess();

        // mail server screen
        _testMailServerScreen();
        _testMailServerSuccess();

        // internal directory screen
        _testInternalDirectoryScreen();
        _testInternalDirectorySuccess();

        // default admin screen
        _testDefaultAdminScreen();
        _testDefaultAdminSuccess();

        // configure integrated apps and finish!
        _testConfigureIntegratedApps();
        _testCompleteSetup();

        // check database was setup properly
        _testDatabaseSystemInfo();
    }


    private void _testLicenseScreen()
    {
        log("Running _testLicenseScreen");
        assertKeyPresent("license.title");
        assertKeyPresent("license.serverid.label");
    }

    private void _testLicenseSuccess()
    {
        log("Running _testLicenseSuccess");

        setTextField("key", UNLIMITED_LICENSE_KEY);
        // Submit on the first page
        submit();

        assertKeyNotPresent("license.title");
    }

    private void _testInstallScreen()
    {
        log("Running _testInstallScreen");

        assertKeyPresent("install.title");
        assertKeyPresent("install.new.label");
        assertKeyPresent("install.upgrade.xml.label");
        assertKeyNotPresent("install.upgrade.db.label"); // 2.0 doesn't allow this
    }

    private void _testInstallNew()
    {
        log("Running _testInstallNew");
        clickRadioOption("installOption", "install.new");
        submit();

        assertKeyNotPresent("install.title");
    }

    private void _testDatabaseScreen()
    {
        log("Running _testDatabaseScreen");

        assertKeyPresent("database.title");
        assertKeyPresent("database.embedded.label");
        assertKeyPresent("database.jdbc.label");
        assertKeyPresent("database.datasource.label");
    }

    private void _testDatabaseSelectJDBC()
    {
        log("Running _testDatabaseSelectJDBC");
        logger.info("Database properties to be used in setup:");
        logger.info("   database.type: <" + DATABASE_TYPE + ">");
        logger.info("   database.username: <" + DATABASE_USERNAME + ">");
        logger.info("   database.password: <" + DATABASE_PASSWORD + ">");
        logger.info("   database.driver: <" + DATABASE_DRIVER + ">");
        logger.info("   database.dialect: <" + DATABASE_DIALECT + ">");
        logger.info("   database.url: <" + DATABASE_URL + ">");

        if (!isDatabaseSpecified())
        {
            logger.warn("Falling back to HSQLDB since system properties to specify which database to use were not found");
            logger.warn("Please set the above system properties to specify a real database!");

            clickRadioOption("databaseOption", "db.embedded");
            submit();
        }
        else
        {
            clickRadioOption("databaseOption", "db.jdbc");
            selectOptionByValue("jdbcDatabaseType", DATABASE_TYPE);

            // enter the db settings
            setTextField("jdbcDriverClassName", "");
            setTextField("jdbcUsername", DATABASE_USERNAME);
            setTextField("jdbcPassword", DATABASE_PASSWORD);
            setTextField("jdbcDriverClassName", DATABASE_DRIVER);
            setTextField("jdbcUrl", DATABASE_URL);
            setTextField("jdbcHibernateDialect", DATABASE_DIALECT);
            checkCheckbox("jdbcOverwriteData");

            submit();
        }

        assertThat(getWarningIfPresent(), IsEmptyString.isEmptyOrNullString());
        assertThat(getErrorBoxIfPresent(), IsEmptyString.isEmptyOrNullString());

        assertKeyNotPresent("database.title");
    }

    private String getWarningIfPresent()
    {
        try
        {
            return getElementTextByXPath("//p[@class='warningBox']").trim();
        }
        catch (AssertionFailedError e)
        {
            return null;
        }
    }

    private String getErrorBoxIfPresent()
    {
        try
        {
            return getElementTextByXPath("//div[@class='errorBox']").trim();
        }
        catch (AssertionFailedError e)
        {
            return null;
        }
    }

    private void _testOptionsScreen()
    {
        log("Running _testOptionsScreen");

        assertKeyPresent("options.title");
        assertKeyPresent("options.title.label");
        assertKeyPresent("session.sessiontime.label");
    }

    private void _testOptionsSuccess()
    {
        log("Running _testOptionsSuccess");
        setTextField("title", "Test Deployment");
        setTextField("sessionTime", "20");
        setTextField("baseURL", HOST_PATH);
        submit();

        assertKeyNotPresent("options.title");
    }

    private void _testMailServerScreen()
    {
        log("Running _testMailServerScreen");
        assertKeyPresent("mailserver.title");
        assertKeyPresent("mailserver.notification.label");
        assertKeyPresent("mailserver.from.label");
        assertKeyPresent("mailserver.prefix.label");
        assertKeyPresent("mailserver.host.label");
        assertKeyPresent("mailserver.username.label");
        assertKeyPresent("mailserver.password.label");
    }

    private void _testMailServerSuccess()
    {
        log("Running _testMailServerSuccess");

        clickRadioOption("jndiMailActive", "false");

        setTextField("notificationEmail", "user@example.com");
        setTextField("host", "localhost");
        setTextField("from", "security@example.com");

        submit();

        assertKeyNotPresent("mailserver.notification.label");
    }

    private void _testInternalDirectoryScreen()
    {
        log("Running _testInternalDirectoryScreen");
        assertKeyPresent("directoryinternal.title");
        assertKeyPresent("directoryinternal.name.label");
        assertKeyPresent("directoryinternal.description.label");
        assertKeyPresent("directoryinternal.passwordmaxchangetime.label");
        assertKeyPresent("directoryinternal.passwordmaxattempts.label");
        assertKeyPresent("directoryinternal.passwordhistorycount.label");
        assertKeyPresent("directoryinternal.passwordregex.label");
        assertKeyPresent("directoryconnector.userencryptionmethod.label");
    }

    private void _testInternalDirectorySuccess()
    {
        log("Running _testInternalDirectorySuccess");
        setTextField("name", "Test Internal Directory");
        setTextField("description", "");
        setTextField("passwordRegex", "");
        setTextField("passwordMaxAttempts", "");
        setTextField("passwordMaxChangeTime", "");
        setTextField("passwordHistoryCount", "");
        selectOptionByValue("userEncryptionMethod", "atlassian-security");
        submit();

        assertKeyNotPresent("directoryinternal.title");
    }

    private void _testDefaultAdminScreen()
    {
        log("Running _testDefaultAdminScreen");
        assertKeyPresent("defaultadmin.title");
        assertKeyPresent("principal.name.label");
        assertKeyPresent("principal.password.label");
        assertKeyPresent("principal.passwordconfirm.label");
        assertKeyPresent("principal.firstname.label");
        assertKeyPresent("principal.lastname.label");
        assertKeyPresent("principal.email.label");
    }

    private void _testDefaultAdminSuccess()
    {
        log("Running _testDefaultAdminSuccess");
        setTextField("email", "admin@example.com");
        setTextField("name", "admin");
        setTextField("password", "admin");
        setTextField("passwordConfirm", "admin");
        setTextField("firstname", "Super");
        setTextField("lastname", "User");

        submit();

        assertKeyNotPresent("defaultadmin.title");
    }

    private void _testConfigureIntegratedApps()
    {
        log("Running _testConfigureIntegratedApps");
        assertKeyPresent("integration.title");

        // disable the demo app, we will test later if it does not show up
        setRadioButton("configureOpenIDServer", Boolean.TRUE.toString());
        setRadioButton("configureDemoApp", Boolean.FALSE.toString());

        submit();
    }

    private void _testCompleteSetup()
    {
        log("Running _testCompleteSetup");
        try
        {
            assertKeyPresent("login.title");
        }
        catch (AssertionFailedError e)
        {
            logger.error("Crowd setup has completed. However for some yet-to-be-found reason, clickElementByXPath for the 'continueButton' has failed. We can ignore this, so proceed with the tests.");
        }
    }

    private static final Set<String> DATABASE_FIELDS = ImmutableSet.of(
            "JDBC URL:",
            "JDBC Driver:",
            "JDBC Username:",
            "Hibernate Dialect:"
    );

    Map<String, String> getDatabaseInformation()
    {
        gotoSystemInfo();

        Map<String, String> information = new HashMap<String, String>();

        for (IElement e : getElementsByXPath("//div[@class='fieldArea required']"))
        {
            String label = e.getElement("label").getTextContent().trim();
            String value = e.getElement("div").getTextContent().trim();

            information.put(label, value);
        }

        return Maps.filterKeys(information, Predicates.in(DATABASE_FIELDS));
    }

    private void _testDatabaseSystemInfo()
    {
        log("Running _testDatabaseSystemInfo");

        _loginAdminUser();

        Map<String, String> expected = new HashMap<String, String>();

        // check we can see the correct JDBC details
        if (isDatabaseSpecified())
        {
            expected.put("JDBC URL:", DATABASE_URL);
            expected.put("JDBC Username:", DATABASE_USERNAME);
            expected.put("JDBC Driver:", DATABASE_DRIVER);
            expected.put("Hibernate Dialect:", DATABASE_DIALECT);
        }
        else
        {
            expected.put("JDBC URL:", HSQLDB_URL_PREFIX + getCrowdHome() + HSQLDB_URL_SUFFIX);
            expected.put("JDBC Username:", HSQLDB_USERNAME);
            expected.put("JDBC Driver:", HSQLDB_DRIVER);
            expected.put("Hibernate Dialect:", HSQLDB_DIALECT);
        }

        assertEquals(expected, getDatabaseInformation());
    }

    /**
     * Checks if a database is specified. If a database is not specified (eg. postgresql or mysql) in system properties
     * it will return false
     *
     * @return true iff a database type is specified in the system properties
     */
    private boolean isDatabaseSpecified()
    {
        return !(DATABASE_TYPE.equals(UNSPECIFIED_DATABASE_TYPE) || StringUtils.isBlank(DATABASE_TYPE));
    }
}
