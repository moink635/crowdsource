package com.atlassian.crowd.acceptance.tests.applications.crowd;

public class AliasTest extends CrowdAcceptanceTestCase
{
    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreCrowdFromXML("aliastest.xml");
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testUpdateAliases()
    {
        intendToModifyData();

        gotoViewPrincipal("admin", "Atlassian Crowd");

        // Go to Applications tab
        clickLink("user-applications-tab");
        setWorkingForm("applicationsForm");
        assertTextInTable("applicationsTable", new String[] { "crowd", "Application aliasing disabled" });
        assertTextFieldEquals("alias-appid-3", "bob");
        assertTextFieldEquals("alias-appid-4", "");

        setTextField("alias-appid-3", "robert");
        setTextField("alias-appid-4", "john");

        submit();

        assertTextInTable("applicationsTable", new String[] { "crowd", "Application aliasing disabled" });
        assertTextFieldEquals("alias-appid-3", "robert");
        assertTextFieldEquals("alias-appid-4", "john");
    }

    public void testClearAliases()
    {
        intendToModifyData();

        gotoViewPrincipal("admin", "Atlassian Crowd");

        // Go to Applications tab
        clickLink("user-applications-tab");
        setWorkingForm("applicationsForm");
        clickLink("remove-alias-appid-3");

        assertTextInTable("applicationsTable", new String[] { "crowd", "Application aliasing disabled" });
        assertTextFieldEquals("alias-appid-3", "");
        assertTextFieldEquals("alias-appid-4", "");
    }

    public void testClearAliasFails()
    {
        intendToModifyData();

        // alias "admin" to "test"
        gotoViewPrincipal("admin", "Atlassian Crowd");
        clickLink("user-applications-tab");
        setWorkingForm("applicationsForm");
        setTextField("alias-appid-3", "test");
        submit();
        assertWarningAndErrorNotPresent();

        // now try remove the alias for "test" to cause a clash
        gotoViewPrincipal("test", "Atlassian Crowd");
        clickLink("user-applications-tab");
        setWorkingForm("applicationsForm");
        clickLink("remove-alias-appid-3");

        assertWarningPresent();
        assertTextPresent("Alias [test] already in use for application [demo] by user [admin]");
    }

    public void testSearchApplicationUsersWithAliases()
    {
        gotoViewApplication("demo");
        clickLink("application-users");
        setWorkingForm("searchusers");
        submit();

        assertUserInTable("admin", "admin man", "shamid@atlassian.com", "bob");
        assertUserInTable("test", "test test", "testuser@atlassian.com", "test12");
    }

    public void testUpdateAliasWithPrimaryUsername()
    {
        gotoViewPrincipal("admin", "Atlassian Crowd");

        // Go to Applications tab
        clickLink("user-applications-tab");
        setWorkingForm("applicationsForm");
        assertTextInTable("applicationsTable", new String[] { "crowd", "Application aliasing disabled" });
        assertTextFieldEquals("alias-appid-3", "bob");
        assertTextFieldEquals("alias-appid-4", "");

        setTextField("alias-appid-3", "admin"); // should not be able to use primary username

        submit();

        assertTextInTable("applicationsTable", new String[] { "crowd", "Application aliasing disabled" });
        // Should not be allowed to use primary username as alias
        assertWarningPresent();
        assertTextFieldEquals("alias-appid-3", "bob"); //invalid update, revert to original alias
        assertTextFieldEquals("alias-appid-4", ""); // untouched
    }

    public void testUpdateAliasWithExistingUsername()
    {
        gotoViewPrincipal("test", "Atlassian Crowd");

        // Go to Applications tab
        clickLink("user-applications-tab");
        setWorkingForm("applicationsForm");
        assertTextInTable("applicationsTable", new String[] { "crowd", "Application aliasing disabled" });
        assertTextFieldEquals("alias-appid-3", "test12");
        assertTextFieldEquals("alias-appid-4", "test34");

        setTextField("alias-appid-4", "admin"); //alias clash - admin does not have alias for app-4

        submit();

        assertTextInTable("applicationsTable", new String[] { "crowd", "Application aliasing disabled" });
        // Cannot use same name as another user
        assertWarningPresent();
        assertTextFieldEquals("alias-appid-3", "test12"); // untouched
        assertTextFieldEquals("alias-appid-4", "test34"); // invalid update, revert to original alias
    }

    public void testUpdateAliasWithPrimaryUsernameAndValidUsername()
    {
        intendToModifyData();

        gotoViewPrincipal("admin", "Atlassian Crowd");

        // Go to Applications tab
        clickLink("user-applications-tab");
        setWorkingForm("applicationsForm");
        assertTextInTable("applicationsTable", new String[] { "crowd", "Application aliasing disabled" });
        assertTextFieldEquals("alias-appid-3", "bob");
        assertTextFieldEquals("alias-appid-4", "");

        setTextField("alias-appid-3", "admin"); // cannot use primary username
        setTextField("alias-appid-4", "john"); // valid update

        submit();

        assertTextInTable("applicationsTable", new String[] { "crowd", "Application aliasing disabled" });
        // Should not be allowed to use primary username as alias
        assertWarningPresent();
        assertTextFieldEquals("alias-appid-3", "bob"); // invalid update, revert to original alias
        assertTextFieldEquals("alias-appid-4", "john"); // updated alias
    }

    public void testUpdateAliasWithExistingUsernameAndValidUsername()
    {
        intendToModifyData();

        gotoViewPrincipal("test", "Atlassian Crowd");

        // Go to Applications tab
        clickLink("user-applications-tab");
        setWorkingForm("applicationsForm");
        assertTextInTable("applicationsTable", new String[] { "crowd", "Application aliasing disabled" });
        assertTextFieldEquals("alias-appid-3", "test12");
        assertTextFieldEquals("alias-appid-4", "test34");

        setTextField("alias-appid-3", "john"); // valid update
        setTextField("alias-appid-4", "admin"); // alias clash - admin does not have alias for app-4

        submit();

        assertTextInTable("applicationsTable", new String[] { "crowd", "Application aliasing disabled" });
        // Cannot use same name as another user
        assertWarningPresent();
        assertTextFieldEquals("alias-appid-3", "john"); // updated alias
        assertTextFieldEquals("alias-appid-4", "test34"); // invalid update, revert to original alias
    }
}
