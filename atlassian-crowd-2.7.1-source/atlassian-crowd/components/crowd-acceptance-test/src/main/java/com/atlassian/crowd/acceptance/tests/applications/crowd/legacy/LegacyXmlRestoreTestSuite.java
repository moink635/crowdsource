package com.atlassian.crowd.acceptance.tests.applications.crowd.legacy;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class LegacyXmlRestoreTestSuite extends TestCase
{
    public static Test suite()
    {
        TestSuite suite = new TestSuite();
        suite.addTestSuite(Crowd10XmlRestoreTest.class);
        suite.addTestSuite(Crowd11XmlRestoreTest.class);
        suite.addTestSuite(Crowd12XmlRestoreTest.class);
        suite.addTestSuite(Crowd13XmlRestoreTest.class);
        suite.addTestSuite(Crowd14XmlRestoreTest.class);
        suite.addTestSuite(Crowd15XmlRestoreTest.class);
        suite.addTestSuite(Crowd16XmlRestoreTest.class);
        return suite;
    }
}
