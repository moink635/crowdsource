package com.atlassian.crowd.acceptance.tests.applications.crowdid.client;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;

public class OpenIDClientOGNLInjectionTest extends CrowdIDClientAcceptanceTestCase
{
    /**
     * CWD-3385
     */
    public void testOGNLCodeInjection()
    {
        // inject OGNL
        gotoPage("/login.action?redirect:${@java.lang.System@exit(0)}");

        // make sure the server is still running
        gotoPage("/login.action");
        assertKeyPresent("login.title");
    }

    /**
     * CWD-3291
     *
     * @throws Exception
     */
    public void testOGNLArbitraryRedirect() throws Exception
    {
        HttpMethod method = new GetMethod(getBaseUrl() + "/login.action?redirect:http://example.test/");
        method.setFollowRedirects(false);
        HttpClient httpClient = new HttpClient();
        httpClient.executeMethod(method);

        assertEquals("HTTP status code should be a 2xx, not a 3xx", 2, method.getStatusCode() / 100);
        assertNull("Should not have a Location header", method.getResponseHeader("Location"));
    }
}
