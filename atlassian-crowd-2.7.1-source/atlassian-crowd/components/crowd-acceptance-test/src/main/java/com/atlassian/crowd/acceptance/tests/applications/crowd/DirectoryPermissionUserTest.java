package com.atlassian.crowd.acceptance.tests.applications.crowd;

public class DirectoryPermissionUserTest extends CrowdAcceptanceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("directorypermissionusertest.xml");
    }

    public void testUnableToAddUser()
    {
        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        setTextField("name", "newtestuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertKeyNotPresent("menu.viewprincipal.label");

        assertTextPresent("Directory does not allow adding of users");
    }
    
    public void testUnableToModifyUser()
    {
        gotoViewPrincipal("testuser", "Test Internal Directory");

        setTextField("email", "newemailaddress@atlassian.com");

        submit();

        assertTextPresent("Directory does not allow user modification");
    }

    public void testUnableToRemoveUser()
    {
        gotoViewPrincipal("testuser", "Test Internal Directory");

        clickLink("remove-principal");

        setWorkingForm("remove-user-form");

        submit();

        assertTextPresent("Directory does not allow user removal");
    }
}
