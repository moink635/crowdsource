package com.atlassian.crowd.acceptance.tests.applications.crowd;

public class CrowdifiedJiraImporterTest extends AppImporterTestBase
{
    @Override
    String getHsqlFileName()
    {
        return "jiradb-4.3-m4";
    }

    public void testImportJiraUsers()
    {
        runImportTest("JIRA", 3, 3, 5);
    }
}
