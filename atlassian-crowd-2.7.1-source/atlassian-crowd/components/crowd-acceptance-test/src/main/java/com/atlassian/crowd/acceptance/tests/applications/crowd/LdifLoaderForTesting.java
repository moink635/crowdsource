package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;

import junit.framework.Test;
import junit.framework.TestResult;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Test utility class for restoring LDIF to the embedded ApacheDS server.
 */
public class LdifLoaderForTesting
{
    private final String baseUrl;

    public LdifLoaderForTesting(String baseUrl)
    {
        this.baseUrl = baseUrl;
        assertTrue(baseUrl.endsWith("/crowd"));
    }

    public void setLdif(InputStream in) throws IOException
    {
        String contentUrl = baseUrl.replaceAll("/crowd$", "/apacheds15/content");

        RequestEntity entity = new InputStreamRequestEntity(in, "application/octet-stream");

        PostMethod pm = new PostMethod(contentUrl);
        pm.setRequestEntity(entity);

        HttpClient hc = new HttpClient();

        hc.executeMethod(pm);
        assertEquals(HttpStatus.SC_OK, pm.getStatusCode());
    }

    public Test createRestoreTest(final InputStream in)
    {
        return new Test() {
            @Override
            public int countTestCases()
            {
                return 1;
            }

            @Override
            public void run(TestResult result)
            {
                result.startTest(this);
                try
                {
                    setLdif(in);
                    result.endTest(this);
                }
                catch (IOException e)
                {
                    result.addError(this, e);
                }
            }
        };
    }

    public static Test createRestoreTest(Class<?> testClass, String resourceName)
    {
        InputStream in = testClass.getResourceAsStream(resourceName);
        assertNotNull(in);

        return new LdifLoaderForTesting(CrowdAcceptanceTestCase.HOST_PATH).createRestoreTest(in);
    }
}
