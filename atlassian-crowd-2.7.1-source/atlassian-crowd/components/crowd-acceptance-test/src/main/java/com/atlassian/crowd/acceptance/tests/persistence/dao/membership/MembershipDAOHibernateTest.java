package com.atlassian.crowd.acceptance.tests.persistence.dao.membership;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.dao.membership.MembershipDAOHibernate;
import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.membership.InternalMembership;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import com.google.common.collect.Lists;

import org.hibernate.Query;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.atlassian.crowd.search.EntityDescriptor.group;
import static com.atlassian.crowd.search.EntityDescriptor.role;
import static com.atlassian.crowd.search.EntityDescriptor.user;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * This class tests MembershipDAOHibernate beyond the implementation of the
 * {@link com.atlassian.crowd.embedded.spi.MembershipDao} interface. In particular, it tests methods that do not
 * exist in the interface, such as
 * {@link com.atlassian.crowd.dao.membership.MembershipDAOHibernate#removeGroupMembers(long, String)}.
 * Therefore, the tests in this class are implementation specific, and cannot be run against
 * other implementations of the interface.
 * The reason to have tests for the implementation (as opposed to just testing the interface)
 * is that some legacy code is coupled to the Hibernate implementation of the interface.
 *
 * @see MembershipDaoCRUDTest
 * @see MembershipDaoSearchTest
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class MembershipDAOHibernateTest
{
    private static final long DIRECTORY_ID = 1L;

    @Inject private MembershipDAOHibernate membershipDAO;
    @Inject private UserDAOHibernate userDAO;
    @Inject private GroupDAOHibernate groupDAO;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    private static final String USERS_ROLE = "users";
    private static final String DEVELOPERS_GROUP = "developers";
    private static final String ADMIN_USER = "aDmin";
    private static final String PRODUCT_MANAGERS_ROLE = "Product Managers";

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Test
    public void testRemoveGroupMembers()
    {
        membershipDAO.removeGroupMembers(DIRECTORY_ID, USERS_ROLE);

        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, group()).childrenOf(group()).withName(USERS_ROLE).returningAtMost(10)).size());
        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(User.class, user()).childrenOf(group()).withName(USERS_ROLE).returningAtMost(10)).size());
    }

    @Test
    public void testRemoveGroupMemberships()
    {
        membershipDAO.removeGroupMemberships(DIRECTORY_ID, DEVELOPERS_GROUP);

        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, group()).parentsOf(group()).withName(DEVELOPERS_GROUP).returningAtMost(10)).size());
    }

    @Test
    public void testRemoveUserMemberships()
    {
        membershipDAO.removeUserMemberships(DIRECTORY_ID, ADMIN_USER);

        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, group()).parentsOf(user()).withName(ADMIN_USER).returningAtMost(10)).size());
    }

    @Test
    public void testRemoveAllRelationships()
    {
        membershipDAO.removeAllRelationships(DIRECTORY_ID);

        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, group()).childrenOf(group()).withName(USERS_ROLE).returningAtMost(10)).size());
        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(User.class, user()).childrenOf(group()).withName(USERS_ROLE).returningAtMost(10)).size());
        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, group()).parentsOf(group()).withName(DEVELOPERS_GROUP).returningAtMost(10)).size());
        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, group()).parentsOf(user()).withName(ADMIN_USER).returningAtMost(10)).size());

        // in fact, all relationships should be gone
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertThat(jdbcTemplate.queryForObject("select count(*) from cwd_membership where directory_id = ?",
                                               Integer.class, DIRECTORY_ID), is(0));
    }

    @Test
    public void testRemoveUserRelationships()
    {
        membershipDAO.removeAllUserRelationships(DIRECTORY_ID);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertThat(jdbcTemplate.queryForObject(
            "select count(*) from cwd_membership where membership_type = 'GROUP_USER' and directory_id = ?",
            Integer.class, DIRECTORY_ID), is(0));
    }

    @Test
    public void testRenameUserRelationships()
    {
        membershipDAO.renameUserRelationships(DIRECTORY_ID, ADMIN_USER, "badmin");

        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, group()).parentsOf(user()).withName(ADMIN_USER).returningAtMost(10)).size());
        assertEquals(2, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, group()).parentsOf(user()).withName("badmin").returningAtMost(10)).size());
        assertEquals(1, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, role()).parentsOf(user()).withName("badmin").returningAtMost(10)).size());
    }

    @Test
    public void testRenameGroupRelationships()
    {
        membershipDAO.renameGroupRelationships(DIRECTORY_ID, USERS_ROLE, "lusers");

        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, role()).childrenOf(role()).withName(USERS_ROLE).returningAtMost(10)).size());
        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(User.class, user()).childrenOf(role()).withName(USERS_ROLE).returningAtMost(10)).size());

        assertEquals(1, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(User.class, user()).childrenOf(role()).withName("lusers").returningAtMost(10)).size());
        assertEquals(1, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, role()).childrenOf(role()).withName("lusers").returningAtMost(10)).size());

        // assert the group-childgroup renaming
        assertEquals(1, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, role()).parentsOf(role()).withName(PRODUCT_MANAGERS_ROLE).returningAtMost(10)).size());
    }

    @Test
    public void testAddAll() throws Exception
    {
        // make 15 users
        Set<UserTemplateWithCredentialAndAttributes> users = new HashSet<UserTemplateWithCredentialAndAttributes>();
        for (int i = 0; i < 15; i++)
        {
            UserTemplateWithCredentialAndAttributes user = new UserTemplateWithCredentialAndAttributes("user" + i, DIRECTORY_ID, new PasswordCredential("secret", true));
            user.setEmailAddress("blah@example.com");
            user.setFirstName("Clone");
            user.setLastName("Drone");
            user.setDisplayName("Clone Drone");
            users.add(user);
        }
        userDAO.addAll(users);

        // make 3 groups
        for (int i = 0; i < 3; i++)
        {
            GroupTemplate group = new GroupTemplate("group" + i, DIRECTORY_ID, GroupType.LEGACY_ROLE);
            group.setDescription("description");
            groupDAO.add(group);
        }

        // make 45 memberships
        List<InternalUser> internalUsers = userDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(InternalUser.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.USERNAME).startingWith("user")).returningAtMost(100));
        List<InternalGroup> internalGroups = groupDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(InternalGroup.class,EntityDescriptor.role()).with(Restriction.on(GroupTermKeys.NAME).startingWith("group")).returningAtMost(100));
        Set<InternalMembership> memberships = new HashSet<InternalMembership>();
        for (InternalUser user : internalUsers)
        {
            for (InternalGroup group : internalGroups)
            {
                memberships.add(new InternalMembership(group, user));
            }
        }

        // do the add
        BatchResult<InternalMembership> result = membershipDAO.addAll(memberships);
        assertEquals(45, result.getTotalAttempted());
        assertEquals(45, result.getTotalSuccessful());

        // verify
        assertEquals(15, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.role()).withName("group0").returningAtMost(100)).size());
        assertEquals(15, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.role()).withName("group1").returningAtMost(100)).size());
        assertEquals(15,
                     membershipDAO.search(DIRECTORY_ID,
                                          QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                                              .childrenOf(EntityDescriptor.role())
                                              .withName("group2")
                                              .returningAtMost(100)).size());
    }

    @Test
    public void testAddAllWithDuplicates() throws Exception
    {
        Set<InternalMembership> memberships = new HashSet<InternalMembership>();

        InternalUser admin = userDAO.findByName(DIRECTORY_ID, "admin");
        InternalUser jane = userDAO.findByName(DIRECTORY_ID, "jane");
        InternalGroup administrators = groupDAO.findByName(DIRECTORY_ID, "administrators");

        // new
        memberships.add(new InternalMembership(administrators, jane));

        // duplicate
        InternalMembership duplicate = new InternalMembership(administrators, admin);
        memberships.add(duplicate);

        BatchResult<InternalMembership> result = membershipDAO.addAll(memberships);

        assertEquals(2, result.getTotalAttempted());
        assertEquals(1, result.getTotalSuccessful());
        assertTrue(result.getFailedEntities().contains(duplicate));
    }

    @Test
    public void testSearchCallsCreateHibernateSearchQuery()
    {
        final Query query = mock(Query.class);
        MembershipDAOHibernate dao = new MembershipDAOHibernate() {
            @Override
            protected Query createHibernateSearchQuery(long directoryId, MembershipQuery<?> crowdQuery) {
                return query;
            }
        };

        when(query.list()).thenReturn(Lists.newArrayList());

        dao.search(1L, null);

        verify(query).list();
    }
}
