package com.atlassian.crowd.acceptance.harness;

import com.atlassian.crowd.acceptance.tests.applications.demo.AddGroupTest;
import com.atlassian.crowd.acceptance.tests.applications.demo.AddUserTest;
import com.atlassian.crowd.acceptance.tests.applications.demo.DemoOGNLDoubleEvaluationTest;
import com.atlassian.crowd.acceptance.tests.applications.demo.DemoOGNLInjectionTest;
import com.atlassian.crowd.acceptance.tests.applications.demo.LoginTest;
import com.atlassian.crowd.acceptance.tests.applications.demo.SingleSignOnTest;
import com.atlassian.crowd.acceptance.tests.applications.demo.UpdateUserTest;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

public class DemoAcceptanceTestHarness extends TestCase
{
    public static Test suite()
    {
        TestSuite suite = new TestSuite();
        suite.addTestSuite(LoginTest.class);
        suite.addTestSuite(AddGroupTest.class);
        suite.addTestSuite(SingleSignOnTest.class);
        suite.addTestSuite(AddUserTest.class);
        suite.addTestSuite(UpdateUserTest.class);
        suite.addTestSuite(DemoOGNLInjectionTest.class);
        suite.addTestSuite(DemoOGNLDoubleEvaluationTest.class);

        return suite;
    }
}
