package com.atlassian.crowd.acceptance.tests.client;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.model.authentication.AuthenticatedToken;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.soap.client.SecurityServerClientImpl;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl;

import java.rmi.RemoteException;
import java.util.Properties;

public class AuthenticationTokenTest extends CrowdAcceptanceTestCase
{
    private SecurityServerClientImpl securityServerClient;
    private UserAuthenticationContext userAuthenticationContext;
    private Properties sscProperties;

    // Not a validation factor. Needed to check the browser is not included in validation.
    private static final String USER_AGENT = "User-Agent";

    public void setUp() throws Exception
    {
        super.setUp();

        restoreCrowdFromXML("tokenauthenticationtest.xml");

        sscProperties = AcceptanceTestHelper.loadProperties("localtest.crowd.properties");
        SoapClientProperties cProperties = SoapClientPropertiesImpl.newInstanceFromProperties(sscProperties);
        securityServerClient = new SecurityServerClientImpl(cProperties);

        userAuthenticationContext = new UserAuthenticationContext();
        userAuthenticationContext.setApplication("integrationtest");

    }


    public void testAuthenticateSimpleDifferentUsersAuthenticationProducesDifferentTokens() throws InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ExpiredCredentialException
    {
        log("Running testAuthenticateSimpleDifferentUsersAuthenticationProducesDifferentTokens");

        String token1 = securityServerClient.authenticatePrincipalSimple("user", "user");
        String token2 = securityServerClient.authenticatePrincipalSimple("user2", "user2");
        assertFalse(token1.equals(token2));
    }

    public void testAuthenticateSimpleSuccessiveAuthenticationProducesSameToken() throws InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ExpiredCredentialException
    {
        log("Running testAuthenticateSimpleSuccessiveAuthenticationProducesSameToken");

        String token1 = securityServerClient.authenticatePrincipalSimple("admin", "admin");
        String token2 = securityServerClient.authenticatePrincipalSimple("admin", "admin");

        assertEquals(token1, token2);
    }

    public void testAuthenticateSimpleDifferentTokenAfterLogout() throws InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ExpiredCredentialException
    {
        log("Running testAuthenticateSimpleDifferentTokenAfterLogout");

        String token1 = securityServerClient.authenticatePrincipalSimple("admin", "admin");
        String token2 = securityServerClient.authenticatePrincipalSimple("admin", "admin");

        assertEquals(token1, token2);

        securityServerClient.invalidateToken(token1); //logout

        token1 = securityServerClient.authenticatePrincipalSimple("admin", "admin");
        assertFalse(token1.equals(token2));
    }


    public void testAuthenticatePrincipalDifferentUsersAuthenticationProducesDifferentTokens() throws InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ExpiredCredentialException
    {
        log("Running testAuthenticatePrincipalDifferentUsersAuthenticationProducesDifferentTokens");

        userAuthenticationContext.setName("user");
        userAuthenticationContext.setCredential(new PasswordCredential("user"));
        String token1 = securityServerClient.authenticatePrincipal(userAuthenticationContext);

        userAuthenticationContext.setName("user2");
        userAuthenticationContext.setCredential(new PasswordCredential("user2"));
        String token2 = securityServerClient.authenticatePrincipal(userAuthenticationContext);

        assertFalse(token1.equals(token2));
    }


    public void testAuthenticatePrincipalSuccessiveAuthenticationProducesSameToken() throws InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ExpiredCredentialException
    {
        log("Running testAuthenticatePrincipalSuccessiveAuthenticationProducesSameToken");

        userAuthenticationContext.setName("user");
        userAuthenticationContext.setCredential(new PasswordCredential("user"));

        String token1 = securityServerClient.authenticatePrincipal(userAuthenticationContext);
        String token2 = securityServerClient.authenticatePrincipal(userAuthenticationContext);

        assertEquals(token1, token2);
    }

    public void testAuthenticatePrincipalDifferentTokenAfterLogout() throws InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ExpiredCredentialException
    {
        log("Running testAuthenticatePrincipalDifferentTokenAfterLogout");

        userAuthenticationContext.setName("user");
        userAuthenticationContext.setCredential(new PasswordCredential("user"));

        String token1 = securityServerClient.authenticatePrincipal(userAuthenticationContext);
        String token2 = securityServerClient.authenticatePrincipal(userAuthenticationContext);

        assertEquals(token1, token2);

        securityServerClient.invalidateToken(token1); //logout

        token1 = securityServerClient.authenticatePrincipal(userAuthenticationContext);
        assertFalse(token1.equals(token2));
    }


    public void testAuthenticatePrincipalSameUserDifferentConditionProducesDifferentTokens() throws InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ExpiredCredentialException
    {
        log("Running testAuthenticatePrincipalSameUserDifferentConditionProducesDifferentTokens");

        userAuthenticationContext.setName("admin");
        userAuthenticationContext.setCredential(new PasswordCredential("admin"));


        userAuthenticationContext.setValidationFactors(new ValidationFactor[]{
                new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, "127.0.0.1")});

        String token1 = securityServerClient.authenticatePrincipal(userAuthenticationContext);

        // Change authentication context
        userAuthenticationContext.setValidationFactors(new ValidationFactor[]{
                new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, "192.168.0.1")});

        String token2 = securityServerClient.authenticatePrincipal(userAuthenticationContext);

        assertFalse(token1.equals(token2));
    }

    public void testAuthenticatePrincipalSameUserDifferentUserAgentProducesSameTokens() throws InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ExpiredCredentialException
    {
        log("Running testAuthenticatePrincipalSameUserDifferentUserAgentProducesSameTokens");

        userAuthenticationContext.setName("admin");
        userAuthenticationContext.setCredential(new PasswordCredential("admin"));


        userAuthenticationContext.setValidationFactors(new ValidationFactor[]{
                new ValidationFactor(USER_AGENT, "IE 8.0"),
                new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, "127.0.0.1")});

        String token1 = securityServerClient.authenticatePrincipal(userAuthenticationContext);

        // Change authentication context
        userAuthenticationContext.setValidationFactors(new ValidationFactor[]{
                new ValidationFactor(USER_AGENT, "IE 7.0"),
                new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, "127.0.0.1")});

        String token2 = securityServerClient.authenticatePrincipal(userAuthenticationContext);

        assertTrue(token1.equals(token2));
    }

    public void testAuthenticateApplicationSuccessiveAuthenticationSameTokens() throws Exception
    {
        log("Running testAuthenticateApplicationSuccessiveAuthenticationSameTokens");


        securityServerClient.authenticate();
        AuthenticatedToken token = securityServerClient.getApplicationToken();
        String token1 = token.getToken();

        securityServerClient.authenticate();
        token = securityServerClient.getApplicationToken();
        String token2 = token.getToken();

        assertEquals(token1, token2);
    }

    public void testAuthenticateApplicationDifferentContextDifferentTokens() throws Exception
    {
        log("Running testAuthenticateApplicationDifferntContextDifferentTokens");

        securityServerClient.authenticate();
        AuthenticatedToken token = securityServerClient.getApplicationToken();
        String token1 = token.getToken();

        // Expire application token
        gotoCurrentApplicationSessions();
        assertKeyPresent("session.application.title");
        assertLinkPresentWithExactText("integrationtest");
        clickLink("expire_" + token1);

        securityServerClient.authenticate();
        token = securityServerClient.getApplicationToken();
        String token2 = token.getToken();

        assertFalse(token1.equals(token2));
    }
}
