package com.atlassian.crowd.acceptance.tests.directory.suite;

import java.util.List;
import java.util.Properties;

import com.atlassian.crowd.acceptance.tests.applications.crowd.LdifLoaderForTesting;
import com.atlassian.crowd.acceptance.tests.directory.BaseTest;
import com.atlassian.crowd.acceptance.tests.directory.BasicTest;
import com.atlassian.crowd.acceptance.tests.directory.GroupRoleTest;
import com.atlassian.crowd.acceptance.tests.directory.LocalAttributesTest;
import com.atlassian.crowd.acceptance.tests.directory.LocalGroupsTest;
import com.atlassian.crowd.acceptance.tests.directory.MockSynchronisationStatusManager;
import com.atlassian.crowd.acceptance.tests.directory.NestedGroupsTest;
import com.atlassian.crowd.acceptance.tests.directory.PageAndRangeTest;
import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.DbCachingRemoteDirectory;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.manager.directory.SynchronisationMode;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;

import com.google.common.collect.ImmutableSet;

import static com.atlassian.crowd.acceptance.tests.directory.BasicTest.getImplementation;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * Tests for ApacheDS 1.5.4, running against the localhost webapp version.
 */
public class ApacheDS154Test extends TestCase
{
    public static Test suite()
    {
        TestSuite suite = new TestSuite();

        suite.addTest(LdifLoaderForTesting.createRestoreTest(ApacheDS154Test.class, "/com/atlassian/crowd/acceptance/tests/default-entries.ldif"));

        suite.addTestSuite(ApacheDS154BasicTest.class);
//        suite.addTestSuite(ApacheDS154NestedGroupsTest.class);// this test is flakey because not sure why but fails randomly on JDK 1.6 with LDAP error code 80
//        suite.addTestSuite(ApacheDS154PageAndRangeTest.class); // this test takes too long as it needs loads of users
//        suite.addTestSuite(ApacheDS154CachingBasicTest.class); // this test is flakey because ApacheDS 1.5's remote event monitoring is crap
//        suite.addTestSuite(ApacheDS154CachingNestedGroupsTest.class); // this test is flakey because ApacheDS 1.5's remote event monitoring is crap
        suite.addTestSuite(ApacheDS154GroupRoleTest.class);
        suite.addTestSuite(ApacheDS154LocalAttributesTest.class);
        suite.addTestSuite(ApacheDS154LocalGroupsTest.class);

        suite.addTest(LdifLoaderForTesting.createRestoreTest(ApacheDS154Test.class, "ApacheDS154Test-non-cn-name-entries.ldif"));
        suite.addTestSuite(SynchroniseMembershipsWithNonCnAttributeForGroupNames.class);
        return suite;
    }

    public static class ApacheDS154BasicTest extends BasicTest
    {
        public ApacheDS154BasicTest()
        {
            super();
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
        }

        public void testAuthenticateAfterPasswordUpdate()
        {
            // Apache DS 1.0.2 will not pick up password changes without a restart (!)
            assertTrue(true);
        }
    }

    public static class ApacheDS154NestedGroupsTest extends NestedGroupsTest
    {
        public ApacheDS154NestedGroupsTest()
        {
            super();
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
        }

        @Override
        protected int getInitialGroupMemberCount()
        {
            // apacheds 1.5.x will always create a new group with the bind user as the initial unique member
            // but since crowd 2.0 this group member will be out of scope and hence not consider a member
            return 0;
        }
    }

    public static class ApacheDS154PageAndRangeTest extends PageAndRangeTest
    {
        public ApacheDS154PageAndRangeTest()
        {
            super();
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
        }
    }

    public static class ApacheDS154GroupRoleTest extends GroupRoleTest
    {
        public ApacheDS154GroupRoleTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
        }
    }

    public static class ApacheDS154LocalAttributesTest extends LocalAttributesTest
    {
        public ApacheDS154LocalAttributesTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
        }
    }

    public static class ApacheDS154LocalGroupsTest extends LocalGroupsTest
    {
        public ApacheDS154LocalGroupsTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
        }
    }

    public static class SynchroniseMembershipsWithNonCnAttributeForGroupNames extends BaseTest
    {
        public SynchroniseMembershipsWithNonCnAttributeForGroupNames()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
        }

        @Override
        protected void configureDirectory(Properties directorySettings)
        {
            super.configureDirectory(directorySettings);
            directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.TRUE.toString());
            directory.setAttribute(LDAPPropertiesMapper.GROUP_NAME_KEY, "ou");
        }

        @Override
        protected void loadTestData() throws Exception
        {
        }

        @Override
        protected void removeTestData()
        {
        }

        public void testSynchroniseUsersAndGroups() throws Exception
        {
            final DbCachingRemoteDirectory internalDirectory = (DbCachingRemoteDirectory) getImplementation(directory);
            internalDirectory.synchroniseCache(SynchronisationMode.FULL, new MockSynchronisationStatusManager());

            User user1 = internalDirectory.findUserByName("user1");
            assertNotNull(user1);

            assertNotNull(internalDirectory.findGroupByName("group-full-name"));

            List<String> groups;

            groups = internalDirectory.searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("user1").returningAtMost(10));
            assertEquals(ImmutableSet.of("group-full-name"), ImmutableSet.copyOf(groups));
        }
    }
}
