package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.acceptance.utils.AbstractDbCachingLoadTest;
import com.atlassian.crowd.acceptance.utils.LoadStatWriter;

/**
 * Summary of the loadTesting ou that exists on crowd-ad1 and TPM
 * https://extranet.atlassian.com/display/CROWD/Load+Testing
 */
public class DbCachingLoadTest extends AbstractDbCachingLoadTest
{
    private LoadStatWriter statWriter;
    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreBaseSetup();

        statWriter = new LoadStatWriter(getClass().getName());
    }

    @Override
    public void tearDown() throws Exception
    {
        statWriter.close();

        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testSynchronise() throws InterruptedException
    {
       // Run this one first so we know how long synchronising 10k users will take on a relatively fresh database
        _testSynchronise10kTo5kUsers();

        // The other tests
        _testSynchroniseSwap2kUsers();
        _testSynchronise1kTo10kUsers();
        _testSynchronise5kTo10kUsers();
        _testSynchronise10kTo1kUsers();
    }


    private void _testSynchroniseSwap2kUsers() throws InterruptedException
    {
        logger.info("Running: _testSynchroniseSwap2kUsers");
        restoreBaseSetup();
        intendToModifyData();

        // Create directory pointed at 2k
        createLoadTestingDirectory(CONNECTOR_URL, CHILD_OU_B, CONNECTOR_USERDN, CONNECTOR_USERPW);

        // Synchronise the directory
        long duration1 = synchroniseDirectory("2k Users");

        // Check that users from CHILD_OU_B can be found. (while CHILD_OU_C are not there)
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUNotPresent("C");
        assertFalse(isLargeRootGroupPresent());
        assertMembershipsPresent("B", MEMBERSHIPS_COUNT);

        // Change to the other 2k OU
        changeDirectoryBaseDN(CHILD_OU_C);

        long duration2 = synchroniseDirectory("2k Users");

        // Check that users have been 'swapped' so CHILD_OU_C can be found while CHILD_OU_B can't
        assertUsersAndGroupsFromOUNotPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertFalse(isLargeRootGroupPresent());
        assertMembershipsPresent("C", MEMBERSHIPS_COUNT);

        // Print overall summary
        logger.info("");
        logger.info("======================================================================");
        logger.info("# Synchronise 2k users and then change ou to synchronise a new set of 2k users");
        logger.info("#    Initial 2k users took (in seconds):\t\t" + duration1);
        logger.info("#    Swap to new 2k users took (in seconds):\t" + duration2);
        logger.info("======================================================================");
        logger.info("");

        statWriter.addPoint("testSynchroniseSwap2kUsers_initial_2k_users", duration1);
        statWriter.addPoint("testSynchroniseSwap2kUsers_swap_to_new_2k_users", duration2);
    }

    private void _testSynchronise1kTo10kUsers() throws InterruptedException
    {
        logger.info("Running: _testSynchronise1kTo10kUsers");
        restoreBaseSetup();
        intendToModifyData();

        // Create directory pointed at 1k
        createLoadTestingDirectory(CONNECTOR_URL, CHILD_OU_D, CONNECTOR_USERDN, CONNECTOR_USERPW);

        // Synchronise the directory
        long duration1 = synchroniseDirectory("1k Users");

        // Check users from CHILD_OU_D can be found
        assertUsersAndGroupsFromOUNotPresent("A");
        assertUsersAndGroupsFromOUPresent("D");
        assertFalse(isLargeRootGroupPresent());
        assertMembershipsPresent("D", MEMBERSHIPS_COUNT);

        // Change to 10k
        changeDirectoryBaseDN(CONNECTOR_BASEDN);

        long duration2 = synchroniseDirectory("10k Users");

        // Check all users can now be found - including the all encompassing group
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertUsersAndGroupsFromOUPresent("D");
        assertTrue(isLargeRootGroupPresent());
        assertMembershipsPresent("A", MEMBERSHIPS_COUNT + 1); // +1 for the all encompassing group
        assertMembershipsPresent("B", MEMBERSHIPS_COUNT + 1);
        assertMembershipsPresent("C", MEMBERSHIPS_COUNT + 1);
        assertMembershipsPresent("D", MEMBERSHIPS_COUNT + 1);

        // Print overall summary
        logger.info("======================================================================");
        logger.info("# Synchronise 1k users and then change ou to synchronise 10k users");
        logger.info("#    Initial 1k users took (in seconds):\t\t" + duration1);
        logger.info("#    Increase to 10k users took (in seconds):\t" + duration2);
        logger.info("======================================================================");

        statWriter.addPoint("testSynchronise1kTo10kUsers_initial_1k_users", duration1);
        statWriter.addPoint("testSynchronise1kTo10kUsers_increase_to_10k_users", duration2);
    }

    private void _testSynchronise5kTo10kUsers() throws InterruptedException
    {
        logger.info("Running: _testSynchronise5kTo10kUsers");
        restoreBaseSetup();
        intendToModifyData();

        // Create directory pointed at 5k
        createLoadTestingDirectory(CONNECTOR_URL, CHILD_OU_A, CONNECTOR_USERDN, CONNECTOR_USERPW);

        // Synchronise the directory
        long duration1 = synchroniseDirectory("5k Users");

        // Check users from CHILD_OU_A can be found
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUNotPresent("D");
        assertFalse(isLargeRootGroupPresent());
        assertMembershipsPresent("A", MEMBERSHIPS_COUNT);

        // Change to 10k
        changeDirectoryBaseDN(CONNECTOR_BASEDN);

        long duration2 = synchroniseDirectory("10k Users");

        // Check all users can now be found - including the all encompassing group
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertUsersAndGroupsFromOUPresent("D");
        assertTrue(isLargeRootGroupPresent());
        assertMembershipsPresent("A", MEMBERSHIPS_COUNT + 1); // +1 for the all encompassing group
        assertMembershipsPresent("B", MEMBERSHIPS_COUNT + 1);
        assertMembershipsPresent("C", MEMBERSHIPS_COUNT + 1);
        assertMembershipsPresent("D", MEMBERSHIPS_COUNT + 1);

        // Print overall summary
        logger.info("======================================================================");
        logger.info("# Synchronise 5k users and then change ou to synchronise 10k users");
        logger.info("#    Initial 5k users took (in seconds):\t\t" + duration1);
        logger.info("#    Increase to 10k users took (in seconds):\t" + duration2);
        logger.info("======================================================================");

        statWriter.addPoint("testSynchronise5kTo10kUsers_initial_5k_users", duration1);
        statWriter.addPoint("testSynchronise5kTo10kUsers_increase_to_10k_users", duration2);
    }

    private void _testSynchronise10kTo1kUsers() throws InterruptedException
    {
        logger.info("Running: _testSynchronise10kTo1kUsers");
        restoreBaseSetup();
        intendToModifyData();

        // Create directory pointed at 10k
        createLoadTestingDirectory(CONNECTOR_URL, CONNECTOR_BASEDN, CONNECTOR_USERDN, CONNECTOR_USERPW);

        // Synchronise the directory
        long duration1 = synchroniseDirectory("10k Users");

        // Check all users can be found - including the all encompassing group
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertUsersAndGroupsFromOUPresent("D");
        assertTrue(isLargeRootGroupPresent());
        assertMembershipsPresent("A", MEMBERSHIPS_COUNT + 1); // +1 for the all encompassing group
        assertMembershipsPresent("B", MEMBERSHIPS_COUNT + 1);
        assertMembershipsPresent("C", MEMBERSHIPS_COUNT + 1);
        assertMembershipsPresent("D", MEMBERSHIPS_COUNT + 1);

        // Change to 1k
        changeDirectoryBaseDN(CHILD_OU_D);

        long duration2 = synchroniseDirectory("1k Users");

        // Check users are now restricted to CHILD_OU_D
        assertUsersAndGroupsFromOUNotPresent("A");
        assertUsersAndGroupsFromOUPresent("D");
        assertFalse(isLargeRootGroupPresent());
        assertMembershipsPresent("D", MEMBERSHIPS_COUNT);

        // Print overall summary
        logger.info("");
        logger.info("======================================================================");
        logger.info("# Synchronise 10k users and then change ou to synchronise 1k users");
        logger.info("#    Initial 10k users took (in seconds):\t\t" + duration1);
        logger.info("#    Decrease to 1k users took (in seconds):\t" + duration2);
        logger.info("======================================================================");
        logger.info("");

        statWriter.addPoint("testSynchronise10kTo1kUsers_initial_10k_users", duration1);
        statWriter.addPoint("testSynchronise10kTo1kUsers_decrease_to_1k_users", duration2);
    }

    private void _testSynchronise10kTo5kUsers() throws InterruptedException
    {
        logger.info("Running: _testSynchronise10kTo5kUsers");
        restoreBaseSetup();
        intendToModifyData();

        // Create directory pointed at 10k
        createLoadTestingDirectory(CONNECTOR_URL, CONNECTOR_BASEDN, CONNECTOR_USERDN, CONNECTOR_USERPW);

        // Synchronise the directory
        long duration1 = synchroniseDirectory("10k Users");

        // Check all users can now be found - including the all encompassing group
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertUsersAndGroupsFromOUPresent("D");
        assertTrue(isLargeRootGroupPresent());
        assertMembershipsPresent("A", MEMBERSHIPS_COUNT + 1); // +1 for the all encompassing group
        assertMembershipsPresent("B", MEMBERSHIPS_COUNT + 1);
        assertMembershipsPresent("C", MEMBERSHIPS_COUNT + 1);
        assertMembershipsPresent("D", MEMBERSHIPS_COUNT + 1);

        // Change to 5k
        changeDirectoryBaseDN(CHILD_OU_A);

        long duration2 = synchroniseDirectory("5k Users");

        // Check users are now restricted to CHILD_OU_A
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUNotPresent("D");
        assertFalse(isLargeRootGroupPresent());
        assertMembershipsPresent("A", MEMBERSHIPS_COUNT);

        // Print overall summary
        logger.info("");
        logger.info("======================================================================");
        logger.info("# Synchronise 10k users and then change ou to synchronise 5k users");
        logger.info("#    Initial 10k users took (in seconds):\t\t" + duration1);
        logger.info("#    Decrease to 5k users took (in seconds):\t" + duration2);
        logger.info("======================================================================");
        logger.info("");

        statWriter.addPoint("testSynchronise10kTo5kUsers_initial_10k_users", duration1);
        statWriter.addPoint("testSynchronise10kTo5kUsers_decrease_to_5k_users", duration2);
    }

    // ----------------------------------------------
    // Helper methods
    // ----------------------------------------------

    private boolean isLargeRootGroupPresent()
    {
        gotoBrowseGroups();
        setWorkingForm("browsegroups");
        setTextField("name", "all-");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME);
        submit();

        // Check if we can see the group listed
        return isTextPresent("all-group");
    }

    private void assertMembershipsPresent(String childOU, int membershipsCount)
    {
        //Go and view the user's membership (test 5 users)
        for (int i = 0; i < 5; i++)
        {
            gotoViewPrincipal(createUserName(childOU, i), CONNECTOR_DIRECTORY_NAME);
            clickLink("user-groups-tab");

            // Users have a fixed number of memberships +1 for header
            setWorkingForm("groupsForm");
            assertTableRowCountEquals("groupsTable", membershipsCount + 1);
            assertTextPresent(childOU + PARTIAL_GROUPNAME); // don't know the exact groups (more so sanity test)
        }
    }

    private void changeDirectoryBaseDN(String newBaseDN)
    {
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME);

        clickLink("connector-connectiondetails");
        setWorkingForm("connectordetails");
        setTextField("baseDN", newBaseDN);
        submit();

        assertTextFieldEquals("baseDN", newBaseDN);
    }
}
