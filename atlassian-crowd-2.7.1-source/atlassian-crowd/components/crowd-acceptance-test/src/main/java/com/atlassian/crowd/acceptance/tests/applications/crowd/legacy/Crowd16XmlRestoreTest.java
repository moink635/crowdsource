package com.atlassian.crowd.acceptance.tests.applications.crowd.legacy;

public class Crowd16XmlRestoreTest extends BaseLegacyXmlRestoreTest
{
    public String getLegacyXmlFileName()
    {
        return "1_6_1_391.xml";
    }
}

