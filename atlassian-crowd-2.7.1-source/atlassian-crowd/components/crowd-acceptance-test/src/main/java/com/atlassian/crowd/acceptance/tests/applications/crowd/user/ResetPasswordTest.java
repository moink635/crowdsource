package com.atlassian.crowd.acceptance.tests.applications.crowd.user;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCaseWithEmailServer;

public class ResetPasswordTest extends CrowdAcceptanceTestCaseWithEmailServer
{
    private static final String USERNAME = "user";
    private static final String DIRECTORY_NAME = "Test Internal Directory";

    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        restoreCrowdFromXML("userconsoletest.xml");

        _loginAdminUser();

        _configureMailServer();
    }

    public void testUserCanLoginAfterResettingPassword() throws Exception
    {
        intendToModifyData();

        _requestPasswordReset(USERNAME);

        _logout();

        _followResetLinkInEmail();

        _resetPasswordTo("newPassword");

        assertKeyPresent("resetpassword.complete.label");

        // user can login with the new password
        _loginAsUser(USERNAME, "newPassword");
    }

    public void testResetPasswordMaintainsPasswordHistory() throws Exception
    {
        intendToModifyData();

        _loginAsUser(USERNAME, "password");

        gotoPage("/console/user/viewchangepassword.action");

        // first change succeeds
        _changePassword("password", "password1");
        assertKeyPresent("passwordupdate.message");

        // second change succeeds
        _changePassword("password1", "password2");
        assertKeyPresent("passwordupdate.message");

        // third change succeeds. At this point, password history contains password2 and password3, but not password1
        _changePassword("password2", "password3");
        assertKeyPresent("passwordupdate.message");

        _loginAdminUser();

        _requestPasswordReset(USERNAME);

        _logout();

        _followResetLinkInEmail();

        // sorry, you cannot go back to password2 (it's still in password history)
        _resetPasswordTo("password2");
        assertKeyNotPresent("resetpassword.complete.label");
        assertTextPresent("Unable to update password since this password matches");

        // changing to the same password is also forbidden
        _resetPasswordTo("password3");
        assertKeyNotPresent("resetpassword.complete.label");
        assertTextPresent("Unable to update password since this password matches");

        // resetting the password1 is OK, and password2 is dropped from password history
        _resetPasswordTo("password1");
        assertKeyPresent("resetpassword.complete.label");

        // user can login with the new password
        _loginAsUser(USERNAME, "password1");

        gotoPage("/console/user/viewchangepassword.action");

        // sorry, you cannot go back to password3 (it's still in password history)
        _changePassword("password1", "password3");
        assertKeyNotPresent("passwordupdate.message");
        assertTextPresent("Unable to update password since this password matches");

        // changing back to password2 is OK because password1 has already been dropped from the password history
        _changePassword("password1", "password2");
        assertKeyPresent("passwordupdate.message");
    }

    public void testResetPasswordWithBadTokenShowsErrorMessage()
    {
        _logout();

        long directoryId = 32769;

        String queryWithBadToken = "username=user&directoryId=" + directoryId + "&token=bad-reset-password-token";

        gotoPage("/console/resetpassword.action?" + queryWithBadToken);

        assertKeyPresent("forgottenpassword.title");

        // Make sure that directory exists before going ahead
        assertTextNotPresent("does not exist");

        /* Prompt the user that token is bad without showing the reset password form */
        assertKeyPresent("forgottenpassword.title");
        assertKeyPresent("forgottenlogindetails.error.expiredtoken");
    }

    private void _requestPasswordReset(String user)
    {
        gotoViewPrincipal(user, DIRECTORY_NAME);
        clickLink("reset-password-principal");
        submit();
    }

    private void _followResetLinkInEmail() throws InterruptedException, IOException, MessagingException
    {
        MimeMessage message = waitForExactlyOneMessage();
        Pattern pattern = Pattern.compile("/console/resetpassword\\.action\\?[\\S]+");
        Matcher matcher = pattern.matcher(message.getContent().toString());

        assertTrue("Email message must contain reset link:" + message.getContent().toString(), matcher.find());
        String url = matcher.group();

        gotoPage(url);
        assertKeyPresent("forgottenpassword.title");
    }

    private void _resetPasswordTo(String newPassword)
    {
        assertKeyPresent("forgottenpassword.title");
        setTextField("password", newPassword);
        setTextField("confirmPassword", newPassword);
        submit();
    }

    private void _changePassword(String oldPassword, String newPassword)
    {
        assertKeyPresent("menu.user.console.changepassword.label");

        setTextField("originalPassword", oldPassword);
        setTextField("password", newPassword);
        setTextField("confirmPassword", newPassword);
        submit();

        assertKeyPresent("menu.user.console.changepassword.label");
    }
}
