package com.atlassian.crowd.acceptance.utils;

import javax.servlet.ServletContext;

import com.atlassian.config.HomeLocator;

import org.apache.commons.io.FileUtils;

/**
 * An implementation of HomeLocator that is used for acceptance tests that depend on
 * having a Crowd home directory. It points to a temporary directory.
 */
public class TestHomeLocator implements HomeLocator
{
    @Override
    public String getHomePath()
    {
        return FileUtils.getTempDirectoryPath();
    }

    @Override
    public String getConfigFileName()
    {
        throw new UnsupportedOperationException("This test object does not support this operation");
    }

    @Override
    public void lookupServletHomeProperty(ServletContext context)
    {
        throw new UnsupportedOperationException("This test object does not support this operation");
    }
}
