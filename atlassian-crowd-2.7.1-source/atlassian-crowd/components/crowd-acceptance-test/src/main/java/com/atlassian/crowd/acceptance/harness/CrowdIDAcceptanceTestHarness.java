package com.atlassian.crowd.acceptance.harness;

import com.atlassian.crowd.acceptance.tests.applications.crowdid.PublicIdentityTest;
import com.atlassian.crowd.acceptance.tests.applications.crowdid.client.OpenIDClientOGNLInjectionTest;
import com.atlassian.crowd.acceptance.tests.applications.crowdid.client.OpenIDAuthenticationTest;
import com.atlassian.crowd.acceptance.tests.applications.crowdid.server.OpenIDServerOGNLInjectionTest;
import com.atlassian.crowd.acceptance.tests.applications.crowdid.server.OpenIDServerTest;
import com.atlassian.crowd.acceptance.tests.applications.crowdid.server.OpenIDServerTestWithOpenID2;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * The main testing acceptance harness that will be used to test the CrowdID.
 *
 * POM will ensure that SetupCrowdTest is run prior to the suite.
 */
@RunWith(Suite.class)
@SuiteClasses({
    OpenIDServerTestWithOpenID2.class,
    PublicIdentityTest.class,
    OpenIDAuthenticationTest.class,
    OpenIDServerTest.class,
    OpenIDClientOGNLInjectionTest.class,
    OpenIDServerOGNLInjectionTest.class
})
public class CrowdIDAcceptanceTestHarness
{
}
