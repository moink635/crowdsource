package com.atlassian.crowd.acceptance.tests.rest;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;

import javax.ws.rs.core.MediaType;

import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.acceptance.tests.soap.InformationLeakingTestBase;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.io.IOUtils;
import org.hamcrest.CoreMatchers;

import static org.hamcrest.CoreMatchers.containsString;

import static org.junit.Assert.assertThat;

public class RestXmlParsingTest extends InformationLeakingTestBase
{
    private static String crowdApplicationPassword;

    private final RestServer restServer;

    public RestXmlParsingTest()
    {
        this(RestServerImpl.INSTANCE);
    }

    public RestXmlParsingTest(RestServer restServer)
    {
        this.restServer = restServer;
    }

    public void setUp() throws Exception
    {
        super.setUp();
        restServer.before();
    }

    public void tearDown() throws Exception
    {
        restServer.after();
        super.tearDown();
    }

    @Override
    public String getCrowdApplicationPassword()
    {
        return "qybhDMZh"; // from restsetup.xml
    }

    PostMethod postToRestEndpoint(String content) throws HttpException, UnsupportedEncodingException, IOException
    {
        RequestEntity re = new StringRequestEntity(content, "application/xml", "us-ascii");

        String endpoint = getBaseUrl() + "/rest/usermanagement/1/search?entity-type=user";

        HttpClient client = new HttpClient();

        Credentials creds = new UsernamePasswordCredentials("crowd", getCrowdApplicationPassword());

        URI base = URI.create(HOST_PATH);

        client.getState().setCredentials(
                new AuthScope(base.getHost(), base.getPort(), AuthScope.ANY_REALM), creds);

        PostMethod m = new PostMethod(endpoint);

        m.setRequestHeader("Accept", "application/xml");
        m.setRequestEntity(re);

        client.executeMethod(m);

        return m;
    }

    public void testEntityExpansionDoesNotIncludeFileContents() throws IOException
    {
        InputStream in = getClass().getResourceAsStream("RestXmlParsingTest-rest-include-external-entity.xml");
        assertNotNull(in);

        String contents = IOUtils.toString(in, "us-ascii");

        contents = contents.replace("/etc/passwd", createSecretFile().toURI().toString());

        PostMethod m = postToRestEndpoint(contents);

        String resp = IOUtils.toString(m.getResponseBodyAsStream(), "us-ascii");

        MediaType mt = MediaType.valueOf(m.getResponseHeader("content-type").getValue());

        assertEquals("The response should be XML",
                "application/xml",
                mt.getType() + '/' + mt.getSubtype()
                );
        assertThat(resp, CoreMatchers.not(containsString(secret)));
    }

    public void testValidEntitiesAreExpanded() throws IOException
    {
        InputStream in = getClass().getResourceAsStream("RestXmlParsingTest-rest-with-amp-entity.xml");
        assertNotNull(in);

        String contents = IOUtils.toString(in, "us-ascii");

        PostMethod m = postToRestEndpoint(contents);

        String resp = IOUtils.toString(m.getResponseBodyAsStream(), "us-ascii");

        MediaType mt = MediaType.valueOf(m.getResponseHeader("content-type").getValue());

        assertEquals("The response should be XML",
                "application/xml",
                mt.getType() + '/' + mt.getSubtype()
                );
        assertThat(resp,
                CoreMatchers.anyOf(
                        // JDK 6
                        containsString("No enum const class com.atlassian.crowd.search.query.entity.restriction.MatchMode.&amp;"),

                        // JDK 7
                        containsString("No enum constant com.atlassian.crowd.search.query.entity.restriction.MatchMode.&amp;")
                    ));
    }

    public void testEntityExpansionDoesNotCauseDenialOfService() throws IOException
    {
        InputStream in = getClass().getResourceAsStream("RestXmlParsingTest-rest-billion-laughs.xml");
        assertNotNull(in);

        String contents = IOUtils.toString(in, "us-ascii");

        PostMethod m = postToRestEndpoint(contents);

        String resp = IOUtils.toString(m.getResponseBodyAsStream(), "us-ascii");

        /*
         * Different versions and combinations indicate failure in different ways; these assertions may need to
         * change. The important thing is that the response comes back immediately and indicates that parsing
         * failed without exhausting memory.
         */
        assertThat("The response should indicate a parsing error",
                resp, containsString("The request sent by the client was syntactically incorrect"));

       assertThat("The response should not indicate a server memory error",
                resp, CoreMatchers.not(containsString("java.lang.OutOfMemoryError")));

        MediaType mt = MediaType.valueOf(m.getResponseHeader("content-type").getValue());

//        assertEquals("The response should be XML, not HTML", "application/xml",
//                mt.getType() + '/' + mt.getSubtype());
    }
}
