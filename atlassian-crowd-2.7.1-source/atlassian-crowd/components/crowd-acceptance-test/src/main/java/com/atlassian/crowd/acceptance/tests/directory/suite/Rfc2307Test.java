package com.atlassian.crowd.acceptance.tests.directory.suite;

import java.io.InputStream;
import java.util.List;
import java.util.Properties;

import com.atlassian.crowd.acceptance.harness.CrowdDirectoryTestHarness;
import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.tests.applications.crowd.LdifLoaderForTesting;
import com.atlassian.crowd.acceptance.tests.directory.BaseTest;
import com.atlassian.crowd.acceptance.tests.directory.BasicTest;
import com.atlassian.crowd.acceptance.tests.directory.MockSynchronisationStatusManager;
import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.DbCachingRemoteDirectory;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.RFC2307Directory;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.manager.directory.SynchronisationMode;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

import com.google.common.collect.ImmutableList;

import static com.atlassian.crowd.acceptance.tests.directory.BasicTest.getImplementation;
import junit.framework.Test;
import junit.framework.TestSuite;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.emptyIterable;
import static org.junit.Assert.assertThat;

public abstract class Rfc2307Test extends BaseTest
{
    public static final String CONFIG_FILE = "localhost-apacheDS-rfc2307.properties";

    public static Test suite()
    {
        TestSuite suite = new TestSuite();

        InputStream in = CrowdDirectoryTestHarness.class.getResourceAsStream("/com/atlassian/crowd/acceptance/tests/rfc2307-entries.ldif");
        assertNotNull(in);

        suite.addTest(new LdifLoaderForTesting(CrowdAcceptanceTestCase.HOST_PATH).createRestoreTest(in));
        suite.addTestSuite(Rfc2307SynchronisationTest.class);
        suite.addTestSuite(Rfc2307QueryForNamesTest.class);
        return suite;
    }

    public Rfc2307Test()
    {
        setDirectoryConfigFile(CONFIG_FILE);
    }

    @Override
    protected void loadTestData() throws Exception
    {
    }

    @Override
    protected void removeTestData() throws DirectoryInstantiationException
    {
        DirectoryTestHelper.silentlyRemoveUser("root", getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser("user", getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup("wheel", getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup("users", getRemoteDirectory());
    }

    public static class Rfc2307SynchronisationTest extends Rfc2307Test
    {
        @Override
        protected void configureDirectory(Properties directorySettings)
        {
            super.configureDirectory(directorySettings);
            directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.TRUE.toString());
        }

        public void testSynchroniseUsersAndGroups() throws Exception
        {
            final DbCachingRemoteDirectory internalDirectory = (DbCachingRemoteDirectory) getImplementation(directory);
            internalDirectory.synchroniseCache(SynchronisationMode.FULL, new MockSynchronisationStatusManager());

            User root = internalDirectory.findUserByName("root");
            assertEquals("Root User", root.getDisplayName());

            User user = internalDirectory.findUserByName("user");
            assertEquals("Regular User", user.getDisplayName());

            assertNotNull(internalDirectory.findGroupByName("wheel"));
            assertNotNull(internalDirectory.findGroupByName("users"));

            List<String> groups;

            groups = internalDirectory.searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("root").returningAtMost(10));
            assertThat(groups, containsInAnyOrder("wheel", "users"));

            groups = internalDirectory.searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("user").returningAtMost(10));
            assertEquals(ImmutableList.of("users"), groups);
        }
    }

    public static class Rfc2307QueryForNamesTest extends Rfc2307Test
    {
        @Override
        protected void configureDirectory(Properties directorySettings)
        {
            super.configureDirectory(directorySettings);
            directory.setAttribute(DirectoryProperties.CACHE_ENABLED, "false");
        }

        private RFC2307Directory getImplementation() throws DirectoryInstantiationException
        {
            return (RFC2307Directory) BasicTest.getImplementation(directory);
        }

        public void testGettingGroupMembershipsForUserByName() throws Exception
        {
            List<String> rootGroups = getImplementation().searchGroupRelationships(QueryBuilder.createMembershipQuery(-1, 0, false, EntityDescriptor.group(), String.class, EntityDescriptor.user(), "root"));
            assertThat(rootGroups, containsInAnyOrder("wheel", "users"));

            List<String> userGroups = getImplementation().searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("user").returningAtMost(10));
            assertEquals(ImmutableList.of("users"), userGroups);
        }

        public void testGettingGroupMembershipsForUnknownUserGivesEmptyIterable() throws Exception
        {
            MembershipQuery<String> query = QueryBuilder.createMembershipQuery(-1, 0, false, EntityDescriptor.group(), String.class, EntityDescriptor.user(), "no-such-user");

            List<String> groups = getImplementation().searchGroupRelationships(query);
            assertThat(groups, emptyIterable());
        }

        public void testGettingGroupMembershipsForGroupGivesEmptyIterableBecauseNoNestedGroups() throws Exception
        {
            MembershipQuery<String> query = QueryBuilder.createMembershipQuery(-1, 0, true, EntityDescriptor.group(), String.class, EntityDescriptor.group(), "users");

            List<String> groups = getImplementation().searchGroupRelationships(query);
            assertThat(groups, emptyIterable());
        }

        public void testGettingMembersOfGroupReturnsResults() throws Exception
        {
            MembershipQuery<String> queryMembersOfUsersGroup = QueryBuilder.createMembershipQuery(-1, 0, true, EntityDescriptor.user(), String.class, EntityDescriptor.group(), "users");

            List<String> usersUsers = getImplementation().searchGroupRelationships(queryMembersOfUsersGroup);
            assertThat(usersUsers, containsInAnyOrder("user", "root"));

            MembershipQuery<String> queryMembersOfWheelGroup = QueryBuilder.createMembershipQuery(-1, 0, true, EntityDescriptor.user(), String.class, EntityDescriptor.group(), "wheel");

            List<String> wheelUsers = getImplementation().searchGroupRelationships(queryMembersOfWheelGroup);
            assertThat(wheelUsers, containsInAnyOrder("root"));
        }
    }
}
