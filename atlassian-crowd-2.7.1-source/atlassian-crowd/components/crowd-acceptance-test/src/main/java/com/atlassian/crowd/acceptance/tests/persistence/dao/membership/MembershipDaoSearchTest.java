package com.atlassian.crowd.acceptance.tests.persistence.dao.membership;

import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.dao.membership.MembershipDAOHibernate;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.model.DirectoryEntity;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.Groups;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.Users;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static com.atlassian.crowd.search.EntityDescriptor.group;
import static com.atlassian.crowd.search.EntityDescriptor.role;
import static com.atlassian.crowd.search.EntityDescriptor.user;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsEmptyCollection.*;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;

/**
 * Integration tests for the search operations of {@link MembershipDao}. For convenience, the tests
 * of this interface have been split into multiple test classes.
 *
 * @see MembershipDaoCRUDTest
 * @see MembershipDAOHibernateTest
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class MembershipDaoSearchTest
{
    private static final long DIRECTORY_ID = 1L;

    @Inject private MembershipDao membershipDAO;
    @Inject private DataSource dataSource;

    private static final String ADMINISTRATORS_GROUP_MIXED = "Administrators";
    private static final String ADMINISTRATORS_GROUP_ACTUAL = "administratorS";
    private static final String USERS_ROLE = "users";
    private static final String DEVELOPERS_GROUP = "developers";
    private static final String ADMIN_USER = "aDmin";
    private static final String PRODUCT_MANAGERS_ROLE = "Product Managers";

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Test
    public void testSearchForUserMembersOfGroup()
    {
        final MembershipQuery query = QueryBuilder.queryFor(User.class, user()).childrenOf(group()).withName(ADMINISTRATORS_GROUP_MIXED).startingAt(0).returningAtMost(10);

        final List<InternalUser> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(Users.namesOf(results), contains(toLowerCase(ADMIN_USER)));
    }

    @Test
    public void testSearchForUserMembersOfGroupAsNames()
    {
        final MembershipQuery query = QueryBuilder.queryFor(String.class, user()).childrenOf(group()).withName(ADMINISTRATORS_GROUP_MIXED).startingAt(0).returningAtMost(10);

        final List<String> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(results, contains(toLowerCase(ADMIN_USER)));
    }

    @Test
    public void testSearchForGroupMembershipsOfUser()
    {
        final MembershipQuery query = QueryBuilder.queryFor(Group.class, group()).parentsOf(user()).withName(ADMIN_USER).startingAt(0).returningAtMost(10);

        final List<InternalGroup> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(Groups.namesOf(results), containsInAnyOrder(ADMINISTRATORS_GROUP_ACTUAL, DEVELOPERS_GROUP));
    }

    @Test
    public void testSearchForGroupMembershipsOfUserAsNames()
    {
        final MembershipQuery query = QueryBuilder.queryFor(String.class, group()).parentsOf(user()).withName(ADMIN_USER).startingAt(0).returningAtMost(10);

        final List<String> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(results, containsInAnyOrder(ADMINISTRATORS_GROUP_ACTUAL, DEVELOPERS_GROUP));
    }

    @Test
    public void testSearchForGroupMembershipsOfGroup()
    {
        final MembershipQuery query = QueryBuilder.queryFor(Group.class, role()).parentsOf(role()).withName(PRODUCT_MANAGERS_ROLE).startingAt(0).returningAtMost(10);

        final List<InternalGroup> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(Groups.namesOf(results), contains(USERS_ROLE));
    }

    @Test
    public void testSearchForGroupMembershipsOfGroupAsNames()
    {
        final MembershipQuery query = QueryBuilder.queryFor(String.class, role()).parentsOf(role()).withName(PRODUCT_MANAGERS_ROLE).startingAt(0).returningAtMost(10);

        final List<String> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(results, contains(USERS_ROLE));
    }

    @Test
    public void testSearchForGroupMembersOfGroup()
    {
        final MembershipQuery query = QueryBuilder.queryFor(Group.class, role()).childrenOf(role()).withName(USERS_ROLE).startingAt(0).returningAtMost(10);

        final List<InternalGroup> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(Groups.namesOf(results), contains(PRODUCT_MANAGERS_ROLE));
    }

    @Test
    public void testSearchForGroupMembersOfGroupAsNames()
    {
        final MembershipQuery query = QueryBuilder.queryFor(String.class, role()).childrenOf(role()).withName(USERS_ROLE).startingAt(0).returningAtMost(10);

        final List<String> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(results, contains(PRODUCT_MANAGERS_ROLE));
    }

    //////////

    @Test
    public void testSearchForUserMembersOfGroupOfTypeRole()
    {
        final MembershipQuery query = QueryBuilder.queryFor(User.class, user()).childrenOf(group(GroupType.LEGACY_ROLE)).withName(ADMINISTRATORS_GROUP_MIXED).startingAt(0).returningAtMost(10);

        final List<InternalUser> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(results, emptyCollectionOf(InternalUser.class));
    }

    @Test
    public void testSearchForUserMembersOfGroupOfTypeRoleAsNames()
    {
        final MembershipQuery query = QueryBuilder.queryFor(String.class, user()).childrenOf(group(GroupType.LEGACY_ROLE)).withName(ADMINISTRATORS_GROUP_MIXED).startingAt(0).returningAtMost(10);

        final List<String> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(results, emptyCollectionOf(String.class));
    }

    @Test
    public void testSearchForGroupMembershipsOfUserOfTypeRole()
    {
        final MembershipQuery query = QueryBuilder.queryFor(Group.class, group(GroupType.LEGACY_ROLE)).parentsOf(user()).withName(ADMIN_USER).startingAt(0).returningAtMost(10);

        final List<InternalGroup> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(Groups.namesOf(results), contains(USERS_ROLE));
    }

    @Test
    public void testSearchForGroupMembershipsOfUserOfTypeRoleAsNames()
    {
        final MembershipQuery query = QueryBuilder.queryFor(String.class, group(GroupType.LEGACY_ROLE)).parentsOf(user()).withName(ADMIN_USER).startingAt(0).returningAtMost(10);

        final List<String> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(results, contains(USERS_ROLE));
    }

    @Test
    public void testSearchForGroupMembershipsOfGroupOfTypeRole()
    {
        final MembershipQuery query = QueryBuilder.queryFor(Group.class, group(GroupType.LEGACY_ROLE)).parentsOf(group(
            GroupType.LEGACY_ROLE)).withName(DEVELOPERS_GROUP).startingAt(0).returningAtMost(10);

        final List<InternalGroup> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(results, emptyCollectionOf(InternalGroup.class));
    }

    @Test
    public void testSearchForGroupMembershipsOfGroupOfTypeRoleAsNames()
    {
        final MembershipQuery query = QueryBuilder.queryFor(String.class, group(GroupType.LEGACY_ROLE)).parentsOf(group(GroupType.LEGACY_ROLE)).withName(DEVELOPERS_GROUP).startingAt(0).returningAtMost(10);

        final List<InternalGroup> results = membershipDAO.search(DIRECTORY_ID, query);

        assertThat(results, emptyCollectionOf(InternalGroup.class));
    }

    @Test (expected = IllegalArgumentException.class)
    public void testSearchForGroupMembersfGroupOfMismatchingTypesAsNames()
    {
        final MembershipQuery query = QueryBuilder.queryFor(String.class, group(GroupType.LEGACY_ROLE)).childrenOf(group(GroupType.GROUP)).withName(USERS_ROLE).startingAt(0).returningAtMost(10);

        membershipDAO.search(DIRECTORY_ID, query);
    }
}
