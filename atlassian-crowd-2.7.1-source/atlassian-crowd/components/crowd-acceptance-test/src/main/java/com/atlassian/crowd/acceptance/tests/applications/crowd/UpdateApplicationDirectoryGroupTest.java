package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.util.Arrays;

public class UpdateApplicationDirectoryGroupTest extends CrowdAcceptanceTestCase
{
    private static final String APP_CROWD = "crowd";

    private static final String CROWD_ADMINISTRATORS = "crowd-administrators";
    private static final String GROUPA = "groupA";
    private static final String GROUPB = "groupB";
    private static final String GROUPC = "groupC";

    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreBaseSetup();
        restoreCrowdFromXML("updateapplication_directorygroup.xml");

        // Group memberships
        // crowd-administrators: admin, groupA
        // groupA: user1
        // groupB: user1, user2
        // groupC: user1, user2, admin

        // groupmapping for allowAllToAuthenticate is TRUE
        // all groups have admin access
    }

    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    // With directory mapping allowAllToAuthenticate as TRUE

    public void testRemoveGroupsAllowAllAuthenticateTrue()
    {
        log("Running: testRemoveGroupsAllowAllAuthenticateTrue");

        gotoViewApplication(APP_CROWD);

        assertTextInElement("application-name", APP_CROWD);

        clickLink("application-groups");
        

        // 1 of 2 admin groups (should allow removal)
        clickLink("remove-" + CROWD_ADMINISTRATORS);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  CROWD_ADMINISTRATORS);

        // non-admin group (from user 'admin' perspective)
        clickLink("remove-" + GROUPA);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPA);

        // last admin group - should NOT allow removal
        clickLink("remove-" + GROUPC);
        assertKeyPresent("preventlockout.unassigngroup.label", Arrays.asList(GROUPC));
        assertMatchInTable("groupsTable",  GROUPC);

        // non-admin group (from user 'admin' perspective)
        clickLink("remove-" + GROUPB);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPB);
    }

    public void testRemoveNestedGroupLastAllowAllAuthenticateTrue()
    {
        log("Running: testNestedRemoveGroupLastAllowAllAuthenticateTrue");

        // login as 'user1' who is member of groupA (subgroup of crowd-administrators)
        _loginAsUser("user1", "password");

        gotoViewApplication(APP_CROWD);

        assertTextInElement("application-name", APP_CROWD);

        clickLink("application-groups");

        // one of many admin groups
        clickLink("remove-" + GROUPC);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPC);

        // one of many admin groups
        clickLink("remove-" + GROUPB);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPB);


        // parent group of groupA - can be removed as long groupA is in the groupmapping
        clickLink("remove-" + CROWD_ADMINISTRATORS);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  CROWD_ADMINISTRATORS);


        // last admin group - should NOT allow removal
        clickLink("remove-" + GROUPA);
        assertKeyPresent("preventlockout.unassigngroup.label", Arrays.asList(GROUPA));
        assertMatchInTable("groupsTable",  GROUPA);

        // add crowd-administrators back in...otherwise setup dies! (since admin/admin can't login)
        clickButton("add-group"); // crowd-administrators is listed as first group
    }

    public void testRemoveNestedGroupFirstAllowAllAuthenticateTrue()
    {
        log("Running: testNestedRemoveGroupFirstAllowAllAuthenticateTrue");

        // login as 'user1' who is member of groupA (subgroup of crowd-administrators)
        _loginAsUser("user1", "password");

        gotoViewApplication(APP_CROWD);

        assertTextInElement("application-name", APP_CROWD);

        clickLink("application-groups");

        // one of many admin groups
        clickLink("remove-" + GROUPC);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPC);

        // one of many admin groups
        clickLink("remove-" + GROUPB);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPB);

        // subgroup of crowd-administrators, so can still be removed (user1 will be an indirect member of crowd-administrators)
        clickLink("remove-" + GROUPA);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPA);

        // parent group of groupA - can NOT be removed since groupA was removed
        clickLink("remove-" + CROWD_ADMINISTRATORS);
        assertKeyPresent("preventlockout.unassigngroup.label", Arrays.asList(CROWD_ADMINISTRATORS));
        assertMatchInTable("groupsTable",  CROWD_ADMINISTRATORS);
    }


    // With directory mapping allowAllToAuthenticate as FALSE

    public void testRemoveGroupsAllowAllAuthenticateFalse()
    {
        log("Running: testRemoveGroupsAllowAllAuthenticateFalse");

        gotoViewApplication(APP_CROWD);

        assertTextInElement("application-name", APP_CROWD);

        setAllowAllToAuthenticateToFalse();

        clickLink("application-groups");

        // 1 of 2 admin groups (should allow removal)
        clickLink("remove-" + CROWD_ADMINISTRATORS);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  CROWD_ADMINISTRATORS);

        // non-admin group (from user 'admin' perspective)
        clickLink("remove-" + GROUPA);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPA);

        // last admin group - should NOT allow removal
        clickLink("remove-" + GROUPC);
        assertKeyPresent("preventlockout.unassigngroup.label", Arrays.asList(GROUPC));
        assertMatchInTable("groupsTable",  GROUPC);

        // non-admin group (from user 'admin' perspective)
        clickLink("remove-" + GROUPB);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPB);
    }

    public void testRemoveNestedGroupLastAllowAllAuthenticateFalse()
    {
        log("Running: testNestedRemoveGroupLastAllowAllAuthenticateFalse");

        // login as 'user1' who is member of groupA (subgroup of crowd-administrators)
        _loginAsUser("user1", "password");

        gotoViewApplication(APP_CROWD);

        assertTextInElement("application-name", APP_CROWD);

        setAllowAllToAuthenticateToFalse();

        clickLink("application-groups");

        // one of many admin groups
        clickLink("remove-" + GROUPC);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPC);

        // one of many admin groups
        clickLink("remove-" + GROUPB);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPB);

        // parent group of groupA - can be removed as long groupA is in the groupmapping
        clickLink("remove-" + CROWD_ADMINISTRATORS);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  CROWD_ADMINISTRATORS);

        // last admin group - should NOT allow removal
        clickLink("remove-" + GROUPA);
        assertKeyPresent("preventlockout.unassigngroup.label", Arrays.asList(GROUPA));
        assertMatchInTable("groupsTable",  GROUPA);

        // add crowd-administrators back in...otherwise setup dies! (since admin/admin can't login)
        clickButton("add-group"); // crowd-administrators is listed as first group
    }

    public void testRemoveNestedGroupFirstAllowAllAuthenticateFalse()
    {
        log("Running: testNestedRemoveGroupFirstAllowAllAuthenticateFalse");

        // login as 'user1' who is member of groupA (subgroup of crowd-administrators)
        _loginAsUser("user1", "password");

        gotoViewApplication(APP_CROWD);

        assertTextInElement("application-name", APP_CROWD);

        setAllowAllToAuthenticateToFalse();

        clickLink("application-groups");

        // one of many admin groups
        clickLink("remove-" + GROUPC);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPC);

        // one of many admin groups
        clickLink("remove-" + GROUPB);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPB);

        // subgroup of crowd-administrators, so can still be removed (user1 will be an indirect member of crowd-administrators)
        clickLink("remove-" + GROUPA);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPA);

        // parent group of groupA - can NOT be removed since groupA was removed
        clickLink("remove-" + CROWD_ADMINISTRATORS);
        assertKeyPresent("preventlockout.unassigngroup.label", Arrays.asList(CROWD_ADMINISTRATORS));
        assertMatchInTable("groupsTable",  CROWD_ADMINISTRATORS);
    }

    // Check can remove all admin groups from other applications
    public void testRemoveGroupsFromDemoAllowAllAuthenticateTrue()
    {
        log("Running: testRemoveGroupsAllowAllAuthenticateTrue");

        gotoViewApplication("demo");

        assertTextInElement("application-name", "demo");

        clickLink("application-groups");

        // Should be able to remove ALL groups since it is not Crowd Console
        clickLink("remove-" + CROWD_ADMINISTRATORS);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  CROWD_ADMINISTRATORS);

        clickLink("remove-" + GROUPA);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPA);

        clickLink("remove-" + GROUPC);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPC);

        clickLink("remove-" + GROUPB);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPB);
    }

    public void testRemoveGroupsFromDemoAllowAllAuthenticateFalse()
    {
        log("Running: testRemoveGroupsAllowAllAuthenticateFalse");

        gotoViewApplication("demo");

        assertTextInElement("application-name", "demo");

        setAllowAllToAuthenticateToFalse();

        clickLink("application-groups");

        // Should be able to remove ALL groups since it is not Crowd Console
        clickLink("remove-" + CROWD_ADMINISTRATORS);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  CROWD_ADMINISTRATORS);

        clickLink("remove-" + GROUPA);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPA);

        clickLink("remove-" + GROUPC);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPC);

        clickLink("remove-" + GROUPB);
        assertKeyPresent("updatesuccessful.label");
        assertNoMatchInTable("groupsTable",  GROUPB);
    }

    private void setAllowAllToAuthenticateToFalse()
    {
        clickLink("application-directories");
        selectOptionByValue("directory1-allowAll", "false");
        submit();
        assertKeyPresent("updatesuccessful.label");
    }
}
