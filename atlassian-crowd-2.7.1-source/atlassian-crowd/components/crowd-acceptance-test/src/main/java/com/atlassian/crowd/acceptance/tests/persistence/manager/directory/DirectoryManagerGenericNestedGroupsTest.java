package com.atlassian.crowd.acceptance.tests.persistence.manager.directory;

import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidMembershipException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Tests the directory manager against a HSQL-backed InternalDirectory
 * for the nested groups logic.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml",
    "classpath:/applicationContext-CrowdManagers.xml",
    "classpath:/applicationContext-CrowdUtils.xml",
    "classpath:/applicationContext-internaldirectoryloader-config.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class DirectoryManagerGenericNestedGroupsTest
{
    // directory id of the internal directory
    private static final long DIRECTORY_ID = 1;

    private static final String groupName0 = "group-0";
    private static final String groupName1 = "group-1";
    private static final String groupName2 = "group-2";
    private static final String groupName3 = "group-3";
    private static final String groupName4 = "group-4";
    private static final String groupName5 = "group-5";

    private static final String userNameA = "user-a";
    private static final String userNameB = "user-b";
    private static final String userNameC = "user-c";
    private static final String userNameD = "user-d";
    private static final String userNameE = "user-e";
    private static final String userNameF = "user-f";
    private static final String userNameG = "user-g";

    protected final String firstName = "First";
    protected final String surName = "Last";
    protected final String password = "secret-password";

    @Inject private DirectoryManager directoryManager;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void onSetUp() throws Exception
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);

        addUser(userNameA);
        addUser(userNameB);
        addUser(userNameC);
        addUser(userNameD);
        addUser(userNameE);
        addUser(userNameF);
        addUser(userNameG); // memberof: no groups

        addGroup(groupName0); // members: groupA
        addGroup(groupName1); // members: userF, userA, group2                 memberof: group3
        addGroup(groupName2); // members: userF, userB, group3                 memberof: group1
        addGroup(groupName3); // members: userF, userC, userD, group1, group4  memberof: group2
        addGroup(groupName4); // members: userE, group5                        memberof: group3
        addGroup(groupName5); // members: userF                                memberof: group4

        directoryManager.addUserToGroup(DIRECTORY_ID, userNameA, groupName1);
        directoryManager.addUserToGroup(DIRECTORY_ID, userNameB, groupName2);
        directoryManager.addUserToGroup(DIRECTORY_ID, userNameC, groupName3);
        directoryManager.addUserToGroup(DIRECTORY_ID, userNameD, groupName3);
        directoryManager.addUserToGroup(DIRECTORY_ID, userNameE, groupName4);
        directoryManager.addUserToGroup(DIRECTORY_ID, userNameF, groupName1);
        directoryManager.addUserToGroup(DIRECTORY_ID, userNameF, groupName2);
        directoryManager.addUserToGroup(DIRECTORY_ID, userNameF, groupName3);
        directoryManager.addUserToGroup(DIRECTORY_ID, userNameF, groupName5);

        directoryManager.addGroupToGroup(DIRECTORY_ID, groupName1, groupName0);
        directoryManager.addGroupToGroup(DIRECTORY_ID, groupName2, groupName1);
        directoryManager.addGroupToGroup(DIRECTORY_ID, groupName3, groupName2);
        directoryManager.addGroupToGroup(DIRECTORY_ID, groupName1, groupName3);
        directoryManager.addGroupToGroup(DIRECTORY_ID, groupName4, groupName3);
        directoryManager.addGroupToGroup(DIRECTORY_ID, groupName5, groupName4);
    }

    protected void addGroup(String name) throws Exception
    {
        GroupTemplate group = new GroupTemplate(name, DIRECTORY_ID, GroupType.GROUP);
        directoryManager.addGroup(DIRECTORY_ID, group);
    }

    protected void addUser(String name) throws Exception
    {
        UserTemplate user = new UserTemplate(name, DIRECTORY_ID);
        user.setFirstName("Bob");
        user.setLastName("Smith");
        user.setDisplayName("Bob Smith");
        user.setEmailAddress("bsmith@example.com");
        directoryManager.addUser(DIRECTORY_ID, user, new PasswordCredential("password"));
    }

    @Test
    public void testFindDirectoryById() throws Exception
    {
        Directory directory = directoryManager.findDirectoryById(DIRECTORY_ID);

        assertEquals("directory1", directory.getName());
        assertEquals(DirectoryType.INTERNAL, directory.getType());
    }

    @Test
    public void testSearchDirectUserMembersOfGroup() throws Exception
    {
        QueryBuilder.PartialMembershipQueryWithEntityToMatch<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group());

        List<String> names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName0).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, IsEmptyCollection.emptyCollectionOf(String.class));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName1).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(userNameA, userNameF));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName2).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(userNameB, userNameF));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName3).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(userNameC, userNameD, userNameF));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName4).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(userNameE));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName5).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(userNameF));
    }

    @Test
    public void testSearchDirectGroupMembersOfGroup() throws Exception
    {
        QueryBuilder.PartialMembershipQueryWithEntityToMatch<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group());

        List<String> names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName0).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName1));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName1).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName2));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName2).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName3));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName3).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName1, groupName4));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName4).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName5));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName5).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, IsEmptyCollection.emptyCollectionOf(String.class));
    }

    @Test
    public void testSearchDirectGroupMembershipsOfUser() throws Exception
    {
        QueryBuilder.PartialMembershipQueryWithEntityToMatch<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(
            EntityDescriptor.user());

        List<String> names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(userNameA).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName1));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(userNameB).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName2));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(userNameC).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName3));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(userNameD).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName3));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(userNameE).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName4));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(userNameF).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName1, groupName2, groupName3, groupName5));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(userNameG).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, IsEmptyCollection.emptyCollectionOf(String.class));
    }

    @Test
    public void testSearchDirectGroupMembershipsOfGroup() throws Exception
    {
        QueryBuilder.PartialMembershipQueryWithEntityToMatch<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.group());

        List<String> names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName0).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, IsEmptyCollection.emptyCollectionOf(String.class));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName1).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName0, groupName3));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName2).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName1));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName3).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName2));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName4).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName3));

        names = directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, query.withName(groupName5).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName4));
    }

    @Test
    public void testSearchNestedGroupMembersOfGroup() throws Exception
    {
        QueryBuilder.PartialMembershipQueryWithEntityToMatch<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group());

        List<String> names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID,
                                                                             query.withName(groupName0)
                                                                                 .returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName1, groupName2, groupName3, groupName4, groupName5));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID,
                                                                query.withName(groupName1)
                                                                    .returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName2, groupName3, groupName4, groupName5));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID,
                                                                query.withName(groupName2)
                                                                    .returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName1, groupName3, groupName4, groupName5));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID,
                                                                query.withName(groupName3)
                                                                    .returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName1, groupName2, groupName4, groupName5));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID,
                                                                query.withName(groupName4)
                                                                    .returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(groupName5));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID,
                                                                query.withName(groupName5)
                                                                    .returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, IsEmptyCollection.emptyCollectionOf(String.class));
    }

    @Test
    public void testSearchNestedUserMembersOfGroup() throws Exception
    {
        QueryBuilder.PartialMembershipQueryWithEntityToMatch<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group());

        List<String> names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(groupName0).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(userNameA, userNameB, userNameC, userNameD, userNameE, userNameF));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(groupName1).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(userNameA, userNameB, userNameC, userNameD, userNameE, userNameF));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(groupName2).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(userNameA, userNameB, userNameC, userNameD, userNameE, userNameF));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(groupName3).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(userNameA, userNameB, userNameC, userNameD, userNameE, userNameF));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(groupName4).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(userNameE, userNameF));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(groupName5).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, contains(userNameF));
    }

    @Test
    public void testSearchNestedGroupMembershipsOfGroup() throws Exception
    {
        QueryBuilder.PartialMembershipQueryWithEntityToMatch<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(
            EntityDescriptor.group());

        List<String> names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(groupName0).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, IsEmptyCollection.emptyCollectionOf(String.class));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(groupName1).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName0, groupName3, groupName2));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(groupName2).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName0, groupName1, groupName3));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(groupName3).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName0, groupName1, groupName2));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(groupName4).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName0, groupName1, groupName2, groupName3));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(groupName5).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName0, groupName1, groupName2, groupName3, groupName4));
    }

    @Test
    public void testSearchNestedGroupMembershipsOfUser() throws Exception
    {
        QueryBuilder.PartialMembershipQueryWithEntityToMatch<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user());

        List<String> names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(userNameA).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName0, groupName1, groupName2, groupName3));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(userNameB).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName0, groupName1, groupName2, groupName3));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(userNameC).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName0, groupName1, groupName2, groupName3));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(userNameD).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName0, groupName1, groupName2, groupName3));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(userNameE).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName0, groupName1, groupName2, groupName3, groupName4));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(userNameF).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, containsInAnyOrder(groupName0, groupName1, groupName2, groupName3, groupName4, groupName5));

        names = directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, query.withName(userNameG).returningAtMost(EntityQuery.ALL_RESULTS));
        assertThat(names, IsEmptyCollection.emptyCollectionOf(String.class));
    }

    @Test
    public void testIsUserDirectGroupMember() throws Exception
    {
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameA, groupName0));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameB, groupName0));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameC, groupName0));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameD, groupName0));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameE, groupName0));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameF, groupName0));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameG, groupName0));

        assertTrue(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameA, groupName1));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameB, groupName1));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameC, groupName1));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameD, groupName1));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameE, groupName1));
        assertTrue(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameF, groupName1));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameG, groupName1));

        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameA, groupName2));
        assertTrue(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameB, groupName2));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameC, groupName2));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameD, groupName2));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameE, groupName2));
        assertTrue(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameF, groupName2));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameG, groupName2));

        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameA, groupName3));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameB, groupName3));
        assertTrue(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameC, groupName3));
        assertTrue(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameD, groupName3));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameE, groupName3));
        assertTrue(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameF, groupName3));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameG, groupName3));

        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameA, groupName4));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameB, groupName4));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameC, groupName4));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameD, groupName4));
        assertTrue(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameE, groupName4));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameF, groupName4));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameG, groupName4));

        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameA, groupName5));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameB, groupName5));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameC, groupName5));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameD, groupName5));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameE, groupName5));
        assertTrue(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameF, groupName5));
        assertFalse(directoryManager.isUserDirectGroupMember(DIRECTORY_ID, userNameG, groupName5));
    }

    @Test
    public void testIsUserNestedGroupMember() throws Exception
    {
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameA, groupName0));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameB, groupName0));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameC, groupName0));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameD, groupName0));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameE, groupName0));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameF, groupName0));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameG, groupName0));

        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameA, groupName1));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameB, groupName1));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameC, groupName1));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameD, groupName1));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameE, groupName1));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameF, groupName1));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameG, groupName1));

        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameA, groupName2));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameB, groupName2));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameC, groupName2));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameD, groupName2));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameE, groupName2));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameF, groupName2));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameG, groupName2));

        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameA, groupName3));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameB, groupName3));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameC, groupName3));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameD, groupName3));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameE, groupName3));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameF, groupName3));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameG, groupName3));

        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameA, groupName4));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameB, groupName4));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameC, groupName4));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameD, groupName4));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameE, groupName4));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameF, groupName4));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameG, groupName4));

        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameA, groupName5));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameB, groupName5));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameC, groupName5));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameD, groupName5));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameE, groupName5));
        assertTrue(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameF, groupName5));
        assertFalse(directoryManager.isUserNestedGroupMember(DIRECTORY_ID, userNameG, groupName5));
    }

    @Test
    public void testIsGroupDirectGroupMember() throws Exception
    {
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName0, groupName0));
        assertTrue(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName1, groupName0));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName2, groupName0));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName3, groupName0));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName4, groupName0));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName5, groupName0));

        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName0, groupName1));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName1, groupName1));
        assertTrue(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName2, groupName1));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName3, groupName1));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName4, groupName1));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName5, groupName1));

        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName0, groupName2));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName1, groupName2));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName2, groupName2));
        assertTrue(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName3, groupName2));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName4, groupName2));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName5, groupName2));

        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName0, groupName3));
        assertTrue(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName1, groupName3));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName2, groupName3));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName3, groupName3));
        assertTrue(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName4, groupName3));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName5, groupName3));

        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName0, groupName4));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName1, groupName4));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName2, groupName4));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName3, groupName4));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName4, groupName4));
        assertTrue(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName5, groupName4));

        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName0, groupName5));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName1, groupName5));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName2, groupName5));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName3, groupName5));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName4, groupName5));
        assertFalse(directoryManager.isGroupDirectGroupMember(DIRECTORY_ID, groupName5, groupName5));
    }

    @Test
    public void testIsGroupNestedGroupMember() throws Exception
    {
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName0, groupName0));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName1, groupName0));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName2, groupName0));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName3, groupName0));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName4, groupName0));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName5, groupName0));

        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName0, groupName1));
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName1, groupName1));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName2, groupName1));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName3, groupName1));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName4, groupName1));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName5, groupName1));

        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName0, groupName2));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName1, groupName2));
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName2, groupName2));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName3, groupName2));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName4, groupName2));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName5, groupName2));

        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName0, groupName3));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName1, groupName3));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName2, groupName3));
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName3, groupName3));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName4, groupName3));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName5, groupName3));

        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName0, groupName4));
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName1, groupName4));
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName2, groupName4));
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName3, groupName4));
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName4, groupName4));
        assertTrue(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName5, groupName4));

        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName0, groupName5));
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName1, groupName5));
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName2, groupName5));
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName3, groupName5));
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName4, groupName5));
        assertFalse(directoryManager.isGroupNestedGroupMember(DIRECTORY_ID, groupName5, groupName5));
    }

    @Test (expected = MembershipAlreadyExistsException.class)
    public void testAddGroupToGroupWhenMembershipAlreadyExists() throws Exception
    {
        directoryManager.addGroupToGroup(DIRECTORY_ID, groupName1, groupName0);
    }

    @Test (expected = InvalidMembershipException.class)
    public void testAddGroupToGroupCycleCreationFail() throws Exception
    {
        directoryManager.addGroupToGroup(DIRECTORY_ID, groupName0, groupName0);
    }

    @Test (expected = InvalidMembershipException.class)
    public void testRemoveGroupFromGroupCycleDeletionFail() throws Exception
    {
        directoryManager.removeGroupFromGroup(DIRECTORY_ID, groupName0, groupName0);
    }

    @Test
    public void testRemoveGroupFromGroup() throws Exception
    {
        assertEquals(2, directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(groupName3).returningAtMost(EntityQuery.ALL_RESULTS)).size());
        assertEquals(4, directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(groupName3).returningAtMost(EntityQuery.ALL_RESULTS)).size());

        directoryManager.removeGroupFromGroup(DIRECTORY_ID, groupName1, groupName3);

        assertEquals(1, directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(groupName3).returningAtMost(EntityQuery.ALL_RESULTS)).size());
        assertEquals(2, directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(groupName3).returningAtMost(EntityQuery.ALL_RESULTS)).size());
    }

    public void setDirectoryManager(final DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }

    public void setDataSource(DataSource dataSource)
    {
        this.dataSource = dataSource;
    }

    public void setResetableHiLoGeneratorHelper(ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper)
    {
        this.resetableHiLoGeneratorHelper = resetableHiLoGeneratorHelper;
    }
}
