package com.atlassian.crowd.acceptance.utils;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.directory.MicrosoftActiveDirectory;
import com.atlassian.crowd.directory.OpenLDAP;

import com.google.common.collect.ImmutableList;

import junit.framework.AssertionFailedError;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.Properties;

/**
 * Contains constants and helper methods that are shared between DbCachingLoad tests
 * The LDAP server properties will be determined by the property file provided in the system property: tpm.loadtest.file
 */
public abstract class AbstractDbCachingLoadTest extends CrowdAcceptanceTestCase
{
    // The default properties files are set for ou=loadTesting10k values.
    // Specify the property file (-Dtpm.loadtest.file=crowd-tpm-loadtest10k.properties) to load the correct directory attributes.
    // Active Directory: crowd-ad1-loadtest10k.properties, crowd-tpm-loadtest10k.properties
    // OpenLDAP: crowd-openldap-loadtest10k.properties

    // If you want to test on a different sized (eg. ou=loadTest3k), change the property files accordingly
    // (Note: the text logged probably won't match but for quick/local testing shouldn't be an issue)

    // Property file for the LDAP server
    private static final String PROPERTY_FILE = System.getProperty("tpm.loadtest.file", "crowd-ad1-loadtest10k.properties");
    private static final Properties SERVER_PROPERTIES = DirectoryTestHelper.getConfigProperties(PROPERTY_FILE);

    // Change these sub OU names if you are testing a different loadTesting ou
    public static final String CHILD_OU_A = SERVER_PROPERTIES.getProperty("test.directory.childOUA");
    public static final String CHILD_OU_B = SERVER_PROPERTIES.getProperty("test.directory.childOUB");
    public static final String CHILD_OU_C = SERVER_PROPERTIES.getProperty("test.directory.childOUC");
    public static final String CHILD_OU_D = SERVER_PROPERTIES.getProperty("test.directory.childOUD");

    // Change the properties file if you want to test a different loadTesting ou
    public static final String CONNECTOR_BASEDN = SERVER_PROPERTIES.getProperty("test.directory.basedn");
    public static final String CONNECTOR_URL = SERVER_PROPERTIES.getProperty("test.directory.url");
    public static final String CONNECTOR_SECURE = SERVER_PROPERTIES.getProperty("test.directory.secure");
    public static final String CONNECTOR_REFERRAL = SERVER_PROPERTIES.getProperty("test.directory.referral");
    public static final String CONNECTOR_PAGEDRESULTS = SERVER_PROPERTIES.getProperty("test.directory.pagedresults.use");
    public static final String CONNECTOR_PAGEDRESULTSSIZE = SERVER_PROPERTIES.getProperty("test.directory.pagedresults.size");
    public static final String CONNECTOR_USERDN = SERVER_PROPERTIES.getProperty("test.directory.userdn");
    public static final String CONNECTOR_USERPW = SERVER_PROPERTIES.getProperty("test.directory.password");
    public static final String CONNECTOR_CLASSNAME = SERVER_PROPERTIES.getProperty("test.directory.classname");
    public static final String CONNECTOR_NESTEDGROUPS = SERVER_PROPERTIES.getProperty("test.directory.nested.groups.disabled");


    public static final String CONNECTOR_DIRECTORY_NAME = "Test Connector Directory";
    public static final int ONE_MINUTE_IN_MILLIS = 1000 * 60;

    // Change these if you are testing a different OU - group/user names are slightly different for each OU; number of memberships are also different
    public static final String PARTIAL_GROUPNAME = SERVER_PROPERTIES.getProperty("test.directory.group.name");
    public static final String PARTIAL_USERNAME = SERVER_PROPERTIES.getProperty("test.directory.user.name");
    public static final int MEMBERSHIPS_COUNT = NumberUtils.toInt(SERVER_PROPERTIES.getProperty("test.directory.membership.size"));

    private DbCachingTestHelper dbCachingTestHelper;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        this.dbCachingTestHelper = new DbCachingTestHelper(tester);
    }

    /**
     * Manually starts the sync.
     *
     * @param shortDescription A short description of what is being sync-ed (eg. 10k users) to be printed once we believe the sync has finished.
     * @return The duration of the sync process in seconds.
     * @throws InterruptedException
     */
    public long synchroniseDirectory(String shortDescription) throws InterruptedException
    {
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME);

        // Only manually sync if "Synchronise Started" is not visible (if it is visible then the sync has already kicked off)
        // ...and then wait till it finishes
        if (!isSynchronising())
        {
            clickButton("synchroniseDirectoryButton");
        }

        waitForExistingSyncToFinish();

        // Sync finished - go fetch the sync duration
        String syncDuration = getElementTextById("duration");
        logger.info("Synchronisation took: " + syncDuration + " (" + shortDescription + ")");

        return parsePrettyDurationToSeconds(syncDuration);
    }

    protected static long parsePrettyDurationToSeconds(String duration)
    {
        if (StringUtils.isBlank(duration))
        {
            return 0;
        }

        long seconds = 0 ;
        String[] components = StringUtils.split(duration, ',');
        for(String component:components)
        {
            component = component.trim();
            String[] parts = StringUtils.split(component, ' ');
            long quantity = Long.parseLong(parts[0]);

            if (parts[1].startsWith("second"))
            {
                seconds += quantity;
            }
            else if (parts[1].startsWith("minute"))
            {
                seconds += (60 * quantity);
            }
            else if (parts[1].startsWith("hour"))
            {
                seconds += (60 * 60 * quantity);
            }
            else if (parts[1].startsWith("day"))
            {
                seconds += (24 * 60 * 60 * quantity);
            }
            else
            {
                throw new IllegalArgumentException("unsupported time unit:" + parts[1]);
            }
        }

        return seconds;
    }

    public void waitForExistingSyncToFinish() throws InterruptedException
    {
        gotoBrowseDirectories();
        // wait for 10 seconds to ensure if a sync was started because of an update (new poller), we will still wait.
        Thread.sleep(ONE_MINUTE_IN_MILLIS / 6);
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME);

        logger.info("Waiting for an existing sync to finish");
        while (isSynchronising())
        {
            Thread.sleep(ONE_MINUTE_IN_MILLIS / 2); // wait for 30 seconds
        }
        logger.info("Existing sync should have finished, proceed with tests.");
    }

    public void createLoadTestingDirectory(String url, String baseDN, String userDN, String password)
    {
        gotoCreateDirectory();

        clickButton("create-connector");
        assertKeyPresent("directoryconnectorcreate.title");
        setWorkingForm("directoryconnector");

        // Details Tab
        setTextField("name", CONNECTOR_DIRECTORY_NAME);
        setTextField("description", "");

        // Connector Tab -  set the relevant fields
        clickLinkWithExactText("Connector");
        selectOption("connector", getConnectorType());
        if (Boolean.parseBoolean(CONNECTOR_SECURE))
        {
            checkCheckbox("secure"); // we want to be able to mutate
        }
        setTextField("URL", url);
        setTextField("baseDN", baseDN);
        setTextField("userDN", userDN);
        setTextField("readTimeoutInSec", "0"); // no read or search timeout
        setTextField("searchTimeoutInSec", "0");
        setTextField("pollingIntervalInMin", "10000000000000"); // give it "forever" till it auto polls
        setTextField("ldapPassword", password);
        submit();

        // Quickly check directory created properly
        assertKeyPresent("menu.viewdirectory.label", ImmutableList.of(CONNECTOR_DIRECTORY_NAME));
        setWorkingForm("updateGeneral");
        assertTextFieldEquals("name", CONNECTOR_DIRECTORY_NAME);
        assertTextFieldEquals("directoryDescription", "");
        assertTextPresent(getConnectorType());
        clickLink("connector-connectiondetails");
        assertTextFieldEquals("URL", url);
        assertTextFieldEquals("baseDN", baseDN);
        assertTextFieldEquals("userDN", userDN);
    }

    /**
     * Returns <tt>true</tt> if the directory is currently being synchronised.
     *
     * @return <tt>true</tt> if the directory is currently being synchronised
     */
    protected boolean isSynchronising()
    {
        return dbCachingTestHelper.isSynchronising();
    }

    protected String getConnectorType()
    {
        if (CONNECTOR_CLASSNAME.equals(MicrosoftActiveDirectory.class.getName()))
        {
            return MicrosoftActiveDirectory.getStaticDirectoryType();
        }

        if (CONNECTOR_CLASSNAME.equals(OpenLDAP.class.getName()))
        {
            return OpenLDAP.getStaticDirectoryType();
        }

        throw new AssertionFailedError("Invalid connector type provided in properties file. Could not select appropriate connector type.");
    }

    public void assertUsersAndGroupsFromOUPresent(String childOU)
    {
        // Check users
        searchUsers(childOU);
        // Check for a few users (they are listed alphabetically)
        assertTextPresent(createUserName(childOU, 0));
        assertTextPresent(createUserName(childOU, 1));
        assertTextPresent(createUserName(childOU, 10));

        // Check groups
        searchGroups(childOU);
        // Check for a few users (they are listed alphabetically)
        assertTextPresent(createGroupName(childOU, 0));
        assertTextPresent(createGroupName(childOU, 1));
        assertTextPresent(createGroupName(childOU, 10)); // This will fail if you are testing with loadTesting500 (not enough groups)
    }

    public void assertUsersAndGroupsFromOUNotPresent(String childOU)
    {
        // Check users
        searchUsers(childOU);
        // Check for a few users (they are listed alphabetically)
        assertTextNotPresent(createUserName(childOU, 0));
        assertTextNotPresent(createUserName(childOU, 1));
        assertTextNotPresent(createUserName(childOU, 10));

        // Check groups
        searchGroups(childOU);
        // Check for a few users (they are listed alphabetically)
        assertTextNotPresent(createGroupName(childOU, 0));
        assertTextNotPresent(createGroupName(childOU, 1));
        assertTextNotPresent(createGroupName(childOU, 10)); // This will fail if you are testing with loadTesting500 (not enough groups)
    }

    private void searchGroups(String childOU)
    {
        gotoBrowseGroups();
        setWorkingForm("browsegroups");
        setTextField("name", childOU + "-group");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME);
        submit();
    }

    private void searchUsers(String childOU)
    {
        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        setTextField("search", childOU + "Test");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME);
        submit();
    }

    public String createUserName(String childOU, int index)
    {
        return childOU + PARTIAL_USERNAME + index;
    }

    public String createGroupName(String childOU, int index)
    {
        return childOU + PARTIAL_GROUPNAME + index;
    }
}
