package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.net.URL;

abstract class AppImporterTestBase extends CrowdAcceptanceTestCase
{

    private String jdbcURL;

    abstract String getHsqlFileName();

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);

        restoreBaseSetup();

        String dbLocation = null;
        File fileSystemResource = AcceptanceTestHelper.getResource(getHsqlFileName() + ".properties");

        if (!fileSystemResource.exists())
        {
            URL resource = ClassLoaderUtils.getResource(getHsqlFileName() + ".properties", this.getClass());
            dbLocation = StringUtils.removeEnd(resource.toExternalForm(), ".properties");
        }
        else
        {
            dbLocation = StringUtils.removeEnd(fileSystemResource.getCanonicalPath(), ".properties");
        }

        jdbcURL = new StringBuilder().append("jdbc:hsqldb:").append(dbLocation).toString();

    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    protected void runImportTest(String appName, int expectedUserCount, int expectedGroupCount, int expectedMembershipCount)
    {
        gotoImporters();

        clickButton("importatlassian");

        assertKeyPresent("dataimport.importatlassian.title");

        selectOption("configuration.application", appName);

        selectOption("configuration.directoryID", "Test Internal Directory");

        setTextField("configuration.databaseURL", jdbcURL);

        setTextField("configuration.databaseDriver", "org.hsqldb.jdbcDriver");

        setTextField("configuration.username", "sa");

        checkCheckbox("configuration.importPasswords", "true");

        submit();

        assertKeyPresent("dataimport.atlassianimportsuccess.text");

        assertTextInElement("users-imported", Integer.toString(expectedUserCount));
        assertTextInElement("groups-imported", Integer.toString(expectedGroupCount));
        assertTextInElement("memberships-imported", Integer.toString(expectedMembershipCount));
    }
}
