package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.acceptance.utils.AbstractDbCachingLoadTest;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.integration.rest.service.RestCrowdClient;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestriction;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

/**
 * Tests that DbCaching directories can still function while a synchronisation is occurring. The synchronisation is
 * assumed to take a longer time (at the moment approx. 4 mins) than the tests takes to run, otherwise the test will
 * fail and an even bigger synchronisation task would have to be performed.
 */
public class DbCachingLoadAndOperateTest extends AbstractDbCachingLoadTest
{
    private static final String TEST_USERNAME = "testuser-LDAP";
    private static final String USER_DETAILS_TABLE_ID = "user-details";
    private static final String APPLICATION_NAME = "crowd";
    private static final String APPLICATION_PASSWORD = "Mqb7CAcR";

    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreBaseSetup();
        createLoadTestingDirectory(CONNECTOR_URL, CONNECTOR_BASEDN, CONNECTOR_USERDN, CONNECTOR_USERPW);
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    /**
     * Tests that Crowd still functions properly during a directory sync.  The operations chosen were those which might
     * have interfered with the current directory sync (adding/removing users from the directory being sync'ed), and
     * common operations that applications might perform (retrieving all the users of an application).
     */
    public void testOperationsDuringSync() throws Exception
    {
        intendToModifyData();

        log("Starting testOperationsDuringSync");
        synchroniseDirectoryWithoutWaiting();
        doAddUser();
        doRemoveUser();
        doLogoutLogin();
        doGetAllUsers();
        doRestoreCrowd();
        assertTrue("Failed to test performing operations while synchronising the directory. Synchronisation might have"
                + "finished already.", directoryIsStillSynchronising());
        waitForExistingSyncToFinish();
    }

    /**
     * Adds a user to the LDAP directory.
     */
    private void doAddUser()
    {
        log("Starting doAddUser()");
        gotoAddPrincipal();
        String username = TEST_USERNAME;

        // add principal
        setTextField("email", "testuser@atlassian.com");
        setTextField("name", username);
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME);

        submit();

        // search for the user
        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME);
        setTextField("search", TEST_USERNAME);

        submit("submit-search");
        assertTableRowCountEquals(USER_DETAILS_TABLE_ID, 2); // 1 header + 1 user
    }

    /**
     * Removes a user from the LDAP directory.
     */
    private void doRemoveUser()
    {
        log("Starting doRemoveUser()");
        gotoRemovePrincipal(TEST_USERNAME, CONNECTOR_DIRECTORY_NAME);

        submit();
        // search for the user
        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME);
        setTextField("search", TEST_USERNAME);

        submit("submit-search");
        assertTableRowCountEquals(USER_DETAILS_TABLE_ID, 1); // 1 header
    }

    /**
     * Retrieves all the users with a CrowdClient.
     */
    private void doGetAllUsers()
            throws ApplicationPermissionException, InvalidAuthenticationException, OperationFailedException
    {
        log("Starting doGetAllUsers");

        final Properties properties = new Properties();
        properties.setProperty(Constants.PROPERTIES_FILE_BASE_URL, HOST_PATH);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_NAME, APPLICATION_NAME);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_PASSWORD, APPLICATION_PASSWORD);

        final ClientProperties clientProperties = ClientPropertiesImpl.newInstanceFromProperties(properties);

        CrowdClient crowdClient = new RestCrowdClient(clientProperties);
        List<String> usernames = crowdClient.searchUserNames(new NullRestriction() {}, 0, EntityQuery.ALL_RESULTS);
        assertTrue(usernames.contains("admin"));
    }

    /**
     * Attempts to restore crowd from XML - should fail.
     */
    private void doRestoreCrowd()
    {
        log("Starting doRestoreCrowd");
        gotoRestore();
        submit();

        assertKeyPresent("administration.restore.directory.synchronising.error", Arrays.asList(AbstractDbCachingLoadTest.CONNECTOR_DIRECTORY_NAME));
        assertErrorPresentWithKey("administration.restore.filePath.error");
    }

    /**
     * Log out and log in again.
     */
    private void doLogoutLogin()
    {
        log("Starting doLogoutLogin");
        _logout();
        _loginAdminUser();
    }

    private boolean directoryIsStillSynchronising()
    {
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME);

        return isSynchronising();
    }

    /**
     * Will manually start the sync iff it DOES NOT see the "Sync Started" label.
     */
    private void synchroniseDirectoryWithoutWaiting()
    {
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME);

        if (!isSynchronising())
        {
            clickButton("synchroniseDirectoryButton");
        }
    }
}
