package com.atlassian.crowd.acceptance.tests.applications.crowd;

import org.apache.commons.httpclient.HttpStatus;

/**
 * Test class for testing the updating of a group
 */
public class ViewRoleTest extends CrowdAcceptanceTestCase
{
    private static final String CROWD_USERS_ROLE = "crowd-users";

    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("viewroletest.xml");
    }

    public void tearDown() throws Exception
    {
        //restoreBaseSetup();
        super.tearDown();
    }

    public void testViewRoleNoActionMapped()
    {
        try
        {
            tester.setIgnoreFailingStatusCodes(true);
            gotoPage("/console/secure/role/view.action?name=" + CROWD_USERS_ROLE + "&directoryName=" + "Atlassian");
        }
        finally
        {
            tester.setIgnoreFailingStatusCodes(false);
        }

        // No action mapped
        tester.assertResponseCode(HttpStatus.SC_INTERNAL_SERVER_ERROR);
    }
}
