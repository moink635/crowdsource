package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.crowd.manager.directory.SynchronisationMode;

public class SynchroniseCrowdDirectoryFullTest extends SynchroniseCrowdDirectoryTest
{
    public SynchroniseCrowdDirectoryFullTest()
    {
        super(SynchronisationMode.FULL);
    }

    @Override
    protected void _doInitialSync()
    {
        super._doInitialSync();

        disableIncrementalSync();
    }

    private void disableIncrementalSync()
    {
        gotoBrowseDirectories();
        clickLinkWithExactText(LOCAL_DIRECTORY_NAME);
        clickLink("crowd-connectiondetails");

        setWorkingForm("crowddetails");
        uncheckCheckbox("incrementalSyncEnabled");
        submit();
    }

    @Override
    public void testSynchroniseChanges_RemoveUniqueRemoteUserMembershipAndRecreateUniqueRemoteUserMembership()
    {
        // do nothing, this test is only relevant to incremental synchronisation
    }
}
