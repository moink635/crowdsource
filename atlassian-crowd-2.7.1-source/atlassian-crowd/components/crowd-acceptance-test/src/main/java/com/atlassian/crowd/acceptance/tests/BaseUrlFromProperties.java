package com.atlassian.crowd.acceptance.tests;

import java.util.Properties;

import com.atlassian.core.util.PropertyUtils;

import static org.junit.Assert.assertNotNull;

/**
 * Get a test application's base URL from a properties file, with overrides from system properties.
 */
public class BaseUrlFromProperties
{
    private final Properties specProperties;

    public BaseUrlFromProperties(Properties specProperties)
    {
        this.specProperties = specProperties;
    }

    public BaseUrlFromProperties()
    {
        this(new Properties());
    }

    public static BaseUrlFromProperties withLocalTestProperties()
    {
        Properties props = PropertyUtils.getProperties("localtest.properties", BaseUrlFromProperties.class);
        assertNotNull(props);
        return new BaseUrlFromProperties(props);
    }

    public String baseUrlFor(String applicationName)
    {
        String port = getTestProperty(applicationName + ".port");
        String context = getTestProperty(applicationName + ".context");
        String rootUrl = "http://" + getTestProperty("host.location") + ":" + port;
        return rootUrl + context;
    }

    private String getTestProperty(String propName)
    {
        if (System.getProperty("acceptance.test." + propName) != null)
        {
            return System.getProperty("acceptance.test." + propName);
        }
        else if (specProperties.getProperty(propName) != null)
        {
            return specProperties.getProperty(propName);
        }
        else
        {
            throw new IllegalStateException("No test property found for: " + propName);
        }
    }
}
