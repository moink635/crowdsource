package com.atlassian.crowd.acceptance.tests.persistence.dao.webhook;

import java.util.Date;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.dao.webhook.WebhookDAO;
import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.webhook.Webhook;
import com.atlassian.crowd.model.webhook.WebhookTemplate;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsEmptyIterable.emptyIterableOf;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Integration tests for the CRUD operations of {@link WebhookDAO}
 *
 * @since 2.7
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class WebhookDaoCRUDTest
{
    private static final long APPLICATION_ID = 3L; // from sample-data.xml
    private static final String ENDPOINT_URL1 = "http://example.test/1";
    private static final String ENDPOINT_URL2 = "http://example.test/2";
    private static final String TOKEN1 = "secret1";
    private static final String TOKEN2 = "secret2";

    @Inject private WebhookDAO dao;
    @Inject private ApplicationDAO applicationDao;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;
    @Inject private SessionFactory sessionFactory;

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Test
    public void testAddShouldAssignAnId() throws Exception
    {
        Application application = applicationDao.findById(APPLICATION_ID);
        Webhook createdWebhook = dao.add(new WebhookTemplate(application, ENDPOINT_URL1, TOKEN1));

        assertNotNull(createdWebhook.getId());
    }

    @Test
    public void testAddShouldAssignDifferentIdsToDifferentWebhooks() throws Exception
    {
        Application application = applicationDao.findById(APPLICATION_ID);
        Webhook createdWebhook1 = dao.add(new WebhookTemplate(application, ENDPOINT_URL1, TOKEN1));
        Webhook createdWebhook2 = dao.add(new WebhookTemplate(application, ENDPOINT_URL2, TOKEN2));

        assertNotEquals(createdWebhook1.getId(), createdWebhook2.getId());
    }

    @Test
    public void testAddShouldPersistTemplateFields() throws Exception
    {
        Application application = applicationDao.findById(APPLICATION_ID);
        WebhookTemplate webhookTemplate = new WebhookTemplate(application, ENDPOINT_URL1, TOKEN1);
        webhookTemplate.setFailuresSinceLastSuccess(20L);
        webhookTemplate.setOldestFailureDate(new Date(30L));
        Webhook createdWebhook = dao.add(webhookTemplate);

        assertSame(application, createdWebhook.getApplication());
        assertEquals(ENDPOINT_URL1, createdWebhook.getEndpointUrl());
        assertEquals(TOKEN1, createdWebhook.getToken());
        assertEquals(20L, createdWebhook.getFailuresSinceLastSuccess());
        assertEquals(new Date(30L), createdWebhook.getOldestFailureDate());
    }

    @Test
    public void testFindByIdWebhookJustAdded() throws Exception
    {
        Application application = applicationDao.findById(APPLICATION_ID);
        Webhook createdWebhook1 = dao.add(new WebhookTemplate(application, ENDPOINT_URL1, TOKEN1));
        Webhook createdWebhook2 = dao.add(new WebhookTemplate(application, ENDPOINT_URL2, TOKEN2));

        Webhook returnedWebhook1 = dao.findById(createdWebhook1.getId());
        Webhook returnedWebhook2 = dao.findById(createdWebhook2.getId());

        assertThat(returnedWebhook1, is(createdWebhook1));
        assertSame(application, returnedWebhook1.getApplication());
        assertEquals(ENDPOINT_URL1, returnedWebhook1.getEndpointUrl());
        assertEquals(TOKEN1, returnedWebhook1.getToken());

        assertThat(returnedWebhook2, is(createdWebhook2));
        assertSame(application, returnedWebhook2.getApplication());
        assertEquals(ENDPOINT_URL2, returnedWebhook2.getEndpointUrl());
        assertEquals(TOKEN2, returnedWebhook2.getToken());
    }

    @Test
    public void testFindAllWebhooksJustAdded() throws Exception
    {
        Application application = applicationDao.findById(APPLICATION_ID);
        Webhook createdWebhook = dao.add(new WebhookTemplate(application, ENDPOINT_URL1, TOKEN1));

        assertThat(dao.findAll(), hasItem(createdWebhook));
    }

    @Test (expected = WebhookNotFoundException.class)
    public void testFindByIdWebhookThatDoesNotExist() throws Exception
    {
        dao.findById(1L);
    }

    @Test
    public void testFindByIdWebhookJustRemoved() throws Exception
    {
        Application application = applicationDao.findById(APPLICATION_ID);
        Webhook createdWebhook = dao.add(new WebhookTemplate(application, ENDPOINT_URL1, null));

        dao.remove(createdWebhook);

        try
        {
            dao.findById(createdWebhook.getId());
            fail("WebhookNotFoundException expected");
        }
        catch (WebhookNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testRemovingApplicationRemovesWebhooksInCascade() throws Exception
    {
        Application application = applicationDao.findById(APPLICATION_ID);
        Webhook createdWebhook = dao.add(new WebhookTemplate(application, ENDPOINT_URL1, null));

        sessionFactory.getCurrentSession().flush(); // ensure Webhook is persisted to the database

        applicationDao.remove(application);

        // ensure Application is removed from the database table. If we have referential integrity but not
        // on-cascade deletions, this will fail:
        // java.sql.SQLException: Integrity constraint violation FK_WEBHOOK_APP table
        sessionFactory.getCurrentSession().flush();

        try
        {
            // the Webhook should have been deleted on cascade
            dao.findById(createdWebhook.getId());
            fail("WebhookNotFoundException expected");
        }
        catch (WebhookNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testFindByApplicationAndEndpointUrl() throws Exception
    {
        Application application = applicationDao.findById(APPLICATION_ID);
        Webhook createdWebhook = dao.add(new WebhookTemplate(application, ENDPOINT_URL1, null));

        Webhook foundWebhook = dao.findByApplicationAndEndpointUrl(application, ENDPOINT_URL1);

        assertSame(createdWebhook, foundWebhook);
    }

    @Test
    public void testFindByApplicationAndEndpointUrlWebhookThatDoesNotExist() throws Exception
    {
        Application application = applicationDao.findById(APPLICATION_ID);
        try
        {
            dao.findByApplicationAndEndpointUrl(application, ENDPOINT_URL1);
            fail("WebhookNotFoundException expected");
        }
        catch (WebhookNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testRemoveWebhook() throws Exception
    {
        Application application = applicationDao.findById(APPLICATION_ID);
        Webhook createdWebhook = dao.add(new WebhookTemplate(application, ENDPOINT_URL1, null));

        dao.remove(createdWebhook);

        assertThat(dao.findAll(), emptyIterableOf(Webhook.class));
    }

    @Test (expected = WebhookNotFoundException.class)
    public void testRemoveNonExistingWebhook() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getId()).thenReturn(1L);

        dao.remove(webhook);
    }

    @Test
    public void testUpdateExistingWebhook() throws Exception
    {
        Application application = applicationDao.findById(APPLICATION_ID);
        Webhook initialWebhook = dao.add(new WebhookTemplate(application, ENDPOINT_URL1, null));

        WebhookTemplate webhookTemplate = new WebhookTemplate(initialWebhook);
        webhookTemplate.setFailuresSinceLastSuccess(2);
        webhookTemplate.setOldestFailureDate(new Date(3));

        Webhook updatedWebhook = dao.update(webhookTemplate);

        assertEquals(2, updatedWebhook.getFailuresSinceLastSuccess());
        assertEquals(new Date(3), updatedWebhook.getOldestFailureDate());
    }

    @Test (expected = WebhookNotFoundException.class)
    public void testUpdateNonExistingWebhook() throws Exception
    {
        Webhook webhook = mock(Webhook.class);
        when(webhook.getId()).thenReturn(1L);

        dao.update(webhook);
    }
}
