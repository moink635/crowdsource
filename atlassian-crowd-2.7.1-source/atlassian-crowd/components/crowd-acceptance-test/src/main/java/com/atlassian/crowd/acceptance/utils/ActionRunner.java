package com.atlassian.crowd.acceptance.utils;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Execute an action, repeatedly, concurrently.
 */
public class ActionRunner implements Action
{
    private final int threads;
    private final int iterations;
    private final Action action;
    private int failures;

    public ActionRunner(final int threads, final int iterations, final Action action)
    {
        this.threads = threads;
        this.iterations = iterations;
        this.action = action;
        this.failures = 0;
    }

    public void execute()
    {
        List<ActionWorker> workers = new ArrayList<ActionWorker>(threads);
        List<Thread> executions = new ArrayList<Thread>(threads);

        for (int i = 0; i < threads; i++)
        {
            ActionWorker worker = new ActionWorker(action, iterations);
            workers.add(worker);

            Thread execution = new Thread(worker);
            executions.add(execution);

            execution.start();
        }

        for (Thread execution : executions)
        {
            try
            {
                execution.join();
            }
            catch (InterruptedException e)
            {
                break;
            }
        }

        for (ActionWorker worker : workers)
        {
            failures += worker.getFailures();
        }
    }

    public int getFailures()
    {
        return failures;
    }
}

class ActionWorker implements Runnable
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final Action action;
    private final int iterations;
    private int failures;

    public ActionWorker(final Action action, final int iterations)
    {
        this.action = action;
        this.iterations = iterations;
        this.failures = 0;
    }

    public void run()
    {
        for (int i = 0; i < iterations; i++)
        {
            logger.info("Running iteration: " + i);

            try
            {
                action.execute();
            }
            catch (Exception e)
            {
                logger.warn("Failed to execute action test", e);
                failures++;
            }
        }
    }

    public int getFailures()
    {
        return failures;
    }
}