package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.BasicHttpParams;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class CrowdXsrfProtectionTest extends CrowdAcceptanceTestCase
{
    private static final List<String> ACTIONS = Arrays.asList(
            // Applications
            "console/secure/application/addapplicationconfirmation!completeStep.action",
            "console/secure/application/update!update.action",
            "console/secure/application/updatedirectory!update.action",
            "console/secure/application/updatedirectory!addDirectory.action",
            "console/secure/application/updatedirectory!removeDirectory.action",
            "console/secure/application/updatedirectory!up.action",
            "console/secure/application/updatedirectory!down.action",
            "console/secure/application/updategroups!addGroup.action",
            "console/secure/application/updategroups!removeGroup.action",
            "console/secure/application/updatePermissions!doUpdate.action?tab=4",
            "console/secure/application/updateaddresses!addAddress.action?tab=5",
            "console/secure/application/updateaddresses!removeAddress.action?tab=5",
            "console/secure/application/updateoptional!updateOptional.action",
            "console/secure/application/remove!update.action",

            // Users
            "console/secure/user/add!update.action",
            "console/secure/user/update!updateGeneral.action",
            "console/secure/user/updateattributes!updateAttributes.action",
            "console/secure/user/updateattributes!addAttribute.action",
            "console/secure/user/updateattributes!removeAttribute.action",
            "console/secure/user/updategroups!addGroups.action",
            "console/secure/user/updategroups!removeGroups.action",
            "console/secure/user/updateroles!addRole.action",
            "console/secure/user/updateroles!removeRole.action",
            "console/secure/user/updatealiases!update.action",
            "console/secure/dataimport/importatlassian!import.action",
            "console/secure/dataimport/importjive!update.action",
            "console/secure/dataimport/importdirectory!importdirectory.action",
            "console/secure/dataimport/directoryconfirmation!doExecute.action",
            "console/secure/user/resetpassword!update.action",
            "console/secure/user/remove!update.action",

            // Groups
            "console/secure/group/add!update.action",
            "console/secure/group/update!updateGeneral.action",
            "console/secure/group/updatemembers!addUsers.action",
            "console/secure/group/updatemembers!removeUsers.action",
            "console/secure/group/updatemembers!addGroups.action",
            "console/secure/group/updatemembers!removeGroups.action",
            "console/secure/group/remove!update.action",

            // Roles
            "console/secure/role/add!update.action",
            "console/secure/role/update!updateGeneral.action",
            "console/secure/role/remove!update.action",

            // Directories

            "console/secure/directory/createinternal!update.action",
            "console/secure/directory/createdelegated!update.action",
            "console/secure/directory/createconnector!update.action",
            "console/secure/directory/createremotecrowd!update.action",
            "console/secure/directory/createcustom!update.action",

            "console/secure/directory/synchronisedirectorycache!default.action",

            "console/secure/directory/updateinternal.action",
            "console/secure/directory/updateinternalconfiguration!update.action",
            "console/secure/directory/updateinternalpermissions!update.action",
            "console/secure/directory/updateinternaloptions!addDefaultGroups.action",
            "console/secure/directory/updateinternaloptions!removeDefaultGroup.action",

            "console/secure/directory/updatedelegated.action",
            "console/secure/directory/updatedelegatedconnection!update.action",
            "console/secure/directory/updatedelegatedconfiguration!update.action",
            "console/secure/directory/updatedelegatedpermissions!update.action",
            "console/secure/directory/updatedelegatedoptions!addDefaultGroups.action",
            "console/secure/directory/updatedelegatedoptions!removeDefaultGroup.action",

            "console/secure/directory/updateconnector.action",
            "console/secure/directory/synchconnectorcache!default.action",
            "console/secure/directory/updateconnectorconnection!update.action",
            "console/secure/directory/updateconnectorconfiguration!update.action",
            "console/secure/directory/updateconnectorpermissions!update.action",
            "console/secure/directory/updateconnectoroptions!addDefaultGroups.action",
            "console/secure/directory/updateconnectoroptions!removeDefaultGroup.action",

            "console/secure/directory/updateremotecrowd.action",
            "console/secure/directory/syncremotecrowdcache!default.action",
            "console/secure/directory/updateremotecrowdconnection!update.action",
            "console/secure/directory/updateremotecrowdpermissions!update.action",
            "console/secure/directory/updateremotecrowdoptions!addDefaultGroups.action",
            "console/secure/directory/updateremotecrowdoptions!removeDefaultGroup.action",

            "console/secure/directory/updatecustom.action",
            "console/secure/directory/updatecustomattributes!addAttribute.action",
            "console/secure/directory/updatecustomattributes!update.action",
            "console/secure/directory/updatecustomattributes!removeAttribute.action",
            "console/secure/directory/updatecustompermissions!update.action",

            "console/secure/directory/remove!update.action",

            // Administration
            "console/secure/admin/general!update.action",
            "console/secure/admin/licensing!update.action",
            "console/secure/admin/mailserver!update.action",
            "console/secure/admin/mailtemplate!update.action",
            "console/secure/admin/sessionconfig!update.action",
            "console/secure/session/removeusersession!removePrincipalSession.action",
            "console/secure/session/removesessionapplication!removeApplicationSession.action",
            "console/secure/admin/updatetrustedproxies!addAddress.action",
            "console/secure/admin/updatetrustedproxies!removeAddress.action",
            "console/secure/admin/backup!export.action",
            "console/secure/admin/restore!import.action",
            "console/secure/admin/loggingProfiling!updateProfiling.action",
            "console/secure/admin/loggingProfiling!updateLogging.action",
            "console/secure/admin/connectionpool!update.action",

            // User Console
            "console/user/updateprofile.action",
            "console/user/changepassword.action",

            // Console
            "console/license!update.action",
            "console/resetpassword!update.action",
            "console/forgottenlogindetails!update.action"
    );

    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        _loginAdminUser();

        tester.setIgnoreFailingStatusCodes(true);
    }

    @Override
    public void tearDown() throws Exception
    {
        tester.setIgnoreFailingStatusCodes(false);

        super.tearDown();
    }

    public void testXsrfProtection()
    {
        final String tokenMissingMessage = getMessage("atlassian.xwork.xsrf.notoken");
        final Collection<String> unprotectedActions = new ArrayList<String>();

        for (String action : ACTIONS)
        {
            gotoPage(action);

            if (!getPageSource().contains("warningBox") || !isTextPresent(tokenMissingMessage))
            {
                unprotectedActions.add(action);
                log("Page source for '" + action + "':\n" + tester.getPageSource());
            }
        }

        assertTrue("The following actions are not XSRF protected: " + unprotectedActions, unprotectedActions.isEmpty());
    }

    public void testLoginXsrfProtection() throws Exception
    {
        HttpClient httpClient = new DefaultHttpClient();
        // spring login filter only works on a post, so do a post here even though we shouldn't get to the login stage
        HttpPost httpPost = new HttpPost(getBaseUrl() + "/console/j_security_check");
        httpPost.setParams(new BasicHttpParams().setParameter("j_username", "invalid").setParameter("j_password", "invalid"));

        String cookie;
        try
        {
            HttpResponse response = httpClient.execute(httpPost);
            assertThat(response.getStatusLine().getStatusCode(), equalTo(302)); //302 Found, aka redirect
            assertThat(response.getLastHeader("Location").getValue(), endsWith("/console/login.action?error=true"));

            cookie = response.getLastHeader("Set-Cookie").getValue();
            assertThat(cookie, containsString("JSESSIONID=")); // need the session cookie to get the CSRF error message
        }
        finally
        {
            httpPost.releaseConnection();
        }

        HttpGet httpGet = new HttpGet(getBaseUrl() + "/console/login.action?error=true");
        httpGet.setHeader(new BasicHeader("Cookie", cookie));

        try
        {
            HttpResponse response = httpClient.execute(httpGet);
            String responseContent = IOUtils.toString(response.getEntity().getContent());
            assertThat(responseContent, containsString(getMessage("atlassian.xwork.xsrf.notoken")));
        }
        finally
        {
            httpGet.releaseConnection();
        }
    }
}
