package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.integration.rest.service.RestCrowdClient;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import static com.google.common.collect.Iterables.transform;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalToIgnoringWhiteSpace;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class ExpireSessionTest extends CrowdAcceptanceTestCase
{
    private static final String APPLICATION_NAME = "demo";
    private static final String APPLICATION_PASSWORD = "password";
    private static final ImmutableList<String> APPLICATION_SESSION_TABLE_HEADERS =
        ImmutableList.of("Name", "Initialization", "Last Accessed", "Action");

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        restoreBaseSetup();
    }

    protected void switchToInMemorySessions()
    {
        gotoSessionConfig();
        clickRadioOption("storageType", "memory");
        submit();
    }

    public void testExpireApplicationInMemorySession()
    {
        log("Running testExpireApplicationInMemorySession");
        switchToInMemorySessions();
        doTestExpireApplication();
    }

    public void testExpirePrincipalInMemorySession()
    {
        log("Running testExpirePrincipalInMemorySession");
        switchToInMemorySessions();
        doTestExpirePrincipal();
    }

    public void testExpireApplicationDatabaseSession()
    {
        log("Running testExpireApplicationDatabaseSession");
        doTestExpireApplication();
    }

    public void testExpirePrincipalDatabaseSession()
    {
        log("Running testExpirePrincipalDatabaseSession");
        doTestExpirePrincipal();
    }

    public void testRestClientsAreListedInApplicationSessions() throws Exception
    {
        log("Running testRestClientsAreListedInApplicationSessions");

        // precondition: the application has not created a session yet
        gotoCurrentApplicationSessions();
        List<String> beforeSessions = scrapeTable("application-session-results",
                                                  APPLICATION_SESSION_TABLE_HEADERS,
                                            new Function<List<String>, String>(){
                                                @Override
                                                public String apply(List<String> input)
                                                {
                                                    return input.get(0);
                                                }
                                            });
        assertThat(beforeSessions, not(hasItem("demo")));

        getCrowdClient().getUser("admin"); // use the REST client, should create an application session

        gotoCurrentApplicationSessions();
        List<String> afterSessions = scrapeTable("application-session-results",
                                                 APPLICATION_SESSION_TABLE_HEADERS,
                                            new Function<List<String>, String>(){
                                                @Override
                                                public String apply(List<String> input)
                                                {
                                                    return input.get(0);
                                                }
                                            });
        assertThat(afterSessions, hasItem("demo"));
    }

    public void testRestClientApplicationSessionsCanBeExpired() throws Exception
    {
        log("Running testRestClientApplicationSessionsCanBeExpired");

        getCrowdClient().getUser("admin"); // use the REST client, should create an application session

        gotoCurrentApplicationSessions();
        List<String> afterSessions = scrapeTable("application-session-results",
                                                 APPLICATION_SESSION_TABLE_HEADERS,
                                                 new Function<List<String>, String>(){
                                                     @Override
                                                     public String apply(List<String> input)
                                                     {
                                                         return input.get(0);
                                                     }
                                                 });
        assertThat(afterSessions, hasItem(APPLICATION_NAME));

        // expire the session
        clickElementByXPath("//tr[.//a[normalize-space(.) = '" + APPLICATION_NAME + "']]//a[@title='Expire']");

        // the application session should not appear in the list anymore
        gotoCurrentApplicationSessions();
        List<String> beforeSessions = scrapeTable("application-session-results",
                                                  APPLICATION_SESSION_TABLE_HEADERS,
                                                  new Function<List<String>, String>(){
                                                      @Override
                                                      public String apply(List<String> input)
                                                      {
                                                          return input.get(0);
                                                      }
                                                  });
        assertThat(beforeSessions, not(hasItem(APPLICATION_NAME)));
    }

    protected void doTestExpireApplication()
    {
        gotoCurrentApplicationSessions();
        assertKeyPresent("session.application.title");

        assertLinkPresentWithExactText("crowd");
        clickLinkWithExactText("Expire");

        // crowd gets expired but reappears immediately
        assertLinkPresentWithExactText("crowd");
    }

    protected void doTestExpirePrincipal()
    {
        gotoCurrentPrincipalSessions();
        assertKeyPresent("session.principal.title");

        assertLinkPresentWithExactText("admin");
        clickLinkWithExactText("Expire");

        // expired own session
        assertKeyPresent("login.title");
    }

    private static Function<String, Date> parseDate = new Function<String, Date>() {
        @Override
        public Date apply(@Nullable String input)
        {
            // Assume local timezone
            DateFormat df = new SimpleDateFormat("dd/M/yyyy HH:mm:ss");

            try
            {
                return df.parse(input);
            }
            catch (ParseException e)
            {
                throw new RuntimeException(e);
            }
        }
    };

    private static Matcher<Date> inLastFiveMinutes(long now)
    {
        return Matchers.<Date>both(Matchers.greaterThan(new Date(now - TimeUnit.MINUTES.toMillis(5)))).and(
                Matchers.lessThan(new Date(now)));
    }

    public void testApplicationSessionsShowLastAccessTime()
    {
        gotoCurrentApplicationSessions();
        assertKeyPresent("session.application.title");

        List<String> sessions = scrapeTable("application-session-results",
                                            APPLICATION_SESSION_TABLE_HEADERS,
                new Function<List<String>, String>(){
                    @Override
                    public String apply(List<String> input)
                    {
                        return input.get(2);
                    }
        });

        assertThat(sessions, not(empty()));
        assertThat(sessions, everyItem(Matchers.not(equalToIgnoringWhiteSpace(""))));

        assertThat(transform(sessions, parseDate),
                everyItem(inLastFiveMinutes(System.currentTimeMillis())));

    }

    public void testUserSessionsShowLastAccessTime()
    {
        gotoCurrentPrincipalSessions();
        assertKeyPresent("session.principal.title");

        List<String> sessions = scrapeTable("principal-session-results",
                ImmutableList.of("Username", "Directory", "Initialization", "Last Accessed", "Action"),
                new Function<List<String>, String>(){
                    @Override
                    public String apply(List<String> input)
                    {
                        return input.get(3);
                    }
        });

        assertThat(sessions, not(empty()));
        assertThat(sessions, everyItem(Matchers.not(equalToIgnoringWhiteSpace(""))));

        assertThat(Iterables.transform(sessions, parseDate),
                everyItem(inLastFiveMinutes(System.currentTimeMillis())));
    }

    private CrowdClient getCrowdClient()
    {
        final Properties properties = new Properties();
        properties.setProperty(Constants.PROPERTIES_FILE_BASE_URL, HOST_PATH);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_NAME, APPLICATION_NAME);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_PASSWORD, APPLICATION_PASSWORD);

        final ClientProperties clientProperties = ClientPropertiesImpl.newInstanceFromProperties(properties);
        return new RestCrowdClient(clientProperties);
    }
}
