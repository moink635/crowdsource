package com.atlassian.crowd.acceptance.tests.applications.crowd;

public class BrowsePrincipalSessionsTest extends CrowdAcceptanceTestCase
{
    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        restoreBaseSetup();
    }

    public void testSearchForPrincipalSession()
    {
        gotoCurrentPrincipalSessions();

        setWorkingForm("browse-principals");

        setTextField("name", "admin");

        selectOption("selectedDirectoryID", "Test Internal Directory");

        submit();

        assertTextInTable("principal-session-results", "admin");
    }

    public void testSearchForPrincipalSessionThatDoesntExist()
    {
        gotoCurrentPrincipalSessions();

        setWorkingForm("browse-principals");

        setTextField("name", "bobby");

        selectOption("selectedDirectoryID", "Test Internal Directory");

        submit();

        assertTextNotInTable("principal-session-results", "bobby");
    }
}
