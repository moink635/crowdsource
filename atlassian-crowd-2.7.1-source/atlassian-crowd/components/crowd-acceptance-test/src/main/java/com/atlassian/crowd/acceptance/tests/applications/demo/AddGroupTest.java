package com.atlassian.crowd.acceptance.tests.applications.demo;

import com.atlassian.crowd.acceptance.tests.directory.BaseTest;
import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.exception.DirectoryInstantiationException;

import java.util.Properties;


/**
 * Because viewgrouptest.xml has both Internal and LDAP directories mapped, this will test adding/updating/removing
 * for both major types.
 */
public class AddGroupTest extends DemoAcceptanceTestCase
{
    private static final String TEST_GROUP_NAME = "test-group";
    private static final String TEST_GROUP_DESCRIPTION = "Test Description";

    LDAPLoader loader = new LDAPLoader();

    /**
     * Allows us to remove groups from our ApacheDS instance, bypassing the UI.
     */
    class LDAPLoader extends BaseTest
    {
        LDAPLoader()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
        }

        @Override
        protected void configureDirectory(Properties directorySettings)
        {
            super.configureDirectory(directorySettings);
            directory.setAttribute(LDAPPropertiesMapper.LDAP_BASEDN_KEY, directorySettings.getProperty("test.integration.basedn"));
        }

        @Override
        protected void loadTestData() throws Exception
        {
        }

        @Override
        protected void removeTestData() throws DirectoryInstantiationException
        {
            DirectoryTestHelper.silentlyRemoveGroup(TEST_GROUP_NAME, getRemoteDirectory());
        }

        public void accessibleSetUp() throws Exception
        {
            super.setUp();
        }

        public void accessibleTearDown() throws Exception
        {
            super.tearDown();
        }
    }



    public void setUp() throws Exception
    {
        super.setUp();
        loader.accessibleSetUp();

        restoreCrowdFromXML("viewgrouptest.xml");

        _loginAdminUser();
    }

    public void tearDown() throws Exception
    {
        loader.accessibleTearDown();
        super.tearDown();
    }

    protected void addGroup()
    {
        intendToModifyData();
        gotoPage("/secure/group/browsegroups.action");

        clickLink("add-group");

        setTextField("name", TEST_GROUP_NAME);
        setTextField("groupDescription", TEST_GROUP_DESCRIPTION);
        checkCheckbox("active");

        submit();
    }

    public void testAddGroup()
    {
        intendToModifyData();
        log("Running testAddGroup");

        addGroup();

        assertKeyPresent("menu.viewgroup.label", TEST_GROUP_NAME);

        assertTextFieldEquals("groupDescription", TEST_GROUP_DESCRIPTION);
        assertCheckboxSelected("active");
    }

    public void testUpdateGroup()
    {
        intendToModifyData();
        log("Running testUpdateGroup");

        gotoPage("/secure/group/browsegroups.action");

        clickLink("view-group-crowd-administrators");

        setTextField("groupDescription", TEST_GROUP_DESCRIPTION);

        submit();

        assertTextFieldEquals("groupDescription", TEST_GROUP_DESCRIPTION);
    }

    public void testRemoveGroup()
    {
        intendToModifyData();
        log("Running testRemoveGroup");

        addGroup();

        assertKeyPresent("menu.viewgroup.label", TEST_GROUP_NAME);
        assertTextPresent(TEST_GROUP_NAME);

        clickLink("remove-group");

        submit();

        assertKeyPresent("browser.group.title");
        assertTextNotPresent(TEST_GROUP_NAME);
    }
}
