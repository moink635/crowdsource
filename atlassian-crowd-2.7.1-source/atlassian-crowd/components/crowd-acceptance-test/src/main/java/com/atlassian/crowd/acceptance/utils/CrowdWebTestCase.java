package com.atlassian.crowd.acceptance.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;

import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.sourceforge.jwebunit.html.Cell;
import net.sourceforge.jwebunit.html.Row;

import junit.framework.AssertionFailedError;
import net.sourceforge.jwebunit.junit.WebTestCase;
import net.sourceforge.jwebunit.junit.WebTester;

/**
 * Base Crowd test case, with extra assertions on top of {@link WebTestCase}.
 */
public class CrowdWebTestCase extends WebTestCase
{
    private static final Logger logger = LoggerFactory.getLogger(CrowdWebTestCase.class);

    /**
     * Assert that a web resource's value is present.
     *
     * @param key web resource name
     */
    public void assertKeyPresent(String key)
    {
        try
        {
            super.assertKeyPresent(key);
        } catch (junit.framework.AssertionFailedError error)
        {
            System.err.println("Unable to find key " + key + "(text: \"" + getMessage(key) + "\") in page: ");
            System.err.print(getPageSource());
            throw error;
        }
    }

    /**
     * Assert that a web resource's value is present.
     *
     * @param key  web resource name
     * @param args
     */
    public void assertKeyPresent(String key, List args)
    {
        try
        {
            assertKeyPresent(key, args.toArray());
        } catch (junit.framework.AssertionFailedError error)
        {
            System.err.print(tester.getPageSource());
            throw error;
        }
    }

    public void assertKeyPresent(String key, String... args)
    {
        try
        {
            super.assertKeyPresent(key, args);
        } catch (junit.framework.AssertionFailedError error)
        {
            System.err.print(tester.getPageSource());
            throw error;
        }
    }

    /**
     * Assert that supplied text is present.
     *
     * @param text
     */
    public void assertTextPresent(String text, boolean suppressMessage)
    {
        try
        {
            assertTextPresent(text);
        } catch (junit.framework.AssertionFailedError error)
        {
            if (!suppressMessage)
            {
                System.err.println("Unable to find " + text + " in:");
                System.err.print(tester.getPageSource());
            }
            throw error;
        }
    }

    /**
     * Makes sure the page contains a success message
     */
    public void assertSuccessPresent()
    {
        // success messages are wrapped in <p class="success">...</p>
        assertElementPresentByXPath("//p[@class='success']");
    }

    /**
     * Makes sure the page doesn't contain a warning, as when an exception has occurred.
     */
    public void assertWarningNotPresent()
    {
        // warnings are wrapped in <p class="warningBox"> ... </p>
        if (getPageSource().indexOf("noteBox") > 0)
        {
            System.err.print(getPageSource());
            Assert.fail("Warning present in page!");
        }
    }

    /**
     * Makes sure the page does not contain an error, as when an exception has occurred.
     */
    public void assertErrorNotPresent()
    {
        // errors are wrapped in <p class="errorBox"> ... </p>
        if (getPageSource().indexOf("warningBox") > 0 || getPageSource().indexOf("errorBox") > 0)
        {
            System.err.print(getPageSource());
            Assert.fail("Error present in page!");
        }
    }

    /**
     * {@see assertWarningNotPresent}
     * {@see assertErrorNotPresent}
     */
    public void assertWarningAndErrorNotPresent()
    {
        assertErrorNotPresent();
        assertWarningNotPresent();
    }

    /**
     * Makes sure the page does contain an error, as when an exception has occurred.
     */
    public void assertErrorPresent()
    {
        // errors are wrapped in <p class="errorBox"> ... </p>
        if (getPageSource().indexOf("errorBox") < 0)
        {
            System.err.print(getPageSource());
            Assert.fail("Error not present in page!");
        }
    }

    /**
     * Makes sure the page does contain an error and that it also contains the text of the key. Note: it does not (yet)
     * verify that the text is within the error.
     *
     * @param keyName
     */
    public void assertErrorPresentWithKey(String keyName)
    {
        assertErrorPresent();
        assertKeyPresent(keyName);
    }

    /**
     * Makes sure the page does contain a warning
     */
    public void assertWarningPresent()
    {
        // errors are wrapped in <p class="warningBox"> ... </p>
        if (getPageSource().indexOf("warningBox") < 0)
        {
            System.err.print(getPageSource());
            Assert.fail("Warning not present in page!");
        }
    }

    /**
     * Assert that a link containing the text of the key is present.
     *
     * @param keyName The key whose associated text will be searched for.
     */
    public void assertLinkPresentWithKey(String keyName)
    {
        assertLinkPresentWithText(getMessage(keyName));
    }

    /**
     * Assert that a given element contains a specific web resource's value.
     *
     * @param elementID id of element to be inspected
     * @param key       the web resource's key
     */
    public void assertKeyInElement(String elementID, String key)
    {
        assertTextInElement(elementID, getMessage(key));
    }

    /**
     * Sets a specific radio button.
     *
     * @param radioButtonName The name of the radio button.
     * @param value           The value to set it too.
     */
    public void setRadioButton(String radioButtonName, String value)
    {
        try
        {
            clickRadioOption(radioButtonName, value);
        } catch (AssertionFailedError e)
        {
            System.err.print(getPageSource());
            throw e;
        }
    }

    /**
     * Clicks a link
     *
     * @param key The value of this key is the link clicked
     */
    public void clickLinkWithKey(String key)
    {
        clickLinkWithText(getMessage(key));
    }

    public String getElementTextById(String elementId)
    {
        try
        {
            return getElementById(elementId).getTextContent();
        } catch (AssertionFailedError e)
        {
            System.err.print(getPageSource());
            throw e;
        }
    }

    /**
     * Adds a request header for all subsequent requests.
     *
     * @param header header paramater.
     * @param value  parameter value.
     */
    public void addRequestHeader(String header, String value)
    {
        getTestContext().addRequestHeader(header, value);
    }

    /**
     * Ensures the server's response (HTTP header + body)
     * contains a string.
     *
     * @param string string to search for.
     */
    public void assertServerResponseContains(String string)
    {
        if (!getServerResponse().contains(string))
        {
            System.err.print(getServerResponse());
            throw new AssertionError("Server response did not contain: " + string);
        }
    }

    /**
     * Ensures the server's response (HTTP header + body)
     * dos not contain a string.
     *
     * @param string string to search for.
     */
    public void assertServerResponseDoesNotContain(String string)
    {
        if (getServerResponse().contains(string))
        {
            System.err.print(getServerResponse());
            throw new AssertionError("Server response contains: " + string);
        }
    }

    /**
     * Required so tester can be manually set externally when required.
     *
     * @param tester new tester.
     */
    public void setTester(WebTester tester)
    {
        this.tester = tester;
    }

    protected void log(String message)
    {
        logger.info(message);
    }

    protected String getPageText()
    {
        return getTestingEngine().getPageText();
    }

    /**
     * Waits for the specified key to appear on the page.
     *
     * @param text
     * @param interval
     * @param maxInterval
     * @throws Exception
     */
    public void waitForText(String text, long interval, long maxInterval) throws Exception
    {
        maxInterval = maxInterval <= 0 ? Integer.MAX_VALUE : maxInterval;

        // initial check
        try
        {
            assertTextPresent(text, true);
            return;
        } catch (Throwable throwable)
        {
        }

        //loop
        long timeElapsed = TimeUnit.SECONDS.toMillis(10);
        for (; interval + timeElapsed <= maxInterval; timeElapsed += interval)
        {
            Thread.sleep(interval);
            try
            {
                assertTextPresent(text, true);
                return;
            } catch (Throwable throwable)
            {
                //text not present, keep looping
            }
        }
        Assert.fail("Unable to find text [" + text + "] on page within " + maxInterval + "ms. \n\nDumping page source: \n"
                + getPageSource());
    }

    public void waitForElementByXPath(final String xpath) throws Exception
    {
        Assert.assertTrue("Unable to find element using xpath [" + xpath + "] on page within time limit. \n\nDumping page source: \n" + getPageSource(),
                waitFor(new Callable<Boolean>()
                {
                    public Boolean call() throws Exception
                    {
                        return getTestingEngine().hasElementByXPath(xpath);
                    }
                }));
    }

    public void waitForElementById(final String id) throws Exception
    {
        Assert.assertTrue("Unable to find element using id [" + id + "] on page within time limit. \n\nDumping page source: \n" + getPageSource(),
                waitFor(new Callable<Boolean>()
                {
                    public Boolean call() throws Exception
                    {
                        return getTestingEngine().hasElement(id);
                    }
                }));
    }

    protected boolean waitFor(Callable<Boolean> isPresent) throws Exception
    {
        final long interval = TimeUnit.SECONDS.toMillis(1);
        final long maxInterval = TimeUnit.SECONDS.toMillis(10);

        for (int timeElapsed = 0; timeElapsed < maxInterval; timeElapsed += interval)
        {
            if (isPresent.call().booleanValue())
            {
                return true;
            }
            Thread.sleep(interval);
        }

        return false;
    }

    /**
     * Checks if text is present on the page
     *
     * @param text the text to match for
     * @return true if the text is found. false otherwise
     */
    public boolean isTextPresent(String text)
    {
        try
        {
            assertTextPresent(text, true);
            return true;
        } catch (AssertionFailedError e)
        {
            // error was thrown, so text was not found - ie. not present
            return false;
        }
    }

    public void waitForText(String text) throws Exception
    {
        waitForText(text, TimeUnit.SECONDS.toMillis(2), TimeUnit.SECONDS.toMillis(120));
    }

    /**
     * Checks that the text of a given key only occurs once on the page, such as validation error messages
     *
     * @param key the key to check occurrences of
     */
    public void assertKeyPresentOnce(String key)
    {
        String pageText = getPageText();

        assertKeyPresent(key);

        assertEquals("More than one occurrence of key: " + key, pageText.indexOf(getMessage(key)),
                pageText.lastIndexOf(getMessage(key)));

    }

    protected <T> List<T> scrapeTable(String id, Function<List<String>, T> mapper)
    {
        return scrapeTable(id, null, mapper);
    }

    protected <T> List<T> scrapeTable(String id, List<String> headings, Function<List<String>, T> mapper)
    {
        assertTablePresent(id);

        List<T> results = new ArrayList<T>();
        List<Row> rows = getTable(id).getRows();

        if (headings != null)
        {
            Row header = rows.get(0);
            assertEquals(headings.size(), header.getCells().size());
            for (int i = 0; i < headings.size(); i++)
            {
                assertEquals(headings.get(i), ((Cell) header.getCells().get(i)).getValue());
            }

            rows = rows.subList(1, rows.size());
        }

        for (Row r : rows)
        {
            List<Cell> cells = r.getCells();

            Builder<String> b = ImmutableList.builder();

            if (headings != null)
            {
                assertEquals(headings.size(), cells.size());
            }

            for (Cell c : cells)
            {
                b.add(c.getValue());
            }

            results.add(mapper.apply(b.build()));
        }

        return results;
    }
}
