package com.atlassian.crowd.acceptance.tests.applications.crowd.user;

public class ChangePasswordTest extends CrowdUserConsoleAcceptenceTestCase
{
    private static final String INTERNAL_DIRECTORY_NAME = "Test Internal Directory";

    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        loadXmlOnSetUp("userconsoletest.xml");
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testViewChangePasswordWithoutPolicy()
    {
        log("Running testViewChangePasswordWithoutPolicy");

        _loginTestUser();

        gotoPage("/console/user/viewchangepassword.action");

        assertKeyPresent("menu.user.console.changepassword.label");
        assertKeyNotPresent("principal.password.complexity.policy");
        assertWarningAndErrorNotPresent();

        assertTextFieldEquals("originalPassword", "");
        assertTextFieldEquals("password", "");
        assertTextFieldEquals("confirmPassword", "");
    }

    public void testViewChangePasswordWithPolicy()
    {
        log("Running testViewChangePassword");

        intendToModifyData();

        updateDirectoryWithPasswordRegex("[a-z]", "Passwords <b>must</b> contain at least one lowercase character");

        _loginTestUser();

        gotoPage("/console/user/viewchangepassword.action");

        assertKeyPresent("menu.user.console.changepassword.label");
        assertKeyPresent("principal.password.complexity.policy");
        assertTextPresent("Passwords <b>must</b> contain at least one lowercase character");
        assertWarningAndErrorNotPresent();

        assertTextFieldEquals("originalPassword", "");
        assertTextFieldEquals("password", "");
        assertTextFieldEquals("confirmPassword", "");
    }

    public void testChangePasswordOldPasswordBlank()
    {
        log("Running testChangePasswordOldPasswordBlank");

        _loginTestUser();

        gotoPage("/console/user/viewchangepassword.action");

        assertKeyPresent("menu.user.console.changepassword.label");
        submit();

        assertKeyPresent("menu.user.console.changepassword.label");
        assertKeyPresent("password.invalid");
    }

    public void testChangePasswordNewPasswordBlank()
    {
        log("Running testChangePasswordNewPasswordBlank");

        _loginTestUser();

        gotoPage("/console/user/viewchangepassword.action");

        assertKeyPresent("menu.user.console.changepassword.label");

        setTextField("originalPassword", TEST_USER_PW);
        submit();

        assertKeyPresent("menu.user.console.changepassword.label");
        assertKeyPresent("passwordempty.invalid");
    }

    private void updateDirectoryWithPasswordRegex(String passwordRegex, String passwordComplexityMessage)
    {
        gotoBrowseDirectories();

        clickLinkWithExactText(INTERNAL_DIRECTORY_NAME);

        clickLink("internal-configuration");

        setTextField("passwordRegex", passwordRegex);
        setTextField("passwordComplexityMessage", passwordComplexityMessage);

        submit();
        assertTextFieldEquals("passwordComplexityMessage", passwordComplexityMessage);
    }

    public void testChangePasswordNewPasswordMismatch()
    {
        log("Running testChangePasswordNewPasswordMismatch");

        _loginTestUser();

        gotoPage("/console/user/viewchangepassword.action");

        assertKeyPresent("menu.user.console.changepassword.label");

        setTextField("originalPassword", TEST_USER_PW);
        setTextField("password", "new");
        setTextField("confirmPassword", "somethingelse");
        submit();

        assertKeyPresent("menu.user.console.changepassword.label");
        assertKeyPresent("passworddonotmatch.invalid");
    }

    public void testChangePasswordPasswordDoesNotMeetComplexityRequirements()
    {
        log("Running testChangePasswordPasswordDoesNotMeetComplexityRequirements");

        intendToModifyData();

        _loginAdminUser();
        updateDirectoryWithPasswordRegex("[a-z]", "Passwords <b>must</b> contain at least one lowercase character");

        _loginTestUser();

        gotoPage("/console/user/viewchangepassword.action");

        // inform the user about the requirements upfront, but without scaring him
        assertWarningAndErrorNotPresent();
        assertTextPresent("Passwords <b>must</b> contain at least one lowercase character");  // escaped

        _changePassword(TEST_USER_PW, "!@#$1234");

        assertErrorPresentWithKey("passwordupdate.policy.error.message");
        assertKeyNotPresent("principal.password.complexity.policy");
        assertKeyPresent("menu.user.console.changepassword.label");
        assertTextPresent("Passwords <b>must</b> contain at least one lowercase character");  // escaped
    }

    public void testChangePasswordOldPasswordWrong()
    {
        log("Running testChangePasswordOldPasswordWrong");

        _loginTestUser();

        gotoPage("/console/user/viewchangepassword.action");

        _changePassword("wrong", "new");
        assertKeyPresent("password.invalid");
    }

    public void testChangePasswordSuccess()
    {
        intendToModifyData();

        log("Running testChangePasswordSuccess");

        _loginTestUser();

        gotoPage("/console/user/viewchangepassword.action");

        _changePassword(TEST_USER_PW, "new");
        assertKeyPresent("passwordupdate.message");

        _logout();

        // test old password
        setTextField("j_username", TEST_USER_NAME);
        setTextField("j_password", TEST_USER_PW);
        submit();

        assertKeyPresent("login.failed.label");

        // test new password
        setTextField("j_username", TEST_USER_NAME);
        setTextField("j_password", "new");
        submit();
        assertLinkPresentWithKey("menu.logout.label");
        assertKeyPresent("menu.user.console.editprofile.label");
    }

    public void testChangePasswordNewPasswordEqualsCurrentPassword()
    {
        log("Running testChangePasswordNewPasswordEqualsCurrentPassword");

        _loginTestUser();

        gotoPage("/console/user/viewchangepassword.action");

        _changePassword(TEST_USER_PW, TEST_USER_PW);
        assertKeyNotPresent("passwordupdate.message");
        assertTextPresent("Unable to update password since this password matches"); // not i18n, actual string may vary
    }

    public void testChangePasswordFailsWhenNewPasswordMatchesPasswordHistory()
    {
        intendToModifyData();

        log("Running testChangePasswordFailsWhenNewPasswordMatchesPasswordHistory");

        _loginTestUser();

        gotoPage("/console/user/viewchangepassword.action");

        // first change succeeds
        _changePassword(TEST_USER_PW, "password1");
        assertKeyPresent("passwordupdate.message");

        // second change succeeds
        _changePassword("password1", "password2");
        assertKeyPresent("passwordupdate.message");

        // third change succeeds. At this point, password history contains password2 and password3, but not password1
        _changePassword("password2", "password3");
        assertKeyPresent("passwordupdate.message");

        // sorry, you cannot go back to password2 either (it's still in password history)
        _changePassword("password3", "password2");
        assertKeyNotPresent("passwordupdate.message");
        assertTextPresent("Unable to update password since this password matches");

        // changing to the same password is also forbidden
        _changePassword("password3", "password3");
        assertKeyNotPresent("passwordupdate.message");
        assertTextPresent("Unable to update password since this password matches");

        // changing back to password1 is OK because password1 has already been dropped from the password history
        // changing to a completely new password is OK, and password2 is dropped from the password history
        _changePassword("password3", "password1");
        assertKeyPresent("passwordupdate.message");

        // changing back to password2 is OK because password2 has already been dropped from the password history
        _changePassword("password1", "password2");
        assertKeyPresent("passwordupdate.message");
    }

    // CWD-1979
    public void testCredentialHistoryOverflowDoesNotCauseReferentialIntegrityProblems()
    {
        intendToModifyData();

        log("Running testCredentialHistoryOverflowDoesNotCauseReferentialIntegrityProblems");

        _loginTestUser();

        gotoPage("/console/user/viewchangepassword.action");

        // first change succeeds
        _changePassword(TEST_USER_PW, "password1");
        assertKeyPresent("passwordupdate.message");

        // second change succeeds
        _changePassword("password1", "password2");
        assertKeyPresent("passwordupdate.message");

        // third change succeeds. At this point, password history contains password2 and password3, but not password1
        _changePassword("password2", "password3");
        assertKeyPresent("passwordupdate.message");

        _loginAdminUser();

        // delete the user to assert that the database maintains referential integrity (CWD-1979)
        gotoViewPrincipal(TEST_USER_NAME, INTERNAL_DIRECTORY_NAME);
        clickLink("remove-principal");
        submit();
        assertKeyPresent("updatesuccessful.label");
    }

    public void testChangePasswordNoPermissions()
    {
        log("Running testChangePasswordNoPermissions");

        _loginImmutableUser();

        gotoPage("/console/user/viewchangepassword.action");

        _changePassword(IMMUTABLE_USER_PW, "new");
        assertKeyPresent("user.console.password.permission.error");
    }

    private void _changePassword(String oldPassword, String newPassword)
    {
        assertKeyPresent("menu.user.console.changepassword.label");

        setTextField("originalPassword", oldPassword);
        setTextField("password", newPassword);
        setTextField("confirmPassword", newPassword);
        submit();

        assertKeyPresent("menu.user.console.changepassword.label");
    }
}
