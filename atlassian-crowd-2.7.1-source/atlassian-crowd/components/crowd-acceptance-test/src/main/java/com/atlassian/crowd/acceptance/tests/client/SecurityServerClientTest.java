package com.atlassian.crowd.acceptance.tests.client;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import com.atlassian.crowd.integration.soap.SOAPEntity;
import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.soap.SearchRestriction;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.search.SearchContext;
import com.atlassian.crowd.service.soap.client.SecurityServerClientImpl;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class SecurityServerClientTest extends CrowdAcceptanceTestCase
{
    private SecurityServerClientImpl securityServerClient;
    private UserAuthenticationContext userAuthenticationContext;

    public void setUp() throws Exception
    {
        super.setUp();

        restoreCrowdFromXML("tokenauthenticationtest.xml");

        final Properties sscProperties = AcceptanceTestHelper.loadProperties("localtest.crowd.properties");
        SoapClientProperties cProperties = SoapClientPropertiesImpl.newInstanceFromProperties(sscProperties);
        securityServerClient = new SecurityServerClientImpl(cProperties);

        userAuthenticationContext = new UserAuthenticationContext();
        userAuthenticationContext.setApplication("integrationtest");
    }

    public void testSearchGroupsWhereUserIsAMember() throws Exception
    {
        SearchRestriction restriction = new SearchRestriction(SearchContext.GROUP_PRINCIPAL_MEMBER, "admin");

        final SOAPGroup[] soapGroups = securityServerClient.searchGroups(new SearchRestriction[] { restriction });

        assertContainsExactly(Arrays.asList(soapGroups), "crowd-administrators", "general-group");
    }

    public void testSearchGroupsWithNameRestriction() throws Exception
    {
        SearchRestriction restriction = new SearchRestriction(SearchContext.GROUP_NAME, "crowd-administrators");

        final SOAPGroup[] soapGroups = securityServerClient.searchGroups(new SearchRestriction[] { restriction });

        assertContainsExactly(Arrays.asList(soapGroups), "crowd-administrators");
    }

    public void testSearchGroupsPopulatesEntityFields() throws Exception
    {
        final SearchRestriction restriction = new SearchRestriction(SearchContext.GROUP_NAME, "general-group");

        final SOAPGroup[] soapGroups = securityServerClient.searchGroups(new SearchRestriction[] { restriction });

        assertTrue(soapGroups[0].isActive());
        assertEquals("General Group", soapGroups[0].getDescription());
        assertEquals(32769, soapGroups[0].getDirectoryId());
    }

    static List<String> namesOf(Iterable<? extends SOAPEntity> entities)
    {
        List<String> names = new ArrayList<String>();

        for (SOAPEntity entity : entities)
        {
            names.add(entity.getName());
        }

        return names;
    }

    public static void assertContainsExactly(List<? extends SOAPEntity> entities, String... names)
    {
        assertEquals(namesOf(entities), Arrays.asList(names));
    }

    /**
     * Tests that users with matching aliases are returned in lower case.
     */
    public void testSearchPrincipals_Aliases() throws Exception
    {
        setApplication("aliases", "aliases");

        final SOAPPrincipal[] principals = securityServerClient.searchPrincipals(new SearchRestriction[] {new SearchRestriction(SearchContext.PRINCIPAL_NAME, "alias")});

        assertContainsExactly(Arrays.asList(principals), "alias1", "alias2");
    }

    /**
     * Tests that user with username 'user' is not returned, because that user has an alias.
     */
    public void testSearchPrincipals_AliasesIgnoreUsernameWhenAliasExists() throws Exception
    {
        setApplication("aliases", "aliases");

        final SOAPPrincipal[] principals = securityServerClient.searchPrincipals(new SearchRestriction[] {new SearchRestriction(SearchContext.PRINCIPAL_NAME, "user")});

        assertContainsExactly(Arrays.asList(principals), "user2");
    }

    /**
     * Tests that all users are matched and returned in lower case.
     */
    public void testSearchPrincipals_AliasesAll() throws Exception
    {
        setApplication("aliases", "aliases");

        final SOAPPrincipal[] principals = securityServerClient.searchPrincipals(new SearchRestriction[] {new SearchRestriction(SearchContext.PRINCIPAL_NAME, "s")});

        assertContainsExactly(Arrays.asList(principals), "alias1", "alias2", "user2");
    }

    private void setApplication(final String applicationName, final String password)
    {
        Properties sscProperties = AcceptanceTestHelper.loadProperties("localtest.crowd.properties");
        sscProperties.setProperty("application.password", password);
        sscProperties.setProperty("application.name", applicationName);
        SoapClientProperties cProperties = SoapClientPropertiesImpl.newInstanceFromProperties(sscProperties);
        securityServerClient = new SecurityServerClientImpl(cProperties);
        userAuthenticationContext = new UserAuthenticationContext();
        userAuthenticationContext.setApplication("integrationtest");
    }
}
