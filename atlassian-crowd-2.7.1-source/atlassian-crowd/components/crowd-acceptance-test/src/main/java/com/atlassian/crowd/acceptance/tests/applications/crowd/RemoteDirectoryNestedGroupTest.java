package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.crowd.acceptance.utils.DbCachingTestHelper;
import com.atlassian.crowd.integration.rest.service.factory.RestCrowdClientFactory;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.service.client.CrowdClient;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.util.Set;

public class RemoteDirectoryNestedGroupTest extends CrowdAcceptanceTestCase
{
    private static final String NESTED_APPLICATION_NAME = "nested-crowd";
    private static final String NESTED_APPLICATION_PASSWORD = "nested-crowd";

    private static final String NESTED_DIRECTORY_NAME = "nested-remote-directory";

    private static final String NON_NESTED_APPLICATION_NAME = "non-nested-crowd";
    private static final String NON_NESTED_APPLICATION_PASSWORD = "non-nested-crowd";

    private static final String NON_NESTED_DIRECTORY_NAME = "non-nested-remote-directory";

    private CrowdClient crowdClientWithNestedGroup;
    private CrowdClient crowdClientWithNoNestedGroup;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        final RestCrowdClientFactory clientFactory = new RestCrowdClientFactory();
        crowdClientWithNestedGroup = clientFactory.newInstance(HOST_PATH, NESTED_APPLICATION_NAME, NESTED_APPLICATION_PASSWORD);
        crowdClientWithNoNestedGroup = clientFactory.newInstance(HOST_PATH, NON_NESTED_APPLICATION_NAME, NON_NESTED_APPLICATION_PASSWORD);

        restoreCrowdFromXML("remotedirectorynestedgrouptest.xml");

        // sync the remote directories.
        final DbCachingTestHelper dbCachingTestHelper = new DbCachingTestHelper(tester);
        dbCachingTestHelper.synchroniseDirectory(NESTED_DIRECTORY_NAME, 60000);
        dbCachingTestHelper.synchroniseDirectory(NON_NESTED_DIRECTORY_NAME, 60000);
    }

    public void testNestedGroupMembershipListing() throws Exception
    {
        final Set<String> expectedGroupNames = ImmutableSet.of("dir3childgroup", "dir3parentgroup");
        final Set<String> groupNames = Sets.newHashSet(crowdClientWithNestedGroup.getNamesOfGroupsForNestedUser("nesteduser", 0, EntityQuery.ALL_RESULTS));
        assertEquals(expectedGroupNames, groupNames);
    }

    public void testNonNestedGroupMembershipListing() throws Exception
    {
        final Set<String> expectedGroupNames = ImmutableSet.of("dir3childgroup");
        final Set<String> groupNames = Sets.newHashSet(crowdClientWithNoNestedGroup.getNamesOfGroupsForNestedUser("nesteduser", 0, EntityQuery.ALL_RESULTS));
        assertEquals(expectedGroupNames, groupNames);
    }

    public void testNestedGroupMembership() throws Exception
    {
        assertTrue(crowdClientWithNestedGroup.isUserNestedGroupMember("nesteduser", "dir3childgroup"));
        assertTrue("transitivity expected", crowdClientWithNestedGroup.isUserNestedGroupMember("nesteduser", "dir3parentgroup"));
    }

    public void testNonNestedGroupMembership() throws Exception
    {
        assertTrue(crowdClientWithNoNestedGroup.isUserNestedGroupMember("nesteduser", "dir3childgroup"));
        assertFalse("no transitivity expected here", crowdClientWithNoNestedGroup.isUserNestedGroupMember("nesteduser", "dir3parentgroup"));
    }
}
