package com.atlassian.crowd.acceptance.tests.persistence.dao.user;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.TimestampedUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.restriction.PropertyUtils;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.atlassian.crowd.model.user.Users.namesOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Integration tests for the CRUD operations of {@link UserDao}. For convenience, the tests
 * of this interface have been split into multiple test classes.
 *
 * @see UserDaoSearchTest
 * @see UserDAOHibernateTest
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class UserDaoCRUDTest
{
    @Inject private UserDao userDAO;
    @Inject private MembershipDao membershipDAO;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    private static final long DIRECTORY_ID = 1L;
    private static final String EXISTING_USER_1 = "admin";
    private static final String EXISTING_USER_2 = "bob";
    private static final String NON_EXISTING_USER = "newguy";

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Test
    public void testFindByName() throws Exception
    {
        TimestampedUser user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);

        assertEquals(EXISTING_USER_1, user.getName());
        assertTrue(user.isActive());
        assertEquals("Admin@example.com", user.getEmailAddress());
        assertEquals("Super", user.getFirstName());
        assertEquals("Man", user.getLastName());
        assertEquals("Super Man", user.getDisplayName());
        assertEquals("external-id-2", user.getExternalId());
        assertTrue(user.getCreatedDate().before(new Date()));
        assertTrue(user.getUpdatedDate().before(new Date()));
        assertEquals(DIRECTORY_ID, user.getDirectoryId());
    }

    @Test
    public void testFindByMixedCaseNameExactMatch() throws Exception
    {
        TimestampedUser user = userDAO.findByName(2L, "JohnSmith");

        assertEquals("JohnSmith", user.getName());
        assertTrue(user.isActive());
        assertEquals("johnsmith@smith.com", user.getEmailAddress());
        assertEquals("John", user.getFirstName());
        assertEquals("Smith", user.getLastName());
        assertEquals("John Smith", user.getDisplayName());
        assertTrue(user.getCreatedDate().before(new Date()));
        assertTrue(user.getUpdatedDate().before(new Date()));
        assertEquals(2L, user.getDirectoryId());
    }

    @Test
    public void testFindByMixedCaseNameCaseInsensitiveMatch() throws Exception
    {
        TimestampedUser user = userDAO.findByName(2L, "johnsmith");

        assertEquals("JohnSmith", user.getName());
        assertTrue(user.isActive());
        assertEquals("johnsmith@smith.com", user.getEmailAddress());
        assertEquals("John", user.getFirstName());
        assertEquals("Smith", user.getLastName());
        assertEquals("John Smith", user.getDisplayName());
        assertTrue(user.getCreatedDate().before(new Date()));
        assertTrue(user.getUpdatedDate().before(new Date()));
        assertEquals(2L, user.getDirectoryId());
    }

    @Test (expected = UserNotFoundException.class)
    public void testFindByNameWhereUserDoesNotExist() throws Exception
    {
        userDAO.findByName(DIRECTORY_ID, NON_EXISTING_USER);
    }

    @Test
    public void testFindByNameWithAttributes() throws Exception
    {
        UserWithAttributes user = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);

        assertEquals(EXISTING_USER_1, user.getName());
        assertTrue(user.isActive());
        assertEquals("Admin@example.com", user.getEmailAddress());
        assertEquals("Super", user.getFirstName());
        assertEquals("Man", user.getLastName());
        assertEquals("Super Man", user.getDisplayName());
        assertEquals("external-id-2", user.getExternalId());
        assertEquals(DIRECTORY_ID, user.getDirectoryId());

        assertEquals(2, user.getKeys().size());
        assertTrue(user.getKeys().contains("color"));
        assertTrue(user.getKeys().contains("openid"));

        assertEquals(2, user.getValues("color").size());
        assertTrue(user.getValues("color").contains("black"));
        assertTrue(user.getValues("color").contains("Red"));

        assertEquals(1, user.getValues("openid").size());
        assertEquals("http://my.openid.com/admin", user.getValue("openid"));
    }

    @Test (expected = UserNotFoundException.class)
    public void testFindByNameWithAttributesWhereUserDoesNotExist() throws Exception
    {
        userDAO.findByNameWithAttributes(DIRECTORY_ID, NON_EXISTING_USER);
    }

    @Test
    public void testGetCredential() throws UserNotFoundException
    {
        PasswordCredential credential = userDAO.getCredential(DIRECTORY_ID, EXISTING_USER_1);

        assertTrue(credential.isEncryptedCredential());
        assertEquals("5m0k350m3h4$h", credential.getCredential());
    }

    @Test (expected = UserNotFoundException.class)
    public void testGetCredentialWhereUserDoesNotExist() throws Exception
    {
        userDAO.getCredential(DIRECTORY_ID, NON_EXISTING_USER);
    }

    @Test
    public void testGetCredentialHistory() throws UserNotFoundException
    {
        List<PasswordCredential> credentialHistory = userDAO.getCredentialHistory(DIRECTORY_ID, EXISTING_USER_1);

        assertThat(credentialHistory, hasItems(PasswordCredential.encrypted("mysecret"),
                                               PasswordCredential.encrypted("mysecret2")));
    }

    @Test (expected = UserNotFoundException.class)
    public void testGetCredentialHistoryWhereUserDoesNotExist() throws Exception
    {
        userDAO.getCredentialHistory(DIRECTORY_ID, NON_EXISTING_USER);
    }

    @Test
    public void testAddMixedCaseName() throws Exception
    {
        User createdUser = createNewUser("NewUser");

        assertNotNull(createdUser);
        assertTrue(((InternalUser) createdUser).getId() > 0);

        // find
        User foundUser = userDAO.findByName(DIRECTORY_ID, "NewUser");
        assertTrue(foundUser.getName().equals("NewUser"));
        assertEquals(true, foundUser.isActive());
        assertEquals("New", foundUser.getFirstName());
        assertEquals("Guy", foundUser.getLastName());
        assertEquals("New Guy", foundUser.getDisplayName());
        assertEquals("nOOb@example.com", foundUser.getEmailAddress());

        // test lower case values were stored
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet("SELECT lower_user_name, lower_first_name, lower_last_name, lower_display_name, lower_email_address FROM cwd_user WHERE user_name = 'NewUser'");

        assertTrue(sqlRowSet.next());
        assertEquals("new", sqlRowSet.getString("lower_first_name"));
        assertEquals("guy", sqlRowSet.getString("lower_last_name"));
        assertEquals("noob@example.com", sqlRowSet.getString("lower_email_address"));
        assertEquals("new guy", sqlRowSet.getString("lower_display_name"));
        assertEquals("newuser", sqlRowSet.getString("lower_user_name"));
    }

    @Test
    public void testAdd() throws Exception
    {
        User createdUser = createNewUser(NON_EXISTING_USER);

        assertNotNull(createdUser);
        assertTrue(((InternalUser) createdUser).getId() > 0);

        // find
        User foundUser = userDAO.findByName(DIRECTORY_ID, NON_EXISTING_USER);
        assertTrue(foundUser.getName().equals(NON_EXISTING_USER));
        assertEquals(true, foundUser.isActive());
        assertEquals("New", foundUser.getFirstName());
        assertEquals("Guy", foundUser.getLastName());
        assertEquals("New Guy", foundUser.getDisplayName());
        assertEquals("nOOb@example.com", foundUser.getEmailAddress());
        assertEquals("new-external-id", foundUser.getExternalId());

        // test lower case values were stored
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet("SELECT lower_first_name, lower_last_name, lower_display_name, lower_email_address FROM cwd_user WHERE user_name = '" + NON_EXISTING_USER + "'");

        assertTrue(sqlRowSet.next());
        assertEquals("new", sqlRowSet.getString("lower_first_name"));
        assertEquals("guy", sqlRowSet.getString("lower_last_name"));
        assertEquals("noob@example.com", sqlRowSet.getString("lower_email_address"));
        assertEquals("new guy", sqlRowSet.getString("lower_display_name"));
    }

    @Test
    public void testAddNull() throws Exception
    {
        UserTemplate user = new UserTemplate(NON_EXISTING_USER, DIRECTORY_ID);
        userDAO.add(user, PasswordCredential.encrypted("secret"));

        User foundUser = userDAO.findByName(DIRECTORY_ID, NON_EXISTING_USER);

        assertEquals(NON_EXISTING_USER, foundUser.getName());
        assertEquals(false, foundUser.isActive());
        assertNull(foundUser.getFirstName());
        assertNull(foundUser.getLastName());
        assertNull(foundUser.getDisplayName());
        assertNull(foundUser.getEmailAddress());
        assertNull(foundUser.getExternalId());
    }

    @Test (expected = IllegalArgumentException.class)
    public void testAddWithUnencryptedPassword() throws Exception
    {
        UserTemplate user = new UserTemplate(NON_EXISTING_USER, DIRECTORY_ID);
        userDAO.add(user, PasswordCredential.unencrypted("unencrypted secret"));
    }

    @Test (expected = UserAlreadyExistsException.class)
    public void testAddExistingUserWithSameCaseUsername() throws Exception
    {
        createNewUser(EXISTING_USER_2);
    }

    @Test (expected = UserAlreadyExistsException.class)
    public void testAddExistingUserWithDifferentCaseUsername() throws Exception
    {
        createNewUser(EXISTING_USER_1.toUpperCase());
    }

    @Test
    public void testUpdate() throws Exception
    {
        User user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);

        UserTemplate userTemplate = new UserTemplate(user);
        userTemplate.setFirstName("Someone");
        userTemplate.setLastName("Different");
        userTemplate.setActive(false);
        userTemplate.setDisplayName("A specific displayName");
        userTemplate.setEmailAddress("Someone@different.com");
        userTemplate.setExternalId("different-external-id"); // DAO is not aware of the immutability of this field

        User returnedUser = userDAO.update(userTemplate);
        assertFalse(returnedUser.isActive());
        assertEquals("Someone", returnedUser.getFirstName());
        assertEquals("Different", returnedUser.getLastName());
        assertEquals("A specific displayName", returnedUser.getDisplayName());
        assertEquals("Someone@different.com", returnedUser.getEmailAddress());
        assertEquals("different-external-id", returnedUser.getExternalId());

        // find
        User foundUser = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);
        assertFalse(foundUser.isActive());
        assertEquals("Someone", foundUser.getFirstName());
        assertEquals("Different", foundUser.getLastName());
        assertEquals("A specific displayName", foundUser.getDisplayName());
        assertEquals("Someone@different.com", foundUser.getEmailAddress());
        assertEquals("different-external-id", foundUser.getExternalId());

        // test lower case values were stored
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet("SELECT lower_first_name, lower_last_name, lower_display_name, lower_email_address FROM cwd_user WHERE user_name = '" + EXISTING_USER_1 + "'");

        assertTrue(sqlRowSet.next());
        assertEquals("someone", sqlRowSet.getString("lower_first_name"));
        assertEquals("different", sqlRowSet.getString("lower_last_name"));
        assertEquals("someone@different.com", sqlRowSet.getString("lower_email_address"));
        assertEquals("a specific displayname", sqlRowSet.getString("lower_display_name"));
    }

    @Test
    public void testUpdateShouldNotRenameUser() throws Exception
    {
        User user = userDAO.findByName(DIRECTORY_ID, "bob");

        UserTemplate userTemplate = new UserTemplate(user);
        userTemplate.setName("Bob");  // case-only change
        userTemplate.setFirstName("Someone");

        User returnedUser = userDAO.update(userTemplate);
        assertEquals("Username should be unaltered, update() should not rename", "bob", returnedUser.getName());
        assertEquals("Other attributes should be updated", "Someone", returnedUser.getFirstName());

        // find
        User foundUser = userDAO.findByName(DIRECTORY_ID, "bob");
        assertEquals("Username should be unaltered, update() should not rename", "bob", foundUser.getName());
        assertEquals("Other attributes should be updated", "Someone", foundUser.getFirstName());

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet("SELECT user_name, first_name, lower_first_name FROM cwd_user WHERE lower_user_name = '" + "bob" + "'");

        assertTrue(sqlRowSet.next());
        assertEquals("bob", sqlRowSet.getString("user_name")); // no changes
        assertEquals("Someone", sqlRowSet.getString("first_name")); // changed
        assertEquals("someone", sqlRowSet.getString("lower_first_name"));
    }

    @Test
    public void testUpdateWhenUserDoesNotExist() throws Exception
    {
        // find an existing user
        User user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);

        // then remove it
        userDAO.remove(user);

        try
        {
            userDAO.update(user);
            fail("UserNotFoundException expected");
        }
        catch (UserNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testUpdateCredentialChangesCredential() throws Exception
    {
        User user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);

        final PasswordCredential newPassword = PasswordCredential.encrypted("password");
        userDAO.updateCredential(user, newPassword, 0);

        assertEquals(newPassword, userDAO.getCredential(DIRECTORY_ID, EXISTING_USER_1));
    }

    @Test
    public void testUpdateCredentialAddsNewCredentialToCredentialHistory() throws Exception
    {
        User user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);

        assertThat(userDAO.getCredentialHistory(DIRECTORY_ID, EXISTING_USER_1),
                   hasItems(PasswordCredential.encrypted("mysecret"),
                            PasswordCredential.encrypted("mysecret2")));

        final PasswordCredential newPassword = PasswordCredential.encrypted("password");
        userDAO.updateCredential(user, newPassword, 10);

        assertThat(userDAO.getCredentialHistory(DIRECTORY_ID, EXISTING_USER_1),
                   hasItems(PasswordCredential.encrypted("password"),
                            PasswordCredential.encrypted("mysecret"),
                            PasswordCredential.encrypted("mysecret2")));
    }

    @Test
    public void testUpdateCredentialWhenCredentialHistoryIsFull() throws Exception
    {
        User user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);

        assertThat(userDAO.getCredentialHistory(DIRECTORY_ID, EXISTING_USER_1),
                   hasItems(PasswordCredential.encrypted("mysecret"),
                            PasswordCredential.encrypted("mysecret2")));

        final PasswordCredential newPassword = PasswordCredential.encrypted("password");
        userDAO.updateCredential(user, newPassword, 2);

        assertThat(userDAO.getCredentialHistory(DIRECTORY_ID, EXISTING_USER_1),
                   hasItems(PasswordCredential.encrypted("password"),
                            PasswordCredential.encrypted("mysecret2")));
        assertThat(userDAO.getCredentialHistory(DIRECTORY_ID, EXISTING_USER_1),
                   not(hasItem(PasswordCredential.encrypted("mysecret"))));  // mysecret has been dropped from history
    }

    @Test
    public void testUpdateCredentialPreservesReferentialIntegrity() throws Exception
    {
        User user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);

        assertThat(userDAO.getCredentialHistory(DIRECTORY_ID, EXISTING_USER_1), hasSize(2));

        final PasswordCredential newPassword = PasswordCredential.encrypted("password");
        userDAO.updateCredential(user, newPassword, 2);  // causes password history to overflow

        userDAO.remove(user);

//        flush(); // forces sync with database
    }

    @Test
    public void testRenameToACompletelyDifferentName() throws Exception
    {
        User user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);
        User returnedUser = userDAO.rename(user, NON_EXISTING_USER);
        assertEquals(NON_EXISTING_USER, returnedUser.getName());

        try
        {
            userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);
        }
        catch (UserNotFoundException e)
        {
            // expected
        }

        User renamedUser = userDAO.findByName(DIRECTORY_ID, NON_EXISTING_USER);
        assertEquals(NON_EXISTING_USER, renamedUser.getName());

        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(EXISTING_USER_1).returningAtMost(10)).size());
        assertEquals(2, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(NON_EXISTING_USER).returningAtMost(10)).size());
        assertEquals(1,
                     membershipDAO.search(DIRECTORY_ID,
                                          QueryBuilder.queryFor(Group.class, EntityDescriptor.role())
                                              .parentsOf(EntityDescriptor.user())
                                              .withName(NON_EXISTING_USER)
                                              .returningAtMost(10)).size());
    }

    @Test
    public void testRenameWhenUsernameContainsMixedCase() throws Exception
    {
        User user = userDAO.findByName(2L, "JohnSmith");
        User returnedUser = userDAO.rename(user, "BobSmith");
        assertEquals("BobSmith", returnedUser.getName());

        try
        {
            userDAO.findByName(2L, "JohnSmith");
        }
        catch (UserNotFoundException e)
        {
            // expected
        }

        User renamedUser = userDAO.findByName(2L, "BobSmith");
        assertEquals("BobSmith", renamedUser.getName());

        assertEquals(0, membershipDAO.search(2L, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("johnsmith").returningAtMost(10)).size());
        assertEquals(1, membershipDAO.search(2L, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("bobsmith").returningAtMost(10)).size());
        assertEquals(0, membershipDAO.search(2L, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("JohnSmith").returningAtMost(10)).size());
        assertEquals(1, membershipDAO.search(2L, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(
            EntityDescriptor.user()).withName("BobSmith").returningAtMost(10)).size());
    }

    @Test
    public void testRenameWhenUserDoesNotExist() throws Exception
    {
        // find a user
        User user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);

        // then remove it
        userDAO.remove(user);

        try
        {
            userDAO.rename(user, "NewName");
            fail("UserNotFoundException expected");
        }
        catch (UserNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testRenameWhenThereIsANameClashWithAnotherUser() throws Exception
    {
        // find a user
        User user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);

        try
        {
            userDAO.rename(user, EXISTING_USER_2);
            fail("UserAlreadyExistsException expected");
        }
        catch (UserAlreadyExistsException e)
        {
            // expected
        }
    }

    @Test
    public void testRenameToExactlySameName() throws Exception
    {
        User user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);

        User returnedUser = userDAO.rename(user, EXISTING_USER_1);
        assertEquals(EXISTING_USER_1, returnedUser.getName());
    }

    @Test
    public void testRenameWhenOnlyCaseChanges() throws Exception
    {
        User user = userDAO.findByName(DIRECTORY_ID, "bob");

        User returnedUser = userDAO.rename(user, "Bob");  // case-only change
        assertEquals("Bob", returnedUser.getName());

        // user can still be found as "bob" and "Bob"
        assertEquals("Bob", userDAO.findByName(DIRECTORY_ID, "bob").getName());
        assertEquals("Bob", userDAO.findByName(DIRECTORY_ID, "Bob").getName());

        // memberships can be found by "bob" or "Bob"
        assertEquals(1, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("bob").returningAtMost(10)).size());
        assertEquals(1, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("Bob").returningAtMost(10)).size());
    }

    @Test
    public void testRemove() throws Exception
    {
        User user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);
        long userId = user.getDirectoryId();

        userDAO.remove(user);

        try
        {
            userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);
        }
        catch (UserNotFoundException e)
        {
            // expected
        }

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertThat(jdbcTemplate.queryForObject(
            "SELECT COUNT(*) FROM cwd_user_credential_record WHERE user_id = ?", Integer.class, userId), is(0));
        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(EXISTING_USER_1).returningAtMost(10)).size());
    }

    @Test
    public void testRemoveMixedCaseInsensitive() throws Exception
    {
        User user = userDAO.findByName(2L, "johnSmith");
        long userId = user.getDirectoryId();

        userDAO.remove(user);

        try
        {
            userDAO.findByName(DIRECTORY_ID, "johnSmith");
        }
        catch (UserNotFoundException e)
        {
            // expected
        }

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        assertThat(jdbcTemplate.queryForObject(
            "SELECT COUNT(*) FROM cwd_user_credential_record WHERE user_id = ?", Integer.class, userId), is(0));
        assertEquals(0, membershipDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName("JohnSmith").returningAtMost(10)).size());
    }

    @Test
    public void testStoreAttributesWithUpdateAndInsertForExistingUser() throws Exception
    {
        // find existing user with attributes
        UserWithAttributes user = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);
        assertEquals(2, user.getKeys().size());
        assertEquals(2, user.getValues("color").size());
        assertTrue(user.getValues("color").contains("black"));
        assertTrue(user.getValues("color").contains("Red"));
        assertEquals(1, user.getValues("openid").size());
        assertEquals("http://my.openid.com/admin", user.getValue("openid"));

        // add/update attributes (update color and add phone)
        Map<String, Set<String>> attributes = new HashMap<String, Set<String>>();
        attributes.put("color", Sets.newHashSet("blue", "green", "Red"));
        attributes.put("phone", Sets.newHashSet("131111"));
        userDAO.storeAttributes(user, attributes);

        // retrieve and verify
        UserWithAttributes returnedUser = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);
        assertEquals(3, returnedUser.getKeys().size());
        assertEquals(3, returnedUser.getValues("color").size());
        assertTrue(returnedUser.getValues("color").contains("blue"));
        assertTrue(returnedUser.getValues("color").contains("green"));
        assertTrue(returnedUser.getValues("color").contains("Red"));
        assertEquals(1, returnedUser.getValues("phone").size());
        assertTrue(returnedUser.getValues("phone").contains("131111"));
        assertEquals(1, returnedUser.getValues("openid").size());
        assertEquals("http://my.openid.com/admin", returnedUser.getValue("openid"));
    }

    @Test
    public void testStoreAttributesForExistingMixedCaseUser() throws Exception
    {
        // find existing user with attributes
        UserWithAttributes user = userDAO.findByNameWithAttributes(2L, "JohnSmith" );
        assertEquals(1, user.getKeys().size());
        assertEquals(1, user.getValues("color").size());
        assertTrue(user.getValues("color").contains("violet"));

        // add/update attributes (update color and add phone)
        Map<String, Set<String>> attributes = new HashMap<String, Set<String>>();
        attributes.put("color", Sets.newHashSet("blue", "green", "Red"));
        attributes.put("phone", Sets.newHashSet("131111"));
        userDAO.storeAttributes(user, attributes);

        // retrieve and verify
        UserWithAttributes returnedUser = userDAO.findByNameWithAttributes(2L, "JohnSmith");
        assertEquals(2, returnedUser.getKeys().size());
        assertEquals(3, returnedUser.getValues("color").size());
        assertTrue(returnedUser.getValues("color").contains("blue"));
        assertTrue(returnedUser.getValues("color").contains("green"));
        assertTrue(returnedUser.getValues("color").contains("Red"));
        assertEquals(1, returnedUser.getValues("phone").size());
        assertTrue(returnedUser.getValues("phone").contains("131111"));
    }

    @Test
    public void testStoreAttributesWithUpdateAndDeleteForExistingUser() throws Exception
    {
        // find existing user with attributes
        UserWithAttributes user = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);
        assertEquals(2, user.getKeys().size());
        assertEquals(2, user.getValues("color").size());
        assertTrue(user.getValues("color").contains("black"));
        assertTrue(user.getValues("color").contains("Red"));
        assertEquals(1, user.getValues("openid").size());
        assertEquals("http://my.openid.com/admin", user.getValue("openid"));

        // add/update attributes (update color and add phone)
        Map<String, Set<String>> attributes = new HashMap<String, Set<String>>();
        attributes.put("color", Sets.newHashSet("Red"));
        attributes.put("phone", Sets.newHashSet("131111"));
        userDAO.storeAttributes(user, attributes);

        // retrieve and verify
        UserWithAttributes returnedUser = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);
        assertEquals(3, returnedUser.getKeys().size());
        assertEquals(1, returnedUser.getValues("color").size());
        assertTrue(returnedUser.getValues("color").contains("Red"));
        assertEquals(1, returnedUser.getValues("phone").size());
        assertTrue(returnedUser.getValues("phone").contains("131111"));
        assertEquals(1, returnedUser.getValues("openid").size());
        assertEquals("http://my.openid.com/admin", returnedUser.getValue("openid"));
    }

    @Test
    public void testStoreSingleAttributeWithUpdateForExistingUser() throws Exception
    {
        // find existing user with attributes
        UserWithAttributes user = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);
        assertEquals(2, user.getKeys().size());
        assertEquals(2, user.getValues("color").size());
        assertTrue(user.getValues("color").contains("black"));
        assertTrue(user.getValues("color").contains("Red"));
        assertEquals(1, user.getValues("openid").size());
        assertEquals("http://my.openid.com/admin", user.getValue("openid"));

        // add/update attributes (update color and add phone)
        Map<String, Set<String>> attributes = new HashMap<String, Set<String>>();
        attributes.put("openid", Sets.newHashSet("newval"));
        userDAO.storeAttributes(user, attributes);

        // retrieve and verify
        UserWithAttributes returnedUser = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);
        assertEquals(2, returnedUser.getKeys().size());
        assertEquals(2, returnedUser.getValues("color").size());
        assertTrue(returnedUser.getValues("color").contains("black"));
        assertTrue(returnedUser.getValues("color").contains("Red"));
        assertEquals(1, returnedUser.getValues("openid").size());
        assertEquals("newval", returnedUser.getValue("openid"));
    }

    @Test
    public void testStoreAttributesWithUpdateForExistingUser() throws Exception
    {
        // find existing user with attributes
        UserWithAttributes user = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);
        assertEquals(2, user.getKeys().size());
        assertEquals(2, user.getValues("color").size());
        assertTrue(user.getValues("color").contains("black"));
        assertTrue(user.getValues("color").contains("Red"));
        assertEquals(1, user.getValues("openid").size());
        assertEquals("http://my.openid.com/admin", user.getValue("openid"));

        // add/update attributes (update color and add phone)
        Map<String, Set<String>> attributes = new HashMap<String, Set<String>>();
        attributes.put("color", Sets.newHashSet("orange", "Pink"));
        attributes.put("openid", Sets.newHashSet("new"));
        userDAO.storeAttributes(user, attributes);

        // retrieve and verify
        UserWithAttributes returnedUser = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);
        assertEquals(2, returnedUser.getKeys().size());
        assertEquals(2, returnedUser.getValues("color").size());
        assertTrue(returnedUser.getValues("color").contains("orange"));
        assertTrue(returnedUser.getValues("color").contains("Pink"));
        assertEquals(1, returnedUser.getValues("openid").size());
        assertEquals("new", returnedUser.getValue("openid"));
    }

    @Test
    public void testStoreAttributesWhenUserDoesNotExist() throws Exception
    {
        // find existing user
        UserWithAttributes user = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);

        // and then remote it
        userDAO.remove(user);

        try
        {
            userDAO.storeAttributes(user, ImmutableMap.<String,Set<String>>of());
            fail("UserNotFoundException expected");
        }
        catch (UserNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testRemoveAttribute() throws Exception
    {
        // find existing user with attributes
        UserWithAttributes user = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);
        assertEquals(2, user.getKeys().size());

        // remove attribute (color)
        userDAO.removeAttribute(user, "color");

        // find again
        UserWithAttributes updatedUser = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);
        assertEquals(1, updatedUser.getKeys().size());
        assertEquals(1, updatedUser.getValues("openid").size());
        assertEquals("http://my.openid.com/admin", updatedUser.getValue("openid"));
    }

    @Test
    public void testRemoveAttributeMixedCaseUser() throws Exception
    {
        // find existing user with attributes
        UserWithAttributes user = userDAO.findByNameWithAttributes(2L, "joHnSmith");
        assertEquals(1, user.getKeys().size());

        // remove attribute (color)
        userDAO.removeAttribute(user, "color");

        // find again
        UserWithAttributes updatedUser = userDAO.findByNameWithAttributes(2L, "joHnSmith");
        assertEquals(0, updatedUser.getKeys().size());
    }

    @Test
    public void testRemoveAttributeWhenUserDoesNotExist() throws Exception
    {
        // first find a user
        UserWithAttributes user = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);

        // then remove it
        userDAO.remove(user);

        try
        {
            userDAO.removeAttribute(user, "some attribute");
            fail("UserNotFoundException expected");
        }
        catch (UserNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testRemoveAttributeWhenAttributesDoesNotExist() throws Exception
    {
        UserWithAttributes user = userDAO.findByNameWithAttributes(DIRECTORY_ID, EXISTING_USER_1);

        userDAO.removeAttribute(user, "non-existent attribute name");   // should do nothing
    }

    @Test
    public void testAddAll()
    {
        Set<UserTemplateWithCredentialAndAttributes> users = new HashSet<UserTemplateWithCredentialAndAttributes>();

        for (int i = 0; i < 50; i++)
        {
            UserTemplateWithCredentialAndAttributes user = new UserTemplateWithCredentialAndAttributes("User" + i, DIRECTORY_ID, new PasswordCredential("secret", true));
            user.setEmailAddress("blah@example.com");
            user.setFirstName("Clone");
            user.setLastName("Drone");
            user.setDisplayName("Clone Drone");
            user.setAttribute("flavour", "chocolate");
            user.setAttribute("drink", "coke");
            users.add(user);
        }

        BatchResult<User> result = userDAO.addAll(users);

        assertEquals(50, result.getTotalSuccessful());
        assertEquals(50, result.getTotalAttempted());
        assertTrue(result.getFailedEntities().isEmpty());

        assertEquals(50, userDAO.search(DIRECTORY_ID,
                                        QueryBuilder.queryFor(String.class, EntityDescriptor.user())
                                            .with(Restriction.on(UserTermKeys.USERNAME).startingWith("user"))
                                            .returningAtMost(100)).size());
        assertEquals(50, userDAO.search(DIRECTORY_ID,
                                        QueryBuilder.queryFor(String.class, EntityDescriptor.user())
                                            .with(Restriction.on(PropertyUtils.ofTypeString("flavour")).exactlyMatching("chocolate"))
                                            .returningAtMost(100)).size());
        assertEquals(50, userDAO.search(DIRECTORY_ID,
                                        QueryBuilder.queryFor(String.class, EntityDescriptor.user())
                                            .with(Restriction.on(PropertyUtils.ofTypeString("drink"))
                                                      .exactlyMatching("coke"))
                                            .returningAtMost(100)).size());
    }

    @Test
    public void testAddAllWithErrorsAndDuplicates() throws Exception
    {
        // all users before test
        int userCount = userDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).returningAtMost(100)).size();

        Set<UserTemplateWithCredentialAndAttributes> users = new HashSet<UserTemplateWithCredentialAndAttributes>();

        // new user
        UserTemplateWithCredentialAndAttributes user = new UserTemplateWithCredentialAndAttributes("user", DIRECTORY_ID, new PasswordCredential("secret", true));
        user.setEmailAddress("common@example.com");
        user.setFirstName("Clone");
        user.setLastName("Drone");
        user.setDisplayName("Clone Drone");
        user.setAttribute("flavour", "chocolate");
        user.setAttribute("drink", "coke");
        users.add(user);

        // update existing user
        UserTemplateWithCredentialAndAttributes existingUser = new UserTemplateWithCredentialAndAttributes(userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1), new PasswordCredential("secret", true));
        existingUser.setEmailAddress("common@example.com");
        users.add(existingUser);

        // new incomplete user
        UserTemplateWithCredentialAndAttributes incompleteUser = new UserTemplateWithCredentialAndAttributes("user2", DIRECTORY_ID, new PasswordCredential("secret", true));
        users.add(incompleteUser);

        // new user for different directory
        UserTemplateWithCredentialAndAttributes differentDirectoryUser = new UserTemplateWithCredentialAndAttributes("user3", DIRECTORY_ID + 1, new PasswordCredential("secret", true));
        differentDirectoryUser.setEmailAddress("common@example.com");
        differentDirectoryUser.setFirstName("Clone");
        differentDirectoryUser.setLastName("Drone");
        differentDirectoryUser.setDisplayName("Clone Drone");
        differentDirectoryUser.setAttribute("flavour", "chocolate");
        differentDirectoryUser.setAttribute("drink", "coke");
        users.add(differentDirectoryUser);

        BatchResult<User> result = userDAO.addAll(users);

        assertEquals(3, result.getTotalSuccessful());
        assertEquals(users.size(), result.getTotalAttempted());
        assertThat(namesOf(result.getFailedEntities()), contains(EXISTING_USER_1));

        // two users should have been added
        assertEquals(userCount + 2, userDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).returningAtMost(100)).size());
        assertEquals(1, userDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).with(
            Restriction.on(PropertyUtils.ofTypeString("flavour")).exactlyMatching("chocolate")).returningAtMost(100)).size());
        assertEquals(1, userDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).with(Restriction.on(PropertyUtils.ofTypeString("drink")).exactlyMatching("coke")).returningAtMost(100)).size());
    }

    private User createNewUser(String username) throws Exception
    {
        UserTemplate user = new UserTemplate(username, DIRECTORY_ID);
        user.setActive(true);
        user.setFirstName("New");
        user.setLastName("Guy");
        user.setDisplayName("New Guy");
        user.setEmailAddress("nOOb@example.com");
        user.setExternalId("new-external-id");
        return userDAO.add(user, PasswordCredential.encrypted("secret"));
    }
}
