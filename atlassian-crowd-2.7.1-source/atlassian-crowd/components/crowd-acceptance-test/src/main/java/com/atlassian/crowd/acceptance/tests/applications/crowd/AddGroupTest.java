package com.atlassian.crowd.acceptance.tests.applications.crowd;

/**
 * Web acceptance test for the adding of a Group
 */
public class AddGroupTest extends CrowdAcceptanceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        _loginAdminUser();
        restoreCrowdFromXML("searchdirectories.xml");
    }

    public void testAddGroupWithNoDetails()
    {
        log("Running testAddGroupWithNoDetails");
        gotoAddGroup();

        setTextField("name", "");
        setTextField("description", "");
        selectOptionByValue("directoryID", "-1");

        submit();

        assertKeyPresent("group.name.invalid");
        assertKeyPresent("group.directory.invalid");
    }

    public void testAddGroupThatAlreadyExists()
    {
        log("Running testAddGroupThatAlreadyExists");
        gotoAddGroup();

        setTextField("name", "crowd-administrators");
        setTextField("description", "Crowd Administrators");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertKeyPresent("invalid.namealreadyexist");
    }

    public void testAddGroupWithTrailingWhitespaceFailsWithSpecificError()
    {
        log("Running testAddGroupThatAlreadyExists");
        gotoAddGroup();

        setTextField("name", "name ");
        setTextField("description", "Crowd Administrators");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertKeyPresent("invalid.whitespace");
    }

    public void testAddGroupToInternalDirectory()
    {
        intendToModifyData();

        log("Running testAddGroupToInternalDirectory");
        gotoAddGroup();

        setTextField("name", "test-group");
        setTextField("description", "Crowd Test Group");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent("test-group");
    }

    public void testAddGroupToInternalDirectoryMixedCaseName()
    {
        intendToModifyData();

        log("Running testAddGroupToInternalDirectoryInvalidName");
        gotoAddGroup();

        setTextField("name", "Test-Group");
        setTextField("description", "");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent("Test-Group");
    }

    public void testAddGroupToLDAPMixedCaseName()
    {
        intendToModifyData();
        intendToModifyLdapData();

        log("Running testAddGroupToLDAPMixedCaseName");
        gotoAddGroup();

        setTextField("name", "Test-MixedCase");
        setTextField("description", "");
        selectOption("directoryID", "Apache DS");

        submit();

        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent("Test-MixedCase");

    }

    public void testNoDirectoryIsSelectedByDefault()
    {
        gotoAddGroup();
        assertSelectedOptionEquals("directoryID" , getText("selectdirectory.label"));
    }

    public void testDirectorySelectionIsSaved()
    {
        // Save directory selection
        gotoAddGroup();

        setTextField("name", "testgroup");
        selectOption("directoryID", "Second Directory");

        submit();

        // Check that the directory is selected
        gotoAddGroup();
        assertSelectedOptionEquals("directoryID" , "Second Directory");
        assertSelectOptionNotPresent("directoryID" , getText("selectdirectory.label"));
    }

    public void testNonExistingDirectorySelection()
    {
        intendToModifyData();

        // Save directory selection
        gotoAddGroup();

        setTextField("name", "testgroup");
        selectOption("directoryID", "Second Directory");

        submit();

        // Remove selected directory
        gotoBrowseDirectories();
        clickLinkWithText("Second Directory");
        clickLink("remove-directory");
        submit();

        // Check that no directory is selected
        gotoAddGroup();
        assertSelectedOptionEquals("directoryID" , getText("selectdirectory.label"));
        assertWarningAndErrorNotPresent();
    }

    public void testOnlyDirectoryIsSelected()
    {
        intendToModifyData();

        gotoBrowseDirectories();

        clickLinkWithText("Apache DS");
        clickLink("remove-directory");
        submit();

        clickLinkWithText("Apache DS 1.5.4");
        clickLink("remove-directory");
        submit();

        clickLinkWithText("Second Directory");
        clickLink("remove-directory");
        submit();

        gotoAddGroup();

        assertSelectedOptionEquals("directoryID" , "Test Internal Directory");
    }
}
