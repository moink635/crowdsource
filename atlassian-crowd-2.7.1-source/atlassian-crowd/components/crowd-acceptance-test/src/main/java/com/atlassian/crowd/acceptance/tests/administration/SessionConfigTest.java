package com.atlassian.crowd.acceptance.tests.administration;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;

public class SessionConfigTest extends CrowdAcceptanceTestCase
{
    public void testChangingRequiringConsistentIpAddressLogsOutUserSession()
    {
        restoreBaseSetup();
        intendToModifyData();

        _loginAdminUser();

        gotoSessionConfig();

        /* Make sure that IP is initially included, and disable it */
        assertCheckboxSelected("includeIpAddressInValidationFactors");
        uncheckCheckbox("includeIpAddressInValidationFactors");
        submit();

        /* Verify that we're logged out and then log in again */
        assertKeyPresent("login.title");
        submitLoginFormAsAdminUser();

        /* Now the IP address is excluded. Enable it again. */
        assertKeyPresent("menu.sessionconfig.label");
        assertCheckboxPresent("includeIpAddressInValidationFactors");
        assertCheckboxNotSelected("includeIpAddressInValidationFactors");
        checkCheckbox("includeIpAddressInValidationFactors");
        submit();

        /* Make sure we're logged out and that, after login, the IP address is enabled again */
        submitLoginFormAsAdminUser();
        assertKeyPresent("menu.sessionconfig.label");
        assertCheckboxSelected("includeIpAddressInValidationFactors");
    }
}
