package com.atlassian.crowd.acceptance.tests.administration;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.text.IsEqualIgnoringWhiteSpace.equalToIgnoringWhiteSpace;

public class ErrorPageTest extends CrowdAcceptanceTestCase
{
    public void testShouldNotExposeCookieValueInErrorPage()
    {
        _loginAdminUser();
        gotoPage("/console/500.jsp");
        final Pattern pattern = Pattern.compile("token_key :([^,]+)\\<br>");
        Matcher matcher = pattern.matcher(getPageSource());

        assertTrue("Page should include a token Key.", matcher.find());
        assertThat(matcher.group(1), equalToIgnoringWhiteSpace("********"));
    }
}
