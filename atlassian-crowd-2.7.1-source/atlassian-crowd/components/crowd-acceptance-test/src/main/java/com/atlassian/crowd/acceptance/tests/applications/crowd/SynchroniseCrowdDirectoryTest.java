package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.util.Arrays;
import java.util.List;

import com.atlassian.crowd.acceptance.tests.BaseUrlFromProperties;
import com.atlassian.crowd.acceptance.utils.DbCachingTestHelper;
import com.atlassian.crowd.manager.directory.SynchronisationMode;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import static com.google.common.base.Preconditions.checkState;

/**
 * Tests that we can synchronise successfully with remote Crowd directory.
 */
public class SynchroniseCrowdDirectoryTest extends CrowdAcceptanceTestCase
{
    protected static final String LOCAL_DIRECTORY_NAME = "Loop";
    private static final String REMOTE_DIRECTORY1_NAME = "Test Internal Directory";
    private static final String REMOTE_DIRECTORY2_NAME = "Test Second Internal Directory";
    private static final String APPLICATION_NAME = "loop";
    private static final long LOOP_APPLICATION_ID = 4L;

    private final static String HOST2_PATH = new BaseUrlFromProperties(specProperties).baseUrlFor("crowd2");

    private static final long MAX_SYNC_WAIT_TIME_MS = 60000L; // 1 minute

    private final SynchronisationMode expectedMode;

    private DbCachingTestHelper dbCachingTestHelper;

    public SynchroniseCrowdDirectoryTest()
    {
        this(SynchronisationMode.INCREMENTAL);
    }

    public SynchroniseCrowdDirectoryTest(SynchronisationMode expectedMode)
    {
        this.expectedMode = expectedMode;
    }

    public void setUp() throws Exception
    {
        super.setUp();
        dbCachingTestHelper = new DbCachingTestHelper(tester);
        setScriptingEnabled(true);

        restoreCrowdFromXML("crowdwithcrowd.xml");
        intendToModifyData();

        _switchToCrowdAt(HOST2_PATH);
        restoreCrowdFromXML("crowdwithcrowdremote.xml");
        intendToModifyData();

        _switchToCrowdAt(HOST_PATH);
    }

    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        dbCachingTestHelper = null;
        super.tearDown();
    }


    protected void _doInitialSync()
    {
        assertEquals(SynchronisationMode.FULL, synchroniseDirectory());
    }

    private void _switchToCrowdAt(String hostPath)
    {
        getTestContext().setBaseUrl(hostPath);
        log("Switching to URL: " + hostPath);

        _loginAdminUser();
    }

    public void testSynchroniseChanges_CreateUniqueActiveRemoteUser()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        checkCheckbox("active");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", REMOTE_DIRECTORY1_NAME);

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("testuser");

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a new user has been created
        gotoViewPrincipal("testuser", LOCAL_DIRECTORY_NAME);
        assertTextInElement("email", "testuser@atlassian.com");
        assertCheckboxSelected("active");
        assertTextInElement("firstname", "Test");
        assertTextInElement("lastname", "User");
    }

    public void testSynchroniseChanges_CreateUniqueInactiveRemoteUser()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        uncheckCheckbox("active");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", REMOTE_DIRECTORY1_NAME);

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("testuser");

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a new user has been created
        gotoViewPrincipal("testuser", LOCAL_DIRECTORY_NAME);
        assertTextInElement("email", "testuser@atlassian.com");
        assertCheckboxNotSelected("active");
        assertTextInElement("firstname", "Test");
        assertTextInElement("lastname", "User");
    }

    public void testSynchroniseChanges_CreateUniqueActiveLocalUser()
    {
        _doInitialSync();

        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        checkCheckbox("active");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", LOCAL_DIRECTORY_NAME);

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("testuser");

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a new user has been created
        gotoViewPrincipal("testuser", LOCAL_DIRECTORY_NAME);
        assertTextInElement("email", "testuser@atlassian.com");
        assertCheckboxSelected("active");
        assertTextInElement("firstname", "Test");
        assertTextInElement("lastname", "User");
    }

    public void testSynchroniseChanges_CreateUniqueInactiveLocalUser()
    {
        _doInitialSync();

        gotoAddPrincipal();

        setTextField("email", "testuser@atlassian.com");
        uncheckCheckbox("active");
        setTextField("name", "testuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", LOCAL_DIRECTORY_NAME);

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("testuser");

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a new user has been created
        gotoViewPrincipal("testuser", LOCAL_DIRECTORY_NAME);
        assertTextInElement("email", "testuser@atlassian.com");
        assertCheckboxNotSelected("active");
        assertTextInElement("firstname", "Test");
        assertTextInElement("lastname", "User");
    }

    public void testSynchroniseChanges_CreateMaskingUser()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoAddPrincipal();

        setTextField("email", "existinguser3@atlassian.com");
        setTextField("name", "existinguser3");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        selectOption("directoryID", REMOTE_DIRECTORY1_NAME);

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("existinguser3");

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a user has been updated
        gotoViewPrincipal("existinguser3", LOCAL_DIRECTORY_NAME);
        assertTextFieldEquals("email", "existinguser3@atlassian.com");
    }

    public void testSynchroniseChanges_CreateMaskedUser()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoAddPrincipal();

        setTextField("email", "existinguser1dir2@atlassian.com");
        setTextField("name", "existinguser1");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "Test");
        setTextField("lastname", "User in Dir 2");
        selectOption("directoryID", REMOTE_DIRECTORY2_NAME);

        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent("existinguser1");

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check the new user can not be seen
        gotoViewPrincipal("existinguser1", LOCAL_DIRECTORY_NAME);
        assertTextFieldEquals("email", "existinguser1@atlassian.com");
    }

    public void testSynchroniseChanges_UpdateApplicationForcesFullSync()
    {
        _switchToCrowdAt(HOST2_PATH);

        // updating the application configuration must force a full sync, because aliases (if any) are not
        // deleted from the database when alias support is disabled, and therefore they become active
        // again just after alias support is re-enabled
        _enableAliasingForApplication(APPLICATION_NAME);

        _switchToCrowdAt(HOST_PATH);

        // Sync changes. Incremental sync is not possible, fallback to full sync
        assertEquals(SynchronisationMode.FULL, synchroniseDirectory());
    }

    public void testSynchroniseChanges_ChangeUserAliasForcesFullSync()
    {
        _switchToCrowdAt(HOST2_PATH);

        _enableAliasingForApplication(APPLICATION_NAME);

        _switchToCrowdAt(HOST_PATH);

        assertNotNull(synchroniseDirectory()); // everything up to here is test setup

        _switchToCrowdAt(HOST2_PATH);

        _setUserAliasForApplication("existinguser1", "useralias", REMOTE_DIRECTORY1_NAME, LOOP_APPLICATION_ID);

        _switchToCrowdAt(HOST_PATH);

        // Sync changes. Incremental sync is not possible, fallback to full sync
        assertEquals(SynchronisationMode.FULL, synchroniseDirectory());

        // Check that user exists with the alias, but not with its actual name
        gotoViewPrincipal("useralias", LOCAL_DIRECTORY_NAME);
        assertWarningAndErrorNotPresent();
        assertTextInElement("email", "existinguser1@atlassian.com");

        gotoViewPrincipal("existinguser1", LOCAL_DIRECTORY_NAME);
        assertWarningPresent();
    }

    public void testSynchroniseChanges_RemoveAliasedUserForcesFullSync()
    {
        _switchToCrowdAt(HOST2_PATH);

        _enableAliasingForApplication(APPLICATION_NAME);

        _setUserAliasForApplication("existinguser1", "useralias", REMOTE_DIRECTORY1_NAME, LOOP_APPLICATION_ID);

        _switchToCrowdAt(HOST_PATH);

        assertNotNull(synchroniseDirectory()); // everything up to here is test setup

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser1", REMOTE_DIRECTORY1_NAME);

        clickLink("remove-principal");

        setWorkingForm("remove-user-form");

        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes. Incremental sync is not possible, fallback to full sync
        assertEquals(SynchronisationMode.FULL, synchroniseDirectory());

        // Check that neither the user or its alias exist
        gotoViewPrincipal("existinguser1", LOCAL_DIRECTORY_NAME);
        assertWarningPresent();

        gotoViewPrincipal("useralias", LOCAL_DIRECTORY_NAME);
        assertWarningPresent();
    }

    public void testSynchroniseChanges_RemoveUniqueRemoteUser()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser1", REMOTE_DIRECTORY1_NAME);

        clickLink("remove-principal");

        setWorkingForm("remove-user-form");

        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that the user has been removed
        gotoViewPrincipal("existinguser1", LOCAL_DIRECTORY_NAME);
        assertWarningPresent();
    }

    public void testSynchroniseChanges_RemoveMaskingUser()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser2", REMOTE_DIRECTORY1_NAME);

        clickLink("remove-principal");

        setWorkingForm("remove-user-form");

        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that the user has been updated to masked user
        gotoViewPrincipal("existinguser2", LOCAL_DIRECTORY_NAME);
        assertTextFieldEquals("email", "existinguser2dir2@atlassian.com");

        // Check that membership unique to masking user has been removed
        clickLink("user-groups-tab");
        assertThat(namesOf(getGroupTableContents()),
                containsInAnyOrder("existinggroup2", "dir2group"));
    }

    public void testSynchroniseChanges_RemoveMaskedUser()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser2", REMOTE_DIRECTORY2_NAME);

        clickLink("remove-principal");

        setWorkingForm("remove-user-form");

        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that nothing has changed
        gotoViewPrincipal("existinguser2", LOCAL_DIRECTORY_NAME);
        assertTextFieldEquals("email", "existinguser2@atlassian.com");

        // Check that membership unique to masked user has been removed
        clickLink("user-groups-tab");
        assertThat(namesOf(getGroupTableContents()),
                containsInAnyOrder("existinggroup2", "dir1group"));
    }

    public void testSynchroniseChanges_UpdateUniqueRemoteUser()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser1", REMOTE_DIRECTORY1_NAME);

        setWorkingForm("updateprincipalForm");

        setTextField("email", "updateduser@atlassian.com");
        uncheckCheckbox("active");
        setTextField("firstname", "Updated");
        setTextField("lastname", "Lastname");

        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that user has been updated
        gotoViewPrincipal("existinguser1", LOCAL_DIRECTORY_NAME);
        assertTextFieldEquals("email", "updateduser@atlassian.com");
        assertCheckboxNotSelected("active");
        assertTextFieldEquals("firstname", "Updated");
        assertTextFieldEquals("lastname", "Lastname");
    }

    public void testSynchroniseChanges_UpdateUniqueLocalUser()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST_PATH);

        gotoViewPrincipal("existinguser1", LOCAL_DIRECTORY_NAME);

        setWorkingForm("updateprincipalForm");

        setTextField("email", "updateduser@atlassian.com");
        uncheckCheckbox("active");
        setTextField("firstname", "Updated");
        setTextField("lastname", "Lastname");

        submit();

        assertErrorNotPresent();

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        _switchToCrowdAt(HOST2_PATH);

        // Check that user has been updated
        gotoViewPrincipal("existinguser1", REMOTE_DIRECTORY1_NAME);
        assertTextFieldEquals("email", "updateduser@atlassian.com");
        assertCheckboxNotSelected("active");
        assertTextFieldEquals("firstname", "Updated");
        assertTextFieldEquals("lastname", "Lastname");
    }

    public void testSynchroniseChanges_UpdateMaskingUser()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser2", REMOTE_DIRECTORY1_NAME);

        setWorkingForm("updateprincipalForm");

        setTextField("email", "updateduser@atlassian.com");

        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that user has been updated
        gotoViewPrincipal("existinguser2", LOCAL_DIRECTORY_NAME);
        assertTextFieldEquals("email", "updateduser@atlassian.com");
    }

    public void testSynchroniseChanges_UpdateMaskedUser()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser2", REMOTE_DIRECTORY2_NAME);

        setWorkingForm("updateprincipalForm");

        setTextField("email", "updateduser@atlassian.com");

        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that user has not been updated
        gotoViewPrincipal("existinguser2", LOCAL_DIRECTORY_NAME);
        assertTextFieldEquals("email", "existinguser2@atlassian.com");
    }

    public void testSynchroniseChanges_CreateUniqueRemoteGroup()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoAddGroup();

        setTextField("name", "test-group");
        setTextField("description", "Crowd Test Group");
        selectOption("directoryID", REMOTE_DIRECTORY1_NAME);

        submit();

        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent("test-group");

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a new group has been created
        gotoViewGroup("test-group", LOCAL_DIRECTORY_NAME);
        assertTextInElement("description", "Crowd Test Group");
    }

    public void testSynchroniseChanges_CreateUniqueLocalGroup()
    {
        _doInitialSync();

        gotoAddGroup();

        setTextField("name", "test-group");
        setTextField("description", "Crowd Test Group");
        selectOption("directoryID", LOCAL_DIRECTORY_NAME);

        submit();

        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent("test-group");

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a new group has been created
        gotoViewGroup("test-group", LOCAL_DIRECTORY_NAME);
        assertTextInElement("description", "Crowd Test Group");
    }

    public void testSynchroniseChanges_CreateMaskingGroup()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoAddGroup();

        setTextField("name", "existinggroup3");
        setTextField("description", "Dir1");
        selectOption("directoryID", REMOTE_DIRECTORY1_NAME);

        submit();

        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent("existinggroup3");

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check the new group can be seen
        searchAllGroups();

        // Check that a group has been updated
        gotoViewGroup("existinggroup3", LOCAL_DIRECTORY_NAME);
        assertTextInElement("description", "Dir1");
    }

    public void testSynchroniseChanges_CreateMaskedGroup()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoAddGroup();

        setTextField("name", "existinggroup1");
        setTextField("description", "Dir2");
        selectOption("directoryID", REMOTE_DIRECTORY2_NAME);

        submit();

        assertKeyPresent("menu.viewgroup.label");
        assertTextPresent("existinggroup1");

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check the new group can not be seen
        gotoViewGroup("existinggroup1", LOCAL_DIRECTORY_NAME);
        assertTextInElement("description", "Dir1");
    }

    public void testSynchroniseChanges_RemoveUniqueRemoteGroup()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewGroup("dir1group", REMOTE_DIRECTORY1_NAME);

        clickLink("remove-group");

        setWorkingForm(0);
        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check the group has been removed
        gotoViewGroup("dir1group", LOCAL_DIRECTORY_NAME);
        assertWarningPresent();
    }

    public void testSynchroniseChanges_RemoveMaskingGroup()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewGroup("existinggroup2", REMOTE_DIRECTORY1_NAME);

        clickLink("remove-group");

        setWorkingForm(0);
        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check the previously masked group is now visible
        gotoViewGroup("existinggroup2", LOCAL_DIRECTORY_NAME);
        assertTextInElement("description", "Dir2");

        // Check that unique children have been deleted
        clickLink("view-group-users");
        assertThat(getGroupMemberGroupNames(),
                containsInAnyOrder("sharedgroup", "dir2group"));

        // Check that unique parents have been deleted
        gotoViewGroup("existinggroup1", LOCAL_DIRECTORY_NAME);
        clickLink("view-group-users");
        assertTableNotPresent("view-group-groups");
    }

    public void testSynchroniseChanges_RemoveMaskedGroup()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewGroup("existinggroup2", REMOTE_DIRECTORY2_NAME);

        clickLink("remove-group");

        setWorkingForm(0);
        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that group has not changed
        gotoViewGroup("existinggroup2", LOCAL_DIRECTORY_NAME);
        assertTextInElement("description", "Dir1");

        // Check that unique children have been deleted
        clickLink("view-group-users");
        assertThat(getGroupMemberGroupNames(),
                containsInAnyOrder("sharedgroup", "dir1group"));

        // Check that unique parents have been deleted
        gotoViewGroup("existinggroup3", LOCAL_DIRECTORY_NAME);
        clickLink("view-group-users");
        assertTableNotPresent("view-group-groups");
    }

    public void testSynchroniseChanges_UpdateUniqueRemoteGroup()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewGroup("existinggroup1", REMOTE_DIRECTORY1_NAME);
        setWorkingForm("groupForm");
        setTextField("description", "Updated");
        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that the group description has been updated
        gotoViewGroup("existinggroup1", LOCAL_DIRECTORY_NAME);
        assertTextInElement("description", "Updated");
    }

    public void testSynchroniseChanges_UpdateMaskingGroup()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewGroup("existinggroup2", REMOTE_DIRECTORY1_NAME);
        setWorkingForm("groupForm");
        setTextField("description", "Updated");
        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that the group description has been updated
        gotoViewGroup("existinggroup2", LOCAL_DIRECTORY_NAME);
        assertTextInElement("description", "Updated");
    }

    public void testSynchroniseChanges_UpdateMaskedGroup()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewGroup("existinggroup2", REMOTE_DIRECTORY2_NAME);
        setWorkingForm("groupForm");
        setTextField("description", "Updated");
        submit();

        assertErrorNotPresent();

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that the group description has not been updated
        gotoViewGroup("existinggroup2", LOCAL_DIRECTORY_NAME);
        assertTextInElement("description", "Dir1");
    }

    public void testSynchroniseChanges_CreateUniqueRemoteUserMembership() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser1", REMOTE_DIRECTORY1_NAME);
        clickLink("user-groups-tab");

        clickButton("addGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        setTextField("searchString", "existinggroup2");
        submit("searchButton");
        waitForElementById("existinggroup2");

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", "existinggroup2");

        clickButtonWithText(getText("picker.addselected.groups.label"));

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a membership has been created
        gotoViewPrincipal("existinguser1", LOCAL_DIRECTORY_NAME);
        clickLink("user-groups-tab");

        assertTextInTable("groupsTable", "existinggroup2");
    }

    public void testSynchroniseChanges_CreateMaskingUserMembership() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser2", REMOTE_DIRECTORY1_NAME);
        clickLink("user-groups-tab");

        clickButton("addGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        setTextField("searchString", "sharedgroup");
        submit("searchButton");
        waitForElementById("sharedgroup");

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", "sharedgroup");

        clickButtonWithText(getText("picker.addselected.groups.label"));

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a membership has been created
        gotoViewPrincipal("existinguser2", LOCAL_DIRECTORY_NAME);
        clickLink("user-groups-tab");

        assertThat(namesOf(getGroupTableContents()),
                containsInAnyOrder("dir1group", "dir2group", "existinggroup2", "sharedgroup"));
    }

    public void testSynchroniseChanges_CreateMaskedUserMembership() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser2", REMOTE_DIRECTORY2_NAME);
        clickLink("user-groups-tab");

        clickButton("addGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        setTextField("searchString", "sharedgroup");
        submit("searchButton");
        waitForElementById("sharedgroup");

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", "sharedgroup");

        clickButtonWithText(getText("picker.addselected.groups.label"));

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a membership has been created
        gotoViewPrincipal("existinguser2", LOCAL_DIRECTORY_NAME);
        clickLink("user-groups-tab");

        assertThat(namesOf(getGroupTableContents()),
                containsInAnyOrder("dir1group", "dir2group", "existinggroup2", "sharedgroup"));
    }

    public void testSynchroniseChanges_RemoveUniqueRemoteUserMembership() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser1", REMOTE_DIRECTORY1_NAME);
        clickLink("user-groups-tab");

        clickButton("removeGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        setTextField("searchString", "existinggroup1");
        submit("searchButton");
        waitForElementById("existinggroup1");

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", "existinggroup1");

        clickButtonWithText(getText("picker.removeselected.groups.label"));

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a membership has been removed
        gotoViewPrincipal("existinguser1", LOCAL_DIRECTORY_NAME);
        clickLink("user-groups-tab");

        assertThat(namesOf(getGroupTableContents()),
                containsInAnyOrder("dir1group"));
    }

    /**
     * This tests demonstrates that a failed incremental synchronisation falls back to a full synchronisation
     * in the first attempt (CWD-3188). It exploits the bug in CWD-3182 to cause the incremental synchronisation to
     * fail, but it won't work once CWD-3182 is fixed. Once that happens, the assertions at the end of the test
     * will require some changes, but the test should be kept to prevent regressions of CWD-3182.
     *
     * This test is overridden by the subclass because it is only relevant to incremental synchronisation.
     */
    public void testSynchroniseChanges_RemoveUniqueRemoteUserMembershipAndRecreateUniqueRemoteUserMembership()
        throws Exception
    {
        checkState(expectedMode == SynchronisationMode.INCREMENTAL);
        final String groupName = "existinggroup1";
        final String userName = "existinguser1";

        _switchToCrowdAt(HOST2_PATH);

        // first remove the membership
        gotoViewPrincipal(userName, REMOTE_DIRECTORY1_NAME);
        clickLink("user-groups-tab");
        assertTextInTable("groupsTable", groupName);  // initially the user is a member of the group

        clickButton("removeGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        setTextField("searchString", groupName);
        submit("searchButton");
        waitForElementById(groupName);

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", groupName);
        clickButtonWithText(getText("picker.removeselected.groups.label"));

        // then add the membership again
        gotoViewPrincipal(userName, REMOTE_DIRECTORY1_NAME);
        clickLink("user-groups-tab");
        assertTextNotInTable("groupsTable", groupName);  // the user is not a member of the group at this point

        clickButton("addGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        setTextField("searchString", groupName);
        submit("searchButton");
        waitForElementById(groupName);

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", groupName);
        clickButtonWithText(getText("picker.addselected.groups.label"));

        // verify the user has been added to the group
        gotoViewPrincipal(userName, REMOTE_DIRECTORY1_NAME);
        clickLink("user-groups-tab");
        assertTextInTable("groupsTable", groupName);  // initially the user is a member of the group

        // Sync changes
        _switchToCrowdAt(HOST_PATH);

        assertEquals("Incremental synchronisation should fail due to CWD-3182 and fallback to a full synchronisation",
                     SynchronisationMode.FULL, synchroniseDirectory());

        // Check that the membership is still present
        gotoViewPrincipal(userName, LOCAL_DIRECTORY_NAME);
        clickLink("user-groups-tab");
        assertTextInTable("groupsTable", groupName);

        assertEquals("Subsequent synchronisations should be incremental again",
                     expectedMode, synchroniseDirectory());
    }

    public void testSynchroniseChanges_RemoveMaskingUserMembership() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser2", REMOTE_DIRECTORY1_NAME);
        clickLink("user-groups-tab");

        clickButton("removeGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        submit("searchButton");
        waitForElementById("existinggroup2");

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", "dir1group");
        checkCheckbox("selectedEntityNames", "existinggroup2");

        clickButtonWithText(getText("picker.removeselected.groups.label"));

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a membership has been removed
        gotoViewPrincipal("existinguser2", LOCAL_DIRECTORY_NAME);
        clickLink("user-groups-tab");

        assertThat(namesOf(getGroupTableContents()),
                containsInAnyOrder("existinggroup2", "dir2group"));
    }

    public void testSynchroniseChanges_RemoveMaskedUserMembership() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewPrincipal("existinguser2", REMOTE_DIRECTORY2_NAME);
        clickLink("user-groups-tab");

        clickButton("removeGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        submit("searchButton");
        waitForElementById("existinggroup2");

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", "dir2group");
        checkCheckbox("selectedEntityNames", "existinggroup2");

        clickButtonWithText(getText("picker.removeselected.groups.label"));

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a membership has been removed
        gotoViewPrincipal("existinguser2", LOCAL_DIRECTORY_NAME);
        clickLink("user-groups-tab");

        assertThat(namesOf(getGroupTableContents()),
                containsInAnyOrder("existinggroup2", "dir1group"));
    }

    public void testSynchroniseChanges_CreateUniqueRemoteGroupMembership() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewGroup("existinggroup1", REMOTE_DIRECTORY1_NAME);
        clickLink("view-group-users");

        clickButton("addGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        setTextField("searchString", "existinggroup4");
        submit("searchButton");
        waitForElementById("existinggroup4");

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", "existinggroup4");

        clickButtonWithText(getText("picker.addselected.groups.label"));

        assertTextPresent(getText("updatesuccessful.label"));

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a membership has been created
        gotoViewGroup("existinggroup1", LOCAL_DIRECTORY_NAME);
        clickLink("view-group-users");

        assertThat(getGroupMemberGroupNames(),
                containsInAnyOrder("existinggroup2", "existinggroup4"));
    }

    public void testSynchroniseChanges_CreateMaskingGroupMembership() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewGroup("existinggroup2", REMOTE_DIRECTORY1_NAME);
        clickLink("view-group-users");

        clickButton("addGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        setTextField("searchString", "existinggroup4");
        submit("searchButton");
        waitForElementById("existinggroup4");

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", "existinggroup4");

        clickButtonWithText(getText("picker.addselected.groups.label"));

        assertTextPresent(getText("updatesuccessful.label"));

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a membership has been created
        gotoViewGroup("existinggroup2", LOCAL_DIRECTORY_NAME);
        clickLink("view-group-users");

        assertThat(getGroupMemberGroupNames(),
                containsInAnyOrder("dir1group", "dir2group",
                        "sharedgroup", "existinggroup4"));
    }

    public void testSynchroniseChanges_CreateMaskedGroupMembership() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewGroup("existinggroup2", REMOTE_DIRECTORY2_NAME);
        clickLink("view-group-users");

        clickButton("addGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        setTextField("searchString", "existinggroup3");
        submit("searchButton");
        waitForElementById("existinggroup3");

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", "existinggroup3");

        clickButtonWithText(getText("picker.addselected.groups.label"));

        assertTextPresent(getText("updatesuccessful.label"));

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a membership has been created
        gotoViewGroup("existinggroup2", LOCAL_DIRECTORY_NAME);
        clickLink("view-group-users");

        assertThat(getGroupMemberGroupNames(),
                containsInAnyOrder("dir1group", "dir2group",
                        "sharedgroup", "existinggroup3"));
    }

    public void testSynchroniseChanges_RemoveUniqueRemoteGroupMembership() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewGroup("existinggroup1", REMOTE_DIRECTORY1_NAME);
        clickLink("view-group-users");

        clickButton("removeGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        setTextField("searchString", "existinggroup2");
        submit("searchButton");
        waitForElementById("existinggroup2");

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", "existinggroup2");

        clickButtonWithText(getText("picker.removeselected.groups.label"));

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a membership has been removed
        gotoViewGroup("existinggroup1", LOCAL_DIRECTORY_NAME);
        clickLink("view-group-users");

        assertTableNotPresent("view-group-groups");
    }

    public void testSynchroniseChanges_RemoveMaskedGroupMembership() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewGroup("existinggroup2", REMOTE_DIRECTORY1_NAME);
        clickLink("view-group-users");

        clickButton("removeGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        submit("searchButton");
        waitForElementById("sharedgroup");

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", "sharedgroup");
        checkCheckbox("selectedEntityNames", "dir1group");

        clickButtonWithText(getText("picker.removeselected.groups.label"));

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a membership has been removed
        gotoViewGroup("existinggroup2", LOCAL_DIRECTORY_NAME);
        clickLink("view-group-users");

        assertThat(getGroupMemberGroupNames(),
                containsInAnyOrder("sharedgroup", "dir2group"));
    }

    public void testSynchroniseChanges_RemoveMaskingGroupMembership() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewGroup("existinggroup2", REMOTE_DIRECTORY2_NAME);
        clickLink("view-group-users");

        clickButton("removeGroups");
        waitForElementById("browseentities");

        setWorkingForm("browseentities");
        submit("searchButton");
        waitForElementById("sharedgroup");

        setWorkingForm("selectentities");
        checkCheckbox("selectedEntityNames", "sharedgroup");
        checkCheckbox("selectedEntityNames", "dir2group");

        clickButtonWithText(getText("picker.removeselected.groups.label"));

        _switchToCrowdAt(HOST_PATH);

        // Sync changes
        assertEquals(expectedMode, synchroniseDirectory());

        // Check that a membership has been removed
        gotoViewGroup("existinggroup2", LOCAL_DIRECTORY_NAME);
        clickLink("view-group-users");

        assertThat(getGroupMemberGroupNames(),
                containsInAnyOrder("sharedgroup", "dir1group"));
    }

    public void testSynchroniseAll_ClientDirectoryConfigurationChanged() throws Exception
    {
        _doInitialSync();

        gotoBrowseDirectories();
        clickLinkWithExactText(LOCAL_DIRECTORY_NAME);
        assertKeyPresent("menu.viewdirectory.label", Arrays.asList(LOCAL_DIRECTORY_NAME));

        clickButtonWithText(getText("update.label") + " »");

        assertEquals(SynchronisationMode.FULL, synchroniseDirectory());
    }

    public void testSynchroniseAll_ServerDirectoryConfigurationChanged() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoBrowseDirectories();
        clickLinkWithExactText(REMOTE_DIRECTORY2_NAME);
        assertKeyPresent("menu.viewdirectory.label", Arrays.asList(REMOTE_DIRECTORY2_NAME));

        clickButtonWithText(getText("update.label") + " »");

        _switchToCrowdAt(HOST_PATH);

        assertEquals(SynchronisationMode.FULL, synchroniseDirectory());
    }

    public void testSynchroniseAll_ServerDirectoryDeleted() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoBrowseDirectories();
        clickLinkWithExactText(REMOTE_DIRECTORY2_NAME);
        assertKeyPresent("menu.viewdirectory.label", Arrays.asList(REMOTE_DIRECTORY2_NAME));

        clickLinkWithKey("directory.remove.title");
        clickButtonWithText(getText("continue.label") + " »");

        assertKeyPresent("updatesuccessful.label");

        _switchToCrowdAt(HOST_PATH);

        assertEquals(SynchronisationMode.FULL, synchroniseDirectory());
    }

    public void testSynchroniseAll_ServerXMLRestored() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        restoreCrowdFromXML("crowdwithcrowdremote.xml");

        _switchToCrowdAt(HOST_PATH);

        assertEquals(SynchronisationMode.FULL, synchroniseDirectory());
    }

    public void testSynchroniseAll_ServerApplicationDirectoryAdded() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewApplication(APPLICATION_NAME);
        clickLink("application-directories");
        clickButton("add-directory");

        assertKeyPresent("updatesuccessful.label");

        _switchToCrowdAt(HOST_PATH);

        assertEquals(SynchronisationMode.FULL, synchroniseDirectory());
    }

    public void testSynchroniseAll_ServerApplicationDirectoryRemoved() throws Exception
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);

        gotoViewApplication(APPLICATION_NAME);
        clickLink("application-directories");

        clickLink("down-32769");

        assertKeyPresent("updatesuccessful.label");

        _switchToCrowdAt(HOST_PATH);

        assertEquals(SynchronisationMode.FULL, synchroniseDirectory());
    }

    public void testSynchroniseAll_ServerApplicationHasUncachedDirectory()
    {
        _doInitialSync();

        _switchToCrowdAt(HOST2_PATH);
        restoreCrowdFromXML("crowdwithcrowdremoteuncached.xml");
        _switchToCrowdAt(HOST_PATH);
        assertEquals(SynchronisationMode.FULL, synchroniseDirectory());

        assertEquals("Subsequent synchronisation was not full when application contains an uncache directory",
                     SynchronisationMode.FULL, synchroniseDirectory());
    }

    private void searchAllGroups()
    {
        gotoBrowseGroups();
        setWorkingForm("browsegroups");
        selectOption("directoryID", LOCAL_DIRECTORY_NAME);
        submit();
    }

    private void _setUserAliasForApplication(String username, String alias, String directoryName, long applicationId)
    {
        // set an alias for a user
        gotoViewPrincipal(username, directoryName);
        clickLink("user-applications-tab");
        setTextField("alias-appid-" + applicationId, alias);
        submit();

        assertKeyPresent("menu.viewprincipal.label");
        assertTextPresent(alias);
    }

    private void _enableAliasingForApplication(String applicationName)
    {
        gotoViewApplication(applicationName);
        clickLink("application-options");
        checkCheckbox("aliasingEnabled");
        submit();
    }

    private SynchronisationMode synchroniseDirectory()
    {
        return dbCachingTestHelper.synchroniseDirectory(LOCAL_DIRECTORY_NAME, MAX_SYNC_WAIT_TIME_MS).getMode();
    }

    // Copied from DelegatedDirectoryWithNestedGroupsTest; there should be somewhere common for this
    private List<String> getGroupMemberGroupNames()
    {
        return scrapeTable("view-group-groups",
                ImmutableList.of("Group Name", "Description", "Active"),
                new Function<List<String>, String>()
                {
                    @Override
                    public String apply(List<String> input)
                    {
                        return input.get(0);
                    }
                });
    }
}
