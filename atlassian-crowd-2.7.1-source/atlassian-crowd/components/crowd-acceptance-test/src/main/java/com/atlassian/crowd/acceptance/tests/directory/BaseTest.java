package com.atlassian.crowd.acceptance.tests.directory;

import java.io.FileNotFoundException;
import java.util.Date;
import java.util.Properties;

import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelperImpl;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.password.encoder.AtlassianSHA1PasswordEncoder;
import com.atlassian.crowd.password.encoder.PlaintextPasswordEncoder;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.spring.container.SpringContainerContext;

import org.hibernate.SessionFactory;
import org.quartz.Scheduler;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import static com.atlassian.crowd.acceptance.tests.directory.BasicTest.getImplementation;
import static com.atlassian.crowd.acceptance.tests.directory.BasicTest.getRawImplementation;

import static org.mockito.Mockito.mock;

/**
 * Sets properties and connects to a directory for testing. The RemoteDirectory interface is exercised by a subclass.
 *
 * This now includes support for the database to allow testing of internal-backed LDAP directories.
 */
public abstract class BaseTest extends AbstractTransactionalDataSourceSpringContextTests
{
    protected DirectoryImpl directory;
    protected MockDirectoryManager directoryManager;
    private LDAPPropertiesHelperImpl ldapPropertiesHelperImpl;
    protected SessionFactory sessionFactory;

    private String directoryConfigFile; // If not set by subclass, will be loaded from system property directory.config.file

    protected BaseTest()
    {
    }

    protected BaseTest(String name)
    {
        super(name);
    }

    @Override
    protected void customizeBeanFactory(DefaultListableBeanFactory beanFactory)
    {
//        beanFactory.registerSingleton("applicationDao", new Mock(ApplicationDAO.class).proxy());
//        beanFactory.registerSingleton("directoryDao", new Mock(DirectoryDao.class).proxy());
        beanFactory.registerSingleton("scheduler", mock(Scheduler.class));
    }

    protected String[] getConfigLocations()
    {
        return new String[]{
                "classpath:/remoteDirectoryApplicationContext-config.xml",
                "classpath:/applicationContext-CrowdEncryption.xml",
                "classpath:/applicationContext-CrowdUtils.xml",
                "classpath:/applicationContext-CrowdDAO.xml",
                "classpath:/applicationContext-config.xml"};
    }

    private void addPlainTextPasswordEncoder()
    {
        PasswordEncoderFactory passwordEncoderFactory = (PasswordEncoderFactory) ContainerManager.getComponent("passwordEncoderFactory");
        passwordEncoderFactory.addEncoder(new PlaintextPasswordEncoder());
    }

    private void addAtlassianSha1PasswordEncoder()
    {
        PasswordEncoderFactory passwordEncoderFactory = (PasswordEncoderFactory) ContainerManager.getComponent("passwordEncoderFactory");
        passwordEncoderFactory.addEncoder(new AtlassianSHA1PasswordEncoder());
    }

    /**
     * Makes our directory available, via the MockDirectoryManager to the caching and monitoring subsystems.
     * @param dir
     */
    private void setDirectoryInDirectoryManager(DirectoryImpl dir)
    {
        directoryManager = (MockDirectoryManager) ContainerManager.getComponent("directoryManager");
        directoryManager.setDirectory(dir);
    }

    /**
     * Configures a runtime Crowd object that connects to the Sun instance.
     */
    protected void configureDirectory(Properties directorySettings)
    {
        if (directory == null)
        {
            DirectoryImpl directoryTemplate = DirectoryTestHelper.createDirectoryTemplate(directorySettings, ldapPropertiesHelperImpl, false);

            DirectoryDao directoryDao = (DirectoryDao) ContainerManager.getComponent("directoryDao");
            directory = new DirectoryImpl(directoryDao.add(directoryTemplate));
            sessionFactory.getCurrentSession().flush();

            // Set the directoryId before adding directory to be available for mock directory manager
            InternalEntityTemplate template = new InternalEntityTemplate(directory.getId(), directory.getName(), true, new Date(), new Date());
            DirectoryImpl templateForMockDirectoryManager = new DirectoryImpl(template);
            templateForMockDirectoryManager.updateDetailsFrom(directory);
            setDirectoryInDirectoryManager(templateForMockDirectoryManager);
        }
    }

    public void setupSpring()
    {
        // Setup the Container Context that some of our objects use to get access to managers, helpers etc.
        SpringContainerContext springContainerContext = new SpringContainerContext();
        springContainerContext.setApplicationContext(applicationContext);
        ContainerManager.getInstance().setContainerContext(springContainerContext);
    }

    @Override
    protected void onSetUpBeforeTransaction() throws Exception
    {
        super.onSetUpBeforeTransaction();

        setupSpring();

        // make sure plain text password encoder is present for InternalDirectory
        addPlainTextPasswordEncoder();
        // make sure Atlassian Sha1 password encoder is present for DBCachingRemoteDirectory
        addAtlassianSha1PasswordEncoder();

        /* Clean up the database before a test run */
        deleteFromTables(DirectoryTestHelper.TABLE_NAMES);
    }

    @Override
    protected void onSetUpInTransaction() throws Exception
    {
        super.onSetUpInTransaction();

        // load the directory configuration from the config file specified in the system properties.
        if (getDirectoryConfigFile() == null)
        {
            final String configFileFromSystemProperty = System.getProperty("directory.config.file");
            if (configFileFromSystemProperty == null)
            {
                throw new FileNotFoundException("Directory config file not specified. Have you set system property -Ddirectory.config.file?");
            }
            setDirectoryConfigFile(configFileFromSystemProperty);
        }
        logger.info("Attempting to load " + getDirectoryConfigFile());
        Properties properties = com.atlassian.crowd.acceptance.utils.DirectoryTestHelper.getConfigProperties(getDirectoryConfigFile());

        // build our Crowd directory/connection object
        configureDirectory(properties);

        // no point running tests if we can't connect to server
        getRawImplementation(directory).testConnection();

        removeTestData();
        loadTestData();
    }

    @Override
    public void onTearDown() throws Exception
    {
        removeTestData();

        super.onTearDown();
    }

    /**
     * Called before loadTestData() AND after every test run to remove data added for test.
     */
    protected abstract void removeTestData() throws DirectoryInstantiationException;

    /**
     * Called before every test run to add data needed for test.
     */
    protected abstract void loadTestData() throws Exception;


    public void setLdapPropertiesHelper(LDAPPropertiesHelperImpl ldapPropertiesHelperImpl)
    {
        // Spring's auto-wire sets this up for us.
        this.ldapPropertiesHelperImpl = ldapPropertiesHelperImpl;
    }

    public void setDirectoryConfigFile(String directoryConfigFile)
    {
        this.directoryConfigFile = directoryConfigFile;
    }

    public String getDirectoryConfigFile()
    {
        return directoryConfigFile;
    }

    protected RemoteDirectory getRemoteDirectory() throws DirectoryInstantiationException
    {
        return getImplementation(directory);
    }

    public void setSessionFactory(final SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Delete tables without setting the 'zapped' flag so we can commit.
     */
    @Override
    protected void deleteFromTables(String[] names)
    {
        DirectoryTestHelper.deleteFromTables(names, this.jdbcTemplate);
    }

}
