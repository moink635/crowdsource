package com.atlassian.crowd.acceptance.tests.applications.demo;

import com.atlassian.crowd.acceptance.tests.ApplicationAcceptanceTestCase;
import junit.framework.AssertionFailedError;

public class DemoAcceptanceTestCase extends ApplicationAcceptanceTestCase
{
    // users that have access to both Crowd and Demo applications
    protected static final String TEST_USER = "user";
    protected static final String TEST_PW = "password";

    // user that has access only to the Demo application
    protected static final String DEMO_USER = "demo";
    protected static final String DEMO_PW = "password";

    // user that has access only to the Crowd application
    protected static final String CROWD_USER = "immutable";
    protected static final String CROWD_PW = "password";

    protected void _loginAdminUser()
    {
        beginAt(baseUrl);
        assertKeyPresent("login.title");
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        // quick verification
        assertLinkPresentWithKey("menu.logout.label");
    }

    protected void _logout()
    {
        gotoPage("/console/logoff.action");
        assertKeyPresent("login.title");
    }

    protected String getResourceBundleName()
    {
        return "com.atlassian.crowd.security.demo.action.BaseAction";
    }

    protected String getApplicationName()
    {
        return "demo";
    }

    protected String getLocalTestPropertiesFileName()
    {
        return "localtest.properties";
    }

    protected void gotoViewUser(final String username)
    {
        gotoPage("/secure/user/viewuser.action?name="+username);
    }

    protected String loginToDemo(String username, String password)
    {
        // force logoff
        _logout();

        assertKeyPresent("login.title");
        setTextField("username", username);
        setTextField("password", password);
        submit();

        // quick verification
        try
        {
            assertLinkPresentWithKey("menu.logout.label");
            return getElementTextByXPath("//span[@id='userFullName']");
        }
        catch (AssertionFailedError e)
        {
            return null;
        }
    }

    protected void logoutFromDemo()
    {
        this._logout();
    }

    protected String getCurrentlyLoggedInDemoUserFullName()
    {
        gotoPage("/");

        try
        {
            return getElementTextByXPath("//span[@id='userFullName']");
        }
        catch (AssertionFailedError e)
        {
            return null;
        }
    }
}
