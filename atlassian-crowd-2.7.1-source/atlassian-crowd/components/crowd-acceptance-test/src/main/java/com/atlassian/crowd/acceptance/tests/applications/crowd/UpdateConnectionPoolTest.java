package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.crowd.embedded.impl.ConnectionPoolPropertyConstants;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.concurrent.TimeUnit;

public class UpdateConnectionPoolTest extends CrowdAcceptanceTestCase
{

    private static final String TIMEOUT_IN_SECONDS = String.valueOf(TimeUnit.SECONDS.convert(NumberUtils.toLong(ConnectionPoolPropertyConstants.DEFAULT_POOL_TIMEOUT_MS), TimeUnit.MILLISECONDS));

    public void setUp() throws Exception
    {
        super.setUp();
        _loginAdminUser();
    }

    public void testNoChange()
    {
        gotoLdapConnectionPool();
        assertKeyPresent("connectionpool.title");

        checkDefaultValues();

        submit();

        // no changes, so should be all the same
        checkDefaultValues();
        assertTextNotPresent("new value");
    }

    public void testInvalidChange()
    {
        gotoLdapConnectionPool();
        assertKeyPresent("connectionpool.title");

        checkDefaultValues();

        // update the values
        setTextField("initialSize", "blahblah");
        setTextField("maximumSize", "20");
        setTextField("timeoutInSec", "420");
        setTextField("supportedProtocol", "ssl");

        submit();

        // check error has been caught & invalid update not made
        assertErrorPresentWithKey("connectionpool.integer.invalid");
        assertErrorPresentWithKey("connectionpool.update.error");
        assertTextNotPresent("(new value: blahblah)");

        // check valid updates are made
        assertTextPresent("(new value: 20)");
        assertTextPresent("(new value: 420)");
        assertTextPresent("(new value: ssl)");

        // Check that the updated LDAP values are still in the text field
        assertTextInElement("initialSize", ConnectionPoolPropertyConstants.DEFAULT_INITIAL_POOL_SIZE);
        assertTextInElement("preferredSize", ConnectionPoolPropertyConstants.DEFAULT_PREFERRED_POOL_SIZE);
        assertTextInElement("maximumSize", "20");
        assertTextInElement("timeoutInSec", "420");
        assertTextInElement("supportedProtocol", "ssl");
        assertTextInElement("supportedAuthentication", ConnectionPoolPropertyConstants.DEFAULT_POOL_AUTHENTICATION);

        // no change should've been made
        gotoLdapConnectionPool();
        checkDefaultValues();
    }

    public void testInvalidAuthenticationType()
    {
        gotoLdapConnectionPool();
        assertKeyPresent("connectionpool.title");

        checkDefaultValues();

        // update the values
        setTextField("maximumSize", "20");
        setTextField("timeoutInSec", "420");
        setTextField("supportedProtocol", "ssl");
        setTextField("supportedAuthentication", "simple rhubarb");

        submit();

        // check error has been caught & invalid update not made
        assertErrorPresentWithKey("connectionpool.supportedAuthentication.error");
        assertErrorPresentWithKey("connectionpool.update.error");
        assertTextNotPresent("(new value: simple rhubarb)");

        // check valid updates are made
        assertTextPresent("(new value: 20)");
        assertTextPresent("(new value: 420)");
        assertTextPresent("(new value: ssl)");

        // Check that the updated LDAP values are still in the text field
        assertTextInElement("initialSize", ConnectionPoolPropertyConstants.DEFAULT_INITIAL_POOL_SIZE);
        assertTextInElement("preferredSize", ConnectionPoolPropertyConstants.DEFAULT_PREFERRED_POOL_SIZE);
        assertTextInElement("maximumSize", "20");
        assertTextInElement("timeoutInSec", "420");
        assertTextInElement("supportedProtocol", "ssl");
        assertTextInElement("supportedAuthentication", ConnectionPoolPropertyConstants.DEFAULT_POOL_AUTHENTICATION);

        // no change should've been made
        gotoLdapConnectionPool();
        checkDefaultValues();
    }

    public void testInvalidProtocolType()
    {
        gotoLdapConnectionPool();
        assertKeyPresent("connectionpool.title");

        checkDefaultValues();

        // update the values
        setTextField("initialSize", "2");
        setTextField("maximumSize", "20");
        setTextField("timeoutInSec", "420");
        setTextField("supportedProtocol", "ssl plane");

        submit();

        // check error has been caught & invalid update not made
        assertErrorPresentWithKey("connectionpool.supportedProtocol.error");
        assertErrorPresentWithKey("connectionpool.update.error");
        assertTextNotPresent("(new value: ssl plane)");

        // check valid updates are made
        assertTextPresent("(new value: 2)");
        assertTextPresent("(new value: 20)");
        assertTextPresent("(new value: 420)");

        // Check that the updated LDAP values are still in the text field
        assertTextInElement("initialSize", "2");
        assertTextInElement("preferredSize", ConnectionPoolPropertyConstants.DEFAULT_PREFERRED_POOL_SIZE);
        assertTextInElement("maximumSize", "20");
        assertTextInElement("timeoutInSec", "420");
        assertTextInElement("supportedProtocol", ConnectionPoolPropertyConstants.DEFAULT_POOL_PROTOCOL);
        assertTextInElement("supportedAuthentication", ConnectionPoolPropertyConstants.DEFAULT_POOL_AUTHENTICATION);

        // no change should've been made
        gotoLdapConnectionPool();
        checkDefaultValues();
    }

    public void testValidChange()
    {
        gotoLdapConnectionPool();
        assertKeyPresent("connectionpool.title");

        checkDefaultValues();

        // update the values
        setTextField("initialSize", "5");
        setTextField("maximumSize", "20");
        setTextField("timeoutInSec", "420");
        setTextField("supportedProtocol", "ssl");

        submit();

        // Check that the updated LDAP values are in the text field
        assertTextInElement("initialSize", "5");
        assertTextInElement("preferredSize", ConnectionPoolPropertyConstants.DEFAULT_PREFERRED_POOL_SIZE);
        assertTextInElement("maximumSize", "20");
        assertTextInElement("timeoutInSec", "420");
        assertTextInElement("supportedProtocol", "ssl");
        assertTextInElement("supportedAuthentication", ConnectionPoolPropertyConstants.DEFAULT_POOL_AUTHENTICATION);

        // check the display has been updated
        assertTextPresent("(new value: 5)");
        assertTextPresent("(new value: 20)");
        assertTextPresent("(new value: 420)");
        assertTextPresent("(new value: ssl)");
        // non-changed values don't have the new value info
        assertTextNotPresent("(new value: 10)");
        assertTextNotPresent("(new value: simple)");

        // actually made a change so reset values back to defaults
        resetToDefaults();
    }

    private void checkDefaultValues()
    {
        // Check that the LDAP labels/descriptions are displayed
        assertKeyPresent("connectionpool.initialSize.label");
        assertKeyPresent("connectionpool.initialSize.description");
        assertKeyPresent("connectionpool.preferredSize.label");
        assertKeyPresent("connectionpool.preferredSize.description");
        assertKeyPresent("connectionpool.maximumSize.label");
        assertKeyPresent("connectionpool.maximumSize.description");
        assertKeyPresent("connectionpool.timeout.label");
        assertKeyPresent("connectionpool.timeout.description");
        assertKeyPresent("connectionpool.supportedProtocol.label");
        assertKeyPresent("connectionpool.supportedProtocol.description");
        assertKeyPresent("connectionpool.supportedAuthentication.label");
        assertKeyPresent("connectionpool.supportedAuthentication.description");

        // Check that the default LDAP values are in the text field
        assertTextInElement("initialSize", ConnectionPoolPropertyConstants.DEFAULT_INITIAL_POOL_SIZE);
        assertTextInElement("preferredSize", ConnectionPoolPropertyConstants.DEFAULT_PREFERRED_POOL_SIZE);
        assertTextInElement("maximumSize", ConnectionPoolPropertyConstants.DEFAULT_MAXIMUM_POOL_SIZE);
        assertTextInElement("timeoutInSec", TIMEOUT_IN_SECONDS);
        assertTextInElement("supportedProtocol", ConnectionPoolPropertyConstants.DEFAULT_POOL_PROTOCOL);
        assertTextInElement("supportedAuthentication", ConnectionPoolPropertyConstants.DEFAULT_POOL_AUTHENTICATION);
    }

    private void resetToDefaults()
    {
        // fill form with default values
        setTextField("initialSize", ConnectionPoolPropertyConstants.DEFAULT_INITIAL_POOL_SIZE);
        setTextField("preferredSize", ConnectionPoolPropertyConstants.DEFAULT_PREFERRED_POOL_SIZE);
        setTextField("maximumSize", ConnectionPoolPropertyConstants.DEFAULT_MAXIMUM_POOL_SIZE);
        setTextField("timeoutInSec", TIMEOUT_IN_SECONDS);
        setTextField("supportedProtocol", ConnectionPoolPropertyConstants.DEFAULT_POOL_PROTOCOL);
        setTextField("supportedAuthentication", ConnectionPoolPropertyConstants.DEFAULT_POOL_AUTHENTICATION);

        submit();
    }

}
