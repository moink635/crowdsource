package com.atlassian.crowd.acceptance.tests.applications.crowd;

public class DirectoryPermissionGroupTest extends CrowdAcceptanceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("directorypermissiongrouptest.xml");
    }

    public void testUnableToModifyGroup()
    {
        gotoViewGroup("tester-group", "Test Internal Directory");

        setWorkingForm("groupForm");

        setTextField("description", "A new Group description");

        submit();

        assertTextPresent("Directory does not allow group modifications");
    }

    public void testUnableToModifyGroupMembers()
    {
        gotoViewGroup("tester-group", "Test Internal Directory");

        clickLink("view-group-users");

        assertButtonNotPresent("removeUsers");

        assertKeyPresent("group.modify.disabled");
    }

    public void testUnableToDeleteGroup()
    {
        gotoViewGroup("tester-group", "Test Internal Directory");

        clickLink("remove-group");

        submit();

        assertTextPresent("Directory does not allow group removal");
    }


    public void testUnableToAddGroup()
    {
        gotoAddGroup();

        setTextField("name", "tester-group-to-add");
        setTextField("description", "Crowd Acceptance Test Group");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertTextPresent("Directory does not allow adding of groups");
    }
}
