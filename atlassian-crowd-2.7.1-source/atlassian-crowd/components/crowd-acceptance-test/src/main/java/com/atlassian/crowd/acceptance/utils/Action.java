package com.atlassian.crowd.acceptance.utils;

/**
 * Essentially a function pointer.
 */
public interface Action
{
    /**
     * Do something. Anything.
     *
     * @throws Exception failed to do something property.
     */
    void execute() throws Exception;
}
