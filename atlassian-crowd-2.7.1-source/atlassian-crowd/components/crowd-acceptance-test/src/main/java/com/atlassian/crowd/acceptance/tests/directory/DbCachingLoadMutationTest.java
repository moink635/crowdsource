package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.acceptance.utils.AbstractDbCachingLoadTest;
import com.atlassian.crowd.acceptance.utils.LoadStatWriter;
import com.atlassian.crowd.directory.MicrosoftActiveDirectory;
import com.atlassian.crowd.directory.OpenLDAP;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.SynchronisableDirectoryProperties;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapperImpl;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelperImpl;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.password.factory.PasswordEncoderFactoryImpl;
import com.atlassian.crowd.search.ldap.ActiveDirectoryQueryTranslaterImpl;
import com.atlassian.crowd.search.ldap.LDAPQueryTranslaterImpl;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.crowd.util.PasswordHelperImpl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Tests will create and remove users/groups/memberships in the base of the test OU in order to test
 * sync performance if there has been changes made to the LDAP directory.
 * <p/>
 * The tests will leave the LDAP directory 'as is' (ie. the LDAP directory will be back in its original
 * state since any users/groups added will have been removed)
 */
public class DbCachingLoadMutationTest extends AbstractDbCachingLoadTest
{
    private static final int SMALL_NUMBER_OF_USERS = 40;
    private static final int SMALL_NUMBER_OF_GROUPS = 30;
    private static final int SMALL_INITIAL_MEMBERSHIPS = 15;

    private static final int LARGE_NUMBER_OF_USERS = 80;
    private static final int LARGE_NUMBER_OF_GROUPS = 60;
    private static final int LARGE_INITIAL_MEMBERSHIPS = 30;

    private static final String MUTANT_USER_PREFIX = "mutant-user-";
    private static final String MUTANT_GROUP_PREFIX = "mutant-group-";

    private RemoteDirectory ldapDirectory;
    private LoadStatWriter statWriter;

    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreBaseSetup();
        this.ldapDirectory = createLDAPRemoteDirectory();

        statWriter = new LoadStatWriter(getClass().getName());
    }

    @Override
    public void tearDown() throws Exception
    {
        statWriter.close();

        setScriptingEnabled(false);
        super.tearDown();
    }

    // Tests mutation on a larger set of users/groups - more so on timing and correctness of final result
    public void testLargeMutations() throws Exception
    {
        intendToModifyData();

        logger.info("Running: testLargeMutations");
        // Create the Microsoft AD that actually exists in crowd
        createLoadTestingDirectory(CONNECTOR_URL, CONNECTOR_BASEDN, CONNECTOR_USERDN, CONNECTOR_USERPW);
        synchroniseDirectory("Perform initial sync after initial directory creation");

        long addLargeUsersGroupsMemberships = _testAddLargeUsersGroupsMemberships();
        long removeLargeUsersGroupsMemberships = _testRemoveLargeUsersGroupsMemberships();

        // Print overall summary
        logger.info("");
        logger.info("======================================================================");
        logger.info("# Mutating large set of users/groups within ou that contains 10k users, 1k groups");
        logger.info("#    Adding 80 users, 60 groups, 30 memberships per user took (in seconds):\t" + addLargeUsersGroupsMemberships);
        logger.info("#    Removing 80 users, 60 groups, 30 memberships per user took (in seconds):\t" + removeLargeUsersGroupsMemberships);
        logger.info("======================================================================");
        logger.info("");

        statWriter.addPoint("testLargeMutations_adding_80_users_60_groups_30_memberships", addLargeUsersGroupsMemberships);
        statWriter.addPoint("testLargeMutations_removing_80_users_60_groups_30_memberships", removeLargeUsersGroupsMemberships);
    }

    private long _testAddLargeUsersGroupsMemberships() throws Exception
    {
        logger.info("Running _testAddLargeUsersGroupsMemberships");

        // Wait for any sync to finish before doing mutations
        waitForExistingSyncToFinish();

        // Add users and groups directly to LDAP
        addUsersAndGroups(LARGE_NUMBER_OF_USERS, LARGE_NUMBER_OF_GROUPS);

        // Add memberships
        createMemberships(LARGE_NUMBER_OF_USERS, LARGE_INITIAL_MEMBERSHIPS);
        _loginAdminUser(); // direct LDAP actions may have timed out admin's login

        // Check normal users/groups are there
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertUsersAndGroupsFromOUPresent("D");

        // Check mutant users/groups aren't there yet (need to sync first)
        assertMutantUsersAndGroupsNotPresent();
        assertMembershipsNotPresent();

        // Now synchronise!
        long duration = synchroniseDirectory("Adding 80 users, 60 groups, 30 memberships");

        // Now check the users/groups are visible
        assertMutantUsersAndGroupsPresent();
        assertMembershipsPresent(LARGE_INITIAL_MEMBERSHIPS);

        // Check normal users/groups are still there
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertUsersAndGroupsFromOUPresent("D");

        return duration;
    }

    private long _testRemoveLargeUsersGroupsMemberships() throws Exception
    {
        logger.info("Running _testRemoveLargeUsersGroupsMemberships");

        // Wait for any sync to finish before doing mutations
        waitForExistingSyncToFinish();

        // Remove users and groups directly to LDAP
        removeUsersAndGroups(LARGE_NUMBER_OF_USERS, LARGE_NUMBER_OF_GROUPS);
        _loginAdminUser(); // direct LDAP actions may have timed out admin's login

        // Check normal users/groups are there
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertUsersAndGroupsFromOUPresent("D");

        // Check mutant users/groups are still visible (need to sync first)
        assertMutantUsersAndGroupsPresent();
        assertMembershipsPresent(LARGE_INITIAL_MEMBERSHIPS);

        // Now synchronise!
        long duration = synchroniseDirectory("Removing 80 users, 60 groups");

        // Now check the users/groups are no longer visible
        // Memberships should be removed since users/groups don't exist
        assertMutantUsersAndGroupsNotPresent();
        assertMembershipsNotPresent();

        // Check normal users/groups are still there
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertUsersAndGroupsFromOUPresent("D");

        return duration;
    }

    // Tests mutation on a smaller set of users/groups - makes sure behaviour is correct on each step of the mutation
    public void testSmallMutations() throws Exception
    {
        intendToModifyData();

        logger.info("Running: testSmallMutations");
        // Create the Microsoft AD that actually exists in crowd
        createLoadTestingDirectory(CONNECTOR_URL, CONNECTOR_BASEDN, CONNECTOR_USERDN, CONNECTOR_USERPW);
        synchroniseDirectory("Perform initial sync after initial directory creation");

        long addUsersAndGroups = _testAddUsersAndGroups();
        long addUserMemberships = _testAddUserMemberships();
        long changeMemberships = _testChangeUserMemberships();
        long removeUsersAndGroups = _testRemoveUsersAndGroups();

        // Print overall summary
        logger.info("");
        logger.info("======================================================================");
        logger.info("# Mutating small set of users/groups within ou that contains 10k users, 1k groups");
        logger.info("#    Adding 40 users, 30 groups took (in seconds):\t\t" + addUsersAndGroups);
        logger.info("#    Adding users to 15 groups each took (in seconds):\t" + addUserMemberships);
        logger.info("#    Changing user memberships and user attributes took (in seconds):\t" + changeMemberships);
        logger.info("#    Removing 40 users, 30 groups took (in seconds):\t" + removeUsersAndGroups);
        logger.info("======================================================================");
        logger.info("");

        statWriter.addPoint("testSmallMutations_adding_40_users_30_groups", addUsersAndGroups);
        statWriter.addPoint("testSmallMutations_adding_15_users", addUserMemberships);
        statWriter.addPoint("testSmallMutations_changing_user_memberships_and_user_attributes", changeMemberships);
        statWriter.addPoint("testSmallMutations_removing_40_users_30_groups", removeUsersAndGroups);
    }

    private long _testAddUsersAndGroups() throws Exception
    {
        logger.info("Running _testAddUsersAndGroups");

        // Wait for any sync to finish before doing mutations
        waitForExistingSyncToFinish();

        // Add 40 users and 30 groups directly to LDAP
        addUsersAndGroups(SMALL_NUMBER_OF_USERS, SMALL_NUMBER_OF_GROUPS);
        _loginAdminUser(); // direct LDAP actions may have timed out admin's login

        // Check normal users/groups are there
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertUsersAndGroupsFromOUPresent("D");

        // Check mutant users/groups aren't there yet (need to sync first)
        assertMutantUsersAndGroupsNotPresent();

        // Now synchronise!
        long duration = synchroniseDirectory("Adding 40 users, 30 groups");

        // Now check the users/groups are visible
        assertMutantUsersAndGroupsPresent();

        // Check normal users/groups are still there
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertUsersAndGroupsFromOUPresent("D");

        return duration;
    }

    private long _testAddUserMemberships() throws Exception
    {
        logger.info("Running _testAddUserMemberships");

        // Wait for any sync to finish before doing mutations
        waitForExistingSyncToFinish();

        // Add all users to first 15 groups
        createMemberships(SMALL_NUMBER_OF_USERS, SMALL_INITIAL_MEMBERSHIPS);
        _loginAdminUser(); // direct LDAP actions may have timed out admin's login

        // Check mutant memberships aren't visible yet (need to sync first)
        assertMembershipsNotPresent();

        // Now synchronise!
        long duration = synchroniseDirectory("Adding users to 15 groups each");

        // Now check they memberships are visible
        assertMembershipsPresent(SMALL_INITIAL_MEMBERSHIPS);

        return duration;
    }

    private long _testChangeUserMemberships() throws Exception
    {
        logger.info("Running _testChangeUserMemberships");

        // Wait for any sync to finish before doing mutations
        waitForExistingSyncToFinish();

        // Remove existing memberships first
        for (int userIndex = 0; userIndex < SMALL_NUMBER_OF_USERS; userIndex++)
        {
            for (int groupIndex = 0; groupIndex < SMALL_INITIAL_MEMBERSHIPS; groupIndex++)
            {
                ldapDirectory.removeUserFromGroup(MUTANT_USER_PREFIX + userIndex, MUTANT_GROUP_PREFIX + groupIndex);
            }
        }

        // Add all users to the remaining 15 groups
        for (int userIndex = 0; userIndex < SMALL_NUMBER_OF_USERS; userIndex++)
        {
            for (int groupIndex = SMALL_INITIAL_MEMBERSHIPS; groupIndex < SMALL_NUMBER_OF_GROUPS; groupIndex++)
            {
                ldapDirectory.addUserToGroup(MUTANT_USER_PREFIX + userIndex, MUTANT_GROUP_PREFIX + groupIndex);
            }
        }

        // Change user attributes
        for (int i = 0; i < SMALL_NUMBER_OF_USERS; i++)
        {
            User user = ldapDirectory.findUserByName(MUTANT_USER_PREFIX + i);
            UserTemplate userTemplate = new UserTemplate(user);
            userTemplate.setEmailAddress("new@example.com");
            userTemplate.setLastName("Mutated" + i);
            ldapDirectory.updateUser(userTemplate);
        }
        _loginAdminUser(); // direct LDAP actions may have timed out admin's login

        // Check mutant memberships still show the old memberships (15 memberships)
        assertMembershipsPresent(SMALL_INITIAL_MEMBERSHIPS);

        // Check user mutation is not yet visible (need to syc first)
        for (int i = 0; i < SMALL_NUMBER_OF_USERS; i++)
        {
            assertUserDetails(MUTANT_USER_PREFIX + i, "John", "Mutant" + i, "mutant" + i + "@example.com");
        }

        // Now synchronise!
        long duration = synchroniseDirectory("Removing users from 15 groups and adding to new 15 groups");

        // Now check the new memberships are visible
        assertMembershipsPresent(SMALL_NUMBER_OF_GROUPS - SMALL_INITIAL_MEMBERSHIPS);

        // Check user mutation is now visible
        for (int i = 0; i < 10; i++)
        {
            assertUserDetails(MUTANT_USER_PREFIX + i, "John", "Mutated" + i, "new@example.com");
        }

        return duration;
    }

    private long _testRemoveUsersAndGroups()
            throws InterruptedException, ReadOnlyGroupException, UserNotFoundException, GroupNotFoundException, OperationFailedException
    {
        logger.info("Running _testRemoveUsersAndGroups");

        // Wait for any sync to finish before doing mutations
        waitForExistingSyncToFinish();

        // Remove 40 users and 30 groups directly to LDAP
        removeUsersAndGroups(SMALL_NUMBER_OF_USERS, SMALL_NUMBER_OF_GROUPS);
        _loginAdminUser(); // direct LDAP actions may have timed out admin's login

        // Check normal users/groups are still there
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertUsersAndGroupsFromOUPresent("D");

        // Check mutant users/groups are still visible (need to sync first)
        assertMutantUsersAndGroupsPresent();

        // Now synchronise!
        long duration = synchroniseDirectory("Removing 40 users, 30 groups");

        // Now check the users/groups are now removed
        assertMutantUsersAndGroupsNotPresent();

        // Check normal users/groups are still there
        assertUsersAndGroupsFromOUPresent("A");
        assertUsersAndGroupsFromOUPresent("B");
        assertUsersAndGroupsFromOUPresent("C");
        assertUsersAndGroupsFromOUPresent("D");

        return duration;
    }

    // Helper methods
    private void addUsersAndGroups(int numUsers, int numGroups)
            throws UserAlreadyExistsException, InvalidUserException, DirectoryNotFoundException, UserNotFoundException, InvalidCredentialException, OperationFailedException, InvalidGroupException, GroupNotFoundException
    {
        UserTemplate userTemplate;
        for (int i = 0; i < numUsers; i++)
        {
            userTemplate = new UserTemplate(MUTANT_USER_PREFIX + i);
            userTemplate.setDisplayName("Mutant User" + i);
            userTemplate.setEmailAddress("mutant" + i + "@example.com");
            userTemplate.setFirstName("John");
            userTemplate.setLastName("Mutant" + i);
            ldapDirectory.addUser(userTemplate, new PasswordCredential("password"));
        }

        for (int i = 0; i < numGroups; i++)
        {
            ldapDirectory.addGroup(new GroupTemplate(MUTANT_GROUP_PREFIX + i));
        }
    }

    private void removeUsersAndGroups(int numUsers, int numGroups)
            throws UserNotFoundException, OperationFailedException, ReadOnlyGroupException, GroupNotFoundException
    {
        for (int i = 0; i < numUsers; i++)
        {
            ldapDirectory.removeUser(MUTANT_USER_PREFIX + i);
        }

        for (int i = 0; i < numGroups; i++)
        {
            ldapDirectory.removeGroup(MUTANT_GROUP_PREFIX + i);
        }
    }

    private void createMemberships(int numUsers, int numMemberships) throws Exception
    {
        for (int userIndex = 0; userIndex < numUsers; userIndex++)
        {
            for (int groupIndex = 0; groupIndex < numMemberships; groupIndex++)
            {
                ldapDirectory.addUserToGroup(MUTANT_USER_PREFIX + userIndex, MUTANT_GROUP_PREFIX + groupIndex);
            }
        }
    }

    private void assertUserDetails(String username, String firstname, String lastname, String email)
    {
        gotoViewPrincipal(username, CONNECTOR_DIRECTORY_NAME);

        assertTextFieldEquals("email", email);
        assertTextFieldEquals("firstname", firstname);
        assertTextFieldEquals("lastname", lastname);
    }

    private void assertMembershipsPresent(int membershipsCount)
    {
        //Go and view the user's membership (test 5 users)
        for (int i = 0; i < 5; i++)
        {
            gotoViewPrincipal(MUTANT_USER_PREFIX + i, CONNECTOR_DIRECTORY_NAME);
            clickLink("user-groups-tab");

            // Users have a fixed number of memberships +1 for header
            setWorkingForm("groupsForm");
            assertTableRowCountEquals("groupsTable", membershipsCount + 1);
            assertTextPresent("mutant-group"); // don't know the exact groups (more so sanity test)
        }
    }

    private void assertMembershipsNotPresent()
    {
        //Go and view the user's membership (test 5 users)
        for (int i = 0; i < 5; i++)
        {
            gotoViewPrincipal(MUTANT_USER_PREFIX + i, CONNECTOR_DIRECTORY_NAME);
            clickLink("user-groups-tab");

            // No memberships for the user
            assertKeyPresent("principal.nogroupmemberships.text", Arrays.asList(MUTANT_USER_PREFIX + i));
            assertTableNotPresent("groupsTable");
            assertTextNotPresent("mutant-group");
        }
    }

    private void assertMutantUsersAndGroupsNotPresent()
    {
        // Check users
        searchMutantUsers();
        // Check for a few users (they are listed alphabetically)
        assertTextNotPresent(MUTANT_USER_PREFIX + 0);
        assertTextNotPresent(MUTANT_USER_PREFIX + 1);
        assertTextNotPresent(MUTANT_USER_PREFIX + 5);

        // Check groups
        searchMutantGroups();
        // Check for a few users (they are listed alphabetically)
        assertTextNotPresent(MUTANT_GROUP_PREFIX + 0);
        assertTextNotPresent(MUTANT_GROUP_PREFIX + 1);
        assertTextNotPresent(MUTANT_GROUP_PREFIX + 10);
    }

    private void assertMutantUsersAndGroupsPresent()
    {
        // Check users
        searchMutantUsers();
        // Check for a few users (they are listed alphabetically)
        assertTextPresent(MUTANT_USER_PREFIX + 0);
        assertTextPresent(MUTANT_USER_PREFIX + 1);
        assertTextPresent(MUTANT_USER_PREFIX + 5);

        // Check groups
        searchMutantGroups();
        // Check for a few users (they are listed alphabetically)
        assertTextPresent(MUTANT_GROUP_PREFIX + 0);
        assertTextPresent(MUTANT_GROUP_PREFIX + 1);
        assertTextPresent(MUTANT_GROUP_PREFIX + 10);
    }

    private void searchMutantGroups()
    {
        gotoBrowseGroups();
        setWorkingForm("browsegroups");
        setTextField("name", "mutant-group");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME);
        submit();
    }

    private void searchMutantUsers()
    {
        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        setTextField("search", "mutant-user");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME);
        submit();
    }

    private RemoteDirectory createLDAPRemoteDirectory()
    {
        Map<String, String> directoryAttributes = new HashMap<String, String>();

        //START Directory-variable properties
        // set connection information
        directoryAttributes.put(LDAPPropertiesMapper.LDAP_URL_KEY, CONNECTOR_URL);
        directoryAttributes.put(LDAPPropertiesMapper.LDAP_SECURE_KEY, CONNECTOR_SECURE);
        directoryAttributes.put(LDAPPropertiesMapper.LDAP_REFERRAL_KEY, CONNECTOR_REFERRAL);

        // Only set the paged results size if the user has enabled paged results
        directoryAttributes.put(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY, CONNECTOR_PAGEDRESULTS);
        directoryAttributes.put(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE, CONNECTOR_PAGEDRESULTSSIZE);
        directoryAttributes.put(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, CONNECTOR_NESTEDGROUPS);

        directoryAttributes.put(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE_FOR_GROUP_MEMBERSHIP, Boolean.toString(false));

        // set connection pool properites
        directoryAttributes.put(LDAPPropertiesMapper.LDAP_READ_TIMEOUT, "0");
        directoryAttributes.put(LDAPPropertiesMapper.LDAP_SEARCH_TIMELIMIT, "0");
        directoryAttributes.put(LDAPPropertiesMapper.LDAP_CONNECTION_TIMEOUT, "3600000");

        directoryAttributes.put(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL, "360000");
        directoryAttributes.put(LDAPPropertiesMapper.LDAP_RELAXED_DN_STANDARDISATION, Boolean.toString(true));

        directoryAttributes.put(LDAPPropertiesMapper.LDAP_BASEDN_KEY, CONNECTOR_BASEDN);
        directoryAttributes.put(LDAPPropertiesMapper.LDAP_USERDN_KEY, CONNECTOR_USERDN);
        directoryAttributes.put(LDAPPropertiesMapper.LDAP_PASSWORD_KEY, CONNECTOR_USERPW);

        LDAPPropertiesHelperImpl ldapPropertiesHelperImpl = new LDAPPropertiesHelperImpl();
        Properties directorySchema = ldapPropertiesHelperImpl.getConfigurationDetails().get(CONNECTOR_CLASSNAME);

        // set the ldap attributes
        //  groups
        directoryAttributes.put(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY, (String) directorySchema.get(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.GROUP_NAME_KEY, (String) directorySchema.get(LDAPPropertiesMapper.GROUP_NAME_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.GROUP_OBJECTCLASS_KEY, (String) directorySchema.get(LDAPPropertiesMapper.GROUP_OBJECTCLASS_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.GROUP_OBJECTFILTER_KEY, (String) directorySchema.get(LDAPPropertiesMapper.GROUP_OBJECTFILTER_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.GROUP_USERNAMES_KEY, (String) directorySchema.get(LDAPPropertiesMapper.GROUP_USERNAMES_KEY));

        //  roles
        directoryAttributes.put(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.toString(true));
        directoryAttributes.put(LDAPPropertiesMapper.ROLE_DESCRIPTION_KEY, (String) directorySchema.get(LDAPPropertiesMapper.ROLE_DESCRIPTION_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.ROLE_NAME_KEY, (String) directorySchema.get(LDAPPropertiesMapper.ROLE_NAME_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.ROLE_OBJECTCLASS_KEY, (String) directorySchema.get(LDAPPropertiesMapper.ROLE_OBJECTCLASS_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.ROLE_OBJECTFILTER_KEY, (String) directorySchema.get(LDAPPropertiesMapper.ROLE_OBJECTFILTER_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.ROLE_USERNAMES_KEY, (String) directorySchema.get(LDAPPropertiesMapper.ROLE_USERNAMES_KEY));

        //  users
        directoryAttributes.put(LDAPPropertiesMapper.USER_EMAIL_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_EMAIL_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.USER_FIRSTNAME_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_FIRSTNAME_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.USER_GROUP_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_GROUP_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.USER_LASTNAME_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_LASTNAME_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.USER_DISPLAYNAME_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_DISPLAYNAME_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.USER_OBJECTCLASS_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_OBJECTCLASS_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.USER_OBJECTFILTER_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_OBJECTFILTER_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.USER_USERNAME_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_USERNAME_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY));
        directoryAttributes.put(LDAPPropertiesMapper.USER_PASSWORD_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_PASSWORD_KEY));

        InstanceFactory mockInstanceFactory = mock(InstanceFactory.class);
        when(mockInstanceFactory.getInstance(LDAPPropertiesMapperImpl.class)).thenReturn(new LDAPPropertiesMapperImpl(ldapPropertiesHelperImpl));

        // Create the appropriate remotedirectory
        RemoteDirectory remoteDirectory;
        if (getConnectorType().equals(OpenLDAP.getStaticDirectoryType()))
        {
            remoteDirectory = new OpenLDAP(new LDAPQueryTranslaterImpl(), null, mockInstanceFactory, new PasswordEncoderFactoryImpl());
        }
        else
        {
            // Default to Microsoft Active Directory
            remoteDirectory = new MicrosoftActiveDirectory(new ActiveDirectoryQueryTranslaterImpl(), null,
                    mockInstanceFactory, new PasswordHelperImpl());
        }
        remoteDirectory.setAttributes(directoryAttributes);

        return remoteDirectory;
    }
}
