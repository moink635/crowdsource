package com.atlassian.crowd.acceptance.tests.persistence.dao.property;

import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.dao.property.PropertyDAOHibernate;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.persistence.PersistenceException;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class PropertyDAOHibernateTest
{
    @Inject private PropertyDAOHibernate propertyDAO;
    @Inject private SessionFactory sessionFactory;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    private static final String CROWD_KEY = "crowd";
    private static final String NAME = "name1";
    private static final String VALUE = "value1";
    private static final String NEW_NAME = "newName";
    private static final String NEW_VALUE = "newValue";
    private static final String JIRA_KEY = "jira";

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Test
    public void testFindByName() throws Exception
    {
        Property property = propertyDAO.find(CROWD_KEY, NAME);
        assertEquals(VALUE, property.getValue());
    }

    public void testAdd() throws Exception
    {
        Property property = new Property(CROWD_KEY, NEW_NAME, NEW_VALUE);
        propertyDAO.add(property);

        assertEquals(NEW_VALUE, propertyDAO.find(CROWD_KEY, NEW_NAME).getValue());
    }

    @Test (expected = ConstraintViolationException.class)
    public void testAddDuplication()
    {
        Property property = new Property(CROWD_KEY, NAME, VALUE);
        propertyDAO.add(property);

        // the contract of PropertyDAO.add() does not require implementations to throw an exception. Therefore,
        // the exception may come later, when the session is flushed
        sessionFactory.getCurrentSession().flush();
    }

    @Test
    public void testUpdate() throws Exception
    {
        Property property = propertyDAO.find(CROWD_KEY, NAME);

        // Update the property value
        property.setValue(NEW_VALUE);
        propertyDAO.update(property);

        assertEquals(NEW_VALUE, propertyDAO.find(CROWD_KEY, NAME).getValue());
    }

    @Test
    public void testFindAllByKey()
    {
        List<Property> properties = propertyDAO.findAll(CROWD_KEY);
        assertEquals(2, properties.size());

        properties = propertyDAO.findAll(JIRA_KEY);
        assertEquals(1, properties.size());

        properties = propertyDAO.findAll("boguskey");
        assertEquals(0, properties.size());
    }

    @Test
    public void testFindAll() throws PersistenceException
    {
        List<Property> allProperties = propertyDAO.findAll();
        assertEquals(3, allProperties.size());
    }

    @Test
    public void testRemoveProperty()
    {
        propertyDAO.remove(CROWD_KEY, NAME);

        // assert the given property no longer exists
        try
        {
            propertyDAO.find(CROWD_KEY, NAME);

            fail("ObjectNotFoundException expected");
        }
        catch (ObjectNotFoundException e)
        {
            // expected
        }
    }
}
