package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import com.atlassian.core.util.PropertyUtils;
import com.atlassian.crowd.acceptance.tests.BaseUrlFromProperties;
import com.atlassian.crowd.acceptance.tests.TestDataState;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import com.atlassian.crowd.acceptance.utils.CrowdWebTestCase;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.model.DirectoryEntity;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.service.client.ResourceLocator;
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import static org.junit.Assert.assertThat;

public class CrowdAcceptanceTestCase extends CrowdWebTestCase
{
    public static String HOST_PATH;

    protected static final String ADMIN_GROUP = "administrators";
    protected static final String ADMIN_USER = "admin";
    protected static final String ADMIN_PW = "admin";
    protected static final String ADMIN_FULL_NAME = "Super User";
    protected static final String ADMIN_EMAIL = "crowd@example.com";

    protected static final Properties specProperties;

    protected static String URL_HOME = "/console/secure/console.action";
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private ResourceBundle i18n;

    protected static final String WORKGROUP_LICENSE_KEY = "NONnnbGCEhMXASCOrPtIvuKpmrMdafAvINOGBPKfEUbSSwn\n" + "mj2KLPN2K78vDnn5CiW4DEM72L0Mz2ZyRhyyclcRGZjv03C\n" + "pqmUvmorqMooooNMNmvvmqOMPOnPQPrNrtwwStVwwWusPSU\n" + "UwxwSVMWuuwtUXwspOsmvUUnnvrtnnommmmmUUnnvrtnnom\n" + "mmmmUU1qiXppfXkWJlcqtXobWJvpqbjpUUrm";

    protected static final String UNLIMITED_LICENSE_KEY = "MprrTWaDMMgqqQFpjxNimIQipnxxoLHCfBhpuriOFeiHJo\n" + "mi2Kf0GbDqztZI94<4TFAhw72Kl7Ojoz6JPyOhv<sLxwCc\n" + "nMRVURrNpmqPrQoorqVTqomnMqQoOOQQnUVSvsxTtwVuWS\n" + "mOnnqsmvUUnurtvvoqmmmmmUUnurtvvoqmmmmmUU1qiXpp\n" + "fXkUUnmmmm";

    protected static final String EXPIRED_MAINTENANCE_KEY = "AAABFw0ODAoPeNp9kFFLwzAQx9/zKQI+t7TbZGUQEJc4CnWdrlMRX2J607A0LZe0um9vu3UgPvh4x\n" + "/1+97+7eoaSbqGh0ZTGk8VssrieUrEt6CSKEsLBKdSN17VlS6y/yrcF5dCBqRtAooZOKJXXHTCPL\n" + "ZAlghyGufTAesM8iJIgismytr6fW8sKmDISD+EBrL8ppTbHxkgLPlR1NQozrcA6KI4NnAAunkSWb\n" + "8TjRSPue+4/j+ikaU9B2F4aB6QHrAcrrQLx3Wg8XgImY8AcP6TV7szwQUc3Jx/ZAnaAKWe3xWwVr\n" + "HbpXfCSP8yDnL+mY+B1W70D5vudA3QsiMl4QQ9lKb9Uvxa31uhKeyjJpkX1KR38fdgPj3SKEjAsA\n" + "hQDnIHlSx5Ug4jClePbHxCPTYAwnQIUeTzFHGkn0bRuxJZNK5uaXO7y+uU=X02e6";

    // 3 hour license
    protected static final String DEVELOPER_LICENSE_KEY = "AAABgQ0ODAoPeNp1kstuwjAQRff5CktdB4XXAqQsIHELLS+RQCvUjWMGcBvsaJxA+fuaQNQkopJX1\n" +
            "9d3zsz4aaokCSAhzQ5pdvutVr/dJTQISctxelbEjpFSjYngIDXQrUiFki6dhXS5WI4Dau2EPsAFC\n" +
            "kd4ScB9Nhq9wGef+HCCWCWAFo/VCbBi83Kp4pplxwhwvltpQO12La7krsF4Kk7gppiBtciQH5gGn\n" +
            "6XgXgltc5odq5Q7Y0dwfbqmk/mCLosb+pMIvOTPFu1R0Vc5OgA0OGPfHb70Qvtjte7Yb5vNyB46z\n" +
            "XfrSyCrwL+OlwNCZQqYoNC1Xq/UlU6NEGcg+QNf0bIXZ9qkzdQWtOvUBp+nDHPpv6Jlwgd74phxE\n" +
            "cXVRXl3sRI0ZcJUkMzQ1obGUZ23tQCjVF7fb80YV/JbqrO05rhnUmiWEw3SmGktmPwDKu/AQ8h99\n" +
            "fXeKpedxb8raz5ojiLJC4WgUxLfYMhOIUnibC8k2Rak+jax8vv7Hy1Lv3FyG1gwLAIUHEP4zbqEf\n" +
            "1DeOWKb7SpxQ1MEd8ACFGxZV5doE27OtsuNDT50k2xIhAozX02im";

    static
    {
        // load specific properties if defined
        specProperties = AcceptanceTestHelper.loadProperties("localtest.properties");

        HOST_PATH = new BaseUrlFromProperties(specProperties).baseUrlFor("crowd");
    }

    public String getBaseUrl()
    {
        return HOST_PATH;
    }

    public String getCrowdHome()
    {
        String crowdHome = System.getProperty("crowd.home");

        if (crowdHome == null)
        {
            // try grabbing it from the System Info Page:

            gotoSystemInfo();

            String path = getElementTextByXPath("//*[@id=\"home-path\"]");

            File homePath = new File(path);
            if (homePath.exists() && homePath.isDirectory())
            {
                try
                {
                    return homePath.getCanonicalPath();
                }
                catch (IOException e)
                {
                    throw new RuntimeException(e);
                }
            }
        }

        return crowdHome;
    }

    public String getCrowdApplicationPassword()
    {
        return SoapClientPropertiesImpl.newInstanceFromResourceLocator(new ResourceLocator()
        {
            public String getResourceLocation()
            {
                return getCrowdHome() + File.separator + getResourceName();
            }

            public String getResourceName()
            {
                return "crowd.properties";
            }

            public Properties getProperties()
            {
                return PropertyUtils.getPropertiesFromFile(new File(getResourceLocation()));
            }
        }).getApplicationPassword();
    }

    public void _loginAdminUser()
    {
        _loginAsUser(ADMIN_USER, ADMIN_PW);
    }

    public void _loginAsUser(String username, String password)
    {
        _logout();

        submitLoginForm(username, password);
    }

    public void submitLoginFormAsAdminUser()
    {
        submitLoginForm(ADMIN_USER, ADMIN_PW);
    }

    public void submitLoginForm(String username, String password)
    {
        assertKeyPresent("login.title");

        setTextField("j_username", username);
        setTextField("j_password", password);
        submit();

        // quick verification
        assertLinkPresentWithKey("menu.logout.label");
    }

    public void _logout()
    {
        gotoPage("/console/logoff.action");
        assertKeyPresent("login.title");
    }

    /**
     * Mark Crowd's state as modified so we don't try to reuse the current
     * state for the next test.
     *
     * @see TestDataState#intendToModify(java.net.URL)
     */
    public void intendToModifyData()
    {
        TestDataState.INSTANCE.intendToModify(getTestContext().getBaseUrl());
    }

    /**
     * Indicate that a test will modify LDAP data.
     * (<strong>Not implemented</strong>; included so we can start to mark
     * those tests.)
     */
    public void intendToModifyLdapData()
    {
        // Not implemented
    }

    /**
     * Restore Crowd from a backup file, unless we already have this data
     * present.
     *
     * @see TestDataState#isRestoredXml(java.net.URL, String)
     */
    public void restoreCrowdFromXML(String xmlfilename)
    {
        if (TestDataState.INSTANCE.isRestoredXml(getTestContext().getBaseUrl(), xmlfilename))
        {
            _loginAdminUser();
            return;
        }
        if (getPageText().contains(getText("setup.text")))
        {
            // setup crowd as a backup from XML

            // License Screen
            setTextField("key", UNLIMITED_LICENSE_KEY);
            submit();

            // Installation type screen
            clickRadioOption("installOption", "install.xml");
            submit();

            // DB type screen - Using embedded for tests
            clickRadioOption("databaseOption", "db.embedded");
            submit();

            // Restore from XML page
            setTextField("filePath", AcceptanceTestHelper.getResourcePath("xml/" + xmlfilename));
            submit();

            // Final page
            assertKeyPresent("setupcomplete.title");
            clickElementByXPath("//input[@id='continueButton']");
            assertKeyPresent("login.title");
        }
        else
        {
            restoreCrowdFromXMLWithoutReloggingIn(xmlfilename);
        }

        TestDataState.INSTANCE.setRestoredXml(getTestContext().getBaseUrl(), xmlfilename);


        _loginAdminUser();
    }

    private void restoreCrowdFromXMLWithoutReloggingIn(String xmlfilename)
    {
        restoreCrowdFromXMLWithoutReloggingIn(xmlfilename, true);
    }

    void restoreCrowdFromXMLWithoutReloggingIn(String xmlfilename, boolean checkForError)
    {
        TestDataState.INSTANCE.intendToModify(getTestContext().getBaseUrl());

        String absolutePathfileLocation = AcceptanceTestHelper.getResourcePath("xml/" + xmlfilename);

        _loginAdminUser();

        gotoRestore();

        setTextField("importFilePath", absolutePathfileLocation);

        logger.info("Restoring Crowd from: " + absolutePathfileLocation);

        submit("import-submit");

        if (checkForError)
        {
            assertErrorNotPresent();
        }
    }

    public void restoreBaseSetup()
    {
        restoreCrowdFromXML("basesetup.xml");
    }

    public void gotoAddApplication()
    {
        gotoPage("/console/secure/application/addapplicationdetails.action");
    }

    public void gotoCreateDirectory()
    {
        gotoPage("/console/secure/directory/create.action");
    }

    public void gotoAddGroup()
    {
        gotoPage("/console/secure/group/add.action");
    }

    public void gotoViewPrincipal(String principalName, String directoryName)
    {
        gotoPage("/console/secure/user/view.action?name=" + principalName + "&directoryName=" + directoryName);

        assertKeyPresent("menu.viewprincipal.label");
    }

    public void gotoRemovePrincipal(String principalName, String directoryName)
    {
        gotoPage("/console/secure/user/remove.action?name=" + principalName + "&directoryName=" + directoryName);

        assertKeyPresent("menu.removeprincipal.label");
    }

    public void gotoViewGroup(String groupName, String directoryName)
    {
        gotoPage("/console/secure/group/view.action?name=" + groupName + "&directoryName=" + directoryName);
    }

    public void gotoAddPrincipal()
    {
        gotoPage("/console/secure/user/add.action");
    }

    public void gotoBrowseApplications()
    {
        gotoPage("/console/secure/application/browse.action");
        assertKeyPresent("browser.application.title");
    }

    public void gotoBrowsePrincipals()
    {
        gotoPage("/console/secure/user/browse.action");
        assertKeyPresent("browser.principal.title");
    }

    public void gotoBrowsePrincipals(int resultsStart)
    {
        gotoPage("/console/secure/user/browse.action?resultsStart=" + resultsStart);
        assertKeyPresent("browser.principal.title");
    }

    public void gotoBrowseGroups()
    {
        gotoPage("/console/secure/group/browse.action");
        assertKeyPresent("browser.group.title");
    }

    public void gotoBrowseGroups(int resultsStart)
    {
        gotoPage("/console/secure/group/browse.action?resultsStart=" + resultsStart);
        assertKeyPresent("browser.group.title");
    }

    public void gotoBrowseDirectories()
    {
        gotoPage("/console/secure/directory/browse.action");
    }

    public void gotoImporters()
    {
        gotoPage("/console/secure/dataimport/importtype.action");
    }

    public void gotoSystemInfo()
    {
        gotoPage("/console/secure/admin/systeminfo.action");
        assertKeyPresent("systeminfo.title");
    }

    protected void log(String message)
    {
        logger.info(message);
    }

    public void setUp() throws Exception
    {
        super.setUp();

        i18n = ResourceBundle.getBundle("com.atlassian.crowd.console.action.BaseAction");

        getTestContext().setBaseUrl(HOST_PATH);
        getTestContext().setResourceBundleName("com.atlassian.crowd.console.action.BaseAction");

        log("Starting at URL: " + HOST_PATH);

        beginAt(URL_HOME);
    }

    public String getText(String key)
    {
        return i18n.getString(key);
    }

    public void tearDown() throws Exception
    {
        super.tearDown();
        tester = null;
    }

    public void gotoGeneral()
    {
        gotoPage("/console/secure/admin/general.action");
    }

    public void gotoLicensing()
    {
        gotoPage("/console/secure/admin/licensing.action");
    }

    public void gotoMailServer()
    {
        gotoPage("/console/secure/admin/mailserver.action");
    }

    public void gotoMailTemplate()
    {
        gotoPage("/console/secure/admin/mailtemplate.action");
    }

    public void gotoSessionConfig()
    {
        gotoPage("/console/secure/admin/sessionconfig.action");
        assertKeyPresent("menu.sessionconfig.label");
    }

    public void gotoCurrentApplicationSessions()
    {
        gotoPage("/console/secure/session/browseapplicationsessions.action");
    }

    public void gotoCurrentPrincipalSessions()
    {
        gotoPage("/console/secure/session/browseusersessions.action");
    }

    public void gotoTrustedProxies()
    {
        gotoPage("/console/secure/admin/viewtrustedproxies.action");
    }

    public void gotoLoggingProfiling()
    {
        gotoPage("/console/secure/admin/loggingProfiling.action");
    }

    public void gotoBackup()
    {
        gotoPage("/console/secure/admin/backup.action");
        assertKeyPresent("administration.backup.text");
    }

    public void gotoRestore()
    {
        gotoPage("/console/secure/admin/restore.action");
    }

    public void gotoViewApplication(String applicationName)
    {
        gotoPage("/console/secure/application/viewdetails.action?name=" + applicationName);
    }

    public void gotoSAMLConfig()
    {
        gotoPage("/console/secure/application/samlconfig.action");
    }

    public void gotoLdapConnectionPool()
    {
        gotoPage("/console/secure/admin/connectionpool.action");
    }

    public void assertUserInTable(String username, String fullname, String email)
    {
        assertThat(getUserDetailsTableContents(),
                CoreMatchers.<User>hasItem(userWithDetails(username, fullname, email)));
    }

    public void assertUserNotInTable(String username, String fullname, String email)
    {
        assertThat(getUserDetailsTableContents(),
                CoreMatchers.not(CoreMatchers.<User>hasItem(userWithDetails(username, fullname, email))));
    }

    public void assertUserInTable(String username, String fullname, String email, String alias)
    {
        assertThat(getUserDetailsTableContents(),
                CoreMatchers.<UserWithAlias>hasItem(userWithDetailsAndAlias(username, fullname, email, alias)));
    }

    public void assertUnescapedKeyNotPresent(String key)
    {
        // We can't use assertKeyNotPresent, because it doesn't transform '' into '.
        String message = getMessage(key);
        assertTextNotPresent(MessageFormat.format(message, new Object[0]));
    }

    public void assertUnescapedKeyPresent(String key)
    {
        String message = getMessage(key);
        assertTextPresent(MessageFormat.format(message,  new Object[0]));
    }

    private static final Pattern DETAILS = Pattern.compile("Username: (.*)\nEmail Address: ?(.*)");
    private static final Pattern DETAILS_WITH_ALIAS = Pattern.compile("Username: (.*)\nEmail Address: ?(.*)\nAlias: (.*)");

    List<User> getUserDetailsTableContents()
    {
        return scrapeTable("user-details",
                ImmutableList.of("Name", "Details"),
                new Function<List<String>, User>()
                {
                    @Override
                    public User apply(List<String> input)
                    {
                        String fullName = input.get(0);
                        String details = input.get(1);

                        UserTemplate user;

                        String username, email;

                        java.util.regex.Matcher m;

                        m = DETAILS_WITH_ALIAS.matcher(details);
                        if (m.matches())
                        {
                            username = m.group(1);
                            email = m.group(2);
                            String alias = m.group(3);

                            user = new UserWithAlias(username, alias);
                        }
                        else
                        {
                            m = DETAILS.matcher(details);

                            if (!m.matches())
                            {
                                throw new RuntimeException("Unable to match details: " + details);
                            }

                            username = m.group(1);
                            email = m.group(2);

                            user = new UserTemplate(username);
                        }

                        user.setDisplayName(fullName);
                        user.setEmailAddress(email);

                        return user;
                    }
                });
    }

    Matcher<User> userWithDetails(final String username, final String fullname)
    {
        return new TypeSafeMatcher<User>(User.class)
        {
            @Override
            public void describeTo(Description desc)
            {
                desc.appendText("a User with username '" + username + "' and full name '" + fullname + "'");
            }

            @Override
            protected boolean matchesSafely(User u)
            {
                return username.equals(u.getName()) && fullname.equals(u.getDisplayName());
            }
        };
    }

    Matcher<User> userWithDetails(final String username, final String fullname, final String email)
    {
        return new TypeSafeMatcher<User>(User.class)
        {
            @Override
            public void describeTo(Description desc)
            {
                desc.appendText("a User with username '" + username + "', full name '" + fullname + "' and email '" + email + "'");
            }

            @Override
            protected boolean matchesSafely(User u)
            {
                return username.equals(u.getName()) && fullname.equals(u.getDisplayName()) && email.equals(u.getEmailAddress());
            }
        };
    }

    public void gotoAdministrationPage()
    {
        gotoPage("console/secure/admin/general.action");
    }

    private static class UserWithAlias extends UserTemplate
    {
        private final String alias;

        UserWithAlias(String username, String alias)
        {
            super(username);
            this.alias = alias;
        }

        String getAlias()
        {
            return alias;
        }

        public String toString()
        {
            return super.toString() + " and alias '" + alias + "'";
        }
    }

    private Matcher<UserWithAlias> userWithDetailsAndAlias(final String username, final String fullname, final String email, final String alias)
    {
        return new TypeSafeMatcher<UserWithAlias>(UserWithAlias.class)
        {
            @Override
            public void describeTo(Description desc)
            {
                desc.appendText("a User with username '" + username + "', full name '" + fullname + "', email '"
                        + email + "' and alias '" + alias + "'");
            }

            @Override
            protected boolean matchesSafely(UserWithAlias u)
            {
                return username.equals(u.getName()) && fullname.equals(u.getDisplayName()) && email.equals(u.getEmailAddress())
                        && alias.equals(u.getAlias());
            }
        };
    }

    List<Group> getGroupTableContents()
    {
        return scrapeTable("groupsTable",
                ImmutableList.of("Group", "Description", "Active"),
                new Function<List<String>, Group>()
                {
                    @Override
                    public Group apply(List<String> input)
                    {
                        String groupName = input.get(0);
                        String description = input.get(1);
                        String active = input.get(2);

                        GroupTemplate group = new GroupTemplate(groupName, -1);
                        group.setDescription(description);
                        group.setActive(Boolean.parseBoolean(active));

                        return group;
                    }
                });
    }

    static List<String> namesOf(Iterable<? extends DirectoryEntity> entities)
    {
        List<String> names = new ArrayList<String>();

        for (DirectoryEntity e : entities)
        {
            names.add(e.getName());
        }

        return names;
    }
}
