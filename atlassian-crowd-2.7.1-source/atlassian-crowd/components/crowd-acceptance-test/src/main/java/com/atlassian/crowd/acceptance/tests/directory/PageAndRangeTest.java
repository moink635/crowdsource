package com.atlassian.crowd.acceptance.tests.directory;

import java.util.List;

import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.TimedProgressOperation;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * These tests exist to make sure that paged controls work when there are c 2000 users in result set, and that large
 * numbers of group members is also correctly handled (eg. 1000+ users in an AD group).
 */
public class PageAndRangeTest extends BaseTest
{
    private static final Logger logger = LoggerFactory.getLogger(PageAndRangeTest.class);

    private final String USERNAME_PREFIX = "page-user-";
    private final String GROUPNAME_PREFIX = "page-group-";
    private final String LARGE_GROUP_NAME = "PageAndRange-LargeGroup";
    private final int NUM_USERS = 2500;
    private final int NUM_GROUPS = 2500;


    protected String buildUserName(int i)
    {
        return USERNAME_PREFIX + i;
    }

    protected String buildGroupName(int i)
    {
        return GROUPNAME_PREFIX + i;
    }

    protected void addUserToGroup(int userIndex, String groupName) throws Exception
    {
        getRemoteDirectory().addUserToGroup(buildUserName(userIndex), groupName);
    }

    /**
     * Called as part of tearDown()
     */
    @Override
    protected void removeTestData() throws DirectoryInstantiationException
    {
        try
        {
            if (getRemoteDirectory().findUserByName(buildUserName(0)) != null)
            {
                TimedProgressOperation operation =
                    new TimedProgressOperation("Deleting " + NUM_USERS + " users", NUM_USERS, logger);
                for (int i = 0; i < NUM_USERS; i++)
                {
                    operation.incrementProgress();
                    DirectoryTestHelper.silentlyRemoveUser(buildUserName(i), getRemoteDirectory());
                }
                operation.complete("Users deleted");
            }
        }
        catch (Exception e)
        {
        }

        try
        {
            if (getRemoteDirectory().findGroupByName(buildGroupName(0)) != null)
            {
                TimedProgressOperation operation =
                    new TimedProgressOperation("Deleting " + NUM_GROUPS + " groups", NUM_GROUPS, logger);
                for (int i = 0; i < NUM_GROUPS; i++)
                {
                    operation.incrementProgress();
                    DirectoryTestHelper.silentlyRemoveGroup(buildGroupName(i), getRemoteDirectory());
                }
                operation.complete("Groups deleted");
            }
        }
        catch (Exception e)
        {
        }

        DirectoryTestHelper.silentlyRemoveGroup(LARGE_GROUP_NAME, getRemoteDirectory());
    }

    /**
     * Called as part of setUp()
     */
    @Override
    protected void loadTestData()
    {
        // nop - will be different for every test
    }


    /**
     * NUM_USERS users in one searchresult - trigger paging.
     *
     * @throws Exception
     */
    public void testLargeUserSearchResult() throws Exception
    {
        logger.info(">> Running testLargeUserSearchResult");
        int numExistingUsers = 0;
        List<String> users = getRemoteDirectory().searchUsers(QueryBuilder.queryFor(String.class, EntityDescriptor.user()).returningAtMost(EntityQuery.ALL_RESULTS));
        if (users != null)
        {
            numExistingUsers = users.size();
        }

        logger.info("Creating " + NUM_USERS + " users");
        TimedProgressOperation operation =
            new TimedProgressOperation("Creating " + NUM_USERS + " users", NUM_USERS, logger);
        for (int i = 0; i < NUM_USERS; i++)
        {
            operation.incrementProgress();
            DirectoryTestHelper.addUser(buildUserName(i), directory.getId(), "secret-password", getRemoteDirectory());
        }
        operation.complete("Users created");

        users = getRemoteDirectory().searchUsers(QueryBuilder.queryFor(String.class, EntityDescriptor.user()).returningAtMost(EntityQuery.ALL_RESULTS));
        assertNotNull(users);
        assertEquals("Did not return all users added", numExistingUsers + NUM_USERS, users.size());
        // loop through and make sure we can retrieve every user
        for (String user : users)
        {
            assertNotNull(user);
        }
    }

    /**
     * NUM_GROUPS groups in one searchresult - trigger paging.
     *
     * @throws Exception
     */
    public void testLargeGroupSearchResult() throws Exception
    {
        logger.info(">> Running testLargeGroupSearchResult");
        int numExistingGroups = 0;
        List<String> groups = getRemoteDirectory().searchGroups(QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP)).returningAtMost(EntityQuery.ALL_RESULTS));
        if (groups != null)
        {
            numExistingGroups = groups.size();
        }

        logger.info("Creating " + NUM_GROUPS + " groups");
        TimedProgressOperation operation =
            new TimedProgressOperation("Creating " + NUM_GROUPS + " groups", NUM_GROUPS, logger);
        for (int i = 0; i < NUM_GROUPS; i++)
        {
            operation.incrementProgress();
            DirectoryTestHelper.addGroup(buildGroupName(i), directory.getId(), getRemoteDirectory());
        }
        operation.complete("Groups created");

        groups = getRemoteDirectory().searchGroups(QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP)).returningAtMost(EntityQuery.ALL_RESULTS));
        assertNotNull(groups);
        assertEquals("Did not return all groups added", numExistingGroups + NUM_GROUPS, groups.size());
        // loop through and make sure we can retrieve every group
        for (String group : groups)
        {
            assertNotNull(group);
        }
    }

    /**
     * NUM_USERS users as members of one group. Triggers ranged results in AD.
     *
     * @throws Exception
     */
    public void testLargeGroupMemberSearchResult() throws Exception
    {
        logger.info(">> Running testLargeGroupMemberSearchResult");
        DirectoryTestHelper.addGroup(LARGE_GROUP_NAME, directory.getId(), getRemoteDirectory());
        logger.info("Creating " + NUM_USERS + " users, and adding each to group <" + LARGE_GROUP_NAME + ">");
        TimedProgressOperation operation = new TimedProgressOperation("Creating " + NUM_USERS + " users, and adding each to group " + LARGE_GROUP_NAME, NUM_USERS, logger);
        for (int i = 0; i < NUM_USERS; i++)
        {
            operation.incrementProgress();
            DirectoryTestHelper.addUser(buildUserName(i), directory.getId(), "secret-password", getRemoteDirectory());
            addUserToGroup(i, LARGE_GROUP_NAME);
        }
        operation.complete("Users created and added to group");

        List<String> users = getRemoteDirectory().searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(LARGE_GROUP_NAME).returningAtMost(EntityQuery.ALL_RESULTS));
        assertNotNull(users);
        assertEquals(LARGE_GROUP_NAME + " should have " + NUM_USERS + " members", NUM_USERS, users.size());
        for (String user : users)
        {
            assertNotNull(user);
        }
    }

}
