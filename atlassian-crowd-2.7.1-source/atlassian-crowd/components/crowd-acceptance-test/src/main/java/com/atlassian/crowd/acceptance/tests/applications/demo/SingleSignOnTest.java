package com.atlassian.crowd.acceptance.tests.applications.demo;

public class SingleSignOnTest extends DemoAcceptanceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("singlesignontest.xml");

        // ensure we're logged out of both apps
        logoutFromCrowd();
        logoutFromDemo();
    }

    public void testLoginToCrowdConsequentlyLoggedInToDemo()
    {
        log("Running testLoginToCrowdConsequentlyLoggedInToDemo");

        // explicitly logout of both applications
        logoutFromCrowd();
        logoutFromDemo();

        // login successfully to crowd
        String crowdUser = loginToCrowd(CROWD_ADMIN_USER, ADMIN_PW);
        assertNotNull(crowdUser);

        // verify logged in to demo as same user
        String demoUser = getCurrentlyLoggedInDemoUserFullName();
        assertEquals(crowdUser, demoUser);

        // verify still logged in to crowd
        crowdUser = getCurrentlyLoggedInCrowdUserFullName();
        assertEquals(crowdUser, demoUser);
    }

    public void testLoginToDemoConsequentlyLoggedInToCrowd()
    {
        log("Running testLoginToDemoConsequentlyLoggedInToCrowd");

        // explicitly logout of both applications
        logoutFromCrowd();
        logoutFromDemo();

        // login successfully to demo
        String demoUser = loginToDemo(CROWD_ADMIN_USER, ADMIN_PW);
        assertNotNull(demoUser);

        // verify logged in to crowd as same user
        String crowdUser = getCurrentlyLoggedInCrowdUserFullName();
        assertEquals(demoUser, crowdUser);

        // verify still logged in to demo
        demoUser = getCurrentlyLoggedInDemoUserFullName();
        assertEquals(demoUser, crowdUser);
    }

    public void testLogoutOfCrowdConsequentlyLoggedOutOfDemo()
    {
        log("Running testLogoutOfCrowdConsequentlyLoggedOutOfDemo");

        // login to crowd (and hence demo)
        String crowdUser = loginToCrowd(CROWD_ADMIN_USER, ADMIN_PW);
        assertNotNull(crowdUser);
        String demoUser = getCurrentlyLoggedInDemoUserFullName();
        assertNotNull(demoUser);

        // logout of crowd
        logoutFromCrowd();
        
        // verify logged out of demo
        demoUser = getCurrentlyLoggedInDemoUserFullName();
        assertNull(demoUser);
    }

    public void testLogoutOfDemoConsequentlyLoggedOutOfCrowd()
    {
        log("Running testLogoutOfDemoConsequentlyLoggedOutOfCrowd");

        // login to crowd (and hence demo)
        String crowdUser = loginToCrowd(CROWD_ADMIN_USER, ADMIN_PW);
        assertNotNull(crowdUser);
        String demoUser = getCurrentlyLoggedInDemoUserFullName();
        assertNotNull(demoUser);

        // logout of demo
        logoutFromDemo();

        // verify logged out of demo
        crowdUser = getCurrentlyLoggedInCrowdUserFullName();
        assertNull(crowdUser);
    }

    public void testSwitchUserInCrowdConsequentlySwitchesUserInDemo()
    {
        log("Running testSwitchUserInCrowdConsequentlySwitchesUserInDemo");

        // login to crowd (and hence demo)
        String crowdUser = loginToCrowd(CROWD_ADMIN_USER, ADMIN_PW);
        assertNotNull(crowdUser);
        String demoUser = getCurrentlyLoggedInDemoUserFullName();
        assertNotNull(demoUser);

        // logout of crowd
        logoutFromCrowd();

        // switch user in crowd
        String newCrowdUser = loginToCrowd(TEST_USER, TEST_PW);
        assertTrue(!newCrowdUser.equals(crowdUser));

        // verify user is switched in demo
        String newDemoUser = getCurrentlyLoggedInDemoUserFullName();
        assertEquals(newCrowdUser, newDemoUser);
    }

    public void testSwitchUserInDemoConsequentlySwitchesUserInCrowd()
    {
        log("Running testSwitchUserInDemoConsequentlySwitchesUserInCrowd");

        // login to crowd (and hence demo)
        String crowdUser = loginToCrowd(CROWD_ADMIN_USER, ADMIN_PW);
        assertNotNull(crowdUser);
        String demoUser = getCurrentlyLoggedInDemoUserFullName();
        assertNotNull(demoUser);

        // logout of demo
        logoutFromDemo();

        // switch user in demo
        String newDemoUser = loginToDemo(TEST_USER, TEST_PW);
        assertTrue(!newDemoUser.equals(demoUser));

        // verify user is switched in crowd
        String newCrowdUser = getCurrentlyLoggedInCrowdUserFullName();
        assertEquals(newCrowdUser, newDemoUser);
    }

    public void testLoginToDemoConsequentlyLoggedInToCrowd_unauthorisedFailure()
    {
        log("Running testLoginToDemoConsequentlyLoggedInToCrowd_unauthorisedFailure");

        // explicitly logout of both applications
        logoutFromCrowd();
        logoutFromDemo();

        // login successfully to demo
        String demoUser = loginToDemo(DEMO_USER, DEMO_PW);
        assertNotNull(demoUser);

        // verify logged out of crowd as demo user doesn't have authentication access to crowd
        String crowdUser = getCurrentlyLoggedInCrowdUserFullName();
        assertNull(crowdUser);

        // verify still logged in to demo
        String currentDemoUser = getCurrentlyLoggedInDemoUserFullName();
        assertEquals(demoUser, currentDemoUser);
    }

    public void testLoginToCrowdConsequentlyLoggedInToDemo_unauthorisedFailure()
    {
        log("Running testLoginToCrowdConsequentlyLoggedInToDemo_unauthorisedFailure");

        // explicitly logout of both applications
        logoutFromCrowd();
        logoutFromDemo();

        // login successfully to crowd
        String crowdUser = loginToCrowd(CROWD_USER, CROWD_PW);
        assertNotNull(crowdUser);

        // verify logged out of demo as crowd user doesn't have authentication access to demo
        String demoUser = getCurrentlyLoggedInDemoUserFullName();
        assertNull(demoUser);

        // verify still logged in to crowd
        String currentCrowdUser = getCurrentlyLoggedInCrowdUserFullName();
        assertEquals(crowdUser, currentCrowdUser);
    }

    public void testSwitchUserInCrowdConsequentlySwitchesUserInDemo_unauthorisedFailure()
    {
        log("Running testSwitchUserInCrowdConsequentlySwitchesUserInDemo_unauthorisedFailure");

        // login to crowd (and hence demo)
        String crowdUser = loginToCrowd(CROWD_ADMIN_USER, ADMIN_PW);
        assertNotNull(crowdUser);
        String demoUser = getCurrentlyLoggedInDemoUserFullName();
        assertNotNull(demoUser);

        // logout of crowd
        logoutFromCrowd();

        // switch user in crowd to a user that doesn't have access to demo
        String newCrowdUser = loginToCrowd(CROWD_USER, CROWD_PW);
        assertTrue(!newCrowdUser.equals(crowdUser));

        // verify user is not switched in demo
        String newDemoUser = getCurrentlyLoggedInDemoUserFullName();
        assertNull(newDemoUser);
    }

    public void testSwitchUserInDemoConsequentlySwitchesUserInCrowd_unauthorisedFailure()
    {
        log("Running testSwitchUserInDemoConsequentlySwitchesUserInCrowd_unauthorisedFailure");

        // login to crowd (and hence demo)
        String crowdUser = loginToCrowd(CROWD_ADMIN_USER, ADMIN_PW);
        assertNotNull(crowdUser);
        String demoUser = getCurrentlyLoggedInDemoUserFullName();
        assertNotNull(demoUser);

        // logout of demo
        logoutFromDemo();

        // switch user in demo to a user that doesn't have access to crowd
        String newDemoUser = loginToDemo(DEMO_USER, DEMO_PW);
        assertTrue(!newDemoUser.equals(demoUser));

        // verify user is not switched in crowd
        String newCrowdUser = getCurrentlyLoggedInCrowdUserFullName();
        assertNull(newCrowdUser);
    }
}
