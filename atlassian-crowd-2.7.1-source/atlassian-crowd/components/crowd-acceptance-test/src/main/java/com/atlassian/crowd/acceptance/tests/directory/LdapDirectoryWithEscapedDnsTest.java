package com.atlassian.crowd.acceptance.tests.directory;

import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.tests.applications.crowd.EscapedDnTest;
import com.atlassian.crowd.acceptance.tests.applications.crowd.LdifLoaderForTesting;
import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.query.entity.UserQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;

import com.google.common.collect.ImmutableList;

import org.hamcrest.CoreMatchers;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

/**
 * This class goes out of its way to reconfigure its <code>RemoteDirectory</code> without involving Spring
 * or databases. It also modifies the test LDAP, so run it last in its suite.
 */
public class LdapDirectoryWithEscapedDnsTest extends BaseTest
{
    public LdapDirectoryWithEscapedDnsTest()
    {
        setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
    }

    @Override
    protected void loadTestData() throws Exception
    {
        InputStream in = EscapedDnTest.class.getResourceAsStream("EscapedDnTest-entries.ldif");

        assertNotNull(in);
        new LdifLoaderForTesting(CrowdAcceptanceTestCase.HOST_PATH).setLdif(in);
    }

    @Override
    protected void removeTestData()
    {
    }

    RemoteDirectory getReconfiguredDirectory(String baseDn) throws DirectoryInstantiationException
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_BASEDN_KEY, baseDn);

        return getRemoteDirectory();
    }

    public void testCreateNewUserWithoutSlashInDn() throws Exception
    {
        String username = "new-user";

        RemoteDirectory rd = getReconfiguredDirectory("ou=escaping,dc=example,dc=com");

        UserTemplate user = new UserTemplate(username);

        User added = rd.addUser(user, PasswordCredential.NONE);

        assertEquals(username, added.getName());

        rd.removeUser(username);
    }

    public void testCreateNewUserWithSlashInDn() throws Exception
    {
        String usernameWithSlashes = "New/User/with/Slashes";

        RemoteDirectory rd = getReconfiguredDirectory("ou=escaping,dc=example,dc=com");

        UserTemplate user = new UserTemplate(usernameWithSlashes);

        User added = rd.addUser(user, PasswordCredential.NONE);

        assertEquals(usernameWithSlashes, added.getName());

        rd.removeUser(usernameWithSlashes);
    }

    public void testSearchUsersWithoutSlashInBaseDn() throws Exception
    {
        RemoteDirectory rd = getReconfiguredDirectory("ou=escaping,dc=example,dc=com");

        List<User> users = rd.searchUsers(new UserQuery<User>(User.class, NullRestrictionImpl.INSTANCE, 0, UserQuery.ALL_RESULTS));
        assertThat(users, CoreMatchers.not(empty()));
    }

    public void testSearchUsersWhenSlashInBaseDn() throws Exception
    {
        RemoteDirectory rd = getReconfiguredDirectory("cn=group/with/slashes,ou=escaping,dc=example,dc=com");

        List<User> users = rd.searchUsers(new UserQuery<User>(User.class, NullRestrictionImpl.INSTANCE, 0, UserQuery.ALL_RESULTS));
        assertEquals(Collections.emptyList(), users);
    }

    public void testGetMembershipsReturnsAllMembershipsCorrectlyUnescaped() throws OperationFailedException
    {
        RemoteDirectory rd = getReconfiguredDirectory("ou=escaping,dc=example,dc=com");

        List<Membership> memberships = ImmutableList.copyOf(rd.getMemberships());

        assertThat(memberships, hasSize(1));

        assertThat(memberships.get(0).getUserNames(), containsInAnyOrder(
                "User \"with\" Quotes",
                "User with Spaces",
                "User/with/Slashes",
                "User,with,Commas",
                "User\\with\\Backslashes"));
        assertThat(memberships.get(0).getChildGroupNames(), empty());
    }
}
