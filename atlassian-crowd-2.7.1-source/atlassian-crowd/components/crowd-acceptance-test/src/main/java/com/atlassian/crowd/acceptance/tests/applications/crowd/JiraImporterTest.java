package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.net.URL;

public class JiraImporterTest extends AppImporterTestBase
{
    @Override
    String getHsqlFileName()
    {
        return "jiradb";
    }

    public void testImportJIRAUsers()
    {
        runImportTest("JIRA", 1, 3, 4);
    }

    public void testImportJIRAUsersWithNoConfiguration()
    {
        gotoImporters();

        clickButton("importatlassian");

        assertKeyPresent("dataimport.importatlassian.title");

        setTextField("configuration.databaseURL", "");

        setTextField("configuration.databaseDriver", "");

        setTextField("configuration.username", "");

        submit();

        assertKeyPresent("dataimport.configuration.application.error");
        assertKeyPresent("dataimport.configuration.databasedriver.error");
        assertKeyPresent("dataimport.configuration.databaseurl.error");
        assertKeyPresent("dataimport.configuration.username.error");
    }
}