package com.atlassian.crowd.acceptance.tests.applications.crowd;

import java.util.Arrays;

/**
 * Test the removing of a directory
 */
public class RemoveDirectoryTest extends CrowdAcceptanceTestCase
{
    private static final String ADMIN_INTERNAL_DIRECTORY_NAME = "Test Internal Directory";

    private static final String INTERNAL_DIRECTORY_NAME = "Second Directory";

    private static final String CROWD_DIRECTORY_NAME = "Remote Crowd Directory";

    private static final String CONNECTOR_DIRECTORY_NAME = "ApacheDS Directory";

    private static final String CUSTOM_DIRECTORY_NAME = "Custom Directory";

    private static final String DELEGATED_DIRECTORY_NAME = "Delegated Directory";

    private static final String VIEW_INTERNAL_DIRECTORY_PATH = "/crowd/console/secure/directory/viewinternal!default.action";
    private static final String VIEW_CROWD_DIRECTORY_PATH = "/crowd/console/secure/directory/viewremotecrowd!default.action";
    private static final String VIEW_DELEGATED_DIRECTORY_PATH = "/crowd/console/secure/directory/viewdelegated!default.action";
    private static final String VIEW_CUSTOM_DIRECTORY_PATH = "/crowd/console/secure/directory/viewcustom!default.action";
    private static final String VIEW_CONNECTOR_DIRECTORY_PATH = "/crowd/console/secure/directory/viewconnector!default.action";

    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("removedirectory.xml");
    }

    public void testRemoveInternalDirectoryFromConsole()
    {
        intendToModifyData();

        log("Running: testRemoveInternalDirectoryFromConsole");

        gotoBrowseDirectories();

        clickLinkWithExactText(ADMIN_INTERNAL_DIRECTORY_NAME);

        assertTextPresent(ADMIN_INTERNAL_DIRECTORY_NAME);

        clickLink("remove-directory");

        assertKeyPresent("directory.remove.text");

        // Submit the form
        submit();

        // Check remain on same page (remove directory page) and prevent lockout warning message is shown.
        assertKeyPresent("directory.remove.text");
        assertKeyNotPresent("updatesuccessful.label");
        assertKeyPresent("preventlockout.removedirectory.label", Arrays.asList(ADMIN_INTERNAL_DIRECTORY_NAME));
    }

    public void testRemoveRemoteCrowdDirectoryFromConsole()
    {
        intendToModifyData();

        log("Running: testRemoveRemoteCrowdDirectoryFromConsole");

        gotoBrowseDirectories();

        clickLinkWithExactText(CROWD_DIRECTORY_NAME);

        assertTextPresent(CROWD_DIRECTORY_NAME);

        clickLink("remove-directory");

        assertKeyPresent("directory.remove.text");

        // Submit the form
        submit();

        assertKeyPresent("updatesuccessful.label");

        assertTextNotInTable("directory-table", CROWD_DIRECTORY_NAME);
    }

    public void testRemoveSecondInternalDirectoryFromConsole()
    {
        intendToModifyData();

        log("Running: testRemoveSecondInternalDirectoryFromConsole");

        gotoBrowseDirectories();

        clickLinkWithExactText(INTERNAL_DIRECTORY_NAME);

        assertTextPresent(INTERNAL_DIRECTORY_NAME);

        clickLink("remove-directory");

        assertKeyPresent("directory.remove.text");

        // Submit the form
        submit();

        assertKeyPresent("updatesuccessful.label");

        assertTextNotInTable("directory-table", INTERNAL_DIRECTORY_NAME);
    }

    public void testRemoveConnectorDirectoryFromConsole()
    {
        intendToModifyData();

        log("Running: testRemoveConnectorDirectoryFromConsole");

        gotoBrowseDirectories();

        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME);

        assertTextPresent(CONNECTOR_DIRECTORY_NAME);

        clickLink("remove-directory");

        assertKeyPresent("directory.remove.text");

        // Submit the form
        submit();

        assertKeyPresent("updatesuccessful.label");

        assertTextNotInTable("directory-table", CONNECTOR_DIRECTORY_NAME);
    }

    public void testRemoveCustomDirectoryFromConsole()
    {
        intendToModifyData();

        log("Running: testRemoveCustomDirectoryFromConsole");

        gotoBrowseDirectories();

        clickLinkWithExactText(CUSTOM_DIRECTORY_NAME);

        assertTextPresent(CUSTOM_DIRECTORY_NAME);

        clickLink("remove-directory");

        assertKeyPresent("directory.remove.text");

        // Submit the form
        submit();

        assertKeyPresent("updatesuccessful.label");

        assertTextNotInTable("directory-table", CUSTOM_DIRECTORY_NAME);
    }

    public void testRemoveDelegatedDirectoryFromConsole()
    {
        intendToModifyData();

        log("Running: testRemoveDelegatedDirectoryFromConsole");

        gotoBrowseDirectories();

        clickLinkWithExactText(DELEGATED_DIRECTORY_NAME);

        assertTextPresent(DELEGATED_DIRECTORY_NAME);

        clickLink("remove-directory");

        assertKeyPresent("directory.remove.text");

        // Submit the form
        submit();

        assertKeyPresent("updatesuccessful.label");

        assertTextNotInTable("directory-table", DELEGATED_DIRECTORY_NAME);
    }

    /**
     * Tests that cancelling the removal of an internal directory goes back to the correct directory view
     * (viewinternal.action).
     */
    public void testCancelRemoveInternalDirectory()
    {
        log("Running: testCancelRemoveInternalDirectory");

        doCancelRemoveDirectoryTest(INTERNAL_DIRECTORY_NAME, VIEW_INTERNAL_DIRECTORY_PATH);
    }

    /**
     * Tests that cancelling the removal of a remote Crowd directory goes back to the correct directory view
     * (viewremotecrowd.action).
     */
    public void testCancelRemoveRemoteCrowdDirectory()
    {
        log("Running: testCancelRemoveRemoteCrowdDirectory");

        doCancelRemoveDirectoryTest(CROWD_DIRECTORY_NAME, VIEW_CROWD_DIRECTORY_PATH);
    }

    /**
     * Tests that cancelling the removal of a delegated directory goes back to the correct directory view
     * (viewdelegated.action).
     */
    public void testCancelRemoveDelegatedDirectory()
    {
        log("Running: testCancelRemoveDelegatedDirectory");

        doCancelRemoveDirectoryTest(DELEGATED_DIRECTORY_NAME, VIEW_DELEGATED_DIRECTORY_PATH);
    }

    /**
     * Tests that cancelling the removal of a connector directory goes back to the correct directory view
     * (viewconnector.action).
     */
    public void testCancelRemoveConnectorDirectory()
    {
        log("Running: testCancelRemoveConnectorDirectory");

        doCancelRemoveDirectoryTest(CONNECTOR_DIRECTORY_NAME, VIEW_CONNECTOR_DIRECTORY_PATH);
    }

    /**
     * Tests that cancelling the removal of a custom directory goes back to the correct directory view
     * (viewcustom.action).
     */
    public void testCancelRemoveCustomDirectory()
    {
        log("Running: testCancelRemoveCustomDirectory");

        doCancelRemoveDirectoryTest(CUSTOM_DIRECTORY_NAME, VIEW_CUSTOM_DIRECTORY_PATH);
    }

    /**
     * Tests that cancelling the removal of a directory goes back to the correct directory view.
     *
     * @param directoryName name of the directory to cancel the removal on
     * @param expectedPathOnCancel expected path on cancelling the remove directory operation
     */
    private void doCancelRemoveDirectoryTest(final String directoryName, final String expectedPathOnCancel)
    {
        tester.setScriptingEnabled(true);

        gotoBrowseDirectories();

        clickLinkWithExactText(directoryName);

        assertTextPresent(directoryName);

        clickLink("remove-directory");

        assertKeyPresent("directory.remove.text");

        clickButtonWithText("Cancel");

        assertTextPresent(directoryName);

        assertEquals(expectedPathOnCancel, tester.getTestingEngine().getPageURL().getPath());
        tester.setScriptingEnabled(false);
    }
}
