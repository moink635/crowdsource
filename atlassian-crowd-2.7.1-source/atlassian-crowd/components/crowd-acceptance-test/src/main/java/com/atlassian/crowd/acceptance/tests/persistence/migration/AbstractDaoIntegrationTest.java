package com.atlassian.crowd.acceptance.tests.persistence.migration;

import java.util.List;
import java.util.Set;

import com.atlassian.crowd.acceptance.tests.directory.ImmutableUser;
import com.atlassian.crowd.dao.alias.AliasDAO;
import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.dao.property.PropertyDAO;
import com.atlassian.crowd.dao.token.TokenDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.GroupDao;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.ImmutableApplication;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import org.junit.Assert;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.Assert.assertThat;

public abstract class AbstractDaoIntegrationTest
{
    private static final String NEW_GROUP_NAME = "new group";
    private static final String NEW_USER_NAME = "new user";
    private static final String NEW_DIRECTORY_NAME = "directory name";

    // some created domain objects are shared by multiple _test* methods
    private Directory newDirectory;
    private Application newApplication;
    private User newUser;
    private Group newGroup;
    private Token newToken;

    public abstract TokenDAOHibernate getTokenDao();
    public abstract ApplicationDAO getApplicationDao();
    public abstract UserDao getUserDao();
    public abstract GroupDao getGroupDao();
    public abstract MembershipDao getMembershipDao();
    public abstract DirectoryDao getDirectoryDao();
    public abstract AliasDAO getAliasDao();
    public abstract PropertyDAO getPropertyDao();

    protected void _testDirectoryDaoCanInsert() throws Exception
    {
        // add a new directory
        newDirectory = new DirectoryImpl(NEW_DIRECTORY_NAME, DirectoryType.INTERNAL, "implementation.Class");
        newDirectory = getDirectoryDao().add(newDirectory);
        assertThat(getDirectoryDao().findAll(), hasItem(newDirectory));
    }

    protected void _testUserDaoCanInsert() throws Exception
    {
        checkNotNull(newDirectory, "Create the directory before running this test");

        EntityQuery<User> allUsersQuery = QueryBuilder.queryFor(User.class, EntityDescriptor.user())
            .returningAtMost(EntityQuery.ALL_RESULTS);

        // add a new user
        PasswordCredential passwordCredential = new PasswordCredential("password", true);
        newUser = new ImmutableUser(newDirectory.getId(),
                                    NEW_USER_NAME, "user 1st", "user last", "display name", "email");
        newUser = getUserDao().add(newUser, passwordCredential);
        List<User> usersAfterInsertion = getUserDao().search(newDirectory.getId(), allUsersQuery);
        assertThat(usersAfterInsertion, hasItem(newUser));

        // store attributes
        getUserDao().storeAttributes(newUser, ImmutableMap.<String,Set<String>>of("name1", ImmutableSet.of("value1")));
        assertThat(getUserDao().findByNameWithAttributes(newDirectory.getId(), NEW_USER_NAME).getValue("name1"), is("value1"));
    }

    protected void _testGroupDaoCanInsert() throws Exception
    {
        checkNotNull(newDirectory, "Create the directory before running this test");

        EntityQuery<Group> allGroupsQuery = QueryBuilder.queryFor(Group.class, EntityDescriptor.group())
            .returningAtMost(EntityQuery.ALL_RESULTS);

        // add a new group
        Group groupTemplate = new GroupTemplate(NEW_GROUP_NAME, newDirectory.getId(), GroupType.GROUP);
        newGroup = getGroupDao().add(groupTemplate);
        List<Group> groupsAfterInsertion = getGroupDao().search(newDirectory.getId(), allGroupsQuery);
        assertThat(groupsAfterInsertion, hasItem(newGroup));
    }

    protected void _testMembershipDaoCanInsert() throws Exception
    {
        checkNotNull(newDirectory, "Create the directory before running this test");

        MembershipQuery<User> allMembershipsQuery =
            QueryBuilder.createMembershipQuery(EntityQuery.MAX_MAX_RESULTS, 0, true,
                                               EntityDescriptor.user(), User.class,
                                               EntityDescriptor.group(), NEW_GROUP_NAME);

        // add a new membership
        getMembershipDao().addUserToGroup(newDirectory.getId(), NEW_USER_NAME, NEW_GROUP_NAME);
        List<User> membershipsAfterInsertion = getMembershipDao().search(newDirectory.getId(), allMembershipsQuery);
        User newUser = getUserDao().findByName(newDirectory.getId(), NEW_USER_NAME);
        assertThat(membershipsAfterInsertion, hasItem(newUser));
    }

    protected void _testApplicationDaoCanInsert() throws Exception
    {
        checkNotNull(newDirectory, "Create the directory before running this test");

        RemoteAddress remoteAddress = new RemoteAddress("127.0.0.99");
        PasswordCredential passwordCredential = new PasswordCredential("password", true);
        EntityQuery<Application> allApplicationsQuery = QueryBuilder.queryFor(Application.class,
                                                                              EntityDescriptor.application())
            .returningAtMost(EntityQuery.ALL_RESULTS);

        // add a new application
        newApplication = ImmutableApplication.builder("new application", ApplicationType.GENERIC_APPLICATION)
                                             .setDescription("new description")
                                             .setPasswordCredential(passwordCredential)
                                             .setAttributes(ImmutableMap.of("name1", "value1"))
                                             .setRemoteAddresses(ImmutableSet.of(remoteAddress))
                                             .build();
        newApplication = getApplicationDao().add(newApplication, passwordCredential);
        List<Application> applicationsAfterInsertion = getApplicationDao().search(allApplicationsQuery);
        assertThat(applicationsAfterInsertion, hasItem(newApplication));

        // add a new remote address
        getApplicationDao().addRemoteAddress(newApplication.getId(), remoteAddress);
        assertThat(getApplicationDao().findById(newApplication.getId()).getRemoteAddresses(), hasItem(remoteAddress));

        // add a new directory mapping with a group mapping
        getApplicationDao().addDirectoryMapping(newApplication.getId(),
                                                newDirectory.getId(), false, OperationType.CREATE_USER);
        getApplicationDao().addGroupMapping(newApplication.getId(), newDirectory.getId(), "my group");
        assertThat(getApplicationDao().findById(newApplication.getId())
                       .getDirectoryMapping(newDirectory.getId()).getAuthorisedGroups(), hasSize(1));
    }

    protected void _testAliasDaoCanInsert()
    {
        // add a new alias
        getAliasDao().storeAlias(newApplication, NEW_USER_NAME, "new alias");

        Assert.assertEquals(getAliasDao().findUsernameByAlias(newApplication, "new alias"), NEW_USER_NAME);
    }

    protected void _testTokenDaoCanInsert() throws Exception
    {
        checkNotNull(newDirectory, "Create the directory before running this test");

        EntityQuery<Token> allTokensQuery = QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
            .returningAtMost(EntityQuery.ALL_RESULTS);

        // add a new token
        long tokenRandomNumber = 123456789L;
        newToken = getTokenDao().add(new Token.Builder(newDirectory.getId(), NEW_USER_NAME, "tokenHash",
                                                       tokenRandomNumber, "randomHash").create());

        List<Token> tokensAfterInsertion = getTokenDao().search(allTokensQuery);
        assertThat(tokensAfterInsertion, hasItem(newToken));
    }

    protected void _testPropertyDaoCanInsert()
    {
        // add a new property
        Property newProperty = new Property("key", "name", "value");
        newProperty = getPropertyDao().add(newProperty);

        assertThat(getPropertyDao().findAll(), hasItem(newProperty));
    }
}
