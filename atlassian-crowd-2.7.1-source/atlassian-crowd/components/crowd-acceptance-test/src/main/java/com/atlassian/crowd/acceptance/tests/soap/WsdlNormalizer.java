package com.atlassian.crowd.acceptance.tests.soap;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * Sorts WSDL documents to normalize them for test purposes.
 */
public class WsdlNormalizer
{
    static Document parseAndNormalize(String documentText) throws IOException, ParserConfigurationException, SAXException
    {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);
        
        DocumentBuilder db = dbf.newDocumentBuilder();
        
        Document doc = db.parse(new InputSource(new StringReader(documentText)));

        Element root = doc.getDocumentElement();
        
        sortChildren(root);
        
        return doc;
    }

    private static void sortChildren(Element parent)
    {
        List<Element> elements = new ArrayList<Element>();
        
        Node n = parent.getFirstChild();
        
        while (n != null)
        {
            if (n instanceof Element)
            {
                elements.add((Element) n);
            }
            n = n.getNextSibling();
        }

        for (Element e : elements)
        {
            parent.removeChild(e);
        }
        
        Collections.sort(elements, new WsdlElementComparator());
        
        for (Element e : elements)
        {
            parent.appendChild(e);
            if (e.getTagName().equals("wsdl:portType")
                    || e.getTagName().equals("xsd:schema")
                    || e.getTagName().equals("wsdl:types")
                    || e.getTagName().equals("wsdl:operation")
                    || e.getTagName().equals("wsdl:binding"))
            {
                sortChildren(e);
            }
        }
    }

    private static class WsdlElementComparator implements Comparator<Element>
    {
        public int compare(Element o1, Element o2)
        {
            int c1 = o1.getTagName().compareTo(o2.getTagName());
            if (c1 != 0)
            {
                return c1;
            }
            
            return o1.getAttribute("name").compareTo(o2.getAttribute("name"));
        }
    }
}
