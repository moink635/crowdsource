package com.atlassian.crowd.acceptance.tests.directory;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.SynchronisableDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelperImpl;
import com.atlassian.crowd.directory.loader.CacheableDirectoryInstanceLoader;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.directory.SynchronisationMode;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.TimedProgressOperation;

import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate4.SessionHolder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.junit.Assert.assertEquals;

/**
 * Tests {@link com.atlassian.crowd.directory.ldap.mapper.attribute.group.RFC4519MemberDnRangedMapper}
 * and {@link com.atlassian.crowd.directory.ldap.mapper.attribute.group.RFC4519MemberDnRangeOffsetMapper}.
 * This test is intended to be run without a transaction, therefore it uses manual session management.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:/remoteDirectoryApplicationContext-config.xml",
                      "classpath:/applicationContext-CrowdEncryption.xml",
                      "classpath:/applicationContext-CrowdUtils.xml",
                      "classpath:/applicationContext-CrowdDAO.xml",
                      "classpath:/applicationContext-config.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
                          DirtiesContextTestExecutionListener.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)  // because it modifies some mocks
public class DnRangeTest
{
    private static final Logger logger = LoggerFactory.getLogger(DnRangeTest.class);

    private static final String LARGE_GROUP_NAME = "DnRangeTest-LargeGroup";
    private static final String SMALL_GROUP_NAME = "DnRangeTest-SmallGroup";
    private static final String PASSWORD = "Test-Password-123";
    private static final String USERNAME_PREFIX = "dnRange-user-";
    private static final int LARGE_GROUP_SIZE = 1600;
    private static final int SMALL_GROUP_SIZE = 30;

    private final String directoryConfigFile;

    private DirectoryImpl directory;
    private RemoteDirectory remoteDirectory;
    private Session session;

    @Inject DirectoryDao directoryDao;
    @Inject private SessionFactory sessionFactory;
    @Inject private LDAPPropertiesHelperImpl ldapPropertiesHelperImpl;
    @Inject private MockDirectoryManager mockDirectoryManager;
    @Inject private DataSource dataSource;
    @Inject private CacheableDirectoryInstanceLoader directoryInstanceLoader;

    public DnRangeTest()
    {
        this(System.getProperty("directory.config.file"));
    }

    /**
     * JUnit4 requires exactly one public constructor, therefore this one is protected
     * @param directoryConfigFile
     */
    protected DnRangeTest(String directoryConfigFile)
    {
        checkNotNull(directoryConfigFile, "Directory config file not specified. Have you set system property -Ddirectory.config.file?");
        this.directoryConfigFile = directoryConfigFile;
    }

    @Before
    public void setUp() throws Exception
    {
        // clean up the database before a test run
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        DirectoryTestHelper.deleteFromTables(DirectoryTestHelper.TABLE_NAMES, jdbcTemplate);

        Properties properties = DirectoryTestHelper.getConfigProperties(directoryConfigFile);

        // this test is non-transactional, therefore a temporary session is manually managed for directoryDao
        session = sessionFactory.openSession();
        session.setFlushMode(FlushMode.ALWAYS);
        TransactionSynchronizationManager.unbindResourceIfPossible(sessionFactory); // clean up
        TransactionSynchronizationManager.bindResource(sessionFactory, new SessionHolder(session));

        // build our Crowd directory/connection object
        directory = configureDirectory(properties);
        checkNotNull("Overriders of configureDirectory() must initialise the directory Id", directory.getId());

        remoteDirectory = directoryInstanceLoader.getDirectory(directory);

        // no point running tests if we can't connect to server
        directoryInstanceLoader.getRawDirectory(directory.getId(),
                                                directory.getImplementationClass(),
                                                directory.getAttributes()).testConnection();

        // prepare the data in the LDAP server
        removeTestData();
        loadTestData();
    }

    @After
    public void tearDown() throws Exception
    {
        removeTestData();

        TransactionSynchronizationManager.unbindResource(sessionFactory);
        session.close();
    }

    /**
     * Subclasses can override and decorate this method to test different directory configurations
     *
     * @param directorySettings
     * @return the initialised directory, with an ID
     */
    protected DirectoryImpl configureDirectory(Properties directorySettings)
    {
        DirectoryImpl directoryTemplate = DirectoryTestHelper.createDirectoryTemplate(directorySettings, ldapPropertiesHelperImpl, false);

        DirectoryImpl addedDirectory = new DirectoryImpl(directoryDao.add(directoryTemplate));
        sessionFactory.getCurrentSession().flush();

        // Set the directoryId before adding directory to be available for mock directory manager
        InternalEntityTemplate template = new InternalEntityTemplate(addedDirectory.getId(), addedDirectory.getName(), true, new Date(), new Date());
        DirectoryImpl templateForMockDirectoryManager = new DirectoryImpl(template);
        templateForMockDirectoryManager.updateDetailsFrom(addedDirectory);

        // inject the directory into the mocks
        mockDirectoryManager.setDirectory(templateForMockDirectoryManager);

        return addedDirectory;
    }

    @Test
    public void testBrowseGroups() throws Exception
    {
        // Test together so only need to add/remove the users once
        _testBrowseLargeGroup();
        _testBrowseSmallGroup();
    }

    /**
     * Assumes group has more than 1500 members
     * <p/>
     * Check using two ways and assert the number of members found are equal
     *
     * @throws com.atlassian.crowd.exception.OperationFailedException
     * @throws InterruptedException
     */
    public void _testBrowseLargeGroup() throws Exception
    {
        logger.info("Testing _testBrowseLargeGroup");

        MembershipQuery membershipQuery = QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(LARGE_GROUP_NAME).returningAtMost(EntityQuery.ALL_RESULTS);

        // Find number of groups members (findUserMembersOfGroupViaMemberDN)
        directory.setAttribute(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE, "false");
        syncIfCacheEnabled();
        List userMembersViaMemberDn = remoteDirectory.searchGroupRelationships(membershipQuery); // this will test the attribute range iteration

        // Find number of groups members (findUserMembersOfGroupViaMemberOf)
        // with "Use the User Membership Attribute" checked
        directory.setAttribute(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE, "true");
        syncIfCacheEnabled();
        List userMembersViaMemberOf = remoteDirectory.searchGroupRelationships(membershipQuery); // standard we're comparing to

        // the following assertion is no longer valid as the DB caching directory does not copy the memberDNs attribute
        //assertTrue(group.getValues(RFC4519MemberDnRangeMapper.ATTRIBUTE_KEY).size() == LARGE_GROUP_SIZE); // range attribute only applies to vals > 1500
        assertEquals(LARGE_GROUP_SIZE, userMembersViaMemberDn.size());
        assertEquals(LARGE_GROUP_SIZE, userMembersViaMemberOf.size());
    }

    /**
     * Assumes group has less than 1500 members.
     * <p/>
     * This search will not need range iteration.
     *
     * @throws com.atlassian.crowd.exception.OperationFailedException
     * @throws InterruptedException
     *
     */
    public void _testBrowseSmallGroup() throws Exception
    {
        logger.info("Testing _testBrowseSmallGroup");

        MembershipQuery membershipQuery = QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(SMALL_GROUP_NAME).returningAtMost(EntityQuery.ALL_RESULTS);

        // Find number of groups members (findUserMembersOfGroupViaMemberDN)
        directory.setAttribute(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE, "false");
        syncIfCacheEnabled();
        List userMembersViaMemberDn = remoteDirectory.searchGroupRelationships(membershipQuery);

        // Find number of groups members (findUserMembersOfGroupViaMemberOf)
        // with "Use the User Membership Attribute" checked
        directory.setAttribute(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE, "true");
        syncIfCacheEnabled();
        List userMembersViaMemberOf = remoteDirectory.searchGroupRelationships(membershipQuery); // standard we're comparing to

        // the following assertion is no longer valid as the DB caching directory does not copy the memberDNs attribute
        // assertTrue(group.getValues(RFC4519MemberDnRangeMapper.ATTRIBUTE_KEY).size() == SMALL_GROUP_SIZE);
        assertEquals(SMALL_GROUP_SIZE, userMembersViaMemberDn.size());
        assertEquals(SMALL_GROUP_SIZE, userMembersViaMemberOf.size());
    }

    private void syncIfCacheEnabled() throws OperationFailedException
    {
        if (remoteDirectory instanceof SynchronisableDirectory)
        {
            ((SynchronisableDirectory) remoteDirectory).synchroniseCache(SynchronisationMode.FULL, new MockSynchronisationStatusManager());
        }
    }

    private void removeTestData()
    {
        // Remove group and users we've added for this test.
        try
        {
            if (remoteDirectory.findGroupByName(LARGE_GROUP_NAME) != null)
            {
                logger.info("Removing {} users from group {}", LARGE_GROUP_SIZE, LARGE_GROUP_NAME);
                TimedProgressOperation operation = new TimedProgressOperation("Removing users from " + LARGE_GROUP_NAME, LARGE_GROUP_SIZE, logger);
                for (int i = 0; i < LARGE_GROUP_SIZE; i++)
                {
                    operation.incrementedProgress();
                    DirectoryTestHelper.silentlyRemoveUser(USERNAME_PREFIX + i, remoteDirectory);
                }
                operation.complete("Removed users from group " + LARGE_GROUP_NAME);
                DirectoryTestHelper.silentlyRemoveGroup(LARGE_GROUP_NAME, remoteDirectory);
                DirectoryTestHelper.silentlyRemoveGroup(SMALL_GROUP_NAME, remoteDirectory);
                logger.info("Test data removed from the LDAP directory");
            }
        }
        catch (Exception e)
        {
        }
    }

    private void loadTestData() throws Exception
    {
        // Add our own Large/Small group and users
        DirectoryTestHelper.addGroup(LARGE_GROUP_NAME, directory.getId(), remoteDirectory);   // members: 1600
        DirectoryTestHelper.addGroup(SMALL_GROUP_NAME, directory.getId(), remoteDirectory);   // members: just 30 members

        logger.info("Creating {} users", LARGE_GROUP_SIZE);
        TimedProgressOperation addUsersOperation =
            new TimedProgressOperation("Creating " + LARGE_GROUP_SIZE + " users", LARGE_GROUP_SIZE, logger);
        for (int i = 0; i < LARGE_GROUP_SIZE; i++)
        {
            addUsersOperation.incrementProgress();
            DirectoryTestHelper.addUser(USERNAME_PREFIX + i, directory.getId(), PASSWORD, remoteDirectory);
        }
        addUsersOperation.complete("Users created");

        // ensure new users and groups are visible in cached directories
        session.flush();

        logger.info("Adding {} users to group {}", LARGE_GROUP_SIZE, LARGE_GROUP_NAME);
        TimedProgressOperation addMembershipsOperation =
            new TimedProgressOperation("Adding users to group " + LARGE_GROUP_NAME, LARGE_GROUP_SIZE, logger);
        for (int i = 0; i < LARGE_GROUP_SIZE; i++)
        {
            addMembershipsOperation.incrementProgress();
            remoteDirectory.addUserToGroup(USERNAME_PREFIX + i, LARGE_GROUP_NAME);
        }
        addMembershipsOperation.complete("Users added to group " + LARGE_GROUP_NAME);

        logger.info("Adding {} users to group {}", SMALL_GROUP_SIZE, SMALL_GROUP_NAME);
        for (int i = 0; i < SMALL_GROUP_SIZE; i++)
        {
            remoteDirectory.addUserToGroup(USERNAME_PREFIX + i, SMALL_GROUP_NAME);
        }

        // ensure new memberships are visible in cached directories
        session.flush();

        logger.info("Test data loaded to the LDAP directory");
    }
}
