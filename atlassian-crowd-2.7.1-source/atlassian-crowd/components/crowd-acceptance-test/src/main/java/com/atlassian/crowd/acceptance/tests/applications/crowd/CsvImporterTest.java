package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;

import net.sourceforge.jwebunit.html.Table;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Test's the adding of a CSV file into Crowd
 */
public class CsvImporterTest extends CrowdAcceptanceTestCase
{
    private static final String UNKNOWN_FILE_LOCATION = "unknownfilelocation";
    private String userFileLocation = null;
    private String groupmembershipFileLocation = null;

    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        _loginAdminUser();
        restoreBaseSetup();

        gotoImporters();

        clickButton("importcsv");

        userFileLocation = getCsvFileLocation("users.csv");
        groupmembershipFileLocation = getCsvFileLocation("groupmembers.csv");

    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testCsvConfigurationWithBlankForm()
    {
        log("Running testCsvConfigurationWithBlankForm");

        // Remove the default value from the delimiter field
        setTextField("delimiter", "");

        submit();

        // submit form with no inputs
        assertKeyPresent("dataimport.csv.delimiter.error");
        assertKeyPresent("dataimport.csv.userfile.error");
    }

    public void testCsvConfigurationWithInvalidFile()
    {
        log("Running testCsvConfigurationWithInvlidFile");

        setTextField("delimiter", ",");
        setTextField("users", UNKNOWN_FILE_LOCATION);
        setTextField("groupMemberships", UNKNOWN_FILE_LOCATION);

        submit();

        assertKeyPresent("dataimport.csv.fileinvalid.error", UNKNOWN_FILE_LOCATION);
        assertKeyPresent("dataimport.csv.fileinvalid.error", UNKNOWN_FILE_LOCATION);
    }

    public void testCsvImportWithUserFileOnly()
    {
        intendToModifyData();

        log("Running testCsvImportWithUserFileOnly");

        setTextField("delimiter", ",");
        setTextField("users", userFileLocation);

        submit();     // Successful

        assertKeyPresent("dataimport.csv.configuration.text");

        selectOptionByValue("user.0", "user.username");
        selectOptionByValue("user.1", "user.firstname");
        selectOptionByValue("user.2", "user.lastname");
        selectOptionByValue("user.3", "user.emailaddress");
        selectOptionByValue("user.4", "user.password");

        assertKeyNotPresent("dataimport.csv.configuration.groupmapping.label");

        submit();   // Successful

        assertKeyPresent("dataimport.csv.configuration.confirmation.text");

        // Assert the directory we have chosen is present
        assertKeyPresent("dataimport.csv.configuration.passwordencrypted.label", getMessage("yes.label"));
        assertKeyPresent("dataimport.csv.configuration.userfile.label", userFileLocation);

        assertKeyNotPresent("dataimport.csv.configuration.groupmembershipfile.label");

        String[][] userMappingsTableData = {new String[]{getMessage("dataimport.csv.configuration.headerrow"), getMessage("dataimport.csv.configuration.mapping")}, new String[]{"User Name", getMessage("user.username")}, new String[]{"First Name", getMessage("user.firstname")}, new String[]{"Last Name", getMessage("user.lastname")}, new String[]{"Email Address", getMessage("user.emailaddress")}, new String[]{"Password", getMessage("user.password")}};

        assertTableEquals("usermappings", new Table(userMappingsTableData));

        //Assert no Group data
        assertKeyNotPresent("dataimport.csv.configuration.groupmapping.label");
        assertTableNotPresent("groupmappings");

        submit(); // We are all good, run the import

        assertKeyPresent("dataimport.csv.result.text");

        //Assert results

        assertTextInElement("users-imported", "2");
        assertTextInElement("groups-imported", "0");
        assertTextInElement("memberships-imported", "0");

        // perform verification of import
        gotoBrowsePrincipals();
        assertMatchInTable("user-details", new String[]{"admin", "admin@example.com"});
        assertMatchInTable("user-details", new String[]{"JoeSmith", "jsmith@atlassian.com"});
        assertMatchInTable("user-details", new String[]{"peterblogs", "pblogs@atlassian.com"});
    }

    public void testCsvImportMappingsWithNoMappings()
    {
        log("Running testCsvImportMappingsWithNoMappings");

        setTextField("delimiter", ",");
        setTextField("users", userFileLocation);
        setTextField("groupMemberships", groupmembershipFileLocation);

        submit();     // Successful

        assertKeyPresent("dataimport.csv.configuration.text");

        submit(); // Failure

        assertKeyPresent("dataimport.csvmapping.missing.user.mapping.error", getMessage("user.firstname"));
        assertKeyPresent("dataimport.csvmapping.missing.user.mapping.error", getMessage("user.lastname"));
        assertKeyPresent("dataimport.csvmapping.missing.user.mapping.error", getMessage("user.emailaddress"));
        assertKeyPresent("dataimport.csvmapping.missing.user.mapping.error", getMessage("user.username"));

        assertKeyPresent("dataimport.csvmapping.missing.group.mapping.error", getMessage("group.name"));
        assertKeyPresent("dataimport.csvmapping.missing.group.mapping.error", getMessage("group.username"));
    }

    public void testCsvImportMappings()
    {
        intendToModifyData();

        log("Running testCsvImportMappings");

        setTextField("delimiter", ",");
        setTextField("users", userFileLocation);
        setTextField("groupMemberships", groupmembershipFileLocation);

        submit();     // Successful

        assertKeyPresent("dataimport.csv.configuration.text");

        selectOptionByValue("user.0", "user.username");
        selectOptionByValue("user.1", "user.firstname");
        selectOptionByValue("user.2", "user.lastname");
        selectOptionByValue("user.3", "user.emailaddress");
        selectOptionByValue("user.4", "user.password");

        selectOptionByValue("group.0", "group.username");
        selectOptionByValue("group.1", "group.name");

        submit();   // Successful

        assertKeyPresent("dataimport.csv.configuration.confirmation.text");

        // Assert the directory we have chosen is present
        //assertKeyPresent("dataimport.csv.configuration.directoryconfirmation.text", EasyList.build("Atlassian")); // Until we can load data specifically for the test
        assertKeyPresent("dataimport.csv.configuration.passwordencrypted.label", getMessage("yes.label"));
        assertKeyPresent("dataimport.csv.configuration.userfile.label", userFileLocation);
        assertKeyPresent("dataimport.csv.configuration.groupmembershipfile.label", groupmembershipFileLocation);

        String[][] userMappingsTableData = {new String[]{getMessage("dataimport.csv.configuration.headerrow"), getMessage("dataimport.csv.configuration.mapping")}, new String[]{"User Name", getMessage("user.username")}, new String[]{"First Name", getMessage("user.firstname")}, new String[]{"Last Name", getMessage("user.lastname")}, new String[]{"Email Address", getMessage("user.emailaddress")}, new String[]{"Password", getMessage("user.password")}};

        assertTableEquals("usermappings", new Table(userMappingsTableData));

        String[][] groupMappingsTableData = {new String[]{getMessage("dataimport.csv.configuration.headerrow"), getMessage("dataimport.csv.configuration.mapping")}, new String[]{"User", getMessage("group.username")}, new String[]{"Group", getMessage("group.name")}};

        assertTableEquals("groupmappings", new Table(groupMappingsTableData));

        submit(); // We are all good, run the import

        assertKeyPresent("dataimport.csv.result.text");

        assertTextInElement("users-imported", "2");
        assertTextInElement("groups-imported", "0");
        assertTextInElement("memberships-imported", "2");

        // perform verification of import
        gotoBrowsePrincipals();
        assertMatchInTable("user-details", new String[]{"admin", "admin@example.com"});
        assertMatchInTable("user-details", new String[]{"JoeSmith", "jsmith@atlassian.com"});
        assertMatchInTable("user-details", new String[]{"peterblogs", "pblogs@atlassian.com"});
    }

    public void testCsvImportExistingUsersAndGroupMappings() throws IOException
    {
        intendToModifyData();

        log("Running testCsvImportExistingUsersAndGroupMappings");

        userFileLocation = getCsvFileLocation("userswithexisting.csv");
        groupmembershipFileLocation = getCsvFileLocation("groupmemberswithexisting.csv");

        setTextField("delimiter", ",");
        setTextField("users", userFileLocation);
        setTextField("groupMemberships", groupmembershipFileLocation);

        submit();     // Successful

        assertKeyPresent("dataimport.csv.configuration.text");

        selectOptionByValue("user.0", "user.username");
        selectOptionByValue("user.1", "user.firstname");
        selectOptionByValue("user.2", "user.lastname");
        selectOptionByValue("user.3", "user.emailaddress");
        selectOptionByValue("user.4", "user.password");

        selectOptionByValue("group.0", "group.username");
        selectOptionByValue("group.1", "group.name");

        submit();   // Successful

        assertKeyPresent("dataimport.csv.configuration.confirmation.text");

        // Assert the directory we have chosen is present
        assertKeyPresent("dataimport.csv.configuration.passwordencrypted.label", getMessage("yes.label"));
        assertKeyPresent("dataimport.csv.configuration.userfile.label", userFileLocation);
        assertKeyPresent("dataimport.csv.configuration.groupmembershipfile.label", groupmembershipFileLocation);

        String[][] userMappingsTableData = {new String[]{getMessage("dataimport.csv.configuration.headerrow"), getMessage("dataimport.csv.configuration.mapping")}, new String[]{"User Name", getMessage("user.username")}, new String[]{"First Name", getMessage("user.firstname")}, new String[]{"Last Name", getMessage("user.lastname")}, new String[]{"Email Address", getMessage("user.emailaddress")}, new String[]{"Password", getMessage("user.password")}};

        assertTableEquals("usermappings", new Table(userMappingsTableData));

        String[][] groupMappingsTableData = {new String[]{getMessage("dataimport.csv.configuration.headerrow"), getMessage("dataimport.csv.configuration.mapping")}, new String[]{"User", getMessage("group.username")}, new String[]{"Group", getMessage("group.name")}};

        assertTableEquals("groupmappings", new Table(groupMappingsTableData));

        submit(); // We are all good, run the import

        assertKeyPresent("dataimport.csv.result.text");

        assertTextInElement("users-imported", "2");
        assertTextInElement("groups-imported", "1");
        assertTextInElement("memberships-imported", "2"); // should only be 2 since we already have a admin -> crowd-administrators membership

        // perform verification of import

        gotoBrowsePrincipals();
        assertUserInTable("admin", "Super User", "admin@example.com");
        assertUserInTable("JoeSmith", "Joe Smith", "jsmith@atlassian.com");
        assertUserInTable("peterblogs", "Peter Blogs", "pblogs@atlassian.com");

        gotoBrowseGroups();
        assertTextInTable("group-details", new String[]{"crowd-administrators", "true"});
        assertTextInTable("group-details", new String[]{"crowd-users", "true"});

        gotoViewGroup("crowd-users", "Test Internal Directory");
        clickLink("view-group-users");
        assertTextInTable("view-group-users", new String[]{"JoeSmith", "jsmith@atlassian.com", "true"});
        assertTextInTable("view-group-users", new String[]{"peterblogs", "pblogs@atlassian.com", "true"});
    }


    public void testCsvImportWithPipes() throws IOException
    {
        intendToModifyData();

        log("Running testCsvImportWithPipes");

        userFileLocation = getCsvFileLocation("userspipes.csv");
        groupmembershipFileLocation = getCsvFileLocation("groupmemberspipes.csv");

        setTextField("delimiter", "|");
        setTextField("users", userFileLocation);
        setTextField("groupMemberships", groupmembershipFileLocation);

        submit();     // Successful

        validateSampleTableData();
    }

    private void validateSampleTableData()
    {
        assertKeyPresent("dataimport.csv.configuration.text");

        String[][] principalMappingsTableData = {new String[]{getMessage("dataimport.csv.configuration.headerrow"), getMessage("dataimport.csv.configuration.sampledata"), getMessage("dataimport.csv.configuration.mapping")}, new String[]{"User Name", "peterblogs", getMessage("user.none")}, new String[]{"First Name", "Peter", getMessage("user.none")}, new String[]{"Last Name", "Blogs", getMessage("user.none")}, new String[]{"Email Address", "pblogs@atlassian.com", getMessage("user.none")}, new String[]{"Password", "secret", getMessage("user.none")}};

        assertTableEquals("principalsample", new Table(principalMappingsTableData));

        String[][] groupMappingsTableData = {new String[]{getMessage("dataimport.csv.configuration.headerrow"), getMessage("dataimport.csv.configuration.sampledata"), getMessage("dataimport.csv.configuration.mapping")}, new String[]{"User", "peterblogs", getMessage("group.none")}, new String[]{"Group", "crowd-administrators", getMessage("group.none")}};

        assertTableEquals("groupsample", new Table(groupMappingsTableData));
    }

    public void testCsvImportWithComma() throws IOException
    {
        intendToModifyData();

        log("Running testCsvImportWithComma");


        setTextField("delimiter", ",");
        setTextField("users", userFileLocation);
        setTextField("groupMemberships", groupmembershipFileLocation);

        submit();     // Successful

        validateSampleTableData();
    }

    public void testCsvImportInvalidDelimeter() throws IOException
    {
        log("Running testCsvImportInvalidDelimeter");

        userFileLocation = getCsvFileLocation("userspipes.csv");
        groupmembershipFileLocation = getCsvFileLocation("groupmemberspipes.csv");

        setTextField("delimiter", ",");
        setTextField("users", userFileLocation);
        setTextField("groupMemberships", groupmembershipFileLocation);

        submit();

        // Error - warn user that the wrong delimeter has been specified
        assertKeyPresent("dataimport.csv.delimiter.invalid.error", ",");
    }

    private String getCsvFileLocation(String fileName) throws IOException
    {
        // Locate the where the xmlfile is
        File fileLocation = AcceptanceTestHelper.getResource("com/atlassian/crowd/acceptance/tests/applications/crowd/" + fileName);

        if (!fileLocation.exists())
        {
            URL resource = ClassLoaderUtils.getResource("com/atlassian/crowd/acceptance/tests/applications/crowd/" + fileName, this.getClass());
            fileLocation = new File(resource.getFile());
        }

        if (!fileLocation.exists())
        {
            throw new IllegalArgumentException("File: " + fileName + " does not exist in the resources directory, under the 'com/atlassian/crowd/acceptance/tests/console/' directory");
        }

        return fileLocation.getCanonicalPath();
    }
}
