package com.atlassian.crowd.acceptance.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;


public class ArrayAssertions
{
    public static void assertContainsElements(String[] attributes, String... names)
    {
        assertEquals(names.length, attributes.length);

        List<String> attributeValues = Arrays.asList(attributes);

        for (String name : names)
        {
            assertTrue(attributeValues.contains(name));
        }
    }
}