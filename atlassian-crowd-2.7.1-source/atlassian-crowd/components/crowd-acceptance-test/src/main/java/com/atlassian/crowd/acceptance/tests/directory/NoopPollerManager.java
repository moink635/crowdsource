package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.directory.monitor.poller.DirectoryPoller;
import com.atlassian.crowd.manager.directory.SynchronisationMode;
import com.atlassian.crowd.manager.directory.monitor.DirectoryMonitorRegistrationException;
import com.atlassian.crowd.manager.directory.monitor.DirectoryMonitorUnregistrationException;
import com.atlassian.crowd.manager.directory.monitor.poller.DirectoryPollerManager;

/**
 * Noop instance of the poller manager that is used when driving the test harness.
 */
public class NoopPollerManager implements DirectoryPollerManager
{
    public void addPoller(final DirectoryPoller poller) throws DirectoryMonitorRegistrationException
    {
        // Do nothing
    }

    public boolean hasPoller(final long directoryID)
    {
        return false;
    }

    public void triggerPoll(long directoryID, SynchronisationMode synchronisationMode)
    {
        // Do nothing
    }

    public boolean removePoller(final long directoryID) throws DirectoryMonitorUnregistrationException
    {
        // Do nothing
        return false;
    }

    public void removeAllPollers()
    {
        // Do nothing
    }
}
