package com.atlassian.crowd.acceptance.tests.persistence.migration;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.inject.Inject;

import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.SynchronisableDirectoryProperties;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.impl.ConnectionPoolPropertyConstants;
import com.atlassian.crowd.manager.upgrade.UpgradeManager;
import com.atlassian.crowd.migration.legacy.database.DatabaseMapper;
import com.atlassian.crowd.migration.legacy.database.DatabaseMigrator;
import com.atlassian.crowd.migration.legacy.database.sql.HSQLLegacyTableQueries;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.build.BuildUtils;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static com.atlassian.crowd.embedded.api.OperationType.CREATE_GROUP;
import static com.atlassian.crowd.embedded.api.OperationType.CREATE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.DELETE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_GROUP;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_GROUP_ATTRIBUTE;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_USER_ATTRIBUTE;
import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext-databasemigrator-config.xml",
                "classpath:/applicationContext-CrowdDAO.xml",
                "classpath:/applicationContext-CrowdUtils.xml",
                "classpath:/applicationContext-CrowdEncryption.xml",
                "classpath:/applicationContext-CrowdUpgrader.xml"})
public class CrowdDatabaseMigrationTest extends AbstractTransactionalJUnit4SpringContextTests
{
    // Common/shared
    private static final String ID = "ID";
    private static final String CREATED_DATE = "CREATED_DATE";
    private static final String ACTIVE = "ACTIVE";
    private static final String DESCRIPTION = "DESCRIPTION";
    private static final String DIRECTORY_ID = "DIRECTORY_ID";
    private static final String CREDENTIAL = "CREDENTIAL";

    // Directory details
    private static final String DIRECTORY_NAME = "DIRECTORY_NAME";
    private static final String LOWER_DIRECTORY_NAME = "LOWER_DIRECTORY_NAME";
    private static final String IMPL_CLASS = "IMPL_CLASS";
    private static final String DIRECTORY_TYPE = "DIRECTORY_TYPE";
    private static final String LOWER_IMPL_CLASS = "LOWER_IMPL_CLASS";
    // class package locations are updated in UpgradeTask420
    private static final String INTERNAL_DIRECTORY_IMPL = "com.atlassian.crowd.directory.InternalDirectory";
    private static final String ACTIVE_DIRECTORY_IMPL = "com.atlassian.crowd.directory.MicrosoftActiveDirectory";

    // User details
    private static final String USER_NAME = "USER_NAME";
    private static final String LOWER_USER_NAME = "LOWER_USER_NAME";
    private static final String FIRST_NAME = "FIRST_NAME";
    private static final String LOWER_FIRST_NAME = "LOWER_FIRST_NAME";
    private static final String LAST_NAME = "LAST_NAME";
    private static final String LOWER_LAST_NAME = "LOWER_LAST_NAME";
    private static final String DISPLAY_NAME = "DISPLAY_NAME";
    private static final String LOWER_DISPLAY_NAME = "LOWER_DISPLAY_NAME";
    private static final String EMAIL_ADDRESS = "EMAIL_ADDRESS";
    private static final String LOWER_EMAIL_ADDRESS = "LOWER_EMAIL_ADDRESS";

    // User attributes
    private static final String ATTRIBUTE_COLOUR = "colours";
    private static final String ATTRIBUTE_FOOD = "food";
    private static final String PASSWORD_LAST_CHANGED = "passwordLastChanged";
    private static final String REQUIRES_PASSWORD_CHANGE = "requiresPasswordChange";
    private static final String LAST_AUTHENTICATED = "lastAuthenticated";
    private static final String INVALID_PASSWORD_ATTEMPTS = "invalidPasswordAttempts";

    // Group details
    private static final String GROUP_NAME = "GROUP_NAME";
    private static final String LOWER_GROUP_NAME = "LOWER_GROUP_NAME";
    private static final String GROUP_TYPE = "GROUP_TYPE";

    // Membership details
    private static final String MEMBERSHIP_TYPE = "MEMBERSHIP_TYPE";
    private static final String PARENT_NAME = "PARENT_NAME";
    private static final String LOWER_PARENT_NAME = "LOWER_PARENT_NAME";
    private static final String CHILD_NAME = "CHILD_NAME";
    private static final String LOWER_CHILD_NAME = "LOWER_CHILD_NAME";

    // Application details
    private static final String APPLICATION_NAME = "APPLICATION_NAME";
    private static final String LOWER_APPLICATION_NAME = "LOWER_APPLICATION_NAME";
    private static final String APPLICATION_TYPE = "APPLICATION_TYPE";
    private static final String APPLICATION_ID = "APPLICATION_ID";
    private static final String REMOTE_ADDRESS = "REMOTE_ADDRESS";
    private static final String LIST_INDEX = "LIST_INDEX";
    private static final String ALLOW_ALL = "ALLOW_ALL";

    // Property details
    private static final String PROPERTY_KEY = "PROPERTY_KEY";

    @Inject
    JdbcTemplate jdbcTemplate;

    @Inject
    SessionFactory sessionFactory;

    @Inject
    BatchProcessor batchProcessor;

    @Inject
    UpgradeManager upgradeManager;

    @Inject
    DatabaseMigrator legacyDatabaseMigrator;

    private DatabaseMapper databaseMapper; // for any convenience methods needed

    @Before
    public void setUp() throws Exception
    {
        assertNotNull(jdbcTemplate);
        assertNotNull(sessionFactory);
        assertNotNull(batchProcessor);

        databaseMapper = new DatabaseMapper(sessionFactory, batchProcessor, jdbcTemplate);

        assertEquals(1, countRowsInTable("hibernate_unique_key")); // will always have 1 entry

        assertEquals(0, countRowsInTable("cwd_user"));
        assertEquals(0, countRowsInTable("cwd_property"));
        assertEquals(0, countRowsInTable("cwd_group"));
        assertEquals(0, countRowsInTable("cwd_membership"));
        assertEquals(0, countRowsInTable("cwd_directory"));
        assertEquals(0, countRowsInTable("cwd_application"));
    }

    /**
     * Dates come back from the DB as {@link Timestamp}s. Convert
     * expected values here so we don't end up using the asymmetric
     * {@link Timestamp#equals(Object)}.
     */
    private Date getDateFromDatabase(String dateString)
    {
        Date d = databaseMapper.getDateFromDatabase(dateString);
        assertNotNull(d);
        return new Timestamp(d.getTime());
    }

    @Test
    public void testDatabaseMigration() throws Exception
    {
        legacyDatabaseMigrator.importDatabase(new HSQLLegacyTableQueries());

        upgradeManager.doUpgrade();

        assertEquals(1, countRowsInTable("hibernate_unique_key")); // will always have 1 entry
        assertEquals(0, countRowsInTable("cwd_token")); // this should have been cleared

        // Detailed check of all the tables
        assertDirectoryTables();
        assertUserTables();
        assertGroupTables();
        assertMembershipTables();
        assertApplicationTables();
        assertPropertyTables();
    }

    private static Matcher<Iterable<? extends Object>> containsInAnyOrder(Collection<?> c)
    {
        return Matchers.containsInAnyOrder(c.toArray());
    }

    public void assertDirectoryTables()
    {
        // cwd_directory
        // cwd_directory_attribute
        // cwd_directory_operation

        final Map<String, String> dir1ExpectedAttributes = new HashMap<String, String>();
        dir1ExpectedAttributes.put("password_history_count", "0");
        dir1ExpectedAttributes.put("password_max_attempts", "0");
        dir1ExpectedAttributes.put("password_max_change_time", "0");
        dir1ExpectedAttributes.put("user_encryption_method", "atlassian-security");

        final Map<String, String> dir3ExpectedAttributes = new HashMap<String, String>();
        dir3ExpectedAttributes.put(LDAPPropertiesMapper.ROLES_DISABLED, "false"); // added in UpgradeTask361
        dir3ExpectedAttributes.put(LDAPPropertiesMapper.LDAP_RELAXED_DN_STANDARDISATION, "true"); // added in UpgradeTask395 (default true)
//        dir3ExpectedAttributes.put("useCaching", "false"); // added in UpgradeTask396 and then removed in UpgradeTask424
//        dir3ExpectedAttributes.put("useMonitoring", "false"); // added in UpgradeTask396 and then removed in UpgradeTask424
        dir3ExpectedAttributes.put(DirectoryProperties.CACHE_ENABLED, "false"); // added in UpgradeTask428
        dir3ExpectedAttributes.put(LDAPPropertiesMapper.LDAP_BASEDN_KEY, "dc=sydney,dc=atlassian,dc=com");
        dir3ExpectedAttributes.put(LDAPPropertiesMapper.USER_OBJECTFILTER_KEY, "(&(objectCategory=Person)(sAMAccountName=*))");
        dir3ExpectedAttributes.put(LDAPPropertiesMapper.LDAP_URL_KEY, "ldap://crowd-ad1:389/");
        dir3ExpectedAttributes.put(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY, "true");
        dir3ExpectedAttributes.put(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE, "999");
        dir3ExpectedAttributes.put(LDAPPropertiesMapper.LDAP_USERDN_KEY, "cn=Administrator,cn=Users,dc=sydney,dc=atlassian,dc=com");
        dir3ExpectedAttributes.put(LDAPPropertiesMapper.LDAP_PASSWORD_KEY, "atlassian");
        dir3ExpectedAttributes.put(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL, "3600"); // UpgradeTask424
        dir3ExpectedAttributes.put(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED, "true"); // UpgradeTask524
        dir3ExpectedAttributes.put(LDAPPropertiesMapper.LDAP_EXTERNAL_ID, "objectGUID"); // UpgradeTask622ExternalId

        // Quick sanity check
        assertEquals(3, countRowsInTable("cwd_directory"));
        assertEquals(28 + dir1ExpectedAttributes.size() + dir3ExpectedAttributes.size(), countRowsInTable("cwd_directory_attribute"));
        assertEquals(28, countRowsInTable("cwd_directory_operation"));

        // --------------------------------------------
        // TESTING: cwd_directory
        // --------------------------------------------
        String sqlQuery = new StringBuilder()
                .append("SELECT id, directory_name, lower_directory_name, created_date, ")
                .append("RTRIM(active) ACTIVE, description, impl_class, lower_impl_class, directory_type ")
                .append("FROM cwd_directory").toString();
        List<Map<String, Object>> queriedDirectoriesList = convertElementsToHashMap(jdbcTemplate.queryForList(sqlQuery));
        List<Map<String, Object>> actualDirectoriesList = createActualDirectoriesList();
        assertEquals(actualDirectoriesList.size(), queriedDirectoriesList.size());

        assertThat(queriedDirectoriesList, containsInAnyOrder(actualDirectoriesList));

        // --------------------------------------------
        // TESTING: cwd_directory_attribute
        // --------------------------------------------
        // directory 1
        sqlQuery = new StringBuilder()
                .append("SELECT attribute_value, attribute_name ")
                .append("FROM cwd_directory_attribute ")
                .append("WHERE directory_id=1").toString();
        Map<String, String> directoryAttributes = databaseMapper.attributeListToMap(jdbcTemplate.query(sqlQuery, new AttributeMapper()));

        assertEquals(dir1ExpectedAttributes, directoryAttributes);

        // directory 3
        sqlQuery = new StringBuilder()
                .append("SELECT attribute_value, attribute_name ")
                .append("FROM cwd_directory_attribute ")
                .append("WHERE directory_id=3").toString();
        directoryAttributes = databaseMapper.attributeListToMap(jdbcTemplate.query(sqlQuery, new AttributeMapper()));

        assertEquals(24 + dir3ExpectedAttributes.size(), directoryAttributes.size());

        assertTrue("\n Expected all in: " + new TreeMap<String, String>(dir3ExpectedAttributes) +
                   "\n Got:             " + new TreeMap<String, String>(directoryAttributes),
                   directoryAttributes.entrySet().containsAll(dir3ExpectedAttributes.entrySet()));

        // --------------------------------------------
        // TESTING: cwd_directory_operation
        // --------------------------------------------
        sqlQuery = new StringBuilder()
                .append("SELECT operation_type ")
                .append("FROM cwd_directory_operation ")
                .append("WHERE directory_id=1").toString();
        List<OperationType> directoryOperations = jdbcTemplate.query(sqlQuery, new OperationsMapper());
        assertEquals(EnumSet.allOf(OperationType.class), EnumSet.copyOf(directoryOperations));

        sqlQuery = new StringBuilder()
                .append("SELECT operation_type ")
                .append("FROM cwd_directory_operation ")
                .append("WHERE directory_id=2").toString();
        directoryOperations = jdbcTemplate.query(sqlQuery, new OperationsMapper());
        assertEquals(EnumSet.of(CREATE_USER, UPDATE_USER, UPDATE_USER_ATTRIBUTE, DELETE_USER), EnumSet.copyOf(directoryOperations));
    }

    public void assertUserTables()
    {
        // cwd_user
        // cwd_user_attribute
        // cwd_user_credential_record

        // Quick sanity check
        assertEquals(4, countRowsInTable("cwd_user"));
        assertEquals(18, countRowsInTable("cwd_user_attribute"));
        assertEquals(0, countRowsInTable("cwd_user_credential_record"));

        // --------------------------------------------
        // TESTING: cwd_user
        // --------------------------------------------

        // Ignoring user id for now since the order the users are added into the db may not be consistent
        String sqlQuery = new StringBuilder()
                .append("SELECT user_name, lower_user_name, RTRIM(active) ACTIVE, created_date, first_name, lower_first_name, ")
                .append("last_name, lower_last_name, display_name, lower_display_name, ")
                .append("email_address, lower_email_address, directory_id, credential ")
                .append("FROM cwd_user").toString();
        List<Map<String, Object>> queriedUsers = convertElementsToHashMap(jdbcTemplate.queryForList(sqlQuery));
        List<Map<String, Object>> actualUsers = createActualUsersList();

        assertEquals(actualUsers.size(), queriedUsers.size());
        assertThat(queriedUsers, containsInAnyOrder(actualUsers));

        // user ids should all be distinct - but assert just to make doubly sure
        sqlQuery = "SELECT COUNT(DISTINCT id) FROM cwd_user";
        assertEquals(Integer.valueOf(actualUsers.size()), jdbcTemplate.queryForObject(sqlQuery, Integer.class));

        // --------------------------------------------
        // TESTING: cwd_user_attribute
        // --------------------------------------------

        // Use nested SELECTs to get to the specific user we want
        // (user id is dependent on the order the users were added and we can't control this)
        sqlQuery = new StringBuilder()
                .append("SELECT attribute_name, attribute_value ")
                .append("FROM cwd_user_attribute ")
                .append("WHERE user_id=")
                .append("(SELECT id FROM cwd_user WHERE user_name='test1' AND directory_id=1)").toString();
        Map<String, Set<String>> userAttributes = databaseMapper.attributeListToMultiAttributeMap(jdbcTemplate.query(sqlQuery, new AttributeMapper()));
        assertEquals(4, userAttributes.size());
        assertEquals(3, userAttributes.get(ATTRIBUTE_COLOUR).size());
        assertTrue(userAttributes.get(ATTRIBUTE_COLOUR).containsAll(Arrays.asList("blue", "green", "red")));
        assertEquals("pastry", userAttributes.get(ATTRIBUTE_FOOD).iterator().next());
        assertEquals("1265170034108", userAttributes.get(PASSWORD_LAST_CHANGED).iterator().next());
        assertEquals("false", userAttributes.get(REQUIRES_PASSWORD_CHANGE).iterator().next());
        assertNull(userAttributes.get(LAST_AUTHENTICATED));

        sqlQuery = new StringBuilder()
                .append("SELECT attribute_name, attribute_value ")
                .append("FROM cwd_user_attribute ")
                .append("WHERE user_id=")
                .append("(SELECT id FROM cwd_user WHERE user_name='admin' AND directory_id=1)").toString();
        userAttributes = databaseMapper.attributeListToMultiAttributeMap(jdbcTemplate.query(sqlQuery, new AttributeMapper()));
        assertEquals(4, userAttributes.size());
        assertEquals("1265774846524", userAttributes.get(LAST_AUTHENTICATED).iterator().next());
        assertEquals("0", userAttributes.get(INVALID_PASSWORD_ATTEMPTS).iterator().next());
        assertEquals("1265170401218", userAttributes.get(PASSWORD_LAST_CHANGED).iterator().next());
        assertEquals("false", userAttributes.get(REQUIRES_PASSWORD_CHANGE).iterator().next());
        assertNull(userAttributes.get(ATTRIBUTE_COLOUR));

        sqlQuery = new StringBuilder()
                .append("SELECT attribute_name, attribute_value ")
                .append("FROM cwd_user_attribute ")
                .append("WHERE user_id=")
                .append("(SELECT id FROM cwd_user WHERE user_name='test1' AND directory_id=2)").toString();
        userAttributes = databaseMapper.attributeListToMultiAttributeMap(jdbcTemplate.query(sqlQuery, new AttributeMapper()));
        assertEquals(4, userAttributes.size());
        assertEquals(3, userAttributes.get(ATTRIBUTE_COLOUR).size());
        assertTrue(userAttributes.get(ATTRIBUTE_COLOUR).containsAll(Arrays.asList("Black", "White", "grey")));
        assertEquals("Chocolate", userAttributes.get(ATTRIBUTE_FOOD).iterator().next());
        assertEquals("1265170186131", userAttributes.get(PASSWORD_LAST_CHANGED).iterator().next());
        assertEquals("false", userAttributes.get(REQUIRES_PASSWORD_CHANGE).iterator().next());
        assertNull(userAttributes.get(LAST_AUTHENTICATED));

        sqlQuery = new StringBuilder()
                .append("SELECT attribute_name, attribute_value ")
                .append("FROM cwd_user_attribute ")
                .append("WHERE user_id=")
                .append("(SELECT id FROM cwd_user WHERE user_name='testuser' AND directory_id=1)").toString();
        userAttributes = databaseMapper.attributeListToMultiAttributeMap(jdbcTemplate.query(sqlQuery, new AttributeMapper()));
        assertEquals(2, userAttributes.size());
        assertEquals("1265170333000", userAttributes.get(PASSWORD_LAST_CHANGED).iterator().next());
        assertEquals("false", userAttributes.get(REQUIRES_PASSWORD_CHANGE).iterator().next());
        assertNull(userAttributes.get(LAST_AUTHENTICATED));

        // --------------------------------------------
        // TESTING: cwd_user_credential_record
        // --------------------------------------------
        sqlQuery = "SELECT * FROM cwd_user_credential_record";
        assertEquals(0, jdbcTemplate.queryForList(sqlQuery).size());
    }

    public void assertGroupTables()
    {
        // cwd_group
        // cwd_group_attribute

        // Quick sanity check
        assertEquals(4, countRowsInTable("cwd_group"));
        assertEquals(0, countRowsInTable("cwd_group_attribute"));

        // --------------------------------------------
        // TESTING: cwd_group
        // --------------------------------------------

        // Ignoring 'id' for group for now - since groups maybe added in different order.
        String sqlQuery = new StringBuilder()
                .append("SELECT group_name, lower_group_name, RTRIM(active) ACTIVE, created_date, description, group_type, directory_id ")
                .append("FROM cwd_group").toString();

        List<Map<String, Object>> queriedGroups = convertElementsToHashMap(jdbcTemplate.queryForList(sqlQuery));
        List<Map<String, Object>> actualGroups = createActualGroupsList();

        assertEquals(actualGroups.size(), queriedGroups.size());
        assertThat(queriedGroups, containsInAnyOrder(actualGroups));

        sqlQuery = "SELECT COUNT(DISTINCT id) FROM cwd_group"; // check that all IDs are different
        assertEquals(Integer.valueOf(actualGroups.size()), jdbcTemplate.queryForObject(sqlQuery, Integer.class));

        // --------------------------------------------
        // TESTING: cwd_group_attribute
        // --------------------------------------------
        sqlQuery = "SELECT * FROM cwd_group_attribute";
        assertEquals(0, jdbcTemplate.queryForList(sqlQuery).size());
    }

    public void assertMembershipTables()
    {
        // cwd_membership

        // Quick sanity check
        assertEquals(6, countRowsInTable("cwd_membership"));

        // --------------------------------------------
        // TESTING: cwd_membership
        // --------------------------------------------

        // Ignoring parent_id (group) and child_id (user)
        // (ids are dependent on the order they were added into the db)
        String sqlQuery = new StringBuilder()
                .append("SELECT membership_type, group_type, ")
                .append("parent_name, lower_parent_name, child_name, lower_child_name, ")
                .append("directory_id ")
                .append("FROM cwd_membership").toString();
        List<Map<String, Object>> queriedMemberships = convertElementsToHashMap(jdbcTemplate.queryForList(sqlQuery));
        List<Map<String, Object>> actualMemberships = createActualMembershipsList();
        assertEquals(actualMemberships.size(), queriedMemberships.size());
        assertTrue(queriedMemberships.containsAll(actualMemberships));

        // Some basic checks on counts of the id
        // There are 4 groups with members
        sqlQuery = "SELECT COUNT(DISTINCT parent_id) FROM cwd_membership";
        assertEquals(Integer.valueOf(4), jdbcTemplate.queryForObject(sqlQuery, Integer.class));

        // There are 3 users with group memberships
        sqlQuery = "SELECT COUNT(DISTINCT child_id) FROM cwd_membership";
        assertEquals(Integer.valueOf(3), jdbcTemplate.queryForObject(sqlQuery, Integer.class));
    }

    public void assertApplicationTables()
    {
        // cwd_application
        // cwd_application_address
        // cwd_application_alias
        // cwd_application_attribute
        // cwd_app_dir_operation
        // cwd_app_dir_mapping
        // cwd_app_dir_group_mapping

        // Quick sanity check
        assertEquals(4, countRowsInTable("cwd_application"));
        assertEquals(10, countRowsInTable("cwd_application_address"));
        assertEquals(0, countRowsInTable("cwd_application_alias"));
        assertEquals(8, countRowsInTable("cwd_application_attribute"));
        assertEquals(5, countRowsInTable("cwd_app_dir_mapping"));
        assertEquals(55, countRowsInTable("cwd_app_dir_operation"));
        assertEquals(5, countRowsInTable("cwd_app_dir_group_mapping"));

        // The application IDs will have changed a bit during the migration.
        // Creating a simple mapping between old and new so we can test the cwd_application_* tables properly
        String sqlQuery = "SELECT id, application_name FROM cwd_application";
        Map<Long, Long> convertedApplicationIdMap = getApplicationIdMappings(sqlQuery, new ApplicationIdMapper());

        // --------------------------------------------
        // TESTING: cwd_application
        // --------------------------------------------
        sqlQuery = new StringBuilder()
                .append("SELECT application_name, lower_application_name, ")
                .append("created_date, RTRIM(active) ACTIVE, description, application_type, credential ")
                .append("FROM cwd_application").toString();
        List<Map<String, Object>> queriedValues = convertElementsToHashMap(jdbcTemplate.queryForList(sqlQuery));
        List<Map<String, Object>> actualValues = createActualApplicationsList();
        assertEquals(actualValues.size(), queriedValues.size());
        assertThat("Table <cwd_application> does not match expected data", queriedValues,
                containsInAnyOrder(actualValues));


        // --------------------------------------------
        // TESTING: cwd_application_address
        // --------------------------------------------
        sqlQuery = "SELECT * FROM cwd_application_address";
        queriedValues = convertElementsToHashMap(jdbcTemplate.queryForList(sqlQuery));
        actualValues = createActualAddressesList(convertedApplicationIdMap);
        assertEquals(actualValues.size(), queriedValues.size());
        assertThat("Table <cwd_application_address> does not match expected data", queriedValues,
                containsInAnyOrder(actualValues));

        // --------------------------------------------
        // TESTING: cwd_application_alias
        // --------------------------------------------
        assertEquals(0, jdbcTemplate.queryForList("SELECT * FROM cwd_application_alias").size());

        // --------------------------------------------
        // TESTING: cwd_application_attribute
        // --------------------------------------------
        sqlQuery = "SELECT * FROM cwd_application_attribute";
        queriedValues = convertElementsToHashMap(jdbcTemplate.queryForList(sqlQuery));
        actualValues = createActualAttributeList(convertedApplicationIdMap);
        assertEquals(actualValues.size(), queriedValues.size());
        assertThat("Table <cwd_application_attribute> does not match expected data", queriedValues,
                containsInAnyOrder(actualValues));

        // --------------------------------------------
        // TESTING: cwd_app_dir_mapping
        // --------------------------------------------
        sqlQuery = "SELECT application_id, directory_id, RTRIM(allow_all) ALLOW_ALL, list_index FROM cwd_app_dir_mapping";
        queriedValues = convertElementsToHashMap(jdbcTemplate.queryForList(sqlQuery));
        actualValues = createActualAppDirMappingList(convertedApplicationIdMap);
        assertEquals(actualValues.size(), queriedValues.size());
        assertThat("Table <cwd_app_dir_mapping> does not match expected data", queriedValues,
                containsInAnyOrder(actualValues));

        // --------------------------------------------
        // TESTING: cwd_app_dir_operation
        // --------------------------------------------
        AppDirOperationMapper appDirOperationMapper = new AppDirOperationMapper();
        sqlQuery = new StringBuilder()
                .append("SELECT operation_type ")
                .append("FROM cwd_app_dir_operation ")
                .append("WHERE app_dir_mapping_id=")
                .append("(SELECT id FROM cwd_app_dir_mapping ")
                .append("WHERE application_id=? ")
                .append("AND directory_id=?)").toString();

        // Check 'crowd'
        List<OperationType> operations = jdbcTemplate.query(sqlQuery, new Object[]{convertedApplicationIdMap.get(163841L), 1L}, appDirOperationMapper);
        assertEquals(EnumSet.allOf(OperationType.class), EnumSet.copyOf(operations));

        // Check 'crowd-openid-server'
        operations = jdbcTemplate.query(sqlQuery, new Object[]{convertedApplicationIdMap.get(163842L), 1L}, appDirOperationMapper);
        assertEquals(EnumSet.allOf(OperationType.class), EnumSet.copyOf(operations));

        // Check 'demo'
        operations = jdbcTemplate.query(sqlQuery, new Object[]{convertedApplicationIdMap.get(163843L), 1L}, appDirOperationMapper);
        assertEquals(EnumSet.allOf(OperationType.class), EnumSet.copyOf(operations));

        // Check 'google-apps' (directory 1)
        operations = jdbcTemplate.query(sqlQuery, new Object[]{convertedApplicationIdMap.get(163844L), 1L}, appDirOperationMapper);
        assertEquals(EnumSet.of(CREATE_GROUP, UPDATE_GROUP, UPDATE_GROUP_ATTRIBUTE, CREATE_USER, UPDATE_USER, UPDATE_USER_ATTRIBUTE, DELETE_USER), EnumSet.copyOf(operations));

        // Check 'google-apps' (directory 2)
        operations = jdbcTemplate.query(sqlQuery, new Object[]{convertedApplicationIdMap.get(163844L), 2L}, appDirOperationMapper);
        assertEquals(EnumSet.allOf(OperationType.class), EnumSet.copyOf(operations));

        // --------------------------------------------
        // TESTING: cwd_app_dir_group_mapping
        // --------------------------------------------
        AppDirGroupMapper appDirGroupMapper = new AppDirGroupMapper();
        sqlQuery = new StringBuilder()
                .append("SELECT group_name ")
                .append("FROM cwd_app_dir_group_mapping ")
                .append("WHERE app_dir_mapping_id=")
                .append("(SELECT id FROM cwd_app_dir_mapping ")
                .append("WHERE application_id=? ")
                .append("AND directory_id=?)").toString();

        // Check 'crowd'
        List<String> groups = jdbcTemplate.query(sqlQuery, new Object[]{convertedApplicationIdMap.get(163841L), 1L}, appDirGroupMapper);
        assertEquals(1, groups.size());
        assertEquals("crowd-administrators", groups.get(0));

        // Check 'crowd-openid-server'
        groups = jdbcTemplate.query(sqlQuery, new Object[]{convertedApplicationIdMap.get(163842L), 1L}, appDirGroupMapper);
        assertEquals(1, groups.size());
        assertEquals("crowd-administrators", groups.get(0));

        // Check 'demo'
        groups = jdbcTemplate.query(sqlQuery, new Object[]{convertedApplicationIdMap.get(163843L), 1L}, appDirGroupMapper);
        assertEquals(1, groups.size());
        assertEquals("crowd-administrators", groups.get(0));

        // Check 'google-apps'
        groups = jdbcTemplate.query(sqlQuery, new Object[]{convertedApplicationIdMap.get(163844L), 1L}, appDirGroupMapper);
        assertEquals(2, groups.size());
        assertTrue(groups.containsAll(Arrays.asList("crowd-administrators", "test-group1")));

        // Check 'google-apps'
        groups = jdbcTemplate.query(sqlQuery, new Object[]{convertedApplicationIdMap.get(163844L), 2L}, appDirGroupMapper);
        assertThat(groups, Matchers.empty());
    }

    public void assertPropertyTables()
    {
        // cwd_property

        // Quick sanity check
        assertEquals(29, countRowsInTable("cwd_property"));

        // --------------------------------------------
        // TESTING: cwd_property
        // --------------------------------------------
        String sqlQuery = "SELECT property_name, property_value FROM cwd_property ORDER BY property_name";
        Map<String, String> queriedValues = databaseMapper.attributeListToMap(jdbcTemplate.query(sqlQuery, new PropertyTableMapper()));
        assertEquals(29, queriedValues.size());

        // Check values from SERVERPROPERTY are here
        assertEquals(BuildUtils.BUILD_NUMBER, queriedValues.get(Property.BUILD_NUMBER));
        assertEquals("true", queriedValues.get(Property.CACHE_ENABLED));
        assertEquals("0", queriedValues.get(Property.CURRENT_LICENSE_RESOURCE_TOTAL));
        assertEquals("true", queriedValues.get(Property.DATABASE_TOKEN_STORAGE_ENABLED));
        assertEquals("Test Crowd Migration", queriedValues.get(Property.DEPLOYMENT_TITLE));
        assertEquals("6dMZIOr0MZc=", queriedValues.get(Property.DES_ENCRYPTION_KEY));
        assertEquals("true", queriedValues.get(Property.GZIP_ENABLED));
        assertEquals("localhost", queriedValues.get(Property.MAILSERVER_HOST));
        assertEquals("", queriedValues.get(Property.MAILSERVER_JNDI_LOCATION));
        assertEquals("Hello $firstname $lastname, Your password has been reset by a $deploymenttitle administrator at $date. Your new password is: $password $deploymenttitle Administrator", queriedValues.get(Property.FORGOTTEN_PASSWORD_EMAIL_TEMPLATE));
        assertEquals("", queriedValues.get(Property.MAILSERVER_PASSWORD));
        assertEquals("25", queriedValues.get(Property.MAILSERVER_PORT));
        assertEquals("[Test Crowd Migration - Atlassian Crowd]", queriedValues.get(Property.MAILSERVER_PREFIX));
        assertEquals("admin@example.com", queriedValues.get(Property.MAILSERVER_SENDER));
        assertEquals("", queriedValues.get(Property.MAILSERVER_USERNAME));
        assertEquals("admin@example.com", queriedValues.get(Property.NOTIFICATION_EMAIL));
        assertEquals("false", queriedValues.get(Property.SECURE_COOKIE));
        assertEquals("1800000", queriedValues.get(Property.SESSION_TIME));
        assertEquals("YFWGPzH6", queriedValues.get(Property.TOKEN_SEED));

        // Check values from SALPROPERTY are here too
        assertEquals("<list> <map> <entry> <string>password</string> <null/> </entry> <entry> <string>class</string> <java-class>com.atlassian.notifier.NotificationSubscriptionImpl</java-class> </entry> <entry> <string>username</string> <null/> </entry> <entry> <string>url</string> <string>http://localhost/wiki/plugins/servlet/crowdnotify</string> </entry> <entry> <string>authenticationType</string> <com.atlassian.notifier.AuthenticationType>NONE</com.atlassian.notifier.AuthenticationType> </entry> </map> <map> <entry> <string>password</string> <null/> </entry> <entry> <string>class</string> <java-class>com.atlassian.notifier.NotificationSubscriptionImpl</java-class> </entry> <entry> <string>username</string> <null/> </entry> <entry> <string>url</string> <string>http://localhost:3990/plugins/servlet/crowdnotify</string> </entry> <entry> <string>authenticationType</string> <com.atlassian.notifier.AuthenticationType>NONE</com.atlassian.notifier.AuthenticationType> </entry> </map> </list>",
                queriedValues.get("dummyName"));
        assertEquals("A not so interesting value", queriedValues.get("Some SAL Property Name"));
        assertEquals("(something here)", queriedValues.get("this.is.a.property.name"));

        // Check values for LDAP connection pool settings are here too
        assertEquals(ConnectionPoolPropertyConstants.DEFAULT_INITIAL_POOL_SIZE, queriedValues.get(ConnectionPoolPropertyConstants.POOL_INITIAL_SIZE));
        assertEquals(ConnectionPoolPropertyConstants.DEFAULT_PREFERRED_POOL_SIZE, queriedValues.get(ConnectionPoolPropertyConstants.POOL_PREFERRED_SIZE));
        assertEquals(ConnectionPoolPropertyConstants.DEFAULT_MAXIMUM_POOL_SIZE, queriedValues.get(ConnectionPoolPropertyConstants.POOL_MAXIMUM_SIZE));
        assertEquals(ConnectionPoolPropertyConstants.DEFAULT_POOL_TIMEOUT_MS, queriedValues.get(ConnectionPoolPropertyConstants.POOL_TIMEOUT));
        assertEquals(ConnectionPoolPropertyConstants.DEFAULT_POOL_PROTOCOL, queriedValues.get(ConnectionPoolPropertyConstants.POOL_PROTOCOL));
        assertEquals(ConnectionPoolPropertyConstants.DEFAULT_POOL_AUTHENTICATION, queriedValues.get(ConnectionPoolPropertyConstants.POOL_AUTHENTICATION));

        // Check that there are 26 'property_key' with value "crowd"
        sqlQuery = "SELECT count(property_key) FROM cwd_property WHERE property_key='crowd'";
        assertEquals(Integer.valueOf(26), jdbcTemplate.queryForObject(sqlQuery, Integer.class));

        // Check that there are 3 'property_key' with that matches the SAL keys with "plugin." prepended
        sqlQuery = "SELECT property_key FROM cwd_property WHERE property_key<>'crowd'";
        List<Map<String, Object>> propertyKeys = convertElementsToHashMap(jdbcTemplate.queryForList(sqlQuery));
        List<Map<String, Object>> actualKeys = createActualSalKeys();
        assertEquals(actualKeys.size(), propertyKeys.size());
        assertTrue(propertyKeys.containsAll(actualKeys));
    }

    private static List<Map<String, Object>> createActualSalKeys()
    {
        List<String> salColumns = Arrays.asList(PROPERTY_KEY);

        List<Map<String, Object>> actualKeys = new ArrayList<Map<String, Object>>();

        // These are the expected key values that should be in the new database
        addElementsTo(actualKeys, salColumns, Arrays.<Object>asList("plugin.SAL"));
        addElementsTo(actualKeys, salColumns, Arrays.<Object>asList("plugin.Some SAL Key"));
        addElementsTo(actualKeys, salColumns, Arrays.<Object>asList("plugin.null"));

        return actualKeys;
    }

    private static final String T = "T", F = "F";

    private List<Map<String, Object>> createActualGroupsList()
    {
        List<String> groupColumns = Arrays.asList(GROUP_NAME, LOWER_GROUP_NAME, ACTIVE, CREATED_DATE, DESCRIPTION, GROUP_TYPE, DIRECTORY_ID);

        List<Map<String, Object>> actualGroups = new ArrayList<Map<String, Object>>();

        // crowd-administrators
        addElementsTo(actualGroups, groupColumns,
                Arrays.<Object>asList("crowd-administrators", "crowd-administrators", T, getDateFromDatabase("2010-02-03 15:06:03"),
                null, GroupType.GROUP.toString(), 1L));

        // test-group1
        addElementsTo(actualGroups, groupColumns,
                Arrays.<Object>asList("test-group1", "test-group1", T, getDateFromDatabase("2010-02-03 15:09:22"),
                "Group One", GroupType.GROUP.toString(), 1L));

        // Another Group
        addElementsTo(actualGroups, groupColumns,
                Arrays.<Object>asList("Another Group", "another group", T, getDateFromDatabase("2010-02-03 15:15:58"),
                null, GroupType.GROUP.toString(), 1L));

        // testing-role
        addElementsTo(actualGroups, groupColumns,
                Arrays.<Object>asList("testing-role", "testing-role", T, getDateFromDatabase("2010-02-03 15:11:29"),
                "This is a role", GroupType.LEGACY_ROLE.toString(), 1L));

        return actualGroups;
    }

    private List<Map<String, Object>> createActualUsersList()
    {
        List<String> userColumns = Arrays.asList(USER_NAME, LOWER_USER_NAME, ACTIVE, CREATED_DATE, FIRST_NAME, LOWER_FIRST_NAME, LAST_NAME, LOWER_LAST_NAME, DISPLAY_NAME, LOWER_DISPLAY_NAME, EMAIL_ADDRESS, LOWER_EMAIL_ADDRESS, DIRECTORY_ID, CREDENTIAL);

        List<Map<String, Object>> actualUsers = new ArrayList<Map<String, Object>>();

        // user: test1
        addElementsTo(actualUsers, userColumns,
                Arrays.<Object>asList("test1", "test1", T, getDateFromDatabase("2010-02-03 15:07:14"), "Test", "test",
                        "User1", "user1", "Test User1", "test user1", "test1@example.com", "test1@example.com", 1L, "7iaw3Ur350mqGo7jwQrpkj9hiYB3Lkc/iBml1JQODbJ6wYX4oOHV+E+IvIh/1nsUNzLDBMxfqa2Ob1f1ACio/w=="));

        // user: admin
        addElementsTo(actualUsers, userColumns,
                Arrays.<Object>asList("admin", "admin", T, getDateFromDatabase("2010-02-03 15:06:03"), "Super", "super",
                        "User", "user", "Super User", "super user", "admin@example.com", "admin@example.com", 1L, "x61Ey612Kl2gpFL56FT9weDnpSo4AV8j8+qx2AuTHdRyY036xxzTTrw10Wq3+4qQyB+XURPWx1ONxp3Y3pB37A=="));

        // user: test1 (in dir2)
        addElementsTo(actualUsers, userColumns,
                Arrays.<Object>asList("test1", "test1", T, getDateFromDatabase("2010-02-03 15:09:46"), "Test", "test",
                        "User1Dir2", "user1dir2", "Test User1Dir2", "test user1dir2", "test1@example.com", "test1@example.com", 2L, "7iaw3Ur350mqGo7jwQrpkj9hiYB3Lkc/iBml1JQODbJ6wYX4oOHV+E+IvIh/1nsUNzLDBMxfqa2Ob1f1ACio/w=="));

        // user:testuser id:32772
        addElementsTo(actualUsers, userColumns,
                Arrays.<Object>asList("testuser", "testuser", T, getDateFromDatabase("2010-02-03 15:12:13"), "Normal", "normal",
                        "User", "user", "Normal User", "normal user", "user@example.com", "user@example.com", 1L, "PGjVboTbmofzsWWwXcjNL332DjLxLZUIKAURYJmAzz3WNVtlAX3An6RtSqUf0UDz4A7JZE3UQ9780t37qPvROg=="));

        return actualUsers;
    }

    private List<Map<String, Object>> createActualDirectoriesList()
    {
        List<String> directoryColumns = Arrays.asList(ID, DIRECTORY_NAME, LOWER_DIRECTORY_NAME, CREATED_DATE, ACTIVE, DESCRIPTION, IMPL_CLASS, LOWER_IMPL_CLASS, DIRECTORY_TYPE);

        List<Map<String, Object>> actualDirectories = new ArrayList<Map<String, Object>>();

        addElementsTo(actualDirectories, directoryColumns,
                Arrays.<Object>asList(1L, "Internal Directory", "internal directory", getDateFromDatabase("2010-02-03 15:05:54"),
                        T, "", INTERNAL_DIRECTORY_IMPL, toLowerCase(INTERNAL_DIRECTORY_IMPL), DirectoryType.INTERNAL.toString()));

        addElementsTo(actualDirectories, directoryColumns,
                Arrays.<Object>asList(2L, "Second Internal", "second internal", getDateFromDatabase("2010-02-03 15:08:53"),
                        T, "A Second Internal Directory", INTERNAL_DIRECTORY_IMPL, toLowerCase(INTERNAL_DIRECTORY_IMPL), DirectoryType.INTERNAL.toString()));

        addElementsTo(actualDirectories, directoryColumns,
                Arrays.<Object>asList(3L, "Testing Active Directory", "testing active directory", getDateFromDatabase("2010-02-03 15:22:16"),
                        T, "", ACTIVE_DIRECTORY_IMPL, toLowerCase(ACTIVE_DIRECTORY_IMPL), DirectoryType.CONNECTOR.toString()));

        return actualDirectories;
    }

    private List<Map<String, Object>> createActualApplicationsList()
    {
        List<String> applicationColumns = Arrays.asList(APPLICATION_NAME, LOWER_APPLICATION_NAME, CREATED_DATE, ACTIVE, DESCRIPTION, APPLICATION_TYPE, CREDENTIAL);

        List<Map<String, Object>> actualApplications = new ArrayList<Map<String, Object>>();

        addElementsTo(actualApplications, applicationColumns,
                Arrays.<Object>asList("crowd", "crowd", getDateFromDatabase("2010-02-03 15:06:03"), T, "Crowd Console",
                        ApplicationType.CROWD.toString(), "dqW5gPwYpt7Mg+3Xc3Kb3W6xY3fnmp+BDgvazWVEtpHtPTf6/ZuUFwZ5XCm3aFvUw+lmueE1kCW6ul3NykDWYg=="));

        addElementsTo(actualApplications, applicationColumns,
                Arrays.<Object>asList("crowd-openid-server", "crowd-openid-server", getDateFromDatabase("2010-02-03 15:06:07"), T, "CrowdID OpenID Provider",
                        ApplicationType.GENERIC_APPLICATION.toString(), "sQnzu7wkTrgkQZF+0G1hi5AI3Qmzvv0bXgc5THBqi7mAsdd4Xll27ASbRt9fEyavWi6m0QP9B8lThf+rDKy8hg=="));

        addElementsTo(actualApplications, applicationColumns,
                Arrays.<Object>asList("demo", "demo", getDateFromDatabase("2010-02-03 15:06:07"), T, "Crowd Demo Application",
                        ApplicationType.GENERIC_APPLICATION.toString(), "sQnzu7wkTrgkQZF+0G1hi5AI3Qmzvv0bXgc5THBqi7mAsdd4Xll27ASbRt9fEyavWi6m0QP9B8lThf+rDKy8hg=="));

        addElementsTo(actualApplications, applicationColumns,
                Arrays.<Object>asList("google-apps", "google-apps", getDateFromDatabase("2010-02-03 15:05:22"), T, "Google Applications Connector",
                        ApplicationType.PLUGIN.toString(), "/iPwhVwwDOlh0cQqlpvtrYdwBLQ78c81Ld7yHhljWuoPT6RURvH1ZoSDYi2lQ9LfoPSyImrqQ0InFcv99NPSRg=="));

        return actualApplications;
    }

    private List<Map<String, Object>> createActualMembershipsList()
    {
        List<String> membershipColumns = Arrays.asList(MEMBERSHIP_TYPE, GROUP_TYPE, PARENT_NAME, LOWER_PARENT_NAME, CHILD_NAME, LOWER_CHILD_NAME, DIRECTORY_ID);

        List<Map<String, Object>> actualMemberships = new ArrayList<Map<String, Object>>();

        addElementsTo(actualMemberships, membershipColumns, Arrays.<Object>asList(MembershipType.GROUP_USER.toString(), GroupType.GROUP.toString(), "Another Group", "another group", "testuser", "testuser", 1L));
        addElementsTo(actualMemberships, membershipColumns, Arrays.<Object>asList(MembershipType.GROUP_USER.toString(), GroupType.GROUP.toString(), "Another Group", "another group", "admin", "admin", 1L));
        addElementsTo(actualMemberships, membershipColumns, Arrays.<Object>asList(MembershipType.GROUP_USER.toString(), GroupType.GROUP.toString(), "Another Group", "another group", "test1", "test1", 1L));
        addElementsTo(actualMemberships, membershipColumns, Arrays.<Object>asList(MembershipType.GROUP_USER.toString(), GroupType.GROUP.toString(), "crowd-administrators", "crowd-administrators", "admin", "admin", 1L));
        addElementsTo(actualMemberships, membershipColumns, Arrays.<Object>asList(MembershipType.GROUP_USER.toString(), GroupType.GROUP.toString(), "test-group1", "test-group1", "testuser", "testuser", 1L));
        addElementsTo(actualMemberships, membershipColumns, Arrays.<Object>asList(MembershipType.GROUP_USER.toString(), GroupType.LEGACY_ROLE.toString(), "testing-role", "testing-role", "testuser", "testuser", 1L));

        return actualMemberships;
    }

    private List<Map<String, Object>> createActualAppDirMappingList(Map<Long, Long> convertedApplicationIdMap)
    {
        List<String> appDirColumns = Arrays.asList(APPLICATION_ID, DIRECTORY_ID, ALLOW_ALL, LIST_INDEX);

        Long applicationOneId = convertedApplicationIdMap.get(163841L);
        Long applicationTwoId = convertedApplicationIdMap.get(163842L);
        Long applicationThreeId = convertedApplicationIdMap.get(163843L);
        Long applicationFourId = convertedApplicationIdMap.get(163844L);

        List<Map<String, Object>> actualAppDirMappings = new ArrayList<Map<String, Object>>();

        addElementsTo(actualAppDirMappings, appDirColumns, Arrays.<Object>asList(applicationOneId, 1L, F, 0));
        addElementsTo(actualAppDirMappings, appDirColumns, Arrays.<Object>asList(applicationTwoId, 1L, T, 0));
        addElementsTo(actualAppDirMappings, appDirColumns, Arrays.<Object>asList(applicationThreeId, 1L, T, 0));
        addElementsTo(actualAppDirMappings, appDirColumns, Arrays.<Object>asList(applicationFourId, 1L, T, 0));
        addElementsTo(actualAppDirMappings, appDirColumns, Arrays.<Object>asList(applicationFourId, 2L, F, 1));

        return actualAppDirMappings;
    }

    private List<Map<String, Object>> createActualAttributeList(Map<Long, Long> convertedApplicationIdMap)
    {
        List<String> attributeColumns = Arrays.asList(APPLICATION_ID, AttributeMapper.ATTRIBUTE_NAME, AttributeMapper.ATTRIBUTE_VALUE);

        Long applicationOneId = convertedApplicationIdMap.get(163841L);
        Long applicationTwoId = convertedApplicationIdMap.get(163842L);
        Long applicationThreeId = convertedApplicationIdMap.get(163843L);
        Long applicationFourId = convertedApplicationIdMap.get(163844L);

        List<Map<String, Object>> actualAttributes = new ArrayList<Map<String, Object>>();

        addElementsTo(actualAttributes, attributeColumns, Arrays.<Object>asList(applicationOneId, "applicationType", ApplicationType.CROWD.toString()));
        addElementsTo(actualAttributes, attributeColumns, Arrays.<Object>asList(applicationOneId, "atlassian_sha1_applied", "true"));

        addElementsTo(actualAttributes, attributeColumns, Arrays.<Object>asList(applicationTwoId, "applicationType", "APPLICATION"));
        addElementsTo(actualAttributes, attributeColumns, Arrays.<Object>asList(applicationTwoId, "atlassian_sha1_applied", "true"));

        addElementsTo(actualAttributes, attributeColumns, Arrays.<Object>asList(applicationThreeId, "applicationType", "APPLICATION"));
        addElementsTo(actualAttributes, attributeColumns, Arrays.<Object>asList(applicationThreeId, "atlassian_sha1_applied", "true"));

        addElementsTo(actualAttributes, attributeColumns, Arrays.<Object>asList(applicationFourId, "applicationType", ApplicationType.PLUGIN.toString()));
        addElementsTo(actualAttributes, attributeColumns, Arrays.<Object>asList(applicationFourId, "atlassian_sha1_applied", "true"));

        return actualAttributes;
    }

    private Map<Long, Long> getApplicationIdMappings(String sqlQuery, RowMapper<Map<String, String>> oldToNewIdMapper)
    {
        Map<String, String> idMapping = databaseMapper.attributeListToMap(jdbcTemplate.query(sqlQuery, oldToNewIdMapper));
        Map<Long, Long> idMappingAsLong = new HashMap<Long, Long>();
        for (Map.Entry<String, String> entry : idMapping.entrySet())
        {
            idMappingAsLong.put(new Long(entry.getKey()), new Long(entry.getValue()));
        }
        return idMappingAsLong;
    }

    private static List<Map<String, Object>> createActualAddressesList(Map<Long, Long> convertedApplicationIdMap)
    {
        List<String> addressColumns = Arrays.asList(APPLICATION_ID, REMOTE_ADDRESS);

        Long applicationOneId = convertedApplicationIdMap.get(163841L);
        Long applicationTwoId = convertedApplicationIdMap.get(163842L);
        Long applicationThreeId = convertedApplicationIdMap.get(163843L);

        List<Map<String, Object>> actualAddresses = new ArrayList<Map<String, Object>>();

        addElementsTo(actualAddresses, addressColumns, Arrays.<Object>asList(applicationOneId, "127.0.0.1"));
        addElementsTo(actualAddresses, addressColumns, Arrays.<Object>asList(applicationOneId, "172.20.3.56"));
        addElementsTo(actualAddresses, addressColumns, Arrays.<Object>asList(applicationOneId, "localhost"));
        addElementsTo(actualAddresses, addressColumns, Arrays.<Object>asList(applicationOneId, "pyko.sydney.atlassian.com"));

        addElementsTo(actualAddresses, addressColumns, Arrays.<Object>asList(applicationTwoId, "127.0.0.1"));
        addElementsTo(actualAddresses, addressColumns, Arrays.<Object>asList(applicationTwoId, "172.20.3.56"));
        addElementsTo(actualAddresses, addressColumns, Arrays.<Object>asList(applicationTwoId, "localhost"));

        addElementsTo(actualAddresses, addressColumns, Arrays.<Object>asList(applicationThreeId, "127.0.0.1"));
        addElementsTo(actualAddresses, addressColumns, Arrays.<Object>asList(applicationThreeId, "172.20.3.56"));
        addElementsTo(actualAddresses, addressColumns, Arrays.<Object>asList(applicationThreeId, "localhost"));

        return actualAddresses;
    }

    private static void addElementsTo(List<Map<String, Object>> originalMap, List<String> keys, List<Object> values)
    {
        assertEquals("Error creating new element. Keys/values must be matching pairs", keys.size(), values.size());
        Map<String, Object> newElement = new HashMap<String, Object>();
        for (int i = 0; i < keys.size(); i++)
        {
            newElement.put(keys.get(i), values.get(i));
        }
        originalMap.add(newElement);
    }

    private class AttributeMapper implements RowMapper<Map<String, String>>
    {
        private static final String ATTRIBUTE_NAME = "ATTRIBUTE_NAME";
        private static final String ATTRIBUTE_VALUE = "ATTRIBUTE_VALUE";

        public Map<String, String> mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            Map<String, String> attributePair = new HashMap<String, String>();
            String attributeName = rs.getString(ATTRIBUTE_NAME);
            String attributeValue = rs.getString(ATTRIBUTE_VALUE);
            attributePair.put(attributeName, attributeValue);
            return attributePair;
        }
    }

    private class OperationsMapper implements RowMapper<OperationType>
    {
        public OperationType mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return OperationType.valueOf(rs.getString("operation_type"));
        }
    }

    private class ApplicationIdMapper implements RowMapper<Map<String, String>>
    {
        private Map<String, String> originalIdMap;

        private ApplicationIdMapper()
        {
            // These are the original values from the XML file
            originalIdMap = new HashMap<String, String>();
            originalIdMap.put("crowd", "163841");
            originalIdMap.put("crowd-openid-server", "163842");
            originalIdMap.put("demo", "163843");
            originalIdMap.put("google-apps", "163844");
        }

        public Map<String, String> mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            String id = rs.getString("id");
            String applicationName = rs.getString("application_name");
            String originalId = originalIdMap.get(applicationName);
            Map<String, String> idPair = new HashMap<String, String>();
            idPair.put(originalId, id);
            return idPair;
        }
    }

    private class PropertyTableMapper implements RowMapper<Map<String, String>>
    {
        public Map<String, String> mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            Map<String, String> property = new HashMap<String, String>();
            String propertyName = rs.getString("property_name");
            String propertyValue = rs.getString("property_value");
            property.put(propertyName, propertyValue);
            return property;
        }
    }

    private class AppDirOperationMapper implements RowMapper<OperationType>
    {
        public OperationType mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return OperationType.valueOf(rs.getString("operation_type"));
        }
    }

    private class AppDirGroupMapper implements RowMapper<String>
    {
        public String mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return rs.getString("group_name");
        }
    }

    /**
     * Lists returned from jdbcTemplate.queryForList() returns ListOrderedMap instead of HashMap for the elements
     * We don't care about the order, so convert the elements to a HashMap
     *
     * @param array
     * @return
     */
    private List<Map<String, Object>> convertElementsToHashMap(List<Map<String, Object>> array)
    {
        List<Map<String, Object>> convertedArray = new ArrayList<Map<String, Object>>();
        for (Map<String, Object> element : array)
        {
            convertedArray.add(new HashMap<String, Object>(element));
        }
        return convertedArray;
    }
}
