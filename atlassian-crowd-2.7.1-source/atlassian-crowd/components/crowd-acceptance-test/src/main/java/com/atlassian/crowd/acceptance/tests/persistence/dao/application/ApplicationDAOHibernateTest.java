package com.atlassian.crowd.acceptance.tests.persistence.dao.application;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.dao.application.ApplicationDAOHibernate;
import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.ApplicationTermKeys;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;

import org.hibernate.SessionFactory;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static com.atlassian.crowd.model.application.Applications.namesOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class ApplicationDAOHibernateTest
{
    @Inject private ApplicationDAOHibernate applicationDAO;
    @Inject private DirectoryDAOHibernate directoryDAO;
    @Inject private SessionFactory sessionFactory;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    private static final long APPLICATION_ID = 1;

    private static final String APPLICATION_NAME_1 = "Crowd";
    private static final String APPLICATION_NAME_2 = "Application Too";
    private static final String APPLICATION_NAME_3 = "Third One";

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Test
    public void testAddApplication()
    {
        Application application = buildBambooApplication();

        ApplicationImpl returnedApplication = applicationDAO.add(application, new PasswordCredential("hello", true));

        sessionFactory.getCurrentSession().flush();

        assertNotNull(returnedApplication);
        assertNotNull(returnedApplication.getId());
        assertTrue(returnedApplication.isActive());
        assertEquals(ApplicationType.BAMBOO, returnedApplication.getType());
        assertEquals("Bamboo App", returnedApplication.getName());
        assertEquals(toLowerCase("Bamboo App"), returnedApplication.getLowerName());
        assertNotNull(returnedApplication.getCreatedDate());
        assertNotNull(returnedApplication.getUpdatedDate());

        assertNull(returnedApplication.getDescription());
        assertEquals(1, returnedApplication.getAttributes().size());
        assertTrue(returnedApplication.getDirectoryMappings().isEmpty());
        assertTrue(returnedApplication.getRemoteAddresses().isEmpty());
    }

    @Test (expected = ConstraintViolationException.class)
    public void testAddApplicationDuplicate()
    {
        ApplicationImpl application = ApplicationImpl.newInstance(APPLICATION_NAME_1, ApplicationType.CROWD);
        application.setActive(false);

        applicationDAO.add(application, PasswordCredential.encrypted("hello"));

        // the contract of ApplicationDAO.add() does not require implementations to throw an exception. Therefore,
        // the exception may come later, when the session is flushed
        sessionFactory.getCurrentSession().flush();
    }

    @Test (expected = NullPointerException.class)
    public void testAddApplicationIncomplete()
    {
        ApplicationImpl application = ApplicationImpl.newInstance("some name", ApplicationType.GENERIC_APPLICATION);

        applicationDAO.add(application, null);
    }

    @Test (expected = IllegalArgumentException.class)
    public void testAddApplicationIncorrectPassword()
    {
        Application application = buildBambooApplication();

        applicationDAO.add(application, new PasswordCredential("hello", false));
    }

    @Test
    public void testUpdateApplicationCredential() throws Exception
    {
        applicationDAO.updateCredential(applicationDAO.findById(APPLICATION_ID),
                                        PasswordCredential.encrypted("encrypted-string"));

        Application updatedApplication = applicationDAO.findById(APPLICATION_ID);

        assertEquals("encrypted-string", updatedApplication.getCredential().getCredential());
    }

    @Test (expected = IllegalArgumentException.class)
    public void testUpdateApplicationWithUnEncryptedCredential() throws Exception
    {
        applicationDAO.updateCredential(applicationDAO.findById(APPLICATION_ID),
                                        PasswordCredential.unencrypted("encrypted-string"));
    }

    @Test (expected = NullPointerException.class)
    public void testAddApplicationWithNoPassword()
    {
        Application application = buildBambooApplication();

        applicationDAO.add(application, null);
    }

    @Test
    public void testFindById() throws Exception
    {
        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        assertNotNull(application.getId());
        assertEquals(APPLICATION_NAME_1, application.getName());
        assertEquals(toLowerCase(APPLICATION_NAME_1), application.getLowerName());
        assertEquals("Crowd Security Server and Console", application.getDescription());
        assertEquals(ApplicationType.CROWD, application.getType());
        assertTrue(application.isActive());
        assertEquals("h4$|-|3dp455w0r|)", application.getCredential().getCredential());
        assertEquals("Ten", application.getValue("Number"));
        assertEquals("Red", application.getValue("color"));
        assertEquals(2, application.getKeys().size());

        assertEquals(2, application.getDirectoryMappings().size());
        for (DirectoryMapping mapping : application.getDirectoryMappings())
        {
            if (mapping.getDirectory().getId() == 1)
            {
                assertEquals(3, mapping.getAuthorisedGroups().size());
                for (GroupMapping groupMapping : mapping.getAuthorisedGroups())
                {
                    assertEquals(application, groupMapping.getApplication());
                    assertEquals(new Long(1L), groupMapping.getDirectory().getId());
                    assertTrue(Arrays.asList("administrators", "developers", "users").contains(groupMapping.getGroupName()));
                }

                assertEquals(2, mapping.getAllowedOperations().size());
                assertTrue(mapping.getAllowedOperations().contains(OperationType.CREATE_GROUP));
                assertTrue(mapping.getAllowedOperations().contains(OperationType.CREATE_USER));
            }
            else if (mapping.getDirectory().getId() == 2)
            {
                assertEquals(1, mapping.getAuthorisedGroups().size());
                assertEquals(0, mapping.getAllowedOperations().size());
            }
            else
            {
                fail("Directory mapping not found");
            }
        }

        assertEquals(3, application.getRemoteAddresses().size());
        for (RemoteAddress remoteAddress : application.getRemoteAddresses())
        {
            assertTrue(Arrays.asList("127.0.0.1", "127.0.0.2", "127.0.0.3").contains(remoteAddress.getAddress()));
        }
    }

    @Test (expected = ApplicationNotFoundException.class)
    public void testFindByIdNotFound() throws Exception
    {
        applicationDAO.findById(-1);
    }

    @Test
    public void testFindByName() throws Exception
    {
        ApplicationImpl application = applicationDAO.findByName(APPLICATION_NAME_2.toUpperCase());

        assertNotNull(application.getId());
        assertEquals(APPLICATION_NAME_2, application.getName());
        assertEquals(toLowerCase(APPLICATION_NAME_2), application.getLowerName());
        assertEquals("Another Simple Application", application.getDescription());
        assertEquals("hashmash", application.getCredential().getCredential());
        assertEquals(ApplicationType.BAMBOO, application.getType());
        assertFalse(application.isActive());
        assertTrue(application.getAttributes().isEmpty());
        assertEquals(1, application.getDirectoryMappings().size());
        assertTrue(application.getRemoteAddresses().isEmpty());
    }

    @Test (expected = ApplicationNotFoundException.class)
    public void testFindByNameNotFound() throws Exception
    {
        applicationDAO.findByName("bogus");
    }

    @Test
    public void testUpdateApplicationByModifyingThePersistentInstance() throws Exception
    {
        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        application.setActive(false);
        application.setName("New Application");
        application.setCredential(new PasswordCredential("hashed-password", true));
        application.setType(ApplicationType.FISHEYE);
        application.setDescription(null);

        applicationDAO.update(application);

        sessionFactory.getCurrentSession().flush();

        ApplicationImpl updatedApplication = applicationDAO.findById(APPLICATION_ID);
        assertEquals("New Application", updatedApplication.getName());
        assertEquals(toLowerCase("New Application"), updatedApplication.getLowerName());
        assertNull(updatedApplication.getDescription());
        assertEquals(ApplicationType.FISHEYE, updatedApplication.getType());
        assertFalse(updatedApplication.isActive());
        assertEquals("Ten", updatedApplication.getValue("Number"));
        assertEquals("Red", updatedApplication.getValue("color"));
        assertEquals(2, updatedApplication.getKeys().size());
        assertEquals(2, updatedApplication.getDirectoryMappings().size());
        assertEquals(3, updatedApplication.getRemoteAddresses().size());

        // the credential is updated (compare with the next test method)
        assertEquals(new PasswordCredential("hashed-password", true), updatedApplication.getCredential());
    }

    @Test
    public void testUpdateApplicationByUsingATransientTemplate() throws Exception
    {
        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        PasswordCredential originalCredential = application.getCredential();

        assertEquals(new PasswordCredential("h4$|-|3dp455w0r|)", true), originalCredential);

        ApplicationImpl applicationImpl = ApplicationImpl.newInstance(application);
        applicationImpl.setActive(false);
        applicationImpl.setName("New Application");
        applicationImpl.setCredential(new PasswordCredential("different-hashed-password", true)); // useless
        applicationImpl.setType(ApplicationType.FISHEYE);
        applicationImpl.setDescription(null);

        applicationDAO.update(applicationImpl);

        sessionFactory.getCurrentSession().flush();

        ApplicationImpl updatedApplication = applicationDAO.findById(APPLICATION_ID);
        assertEquals("New Application", updatedApplication.getName());
        assertEquals(toLowerCase("New Application"), updatedApplication.getLowerName());
        assertNull(updatedApplication.getDescription());
        assertEquals(ApplicationType.FISHEYE, updatedApplication.getType());
        assertFalse(updatedApplication.isActive());
        assertEquals("Ten", updatedApplication.getValue("Number"));
        assertEquals("Red", updatedApplication.getValue("color"));
        assertEquals(2, updatedApplication.getKeys().size());
        assertEquals(2, updatedApplication.getDirectoryMappings().size());
        assertEquals(3, updatedApplication.getRemoteAddresses().size());

        // the update() method does not update the credential (compare with the previous test method)
        assertEquals(originalCredential, updatedApplication.getCredential());
    }

    private DirectoryImpl loadDirectory(Long id) throws DirectoryNotFoundException
    {
        return directoryDAO.findById(id);
    }

    @Test
    public void testUpdateApplicationWithBellsAndWhistles() throws Exception
    {
        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        application.setActive(false);
        application.setName("New Application");
        application.setType(ApplicationType.BAMBOO);
        application.setDescription(null);
        application.setAttribute("Number", "one");
        DirectoryImpl dir = loadDirectory(3L);
        application.getDirectoryMappings().add(new DirectoryMapping(application, dir, true));
        application.getDirectoryMapping(1L).getAuthorisedGroups().clear();
        application.getDirectoryMapping(1L).getAllowedOperations().add(OperationType.DELETE_USER);
        application.addRemoteAddress("10.0.0.1");

        applicationDAO.update(application);

        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();

        ApplicationImpl updatedApplication = applicationDAO.findById(APPLICATION_ID);
        assertEquals("New Application", updatedApplication.getName());
        assertEquals(toLowerCase("New Application"), updatedApplication.getLowerName());
        assertNull(updatedApplication.getDescription());
        assertEquals(ApplicationType.BAMBOO, updatedApplication.getType());
        assertFalse(updatedApplication.isActive());
        assertEquals("one", updatedApplication.getValue("Number"));
        assertEquals("Red", updatedApplication.getValue("color"));
        assertEquals(2, updatedApplication.getKeys().size());
        assertEquals(3, updatedApplication.getDirectoryMappings().size());
        assertEquals(0, updatedApplication.getDirectoryMapping(1L).getAuthorisedGroups().size());
        assertEquals(3, updatedApplication.getDirectoryMapping(1L).getAllowedOperations().size());
        assertEquals(4, updatedApplication.getRemoteAddresses().size());
    }

    @Test
    public void testRemove() throws Exception
    {
        Application application = applicationDAO.findById(APPLICATION_ID);

        applicationDAO.remove(application);

        sessionFactory.getCurrentSession().flush();

        try
        {
            applicationDAO.findById(APPLICATION_ID);
            fail("ApplicationNotFoundException expected");
        }
        catch (ApplicationNotFoundException e)
        {
            // expected
        }
    }

    @Test
    public void testAddDirectoryMapping() throws Exception
    {
        applicationDAO.addDirectoryMapping(APPLICATION_ID, 3L, true, OperationType.CREATE_GROUP, OperationType.CREATE_USER, OperationType.CREATE_ROLE);

        sessionFactory.getCurrentSession().flush();

        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        DirectoryMapping directoryMapping = application.getDirectoryMapping(3L);

        assertNotNull(directoryMapping);
        assertNotNull(directoryMapping.getId());
        assertEquals(new Long(3L), directoryMapping.getDirectory().getId());
        assertTrue(directoryMapping.isAllowAllToAuthenticate());
        assertEquals(3, directoryMapping.getAllowedOperations().size());
        assertThat(directoryMapping.getAllowedOperations(), containsInAnyOrder(OperationType.CREATE_GROUP,
                                                                               OperationType.CREATE_USER,
                                                                               OperationType.CREATE_ROLE));
    }

    @Test
    public void testAddGroupMapping() throws Exception
    {
        applicationDAO.addGroupMapping(APPLICATION_ID, 2L, "testers");

        sessionFactory.getCurrentSession().flush();

        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        DirectoryMapping directoryMapping = application.getDirectoryMapping(2L);

        Set<GroupMapping> authorisedGroups = directoryMapping.getAuthorisedGroups();

        assertEquals(2, authorisedGroups.size());

        Collection<String> groupNames = Collections2.transform(authorisedGroups, new Function<GroupMapping, String>()
        {
            public String apply(GroupMapping groupMapping)
            {
                assertNotNull(groupMapping.getId());
                return groupMapping.getGroupName();
            }
        });

        assertTrue(groupNames.containsAll(Sets.newHashSet("testers", "developers")));
    }

    @Test
    public void testRemoveDirectoryMappingFromApplication() throws Exception
    {
        applicationDAO.removeDirectoryMapping(APPLICATION_ID, 2L);

        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        assertEquals(1, application.getDirectoryMappings().size());
    }

    @Test
    public void testRemoveDirectoryMappingFromApplicationMultipleApplications() throws Exception
    {
        // Directory with id '2' is associated to Application with id '1' and Application with id '2'
        applicationDAO.removeDirectoryMappings(2L);

        ApplicationImpl application1 = applicationDAO.findById(APPLICATION_ID);

        assertEquals(1, application1.getDirectoryMappings().size());

        ApplicationImpl application2 = applicationDAO.findById(2L);

        assertEquals(0, application2.getDirectoryMappings().size());
    }

    @Test
    public void testRemoveGroupMapping() throws Exception
    {
        Application application = applicationDAO.findById(APPLICATION_ID);
        assertEquals(3, application.getDirectoryMapping(1L).getAuthorisedGroups().size()); // cache directory mappings

        applicationDAO.removeGroupMapping(APPLICATION_ID, 1L, "users");

        assertEquals(2, application.getDirectoryMapping(1L).getAuthorisedGroups().size());
        assertFalse(application.getDirectoryMapping(1L).isAuthorised("users"));
        assertTrue(application.getDirectoryMapping(1L).isAuthorised("developers"));
        assertTrue(application.getDirectoryMapping(1L).isAuthorised("administrators"));
    }

    @Test
    public void testAddAndThenRemoveAGroupMapping() throws Exception
    {
        Application application = applicationDAO.findById(APPLICATION_ID);

        applicationDAO.addGroupMapping(APPLICATION_ID, 1L, "new-group");
        assertEquals(4, application.getDirectoryMapping(1L).getAuthorisedGroups().size());
        assertTrue(application.getDirectoryMapping(1L).isAuthorised("new-group"));

        applicationDAO.removeGroupMapping(APPLICATION_ID, 1L, "new-group");
        assertEquals(3, application.getDirectoryMapping(1L).getAuthorisedGroups().size());
        assertFalse(application.getDirectoryMapping(1L).isAuthorised("new-group"));
    }

    @Test
    public void testRemoveAndThenAddAgainGroupMapping() throws Exception
    {
        // why do we want to remove and then add the same thing? @see action.application.UpdateGroups
        Application application = applicationDAO.findById(APPLICATION_ID);
        assertEquals(3, application.getDirectoryMapping(1L).getAuthorisedGroups().size()); // cache directory mappings

        applicationDAO.removeGroupMapping(APPLICATION_ID, 1L, "users");
        assertEquals(2, application.getDirectoryMapping(1L).getAuthorisedGroups().size());
        assertFalse(application.getDirectoryMapping(1L).isAuthorised("users"));

        applicationDAO.addGroupMapping(APPLICATION_ID, 1L, "users");
        assertTrue(application.getDirectoryMapping(1L).isAuthorised("users"));
        assertTrue(application.getDirectoryMapping(1L).isAuthorised("developers"));
        assertTrue(application.getDirectoryMapping(1L).isAuthorised("administrators"));
    }

    @Test
    public void testRemoveAllGroupMappingsForAGroup() throws Exception
    {
        applicationDAO.removeGroupMappings(2L, "developers");

        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        assertFalse(application.getDirectoryMapping(2L).isAuthorised("developers"));

        ApplicationImpl application2 = applicationDAO.findById(2L);
        assertFalse(application2.getDirectoryMapping(2L).isAuthorised("developers"));
    }

    @Test
    public void testAddRemoteAddress() throws Exception
    {
        RemoteAddress remoteAddress = new RemoteAddress("192.168.1.1");
        applicationDAO.addRemoteAddress(APPLICATION_ID, remoteAddress);

        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        assertTrue(application.getRemoteAddresses().contains(remoteAddress));
    }

    @Test
    public void testRemoveRemoteAddress() throws Exception
    {
        RemoteAddress remoteAddress = new RemoteAddress("127.0.0.1");
        applicationDAO.removeRemoteAddress(APPLICATION_ID, remoteAddress);

        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        assertFalse(application.getRemoteAddresses().contains(remoteAddress));
    }

    @Test
    public void testUpdateDirectoryMappingPosition() throws Exception
    {
        applicationDAO.updateDirectoryMapping(APPLICATION_ID, 2L, 0);

        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();

        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        assertEquals(2L, application.getDirectoryMappings().get(0).getDirectory().getId().longValue());
        assertEquals(1L, application.getDirectoryMappings().get(1).getDirectory().getId().longValue());
    }

    @Test
    public void testUpdateDirectoryMappingPositionWithNegativePosition() throws Exception
    {
        applicationDAO.updateDirectoryMapping(APPLICATION_ID, 2L, -1);

        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();

        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        assertEquals(1L, application.getDirectoryMappings().get(0).getDirectory().getId().longValue());
        assertEquals(2L, application.getDirectoryMappings().get(1).getDirectory().getId().longValue());
    }

    @Test
    public void testUpdateDirectoryMappingPositionWithUnboundedPosition() throws Exception
    {
        applicationDAO.updateDirectoryMapping(APPLICATION_ID, 1L, 3);

        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();

        ApplicationImpl application = applicationDAO.findById(APPLICATION_ID);

        assertEquals(1L, application.getDirectoryMappings().get(0).getDirectory().getId().longValue());
        assertEquals(2L, application.getDirectoryMappings().get(1).getDirectory().getId().longValue());
    }

    @Test
    public void testFindAuthorisedApplicationsMatchByGroup()
    {
        Long directoryId = 1L;

        List<Application> applications = applicationDAO.findAuthorisedApplications(directoryId, Arrays.asList("users"));
        assertEquals(1, applications.size());
        assertEquals("crowd", ((ApplicationImpl) applications.get(0)).getLowerName());

        applications = applicationDAO.findAuthorisedApplications(directoryId, Arrays.asList("administrators"));
        assertEquals(1, applications.size());
        assertEquals("crowd", ((ApplicationImpl) applications.get(0)).getLowerName());

        applications = applicationDAO.findAuthorisedApplications(directoryId, Arrays.asList("developers"));
        assertEquals(1, applications.size());
        assertEquals("crowd", ((ApplicationImpl) applications.get(0)).getLowerName());
    }

    @Test
    public void testFindAuthorisedApplicationsNoMatchByGroup()
    {
        Long directoryId = 1L;

        List<Application> applications = applicationDAO.findAuthorisedApplications(directoryId, Arrays.asList("bogus"));
        assertEquals(0, applications.size());

        applications = applicationDAO.findAuthorisedApplications(directoryId, new ArrayList<String>());
        assertEquals(0, applications.size());
    }

    @Test
    public void testFindAuthorisedApplicationsMatchByDirectoryAllowAll()
    {
        Long directoryId = 2L;

        List<Application> applications = applicationDAO.findAuthorisedApplications(directoryId, Arrays.asList("bogus"));
        assertEquals(1, applications.size());

        applications = applicationDAO.findAuthorisedApplications(directoryId, Arrays.asList("administrators"));
        assertEquals(1, applications.size());

        applications = applicationDAO.findAuthorisedApplications(directoryId, new ArrayList<String>());
        assertEquals(1, applications.size());
    }

    @Test
    public void testFindAuthorisedApplicationsNoMatchByDirectoryAllowAll()
    {
        Long directoryId = 3L;

        List<Application> applications = applicationDAO.findAuthorisedApplications(directoryId, Arrays.asList("bogus"));
        assertEquals(0, applications.size());

        applications = applicationDAO.findAuthorisedApplications(directoryId, Arrays.asList("administrators"));
        assertEquals(0, applications.size());

        applications = applicationDAO.findAuthorisedApplications(directoryId, new ArrayList<String>());
        assertEquals(0, applications.size());
    }

    @Test
    public void testSearchAll()
    {
        List<Application> applications = applicationDAO.search(QueryBuilder.queryFor(Application.class, EntityDescriptor.application()).returningAtMost(10));

        assertThat(namesOf(applications),
                   containsInAnyOrder(APPLICATION_NAME_1, APPLICATION_NAME_2, APPLICATION_NAME_3));
    }

    @Test
    public void testSearchByNameStartingWith()
    {
        List<Application> applications = applicationDAO.search(QueryBuilder.queryFor(Application.class, EntityDescriptor.application()).with(Restriction.on(ApplicationTermKeys.NAME).startingWith("application")).returningAtMost(10));

        assertThat(namesOf(applications), contains(APPLICATION_NAME_2));
    }

    @Test
    public void testSearchByNameContaining()
    {
        List<Application> applications = applicationDAO.search(QueryBuilder.queryFor(Application.class, EntityDescriptor.application()).with(Restriction.on(ApplicationTermKeys.NAME).containing("d")).returningAtMost(10));

        assertThat(namesOf(applications), containsInAnyOrder(APPLICATION_NAME_1, APPLICATION_NAME_3));
    }

    @Test
    public void testSearchByActive()
    {
        List<Application> applications = applicationDAO.search(QueryBuilder.queryFor(Application.class, EntityDescriptor.application()).with(Restriction.on(ApplicationTermKeys.ACTIVE).exactlyMatching(true)).returningAtMost(10));

        assertThat(namesOf(applications), contains(APPLICATION_NAME_1));
    }

    @Test
    public void testSearchByType()
    {
        List<Application> applications = applicationDAO.search(QueryBuilder.queryFor(Application.class, EntityDescriptor.application()).with(Restriction.on(ApplicationTermKeys.TYPE).exactlyMatching(ApplicationType.BAMBOO)).returningAtMost(10));

        assertThat(namesOf(applications), contains(APPLICATION_NAME_2));
    }

    @Test
    public void testSearchNestedQuery()
    {
        List<Application> applications = applicationDAO.search(
            QueryBuilder.queryFor(Application.class, EntityDescriptor.application())
                .with(Combine.anyOf(Combine.allOf(Restriction.on(ApplicationTermKeys.NAME).startingWith("CR"),
                                                  Restriction.on(ApplicationTermKeys.ACTIVE).exactlyMatching(true)),
                                    Combine.allOf(Restriction.on(ApplicationTermKeys.ACTIVE).exactlyMatching(false),
                                                  Restriction.on(ApplicationTermKeys.TYPE)
                                                      .exactlyMatching(ApplicationType.BAMBOO))))
                .returningAtMost(10));

        assertThat(namesOf(applications), containsInAnyOrder(APPLICATION_NAME_1, APPLICATION_NAME_2));
    }

    private static Application buildBambooApplication()
    {
        return ApplicationImpl.newInstance("Bamboo App", ApplicationType.BAMBOO);
    }
}
