package com.atlassian.crowd.acceptance.tests.applications.crowd;

import net.sourceforge.jwebunit.api.IElement;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * Test class to test the setting of options in the Crowd Console
 */
public class ViewOptionsTest extends CrowdAcceptanceTestCase
{
    private static final String TEST_CLAZZ = "com.atlassian.crowd";
    private static final String FORGOTTEN_PASSWORD_TEMPLATE_TEXTAREA = "forgottenPasswordTemplate";

    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        restoreCrowdFromXML("viewoptions.xml");
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testGeneralTabWithBlankData()
    {
        gotoGeneral();

        setWorkingForm("general");

        // Set all fields to null
        setTextField("title", "");
        setTextField("domain", "");

        submit();

        // assert that we have the correct errors
        assertKeyPresent("options.title.invalid");
    }

    public void testGeneralWithInvalidDomain()
    {
        gotoGeneral();

        setWorkingForm("general");

        // Set an invalid domain
        setTextField("domain", ".bad.localhost");

        submit();

        // assert we have domain error
        assertKeyPresent("options.domain.invalid");
    }

    public void testGeneralCancelButton()
    {
        gotoGeneral();

        assertKeyPresent("menu.options.label");

        clickButton("cancel");

        assertWarningAndErrorNotPresent();
        assertKeyPresent("menu.options.label");
    }

    public void testGeneralTab()
    {
        gotoGeneral();

        setWorkingForm("general");

        // Set all fields to new values
        setTextField("title", "New Atlassian Title");
        setTextField("domain", "localhost");
        assertThat(isElementDisabled("secureCookie"), equalTo(true));

        submit();

        // assert that we have the correct values
        assertKeyPresent("updatesuccessful.label");

        assertTextFieldEquals("title", "New Atlassian Title");
        assertTextFieldEquals("domain", "localhost");

        submit();

        // assert that we have the correct values
        assertKeyPresent("updatesuccessful.label");
        assertCheckboxNotSelected("secureCookie");
    }

    public void testMailServerWithBlankData()
    {
        gotoMailServer();

        // Set all fields to blank values
        setTextField("notificationEmail", "");
        setTextField("host", "");
        setTextField("from", "");
        setTextField("prefix", "");
        setTextField("username", "");
        setTextField("password", "");

        submit();

        // assert that we have the correct errors
        assertKeyPresent("mailserver.notification.invalid");
        assertKeyPresent("mailserver.host.invalid");
        assertKeyPresent("mailserver.from.invalid");
    }

    public void testJNDIMailServerWithBlankData()
    {
        gotoMailServer();

        // Set all fields to blank values
        setTextField("notificationEmail", "");
        setTextField("from", "");

        clickRadioOption("jndiMailActive", "true");
        assertRadioOptionSelected("jndiMailActive", "true");

        setTextField("jndiLocation", "");

        submit();


        // assert that we have the correct errors
        assertKeyPresent("mailserver.jndiLocation.invalid");
        assertKeyPresent("mailserver.notification.invalid");
        assertKeyPresent("mailserver.from.invalid");
    }

    public void testMailServer()
    {
        gotoMailServer();

        // Set all fields to a new value
        setTextField("notificationEmail", "crowd-admin@test.com");
        setTextField("host", "localhost");
        setTextField("from", "admin@test.com");
        setTextField("prefix", "Crowd Email");
        setTextField("username", "admin");
        setTextField("password", "password");

        submit();

        // assert that we have the correct values
        assertKeyPresent("updatesuccessful.label");

        assertTextFieldEquals("notificationEmail", "crowd-admin@test.com");
        assertTextFieldEquals("host", "localhost");
        assertTextFieldEquals("from", "admin@test.com");
        assertTextFieldEquals("prefix", "Crowd Email");
        assertTextFieldEquals("username", "admin");
    }

    public void testMailServerSSLPort()
    {
        gotoMailServer();

        setScriptingEnabled(true);

        checkCheckbox("useSSL");

        assertTextInElement("port", "465");

        uncheckCheckbox("useSSL");

        assertTextInElement("port", "25");

        setScriptingEnabled(false);
    }

    public void testMailServerCancelButton()
    {
        gotoMailServer();

        assertKeyPresent("menu.mailserver.label");

        clickButton("cancel");

        assertWarningAndErrorNotPresent();
        assertKeyPresent("menu.mailserver.label");
    }

    public void testMailTemplateWithBlankData()
    {
        gotoMailTemplate();

        setTextField(FORGOTTEN_PASSWORD_TEMPLATE_TEXTAREA, "");

        submit();

        assertKeyPresent("mailtemplate.template.invalid");
    }

    public void testMailTemplate()
    {
        gotoMailTemplate();

        setTextField(FORGOTTEN_PASSWORD_TEMPLATE_TEXTAREA, "A mail template that is for testing");

        submit();

        assertTextFieldEquals(FORGOTTEN_PASSWORD_TEMPLATE_TEXTAREA, "A mail template that is for testing");
    }

    public void testMailTemplateCancelButton()
    {
        gotoMailTemplate();

        assertKeyPresent("menu.mailtemplate.label");

        clickButton("cancel");

        assertWarningAndErrorNotPresent();
        assertKeyPresent("menu.mailtemplate.label");
    }

    public void testSessionWithBlankData()
    {
        gotoSessionConfig();

        setTextField("sessionTime", "");

        submit();

        assertKeyPresent("session.sessiontime.invalid");
    }

    public void testSessionWithInvalidData()
    {
        gotoSessionConfig();

        setTextField("sessionTime", "asda");

        submit();

        assertKeyPresent("session.sessiontime.invalid");
    }

    public void testTrustedProxyUpdate()
    {
        gotoTrustedProxies();

        setTextField("address", "10.1.2.3");

        submit();

        assertWarningAndErrorNotPresent();
        assertTextPresent("10.1.2.3");
    }

    public void testProfiling()
    {
        gotoLoggingProfiling();

        setWorkingForm("profiling");

        assertKeyPresent("loglevel.precursor");

        assertKeyPresent("loglevel.profilingOn");

        submit();

        assertKeyPresent("loglevel.profilingOff");

        setWorkingForm("profiling");

        submit();

        assertKeyPresent("loglevel.profilingOn");

    }

    /**
     * Also tested in {@link com.atlassian.crowd.acceptance.tests.administration.LoggingProfilingTest}.
     */
    public void testLogging()
    {
        gotoLoggingProfiling();

        setWorkingForm("logging");

        String row = getElementTextByXPath("id(\"" + TEST_CLAZZ + "\")");

        assertTrue(row.indexOf("INFO") >= 0);

        selectOption("levelNames", "DEBUG");

        submit("updateLogging");

        row = getElementTextByXPath("id(\"" + TEST_CLAZZ + "\")");

        assertTrue(row.indexOf("DEBUG") >= 0);

        clickButton("revertToDefault");

        row = getElementTextByXPath("id(\"" + TEST_CLAZZ + "\")");

        assertTrue(row.indexOf("INFO") >= 0);

    }

    public void testSession()
    {
        gotoSessionConfig();

        setTextField("sessionTime", "1234");

        submit();

        assertTextFieldEquals("sessionTime", "1234");
    }

    public void testLicenseWithBlankData()
    {
        gotoLicensing();

        setTextField("key", "");

        submit();

        assertKeyPresent("license.key.error.blank");
    }

    public void testLicenseWithInvalidData()
    {
        gotoLicensing();

        setTextField("key", "some bad data");

        submit();

        assertKeyPresent("license.key.error.invalid");
    }

    public void testLicenseWithAValidKey()
    {
        gotoLicensing();

        setTextField("key", UNLIMITED_LICENSE_KEY);

        submit();

        assertKeyPresent("updatesuccessful.label");
    }

    public void testLicenseWithAnExpiredMaintenaceKeyTooOldForBuild()
    {
        gotoLicensing();

        setTextField("key", EXPIRED_MAINTENANCE_KEY);

        submit();

        assertKeyPresent("license.key.error.maintenance.expired");
    }

    private boolean isElementDisabled(String elementId)
    {
        IElement secureCookie = getElementById(elementId);
        return "disabled".equals(secureCookie.getAttribute("disabled"));
    }
}
