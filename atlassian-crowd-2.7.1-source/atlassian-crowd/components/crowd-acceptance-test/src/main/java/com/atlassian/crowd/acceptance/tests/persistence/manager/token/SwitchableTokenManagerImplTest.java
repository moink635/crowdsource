package com.atlassian.crowd.acceptance.tests.persistence.manager.token;

import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.dao.token.TokenDAOHibernate;
import com.atlassian.crowd.dao.token.TokenDAOMemory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.ObjectAlreadyExistsException;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.token.SwitchableTokenManagerImpl;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.spring.container.SpringContainerContext;
import org.junit.Assert;
import org.quartz.Scheduler;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.test.AbstractTransactionalSpringContextTests;

import static org.mockito.Mockito.mock;


public class SwitchableTokenManagerImplTest extends AbstractTransactionalSpringContextTests
{
    private SwitchableTokenManagerImpl tokenManagerImpl;

    private int VOLUME_TEST_SIZE = 500;

    @Override
    protected String[] getConfigLocations()
    {
        setAutowireMode(AUTOWIRE_BY_NAME);
        return new String[]{

                "classpath:/applicationContext-CrowdDAO.xml",
                "classpath:/applicationContext-CrowdLDAP.xml",
                "classpath:/applicationContext-CrowdManagers.xml",
                "classpath:/applicationContext-CrowdServer.xml",
                "classpath:/applicationContext-CrowdEncryption.xml",
                "classpath:/applicationContext-CrowdUtils.xml",
                "classpath:/applicationContext-CrowdClient.xml",
                "classpath:/applicationContext-config.xml"
        };
    }

    @Override
    protected void customizeBeanFactory(DefaultListableBeanFactory beanFactory)
    {
        beanFactory.registerSingleton("applicationDao", mock(ApplicationDAO.class));
        beanFactory.registerSingleton("directoryDao", mock(DirectoryDao.class));
        beanFactory.registerSingleton("scheduler", mock(Scheduler.class));
        beanFactory.registerSingleton("bootstrapManager", mock(CrowdBootstrapManager.class));
        beanFactory.registerSingleton("crowdUserDetailsService", mock(CrowdUserDetailsService.class));
        beanFactory.registerSingleton("webInterfaceManager", mock(WebInterfaceManager.class));
    }

    @Override
    protected void onSetUp() throws Exception
    {
        // Setup the Container Context that some of our objects use to get access to managers, helpers etc.
        SpringContainerContext springContainerContext = new SpringContainerContext();
        springContainerContext.setApplicationContext(applicationContext);
        ContainerManager.getInstance().setContainerContext(springContainerContext);

        tokenManagerImpl = new SwitchableTokenManagerImpl(true, // start with in-memory token manager.
                (TokenDAOMemory) ContainerManager.getComponent("tokenDAOMemory"),
                (TokenDAOHibernate) ContainerManager.getComponent("tokenDAOHibernate"),
                (PropertyManager) ContainerManager.getComponent("propertyManager"));

        super.onSetUp();
    }

    @Override
    protected void onTearDown() throws Exception
    {
        // clear caches.
        ((TokenDAOMemory) ContainerManager.getComponent("tokenDAOMemory")).removeAll();
        ((TokenDAOHibernate) ContainerManager.getComponent("tokenDAOHibernate")).removeAll();

        tokenManagerImpl = null;

        super.onTearDown();
    }

    public void testAdd() throws Exception
    {
        tokenManagerImpl.add(new Token.Builder(-1L, "my-test-token", "idhash", 12345, "secret-key").create());
    }

    public void testSwitch()
    {
        Assert.assertFalse(tokenManagerImpl.isUsingDatabaseStorage());
        tokenManagerImpl.switchToHibernate();
        Assert.assertTrue(tokenManagerImpl.isUsingDatabaseStorage());
    }

    public void testInvalidSwitch()
    {
        Assert.assertFalse(tokenManagerImpl.isUsingDatabaseStorage());
        try
        {
            tokenManagerImpl.switchToMemory();

            Assert.fail("Should not have allowed switch to memory caching while in memory caching");
        }
        catch (IllegalStateException e)
        {
        }
        Assert.assertFalse(tokenManagerImpl.isUsingDatabaseStorage());
    }

    public void testSwitchAfterAdd() throws Exception
    {
        EntityQuery<Token> allTokensQuery =
            QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(EntityQuery.ALL_RESULTS);

        Assert.assertFalse(tokenManagerImpl.isUsingDatabaseStorage());

        tokenManagerImpl.add(new Token.Builder(-1, "admin", "idhash1", 12345, "my-test-token").create());
        tokenManagerImpl.add(new Token.Builder(-1, "admin2", "idhash2", 123456, "my-test-token-2").create());
        tokenManagerImpl.add(new Token.Builder(-1, "admin3", "idhash3", 1234567, "my-test-token-3").create());
        assertEquals(3, tokenManagerImpl.search(allTokensQuery).size());

        tokenManagerImpl.switchToHibernate();

        Assert.assertTrue(tokenManagerImpl.isUsingDatabaseStorage());
        assertEquals(3, tokenManagerImpl.search(allTokensQuery).size());
    }


    private void addLotsOfTokens(int numTokens) throws InvalidTokenException, ObjectAlreadyExistsException
    {
        for (int i = 0; i < numTokens; i++)
        {
            tokenManagerImpl.add(new Token.Builder(-1L, "admin-" + 1, "idhash" + i, i, "vol-token-" + i).create());
        }
    }

    /**
     * Makes sure that we can complete test in a 'reasonable' amount of time.
     */
    public void testSwitchToMemoryVolumeTest() throws Exception
    {
        tokenManagerImpl.switchToHibernate();

        Assert.assertTrue(tokenManagerImpl.isUsingDatabaseStorage());
        addLotsOfTokens(VOLUME_TEST_SIZE);

        assertEquals(VOLUME_TEST_SIZE, tokenManagerImpl.search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(1000)).size());
        assertEquals(VOLUME_TEST_SIZE, ((TokenDAOHibernate) ContainerManager.getComponent("tokenDAOHibernate")).search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(1000)).size());
        assertEquals(0, ((TokenDAOMemory) ContainerManager.getComponent("tokenDAOMemory")).search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(10)).size());

        long sysTimeStart = System.currentTimeMillis();
        tokenManagerImpl.switchToMemory();
        long sysTimeEnd = System.currentTimeMillis();
        System.out.println("Hibernate->Mem execution time: " + (sysTimeEnd - sysTimeStart) + "ms for " + VOLUME_TEST_SIZE + " elements.");

        assertEquals(VOLUME_TEST_SIZE, tokenManagerImpl.search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(1000)).size());
        assertEquals(VOLUME_TEST_SIZE, ((TokenDAOMemory) ContainerManager.getComponent("tokenDAOMemory")).search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(1000)).size());
        assertEquals(0, ((TokenDAOHibernate) ContainerManager.getComponent("tokenDAOHibernate")).search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(1000)).size());
    }

    /**
     * Makes sure that we can complete test in a 'reasonable' amount of time.
     */
    public void testSwitchToHibernateVolumeTest() throws Exception
    {
        Assert.assertFalse(tokenManagerImpl.isUsingDatabaseStorage());
        addLotsOfTokens(VOLUME_TEST_SIZE);

        assertEquals(VOLUME_TEST_SIZE, tokenManagerImpl.search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(1000)).size());
        assertEquals(VOLUME_TEST_SIZE, ((TokenDAOMemory) ContainerManager.getComponent("tokenDAOMemory")).search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(1000)).size());
        assertEquals(0, ((TokenDAOHibernate) ContainerManager.getComponent("tokenDAOHibernate")).search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(1000)).size());

        long sysTimeStart = System.currentTimeMillis();
        tokenManagerImpl.switchToHibernate();
        long sysTimeEnd = System.currentTimeMillis();
        System.out.println("Mem->Hibernate execution time: " + (sysTimeEnd - sysTimeStart) + "ms for " + VOLUME_TEST_SIZE + " elements.");

        assertEquals(VOLUME_TEST_SIZE, tokenManagerImpl.search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(1000)).size());
        assertEquals(VOLUME_TEST_SIZE, ((TokenDAOHibernate) ContainerManager.getComponent("tokenDAOHibernate")).search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(1000)).size());
        assertEquals(0, ((TokenDAOMemory) ContainerManager.getComponent("tokenDAOMemory")).search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(1000)).size());
    }
}
