package com.atlassian.crowd.acceptance.tests.persistence.dao.user;

import java.util.List;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.UserQuery;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.MatchMode;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.search.query.entity.restriction.PropertyUtils;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.atlassian.crowd.model.user.Users.namesOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Integration tests for the search() method of {@link UserDao}. For convenience, the tests
 * of this interface have been split into multiple test classes.
 *
 * @see UserDaoCRUDTest
 * @see UserDAOHibernateTest
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class UserDaoSearchTest
{
    @Inject private UserDao userDAO;
    @Inject private DataSource dataSource;

    private static final long DIRECTORY_ID = 1L;
    private static final String EXISTING_USER_1 = "admin";
    private static final String EXISTING_USER_2 = "bob";
    private static final String EXISTING_USER_3 = "jane";

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Test
    public void testSearchAllUserNames()
    {
        EntityQuery query = QueryBuilder.queryFor(String.class, EntityDescriptor.user()).returningAtMost(10);

        List<String> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(users, containsInAnyOrder(EXISTING_USER_1, EXISTING_USER_2, EXISTING_USER_3));
    }

    @Test
    public void testSearchUsernameStartsWith() throws Exception
    {
        PropertyRestriction<String>
            userNameRestriction = new TermRestriction<String>(UserTermKeys.USERNAME, MatchMode.STARTS_WITH, "a");
        UserQuery query = new UserQuery(User.class, userNameRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), contains(EXISTING_USER_1));
    }

    @Test
    public void testSearchEmailContains() throws Exception
    {
        PropertyRestriction<String> userNameRestriction = new TermRestriction<String>(UserTermKeys.EMAIL, MatchMode.CONTAINS, "@eXample");
        UserQuery query = new UserQuery(User.class, userNameRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), containsInAnyOrder(EXISTING_USER_1, EXISTING_USER_2));
    }

    @Test
    public void testSearchFirstNameExact() throws Exception
    {
        PropertyRestriction<String> userNameRestriction = new TermRestriction<String>(UserTermKeys.FIRST_NAME, MatchMode.EXACTLY_MATCHES, "BOB");
        UserQuery query = new UserQuery(User.class, userNameRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), contains(EXISTING_USER_2));
    }

    @Test
    public void testSearchLastNameStartsWith() throws Exception
    {
        PropertyRestriction<String> userNameRestriction = new TermRestriction<String>(UserTermKeys.LAST_NAME, MatchMode.STARTS_WITH, "s");
        UserQuery query = new UserQuery(User.class, userNameRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), contains(EXISTING_USER_2));
    }

    @Test
    public void testSearchLastNameStartsWithUsingBuilder() throws Exception
    {
        PropertyRestriction<String> userNameRestriction = Restriction.on(UserTermKeys.LAST_NAME).startingWith("s");
        UserQuery query = new UserQuery(User.class, userNameRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), contains(EXISTING_USER_2));
    }

    @Test
    public void testSearchForActiveUsersUsingBuilder() throws Exception
    {
        PropertyRestriction<Boolean> propertyRestriction = Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(true);
        UserQuery query = new UserQuery(User.class, propertyRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), containsInAnyOrder(EXISTING_USER_1, EXISTING_USER_2));
    }

    @Test
    public void testSearchForInactiveUsersUsingBuilder() throws Exception
    {
        PropertyRestriction<Boolean> propertyRestriction = Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(false);
        UserQuery query = new UserQuery(User.class, propertyRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), contains(EXISTING_USER_3));
    }

    @Test
    public void testSearchDisplayNameContains() throws Exception
    {
        PropertyRestriction<String> userNameRestriction = new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, MatchMode.CONTAINS, "mI");
        UserQuery query = new UserQuery(User.class, userNameRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), contains(EXISTING_USER_2));
    }

    @Test
    public void testSearchColorExact() throws Exception
    {
        PropertyRestriction<String> userNameRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "black");
        UserQuery query = new UserQuery(User.class, userNameRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), contains(EXISTING_USER_1));
    }

    @Test
    public void testSearchColorNoResults() throws Exception
    {
        PropertyRestriction<String> userNameRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "crimson");
        UserQuery query = new UserQuery(User.class, userNameRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertEquals(0, users.size());
    }

    @Test
    public void testSearchNullCustomAttribute()
    {
        PropertyRestriction<String> userAttributeRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("openid"), MatchMode.NULL, null);

        UserQuery query = new UserQuery(User.class, userAttributeRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertEquals(2, users.size());
    }

    @Test
    public void testSearchPrimaryAttributeAndNullCustomAttribute()
    {
        PropertyRestriction<String> userEmailRestriction = new TermRestriction<String>(UserTermKeys.USERNAME, MatchMode.CONTAINS, "a");
        PropertyRestriction<String> userAttributeRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("openid"), MatchMode.NULL, null);

        UserQuery query = new UserQuery(User.class, Combine.allOf(userEmailRestriction, userAttributeRestriction), 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertEquals(1, users.size());
    }

    @Test
    public void testSearchMultipleNullValues()
    {
        PropertyRestriction<String> r1 = new TermRestriction<String>(PropertyUtils.ofTypeString("openid"), MatchMode.NULL, null);
        PropertyRestriction<String> r2 = new TermRestriction<String>(PropertyUtils.ofTypeString("something"), MatchMode.NULL, null);

        UserQuery query = new UserQuery(User.class, Combine.anyOf(r1, r2), 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertEquals(3, users.size());
    }

    @Test
    public void testSearchFirstClassDisjunction() throws Exception
    {
        PropertyRestriction<String> userNameRestriction1 = new TermRestriction<String>(UserTermKeys.LAST_NAME, MatchMode.STARTS_WITH, "s");
        PropertyRestriction<String> userNameRestriction2 = new TermRestriction<String>(UserTermKeys.LAST_NAME, MatchMode.STARTS_WITH, "m");
        BooleanRestriction userNameDisjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, userNameRestriction1, userNameRestriction2);

        UserQuery query = new UserQuery(User.class, userNameDisjunction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), containsInAnyOrder(EXISTING_USER_1, EXISTING_USER_2));
    }

    @Test
    public void testSearchFirstClassConjunction() throws Exception
    {
        PropertyRestriction<String> userNameRestriction1 = new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "b");
        PropertyRestriction<String> userNameRestriction2 = new TermRestriction<String>(UserTermKeys.EMAIL, MatchMode.CONTAINS, "@examplE");
        BooleanRestriction userNameDisjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, userNameRestriction1, userNameRestriction2);

        UserQuery query = new UserQuery(User.class, userNameDisjunction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), contains(EXISTING_USER_2));
    }

    @Test
    public void testSearchSecondClassDisjuction() throws Exception
    {
        PropertyRestriction<String> colorRestriction1 = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "Red");
        PropertyRestriction<String> colorRestriction2 = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "yellow");
        BooleanRestriction userNameDisjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, colorRestriction1, colorRestriction2);

        UserQuery query = new UserQuery(User.class, userNameDisjunction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), containsInAnyOrder(EXISTING_USER_1, EXISTING_USER_2));
    }

    @Test
    public void testSearchSecondClassConjunction() throws Exception
    {
        PropertyRestriction<String> colorRestriction1 = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "Red");
        PropertyRestriction<String> colorRestriction2 = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "black");
        BooleanRestriction userNameDisjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, colorRestriction1, colorRestriction2);

        UserQuery query = new UserQuery(User.class, userNameDisjunction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), contains(EXISTING_USER_1));
    }

    @Test
    public void testSearchMixedClassDisjunction() throws Exception
    {
        PropertyRestriction<String> colorRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "black");
        PropertyRestriction<String> displayNameRestriction = new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "b");
        BooleanRestriction userNameDisjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, colorRestriction, displayNameRestriction);

        UserQuery query = new UserQuery(User.class, userNameDisjunction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), containsInAnyOrder(EXISTING_USER_1, EXISTING_USER_2));
    }

    @Test
    public void testSearchMixedClassConjunction() throws Exception
    {
        PropertyRestriction<String> colorRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "Red");
        PropertyRestriction<String> displayNameRestriction = new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "b");
        BooleanRestriction userNameConjunction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, colorRestriction, displayNameRestriction);

        UserQuery query = new UserQuery(User.class, userNameConjunction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), contains(EXISTING_USER_2));
    }

    @Test
    public void testSearchNested()
    {
        PropertyRestriction<String> colorRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "Red");
        PropertyRestriction<String> displayNameRestriction = new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "b");
        BooleanRestriction conjunction1 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, colorRestriction, displayNameRestriction);

        PropertyRestriction<String> colorRestriction2 = new TermRestriction<String>(PropertyUtils.ofTypeString("color"), MatchMode.EXACTLY_MATCHES, "violet");
        PropertyRestriction<String> displayNameRestriction2 = new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "j");
        BooleanRestriction conjunction2 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, colorRestriction2, displayNameRestriction2);

        BooleanRestriction nestedRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, conjunction1, conjunction2);

        UserQuery query = new UserQuery(User.class, nestedRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), containsInAnyOrder(EXISTING_USER_2, EXISTING_USER_3));
    }

    @Test
    public void testSearchNestedCustomNullAttributes()
    {
        PropertyRestriction<String> openidRestriction = new TermRestriction<String>(PropertyUtils.ofTypeString("openid"), MatchMode.NULL, null);
        PropertyRestriction<String> displayNameRestriction = new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "Jane");
        BooleanRestriction conjunction1 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, openidRestriction, displayNameRestriction);

        PropertyRestriction<String> openidRestriction2 = new TermRestriction<String>(PropertyUtils.ofTypeString("openid"), MatchMode.NULL, null);
        PropertyRestriction<String> displayNameRestriction2 = new TermRestriction<String>(UserTermKeys.DISPLAY_NAME, MatchMode.STARTS_WITH, "Bob");
        BooleanRestriction conjunction2 = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, openidRestriction2, displayNameRestriction2);

        BooleanRestriction nestedRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.OR, conjunction1, conjunction2, new TermRestriction(PropertyUtils.ofTypeString("openid"), MatchMode.CONTAINS, "/"));

        UserQuery query = new UserQuery(User.class, nestedRestriction, 0, 100);

        List<User> users = userDAO.search(DIRECTORY_ID, query);

        assertThat(namesOf(users), containsInAnyOrder(EXISTING_USER_1, EXISTING_USER_2, EXISTING_USER_3));
    }

    @Test
    public void testSearchMixedCaseUsername()
    {
        List<User> users = userDAO.search(2L, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.USERNAME).containing("john")).returningAtMost(10));
        assertThat(namesOf(users), contains("JohnSmith"));

        users = userDAO.search(2L, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.USERNAME).exactlyMatching("jOhNsMiTh")).returningAtMost(10));
        assertThat(namesOf(users), contains("JohnSmith"));
    }

    @Test
    public void testSearchMixedCaseUsernameAttributeMatch()
    {
        List<User> users = userDAO.search(2L, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(PropertyUtils.ofTypeString("color")).containing("violet")).returningAtMost(10));
        assertThat(namesOf(users), containsInAnyOrder("jane", "JohnSmith"));
    }
}
