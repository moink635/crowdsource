package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.crowd.acceptance.utils.*;

/**
 * Test the searching of principals
 */
public class BrowsePrincipalsTest extends CrowdAcceptanceTestCase
{
    private static final long MAX_WAIT_TIME_MS = 10000L;

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("searchdirectories.xml");
        synchroniseDirectory();
    }

    private void synchroniseDirectory()
    {
        DbCachingTestHelper.synchroniseDirectory(tester, "Apache DS", MAX_WAIT_TIME_MS);
        DbCachingTestHelper.synchroniseDirectory(tester, "Apache DS 1.5.4", MAX_WAIT_TIME_MS);
    }

    public void testBrowseInternalDirectoryUsers()
    {
        gotoBrowsePrincipals();

        setWorkingForm("searchusers");
        selectOption("directoryID", "Test Internal Directory");

        submit();

        assertUserInTable("admin", "Justin Koke", "admin@test.org");
        assertUserInTable("justin", "Justin Koke", "justin@test.org");
    }

    public void testBrowseApacheDSUsers()
    {
        gotoBrowsePrincipals();

        setWorkingForm("searchusers");
        selectOption("directoryID", "Apache DS");

        submit("submit-search");

        assertUserInTable("jdoe", "John Doe", "jdoe@testingarea.org");
        assertUserInTable("ldapusera", "LDAP User A", "ldapusera@testingarea.org");
        assertUserInTable("ldapuserb", "LDAP User B", "ldapuserb@testingarea.org");
        assertUserInTable("ldapuserc", "LDAP User C", "ldapuserc@testingarea.org");
        assertUserInTable("ldapuserd", "LDAP User D", "ldapuserd@testingarea.org");
        assertUserInTable("ldapusere", "LDAP User E", "ldapusere@testingarea.org");
        assertUserInTable("ldapuserf", "LDAP User F", "ldapuserf@testingarea.org");
        assertUserInTable("ldapuserg", "LDAP User G", "ldapuserg@testingarea.org");
        assertUserInTable("ldapuserh", "LDAP User H", "ldapuserh@testingarea.org");
    }

    public void testSearchForSpecificUsersByUsernameInApacheDS154()
    {
        gotoBrowsePrincipals();

        setWorkingForm("searchusers");
        selectOption("directoryID", "Apache DS 1.5.4");
        setTextField("search", "aeinstein");

        submit("submit-search");

        assertUserInTable("aeinstein", "Albert Einstein", "aeinstein@example.com");

        assertUserNotInTable("mplanck", "Max Planck", "mplanck@example.com");
    }

    public void testSearchForSpecificUsersByEmailInApacheDS154()
    {
        gotoBrowsePrincipals();

        setWorkingForm("searchusers");
        selectOption("directoryID", "Apache DS 1.5.4");
        setTextField("search", "@example.com");

        submit("submit-search");

        assertUserInTable("aeinstein", "Albert Einstein", "aeinstein@example.com");
        assertUserInTable("mplanck", "Max Planck", "mplanck@example.com");
        assertUserInTable("nbohr", "Neils Bohr", "nbohr@example.com");
        assertUserInTable("mborn", "Max Born", "mborn@example.com");
        assertUserInTable("wpauli", "Wolfgang Pauli", "wpauli@example.com");
        assertUserInTable("mcurie", "Marie Curie", "mcurie@example.com");
    }

    public void testSearchForSpecificUsersByUsernameInInternalDirectory()
    {
        gotoBrowsePrincipals();

        setWorkingForm("searchusers");
        selectOption("directoryID", "Test Internal Directory");
        setTextField("search", "admin");

        submit("submit-search");

        assertUserInTable("admin", "Justin Koke", "admin@test.org");

        assertUserNotInTable("justin", "Justin Koke", "justin@test.org");
    }

    public void testSearchForSpecificUsersByEmailInInternalDirectory()
    {
        gotoBrowsePrincipals();

        setWorkingForm("searchusers");
        selectOption("directoryID", "Test Internal Directory");
        setTextField("search", "admin@test.org");

        submit("submit-search");

        assertUserInTable("admin", "Justin Koke", "admin@test.org");

        assertUserNotInTable("justin", "Justin Koke", "justin@test.org");
    }

    public void testSearchForInactiveUsersWithNoSearchText()
    {
        restoreCrowdFromXML("authenticationtest.xml");
        gotoBrowsePrincipals();

        setWorkingForm("searchusers");
        selectOption("directoryID", "Test Internal Directory");
        selectOption("active", "Inactive");
        submit("submit-search");

        assertUserInTable("inactive", "Inactive User", "inactive@example.com");
        assertUserNotInTable("user", "Test User", "user@example.com");
        assertUserNotInTable("admin", "Shihab Hamid", "shihab@gmai.com");
    }

    public void testSearchForActiveUsersWithSearchText()
    {
        restoreCrowdFromXML("authenticationtest.xml");
        gotoBrowsePrincipals();

        setWorkingForm("searchusers");
        selectOption("directoryID", "Test Internal Directory");
        selectOption("active", "Active");
        setTextField("search", "user");
        submit("submit-search");

        assertUserInTable("user", "Test User", "user@example.com");
        assertUserNotInTable("inactive", "Inactive User", "inactive@example.com");
        assertUserNotInTable("admin", "Shihab Hamid", "shihab@gmai.com");
    }

    public void testNoDirectoryIsSelectedByDefault()
    {
        gotoBrowsePrincipals();
        assertSelectedOptionEquals("directoryID" , getText("selectdirectory.label"));
    }

    public void testDirectorySelectionIsSaved()
    {
        // Save directory selection
        gotoBrowsePrincipals();
        selectOption("directoryID", "Second Directory");
        submit("submit-search");

        // Check that the directory is selected
        gotoBrowsePrincipals();
        assertSelectedOptionEquals("directoryID" , "Second Directory");
        assertSelectOptionNotPresent("directoryID" , getText("selectdirectory.label"));
    }

    public void testNonExistingDirectorySelection()
    {
        intendToModifyData();

        // Save directory selection
        gotoBrowsePrincipals();
        selectOption("directoryID", "Second Directory");
        submit("submit-search");

        // Remove selected directory
        gotoBrowseDirectories();
        clickLinkWithText("Second Directory");
        clickLink("remove-directory");
        submit();

        // Check that no directory is selected
        gotoBrowsePrincipals();
        assertSelectedOptionEquals("directoryID" , getText("selectdirectory.label"));
        assertWarningAndErrorNotPresent();
    }

    public void testOnlyDirectoryIsSelected()
    {
        intendToModifyData();

        gotoBrowseDirectories();

        clickLinkWithText("Apache DS");
        clickLink("remove-directory");
        submit();

        clickLinkWithText("Apache DS 1.5.4");
        clickLink("remove-directory");
        submit();

        clickLinkWithText("Second Directory");
        clickLink("remove-directory");
        submit();

        gotoBrowsePrincipals();

        assertSelectedOptionEquals("directoryID" , "Test Internal Directory");
    }
}
