package com.atlassian.crowd.acceptance.tests.soap;

import java.io.File;
import java.io.IOException;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;

import org.apache.commons.io.FileUtils;
import org.custommonkey.xmlunit.XMLAssert;
import org.custommonkey.xmlunit.XMLUnit;
import org.w3c.dom.Document;

/**
 * Tests that the WSDL is exactly the same as in Crowd 2.0.x.
 *
 * @since v2.1
 */
public class WsdlTest extends CrowdAcceptanceTestCase
{
    private static final String TEST_WSDL_HOST_PATH = "http://localhost:8095/crowd";

    /**
     * Tests that the WSDL is exactly the same as in Crowd 2.0.x.
     */
    public void testWsdlSame() throws Exception
    {
        final File file = AcceptanceTestHelper.getResource("SecurityServer-2.0.7.xml");
        String expectedWsdlText = FileUtils.readFileToString(file);
        
        // replace the original host path with the current host path
        expectedWsdlText = expectedWsdlText.replaceAll(TEST_WSDL_HOST_PATH, HOST_PATH);
        
        Document expectedWsdl = WsdlNormalizer.parseAndNormalize(expectedWsdlText);
        
        tester.gotoPage(getBaseUrl() + "/services/SecurityServer?wsdl");
        String currentWsdlText = tester.getPageSource();

        Document currentWsdl = WsdlNormalizer.parseAndNormalize(currentWsdlText);
        
        XMLUnit.setIgnoreWhitespace(true);
        
        XMLAssert.assertXMLEqual(expectedWsdl, currentWsdl);
    }
}
