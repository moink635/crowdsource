package com.atlassian.crowd.acceptance.tests.applications.crowd.legacy;

public class Crowd20XmlRestoreTest extends BaseLegacyXmlRestoreTest
{
    public String getLegacyXmlFileName()
    {
        return "2_0_7_416.xml";
    }
}

