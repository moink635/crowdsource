package com.atlassian.crowd.acceptance.tests.applications.crowd.user;

public class ViewApplicationsTest extends CrowdUserConsoleAcceptenceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        loadXmlOnSetUp("userconsoletest.xml");
    }

    public void testViewApplicationsForAdmin()
    {
        log("Running testViewApplicationsForAdmin");

        _loginAdminUser();

        gotoPage("/console/user/viewapplications.action");

        assertKeyPresent("menu.user.console.viewapplications.label");
        assertKeyPresent("user.console.applications.text");
        assertTextPresent("crowd");
        assertTextPresent("crowd-openid-server");
        assertTextPresent("demo");
    }

    public void testViewApplicationsForUser()
    {
        log("Running testViewApplicationsForUser");

        _loginImmutableUser();

        gotoPage("/console/user/viewapplications.action");

        assertKeyPresent("menu.user.console.viewapplications.label");
        assertKeyPresent("user.console.applications.text");
        assertTextPresent("crowd");
        assertTextNotPresent("crowd-openid-server");
        assertTextNotPresent("demo");
    }

    public void testViewApplicationsForUserWithAliases()
    {
        log("Running testViewApplicationsForUserWithAliases");

        restoreCrowdFromXML("aliastest.xml");

        gotoPage("/console/user/viewapplications.action");

        assertKeyPresent("menu.user.console.viewapplications.label");
        assertKeyPresent("user.console.applications.text");

        assertTextInTable("applicationsTable", new String[] { "crowd", "Crowd Console", "(none)"});
        assertTextInTable("applicationsTable", new String[] { "demo", "Crowd Demo Application", "bob"});
        assertTextInTable("applicationsTable", new String[] { "crowd-openid-server", "CrowdID OpenID Provider", "(none)"});
    }
}
