package com.atlassian.crowd.acceptance.tests.applications.crowd.user;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;

public abstract class CrowdUserConsoleAcceptenceTestCase extends CrowdAcceptanceTestCase
{
    protected static final String TEST_USER_NAME = "user";
    protected static final String TEST_USER_PW = "password";
    protected static final String IMMUTABLE_USER_NAME = "immutable";
    protected static final String IMMUTABLE_USER_PW = "password";
    protected static final String URI_BASE = "/console";

    protected void _loginTestUser()
    {
        _loginTestUser(TEST_USER_NAME, TEST_USER_PW);
   }

    protected void _loginTestUser(final String username, final String password)
    {
        beginAt(URI_BASE);
        assertKeyPresent("login.title");
        setTextField("j_username", username);
        setTextField("j_password", password);
        submit();

        // quick verification
        assertLinkPresentWithKey("menu.logout.label");
        assertKeyNotPresent("console.welcome");   // shouldn't be an admin
        assertKeyPresent("menu.user.console.editprofile.label"); // landing page of the user console
    }

    protected void _loginImmutableUser()
    {
        _loginTestUser(IMMUTABLE_USER_NAME, IMMUTABLE_USER_PW);
    }

    protected void loadXmlOnSetUp(String xmlfile) throws Exception
    {
        _loginAdminUser();
        restoreCrowdFromXML(xmlfile);
        _loginAdminUser();
    }

    protected void _loginExpiredCredentialUser(final String username, final String password)
    {
        beginAt(URI_BASE);
        assertKeyPresent("login.title");
        setTextField("j_username", username);
        setTextField("j_password", password);
        submit();

        // quick verification (can't check against logout link since we have not logged into the account)
        assertKeyNotPresent("invalidlogin.label");
    }
}
