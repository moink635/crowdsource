package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;

import javax.annotation.Nullable;
import java.util.List;

import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertThat;

public class DirectoryConfigurationReadFromFileTest extends CrowdAcceptanceTestCase
{

    public static final String DIRECTORY_TABLE_ID = "directory-table";
    public static final ImmutableList<String> DIRECTORY_TABLE_HEADERS = ImmutableList.of("Name", "Active", "Type", "Action");
    public static final Function<List<String>,String> MAP_DIRECTORY_NAME = new Function<List<String>, String>()
    {
        @Override
        public String apply(@Nullable List<String> input)
        {
            return input.get(0);
        }
    };

    public void testBrowseAllDirectories()
    {
        log("Running: testBrowseAllDirectories");

        _loginAdminUser();
        gotoBrowseDirectories();

        List<String> directories = scrapeTable(DIRECTORY_TABLE_ID, DIRECTORY_TABLE_HEADERS, MAP_DIRECTORY_NAME);
        assertThat(directories, hasItems("Directory One From File", "Directory Two From File"));  // data from the file
    }

    public void testFilterDirectoriesByName()
    {
        log("Running: testFilterDirectoriesByName");

        _loginAdminUser();
        gotoBrowseDirectories();

        setTextField("name", "One");
        submit();

        assertWarningPresent();
    }

    public void testCannotAddNewDirectory()
    {
        log("Running: testCannotAddNewDirectory");

        _loginAdminUser();
        gotoCreateDirectory();

        clickButton("create-internal");

        setWorkingForm("directoryinternal");
        setTextField("name", "New directory");
        submit();

        assertWarningPresent();
    }

    public void testCannotUpdateDirectory()
    {
        log("Running: testCannotAddNewDirectory");

        _loginAdminUser();
        gotoBrowseDirectories();

        clickLinkWithExactText("Directory One From File");

        setWorkingForm("updateGeneral");
        setTextField("name", "New name");
        submit();

        assertWarningPresent();
    }

}
