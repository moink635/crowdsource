package com.atlassian.crowd.acceptance.utils;

import java.util.Date;
import java.util.Properties;

import com.atlassian.crowd.acceptance.tests.directory.BaseTest;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.SpringLDAPConnector;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelper;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;

import com.google.common.collect.Sets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.jdbc.JdbcTestUtils;

import static org.junit.Assert.assertEquals;

/**
 * This class contains code shared by a number of directory integration tests. It
 * is preferable to have it here and not in {@link BaseTest} because for some tests it is not
 * convenient to extend {@link BaseTest}.
 * This class is currently abstract since it is a just container of static methods.
 */
public abstract class DirectoryTestHelper
{
    private static final Logger logger = LoggerFactory.getLogger(DirectoryTestHelper.class);

    public static final String[] TABLE_NAMES =
    {
        "cwd_membership",
        "cwd_user",
        "cwd_group",
        "cwd_directory_attribute",
        "cwd_directory_operation",
        "cwd_directory"
    };
    private static final String APACHEDS102_CONFIG_FILE = "localhost-apacheDS102.properties";
    private static final String APACHEDS154_CONFIG_FILE = "localhost-apacheDS154.properties";

    private static final String AD2K3_CONFIG_FILE = System.getProperty("tpm.ad2k3.filename","crowd-ad1.properties");
    private static final String AD2K3_LOAD_TEST_10K_FILE = System.getProperty("tpm.ad2k3.filename", "crowd-ad1-loadtest10k.properties");

    public static String getApacheDS102ConfigFileName()
    {
        return APACHEDS102_CONFIG_FILE;
    }

    public static String getApacheDS154ConfigFileName()
    {
        return APACHEDS154_CONFIG_FILE;
    }

    public static String getAD2K3ConfigFileName()
    {
        return AD2K3_CONFIG_FILE;
    }

    public static String getAD2K3LoadTest10KFileName()
    {
        return AD2K3_LOAD_TEST_10K_FILE;
    }

    public static Properties getConfigProperties(String configFileName)
    {
        return AcceptanceTestHelper.loadProperties(configFileName);
    }

    public static DirectoryImpl createDirectoryTemplate(Properties directorySettings, LDAPPropertiesHelper ldapPropertiesHelper, boolean useExternalId)
    {
        InternalEntityTemplate
            template = new InternalEntityTemplate(null, "Floating Directory", true, new Date(), new Date());

        DirectoryImpl directoryTemplate = new DirectoryImpl(template);

        directoryTemplate.setActive(true);
        directoryTemplate.setDescription("");
        directoryTemplate.setName(BaseTest.class.getName());
        directoryTemplate.setType(DirectoryType.CONNECTOR);

        //START Directory-variable properties
        String className = directorySettings.getProperty("test.directory.classname");
        directoryTemplate.setImplementationClass(className);

        // set connection information
        directoryTemplate.setAttribute(LDAPPropertiesMapper.LDAP_URL_KEY, directorySettings.getProperty("test.directory.url"));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.LDAP_SECURE_KEY, directorySettings.getProperty("test.directory.secure"));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.LDAP_REFERRAL_KEY, directorySettings.getProperty("test.directory.referral"));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY, directorySettings.getProperty("test.directory.pagedresults.use"));

        String pooling = directorySettings.getProperty("test.directory.connectionpooling");
        if (pooling != null)
        {
            directoryTemplate.setAttribute(LDAPPropertiesMapper.LDAP_POOLING_KEY, pooling);
        }

        String pagedResultsSize = directorySettings.getProperty("test.directory.pagedresults.size");
        if (pagedResultsSize == null)
        {
            pagedResultsSize = new Integer(SpringLDAPConnector.DEFAULT_PAGE_SIZE).toString();
        }
        directoryTemplate.setAttribute(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE, pagedResultsSize);

        directoryTemplate.setAttribute(LDAPPropertiesMapper.LDAP_BASEDN_KEY, directorySettings.getProperty("test.directory.basedn"));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.LDAP_USERDN_KEY, directorySettings.getProperty("test.directory.userdn"));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.LDAP_PASSWORD_KEY, directorySettings.getProperty("test.directory.password"));

        // Use Nested Groups
        directoryTemplate.setAttribute(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, directorySettings.getProperty("test.directory.nested.groups.disabled"));

        // Set the encryption type for the directoryTemplate
        String encryption = directorySettings.getProperty("test.directory.encryption");
        if (encryption != null)
        {
            directoryTemplate.setAttribute(LDAPPropertiesMapper.LDAP_USER_ENCRYPTION_METHOD, encryption);
        }

        // roles disabled?
        String rolesDisabled = directorySettings.getProperty(LDAPPropertiesMapper.ROLES_DISABLED);
        if (rolesDisabled != null)
        {
            directoryTemplate.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, rolesDisabled);
        }

        Properties directorySchema = (Properties) ldapPropertiesHelper.getConfigurationDetails().get(className);
        //END Directory-variable properties

        // set the ldap attributes
        //  groups
        directoryTemplate.setAttribute(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY, (String) directorySchema.get(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.GROUP_NAME_KEY, (String) directorySchema.get(LDAPPropertiesMapper.GROUP_NAME_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.GROUP_OBJECTCLASS_KEY, (String) directorySchema.get(LDAPPropertiesMapper.GROUP_OBJECTCLASS_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.GROUP_OBJECTFILTER_KEY, (String) directorySchema.get(LDAPPropertiesMapper.GROUP_OBJECTFILTER_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.GROUP_USERNAMES_KEY, (String) directorySchema.get(LDAPPropertiesMapper.GROUP_USERNAMES_KEY));

        //  roles
        directoryTemplate.setAttribute(LDAPPropertiesMapper.ROLE_DESCRIPTION_KEY, (String) directorySchema.get(LDAPPropertiesMapper.ROLE_DESCRIPTION_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.ROLE_NAME_KEY, (String) directorySchema.get(LDAPPropertiesMapper.ROLE_NAME_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.ROLE_OBJECTCLASS_KEY, (String) directorySchema.get(LDAPPropertiesMapper.ROLE_OBJECTCLASS_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.ROLE_OBJECTFILTER_KEY, (String) directorySchema.get(LDAPPropertiesMapper.ROLE_OBJECTFILTER_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.ROLE_USERNAMES_KEY, (String) directorySchema.get(LDAPPropertiesMapper.ROLE_USERNAMES_KEY));

        //  users
        directoryTemplate.setAttribute(LDAPPropertiesMapper.USER_EMAIL_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_EMAIL_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.USER_FIRSTNAME_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_FIRSTNAME_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.USER_GROUP_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_GROUP_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.USER_LASTNAME_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_LASTNAME_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.USER_DISPLAYNAME_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_DISPLAYNAME_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.USER_OBJECTCLASS_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_OBJECTCLASS_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.USER_OBJECTFILTER_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_OBJECTFILTER_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.USER_USERNAME_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_USERNAME_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY));
        directoryTemplate.setAttribute(LDAPPropertiesMapper.USER_PASSWORD_KEY, (String) directorySchema.get(LDAPPropertiesMapper.USER_PASSWORD_KEY));
        if (useExternalId)
        {
            directoryTemplate.setAttribute(LDAPPropertiesMapper.LDAP_EXTERNAL_ID, (String) directorySchema.get(LDAPPropertiesMapper.LDAP_EXTERNAL_ID));
        }

        // set the permissions
        directoryTemplate.setAllowedOperations(Sets.newHashSet(OperationType.CREATE_GROUP,
                                                               OperationType.CREATE_USER,
                                                               OperationType.CREATE_ROLE,
                                                               OperationType.UPDATE_GROUP,
                                                               OperationType.UPDATE_USER,
                                                               OperationType.UPDATE_ROLE,
                                                               OperationType.DELETE_GROUP,
                                                               OperationType.DELETE_USER,
                                                               OperationType.DELETE_ROLE));
        return directoryTemplate;
    }

    public static String getEncryptionType(Properties directorySettings)
    {
        return directorySettings.getProperty("test.directory.encryption");
    }

    /**
     * Convenience method to remove a user (if possible). Doesn't log or throw on exception - use only in tearDown()
     * @param userName
     */
    public static void silentlyRemoveUser(String userName, RemoteDirectory remoteDirectory)
    {
        try
        {
            remoteDirectory.removeUser(userName);
        }
        catch (UserNotFoundException e)
        {
            logger.debug("While removing user", e);
        }
        catch (CrowdException e)
        {
            logger.warn("While removing user", e);
        }
    }

    /**
     * Convenience method to remove a group (if possible). Doesn't log or throw on exception - use only in tearDown()
     * @param groupName
     */
    public static void silentlyRemoveGroup(String groupName, RemoteDirectory remoteDirectory)
    {
        try
        {
            remoteDirectory.removeGroup(groupName);
        }
        catch (GroupNotFoundException e)
        {
            logger.debug("While removing group", e);
        }
        catch (CrowdException e)
        {
            logger.warn("While removing group", e);
        }
    }

    /**
     * Convenience method to remove a membership (if possible). Doesn't log or throw on exception - use only in
     * tearDown()
     * @param userName
     * @param groupName
     * @param remoteDirectory
     */
    public static void silentlyRemoveUserFromGroup(String userName, String groupName, RemoteDirectory remoteDirectory)
    {
        try
        {
            remoteDirectory.removeUserFromGroup(userName, groupName);
        }
        catch (MembershipNotFoundException e)
        {
            logger.debug("While removing user membership from group", e);
        }
        catch (CrowdException e)
        {
            logger.warn("While removing user membership from group", e);
        }
    }

    /**
     * Convenience method to remove a membership (if possible). Doesn't log or throw on exception - use only in
     * tearDown()
     * @param childGroup
     * @param parentGroup
     * @param remoteDirectory
     */
    public static void silentlyRemoveGroupFromGroup(String childGroup, String parentGroup,
                                                    RemoteDirectory remoteDirectory)
    {
        try
        {
            remoteDirectory.removeGroupFromGroup(childGroup, parentGroup);
        }
        catch (MembershipNotFoundException e)
        {
            logger.debug("While removing group membership from group", e);
        }
        catch (CrowdException e)
        {
            logger.warn("While removing group membership from group", e);
        }
    }

    /**
     * Removes the content of the named tables
     * @param names
     * @param jdbcTemplate
     */
    public static void deleteFromTables(String[] names, JdbcTemplate jdbcTemplate)
    {
        for (String table : names)
        {
            int rowCount = JdbcTestUtils.deleteFromTables(jdbcTemplate, table);
            if (logger.isInfoEnabled()) {
                logger.info("Deleted {} rows from table {}", rowCount, table);
            }
        }
    }

    public static UserTemplateWithAttributes buildUser(String userName, Long directoryId)
    {
        return buildUser(userName, "test@example.com", "Test", "User", directoryId);
    }

    public static UserTemplateWithAttributes buildUser(String userName, String emailAddress, String firstName, String lastName, Long directoryId)
    {
        UserTemplateWithAttributes user = new UserTemplateWithAttributes(userName, directoryId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setEmailAddress(emailAddress);
        user.setDisplayName(firstName + " " + lastName);
        user.setActive(true);
        return user;
    }

    public static GroupTemplate buildGroup(String groupName, Long directoryId)
    {
        GroupTemplate group = new GroupTemplate(groupName, directoryId, GroupType.GROUP);
        group.setActive(true);
        return group;
    }

    public static void addUser(UserTemplate user, PasswordCredential passwordCredential,
                               RemoteDirectory remoteDirectory)
        throws InvalidCredentialException, InvalidUserException, UserAlreadyExistsException, OperationFailedException
    {
        user.setDirectoryId(remoteDirectory.getDirectoryId());
        remoteDirectory.addUser(user, new PasswordCredential(passwordCredential));
    }

    public static void addUser(String userName, Long directoryId, String password, RemoteDirectory remoteDirectory)
        throws InvalidCredentialException, InvalidUserException, UserAlreadyExistsException, OperationFailedException
    {
        UserTemplateWithAttributes user = buildUser(userName, directoryId);
        remoteDirectory.addUser(user, new PasswordCredential(password));
    }

    public static void addGroup(GroupTemplate group, RemoteDirectory remoteDirectory)
        throws InvalidGroupException, OperationFailedException
    {
        group.setDirectoryId(remoteDirectory.getDirectoryId());
        remoteDirectory.addGroup(group);
    }

    public static void addGroup(String groupName, Long directoryId, RemoteDirectory remoteDirectory)
        throws InvalidGroupException, OperationFailedException
    {
        GroupTemplate groupTemplate = buildGroup(groupName, directoryId);
        Group addedGroup = remoteDirectory.addGroup(groupTemplate);

        assertEquals("The added group must be exactly equal to the provided template",
                     groupTemplate, new GroupTemplate(addedGroup));
    }
}
