package com.atlassian.crowd.acceptance.page;

import net.sourceforge.jwebunit.api.ITestingEngine;

/**
 * Thrown when the current page is not what it should be.
 */
public class IllegalPageStateException extends IllegalStateException
{
    public IllegalPageStateException(ITestingEngine engine, String message)
    {
        super(message + "\nPage test:\n" + engine.getPageText());
    }
}
