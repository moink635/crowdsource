package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectorySynchronisationInformation;
import com.atlassian.crowd.manager.directory.SynchronisationStatusManager;

import java.io.Serializable;

public class MockSynchronisationStatusManager implements SynchronisationStatusManager
{
    public void syncStarted(Directory directory)
    {
        // Do nothing
    }

    public void syncStatus(long directoryId, String key, Serializable... parameters)
    {
        // Do nothing
    }

    public void syncFinished(long directoryId)
    {
        // Do nothing
    }

    public DirectorySynchronisationInformation getDirectorySynchronisationInformation(Directory directory)
    {
        return new DirectorySynchronisationInformation(null, null);
    }
}
