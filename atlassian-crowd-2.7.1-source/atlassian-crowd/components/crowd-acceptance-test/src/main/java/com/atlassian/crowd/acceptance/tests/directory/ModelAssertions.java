package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.user.User;

import static org.junit.Assert.assertEquals;

final class ModelAssertions
{
    private ModelAssertions()
    {
        // prevent construction
    }

    public static void assertUsersEqual(User expected, User actual)
    {
        assertEquals("directory IDs should be the same", expected.getDirectoryId(), actual.getDirectoryId());
        assertEquals("usernames should be the same", expected.getName(), actual.getName());
        assertEquals("first names should be the same", expected.getFirstName(), actual.getFirstName());
        assertEquals("last names should be the same", expected.getLastName(), actual.getLastName());
        assertEquals("display names should be the same", expected.getDisplayName(), actual.getDisplayName());
        assertEquals("email addresses should be the same", expected.getEmailAddress(), actual.getEmailAddress());
    }

    public static void assertGroupsEqual(Group expected, Group actual)
    {
        assertEquals("directory IDs should be the same", expected.getDirectoryId(), actual.getDirectoryId());
        assertEquals("group names should be the same", expected.getName(), actual.getName());
        assertEquals("group types should be the same", expected.getType(), actual.getType());
    }
}
