package com.atlassian.crowd.acceptance.tests.applications.crowd.performance;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import net.sourceforge.jwebunit.html.Table;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;

/**
 * These two tests are not part of the standard
 * acceptance test harness:
 * <p/>
 * 1. Import 100k principals from CSV (approx 8 mins)
 * 2. Import 100k principals, 1k groups, 1M memberships (approx 2.5 hours)
 * <p/>
 * Proceed with caution :)
 */

public class VeryLargeCsvImporterTest extends CrowdAcceptanceTestCase
{
    private String userFileLocation = null;
    private String groupmembershipFileLocation = null;

    public void setUp() throws Exception
    {
        super.setUp();
        _loginAdminUser();
        restoreBaseSetup();

        gotoImporters();

        clickButton("importcsv");
    }

    public void testCsvImportVeryLargeDatasetOfUsers() throws IOException
    {
        testCsvImportUsers(100000);
    }

    public void testCsvImportVeryLargeDatasetOfUsersAndGroupMemberships() throws IOException
    {
        testCsvImportUsersAndGroupMappings(100000, 1000, 10);
    }

    private File createCsvUsersFile(int users) throws IOException
    {
        File file = new File("users.csv");
        BufferedWriter userWriter = new BufferedWriter(new FileWriter(file));

        userWriter.write("User Name, First Name, Last Name, Email Address, Password");
        userWriter.newLine();

        DecimalFormat df = new DecimalFormat("0000000");

        String userNumber;

        for (int j = 0; j < users; j++)
        {
            userNumber = df.format(j);

            userWriter.write("user" + userNumber);
            userWriter.write(", First" + userNumber);
            userWriter.write(", Last" + userNumber);
            userWriter.write(", user" + userNumber + "@example.com");
            userWriter.write(", secret" + userNumber);
            userWriter.newLine();
        }

        userWriter.close();

        return file;
    }

    private File createCsvMembershipsFile(int users, int groups, int membershipsPerUser) throws IOException
    {
        File file = new File("memberships.csv");
        BufferedWriter membershipWriter = new BufferedWriter(new FileWriter(file));

        membershipWriter.write("User,Group");
        membershipWriter.newLine();

        DecimalFormat df = new DecimalFormat("0000000");

        for (int j = 0; j < users; j++)
        {
            String userNumber = df.format(j);

            for (int k = 0; k < membershipsPerUser; k++)
            {
                membershipWriter.write("user" + userNumber);
                String groupNumber = df.format((k + j * groups / users) % groups);
                membershipWriter.write(", group" + groupNumber);
                membershipWriter.newLine();
            }
        }

        membershipWriter.close();

        return file;
    }

    private void testCsvImportUsers(int users) throws IOException
    {
        log("Running testCsvImportUsers(" + users + " users)");

        userFileLocation = createCsvUsersFile(users).getCanonicalPath();

        setWorkingForm("dataimport");
        setTextField("delimiter", ",");
        setTextField("users", userFileLocation);

        submit();     // Successful

        assertKeyPresent("dataimport.csv.configuration.text");

        selectOptionByValue("user.0", "user.username");
        selectOptionByValue("user.1", "user.firstname");
        selectOptionByValue("user.2", "user.lastname");
        selectOptionByValue("user.3", "user.emailaddress");
        selectOptionByValue("user.4", "user.password");

        assertKeyNotPresent("dataimport.csv.configuration.groupmapping.label");

        submit();   // Successful

        assertKeyPresent("dataimport.csv.configuration.confirmation.text");

        // Assert the directory we have chosen is present
        assertKeyPresent("dataimport.csv.configuration.passwordencrypted.label", getMessage("yes.label"));
        assertKeyPresent("dataimport.csv.configuration.userfile.label", userFileLocation);

        assertKeyNotPresent("dataimport.csv.configuration.groupmembershipfile.label");

        String[][] userMappingsTableData = {new String[]{getMessage("dataimport.csv.configuration.headerrow"), getMessage("dataimport.csv.configuration.mapping")}, new String[]{"User Name", getMessage("user.username")}, new String[]{"First Name", getMessage("user.firstname")}, new String[]{"Last Name", getMessage("user.lastname")}, new String[]{"Email Address", getMessage("user.emailaddress")}, new String[]{"Password", getMessage("user.password")}};

        assertTableEquals("usermappings", new Table(userMappingsTableData));

        //Assert no Group data
        assertKeyNotPresent("dataimport.csv.configuration.groupmapping.label");
        assertTableNotPresent("groupmappings");

        long startTime = System.currentTimeMillis();
        submit();   // Successful
        long timeTaken = System.currentTimeMillis() - startTime;
        log("Import process took " + timeTaken + " ms");

        assertKeyPresent("dataimport.csv.result.text");
        assertTextInElement("users-imported", "" + users);
        assertTextInElement("groups-imported", "0");
        assertTextInElement("memberships-imported", "0");

        log("Finished importing " + users + " users");
    }

    public void testCsvImportUsersAndGroupMappings(int users, int groups, int membershipsPerUser) throws IOException
    {
        log("Running testCsvImportUsersAndGroupMappings(" + users + " users, " + groups + " groups, " + membershipsPerUser + " membershipsPerUser)");

        userFileLocation = createCsvUsersFile(users).getCanonicalPath();
        groupmembershipFileLocation = createCsvMembershipsFile(users, groups, membershipsPerUser).getCanonicalPath();

        setTextField("delimiter", ",");
        setTextField("users", userFileLocation);
        setTextField("groupMemberships", groupmembershipFileLocation);

        submit();     // Successful

        assertKeyPresent("dataimport.csv.configuration.text");

        selectOptionByValue("user.0", "user.username");
        selectOptionByValue("user.1", "user.firstname");
        selectOptionByValue("user.2", "user.lastname");
        selectOptionByValue("user.3", "user.emailaddress");
        selectOptionByValue("user.4", "user.password");

        selectOptionByValue("group.0", "group.username");
        selectOptionByValue("group.1", "group.name");

        submit();   // Successful

        assertKeyPresent("dataimport.csv.configuration.confirmation.text");

        // Assert the directory we have chosen is present
        assertKeyPresent("dataimport.csv.configuration.passwordencrypted.label", getMessage("yes.label"));
        assertKeyPresent("dataimport.csv.configuration.userfile.label", userFileLocation);
        assertKeyPresent("dataimport.csv.configuration.groupmembershipfile.label", groupmembershipFileLocation);

        String[][] userMappingsTableData = {new String[]{getMessage("dataimport.csv.configuration.headerrow"), getMessage("dataimport.csv.configuration.mapping")}, new String[]{"User Name", getMessage("user.username")}, new String[]{"First Name", getMessage("user.firstname")}, new String[]{"Last Name", getMessage("user.lastname")}, new String[]{"Email Address", getMessage("user.emailaddress")}, new String[]{"Password", getMessage("user.password")}};

        assertTableEquals("usermappings", new Table(userMappingsTableData));

        String[][] groupMappingsTableData = {new String[]{getMessage("dataimport.csv.configuration.headerrow"), getMessage("dataimport.csv.configuration.mapping")}, new String[]{"User", getMessage("group.username")}, new String[]{"Group", getMessage("group.name")}};

        assertTableEquals("groupmappings", new Table(groupMappingsTableData));

        long startTime = System.currentTimeMillis();
        submit();   // Successful
        assertTextInElement("users-imported", "" + users);
        assertTextInElement("groups-imported", "" + groups);
        assertTextInElement("memberships-imported", "" + membershipsPerUser * users);

        long timeTaken = System.currentTimeMillis() - startTime;
        log("Import process took " + timeTaken + " ms");

        log("Finished importing " + users + " users, " + groups + " groups and " + membershipsPerUser * users + " memberships");
    }
}
