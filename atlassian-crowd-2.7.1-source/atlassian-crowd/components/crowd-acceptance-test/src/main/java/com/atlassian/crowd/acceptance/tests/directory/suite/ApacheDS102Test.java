package com.atlassian.crowd.acceptance.tests.directory.suite;

import com.atlassian.crowd.acceptance.tests.directory.BasicTest;
import com.atlassian.crowd.acceptance.tests.directory.LocalAttributesTest;
import com.atlassian.crowd.acceptance.tests.directory.LocalGroupsTest;
import com.atlassian.crowd.acceptance.tests.directory.NestedGroupsTest;
import com.atlassian.crowd.acceptance.tests.directory.PageAndRangeTest;
import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


/**
 * Tests for ApacheDS 1.0.2, running against the localhost webapp version.
 */
public class ApacheDS102Test extends TestCase
{
    public static Test suite()
    {
        TestSuite suite = new TestSuite();
        suite.addTestSuite(ApacheDS102BasicTest.class);
        suite.addTestSuite(ApacheDS102NestedGroupsTest.class);
//        suite.addTestSuite(ApacheDS102PageAndRangeTest.class); // this test takes too long as it needs loads of users
        suite.addTestSuite(ApacheDS102LocalAttributesTest.class);
        suite.addTestSuite(ApacheDS102LocalGroupsTest.class);
        return suite;
    }

    public static class ApacheDS102BasicTest extends BasicTest
    {
        public ApacheDS102BasicTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS102ConfigFileName());
        }

        public void testAuthenticateAfterPasswordUpdate()
        {
            // Apache DS 1.0.2 will not pick up password changes without a restart (!)
            assertTrue(true);
        }

        @Override
        public void testAuthenticateAfterUserAddWithUnicodePassword()
        {
            // Apache DS 1.0.2 seems to not like unicode characters in passwords (!)
            assertTrue(true);
        }

        @Override
        public void testAuthenticateAfterUnicodePasswordUpdate()
        {
            // Apache DS 1.0.2 will not pick up password changes without a restart (!)
            assertTrue(true);
        }
    }

    public static class ApacheDS102NestedGroupsTest extends NestedGroupsTest
    {
        public ApacheDS102NestedGroupsTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS102ConfigFileName());
        }
    }

    public static class ApacheDS102PageAndRangeTest extends PageAndRangeTest
    {
        public ApacheDS102PageAndRangeTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS102ConfigFileName());
        }
    }
    
    public static class ApacheDS102LocalAttributesTest extends LocalAttributesTest
    {
        public ApacheDS102LocalAttributesTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS102ConfigFileName());
        }
    }

    public static class ApacheDS102LocalGroupsTest extends LocalGroupsTest
    {
        public ApacheDS102LocalGroupsTest()
        {
            setDirectoryConfigFile(DirectoryTestHelper.getApacheDS102ConfigFileName());
        }
    }
}
