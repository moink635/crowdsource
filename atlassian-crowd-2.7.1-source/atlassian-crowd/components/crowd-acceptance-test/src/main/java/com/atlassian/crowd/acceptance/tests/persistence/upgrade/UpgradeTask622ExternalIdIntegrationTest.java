package com.atlassian.crowd.acceptance.tests.persistence.upgrade;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelper;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.upgrade.tasks.UpgradeTask622ExternalId;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class UpgradeTask622ExternalIdIntegrationTest
{
    @Inject private DirectoryDao directoryDao;
    @Inject private LDAPPropertiesHelper ldapPropertiesHelper;
    @Inject private DataSource dataSource;

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Test
    public void testUpgrade() throws Exception
    {
        UpgradeTask622ExternalId task = new UpgradeTask622ExternalId(directoryDao, ldapPropertiesHelper);

        task.doUpgrade();

        // default ExternalID attribute for ApacheDS
        assertThat(directoryDao.findById(3).getValue(LDAPPropertiesMapper.LDAP_EXTERNAL_ID), is("entryUUID"));
    }

    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }

    public void setLdapPropertiesHelper(LDAPPropertiesHelper ldapPropertiesHelper)
    {
        this.ldapPropertiesHelper = ldapPropertiesHelper;
    }

    public void setDataSource(DataSource dataSource)
    {
        this.dataSource = dataSource;
    }
}
