package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.restriction.Property;
import com.atlassian.crowd.search.query.entity.restriction.PropertyUtils;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import static com.atlassian.crowd.acceptance.tests.directory.BasicTest.getImplementation;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import static com.atlassian.crowd.acceptance.tests.directory.ModelAssertions.assertGroupsEqual;
import static com.atlassian.crowd.acceptance.tests.directory.ModelAssertions.assertUsersEqual;

/**
 * Tests local (InternalDirectory) attribute storage for LDAP directories.
 * <p/>
 * This test works by inserting an actual Directory object in the database
 * but mocks everything higher than or peripheral to that.
 * <p/>
 * This is a real integration test:
 * - LDAP calls go out to a real LDAP server (eg. ApacheDS).
 * - Internal directory calls go the database (eg. HSQLDB).
 * <p/>
 * See {@link com.atlassian.crowd.acceptance.tests.directory.BaseTest} for implementation details on how exactly this is achieved.
 * <p/>
 * Local Groups are turned OFF for this set of tests.
 */
public abstract class LocalAttributesTest extends BaseTest
{
    private static final String USER_NAME = "bsmith";
    private static final String USER_FIRSTNAME = "Bob";
    private static final String USER_LASTNAME = "Smith";
    private static final String USER_DISPLAYNAME = USER_FIRSTNAME + " " + USER_LASTNAME;
    private static final String USER_EXTERNAL_ID = "1-user-external-id";
    private static final String USER_EMAIL = "bsmith@example.com";

    private static final String USER2_NAME = "jsmith";
    private static final String USER2_FIRSTNAME = "John";
    private static final String USER2_LASTNAME = "Smith";
    private static final String USER2_DISPLAYNAME = USER2_FIRSTNAME + " " + USER2_LASTNAME;
    private static final String USER2_EXTERNAL_ID = "2-user-external-id";
    private static final String USER2_EMAIL = "jsmith@example.com";

    private static final String USER3_NAME = "jdoe";
    private static final String USER3_FIRSTNAME = "Jane";
    private static final String USER3_LASTNAME = "Doe";
    private static final String USER3_DISPLAYNAME = USER3_FIRSTNAME + " " + USER3_LASTNAME;
    private static final String USER3_EXTERNAL_ID = "3-user-external-id";
    private static final String USER3_EMAIL = "jdoe@example.com";

    private static final String GROUP_NAME = "cool-crew";
    private static final String GROUP2_NAME = "group2";
    private static final String GROUP3_NAME = "group3";

    private User user;
    private Group group;

    public LocalAttributesTest()
    {
//        setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
    }

    @Override
    protected void configureDirectory(final Properties directorySettings)
    {
        super.configureDirectory(directorySettings);
        directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.TRUE.toString());
    }

    protected void loadTestData() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);
        user = new ImmutableUser(directory.getId(), USER_NAME, USER_FIRSTNAME, USER_LASTNAME, USER_DISPLAYNAME, USER_EMAIL, USER_EXTERNAL_ID);
        remoteDirectory.addUser(new UserTemplate(user), new PasswordCredential("password"));

        User user2 = new ImmutableUser(directory.getId(), USER2_NAME, USER2_FIRSTNAME, USER2_LASTNAME, USER2_DISPLAYNAME, USER2_EMAIL, USER2_EXTERNAL_ID);
        remoteDirectory.addUser(new UserTemplate(user2), new PasswordCredential("password"));

        User user3 = new ImmutableUser(directory.getId(), USER3_NAME, USER3_FIRSTNAME, USER3_LASTNAME, USER3_DISPLAYNAME, USER3_EMAIL, USER3_EXTERNAL_ID);
        remoteDirectory.addUser(new UserTemplate(user3), new PasswordCredential("password"));

        group = remoteDirectory.addGroup(new GroupTemplate(GROUP_NAME, directory.getId()));
        remoteDirectory.addGroup(new GroupTemplate(GROUP2_NAME, directory.getId()));
        remoteDirectory.addGroup(new GroupTemplate(GROUP3_NAME, directory.getId()));
    }

    @Override
    protected void removeTestData() throws DirectoryInstantiationException
    {
        DirectoryTestHelper.silentlyRemoveUser(USER_NAME, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser(USER2_NAME, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser(USER3_NAME, getRemoteDirectory());

        DirectoryTestHelper.silentlyRemoveGroup(GROUP_NAME, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup(GROUP2_NAME, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup(GROUP3_NAME, getRemoteDirectory());
    }

    // USER ATTRIBUTES

    public void testFindUserWithAttributesByNameAfterStoringNoAttributes() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        UserWithAttributes foundUser = remoteDirectory.findUserWithAttributesByName(user.getName());
        assertUsersEqual(user, foundUser);
        assertEquals(0, foundUser.getKeys().size());

        // store nothing
        remoteDirectory.storeUserAttributes(USER_NAME, Collections.<String, Set<String>>emptyMap());

        // expect nothing
        foundUser = remoteDirectory.findUserWithAttributesByName(USER_NAME);
        assertUsersEqual(user, foundUser);
        assertEquals(0, foundUser.getKeys().size());
    }

    public void testFindUserWithAttributesByNameWithNoAttributes() throws Exception
    {
        UserWithAttributes foundUser = getImplementation(directory).findUserWithAttributesByName(USER_NAME);
        assertUsersEqual(user, foundUser);

        // assert user has no custom attributes
        assertEquals(0, foundUser.getKeys().size());
    }

    public void testStoreUserAttributes() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        UserWithAttributes foundUser = remoteDirectory.findUserWithAttributesByName(USER_NAME);
        assertUsersEqual(user, foundUser);
        assertEquals(0, foundUser.getKeys().size());

        Set<String> letters = ImmutableSet.<String>builder().add("a").add("b").add("c").build();
        Set<String> numbers = ImmutableSet.<String>builder().add("1").add("2").add("3").build();
        Set<String> nigel = ImmutableSet.<String>builder().add("No Friends").build();

        // store stuff
        remoteDirectory.storeUserAttributes(USER_NAME, ImmutableMap.<String, Set<String>>builder()
                .put("numbers", numbers)
                .put("letters", letters)
                .put("nigel", nigel)
                .put("nothing", Collections.<String>emptySet())
                .build());

        // expect stuff
        foundUser = remoteDirectory.findUserWithAttributesByName(USER_NAME);
        assertUsersEqual(user, foundUser);
        assertEquals(3, foundUser.getKeys().size());
        assertEquals(letters, foundUser.getValues("letters"));
        assertEquals(numbers, foundUser.getValues("numbers"));
        assertEquals(nigel, foundUser.getValues("nigel"));
    }

    public void testRemoveUserAttributes() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        Set<String> letters = ImmutableSet.<String>builder().add("a").add("b").add("c").build();
        Set<String> numbers = ImmutableSet.<String>builder().add("1").add("2").add("3").build();

        // store stuff
        remoteDirectory.storeUserAttributes(USER_NAME, ImmutableMap.<String, Set<String>>builder()
                .put("numbers", numbers)
                .put("letters", letters)
                .build());

        // expect stored
        UserWithAttributes foundUser = remoteDirectory.findUserWithAttributesByName(USER_NAME);
        assertUsersEqual(user, foundUser);
        assertEquals(ImmutableSet.<String>builder().add("numbers").add("letters").build(), foundUser.getKeys());

        // remove
        remoteDirectory.removeUserAttributes(USER_NAME, "numbers");

        // expect removed
        foundUser = remoteDirectory.findUserWithAttributesByName(USER_NAME);
        assertUsersEqual(user, foundUser);
        assertEquals(ImmutableSet.<String>builder().add("letters").build(), foundUser.getKeys());
    }

    public void testRemoveUserAttributeWithNoAttributes() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        // remove shouldn't throw anything
        remoteDirectory.removeUserAttributes(USER_NAME, "stuff");

        UserWithAttributes foundUser = remoteDirectory.findUserWithAttributesByName(USER_NAME);
        assertUsersEqual(user, foundUser);
        assertEquals(0, foundUser.getKeys().size());
    }

    public void testRemoveUserRemovesAttributes() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        Set<String> letters = ImmutableSet.<String>builder().add("a").add("b").add("c").build();
        Set<String> numbers = ImmutableSet.<String>builder().add("1").add("2").add("3").build();

        // store stuff
        remoteDirectory.storeUserAttributes(USER_NAME, ImmutableMap.<String, Set<String>>builder()
                .put("numbers", numbers)
                .put("letters", letters)
                .build());

        // expect stored
        UserWithAttributes foundUser = remoteDirectory.findUserWithAttributesByName(USER_NAME);
        assertUsersEqual(user, foundUser);
        assertEquals(ImmutableSet.<String>builder().add("numbers").add("letters").build(), foundUser.getKeys());

        // remove the user
        remoteDirectory.removeUser(USER_NAME);

        sessionFactory.getCurrentSession().flush();

        // add a new user with the same name
        User newUser = new ImmutableUser(directory.getId(), USER_NAME, USER_FIRSTNAME, USER_LASTNAME, USER_DISPLAYNAME, USER_EMAIL);
        remoteDirectory.addUser(new UserTemplate(newUser), new PasswordCredential("password"));

        // assert that the new user doesn't have the old user's attributes
        foundUser = remoteDirectory.findUserWithAttributesByName(USER_NAME);
        assertEquals(0, foundUser.getKeys().size());
    }

    public void testSearchAllUsers() throws OperationFailedException
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        List<User> users = remoteDirectory.searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).returningAtMost(10));

        assertEquals(3, users.size());
    }

    public void testSearchUsersOnLdapOnlyAttributes() throws OperationFailedException
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        List<User> users = remoteDirectory.searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(
                Combine.anyOf(
                        Restriction.on(UserTermKeys.USERNAME).exactlyMatching(USER_NAME),
                        Restriction.on(UserTermKeys.FIRST_NAME).exactlyMatching(USER2_FIRSTNAME)
                )
        ).returningAtMost(10));

        assertEquals(2, users.size());
        Set<String> result = ImmutableSet.of(users.get(0).getName(), users.get(1).getName());
        assertTrue(result.contains(USER_NAME));
        assertTrue(result.contains(USER2_NAME));
    }

    public void testSearchUsernamesOnLdapOnlyAttributes() throws OperationFailedException
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        List<String> usernames = remoteDirectory.searchUsers(QueryBuilder.queryFor(String.class, EntityDescriptor.user()).with(
                Combine.anyOf(
                        Restriction.on(UserTermKeys.USERNAME).exactlyMatching(USER_NAME),
                        Restriction.on(UserTermKeys.FIRST_NAME).exactlyMatching(USER2_FIRSTNAME)
                )
        ).returningAtMost(10));

        assertEquals(2, usernames.size());
        assertTrue(usernames.contains(USER_NAME));
        assertTrue(usernames.contains(USER2_NAME));
    }

    public void testSearchUsersOnInternalOnlyAttributes() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        // setup
        Map<String, Set<String>> maleAttrib = ImmutableMap.of("gender", (Set<String>)ImmutableSet.of("male"));
        Map<String, Set<String>> femaleAttrib = ImmutableMap.of("gender", (Set<String>)ImmutableSet.of("female"));

        remoteDirectory.storeUserAttributes(USER_NAME, maleAttrib);
        remoteDirectory.storeUserAttributes(USER2_NAME, maleAttrib);
        remoteDirectory.storeUserAttributes(USER3_NAME, femaleAttrib);

        // search
        List<User> users = remoteDirectory.searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(
                Restriction.on(PropertyUtils.ofTypeString("gender")).startingWith("M")
        ).returningAtMost(10));

        assertEquals(2, users.size());
        Set<String> result = ImmutableSet.of(users.get(0).getName(), users.get(1).getName());
        assertTrue(result.contains(USER_NAME));
        assertTrue(result.contains(USER2_NAME));
    }

    public void testSearchUsernamesOnInternalOnlyAttributes() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        // setup
        Map<String, Set<String>> maleAttrib = ImmutableMap.of("gender", (Set<String>)ImmutableSet.of("male"));
        Map<String, Set<String>> femaleAttrib = ImmutableMap.of("gender", (Set<String>)ImmutableSet.of("female"));

        remoteDirectory.storeUserAttributes(USER_NAME, maleAttrib);
        remoteDirectory.storeUserAttributes(USER2_NAME, maleAttrib);
        remoteDirectory.storeUserAttributes(USER3_NAME, femaleAttrib);

        // search
        List<String> usernames = remoteDirectory.searchUsers(QueryBuilder.queryFor(String.class, EntityDescriptor.user()).with(
                Restriction.on(PropertyUtils.ofTypeString("gender")).startingWith("M")
        ).returningAtMost(10));

        assertEquals(2, usernames.size());
        assertTrue(usernames.contains(USER_NAME));
        assertTrue(usernames.contains(USER2_NAME));
    }

    // GROUP ATTRIBUTES

    public void testFindGroupWithAttributesByNameAfterStoringNoAttributes() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        GroupWithAttributes foundGroup = remoteDirectory.findGroupWithAttributesByName(GROUP_NAME);
        assertGroupsEqual(group, foundGroup);
        assertEquals(0, foundGroup.getKeys().size());

        // store nothing
        remoteDirectory.storeGroupAttributes(GROUP_NAME, Collections.<String, Set<String>>emptyMap());

        // expect nothing
        foundGroup = remoteDirectory.findGroupWithAttributesByName(GROUP_NAME);
        assertGroupsEqual(group, foundGroup);
        assertEquals(0, foundGroup.getKeys().size());
    }

    public void testFindGroupWithAttributesByNameWithNoAttributes() throws Exception
    {
        GroupWithAttributes foundGroup = getImplementation(directory).findGroupWithAttributesByName(GROUP_NAME);
        assertGroupsEqual(group, foundGroup);

        // assert group has no custom attributes
        assertEquals(0, foundGroup.getKeys().size());
    }

    public void testStoreGroupAttributes() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        GroupWithAttributes foundGroup = remoteDirectory.findGroupWithAttributesByName(GROUP_NAME);
        assertGroupsEqual(group, foundGroup);
        assertEquals(0, foundGroup.getKeys().size());

        Set<String> letters = ImmutableSet.<String>builder().add("a").add("b").add("c").build();
        Set<String> numbers = ImmutableSet.<String>builder().add("1").add("2").add("3").build();
        Set<String> nigel = ImmutableSet.<String>builder().add("No Friends").build();

        // store stuff
        remoteDirectory.storeGroupAttributes(GROUP_NAME, ImmutableMap.<String, Set<String>>builder()
                .put("numbers", numbers)
                .put("letters", letters)
                .put("nigel", nigel)
                .put("nothing", Collections.<String>emptySet())
                .build());

        // expect stuff
        foundGroup = remoteDirectory.findGroupWithAttributesByName(GROUP_NAME);
        assertGroupsEqual(group, foundGroup);
        assertEquals(3, foundGroup.getKeys().size());
        assertEquals(letters, foundGroup.getValues("letters"));
        assertEquals(numbers, foundGroup.getValues("numbers"));
        assertEquals(nigel, foundGroup.getValues("nigel"));
    }

    public void testRemoveGroupAttributes() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        Set<String> letters = ImmutableSet.<String>builder().add("a").add("b").add("c").build();
        Set<String> numbers = ImmutableSet.<String>builder().add("1").add("2").add("3").build();

        // store stuff
        remoteDirectory.storeGroupAttributes(GROUP_NAME, ImmutableMap.<String, Set<String>>builder()
                .put("numbers", numbers)
                .put("letters", letters)
                .build());

        // expect stored
        GroupWithAttributes foundGroup = remoteDirectory.findGroupWithAttributesByName(GROUP_NAME);
        assertGroupsEqual(group, foundGroup);
        assertEquals(ImmutableSet.<String>builder().add("numbers").add("letters").build(), foundGroup.getKeys());

        // remove
        remoteDirectory.removeGroupAttributes(GROUP_NAME, "numbers");

        // expect removed
        foundGroup = remoteDirectory.findGroupWithAttributesByName(GROUP_NAME);
        assertGroupsEqual(group, foundGroup);
        assertEquals(ImmutableSet.<String>builder().add("letters").build(), foundGroup.getKeys());
    }

    public void testRemoveGroupAttributeWithNoAttributes() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        // remove shouldn't throw anything
        remoteDirectory.removeGroupAttributes(GROUP_NAME, "stuff");

        GroupWithAttributes foundGroup = remoteDirectory.findGroupWithAttributesByName(GROUP_NAME);
        assertGroupsEqual(group, foundGroup);
        assertEquals(0, foundGroup.getKeys().size());
    }

    public void testRemoveGroupRemovesAttributes() throws Exception
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        Set<String> letters = ImmutableSet.<String>builder().add("a").add("b").add("c").build();
        Set<String> numbers = ImmutableSet.<String>builder().add("1").add("2").add("3").build();

        // store stuff
        remoteDirectory.storeGroupAttributes(GROUP_NAME, ImmutableMap.<String, Set<String>>builder()
                .put("numbers", numbers)
                .put("letters", letters)
                .build());

        // expect stored
        GroupWithAttributes foundGroup = remoteDirectory.findGroupWithAttributesByName(GROUP_NAME);
        assertGroupsEqual(group, foundGroup);
        assertEquals(ImmutableSet.<String>builder().add("numbers").add("letters").build(), foundGroup.getKeys());

        // remove the group
        remoteDirectory.removeGroup(GROUP_NAME);

        sessionFactory.getCurrentSession().flush();

        // add a new group with the same name
        GroupTemplate groupTemplate = new GroupTemplate(GROUP_NAME, directory.getId());
        getImplementation(directory).addGroup(groupTemplate);

        // assert that the new group doesn't have the old group's attributes
        foundGroup = remoteDirectory.findGroupWithAttributesByName(GROUP_NAME);
        assertEquals(0, foundGroup.getKeys().size());
    }

    public void testSearchAllGroups() throws OperationFailedException
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        List<Group> groups = remoteDirectory.searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).returningAtMost(10));

        assertEquals(3, groups.size());
    }

    public void testSearchGroupsOnLdapOnlyAttributes() throws OperationFailedException
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        List<Group> groups = remoteDirectory.searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).with(
                Combine.anyOf(
                        Restriction.on(GroupTermKeys.NAME).exactlyMatching(GROUP2_NAME),
                        Restriction.on(GroupTermKeys.NAME).exactlyMatching(GROUP3_NAME)
                )
        ).returningAtMost(10));

        assertEquals(2, groups.size());
        Set<String> result = ImmutableSet.of(groups.get(0).getName(), groups.get(1).getName());
        assertTrue(result.contains(GROUP2_NAME));
        assertTrue(result.contains(GROUP3_NAME));
    }

    public void testSearchGroupNamesOnLdapOnlyAttributes() throws OperationFailedException
    {
        RemoteDirectory remoteDirectory = getImplementation(directory);

        List<String> groupnames = remoteDirectory.searchGroups(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).with(
                Combine.anyOf(
                        Restriction.on(GroupTermKeys.NAME).exactlyMatching(GROUP2_NAME),
                        Restriction.on(GroupTermKeys.NAME).exactlyMatching(GROUP3_NAME)
                )
        ).returningAtMost(10));

        assertEquals(2, groupnames.size());
        assertTrue(groupnames.contains(GROUP2_NAME));
        assertTrue(groupnames.contains(GROUP3_NAME));
    }

}

