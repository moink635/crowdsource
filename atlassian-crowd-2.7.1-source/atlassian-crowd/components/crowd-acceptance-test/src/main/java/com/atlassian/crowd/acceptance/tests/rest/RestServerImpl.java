package com.atlassian.crowd.acceptance.tests.rest;

import java.net.MalformedURLException;
import java.net.URL;

import com.atlassian.crowd.acceptance.page.RestorePage;
import com.atlassian.crowd.acceptance.rest.RestServer;
import com.atlassian.crowd.acceptance.tests.TestDataState;
import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;
import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;
import com.sun.jersey.api.client.Client;
import net.sourceforge.jwebunit.junit.WebTester;

public enum RestServerImpl implements RestServer
{
    INSTANCE;

    private final WebTester webTester = new WebTester();

    @Override
    public final URL getBaseUrl()
    {
        try
        {
            return new URL(CrowdAcceptanceTestCase.HOST_PATH);
        }
        catch (MalformedURLException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void before() throws Exception
    {
        webTester.beginAt(getBaseUrl().toString());

        String xmlfilename = AcceptanceTestHelper.getResourcePath("xml/restsetup.xml");

        if (TestDataState.INSTANCE.isRestoredXml(getBaseUrl(), xmlfilename))
        {
            return;
        }

        new RestorePage(webTester, getBaseUrl().toString(), "Super User", "admin", "admin")
                .restore(xmlfilename);

        TestDataState.INSTANCE.setRestoredXml(getBaseUrl(), xmlfilename);
    }

    public void after()
    {
        webTester.closeBrowser();
    }

    public Client decorateClient(final Client client)
    {
        return client;
    }
}
