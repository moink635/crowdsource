package com.atlassian.crowd.acceptance.tests.applications.demo;

public class AddUserTest extends DemoAcceptanceTestCase
{

    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("adduserdemotest.xml");
    }

    @Override
    public void tearDown() throws Exception
    {
        restoreBaseSetup();
        super.tearDown();
    }

    public void testAddUser()
    {
        intendToModifyData();

        gotoPage("/secure/user/adduser.action");

        setTextField("email", "tester@test.com");
        setTextField("name", "newuser");
        setTextField("password", "password");
        setTextField("passwordConfirm", "password");
        setTextField("firstname", "New");
        setTextField("lastname", "User");
        checkCheckbox("active");

        submit();

        assertTextFieldEquals("email", "tester@test.com");
        assertTextFieldEquals("name", "newuser");
        assertTextFieldEquals("firstname", "New");
        assertTextFieldEquals("lastname", "User");
        assertCheckboxSelected("active");
    }
}
