package com.atlassian.crowd.acceptance.harness;

import com.atlassian.crowd.acceptance.tests.directory.DbCachingLoadAndOperateTest;
import com.atlassian.crowd.acceptance.tests.directory.DbCachingLoadMutationTest;
import com.atlassian.crowd.acceptance.tests.directory.DbCachingLoadTest;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Load testing the DB sync operation with external LDAP servers.
 */
public class DbCachingLoadTestHarness extends TestCase
{
    public static Test suite()
    {
        TestSuite suite = new TestSuite();
        suite.addTestSuite(DbCachingLoadTest.class);
        suite.addTestSuite(DbCachingLoadMutationTest.class);
        suite.addTestSuite(DbCachingLoadAndOperateTest.class);

        return suite;
    }
}