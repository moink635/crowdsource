package com.atlassian.crowd.acceptance.tests.soap;

import java.io.File;
import java.io.IOException;

import com.atlassian.crowd.acceptance.tests.applications.crowd.CrowdAcceptanceTestCase;

import org.apache.commons.io.FileUtils;

public class InformationLeakingTestBase extends CrowdAcceptanceTestCase
{
    /* A secret that shouldn't be exposed to anonymous requests */
    protected String secret = Long.toString(Double.doubleToLongBits(Math.random()));

    protected File createSecretFile() throws IOException
    {
        File f = File.createTempFile("secret-file", ".txt");
        FileUtils.writeStringToFile(f, secret);
        f.deleteOnExit();
        return f;
    }
}
