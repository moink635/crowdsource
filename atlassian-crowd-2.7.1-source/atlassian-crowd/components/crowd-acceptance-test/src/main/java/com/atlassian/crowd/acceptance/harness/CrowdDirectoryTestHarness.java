package com.atlassian.crowd.acceptance.harness;

import com.atlassian.crowd.acceptance.tests.applications.crowd.LdifLoaderForTesting;
import com.atlassian.crowd.acceptance.tests.directory.DbCachingLdapTest;
import com.atlassian.crowd.acceptance.tests.directory.LdapDirectoryMappersWithRequiredAttributesTest;
import com.atlassian.crowd.acceptance.tests.directory.LdapDirectoryWithEscapedDnsTest;
import com.atlassian.crowd.acceptance.tests.directory.RemoteCrowdDirectoryTest;
import com.atlassian.crowd.acceptance.tests.directory.suite.ApacheDS102Test;
import com.atlassian.crowd.acceptance.tests.directory.suite.ApacheDS154Test;
import com.atlassian.crowd.acceptance.tests.directory.suite.Rfc2307Test;

import junit.framework.JUnit4TestAdapter;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Tests related to external directory servers
 */
public class CrowdDirectoryTestHarness extends TestCase
{
    public static Test suite()
    {
        TestSuite suite = new TestSuite();
        suite.addTest(LdifLoaderForTesting.createRestoreTest(CrowdDirectoryTestHarness.class, "/com/atlassian/crowd/acceptance/tests/default-entries.ldif"));
        suite.addTest(ApacheDS102Test.suite());
        suite.addTest(ApacheDS154Test.suite());

        /* Restore again, because ApacheDS154Test repopulates LDAP */
        suite.addTest(LdifLoaderForTesting.createRestoreTest(CrowdDirectoryTestHarness.class, "/com/atlassian/crowd/acceptance/tests/default-entries.ldif"));
        suite.addTest(new JUnit4TestAdapter(DbCachingLdapTest.class));
        suite.addTestSuite(RemoteCrowdDirectoryTest.class);

//        suite.addTest(ActiveDirectory2K3Test.suite());    // we don't yet have an AD server that's guaranteed available for all builds.

        suite.addTestSuite(LdapDirectoryMappersWithRequiredAttributesTest.class);

        suite.addTestSuite(LdapDirectoryWithEscapedDnsTest.class);

        suite.addTest(Rfc2307Test.suite());

        return suite;
    }
}
