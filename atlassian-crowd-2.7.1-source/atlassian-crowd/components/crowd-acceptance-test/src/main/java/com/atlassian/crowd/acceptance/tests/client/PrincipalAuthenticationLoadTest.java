package com.atlassian.crowd.acceptance.tests.client;

import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class AuthenticationWorker implements Runnable
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final SecurityServerClient securityServerClient;
    private final UserAuthenticationContext userAuthenticationContext;
    private final int iterations;
    private int failures;

    AuthenticationWorker(final SecurityServerClient securityServerClient, final UserAuthenticationContext userAuthenticationContext, final int iterations)
    {
        this.securityServerClient = securityServerClient;
        this.userAuthenticationContext = userAuthenticationContext;
        this.iterations = iterations;
        this.failures = 0;
    }

    public void run()
    {
        for (int i = 0; i < iterations; i++)
        {
            try
            {
                logger.info("Authenticating: " + i);

                String token = securityServerClient.authenticatePrincipal(userAuthenticationContext);

                if (token == null)
                {
                    failures++;
                }
            }
            catch (Exception e)
            {
                logger.warn("Failed to authenticate", e);
                failures++;
            }
        }
    }

    public boolean hasFailures()
    {
        return failures > 0;
    }

    public int getFailures()
    {
        return failures;
    }
}
