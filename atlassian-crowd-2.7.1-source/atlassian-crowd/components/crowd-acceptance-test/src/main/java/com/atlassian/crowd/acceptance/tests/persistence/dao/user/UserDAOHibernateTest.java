package com.atlassian.crowd.acceptance.tests.persistence.dao.user;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.InternalUserWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import org.hamcrest.Matchers;
import org.hibernate.proxy.HibernateProxy;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static com.atlassian.crowd.model.user.Users.namesOf;
import static com.atlassian.crowd.test.matchers.CrowdMatchers.userNamed;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * This class tests UserDAOHibernate beyond the implementation of the {@link UserDao} interface. This includes tests for
 * methods with a more concrete return type (e.g., {@link InternalUser} for the return of {@link
 * UserDAOHibernate#findByName(long, String)}, and also methods that do not exist in the interface, such as {@link
 * UserDAOHibernate#removeAll(long)}. Therefore, the tests in this class are implementation specific, and cannot be run
 * against other implementations of the UserDao interface. The reason to have tests for the implementation (as opposed
 * to just testing the interface) is that some legacy code is coupled to the Hibernate implementation of the interface.
 *
 * @see UserDaoCRUDTest
 * @see UserDaoSearchTest
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class UserDAOHibernateTest
{
    @Inject private UserDAOHibernate userDAO;
    @Inject private DataSource dataSource;
    @Inject private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    private static final long DIRECTORY_ID = 1L;
    private static final String EXISTING_USER_1 = "admin";
    private static final String EXISTING_USER_2 = "bob";
    private static final String EXISTING_USER_3 = "jane";
    private static final String NON_EXISTING_USER = "newguy";
    private static final String NON_EXISTING_USER_MIXED_CASE = "NewGuy";

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Before
    public void fixHiLo()
    {
        PersistenceTestHelper.fixHiLo(resetableHiLoGeneratorHelper);
    }

    @Test
    public void testFindByNameReturnsUserWithLazyLoadedDirectory() throws Exception
    {
        InternalUser user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);

        // lazy-load check
        assertTrue(((HibernateProxy) user.getDirectory()).getHibernateLazyInitializer().isUninitialized());
    }

    @Test
    public void testFindByMixedCaseNameExactMatch() throws Exception
    {
        InternalUser user = userDAO.findByName(2L, "JohnSmith");

        assertEquals(toLowerCase("JohnSmith"), user.getLowerName());
    }

    @Test
    public void testFindByMixedCaseNameCaseInsensitiveMatch() throws Exception
    {
        InternalUser user = userDAO.findByName(2L, "johnsmith");

        assertEquals(toLowerCase("JohnSmith"), user.getLowerName());
    }

    @Test
    public void testExternalIdIsSet() throws Exception
    {
        InternalUser user = userDAO.findByName(2L, "johnsmith");

        assertEquals(toLowerCase("JohnSmith"), user.getLowerName());
        assertThat(user.getExternalId(), equalTo("external-id-1"));
    }

    @Test
    public void testFindByExternalId() throws Exception
    {
        InternalUser user = userDAO.findByExternalId(2L, "external-id-1");

        assertEquals("JohnSmith", user.getName());
        assertThat(user.getExternalId(), equalTo("external-id-1"));
    }

    @Test
    public void testFindByExternalIdIsCaseSensitive() throws Exception
    {
        try
        {
            userDAO.findByExternalId(2L, "External-Id-1");
            fail("UserNotFoundException should be thrown");
        }
        catch (UserNotFoundException e)
        {
            assertThat(e.getMessage(), equalTo("User <externalId=External-Id-1> does not exist"));
        }
    }

    @Test
    public void testFindByExternalIdAcceptsNullExternalId() throws Exception
    {
        try
        {
            userDAO.findByExternalId(2L, null);
            fail("NullPointerException should be thrown");
        }
        catch (NullPointerException e)
        {
            assertThat(e.getMessage(), equalTo("externalId"));
        }
    }

    @Test
    public void testPasswordCanBeChangedViaUpdate() throws Exception
    {
        InternalUser user = userDAO.findByName(DIRECTORY_ID, EXISTING_USER_1);

        user.updateCredentialTo(new PasswordCredential("password", true), 10);

        userDAO.update(user);

        assertTrue(user.getCredentialRecords().size() > 0);
    }

    @Test
    public void testRenamedUserHasLowercaseName() throws Exception
    {
        User user = userDAO.findByName(2L, "JohnSmith");
        userDAO.rename(user, "BobSmith");

        InternalUser renamedUser = userDAO.findByName(2L, "BobSmith");
        assertEquals("bobsmith", renamedUser.getLowerName());
    }

    @Test
    public void testRemoveAll()
    {
        userDAO.removeAll(DIRECTORY_ID);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

        assertTrue(userDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).returningAtMost(10)).isEmpty());
        assertThat(jdbcTemplate.queryForObject(
            "SELECT COUNT(*) FROM cwd_membership WHERE membership_type = 'GROUP_USER' AND directory_id = ?",
            Integer.class, DIRECTORY_ID), is(0));
        assertThat(jdbcTemplate.queryForObject("SELECT COUNT(*) FROM cwd_user_credential_record", Integer.class), is(0)); // this assumes only DIRECTORY_ID has user credentials and no other directory
    }

    @Test
    public void testRemoveAllUsers()
    {
        userDAO.removeAllUsers(DIRECTORY_ID, ImmutableSet.of(EXISTING_USER_1, EXISTING_USER_3));

        List<User> users = userDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).returningAtMost(10));

        assertThat(users, contains(userNamed(EXISTING_USER_2)));
    }

    @Test
    public void testAddAllUsers()
    {
        userDAO.addAll(ImmutableSet.of(
                new UserTemplateWithCredentialAndAttributes("timothy", DIRECTORY_ID, new PasswordCredential("", true)),
                new UserTemplateWithCredentialAndAttributes("obadiah", DIRECTORY_ID, new PasswordCredential("", true))));

        List<User> users = userDAO.search(DIRECTORY_ID, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).returningAtMost(10));

        assertThat(users, Matchers.<User>hasItem(userNamed("timothy")));
        assertThat(users, Matchers.<User>hasItem(userNamed("obadiah")));
    }

    @Test
    public void testStoreAttributesWithInsertForNewUser() throws Exception
    {
        // add user
        User createdUser = createNewUser(NON_EXISTING_USER_MIXED_CASE);

        // add attributes
        Map<String, Set<String>> attributes = new HashMap<String, Set<String>>();
        attributes.put("color", Sets.newHashSet("Blue", "Green"));
        attributes.put("phone", Sets.newHashSet("131111"));
        userDAO.storeAttributes(createdUser, attributes);

        // retrieve and verify
        InternalUserWithAttributes returnedUser = userDAO.findByNameWithAttributes(DIRECTORY_ID, NON_EXISTING_USER_MIXED_CASE);
        assertEquals(NON_EXISTING_USER_MIXED_CASE, returnedUser.getInternalUser().getName());
        assertEquals(NON_EXISTING_USER, returnedUser.getInternalUser().getLowerName());
        assertEquals(2, returnedUser.getKeys().size());
        assertEquals(2, returnedUser.getValues("color").size());
        assertTrue(returnedUser.getValues("color").contains("Blue"));
        assertTrue(returnedUser.getValues("color").contains("Green"));
        assertEquals(1, returnedUser.getValues("phone").size());
        assertTrue(returnedUser.getValues("phone").contains("131111"));

        // test lower case values were stored
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        SqlRowSet sqlRowSet = jdbcTemplate.queryForRowSet(
                "SELECT attribute_lower_value FROM cwd_user_attribute WHERE user_id = " + returnedUser.getInternalUser()
                        .getId());

        Set<String> lowerCaseAttributeValues = new HashSet<String>();
        while (sqlRowSet.next())
        {
            lowerCaseAttributeValues.add(sqlRowSet.getString("attribute_lower_value"));
        }

        assertEquals(3, lowerCaseAttributeValues.size());
        assertTrue(lowerCaseAttributeValues.contains(toLowerCase("Blue")));
        assertTrue(lowerCaseAttributeValues.contains(toLowerCase("Green")));
        assertTrue(lowerCaseAttributeValues.contains(toLowerCase("131111")));
    }

    private User createNewUser(String username) throws Exception
    {
        UserTemplate user = new UserTemplate(username, DIRECTORY_ID);
        user.setActive(true);
        user.setFirstName("New");
        user.setLastName("Guy");
        user.setDisplayName("New Guy");
        user.setEmailAddress("nOOb@example.com");
        return userDAO.add(user, PasswordCredential.encrypted("secret"));
    }

    @Test
    public void testFindByNames()
    {
        List<User> users = new ArrayList<User>(userDAO.findByNames(DIRECTORY_ID, ImmutableSet.of(EXISTING_USER_1, "bogus", EXISTING_USER_3)));

        assertThat(namesOf(users), containsInAnyOrder(EXISTING_USER_1, EXISTING_USER_3));
    }
}
