package com.atlassian.crowd.acceptance.tests.applications.crowdid.server;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.atlassian.crowd.acceptance.tests.BaseUrlFromProperties;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.sun.jersey.api.uri.UriBuilderImpl;

import org.hamcrest.CoreMatchers;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.core.AnyOf;
import org.junit.Before;
import org.junit.Test;
import org.openid4java.association.Association;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.discovery.Discovery;
import org.openid4java.discovery.DiscoveryException;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.util.HttpCache;
import org.openid4java.util.HttpFetcher;
import org.openid4java.util.HttpResponse;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * Direct HTTP tests against the OpenID resources, as a complement to the full browser
 * tests in {@link com.atlassian.crowd.acceptance.tests.applications.crowdid.client.OpenIDAuthenticationTest}.
 */
public class OpenIDServerTestWithOpenID2
{
    private String baseUrl;

    @Before
    public void findBaseUrlForApplication()
    {
        baseUrl = BaseUrlFromProperties.withLocalTestProperties().baseUrlFor("crowdid");
    }

    private String getOpEndpoint()
    {
        return baseUrl + "/op";
    }

    @Test
    public void testOpenId2ResourceHasCorrectMediaType() throws Exception
    {
        HttpURLConnection connection = (HttpURLConnection) new URL(getOpEndpoint()).openConnection();

        try
        {
            assertEquals(200, connection.getResponseCode());

            MediaType mt = MediaType.valueOf(connection.getContentType());
            assertEquals("application/xrds+xml", mt.getType() + "/" + mt.getSubtype());

            assertThat("There should be no parameters, except for a possible redundant charset declaration",
                    mt.getParameters(),
                    AnyOf.anyOf(
                            CoreMatchers.is(Collections.<String, String>emptyMap()),
                            CoreMatchers.<Map<String, String>>is(ImmutableMap.of("charset", "UTF-8"))));
        }
        finally
        {
            connection.getInputStream().close();
        }
    }

    @Test
    public void testOpenId2ResourceIsDiscoverable() throws DiscoveryException
    {
        Discovery discovery = new Discovery();

        List<DiscoveryInformation> discoveries = discovery.discover(getOpEndpoint());

        assertThat(discoveries, IsCollectionWithSize.hasSize(1));

        DiscoveryInformation discovered = discoveries.get(0);

        assertEquals(getOpEndpoint(), discovered.getOPEndpoint().toString());
        assertNull(discovered.getClaimedIdentifier());
        assertEquals("http://specs.openid.net/auth/2.0/server", discovered.getVersion());
        assertTrue(discovered.isVersion2());
    }

    @Test
    public void ableToAssociateWithProvider() throws Exception
    {
        ConsumerManager manager = new ConsumerManager();

        List<DiscoveryInformation> discoveries = manager.discover(getOpEndpoint());

        DiscoveryInformation discovered = manager.associate(discoveries);

        assertTrue(discovered.isVersion2());

        Association assoc = manager.getAssociations().load(getOpEndpoint());

        assertNotNull(assoc);
        assertThat(assoc.getExpiry().getTime() - System.currentTimeMillis(), Matchers.greaterThan(TimeUnit.MINUTES.toMillis(10)));
        assertEquals("HMAC-SHA256", assoc.getType());
    }

    @Test
    public void authenticationRequestProvidesPromptForUserAuthentication() throws Exception
    {
        /* Associate as a Relying Party */
        ConsumerManager manager = new ConsumerManager();

        manager.associate(manager.discover(getOpEndpoint()));

        /* Attempt authentication as a User-Agent */
        HttpFetcher fetcher = new HttpCache();

        Builder<String, String> mb = ImmutableMap.<String, String>builder();

        mb.put("openid.ns", "http://specs.openid.net/auth/2.0");
        mb.put("openid.mode", "checkid_setup"); // checkid_immediate

        mb.put("openid.claimed_id", getOpEndpoint());
        mb.put("openid.identity",  "http://specs.openid.net/auth/2.0/identifier_select");

        mb.put("openid.assoc_handle",  manager.getAssociations().load(getOpEndpoint()).getHandle());

        mb.put("openid.return_to",  "http://localhost/return-to");

        HttpResponse resp = fetcher.post(getOpEndpoint(), mb.build());

        /* Assert that we are directed to approve the request */
        assertEquals(302, resp.getStatusCode());
        assertEquals(baseUrl + "/secure/interaction/allowauthentication!default.action",
                resp.getResponseHeader("Location").getValue());
    }

    @Test
    public void authenticationRequestFailsWhenRealmDoesNotMatchReturnToUrl() throws Exception
    {
        /* Associate as a Relying Party */
        ConsumerManager manager = new ConsumerManager();

        manager.associate(manager.discover(getOpEndpoint()));

        /* Attempt authentication as a User-Agent */
        HttpFetcher fetcher = new HttpCache();

        Builder<String, String> mb = ImmutableMap.<String, String>builder();

        mb.put("openid.ns", "http://specs.openid.net/auth/2.0");
        mb.put("openid.mode", "checkid_setup"); // checkid_immediate

        mb.put("openid.claimed_id", getOpEndpoint());
        mb.put("openid.identity",  "http://specs.openid.net/auth/2.0/identifier_select");

        mb.put("openid.assoc_handle",  manager.getAssociations().load(getOpEndpoint()).getHandle());

        mb.put("openid.return_to",  "http://example.test/return-to");
        mb.put("openid.realm",  "http://localhost/");

        HttpResponse resp = fetcher.post(getOpEndpoint(), mb.build());

        /* Assert that it's a bad request */
        assertEquals(400, resp.getStatusCode());
        assertThat(resp.getBody(), containsString("AuthRequest could not be reconstructed from RequestParameters"));
    }

    @Test
    public void authenticationRequestPassesWhenRealmIsProvidedAndMatchesReturnToUrl() throws Exception
    {
        /* Associate as a Relying Party */
        ConsumerManager manager = new ConsumerManager();

        manager.associate(manager.discover(getOpEndpoint()));

        /* Attempt authentication as a User-Agent */
        HttpFetcher fetcher = new HttpCache();

        Builder<String, String> mb = ImmutableMap.<String, String>builder();

        mb.put("openid.ns", "http://specs.openid.net/auth/2.0");
        mb.put("openid.mode", "checkid_setup"); // checkid_immediate

        mb.put("openid.claimed_id", getOpEndpoint());
        mb.put("openid.identity",  "http://specs.openid.net/auth/2.0/identifier_select");

        mb.put("openid.assoc_handle",  manager.getAssociations().load(getOpEndpoint()).getHandle());

        mb.put("openid.return_to",  "http://www.example.test/return-to");
        mb.put("openid.realm",  "http://*.example.test/");

        HttpResponse resp = fetcher.post(getOpEndpoint(), mb.build());

        /* Assert that we are directed to approve the request */
        assertEquals(302, resp.getStatusCode());
        assertEquals(baseUrl + "/secure/interaction/allowauthentication!default.action",
                resp.getResponseHeader("Location").getValue());
    }

    /**
     * The request from the user agent may be a POST or a GET, so test the GET case.
     */
    @Test
    public void authenticationRequestRedirectedAsGetProvidesPromptForUserAuthentication() throws Exception
    {
        /* Associate as a Relying Party */
        ConsumerManager manager = new ConsumerManager();

        manager.associate(manager.discover(getOpEndpoint()));

        Builder<String, String> mb = ImmutableMap.<String, String>builder();

        mb.put("openid.ns", "http://specs.openid.net/auth/2.0");
        mb.put("openid.mode", "checkid_setup"); // checkid_immediate

        mb.put("openid.claimed_id", getOpEndpoint());
        mb.put("openid.identity",  "http://specs.openid.net/auth/2.0/identifier_select");

        mb.put("openid.assoc_handle",  manager.getAssociations().load(getOpEndpoint()).getHandle());

        mb.put("openid.return_to",  "http://localhost/return-to");

        UriBuilder builder = UriBuilderImpl.fromUri(getOpEndpoint());

        for (Map.Entry<String, String> e : mb.build().entrySet())
        {
            builder.queryParam(e.getKey(), e.getValue());
        }

        URL url = builder.build().toURL();

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setInstanceFollowRedirects(false);

        /* Assert that we are directed to approve the request */
        assertEquals(302, conn.getResponseCode());
        assertEquals(baseUrl + "/secure/interaction/allowauthentication!default.action",
                conn.getHeaderField("Location"));
    }

    @Test
    public void userIdentityPageIsServedAsHtmlByDefault() throws IOException
    {
        String userIdentifier = baseUrl + "/users/admin";

        HttpURLConnection connection = (HttpURLConnection) new URL(userIdentifier).openConnection();

        try
        {
            assertEquals(200, connection.getResponseCode());

            MediaType mt = MediaType.valueOf(connection.getContentType());
            assertEquals("text/html", mt.getType() + "/" + mt.getSubtype());

            assertThat("There should be no parameters, except for a possible redundant charset declaration",
                    mt.getParameters(),
                    AnyOf.anyOf(
                            CoreMatchers.is(Collections.<String, String>emptyMap()),
                            CoreMatchers.<Map<String, String>>is(ImmutableMap.of("charset", "UTF-8"))));
        }
        finally
        {
            connection.getInputStream().close();
        }
    }

    @Test
    public void discoveryOfUserIdentifierFindsSignons() throws DiscoveryException
    {
        Discovery discovery = new Discovery();

        String userIdentifier = baseUrl + "/users/admin";

        List<DiscoveryInformation> discoveries = discovery.discover(userIdentifier);

        assertThat(discoveries, CoreMatchers.<DiscoveryInformation>hasItem(openIdWithVersionEndpointAndClaimedId(
                DiscoveryInformation.OPENID11,
                baseUrl + "/server.openid", userIdentifier)));

        assertThat(discoveries, CoreMatchers.<DiscoveryInformation>hasItem(openIdWithVersionEndpointAndClaimedId(
                DiscoveryInformation.OPENID2,
                getOpEndpoint(), userIdentifier)));
    }

    public static Matcher<DiscoveryInformation> openIdWithVersionEndpointAndClaimedId(final String version, final String endpoint, final String claimedId)
    {
        return new TypeSafeMatcher<DiscoveryInformation>(DiscoveryInformation.class)
        {
            @Override
            public void describeTo(Description desc)
            {
                desc.appendText("A version '" + version + "' identifier with endpoint '" + endpoint + "' and claimed ID '" + claimedId + "'");
            }

            @Override
            protected boolean matchesSafely(DiscoveryInformation info)
            {
                return info.getVersion().equals(version) && info.getOPEndpoint().toString().equals(endpoint) && info.getClaimedIdentifier().toString().equals(claimedId);
            }
        };
    }
}
