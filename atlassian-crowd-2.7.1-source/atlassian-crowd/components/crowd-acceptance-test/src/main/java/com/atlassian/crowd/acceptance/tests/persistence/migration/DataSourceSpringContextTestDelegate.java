package com.atlassian.crowd.acceptance.tests.persistence.migration;


import java.io.File;

import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;

import org.apache.commons.lang3.StringUtils;


public class DataSourceSpringContextTestDelegate
{
    public String getDbUrl()
    {
        final File resource = AcceptanceTestHelper.getResource("crowd15db.script");
        String crowddbLocation = StringUtils.removeEnd(resource.getPath(), ".script");
        return "jdbc:hsqldb:" + crowddbLocation;
    }
}
