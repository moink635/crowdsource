package com.atlassian.crowd.acceptance.tests.applications.crowd;

import com.atlassian.crowd.acceptance.utils.DbCachingTestHelper;

/**
 * Relevant structure of Apache DS 151

dc=example,dc=com
	ou=Users
		[users - 6 in total]
		uid=mplanck (Max Planck)
		uid=aeinstein (Albert Einstein)
		uid=nbohr (Neils Bohr)
		uid=mborn (Max Born)
		uid=wpauli (Wolfgang Pauli)
		uid=mcurie (Marie Curie)
		[groups - none]

	ou=sandbox
		[users - 4 in total]
		uid=mflinders (Matthew Flinders)
		uid=jcook (James Cook)
		uid=jstuart (John Stuart)
		uid=joxley (John Oxley)
		[groups - 2 in total]
		cn=ExplorersClub (members: mflinders, jcook, jstuart, joxley)
		cn=EliteExplorersClub (members: jstuart, joxley)

	ou=Groups
		[users - none]
		[groups - 3 in total]
		cn=superUsers (member: uid=admin,ou=system)
		cn=userAdmins (member: uid=admin,ou=system)
		cn=Misc (members: mplanck, aeinstein, mborn, mflinders, joxley)

	ou=test
		[users - none]
		[groups - none]

ou=system
    uid=admin


 Note: The user "uid=admin,ou=system" is not visible as it is actually on the same level as 'dc=example,dc=com'
 */
public class SynchroniseDirectoryTest extends CrowdAcceptanceTestCase
{
    private static final String CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING = "ApacheDS Caching Directory";

    private static final long MAX_SYNC_WAIT_TIME_MS = 60000L; // 1 minute

    private DbCachingTestHelper dbCachingTestHelper;

    public void setUp() throws Exception
    {
        super.setUp();
        dbCachingTestHelper = new DbCachingTestHelper(tester);
        setScriptingEnabled(true);
        restoreCrowdFromXML("viewdirectory.xml");
        intendToModifyData();
    }

    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        dbCachingTestHelper = null;
        super.tearDown();
    }

    public void testUserWithAttributesRemoved() throws InterruptedException
    {
        // Tests that users with attributes are removed successfully upon sync when the OU changes
        log("Running: testUserWithAttributesRemoved");

        //Go and sync the directory
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("directory.caching.sync.never.label");

        synchroniseDirectory();

        // now check the users can be found
        searchAllPrincipals();
        assertTableRowCountEquals("user-details", 7); // header + 6 users

        // Add the directory to the application (to allow auth of user from apacheds)
        gotoViewApplication("crowd");
        clickLink("application-directories");
        selectOption("unsubscribedDirectoriesID", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        clickButton("add-directory");
        assertTextInTable("directoriesTable", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        setWorkingForm("directoriesForm");
        selectOption("directory3-allowAll", "True"); // the apacheds directory is the last in the list. Allow all to auth
        submit();

        // Login as Max Born (this is to add some attributes)
        // Note: need to give him a password first since we can import passwords via ldif
        gotoViewPrincipal("Max Born", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        setTextField("password", "mypassword");
        setTextField("passwordConfirm", "mypassword");
        submit();
        _logout();
        _loginAsUser("Max Born", "mypassword");

        // log back in as admin to check
        _logout();
        _loginAdminUser();

        // Check user does actually have extra attributes
        gotoViewPrincipal("Max Born", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        clickLink("user-attributes-tab");
        assertTableRowCountEquals("attributesTable", 3); // 2 attributes + heading

        // Change OU!
        // Go back to the directory and change the config
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        // Connector Tab
        clickLinkWithExactText("Connector");
        setWorkingForm("connectordetails");
        setTextField("pollingIntervalInMin", "3600");
        setTextField("baseDN", "ou=sandbox,dc=example,dc=com"); // user does not belong here, so he should disappear
        submit();
        // Config Tab
        clickLinkWithExactText("Configuration");
        setWorkingForm("configuration_details");
        setTextField("userDNaddition", "");
        submit();

        // re-sync the directory
        synchroniseDirectory();

        // check the correct users are now displayed
        searchAllPrincipals();
        assertTableRowCountEquals("user-details", 5); // header + 4 users
        assertTextNotPresent("Max Born");
    }

    public void testSynchronisation() throws Exception
    {
        log("Running: testSynchronisation");

        // Search for users before sync
        searchAllPrincipals();
        // Table should be empty since sync should not have yet kicked in...
        // (we are racing with time here)
        assertTableRowCountEquals("user-details", 1); // just the header

        // Now let's go and sync the directory
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("directory.caching.sync.never.label");
        synchroniseDirectory();

        // now check the users can be found
        searchAllPrincipals();

        assertTableRowCountEquals("user-details", 7); // header + 6 users (cn=Users set for users)
    }

    public void testChangeDirectoryThenSync() throws Exception
    {
        log("Running: testChangeDirectoryThenSync");

        // First synchronise the directory
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("directory.caching.sync.never.label");
        synchroniseDirectory();

        // now check the users can be found
        searchAllPrincipals();
        // sync has completed so should be able to find the users
        assertTableRowCountEquals("user-details", 7); // header + 6 users

        // Go back to the directory and change the config
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        // Config Tab
        clickLinkWithExactText("Configuration");
        setWorkingForm("configuration_details");
        setTextField("userDNaddition", ""); // remove "users" restriction"
        submit();

        // re-sync the directory
        synchroniseDirectory();

        // now check the correct users can be found (based on new config)
        searchAllPrincipals();
        // sync has completed so should be able to find the users
        assertTableRowCountEquals("user-details", 11); // header + 10 users (we changed the config)
    }

    public void testChangedToEmptyOU() throws InterruptedException
    {
        log("Running: testChangedToEmptyOU");

        // First synchronise the directory
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("directory.caching.sync.never.label");
        synchroniseDirectory();

        // now check the users can be found
        searchAllPrincipals();
        // sync has completed so should be able to find the users
        assertTableRowCountEquals("user-details", 7); // header + 6 users

        // Go back to the directory and change the config
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        // Config Tab
        clickLinkWithExactText("Connector");
        setWorkingForm("connectordetails");
        setTextField("pollingIntervalInMin", "3600");
        setTextField("baseDN", "ou=test,dc=example,dc=com"); // ou=test is empty, no users
        submit();
        clickLinkWithExactText("Configuration");
        setWorkingForm("configuration_details");
        setTextField("userDNaddition", "");
        submit();

        // re-sync the directory
        synchroniseDirectory();

        // now check the correct users (ie no users) can be found (based on new config)
        searchAllPrincipals();
        // sync has completed so should be able to find the users
        assertTableRowCountEquals("user-details", 1); // header only (no users in OU)
    }

    public void testSynchroniseCheckGroups() throws InterruptedException
    {
        log("Running: testSynchroniseCheckGroups");

        // First synchronise the directory
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("directory.caching.sync.never.label");
        synchroniseDirectory();

        // now check the users can be found
        gotoBrowseGroups();
        setWorkingForm("browsegroups");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        submit();
        // sync has completed so should be able to find the groups
        assertTableRowCountEquals("group-details", 6); // header + 5 groups

        // Go back to the directory and change the config
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        // Config Tab
        clickLinkWithExactText("Connector");
        setWorkingForm("connectordetails");
        setTextField("pollingIntervalInMin", "3600");
        setTextField("baseDN", "ou=Users,dc=example,dc=com"); // ou=Users has no groups
        submit();
        clickLinkWithExactText("Configuration");
        setWorkingForm("configuration_details");
        setTextField("userDNaddition", "");
        submit();

        // re-sync the directory
        synchroniseDirectory();

        // now check the correct users (ie no users) can be found (based on new config)
        gotoBrowseGroups();
        setWorkingForm("browsegroups");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        submit();
        // sync has completed so should be able to find the users
        assertTableRowCountEquals("group-details", 1); // header only (no groups in OU)
    }

    public void testCheckMembershipsRemoved() throws InterruptedException
    {
        log("Running: testCheckMembershipsRemoved");

        // First update and synchronise the directory
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("directory.caching.sync.never.label");
        // Config Tab
        clickLinkWithExactText("Configuration");
        setWorkingForm("configuration_details");
        setTextField("userDNaddition", "");
        submit();
        // General tab
        clickLink("connector-general");
        synchroniseDirectory();

        // Check all groups are visible
        gotoBrowseGroups();
        setWorkingForm("browsegroups");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        submit();
        // sync has completed so should be able to find the groups
        assertTableRowCountEquals("group-details", 6); // header + 5 groups

        // Check for group memberships
        // aeinstein (Misc)
        searchAllPrincipals();
        clickLinkWithExactText("Albert Einstein");
        clickLink("user-groups-tab");
        assertTableRowCountEquals("groupsTable", 2); // header + 1 group
        assertTextInTable("groupsTable", "Misc");
        assertTextNotInTable("groupsTable", new String[]{"ExplorersClub", "EliteExplorersClub"});

        // joxley (Misc, ExplorersClub, EliteExplorersClub)
        searchAllPrincipals();
        clickLinkWithExactText("John Oxley");
        clickLink("user-groups-tab");
        assertTableRowCountEquals("groupsTable", 4); // header + 3 group
        assertTextInTable("groupsTable", new String[]{"Misc", "ExplorersClub", "EliteExplorersClub"});

        // jcook (ExplorersClub)
        searchAllPrincipals();
        clickLinkWithExactText("James Cook");
        clickLink("user-groups-tab");
        assertTableRowCountEquals("groupsTable", 2); // header + 1 group
        assertTextInTable("groupsTable", "ExplorersClub");
        assertTextNotInTable("groupsTable", "EliteExplorersClub");

        // Go back to the directory and change the config
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        // Config Tab
        clickLinkWithExactText("Connector");
        setWorkingForm("connectordetails");
        setTextField("pollingIntervalInMin", "3600");
        setTextField("baseDN", "dc=example,dc=com"); // make sure we are on top level
        submit();
        clickLinkWithExactText("Configuration");
        setWorkingForm("configuration_details");
        setTextField("groupDNaddition", "ou=sandbox");// restrict to sandbox only groups
        submit();

        // re-sync the directory
        synchroniseDirectory();

        // now check we only see sandbox groups
        gotoBrowseGroups();
        setWorkingForm("browsegroups");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        submit();
        // sync has completed so should be able to find the users
        assertTableRowCountEquals("group-details", 3); // header + 2 groups only

        // check memberships (users should now be members of less groups)
        // aeinstein (none - Misc is not in ou=sandbox)
        searchAllPrincipals();
        clickLinkWithExactText("Albert Einstein");
        clickLink("user-groups-tab");
        assertTableNotPresent("groupsTable"); // now no longer member of any groups

        // joxley (Explorers, EliteExplorers)
        searchAllPrincipals();
        clickLinkWithExactText("John Oxley");
        clickLink("user-groups-tab");
        assertTableRowCountEquals("groupsTable", 3); // header + 3 group
        assertTextInTable("groupsTable", new String[]{"ExplorersClub", "EliteExplorersClub"});
        assertTextNotInTable("groupsTable", "Misc");

        // jcook (Explorers)
        searchAllPrincipals();
        clickLinkWithExactText("James Cook");
        clickLink("user-groups-tab");
        assertTableRowCountEquals("groupsTable", 2); // header + 1 group
        assertTextInTable("groupsTable", "ExplorersClub");
        assertTextNotInTable("groupsTable", "EliteExplorersClub");
    }

    public void testCheckMembershipsAdded() throws InterruptedException
    {
        log("Running: testCheckMembershipsAdded");

        // First update and synchronise the directory
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("directory.caching.sync.never.label");
        // Config Tab
        clickLinkWithExactText("Configuration");
        setWorkingForm("configuration_details");
        setTextField("userDNaddition", "");
        setTextField("groupDNaddition", "ou=Groups");
        submit();
        // General tab
        clickLink("connector-general");
        synchroniseDirectory();

        // Check all groups are visible
        gotoBrowseGroups();
        setWorkingForm("browsegroups");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        submit();
        // sync has completed so should be able to find the groups
        assertTableRowCountEquals("group-details", 4); // header + 3 groups

        // Check for group memberships
        // aeinstein (Misc)
        searchAllPrincipals();
        clickLinkWithExactText("Albert Einstein");
        clickLink("user-groups-tab");
        assertTableRowCountEquals("groupsTable", 2); // header + 1 group
        assertTextInTable("groupsTable", "Misc");
        assertTextNotInTable("groupsTable", new String[]{"ExplorersClub", "EliteExplorersClub"});

        // joxley (Misc)
        searchAllPrincipals();
        clickLinkWithExactText("John Oxley");
        clickLink("user-groups-tab");
        assertTableRowCountEquals("groupsTable", 2); // header + 1 group
        assertTextInTable("groupsTable", "Misc");
        assertTextNotInTable("groupsTable", new String[]{"ExplorersClub", "EliteExplorersClub"});

        // jcook (none)
        searchAllPrincipals();
        clickLinkWithExactText("James Cook");
        clickLink("user-groups-tab");
        assertTableNotPresent("groupsTable"); // not in any groups with ou=Groups

        // Go back to the directory and change the config
        gotoBrowseDirectories();
        clickLinkWithExactText(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        assertKeyPresent("menu.viewdirectory.label", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        // Config Tab
        clickLinkWithExactText("Connector");
        setWorkingForm("connectordetails");
        setTextField("pollingIntervalInMin", "3600");
        setTextField("baseDN", "dc=example,dc=com"); // make sure we are on top level
        submit();
        clickLinkWithExactText("Configuration");
        setWorkingForm("configuration_details");
        setTextField("groupDNaddition", "");// lift the restriction on groups
        submit();

        // re-sync the directory
        synchroniseDirectory();

        // now check we only see all groups
        gotoBrowseGroups();
        setWorkingForm("browsegroups");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        submit();
        // sync has completed so should be able to find the users
        assertTableRowCountEquals("group-details", 6); // header + 5 groups only

        // Check for group memberships (users should be now in members of more groups)
        // aeinstein (Misc)
        searchAllPrincipals();
        clickLinkWithExactText("Albert Einstein");
        clickLink("user-groups-tab");
        assertTableRowCountEquals("groupsTable", 2); // header + 1 group
        assertTextInTable("groupsTable", "Misc");
        assertTextNotInTable("groupsTable", new String[]{"ExplorersClub", "EliteExplorersClub"});

        // joxley (Misc, ExplorersClub, EliteExplorersClub)
        searchAllPrincipals();
        clickLinkWithExactText("John Oxley");
        clickLink("user-groups-tab");
        assertTableRowCountEquals("groupsTable", 4); // header + 3 group
        assertTextInTable("groupsTable", new String[]{"Misc", "ExplorersClub", "EliteExplorersClub"});

        // jcook (ExplorersClub)
        searchAllPrincipals();
        clickLinkWithExactText("James Cook");
        clickLink("user-groups-tab");
        assertTableRowCountEquals("groupsTable", 2); // header + 1 group
        assertTextInTable("groupsTable", "ExplorersClub");
        assertTextNotInTable("groupsTable", "EliteExplorersClub");
    }

    private void searchAllPrincipals()
    {
        gotoBrowsePrincipals();
        setWorkingForm("searchusers");
        selectOption("directoryID", CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING);
        submit("submit-search");
    }

    private void synchroniseDirectory()
    {
        dbCachingTestHelper.synchroniseDirectory(CONNECTOR_DIRECTORY_NAME_APACHEDS_CACHING, MAX_SYNC_WAIT_TIME_MS);
    }
}

