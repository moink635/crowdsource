package com.atlassian.crowd.acceptance.tests.applications.demo;

public class UpdateUserTest extends DemoAcceptanceTestCase
{
    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("adduserdemotest.xml");
    }

    @Override
    public void tearDown() throws Exception
    {
        restoreBaseSetup();
        super.tearDown();
    }

    public void testSetUserInActiveAndActive()
    {
        intendToModifyData();

        gotoViewUser("tester");

        setWorkingForm("generalForm");

        uncheckCheckbox("active");

        submit();

        assertCheckboxNotSelected("active");

        setWorkingForm("generalForm");

        checkCheckbox("active");

        submit();

        assertCheckboxSelected("active");
    }

    public void testUpdateUserPassword()
    {
        intendToModifyData();

        gotoViewUser("tester");

        setWorkingForm("generalForm");

        setTextField("password", "testing");

        setTextField("passwordConfirm", "testing");

        submit();

        clickLink("logout-link");

        gotoPage("/console/login.action");

        setWorkingForm("login");

        setTextField("username", "tester");
        setTextField("password", "testing");

        submit();

        assertLinkPresent("logout-link");
    }
}
