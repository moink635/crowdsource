package com.atlassian.crowd.acceptance.tests.applications.crowd;

public class LicenseMaintenanceTest extends CrowdAcceptanceTestCase
{
    private static final String LICENSE_ACTION = "/console/license.action";
    private static final String REGULAR_USER_NAME = "regularuser";
    private static final String REGULAR_USER_PASSWORD = "regularuser";

    @Override
    public void setUp() throws Exception
    {
        super.setUp();

        restoreCrowdFromXML("restsetup.xml");
    }

    public void testShouldNotInstallUnparseableLicense()
    {
        gotoPage(LICENSE_ACTION);

        setTextField("username", ADMIN_USER);
        setTextField("password", ADMIN_PW);
        setTextField("key", "unparseable license");
        submit();

        assertKeyPresent("license.key.error.invalid");
    }

    public void testShouldNotInstallAnExpiredLicense()
    {
        gotoPage(LICENSE_ACTION);

        setTextField("username", ADMIN_USER);
        setTextField("password", ADMIN_PW);
        setTextField("key", EXPIRED_MAINTENANCE_KEY);
        submit();

        assertKeyPresent("license.key.error.maintenance.expired");
    }

    public void testShouldValidateCredentialsToUpdateLicense()
    {
        gotoPage(LICENSE_ACTION);

        setTextField("username", ADMIN_USER);
        setTextField("password", "badpassword");
        if (SetupCrowdTest.useDeveloperLicense())
        {
            setTextField("key", DEVELOPER_LICENSE_KEY);
        }
        else
        {
            setTextField("key", UNLIMITED_LICENSE_KEY);
        }
        submit();

        assertKeyPresent("login.failed.invalidcredentials");
    }

    public void testRegularUserShouldNotBeAbleToUpdateValidLicense()
    {
        gotoPage(LICENSE_ACTION);

        setTextField("username", REGULAR_USER_NAME);
        setTextField("password", REGULAR_USER_PASSWORD);
        if (SetupCrowdTest.useDeveloperLicense())
        {
            setTextField("key", DEVELOPER_LICENSE_KEY);
        }
        else
        {
            setTextField("key", UNLIMITED_LICENSE_KEY);
        }
        submit();

        assertKeyPresent("login.failed.invalidcredentials");
    }

    public void testInformationIsNotVisibleToAnonymousUser()
    {
        _logout();

        gotoPage(LICENSE_ACTION);
        assertKeyPresent("license.title");
        assertLicenseAndServerInformationNotPresent();
    }

    public void testInformationIsNotVisibleToRegularUser()
    {
        _loginAsUser(REGULAR_USER_NAME, REGULAR_USER_PASSWORD);

        gotoPage(LICENSE_ACTION);
        assertKeyPresent("license.title");
        assertLicenseAndServerInformationNotPresent();
    }

    public void testInformationIsVisibleToAdmin()
    {
        _loginAdminUser();

        gotoPage(LICENSE_ACTION);
        assertKeyPresent("license.title");
        assertLicenseAndServerInformationPresent();
    }

    private void assertLicenseAndServerInformationPresent()
    {
        assertKeyPresent("license.organization.label");
        assertKeyPresent("license.type.label");
        assertKeyPresent("license.sen.label");
        assertKeyPresent("license.supportperiod.label");
        assertKeyPresent("license.userlimit.label");
        assertKeyPresent("license.userresources.label");
        assertKeyPresent("systeminfo.serverid.label");
    }

    private void assertLicenseAndServerInformationNotPresent()
    {
        assertKeyNotPresent("license.organization.label");
        assertKeyNotPresent("license.type.label");
        assertKeyNotPresent("license.datepurchased.label");
        assertKeyNotPresent("license.sen.label");
        assertKeyNotPresent("license.supportperiod.label");
        assertKeyNotPresent("license.partner.label");
        assertKeyNotPresent("license.userlimit.label");
        assertKeyNotPresent("license.userresources.label");
        assertKeyNotPresent("systeminfo.serverid.label");
    }
}
