package com.atlassian.crowd.acceptance.tests;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * A singleton to keep track of the state of a test data. This can
 * prevent unnecessary restore. All methods are keyed by the
 * <code>baseUrl</code> so parallel instances of Crowd can be
 * tracked during a single test run.
 */
public class TestDataState
{
    public static final TestDataState INSTANCE = new TestDataState();

    private final Map<URL, String> restoredXml = new HashMap<URL, String>();

    /**
     * Report on whether an unmodified version of the data in
     * <code>xmlfilename</code> is available.
     * @return <code>true</code> if a test can rely on Crowd's state being from
     * <code>xmlfilename</code>
     */
    public synchronized boolean isRestoredXml(URL baseUrl, String xmlfilename)
    {
        return xmlfilename.equals(restoredXml.get(baseUrl));
    }

    /**
     * Declare that the data in <code>xmlfilename</code> has been restored.
     */
    public synchronized void setRestoredXml(URL baseUrl, String xmlfilename)
    {
        this.restoredXml.put(baseUrl, xmlfilename);
    }

    /**
     * Indicate that a test intends to modify the data in Crowd. After this
     * {@link #isRestoredXml(URL, String)} will report <code>false</code> until
     * another restore.
     */
    public synchronized void intendToModify(URL baseUrl)
    {
        this.restoredXml.remove(baseUrl);
    }
}
