package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.embedded.api.UserComparator;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.util.UserUtils;

public final class ImmutableUser implements User
{
    private final long directoryId;
    private final String name;
    private final String firstName;
    private final String lastName;
    private final String emailAddress;
    private final String displayName;
    private final boolean active = true;
    private final String externalId;

    public ImmutableUser(long directoryId, String name, String firstName, String lastName, String displayName, String emailAddress)
    {
        this(directoryId, name, firstName, lastName, displayName, emailAddress, null);
    }


    public ImmutableUser(long directoryId, String name, String firstName, String lastName, String displayName, String emailAddress, final String externalId)
    {
        this.directoryId = directoryId;
        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.emailAddress = emailAddress;
        this.displayName = displayName;
        this.externalId = externalId;
    }

    public long getDirectoryId()
    {
        return directoryId;
    }

    public String getName()
    {
        return name;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }

    public String getDisplayName()
    {
        return displayName;
    }

    public boolean isActive()
    {
        return active;
    }

    @Override
    public String getExternalId()
    {
        return externalId;
    }

    @Override
    public boolean equals(final Object o)
    {
        return UserComparator.equalsObject(this, o);
    }

    @Override
    public int hashCode()
    {
        return UserComparator.hashCode(this);
    }

    public int compareTo(com.atlassian.crowd.embedded.api.User other)
    {
        return UserComparator.compareTo(this, other);
    }

}
