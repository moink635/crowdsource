package com.atlassian.crowd.acceptance.tests;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.TypeSafeMatcher;

public class PatternMatcher extends TypeSafeMatcher<String>
{
    private final String pattern;

    public PatternMatcher(String pattern)
    {
        this.pattern = pattern;
    }

    @Override
    protected boolean matchesSafely(String item)
    {
        return item.matches(pattern);
    }

    public void describeTo(Description description)
    {
        description.appendText("a string matching ");
        description.appendText(this.toString());
    }

    @Factory
    public static PatternMatcher matchesPattern(String pattern)
    {
        return new PatternMatcher(pattern);
    }

    @Override
    public String toString()
    {
        return pattern.toString();
    }
}
