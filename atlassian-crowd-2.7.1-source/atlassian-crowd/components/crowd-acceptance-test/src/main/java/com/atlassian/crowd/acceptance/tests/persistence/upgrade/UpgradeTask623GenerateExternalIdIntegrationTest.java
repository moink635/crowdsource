package com.atlassian.crowd.acceptance.tests.persistence.upgrade;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.upgrade.tasks.UpgradeTask623GenerateExternalId;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
        "classpath:/applicationContext-config.xml",
        "classpath:/applicationContext-CrowdDAO.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
        DependencyInjectionTestExecutionListener.class})
@Transactional
public class UpgradeTask623GenerateExternalIdIntegrationTest
{
    private static final long INTERNAL_DIRECTORY = 1L;
    private static final long NON_INTERNAL_DIRECTORY = 2L;
    private static final String INTERNAL_USER = "bob";
    private static final String NON_INTERNAL_USER = "jane";

    @Inject private DirectoryDao directoryDao;
    @Inject private UserDao userDao;
    @Inject private DataSource dataSource;

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase(PersistenceTestHelper.TEST_DATA_XML, jdbcTemplate);
    }

    @Test
    public void testUpgrade() throws Exception
    {
        // preconditions. If the externalId is added to the fixture data, then this test is meaningless
        assertNull("Should not have an externalId yet",
                   userDao.findByName(INTERNAL_DIRECTORY, INTERNAL_USER).getExternalId());
        assertNull("Should not have an externalId yet",
                   userDao.findByName(NON_INTERNAL_DIRECTORY, NON_INTERNAL_USER).getExternalId());

        UpgradeTask623GenerateExternalId task = new UpgradeTask623GenerateExternalId(directoryDao, userDao);

        task.doUpgrade();

        assertNotNull("Should have a generated externalId for user in internal directory",
                      userDao.findByName(INTERNAL_DIRECTORY, INTERNAL_USER).getExternalId());
        assertNull("Should not update user in non-internal directory",
                   userDao.findByName(NON_INTERNAL_DIRECTORY, NON_INTERNAL_USER).getExternalId());
    }
}
