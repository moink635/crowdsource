package com.atlassian.crowd.acceptance.tests.applications.crowd;

/**
 * Tests to make sure Gzip filer on/off works.
 *
 * This tests checks the headers of the response when
 * the client has Accept-Encoding: gzip.
 */

public class GzipFilterOptionTest extends CrowdAcceptanceTestCase
{

    public void setUp() throws Exception
    {
        super.setUp();

        addRequestHeader("Accept-Encoding", "gzip");

        // Restart session so that request header will take effect
        closeBrowser();
        beginAt(URL_HOME);

        _loginAdminUser();
    }
    
    public void testGzipOptionOn()
    {
        log("Running testGzipOptionOn");

        gotoGeneral();
        checkCheckbox("gzip");

        submit();

        // first response will still contain the old setting so reload
        gotoGeneral();

        assertServerResponseContains("\nContent-Encoding: gzip\n");
//        assertKeyPresent("updatesuccessful.label");
        assertCheckboxSelected("gzip");
    }

    public void testGzipOptionOff()
    {
        log("Running testGzipOptionOff");

        gotoGeneral();
        uncheckCheckbox("gzip");

        submit();

        // first response will still contain the old setting so reload
        gotoGeneral();

        assertServerResponseDoesNotContain("\nContent-Encoding: gzip\n");
//        assertKeyPresent("updatesuccessful.label");
        assertCheckboxNotSelected("gzip");
    }

}
