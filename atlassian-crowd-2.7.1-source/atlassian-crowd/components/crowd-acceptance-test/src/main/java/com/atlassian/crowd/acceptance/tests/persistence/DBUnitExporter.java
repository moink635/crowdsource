package com.atlassian.crowd.acceptance.tests.persistence;

import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;

import java.io.FileOutputStream;
import java.sql.Connection;
import java.sql.DriverManager;

/**
 * A quick and dirty class that will export data to a file so we can have a base to run our DBUnit tests from
 */
public class DBUnitExporter
{

    public static void main(String[] args) throws Exception
    {
        // database connection
        Class driverClass = Class.forName("com.mysql.jdbc.Driver");
        Connection jdbcConnection = DriverManager.getConnection(
                "jdbc:mysql://localhost/crowddb?autoReconnect=true", "crowduser", "crowduser");
        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);

        // partial database export
        QueryDataSet partialDataSet = new QueryDataSet(connection);
        partialDataSet.addTable("APPLICATION", "SELECT * FROM APPLICATION");
        partialDataSet.addTable("APPLICATIONADDRESSES", "SELECT * FROM APPLICATIONADDRESSES");
        partialDataSet.addTable("APPLICATIONCREDENTIALS", "SELECT * FROM APPLICATIONCREDENTIALS");
        partialDataSet.addTable("APPLICATIONDIRECTORIES", "SELECT * FROM APPLICATIONDIRECTORIES");
        partialDataSet.addTable("APPLICATIONGROUPS", "SELECT * FROM APPLICATIONGROUPS");

        partialDataSet.addTable("ATTRIBUTES", "SELECT * FROM ATTRIBUTES");
        partialDataSet.addTable("ATTRIBUTEVALUES", "SELECT * FROM ATTRIBUTEVALUES");

        partialDataSet.addTable("DIRECTORY", "SELECT * FROM DIRECTORY");
        partialDataSet.addTable("DIRECTORYPERMISSIONS", "SELECT * FROM DIRECTORYPERMISSIONS");

        partialDataSet.addTable("PRINCIPALCREDENTIALHISTORY", "SELECT * FROM PRINCIPALCREDENTIALHISTORY");
        partialDataSet.addTable("REMOTEGROUP", "SELECT * FROM REMOTEGROUP");
        partialDataSet.addTable("REMOTEGROUPMEMBERS", "SELECT * FROM REMOTEGROUPMEMBERS");

        partialDataSet.addTable("REMOTEPRINCIPAL", "SELECT * FROM REMOTEPRINCIPAL");
        partialDataSet.addTable("REMOTEPRINCIPALCREDENTIALS", "SELECT * FROM REMOTEPRINCIPALCREDENTIALS");

        partialDataSet.addTable("REMOTEROLE", "SELECT * FROM REMOTEROLE");
        partialDataSet.addTable("REMOTEROLEMEMBERS", "SELECT * FROM REMOTEROLEMEMBERS");

        partialDataSet.addTable("SERVERPROPERTY", "SELECT * FROM SERVERPROPERTY");
        partialDataSet.addTable("TOKEN", "SELECT * FROM TOKEN");

        partialDataSet.addTable("APPLICATIONDIRECTORYPERMISSION", "SELECT * FROM APPLICATIONDIRECTORYPERMISSION");
        partialDataSet.addTable("hibernate_unique_key", "SELECT * FROM hibernate_unique_key");
        
        FlatXmlDataSet.write(partialDataSet, new FileOutputStream("partial.xml"));
    }
}
