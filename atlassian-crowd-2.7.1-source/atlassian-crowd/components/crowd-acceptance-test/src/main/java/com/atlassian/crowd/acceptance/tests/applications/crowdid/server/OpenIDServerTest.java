package com.atlassian.crowd.acceptance.tests.applications.crowdid.server;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.crowd.acceptance.utils.AcceptanceTestHelper;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.hamcrest.text.IsEqualIgnoringWhiteSpace;

import net.sourceforge.jwebunit.exception.TestingEngineResponseException;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

public class OpenIDServerTest extends CrowdIDServerAcceptanceTestCase
{
    protected static String OPENIDSERVER_LOGIN_URL_ADMIN;
    protected static String OPENID_CLIENT_LOGIN;
    protected static String OPENID_SERVER_URL;

    protected final static String CLIENT_NAME = "crowdidclient";


    @Override
    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);

        loginToCrowd();
        restoreCrowdFromXML("openidauthtest.xml");
        logoutFromCrowd();

        // load specific properties if defined
        specProperties = AcceptanceTestHelper.loadProperties("localtest.properties");

        // CrowdID Client details
        String client_port = getTestProperty(CLIENT_NAME + ".port");
        String client_context = getTestProperty(CLIENT_NAME + ".context");
        String client_rootUrl = "http://" + getTestProperty("host.location") + ":" + client_port;

        // CrowdID Server details
        String server_port = getTestProperty(getApplicationName() + ".port");
        String server_context = getTestProperty(getApplicationName() + ".context");
        String server_rootUrl = "http://" + getTestProperty("host.location") + ":" + server_port;

        OPENID_CLIENT_LOGIN = client_rootUrl + client_context;
        OPENID_SERVER_URL = server_rootUrl + server_context;
        OPENIDSERVER_LOGIN_URL_ADMIN = OPENID_SERVER_URL + "/users/" + CROWD_ADMIN_USER;
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testProfile()
    {
        log("Running testProfile");

        // Login via client
        gotoPage(OPENID_CLIENT_LOGIN);

        // login page (client)
        //assertAtClientLoginPage();
        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        submit();

        // login page (server)
        //assertAtServerLoginPage();
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();


        // create new profile
        clickLink("myprofiles");
        assertKeyPresent("profiles.select.title");

        selectOptionByValue("profileID","-1"); // -- Create New Profile -- option
        setTextField("profileName", ""); // Invalid - cannot be empty
        submit();
        assertKeyPresent("profiles.nameempty.error");

        setTextField("profileName", "My Profile"); // Invalid - profile "My Profile" already exists
        submit();
        assertKeyPresent("exception.profile.already.exists");

        setTextField("profileName", "Test Profile"); // Valid profile
        // Assert original values carried over and change profile values
        assertTextFieldEquals("nickname", "admin");
        setTextField("nickname", "tester");
        assertTextFieldEquals("fullName", "Super User");
        setTextField("fullName", "Test User");
        assertTextFieldEquals("email", "admin@example.com");
        setTextField("email", "test@example.com");

        // Set new attribute
        selectOption("dobDay", "1");
        selectOption("dobMonth", "April");
        selectOption("dobYear", "1990");
        submit();

        assertKeyPresent("profiles.updated.message");

        // Resume Approval Process
        clickLink("resumeauthentication");


        // Select the new 'Test Profile' from profile dropdown
//        clickElementByXPath("//select[@id='profileID']/option[1]"); //select element not in form, so use xpath
        selectOption("profileID", "Test Profile");

        // check table entries
        assertTextInTable("attributeTable", new String[] {"Nickname", "tester"});
        assertTextInTable("attributeTable", new String[] {"Full Name", "Test User"});
        assertTextInTable("attributeTable", new String[] {"Email", "test@example.com"});
        assertTextInTable("attributeTable", new String[] {"Birth Date", "1 April 1990"});


        // Delete profile (to return it back to 'original' profile state since restoring crowd does not restore crowdid)
        gotoPage(OPENID_SERVER_URL);
        clickLink("myprofiles");
        selectOption("profileID","Test Profile");
        clickButtonWithText("Delete");
        assertKeyPresent("profiles.deleted.message");
    }

    public void testApprovedSites()
    {
        log("Running testApprovedSites");

        // Login via client
        gotoPage(OPENID_CLIENT_LOGIN);

        // login page (client)
        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        submit();

        // login page (server)
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        // Go to Approved Sites
        clickLink("mysites");
        assertKeyPresent("allow.nosites.label");

        // Resume auth & choose 'allow always' to add approved site
        clickLink("resumeauthentication");
        clickLink("allowAlways");

        // Go through server to check approved sites
        gotoPage(OPENID_SERVER_URL);
        clickLink("mysites");
        assertKeyPresent("allow.siteurl.label");
        assertTextPresent(OPENID_CLIENT_LOGIN);
        clickElementByXPath("//img[@id='removeSite-0']"); // Remove the site just added
        assertTextNotPresent(OPENID_CLIENT_LOGIN);
        clickButtonWithText("Apply");
        assertKeyPresent("updatesuccessful.label");
        assertKeyPresent("allow.nosites.label");

    }

    public void testLoginHistory()
    {

        // Login via client
        gotoPage(OPENID_CLIENT_LOGIN);

        // login page (client)
        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        submit();

        // login page (server)
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        clickLink("allow"); // Allow Once
        clickLinkWithExactText("Log Out");

        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        submit();

        clickLink("deny"); // deny
        // automatically logged out

        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        submit();

        clickLink("allowAlways"); // Allow Always
        clickLinkWithExactText("Log Out");

        // Go through server to check history
        gotoPage(OPENID_SERVER_URL);
        clickLink("activity");
        assertKeyPresent("authaction.allow.label");
        assertKeyPresent("authaction.deny.label");
        assertKeyPresent("authaction.allowalways.label");

        // remove site from "allow always" (to prevent breaking other tests)
        clickLink("mysites");
        assertKeyPresent("allow.siteurl.label");
        assertTextPresent(OPENID_CLIENT_LOGIN);
        //clickLinkWithImage("delete.png");
        clickElementByXPath("//img[@id='removeSite-0']");
        assertTextNotPresent(OPENID_CLIENT_LOGIN);
        clickButtonWithText("Apply");
        assertKeyPresent("updatesuccessful.label");
        assertKeyPresent("allow.nosites.label");

    }

    public void testAdminTrustRelationships()
    {
         // Go through server to check history
        gotoPage(OPENID_SERVER_URL);
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        clickLinkWithExactText("Administration");
        clickLink("loginrestrictions");

        assertKeyPresent("trusts.label");

        // Test Blacklist first
        clickRadioOption("trustType", "2"); //blacklist
        assertKeyPresent("blacklisthost.label");

        String otherHostname = "127.0.0.2";

        setTextField("address", "localhost");
        submit();
        setTextField("address", otherHostname);
        submit();

        assertTextInTable("addressesTable", new String[] {"localhost", "Remove"});
        assertTextInTable("addressesTable", new String[] {otherHostname, "Remove"});

        clickLink("remove-1"); // remove 'otherHostname'

        assertTextInTable("addressesTable", new String[] {"localhost", "Remove"});
        assertTextNotInTable("addressesTable", otherHostname);

        // Try to login via client - should be denied access
        gotoPage(OPENID_CLIENT_LOGIN);
        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        submit();


        assertKeyPresent("allow.error.title");

        // Test Whitelist
        clickLinkWithExactText("Administration");
        clickLink("loginrestrictions");

        assertKeyPresent("trusts.label");

        clickRadioOption("trustType", "1"); //whitelist
        assertKeyPresent("whitelisthost.label");

        // address should be already in table
        assertTextInTable("addressesTable", new String[] {"localhost", "Remove"});

        // Try to login via client - should be allowed
        gotoPage(OPENID_CLIENT_LOGIN);
        setTextField("openid_identifier", OPENIDSERVER_LOGIN_URL_ADMIN);
        submit();

        assertKeyPresent("allow.auth.title");

        // all good - remove trust relationships
        clickLinkWithExactText("Administration");
        clickLink("loginrestrictions");
        clickLink("remove-0");
        clickRadioOption("trustType", "0"); // None
    }

    public void testInternalOpenIDProfilePageShowsURLInEncodedFormat()
    {
        gotoPage(OPENID_SERVER_URL);
        setTextField("username", "john.tøstinógé");
        setTextField("password", "john");
        submit();

        clickLinkWithText("My OpenID");
        String expectedUri = OPENID_SERVER_URL + "/users/" + "john.t%C3%B8stin%C3%B3g%C3%A9";
        assertThat(getElementTextByXPath("//div[@class='identity-bar']"),
                   IsEqualIgnoringWhiteSpace.equalToIgnoringWhiteSpace(expectedUri));
    }

    public void testViewPublicProfilePageWithEncodedCharactersShowsOpenIdUrlInEncodedFormat()
    {
        // Most modern browsers should encode the uri to ascii when we request the uri OPENID_SERVER_URL + "/users/" + "john.tøstinógé")
        // but the java client gotoPage here makes request without encoding it correctly.The tomcat configuration will accept URI's of
        // this format too.
        gotoPage(OPENID_SERVER_URL + "/users/" + "john.t%C3%B8stin%C3%B3g%C3%A9");

        String expectedUri = OPENID_SERVER_URL + "/users/" + "john.t%C3%B8stin%C3%B3g%C3%A9";
        assertThat(getElementTextByXPath("//div[@class='identity-bar']"),
                   IsEqualIgnoringWhiteSpace.equalToIgnoringWhiteSpace(expectedUri));
    }

    public void testEditProfilesXsrfProtectionRejectsActionWithNoXsrfToken()
    {
        gotoPage(OPENID_SERVER_URL);
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        tester.setIgnoreFailingStatusCodes(true);

        try
        {
            gotoPage(OPENID_SERVER_URL + "/secure/profile/editprofiles!update.action");

            assertTrue(getPageSource().contains("warningBox"));
            assertTrue(getPageSource().contains(getMessage("atlassian.xwork.xsrf.notoken")));
        }
        finally
        {
            tester.setIgnoreFailingStatusCodes(false);
        }
    }

    public void testEditProfilesSetDefaultProfileForUserIsCsrfProtected()
    {
        gotoPage(OPENID_SERVER_URL);
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        tester.setIgnoreFailingStatusCodes(true);

        try
        {
            gotoPage(OPENID_SERVER_URL + "/secure/profile/editprofiles!makeDefault.action");

            assertTrue(getPageSource().contains("warningBox"));
            assertTrue(getPageSource().contains(getMessage("atlassian.xwork.xsrf.notoken")));
        }
        finally
        {
            tester.setIgnoreFailingStatusCodes(false);
        }
    }

    public void testEditProfilesCanSetNewDefaultProfileForUser()
    {
        gotoPage(OPENID_SERVER_URL);
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        // navigate to new profile page
        gotoPage(OPENID_SERVER_URL + "/secure/profile/editprofiles.action");
        selectOption("profileID", "-- Create New Profile --");

        // create a new profile
        setTextField("profileName", "Second Profile");
        submit();

        // make it the default for the admin user
        clickLinkWithExactText("Make Default");

        assertTextPresent("Second Profile (default)");
    }

    public void testRejectedAuthenticationRepeatsCorrectlyEncodedUsername()
    {
        gotoPage(OPENID_SERVER_URL);
        setTextField("username", "john.tøstinógé");
        setTextField("password", "");
        submit();

        assertWarningPresent();
        assertTextFieldEquals("username", "john.tøstinógé");
        assertTextFieldEquals("password", "");
    }

    public void testFooterShowsVersion()
    {
        gotoPage(OPENID_SERVER_URL);

        String poweredBy = getElementTextByXPath("id('footer')/p");
        assertThat(poweredBy, startsWith("Powered by Atlassian CrowdID Version:"));

        String version = poweredBy.replaceFirst(".*?:\\s+", "");
        assertThat(version, not(isEmptyString()));
    }

    public void testEditProfilesPageCorrectlyEscapesProfileNameToProtectAgainstPersistedXSS()
    {
        gotoPage(OPENID_SERVER_URL);
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        gotoPage(OPENID_SERVER_URL + "/secure/profile/editprofiles.action");

        selectOption("profileID", "-- Create New Profile --");

        setTextField("profileName", "profileNameDoubleEscape&amp;XssTest<script>alert(1)</script>");
        submit();

        assertThat(getPageSource(),
                containsString("profileNameDoubleEscape&amp;amp;XssTest&lt;script&gt;alert(1)&lt;/script&gt;"));
    }

    /**
     * Regression test for CWD-3428
     */
    public void testEditProfilesPageIsNotVulnerableToReflectedXSS()
    {
        gotoPage(OPENID_SERVER_URL);
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        gotoPage(OPENID_SERVER_URL + "/secure/profile/editprofiles.action"
                + "?warning=escapeWarning<script>alert(1);</script>"
                + "&description=escapeDescription<script>alert(2);</script>");

        String pageSource = getPageSource();
        assertThat(pageSource, not(containsString("escapeWarning<script>alert(1);</script>")));
        assertThat(pageSource, not(containsString("escapeDescription<script>alert(2);</script>")));
    }

    public void testIncludeFilesAreNotDirectlyAccessible()
    {
        try
        {
            gotoPage(OPENID_SERVER_URL + "/include/nonExistentFile.jsp");
            fail("/include/* files should be inaccessible before login");
        }
        catch (TestingEngineResponseException e)
        {
            assertThat(e.getMessage(), containsString("unexpected status code [403]"));
        }

        gotoPage(OPENID_SERVER_URL);
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        try
        {
            gotoPage(OPENID_SERVER_URL + "/include/nonExistentFile.jsp");
            fail("/include/* files should still be inaccessible after login");
        }
        catch (TestingEngineResponseException e)
        {
            assertThat(e.getMessage(), containsString("unexpected status code [403]"));
        }
    }

    private long getProfileIdForAdmin()
    {
        gotoPage(OPENID_SERVER_URL);
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        /* Log in as the admin and find the profile ID */
        clickLink("myprofiles");
        assertKeyPresent("profiles.select.title");

//        HtmlSelect select = (HtmlSelect) ((HtmlUnitElementImpl) getElementById("profileID")).getHtmlElement();
//        HtmlOption option = Iterables.getOnlyElement(select.getSelectedOptions());
//
//        String profileIdStr = option.getValueAttribute();

        Matcher m = Pattern.compile("\\?profileID=(\\d+)(?!\\d)").matcher(getPageSource());
        assertTrue(m.find());
        String profileIdStr = m.group(1);

        gotoPage(OPENID_SERVER_URL + "/logoff.action");
        assertKeyPresent("login.title");

        return Long.parseLong(profileIdStr);
    }

    public void testUnableToViewProfileForAnotherUser()
    {
        long profileId = getProfileIdForAdmin();

        /* Log in as another user */
        gotoPage(OPENID_SERVER_URL);
        setTextField("username", "john.tøstinógé");
        setTextField("password", "john");
        submit();

        gotoPage(OPENID_SERVER_URL + "/secure/profile/editprofiles.action?profileID=" + profileId);
        assertKeyPresent("exception.profile.access.violation.exception");
    }

    public void testUnableToEditProfileForAnotherUser()
    {
        long profileId = getProfileIdForAdmin();

        /* Log in as another user */
        gotoPage(OPENID_SERVER_URL);
        setTextField("username", "john.tøstinógé");
        setTextField("password", "john");
        submit();

        gotoPage(OPENID_SERVER_URL + "/secure/profile/editprofiles.action");

//        String token = getElementTextByXPath("//input[@id='atl_token']/@value");

        Matcher m = Pattern.compile("atl_token=(.*)'").matcher(getPageSource());
        assertTrue(m.find());
        String token = m.group(1);

        gotoPage(OPENID_SERVER_URL + "/secure/profile/editprofiles!update.action?profileID=" + profileId +
                "&nickname=Modified" + "&atl_token=" + token);

        assertKeyPresent("exception.profile.access.violation.exception");
        assertKeyNotPresent("profiles.updated.message");

        gotoPage(OPENID_SERVER_URL + "/logoff.action");
        assertKeyPresent("login.title");

        /* Log in as the admin and check nothing has changed */
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        clickLink("myprofiles");
        assertKeyPresent("profiles.select.title");

        assertTextInElement("nickname", "admin");
    }

    public void testLoginPageHasCsrfProtection() throws Exception
    {
        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(OPENID_SERVER_URL + "/login!update.action");
        try
        {
            HttpResponse response = httpClient.execute(httpPost);
            assertThat(response.getStatusLine().getStatusCode(), equalTo(403));
        }
        finally
        {
            httpPost.releaseConnection();
        }
    }

    public void testAboutPageAccessible()
    {
        gotoPage(OPENID_SERVER_URL + "/about.jsp");
        assertKeyPresent("about.title");
    }
}
