package com.atlassian.crowd.acceptance.tests.persistence.dao.token;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.atlassian.crowd.dao.token.TokenDAO;
import com.atlassian.crowd.exception.ObjectAlreadyExistsException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.TokenTermKeys;
import com.atlassian.crowd.util.persistence.PersistenceException;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.hamcrest.CoreMatchers;
import org.hamcrest.collection.IsEmptyCollection;
import org.hamcrest.number.OrderingComparison;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Base class for TokenDAO tests. It contains the tests that are shared by the implementations of TokenDAO.
 *
 * Note the @Transactional annotation at the class level. This is necessary because the annotation must be
 * in the same class as the @Test annotations, or it won't take effect.
 */
@Transactional
public abstract class TokenDAOTester
{
    protected static final String TOKEN_KEY = "oFpXcZ2lfvtc0YQBgE1EeA00";
    protected static final long DIRECTORY_ID = 1L;
    protected static final long RANDOM_NUMBER = 123456;
    protected static final Long TOKEN_ID = 491522L;
    protected static final String TOKEN1_ID_HASH = "blah";
    protected static final String TOKEN2_ID_HASH = "bleh";
    protected static final String TOKEN3_ID_HASH = "bloh";

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    private static void waitAtLeastAMillisecond() throws InterruptedException
    {
        long until = System.currentTimeMillis() + 1;

        while (until > System.currentTimeMillis())
        {
            Thread.sleep(1);
        }
    }

    @Test
    public void testAdd() throws Exception
    {
        Token token = new Token.Builder(DIRECTORY_ID, "admin", "hash", RANDOM_NUMBER, TOKEN_KEY).create();

        Token returnedToken = getTokenDAO().add(token);
        assertNotNull(returnedToken);
    }

    @Test (expected = ObjectAlreadyExistsException.class)
    public void testAddWithDuplicateIdentifierHash() throws Exception
    {
        Token token = new Token.Builder(DIRECTORY_ID, "admin", TOKEN1_ID_HASH, RANDOM_NUMBER, TOKEN_KEY).create();

        getTokenDAO().add(token);
    }

    @Test
    public void testUpdate() throws Exception
    {
        Token token = getTokenDAO().findByRandomHash(TOKEN_KEY);
        long originalTime = token.getLastAccessedTime();

        waitAtLeastAMillisecond();
        Token returnedToken = getTokenDAO().update(token);
        assertNotNull(returnedToken);
        final long nextMillisecondFromNow = System.currentTimeMillis() + 1;

        assertThat(returnedToken.getLastAccessedTime(), OrderingComparison.lessThan(nextMillisecondFromNow));
        assertThat(returnedToken.getLastAccessedTime(), OrderingComparison.greaterThan(originalTime));
    }

    @Test
    public void testRemove() throws Exception
    {
        Token token = null;
        try
        {
            token = getTokenDAO().findByRandomHash(TOKEN_KEY);
        }
        catch (ObjectNotFoundException e)
        {
            // we don't care at this point
        }

        // Now remove the token
        getTokenDAO().remove(token);

        // The token should not exist anymore
        expectedException.expect(ObjectNotFoundException.class);
        getTokenDAO().findByRandomHash(TOKEN_KEY);
    }

    @Test
    public void testRemoveByName()
    {
        getTokenDAO().remove(DIRECTORY_ID, "admin");

        List<Token> list = getTokenDAO().search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).with(Restriction.on(TokenTermKeys.NAME).exactlyMatching("admin")).returningAtMost(10));

        assertThat(list, IsEmptyCollection.<Token>empty());
    }

    @Test
    public void testSearch() throws PersistenceException
    {
        List<Token> tokens = getTokenDAO().search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).returningAtMost(10));
        assertNotNull(tokens);
        assertThat(tokens, CoreMatchers.not(IsEmptyCollection.<Token>empty()));
    }

    private static Collection<String> principalNames(Iterable<? extends Token> tokens)
    {
        ArrayList<String> names = new ArrayList<String>();

        for (Token t : tokens)
        {
            names.add(t.getName());
        }

        return names;
    }

    @Test
    public void testSearchForPrincipalTokensByDirectoryId() throws Exception
    {
        // Admin and user
        List<Token> list = getTokenDAO().search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
                                                    .with(Restriction.on(TokenTermKeys.DIRECTORY_ID)
                                                              .exactlyMatching(DIRECTORY_ID))
                                                    .returningAtMost(10));

        assertThat(list, containsInAnyOrder(getToken1(), getToken2()));
        assertThat(principalNames(list), containsInAnyOrder("admin", "user"));
    }

    @Test
    public void testSearchForApplicationTokens() throws Exception
    {
        // Crowd
        List<Token> list = getTokenDAO().search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
                                                    .with(Restriction.on(TokenTermKeys.DIRECTORY_ID)
                                                              .exactlyMatching(Token.APPLICATION_TOKEN_DIRECTORY_ID))
                                                    .returningAtMost(10));

        assertEquals(ImmutableList.of(getToken3()), list);
        assertEquals("crowd", Iterables.getOnlyElement(list).getName());
    }

    @Test
    public void testSearchForPrincipalTokenByName() throws Exception
    {
        EntityQuery<Token> query = QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).with(Combine.allOf(
            Restriction.on(TokenTermKeys.NAME).exactlyMatching("admin"),
            Restriction.on(TokenTermKeys.DIRECTORY_ID).greaterThan(Token.APPLICATION_TOKEN_DIRECTORY_ID))).returningAtMost(10);

        // admin token
        List<Token> list = getTokenDAO().search(query);

        assertEquals(ImmutableList.of(getToken1()), list);
        assertEquals("admin", Iterables.getOnlyElement(list).getName());
    }

    @Test
    public void testSearchByLastAccessedTime() throws Exception
    {
        EntityQuery<Token> query = QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).with(Restriction.on(
            TokenTermKeys.LAST_ACCESSED_TIME).lessThan(
            TimeUnit.MINUTES.toMillis(15))).returningAtMost(10);

        List<Token> list = getTokenDAO().search(query);

        assertEquals(ImmutableList.of(getToken1()), list);
    }

    @Test
    public void testSearchWithIndexConstraints()
    {
        // Would return 3 tokens
        List<Token> tokenList = getTokenDAO().search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
                                                         .returningAtMost(1));

        assertThat(tokenList, hasSize(1));
    }

    @Test
    public void testSearchWithIndexConstraintsSkippingTheFirstResult()
    {
        // Would return 3 tokens
        List<Token> tokenList = getTokenDAO().search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).startingAt(
            1).returningAtMost(20));

        assertThat(tokenList, hasSize(2));
    }

    @Test
    public void testSearchForTokenBySecretNumber()
    {
        List<Token> tokenList = getTokenDAO().search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
                                                         .with(Restriction.on(TokenTermKeys.RANDOM_NUMBER)
                                                                   .exactlyMatching(1234567890L))
                                                         .returningAtMost(10));

        assertThat(tokenList, hasSize(1));
        assertEquals(1234567890L, Iterables.getOnlyElement(tokenList).getRandomNumber());
    }

    @Test
    public void testSearchForSecretNumberWhereSecretNumberIsEmpty() throws Exception
    {
        List<Token> tokenList = getTokenDAO().search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
                                                         .with(Restriction.on(TokenTermKeys.RANDOM_NUMBER)
                                                                   .exactlyMatching(0L))
                                                         .returningAtMost(10));

        assertEquals(ImmutableList.of(getToken2()), tokenList);
    }

    @Test
    public void testRemoveAllByDirectory()
    {
        getTokenDAO().removeAll(DIRECTORY_ID);

        List<Token> tokenList = getTokenDAO().search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
                                                         .with(Restriction.on(TokenTermKeys.DIRECTORY_ID)
                                                                   .exactlyMatching(DIRECTORY_ID))
                                                         .returningAtMost(10));

        assertThat(tokenList, IsEmptyCollection.<Token>empty());
    }

    @Test
    public void testRemoveAllExpiredTokens() throws Exception
    {
        Date currentDate = new Date(TimeUnit.MINUTES.toMillis(180));
        long maxLifeInSeconds = TimeUnit.HOURS.toSeconds(1);

        getTokenDAO().removeExpiredTokens(currentDate, maxLifeInSeconds);

        EntityQuery<Token> allTokensQuery = QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
            .returningAtMost(EntityQuery.ALL_RESULTS);
        List<Token> tokenList = getTokenDAO().search(allTokensQuery);

        // enough time has passed to expire all tokens
        assertThat(tokenList, IsEmptyCollection.<Token>empty());
    }

    @Test
    public void testRemoveExpiredTokensWithSpecificDurationLessThanSessionMaxLife() throws Exception
    {
        Date currentDate = new Date(TimeUnit.MINUTES.toMillis(70));
        long maxLifeInSeconds = TimeUnit.HOURS.toSeconds(1);

        getTokenDAO().removeExpiredTokens(currentDate, maxLifeInSeconds);

        EntityQuery<Token> allTokensQuery = QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
            .returningAtMost(EntityQuery.ALL_RESULTS);
        List<Token> tokenList = getTokenDAO().search(allTokensQuery);

        // token1 expires because of maxLifeInSeconds (more than 1 hour since last accessed)
        // token2 survives
        // token3 expires because it is a short-lived one
        assertEquals(ImmutableList.of(getToken2()), tokenList);
        assertNotNull("Token has survived", getTokenDAO().findByIdentifierHash("bleh"));
    }

    @Test
    public void testRemoveExpiredTokensWithSpecificDurationGreaterThanSessionMaxLife() throws Exception
    {
        Date currentDate = new Date(TimeUnit.MINUTES.toMillis(32));
        long maxLifeInSeconds = 1; // a really short default max time

        getTokenDAO().removeExpiredTokens(currentDate, maxLifeInSeconds);

        EntityQuery<Token> allTokensQuery = QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
            .returningAtMost(EntityQuery.ALL_RESULTS);
        List<Token> tokenList = getTokenDAO().search(allTokensQuery);

        // token1 and 2 expire because of maxLifeInSeconds
        // token3 expires because maxLifeInSeconds is an upper limit to its requested duration (15 min)
        assertThat(tokenList, IsEmptyCollection.<Token>empty());
    }

    @Test
    public void testFindByKey() throws Exception
    {
        Token token = getTokenDAO().findByRandomHash(TOKEN_KEY);
        assertNotNull(token);
        assertEquals(TOKEN_KEY, token.getRandomHash());
    }

    @Test
    public void testFindByIdentifierHash() throws Exception
    {
        Token newToken = new Token.Builder(DIRECTORY_ID, "admin", "blah", RANDOM_NUMBER + 1, "xxx-not-a-random-hash-xxx")
            .create();

        Token token = getTokenDAO().findByIdentifierHash(newToken.getIdentifierHash());

        assertEquals(newToken.getIdentifierHash(), token.getIdentifierHash());
    }

    @Test
    public void testRemoveAll()
    {
        getTokenDAO().removeAll();

        List<Token> tokenList = getTokenDAO().search(QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).with(Restriction.on(TokenTermKeys.DIRECTORY_ID).exactlyMatching(DIRECTORY_ID)).returningAtMost(10));

        assertThat(tokenList, IsEmptyCollection.<Token>empty());
    }

    @Test
    public void testLoadAll() throws Exception
    {
        assertThat(principalNames(getTokenDAO().loadAll()), containsInAnyOrder("admin", "user", "crowd"));
    }

    @Test
    public void testSaveAllWithNoTokens() throws Exception
    {
        getTokenDAO().removeAll();
        assertThat(getTokenDAO().loadAll(), IsEmptyCollection.<Token>empty());

        getTokenDAO().saveAll(Collections.<Token>emptyList());
        assertThat(getTokenDAO().loadAll(), IsEmptyCollection.<Token>empty());
    }

    @Test
    public void testSaveAllWithTestTokens() throws Exception
    {
        getTokenDAO().removeAll();
        assertThat(getTokenDAO().loadAll(), IsEmptyCollection.<Token>empty());

        getTokenDAO().saveAll(ImmutableList.copyOf(TokenDAOMemoryTest.getTestTokens()));
        assertThat(principalNames(getTokenDAO().loadAll()), containsInAnyOrder("admin", "user", "crowd"));
    }

    protected abstract TokenDAO getTokenDAO();

    private Token getToken1() throws ObjectNotFoundException
    {
        Token token1 = getTokenDAO().findByIdentifierHash(TOKEN1_ID_HASH);

        // since this test relies on data provided by subclasses, we first assert it as a precondition
        assertEquals(1, token1.getLastAccessedTime());
        assertTrue(token1.getLifetime().isDefault());

        return token1;
    }

    private Token getToken2() throws ObjectNotFoundException
    {
        Token token2 = getTokenDAO().findByIdentifierHash(TOKEN2_ID_HASH);

        // since this test relies on data provided by subclasses, we first assert it as a precondition
        assertEquals(TimeUnit.MINUTES.toMillis(30), token2.getLastAccessedTime());
        assertTrue(token2.getLifetime().isDefault());

        return token2;
    }

    private Token getToken3() throws ObjectNotFoundException
    {
        Token token3 = getTokenDAO().findByIdentifierHash(TOKEN3_ID_HASH);

        // since this test relies on data provided by subclasses, we first assert it as a precondition
        assertEquals(TimeUnit.MINUTES.toMillis(30), token3.getLastAccessedTime());
        assertEquals(TimeUnit.MINUTES.toSeconds(15), token3.getLifetime().getSeconds());

        return token3;
    }
}
