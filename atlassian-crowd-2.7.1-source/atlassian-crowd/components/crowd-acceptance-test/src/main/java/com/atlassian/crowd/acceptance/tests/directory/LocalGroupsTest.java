package com.atlassian.crowd.acceptance.tests.directory;

import com.atlassian.crowd.acceptance.utils.DirectoryTestHelper;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.SynchronisableDirectory;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.ReadOnlyGroupException;
import com.atlassian.crowd.manager.directory.SynchronisationMode;
import com.atlassian.crowd.model.DirectoryEntity;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.spring.container.ContainerManager;

import com.google.common.collect.ImmutableSet;

import static com.atlassian.crowd.acceptance.tests.directory.BasicTest.getImplementation;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import static com.atlassian.crowd.acceptance.tests.directory.ModelAssertions.assertGroupsEqual;
import static com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper.LOCAL_GROUPS;
import static java.lang.Boolean.TRUE;

/**
 * Tests the local groups functionality of the {@link com.atlassian.crowd.directory.DbCachingRemoteDirectory}.
 */
public abstract class LocalGroupsTest extends BaseTest
{
    private static final String LDAP_USER_NAME = "user1";
    private static final String LDAP_USER2_NAME = "user2";
    private static final String LDAP_USER3_NAME = "user3";

    private static final String LDAP_GROUP_NAME = "ldap-group";
    private static final String LDAP_GROUP2_NAME = "ldap-group2";
    private static final String LOCAL_GROUP_NAME = "local-group";
    private static final String LOCAL_GROUP2_NAME = "local-group2";

    private RemoteDirectory remoteDirectory;
    private RemoteDirectory ldapDirectory;
    private Group localGroup;
    private Group ldapGroup;

    public LocalGroupsTest()
    {
//        setDirectoryConfigFile(DirectoryTestHelper.getApacheDS154ConfigFileName());
    }

    @Override
    protected void configureDirectory(Properties directorySettings)
    {
        super.configureDirectory(directorySettings);
        directory.setAttribute(LOCAL_GROUPS, TRUE.toString());
        directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.TRUE.toString());
    }

    protected void loadTestData() throws Exception
    {
        remoteDirectory = getImplementation(directory);
        ldapDirectory = ((DirectoryInstanceLoader) ContainerManager.getComponent("ldapDirectoryInstanceLoader")).getDirectory(directory);

        User user = new ImmutableUser(directory.getId(), LDAP_USER_NAME, "Bob", "Smith", "Bob Smith", "bsmith@example.com");
        User user2 = new ImmutableUser(directory.getId(), LDAP_USER2_NAME, "Bob", "Smith", "Bob Smith", "bsmith@example.com");
        User user3 = new ImmutableUser(directory.getId(), LDAP_USER3_NAME, "Bob", "Smith", "Bob Smith", "bsmith@example.com");

        ldapDirectory.addUser(new UserTemplate(user), new PasswordCredential("password"));
        ldapDirectory.addUser(new UserTemplate(user2), new PasswordCredential("password"));
        ldapDirectory.addUser(new UserTemplate(user3), new PasswordCredential("password"));

        localGroup = remoteDirectory.addGroup(new GroupTemplate(LOCAL_GROUP_NAME, directory.getId()));
        remoteDirectory.addGroup(new GroupTemplate(LOCAL_GROUP2_NAME, directory.getId()));

        ldapGroup = ldapDirectory.addGroup(new GroupTemplate(LDAP_GROUP_NAME, directory.getId()));
        ldapDirectory.addGroup(new GroupTemplate(LDAP_GROUP2_NAME, directory.getId()));

        // memberships
        ldapDirectory.addUserToGroup(LDAP_USER2_NAME, LDAP_GROUP2_NAME);
        ldapDirectory.addUserToGroup(LDAP_USER3_NAME, LDAP_GROUP2_NAME);
        ldapDirectory.addGroupToGroup(LDAP_GROUP_NAME, LDAP_GROUP2_NAME);

        synchronizeCache(remoteDirectory);

        remoteDirectory.addUserToGroup(LDAP_USER2_NAME, LOCAL_GROUP2_NAME);
        remoteDirectory.addUserToGroup(LDAP_USER3_NAME, LOCAL_GROUP2_NAME);
        remoteDirectory.addGroupToGroup(LOCAL_GROUP_NAME, LOCAL_GROUP2_NAME);
    }

    @Override
    protected void removeTestData() throws DirectoryInstantiationException
    {
        DirectoryTestHelper.silentlyRemoveUser(LDAP_USER_NAME, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser(LDAP_USER2_NAME, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveUser(LDAP_USER3_NAME, getRemoteDirectory());

        DirectoryTestHelper.silentlyRemoveGroup(LOCAL_GROUP_NAME, getRemoteDirectory());
        DirectoryTestHelper.silentlyRemoveGroup(LOCAL_GROUP2_NAME, getRemoteDirectory());

        try
        {
            ldapDirectory = ((DirectoryInstanceLoader) ContainerManager.getComponent("ldapDirectoryInstanceLoader")).getDirectory(directory);
            ldapDirectory.removeGroup(LDAP_GROUP_NAME);
            ldapDirectory.removeGroup(LDAP_GROUP2_NAME);
        }
        catch (Exception e)
        {
            logger.debug(e);
        }
    }

    /**
     * Synchronise the cache
     * @param remoteDirectory Directory to synchronize.
     * @throws com.atlassian.crowd.directory.monitor.DirectoryMonitorCreationException
     */
    private void synchronizeCache(final RemoteDirectory remoteDirectory) throws OperationFailedException
    {
        ((SynchronisableDirectory) remoteDirectory).synchroniseCache(SynchronisationMode.FULL, new MockSynchronisationStatusManager());
    }

    public void testFindLocalGroupByName() throws Exception
    {
        Group retrievedGroup = remoteDirectory.findGroupByName(LOCAL_GROUP_NAME);
        assertGroupsEqual(localGroup, retrievedGroup);
    }

    public void testFindLdapGroupByName() throws Exception
    {
        Group retrievedGroup = remoteDirectory.findGroupByName(LDAP_GROUP_NAME);
        assertGroupsEqual(ldapGroup, retrievedGroup);
    }

    public void testAddUserToLocalGroup() throws Exception
    {
        remoteDirectory.addUserToGroup(LDAP_USER_NAME, LOCAL_GROUP_NAME);

        boolean isMember = remoteDirectory.isUserDirectGroupMember(LDAP_USER_NAME, LOCAL_GROUP_NAME);
        assertTrue("should be a member", isMember);
    }

    public void testAddUserToLdapGroupFails() throws Exception
    {
        try
        {
            remoteDirectory.addUserToGroup(LDAP_USER_NAME, LDAP_GROUP_NAME);
            fail("Expected exception not thrown");
        }
        catch (ReadOnlyGroupException e)
        {
            // success
        }
    }

    public void testRemoveUserFromLocalGroup() throws Exception
    {
        remoteDirectory.addUserToGroup(LDAP_USER_NAME, LOCAL_GROUP_NAME);
        remoteDirectory.removeUserFromGroup(LDAP_USER_NAME, LOCAL_GROUP_NAME);

        boolean isMember = remoteDirectory.isUserDirectGroupMember(LDAP_USER_NAME, LOCAL_GROUP_NAME);
        assertFalse("should no longer be a member", isMember);
    }

    public void testRemoveUserFromLdapGroupFails() throws Exception
    {
        try
        {
            remoteDirectory.removeUserFromGroup(LDAP_USER_NAME, LDAP_GROUP_NAME);
            fail("Expected exception not thrown");
        }
        catch (ReadOnlyGroupException e)
        {
            // success
        }
    }

    public void testAddLocalGroupToLocalGroup() throws Exception
    {
        Group childGroup = remoteDirectory.addGroup(new GroupTemplate("child-group", directory.getId()));
        try
        {
            remoteDirectory.addGroupToGroup(childGroup.getName(), LOCAL_GROUP_NAME);

            boolean isMember = remoteDirectory.isGroupDirectGroupMember(childGroup.getName(), LOCAL_GROUP_NAME);
            assertTrue("should be a member", isMember);
        }
        finally
        {
            DirectoryTestHelper.silentlyRemoveGroup(childGroup.getName(), getRemoteDirectory());
        }
    }

    public void testRemoveLocalGroupFromLocalGroup() throws Exception
    {
        Group childGroup = remoteDirectory.addGroup(new GroupTemplate("child-group", directory.getId()));
        try
        {
            remoteDirectory.addGroupToGroup(childGroup.getName(), LOCAL_GROUP_NAME);

            boolean isMember = remoteDirectory.isGroupDirectGroupMember(childGroup.getName(), LOCAL_GROUP_NAME);
            assertTrue("should be a member", isMember);

            remoteDirectory.removeGroupFromGroup(childGroup.getName(), LOCAL_GROUP_NAME);

            isMember = remoteDirectory.isGroupDirectGroupMember(childGroup.getName(), LOCAL_GROUP_NAME);
            assertFalse("should no longer be a member", isMember);
        }
        finally
        {
            DirectoryTestHelper.silentlyRemoveGroup(childGroup.getName(), getRemoteDirectory());
        }
    }

    public void testCannotAddLocalGroupWithSameNameAsLdap() throws Exception
    {
        try
        {
            remoteDirectory.addGroup(new GroupTemplate(ldapGroup.getName(), directory.getId()));
            fail("Expected exception not thrown: InvalidGroupException");
        }
        catch (InvalidGroupException e)
        {
            // success
        }
    }

    public void testCannotAddLocalGroupToLdapGroup() throws Exception
    {
        try
        {
            remoteDirectory.addGroupToGroup(LOCAL_GROUP_NAME, LDAP_GROUP_NAME);
            fail("Expected exception not thrown");
        }
        catch (ReadOnlyGroupException e)
        {
            // success
        }
    }

    public void testCanAddLdapGroupToLocalGroupWhenLocalGroupsAreEnabled() throws Exception
    {
        remoteDirectory.addGroupToGroup(LDAP_GROUP_NAME, LOCAL_GROUP_NAME);

        assertTrue("Should be a member",
                remoteDirectory.isGroupDirectGroupMember(LDAP_GROUP_NAME, LOCAL_GROUP_NAME));
    }

    public void testSearchAllGroups() throws OperationFailedException
    {
        List<Group> groups = remoteDirectory.searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).returningAtMost(10));

        assertEquals(4, groups.size());
    }

    public void testSearchAllGroupNamesWithRestrictions() throws OperationFailedException
    {
        List<String> groupNames = remoteDirectory.searchGroups(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).startingAt(1).returningAtMost(2));

        assertEquals(2, groupNames.size());
        assertTrue(groupNames.contains(LDAP_GROUP_NAME) || groupNames.contains(LDAP_GROUP2_NAME)); // since we cannot guarantee order
        assertTrue(groupNames.contains(LOCAL_GROUP_NAME) || groupNames.contains(LOCAL_GROUP2_NAME));
    }

    public void testSearchGroupsOnLdapOnlyAttributes() throws OperationFailedException
    {
        List<Group> groups = remoteDirectory.searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).with(
                Combine.anyOf(
                        Restriction.on(GroupTermKeys.NAME).exactlyMatching(LDAP_GROUP_NAME),
                        Restriction.on(GroupTermKeys.NAME).exactlyMatching(LOCAL_GROUP_NAME)
                )
        ).returningAtMost(10));

        assertEquals(2, groups.size());
        Set<String> result = ImmutableSet.of(groups.get(0).getName(), groups.get(1).getName());
        assertTrue(result.contains(LDAP_GROUP_NAME));
        assertTrue(result.contains(LOCAL_GROUP_NAME));
    }

    public void testSearchGroupNamesOnLdapOnlyAttribute() throws OperationFailedException
    {
        List<String> groupnames = remoteDirectory.searchGroups(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).with(
                Restriction.on(GroupTermKeys.NAME).containing("2")
        ).returningAtMost(10));

        assertEquals(2, groupnames.size());
        assertTrue(groupnames.contains(LDAP_GROUP2_NAME));
        assertTrue(groupnames.contains(LOCAL_GROUP2_NAME));
    }

    private Set<String> asNames(Collection<? extends DirectoryEntity> entities)
    {
        Set<String> names = new HashSet<String>(entities.size());
        for (DirectoryEntity entity : entities)
        {
            names.add(entity.getName());
        }
        return names;
    }

    private <T> void assertSearchReturns(MembershipQuery<T> query, String... expectedResults) throws OperationFailedException
    {
        List<T> results = remoteDirectory.searchGroupRelationships(query);

        assertEquals(expectedResults.length, results.size());

        // ignore ordering
        Set<String> expectedSet = new HashSet<String>(Arrays.asList(expectedResults));
        Set<String> actualSet = new HashSet<String>();

        if (query.getReturnType().equals(String.class))
        {
            actualSet.addAll((List<String>) results);
        }
        else
        {
            for (T result : results)
            {
                if (!query.getReturnType().isAssignableFrom(result.getClass()))
                {
                    fail("Returned result type: " + result.getClass() + " does not match requested return type: " + query.getReturnType());
                }

                if (Group.class.isAssignableFrom(query.getReturnType()))
                {
                    actualSet.add(((Group) result).getName());
                }
                else if (User.class.isAssignableFrom(query.getReturnType()))
                {
                    actualSet.add(((User) result).getName());
                }
                else
                {
                    fail("Result is not of type String, User or Group: " + query.getReturnType());
                }
            }
        }

        assertEquals(expectedSet, actualSet);
    }

    public void testSearchGroupRelationshipsForUserMembersOfLocalGroup() throws OperationFailedException
    {
        MembershipQuery<User> query = QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LOCAL_GROUP2_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LDAP_USER2_NAME, LDAP_USER3_NAME);
    }

    public void testSearchGroupRelationshipsForUserNameMembersOfLocalGroup() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LOCAL_GROUP2_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LDAP_USER2_NAME, LDAP_USER3_NAME);
    }

    public void testSearchGroupRelationshipsForUserMembersOfLdapGroup() throws OperationFailedException
    {
        MembershipQuery<User> query = QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LDAP_USER2_NAME, LDAP_USER3_NAME);
    }

    public void testSearchGroupRelationshipsForUserNameMembersOfLdapGroup() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LDAP_USER2_NAME, LDAP_USER3_NAME);
    }

    public void testSearchGroupRelationshipsForGroupMembersOfLocalGroup() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(LOCAL_GROUP2_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LOCAL_GROUP_NAME);
    }

    public void testSearchGroupRelationshipsForGroupNameMembersOfLocalGroup() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(LOCAL_GROUP2_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LOCAL_GROUP_NAME);
    }

    public void testSearchGroupRelationshipsForGroupMembersOfLdapGroup() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LDAP_GROUP_NAME);
    }

    public void testSearchGroupRelationshipsForGroupNameMembersOfLdapGroup() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(LDAP_GROUP2_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LDAP_GROUP_NAME);
    }

    public void testSearchGroupRelationshipsForCombinedGroupMembershipsOfUser() throws OperationFailedException
    {
        MembershipQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(LDAP_USER2_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LOCAL_GROUP2_NAME, LDAP_GROUP2_NAME);
    }

    public void testSearchGroupRelationshipsForCombinedGroupNameMembershipsOfUser() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(LDAP_USER2_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LOCAL_GROUP2_NAME, LDAP_GROUP2_NAME);
    }

    public void testSearchGroupRelationshipsForGroupMembershipsOfLocalGroup() throws OperationFailedException
    {
        MembershipQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.group()).withName(LOCAL_GROUP_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LOCAL_GROUP2_NAME);
    }

    public void testSearchGroupRelationshipsForGroupNameMembershipsOfLocalGroup() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.group()).withName(LOCAL_GROUP_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LOCAL_GROUP2_NAME);
    }

    public void testSearchGroupRelationshipsForGroupMembershipsOfLdapGroup() throws OperationFailedException
    {
        MembershipQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.group()).withName(LDAP_GROUP_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LDAP_GROUP2_NAME);
    }

    public void testSearchGroupRelationshipsForGroupNameMembershipsOfLdapGroup() throws OperationFailedException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.group()).withName(LDAP_GROUP_NAME).returningAtMost(EntityQuery.ALL_RESULTS);
        assertSearchReturns(query, LDAP_GROUP2_NAME);
    }

}
