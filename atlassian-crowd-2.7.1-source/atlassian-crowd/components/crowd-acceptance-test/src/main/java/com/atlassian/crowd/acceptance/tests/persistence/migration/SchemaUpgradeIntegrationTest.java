package com.atlassian.crowd.acceptance.tests.persistence.migration;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.dao.alias.AliasDAO;
import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.dao.property.PropertyDAO;
import com.atlassian.crowd.dao.token.TokenDAOHibernate;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.GroupDao;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.persistence.hibernate.SchemaHelper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * This test is HSQLDB-specific because it uses a SQL script with the HSQLDB dialect to
 * restore the database to a known state.
 * The DirtiesContext annotation ensures that the Spring context created for this
 * test is not reused by any other test (such as {@link SchemaCreationIntegrationTest}).
 * Each test class requires a fresh instance of the HSQL database, a fresh instance
 * of the SessionFactory and a reset of the auto-increment ID generators.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:/applicationContext-schemahelper-config.xml",
                                   "classpath:/applicationContext-CrowdDAO.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, TransactionalTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class})
@TransactionConfiguration(defaultRollback = true)
@DirtiesContext(classMode= DirtiesContext.ClassMode.AFTER_CLASS)
public class SchemaUpgradeIntegrationTest extends AbstractDaoIntegrationTest
{

    private static final Logger logger = LoggerFactory.getLogger(SchemaUpgradeIntegrationTest.class);

    private static final String REFERENCE_DATABASE_SCRIPT = "database-crowd-2.1.sql";

    private static final long EXISTING_APPLICATION_ID = 163841L;
    private static final long EXISTING_DIRECTORY_ID = 2;
    private static final String EXISTING_GROUP_NAME = "badgers";
    private static final String EXISTING_USER_NAME = "penny";

    @Inject private SchemaHelper schemaHelper;
    @Inject private DataSource dataSource;

    @Inject private TokenDAOHibernate tokenDao;
    @Inject private ApplicationDAO applicationDao;
    @Inject private UserDao userDao;
    @Inject private GroupDao groupDao;
    @Inject private MembershipDao membershipDao;
    @Inject private AliasDAO aliasDao;
    @Inject private PropertyDAO propertyDao;
    @Inject private DirectoryDao directoryDao;

    /**
     * The database needs to be restored and upgraded before a transaction is started, because
     * otherwise Hibernate's SessionFactory will initialise it with the new schema.
     * @throws Exception
     */
    @BeforeTransaction
    public void restoreDatabaseAndUpgradeSchema() throws Exception
    {
        logger.info("Restoring database");
        assertNotNull("Data source must have been injected", dataSource);
        JdbcTemplate template = new JdbcTemplate(dataSource);
        ClassPathResource resource = new ClassPathResource(REFERENCE_DATABASE_SCRIPT);

        JdbcTestUtils.executeSqlScript(template, resource, false);
        logger.info("Database restored");

        logger.info("Updating schema");
        schemaHelper.updateSchemaIfNeeded(true);
        logger.info("Schema updated");
    }

    /**
     * DAOs must be run within a transaction.
     * @throws Exception
     */
    @Test
    @Transactional
    public void existingDataIsPreservedBySchemaUpgradeAndNewDataCanBeInserted() throws Exception
    {
        // existing data must be preserved
        _testDirectoryDaoCanReadExistingData();
        _testUserDaoCanReadExistingData();
        _testGroupDaoCanReadExistingData();
        _testMembershipDaoCanReadExistingData();
        _testApplicationDaoCanReadExistingData();
        _testAliasDaoCanReadExistingData();
        _testTokenDaoCanReadExistingData();
        _testPropertyDaoCanReadExistingData();

        // it must be possible to add new data
        // order is important due to referential integrity
        _testDirectoryDaoCanInsert();
        _testUserDaoCanInsert();
        _testGroupDaoCanInsert();
        _testMembershipDaoCanInsert();
        _testApplicationDaoCanInsert();
        _testAliasDaoCanInsert();
        _testTokenDaoCanInsert();
        _testPropertyDaoCanInsert();
    }

    private void _testAliasDaoCanReadExistingData() throws Exception
    {
        // verify it can read existing alias
        assertThat(aliasDao.findUsernameByAlias(applicationDao.findById(EXISTING_APPLICATION_ID), "pen"), is(EXISTING_USER_NAME));
    }

    private void _testDirectoryDaoCanReadExistingData() throws Exception
    {
        // verify number of existing directories
        assertThat(directoryDao.findAll(), hasSize(2));

        // verify it can read existing directory
        assertThat(directoryDao.findById(EXISTING_DIRECTORY_ID).getName(), is("Directory One"));
    }

    private void _testApplicationDaoCanReadExistingData() throws Exception
    {
        EntityQuery<Application> allApplicationsQuery = QueryBuilder.queryFor(Application.class,
                                                                              EntityDescriptor.application())
            .returningAtMost(EntityQuery.ALL_RESULTS);

        // verify applications exist
        assertThat(applicationDao.search(allApplicationsQuery), hasSize(2));

        // verify directory mappings exist
        assertNotNull("There should exist a directory mapping for existing application",
                      applicationDao.findById(EXISTING_APPLICATION_ID).getDirectoryMapping(1));

        // verify group mappings exist
        assertThat(applicationDao.findById(EXISTING_APPLICATION_ID).getRemoteAddresses(), hasSize(4));
    }

    private void _testTokenDaoCanReadExistingData()
    {
        EntityQuery<Token> allTokensQuery = QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
            .returningAtMost(EntityQuery.ALL_RESULTS);

        // verify number of existing tokens (in the future, upgrades may clean the table, but this is not the case yet)
        assertThat(tokenDao.search(allTokensQuery), hasSize(3));
    }

    private void _testUserDaoCanReadExistingData() throws Exception
    {
        EntityQuery<User> allUsersQuery = QueryBuilder.queryFor(User.class, EntityDescriptor.user())
            .returningAtMost(EntityQuery.ALL_RESULTS);

        // verify number of existing existing users
        assertThat(userDao.search(EXISTING_DIRECTORY_ID, allUsersQuery), hasSize(4));

        // verify user attributes
        assertThat(userDao.findByNameWithAttributes(EXISTING_DIRECTORY_ID, "eeeep").getKeys(), hasSize(4));
    }

    private void _testGroupDaoCanReadExistingData() throws Exception
    {
        EntityQuery<Group> allGroupsQuery = QueryBuilder.queryFor(Group.class, EntityDescriptor.group())
            .returningAtMost(EntityQuery.ALL_RESULTS);

        // verify number of existing groups
        assertThat(groupDao.search(EXISTING_DIRECTORY_ID, allGroupsQuery), hasSize(4));

        // verify it can read an existing group
        assertThat(groupDao.findByName(EXISTING_DIRECTORY_ID, EXISTING_GROUP_NAME).isLocal(), is(false));
    }

    private void _testMembershipDaoCanReadExistingData() throws Exception
    {
        MembershipQuery<User> allMembershipsQuery =
            QueryBuilder.createMembershipQuery(EntityQuery.MAX_MAX_RESULTS, 0, true,
                                               EntityDescriptor.user(), User.class,
                                               EntityDescriptor.group(), EXISTING_GROUP_NAME);

        // verify number of existing memberships
        assertThat(membershipDao.search(EXISTING_DIRECTORY_ID, allMembershipsQuery), hasSize(2));
    }

    private void _testPropertyDaoCanReadExistingData() throws Exception
    {
        // verify number of existing properties
        assertThat(getPropertyDao().findAll(), hasSize(27));

        // verify it can read an existing property
        assertThat(getPropertyDao().find("crowd", "build.number").getValue(), is("431"));
    }

    @Override
    public TokenDAOHibernate getTokenDao()
    {
        return tokenDao;
    }

    @Override
    public ApplicationDAO getApplicationDao()
    {
        return applicationDao;
    }

    @Override
    public UserDao getUserDao()
    {
        return userDao;
    }

    @Override
    public GroupDao getGroupDao()
    {
        return groupDao;
    }

    @Override
    public MembershipDao getMembershipDao()
    {
        return membershipDao;
    }

    @Override
    public AliasDAO getAliasDao()
    {
        return aliasDao;
    }

    @Override
    public PropertyDAO getPropertyDao()
    {
        return propertyDao;
    }

    @Override
    public DirectoryDao getDirectoryDao()
    {
        return directoryDao;
    }
}
