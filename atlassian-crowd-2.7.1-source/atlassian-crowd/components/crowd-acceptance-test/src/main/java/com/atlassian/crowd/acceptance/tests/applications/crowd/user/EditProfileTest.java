package com.atlassian.crowd.acceptance.tests.applications.crowd.user;

public class EditProfileTest extends CrowdUserConsoleAcceptenceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        setScriptingEnabled(true);
        loadXmlOnSetUp("userconsoletest.xml");
    }

    @Override
    public void tearDown() throws Exception
    {
        setScriptingEnabled(false);
        super.tearDown();
    }

    public void testViewProfile()
    {
        log("Running testViewProfile");

        _loginTestUser();

        gotoPage("/console/user/viewprofile.action");

        assertKeyPresent("menu.user.console.editprofile.label");
        assertTextFieldEquals("firstname", "Test");
        assertTextFieldEquals("lastname", "User");
        assertTextFieldEquals("email", "user@example.com");
        assertTextPresent(TEST_USER_NAME);
    }

    public void testUpdateProfileBlank()
    {
        log("Running testUpdateProfileBlank");

        _loginTestUser();

        gotoPage("/console/user/viewprofile.action");

        assertKeyPresent("menu.user.console.editprofile.label");
        setTextField("firstname", "");
        setTextField("lastname", "");
        setTextField("email", "");
        submit();

        assertKeyPresent("menu.user.console.editprofile.label");
        assertKeyPresent("principal.firstname.invalid");
        assertKeyPresent("principal.lastname.invalid");
        assertKeyPresent("principal.email.invalid");
    }

    public void testUpdateProfileBadEmail()
    {
        log("Running testUpdateProfileBadEmail");

        _loginTestUser();

        gotoPage("/console/user/viewprofile.action");

        assertKeyPresent("menu.user.console.editprofile.label");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        setTextField("email", "bad-email");
        submit();

        assertKeyPresent("menu.user.console.editprofile.label");
        assertKeyPresent("principal.email.invalid");
    }

    public void testUpdateProfileEmailWithTrailingWhitespace()
    {
        log("Running testUpdateProfileBadEmail");

        _loginTestUser();

        gotoPage("/console/user/viewprofile.action");

        assertKeyPresent("menu.user.console.editprofile.label");
        setTextField("firstname", "Test");
        setTextField("lastname", "User");
        setTextField("email", " john@example.test ");
        submit();

        assertKeyPresent("menu.user.console.editprofile.label");
        assertKeyPresent("principal.email.whitespace");
    }

    public void testUpdateProfileSuccess()
    {
        intendToModifyData();

        log("Running testUpdateProfileSuccess");

        _loginTestUser();

        gotoPage("/console/user/viewprofile.action");

        assertKeyPresent("menu.user.console.editprofile.label");
        setTextField("firstname", "Test1");
        setTextField("lastname", "User1");
        setTextField("email", "user@example.com1");
        submit();

        assertKeyPresent("menu.user.console.editprofile.label");
        assertKeyPresent("updatesuccessful.label");
        assertTextFieldEquals("firstname", "Test1");
        assertTextFieldEquals("lastname", "User1");
        assertTextFieldEquals("email", "user@example.com1");
    }

    public void testUpdateProfileCancel()
    {
        log("Running testUpdateProfileCancel");

        _loginTestUser();

        gotoPage("/console/user/viewprofile.action");

        assertKeyPresent("menu.user.console.editprofile.label");
        setTextField("firstname", "Test1");
        setTextField("lastname", "User1");
        setTextField("email", "user@example.com1");
        clickButton("cancel");

        assertKeyPresent("menu.user.console.editprofile.label");
        assertKeyNotPresent("updatesuccessful.label");
        assertTextFieldEquals("firstname", "Test");
        assertTextFieldEquals("lastname", "User");
        assertTextFieldEquals("email", "user@example.com");
    }

    public void testUpdateProfileNoPermissions()
    {
        log("Running testUpdateProfileNoPermissions");

        _loginImmutableUser();

        gotoPage("/console/user/viewprofile.action");

        assertKeyPresent("menu.user.console.editprofile.label");
        setTextField("firstname", "Test1");
        setTextField("lastname", "User1");
        setTextField("email", "user@example.com1");
        submit();

        assertKeyPresent("menu.user.console.editprofile.label");
        assertKeyPresent("user.console.profile.permission.error");
    }

    public void testUpdateProfileSuccessAndLoginAgain()
    {
        intendToModifyData();

        log("Running testUpdateProfileSuccessAndLoginAgain");

        _loginTestUser();

        gotoPage("/console/user/viewprofile.action");

        assertKeyPresent("menu.user.console.editprofile.label");
        setTextField("firstname", "Test1");
        setTextField("lastname", "User1");
        setTextField("email", "user@example.com1");
        submit();

        assertKeyPresent("menu.user.console.editprofile.label");
        assertKeyPresent("updatesuccessful.label");
        assertTextFieldEquals("firstname", "Test1");
        assertTextFieldEquals("lastname", "User1");
        assertTextFieldEquals("email", "user@example.com1");

        _logout();
        _loginTestUser();

        gotoPage("/console/user/viewprofile.action");

        assertKeyPresent("menu.user.console.editprofile.label");
        assertTextFieldEquals("firstname", "Test1");
        assertTextFieldEquals("lastname", "User1");
        assertTextFieldEquals("email", "user@example.com1");
    }
}
