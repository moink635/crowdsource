package com.atlassian.crowd.acceptance.tests.applications.crowdid.client;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import com.atlassian.crowd.acceptance.tests.BaseUrlFromProperties;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import org.apache.commons.lang3.StringUtils;

import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

public class CrowdIDClientTest extends CrowdIDClientAcceptanceTestCase
{
    String opUrl;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        BaseUrlFromProperties props = BaseUrlFromProperties.withLocalTestProperties();

        this.opUrl = props.baseUrlFor("crowdid") + "/op";
    }

    /**
     * Test that OpenID redirection to CrowdID, which will involve short URLs,
     * uses HTTP redirection rather than form redirection.
     */
    public void testRedirectionUsesHttpRedirect() throws IOException
    {
        String requestParams = "openid_identifier=" + opUrl;

        URL url = new URL(baseUrl + "/login!login.action?" + requestParams);

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setInstanceFollowRedirects(false);

        /* Assert that we are redirected to the OpenID server */
        assertEquals(302, conn.getResponseCode());
        assertThat(conn.getHeaderField("Location"), startsWith(opUrl + "?"));
    }

    /**
     * Test that OpenID redirection with an artificially-padded set of sreg
     * attributes uses form redirection.
     */
    public void testRedirectionForLongerUrlUsesFormRedirection() throws IOException
    {
        String padding = StringUtils.repeat('x', 2048);

        String requestParams = "openid_identifier=" + opUrl + "&requiredAttribs=" + padding;

        URL url = new URL(baseUrl + "/login!login.action?" + requestParams);

        WebClient webClient = new WebClient();

        webClient.getOptions().setRedirectEnabled(false);
        webClient.getOptions().setJavaScriptEnabled(false);

        HtmlPage htmlPage = webClient.getPage(url);

        /* Assert that we get a form redirection */
        assertEquals(200, htmlPage.getWebResponse().getStatusCode());

        assertEquals(opUrl,
                htmlPage.getFormByName("openid-form-redirection").getAttribute("action"));

        assertEquals(padding,
                htmlPage.getFormByName("openid-form-redirection").getInputByName("openid.sreg.required").getValueAttribute());
    }
}
