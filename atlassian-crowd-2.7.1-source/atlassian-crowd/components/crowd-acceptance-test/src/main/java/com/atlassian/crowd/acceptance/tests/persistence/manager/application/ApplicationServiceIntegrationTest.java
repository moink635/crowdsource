package com.atlassian.crowd.acceptance.tests.persistence.manager.application;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import javax.inject.Inject;
import javax.sql.DataSource;

import com.atlassian.crowd.acceptance.tests.persistence.PersistenceTestHelper;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.group.GroupTemplateWithAttributes;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;
import com.atlassian.crowd.model.user.UserWithAttributes;

import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Integration tests for ApplicationService with Hibernate.
 * Originally these tests were setup to test that "having two internal directories should persist the mutation calls to both directories".
 * Now the amalgamation specs have changed and this test will be used to prove integration with hibernate, and the modern almagation expectations.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:/applicationContext-config.xml",
    "classpath:/applicationContext-CrowdDAO.xml",
    "classpath:/applicationContext-CrowdManagers.xml",
    "classpath:/applicationContext-CrowdUtils.xml",
    "classpath:/applicationContext-internaldirectoryloader-config.xml"
})
@TestExecutionListeners({TransactionalTestExecutionListener.class,
                         DependencyInjectionTestExecutionListener.class})
@Transactional
public class ApplicationServiceIntegrationTest
{
    @Inject private ApplicationService applicationService;
    @Inject private ApplicationManager applicationManager;
    @Inject private DirectoryManager directoryManager;
    @Inject private DataSource dataSource;

    private static final long DIRECTORY_1_ID = 32769;
    private static final long DIRECTORY_2_ID = 32770;

    @BeforeTransaction
    public void loadTestData() throws Exception
    {
        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        PersistenceTestHelper.populateDatabase("application-manager-test.xml", jdbcTemplate);
    }

    @Test
    public void testAddPrincipalOverMultipleInternalDirectories() throws Exception
    {
        Application application = applicationManager.findByName("crowd");

        List<DirectoryMapping> directoryMappings = application.getDirectoryMappings();

        UserTemplateWithAttributes testUser = UserTemplateWithAttributes.ofUserWithNoAttributes(new UserTemplate(
            "testuser"));
        testUser.setFirstName("beep");
        testUser.setLastName("boop");
        testUser.setEmailAddress("testuser@example.com");
        testUser.setAttribute("phone", "9568-7856");
        testUser.setAttribute("sn", "User");
        testUser.setAttribute("givenName", "Test");

        applicationService.addUser(application, testUser, new PasswordCredential("secret"));
        applicationService.storeUserAttributes(application, testUser.getName(), testUser.getAttributes());

        // The user should have been added only to the first directory:
        Long directoryID = directoryMappings.get(0).getDirectory().getId();
        UserWithAttributes savedUser = directoryManager.findUserWithAttributesByName(directoryID, "testuser");
        assertEquals(testUser.getEmailAddress(), savedUser.getEmailAddress());
        assertEquals(testUser.getName(), savedUser.getName());
        assertEquals(testUser.getValue("sn"), savedUser.getValue("sn"));
        assertEquals(testUser.getValue("phone"), savedUser.getValue("phone"));
        assertEquals(testUser.getValue("givenName"), savedUser.getValue("givenName"));

        // Assert that the user is not in the second directory:
        directoryID = directoryMappings.get(1).getDirectory().getId();
        try
        {
            directoryManager.findUserWithAttributesByName(directoryID, "testuser");
            fail("The user should not have been created in the second directory.");
        }
        catch (UserNotFoundException ex)
        {
            // Expected
        }
    }

    @Test
    public void testUpdatePrincipalOverMultipleInternalDirectories() throws Exception
    {
        Application application = applicationManager.findByName("crowd");

        // We have an "admin" user in both directories - assert the preconditions
        UserWithAttributes user1 = directoryManager.findUserWithAttributesByName(DIRECTORY_1_ID, "admin");
        assertThat(user1, userWithNameAndEmail("Super Man", "Admin@example.com"));
        UserWithAttributes user2 = directoryManager.findUserWithAttributesByName(DIRECTORY_2_ID, "admin");
        assertThat(user2, userWithNameAndEmail("Super Man", "Admin@example.com"));

        // Update the user
        UserTemplate updatedUser = new UserTemplate(user1);
        updatedUser.setDisplayName("Peter Griffin");
        updatedUser.setEmailAddress("peterg@example.com");
        applicationService.updateUser(application, updatedUser);

        // Should be updated in directory 1
        user1 = directoryManager.findUserWithAttributesByName(DIRECTORY_1_ID, "admin");
        assertThat(user1, userWithNameAndEmail("Peter Griffin", "peterg@example.com"));

        // The user in directory 2 should remain the same as before
        user2 = directoryManager.findUserWithAttributesByName(DIRECTORY_2_ID, "admin");
        assertThat(user2, userWithNameAndEmail("Super Man", "Admin@example.com"));
    }

    @Test
    public void renamePrincipalOverMultipleInternalDirectoriesShouldOnlyRenameOneUser() throws Exception
    {
        Application application = applicationManager.findByName("crowd");

        // We have an "admin" user in both directories - assert the preconditions
        UserWithAttributes user1 = directoryManager.findUserWithAttributesByName(DIRECTORY_1_ID, "admin");
        assertThat(user1, userWithNameAndEmail("Super Man", "Admin@example.com"));
        UserWithAttributes user2 = directoryManager.findUserWithAttributesByName(DIRECTORY_2_ID, "admin");
        assertThat(user2, userWithNameAndEmail("Super Man", "Admin@example.com"));

        // Rename the user
        applicationService.renameUser(application, "admin", "admin2");

        // Should be renamed in directory 1
        user1 = directoryManager.findUserWithAttributesByName(DIRECTORY_1_ID, "admin2");
        assertThat(user1, userWithNameAndEmail("Super Man", "Admin@example.com"));

        // The user in directory 2 should remain the same as before
        user2 = directoryManager.findUserWithAttributesByName(DIRECTORY_2_ID, "admin");
        assertThat(user2, userWithNameAndEmail("Super Man", "Admin@example.com"));
    }

    @Test
    public void testAddPrincipalAttributeOverMultipleInternalDirectories() throws Exception
    {
        Application application = applicationManager.findByName("crowd");

        // We have an "admin" user in both directories - assert the preconditions
        UserWithAttributes user1 = directoryManager.findUserWithAttributesByName(DIRECTORY_1_ID, "admin");
        assertEquals(0, user1.getValues("key").size());
        UserWithAttributes user2 = directoryManager.findUserWithAttributesByName(DIRECTORY_2_ID, "admin");
        assertEquals(0, user2.getValues("key").size());

        Set<String> testValues = new HashSet<String>();
        testValues.add("val1");
        testValues.add("val2");
        Map<String, Set<String>> testAttributes = new TreeMap<String, Set<String>>();
        testAttributes.put("key", testValues);

        // user "admin" is in both internal directories
        applicationService.storeUserAttributes(application, "admin", testAttributes);

        // Attributes should be updated in directory 1
        user1 = directoryManager.findUserWithAttributesByName(DIRECTORY_1_ID, "admin");
        Set<String> savedValues = user1.getValues("key");
        assertEquals(2, savedValues.size());
        assertTrue(savedValues.contains("val1"));
        assertTrue(savedValues.contains("val2"));

        // The user in directory 2 should remain the same as before
        user2 = directoryManager.findUserWithAttributesByName(DIRECTORY_2_ID, "admin");
        assertEquals(0, user2.getValues("key").size());
    }

    @Test
    public void testAddGroupOverMultipleInternalDirectories() throws Exception
    {
        Application application = applicationManager.findByName("crowd");

        List<DirectoryMapping> directoryMappings = application.getDirectoryMappings();

        GroupTemplateWithAttributes testGroup = new GroupTemplateWithAttributes("testgroup", -1L, GroupType.GROUP);
        testGroup.setDescription("test description");
        testGroup.setActive(true);

        applicationService.addGroup(application, testGroup);

        for (DirectoryMapping directoryMapping : directoryMappings)
        {
            GroupWithAttributes savedGroup = directoryManager.findGroupWithAttributesByName(directoryMapping.getDirectory().getId(), "testgroup");

            assertEquals(testGroup.getName(), savedGroup.getName());
            assertEquals(testGroup.getDescription(), savedGroup.getDescription());
            assertEquals(testGroup.isActive(), savedGroup.isActive());
        }
    }

    @Test
    public void testStoreAndRemoveUserAttributes() throws Exception
    {
        final Application application = applicationManager.findByName("demo");

        applicationService.storeUserAttributes(application, "testuser",
                Collections.singletonMap("testKey", Collections.singleton("testValue")));
        applicationService.removeUserAttributes(application, "testuser", "testKey");
    }

    @Test
    public void testStoreAndRemoveGroupAttributes() throws Exception
    {
        final Application application = applicationManager.findByName("demo");

        applicationService.storeGroupAttributes(application, "testgroup",
                Collections.singletonMap("testKey", Collections.singleton("testValue")));
        applicationService.removeGroupAttributes(application, "testgroup", "testKey");
    }

    public void setApplicationService(ApplicationService applicationService)
    {
        this.applicationService = applicationService;
    }

    public void setApplicationManager(ApplicationManager applicationManager)
    {
        this.applicationManager = applicationManager;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }

    public void setDataSource(DataSource dataSource)
    {
        this.dataSource = dataSource;
    }

    private static Matcher<User> userWithNameAndEmail(final String name, final String email)
    {
        return allOf(hasProperty("displayName", is(name)), hasProperty("emailAddress", is(email)));
    }
}
