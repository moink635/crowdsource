package com.atlassian.crowd.acceptance.tests.persistence;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.hsqldb.HsqldbDataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.hamcrest.collection.IsEmptyCollection;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceUtils;

import static org.junit.Assert.assertThat;

/**
 * A helper with methods and constants used for persistence/DAO integration tests
 *
 * @since v2.7
 */
public class PersistenceTestHelper
{
    public static final String TEST_DATA_XML = "sample-data.xml";

    public static void populateDatabase(String dataFileName, JdbcTemplate jdbcTemplate)
        throws DatabaseUnitException, IOException, SQLException
    {
        DataSource ds = jdbcTemplate.getDataSource();
        Connection con = DataSourceUtils.getConnection(ds);
        IDatabaseConnection dbUnitCon = new DatabaseConnection(con);

        DatabaseConfig config = dbUnitCon.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new HsqldbDataTypeFactory());

        InputStream datasetStream = com.atlassian.core.util.ClassLoaderUtils.getResourceAsStream(dataFileName, PersistenceTestHelper.class);
        IDataSet dataSet = new FlatXmlDataSetBuilder().build(datasetStream);
        try
        {
            DatabaseOperation.CLEAN_INSERT.execute(dbUnitCon, dataSet);
        }
        finally
        {
            DataSourceUtils.releaseConnection(con, ds);
        }
    }

    public static void fixHiLo(ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper)
    {
        // Fix up the hi-lo stuff so we can insert attributes etc with the correct primary key
        List<String> errors = new ArrayList<String>();
        resetableHiLoGeneratorHelper.setNextHiValue(errors);
        assertThat(errors, IsEmptyCollection.emptyCollectionOf(String.class));
    }
}
