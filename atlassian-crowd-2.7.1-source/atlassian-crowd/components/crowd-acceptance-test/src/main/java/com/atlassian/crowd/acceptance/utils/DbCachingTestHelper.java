package com.atlassian.crowd.acceptance.utils;

import com.atlassian.crowd.manager.directory.SynchronisationMode;
import junit.framework.AssertionFailedError;
import net.sourceforge.jwebunit.junit.WebTester;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.lang3.time.DurationFormatUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ResourceBundle;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Helper class for testing DB caching directories.
 *
 * @since v2.1
 */
public class DbCachingTestHelper
{
    private static final Logger LOGGER = LoggerFactory.getLogger(DbCachingTestHelper.class); 

    private static final String SYNC_DIRECTORY_BUTTON_ID = "synchroniseDirectoryButton";

    private static final String CURRENT_SYNC_INFO_ELEM_ID = "currentsyncinfo";

    protected static final String LAST_SYNC_INFO_ELEM_ID = "lastsyncinfo";

    private static final long ONE_SEC_IN_MILLIS = 1000L;

    public static final long DEFAULT_MAX_SYNC_WAIT_TIME_MS = 60000L; // 1 minute

    private final WebTester tester;
    private final ResourceBundle i18n;

    public DbCachingTestHelper(final WebTester tester)
    {
        this.tester = tester;
        i18n = ResourceBundle.getBundle(tester.getTestContext().getResourceBundleName());
    }

    /**
     * Synchronises the directory and waits until the synchronisation has finished.
     *
     * @param directoryName name of the directory to synchronise
     * @param maxWaitTimeMs maximum time in milliseconds to wait for the synchronisation to finish
     * @return SyncInfo
     * @throws RuntimeException if the synchronisation doesn't finish within <tt>maxWaitTimeMs</tt> milliseconds.
     */
    public SyncInfo synchroniseDirectory(final String directoryName, final long maxWaitTimeMs)
    {
        return synchroniseDirectory(directoryName, maxWaitTimeMs, null);
    }

    /**
     * Synchronises the directory and waits until the synchronisation has finished.
     *
     * @param directoryName name of the directory to synchronise
     * @param maxWaitTimeMs maximum time in milliseconds to wait for the synchronisation to finish
     * @param shortDescription description about the synchronisation task
     * @return SyncInfo
     * @throws RuntimeException if the synchronisation doesn't finish within <tt>maxWaitTimeMs</tt> milliseconds.
     */
    public SyncInfo synchroniseDirectory(final String directoryName, final long maxWaitTimeMs, final String shortDescription)
    {
        gotoBrowseDirectories();
        tester.clickLinkWithExactText(directoryName);
        tester.assertKeyPresent("menu.viewdirectory.label", new Object[] { directoryName });

        syncAndWaitTillFinish(maxWaitTimeMs);
        // Sync finished - go fetch the sync duration
        String syncDuration = getElementTextById("duration");

        SynchronisationMode mode = null;
        final String synchronisationStatus = tester.getElementById("status").getTextContent();
        if (synchronisationStatus.equals(getText("directory.caching.sync.completed.FULL")))
        {
            mode = SynchronisationMode.FULL;
        }
        else if (synchronisationStatus.equals(getText("directory.caching.sync.completed.INCREMENTAL")))
        {
            mode = SynchronisationMode.INCREMENTAL;
        }
        else
        {
            LOGGER.warn("Synchronisation was not completed: {}\n{}", synchronisationStatus, tester.getPageSource());
        }

        if (StringUtils.isNotBlank(shortDescription))
        {
            LOGGER.info("Synchronisation took: {} ({})", syncDuration, shortDescription);
        }
        else
        {
            LOGGER.info("Synchronisation took: {}", syncDuration);
        }

        // If the sync has finished, the button should now be enabled (no value for disabled attribute)
        assertEquals("", tester.getElementAttributeByXPath("//input[@id='" + SYNC_DIRECTORY_BUTTON_ID + "']", "disabled"));
        return new SyncInfo(directoryName, syncDuration, mode);
    }

    /**
     * Synchronises the directory and waits until the synchronisation has finished.  Use as a convenience method for a
     * one-off synchronisation.  Otherwise instantiate an instance of <tt>DbCachingTestHelper</tt>.
     *
     * @param tester WebTester
     * @param directoryName name of the directory to synchronise
     * @param maxWaitTimeMs maximum time in milliseconds to wait for the synchronisation to finish
     * @throws RuntimeException if the synchronisation doesn't finish within <tt>DEFAULT_MAX_SYNC_WAIT_TIME_MS</tt> milliseconds.
     */
    public static SyncInfo synchroniseDirectory(final WebTester tester, final String directoryName, final long maxWaitTimeMs)
    {
        return new DbCachingTestHelper(tester).synchroniseDirectory(directoryName, maxWaitTimeMs);
    }

    /**
     * Synchronises the directory and waits until the synchronisation has finished.  Use as a convenience method for a
     * one-off synchronisation.  Otherwise instantiate an instance of <tt>DbCachingTestHelper</tt>.
     *
     * @param tester WebTester
     * @param directoryName name of the directory to synchronise
     * @param maxWaitTimeMs maximum time in milliseconds to wait for the synchronisation to finish
     * @param shortDescription description about the synchronisation task
     * @throws RuntimeException if the synchronisation doesn't finish within <tt>DEFAULT_MAX_SYNC_WAIT_TIME_MS</tt> milliseconds.
     */
    public static SyncInfo synchroniseDirectory(final WebTester tester, final String directoryName, final long maxWaitTimeMs, final String shortDescription)
    {
        return new DbCachingTestHelper(tester).synchroniseDirectory(directoryName, maxWaitTimeMs, shortDescription);
    }

    /**
     * Creates a partial sync task.  Typical usage would be:
     *
     * <code>
     *     DbCachingTestHelper dbCachingTestHelper = new DbCachingTestHelper(tester);
     *     dbCachingTestHelper.syncDirectoryTask("ApacheDS).
     *         maxWaitMs(1000L).
     *         shortDescription("Adding 10 users").
     *         sync();
     * </code>
     * @param directoryName name of the directory to synchronise
     * @return PartialSyncTask
     */
    public PartialSyncTask syncDirectoryTask(final String directoryName)
    {
        Validate.notEmpty(directoryName);
        return new PartialSyncTask(directoryName);
    }

    /**
     * Clicks on the synchronise button and waits until the synchronisation finishes.  Polls every 1 second to check if
     * the synchronisation has finished.
     *
     * @param maxWaitTimeMs maximum time in milliseconds to wait for the synchronisation to finish
     * @throws RuntimeException if the synchronisation doesn't finish within <tt>maxWaitTimeMs</tt> milliseconds.
     */
    private void syncAndWaitTillFinish(final long maxWaitTimeMs)
    {
        tester.setScriptingEnabled(true);

        if (isSynchronising())
        {
            fail("Synchronisation has already been started.");
        }

        tester.clickButton(SYNC_DIRECTORY_BUTTON_ID);

        // Need to reload the page as jwebunit seems to not handle the js in the button synchronously.
        tester.gotoPage(tester.getTestingEngine().getPageURL().toString());

        for (long now = System.currentTimeMillis(), end = System.currentTimeMillis() + maxWaitTimeMs; now < end; now = System.currentTimeMillis())
        {
            if (!isSynchronising())
            {
                return; // Synchronisation has completed
            }

            try
            {
                Thread.sleep(ONE_SEC_IN_MILLIS); // poll every second
            }
            catch (InterruptedException e)
            {
                throw new RuntimeException(e);
            }
        }

        fail("Sync did not complete within the maximum wait time: " +
                DurationFormatUtils.formatDurationWords(maxWaitTimeMs, true, false));
    }

    /**
     * Returns <tt>true</tt> if the directory is currently being synchronised.
     *
     * @return <tt>true</tt> if the directory is currently being synchronised
     */
    public boolean isSynchronising()
    {
        // 'Last Synchronised' header is hidden while synchronising
        final String style = tester.getElementById("lastSyncLabel").getAttribute("style");
        return style.equals("display: none");
    }

    private void gotoBrowseDirectories()
    {
        tester.gotoPage("/console/secure/directory/browse.action");
    }

    private String getElementTextById(String elementId)
    {
        try
        {
            return tester.getElementById(elementId).getTextContent();
        }
        catch (AssertionFailedError e)
        {
            System.err.print(tester.getPageSource());
            throw e;
        }
    }

    private String getText(String key)
    {
        return i18n.getString(key);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /**
     * Uses the Builder pattern to create a synchronisation task.
     */
    public class PartialSyncTask
    {
        private String directoryName;
        private long maxWaitMs;
        private String shortDescription;

        /**
         * @param directoryName Name of the directory to synchronise.
         */
        public PartialSyncTask(final String directoryName)
        {
            this.directoryName = directoryName;
            maxWaitMs = DEFAULT_MAX_SYNC_WAIT_TIME_MS;
            shortDescription = "";
        }

        /**
         * Sets the maximum time in milliseconds to wait for synchronisation to finish.
         *
         * @param maxWaitMs maximum wait time in milliseconds
         */
        public PartialSyncTask maxWaitMs(final long maxWaitMs)
        {
            this.maxWaitMs = maxWaitMs;
            return this;
        }

        /**
         * Sets a short description of the synchronisation task.
         *
         * @param shortDescription of the synchronisation task.
         */
        public PartialSyncTask shortDescription(final String shortDescription)
        {
            this.shortDescription = shortDescription;
            return this;
        }

        /**
         * Synchronises the directory and waits until the synchronisation has finished.  Use as a convenience method for a
         * one-off synchronisation.  Otherwise instantiate an instance of <tt>DbCachingTestHelper</tt>.
         *
         * @return SyncInfo
         * @throws RuntimeException if the synchronisation doesn't finish within the specified maximum wait time (or
         * default time if no max wait time was set).
         */
        public SyncInfo sync()
        {
            return synchroniseDirectory(this.directoryName, this.maxWaitMs, this.shortDescription);
        }
    }

    /**
     * Information about the synchronisation.
     */
    public static class SyncInfo
    {
        private final String directoryName;
        private final String duration;
        private final SynchronisationMode mode;

        private SyncInfo(final String directoryName, final String duration, SynchronisationMode mode)
        {
            this.directoryName = directoryName;
            this.duration = duration;
            this.mode = mode;
        }

        /**
         * Returns of the name of the synchronised directory.
         *
         * @return name of synchronised directory
         */
        public String getDirectoryName()
        {
            return directoryName;
        }

        /**
         * Returns the duration of the synchronisation.
         *
         * @return duration of the synchronisation.
         */
        public String getDuration()
        {
            return duration;
        }

        /**
         * Returns the mode of the synchronisation.
         *
         * @return mode of the synchronisation
         */
        public SynchronisationMode getMode()
        {
            return mode;
        }
    }
}
