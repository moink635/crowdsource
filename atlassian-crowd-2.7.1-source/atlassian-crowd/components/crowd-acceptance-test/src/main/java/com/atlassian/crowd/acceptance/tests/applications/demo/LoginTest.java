package com.atlassian.crowd.acceptance.tests.applications.demo;

public class LoginTest extends DemoAcceptanceTestCase
{
    public void setUp() throws Exception
    {
        super.setUp();
        restoreCrowdFromXML("singlesignontest.xml");
    }

    public void testLogin()
    {
        useApp();

        log("Running testLogin");

        // force logoff
        _logout();

        assertKeyPresent("login.title");
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", ADMIN_PW);
        submit();

        // quick verification
        assertLinkPresentWithKey("menu.logout.label");
    }

    public void testLoginBadCredentials()
    {
        log("Running testLogin");

        // force logoff
        _logout();

        assertKeyPresent("login.title");
        setTextField("username", CROWD_ADMIN_USER);
        setTextField("password", "bogus");
        submit();

        // quick verification
        assertKeyPresent("invalidlogin.label");
    }

    public void testLoginWithNoAuthenticationAccess()
    {
        log("Running testLogin");

        // force logoff
        _logout();

        assertKeyPresent("login.title");
        setTextField("username", CROWD_USER);
        setTextField("password", CROWD_PW);
        submit();

        // quick verification
        assertKeyPresent("invalidlogin.label");
    }
}
