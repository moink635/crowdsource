package com.atlassian.crowd.acceptance.tests;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Tests for {@link TestDataState}.
 */
public class TestDataStateTest
{
    private URL baseUrl;

    @Before
    public void initUrl() throws Exception
    {
        baseUrl = new URL("http://www.example.com/");
    }

    @Test
    public void nothingIsRestoredInitially() throws Exception
    {
        assertFalse(new TestDataState().isRestoredXml(baseUrl, "filename.xml"));
    }

    @Test
    public void dataIsReportedAsRestoredAfterARestore() throws Exception
    {
        TestDataState tds = new TestDataState();

        tds.setRestoredXml(baseUrl, "filename.xml");

        assertTrue(tds.isRestoredXml(baseUrl, "filename.xml"));
    }

    @Test
    public void differentDataIsReportedAsNotRestoredAfterARestore() throws Exception
    {
        TestDataState tds = new TestDataState();

        tds.setRestoredXml(baseUrl, "filename.xml");

        assertFalse(tds.isRestoredXml(baseUrl, "different-filename.xml"));
    }

    @Test
    public void dataIsNoLongerReportedAsRestoredAfterModification() throws Exception
    {
        TestDataState tds = new TestDataState();

        tds.setRestoredXml(new URL("http://www.example.com/"), "filename.xml");

        tds.intendToModify(new URL("http://www.example.com/"));

        assertFalse(tds.isRestoredXml(new URL("http://www.example.com/"), "filename.xml"));
    }
}
