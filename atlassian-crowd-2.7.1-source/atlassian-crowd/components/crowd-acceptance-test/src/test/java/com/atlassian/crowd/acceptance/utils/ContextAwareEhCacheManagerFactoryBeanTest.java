package com.atlassian.crowd.acceptance.utils;

import java.io.IOException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;

import net.sf.ehcache.CacheException;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Status;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class ContextAwareEhCacheManagerFactoryBeanTest
{
    @Mock private ApplicationContext applicationContext1;
    @Mock private ApplicationContext applicationContext2;

    @Test
    public void shouldBePossibleToInstantiateTwoConcurrentNonSharedCacheManagers() throws Exception
    {
        when(applicationContext1.getId()).thenReturn("appContext1");
        when(applicationContext2.getId()).thenReturn("appContext2");

        ContextAwareEhCacheManagerFactoryBean factory1 = createFactory(applicationContext1);
        ContextAwareEhCacheManagerFactoryBean factory2 = createFactory(applicationContext2);

        CacheManager cacheManager1 = factory1.getObject();
        CacheManager cacheManager2 = factory2.getObject();

        assertNotSame("CacheManager should not be shared at JVM level", cacheManager1, cacheManager2);

        assertEquals("appContext1", cacheManager1.getName());
        assertEquals("appContext2", cacheManager2.getName());

        factory1.destroy();
        factory2.destroy();
    }

    private static ContextAwareEhCacheManagerFactoryBean createFactory(ApplicationContext applicationContext) throws IOException
    {
        ContextAwareEhCacheManagerFactoryBean factory = new ContextAwareEhCacheManagerFactoryBean();
        factory.setApplicationContext(applicationContext);
        factory.setConfigLocation(new ClassPathResource("/crowd-ehcache.xml"));
        factory.setShared(false);
        factory.afterPropertiesSet();
        return factory;
    }

    @Test
    public void sharedCacheManagersAcrossFactoriesAreDestroyedAlongWithAnyFactory() throws CacheException, IOException
    {
        EhCacheManagerFactoryBean factory1 = new EhCacheManagerFactoryBean();
        factory1.setConfigLocation(new ClassPathResource("/crowd-ehcache.xml"));
        factory1.setShared(true);
        factory1.afterPropertiesSet();

        EhCacheManagerFactoryBean factory2 = new EhCacheManagerFactoryBean();
        factory2.setConfigLocation(new ClassPathResource("/crowd-ehcache.xml"));
        factory2.setShared(true);
        factory2.afterPropertiesSet();

        /* Both factories are alive */
        assertEquals(Status.STATUS_ALIVE, factory1.getObject().getStatus());
        assertEquals(Status.STATUS_ALIVE, factory2.getObject().getStatus());

        /* They share a CacheManager */
        assertSame(factory1.getObject(), factory2.getObject());

        /* Shutting one down... */
        factory1.destroy();
        assertEquals(Status.STATUS_SHUTDOWN, factory1.getObject().getStatus());

        /* ...also destroys the cache the other uses. */
        assertEquals(Status.STATUS_SHUTDOWN, factory2.getObject().getStatus());
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    /**
     * Demonstrate EhCache's problematic behaviour, preventing duplicate anonymous CacheManagers
     * within a JVM.
     */
    @Test
    public void cacheManagersWithoutUniqueNamesCauseFailure()
    {
        CacheManager cm1 = new CacheManager();

        try
        {
            thrown.expect(CacheException.class);
            thrown.expectMessage("Another unnamed CacheManager already exists in the same VM.");

            new CacheManager();
        }
        finally
        {
            cm1.shutdown();
        }
    }
}
