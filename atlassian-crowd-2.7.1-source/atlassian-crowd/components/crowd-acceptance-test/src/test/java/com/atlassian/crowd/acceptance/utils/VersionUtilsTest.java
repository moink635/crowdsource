package com.atlassian.crowd.acceptance.utils;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class VersionUtilsTest
{
    @Test
    public void testCompareVersions()
    {
        assertTrue(VersionUtils.compareVersions("1.0", "1.0") == 0);
        assertTrue(VersionUtils.compareVersions("1.1", "1.0") > 0);
        assertTrue(VersionUtils.compareVersions("1.0", "1.1") < 0);
        assertTrue(VersionUtils.compareVersions("1.0.1", "1.0") > 0);
        assertTrue(VersionUtils.compareVersions("1.0", "1.0.1") < 0);
        assertTrue(VersionUtils.compareVersions("1.10", "1.2") > 0);
        assertTrue(VersionUtils.compareVersions("1.1-beta1", "1.0") > 0);
        assertTrue(VersionUtils.compareVersions("1.0", "1.1-beta1") < 0);
        assertTrue(VersionUtils.compareVersions("1.0-beta1", "1.1") < 0);
        assertTrue(VersionUtils.compareVersions("1.1", "1.0-beta1") > 0);
        assertTrue(VersionUtils.compareVersions("1.0", "1.0-beta1") == 0);
        assertTrue(VersionUtils.compareVersions("1.0-beta1", "1.0") == 0);
        assertTrue(VersionUtils.compareVersions("1.0", "1.0.0") == 0);
        assertTrue(VersionUtils.compareVersions("1.0.0", "1.0") == 0);
    }
}
