package com.atlassian.crowd.integration.soap;

import com.atlassian.crowd.integration.authentication.PasswordCredential;

/**
 * A wrapper class, which encapsulates the SOAPPrincipal and PasswordCredential.
 *
 */
public class SOAPPrincipalWithCredential
{
    private SOAPPrincipal principal;
    private PasswordCredential passwordCredential;

    public SOAPPrincipalWithCredential()
    {

    }


    public SOAPPrincipalWithCredential(SOAPPrincipal principal, PasswordCredential passwordCredential)
    {
        this.principal = principal;
        this.passwordCredential = passwordCredential;
    }

    public SOAPPrincipal getPrincipal()
    {
        return principal;
    }

    public PasswordCredential getPasswordCredential()
    {
        return passwordCredential;
    }

    public void setPrincipal(SOAPPrincipal principal)
    {
        this.principal = principal;
    }

    public void setPasswordCredential(PasswordCredential passwordCredential)
    {
        this.passwordCredential = passwordCredential;
    }

}