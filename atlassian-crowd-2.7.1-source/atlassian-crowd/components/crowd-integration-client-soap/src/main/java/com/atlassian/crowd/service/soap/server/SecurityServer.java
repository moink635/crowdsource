/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.service.soap.server;

import com.atlassian.crowd.integration.authentication.PasswordCredential;
import com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.integration.exception.ApplicationPermissionException;
import com.atlassian.crowd.integration.exception.BulkAddFailedException;
import com.atlassian.crowd.integration.exception.ExpiredCredentialException;
import com.atlassian.crowd.integration.exception.InactiveAccountException;
import com.atlassian.crowd.integration.exception.InvalidAuthenticationException;
import com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.integration.exception.InvalidCredentialException;
import com.atlassian.crowd.integration.exception.InvalidEmailAddressException;
import com.atlassian.crowd.integration.exception.InvalidGroupException;
import com.atlassian.crowd.integration.exception.InvalidRoleException;
import com.atlassian.crowd.integration.exception.InvalidTokenException;
import com.atlassian.crowd.integration.exception.InvalidUserException;
import com.atlassian.crowd.integration.exception.ObjectNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPCookieInfo;
import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SOAPNestableGroup;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.soap.SOAPPrincipalWithCredential;
import com.atlassian.crowd.integration.soap.SOAPRole;
import com.atlassian.crowd.integration.soap.SearchRestriction;

import java.rmi.RemoteException;

/**
 * <p>
 * Atlassian Crowd SOAP security services interface. All methods, other than the <code>authenticateApplication</code>
 * must first authenticate before an API call can be made. The {@link com.atlassian.crowd.model.authentication.AuthenticatedToken application token}
 * may be reused for more than one call. If the application token expires, an
 * {@link InvalidAuthorizationTokenException} exception will be thrown and the client will need to re-authenticate.
 * </p>
 */
public interface SecurityServer {

    /**
     * Authenticates a principal without validating a password.
     *
     * @param authenticationContext The application authentication details.
     * @param username The username to create an authenticate token for.
     * @param validationFactors The known attributes of the user to use when creating a token, such as their remote IP address and user-agent.
     * @return The principal's authentication token.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthenticationException An invalid authentication occurred.
     * @throws InvalidAuthorizationTokenException An invalid authentication occurred.
     * @throws InactiveAccountException The principal's account is inactive.
     * @throws ApplicationAccessDeniedException User does not have access to authenticate against application
     */
    String createPrincipalToken(com.atlassian.crowd.integration.authentication.AuthenticatedToken authenticationContext, String username, com.atlassian.crowd.integration.authentication.ValidationFactor[] validationFactors)
            throws RemoteException, InvalidAuthenticationException, InvalidAuthorizationTokenException, InactiveAccountException, ApplicationAccessDeniedException;

    /**
     * Authenticates a principal without SSO details utilizing centralized authentication only.
     *
     * @param authenticationContext The application authentication details.
     * @param username The username of the principal.
     * @param password The password credential.
     * @return The principal's authentication token.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthenticationException An invalid authentication occurred.
     * @throws InvalidAuthorizationTokenException An invalid authentication occurred.
     * @throws InactiveAccountException The principal's account is inactive.
     * @throws ExpiredCredentialException     The user's credentials have expired.  The user must change their credentials in order to successfully authenticate.
     * @throws ApplicationAccessDeniedException User does not have access to authenticate against application
     */
    String authenticatePrincipalSimple(com.atlassian.crowd.integration.authentication.AuthenticatedToken authenticationContext, String username, String password)
            throws RemoteException, InvalidAuthenticationException, InvalidAuthorizationTokenException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException;

    /**
     * Authenticates an application client to the Crowd security server.
     *
     * @param authenticationContext The application authentication details.
     * @return The application's authenticated token that will be reused for operations verses the security server.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthenticationException An invalid authentication occurred.
     * @throws InvalidAuthorizationTokenException An invalid authentication occurred.
     */
    com.atlassian.crowd.integration.authentication.AuthenticatedToken authenticateApplication(com.atlassian.crowd.integration.authentication.ApplicationAuthenticationContext authenticationContext)
            throws RemoteException, InvalidAuthenticationException, InvalidAuthorizationTokenException;

    /**
     * Authenticates a principal verses the calling who is in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param authenticateContext The principal's authentication details.
     * @return The principal's authenticated token.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException The principal's authentication details were invalid.
     * @throws InactiveAccountException The principal's account is not active.
     * @throws ExpiredCredentialException     The user's credentials have expired.  The user must change their credentials in order to successfully authenticate.
     * @throws ApplicationAccessDeniedException User does not have access to authenticate against application
     */
    String authenticatePrincipal(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken,
                                        com.atlassian.crowd.integration.authentication.UserAuthenticationContext authenticateContext)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException;

    /**
     * Checks if the principal's current token is still valid.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principalToken The token to check.
     * @param validationFactors The known identity factors used when creating the principal's token.
     * @return <code>true</code> if and only if the token is active, otherwise <code>false</code>.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ApplicationAccessDeniedException User does not have access to authenticate against application
     */
    boolean isValidPrincipalToken(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principalToken,
                                         com.atlassian.crowd.integration.authentication.ValidationFactor[] validationFactors)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationAccessDeniedException;

    /**
     * Invalidates a token for all integrated applications. If the token is later validated, the token will not be found valid.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param token The token to invalidate.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    void invalidatePrincipalToken(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String token)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Searches for groups that are in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param searchRestrictions The search restrictions to use when performing this search.
     * @return The search results.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    SOAPGroup[] searchGroups(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SearchRestriction[] searchRestrictions)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Searches for principals that are in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param searchRestrictions The search restrictions to use when performing this search.
     * @return The search results.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    SOAPPrincipal[] searchPrincipals(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SearchRestriction[] searchRestrictions)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Searches for roles that are in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param searchRestrictions The search restrictions to use when performing this search.
     * @return The search results.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    SOAPRole[] searchRoles(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SearchRestriction[] searchRestrictions)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Adds a group to the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param group The group to add.
     * @return The populated details after the add of the group to the directory server.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidGroupException An error occurred adding the group to the directory server.
     * @throws ApplicationPermissionException The application does not have the proper permissions to add the entity to the directory server.
     */
    SOAPGroup addGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SOAPGroup group)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidGroupException, ApplicationPermissionException;

    /**
     * Updates the first group located from the list of directories assigned to an application
     * Available fields that can be updated are <code>description</code> and <code>active</code>
     *
     * @param authenticatedToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param group The name of the group to update.
     * @param description the new description of the group.
     * @param active the new active flag for the group.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity to the directory server.
     * @throws ObjectNotFoundException no groups matching the supplied name is found.
     */
    void updateGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken authenticatedToken, String group, String description, boolean active) throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, ObjectNotFoundException;


    /**
     * Find a group by name for the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param name The name of the group.
     * @return The group object.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException Unable to find the specific group.
     */
    SOAPGroup findGroupByName(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String name)
            throws RemoteException, InvalidAuthorizationTokenException, ObjectNotFoundException;

     /**
     * Find a group by name for the application's assigned directory.
     *
     * This will retrieve the group and all its attributes.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param name The name of the group.
     * @return The group object.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException Unable to find the specific group.
     */
    SOAPGroup findGroupWithAttributesByName(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String name)
            throws RemoteException, InvalidAuthorizationTokenException, ObjectNotFoundException;

    /**
     * Adds a role to the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param role The name of the role.
     * @return The role object.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidRoleException An error occurred adding the role to the directory server.
     * @throws ApplicationPermissionException The application does not have the proper permissions to add the entity to the directory server.
     * @deprecated
     */
    SOAPRole addRole(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SOAPRole role)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidRoleException, ApplicationPermissionException;

    /**
     * Finds a role by name for the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param name The name of the role.
     * @return The role object.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException Unable to find the specified role.
     * @deprecated
     */
    SOAPRole findRoleByName(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String name)
            throws RemoteException, InvalidAuthorizationTokenException, ObjectNotFoundException;

    /**
     * Finds a principal by token.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param key The principal's token.
     * @return The principal object.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidTokenException Unable to find the specified token.
     */
    SOAPPrincipal findPrincipalByToken(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String key)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidTokenException;

    /**
     * Updates an attribute for a principal who is in the application's assigned directory..
     *
     * Note: This is the same as calling <code>addAttributeToPrincipal</code>
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param name The name of the principal.
     * @param attribute The name of the attribute to update.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException Unable to find the specified principal.
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     */
    void updatePrincipalAttribute(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String name, SOAPAttribute attribute)
            throws RemoteException, InvalidAuthorizationTokenException , ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Updates an attribute for a group that is in the application's assigned directory..
     *
     * Note: This is the same as calling <code>addAttributeToGroup</code>
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param group The name of the group.
     * @param attribute The name of the attribute to update.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException Unable to find the specified group.
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     */
    void updateGroupAttribute(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String group, SOAPAttribute attribute)
            throws RemoteException, InvalidAuthorizationTokenException , ApplicationPermissionException, ObjectNotFoundException;
    /**
     * Finds a principal by name who is in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param name The name of the principal.
     * @return The principal object.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException Unable to find the specified principal.
     */
    SOAPPrincipal findPrincipalByName(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String name)
            throws RemoteException, InvalidAuthorizationTokenException, ObjectNotFoundException;

    /**
     * Finds a principal by name who is in the application's assigned directory.
     *
     * This will retrieve the principal and all its attributes.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param name The name of the principal.
     * @return The principal object.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException Unable to find the specified principal.
     */
    SOAPPrincipal findPrincipalWithAttributesByName(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String name)
            throws RemoteException, InvalidAuthorizationTokenException, ObjectNotFoundException;

    /**
     * Adds a principal to the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principal The populated principal object to added.
     * @param credential The password for the principal.
     * @return The principal object.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidCredentialException The supplied password is invalid.
     * @throws InvalidUserException The supplied principal is invalid.
     * @throws ApplicationPermissionException The application does not have the proper permissions to add the entity to the directory server.
     */
    SOAPPrincipal addPrincipal(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SOAPPrincipal principal, PasswordCredential credential)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidCredentialException, InvalidUserException, ApplicationPermissionException;

    /**
     * Adds principals to the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principals Array of SOAPPrincipalWithCredential
     *
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws ApplicationPermissionException thrown when no Create User Permission for any of the directories.
     * @throws BulkAddFailedException throw when it failed to create a user in of the directories.
     */
    void addAllPrincipals(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SOAPPrincipalWithCredential[] principals)
            throws InvalidAuthorizationTokenException, RemoteException, ApplicationPermissionException, BulkAddFailedException;

    /**
     * Adds a principal to a group for the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principal The name of the principal.
     * @param group The name of the group.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException      the user or group could not be found
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     */
    void addPrincipalToGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, String group)
            throws RemoteException, InvalidAuthorizationTokenException , ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Updates the password credential for a principal who is in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principal The name of the principal.
     * @param credential The password.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException Unable to find the specified principal.
     * @throws InvalidCredentialException The supplied password is invalid.
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     */
    void updatePrincipalCredential(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, PasswordCredential credential)
            throws RemoteException, InvalidAuthorizationTokenException , InvalidCredentialException, ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Resets a principal's password credential to a random password and emails the new password who is in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principal The name of the principal.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidCredentialException Unable to reset the principal's password.
     * @throws ObjectNotFoundException Unable to find the specified principal.
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     * @throws InvalidEmailAddressException invalid email address
     */
    void resetPrincipalCredential(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal)
            throws RemoteException, InvalidEmailAddressException, InvalidAuthorizationTokenException, InvalidCredentialException , ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Removes a group from the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param group The name of the group.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException Unable to find the specified group.
     * @throws ApplicationPermissionException The application does not have the proper permissions to remove the entity from the directory server.
     */
    void removeGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String group)
            throws RemoteException, InvalidAuthorizationTokenException , ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Removes a role from the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param role The name of the role.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException The specified role is invalid.
     * @throws ApplicationPermissionException The application does not have the proper permissions to remove the entity from the directory server.
     */
    void removeRole(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String role)
            throws RemoteException, InvalidAuthorizationTokenException , ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Removes a principal from the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principal The name of the principal.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException The specified principal is invalid.
     * @throws ApplicationPermissionException The application does not have the proper permissions to remove the entity from the directory server.
     */
    void removePrincipal(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal)
            throws RemoteException, InvalidAuthorizationTokenException , ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Adds the principal to a role for the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principal The name of the principal.
     * @param role The name of the role.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException Unable to get user the specified user or group (of type Role) by name
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity to the directory server.
     */
    void addPrincipalToRole(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, String role)
            throws RemoteException, InvalidAuthorizationTokenException , ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Checks if a principal is a member of a group for the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param group The name of the group.
     * @param principal The name of the principal.
     * @return <code>true</code> if and only if the principal is a group member, otherwise <code>false</code>.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    boolean isGroupMember(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String group, String principal)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Checks if a principal is a member of a role for the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param role The name of the role.
     * @param principal The name of the principal.
     * @return <code>true</code> if and only if the principal is a role member, otherwise <code>false</code>.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    boolean isRoleMember(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String role, String principal)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Removes a principal from a group for the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principal The name of the principal.
     * @param group The name of the group.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws ObjectNotFoundException if the user, group or membership could not be found
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     */
    void removePrincipalFromGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, String group)
            throws InvalidAuthorizationTokenException, RemoteException, ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Removes a principal from a role for the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principal The name of the principal.
     * @param role The name of the role.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws ObjectNotFoundException if the specified user, group (of type Role) or membership could not be found
     * @throws ApplicationPermissionException The application does not have the proper permissions to remove the entity from the directory server.
     */
    void removePrincipalFromRole(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, String role)
            throws InvalidAuthorizationTokenException, RemoteException, ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Adds an attribute to a principal who is in the application's assigned directory.
     *
     * Note: This is the same as calling <code>updatePrincipalAttribute </code>
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principal The name of the principal.
     * @param attribute The name attribute to add.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException The specified principal is invalid.
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     */
    void addAttributeToPrincipal(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, SOAPAttribute attribute)
            throws RemoteException, InvalidAuthorizationTokenException , ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Adds an attribute to a group that is in the application's assigned directory.
     *
     * Note: This is the same as calling <code>updateGroupAttribute </code>
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param group The name of the group.
     * @param attribute The name attribute to add.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException if the specified group is invalid.
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     */
    void addAttributeToGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String group, SOAPAttribute attribute)
            throws RemoteException, InvalidAuthorizationTokenException , ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Removes an attribute from a principal who is in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principal The name of the principal.
     * @param attribute The name of the attribute.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException The specified principal is invalid.
     * @throws ApplicationPermissionException The application does not have the proper permissions to remove the entity from the directory server.
     */
    void removeAttributeFromPrincipal(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, String attribute)
            throws RemoteException, InvalidAuthorizationTokenException , ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Removes an attribute from a group that is in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param group The name of the group.
     * @param attribute The name of the attribute.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException The specified group is not found.
     * @throws ApplicationPermissionException The application does not have the proper permissions to remove the entity from the directory server.
     */
    void removeAttributeFromGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String group, String attribute)
            throws RemoteException, InvalidAuthorizationTokenException , ApplicationPermissionException, ObjectNotFoundException;

    /**
     * Gets the amount of time a client should cache security information from the Crowd server.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @return The cache time in minutes.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @deprecated This method is now implemented by the <code>crowd-ehcache.xml</code> configuration file.
     */
    long getCacheTime(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Checks if the client application should cache security information from the Crowd server.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @return <code>true</code> if and only if the cache is enabled, otherwise <code>false</code>.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    boolean isCacheEnabled(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * This will return the domain configured in Crowd or null if no domain has been set.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @return the domain to set the SSO cookie for, or null
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     *
     * @deprecated This method has been superceded by {@link SecurityServer#getCookieInfo(com.atlassian.crowd.integration.authentication.AuthenticatedToken)}
     */
    String getDomain(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Finds all of the principals who are visable in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @return The names of all known principals.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    String[] findAllPrincipalNames(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Finds all of the groups who are visible in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @return A {@link String} listing of the group names.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    String[] findAllGroupNames(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Finds all of the groups who are visible in the application's assigned directory. The groups will have their
     * application's direct sub-groups populated. Principals will not be populated.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @return A {@link SOAPNestableGroup} listing of the groups, plus any direct sub-groups.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    SOAPNestableGroup[] findAllGroupRelationships(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Finds all of the roles who are visible in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @return A {@link String} listing of the role names.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    String[] findAllRoleNames(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken)
            throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Finds all of the principals who are members of a group that is in the application's assigned directory. This call does not resolve nesting.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principalName The name of the principal to use when performing the lookup.
     * @return A {@link String} listing of the principal's group memberships.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException Will never throw this exception. Left in to keep compatibility with Crowd 2.0.x.
     */
    String[] findGroupMemberships(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principalName)
            throws RemoteException, InvalidAuthorizationTokenException, ObjectNotFoundException;

    /**
     * Finds all of the principals who are members of a role that is in the application's assigned directory.
     *
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @param principalName The name of the principal to use role performing the lookup.
     * @return A {@link String} listing of the principal's group memberships.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ObjectNotFoundException Will never throw this exception. Left in to keep compatibility with Crowd 2.0.x.
     */
    String[] findRoleMemberships(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principalName)
            throws RemoteException, InvalidAuthorizationTokenException, ObjectNotFoundException;

    /**
     * Will return the List of group names that have been given access to connect to the application
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @return a list of group names that are associated to the application represented by the application token
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    String[] getGrantedAuthorities(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken) throws RemoteException, InvalidAuthorizationTokenException;

    /**
     * Returns information needed to set the SSO cookie correctly.
     * @param applicationToken The application's authentication token. Obtained from the <code>authenticateApplication</code> method.
     * @return An object with lots of tasty configuration information
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken} is invalid.
     */
    SOAPCookieInfo getCookieInfo(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken)
            throws RemoteException, InvalidAuthorizationTokenException;
}