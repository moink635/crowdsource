package com.atlassian.crowd.service.soap.client;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.BulkAddFailedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidEmailAddressException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.InvalidRoleException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPCookieInfo;
import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SOAPNestableGroup;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.soap.SOAPPrincipalWithCredential;
import com.atlassian.crowd.integration.soap.SOAPRole;
import com.atlassian.crowd.integration.soap.SearchRestriction;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;

import java.rmi.RemoteException;
import java.util.Collection;

/**
 * An interface to communicate with the Crowd Security Server.
 *
 * @see SecurityServerClientFactory for a singleton factory implementation.
 */

public interface SecurityServerClient
{
    /**
     * Authenticate the client using the application name and password
     * from crowd.properties.
     * <p/>
     * NOTE: this will perform an explicit authentication call to
     * the server. There is no reason to directly call this as all
     * the methods will automatically attempt to authenticate when
     * required.
     *
     * @throws RemoteException there was a problem communicating with the Crowd Security Server.
     * @throws InvalidAuthorizationTokenException the newly created application token is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid. Ensure the application.password
     * in crowd.properties matches the one defined in the Crowd Console.
     */
    public void authenticate()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Authenticates a principal who is in the application's assigned directory.
     *
     * @param userAuthenticationContext The principal's authentication details.
     * @return The principal's authenticated token.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException
     *                                  The principal's authentication details were invalid.
     * @throws InactiveAccountException
     *                                  The principal's account is not active.
     * @throws ApplicationAccessDeniedException
     *                                  if the user does not have access to this application.
     * @throws ExpiredCredentialException The user's credentials have expired.
     */
    String authenticatePrincipal(UserAuthenticationContext userAuthenticationContext)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException,
            InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException;

    /**
     * Checks if the principal's current token is still valid.
     *
     * @param principalToken    The token to check.
     * @param validationFactors The known identity factors used when creating the principal's token.
     * @return <code>true</code> if and only if the token is active, otherwise <code>false</code>.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ApplicationAccessDeniedException
     *                                  if the user does not have access to this application.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    boolean isValidToken(String principalToken, ValidationFactor[] validationFactors)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException;

    /**
     * Invalidates a principal token for all integrated applications. If the token is later validated, the token will not be found valid.
     *
     * @param token The token to invalidate.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void invalidateToken(String token)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Searches for groups that are in the application's assigned directory.
     *
     * @param searchRestrictions The search restrictions to use when performing this search.
     * @return The search results.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPGroup[] searchGroups(SearchRestriction[] searchRestrictions)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Searches for principals that are in the application's assigned directory.
     *
     * @param searchRestrictions The search restrictions to use when performing this search.
     * @return The search results.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPPrincipal[] searchPrincipals(SearchRestriction[] searchRestrictions)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Searches for roles that are in the application's assigned directory.
     *
     * @param searchRestrictions The search restrictions to use when performing this search.
     * @return The search results.
     * @throws RemoteException An unknown remote exception occured.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @deprecated
     */
    SOAPRole[] searchRoles(SearchRestriction[] searchRestrictions)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Retrieves all groups in the application's assigned directories.
     *
     * @return all groups in the application's assigned directories.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPGroup[] findAllGroups()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Retrieves all roles in the application's assigned directories.
     *
     * @return all roles in the application's assigned directories.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @deprecated
     */
    SOAPRole[] findAllRoles() throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Retrieves all principals in the application's assigned directories.
     *
     * @return all principals in the application's assigned directories.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPPrincipal[] findAllPrincipals()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Adds a group to the application's assigned directory.
     *
     * @param group The group to add.
     * @return The populated details after the add of the group to the directory server.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidGroupException
     *                                  An error occured adding the group to the directory server.
     * @throws ApplicationPermissionException
     *                                  The application does not have the proper permissions to add the entity to the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPGroup addGroup(SOAPGroup group)
            throws RemoteException, InvalidGroupException, InvalidAuthorizationTokenException, ApplicationPermissionException, InvalidAuthenticationException;

    /**
     * Updates the first group located from the list of directories assigned to an application
     * Available fields that can be updated are <code>description</code> and <code>active</code>
     *
     * @param group       The name of the group to update.
     * @param description the new description of the group.
     * @param active      the new active flag for the group.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ApplicationPermissionException
     *                                  The application does not have the proper permissions to update the entity to the directory server.
     * @throws GroupNotFoundException   no groups matching the supplied name is found.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void updateGroup(String group, String description, boolean active) throws RemoteException, GroupNotFoundException,
            ApplicationPermissionException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Find a group by name for the application's assigned directory.
     *
     * @param name The name of the group.
     * @return The group object.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws GroupNotFoundException   Unable to find the specific group.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPGroup findGroupByName(String name)
            throws RemoteException, InvalidAuthorizationTokenException, GroupNotFoundException, InvalidAuthenticationException;

    /**
     * Find a group by name for the application's assigned directory.
     * <p/>
     * This will retrieve the group and all its attributes.
     *
     * @param name The name of the group.
     * @return The group object.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws GroupNotFoundException   Unable to find the specific group.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPGroup findGroupWithAttributesByName(String name)
            throws RemoteException, InvalidAuthorizationTokenException, GroupNotFoundException, InvalidAuthenticationException;

    /**
     * Adds a role to the application's assigned directory.
     *
     * @param role The name of the role.
     * @return The role object.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidRoleException
     *                                  An error occured adding the role to the directory server.
     * @throws ApplicationPermissionException
     *                                  The application does not have the proper permissions to add the entity to the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @deprecated
     */
    SOAPRole addRole(SOAPRole role)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidRoleException, ApplicationPermissionException, InvalidAuthenticationException;

    /**
     * Finds a role by name for the application's assigned directory.
     *
     * @param name The name of the role.
     * @return The role object.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws GroupNotFoundException   Unable to find the specified role.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @deprecated
     */
    SOAPRole findRoleByName(String name)
            throws RemoteException, InvalidAuthorizationTokenException, GroupNotFoundException, InvalidAuthenticationException;

    /**
     * Finds a principal by token.
     *
     * @param key The principal's token.
     * @return The principal object.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidTokenException Unable to find the specified token.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPPrincipal findPrincipalByToken(String key)
            throws RemoteException, InvalidTokenException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Updates an attribute for a principal who is in the application's assigned directory.
     * <p/>
     * Note: This is the same as calling <code>addAttributeToPrincipal </code>
     *
     * @param name      The name of the principal.
     * @param attribute The name of the attribute to update.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws UserNotFoundException    Unable to find the specified principal.
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void updatePrincipalAttribute(String name, SOAPAttribute attribute)
            throws RemoteException, UserNotFoundException, ApplicationPermissionException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Updates an attribute for a group that is in the application's assigned directory.
     * <p/>
     * Note: This is the same as calling <code>addAttributeToGroup</code>
     *
     * @param name      The name of the group.
     * @param attribute The name of the attribute to update.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws GroupNotFoundException   Unable to find the specified principal.
     * @throws ApplicationPermissionException
     *                                  The application does not have the proper permissions to update the entity in the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void updateGroupAttribute(String name, SOAPAttribute attribute)
            throws RemoteException, GroupNotFoundException, ApplicationPermissionException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Finds a principal by name who is in the application's assigned directory.
     *
     * @param name The name of the principal.
     * @return The principal object.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws UserNotFoundException    Unable to find the specified principal.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPPrincipal findPrincipalByName(String name)
            throws RemoteException, InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException;

    /**
     * Finds a principal by name who is in the application's assigned directory.
     * <p/>
     * This will retrieve the principal and all its attributes.
     *
     * @param name The name of the principal.
     * @return The principal object.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws UserNotFoundException    Unable to find the specified principal.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPPrincipal findPrincipalWithAttributesByName(String name)
            throws RemoteException, UserNotFoundException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Adds principals to the application's assigned directory.
     *
     * @param principals Array of {@link com.atlassian.crowd.integration.soap.SOAPPrincipalWithCredential}
     *
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws ApplicationPermissionException thrown when no Create User Permission for any of the directories.
     * @throws BulkAddFailedException throw when it failed to create a user in of the directories.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    public void addAllPrincipals(Collection<SOAPPrincipalWithCredential> principals)
            throws InvalidAuthorizationTokenException, RemoteException, ApplicationPermissionException, BulkAddFailedException, InvalidAuthenticationException;

    /**
     * Adds a principal to the application's assigned directory.
     *
     * @param principal  The populated principal object to added.
     * @param credential The password for the principal.
     * @return The principal object.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws RemoteException An unknown remote exception occured.
     * @throws InvalidCredentialException The supplied password is invalid.
     * @throws InvalidUserException The supplied principal is invalid.
     * @throws ApplicationPermissionException The application does not have the proper permissions to add the entity to the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPPrincipal addPrincipal(SOAPPrincipal principal, PasswordCredential credential)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidCredentialException, InvalidUserException, ApplicationPermissionException, InvalidAuthenticationException;

    /**
     * Adds a principal to a group for the application's assigned directory.
     *
     * @param principal The name of the principal.
     * @param group     The name of the group.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws GroupNotFoundException   Unable to find group by name.
     * @throws UserNotFoundException    Unable to find user by name.
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void addPrincipalToGroup(String principal, String group)
            throws RemoteException, InvalidAuthorizationTokenException, UserNotFoundException, GroupNotFoundException, ApplicationPermissionException, InvalidAuthenticationException;

    /**
     * Updates the password credential for a principal who is in the application's assigned directory.
     *
     * @param principal  The name of the principal.
     * @param credential The password.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws UserNotFoundException    Unable to find the specified principal.
     * @throws InvalidCredentialException The supplied password is invalid.
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void updatePrincipalCredential(String principal, PasswordCredential credential)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidCredentialException, UserNotFoundException,
            ApplicationPermissionException, InvalidAuthenticationException;

    /**
     * Resets a principal's password credential to a random password and emails the new password who is in the application's assigned directory.
     *
     * @param principal The name of the principal.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidCredentialException Unable to reset the principal's password.
     * @throws UserNotFoundException    Unable to find the specified principal.
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @throws InvalidEmailAddressException invalid email address
     */
    void resetPrincipalCredential(String principal)
            throws RemoteException, InvalidEmailAddressException, InvalidCredentialException, UserNotFoundException,
            ApplicationPermissionException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Removes a group from the application's assigned directory.
     *
     * @param group The name of the group.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws GroupNotFoundException Unable to find the specified group.
     * @throws ApplicationPermissionException The application does not have the proper permissions to remove the entity from the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void removeGroup(String group)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, GroupNotFoundException, InvalidAuthenticationException;

    /**
     * Removes a role from the application's assigned directory.
     *
     * @param role The name of the role.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws GroupNotFoundException The specified role is invalid.
     * @throws ApplicationPermissionException The application does not have the proper permissions to remove the entity from the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @deprecated
     */
    void removeRole(String role)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, GroupNotFoundException, InvalidAuthenticationException;

    /**
     * Removes a principal from the application's assigned directory.
     *
     * @param principal The name of the principal.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws UserNotFoundException The specified principal is invalid.
     * @throws ApplicationPermissionException The application does not have the proper permissions to remove the entity from the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void removePrincipal(String principal)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException;

    /**
     * Adds the principal to a role for the application's assigned directory.
     *
     * @param principal The name of the principal.
     * @param role      The name of the role.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws UserNotFoundException Unable to user by name
     * @throws GroupNotFoundException Unable to group (of type Role) by name.
     * @throws ApplicationPermissionException
     *                                  The application does not have the proper permissions to update the entity to the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @deprecated
     */
    void addPrincipalToRole(String principal, String role)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, GroupNotFoundException, InvalidAuthenticationException;

    /**
     * Checks if a principal is a member of a group for the application's assigned directory.
     *
     * @param group     The name of the group.
     * @param principal The name of the principal.
     * @return <code>true</code> if and only if the principal is a group member, otherwise <code>false</code>.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    boolean isGroupMember(String group, String principal)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Checks if a principal is a member of a role for the application's assigned directory.
     *
     * @param role      The name of the role.
     * @param principal The name of the principal.
     * @return <code>true</code> if and only if the principal is a role member, otherwise <code>false</code>.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @deprecated
     */
    boolean isRoleMember(String role, String principal)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Removes a principal from a group for the application's assigned directory.
     *
     * @param principal The name of the principal.
     * @param group     The name of the group.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws UserNotFoundException Unable to find user by name
     * @throws GroupNotFoundException Unable to find group by name.
     * @throws ApplicationPermissionException The application does not have the proper permissions to update the entity in the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @throws MembershipNotFoundException Unable to find the membership
     */
    void removePrincipalFromGroup(String principal, String group)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, GroupNotFoundException, UserNotFoundException, MembershipNotFoundException, InvalidAuthenticationException;

    /**
     * Removes a principal from a role for the application's assigned directory.
     *
     * @param principal The name of the principal.
     * @param role      The name of the role.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws UserNotFoundException Unable to find user by name
     * @throws GroupNotFoundException Unable to find group (of type Role) by name.
     * @throws ApplicationPermissionException The application does not have the proper permissions to remove the entity from the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @throws MembershipNotFoundException Unable to find the membership
     * @deprecated
     */
    void removePrincipalFromRole(String principal, String role)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, GroupNotFoundException, MembershipNotFoundException, InvalidAuthenticationException;

    /**
     * Adds an attribute to a principal who is in the application's assigned directory.
     * <p/>
     * Note: This is the same as calling <code>updatePrincipalAttribute </code>
     *
     * @param principal The name of the principal.
     * @param attribute The name attribute to add.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws UserNotFoundException The specified principal is invalid.
     * @throws ApplicationPermissionException
     *                                  The application does not have the proper permissions to update the entity in the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void addAttributeToPrincipal(String principal, SOAPAttribute attribute)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException;

    /**
     * Removes an attribute from a principal who is in the application's assigned directory.
     *
     * @param principal The name of the principal.
     * @param attribute The name of the attribute.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws UserNotFoundException The specified principal is invalid.
     * @throws ApplicationPermissionException
     *                                  The application does not have the proper permissions to remove the entity from the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void removeAttributeFromPrincipal(String principal, String attribute)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException;

    /**
     * Adds an attribute to a group that is in the application's assigned directory.
     * </p>
     * Note: This is the same as calling <code>updateGroupAttribute </code>
     *
     * @param group     The name of the group.
     * @param attribute The name attribute to add.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws GroupNotFoundException The specified group is invalid.
     * @throws ApplicationPermissionException
     *                                  The application does not have the proper permissions to update the entity in the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void addAttributeToGroup(String group, SOAPAttribute attribute)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, GroupNotFoundException, InvalidAuthenticationException;

    /**
     * Removes an attribute from a group that is in the application's assigned directory.
     * <p/>
     * Note: This is the same as calling <code>updateGroupAttribute </code>
     *
     * @param group     The name of the group.
     * @param attribute The name of the attribute.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws GroupNotFoundException The specified principal is invalid.
     * @throws ApplicationPermissionException
     *                                  The application does not have the proper permissions to remove the entity from the directory server.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void removeAttributeFromGroup(String group, String attribute)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, GroupNotFoundException, InvalidAuthenticationException;

    /**
     * Gets the amount of time a client should cache security information from the Crowd server.
     *
     * @return The cache time in minutes.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @deprecated This method is now implemented by the <code>crowd-ehcache.xml</code> configuration file.
     */
    long getCacheTime() throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Checks if the client application should cache security information from the Crowd server.
     *
     * @return <code>true</code> if and only if the cache is enabled, otherwise <code>false</code>.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    boolean isCacheEnabled() throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * This will return the domain configured in Crowd or null if no domain has been set.
     *
     * @return the domain to set the SSO cookie for, or null
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    String getDomain() throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Finds all of the principals who are visible in the application's assigned directory.
     *
     * @return The names of all known principals.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    String[] findAllPrincipalNames()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Finds all of the groups who are visible in the application's assigned directory.
     *
     * @return A {@link String} listing of the group names.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    String[] findAllGroupNames()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Finds all of the groups who are visible in the application's assigned directory. The groups will have their
     * application's direct sub-groups populated. Principals will not be populated.
     *
     * @return A {@link SOAPNestableGroup} listing of the groups, plus any direct sub-groups.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                         The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPNestableGroup[] findAllGroupRelationships()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Finds all of the roles who are visible in the application's assigned directory.
     *
     * @return A {@link String} listing of the role names.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @deprecated
     */
    String[] findAllRoleNames()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Finds all the groups the principal is a direct member of. This call does not resolve nesting.
     *
     * @param principalName The name of the principal to use when performing the lookup.
     * @return A {@link String} listing of the principal's group memberships.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws UserNotFoundException  The principal was not found
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    String[] findGroupMemberships(String principalName)
            throws RemoteException, InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException;

    /**
     * Finds all the roles the principal is a member of.
     *
     * @param principalName The name of the principal to use role performing the lookup.
     * @return A {@link String} listing of the principal's group memberships.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                                  The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws UserNotFoundException  The principal was not found
     * @throws InvalidAuthenticationException application authentication is not valid
     * @deprecated
     */
    String[] findRoleMemberships(String principalName)
            throws RemoteException, InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException;

    /**
     * Authenticates a principal without SSO details utilizing centralized authentication only.
     *
     * @param username The username of the principal.
     * @param password The password credential.
     * @return The principal's authentication token.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthenticationException An invalid authentication occurred.
     * @throws InvalidAuthorizationTokenException An invalid authentication occurred.
     * @throws InactiveAccountException The principal's account is inactive.
     * @throws ApplicationAccessDeniedException user does not have access to the application.
     * @throws ExpiredCredentialException The user's credentials have expired.  The user must change their credentials in order to successfully authenticate.
     */
    String authenticatePrincipalSimple(String username, String password)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException;

    /**
     * Authenticates a principal without validating a password.
     *
     * @param username          The username to create an authenticate token for.
     * @param validationFactors The known attributes of the user to use when creating a token, such as their remote IP address and user-agent.
     * @return The principal's authentication token.
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthenticationException An invalid authentication occurred.
     * @throws InvalidAuthorizationTokenException An invalid authentication occurred.
     * @throws InactiveAccountException The principal's account is inactive.
     * @throws ApplicationAccessDeniedException user does not have access to authenticate against application
     */
    String createPrincipalToken(String username, ValidationFactor[] validationFactors)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException, InactiveAccountException, ApplicationAccessDeniedException;

    /**
     * Will return the List of group names that have been given access to connect to the application
     *
     * @return a String[] of group names
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException An invalid authentication occurred.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    String[] getGrantedAuthorities()
            throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException;

    /**
     * Returns information needed to set the SSO cookie correctly.
     *
     * @return An object with lots of tasty configuration information
     * @throws RemoteException An unknown remote exception occurred.
     * @throws InvalidAuthorizationTokenException
     *                         The calling application's {@link com.atlassian.crowd.model.authentication.AuthenticatedToken applicationToken} is invalid.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPCookieInfo getCookieInfo()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Retrieve the SOAP client properties used to connect to the Crowd Security Server.
     *
     * @return client properties used.
     */
    SoapClientProperties getSoapClientProperties();
}
