package com.atlassian.crowd.service.cache;

/**
 * Allows access to 'meta' cache functionality. Provided so we can implement the OSUser UserProvider.flushCaches()
 * method sensibly.
 */
public interface CacheExpiryManager
{
    /**
     * Expires/flushes/clears all the caches.
     */
    void flush();
}
