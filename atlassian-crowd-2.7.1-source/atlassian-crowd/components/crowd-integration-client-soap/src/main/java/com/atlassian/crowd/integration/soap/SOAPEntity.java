package com.atlassian.crowd.integration.soap;

import org.apache.commons.lang3.builder.ToStringBuilder;
import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static com.atlassian.crowd.embedded.impl.IdentifierUtils.equalsInLowerCase;

import java.io.Serializable;
import java.util.Date;

public abstract class SOAPEntity implements Serializable
{
    /**
     * The unique identifier for the entity.
     */
    protected long ID = -1;

    /**
     * The unique name of the entity for the directory entity
     */
    protected String name;

    /**
     * The crowd server internal directory tracking ID.
     */
    protected long directoryId;

    /**
     * Description of the entity.
     */
    protected String description;

    /**
     * If the entity is active or not.
     */
    protected boolean active;

    /**
     * The time entity was created.
     */
    protected Date conception;

    /**
     * The time entity was last modification.
     */
    protected Date lastModified;

    /**
     * The entity attributes.
     */
    protected SOAPAttribute[] attributes;

    protected SOAPEntity()
    {
    }

    protected SOAPEntity(final long ID, final String name, final long directoryId, final String description, final boolean active, final Date conception, final Date lastModified, final SOAPAttribute[] attributes)
    {
        this.ID = ID;
        this.name = name;
        this.directoryId = directoryId;
        this.description = description;
        this.active = active;
        this.conception = conception;
        this.lastModified = lastModified;
        this.attributes = attributes;
    }

    public SOAPAttribute[] getAttributes()
    {
        return attributes;
    }

    public SOAPAttribute getAttribute(final String attributeName)
    {
        SOAPAttribute soapAttribute = null;
        if (attributes != null && attributes.length > 0 && attributeName != null)
        {
            for (int i = 0; i < attributes.length; i++)
            {
                SOAPAttribute attribute = attributes[i];
                if (attributeName.equals(attribute.getName()))
                {
                    soapAttribute = attribute;
                    break;
                }
            }
        }
        return soapAttribute;
    }

    public void setAttributes(SOAPAttribute[] attributes)
    {
        this.attributes = attributes;
    }

    public Date getConception()
    {
        return conception;
    }

    public void setConception(Date conception)
    {
        this.conception = conception;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public long getDirectoryId()
    {
        return directoryId;
    }

    public void setDirectoryId(long directoryId)
    {
        this.directoryId = directoryId;
    }

    public long getID()
    {
        return ID;
    }

    public void setID(long ID)
    {
        this.ID = ID;
    }

    public Date getLastModified()
    {
        return lastModified;
    }

    public void setLastModified(Date lastModified)
    {
        this.lastModified = lastModified;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        final SOAPEntity that = (SOAPEntity) o;

        // convert to lowercase because most directories allow upper/lower for authentication
        return !(name != null ? !equalsInLowerCase(name, that.name) : that.name != null);
    }

    public int hashCode()
    {
        return (name != null ? toLowerCase(name).hashCode() : 0);
    }

    public String toString()
    {
        return new ToStringBuilder(this).
                append("ID", ID).
                append("name", name).
                append("directoryID", directoryId).
                append("active", active).
                append("conception", conception).
                append("lastModified", lastModified).
                append("attributes", attributes).toString();
    }
}