package com.atlassian.crowd.util;

import com.atlassian.crowd.integration.soap.SOAPNestableGroup;
import com.atlassian.crowd.service.cache.BasicCache;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Encapsulates nasty logic related to making sure that all membership information returned to client applications
 * is nesting-aware. The methods are named to match where they're called from in the GroupManager &
 * GroupMembershipsManager.
 * <p/>
 * Takes care of getting information in & out of the caches.
 */
public class NestingHelper
{
    private static final Logger logger = LoggerFactory.getLogger(NestingHelper.class);

    /**
     * Takes a list of all groups, along with their direct children, and returns a list of group names. Also caches
     * group relationship data that's needed for other calls, such as {@link com.atlassian.crowd.service.GroupMembershipManager#getMemberships(String)}.
     *
     * @param allGroups The result of a call to {@link com.atlassian.crowd.service.soap.client.SecurityServerClient#findAllGroupRelationships()}.
     * @param cache     The GroupCache to add the name & relationship data to.
     * @return list of all group names
     */
    public static List<String> cacheGroupRelationships(SOAPNestableGroup[] allGroups, BasicCache cache)
    {
        Assert.notNull(allGroups);

        List<String> allGroupNames = new ArrayList<String>(allGroups.length);
        for (SOAPNestableGroup group : allGroups)
        {
            allGroupNames.add(group.getName());
        }
        cache.cacheAllGroupNames(allGroupNames);

        cacheAncestors(allGroups, cache);

        return allGroupNames;
    }

    /**
     * Saves ancestors group names by group name in cache.
     *
     * @param allGroups result of a call to {@link com.atlassian.crowd.service.soap.client.SecurityServerClient#findAllGroupRelationships()}
     * @param cache GroupCache to add the name & relationship data to
     */
    private static void cacheAncestors(SOAPNestableGroup[] allGroups, BasicCache cache)
    {
        final Map<String, Set<String>> ancestorsByGroup = new HashMap<String, Set<String>>(allGroups.length);

        final Map<String, Set<String>> parentsByGroup = getParentsByGroup(allGroups);
        for (SOAPNestableGroup group : allGroups)
        {
            final String groupName = group.getName();
            final Set<String> ancestors = new HashSet<String>();
            addAncestors(parentsByGroup, groupName, ancestors, groupName);
            ancestors.remove(groupName);
            ancestorsByGroup.put(groupName, ancestors);
        }

        cache.cacheAncestorsForGroups(ancestorsByGroup);
    }

    /**
     * Builds a group name -> parent group names map.
     *
     * @param allGroups result of a call to {@link com.atlassian.crowd.service.soap.client.SecurityServerClient#findAllGroupRelationships()}
     * @return map of parent groups names by group name
     */
    private static Map<String, Set<String>> getParentsByGroup(SOAPNestableGroup[] allGroups)
    {
        final Map<String, Set<String>> parentsByGroup = new HashMap<String, Set<String>>(allGroups.length);

        // Add entry for each group
        for (SOAPNestableGroup group : allGroups)
        {
            parentsByGroup.put(group.getName(), new HashSet<String>());
        }

        // Add parents for groups
        for (SOAPNestableGroup parentGroup : allGroups)
        {
            if (parentGroup.getGroupMembers() != null)
            {
                for (String groupName : parentGroup.getGroupMembers())
                {
                    Set<String> parents = parentsByGroup.get(groupName);
                    if (parents != null)
                    {
                        parents.add(parentGroup.getName());
                    }
                    else
                    {
                        logger.warn("Unknown group '" + groupName + "' referenced as a child of '" + parentGroup.getName() + "'");
                    }
                }
            }
        }

        return parentsByGroup;
    }

    /**
     * Populates {@code ancestors} parameter with all the ancestors of a group.
     *
     * @param parentsByGroup map of parent group names by group name
     * @param group group to find the ancestors for
     * @param ancestors groups that are ancestors of the group
     * @param ancestorGroup name of the currently processed group
     */
    private static void addAncestors(Map<String, Set<String>> parentsByGroup, String group, Set<String> ancestors, String ancestorGroup)
    {
        final Set<String> parents = parentsByGroup.get(ancestorGroup);
        for (String parent : parents)
        {
            if (!ancestors.contains(parent) && !group.equals(parent)) // Avoid loops
            {
                ancestors.add(parent);
                addAncestors(parentsByGroup, group, ancestors, parent);
            }
        }
    }

    /**
     * Returns an unsorted list of groups that the user is a member of, either directly or indirectly.
     *
     * @param directGroups list of groups that the user is a direct member of
     * @param ancestorsByGroup map of ancestor group names by group name
     * @return unsorted list of groups that the user is a member of
     */
    public static List<String> getAllGroupsForUser(List<String> directGroups, Map<String, Set<String>> ancestorsByGroup)
    {
        Assert.notNull(directGroups, "directGroups");
        Assert.notNull(ancestorsByGroup, "ancestorsByGroup");

        final Set<String> allGroups = new HashSet<String>(directGroups);

        for (String directGroup : directGroups)
        {
            final Set<String> ancestors = ancestorsByGroup.get(directGroup);
            if (ancestors != null)
            {
                allGroups.addAll(ancestors);
            }
            else
            {
                logger.warn("Could not find group ancestor information for group '" + directGroup + "' in cache.");
            }
        }

        return new ArrayList<String>(allGroups);
    }

}
