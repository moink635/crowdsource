package com.atlassian.crowd.integration.exception;

import com.atlassian.crowd.integration.model.DirectoryEntity;

/**
 * Base class of directory entity (e.g. group, user, role) exceptions.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public abstract class InvalidDirectoryEntityException extends CheckedSoapException
{
    private DirectoryEntity entity;

    public InvalidDirectoryEntityException()
    {
        // needed for xfire
        super();
    }

    public InvalidDirectoryEntityException(DirectoryEntity entity, Throwable cause)
    {
        this(entity, "", cause);
    }

    public InvalidDirectoryEntityException(DirectoryEntity entity, String message)
    {
        this(entity, message, null);
    }

    public InvalidDirectoryEntityException(DirectoryEntity entity, String message, Throwable cause)
    {
        super(message, cause);
        this.entity = entity;
    }

    public void setEntity(DirectoryEntity entity)
    {
        this.entity = entity;
    }

    public DirectoryEntity getEntity()
    {
        return entity;
    }
}
