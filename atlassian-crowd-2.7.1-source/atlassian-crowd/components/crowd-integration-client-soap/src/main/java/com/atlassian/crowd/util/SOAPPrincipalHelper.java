package com.atlassian.crowd.util;

import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.model.user.UserConstants;

/**
 * A helper to retrieve attributes from SOAPPrincipal
 */
public class SOAPPrincipalHelper
{
    /**
     * Retrieves the first name from the specified user
     * @param principal SOAPPrincipal
     * @return the user's first name
     */
    public String getFirstName(SOAPPrincipal principal)
    {
        return getFirstAttributeValue(UserConstants.FIRSTNAME, principal);
    }

    /**
     * Retrieves the last name from the specified user
     * @param principal SOAPPrincipal
     * @return the user's last name
     */
    public String getLastName(SOAPPrincipal principal)
    {
        return getFirstAttributeValue(UserConstants.LASTNAME, principal);
    }

    /**
     * Retrieves the email address from the specified user
     * @param principal SOAPPrincipal
     * @return the user's email address
     */
    public String getEmail(SOAPPrincipal principal)
    {
        return getFirstAttributeValue(UserConstants.EMAIL, principal);
    }

    /**
     * Retrieves the full name from the specified user
     * @param principal SOAPPrincipal
     * @return the user's full name (display naame)
     */
    public String getFullName(SOAPPrincipal principal)
    {
        return getFirstAttributeValue(UserConstants.DISPLAYNAME, principal);
    }

    /**
     * Retrieves the first attribute from the specified SOAPAttribute belonging to the user
     * @param name the name of the SOAPAttribute
     * @param principal SOAPPrincipal
     * @return the first value of the specified SOAPAttribute. <tt>null</tt> if SOAPAttribute does not exist or does not contain any values
     */
    public String getFirstAttributeValue(String name, SOAPPrincipal principal)
    {
        SOAPAttribute attribute = getAttribute(name, principal);
        if (attribute != null && attribute.getValues() != null && attribute.getValues().length > 0)
        {
            return attribute.getValues()[0];
        }
        else
        {
            return null;
        }
    }

    /**
     * Retrieves the SOAPAttribute belonging to the user
     * @param name name of the SOAPAttribute
     * @param principal SOAPPrincipal
     * @return the SOAPAttribute belonging to the principal. <tt>null</tt> if no attribute is found
     */
    public SOAPAttribute getAttribute(String name, SOAPPrincipal principal)
    {
        SOAPAttribute[] attributes = principal.getAttributes();

        if (attributes != null)
        {
            for (int i = 0; i < attributes.length; i++)
            {
                if (attributes[i].getName().equals(name))
                {
                    return attributes[i];
                }
            }
        }

        // no attribute found
        return null;
    }
}
