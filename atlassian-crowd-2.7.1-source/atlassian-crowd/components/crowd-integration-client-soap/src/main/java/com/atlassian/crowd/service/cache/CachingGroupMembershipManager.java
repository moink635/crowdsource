package com.atlassian.crowd.service.cache;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.service.GroupManager;
import com.atlassian.crowd.service.GroupMembershipManager;
import com.atlassian.crowd.service.UserManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.util.Assert;
import com.atlassian.crowd.util.NestingHelper;
import org.apache.commons.collections.CollectionUtils;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class handles group memberships, but also handles some funky translation
 */
public class CachingGroupMembershipManager implements GroupMembershipManager
{
    private final Server server;
    private final GroupManager groupManager;
    private final UserManager userManager;
    private final BasicCache basicCache;

    public CachingGroupMembershipManager(final SecurityServerClient securityServerClient, final UserManager userManager, final GroupManager groupManager, BasicCache basicCache)
    {
        this.userManager = userManager;
        this.groupManager = groupManager;
        this.basicCache = basicCache;
        this.server = new Server(securityServerClient);
    }

    public boolean isMember(String userName, String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        Assert.notNull(userName);
        Assert.notNull(groupName);

        // TODO: we should add a step 2.5 to check the user cache before making the call to the server

        // Algo:
        //  1- Check if we have a cache for this query.
        //  2- If not, see if we have membership data for this group. Cache isMember data if so.
        //  3- If not, make isMember query to server.

        // Other options:
        //   - Always reload the group->users cache and use that.

        Boolean isMember = basicCache.isMember(userName, groupName);
        if (isMember == null)
        {
            isMember = basicCache.isMemberInGroupCache(userName, groupName);
            if (isMember == null)
            {
                isMember = server.isMember(userName, groupName);
            }
            basicCache.setMembership(userName, groupName, isMember);
        }

        return isMember.booleanValue();
    }

    public void addMembership(String userName, String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, InvalidAuthenticationException, UserNotFoundException, GroupNotFoundException
    {
        Assert.notNull(userName);
        Assert.notNull(groupName);

        server.addMembership(userName, groupName);          // will throw if there's a problem

        // Prime the caches, so we do have memberhips
        getMemberships(userName);
        getMembers(groupName);

        basicCache.setMembership(userName, groupName, Boolean.TRUE);

        basicCache.addUserToGroup(userName, groupName);
        basicCache.addGroupToUser(userName, groupName);
    }

    public void removeMembership(String userName, String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, MembershipNotFoundException, UserNotFoundException, InvalidAuthenticationException, GroupNotFoundException
    {
        Assert.notNull(userName);
        Assert.notNull(groupName);

        server.removeMembership(userName, groupName);          // will throw if there's a problem

        basicCache.setMembership(userName, groupName, Boolean.FALSE); // also updates the allmemberships list on this interface

        basicCache.removeUserFromGroup(userName, groupName);
        basicCache.removeGroupFromUser(userName, groupName);
    }

    public List/*<String>*/ getMemberships(String userName)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException, UserNotFoundException
    {
        Assert.notNull(userName);

        List/*<String>*/ groupNames = basicCache.getAllMemberships(userName);
        if (groupNames == null)
        {
            List<String> directGroups = server.getMemberships(userName);
            if (CollectionUtils.isEmpty(directGroups))
            {
                groupNames = directGroups;
            }
            else
            {
                groupNames = NestingHelper.getAllGroupsForUser(directGroups, getAncestorsForGroups());
            }

            basicCache.cacheAllMemberships(userName, groupNames);
        }

        // convert the names from potentially lower-case to correct-case
        if (groupNames != null)
        {
            groupNames = getCorrectGroupNames(groupNames);
        }

        if (groupNames == null || groupNames.size() == 0)
        {
            // contract says we return an empty List when there are no memberships.
            return new ArrayList(1); // can't return Collections.EMPTY_LIST as this list may need to be mutable
        }

        Collections.sort(groupNames);

        return groupNames;
    }

    /**
     * Also see {@link CachingGroupManager#getGroup(String)} for very similar logic.
     */
    public List/*<String>*/ getMembers(String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException, GroupNotFoundException
    {
        Assert.notNull(groupName);

        // Get the group, then returns its members.
        List/*<String>*/ users = basicCache.getAllMembers(groupName);
        if (users == null)
        {
            SOAPGroup group = basicCache.getGroup(groupName);
            if (group == null)
            {
                group = server.getGroup(groupName);

                users = basicCache.setMembers(groupName, group.getMembers());

                basicCache.cacheGroup(group);
            }
        }

        // convert the names from potentially lower-case to correct-case
        if (users != null)
        {
            users = getCorrectUserNames(users);
        }

        if (users == null)
        {
            users = new ArrayList(1); // can't return Collections.EMPTY_LIST as this list may need to be mutable
        }

        return users;
    }

    public Boolean isUserOrGroup(String name)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException
    {
        Assert.notNull(name);

        Boolean isUserOrGroup = basicCache.isUserOrGroup(name);

        if (isUserOrGroup == null)
        {
            if (groupManager.isGroup(name) || userManager.isUser(name))
            {
                isUserOrGroup = Boolean.TRUE;
                basicCache.addIsUserOrGroup(name, Boolean.TRUE);
            }
            else
            {
                isUserOrGroup = Boolean.FALSE;
                basicCache.addIsUserOrGroup(name, Boolean.FALSE);
            }
        }

        return isUserOrGroup;
    }

    /**
     * Returns a map of ancestors by group from cache, populating it if necessary.
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                         The application (not the user) was not authenticated correctly.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @return a map of ancestors by group from cache
     */
    private Map<String, Set<String>> getAncestorsForGroups()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        Map<String, Set<String>> ancestorsByGroup = basicCache.getAncestorsForGroups();
        if (ancestorsByGroup == null)
        {
            // calls findAllGroupRelationships(), which will make sure we have child->parent relationships in the cache.
            groupManager.getAllGroupNames();
            ancestorsByGroup = basicCache.getAncestorsForGroups();
        }
        return ancestorsByGroup;
    }

    private List<String> getCorrectUserNames(List<String> names)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException
    {
        List<String> correctNames = new ArrayList<String>(names.size());
        for (String name : names)
        {
            try
            {
                correctNames.add(getCorrectUserName(name));
            }
            catch (UserNotFoundException e)
            {
                // this entity doesn't exist anymore, remove it from the list (ie. skip over it)
            }
        }
        return correctNames;
    }

    private List<String> getCorrectGroupNames(List<String> names)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException
    {
        List<String> correctNames = new ArrayList<String>(names.size());
        for (String name : names)
        {
            try
            {
                correctNames.add(getCorrectGroupName(name));
            }
            catch (GroupNotFoundException e)
            {
                // this entity doesn't exist anymore, remove it from the list (ie. skip over it)
            }
        }
        return correctNames;
    }

    private String getCorrectUserName(String name)
            throws InvalidAuthorizationTokenException, RemoteException, UserNotFoundException, InvalidAuthenticationException
    {
        String correctName = basicCache.getUserName(name);
        if (correctName == null)
        {
            SOAPPrincipal principal = userManager.getUser(name);
            correctName = principal.getName();
        }
        return correctName;
    }

    private String getCorrectGroupName(String name)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException, GroupNotFoundException
    {
        String correctName = basicCache.getGroupName(name);
        if (correctName == null)
        {
            SOAPGroup group = groupManager.getGroup(name);
            correctName = group.getName();
        }
        return correctName;
    }

    /**
     * Encapsulates communications with the Crowd server.
     */
    private static class Server
    {
        private final SecurityServerClient securityServerClient;

        private Server(SecurityServerClient securityServerClient)
        {
            this.securityServerClient = securityServerClient;
        }

        public boolean isMember(String userName, String groupName)
                throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
        {
            return securityServerClient.isGroupMember(groupName, userName);
        }

        public void addMembership(String userName, String groupName)
                throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException, GroupNotFoundException
        {
            securityServerClient.addPrincipalToGroup(userName, groupName);
        }

        public void removeMembership(String userName, String groupName)
                throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, MembershipNotFoundException, UserNotFoundException, InvalidAuthenticationException, GroupNotFoundException
        {
            securityServerClient.removePrincipalFromGroup(userName, groupName);
        }

        public List<String> getMemberships(String userName)
                throws RemoteException, InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException
        {
            String[] groupNames = securityServerClient.findGroupMemberships(userName);
            return Arrays.asList(groupNames);
        }

        public SOAPGroup getGroup(String groupName)
                throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException, GroupNotFoundException
        {
            return securityServerClient.findGroupByName(groupName);
        }
    }
}
