package com.atlassian.crowd.search;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.HashMap;
import java.util.Map;

public class SearchContext extends HashMap<String, Object>
{

    public static final String SEARCH_INDEX_START = "search.index.start";

    public static final String SEARCH_MAX_RESULTS = "search.max.results";

    public static final String APPLICATION_ACTIVE = "application.active";

    public static final String APPLICATION_GROUP_MAPPING = "application.group.mapping";

    public static final String APPLICATION_DIRECTORY_MAPPING = "application.directory.mapping";

    public static final String APPLICATION_NAME = "application.name";

    public static final String DIRECTORY_ACTIVE = "directory.active";

    public static final String DIRECTORY_TYPE = "directory.type";

    public static final String DIRECTORY_NAME = "directory.name";

    public static final String GROUP_ACTIVE = "group.active";

    public static final String GROUP_NAME = "group.name";

    public static final String GROUP_DN = "group.dn";

    public static final String GROUP_DIRECTORY_ID = "group.directory.id";

    public static final String GROUP_PRINCIPAL_MEMBER = "group.principal.member";

    public static final String GROUP_POPULATE_MEMBERSHIPS = "group.populate.memberships";

    public static final String GROUP_POPULATE_DIRECT_SUBGROUPS = "group.populate.direct.sub.groups";

    public static final String CONTAINER_POPULATE_DIRECT_MEMBER_DN = "container.populate.direct.member.dn";

    public static final String ROLE_ACTIVE = "role.active";

    public static final String ROLE_NAME = "role.name";

    public static final String ROLE_DIRECTORY_ID = "role.directory.id";

    public static final String ROLE_PRINCIPAL_MEMBER = "role.principal.memeber";

    public static final String ROLE_POPULATE_MEMBERSHIPS = "role.populate.memberships";

    public static final String PRINCIPAL_ACTIVE = "principal.active";

    public static final String PRINCIPAL_EMAIL = "principal.email";

    public static final String PRINCIPAL_NAME = "principal.name";

    public static final String PRINCIPAL_FULLNAME = "principal.fullname";

    public static final String PRINCIPAL_DIRECTORY_ID = "principal.directory.id";

    public static final String TOKEN_APPLICATION_ONLY = "token.application.only";

    public static final String TOKEN_SECRET_NUMBER = "token.secret.number";

    public static final String TOKEN_NAME = "token.account.name";

    public static final String TOKEN_EXPIRATION_DATE = "token.expiration.date";

    public static final String TOKEN_PRINCIPAL_DIRECTORY_ID = "token.principal.directory.id";

    public static final String TOKEN_PRINCIPAL_ONLY = "token.principal.only";

    public enum PopulateMemberships
    {
        NONE,
        DIRECT,
        ALL;

        // The enum may have been sent over SOAP as a String. This method also gives us 1.3-and-earlier compatibility
        //  as in that release the *_POPULATE_MEMBERSHIPS values were defined as Boolean.
        public static PopulateMemberships parseString(String type)
        {
            if (   "none".equalsIgnoreCase(type)
                || "false".equalsIgnoreCase(type))
            {
                return NONE;
            }
            else if ("direct".equalsIgnoreCase(type))
            {
                return DIRECT;
            }
            else if (   "all".equalsIgnoreCase(type)
                     || "true".equalsIgnoreCase(type))
            {
                return ALL;
            }
            else
            {
                throw new IllegalArgumentException("An invalid type (" + type + ") was passed to parseString()");
            }
        }
    }

    public SearchContext()
    {

    }

    public SearchContext(Map<String, Object> searchParameters)
    {
        super(searchParameters);
    }

    public String toString()
    {
        final ToStringBuilder builder = new ToStringBuilder(this);

        for (java.util.Iterator iterator = this.entrySet().iterator(); iterator.hasNext();)
        {
            Map.Entry entry = (Map.Entry) iterator.next();
            builder.append(entry);
        }

        return builder.toString();
    }
}
