package com.atlassian.crowd.integration.http;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.integration.http.util.CrowdHttpTokenHelper;
import com.atlassian.crowd.integration.http.util.CrowdHttpTokenHelperImpl;
import com.atlassian.crowd.integration.http.util.CrowdHttpValidationFactorExtractorImpl;
import com.atlassian.crowd.integration.soap.SOAPCookieInfo;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.model.authentication.CookieConfiguration;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.AuthenticationManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * This bean is used to manage HTTP authentication.
 * <p/>
 * It is the fundamental class for web/SSO authentication integration.
 * <p/>
 * This class contains many convenience methods for
 * authentication integration with existing applications.
 * For most applications, using the following methods will
 * be sufficient to achieve SSO:
 * <ol>
 * <li><code>authenticate:</code> authenticate a user.</li>
 * <li><code>isAuthenticated:</code> determine if a request is authenticated.</li>
 * <li><code>getPrincipal:</code> retrieve the principal for an authenticated request.</li>
 * <li><code>logoff:</code> sign the user out.</li>
 * </ol>
 * <p/>
 * Use the <code>HttpAuthenticatorFactory</code> to get an instance
 * of this class, or use an IoC container (like Spring) to manage this
 * class as a singleton.
 *
 * @see com.atlassian.crowd.service.soap.client.SecurityServerClient
 */
public class HttpAuthenticatorImpl implements HttpAuthenticator
{
    private static final Logger logger = LoggerFactory.getLogger(HttpAuthenticatorImpl.class);

    private final AuthenticationManager authenticationManager;

    private final CrowdHttpTokenHelper tokenHelper;

    /**
     * @param authenticationManager the client to use to talk to the Crowd Server.
     */
    public HttpAuthenticatorImpl(AuthenticationManager authenticationManager)
    {
        this.authenticationManager = authenticationManager;
        this.tokenHelper = CrowdHttpTokenHelperImpl.getInstance(CrowdHttpValidationFactorExtractorImpl.getInstance());
    }

    private void invalidateClient(HttpServletRequest request, HttpServletResponse response, String token)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Invalidating the Crowd token: " + token);
        }

        CookieConfiguration cookieConfig = null;
        if (response != null)
        {
            SOAPCookieInfo soapCookieInfo = getSecurityServerClient().getCookieInfo();
            cookieConfig = new CookieConfiguration(soapCookieInfo.getDomain(), soapCookieInfo.isSecure(), Constants.COOKIE_TOKEN_KEY);
        }
        tokenHelper.removeCrowdToken(request, response, getSoapClientProperties(), cookieConfig);
    }

    public void setPrincipalToken(HttpServletRequest request, HttpServletResponse response, String token)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("Setting the Crowd token: " + token);
        }

        CookieConfiguration cookieConfig = null;
        if (response != null)
        {
            SOAPCookieInfo soapCookieInfo = getSecurityServerClient().getCookieInfo();
            cookieConfig = new CookieConfiguration(soapCookieInfo.getDomain(), soapCookieInfo.isSecure(), Constants.COOKIE_TOKEN_KEY);
        }
        tokenHelper.setCrowdToken(request, response, token, getSoapClientProperties(), cookieConfig);
    }

    public SOAPPrincipal getPrincipal(HttpServletRequest request)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidTokenException, InvalidAuthenticationException
    {
        return getSecurityServerClient().findPrincipalByToken(getToken(request));
    }

    public String getToken(HttpServletRequest request) throws InvalidTokenException
    {
        String token = tokenHelper.getCrowdToken(request, getCookieTokenKey());
        if (token == null)
        {
            throw new InvalidTokenException("Unable to find a valid principal token.");
        }
        else
        {
            return token;
        }
    }

    public boolean isAuthenticated(HttpServletRequest request, HttpServletResponse response)
            throws InvalidAuthorizationTokenException, RemoteException, ApplicationAccessDeniedException, InvalidAuthenticationException
    {

        HttpSession session = request.getSession();

        // Check if we have a token, if it is not present, assume we are no longer authenticated
        String token;
        try
        {
            token = getToken(request);
        }
        catch (InvalidTokenException e)
        {
            logger.debug("Non authenticated request, unable to find a valid Crowd token.");
            return false;
        }

        // get the last validation from the session
        Date lastValidation = (Date) session.getAttribute(getSoapClientProperties().getSessionLastValidation());

        // only check if the last validation has occurred, and the validation is not required on every request
        if (lastValidation != null && getSoapClientProperties().getSessionValidationInterval() > 0)
        {

            // if the validation has previously been done, add the previous time plus the allowed interval
            long timeSpread = lastValidation.getTime() + TimeUnit.MINUTES.toMillis(getSoapClientProperties().getSessionValidationInterval());

            // if the interval has not been reached, allow the previous validation
            if (timeSpread > System.currentTimeMillis())
            {
                return true;
            }
        }

        // look in the crowd token for authentication
        if (authenticationManager.isAuthenticated(token, getValidationFactors(request)))
        {
            setPrincipalToken(request, response, token);

            return true;
        }

        // don't invalidate the client unless asked to do so (authentication status fails, or logoff).
        //invalidateClient(request, response, null);

        return false;

    }

    public void authenticate(HttpServletRequest request, HttpServletResponse response, String username, String password)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {

        String token = null;

        try
        {
            // no longer needed -- we turned this off because null cookies were being wrote back to the browser
            // followed up by a valid cookie. sometimes the valid cookie was lost because it was second.
            //logoff(request, response);

            UserAuthenticationContext userAuthenticationContext =
                    getPrincipalAuthenticationContext(request, response, username, password);

            token = authenticationManager.authenticate(userAuthenticationContext);
        }
        finally
        {

            // clean up the session information if the authentication shouldn't be allowed
            if (token == null)
            {
                invalidateClient(request, response, null);
            }
            else
            {
                setPrincipalToken(request, response, token);
            }
        }
    }

    public void authenticateWithoutValidatingPassword(HttpServletRequest request, HttpServletResponse response, String username) throws ApplicationAccessDeniedException, InvalidAuthenticationException, InvalidAuthorizationTokenException, InactiveAccountException, RemoteException
    {
        String token = null;

        try
        {
            UserAuthenticationContext userAuthenticationContext = getPrincipalAuthenticationContext(request, response, username, null);

            token = authenticationManager.authenticateWithoutValidatingPassword(userAuthenticationContext);
        }
        finally
        {

            // clean up the session information if the authentication shouldn't be allowed
            if (token == null)
            {
                invalidateClient(request, response, null);

            }
            else
            {
                setPrincipalToken(request, response, token);
            }
        }
    }

    public String verifyAuthentication(String username, String password, ValidationFactor[] validationFactors) throws InvalidAuthorizationTokenException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {
        PasswordCredential credential = new PasswordCredential(password);

        UserAuthenticationContext userAuthenticationContext = new UserAuthenticationContext();

        userAuthenticationContext.setApplication(getSoapClientProperties().getApplicationName());
        userAuthenticationContext.setCredential(credential);
        userAuthenticationContext.setName(username);
        userAuthenticationContext.setValidationFactors(validationFactors);

        return authenticationManager.authenticate(userAuthenticationContext);
    }

    public void verifyAuthentication(String username, String password) throws InvalidAuthorizationTokenException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {
        authenticationManager.authenticate(username, password);
    }

    public ValidationFactor[] getValidationFactors(HttpServletRequest request)
    {
        List<ValidationFactor> validationFactors = new ArrayList<ValidationFactor>();

        if (request != null)
        {
            // add the proxy through address is necessary
            String remoteAddress = request.getRemoteAddr();
            if (remoteAddress != null && remoteAddress.length() > 0)
            {
                validationFactors.add(new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, remoteAddress));
            }

            String remoteAddressXForwardFor = request.getHeader(ValidationFactor.X_FORWARDED_FOR);
            if (remoteAddressXForwardFor != null && !remoteAddressXForwardFor.equals(remoteAddress))
            {
                validationFactors.add(new ValidationFactor(ValidationFactor.X_FORWARDED_FOR, remoteAddressXForwardFor));
            }

            // Not including USER_AGENT as a validation factor because IE8 sometimes gives IE7 user agent string (see CWD-1827)
        }

        return validationFactors.toArray(new ValidationFactor[validationFactors.size()]);
    }

    public void logoff(HttpServletRequest request, HttpServletResponse response)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException
    {

        String token = null;

        try
        {
            token = getToken(request);

            authenticationManager.invalidate(token);
        }
        catch (InvalidTokenException e)
        {
            // this client doesn't have a valid token
        }

        invalidateClient(request, response, token);
    }

    public UserAuthenticationContext getPrincipalAuthenticationContext(HttpServletRequest request, HttpServletResponse response, String username, String password)
    {

        PasswordCredential credential = new PasswordCredential(password);

        UserAuthenticationContext userAuthenticationContext = new UserAuthenticationContext();

        userAuthenticationContext.setApplication(getSoapClientProperties().getApplicationName());
        userAuthenticationContext.setCredential(credential);
        userAuthenticationContext.setName(username);
        userAuthenticationContext.setValidationFactors(getValidationFactors(request));

        return userAuthenticationContext;
    }

    public SoapClientProperties getSoapClientProperties()
    {
        return getSecurityServerClient().getSoapClientProperties();
    }

    protected String getCookieTokenKey()
    {
        return getSoapClientProperties().getCookieTokenKey();
    }

    public SecurityServerClient getSecurityServerClient()
    {
        return authenticationManager.getSecurityServerClient();
    }
}
