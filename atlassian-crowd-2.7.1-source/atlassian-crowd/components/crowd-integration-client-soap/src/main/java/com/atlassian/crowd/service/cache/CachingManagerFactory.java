package com.atlassian.crowd.service.cache;

import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.service.AuthenticationManager;
import com.atlassian.crowd.service.GroupManager;
import com.atlassian.crowd.service.GroupMembershipManager;
import com.atlassian.crowd.service.UserManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.service.soap.client.SecurityServerClientFactory;

import java.net.URL;

public class CachingManagerFactory
{
    /**
     * Inner class enables lazy-loading and thread-safe initialization.
     * See http://en.wikipedia.org/wiki/Singleton_pattern#The_solution_of_Bill_Pugh
     */
    private static class SimpleAuthenticationManagerHolder
    {
        private static final AuthenticationManager authenticationManager = getSimpleAuthenticationManager();
    }

    private static class CachingManagerHolder
    {
        private static final CachingGroupMembershipManager groupMembershipManager = getGroupMembershipManager();
        private static final CachingGroupManager groupManager = getGroupManager();
        private static final CachingUserManager userManager = getUserManager();
        private static final CacheAwareAuthenticationManager authenticationManager = getCacheAwareAuthenticationManager();
    }

    private static class CacheHolder
    {
        private static final URL cacheUrlWithoutSlash = CachingManagerFactory.class.getResource(Constants.CACHE_CONFIGURATION);
        private static final URL cacheUrlWithSlash = CachingManagerFactory.class.getResource("/" + Constants.CACHE_CONFIGURATION);
        private static final BasicCache basicCache = new CacheImpl(cacheUrlWithSlash != null ? cacheUrlWithSlash : cacheUrlWithoutSlash);
    }

    private static class CacheExpiryManagerHolder
    {
        private static final CacheExpiryManager cacheExpiryManager = new CacheExpiryManagerImpl(getCache());
    }

    /**
     * Split out so implementations can be overridden by subclasses.
     *
     * @return a new instance of CachingGroupMembershipManager
     */
    protected static CachingGroupMembershipManager getGroupMembershipManager()
    {
        return new CachingGroupMembershipManager(getSSC(), getUserManager(), getGroupManager(), getCache());
    }

    /**
     * Split out so implementations can be overridden by subclasses.
     *
     * @return a new instance of CachingUserManager
     */
    protected static CachingUserManager getUserManager()
    {
        return new CachingUserManager(getSSC(), getCache());
    }

    /**
     * Split out so implementations can be overridden by subclasses.
     *
     * @return a new instance of CachingGroupManager
     */
    protected static CachingGroupManager getGroupManager()
    {
        return new CachingGroupManager(getSSC(), getCache());
    }

    /**
     * Creates a new CacheAwareAuthenticationManager
     *
     * @return a new instance of CacheAwareAuthenticationManager
     */
    protected static CacheAwareAuthenticationManager getCacheAwareAuthenticationManager()
    {
        return new CacheAwareAuthenticationManager(getSSC(), getUserManager());
    }

    /**
     * Creates a new SecurityServerClient
     *
     * @return a new instance of SecurityServerClient
     */
    private static SecurityServerClient getSSC()
    {
        return SecurityServerClientFactory.getSecurityServerClient();
    }

    /**
     * Creates a new SimpleAuthenticationManager (this authentication manager is not cache aware)
     *
     * @return a new instance of SimpleAuthenticationManager
     */
    protected static SimpleAuthenticationManager getSimpleAuthenticationManager()
    {
        return new SimpleAuthenticationManager(getSSC());
    }

    /**
     * Retrieve a singleton instance of the Authentication Manager (cache aware)
     * If the user successfully authenticates, they will exist in the cache.
     *
     * @return the instance of a cache aware AuthenticationManager
     */
    public static AuthenticationManager getCacheAwareAuthenticationManagerInstance()
    {
        return CachingManagerHolder.authenticationManager;
    }

    /**
     * Retrieve a singleton instance of the Authentication Manager (not cache aware)
     *
     * @return the instance of a non cache aware AuthenticationManager
     */
    public static AuthenticationManager getSimpleAuthenticationManagerInstance()
    {
        return SimpleAuthenticationManagerHolder.authenticationManager;
    }

    public static UserManager getUserManagerInstance()
    {
        return CachingManagerHolder.userManager;
    }

    public static GroupManager getGroupManagerInstance()
    {
        return CachingManagerHolder.groupManager;
    }

    public static GroupMembershipManager getGroupMembershipManagerInstance()
    {
        return CachingManagerHolder.groupMembershipManager;
    }

    public static CacheExpiryManager getCacheExpiryManagerInstance()
    {
        return CacheExpiryManagerHolder.cacheExpiryManager;
    }

    public static BasicCache getCache()
    {
        return CacheHolder.basicCache;
    }
}