package com.atlassian.crowd.service;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SearchRestriction;

import java.rmi.RemoteException;
import java.util.List;

/**
 * Methods related to operations on groups, but not on their users.
 */
public interface GroupManager
{
    /**
     * Returns true if <code>groupName</code> represents a valid group, false otherwise.
     *
     * @param groupName The name to check
     * @return true if it's a group, false otherwise
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                         The application (not the user) was not authenticated correctly.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    boolean isGroup(String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Given a <code>groupName</code>, returns a <code>SOAPGroup</code> that represents the group. This group object
     * will not have any membership information associated with it.
     *
     * @param groupName The name of the group to fetch
     * @return An object representing the group, or null if it does not exist
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                         The application (not the user) was not authenticated correctly.
     * @throws GroupNotFoundException
     *                         The group could not be found.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPGroup getGroup(String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, GroupNotFoundException, InvalidAuthenticationException;

    /**
     * Adds <code>group</code> to the Crowd server.
     *
     * @param group The object containing the details of the group.
     * @return The <code>SOAPGroup</code>, as returned by the Crowd server.
     * @throws RemoteException                A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                                        The application (not the user) was not authenticated correctly.
     * @throws InvalidGroupException          Could not add the group - it may be malformed, or a group with that name may already exist.
     * @throws ApplicationPermissionException The application does not have permission to add groups.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    SOAPGroup addGroup(SOAPGroup group)
            throws RemoteException, InvalidGroupException, InvalidAuthorizationTokenException, ApplicationPermissionException, InvalidAuthenticationException;


    /**
     * Searches the list of all available groups based on the passed-in <code>restrictions</code> and returns a list
     * of groups that match.
     *
     * @param restrictions search restrictions
     * @return A {@link List} of {@link SOAPGroup}s that match the criteria.
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                         The application (not the user) was not authenticated correctly.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    List/*<SOAPGroup>*/ searchGroups(SearchRestriction[] restrictions)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;


    /**
     * Updates a group's details in Crowd.
     *
     * @param group The group to update
     * @throws RemoteException                A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                                        The application (not the user) was not authenticated correctly.
     * @throws GroupNotFoundException         The group to update could not be found.
     * @throws ApplicationPermissionException The application does not have permission to update groups.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void updateGroup(SOAPGroup group)
            throws RemoteException, ApplicationPermissionException, InvalidAuthorizationTokenException, GroupNotFoundException, InvalidAuthenticationException;

    /**
     * Removes a group from Crowd.
     *
     * @param groupName The name of the group to remove.
     * @throws RemoteException                A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                                        The application (not the user) was not authenticated correctly.
     * @throws GroupNotFoundException         The group to remove could not be found.
     * @throws ApplicationPermissionException The application does not have permission to remove groups.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void removeGroup(String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, GroupNotFoundException, ApplicationPermissionException, InvalidAuthenticationException;

    /**
     * Returns a list of all available groups. We recommend against use of this method; depending on the environment, it
     * may prevent your app from scaling. Some large organisations build very complex group processioning structures,
     * with total group numbers in the tens of thousands.
     *
     * @return A {@link java.util.List} of {@link String}s that list all the groups visible to this application.
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                         The application (not the user) was not authenticated correctly.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @deprecated Since 1.4.
     */
    List/*<String>*/ getAllGroupNames()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;
}
