package com.atlassian.crowd.integration.exception;

import com.atlassian.crowd.integration.model.user.User;

/**
 * Thrown when the user's account is inactive.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class InactiveAccountException extends CheckedSoapException
{
    private User user;

    public InactiveAccountException(User user)
    {
        this(user, null);
    }

    public InactiveAccountException(User user, Throwable e)
    {
        super("Account with name <" + user.getName() + "> is inactive", e);
        this.user = user;
    }

    public InactiveAccountException(String message, Throwable e)
    {
        super(message, e);
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(final User user)
    {
        this.user = user;
    }
}
