package com.atlassian.crowd.service.cache;

public class CacheExpiryManagerImpl implements CacheExpiryManager
{
    private final BasicCache basicCache;

    public CacheExpiryManagerImpl(BasicCache basicCache)
    {
        this.basicCache = basicCache;
    }


    public void flush()
    {
        basicCache.flush();        
    }
}
