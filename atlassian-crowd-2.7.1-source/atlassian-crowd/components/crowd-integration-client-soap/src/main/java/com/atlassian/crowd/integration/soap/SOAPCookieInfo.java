package com.atlassian.crowd.integration.soap;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Configuration information for setting client cookie details.
 */
public class SOAPCookieInfo implements Serializable
{
    private String  domain;     // eg. ".atlassian.com."
    private boolean secure;     // if true, set the "Secure" flag on the cookie

    public SOAPCookieInfo()
    {
    }

    public SOAPCookieInfo(String domain, boolean secure)
    {
        this.domain = domain;
        this.secure = secure;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public void setSecure(boolean secure)
    {
        this.secure = secure;
    }

    public String getDomain()
    {
        return domain;
    }

    public boolean isSecure()
    {
        return secure;
    }

    public String toString()
    {
        return new ToStringBuilder(this).
                append("domain", domain).
                append("secure", secure).toString();
    }

    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        final SOAPCookieInfo that = (SOAPCookieInfo) o;

        boolean isSameDomain = (getDomain() != null ? getDomain().equals(that.getDomain()) : that.getDomain() == null);
        boolean isSameSecurity = (isSecure() == that.isSecure());
        return isSameDomain && isSameSecurity;
    }

    public int hashCode()
    {
        return (getDomain() != null ? getDomain().hashCode() : 0);
    }
}
