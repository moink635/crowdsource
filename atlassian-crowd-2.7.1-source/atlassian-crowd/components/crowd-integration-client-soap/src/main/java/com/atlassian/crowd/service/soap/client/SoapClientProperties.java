package com.atlassian.crowd.service.soap.client;

import com.atlassian.crowd.service.client.ClientProperties;

/**
 * SOAP specific client properties.
 */
public interface SoapClientProperties extends ClientProperties
{
    /**
     * Returns Crowd SOAP server web services URL
     *
     * @return Crowd SOAP server URL
     */
    String getSecurityServerURL();
}
