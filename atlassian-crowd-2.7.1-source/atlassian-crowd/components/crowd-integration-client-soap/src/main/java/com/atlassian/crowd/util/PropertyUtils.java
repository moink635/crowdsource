package com.atlassian.crowd.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * PropertyUtils class to enable the writing back of Properties
 */
public class PropertyUtils
{
    private static final Logger logger = LoggerFactory.getLogger(PropertyUtils.class);

    /**
     * Retrieves the properties from the specified properties file
     * @param propertyResourceLocation location of the properties file
     * @return Properties retrieved from the properties file specified
     */
    public Properties getProperties(final String propertyResourceLocation)
    {
        Properties properties = null;

        if (propertyResourceLocation != null)
        {
            try
            {
                URL url = new URL(propertyResourceLocation);

                properties = getPropertiesFromStream(url.openStream());
            }
            catch (IOException e)
            {
                logger.error(e.getMessage(), e);
            }
        }

        return properties;
    }

    /**
     * Loads Properties from an InputStream
     * @param is InputStream containing properties to load
     * @return Properties loaded from input stream
     */
    public Properties getPropertiesFromStream(InputStream is)
    {
        if (is == null)
        {
            return null;
        }

        Properties props = new Properties();
        try
        {
            props.load(is);
        }
        catch (IOException e)
        {
            logger.error("Error loading properties from stream.", e);
        }
        finally
        {
            try
            {
                is.close();
            }
            catch (IOException e)
            {
                logger.error("Failed to close the stream: " + e.getMessage(), e);
            }
        }

        return props;
    }

    /**
     * Removes the specified property from the specified property file
     * @param propertyResourceLocation location of the properties file
     * @param key the key of the property to be removed
     * @return <code>true</true> iff the specified property has been successfully removed from the properties file; <code>false</code> otherwise
     */
    public boolean removeProperty(final String propertyResourceLocation, final String key)
    {
        boolean success = false;
        // create a new properties reference
        Properties properties = getProperties(propertyResourceLocation);

        // Set the new property
        Object currentValue = properties.remove(key);
        logger.info("Updating properties resource: " + propertyResourceLocation + " removing property with key: " + key);
        if (currentValue != null)
        {
            success = true;
        }

        // Store this property to disk
        storeProperties(propertyResourceLocation, properties);

        return success;
    }

    /**
     * Updates the specified properties file with the specified key/value property
     * @param propertyResourceLocation location of the properties file
     * @param key the key for the property to be added/updated
     * @param value the value of the property to be added/updated
     */
    public void updateProperty(final String propertyResourceLocation, final String key, final String value)
    {
        // create a new properties reference
        Properties properties = getProperties(propertyResourceLocation);

        if (properties != null)
        {
            // Set the new property
            properties.setProperty(key, value);
            logger.info("Updating properties resource: " + propertyResourceLocation + " adding property: " + key + "|" + value);

            // Store this property to disk
            storeProperties(propertyResourceLocation, properties);
        }
    }

    private void storeProperties(String propertyResourceLocation, Properties properties)
    {
        // Get a handle to the properties file
        // NOTE: we URL decode the FILE URL because on Windows, the returning file string is URL-encoded
        String crowdFile;
        OutputStream ostream = null;
        try
        {
            crowdFile = URLDecoder.decode(new URL(propertyResourceLocation).getFile(), "UTF-8");

            ostream = new FileOutputStream(crowdFile);

            // store the new property
            properties.store(ostream, null);
        }
        catch (IOException e)
        {
            logger.error(e.getMessage(), e);
        }
        finally
        {
            if (ostream != null)
            {
                try
                {
                    ostream.close();
                }
                catch (IOException e)
                {
                    logger.error(e.getMessage(), e);
                }
            }
        }
    }
}
