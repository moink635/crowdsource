package com.atlassian.crowd.integration.model;

/**
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility.
 */
public interface DirectoryEntity
{
    /**
     * @return id of the directory in which the DirectoryEntity is stored.
     */
    Long getDirectoryId();

    /**
     * @return name of the entity.
     */
    String getName();

    /**
     * Implementations must ensure equality based on
     * getDirectoryId() and case-insensitive getName().
     *
     * @param o object to compare to.
     * @return <code>true</code> if and only if the directoryId
     * and lowercase of name (subject to the pre-configured locale) of the directory entities match.
     */
    boolean equals(Object o);

    /**
     * Implementations must produce a hashcode based on
     * getDirectoryId() and case-insensitive getName().
     *
     * @return hashcode.
     */
    int hashCode();
}
