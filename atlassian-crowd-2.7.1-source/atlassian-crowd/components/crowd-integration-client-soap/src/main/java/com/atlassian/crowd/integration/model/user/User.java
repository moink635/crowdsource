package com.atlassian.crowd.integration.model.user;

import com.atlassian.crowd.integration.model.DirectoryEntity;

import java.security.Principal;

/**
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility.
 */
public interface User extends Principal, DirectoryEntity
{
    /**
     * @return <code>true<code> if and only if the user is allowed to authenticate.
     */
    boolean isActive();

    /**
     * @return email address of the user.
     */
    String getEmailAddress();

    /**
     * @return display name (eg. full name) of the user, must never be null.
     */
    String getDisplayName();

    /**
     * @return first name of the user
     */
    String getFirstName();

    /**
     * @return last name of the user
     */
    String getLastName();

    /**
     * @return icon location of the user
     */
    String getIconLocation();
}
