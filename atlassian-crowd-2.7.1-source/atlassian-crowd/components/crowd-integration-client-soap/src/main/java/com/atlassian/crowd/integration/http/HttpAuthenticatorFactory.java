package com.atlassian.crowd.integration.http;

import com.atlassian.crowd.service.cache.CachingManagerFactory;

/**
 * Use this class to maintain a singleton instance
 * of the HttpAuthenticator. If you are managing
 * the HttpAuthenticator instance externally,
 * eg. via Spring, you do not need to use this class.
 *
 * Note that using this factory will implicitly use
 * the SecurityServerClientFactory to maintain the
 * underlying instance of the SecurityServerClient.
 *
 */
public class HttpAuthenticatorFactory
{
    private static final HttpAuthenticator cacheAwareHttpAuthenticator
            = new HttpAuthenticatorImpl(CachingManagerFactory.getCacheAwareAuthenticationManagerInstance());

    private static final HttpAuthenticator httpAuthenticator
                = new HttpAuthenticatorImpl(CachingManagerFactory.getSimpleAuthenticationManagerInstance());

    /**
     * Retrieve the singleton instance of the HttpAuthenticator (cache aware).
     *
     * @return singleton instance of the HttpAuthenticator (cache aware).
     */
    public static HttpAuthenticator getCacheAwareHttpAuthenticator()
    {
        return cacheAwareHttpAuthenticator;
    }

    /**
     * Retrieve the singleton instance of the HttpAuthenticator (not cache aware).
     *
     * @return singleton instance of the HttpAuthenticator (not cache aware).
     */
    public static HttpAuthenticator getHttpAuthenticator()
    {
        return httpAuthenticator;
    }
}
