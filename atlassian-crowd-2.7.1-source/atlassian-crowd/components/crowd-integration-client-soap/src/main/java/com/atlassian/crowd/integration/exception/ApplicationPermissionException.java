package com.atlassian.crowd.integration.exception;

/**
 * This is Exception is thrown if an Application does not have a required permission.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class ApplicationPermissionException extends CheckedSoapException
{
    /**
     * Default constructor.
     */
    public ApplicationPermissionException()
    {
    }

    /**
     * Default constructor.
     *
     * @param s the message.
     */
    public ApplicationPermissionException(String s)
    {
        super(s);
    }

    /**
     * Default constructor.
     *
     * @param s         the message.
     * @param throwable the {@link Exception Exception}.
     */
    public ApplicationPermissionException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    /**
     * Default constructor.
     *
     * @param throwable the {@link Exception Exception}.
     */
    public ApplicationPermissionException(Throwable throwable)
    {
        super(throwable);
    }
}
