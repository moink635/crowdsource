package com.atlassian.crowd.integration.exception;

/**
 * Thrown when the email address is not valid.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class InvalidEmailAddressException extends CheckedSoapException
{
    /**
     * Default constructor.
     */
    public InvalidEmailAddressException()
    {
    }

    /**
     * @param s the message.
     */
    public InvalidEmailAddressException(String s)
    {
        super(s);
    }

    /**
     * @param s         the message.
     * @param throwable the {@link Exception Exception}.
     */
    public InvalidEmailAddressException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    /**
     *
     * @param throwable the {@link Exception Exception}.
     */
    public InvalidEmailAddressException(Throwable throwable)
    {
        super(throwable);
    }
}