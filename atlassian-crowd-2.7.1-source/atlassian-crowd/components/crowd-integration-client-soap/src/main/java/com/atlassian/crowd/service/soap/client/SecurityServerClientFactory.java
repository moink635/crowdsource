package com.atlassian.crowd.service.soap.client;

import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.service.client.ClientResourceLocator;

/**
 * Use this class to maintain a singleton instance
 * of the SecurityServerClient. If you are managing
 * the SecurityServerClient instance externally,
 * eg. via Spring, you do not need to use this class.
 *
 */
public class SecurityServerClientFactory
{
    private static final SecurityServerClient securityServerClient
        = new SecurityServerClientImpl(
                SoapClientPropertiesImpl.newInstanceFromResourceLocator(
                    new ClientResourceLocator(Constants.PROPERTIES_FILE)
                )
          );


    /**
     * Retrieve a singleton instance of the SecurityServerClient.
     *
     * @return singleton instance of the SecurityServerClient.
     */
    public static SecurityServerClient getSecurityServerClient()
    {
        return securityServerClient;
    }
}
