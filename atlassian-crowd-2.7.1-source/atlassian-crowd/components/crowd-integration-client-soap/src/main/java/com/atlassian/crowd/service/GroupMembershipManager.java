package com.atlassian.crowd.service;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;

import java.rmi.RemoteException;
import java.util.List;

/**
 * This interface allows applications to query relationships between users and groups.
 */
public interface GroupMembershipManager
{
    /**
     * Returns true if the user represented by <code>userName</code> is a member of <code>groupName</code>
     *
     * @param userName  The user to check
     * @param groupName The group to check.
     * @return true if a member, false otherwise.
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                         The application (not the user) was not authenticated correctly.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    boolean isMember(String userName, String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Adds a user identified by <code>userName</code> to the group identified by <code>groupName</code>. If the user
     * is already a member of the group, this method will return successfully.
     *
     * @param userName  The user to add
     * @param groupName The group the user will be added to
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException The application (not the user) was not authenticated correctly.
     * @throws GroupNotFoundException the group could not be found
     * @throws UserNotFoundException the user could not be found
     * @throws ApplicationPermissionException This application does not have permission to update group memberships.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    void addMembership(String userName, String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, GroupNotFoundException, UserNotFoundException, InvalidAuthenticationException;

    /**
     * Removes a user identified by <code>userName</code> from the group identified by <code>groupName</code>. If the
     * user was not a member of the group, this method will return successfully.
     *
     * @param userName  The user to remove
     * @param groupName The group the user will be added from
     * @throws RemoteException                A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException The application (not the user) was not authenticated correctly.
     * @throws GroupNotFoundException the group could not be found
     * @throws UserNotFoundException the user could not be found
     * @throws ApplicationPermissionException This application does not have permission to update group memberships.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @throws MembershipNotFoundException Unable to find membership
     */
    void removeMembership(String userName, String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, GroupNotFoundException, UserNotFoundException, MembershipNotFoundException, InvalidAuthenticationException;

    /**
     * Obtains a list of all groups that the user identified by <code>userName</code> belongs to, sorted naturally.
     * <p/>
     * This can be a slow call, particularly on some LDAP directories, and so should be avoided. Use
     * <code>isMember()</code> instead.
     *
     * @param userName The user whose group memberships are desired.
     * @return A {@link List} of {@link String}s containing all the groups the user belongs to, or an empty List
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException The application (not the user) was not authenticated correctly.
     * @throws UserNotFoundException If the user could not be found.
     * @throws InvalidAuthenticationException application authentication is not valid
     * @deprecated Since 1.4
     */
    List/*<String>*/ getMemberships(String userName)
            throws RemoteException, InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException;

    /**
     * Obtains a list of all users in the group identified by <code>groupName</code>, sorted naturally.
     * <p/>
     * This is a fairly fast call, but implementers should still be using <code>isMember()</code> wherever practicable.
     *
     * @param groupName The group whose member users are desired.
     * @return A {@link List} of {@link String}s containing all the users in the group, or an empty list.
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException The application (not the user) was not authenticated correctly.
     * @throws GroupNotFoundException If the group could not be found.
     * @throws InvalidAuthenticationException application authentication is not valid
     */
    List/*<String>*/ getMembers(String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, GroupNotFoundException, InvalidAuthenticationException;
}
