package com.atlassian.crowd.integration.exception;

/**
 * Thrown when a User does not have access to authenticate against application.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class ApplicationAccessDeniedException extends CheckedSoapException
{
    public ApplicationAccessDeniedException()
    {
        super();
    }

    public ApplicationAccessDeniedException(String s)
    {
        super(s);
    }

    public ApplicationAccessDeniedException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    public ApplicationAccessDeniedException(Throwable throwable)
    {
        super(throwable);
    }
}
