package com.atlassian.crowd.service.soap.client;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.BulkAddFailedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidEmailAddressException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.InvalidRoleException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPCookieInfo;
import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SOAPNestableGroup;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.soap.SOAPPrincipalWithCredential;
import com.atlassian.crowd.integration.soap.SOAPRole;
import com.atlassian.crowd.integration.soap.SearchRestriction;
import com.atlassian.crowd.model.authentication.AuthenticatedToken;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.soap.server.SecurityServer;
import com.atlassian.crowd.service.soap.xfire.XFireFaultLoggingMethodHandler;
import com.atlassian.crowd.service.soap.xfire.XFireInLoggingMethodHandler;
import com.atlassian.crowd.service.soap.xfire.XFireOutLoggingMethodHandler;
import com.atlassian.crowd.util.SoapExceptionTranslator;
import com.atlassian.crowd.util.SoapObjectTranslator;
import com.atlassian.crowd.util.build.BuildUtils;
import org.codehaus.xfire.XFireFactory;
import org.codehaus.xfire.client.Client;
import org.codehaus.xfire.client.XFireProxy;
import org.codehaus.xfire.client.XFireProxyFactory;
import org.codehaus.xfire.service.Service;
import org.codehaus.xfire.service.binding.ObjectServiceFactory;
import org.codehaus.xfire.transport.http.AbstractMessageSender;
import org.codehaus.xfire.transport.http.CommonsHttpMessageSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Proxy;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * An implementation of the SecurityServerClient interface.
 * <p/>
 * This class with establish a connection with the Crowd security server,
 * authenticate the application client and store the authentication token
 * for the client for server operations. If the token becomes invalid,
 * a new authentication token will be obtained and the request will run again.
 * If the request fails a <code>InvalidAuthorizationTokenException</code>
 * exception will be thrown.
 * <p/>
 * This bean should be used in a singleton fashion.
 *
 * @see SecurityServerClientFactory for a singleton factory implementation.
 */
public class SecurityServerClientImpl implements SecurityServerClient
{
    private static final String SHOULD_NOT_REACH_HERE_MSG = "Should not reach here.";

    private static final Logger logger = LoggerFactory.getLogger(SecurityServerClientImpl.class);

    private static final String USER_AGENT = "Mozilla/5.0 (Crowd Client " + BuildUtils.BUILD_VERSION + "; XFire Client)";

    // members
    protected com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken;
    protected Service service;
    protected XFireProxyFactory factory;

    private final SoapClientProperties clientProperties;

    /**
     * @param clientProperties properties used to configure the client.
     */
    public SecurityServerClientImpl(SoapClientProperties clientProperties)
    {
        this.clientProperties = clientProperties;
    }

    /**
     * Create and return an instance to the Crowd SecurityServer.
     *
     * @return SecurityServer instance.
     * @throws RemoteException there is a problem connecting to the Crowd Security Server.
     */
    protected SecurityServer getSecurityServer() throws RemoteException
    {
        try
        {
            if (service == null)
            {
                service = new ObjectServiceFactory(XFireFactory.newInstance().getXFire().getTransportManager()).create(SecurityServer.class, Constants.SECURITY_SERVER_NAME, "urn:" + Constants.SECURITY_SERVER_NAME, null);

                // Logging sample from  http://xfire.codehaus.org/Logging
                // Tell XFire to cache a DOM document for the various in/out/fault flows
                service.addInHandler(new org.codehaus.xfire.util.dom.DOMInHandler());
                service.addOutHandler(new org.codehaus.xfire.util.dom.DOMOutHandler());
                service.addFaultHandler(new org.codehaus.xfire.util.dom.DOMOutHandler());

                // Add a logging handler to each flow
                service.addInHandler(new XFireInLoggingMethodHandler());
                service.addOutHandler(new XFireOutLoggingMethodHandler());
                service.addFaultHandler(new XFireFaultLoggingMethodHandler());

                // override the default message sender so it cleans up after itself.
                service.setProperty(AbstractMessageSender.MESSAGE_SENDER_CLASS_NAME, CleaningHttpMessageSender.class.getName());
            }

            if (factory == null)
            {
                factory = new XFireProxyFactory();
            }

            logger.debug("Connection URL: " + clientProperties.getSecurityServerURL());

            SecurityServer securityServer = (SecurityServer) factory.create(service, clientProperties.getSecurityServerURL());

            Client client = ((XFireProxy) Proxy.getInvocationHandler(securityServer)).getClient();/*or use:Client client = Client.getInstance(this.client); */

            // enable gzip compression on the request and response

            // disabled because of http://jira.codehaus.org/browse/XFIRE-913
            //client.setProperty(CommonsHttpMessageSender.GZIP_REQUEST_ENABLED, true);

            client.setProperty(CommonsHttpMessageSender.GZIP_RESPONSE_ENABLED, Boolean.TRUE);

            // Changing the default user-agent for http://jira.atlassian.com/browse/CWD-1404
            Map<String, String> headers = new HashMap<String, String>(1);
            headers.put("User-Agent", USER_AGENT);
            client.setProperty(CommonsHttpMessageSender.HTTP_HEADERS, headers);


            // optional client proxy configuration
            if (clientProperties.getHttpProxyHost() != null && clientProperties.getHttpProxyPort() != null)
            {
                client.setProperty(CommonsHttpMessageSender.HTTP_PROXY_HOST, clientProperties.getHttpProxyHost());
                client.setProperty(CommonsHttpMessageSender.HTTP_PROXY_PORT, clientProperties.getHttpProxyPort());

                if (clientProperties.getHttpProxyUsername() != null && clientProperties.getHttpProxyPassword() != null)
                {
                    client.setProperty(CommonsHttpMessageSender.HTTP_PROXY_USER, clientProperties.getHttpProxyUsername());
                    client.setProperty(CommonsHttpMessageSender.HTTP_PROXY_PASS, clientProperties.getHttpProxyPassword());
                }
            }

            // optional client connection pool configuration
            if (clientProperties.getHttpMaxConnections() != null)
            {
                client.setProperty(CommonsHttpMessageSender.MAX_TOTAL_CONNECTIONS, clientProperties.getHttpMaxConnections());
                client.setProperty(CommonsHttpMessageSender.MAX_CONN_PER_HOST, clientProperties.getHttpMaxConnections());
            }
            if (clientProperties.getHttpTimeout() != null)
            {
                client.setProperty(CommonsHttpMessageSender.HTTP_TIMEOUT, clientProperties.getHttpTimeout());
            }

            return securityServer;
        }
        catch (Exception e)
        {
            String error = "Unable to connect to crowd server URL: " + clientProperties.getSecurityServerURL();
            logger.error(error, e);
            throw new RemoteException(error, e);
        }
    }

    /**
     * Retrieve the current application token.
     * <p/>
     * If this client is not authenticated, then authenticate
     * prior to returning the token.
     *
     * @return current application token.
     * @throws RemoteException there was a problem communicating with the Crowd Security Server.
     * @throws InvalidAuthorizationTokenException
     *                         incorrect credentials were used to authenticate the client.
     *                         Ensure the application.password in crowd.properties matches the one defined in the Crowd Console.
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    public AuthenticatedToken getApplicationToken()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        return SoapObjectTranslator.fromSoapAuthenticatedToken(getSoapApplicationToken());
    }

    /**
     * Returns the SOAP authentication token.
     *
     * @return authentication token
     * @throws RemoteException if there was a problem communicating with the Crowd Security Server.
     * @throws InvalidAuthorizationTokenException if a new valid application token cannot be created
     */
    private com.atlassian.crowd.integration.authentication.AuthenticatedToken getSoapApplicationToken()
            throws RemoteException, InvalidAuthorizationTokenException
    {
        if (applicationToken == null)
        {
            logger.info("Existing application token is null, authenticating ...");

            try
            {
                authenticate();
            }
            catch (InvalidAuthenticationException e)
            {
                throw new InvalidAuthorizationTokenException("Authorization token cannot be created because client application credentials are invalid", e);
            }

            logger.info("Created new application token: " + applicationToken.getToken());
        }

        logger.debug("Using existing token: " + applicationToken.getToken());

        return applicationToken;
    }

    public void authenticate()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            applicationToken = getSecurityServer().authenticateApplication(SoapObjectTranslator.toSoapApplicationAuthenticationContext(clientProperties.getApplicationAuthenticationContext()));
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthenticationException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
    }

    public String authenticatePrincipal(UserAuthenticationContext userAuthenticationContext)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException,
            InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {
        try
        {
            try
            {
                return getSecurityServer().authenticatePrincipal(getSoapApplicationToken(), SoapObjectTranslator.toSoapUserAuthenticationContext(userAuthenticationContext));
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().authenticatePrincipal(getSoapApplicationToken(), SoapObjectTranslator.toSoapUserAuthenticationContext(userAuthenticationContext));
            }
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InactiveAccountException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ExpiredCredentialException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthenticationException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public boolean isValidToken(String principalToken, ValidationFactor[] validationFactors)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().isValidPrincipalToken(getSoapApplicationToken(), principalToken, SoapObjectTranslator.toSoapValidationFactors(validationFactors));
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                try
                {
                    authenticate();
                }
                catch (InvalidAuthenticationException e1)
                {
                    throw e;
                }
                return getSecurityServer().isValidPrincipalToken(getSoapApplicationToken(), principalToken, SoapObjectTranslator.toSoapValidationFactors(validationFactors));
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public void invalidateToken(String token)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().invalidatePrincipalToken(getSoapApplicationToken(), token);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().invalidatePrincipalToken(getSoapApplicationToken(), token);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
    }

    public SOAPGroup[] searchGroups(SearchRestriction[] searchRestrictions)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().searchGroups(getSoapApplicationToken(), searchRestrictions);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().searchGroups(getSoapApplicationToken(), searchRestrictions);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public SOAPPrincipal[] searchPrincipals(SearchRestriction[] searchRestrictions)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().searchPrincipals(getSoapApplicationToken(), searchRestrictions);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().searchPrincipals(getSoapApplicationToken(), searchRestrictions);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public SOAPRole[] searchRoles(SearchRestriction[] searchRestrictions)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        return new SOAPRole[0];
    }

    public SOAPGroup[] findAllGroups()
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        SearchRestriction[] searchRestrictions = new SearchRestriction[0];

        return searchGroups(searchRestrictions);
    }

    public SOAPRole[] findAllRoles()
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        return new SOAPRole[0];
    }

    public SOAPPrincipal[] findAllPrincipals()
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        SearchRestriction[] searchRestrictions = new SearchRestriction[0];

        return searchPrincipals(searchRestrictions);
    }

    public SOAPGroup addGroup(SOAPGroup group)
            throws RemoteException, InvalidGroupException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationPermissionException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().addGroup(getSoapApplicationToken(), group);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().addGroup(getSoapApplicationToken(), group);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidGroupException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public void updateGroup(String group, String description, boolean active) throws RemoteException,
            com.atlassian.crowd.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.exception.GroupNotFoundException, com.atlassian.crowd.exception.ApplicationPermissionException, com.atlassian.crowd.exception.InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().updateGroup(getSoapApplicationToken(), group, description, active);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().updateGroup(getSoapApplicationToken(), group, description, active);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentGroupNotFoundException(e);
        }
    }

    public SOAPGroup findGroupByName(String name)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, GroupNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().findGroupByName(getSoapApplicationToken(), name);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().findGroupByName(getSoapApplicationToken(), name);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentGroupNotFoundException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public SOAPGroup findGroupWithAttributesByName(String name)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, GroupNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().findGroupWithAttributesByName(getSoapApplicationToken(), name);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().findGroupWithAttributesByName(getSoapApplicationToken(), name);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentGroupNotFoundException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public SOAPRole addRole(SOAPRole role)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidRoleException, ApplicationPermissionException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().addRole(getSoapApplicationToken(), role);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().addRole(getSoapApplicationToken(), role);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidRoleException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public SOAPRole findRoleByName(String name)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, GroupNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().findRoleByName(getSoapApplicationToken(), name);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().findRoleByName(getSoapApplicationToken(), name);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentGroupNotFoundException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public SOAPPrincipal findPrincipalByToken(String key)
            throws RemoteException, InvalidTokenException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().findPrincipalByToken(getSoapApplicationToken(), key);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().findPrincipalByToken(getSoapApplicationToken(), key);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public void updatePrincipalAttribute(String name, SOAPAttribute attribute)
            throws RemoteException, ApplicationPermissionException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().updatePrincipalAttribute(getSoapApplicationToken(), name, attribute);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().updatePrincipalAttribute(getSoapApplicationToken(), name, attribute);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserNotFoundException(e);
        }
    }

    public void updateGroupAttribute(String name, SOAPAttribute attribute)
            throws RemoteException, ApplicationPermissionException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, GroupNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().updateGroupAttribute(getSoapApplicationToken(), name, attribute);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().updateGroupAttribute(getSoapApplicationToken(), name, attribute);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentGroupNotFoundException(e);
        }
    }

    public SOAPPrincipal findPrincipalByName(String name)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().findPrincipalByName(getSoapApplicationToken(), name);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().findPrincipalByName(getSoapApplicationToken(), name);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserNotFoundException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public SOAPPrincipal findPrincipalWithAttributesByName(String name)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().findPrincipalWithAttributesByName(getSoapApplicationToken(), name);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().findPrincipalWithAttributesByName(getSoapApplicationToken(), name);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserNotFoundException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public SOAPPrincipal addPrincipal(SOAPPrincipal principal, PasswordCredential credential)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidCredentialException, InvalidUserException, ApplicationPermissionException, InvalidAuthenticationException
    {
        try
        {
            com.atlassian.crowd.integration.authentication.PasswordCredential cred = new com.atlassian.crowd.integration.authentication.PasswordCredential(credential.getCredential(), credential.isEncryptedCredential());
            try
            {
                return getSecurityServer().addPrincipal(getSoapApplicationToken(), principal, cred);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().addPrincipal(getSoapApplicationToken(), principal, cred);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidCredentialException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidUserException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public void addAllPrincipals(final Collection<SOAPPrincipalWithCredential> principals)
            throws com.atlassian.crowd.exception.InvalidAuthorizationTokenException, RemoteException, ApplicationPermissionException, BulkAddFailedException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().addAllPrincipals(getSoapApplicationToken(), principals.toArray(new SOAPPrincipalWithCredential[principals.size()]));
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().addAllPrincipals(getSoapApplicationToken(), principals.toArray(new SOAPPrincipalWithCredential[principals.size()]));
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.BulkAddFailedException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
    }

    public void addPrincipalToGroup(String principal, String group)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationPermissionException, GroupNotFoundException, UserNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().addPrincipalToGroup(getSoapApplicationToken(), principal, group);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().addPrincipalToGroup(getSoapApplicationToken(), principal, group);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserNotFoundException(e);
        }
    }

    public void updatePrincipalCredential(String principal, PasswordCredential credential)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidCredentialException,
            ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException
    {
        try
        {
            com.atlassian.crowd.integration.authentication.PasswordCredential cred = new com.atlassian.crowd.integration.authentication.PasswordCredential(credential.getCredential(), credential.isEncryptedCredential());
            try
            {
                getSecurityServer().updatePrincipalCredential(getSoapApplicationToken(), principal, cred);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().updatePrincipalCredential(getSoapApplicationToken(), principal, cred);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidCredentialException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserNotFoundException(e);
        }
    }

    public void resetPrincipalCredential(String principal)
            throws RemoteException, InvalidEmailAddressException, InvalidCredentialException, ApplicationPermissionException,
            com.atlassian.crowd.exception.InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().resetPrincipalCredential(getSoapApplicationToken(), principal);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().resetPrincipalCredential(getSoapApplicationToken(), principal);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidEmailAddressException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidCredentialException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserNotFoundException(e);
        }
    }

    public void removeGroup(String group)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationPermissionException, GroupNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().removeGroup(getSoapApplicationToken(), group);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().removeGroup(getSoapApplicationToken(), group);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentGroupNotFoundException(e);
        }
    }

    public void removeRole(String role)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationPermissionException, GroupNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().removeRole(getSoapApplicationToken(), role);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().removeRole(getSoapApplicationToken(), role);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentGroupNotFoundException(e);
        }
    }

    public void removePrincipal(String principal)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().removePrincipal(getSoapApplicationToken(), principal);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().removePrincipal(getSoapApplicationToken(), principal);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserNotFoundException(e);
        }
    }

    public void addPrincipalToRole(String principal, String role)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, GroupNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().addPrincipalToRole(getSoapApplicationToken(), principal, role);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().addPrincipalToRole(getSoapApplicationToken(), principal, role);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserOrGroupNotFoundException(e);
        }
    }

    public boolean isGroupMember(String group, String principal)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().isGroupMember(getSoapApplicationToken(), group, principal);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().isGroupMember(getSoapApplicationToken(), group, principal);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public boolean isRoleMember(String role, String principal)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().isRoleMember(getSoapApplicationToken(), role, principal);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().isRoleMember(getSoapApplicationToken(), role, principal);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public void removePrincipalFromGroup(String principal, String group)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, GroupNotFoundException, MembershipNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().removePrincipalFromGroup(getSoapApplicationToken(), principal, group);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().removePrincipalFromGroup(getSoapApplicationToken(), principal, group);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserOrGroupOrMembershipNotFoundException(e);
        }
    }

    public void removePrincipalFromRole(String principal, String role)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, GroupNotFoundException, MembershipNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().removePrincipalFromRole(getSoapApplicationToken(), principal, role);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().removePrincipalFromRole(getSoapApplicationToken(), principal, role);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserOrGroupOrMembershipNotFoundException(e);
        }
    }

    public void addAttributeToPrincipal(String principal, SOAPAttribute attribute)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().addAttributeToPrincipal(getSoapApplicationToken(), principal, attribute);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().addAttributeToPrincipal(getSoapApplicationToken(), principal, attribute);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserNotFoundException(e);
        }
    }

    public void removeAttributeFromPrincipal(String principal, String attribute)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().removeAttributeFromPrincipal(getSoapApplicationToken(), principal, attribute);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().removeAttributeFromPrincipal(getSoapApplicationToken(), principal, attribute);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserNotFoundException(e);
        }
    }

    public void addAttributeToGroup(String group, SOAPAttribute attribute)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationPermissionException, GroupNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().addAttributeToGroup(getSoapApplicationToken(), group, attribute);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().addAttributeToGroup(getSoapApplicationToken(), group, attribute);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentGroupNotFoundException(e);
        }
    }

    public void removeAttributeFromGroup(String group, String attribute)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, ApplicationPermissionException, GroupNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                getSecurityServer().removeAttributeFromGroup(getSoapApplicationToken(), group, attribute);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                getSecurityServer().removeAttributeFromGroup(getSoapApplicationToken(), group, attribute);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentGroupNotFoundException(e);
        }
    }

    public long getCacheTime()
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().getCacheTime(getSoapApplicationToken());
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().getCacheTime(getSoapApplicationToken());
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public boolean isCacheEnabled()
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().isCacheEnabled(getSoapApplicationToken());
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().isCacheEnabled(getSoapApplicationToken());
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public String getDomain()
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().getDomain(getSoapApplicationToken());
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().getDomain(getSoapApplicationToken());
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public String[] findAllPrincipalNames()
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().findAllPrincipalNames(getSoapApplicationToken());
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().findAllPrincipalNames(getSoapApplicationToken());
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public String[] findAllGroupNames()
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().findAllGroupNames(getSoapApplicationToken());
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().findAllGroupNames(getSoapApplicationToken());
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public SOAPNestableGroup[] findAllGroupRelationships()
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().findAllGroupRelationships(getSoapApplicationToken());
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().findAllGroupRelationships(getSoapApplicationToken());
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public String[] findAllRoleNames()
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().findAllRoleNames(getSoapApplicationToken());
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().findAllRoleNames(getSoapApplicationToken());
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public String[] findGroupMemberships(String principalName)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().findGroupMemberships(getSoapApplicationToken(), principalName);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().findGroupMemberships(getSoapApplicationToken(), principalName);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserNotFoundException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public String[] findRoleMemberships(String principalName)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().findRoleMemberships(getSoapApplicationToken(), principalName);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().findRoleMemberships(getSoapApplicationToken(), principalName);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            SoapExceptionTranslator.throwEquivalentUserNotFoundException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public String authenticatePrincipalSimple(String username, String password)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException,
            InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {
        try
        {
            try
            {
                return getSecurityServer().authenticatePrincipalSimple(getSoapApplicationToken(), username, password);
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().authenticatePrincipalSimple(getSoapApplicationToken(), username, password);
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InactiveAccountException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ExpiredCredentialException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthenticationException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public String createPrincipalToken(String username, ValidationFactor[] validationFactors)
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException,
            InactiveAccountException, ApplicationAccessDeniedException
    {
        try
        {
            try
            {
                return getSecurityServer().createPrincipalToken(getSoapApplicationToken(), username, SoapObjectTranslator.toSoapValidationFactors(validationFactors));
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().createPrincipalToken(getSoapApplicationToken(), username, SoapObjectTranslator.toSoapValidationFactors(validationFactors));
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InactiveAccountException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthenticationException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public String[] getGrantedAuthorities()
            throws com.atlassian.crowd.exception.InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().getGrantedAuthorities(getSoapApplicationToken());
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().getGrantedAuthorities(getSoapApplicationToken());
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public SOAPCookieInfo getCookieInfo()
            throws RemoteException, com.atlassian.crowd.exception.InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        try
        {
            try
            {
                return getSecurityServer().getCookieInfo(getSoapApplicationToken());
            }
            catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            {
                authenticate();
                return getSecurityServer().getCookieInfo(getSoapApplicationToken());
            }
        }
        catch (com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NOT_REACH_HERE_MSG);
    }

    public SoapClientProperties getSoapClientProperties()
    {
        return clientProperties;
    }
}
