package com.atlassian.crowd.integration.exception;

/**
 * An exception to denote an invalid group (of type Role).
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class InvalidRoleException extends InvalidDirectoryEntityException
{
    public InvalidRoleException()
    {
        // needed for xfire
        super();
    }

    public InvalidRoleException(com.atlassian.crowd.integration.model.DirectoryEntity group, Throwable cause)
    {
        super(group, cause);
    }

    public InvalidRoleException(com.atlassian.crowd.integration.model.DirectoryEntity group, String message)
    {
        super(group, message);
    }

    public InvalidRoleException(com.atlassian.crowd.integration.model.DirectoryEntity group, String message, Throwable cause)
    {
        super(group, message, cause);
    }
}
