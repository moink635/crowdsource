package com.atlassian.crowd.service.cache;

import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.util.Assert;
import com.atlassian.crowd.util.SOAPPrincipalHelper;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CacheImpl implements BasicCache
{
    private static final Logger logger = LoggerFactory.getLogger(CacheImpl.class);

    private final net.sf.ehcache.CacheManager ehCacheManager;

    private static final String USER_CACHE = "com.atlassian.crowd.integration-user";
    private static final String USER_WITH_ATTRIBUTE_CACHE = "com.atlassian.crowd.integration-user-with-attributes";
    private static final String USERNAMES_CACHE = "com.atlassian.crowd.integration-usernames";
    private static final String USERNAME_CASE_CACHE = "com.atlassian.crowd.integration-username-case";
    private static final String GROUP_CACHE = "com.atlassian.crowd.integration-group";
    private static final String GROUPNAMES_CACHE = "com.atlassian.crowd.integration-groupnames";
    private static final String GROUPNAME_CASE_CACHE = "com.atlassian.crowd.integration-groupname-case";
    private static final String PARENT_GROUPS_CACHE = "com.atlassian.crowd.integration-parentgroup";
    private static final String GROUP_MEMBERSHIP_CACHE = "com.atlassian.crowd.integration-group-membership";
    private static final String ALL_USER_MEMBERSHIPS_CACHE = "com.atlassian.crowd.integration-all-memberships";
    private static final String ALL_GROUP_MEMBERS_CACHE = "com.atlassian.crowd.integration-all-group-members";
    private static final String IS_USER_OR_GROUP_CACHE = "com.atlassian.crowd.integration-is-user-or-group";

    // Specific keys
    private static final String USERNAMES_KEY = "usernames";
    private static final String GROUPNAMES_KEY = "groupnames";
    private static final String ANCESTORS_BY_GROUP_KEY = "ancestorsByGroup";

    public CacheImpl(URL configLocation)
    {
        logger.info("config location: " + configLocation);

        this.ehCacheManager = new net.sf.ehcache.CacheManager(configLocation);

        // we may have issues loading the caches from the config file, so as a fail-safe
        // create any uncreated caches
        createCachesIfNeeded();
    }

    public CacheImpl(final CacheManager ehCacheManager)
    {
        this.ehCacheManager = ehCacheManager;

        createCachesIfNeeded();
    }

    private void createCachesIfNeeded()
    {
        createIfNeeded(USER_CACHE);
        createIfNeeded(USER_WITH_ATTRIBUTE_CACHE);
        createIfNeeded(USERNAMES_CACHE);
        createIfNeeded(USERNAME_CASE_CACHE);
        createIfNeeded(GROUP_CACHE);
        createIfNeeded(GROUPNAMES_CACHE);
        createIfNeeded(GROUPNAME_CASE_CACHE);
        createIfNeeded(PARENT_GROUPS_CACHE);
        createIfNeeded(GROUP_MEMBERSHIP_CACHE);
        createIfNeeded(ALL_USER_MEMBERSHIPS_CACHE);
        createIfNeeded(ALL_GROUP_MEMBERS_CACHE);
        createIfNeeded(IS_USER_OR_GROUP_CACHE);
    }

    /**
     * If the loaded configuration didn't create a cache named <code>cacheName</code>, create one.
     *
     * @param cacheName name of the cache
     */
    private void createIfNeeded(String cacheName)
    {
        if (!ehCacheManager.cacheExists(cacheName))
        {
            ehCacheManager.addCache(cacheName);
        }
    }

    private Cache getUserCache()
    {
        // potential optimisation: look this up once, then save reference.
        return ehCacheManager.getCache(USER_CACHE);
    }

    private Cache getUserNamesCache()
    {
        // potential optimisation: look this up once, then save reference.
        return ehCacheManager.getCache(USERNAMES_CACHE);
    }

    private Cache getGroupCache()
    {
        // potential optimisation: look this up once, then save reference.
        return ehCacheManager.getCache(GROUP_CACHE);
    }

    private Cache getGroupNamesCache()
    {
        // potential optimisation: look this up once, then save reference.
        return ehCacheManager.getCache(GROUPNAMES_CACHE);
    }

    private Cache getParentGroupsCache()
    {
        // potential optimisation: look this up once, then save reference.
        return ehCacheManager.getCache(PARENT_GROUPS_CACHE);
    }

    private Cache getGroupMembershipCache()
    {
        // potential optimisation: look this up once, then save reference.
        return ehCacheManager.getCache(GROUP_MEMBERSHIP_CACHE);
    }

    private Cache getUserMembershipsCache()
    {
        // potential optimisation: look this up once, then save reference.
        return ehCacheManager.getCache(ALL_USER_MEMBERSHIPS_CACHE);
    }

    private Cache getUserWithAttributeCache()
    {
        return ehCacheManager.getCache(USER_WITH_ATTRIBUTE_CACHE);
    }

    private Cache getGroupMembersCache()
    {
        // potential optimisation: look this up once, then save reference.
        return ehCacheManager.getCache(ALL_GROUP_MEMBERS_CACHE);
    }

    private Cache getIsUserOrGroupCache()
    {
        // potential optimisation: look this up once, then save reference.
        return ehCacheManager.getCache(IS_USER_OR_GROUP_CACHE);
    }

    private Cache getUserNameCaseCache()
    {
        return ehCacheManager.getCache(USERNAME_CASE_CACHE);
    }

    private Cache getGroupNameCaseCache()
    {
        return ehCacheManager.getCache(GROUPNAME_CASE_CACHE);
    }

    /**
     * Return a version of the name
     * appropriate for use as a cache
     * key.
     * <p/>
     * The default implementation toLowers
     * the name to produce a case-insensitive
     * cache.
     *
     * @param name key.
     * @return standardised key.
     */
    protected String getKey(String name)
    {
        if (name != null)
        {
            return toLowerCase(name);
        }
        else
        {
            return name;
        }
    }

    /**
     * Applies getKey over a collection.
     *
     * @param names set of names.
     * @return collection of keys.
     */
    protected List<String> getKeys(List<String> names)
    {
        List<String> keys = new ArrayList<String>(names.size());
        for (String name : names)
        {
            keys.add(getKey(name));
        }
        return keys;
    }

    public void flush()
    {
        ehCacheManager.clearAll();
    }

    public void cacheUser(SOAPPrincipal user)
    {
        getUserCache().put(new Element(getKey(user.getName()), user));
        getUserNameCaseCache().put(new Element(getKey(user.getName()), user.getName()));

        // If the primary user attributes have changed we need to remove the "user with attributes", and we will allow the user to be 're-fetched'
        Element element = getUserWithAttributeCache().get(getKey(user.getName()));
        if (element != null && isUserDirty(user, (SOAPPrincipal) element.getValue()))
        {
            getUserWithAttributeCache().remove(getKey(user.getName())); // remove value due to the update
        }
    }

    protected boolean isUserDirty(SOAPPrincipal latestSoapPrincipal, SOAPPrincipal cachedSoapPrincipal)
    {
        SOAPPrincipalHelper principalHelper = new SOAPPrincipalHelper();
        if (!StringUtils.equalsIgnoreCase(latestSoapPrincipal.getName(), cachedSoapPrincipal.getName()))
        {
            throw new IllegalArgumentException("You cannot compare users with different usernames <" + latestSoapPrincipal.getName() + ">,<" + cachedSoapPrincipal + ">");
        }

        return !(StringUtils.equals(latestSoapPrincipal.getName(), cachedSoapPrincipal.getName()) &&
                StringUtils.equals(principalHelper.getFullName(latestSoapPrincipal), principalHelper.getFullName(cachedSoapPrincipal)) &&
                StringUtils.equals(principalHelper.getFirstName(latestSoapPrincipal), principalHelper.getFirstName(cachedSoapPrincipal)) &&
                StringUtils.equals(principalHelper.getLastName(latestSoapPrincipal), principalHelper.getLastName(cachedSoapPrincipal)) &&
                StringUtils.equals(principalHelper.getEmail(latestSoapPrincipal), principalHelper.getEmail(cachedSoapPrincipal)));
    }

    public void cacheUserWithAttributes(SOAPPrincipal user)
    {
        getUserCache().put(new Element(getKey(user.getName()), user));
        getUserNameCaseCache().put(new Element(getKey(user.getName()), user.getName()));
        getUserWithAttributeCache().put(new Element(getKey(user.getName()), user));
    }

    public boolean removeUser(String userName)
    {
        getUserWithAttributeCache().remove(getKey(userName)); // May not actually exist, but we should still attempt to remove the user
        return getUserCache().remove(getKey(userName));
    }

    public SOAPPrincipal getUser(String userName)
    {
        Element element = getUserCache().get(getKey(userName));
        if (element == null)
        {
            return null;
        }
        return (SOAPPrincipal) element.getValue();
    }

    public SOAPPrincipal getUserWithAttributes(String userName)
    {
        Element element = getUserWithAttributeCache().get(getKey(userName));
        if (element == null)
        {
            return null;
        }
        return (SOAPPrincipal) element.getValue();
    }

    public List/*<String>*/ getAllUserNames()
    {
        Element element = getUserNamesCache().get(USERNAMES_KEY);
        if (element == null)
        {
            return null;
        }
        return (List/*<String>*/) element.getValue();
    }

    public void cacheAllUserNames(List/*<String>*/ userNames)
    {
        getUserNamesCache().put(new Element(USERNAMES_KEY, userNames));

        for (String userName : (List<String>) userNames)
        {
            getUserNameCaseCache().put(new Element(getKey(userName), userName));
        }
    }

    public SOAPGroup getGroup(String groupName)
    {
        Element element = getGroupCache().get(getKey(groupName));
        if (element == null)
        {
            return null;
        }
        return (SOAPGroup) element.getValue();
    }

    public void cacheGroup(SOAPGroup group)
    {
        getGroupCache().put(new Element(getKey(group.getName()), group));
        getGroupNameCaseCache().put(new Element(getKey(group.getName()), group.getName()));
    }

    public void removeGroup(String groupName)
    {
        getGroupCache().remove(getKey(groupName));
        getGroupNamesCache().removeAll();
        getGroupNameCaseCache().remove(getKey(groupName));
    }

    public List/*<String>*/ getAllGroupNames()
    {
        Element element = getGroupNamesCache().get(GROUPNAMES_KEY);
        if (element == null)
        {
            return null;
        }
        return (List/*<String>*/) element.getValue();
    }

    public void cacheAllGroupNames(List/*<String>*/ groupNames)
    {
        getGroupNamesCache().put(new Element(GROUPNAMES_KEY, groupNames));

        for (String groupName : (List<String>) groupNames)
        {
            getGroupNameCaseCache().put(new Element(getKey(groupName), groupName));
        }
    }

    public void cacheAncestorsForGroups(Map<String, Set<String>> ancestorsByGroup)
    {
        getParentGroupsCache().put(new Element(ANCESTORS_BY_GROUP_KEY, ancestorsByGroup));
    }

    @SuppressWarnings("unchecked")
    public Map<String, Set<String>> getAncestorsForGroups()
    {
        final Element element = getParentGroupsCache().get(ANCESTORS_BY_GROUP_KEY);
        if (element == null)
        {
            return null;
        }
        return (Map<String, Set<String>>) element.getValue();
    }

    public boolean containsGroupRelationships()
    {
        return (getAllGroupNames() != null);
    }

    public Boolean isMemberInGroupCache(String userName, String groupName)
    {
        Boolean isMember = null;
        List/*<String>*/ allUsers = getAllMembers(getKey(groupName));
        if (allUsers != null)
        {
            isMember = Boolean.valueOf(allUsers.contains(getKey(userName)));
        }
        return isMember;
    }

    public void addGroupToUser(String userName, String groupName)
    {
        userName = getKey(userName);
        groupName = getKey(groupName);

        List/*<String>*/ groups = getAllMemberships(userName);
        if (groups == null)
        {
            groups = new ArrayList(1);
        }
        // could optimise by reworking underlying collection to be a SortedSet
        if (!groups.contains(groupName))
        {
            groups.add(groupName);
            Collections.sort(groups);
            cacheAllMemberships(userName, groups);  // If it's the first membership, the ArrayList created above will not be cached.
        }
    }

    public void addUserToGroup(String userName, String groupName)
    {
        userName = getKey(userName);
        groupName = getKey(groupName);

        List/*<String>*/ users = getAllMembers(groupName);
        if (users == null)
        {
            users = new ArrayList(1);
        }
        // could optimise by reworking underlying collection to be a SortedSet
        if (!users.contains(userName))
        {
            users.add(userName);
            Collections.sort(users);
            cacheAllMembers(groupName, users);  // If it's the first member, the ArrayList created above will not be cached.
        }
    }

    public void removeGroupFromUser(String userName, String groupName)
    {
        List/*<String>*/ groupsForUser = getAllMemberships(getKey(userName));
        if (groupsForUser != null)
        {
            groupsForUser.remove(getKey(groupName));
        }
    }

    public void removeUserFromGroup(String userName, String groupName)
    {
        List/*<String>*/ usersForGroup = getAllMembers(getKey(groupName));
        if (usersForGroup != null)
        {
            usersForGroup.remove(getKey(userName));
        }
    }

    public List/*<String>*/ setMembers(String groupName, String[] userNameArray)
    {
        Assert.notNull(groupName);

        List/*<String>*/ userNames = null;
        if (userNameArray != null && userNameArray.length > 0)
        {
            userNames = new ArrayList(Arrays.asList(userNameArray));    // need a mutable list
            cacheAllMembers(groupName, userNames);
        }
        return userNames;
    }

    public void removeCachedGroupMembership(String userName, String groupName)
    {
        Assert.notNull(userName);
        Assert.notNull(groupName);

        removeMembership(userName, groupName);
        removeGroupFromUser(userName, groupName);
    }

    public void removeCachedGroupMemberships(String groupName)
    {
        Assert.notNull(groupName);

        // Remove group from all the appropriate user->groups and membership caches.
        List/*<String>*/ userNames = getAllMembers(groupName);
        if (userNames != null)
        {
            for (Iterator userIt = userNames.iterator(); userIt.hasNext();)
            {
                removeCachedGroupMembership((String) userIt.next(), groupName);
            }
        }
        removeAllMembers(groupName);
    }

    public void removeFromAllGroupNamesCache(String groupName)
    {
        Assert.notNull(groupName);

        List/*<String>*/ allGroups = getAllGroupNames();
        if (allGroups != null)
        {
            allGroups.remove(getKey(groupName));
        }
    }

    /**
     * @param groupName MUST be in the case returned by the underlying system (ie. not forced
     * to lower-case).
     */
    public void addToAllGroupNamesCache(String groupName)
    {
        Assert.notNull(groupName);

        List/*<String>*/ allGroups = getAllGroupNames();
        if (allGroups != null)
        {
            // could optimise by reworking underlying collection to be a SortedSet
            if (!allGroups.contains(groupName))
            {
                allGroups.add(groupName);
                Collections.sort(allGroups);
            }
        }

        getGroupNameCaseCache().put(new Element(getKey(groupName), groupName));
    }

    public void removeCachedUser(String userName, String groupName)
    {
        Assert.notNull(userName);
        Assert.notNull(groupName);

        removeMembership(userName, groupName);
        removeUserFromGroup(userName, groupName);
    }

    public void removeAllMemberships(String userName)
    {
        Assert.notNull(userName);

        List/*<String>*/ groups = getAllMemberships(userName);
        if (groups != null)
        {
            for (Iterator groupIt = groups.iterator(); groupIt.hasNext();)
            {
                removeCachedUser(userName, (String) groupIt.next());
            }
        }
        removeAllMembershipsFromUserMembershipsCache(userName);
    }

    public void setMembership(String userName, String groupName, Boolean isMember)
    {
        Assert.notNull(isMember);

        getGroupMembershipCache().put(new Element(new UserGroupKey(getKey(userName), getKey(groupName)), isMember));
    }

    public void removeMembership(String userName, String groupName)
    {
        getGroupMembershipCache().remove(new UserGroupKey(getKey(userName), getKey(groupName)));
    }

    public Boolean isMember(String userName, String groupName)
    {
        Element element = getGroupMembershipCache().get(new UserGroupKey(getKey(userName), getKey(groupName)));
        if (element == null)
        {
            return null;
        }
        return (Boolean) element.getValue();
    }

    /**
     * @return list of all memberships in lower-case.
     */
    public List/*<String>*/ getAllMemberships(String userName)
    {
        Element element = getUserMembershipsCache().get(getKey(userName));
        if (element == null)
        {
            return null;
        }
        return (List/*<String>*/) element.getValue();
    }

    public void cacheAllMemberships(String userName, List/*<String>*/ groupNames)
    {
        getUserMembershipsCache().put(new Element(getKey(userName), getKeys(groupNames)));
    }

    public void removeAllMembershipsFromUserMembershipsCache(String userName)
    {
        getUserMembershipsCache().remove(getKey(userName));
    }

    /**
     * @return list of all members in lower-case.
     */
    public List/*<String>*/ getAllMembers(String groupName)
    {
        Element element = getGroupMembersCache().get(getKey(groupName));
        if (element == null)
        {
            return null;
        }
        return (List/*<String>*/) element.getValue();
    }

    public void cacheAllMembers(String groupName, List/*<String>*/ userNames)
    {
        getGroupMembersCache().put(new Element(getKey(groupName), getKeys(userNames)));
    }

    public void removeAllMembers(String groupName)
    {
        getGroupMembersCache().remove(getKey(groupName));
    }

    public Boolean isUserOrGroup(String name)
    {
        Boolean isUserInGroup = null;
        Element element = getIsUserOrGroupCache().get(getKey(name));
        if (element != null)
        {
            isUserInGroup = (Boolean) element.getValue();
        }

        return isUserInGroup;
    }

    public void addIsUserOrGroup(String name, Boolean isValidUserOrGroup)
    {
        getIsUserOrGroupCache().put(new Element(getKey(name), isValidUserOrGroup));
    }

    /**
     * @param userName MUST be in the case returned by the underlying system (ie. not forced to lower-case).
     */
    public void addToAllUsers(String userName)
    {
        Assert.notNull(userName);

        List/*<String>*/ userNames = getAllUserNames();
        if (userNames != null)  // no cache; when fetched will contain all users.
        {
            // could optimise by reworking underlying collection to be a SortedSet
            if (!userNames.contains(userName))
            {
                userNames.add(userName);
                Collections.sort(userNames);
            }

            getUserNameCaseCache().put(new Element(getKey(userName), userName));
        }
    }

    /**
     * @param userName can be in a case different to that stored in the underlying system.
     */
    public void removeFromAllUsers(String userName)
    {
        Assert.notNull(userName);

        List/*<String>*/ userNames = getAllUserNames();
        if (userNames != null)
        {
            // TODO: this is slow, but not much slower than the case-sensitive version
            // TODO: we should really move all collections in this cache to Sets and not Lists
            // TODO: um..hello ConcurrentModificationException!..wait..the entire client-side cache is "very optimistic" with regards to threadsafety..

            for (Iterator iterator = userNames.iterator(); iterator.hasNext();)
            {
                String name = (String) iterator.next();
                if (userName.equalsIgnoreCase(name))
                {
                    iterator.remove();
                    break;
                }
            }
        }
    }

    public String getUserName(String username)
    {
        Element element = getUserNameCaseCache().get(getKey(username));
        if (element != null)
        {
            return (String) element.getValue();
        }
        else
        {
            return null;
        }
    }

    public String getGroupName(String groupname)
    {
        Element element = getGroupNameCaseCache().get(getKey(groupname));
        if (element != null)
        {
            return (String) element.getValue();
        }
        else
        {
            return null;
        }
    }

    /**
     * A composite key for group membership. Allows us to cache negative group memberships.
     */
    private static class UserGroupKey implements Serializable
    {
        private String userName;
        private String groupName;

        UserGroupKey(String userName, String groupName)
        {
            Assert.notNull(userName);
            Assert.notNull(groupName);

            this.userName = userName;
            this.groupName = groupName;
        }

        public boolean equals(Object o)
        {
            if (o == null || !(o instanceof UserGroupKey))
            {
                return false;
            }
            UserGroupKey other = (UserGroupKey) o;
            return userName.equals(other.userName) && groupName.equals(other.groupName);
        }

        public int hashCode()
        {
            return (userName.hashCode() / 2) + (groupName.hashCode() / 2);
        }
    }
}