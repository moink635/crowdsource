package com.atlassian.crowd.integration.exception;

/**
 * Thrown when the supplied credential is not valid.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class InvalidCredentialException extends CheckedSoapException
{
    public InvalidCredentialException()
    {
    }

    public InvalidCredentialException(String message)
    {
        super(message);
    }

    public InvalidCredentialException(String message, Throwable cause)
    {
        super(message, cause);
    }

    /**
     * Default constructor.
     *
     * @param throwable the {@link Exception Exception}.
     */
    public InvalidCredentialException(Throwable throwable)
    {
        super(throwable);
    }
}
