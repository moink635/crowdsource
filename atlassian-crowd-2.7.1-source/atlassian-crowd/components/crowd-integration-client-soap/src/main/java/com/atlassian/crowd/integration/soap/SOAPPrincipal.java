/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.integration.soap;

import java.io.Serializable;
import java.util.Date;

public class SOAPPrincipal extends SOAPEntity implements Serializable
{
    public SOAPPrincipal()
    {
    }

    public SOAPPrincipal(String name)
    {
        this.name = name;
    }

    public SOAPPrincipal(final long ID, final String name, final long directoryId, final String description, final boolean active, final Date conception, final Date lastModified, final SOAPAttribute[] attributes)
    {
        super(ID, name, directoryId, description, active, conception, lastModified, attributes);
    }
}