package com.atlassian.crowd.service.cache;

import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Non-domain-specific methods
 */
public interface BasicCache
{
    /**
     * Clears the underlying cache. May clear more than just one cache - assume that all state in all caches is lost
     * when you call this.
     */
    void flush();

    /**
     * Returns the group, or null if it's not in the cache.
     *
     * @param groupName the group to retrieve
     * @return the group or null if it is not in cache
     */
    SOAPGroup getGroup(String groupName);

    /**
     * Adds a group, or if it exists, replaces the old <code>SOAPGroup</code> instance.
     *
     * @param group the group to add/replace in cache
     */
    void cacheGroup(SOAPGroup group);

    /**
     * Removes the group, if it exists
     *
     * @param groupName the group to remove in cache
     */
    void removeGroup(String groupName);

    /**
     * Returns a List of Strings containing all group names, or null if the list is not cached.
     *
     * @return a List of Strings containing all group names, or null if the list is not cached.
     */
    List/*<String>*/ getAllGroupNames();

    /**
     * Adds, or if it already exists, replaces the list of all group names.
     *
     * @param groupNames list of group names to add/replace
     */
    void cacheAllGroupNames(List/*<String>*/ groupNames);

    /**
     * Cache a map of group name -> ancestor group names.
     *
     * @param ancestorsByGroup map containing ancestor group names by group name
     */
    void cacheAncestorsForGroups(Map<String, Set<String>> ancestorsByGroup);

    /**
     * Returns a map of group name -> ancestor group names, or null if the map is not cached.
     *
     * @return map containing ancestor group names by group name
     */
    Map<String, Set<String>> getAncestorsForGroups();

    /**
     * @return true if the cache contains enough information to reliably compute parent group relationships. If not,
     * returns false.
     */
    boolean containsGroupRelationships();

    /**
     * Adds or replaces a membership relationship between the user and the group. Updates the list of memberships
     * returned by <code>getAllMemberships()</code> to keep them in sync.
     *
     * @param userName the username associated to the membership to cache
     * @param groupName the group name associated to the membership to cache
     * @param isMember  true if the user is a member, false if not.
     */
    void setMembership(String userName, String groupName, Boolean isMember);

    /**
     * Remove any stored [yes,no] membership relationship for a userName & groupName combination.
     *
     * @param userName the username associated to the membership to cache
     * @param groupName the group name associated to the membership to cache
     */
    void removeMembership(String userName, String groupName);

    /**
     * Maintains a list of true/false for "is this user a member of this group". Returns null if no information is
     * cached.
     *
     * @param userName the username associated to the membership to update
     * @param groupName the group name associated to the membership to update
     * @return true if the user is a member of the group, false if not. If there is no information in cache, returns null
     */
    Boolean isMember(String userName, String groupName);

    /**
     * Returns a list of groups that <code>userName</code> belongs to, or null if they don't belong to any.
     *
     * @param userName the user
     * @return list of groups that the specified user belongs to, or null if they don't belong to any.
     */
    List/*<String>*/ getAllMemberships(String userName);

    /**
     * Cache a list of all groups this user belongs to.
     *
     * @param userName the username associated to the membership to cache
     * @param groupNames list of group names associated to the membership to cache
     */
    void cacheAllMemberships(String userName, List/*<String>*/ groupNames);

    /**
     * Removes the list of groups the user belongs to. Used when the user is being deleted.
     *
     * @param userName the username for which the memberships will be removed
     */
    void removeAllMembershipsFromUserMembershipsCache(String userName);

    /**
     * Returns a list of users that belong to <code>groupName</code> belongs to, or null if there aren't any.
     *
     * @param groupName the group
     * @return list of users belonging to the specified group. null if there are no memberships
     */
    List/*<String>*/ getAllMembers(String groupName);

    /**
     * Cache a list of all users in this group.
     *
     * @param groupName the group name to cache
     * @param userNames the list of usersnames to cache
     */
    void cacheAllMembers(String groupName, List/*<String>*/ userNames);

    /**
     * Removes the list of users in the group. Used when the group is being deleted.
     *
     * @param groupName the group name for which the memberships will be removed
     */
    void removeAllMembers(String groupName);

    /**
     * Will return true if there is enough information in the cache to determine if the string represents a user or group
     * @param name the name to check
     * @return if there is enough information in the cache true iff the string represents a user or group. null if there is no information in the cache
     */
    Boolean isUserOrGroup(String name);

    /**
     * Updates the cache with information if the specified name is a valid user or group
     * @param name the name to add to cache
     * @param isValidUserOrGroup boolean to signify if the specified name is a valid user or group
     */
    void addIsUserOrGroup(String name, Boolean isValidUserOrGroup);

    /**
     * Adds, or if they exist, replaces an existing user.
     *
     * @param user the user to add/replace
     */
    void cacheUser(SOAPPrincipal user);

    /**
     * Removes the user from the cache.
     *
     * @param userName the user to remove
     * @return true if the user was cached, false otherwise.
     */
    boolean removeUser(String userName);

    /**
     * Returns the {@link SOAPPrincipal} representing <code>userName</code>, or <code>null</code> if it's not in the
     * cache.
     *
     * @param userName the user to retrieve
     * @return SOAPPrincipal representing the user, or null if not in cache
     */
    SOAPPrincipal getUser(String userName);

    /**
     * Returns the {@link SOAPPrincipal} with Attributes representing <code>userName</code>, or <code>null</code> if it's not in the
     * cache.
     *
     * @param userName the user to retrieve
     * @return SOAPPrincipal with Attributes representing the user, or null i fnot in cache
     */
    SOAPPrincipal getUserWithAttributes(String userName);

    /**
     * @return a List with all user names, or null if none are cached.
     */
    List/*<String>*/ getAllUserNames();

    /**
     * Adds a list of all users, or if one already exists, replaces it.
     *
     * @param userNames the list of usernames to add
     */
    void cacheAllUserNames(List/*<String>*/ userNames);

    /**
     * Checks the group->user cache to see if the user is a member.
     *
     * @param userName the user to check
     * @param groupName the group to check
     * @return <code>Boolean.TRUE</code> if the user is a member. <code>Boolean.FALSE</code> if not. null if we don't
     *         have enough information to tell.
     */
    Boolean isMemberInGroupCache(String userName, String groupName);

    /**
     * Adds the group to the user/group membership in cache
     * @param userName the user to update
     * @param groupName the group to add
     */
    void addGroupToUser(String userName, String groupName);

    /**
     * Adds the user to the user/group membership in cache
     * @param userName the user to add
     * @param groupName the group to update
     */
    void addUserToGroup(String userName, String groupName);

    /**
     * Removes the group from the user/group membership in the cache
     * @param userName the user to update
     * @param groupName the group to remove
     */
    void removeGroupFromUser(String userName, String groupName);

    /**
     * Removes the user from the user/group membership in the cache
     * @param userName the user to remove
     * @param groupName the group to update
     */
    void removeUserFromGroup(String userName, String groupName);

    /**
     * Caches the member list for a group called <code>groupName</code>. Since group member information arrives when
     * the group is fetched from the Crowd server, this is also called from {@link com.atlassian.crowd.service.cache.CachingGroupManager#getGroup(String)}.
     *
     * @param groupName the group
     * @param userNameArray the list of user names
     * @return A <code>List<String></code> of all members of the group.
     */
    List/*<String>*/ setMembers(String groupName, String[] userNameArray);

    /**
     * Removes all the membership details for this specific userName, groupName pair, when a group is being removed.
     *
     * @param userName the user associated with the membership
     * @param groupName the group associate with the membership
     */
    void removeCachedGroupMembership(String userName, String groupName);

    /**
     * Called by {@link com.atlassian.crowd.service.cache.CachingGroupManager#removeGroup(String)} to ensure the group details are removed from any and
     * all user->groups and (user+group)->[yes,no] caches.
     *
     * @param groupName the group to clean up
     */
    void removeCachedGroupMemberships(String groupName);

    /**
     * When we remove a group, if we have already cached the list of all available groups, we need to remove this group.
     *
     * @param groupName the group to remove form cache
     */
    void removeFromAllGroupNamesCache(String groupName);

    /**
     * If we have a list of all groups, when we add one we should update the list.
     *
     * @param groupName the group to add to cache
     */
    void addToAllGroupNamesCache(String groupName);

    /**
     * Removes membership details for the specified user/group
     * @param userName the user to update
     * @param groupName the group to update
     */
    void removeCachedUser(String userName, String groupName);

    /**
     * Called by {@link com.atlassian.crowd.service.cache.CachingUserManager#removeUser(String)} to ensure the users details are removed from any and
     * all user->groups and (user+group)->[yes,no] caches.
     *
     * @param userName the user to cleanup
     */
    void removeAllMemberships(String userName);

    /**
     * Adds <code>userName</code> to the list of all users in the system.
     *
     * @param userName the user to add to cache
     */
    void addToAllUsers(String userName);

    /**
     * Removes the user from the cache
     * @param userName the user to remove
     */
    void removeFromAllUsers(String userName);

    /**
     * Returns the username in correct case if it exists in the cache, null otherwise.
     *
     * @param username the username
     * @return the username in correct case, null if it doesn't exist in cache
     */
    String getUserName(String username);

    /**
     * Returns the groupname in correct case if it exists in the cache, null otherwise.
     *
     * @param groupname the group
     * @return the group name in correct case, null if it doesn't exist in cache
     */
    String getGroupName(String groupname);

    /**
     * Caches the specified user and it's properties
     * @param user SOAPPrincipal to cache
     */
    void cacheUserWithAttributes(SOAPPrincipal user);
}
