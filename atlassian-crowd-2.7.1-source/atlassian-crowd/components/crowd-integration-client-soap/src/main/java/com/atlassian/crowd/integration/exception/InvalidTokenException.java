package com.atlassian.crowd.integration.exception;

/**
 * Thrown when an invalid token is provided.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class InvalidTokenException extends CheckedSoapException
{
    /**
     * Default constructor.
     */
    public InvalidTokenException()
    {
    }

    /**
     * Default constructor.
     *
     * @param s The message.
     */
    public InvalidTokenException(String s)
    {
        super(s);
    }

    /**
     * Default constructor.
     *
     * @param s         The message.
     * @param throwable the {@link Exception Exception}.
     */
    public InvalidTokenException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    /**
     * Default constructor.
     *
     * @param throwable the {@link Exception Exception}.
     */
    public InvalidTokenException(Throwable throwable)
    {
        super(throwable);
    }
}
