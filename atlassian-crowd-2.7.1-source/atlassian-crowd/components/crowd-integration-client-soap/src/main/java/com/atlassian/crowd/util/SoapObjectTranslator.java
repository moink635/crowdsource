package com.atlassian.crowd.util;

/**
 * Converts to and from SOAP versions of classes.
 */
public class SoapObjectTranslator
{
    /**
     * Converts to the SOAP version of <tt>PasswordCredential</tt>.
     * @param passwordCredential PasswordCredential
     * @return SOAP version of PasswordCredential
     */
    public static com.atlassian.crowd.integration.authentication.PasswordCredential toSoapPasswordCredential(com.atlassian.crowd.embedded.api.PasswordCredential passwordCredential)
    {
        if (passwordCredential == null)
        {
            return null;
        }
        else
        {
            return new com.atlassian.crowd.integration.authentication.PasswordCredential(passwordCredential.getCredential(), passwordCredential.isEncryptedCredential());
        }
    }

    /**
     * Converts from SOAP version of <tt>PasswordCredential</tt>.
     * @param passwordCredential PasswordCredential
     * @return {@link com.atlassian.crowd.embedded.api.PasswordCredential}
     */
    public static com.atlassian.crowd.embedded.api.PasswordCredential fromSoapPasswordCredential(com.atlassian.crowd.integration.authentication.PasswordCredential passwordCredential)
    {
        if (passwordCredential == null)
        {
            return null;
        }
        else
        {
            return new com.atlassian.crowd.embedded.api.PasswordCredential(passwordCredential.getCredential(), passwordCredential.isEncryptedCredential());
        }
    }

    /**
     * Converts to the SOAP version of <tt>ValidationFactor</tt>.
     * @param validationFactor ValidationFactor
     * @return SOAP version of ValidationFactor
     */
    public static com.atlassian.crowd.integration.authentication.ValidationFactor toSoapValidationFactor(com.atlassian.crowd.model.authentication.ValidationFactor validationFactor)
    {
        if (validationFactor == null)
        {
            return null;
        }
        else
        {
            return new com.atlassian.crowd.integration.authentication.ValidationFactor(validationFactor.getName(), validationFactor.getValue());
        }
    }

    /**
     * Converts from SOAP version of <tt>ValidationFactor</tt>.
     * @param validationFactor ValidationFactor
     * @return {@link com.atlassian.crowd.model.authentication.ValidationFactor}
     */
    public static com.atlassian.crowd.model.authentication.ValidationFactor fromSoapValidationFactor(com.atlassian.crowd.integration.authentication.ValidationFactor validationFactor)
    {
        if (validationFactor == null)
        {
            return null;
        }
        else
        {
            return new com.atlassian.crowd.model.authentication.ValidationFactor(validationFactor.getName(), validationFactor.getValue());
        }
    }

    /**
     * Converts to the SOAP version of <tt>ValidationFactor</tt>.
     * @param validationFactors ValidationFactors
     * @return SOAP version of ValidationFactors
     */
    public static com.atlassian.crowd.integration.authentication.ValidationFactor[] toSoapValidationFactors(com.atlassian.crowd.model.authentication.ValidationFactor[] validationFactors)
    {
        if (validationFactors == null)
        {
            return null;
        }
        else
        {
            com.atlassian.crowd.integration.authentication.ValidationFactor[] result = new com.atlassian.crowd.integration.authentication.ValidationFactor[validationFactors.length];
            for (int i = 0; i < validationFactors.length; i++)
            {
                result[i] = toSoapValidationFactor(validationFactors[i]);
            }
            return result;
        }
    }

    /**
     * Converts from SOAP version of <tt>ValidationFactor</tt>.
     * @param validationFactors ValidationFactor
     * @return {@link com.atlassian.crowd.model.authentication.ValidationFactor}
     */
    public static com.atlassian.crowd.model.authentication.ValidationFactor[] fromSoapValidationFactors(com.atlassian.crowd.integration.authentication.ValidationFactor[] validationFactors)
    {
        if (validationFactors == null)
        {
            return null;
        }
        else
        {
            com.atlassian.crowd.model.authentication.ValidationFactor[] result = new com.atlassian.crowd.model.authentication.ValidationFactor[validationFactors.length];
            for (int i = 0; i < validationFactors.length; i++)
            {
                result[i] = fromSoapValidationFactor(validationFactors[i]);
            }
            return result;
        }
    }

    /**
     * Converts to the SOAP version of <tt>AuthenticatedToken</tt>.
     * @param authenticatedToken AuthenticatedToken
     * @return SOAP version of AuthenticatedToken
     */
    public static com.atlassian.crowd.integration.authentication.AuthenticatedToken toSoapAuthenticatedToken(com.atlassian.crowd.model.authentication.AuthenticatedToken authenticatedToken)
    {
        if (authenticatedToken == null)
        {
            return null;
        }
        else
        {
            return new com.atlassian.crowd.integration.authentication.AuthenticatedToken(authenticatedToken.getName(), authenticatedToken.getToken());
        }
    }

    /**
     * Converts from SOAP version of <tt>AuthenticatedToken</tt>.
     * @param authenticatedToken AuthenticatedToken
     * @return {@link com.atlassian.crowd.model.authentication.AuthenticatedToken}
     */
    public static com.atlassian.crowd.model.authentication.AuthenticatedToken fromSoapAuthenticatedToken(com.atlassian.crowd.integration.authentication.AuthenticatedToken authenticatedToken)
    {
        if (authenticatedToken == null)
        {
            return null;
        }
        else
        {
            return new com.atlassian.crowd.model.authentication.AuthenticatedToken(authenticatedToken.getName(), authenticatedToken.getToken());
        }
    }

    /**
     * Converts to SOAP version of <tt>UserAuthenticationContext</tt>.
     * @param context UserAuthenticationContext
     * @return {@link com.atlassian.crowd.integration.authentication.UserAuthenticationContext}
     */
    public static com.atlassian.crowd.integration.authentication.UserAuthenticationContext toSoapUserAuthenticationContext(com.atlassian.crowd.model.authentication.UserAuthenticationContext context)
    {
        if (context == null)
        {
            return null;
        }
        else
        {
            return new com.atlassian.crowd.integration.authentication.UserAuthenticationContext(context.getName(), toSoapPasswordCredential(context.getCredential()), toSoapValidationFactors(context.getValidationFactors()), context.getApplication());
        }
    }

    /**
     * Converts from SOAP version of <tt>UserAuthenticationContext</tt>.
     * @param context UserAuthenticationContext
     * @return SOAP version of UserAuthenticationContext
     */
    public static com.atlassian.crowd.model.authentication.UserAuthenticationContext fromSoapUserAuthenticationContext(com.atlassian.crowd.integration.authentication.UserAuthenticationContext context)
    {
        if (context == null)
        {
            return null;
        }
        else
        {
            return new com.atlassian.crowd.model.authentication.UserAuthenticationContext(context.getName(), fromSoapPasswordCredential(context.getCredential()), fromSoapValidationFactors(context.getValidationFactors()), context.getApplication());
        }
    }

    /**
     * Converts to SOAP version of <tt>ApplicationAuthenticationContext</tt>.
     * @param context ApplicationAuthenticationContext
     * @return {@link com.atlassian.crowd.integration.authentication.ApplicationAuthenticationContext}
     */
    public static com.atlassian.crowd.integration.authentication.ApplicationAuthenticationContext toSoapApplicationAuthenticationContext(com.atlassian.crowd.model.authentication.ApplicationAuthenticationContext context)
    {
        if (context == null)
        {
            return null;
        }
        else
        {
            return new com.atlassian.crowd.integration.authentication.ApplicationAuthenticationContext(context.getName(), toSoapPasswordCredential(context.getCredential()), toSoapValidationFactors(context.getValidationFactors()));
        }
    }

    /**
     * Converts from SOAP version of <tt>ApplicationAuthenticationContext</tt>.
     * @param context ApplicationAuthenticationContext
     * @return SOAP version of ApplicationAuthenticationContext
     */
    public static com.atlassian.crowd.model.authentication.ApplicationAuthenticationContext fromSoapApplicationAuthenticationContext(com.atlassian.crowd.integration.authentication.ApplicationAuthenticationContext context)
    {
        if (context == null)
        {
            return null;
        }
        else
        {
            return new com.atlassian.crowd.model.authentication.ApplicationAuthenticationContext(context.getName(), fromSoapPasswordCredential(context.getCredential()), fromSoapValidationFactors(context.getValidationFactors()));
        }
    }
}
