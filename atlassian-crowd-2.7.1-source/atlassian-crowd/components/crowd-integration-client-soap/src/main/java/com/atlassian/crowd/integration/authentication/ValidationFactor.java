package com.atlassian.crowd.integration.authentication;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Validation factors are used to construct a unique token when a {@see com.atlassian.crowd.integration.model.RemotePrincipal}
 * authenticates with the crowd server. When the remote principal later makes an authentication attempt, if the validation
 * factors do not match the client will then be considered invalid.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility.
 */
public class ValidationFactor implements Serializable
{
    /**
     * The key to use when setting the client remote address. Host should not be used because if a DNS server is
     * compromised, the host value can then be forged.
     */
    public static final String REMOTE_ADDRESS = "remote_address";

    /**
     * If you need to set the host - use only for an application.
     */
    public static final String REMOTE_HOST = "remote_host";

    /**
     * The key to use when setting the client remote forwarding address through a proxy.
     */
    public static final String X_FORWARDED_FOR = "X-Forwarded-For";

    /**
     * The Key that represents the generated 'Random-Number' ValidationFactor
     */
    public static final String RANDOM_NUMBER = "Random-Number";

    /**
     * Application name
     */
    public static final String NAME = "NAME";

    /**
     * Application-specific privilege level.
     * This exists to allow applications to create multiple tokens for the same user
     * with different privileges (e.g., WebSudo). Its values are application-specific.
     */
    public static final String PRIVILEGE_LEVEL = "PRIVILEGE_LEVEL";

    private String name;
    private String value;

    public ValidationFactor()
    {
    }

    public ValidationFactor(String name, String value)
    {
        this.name = name;
        this.value = value;
    }

    /**
     * Gets the name.
     *
     * @return The name.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name The name.
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Gets the value.
     *
     * @return The value.
     */
    public String getValue()
    {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value The value.
     */
    public void setValue(String value)
    {
        this.value = value;
    }

    public String toString()
    {
        return new ToStringBuilder(this).
                append("name", name).
                append("value", value).toString();
    }

    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof ValidationFactor))
        {
            return false;
        }

        ValidationFactor that = (ValidationFactor) o;

        if (name != null ? !name.equals(that.name) : that.name != null)
        {
            return false;
        }
        if (value != null ? !value.equals(that.value) : that.value != null)
        {
            return false;
        }

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
