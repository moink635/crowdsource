package com.atlassian.crowd.service.cache;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.BulkAddFailedException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.soap.SOAPPrincipalWithCredential;
import com.atlassian.crowd.integration.soap.SearchRestriction;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.service.UserManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.util.Assert;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

public class CachingUserManager implements UserManager
{

    private final static String DEFAULT_BLANK = "-";
    private final Server server;
    private final BasicCache basicCache;

    public CachingUserManager(final SecurityServerClient securityServerClient, final BasicCache basicCache)
    {
        this.basicCache = basicCache;
        this.server = new Server(securityServerClient);
    }

    public SOAPPrincipal getUser(String userName)
            throws RemoteException, InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException
    {
        Assert.notNull(userName);

        SOAPPrincipal user = basicCache.getUser(userName);
        if (user == null)
        {
            user = server.getUser(userName);
            basicCache.cacheUser(user);
        }
        return user;
    }

    public SOAPPrincipal getUserWithAttributes(String userName)
            throws RemoteException, InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException
    {
        Assert.notNull(userName);

        SOAPPrincipal user = basicCache.getUserWithAttributes(userName);
        if (user == null)
        {
            user = server.getUserWithAttributes(userName);
            basicCache.cacheUserWithAttributes(user);
        }
        return user;
    }

    /**
     * Note: the lookup is not currently cached - it will always hit the server. The returned user is cached and will
     * by found by a call to getUser()
     */
    public SOAPPrincipal getUserFromToken(String token)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidTokenException, InvalidAuthenticationException
    {
        SOAPPrincipal user = server.getUserFromToken(token);
        basicCache.cacheUser(user);

        return user;
    }

    public List/*<SOAPPrincipal>*/ searchUsers(SearchRestriction[] restrictions)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        return server.searchUsers(restrictions);
    }

    public SOAPPrincipal addUser(SOAPPrincipal user, PasswordCredential credential)
            throws RemoteException, ApplicationPermissionException, InvalidCredentialException, InvalidUserException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        Assert.notNull(user);
        Assert.notNull(user.getName());

        List<SOAPAttribute> attributesToAdd = new ArrayList<SOAPAttribute>();
        updateNullToDefaultValue(user, UserConstants.EMAIL, DEFAULT_BLANK, attributesToAdd);
        updateNullToDefaultValue(user, UserConstants.FIRSTNAME, "", attributesToAdd);
        updateNullToDefaultValue(user, UserConstants.LASTNAME, "", attributesToAdd);
        updateNullToDefaultValue(user, UserConstants.DISPLAYNAME, "", attributesToAdd);

        if (!attributesToAdd.isEmpty())
        {
            if (user.getAttributes() != null)
            {
                attributesToAdd.addAll(Arrays.asList(user.getAttributes()));
            }
            user.setAttributes(attributesToAdd.toArray(new SOAPAttribute[attributesToAdd.size()]));
        }

        if (credential == null || credential.getCredential() == null || "".equals(credential.getCredential()))
        {
            credential = PasswordCredential.NONE;
        }
        user = server.addUser(user, credential);    // throws if there's a problem.
        basicCache.cacheUser(user);          // Adds the user as returned from the server to the cache.
        basicCache.addToAllUsers(user.getName());

        return user;
    }

     public void addAllUsers(final Collection<SOAPPrincipalWithCredential> users)
             throws InvalidAuthorizationTokenException, BulkAddFailedException, RemoteException, ApplicationPermissionException, InvalidAuthenticationException
     {
        Assert.notNull(users);
        server.addAllUsers(users);
        //Flushing cache as it will not include the new bulk added user
        basicCache.flush();
    }

    private void updateNullToDefaultValue(SOAPPrincipal user, String attributeName, String defaultValue, List<SOAPAttribute> attributesToAdd)
    {
        SOAPAttribute attribute = user.getAttribute(attributeName);

        if (attribute == null)
        {
            // need to add the attribute
            attribute = new SOAPAttribute(attributeName, defaultValue);
            attributesToAdd.add(attribute);
        }
        else if (attribute.getValues() == null || attribute.getValues().length == 0 || attribute.getValues()[0] == null)
        {
            // need to update the existing attribute
            String[] value = {defaultValue};
            attribute.setValues(value);
        }
    }

    public void updateUser(SOAPPrincipal user)
            throws RemoteException, ApplicationPermissionException, InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException
    {
        Assert.notNull(user);

        server.updateUser(user);            // will throw if there's a problem
        basicCache.cacheUser(user);
    }

    public void updatePassword(String userName, PasswordCredential credential)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidCredentialException, ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException
    {
        Assert.notNull(userName);

        server.updatePassword(userName, credential);
    }

    public void removeUser(String userName)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException
    {
        Assert.notNull(userName);

        server.removeUser(userName);     // throws on error

        // remove the user from the group->users cache & memberships cache.
        basicCache.removeAllMemberships(userName);

        basicCache.removeUser(userName);

        basicCache.removeFromAllUsers(userName);
    }

    /**
     * This optimises the background getAllUserNames() call. JIRA goes "give me all the usernames", followed by
     * "give me each user, individually". This is slow because we scan the directory once for the list of usernames,
     * then once again for each user.
     * <p/>
     * The optimisation reduces the number of directory scans to one. A huge benefit, particularly with Active
     * Directory.
     */
    /*@Override*/
    public List/*<String>*/ getAllUserNames()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        return getAllUserNamesFromCacheOrServer();
    }

    /**
     * With JIRA we're guaranteed that if there are any names in the user list, all the names are in the user list.
     * We go to the user cache because a map lookup should be much faster than using contains() on a List, because it's
     * implemented as a straight iterative search.
     */
    public boolean isUser(String userName)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        Assert.notNull(userName);

        getAllUserNamesFromCacheOrServer();                 // makes sure the cache is loaded with all users.
        return (basicCache.getUser(userName) != null);
    }

    /**
     * This guarantees that all usernames (and users) are in the cache, or none are.
     *
     * @return list of user names
     * @throws RemoteException if there was an error in the connection
     * @throws InvalidAuthorizationTokenException if the authorisation token is invalid
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    private List/*<String>*/ getAllUserNamesFromCacheOrServer()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        List/*<String>*/ userNames = basicCache.getAllUserNames();
        if (userNames == null)
        {
            loadAllUsers();
            userNames = basicCache.getAllUserNames();
        }
        return userNames;
    }

    /**
     * Loads all users from Crowd, builds a list of usernames, and caches both the list and the individual users.
     *
     * @throws RemoteException if there were communication problems
     * @throws InvalidAuthorizationTokenException if the authorisation token is invalid
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    private void loadAllUsers()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        List/*<SOAPPrincipal>*/ users = searchUsers(new SearchRestriction[]{});
        if (users != null)
        {
            List/*<String>*/ userNames = new ArrayList(users.size());
            for (Iterator userIt = users.iterator(); userIt.hasNext();)
            {
                SOAPPrincipal user = (SOAPPrincipal) userIt.next();

                userNames.add(user.getName());
                basicCache.cacheUser(user);
            }
            basicCache.cacheAllUserNames(userNames);
        }
    }

    /**
     * Encapsulates all server communication.
     */
    private static class Server
    {
        private final SecurityServerClient securityServerClient;

        private Server(SecurityServerClient securityServerClient)
        {
            this.securityServerClient = securityServerClient;
        }

        /**
         * Handles communication with the server. See {@link UserManager#addUser(com.atlassian.crowd.integration.soap.SOAPPrincipal , com.atlassian.crowd.embedded.api.PasswordCredential)} for Javadoc.
         *
         * @param user New user
         * @param credential password of the user
         * @return new user
         * @throws RemoteException if there was a communication problem
         * @throws ApplicationPermissionException if the application is not permitted to perform the operation
         * @throws InvalidUserException if the given user is invalid
         * @throws InvalidCredentialException if the given credential is not valid
         * @throws InvalidAuthorizationTokenException if the authorisation token is not valid
         * @throws InvalidAuthenticationException if the application name/password is not valid
         */
        protected SOAPPrincipal addUser(SOAPPrincipal user, PasswordCredential credential)
                throws RemoteException, ApplicationPermissionException, InvalidCredentialException, InvalidUserException, InvalidAuthorizationTokenException, InvalidAuthenticationException
        {
            return securityServerClient.addPrincipal(user, credential);
        }

        /**
         * Handles communication with the server. See {@link UserManager#updateUser(com.atlassian.crowd.integration.soap.SOAPPrincipal)} for Javadoc.
         *
         * @param user updated user
         * @throws RemoteException if there was a communication problem
         * @throws ApplicationPermissionException if the application is not permitted to update the user
         * @throws UserNotFoundException if the user could not be found
         * @throws InvalidAuthorizationTokenException if the authorisation token is invalid
         * @throws InvalidAuthenticationException if the application name/password combination is invalid
         */
        protected void updateUser(SOAPPrincipal user)
                throws RemoteException, ApplicationPermissionException, InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException
        {
            SOAPAttribute[] attributes = user.getAttributes();
            if (attributes != null)
            {
                for (SOAPAttribute attribute : attributes)
                {
                    securityServerClient.updatePrincipalAttribute(user.getName(), attribute);
                }
            }
        }

        /**
         * Handles server communication. See {@link UserManager#updatePassword(String, com.atlassian.crowd.embedded.api.PasswordCredential)} for Javadoc.
         *
         * @param userName name of the user to update
         * @param credential new password
         * @throws RemoteException if there was a problem with the communication
         * @throws InvalidCredentialException if the given credential does not satisfy the criteria for a valid password
         * @throws UserNotFoundException if the user could not be found
         * @throws ApplicationPermissionException if the application does not have permission to update the user password
         * @throws InvalidAuthorizationTokenException if the authorisation token is invalid
         * @throws InvalidAuthenticationException if the application name/password combination is invalid
         */
        protected void updatePassword(String userName, PasswordCredential credential)
                throws RemoteException, InvalidAuthorizationTokenException, InvalidCredentialException, ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException
        {
            securityServerClient.updatePrincipalCredential(userName, credential);
        }

        /**
         * Handles communication with the server. See {@link UserManager#removeUser(String)} for Javadoc.
         *
         * @param userName The user to remove.
         * @throws RemoteException if there was a problem with the communication
         * @throws UserNotFoundException if the user could not be found
         * @throws ApplicationPermissionException if the application is not permitted to remove the user
         * @throws InvalidAuthorizationTokenException if the authorisation token is invalid
         * @throws InvalidAuthenticationException if the application name/password combination is invalid
         */
        protected void removeUser(String userName)
                throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException
        {
            securityServerClient.removePrincipal(userName);
        }

        /**
         * Handles communication with the server. See {@link com.atlassian.crowd.service.UserManager#getUser(String)} for Javadoc.
         *
         * @param userName name of the user to return
         * @return the user
         * @throws RemoteException if there was a problem with the communication
         * @throws UserNotFoundException if the user could not be found
         * @throws InvalidAuthorizationTokenException if the authorisation token is invalid
         * @throws InvalidAuthenticationException if the application name/password combination is invalid
         */
        protected SOAPPrincipal getUser(String userName)
                throws RemoteException, InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException
        {
            return securityServerClient.findPrincipalByName(userName);
        }

        /**
         * Handles communication with the server. See {@link UserManager#getUserFromToken(String)} for Javadoc.
         *
         * @param token authenticated token.
         * @return authenticated user
         * @throws RemoteException if there was a problem with the communication
         * @throws InvalidTokenException if the given token is invalid
         * @throws InvalidAuthorizationTokenException if the authorisation token is invalid
         * @throws InvalidAuthenticationException if the application name/password combination is invalid
         */
        protected SOAPPrincipal getUserFromToken(String token)
                throws RemoteException, InvalidAuthorizationTokenException, InvalidTokenException, InvalidAuthenticationException
        {
            return securityServerClient.findPrincipalByToken(token);
        }


        protected List/*<String>*/ getAllUserNames()
                throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
        {
            String[] userNames = securityServerClient.findAllPrincipalNames();
            if (userNames != null)
            {
                return new ArrayList(Arrays.asList(userNames)); // deep copy, as we need the array to be mutable
            }
            else
            {
                return null;
            }
        }

        public List/*<SOAPPrincipal>*/ searchUsers(SearchRestriction[] restrictions)
                throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
        {
            SOAPPrincipal[] users = securityServerClient.searchPrincipals(restrictions);
            if (users != null)
            {
                return Arrays.asList(users);
            }
            else
            {
                return null;
            }
        }

        public void addAllUsers(final Collection<SOAPPrincipalWithCredential> users)
                throws InvalidAuthorizationTokenException, BulkAddFailedException, RemoteException, ApplicationPermissionException, InvalidAuthenticationException
        {
            securityServerClient.addAllPrincipals(users);
        }

        public SOAPPrincipal getUserWithAttributes(String userName)
                throws UserNotFoundException, InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException
        {
            return securityServerClient.findPrincipalWithAttributesByName(userName);
        }

    }
}
