package com.atlassian.crowd.service;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.BulkAddFailedException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.soap.SOAPPrincipalWithCredential;
import com.atlassian.crowd.integration.soap.SearchRestriction;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.List;

/**
 * Methods related to operations on users, but not on their group or role memberships.
 */
public interface UserManager
{
    /**
     * Returns <code>true</code> if <code>userName</code> represents a valid user. Will return true if the user is valid
     * but inactive.
     *
     * @param userName name of the user
     * @return true if the user is valid, false otherwise.
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                         The application (not the user) was not authenticated correctly.
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    boolean isUser(String userName)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Given a <code>userName</code>, fetches the user's details, either from cache or from the Crowd server.
     *
     * @param userName The user's identifier
     * @return An object representing the user
     * @throws RemoteException       A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                               The application (not the user) was not authenticated correctly.
     * @throws UserNotFoundException Could not find the user.
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    SOAPPrincipal getUser(String userName)
            throws RemoteException, InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException;

    /**
     * Given a <code>userName</code>, fetches the user's details and their associated attributes, either from cache or from the Crowd server.
     *
     * @param userName The user's identifier
     * @return An object representing the user and their attributes
     * @throws RemoteException         A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                                 The application (not the user) was not authenticated correctly.
     * @throws UserNotFoundException Could not find the user.
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    SOAPPrincipal getUserWithAttributes(String userName)
            throws RemoteException, InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException;

    /**
     * Given an authentication token, retrieves the user associated with it.
     *
     * @param token The token presented by the client browser to the webserver.
     * @return An object representing the user
     * @throws RemoteException       A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                               The application (not the user) was not authenticated correctly.
     * @throws InvalidTokenException The token presented was not a valid Crowd token.
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    SOAPPrincipal getUserFromToken(String token)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidTokenException, InvalidAuthenticationException;


    /**
     * Searches the list of all available users based on the passed-in <code>restrictions</code> and returns a list
     * of users that match.
     *
     * @param restrictions search restrictions
     * @return A {@link java.util.List} of {@link com.atlassian.crowd.integration.soap.SOAPPrincipal}s that match the criteria.
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                         The application (not the user) was not authenticated correctly.
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    List/*<SOAPPrincipal>*/ searchUsers(SearchRestriction[] restrictions)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Adds a user to Crowd.
     *
     * @param user       The user to add to Crowd
     * @param credential The credential (eg. password) for the user. May be null.
     * @return The <code>SOAPPrincipal</code>, as returned by the Crowd server.
     * @throws RemoteException                A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                                        The application (not the user) was not authenticated correctly.
     * @throws ApplicationPermissionException The application does not have rights to add users.
     * @throws InvalidUserException           The user was malformed or already exists.
     * @throws InvalidCredentialException     The credentials were malformed or did not meet directory complexity
     *                                        requirements.
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    SOAPPrincipal addUser(SOAPPrincipal user, PasswordCredential credential)
            throws RemoteException, ApplicationPermissionException, InvalidCredentialException, InvalidUserException,
            InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Adds all users to Crowd.
     * N.B.: After adding the users the cache gets flushed.
     *
     * @param users The users to add to Crowd
     * @throws InvalidAuthorizationTokenException
     *                                        The application (not the user) was not authenticated correctly.
     * @throws RemoteException                A communication error occurred - the Crowd server may not be available.
     * @throws ApplicationPermissionException The application does not have rights to add users.
     * @throws BulkAddFailedException         Thrown if it failed to create a user in of the directories.
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    void addAllUsers(Collection<SOAPPrincipalWithCredential> users)
            throws InvalidAuthorizationTokenException, RemoteException, ApplicationPermissionException, BulkAddFailedException, InvalidAuthenticationException;

    /**
     * Updates a user's details in Crowd.
     *
     * @param user The user to update
     * @throws RemoteException                A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                                        The application (not the user) was not authenticated correctly.
     * @throws UserNotFoundException          The user to update could not be found.
     * @throws ApplicationPermissionException This application does not have the rights to update the user.
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    void updateUser(SOAPPrincipal user)
            throws RemoteException, ApplicationPermissionException,
            InvalidAuthorizationTokenException, UserNotFoundException, InvalidAuthenticationException;

    /**
     * Changes the password for the user specified by <code>userName</code>.
     *
     * @param userName   The identifier of the user
     * @param credential The new credentials for the user.
     * @throws RemoteException                A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                                        The application (not the user) was not authenticated correctly.
     * @throws InvalidCredentialException     The credentials were malformed or did not meet directory complexity
     *                                        requirements.
     * @throws UserNotFoundException          The user could not be found in any directory mapped to this application.
     * @throws ApplicationPermissionException This application does not have the rights to update the user's password.
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    void updatePassword(String userName, PasswordCredential credential)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidCredentialException,
            ApplicationPermissionException, UserNotFoundException, InvalidAuthenticationException;

    /**
     * Removes a user from Crowd.
     *
     * @param userName The name of the user to remove.
     * @throws RemoteException                A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                                        The application (not the user) was not authenticated correctly.
     * @throws UserNotFoundException          The user to remove could not be found.
     * @throws ApplicationPermissionException This application is not allowed to remove this user.
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     */
    void removeUser(String userName)
            throws RemoteException, InvalidAuthorizationTokenException, UserNotFoundException,
            ApplicationPermissionException, InvalidAuthenticationException;

    /**
     * Returns a list of all available users. We strongly recommend against use of this API; it will prevent your
     * application from scaling.
     * <p/>
     * If you're implementing a user-picker or similar, use <code>searchUsers()</code> instead.
     * Presenting a drop-down list of 10m users simply won't work.
     *
     * @return A {@link java.util.List} of {@link String}s that list all the users visible to this application.
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                         The application (not the user) was not authenticated correctly.
     * @throws InvalidAuthenticationException if the application name/password combination is invalid
     * @deprecated Since 1.4.
     */
    List/*<String>*/ getAllUserNames()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;
}
