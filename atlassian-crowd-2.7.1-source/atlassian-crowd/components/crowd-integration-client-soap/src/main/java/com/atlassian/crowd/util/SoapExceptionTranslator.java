package com.atlassian.crowd.util;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.api.UserComparator;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupComparator;
import com.atlassian.crowd.model.group.GroupType;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Translates <tt>com.atlassian.crowd.exceptions</tt> Exception classes to and from <tt>com.atlassian.crowd.integration.exceptions</tt>
 * Exception classes to maintain backwards compatibility with Crowd 2.0.x SOAP service.
 */
public class SoapExceptionTranslator
{
    /////////////////////// Throw Checked SOAP Exception ////////////////////////////
    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.ApplicationPermissionException e)
            throws com.atlassian.crowd.integration.exception.ApplicationPermissionException
    {
        throw new com.atlassian.crowd.integration.exception.ApplicationPermissionException(e.getMessage(), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.ApplicationAccessDeniedException e)
            throws com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException
    {
        throw new com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException(e.getMessage(), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.BulkAddFailedException e)
            throws com.atlassian.crowd.integration.exception.BulkAddFailedException
    {
        throw new com.atlassian.crowd.integration.exception.BulkAddFailedException(e.getMessage(), e.getFailedUsers(), e.getExistingUsers(), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
            throws com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        throw new com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException(e.getMessage(), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.InvalidEmailAddressException e)
        throws com.atlassian.crowd.integration.exception.InvalidEmailAddressException
    {
        throw new com.atlassian.crowd.integration.exception.InvalidEmailAddressException(e.getMessage(), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.InvalidTokenException e)
        throws com.atlassian.crowd.integration.exception.InvalidTokenException
    {
        throw new com.atlassian.crowd.integration.exception.InvalidTokenException(e.getMessage(), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.ExpiredCredentialException e)
        throws com.atlassian.crowd.integration.exception.ExpiredCredentialException
    {
        throw new com.atlassian.crowd.integration.exception.ExpiredCredentialException(e.getMessage(), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.InactiveAccountException e)
        throws com.atlassian.crowd.integration.exception.InactiveAccountException
    {
        com.atlassian.crowd.integration.model.user.User user = toSoapIntegrationUser(e.getName());
        throw new com.atlassian.crowd.integration.exception.InactiveAccountException(user, e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.InvalidAuthenticationException e)
        throws com.atlassian.crowd.integration.exception.InvalidAuthenticationException
    {
        throw new com.atlassian.crowd.integration.exception.InvalidAuthenticationException(e.getMessage(), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.InvalidCredentialException e)
        throws com.atlassian.crowd.integration.exception.InvalidCredentialException
    {
        throw new com.atlassian.crowd.integration.exception.InvalidCredentialException(e.getMessage(), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.InvalidGroupException e)
        throws com.atlassian.crowd.integration.exception.InvalidGroupException
    {
        throw new com.atlassian.crowd.integration.exception.InvalidGroupException(toDirectoryEntity(e.getGroup()), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.InvalidRoleException e)
        throws com.atlassian.crowd.integration.exception.InvalidRoleException
    {
        throw new com.atlassian.crowd.integration.exception.InvalidRoleException(toDirectoryEntity(e.getGroup()), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.InvalidUserException e)
        throws com.atlassian.crowd.integration.exception.InvalidUserException
    {
        throw new com.atlassian.crowd.integration.exception.InvalidUserException(toDirectoryEntity(e.getUser()), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.ObjectNotFoundException e)
        throws com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        throw new com.atlassian.crowd.integration.exception.ObjectNotFoundException(e.getMessage(), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.GroupNotFoundException e)
        throws com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        throw new com.atlassian.crowd.integration.exception.ObjectNotFoundException(GroupNotFoundException.class.getName() + " " + e.getMessage(), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.UserNotFoundException e)
        throws com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        throw new com.atlassian.crowd.integration.exception.ObjectNotFoundException(UserNotFoundException.class.getName() + " " + e.getMessage(), e.getCause());
    }

    public static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.exception.MembershipNotFoundException e)
        throws com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        throw new com.atlassian.crowd.integration.exception.ObjectNotFoundException(MembershipNotFoundException.class.getName() + " " + e.getMessage(), e.getCause());
    }


    
    ///////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////// SOAP TO "NORMAL" //////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////



    /////////////////////// Throw Checked Exception ////////////////////////////
    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.ApplicationPermissionException e)
            throws com.atlassian.crowd.exception.ApplicationPermissionException
    {
        throw new com.atlassian.crowd.exception.ApplicationPermissionException(e.getMessage(), e.getCause());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException e)
            throws com.atlassian.crowd.exception.ApplicationAccessDeniedException
    {
        throw new com.atlassian.crowd.exception.ApplicationAccessDeniedException(e.getMessage(), e.getCause());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.BulkAddFailedException e)
            throws com.atlassian.crowd.exception.BulkAddFailedException
    {
        throw new com.atlassian.crowd.exception.BulkAddFailedException(e.getMessage(), e.getFailedUsers(), e.getExistingUsers(), e.getCause());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException e)
            throws com.atlassian.crowd.exception.InvalidAuthorizationTokenException
    {
        throw new com.atlassian.crowd.exception.InvalidAuthorizationTokenException(e.getMessage(), e.getCause());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.InvalidEmailAddressException e)
        throws com.atlassian.crowd.exception.InvalidEmailAddressException
    {
        throw new com.atlassian.crowd.exception.InvalidEmailAddressException(e.getMessage(), e.getCause());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.InvalidTokenException e)
        throws com.atlassian.crowd.exception.InvalidTokenException
    {
        throw new com.atlassian.crowd.exception.InvalidTokenException(e.getMessage(), e.getCause());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.ExpiredCredentialException e)
        throws com.atlassian.crowd.exception.ExpiredCredentialException
    {
        throw new com.atlassian.crowd.exception.ExpiredCredentialException(e.getMessage(), e.getCause());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.InactiveAccountException e)
        throws com.atlassian.crowd.exception.InactiveAccountException
    {
        throw new com.atlassian.crowd.exception.InactiveAccountException(e.getUser().getName(), e.getCause());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.InvalidAuthenticationException e)
        throws com.atlassian.crowd.exception.InvalidAuthenticationException
    {
        String userName = extractEntityName(e.getMessage());
        throw new com.atlassian.crowd.exception.InvalidAuthenticationException(userName, e.getCause());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.InvalidCredentialException e)
        throws com.atlassian.crowd.exception.InvalidCredentialException
    {
        throw new com.atlassian.crowd.exception.InvalidCredentialException(e.getMessage(), e.getCause());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.InvalidGroupException e)
        throws com.atlassian.crowd.exception.InvalidGroupException
    {
        throw new com.atlassian.crowd.exception.InvalidGroupException(toModelGroup(e.getEntity(), GroupType.GROUP), e.getCause());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.InvalidRoleException e)
        throws com.atlassian.crowd.exception.InvalidRoleException
    {
        throw new com.atlassian.crowd.exception.InvalidRoleException(toModelGroup(e.getEntity(), GroupType.LEGACY_ROLE), e.getMessage());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.InvalidUserException e)
        throws com.atlassian.crowd.exception.InvalidUserException
    {
        throw new com.atlassian.crowd.exception.InvalidUserException(toModelUser(e.getEntity()), e.getCause());
    }

    public static void throwEquivalentCheckedException(final com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        throws com.atlassian.crowd.exception.ObjectNotFoundException
    {
        String msg = e.getMessage();
        if (msg.startsWith(UserNotFoundException.class.getName()))
        {
            throw new com.atlassian.crowd.exception.UserNotFoundException(extractEntityName(msg), e.getCause());
        }
        else if (msg.startsWith(GroupNotFoundException.class.getName()))
        {
            throw new com.atlassian.crowd.exception.GroupNotFoundException(extractEntityName(msg), e.getCause());
        }
        else if (msg.startsWith(MembershipNotFoundException.class.getName()))
        {
            List<String> entityNames = extractEntityNames(msg);
            if (entityNames.size() >= 2)
            {
                String childName = entityNames.get(0);
                String parentName = entityNames.get(1);
                throw new com.atlassian.crowd.exception.MembershipNotFoundException(childName, parentName, e.getCause());
            }
            else
            {
                throw new com.atlassian.crowd.exception.MembershipNotFoundException("", "", e.getCause());
            }
        }
        else
        {
            throw new com.atlassian.crowd.exception.ObjectNotFoundException(e.getMessage(), e.getCause());
        }
    }

    /**
     * Throws the equivalent UserNotFoundException from  {@link com.atlassian.crowd.integration.exception.ObjectNotFoundException}.
     * 
     * @param e must be able to translate to a UserNotFoundException
     * @throws com.atlassian.crowd.exception.UserNotFoundException the equivalent UserNotFoundException
     * @throws AssertionError if the exception cannot be translated to a UserNotFoundException.
     */
    public static void throwEquivalentUserNotFoundException(final com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        throws com.atlassian.crowd.exception.UserNotFoundException
    {
        try
        {
            throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.UserNotFoundException ex)
        {
            throw ex;
        }
        catch (ObjectNotFoundException ex)
        {
            throw new AssertionError("Should not throw " + ex.getClass().getCanonicalName());
        }
    }
    
    /**
     * Throws the equivalent GroupNotFoundException from  {@link com.atlassian.crowd.integration.exception.ObjectNotFoundException}.
     * 
     * @param e must be able to translate to a GroupNotFoundException
     * @throws com.atlassian.crowd.exception.GroupNotFoundException the equivalent GroupNotFoundException
     * @throws AssertionError if the exception cannot be translated to a GroupNotFoundException.
     */
    public static void throwEquivalentGroupNotFoundException(final com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        throws com.atlassian.crowd.exception.GroupNotFoundException
    {
        try
        {
            throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException ex)
        {
            throw ex;
        }
        catch (ObjectNotFoundException ex)
        {
            throw new AssertionError("Should not throw " + ex.getClass().getCanonicalName());
        }
    }
    
    /**
     * Throws the equivalent MembershipNotFoundException from  {@link com.atlassian.crowd.integration.exception.ObjectNotFoundException}.
     * 
     * @param e must be able to translate to a MembershipNotFoundException
     * @throws com.atlassian.crowd.exception.MembershipNotFoundException the equivalent MembershipNotFoundException
     * @throws AssertionError if the exception cannot be translated to a MembershipNotFoundException.
     */
    public static void throwEquivalentMembershipNotFoundException(final com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        throws com.atlassian.crowd.exception.MembershipNotFoundException
    {
        try
        {
            throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.MembershipNotFoundException ex)
        {
            throw ex;
        }
        catch (ObjectNotFoundException ex)
        {
            throw new AssertionError("Should not throw " + ex.getClass().getCanonicalName());
        }
    }
    
    /**
     * Throws the equivalent {@link com.atlassian.crowd.exception.ObjectNotFoundException} from  {@link com.atlassian.crowd.integration.exception.ObjectNotFoundException}.
     * 
     * @param e must be able to translate to a {@link com.atlassian.crowd.exception.ObjectNotFoundException}
     * @throws com.atlassian.crowd.exception.ObjectNotFoundException the equivalent ObjectNotFoundException
     * @throws AssertionError if the exception cannot be translated to a {@link com.atlassian.crowd.exception.ObjectNotFoundException}.
     */
    public static void throwEquivalentObjectNotFoundException(final com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        throws com.atlassian.crowd.exception.ObjectNotFoundException
    {
        try
        {
            throwEquivalentCheckedException(e);
        }
        catch (UserNotFoundException e1)
        {
            throw new AssertionError("Should not throw " + e1.getClass().getCanonicalName());
        }
        catch (GroupNotFoundException e1)
        {
            throw new AssertionError("Should not throw " + e1.getClass().getCanonicalName());
        }
        catch (MembershipNotFoundException e1)
        {
            throw new AssertionError("Should not throw " + e1.getClass().getCanonicalName());
        }
    }

    /**
     * Throws the equivalent UserNotFoundException or GroupNotFoundException from {@link com.atlassian.crowd.integration.exception.ObjectNotFoundException}.
     *
     * @param e must be able to translate to a UserNotFoundException or GroupNotFoundException
     * @throws com.atlassian.crowd.exception.UserNotFoundException the equivalent UserNotFoundException
     * @throws com.atlassian.crowd.exception.GroupNotFoundException the equivalent GroupNotFoundException
     * @throws AssertionError if the exception cannot be translated to a UserNotFoundException or GroupNotFoundException.
     */
    public static void throwEquivalentUserOrGroupNotFoundException(final com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        throws com.atlassian.crowd.exception.UserNotFoundException, com.atlassian.crowd.exception.GroupNotFoundException
    {
        try
        {
            throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.UserNotFoundException ex)
        {
            throw ex;
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException ex)
        {
            throw ex;
        }
        catch (ObjectNotFoundException ex)
        {
            throw new AssertionError("Should not throw " + ex.getClass().getCanonicalName());
        }
    }

    /**
     * Throws the equivalent UserNotFoundException, GroupNotFoundException, MembershipNotFoundException from {@link com.atlassian.crowd.integration.exception.ObjectNotFoundException}.
     *
     * @param e must be able to translate to a UserNotFoundException, GroupNotFoundException or MembershipNotFoundException
     * @throws com.atlassian.crowd.exception.UserNotFoundException the equivalent UserNotFoundException
     * @throws com.atlassian.crowd.exception.GroupNotFoundException the equivalent GroupNotFoundException
     * @throws com.atlassian.crowd.exception.MembershipNotFoundException the equivalent MembershipNotFoundException
     * @throws AssertionError if the exception cannot be translated to a UserNotFoundException, GroupNotFoundException or MembershipNotFoundException.
     */
    public static void throwEquivalentUserOrGroupOrMembershipNotFoundException(final com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        throws com.atlassian.crowd.exception.UserNotFoundException, com.atlassian.crowd.exception.GroupNotFoundException, com.atlassian.crowd.exception.MembershipNotFoundException
    {
        try
        {
            throwEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.UserNotFoundException ex)
        {
            throw ex;
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException ex)
        {
            throw ex;
        }
        catch (com.atlassian.crowd.exception.MembershipNotFoundException ex)
        {
            throw ex;
        }
        catch (ObjectNotFoundException ex)
        {
            throw new AssertionError("Should not throw " + ex.getClass().getCanonicalName());
        }
    }

    /**
     * Extracts an entity name.  Strings should be of the expected format: "blah blah <entity_name> blah blah".
     * @param line line
     * @return entity name, otherwise null.
     */
    private static String extractEntityName(final String line)
    {
        List<String> entityNames = extractEntityNames(line);
        if (entityNames.isEmpty())
        {
            return "";
        }
        else
        {
            return entityNames.get(0);
        }
    }

    /**
     * Extracts entity names.  Strings should be of the expected format: "<entity_name1> <entity_name2>".
     * @param line line
     * @return entity names.
     */
    private static List<String> extractEntityNames(final String line)
    {
        List<String> entityNames = new ArrayList<String>();
        Pattern p = Pattern.compile("<([^\"]*?)>");
        Matcher m = p.matcher(line);
        while (m.find())
        {
            entityNames.add(m.group(1));
        }

        return entityNames;
    }

    private static com.atlassian.crowd.integration.model.DirectoryEntity toDirectoryEntity(final User user)
    {
        return new com.atlassian.crowd.integration.model.DirectoryEntity()
        {
            public Long getDirectoryId()
            {
                return user.getDirectoryId();
            }

            public String getName()
            {
                return user.getName();
            }
        };
    }

    private static com.atlassian.crowd.integration.model.DirectoryEntity toDirectoryEntity(final Group group)
    {
        return new com.atlassian.crowd.integration.model.DirectoryEntity()
        {
            public Long getDirectoryId()
            {
                return group.getDirectoryId();
            }

            public String getName()
            {
                return group.getName();
            }
        };
    }

    private static com.atlassian.crowd.model.user.User toModelUser(final com.atlassian.crowd.integration.model.DirectoryEntity entity)
    {
        return new com.atlassian.crowd.model.user.User()
        {
            public String getFirstName()
            {
                return null;
            }

            public String getLastName()
            {
                return null;
            }

            @Override
            public String getExternalId()
            {
                return null;
            }

            public long getDirectoryId()
            {
                return entity.getDirectoryId();
            }

            public boolean isActive()
            {
                return false;
            }

            public String getEmailAddress()
            {
                return null;
            }

            public String getDisplayName()
            {
                return entity.getName();
            }

            public int compareTo(final com.atlassian.crowd.embedded.api.User user)
            {
                return UserComparator.compareTo(this, user);
            }

            public String getName()
            {
                return entity.getName();
            }
        };
    }

    private static com.atlassian.crowd.integration.model.user.User toSoapIntegrationUser(final String username)
    {
        return new com.atlassian.crowd.integration.model.user.User()
        {
            public String getFirstName()
            {
                return null;
            }

            public String getLastName()
            {
                return null;
            }

            public Long getDirectoryId()
            {
                return -1L;
            }

            public boolean isActive()
            {
                return false;
            }

            public String getEmailAddress()
            {
                return null;
            }

            public String getDisplayName()
            {
                return username;
            }

            public String getName()
            {
                return username;
            }

            public String getIconLocation()
            {
                return "";
            }
        };
    }

    private static Group toModelGroup(final com.atlassian.crowd.integration.model.DirectoryEntity entity, final GroupType type)
    {
        return new Group()
        {
            public GroupType getType()
            {
                return type;
            }

            public boolean isActive()
            {
                return false;
            }

            public String getDescription()
            {
                return null;
            }

            public int compareTo(final Group o)
            {
                return GroupComparator.compareTo(this, o);
            }

            public long getDirectoryId()
            {
                return entity.getDirectoryId();
            }

            public String getName()
            {
                return entity.getName();
            }
        };
    }
}
