package com.atlassian.crowd.integration.authentication;

/**
 * The <code>ApplicationAuthenticationContext</code> is used to authenticate Applications.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility.
 */
public class ApplicationAuthenticationContext extends AuthenticationContext
{
    public ApplicationAuthenticationContext()
    {
    }

    public ApplicationAuthenticationContext(String name, PasswordCredential credential, ValidationFactor[] validationFactors)
    {
        super(name, credential, validationFactors);
    }
}
