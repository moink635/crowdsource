package com.atlassian.crowd.service.soap.client;

import java.util.Properties;

import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.ResourceLocator;

import org.apache.commons.lang3.StringUtils;

/**
 * This bean is a container for the application's crowd.properties.
 */
public class SoapClientPropertiesImpl extends ClientPropertiesImpl implements SoapClientProperties
{
    /**
     * Configuration file property value for the Crowd server web services URL.
     */
    public static final String PROPERTIES_FILE_SECURITY_SERVER_URL = "crowd.server.url";

    // members
    private String securityServerURL = null;

    private SoapClientPropertiesImpl()
    {
    }

    public void updateProperties(Properties properties)
    {
        super.updateProperties(properties);

        setSecurityServerURL(loadAndLogPropertyString(properties, Constants.PROPERTIES_FILE_SECURITY_SERVER_URL));
    }

    public String getSecurityServerURL()
    {
        return securityServerURL;
    }

    private void setSecurityServerURL(String url)
    {
        url = StringUtils.removeEnd(url, "/");

        if (url != null)
        {
            if (url.endsWith(Constants.SECURITY_SERVER_NAME))
            {
                securityServerURL = url;
            }
            else if (url.endsWith(Constants.CROWD_SERVICE_LOCATION))
            {
                securityServerURL = new StringBuilder(url).append("/").append(Constants.SECURITY_SERVER_NAME).toString();
            }
            else
            {
                securityServerURL = new StringBuilder(url).append("/").append(Constants.CROWD_SERVICE_LOCATION).
                        append("/").append(Constants.SECURITY_SERVER_NAME).toString();
            }
        }
        else
        {
            securityServerURL = url;
        }
    }

    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();
        sb.append("ClientPropertiesImpl");
        sb.append("{applicationName='").append(getApplicationName()).append('\'');
        sb.append(", applicationPassword='").append(getApplicationPassword()).append('\'');
        sb.append(", applicationAuthenticationURL='").append(getApplicationAuthenticationURL()).append('\'');
        sb.append(", securityServerURL='").append(securityServerURL).append('\'');
        sb.append(", cookieTokenKey='").append(getCookieTokenKey()).append('\'');
        sb.append(", sessionTokenKey='").append(getSessionTokenKey()).append('\'');
        sb.append(", sessionLastValidation='").append(getSessionLastValidation()).append('\'');
        sb.append(", sessionValidationInterval=").append(getSessionValidationInterval());
        sb.append(", baseURL='").append(getBaseURL()).append('\'');
        sb.append(", httpProxyPort='").append(getHttpProxyPort()).append('\'');
        sb.append(", httpProxyHost='").append(getHttpProxyHost()).append('\'');
        sb.append(", httpProxyUsername='").append(getHttpProxyUsername()).append('\'');
        sb.append(", httpProxyPassword='").append(getHttpProxyPassword()).append('\'');
        sb.append(", httpMaxConnections='").append(getHttpMaxConnections()).append('\'');
        sb.append(", httpTimeout='").append(getHttpTimeout()).append('\'');
        sb.append(", applicationAuthenticationContext=").append(getApplicationAuthenticationContext());
        sb.append('}');
        return sb.toString();
    }

    public static SoapClientPropertiesImpl newInstanceFromResourceLocator(final ResourceLocator resourceLocator)
    {
        Properties properties = resourceLocator.getProperties();
        return newInstanceFromProperties(properties);
    }

    public static SoapClientPropertiesImpl newInstanceFromProperties(final Properties properties)
    {
        SoapClientPropertiesImpl clientProperties = new SoapClientPropertiesImpl();
        clientProperties.updateProperties(properties);
        return clientProperties;
    }

    public static SoapClientPropertiesImpl newInstanceFromPropertiesWithoutOverrides(Properties properties)
    {
        SoapClientPropertiesImpl clientProperties = new SoapClientPropertiesImpl() {
            @Override
            public String loadPropertyString(Properties properties, String propertyName)
            {
                return StringUtils.trimToNull(properties.getProperty(propertyName));
            }
        };
        clientProperties.updateProperties(properties);
        return clientProperties;
    }
}
