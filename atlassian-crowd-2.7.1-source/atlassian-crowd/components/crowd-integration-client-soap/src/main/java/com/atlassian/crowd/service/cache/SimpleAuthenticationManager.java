package com.atlassian.crowd.service.cache;

import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.AuthenticationManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.util.Assert;

import java.rmi.RemoteException;

/**
 * This class provides a version of the AuthenticationManager interface that, at least initially, will not perform
 * caching. The abstraction is in place so we can decouple client-side code from the
 * {@link com.atlassian.crowd.service.soap.client.SecurityServerClient}.
 *
 * It also serves to logically break out the API.
 */
public class SimpleAuthenticationManager implements AuthenticationManager
{
    private final SecurityServerClient securityServerClient;

    public SimpleAuthenticationManager(SecurityServerClient securityServerClient)
    {
        this.securityServerClient = securityServerClient;
    }

    public String authenticate(UserAuthenticationContext authenticationContext) throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {
        Assert.notNull(authenticationContext);

        // So, if we're in (say) JIRA, we check that the user can auth against JIRA.
        if (authenticationContext.getApplication() == null)
        {
            authenticationContext.setApplication(getSecurityServerClient().getSoapClientProperties().getApplicationName());
        }

        return getSecurityServerClient().authenticatePrincipal(authenticationContext);
    }

    public String authenticateWithoutValidatingPassword(UserAuthenticationContext authenticationContext) throws ApplicationAccessDeniedException, InvalidAuthenticationException, InvalidAuthorizationTokenException, InactiveAccountException, RemoteException
    {
        Assert.notNull(authenticationContext);

        return getSecurityServerClient().createPrincipalToken(authenticationContext.getName(), authenticationContext.getValidationFactors());
    }

    public String authenticate(String username, String password) throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {
        Assert.notNull(username);
        Assert.notNull(password);

        return getSecurityServerClient().authenticatePrincipalSimple(username, password);
    }

    public boolean isAuthenticated(String token, ValidationFactor[] validationFactors)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException
    {
        Assert.notNull(token);

        // we allow null ValidationFactor[] - wise?

        return getSecurityServerClient().isValidToken(token, validationFactors);
    }

    public void invalidate(String token)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        Assert.notNull(token);

        getSecurityServerClient().invalidateToken(token);
    }

    public SecurityServerClient getSecurityServerClient()
    {
        return securityServerClient;
    }
}
