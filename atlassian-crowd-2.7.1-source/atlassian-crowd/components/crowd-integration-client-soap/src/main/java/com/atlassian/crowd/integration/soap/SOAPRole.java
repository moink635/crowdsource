/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.integration.soap;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * @deprecated
 */
public class SOAPRole extends SOAPEntity implements Serializable
{
    private String[] members;

    public SOAPRole()
    {
    }

    public SOAPRole(String name, String[] members)
    {
        this.name = name;
        this.members = members;
    }

    public String[] getMembers()
    {
        return members;
    }

    public void setMembers(String[] members)
    {
        this.members = members;
    }

    public String toString()
    {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("members", members).toString();
    }
}