package com.atlassian.crowd.integration.exception;

/**
 * Base class of all checked SOAP exceptions.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public abstract class CheckedSoapException extends Exception
{
    public CheckedSoapException()
    {
        super();
    }

    public CheckedSoapException(String s)
    {
        super(s);
    }

    public CheckedSoapException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    public CheckedSoapException(Throwable throwable)
    {
        super(throwable);
    }
}
