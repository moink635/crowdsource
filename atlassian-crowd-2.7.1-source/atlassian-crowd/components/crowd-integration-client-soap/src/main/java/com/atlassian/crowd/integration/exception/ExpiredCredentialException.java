package com.atlassian.crowd.integration.exception;

/**
 * Thrown when the user's credentials have expired.
 * Note: This exception should only be thrown if the user has actually successfully authenticated with their old password
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class ExpiredCredentialException extends CheckedSoapException
{
    public ExpiredCredentialException()
    {
    }

    public ExpiredCredentialException(Throwable cause)
    {
        super(cause);
    }

    public ExpiredCredentialException(String message)
    {
        super(message);
    }

    public ExpiredCredentialException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
