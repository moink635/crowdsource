/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.integration.soap;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class SOAPAttribute implements Serializable
{
    String name;
    String[] values;

    public SOAPAttribute()
    {
    }

    public SOAPAttribute(String name, String value)
    {
        this.name = name;
        this.values = new String[]{value};
    }

    public SOAPAttribute(String name, String[] values)
    {
        this.name = name;
        this.values = values;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String[] getValues()
    {
        return values;
    }

    public void setValues(String[] values)
    {
        this.values = values;
    }

    public String toString()
    {
        return new ToStringBuilder(this).
                append("name", name).
                append("values", values).toString();
    }
}