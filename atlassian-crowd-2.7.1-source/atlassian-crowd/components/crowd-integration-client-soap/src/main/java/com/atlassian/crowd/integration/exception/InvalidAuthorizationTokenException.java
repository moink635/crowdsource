package com.atlassian.crowd.integration.exception;

/**
 * Thrown when the authenticated token is invalid.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class InvalidAuthorizationTokenException extends CheckedSoapException
{
    /**
     * Default constructor.
     */
    public InvalidAuthorizationTokenException()
    {
    }

    /**
     * Default constructor.
     *
     * @param s the message.
     */
    public InvalidAuthorizationTokenException(String s)
    {
        super(s);
    }

    /**
     * Default constructor.
     *
     * @param s         the message.
     * @param throwable the {@link Exception Exception}.
     */
    public InvalidAuthorizationTokenException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    /**
     * Default constructor.
     *
     * @param throwable the {@link Exception Exception}.
     */
    public InvalidAuthorizationTokenException(Throwable throwable)
    {
        super(throwable);
    }
}
