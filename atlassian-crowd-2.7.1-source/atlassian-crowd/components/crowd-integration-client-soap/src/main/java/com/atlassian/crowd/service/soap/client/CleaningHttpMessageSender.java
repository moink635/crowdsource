package com.atlassian.crowd.service.soap.client;

import org.apache.commons.httpclient.HttpClient;
import org.codehaus.xfire.MessageContext;
import org.codehaus.xfire.XFireException;
import org.codehaus.xfire.exchange.OutMessage;
import org.codehaus.xfire.transport.http.CommonsHttpMessageSender;

/**
 * There's a known issue with HttpClient under heavy load, where it can leak connections into CLOSE_WAIT status.
 *
 * XFire doesn't handle creation and disposal of HttpClient and MultithreadedHttpConnectionManager objects smartly
 * enough to prevent this connection leak. So, we subclass the sending component and clean up properly.
 *
 * Discussion that lead us to a solution:
 * http://www.nabble.com/tcp-connections-left-with-CLOSE_WAIT-td13757202.html
 */
public class CleaningHttpMessageSender extends CommonsHttpMessageSender
{
    public CleaningHttpMessageSender(OutMessage message, MessageContext context)
    {
        super(message, context);
    }

    /**
     * Overridden to make sure HttpClient cleans up after itself.
     * @throws XFireException
     */
    @Override
    public void close() throws XFireException
    {
        super.close();
        HttpClient localClient = getClient();
        if (localClient != null)
        {
            localClient.getHttpConnectionManager().closeIdleConnections(0);
        }
    }
}
