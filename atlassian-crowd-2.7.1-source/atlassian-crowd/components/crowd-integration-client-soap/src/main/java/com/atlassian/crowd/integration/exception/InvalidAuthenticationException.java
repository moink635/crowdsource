package com.atlassian.crowd.integration.exception;

/**
 * Thrown when the attempted authentication is not valid.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class InvalidAuthenticationException extends CheckedSoapException
{
    public InvalidAuthenticationException(String msg)
    {
        super(msg);
    }

    public InvalidAuthenticationException(String msg, Throwable e)
    {
        super(msg, e);
    }
}
