package com.atlassian.crowd.service.cache;

import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.AuthenticationManager;
import com.atlassian.crowd.service.UserManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.util.Assert;

import java.rmi.RemoteException;

/**
 * This class provides a version of the AuthenticationManager interface that, will ensure that if the authentication
 * is successful, the user will exist in the cache.
 *
 * The abstraction is in place so we can decouple client-side code from the
 * {@link com.atlassian.crowd.service.soap.client.SecurityServerClient}.
 * <p/>
 * It also serves to logically break out the API.
 */
public class CacheAwareAuthenticationManager implements AuthenticationManager
{
    private final SecurityServerClient securityServerClient;
    private final UserManager crowdUserManager;

    public CacheAwareAuthenticationManager(SecurityServerClient securityServerClient, UserManager crowdUserManager)
    {
        this.securityServerClient = securityServerClient;
        this.crowdUserManager = crowdUserManager;
    }

    public String authenticate(UserAuthenticationContext authenticationContext) throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {
        Assert.notNull(authenticationContext);

        // So, if we're in (say) JIRA, we check that the user can authN against JIRA.
        if (authenticationContext.getApplication() == null)
        {
            authenticationContext.setApplication(getSecurityServerClient().getSoapClientProperties().getApplicationName());
        }

        String token = getSecurityServerClient().authenticatePrincipal(authenticationContext);

        ensureUserExistsInCache(authenticationContext.getName());
        // TODO: update user's group details in the cache here.

        return token;
    }

    public String authenticateWithoutValidatingPassword(UserAuthenticationContext authenticationContext) throws ApplicationAccessDeniedException, InvalidAuthenticationException, InvalidAuthorizationTokenException, InactiveAccountException, RemoteException
    {
        Assert.notNull(authenticationContext);

        String token = getSecurityServerClient().createPrincipalToken(authenticationContext.getName(), authenticationContext.getValidationFactors());

        ensureUserExistsInCache(authenticationContext.getName());
        // TODO: update user's group details in the cache here.

        return token;
    }

    public String authenticate(String username, String password) throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {
        Assert.notNull(username);
        Assert.notNull(password);

        String token = getSecurityServerClient().authenticatePrincipalSimple(username, password);

        ensureUserExistsInCache(username);
        // TODO: update user's group details in the cache here.

        return token;
    }

    public boolean isAuthenticated(String token, ValidationFactor[] validationFactors)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException
    {
        Assert.notNull(token);

        // we allow null ValidationFactor[] - wise?

        return getSecurityServerClient().isValidToken(token, validationFactors);
    }

    public void invalidate(String token)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        Assert.notNull(token);

        getSecurityServerClient().invalidateToken(token);
    }

    public SecurityServerClient getSecurityServerClient()
    {
        return securityServerClient;
    }

    /**
     * Fetch the user from the UserManager to ensure that the user will exist in the cache.
     * This is needed so JIRA knows about the user when using delegated authentication - see: CWD-1972
     *
     * @param name username of the user
     * @throws RemoteException                A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException
     *                                        The application (not the user) was not authenticated correctly.
     * @throws InvalidAuthenticationException the user could not be found (original exception was ONFE)
     */
    private void ensureUserExistsInCache(String name) throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException
    {
        try
        {
            crowdUserManager.getUser(name);
        }
        catch (UserNotFoundException e)
        {
            throw InvalidAuthenticationException.newInstanceWithName(name);
        }
    }
}
