/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.integration.soap;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class SearchRestriction implements Serializable
{
    private String name;
    private String value;

    public SearchRestriction()
    {
    }

    public SearchRestriction(String name, String value)
    {
        this.name = name;
        this.value = value;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public String toString()
    {
        return new ToStringBuilder(this).
                append("name", name).
                append("value", value).toString();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof SearchRestriction))
        {
            return false;
        }

        SearchRestriction that = (SearchRestriction) o;

        if (name != null ? !name.equals(that.name) : that.name != null)
        {
            return false;
        }
        if (value != null ? !value.equals(that.value) : that.value != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}