package com.atlassian.crowd.service;

import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;

import java.rmi.RemoteException;

/**
 * Used by applications that only need user authentication and validation services.
 */
public interface AuthenticationManager
{
    /**
     * Authenticate a user. The <code>PrincipalAuthenticationContext</code> contains the details of who they are,
     * the credentials they're presenting, and where they're coming from.
     *
     * If the authenticationContext does not contain application details, they will be added using details provided
     * by the SecurityServerClient.
     * @param authenticationContext The details of the user that is to be authenticated.
     * @return Returns an authorization token if successful.
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException The application (not the user) was not authenticated correctly.
     * @throws InvalidAuthenticationException The user was not successfully authenticated.
     * @throws InactiveAccountException The user's account is inactive and they are not be allowed to authenticate.
     * @throws ExpiredCredentialException The user's credentials have expired.  The user must change their credentials in order to successfully authenticate.
     * @throws ApplicationAccessDeniedException user does not have authorisation to access application.
     */
    String authenticate(UserAuthenticationContext authenticationContext)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException,
            InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException;

    /**
     * Authenticate a user without validating their password. The <code>PrincipalAuthenticationContext</code> contains the details of who they are
     * and where they're coming from but does not need to contain any credentials.
     *
     * @param authenticationContext The details of the user that is to be authenticated. Username and validation factors are required.
     * @return Returns an authorization token if successful.
     *
     * @throws ApplicationAccessDeniedException user does not have authorisation to access application.
     * @throws InactiveAccountException The user's account is inactive and they are not be allowed to authenticate.
     * @throws InvalidAuthenticationException The user was not successfully authenticated.
     * @throws InvalidAuthorizationTokenException The application (not the user) was not authenticated correctly.
     * @throws java.rmi.RemoteException A communication error occurred - the Crowd server may not be available.
     */
    String authenticateWithoutValidatingPassword(UserAuthenticationContext authenticationContext) throws ApplicationAccessDeniedException, InvalidAuthenticationException, InvalidAuthorizationTokenException, InactiveAccountException, RemoteException;


    /**
     * Authenticates a user, using just a username and password.
     * @param username username of user.
     * @param password credentials of user.
     * @return Returns an authorization token if successful.
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException The application (not the user) was not authenticated correctly.
     * @throws InvalidAuthenticationException The user was not successfully authenticated.
     * @throws InactiveAccountException The user's account is inactive and they are not be allowed to authenticate.
     * @throws ExpiredCredentialException The user's credentials have expired.
     * @throws ApplicationAccessDeniedException user does not have authorisation to access application.
     */
    String authenticate(String username, String password)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException,
            InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException;

    /**
     * Checks that the token (as returned from {@see authenticate()} is still valid, given the validation factors.
     * @param token The token presented by the user as evidence of their authenticity
     * @param validationFactors Details of where the user's come from. If presented, must match those presented during
     * authentication.
     * @return true if the user is still authenticated, false if not.
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException The application (not the user) was not authenticated correctly.
     * @throws ApplicationAccessDeniedException user does not have authorisation to access application.
     * @throws InvalidAuthenticationException The user was not successfully authenticated.
     */
    boolean isAuthenticated(String token, ValidationFactor[] validationFactors)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationAccessDeniedException, InvalidAuthenticationException;

    /**
     * Marks the presented <code>token</code> as invalid, meaning that the principal it represents is no longer
     * authenticated. Usually used to make the user logged-off.
     * @param token The token presented by the user, as returned from {@see authenticate()}
     * @throws RemoteException A communication error occurred - the Crowd server may not be available.
     * @throws InvalidAuthorizationTokenException The application (not the user) was not authenticated correctly.
     * @throws InvalidAuthenticationException The user was not successfully authenticated.
     */
    void invalidate(String token)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException;

    /**
     * Returns an instance of the <code>SecurityServerClient</code>, for when you need more API access than the
     * authentication manager provides.
     * @return underlying SecurityServerClient.
     */
    SecurityServerClient getSecurityServerClient();
}
