package com.atlassian.crowd.integration.exception;

/**
 * An exception to denote an invalid group.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class InvalidGroupException extends InvalidDirectoryEntityException
{
    public InvalidGroupException()
    {
        // needed for xfire
        super();
    }

    public InvalidGroupException(com.atlassian.crowd.integration.model.DirectoryEntity group, Throwable cause)
    {
        super(group, cause);
    }

    public InvalidGroupException(com.atlassian.crowd.integration.model.DirectoryEntity group, String message)
    {
        super(group, message);
    }

    public InvalidGroupException(com.atlassian.crowd.integration.model.DirectoryEntity group, String message, Throwable cause)
    {
        super(group, message, cause);
    }
}
