package com.atlassian.crowd.service.cache;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SOAPNestableGroup;
import com.atlassian.crowd.integration.soap.SearchRestriction;
import com.atlassian.crowd.service.GroupManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.util.Assert;
import com.atlassian.crowd.util.NestingHelper;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.List;

/**
 * Handles group names & groups themselves.
 */
public class CachingGroupManager implements GroupManager
{
    private final Server server;
    private final BasicCache basicCache;

    private final Object basicCacheLock = new Object();

    public CachingGroupManager(SecurityServerClient securityServerClient, BasicCache basicCache)
    {
        this.server = new Server(securityServerClient);
        this.basicCache = basicCache;
    }

    public boolean isGroup(String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        Assert.notNull(groupName);

        // Easier to get all the groups at once (one large call), than many small calls
        List/*<String>*/ groupNames = getAllGroupNames();
        if (groupNames != null)
        {
            return groupNames.contains(groupName);  // fairly slow, but much quicker than a network call.
        }
        return false;
    }

    /**
     * See also {@link CachingGroupMembershipManager#getMembers(String)}} for similar logic.
     */
    public SOAPGroup getGroup(String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException, GroupNotFoundException
    {
        Assert.notNull(groupName);

        SOAPGroup group = basicCache.getGroup(groupName);
        if (group == null)
        {
            group = server.getGroup(groupName);

            basicCache.setMembers(groupName, group.getMembers());
            basicCache.cacheGroup(group);
        }

        // Remove all group members from the returned object, this is an API change to force users to use the GroupMembership calls
        group.setMembers(null);

        return group;
    }

    public SOAPGroup addGroup(SOAPGroup group)
            throws RemoteException, InvalidGroupException, InvalidAuthorizationTokenException, ApplicationPermissionException, InvalidAuthenticationException
    {
        Assert.notNull(group);
        Assert.notNull(group.getName());

        group = server.addGroup(group);     // throws if the add failed
        basicCache.cacheGroup(group);
        basicCache.addToAllGroupNamesCache(group.getName());

        return group;
    }

    public List/*<SOAPGroup>*/ searchGroups(SearchRestriction[] restrictions)
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        return server.searchGroups(restrictions);
    }

    public void updateGroup(SOAPGroup group)
            throws RemoteException, ApplicationPermissionException, InvalidAuthorizationTokenException, InvalidAuthenticationException, GroupNotFoundException
    {
        Assert.notNull(group);

        server.updateGroup(group);          // will throw if there's a problem
        basicCache.cacheGroup(group);
    }

    public void removeGroup(String groupName)
            throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, InvalidAuthenticationException, GroupNotFoundException
    {
        Assert.notNull(groupName);

        server.removeGroup(groupName);      // will throw if there's a problem
        basicCache.removeGroup(groupName);
        basicCache.removeCachedGroupMemberships(groupName); // remove the group from the user->groups & memberships cache.
        basicCache.removeFromAllGroupNamesCache(groupName);
    }

    public List/*<String>*/ getAllGroupNames()
            throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
    {
        List/*<String>*/ groupNames = basicCache.getAllGroupNames();

        // Common case should be fast
        if (groupNames != null)
        {
            return groupNames;
        }

        /*
         * Slow path - fetching group names from the server can take a long
         * time. Ensure that only one thread will perform it.
         */
        synchronized (basicCacheLock)
        {
            // Another thread might have fetched group names after the first check
            groupNames = basicCache.getAllGroupNames();
            if (groupNames == null)
            {
                SOAPNestableGroup[] allGroupRelationships = server.getAllGroupRelationships();
                if (allGroupRelationships != null)
                {
                    groupNames =  NestingHelper.cacheGroupRelationships(allGroupRelationships, basicCache);
                }
            }

            return groupNames;
        }
    }

    /**
     * Encapsulates communications with the Crowd server.
     */
    private class Server
    {
        private final SecurityServerClient ssc;

        public Server(SecurityServerClient securityServerClient)
        {
            this.ssc = securityServerClient;
        }

        public SOAPGroup getGroup(String groupName)
                throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException, GroupNotFoundException
        {
            return ssc.findGroupByName(groupName);
        }

        public List/*<SOAPGroup>*/ searchGroups(SearchRestriction[] restrictions)
                throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
        {
            SOAPGroup[] groups = ssc.searchGroups(restrictions);
            return Arrays.asList(groups);
        }

        public SOAPGroup addGroup(SOAPGroup group)
                throws RemoteException, ApplicationPermissionException, InvalidAuthorizationTokenException, InvalidGroupException, InvalidAuthenticationException
        {
            return ssc.addGroup(group);
        }

        public void updateGroup(SOAPGroup group)
                throws RemoteException, ApplicationPermissionException, InvalidAuthorizationTokenException, InvalidAuthenticationException, GroupNotFoundException
        {
            ssc.updateGroup(group.getName(), group.getDescription(), group.isActive());
        }

        public void removeGroup(String groupName)
                throws RemoteException, InvalidAuthorizationTokenException, ApplicationPermissionException, InvalidAuthenticationException, GroupNotFoundException
        {
            ssc.removeGroup(groupName);
        }

        public SOAPNestableGroup[] getAllGroupRelationships()
                throws RemoteException, InvalidAuthorizationTokenException, InvalidAuthenticationException
        {
            return ssc.findAllGroupRelationships();
        }
    }
}