package com.atlassian.crowd.service.soap.xfire;

import org.codehaus.xfire.MessageContext;
import org.codehaus.xfire.exchange.AbstractMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;

/**
 * Logs outgoing service request to Crowd from XFire.
 */
public class XFireOutLoggingMethodHandler extends XFireLoggingMethodHandler
{
    private static final Logger logger = LoggerFactory.getLogger(XFireOutLoggingMethodHandler.class);

    AbstractMessage outHandler;

    /**
     * @see XFireLoggingMethodHandler#invoke(MessageContext)
     */
    public void invoke(MessageContext messageContext) throws Exception
    {
        outHandler = messageContext.getExchange().getOutMessage();

        super.invoke(messageContext);
    }

    /**
     * @see XFireLoggingMethodHandler#getMessageBodyObjects()
     */
    public Object[] getMessageBodyObjects()
    {
        if (outHandler.getBody() instanceof Object[])
        {
            return (Object[]) outHandler.getBody();
        }
        else
        {
            return Collections.EMPTY_LIST.toArray();
        }
    }

    /**
     * @see XFireLoggingMethodHandler#getLogger()
     */
    protected Logger getLogger()
    {
        return logger;
    }
}