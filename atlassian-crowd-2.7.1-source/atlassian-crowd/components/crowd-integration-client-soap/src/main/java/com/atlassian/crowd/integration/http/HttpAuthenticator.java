package com.atlassian.crowd.integration.http;

import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.rmi.RemoteException;

/**
 * This interface is used to manage HTTP authentication.
 * <p>
 * It is the fundamental class for web/SSO authentication integration.
 * </p>
 * This interface contains many convenience methods for
 * authentication integration with existing applications.
 * For most applications, using the following methods will
 * be sufficient to achieve SSO:
 * <ol>
 * <li><code>authenticate:</code> authenticate a user.</li>
 * <li><code>isAuthenticated:</code> determine if a request is authenticated.</li>
 * <li><code>getPrincipal:</code> retrieve the principal for an authenticated request.</li>
 * <li><code>logoff:</code> sign the user out.</li>
 * </ol>
 *
 * Use the <code>HttpAuthenticatorFactory<code> to get an
 * instance of a class, or use an IoC container (like Spring)
 * to manage the underlying implementation as a singleton.
 *
 * @see com.atlassian.crowd.integration.http.HttpAuthenticatorImpl
 */
public interface HttpAuthenticator
{
    /**
     * Retrieve the underlying client properties used
     * to communicate with the Crowd Security Server.
     *
     * @return client properties.
     * @see com.atlassian.crowd.service.soap.client.SecurityServerClient#getSoapClientProperties()
     */
    public SoapClientProperties getSoapClientProperties();

    /**
     * Retrieve the underlying SecurityServerClient used
     * to communicate with the Crowd Security Server.
     *
     * @return SecurityServerClient.
     */
    public SecurityServerClient getSecurityServerClient();

    /**
     * Sets the underlying principal token on:
     * <ol>
     * <li>the request: as an attribute, so the user is authenticated for the span of the request.</li>
     * <li>the response: as a cookie, so the user is authenticated for subsequent requests.</li>
     * </ol>
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param token token value to use.
     * @throws InvalidAuthorizationTokenException   the application client's token is invalid.
     * @throws RemoteException                      there was an underlying error communicating with the server.
     * @throws InvalidAuthenticationException       the username/password combination is invalid.
     */
    void setPrincipalToken(HttpServletRequest request, HttpServletResponse response, String token)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException;

    /**
     * Attempts to retrieve the principal from the request.
     *
     * @param request servlet request
     * @return SOAPPrincipal of the authenticated user
     * @throws InvalidAuthorizationTokenException   the application client's token is invalid
     * @throws RemoteException                      there are communication issues between the client and Crowd server
     * @throws InvalidTokenException                unable to find the token
     * @throws InvalidAuthenticationException       he username/password combination is invalid
     */
    SOAPPrincipal getPrincipal(HttpServletRequest request)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidTokenException, InvalidAuthenticationException;

    /**
     * Retrieve the Crowd authentication token from the request either via:
     *
     * <ol>
     * <li>a request attribute (<b>not</b> request parameter), OR</li>
     * <li>a cookie on the request</li>
     * </ol>
     *
     * @param request HttpServletRequest.
     * @return value of the token.
     * @throws InvalidTokenException unable to find token in either a request attribute or cookie.
     * @see com.atlassian.crowd.integration.http.HttpAuthenticator#setPrincipalToken(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, String)
     */
    String getToken(HttpServletRequest request) throws InvalidTokenException;

    /**
     * Tests whether a request is authenticated via SSO.
     *
     * This only tests against the Crowd server if the validation
     * interval is exceeded, this value is obtained from crowd.properties <b>AND</b>
     * that there is a valid token present for the user in the Crowd Cookie.
     *
     * @param request   HttpServletRequest
     * @param response  HttpServletResponse
     * @return <code>true</code> if and only if the request has been authenticated
     * @throws InvalidAuthorizationTokenException   the application client's token is invalid
     * @throws RemoteException                      there was an underlying error communicating with the server
     * @throws ApplicationAccessDeniedException     user does not have access to the application
     * @throws InvalidAuthenticationException       the username/password combination is invalid
     */
    boolean isAuthenticated(HttpServletRequest request, HttpServletResponse response)
            throws InvalidAuthorizationTokenException, RemoteException, ApplicationAccessDeniedException, InvalidAuthenticationException;

    /**
     * Authenticate a remote user using SSO.
     * <p>
     * See {@link com.atlassian.crowd.integration.http.HttpAuthenticator#getValidationFactors(javax.servlet.http.HttpServletRequest)} for details regarding the validation factors used for authentication
     * </p>
     * @param request   HttpServletRequest to obtain validation factors
     * @param response  HttpServletResponse to write SSO cookie
     * @param username  username of principal
     * @param password  password of principal
     * @throws InvalidAuthorizationTokenException   the application client's token is invalid
     * @throws RemoteException                      there was an underlying error communicating with the server
     * @throws InvalidAuthenticationException       the username/password combination is invalid
     * @throws InactiveAccountException             the principal's account has been deactivated
     * @throws ApplicationAccessDeniedException     user does not have access to the application
     * @throws ExpiredCredentialException           the user's credentials have expired. The user must change their credentials in order to successfully authenticate.
     * @see   com.atlassian.crowd.integration.http.HttpAuthenticator#getValidationFactors(javax.servlet.http.HttpServletRequest)
     */
    void authenticate(HttpServletRequest request, HttpServletResponse response, String username, String password)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException;

    /**
     * Authenticate a remote user using SSO, without validating their password.
     *
     * <b>You should not be using this method unless you have previously that the user has been authenticated
     * via some other external means (eg. remember-me cookie etc).</b>
     *
     * If you are unsure whether you should be using this method or not, then you should really be
     * using {@link com.atlassian.crowd.integration.http.HttpAuthenticator#authenticate(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, String, String)}
     * instead.
     *
     * @param request   HttpServletRequest to obtain validation factors
     * @param response  HttpServletResponse to write SSO cookie
     * @param username  username of the principal that you have already authenticated via some external means
     * @throws InvalidAuthorizationTokenException   the application client's token is invalid
     * @throws RemoteException                      there was an underlying error communicating with the server
     * @throws InvalidAuthenticationException       the username/password combination is invalid
     * @throws InactiveAccountException             the principal's account has been deactivated
     * @throws ApplicationAccessDeniedException     user does not have access to the application
     */
    void authenticateWithoutValidatingPassword(HttpServletRequest request, HttpServletResponse response, String username) throws ApplicationAccessDeniedException, InvalidAuthenticationException, InvalidAuthorizationTokenException, InactiveAccountException, RemoteException;

    /**
     * Verifies the authentication of a principal's username/password,
     * given a set of validation factors.
     *
     * This will authenticate the principal using the username and password
     * provided, and will use the validation factors to generate an SSO
     * token. This token can then be used by 3rd party systems to implement
     * SSO or can be ignored to only provide centralised authentication.
     *
     * NOTE: This method will not provide SSO functionality directly - use the
     * <code>authenticate</code> method instead.
     *
     * @param username              username of principal
     * @param password              password of principal
     * @param validationFactors     validation factors used to generate a token
     * @return Crowd authentication token
     * @throws InvalidAuthorizationTokenException   the application client's token is invalid.
     * @throws RemoteException                      there was an underlying error communicating with the server.
     * @throws InvalidAuthenticationException       the username/password combination is invalid.
     * @throws InactiveAccountException             the principal's account has been deactivate.
     * @throws ApplicationAccessDeniedException     user does not have access to the application.
     * @throws ExpiredCredentialException           the user's credentials have expired.  The user must change their credentials in order to successfully authenticate/
     */
    String verifyAuthentication(String username, String password, ValidationFactor[] validationFactors) throws InvalidAuthorizationTokenException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException;

    /**
     * Authenticate a remote principal <i>without</i> using SSO.
     *
     * This performs an instant verification of username/password with
     * the centralised user repository (Crowd Server).
     *
     * @param username username of the principal.
     * @param password password of the principal.
     * @throws InvalidAuthorizationTokenException   the application client's token is invalid
     * @throws InvalidAuthenticationException       the username/password combination is invalid
     * @throws RemoteException                      there was an underlying error while connecting to the remote server.
     * @throws InactiveAccountException             the user's account is invalid.
     * @throws ApplicationAccessDeniedException     the user does not have access to the application.
     * @throws ExpiredCredentialException           the user's credentials have expired.  The user must change their credentials in order to successfully authenticate.
     * @see com.atlassian.crowd.service.soap.client.SecurityServerClient#authenticatePrincipalSimple(String, String)
     */
    void verifyAuthentication(String username, String password) throws InvalidAuthorizationTokenException, InvalidAuthenticationException, RemoteException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException;

    /**
     * Retrieves validation factors from the request:
     *
     * <ol>
     * <li>Remote Address: the source IP address of the HTTP request.</li>
     * <li>Original Address: the X-Forwarded-For HTTP header (if present and distinct from the Remote Address).</li>
     * </ol>
     *
     * @param request HttpServletRequest.
     * @return array of validation factors.
     */
    ValidationFactor[] getValidationFactors(HttpServletRequest request);

    /**
     * Log off the SSO authenticated user. This will also effectively
     * log them off from all SSO applications.
     *
     * This will sign out an authenticated user by invalidating their
     * SSO token and removing it from their cookies.
     *
     * If the request is not authenticated, this method will have
     * no effect and will not throw an exception.
     *
     * @param request   HttpServletRequest.
     * @param response  HttpServletResponse.
     * @throws InvalidAuthorizationTokenException   the application client's token is invalid
     * @throws RemoteException                      there was an error while connecting to the remote server.
     * @throws InvalidAuthenticationException       the username/password combination is invalid.
     */
    void logoff(HttpServletRequest request, HttpServletResponse response)
            throws InvalidAuthorizationTokenException, RemoteException, InvalidAuthenticationException;

    /**
     * Generate a PrincipalAuthenticationContext object containing the
     * provided username and password, and validation factors from the
     * the request.
     *
     * @param request       HttpServletRequest.
     * @param response      unused.
     * @param username      username of principal.
     * @param password      password of principal.
     * @return populated    PrincipalAuthenticationContext.
     */
    UserAuthenticationContext getPrincipalAuthenticationContext(HttpServletRequest request, HttpServletResponse response, String username, String password);
}
