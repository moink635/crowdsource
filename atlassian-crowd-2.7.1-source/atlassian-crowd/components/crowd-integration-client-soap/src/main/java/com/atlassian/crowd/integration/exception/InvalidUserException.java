package com.atlassian.crowd.integration.exception;

/**
 * An exception to denote an invalid model user.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class InvalidUserException extends InvalidDirectoryEntityException
{
    public InvalidUserException()
    {
        // needed for xfire
        super();
    }

    public InvalidUserException(com.atlassian.crowd.integration.model.DirectoryEntity user, Throwable cause)
    {
        super(user, cause);
    }

    public InvalidUserException(com.atlassian.crowd.integration.model.DirectoryEntity user, String message)
    {
        super(user, message);
    }

    public InvalidUserException(com.atlassian.crowd.integration.model.DirectoryEntity user, String message, Throwable cause)
    {
        super(user, message, cause);
    }
}
