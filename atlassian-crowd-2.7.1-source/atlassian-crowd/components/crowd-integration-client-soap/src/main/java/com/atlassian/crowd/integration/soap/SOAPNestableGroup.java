package com.atlassian.crowd.integration.soap;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

public class SOAPNestableGroup extends SOAPEntity implements Serializable
{
    private String[] groupMembers;

    public SOAPNestableGroup()
    {
    }

    public SOAPNestableGroup(String name, String[] groupMembers)
    {
        this.name = name;
        this.groupMembers = groupMembers;
    }

    public String[] getGroupMembers()
    {
        return groupMembers;
    }

    public void setGroupMembers(String[] groupMembers)
    {
        this.groupMembers = groupMembers;
    }

    public String toString()
    {
        return new ToStringBuilder(this)
                .appendSuper(super.toString())
                .append("group members", groupMembers).toString();
    }
}
