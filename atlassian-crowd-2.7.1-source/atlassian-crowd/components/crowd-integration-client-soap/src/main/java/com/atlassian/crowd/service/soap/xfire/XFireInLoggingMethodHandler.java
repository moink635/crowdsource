package com.atlassian.crowd.service.soap.xfire;

import org.codehaus.xfire.MessageContext;
import org.codehaus.xfire.exchange.AbstractMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;

/**
 * Logs incoming service request to Crowd from XFire.
 */
public class XFireInLoggingMethodHandler extends XFireLoggingMethodHandler
{
    private static final Logger logger = LoggerFactory.getLogger(XFireInLoggingMethodHandler.class);

    AbstractMessage inHandler;

    /**
     * @see XFireLoggingMethodHandler#invoke(MessageContext)
     */
    public void invoke(MessageContext messageContext) throws Exception
    {
        inHandler = messageContext.getExchange().getInMessage();

        super.invoke(messageContext);
    }

    /**
     * @see XFireLoggingMethodHandler#getMessageBodyObjects()
     */
    public Object[] getMessageBodyObjects()
    {
        if (inHandler.getBody() instanceof Collection)
        {
            return ((Collection) inHandler.getBody()).toArray();
        }
        else
        {
            return Collections.EMPTY_LIST.toArray();
        }
    }

    /**
     * @see XFireLoggingMethodHandler#getLogger()
     */
    protected Logger getLogger()
    {
        return logger;
    }
}