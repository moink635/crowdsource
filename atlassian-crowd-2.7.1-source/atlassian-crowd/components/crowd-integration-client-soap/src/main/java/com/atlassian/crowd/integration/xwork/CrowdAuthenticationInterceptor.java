package com.atlassian.crowd.integration.xwork;

import com.atlassian.crowd.integration.http.HttpAuthenticator;
import com.atlassian.crowd.integration.http.HttpAuthenticatorFactory;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.interceptor.Interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Enumeration;

/**
 * This interceptor manages protecting a <code>web.xml</code> <code>url-pattern</code>. If the requesting principal does
 * not have a valid token, they will be redirected to the authentication path specified in the <code>crowd.properties</code>
 * configuration file. Additional values are stored to the principal's session such as their originally requested
 * URL should their authentication be found invalid.
 */
public class CrowdAuthenticationInterceptor implements Interceptor
{

    /**
     * create a static reference to the logger.
     */
    private static final Logger logger = LoggerFactory.getLogger
            (CrowdAuthenticationInterceptor.class);

    /**
     * The session key stored as a <code>String<code>, is the
     * requested secure url before redirect to the authentication
     * page.
     */
    public static final String ORIGINAL_URL =
            CrowdAuthenticationInterceptor.class.getName() + ".ORIGINAL_URL";

    private final HttpAuthenticator httpAuthenticator;

    /**
     * Use this default constructor if you are not
     * using an IoC managed HttpAuthenticator/SecurityServerClient.
     * This will delegate calls to HttpAuthenticatorFactory when
     * an instance of the HttpAuthenticator is required.
     */
    public CrowdAuthenticationInterceptor()
    {
        this(HttpAuthenticatorFactory.getHttpAuthenticator());
    }

    /**
     * Use this constructor to provide an externally managed
     * singleton instance of the HttpAuthenticator.
     * <p/>
     * This should be used if you are using an IoC container
     * to manage the HttpAuthenticator/SecurityServerClient
     * instances. This should NOT be used if you are using
     * the HttpAuthenticatorFactory or SecurityServerClientFactory
     * anywhere in your application.
     *
     * @param httpAuthenticator externally managed singleton.
     */
    public CrowdAuthenticationInterceptor(HttpAuthenticator httpAuthenticator)
    {
        this.httpAuthenticator = httpAuthenticator;
    }

    public void destroy()
    {
    }

    public void init()
    {

    }

    public String intercept(ActionInvocation actionInvocation)
            throws Exception
    {

        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse response = ServletActionContext.getResponse();

        boolean isValidated = httpAuthenticator.isAuthenticated(request, response);

        // get the request URL
        StringBuffer originalURL = request.getRequestURL();

        boolean foundParameter = false;
        if (request.getParameterMap().size() > 0)
        {
            originalURL.append("?");

            Enumeration params = request.getParameterNames();
            for (; params.hasMoreElements();)
            {
                if (!foundParameter)
                {
                    foundParameter = true;
                }
                else
                {
                    originalURL.append("&");
                }

                String name = (String) params.nextElement();
                String values[] = request.getParameterValues(name);

                for (int i = 0; i < values.length; i++)
                {
                    originalURL.append(name + "=" + values[i]);
                }
            }
        }

        if (!isValidated)
        {
            logger.debug("Requesting URL is: " + originalURL);
            request.getSession().setAttribute(ORIGINAL_URL, originalURL.toString());

            String applicationAuthenticationURL = httpAuthenticator.getSoapClientProperties().getApplicationAuthenticationURL();
            logger.info("Authentication is not valid, redirecting to: " + applicationAuthenticationURL);
            response.sendRedirect(applicationAuthenticationURL);

        }
        else
        {
            request.removeAttribute(ORIGINAL_URL);
        }

        return actionInvocation.invoke();

    }
}