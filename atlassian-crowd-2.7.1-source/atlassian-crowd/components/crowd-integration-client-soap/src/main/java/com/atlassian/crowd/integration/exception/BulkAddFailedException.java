package com.atlassian.crowd.integration.exception;

import java.util.Set;

/**
 * An exception that is raised when the Bulk Creation of users failed.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class BulkAddFailedException extends CheckedSoapException
{
    private Set<String> failedUsers;
    private Set<String> existingUsers;

    public BulkAddFailedException()
    {
    }

    public BulkAddFailedException(final Set<String> failedUsers, final Set<String> existingUsers)
    {
        this.failedUsers = failedUsers;
        this.existingUsers = existingUsers;
    }

    public BulkAddFailedException(final String message, final Set<String> failedUsers, final Set<String> existingUsers)
    {
        super(message);
        this.failedUsers = failedUsers;
        this.existingUsers = existingUsers;
    }

    public BulkAddFailedException(final String message, final Set<String> failedUsers, final Set<String> existingUsers, final Throwable throwable)
    {
        super(message, throwable);
        this.failedUsers = failedUsers;
        this.existingUsers = existingUsers;
    }

    /**
     * @return the usernames of the users that it failed to create
     */
    public Set<String> getFailedUsers()
    {
        return failedUsers;
    }

    /**
     * @return the usernames of the users that it failed to create because they already existed.
     */
    public Set<String> getExistingUsers()
    {
        return existingUsers;
    }

    public void setExistingUsers(Set<String> existingUsers)
    {
        this.existingUsers = existingUsers;
    }

    public void setFailedUsers(Set<String> failedUsers)
    {
        this.failedUsers = failedUsers;
    }
}
