package com.atlassian.crowd.integration.exception;

/**
 * Thrown when an entity is not found.
 *
 * <p>Use for SOAP only. Class exists strictly to maintain Crowd 2.0.x compatibility. Use the exception classes in
 * <tt>com.atlassian.crowd.exception</tt> instead.
 */
public class ObjectNotFoundException extends CheckedSoapException
{
    public ObjectNotFoundException()
    {
    }

    public ObjectNotFoundException(Class entityClass, Object identifier)
    {
        super(new StringBuilder(64).append("Failed to find entity of type [").append(entityClass.getCanonicalName()).append("] with identifier [").append(identifier).append("]").toString());
    }

    /**
     * Default constructor.
     * @param s The message.
     * @param throwable The {@link Exception Exception}.
     */
    public ObjectNotFoundException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    /**
     * Default constructor.
     * @param throwable The {@link Exception Exception}.
     */
    public ObjectNotFoundException(Throwable throwable)
    {
        super(throwable);
    }
}
