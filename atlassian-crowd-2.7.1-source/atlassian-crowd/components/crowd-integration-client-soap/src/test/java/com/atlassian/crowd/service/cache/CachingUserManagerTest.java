package com.atlassian.crowd.service.cache;

import java.util.List;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.soap.SearchRestriction;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.service.UserManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CachingUserManagerTest
{
    private static final String USER_NAME_1 = "a-testable-user";
    private static final String USER_NAME_2 = "a-testable-user-two";
    private static final String USER_NAME_3 = "a-testable-user-three";

    private static final String ATTRIB_NAME_1 = "name-1";
    private static final String ATTRIB_NAME_2 = "name-2";
    private static final String ATTRIB_VALUE_1 = "val-1";
    private static final String ATTRIB_VALUE_2 = "val-2";
    private static final String ATTRIB_VALUE_3 = "val-3";

    private static final String DEFAULT_BLANK = "-";

    private UserManager userManager;
    private SecurityServerClient mockSecurityServerClient;
    private SOAPPrincipal soapPrincipal1;
    private SOAPPrincipal soapPrincipal2;
    private SOAPPrincipal soapPrincipal3;

    @After
    public void tearDown() throws Exception
    {
        CachingManagerFactory.getCache().flush();

        userManager = null;
        mockSecurityServerClient = null;

        soapPrincipal1 = null;
        soapPrincipal2 = null;
        soapPrincipal3 = null;
    }

    @Before
    public void setUp() throws Exception
    {
        mockSecurityServerClient = mock(SecurityServerClient.class);

        userManager = new CachingUserManager(mockSecurityServerClient, CachingManagerFactory.getCache());

        soapPrincipal1 = new SOAPPrincipal(USER_NAME_1);
        soapPrincipal2 = new SOAPPrincipal(USER_NAME_2);
        soapPrincipal3 = new SOAPPrincipal(USER_NAME_3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetUser_Null() throws Exception
    {
        userManager.getUser(null);
    }

    @Test
    public void testGetUser_NotInCache() throws Exception
    {
        SOAPPrincipal user = new SOAPPrincipal(USER_NAME_1);

        when(mockSecurityServerClient.findPrincipalByName(USER_NAME_1)).thenReturn(user);

        SOAPPrincipal foundUser = userManager.getUser(USER_NAME_1);

        assertNotNull(foundUser);
        assertEquals(user, foundUser);

        // check they're now in the cache - SSC will assert if findPrincipalByName() is called a second time.
        SOAPPrincipal foundTwice = userManager.getUser(USER_NAME_1);

        assertNotNull(foundTwice);
        assertEquals(user, foundTwice);
    }

    @Test(expected = UserNotFoundException.class)
    public void testGetUser_NotInCrowd() throws Exception
    {
        when(mockSecurityServerClient.findPrincipalByName(USER_NAME_1)).thenThrow(new UserNotFoundException(USER_NAME_1));

        userManager.getUser(USER_NAME_1);
    }

    @Test
    public void testGetUserFromToken_PassToServer() throws Exception
    {
        when(mockSecurityServerClient.findPrincipalByToken("MyTokenIsHappy")).thenReturn(soapPrincipal1);

        SOAPPrincipal foundUser = userManager.getUserFromToken("MyTokenIsHappy");

        assertNotNull(foundUser);
        assertEquals(USER_NAME_1, foundUser.getName());

        foundUser = userManager.getUser(USER_NAME_1);

        assertNotNull(foundUser);
        assertEquals(USER_NAME_1, foundUser.getName());
    }

    @Test
    public void testSearchUsers_PassToServer() throws Exception
    {
        Mockito.doReturn(new SOAPPrincipal[]{soapPrincipal1, soapPrincipal2})
            .when(mockSecurityServerClient).searchPrincipals(Mockito.<SearchRestriction[]>any());

        List/*<SOAPPrincipal>*/ users = userManager.searchUsers(new SearchRestriction[0]);

        assertNotNull(users);
        assertEquals("Should have been two users returned", 2, users.size());
        assertTrue(users.contains(soapPrincipal1));
        assertTrue(users.contains(soapPrincipal2));
        assertFalse(users.contains(USER_NAME_3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddUser_Null() throws Exception
    {
        userManager.addUser(null, null);
    }

    @Test
    public void testAddUser_AlreadyExisting() throws Exception
    {
        when(mockSecurityServerClient.addPrincipal(Mockito.<SOAPPrincipal>any(), Mockito.<PasswordCredential>any())).thenThrow(new InvalidUserException(null, "you're already here!"));

        try
        {
            userManager.addUser(soapPrincipal1, PasswordCredential.unencrypted("eepe"));

            fail("Should have thrown InvalidPrincipalException");
        }
        catch (InvalidUserException e)
        {
        }

        // check they're not in the cache
        assertNull(CachingManagerFactory.getCache().getUser(USER_NAME_1));
    }

    @Test
    public void testAddUser() throws Exception
    {
        when(mockSecurityServerClient.addPrincipal(Mockito.eq(soapPrincipal1), Mockito.<PasswordCredential>any())).thenReturn(soapPrincipal1);

        userManager.addUser(soapPrincipal1, new PasswordCredential("eepe"));

        // make sure they've been cached
        SOAPPrincipal foundUser = userManager.getUser(USER_NAME_1);

        assertNotNull(foundUser);
        assertEquals(USER_NAME_1, foundUser.getName());
    }

    @Test
    public void testAddUser_UpdatesAllUsers() throws Exception
    {
        when(mockSecurityServerClient.addPrincipal(Mockito.eq(soapPrincipal3), Mockito.<PasswordCredential>any())).thenReturn(soapPrincipal3);
        Mockito.doReturn(new SOAPPrincipal[]{soapPrincipal1, soapPrincipal2})
            .when(mockSecurityServerClient).searchPrincipals(Mockito.<SearchRestriction[]>any());

        // pre-add check
        List/*<String>*/ allUserNames = userManager.getAllUserNames();

        assertNotNull(allUserNames);
        assertEquals("Should be two in list before add", 2, allUserNames.size());
        assertTrue(allUserNames.contains(USER_NAME_1));
        assertTrue(allUserNames.contains(USER_NAME_2));
        assertFalse(allUserNames.contains(USER_NAME_3));

        userManager.addUser(soapPrincipal3, new PasswordCredential("eepe"));

        // make sure they've been cached
        SOAPPrincipal foundUser = userManager.getUser(USER_NAME_3);

        assertNotNull(foundUser);
        assertEquals(USER_NAME_3, foundUser.getName());

        // make sure the allusers cache has been updated.
        allUserNames = userManager.getAllUserNames();

        assertNotNull(allUserNames);
        assertEquals("Should be three in list after add", 3, allUserNames.size());
        assertTrue(allUserNames.contains(USER_NAME_1));
        assertTrue(allUserNames.contains(USER_NAME_2));
        assertTrue(allUserNames.contains(USER_NAME_3));
    }

    @Test
    public void testAddUser_NullPrimaryAttributeValues() throws Exception
    {
        when(mockSecurityServerClient.addPrincipal(Mockito.eq(soapPrincipal1), Mockito.<PasswordCredential>any())).thenReturn(soapPrincipal1);

        SOAPAttribute[] attributes = new SOAPAttribute[1];
        attributes[0] = new SOAPAttribute(UserConstants.EMAIL, "");
        attributes[0].setValues(null); // Force the attribute value to null for testing
        soapPrincipal1.setAttributes(attributes);
        userManager.addUser(soapPrincipal1, new PasswordCredential("eepe"));

        // make sure they've been cached
        SOAPPrincipal foundUser = userManager.getUser(USER_NAME_1);

        assertNotNull(foundUser);
        assertEquals(USER_NAME_1, foundUser.getName());
        // check null value has been replaced by the default blank value: -
        assertEquals(DEFAULT_BLANK, foundUser.getAttribute(UserConstants.EMAIL).getValues()[0]);
    }

    @Test
    public void testAddUser_NonNullPrimaryAttributeValues() throws Exception
    {
        when(mockSecurityServerClient.addPrincipal(Mockito.eq(soapPrincipal1), Mockito.<PasswordCredential>any())).thenReturn(soapPrincipal1);

        SOAPAttribute[] attributes = new SOAPAttribute[1];
        attributes[0] = new SOAPAttribute(UserConstants.EMAIL, "test@example.com");
        soapPrincipal1.setAttributes(attributes);
        userManager.addUser(soapPrincipal1, new PasswordCredential("eepe"));

        // make sure they've been cached
        SOAPPrincipal foundUser = userManager.getUser(USER_NAME_1);

        assertNotNull(foundUser);
        assertEquals(USER_NAME_1, foundUser.getName());
        assertEquals("test@example.com", foundUser.getAttribute(UserConstants.EMAIL).getValues()[0]);
    }

    @Test
    public void testAddUser_NullAttributeValues() throws Exception
    {
        when(mockSecurityServerClient.addPrincipal(Mockito.eq(soapPrincipal1), Mockito.<PasswordCredential>any())).thenReturn(soapPrincipal1);

        SOAPAttribute[] attributes = new SOAPAttribute[1];
        attributes[0] = new SOAPAttribute(ATTRIB_NAME_1, "");
        attributes[0].setValues(null); // Force the attribute value to null for testing
        soapPrincipal1.setAttributes(attributes);
        userManager.addUser(soapPrincipal1, new PasswordCredential("eepe"));

        // make sure they've been cached
        SOAPPrincipal foundUser = userManager.getUser(USER_NAME_1);

        assertNotNull(foundUser);
        assertEquals(USER_NAME_1, foundUser.getName());
        assertNotNull(foundUser.getAttribute(ATTRIB_NAME_1));
        assertNull(foundUser.getAttribute(ATTRIB_NAME_1).getValues());
    }

    @Test
    public void testAddUser_NonNullAttributeValues() throws Exception
    {
        when(mockSecurityServerClient.addPrincipal(Mockito.eq(soapPrincipal1), Mockito.<PasswordCredential>any())).thenReturn(soapPrincipal1);

        SOAPAttribute[] attributes = new SOAPAttribute[2];
        attributes[0] = new SOAPAttribute(ATTRIB_NAME_1, ATTRIB_VALUE_1);
        attributes[1] = new SOAPAttribute(ATTRIB_NAME_2, new String[]{ATTRIB_VALUE_2, ATTRIB_VALUE_3});
        soapPrincipal1.setAttributes(attributes);
        userManager.addUser(soapPrincipal1, new PasswordCredential("eepe"));

        // make sure they've been cached
        SOAPPrincipal foundUser = userManager.getUser(USER_NAME_1);

        assertNotNull(foundUser);
        assertEquals(USER_NAME_1, foundUser.getName());
        assertEquals(ATTRIB_VALUE_1, foundUser.getAttribute(ATTRIB_NAME_1).getValues()[0]);
        assertEquals(ATTRIB_VALUE_2, foundUser.getAttribute(ATTRIB_NAME_2).getValues()[0]);
        assertEquals(ATTRIB_VALUE_3, foundUser.getAttribute(ATTRIB_NAME_2).getValues()[1]);
    }

    @Test
    public void testUpdateUser_NoAttributes() throws Exception
    {
        userManager.updateUser(soapPrincipal1);
        verify(mockSecurityServerClient, Mockito.never()).updatePrincipalAttribute(Mockito.anyString(), Mockito.<SOAPAttribute>any());
    }

    @Test
    public void testUpdateUser_FailedUpdate() throws Exception
    {
        Mockito.doThrow(new UserNotFoundException(USER_NAME_1))
            .when(mockSecurityServerClient).updatePrincipalAttribute(Mockito.anyString(), Mockito.<SOAPAttribute>any());

        try
        {
            SOAPPrincipal user = new SOAPPrincipal();
            user.setName(USER_NAME_1);
            SOAPAttribute[] attributes = new SOAPAttribute[2];
            attributes[0] = new SOAPAttribute(ATTRIB_NAME_1, ATTRIB_VALUE_1);
            attributes[1] = new SOAPAttribute(ATTRIB_NAME_2, ATTRIB_VALUE_2);
            user.setAttributes(attributes);

            userManager.updateUser(user);

            fail("Should have thrown UserNotFoundException");
        }
        catch (UserNotFoundException e)
        {
        }

        // check that the cache was not updated.
        assertNull("After failed update, user should not have been added to the cache", CachingManagerFactory.getCache().getUser(USER_NAME_1));
    }

    @Test
    public void testUpdateUser_SuccessfulUpdate() throws Exception
    {
        SOAPAttribute[] attributes = new SOAPAttribute[2];
        attributes[0] = new SOAPAttribute(ATTRIB_NAME_1, ATTRIB_VALUE_1);
        attributes[1] = new SOAPAttribute(ATTRIB_NAME_2, ATTRIB_VALUE_2);
        soapPrincipal1.setAttributes(attributes);
        userManager.updateUser(soapPrincipal1);

        verify(mockSecurityServerClient, Mockito.times(2)).updatePrincipalAttribute(Mockito.anyString(), Mockito.<SOAPAttribute>any());
    }

    @Test
    public void testUpdateUser_SuccessfulUpdate_CheckCaches() throws Exception
    {
        SOAPAttribute[] attributes = new SOAPAttribute[2];
        attributes[0] = new SOAPAttribute(ATTRIB_NAME_1, ATTRIB_VALUE_1);
        attributes[1] = new SOAPAttribute(ATTRIB_NAME_2, ATTRIB_VALUE_2);
        soapPrincipal1.setAttributes(attributes);

        when(mockSecurityServerClient.addPrincipal(Mockito.eq(soapPrincipal1), Mockito.<PasswordCredential>any())).thenReturn(soapPrincipal1);

        userManager.addUser(soapPrincipal1, new PasswordCredential(""));

        attributes[1] = new SOAPAttribute(ATTRIB_NAME_2, ATTRIB_VALUE_3);
        soapPrincipal1.setAttributes(attributes);

        userManager.updateUser(soapPrincipal1);

        SOAPPrincipal foundUser = userManager.getUser(USER_NAME_1);

        assertNotNull(foundUser);
        assertEquals(USER_NAME_1, foundUser.getName());
        assertEquals(ATTRIB_VALUE_3, foundUser.getAttributes()[1].getValues()[0]);

        verify(mockSecurityServerClient, Mockito.times(2)).updatePrincipalAttribute(Mockito.anyString(), Mockito.<SOAPAttribute>any());
    }

    @Test(expected = ApplicationPermissionException.class)
    public void testRemoveUser_NoPermissions() throws Exception
    {
        Mockito.doThrow(new ApplicationPermissionException("You wish...")).when(mockSecurityServerClient).removePrincipal(USER_NAME_1);

        userManager.removeUser(USER_NAME_1);
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveUser_NoUser() throws Exception
    {
        Mockito.doThrow(new UserNotFoundException(USER_NAME_1)).when(mockSecurityServerClient).removePrincipal(USER_NAME_1);

        userManager.removeUser(USER_NAME_1);
    }

    @Test
    public void testRemoveUser() throws Exception
    {
        when(mockSecurityServerClient.findPrincipalByName(USER_NAME_1)).thenThrow(new UserNotFoundException(USER_NAME_1));

        // remove the user...
        userManager.removeUser(USER_NAME_1); // will throw if there's a problem.

        verify(mockSecurityServerClient).removePrincipal(USER_NAME_1);

        // ...and check the user cache
        try
        {
            userManager.getUser(USER_NAME_1);
            fail("Should have thrown UserNotFoundException");
        }
        catch (UserNotFoundException e)
        {
        }

        verify(mockSecurityServerClient).findPrincipalByName(USER_NAME_1);
    }

    @Test
    public void testRemoveUser_UpdatesAllUsers() throws Exception
    {
        Mockito.doReturn(new SOAPPrincipal[]{soapPrincipal1, soapPrincipal2})
            .when(mockSecurityServerClient).searchPrincipals(Mockito.<SearchRestriction[]>any());
        when(mockSecurityServerClient.findPrincipalByName(USER_NAME_1)).thenThrow(new UserNotFoundException(USER_NAME_1));

        // Load the list of all users from the SSC, so that we can update the cache on the remove...
        List/*<String>*/ allUsers = userManager.getAllUserNames();
        assertNotNull(allUsers);
        assertEquals("Should be two users before remove", 2, allUsers.size());

        // remove the user...
        userManager.removeUser(USER_NAME_1); // will throw if there's a problem.

        verify(mockSecurityServerClient).removePrincipal(USER_NAME_1);

        // ...and check the user cache
        try
        {
            userManager.getUser(USER_NAME_1);
            fail("Should have thrown UserNotFoundException");
        }
        catch (UserNotFoundException e)
        {
        }

        // check all the users list
        allUsers = userManager.getAllUserNames();
        assertNotNull(allUsers);
        assertEquals("Should only be one user left", 1, allUsers.size());
        assertTrue(allUsers.contains(USER_NAME_2));
        assertFalse(allUsers.contains(USER_NAME_1));
    }

    @Test
    public void testGetAllUserNames_NotInCache() throws Exception
    {
        Mockito.doReturn(new SOAPPrincipal[]{soapPrincipal1, soapPrincipal2})
            .when(mockSecurityServerClient).searchPrincipals(Mockito.<SearchRestriction[]>any());

        List/*<String>*/ userNames = userManager.getAllUserNames();
        assertNotNull(userNames);
        assertEquals("Should be two usernames in list", 2, userNames.size());
        assertTrue(userNames.contains(USER_NAME_1));
        assertTrue(userNames.contains(USER_NAME_2));
        assertFalse(userNames.contains(USER_NAME_3));

    }

    @Test
    public void testGetAllUserNames_InCache() throws Exception
    {
        Mockito.doReturn(new SOAPPrincipal[]{soapPrincipal1, soapPrincipal2})
            .when(mockSecurityServerClient).searchPrincipals(Mockito.<SearchRestriction[]>any());

        List/*<String>*/ userNames = userManager.getAllUserNames();
        assertNotNull(userNames);
        assertEquals("Should be two usernames in list", 2, userNames.size());
        assertTrue(userNames.contains(USER_NAME_1));
        assertTrue(userNames.contains(USER_NAME_2));
        assertFalse(userNames.contains(USER_NAME_3));

        // second call should not hit SSC, but should fetch from cache.
        List/*<String>*/ moreUserNames = userManager.getAllUserNames();
        assertNotNull(moreUserNames);
        assertEquals("Should be two usernames in list", 2, moreUserNames.size());
        assertTrue(moreUserNames.contains(USER_NAME_1));
        assertTrue(moreUserNames.contains(USER_NAME_2));
        assertFalse(moreUserNames.contains(USER_NAME_3));
    }

    @Test
    public void testGetUserWithAttributes_NotInCache() throws Exception
    {
        SOAPPrincipal user = new SOAPPrincipal(USER_NAME_1);

        when(mockSecurityServerClient.findPrincipalWithAttributesByName(USER_NAME_1)).thenReturn(user);

        SOAPPrincipal foundUser = userManager.getUserWithAttributes(USER_NAME_1);

        assertNotNull(foundUser);
        assertEquals(user, foundUser);

        // check they're now in the cache - SSC will assert if findPrincipalByName() is called a second time.
        SOAPPrincipal foundTwice = userManager.getUserWithAttributes(USER_NAME_1);

        assertNotNull(foundTwice);
        assertEquals(user, foundTwice);
    }

    @Test(expected = UserNotFoundException.class)
    public void testGetUserWithAttributes_NotInCrowd() throws Exception
    {
        when(mockSecurityServerClient.findPrincipalWithAttributesByName(USER_NAME_1)).thenThrow(new UserNotFoundException(USER_NAME_1));

        userManager.getUserWithAttributes(USER_NAME_1);
    }
}
