package com.atlassian.crowd.service.cache;

import java.util.Arrays;
import java.util.List;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SOAPNestableGroup;
import com.atlassian.crowd.integration.soap.SearchRestriction;
import com.atlassian.crowd.service.GroupManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class CachingGroupManagerTest
{
    private static final String GROUP_NAME_1 = "a-testable-group";
    private static final String GROUP_NAME_2 = "a-testable-group-two";
    private static final String GROUP_NAME_3 = "a-testable-group-three";

    private static final String GROUP_DESCRIPTION = "something-witty-goes-here";

    private GroupManager groupManager;
    private SecurityServerClient securityServerClient;

    @After
    public void tearDown()
    {
        CachingManagerFactory.getCache().flush();
        groupManager = null;
        securityServerClient = null;
    }

    @Before
    public void setUp() throws Exception
    {
        securityServerClient = mock(SecurityServerClient.class);

        groupManager = new CachingGroupManager(securityServerClient, CachingManagerFactory.getCache());
    }


    @Test(expected = IllegalArgumentException.class)
    public void testGetGroup_Null() throws Exception
    {
        GroupManager gm = new CachingGroupManager(null, null);
        gm.getGroup(null);
    }

    @Test
    public void testGetGroup_NotInCache() throws Exception
    {
        SOAPGroup group = new SOAPGroup(GROUP_NAME_1, null);

        when(securityServerClient.findGroupByName(GROUP_NAME_1)).thenReturn(group);

        SOAPGroup foundGroup = groupManager.getGroup(GROUP_NAME_1);
        assertNotNull(foundGroup);
        assertEquals(group, foundGroup);

        // check they're now in the cache - SSC will assert if findGroupByName() is called a second time.
        SOAPGroup foundTwice = groupManager.getGroup(GROUP_NAME_1);
        assertNotNull(foundTwice);
        assertEquals(group, foundTwice);
    }

    @Test(expected = GroupNotFoundException.class)
    public void testGetGroup_NotInCrowd() throws Exception
    {
        when(securityServerClient.findGroupByName(Mockito.anyString())).thenThrow(new GroupNotFoundException(GROUP_NAME_1));

        groupManager.getGroup(GROUP_NAME_1);
    }

    @Test
    public void testSearchGroups_PassToServer() throws Exception
    {
        SOAPGroup group1 = new SOAPGroup(GROUP_NAME_1, null);
        SOAPGroup group2 = new SOAPGroup(GROUP_NAME_2, null);

        when(securityServerClient.searchGroups(Mockito.<SearchRestriction[]>any())).thenReturn(new SOAPGroup[]{group1, group2});

        SearchRestriction[] restrictions = new SearchRestriction[0];

        List/*<SOAPGroup>*/ groups = groupManager.searchGroups(restrictions);

        assertNotNull(groups);
        assertEquals("Should have been two groups returned", 2, groups.size());
        assertTrue(groups.contains(group1));
        assertTrue(groups.contains(group2));
        assertFalse(groups.contains(GROUP_NAME_3));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddGroup_Null() throws Exception
    {
        GroupManager gm = new CachingGroupManager(null, null);

        gm.addGroup(null);
    }

    @Test(expected = InvalidGroupException.class)
    public void testAddGroup_Existing() throws Exception
    {
        when(securityServerClient.addGroup(Mockito.<SOAPGroup>any())).thenThrow(new InvalidGroupException(null, "Already here mate."));

        SOAPGroup group = new SOAPGroup(GROUP_NAME_1, null);
        groupManager.addGroup(group);
    }

    @Test
    public void testAddGroup() throws Exception
    {
        SOAPGroup group = new SOAPGroup(GROUP_NAME_1, null);

        when(securityServerClient.addGroup(group)).thenReturn(group);

        groupManager.addGroup(group);

        verify(securityServerClient).addGroup(group);

        // check that it's now in the cache.
        SOAPGroup foundGroup = groupManager.getGroup(GROUP_NAME_1);
        assertNotNull(foundGroup);
        assertEquals(GROUP_NAME_1, foundGroup.getName());
    }

    @Test
    public void testAddGroup_UpdatesAllGroups() throws Exception
    {
        SOAPNestableGroup[] groups = new SOAPNestableGroup[2];
        groups[0] = new SOAPNestableGroup();
        groups[0].setName(GROUP_NAME_1);
        groups[1] = new SOAPNestableGroup();
        groups[1].setName(GROUP_NAME_2);

        SOAPGroup group = new SOAPGroup(GROUP_NAME_3, null);

        when(securityServerClient.addGroup(group)).thenReturn(group);
        when(securityServerClient.findAllGroupRelationships()).thenReturn(groups);

        // make sure the allgroups cache is populated
        List/*<String>*/ allGroups = groupManager.getAllGroupNames();
        assertNotNull(allGroups);
        assertEquals("Should be two groups before add", 2, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_1));
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertFalse(allGroups.contains(GROUP_NAME_3));

        groupManager.addGroup(group);

        // check that it's now in the cache.
        SOAPGroup foundGroup = groupManager.getGroup(GROUP_NAME_3);
        assertNotNull(foundGroup);
        assertEquals(GROUP_NAME_3, foundGroup.getName());

        // check that we've updated the list of all groups
        allGroups = groupManager.getAllGroupNames();
        assertNotNull(allGroups);
        assertEquals("Should be three groups after add", 3, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_1));
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertTrue(allGroups.contains(GROUP_NAME_3));
    }

    @Test
    public void testUpdateGroup_FailedUpdate() throws Exception
    {
        Mockito.doThrow(new GroupNotFoundException(GROUP_NAME_1)).when(securityServerClient)
            .updateGroup(Mockito.anyString(), Mockito.anyString(), Mockito.anyBoolean());

        try
        {
            SOAPGroup group = new SOAPGroup(GROUP_NAME_1, null);
            group.setDescription(GROUP_DESCRIPTION);
            groupManager.updateGroup(group);

            fail("Should have thrown GroupNotFoundException");
        }
        catch (GroupNotFoundException e)
        {
        }

        // check that the cache was not updated.
        BasicCache gc = CachingManagerFactory.getCache();
        assertNull("After failed update, group should not have been added to the cache", gc.getGroup(GROUP_NAME_1));
    }

    @Test
    public void testUpdateGroup_SuccessfulUpdate() throws Exception
    {
        SOAPGroup group = new SOAPGroup(GROUP_NAME_1, null);
        group.setDescription(GROUP_DESCRIPTION);
        group.setActive(true);
        groupManager.updateGroup(group);

        verify(securityServerClient).updateGroup(GROUP_NAME_1, GROUP_DESCRIPTION, true);
    }

    @Test(expected = ApplicationPermissionException.class)
    public void testRemoveGroup_NoPermissions() throws Exception
    {
        Mockito.doThrow(new ApplicationPermissionException("You wish...")).when(securityServerClient).removeGroup(GROUP_NAME_1);

        groupManager.removeGroup(GROUP_NAME_1);
    }

    @Test(expected = GroupNotFoundException.class)
    public void testRemoveGroup_NoGroup() throws Exception
    {
        Mockito.doThrow(new GroupNotFoundException(GROUP_NAME_1)).when(securityServerClient).removeGroup(GROUP_NAME_1);

        groupManager.removeGroup(GROUP_NAME_1);
    }

    @Test
    public void testRemoveGroup() throws Exception
    {
        groupManager.removeGroup(GROUP_NAME_1);

        verify(securityServerClient).removeGroup(GROUP_NAME_1);

        // will have thrown if there's a problem.

        // TODO assert that there are no references in the caches'
    }

    @Test
    public void testRemoveGroup_UpdatesAllGroups() throws Exception
    {
        SOAPNestableGroup[] groups = new SOAPNestableGroup[2];
        groups[0] = new SOAPNestableGroup(GROUP_NAME_1, null);
        groups[1] = new SOAPNestableGroup(GROUP_NAME_2, null);

        when(securityServerClient.findAllGroupRelationships()).thenReturn(groups);
        when(securityServerClient.findGroupByName(GROUP_NAME_1)).thenThrow(new GroupNotFoundException(GROUP_NAME_1));

        // Load the list of all groups from the SSC, so that we can check the cache update on the remove...
        List/*<String>*/ allGroups = groupManager.getAllGroupNames();
        assertNotNull(allGroups);
        assertEquals("Should be two groups before remove", Arrays.asList(GROUP_NAME_1, GROUP_NAME_2), allGroups);

        // remove the group...
        groupManager.removeGroup(GROUP_NAME_1); // will throw if there's a problem.

        // ...and check the group cache
        try
        {
            groupManager.getGroup(GROUP_NAME_1);
            fail("Should have thrown GroupNotFoundException");
        }
        catch (GroupNotFoundException e)
        {
        }

        verify(securityServerClient).removeGroup(GROUP_NAME_1);

        // check the "all groups" list
        assertNull(CachingManagerFactory.getCache().getAllGroupNames());
        assertNull(CachingManagerFactory.getCache().getGroup(GROUP_NAME_1));
        assertNull(CachingManagerFactory.getCache().getGroupName(GROUP_NAME_1));
    }

    @Test
    public void testGetAllGroupNames_NotInCache() throws Exception
    {
        SOAPNestableGroup group1 = new SOAPNestableGroup(GROUP_NAME_1, null);
        SOAPNestableGroup group2 = new SOAPNestableGroup(GROUP_NAME_2, null);

        when(securityServerClient.findAllGroupRelationships()).thenReturn(
                new SOAPNestableGroup[]{group1, group2});

        List/*<String>*/ groupNames = groupManager.getAllGroupNames();

        assertNotNull(groupNames);
        assertEquals("Should be two group names in list", 2, groupNames.size());
        assertTrue(groupNames.contains(GROUP_NAME_1));
        assertTrue(groupNames.contains(GROUP_NAME_2));
        assertFalse(groupNames.contains(GROUP_NAME_3));
    }

    @Test
    public void testGetAllGroupNames_InCache() throws Exception
    {
        SOAPNestableGroup group1 = new SOAPNestableGroup(GROUP_NAME_1, null);
        SOAPNestableGroup group2 = new SOAPNestableGroup(GROUP_NAME_2, null);

        when(securityServerClient.findAllGroupRelationships()).thenReturn(
                new SOAPNestableGroup[]{group1, group2});

        List/*<String>*/ groupNames = groupManager.getAllGroupNames();

        assertNotNull(groupNames);
        assertEquals("Should be two groupnames in list", 2, groupNames.size());
        assertTrue(groupNames.contains(GROUP_NAME_1));
        assertTrue(groupNames.contains(GROUP_NAME_2));
        assertFalse(groupNames.contains(GROUP_NAME_3));

        // second call should not hit SSC, but should fetch from cache.
        List/*<String>*/ moreGroupNames = groupManager.getAllGroupNames();

        assertNotNull(moreGroupNames);
        assertEquals("Should be two groupnames in list", 2, moreGroupNames.size());
        assertTrue(moreGroupNames.contains(GROUP_NAME_1));
        assertTrue(moreGroupNames.contains(GROUP_NAME_2));
        assertFalse(moreGroupNames.contains(GROUP_NAME_3));
    }
}
