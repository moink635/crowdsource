package com.atlassian.crowd.integration.soap;

import junit.framework.TestCase;

public class SOAPCookieInfoTest extends TestCase
{
    private static final String DOMAIN_1 = ".domain.example.org.";
    private static final String DOMAIN_2 = ".example.org.";

    public void testToStringDefaultConstructor()
    {
        SOAPCookieInfo a = new SOAPCookieInfo();
        assertNotNull(a.toString());
    }

    public void testEqualsWithNullOther()
    {
        SOAPCookieInfo a = new SOAPCookieInfo();
        assertFalse(a.equals(null));
    }

    public void testEqualsWithDefaultConstructors()
    {
        SOAPCookieInfo a = new SOAPCookieInfo();
        SOAPCookieInfo b = new SOAPCookieInfo();
        assertTrue(a.equals(b));
    }

    public void testEqualsWithDefaultOther()
    {
        SOAPCookieInfo a = new SOAPCookieInfo();
        a.setDomain(DOMAIN_1);
        SOAPCookieInfo b = new SOAPCookieInfo();
        assertFalse(a.equals(b));
    }

    public void testEqualsWithDifferentDomain()
    {
        SOAPCookieInfo a = new SOAPCookieInfo();
        a.setDomain(DOMAIN_1);
        SOAPCookieInfo b = new SOAPCookieInfo();
        b.setDomain(DOMAIN_2);
        assertFalse(a.equals(b));
    }

    public void testEqualsWithDifferentSecure()
    {
        SOAPCookieInfo a = new SOAPCookieInfo();
        a.setDomain(DOMAIN_1);
        a.setSecure(false);
        SOAPCookieInfo b = new SOAPCookieInfo();
        b.setDomain(DOMAIN_1);
        b.setSecure(true);
        assertFalse(a.equals(b));
    }

    public void testEqualsWithDifferentDomainAndSecure()
    {
        SOAPCookieInfo a = new SOAPCookieInfo();
        a.setDomain(DOMAIN_1);
        a.setSecure(false);
        SOAPCookieInfo b = new SOAPCookieInfo();
        b.setDomain(DOMAIN_2);
        b.setSecure(true);
        assertFalse(a.equals(b));
    }

    public void testEquals()
    {
        SOAPCookieInfo a = new SOAPCookieInfo();
        a.setDomain(DOMAIN_1);
        a.setSecure(true);
        SOAPCookieInfo b = new SOAPCookieInfo();
        b.setDomain(DOMAIN_1);
        b.setSecure(true);
        assertTrue(a.equals(b));
    }

    public void testHashCodeDifference()
    {
        SOAPCookieInfo a = new SOAPCookieInfo();
        a.setDomain(DOMAIN_1);
        a.setSecure(true);
        SOAPCookieInfo b = new SOAPCookieInfo();
        b.setDomain(DOMAIN_2);
        b.setSecure(true);

        assertNotNull(a.hashCode());
        assertNotNull(b.hashCode());
        assertFalse(a.hashCode() == b.hashCode());
    }

    public void testHashCodeEquality()
    {
        SOAPCookieInfo a = new SOAPCookieInfo();
        a.setDomain(DOMAIN_1);
        a.setSecure(true);
        SOAPCookieInfo b = new SOAPCookieInfo();
        b.setDomain(DOMAIN_1);
        b.setSecure(true);

        assertNotNull(a.hashCode());
        assertNotNull(b.hashCode());
        assertEquals(a.hashCode(), b.hashCode());
    }
}
