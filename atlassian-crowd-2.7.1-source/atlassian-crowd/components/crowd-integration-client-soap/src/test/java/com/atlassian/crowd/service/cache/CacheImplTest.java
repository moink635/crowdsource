package com.atlassian.crowd.service.cache;

import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.model.user.UserConstants;
import net.sf.ehcache.CacheManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

public class CacheImplTest
{
    private CacheImpl cache;

    @Before
    public void setUp() throws Exception
    {
        cache = new CacheImpl(mock(CacheManager.class));
    }

    @After
    public void tearDown()
    {
        cache = null;
    }

    @Test
    public void testIsUserDirty()
    {
        Date currentDate = new Date();
        SOAPAttribute[] soapAttributes = {new SOAPAttribute(UserConstants.FIRSTNAME, "Ted"), new SOAPAttribute(UserConstants.LASTNAME, "Phillips"), new SOAPAttribute(UserConstants.EMAIL, "ted@test.com"), new SOAPAttribute(UserConstants.DISPLAYNAME, "Ted Phillips")};
        SOAPPrincipal latestPrincipal = new SOAPPrincipal(1l, "ted", 1l, "description", true, currentDate, currentDate, soapAttributes);
        SOAPPrincipal cachePrincipal = new SOAPPrincipal(1l, "ted", 1l, "description", true, currentDate, currentDate, soapAttributes);

        assertFalse(cache.isUserDirty(latestPrincipal, cachePrincipal));

        // Keep all the same except email address
        soapAttributes = new SOAPAttribute[]{new SOAPAttribute(UserConstants.FIRSTNAME, "Ted"), new SOAPAttribute(UserConstants.LASTNAME, "Phillips"), new SOAPAttribute(UserConstants.EMAIL, "ted@example.com"), new SOAPAttribute(UserConstants.DISPLAYNAME, "Ted Phillips")};
        latestPrincipal = new SOAPPrincipal(1l, "ted", 1l, "description", true, currentDate, currentDate, soapAttributes);
        assertTrue(cache.isUserDirty(latestPrincipal, cachePrincipal));

    }

    @Test
    public void testIsUserDirtyWithCompletelyDifferentUsers()
    {
        try
        {
            cache.isUserDirty(new SOAPPrincipal("bill"), new SOAPPrincipal("bob"));

            fail("This should not be a valid comparison");
        }
        catch (IllegalArgumentException e)
        {

        }
    }

}

