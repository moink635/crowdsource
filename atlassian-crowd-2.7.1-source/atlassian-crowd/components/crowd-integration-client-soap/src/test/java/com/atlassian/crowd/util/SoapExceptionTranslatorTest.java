package com.atlassian.crowd.util;

import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.exception.InvalidAuthenticationException;

import org.junit.Test;

import static org.junit.Assert.fail;

import static org.junit.Assert.assertEquals;

/**
 * Tests for SoapExceptionTranslator.
 */
public class SoapExceptionTranslatorTest
{
    /**
     * Tests that a UserNotFoundException is converted to the SOAP version of ObjectNotFoundException.
     */
    @Test
    public void testThrowSoapEquivalentUncheckedException_UNFE() throws Exception
    {
        UserNotFoundException unfe = new UserNotFoundException("bob");
        try
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(unfe);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            assertEquals(UserNotFoundException.class.getName() + " " + unfe.getMessage(), e.getMessage());
        }
    }

    /**
     * Tests that a GroupNotFoundException is converted to the SOAP version of ObjectNotFoundException.
     */
    @Test
    public void testThrowSoapEquivalentUncheckedException_GNFE() throws Exception
    {
        GroupNotFoundException gnfe = new GroupNotFoundException("group1");
        try
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(gnfe);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            assertEquals(GroupNotFoundException.class.getName() + " " + gnfe.getMessage(), e.getMessage());
        }
    }

    /**
     * Tests that a MembershipNotFoundException is converted to the SOAP version of ObjectNotFoundException.
     */
    @Test
    public void testThrowSoapEquivalentUncheckedException_MNFE() throws Exception
    {
        MembershipNotFoundException mnfe = new MembershipNotFoundException("child", "parent");
        try
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(mnfe);
        }
        catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
        {
            assertEquals(MembershipNotFoundException.class.getName() + " " + mnfe.getMessage(), e.getMessage());
        }
    }

    /**
     * Tests that converting from UserNotFoundException to ObjectNotFoundException and then back to UserNotFoundException
     * restores the original exception.
     */
    @Test
    public void testThrowEquivalentUncheckedException_UNFERoundTrip() throws Exception
    {
        UserNotFoundException unfe = new UserNotFoundException("bob");
        try
        {
            try
            {
                SoapExceptionTranslator.throwSoapEquivalentCheckedException(unfe);
            }
            catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
            {
                SoapExceptionTranslator.throwEquivalentCheckedException(e);
            }
        }
        catch (UserNotFoundException e)
        {
            assertEquals(unfe.getMessage(), e.getMessage());
        }
    }

    /**
     * Tests that converting from GroupNotFoundException to ObjectNotFoundException and then back to GroupNotFoundException
     * restores the original exception.
     */
    @Test
    public void testThrowEquivalentUncheckedException_GNFERoundTrip() throws Exception
    {
        GroupNotFoundException gnfe = new GroupNotFoundException("group");
        try
        {
            try
            {
                SoapExceptionTranslator.throwSoapEquivalentCheckedException(gnfe);
            }
            catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
            {
                SoapExceptionTranslator.throwEquivalentCheckedException(e);
            }
        }
        catch (GroupNotFoundException e)
        {
            assertEquals(gnfe.getMessage(), e.getMessage());
        }
    }

    /**
     * Tests that converting from MembershipNotFoundException to ObjectNotFoundException and then back to MembershipNotFoundException
     * restores the original exception.
     */
    @Test
    public void testThrowEquivalentUncheckedException_MNFERoundTrip() throws Exception
    {
        MembershipNotFoundException mnfe = new MembershipNotFoundException("child", "parent");
        try
        {
            try
            {
                SoapExceptionTranslator.throwSoapEquivalentCheckedException(mnfe);
            }
            catch (com.atlassian.crowd.integration.exception.ObjectNotFoundException e)
            {
                SoapExceptionTranslator.throwEquivalentCheckedException(e);
            }
        }
        catch (MembershipNotFoundException e)
        {
            assertEquals(mnfe.getMessage(), e.getMessage());
        }
    }

    @Test
    public void usernamesAreExtractedFromMessages()
    {
        InvalidAuthenticationException iae = new InvalidAuthenticationException("Failed to log in user <username> for some reason");
        try
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(iae);
            fail();
        }
        catch (com.atlassian.crowd.exception.InvalidAuthenticationException e)
        {
            assertEquals("username", e.getMessage());
        }
    }

    @Test
    public void firstUsernameIsExtractedFromMessagesWithAngledBracketsLater()
    {
        InvalidAuthenticationException iae = new InvalidAuthenticationException("First <username> and <> <<user>> <<");
        try
        {
            SoapExceptionTranslator.throwEquivalentCheckedException(iae);
            fail();
        }
        catch (com.atlassian.crowd.exception.InvalidAuthenticationException e)
        {
            assertEquals("username", e.getMessage());
        }
    }
}
