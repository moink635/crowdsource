package com.atlassian.crowd.service.soap.client;


import java.util.Properties;

import com.atlassian.crowd.integration.Constants;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SoapClientPropertiesImplTest
{
    private Properties property;

    private SoapClientPropertiesImpl clientProperties;

    private String url1 = "http://localhost:8095/crowd";
    private String url2 = "http://localhost:8095/crowd/";
    private String url3 = "http://localhost:8095/crowd/services";
    private String url4 = "http://localhost:8095/crowd/services/";
    private String url5 = "http://localhost:8095/crowd/services/SecurityServer";
    private String url6 = "http://localhost:8095/crowd/services/SecurityServer/";

    private final String TARGET_BASE_URL = "http://localhost:8095/crowd";
    private final String TARGET_SECURITY_SERVER_URL = "http://localhost:8095/crowd/services/SecurityServer";


    @Before
    public void setUp()
    {
        // Create the base property with all properties except "Constants.PROPERTIES_FILE_SECURITY_SERVER_URL"
        property = new Properties();
        property.setProperty(Constants.PROPERTIES_FILE_APPLICATION_NAME, "crowd");
        property.setProperty(Constants.PROPERTIES_FILE_APPLICATION_PASSWORD, "sDRkwFwX");
        property.setProperty(Constants.PROPERTIES_FILE_APPLICATION_LOGIN_URL, "http://localhost:8095/crowd/console/");
        property.setProperty(Constants.PROPERTIES_FILE_COOKIE_TOKENKEY, "session.tokenkey");
        property.setProperty(Constants.PROPERTIES_FILE_SESSIONKEY_VALIDATIONINTERVAL, "0");
        property.setProperty(Constants.PROPERTIES_FILE_SESSIONKEY_LASTVALIDATION, "session.lastvalidation");

        clientProperties = SoapClientPropertiesImpl.newInstanceFromProperties(property);
    }

    @Test
    public void testGetUrls()
    {
        property.setProperty(Constants.PROPERTIES_FILE_SECURITY_SERVER_URL, url1);
        updateAndAssertProperties();

        property.setProperty(Constants.PROPERTIES_FILE_SECURITY_SERVER_URL, url2);
        updateAndAssertProperties();

        property.setProperty(Constants.PROPERTIES_FILE_SECURITY_SERVER_URL, url3);
        updateAndAssertProperties();

        property.setProperty(Constants.PROPERTIES_FILE_SECURITY_SERVER_URL, url4);
        updateAndAssertProperties();

        property.setProperty(Constants.PROPERTIES_FILE_SECURITY_SERVER_URL, url5);
        updateAndAssertProperties();

        property.setProperty(Constants.PROPERTIES_FILE_SECURITY_SERVER_URL, url6);
        updateAndAssertProperties();
    }

    private void updateAndAssertProperties()
    {
        String url;
        clientProperties.updateProperties(property);
        url = clientProperties.getBaseURL();
        assertEquals(TARGET_BASE_URL, url);

        url = clientProperties.getSecurityServerURL();
        assertEquals(TARGET_SECURITY_SERVER_URL, url);
    }

    @Test
    public void instanceCanBeLoadedFromOnlyProperties()
    {
        Properties props = new Properties();
        props.setProperty("crowd.server.url", "server-url");

        String systemPropName = "crowd.property.crowd.server.url";

        String oldValue = System.getProperty(systemPropName);
        try
        {
            System.setProperty(systemPropName, "system-property-server-url");

            SoapClientPropertiesImpl clientProps = SoapClientPropertiesImpl.newInstanceFromPropertiesWithoutOverrides(props);
            assertEquals("server-url", clientProps.getBaseURL());
        }
        finally
        {
            if (oldValue != null)
            {
                System.setProperty(systemPropName, oldValue);
            }
            else
            {
                System.clearProperty(systemPropName);
            }
        }
    }
}
