package com.atlassian.crowd.service.cache;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

public class GroupMembershipCacheTest extends TestCase
{
    private static final String USER_NAME_1 = "User";
    private static final String USER_NAME_2 = "'Nother User";
    private static final String GROUP_NAME_1 = "Group";
    private static final String GROUP_NAME_2 = "Boop Group";
    private BasicCache cache;

    public void tearDown()
    {
        cache.flush();
        cache = null;
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        cache = CachingManagerFactory.getCache();
    }

    public void testSetMembership_True()
    {

        cache.setMembership(USER_NAME_1, GROUP_NAME_1, Boolean.TRUE);

        assertTrue(cache.isMember(USER_NAME_1, GROUP_NAME_1).booleanValue());
    }

    public void testSetMembership_False()
    {

        cache.setMembership(USER_NAME_1, GROUP_NAME_1, Boolean.FALSE);

        assertFalse(cache.isMember(USER_NAME_1, GROUP_NAME_1).booleanValue());
    }

    public void testSetMembership_NotSet()
    {

        cache.setMembership(USER_NAME_1, GROUP_NAME_1, Boolean.TRUE);

        assertNull(cache.isMember(USER_NAME_2, GROUP_NAME_1));
    }

    public void testSetMembership_TwoSet()
    {

        cache.setMembership(USER_NAME_1, GROUP_NAME_1, Boolean.TRUE);
        cache.setMembership(USER_NAME_2, GROUP_NAME_1, Boolean.FALSE);

        assertTrue(cache.isMember(USER_NAME_1, GROUP_NAME_1).booleanValue());
    }

    public void testGetAllMemberships()
    {

        List groups = new ArrayList(2);
        groups.add(GROUP_NAME_1);
        groups.add(GROUP_NAME_2);
        cache.cacheAllMemberships(USER_NAME_1, groups);

        List foundGroups = cache.getAllMemberships(USER_NAME_1);
        assertNotNull(foundGroups);
        assertEquals("Should have two groups", 2, foundGroups.size());
        assertTrue(foundGroups.contains(toLowerCase(GROUP_NAME_1)));
        assertTrue(foundGroups.contains(toLowerCase(GROUP_NAME_2)));
        assertFalse(foundGroups.contains(toLowerCase(USER_NAME_1)));
    }

    public void testGetAllMemberships_TwoSet()
    {

        List groups = new ArrayList(2);
        groups.add(GROUP_NAME_1);
        groups.add(GROUP_NAME_2);
        cache.cacheAllMemberships(USER_NAME_1, groups);

        List groups2 = new ArrayList(1);
        groups2.add(GROUP_NAME_2);
        cache.cacheAllMemberships(USER_NAME_2, groups2);

        List foundGroups = cache.getAllMemberships(USER_NAME_1);
        assertNotNull(foundGroups);
        assertEquals("Should have two groups", 2, foundGroups.size());
        assertTrue(foundGroups.contains(toLowerCase(GROUP_NAME_1)));
        assertTrue(foundGroups.contains(toLowerCase(GROUP_NAME_2)));
        assertFalse(foundGroups.contains(toLowerCase(USER_NAME_1)));

        foundGroups = cache.getAllMemberships(USER_NAME_2);
        assertNotNull(foundGroups);
        assertEquals("Should have one groups", 1, foundGroups.size());
        assertTrue(foundGroups.contains(toLowerCase(GROUP_NAME_2)));
        assertFalse(foundGroups.contains(toLowerCase(GROUP_NAME_1)));
    }

    public void testGetAllMemberships_None()
    {

        List groups = new ArrayList(2);
        groups.add(GROUP_NAME_1);
        groups.add(GROUP_NAME_2);
        cache.cacheAllMemberships(USER_NAME_1, groups);

        List foundGroups = cache.getAllMemberships(USER_NAME_2);
        assertNull(foundGroups);
    }
}
