package com.atlassian.crowd.service.cache;

import com.atlassian.crowd.integration.soap.SOAPGroup;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class GroupCacheTest extends TestCase
{
    private static final String GROUP_NAME_1 = "GroupLove";
    private static final String GROUP_NAME_2 = "I'm_a_Badger!";
    private static final String GROUP_NAME_3 = "However, I'm actually an echidna";
    private static final String GROUP_NAME_4 = "Dia dhuit!";
    private static final String DESCRIPTION_1 = "MyDesc";
    private static final String DESCRIPTION_2 = "A.N. Other Desc";

    private BasicCache cache;

    public void tearDown()
    {
        cache.flush();
        cache = null;
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        cache = CachingManagerFactory.getCache();
    }

    public void testAddOrReplace_Add()
    {


        SOAPGroup group = new SOAPGroup();
        group.setName(GROUP_NAME_1);

        cache.cacheGroup(group);

        SOAPGroup retrievedUser = cache.getGroup(GROUP_NAME_1);
        assertNotNull(retrievedUser);
        assertEquals(group, retrievedUser);
    }

    public void testAddOrReplace_Replace()
    {


        SOAPGroup group1 = new SOAPGroup();
        group1.setName(GROUP_NAME_1);
        group1.setDescription(DESCRIPTION_1);
        cache.cacheGroup(group1);

        SOAPGroup retrievedGroup = cache.getGroup(GROUP_NAME_1);
        assertNotNull(retrievedGroup);
        assertEquals(group1, retrievedGroup);
        assertEquals(DESCRIPTION_1, retrievedGroup.getDescription());

        SOAPGroup group2 = new SOAPGroup();
        group2.setName(GROUP_NAME_1);
        group2.setDescription(DESCRIPTION_2);
        cache.cacheGroup(group2);

        retrievedGroup = cache.getGroup(GROUP_NAME_1);
        assertNotNull(retrievedGroup);
        assertEquals(group2, retrievedGroup);
        assertEquals(DESCRIPTION_2, retrievedGroup.getDescription());
    }

    public void testAddOrReplace_AddAnother()
    {


        SOAPGroup group1 = new SOAPGroup();
        group1.setName(GROUP_NAME_1);
        group1.setDescription(DESCRIPTION_1);
        cache.cacheGroup(group1);

        SOAPGroup group2 = new SOAPGroup();
        group2.setName(GROUP_NAME_2);
        group2.setDescription(DESCRIPTION_2);
        cache.cacheGroup(group2);

        SOAPGroup retrievedGroup = cache.getGroup(GROUP_NAME_1);
        assertNotNull(retrievedGroup);
        assertEquals(group1, retrievedGroup);
        assertEquals(DESCRIPTION_1, retrievedGroup.getDescription());

        retrievedGroup = cache.getGroup(GROUP_NAME_2);
        assertNotNull(retrievedGroup);
        assertEquals(group2, retrievedGroup);
        assertEquals(DESCRIPTION_2, retrievedGroup.getDescription());
    }

    public void testRemove_AddTwoRemoveOne()
    {


        SOAPGroup group1 = new SOAPGroup();
        group1.setName(GROUP_NAME_1);
        group1.setDescription(DESCRIPTION_1);
        cache.cacheGroup(group1);

        SOAPGroup group2 = new SOAPGroup();
        group2.setName(GROUP_NAME_2);
        group2.setDescription(DESCRIPTION_2);
        cache.cacheGroup(group2);

        SOAPGroup retrievedGroup = cache.getGroup(GROUP_NAME_1);
        assertNotNull(retrievedGroup);
        assertEquals(group1, retrievedGroup);
        assertEquals(DESCRIPTION_1, retrievedGroup.getDescription());

        retrievedGroup = cache.getGroup(GROUP_NAME_2);
        assertNotNull(retrievedGroup);
        assertEquals(group2, retrievedGroup);
        assertEquals(DESCRIPTION_2, retrievedGroup.getDescription());

        cache.removeGroup(GROUP_NAME_2);

        retrievedGroup = cache.getGroup(GROUP_NAME_1);
        assertNotNull(retrievedGroup);
        assertEquals(group1, retrievedGroup);
        assertEquals(DESCRIPTION_1, retrievedGroup.getDescription());

        assertNull(cache.getGroup(GROUP_NAME_2));
    }

    public void testAddOrReplaceAllGroupNames_Add()
    {


        List groupNames = new ArrayList(3);
        groupNames.add(GROUP_NAME_1);
        groupNames.add(GROUP_NAME_2);
        groupNames.add(GROUP_NAME_3);
        cache.cacheAllGroupNames(groupNames);

        List foundGroupNames = cache.getAllGroupNames();
        assertNotNull(foundGroupNames);
        assertEquals("Should be three groupnames in list", 3, foundGroupNames.size());
        assertTrue(foundGroupNames.contains(GROUP_NAME_1));
        assertTrue(foundGroupNames.contains(GROUP_NAME_2));
        assertTrue(foundGroupNames.contains(GROUP_NAME_3));
    }

    public void testAddOrReplaceAllGroupNames_Replace()
    {


        List groupNames = new ArrayList(3);
        groupNames.add(GROUP_NAME_1);
        groupNames.add(GROUP_NAME_2);
        groupNames.add(GROUP_NAME_3);
        cache.cacheAllGroupNames(groupNames);

        List moreGroupNames = new ArrayList(1);
        moreGroupNames.add(GROUP_NAME_4);
        cache.cacheAllGroupNames(moreGroupNames);

        List foundGroupNames = cache.getAllGroupNames();
        assertNotNull(foundGroupNames);
        assertEquals("Should be just one username in list", 1, foundGroupNames.size());
        assertTrue(foundGroupNames.contains(GROUP_NAME_4));
        assertFalse(foundGroupNames.contains(GROUP_NAME_1));
        assertFalse(foundGroupNames.contains(GROUP_NAME_2));
        assertFalse(foundGroupNames.contains(GROUP_NAME_3));
    }

    public void testGetAllGroupNames_Null()
    {

        List userNames = cache.getAllGroupNames();
        assertNull(userNames);
    }
}
