package com.atlassian.crowd.util;

import com.atlassian.crowd.integration.soap.SOAPNestableGroup;
import com.atlassian.crowd.service.cache.BasicCache;
import com.atlassian.crowd.service.cache.CachingManagerFactory;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import org.mockito.Mockito;

import junit.framework.TestCase;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.*;

public class NestingHelperTest extends TestCase
{
    private static final String GROUP_NAME_1 = "groupy-1";
    private static final String GROUP_NAME_2 = "groupy-2";
    private static final String GROUP_NAME_3 = "groupy-3";
    private static final String GROUP_NAME_4 = "groupy-4";
    private static final String GROUP_NAME_5 = "groupy-5";
    private static final String GROUP_NAME_6 = "groupy-6";
    private BasicCache cache;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        cache = CachingManagerFactory.getCache();
    }

    @Override
    protected void tearDown() throws Exception
    {
        super.tearDown();
        cache.flush();
        cache = null;
    }

    public void testGetAllGroupNames_NoChildren()
    {
        SOAPNestableGroup[] groupList = new SOAPNestableGroup[3];
        groupList[0] = new SOAPNestableGroup();
        groupList[0].setName(GROUP_NAME_1);
        groupList[1] = new SOAPNestableGroup();
        groupList[1].setName(GROUP_NAME_2);
        groupList[2] = new SOAPNestableGroup();
        groupList[2].setName(GROUP_NAME_3);

        BasicCache mockCache = mock(BasicCache.class);
        List/*<String>*/ allGroups = NestingHelper.cacheGroupRelationships(groupList, mockCache);

        verify(mockCache).cacheAncestorsForGroups(any(Map.class));
        verify(mockCache).cacheAllGroupNames(any(List.class));
        verify(mockCache, never()).getAllGroupNames();

        assertNotNull(allGroups);
        assertEquals("Should be three groups in total", 3, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_1));
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertTrue(allGroups.contains(GROUP_NAME_3));
        assertFalse(allGroups.contains(GROUP_NAME_4));
    }

    public void testGetAllGroupNames_Children()
    {
        SOAPNestableGroup[] groupList = new SOAPNestableGroup[4];
        groupList[0] = new SOAPNestableGroup();
        groupList[0].setName(GROUP_NAME_1);
        groupList[1] = new SOAPNestableGroup();
        groupList[1].setName(GROUP_NAME_2);
        groupList[1].setGroupMembers(new String[]{GROUP_NAME_3});
        groupList[2] = new SOAPNestableGroup();
        groupList[2].setName(GROUP_NAME_3);
        groupList[2].setGroupMembers(new String[]{GROUP_NAME_4});
        groupList[3] = new SOAPNestableGroup();
        groupList[3].setName(GROUP_NAME_4);

        // check list returned from method is correct
        List/*<String>*/ allGroups = NestingHelper.cacheGroupRelationships(groupList, cache);
        assertNotNull(allGroups);
        assertEquals("Should be four groups in total", 4, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_1));
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertTrue(allGroups.contains(GROUP_NAME_3));
        assertTrue(allGroups.contains(GROUP_NAME_4));
        assertFalse(allGroups.contains(GROUP_NAME_5));

        // check list saved in cache is correct
        allGroups = cache.getAllGroupNames();
        assertNotNull(allGroups);
        assertEquals("Should be four groups in total", 4, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_1));
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertTrue(allGroups.contains(GROUP_NAME_3));
        assertTrue(allGroups.contains(GROUP_NAME_4));
        assertFalse(allGroups.contains(GROUP_NAME_5));

        final Map<String, Set<String>> ancestorsForGroups = cache.getAncestorsForGroups();

        // check child-parent relationships are complete and correct
        Set<String> parentGroups = ancestorsForGroups.get(GROUP_NAME_1);
        assertTrue("GROUP_NAME_1 should have no parents", parentGroups.isEmpty());

        parentGroups = ancestorsForGroups.get(GROUP_NAME_2);
        assertTrue("GROUP_NAME_2 should have no parents", parentGroups.isEmpty());

        parentGroups = ancestorsForGroups.get(GROUP_NAME_3);
        assertEquals("GROUP_NAME_3 should have 1 parent", 1, parentGroups.size());
        assertTrue(parentGroups.contains(GROUP_NAME_2));
        assertFalse(parentGroups.contains(GROUP_NAME_4));

        parentGroups = ancestorsForGroups.get(GROUP_NAME_4);
        assertEquals("GROUP_NAME_4 should have 2 parents", 2, parentGroups.size());
        assertTrue(parentGroups.contains(GROUP_NAME_2));
        assertTrue(parentGroups.contains(GROUP_NAME_3));
        assertFalse(parentGroups.contains(GROUP_NAME_4));
        assertFalse(parentGroups.contains(GROUP_NAME_1));
    }

    public void testGetAllGroupNames_CircularReference()
    {
        SOAPNestableGroup[] groupList = new SOAPNestableGroup[5];
        groupList[0] = new SOAPNestableGroup();
        groupList[0].setName(GROUP_NAME_1);
        groupList[0].setGroupMembers(new String[]{GROUP_NAME_3, GROUP_NAME_4});
        groupList[1] = new SOAPNestableGroup();
        groupList[1].setName(GROUP_NAME_2);
        groupList[1].setGroupMembers(new String[]{GROUP_NAME_3});
        groupList[2] = new SOAPNestableGroup();
        groupList[2].setName(GROUP_NAME_3);
        groupList[2].setGroupMembers(new String[]{GROUP_NAME_4});
        groupList[3] = new SOAPNestableGroup();
        groupList[3].setName(GROUP_NAME_4);
        groupList[3].setGroupMembers(new String[]{GROUP_NAME_1});
        groupList[4] = new SOAPNestableGroup();
        groupList[4].setName(GROUP_NAME_5);

        // check list returned from method is correct
        List/*<String>*/ allGroups = NestingHelper.cacheGroupRelationships(groupList, cache);
        assertNotNull(allGroups);
        assertEquals("Should be five groups in total", 5, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_1));
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertTrue(allGroups.contains(GROUP_NAME_3));
        assertTrue(allGroups.contains(GROUP_NAME_4));
        assertTrue(allGroups.contains(GROUP_NAME_5));
        assertFalse(allGroups.contains(GROUP_NAME_6));

        // check list saved in cache is correct
        allGroups = cache.getAllGroupNames();
        assertNotNull(allGroups);
        assertEquals("Should be five groups in total", 5, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_1));
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertTrue(allGroups.contains(GROUP_NAME_3));
        assertTrue(allGroups.contains(GROUP_NAME_4));
        assertTrue(allGroups.contains(GROUP_NAME_5));
        assertFalse(allGroups.contains(GROUP_NAME_6));

        final Map<String, Set<String>> ancestorsForGroups = cache.getAncestorsForGroups();

        // check child-parent relationships are complete and correct
        Set<String> parentGroups = ancestorsForGroups.get(GROUP_NAME_1);
        assertEquals("GROUP_NAME_1 should have 3 parents", 3, parentGroups.size());
        assertTrue(parentGroups.contains(GROUP_NAME_2));
        assertTrue(parentGroups.contains(GROUP_NAME_3));
        assertTrue(parentGroups.contains(GROUP_NAME_4));
        assertFalse(parentGroups.contains(GROUP_NAME_1));
        assertFalse(parentGroups.contains(GROUP_NAME_5));

        parentGroups = ancestorsForGroups.get(GROUP_NAME_2);
        assertTrue("GROUP_NAME_2 should have no parents", parentGroups.isEmpty());

        parentGroups = ancestorsForGroups.get(GROUP_NAME_3);
        assertEquals("GROUP_NAME_3 should have 3 parents", 3, parentGroups.size());
        assertTrue(parentGroups.contains(GROUP_NAME_1));
        assertTrue(parentGroups.contains(GROUP_NAME_2));
        assertTrue(parentGroups.contains(GROUP_NAME_4));
        assertFalse(parentGroups.contains(GROUP_NAME_5));

        parentGroups = ancestorsForGroups.get(GROUP_NAME_4);
        assertEquals("GROUP_NAME_4 should have 3 parents", 3, parentGroups.size());
        assertTrue(parentGroups.contains(GROUP_NAME_1));
        assertTrue(parentGroups.contains(GROUP_NAME_2));
        assertTrue(parentGroups.contains(GROUP_NAME_3));
        assertFalse(parentGroups.contains(GROUP_NAME_4));

        parentGroups = ancestorsForGroups.get(GROUP_NAME_5);
        assertTrue("GROUP_NAME_5 should have no parents", parentGroups.isEmpty());
    }

    public void testGetAllGroupNames_CircularReference2()
    {
        SOAPNestableGroup[] groupList = new SOAPNestableGroup[5];
        groupList[0] = new SOAPNestableGroup();
        groupList[0].setName(GROUP_NAME_1);
        groupList[0].setGroupMembers(new String[]{GROUP_NAME_2});
        groupList[1] = new SOAPNestableGroup();
        groupList[1].setName(GROUP_NAME_2);
        groupList[1].setGroupMembers(new String[]{GROUP_NAME_3});
        groupList[2] = new SOAPNestableGroup();
        groupList[2].setName(GROUP_NAME_3);
        groupList[2].setGroupMembers(new String[]{GROUP_NAME_4});
        groupList[3] = new SOAPNestableGroup();
        groupList[3].setName(GROUP_NAME_4);
        groupList[3].setGroupMembers(new String[]{GROUP_NAME_5});
        groupList[4] = new SOAPNestableGroup();
        groupList[4].setName(GROUP_NAME_5);
        groupList[4].setGroupMembers(new String[]{GROUP_NAME_1});

        // check list returned from method is correct
        List/*<String>*/ allGroups = NestingHelper.cacheGroupRelationships(groupList, cache);
        assertNotNull(allGroups);
        assertEquals("Should be five groups in total", 5, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_1));
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertTrue(allGroups.contains(GROUP_NAME_3));
        assertTrue(allGroups.contains(GROUP_NAME_4));
        assertTrue(allGroups.contains(GROUP_NAME_5));
        assertFalse(allGroups.contains(GROUP_NAME_6));

        // check list saved in cache is correct
        allGroups = cache.getAllGroupNames();
        assertNotNull(allGroups);
        assertEquals("Should be five groups in total", 5, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_1));
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertTrue(allGroups.contains(GROUP_NAME_3));
        assertTrue(allGroups.contains(GROUP_NAME_4));
        assertTrue(allGroups.contains(GROUP_NAME_5));
        assertFalse(allGroups.contains(GROUP_NAME_6));

        final Map<String, Set<String>> ancestorsForGroups = cache.getAncestorsForGroups();

        // check child-parent relationships are complete and correct
        Set<String> parentGroups = ancestorsForGroups.get(GROUP_NAME_1);
        assertEquals("GROUP_NAME_1 should have 4 parents", 4, parentGroups.size());
        assertTrue(parentGroups.contains(GROUP_NAME_2));
        assertTrue(parentGroups.contains(GROUP_NAME_3));
        assertTrue(parentGroups.contains(GROUP_NAME_4));
        assertTrue(parentGroups.contains(GROUP_NAME_5));
        assertFalse(parentGroups.contains(GROUP_NAME_1));

        parentGroups = ancestorsForGroups.get(GROUP_NAME_2);
        assertEquals("GROUP_NAME_2 should have 4 parents", 4, parentGroups.size());
        assertTrue(parentGroups.contains(GROUP_NAME_1));
        assertTrue(parentGroups.contains(GROUP_NAME_3));
        assertTrue(parentGroups.contains(GROUP_NAME_4));
        assertTrue(parentGroups.contains(GROUP_NAME_5));
        assertFalse(parentGroups.contains(GROUP_NAME_2));

        parentGroups = ancestorsForGroups.get(GROUP_NAME_3);
        assertEquals("GROUP_NAME_3 should have 4 parents", 4, parentGroups.size());
        assertTrue(parentGroups.contains(GROUP_NAME_1));
        assertTrue(parentGroups.contains(GROUP_NAME_2));
        assertTrue(parentGroups.contains(GROUP_NAME_4));
        assertTrue(parentGroups.contains(GROUP_NAME_5));
        assertFalse(parentGroups.contains(GROUP_NAME_3));

        parentGroups = ancestorsForGroups.get(GROUP_NAME_4);
        assertEquals("GROUP_NAME_4 should have 4 parents", 4, parentGroups.size());
        assertTrue(parentGroups.contains(GROUP_NAME_1));
        assertTrue(parentGroups.contains(GROUP_NAME_2));
        assertTrue(parentGroups.contains(GROUP_NAME_3));
        assertTrue(parentGroups.contains(GROUP_NAME_5));
        assertFalse(parentGroups.contains(GROUP_NAME_4));

        parentGroups = ancestorsForGroups.get(GROUP_NAME_5);
        assertEquals("GROUP_NAME_5 should have 4 parents", 4, parentGroups.size());
        assertTrue(parentGroups.contains(GROUP_NAME_1));
        assertTrue(parentGroups.contains(GROUP_NAME_2));
        assertTrue(parentGroups.contains(GROUP_NAME_3));
        assertTrue(parentGroups.contains(GROUP_NAME_4));
        assertFalse(parentGroups.contains(GROUP_NAME_5));
    }

    public void testGetAllGroupsForUser_NoParents()
    {

    }

    public void testGetAllGroupsForUser_Parents()
    {

    }

    public void testGetAllGroupsForUser_TwoSetParents()
    {

    }

    public void testGetAllGroupsForUser_CircularReference()
    {
        SOAPNestableGroup[] groupList = new SOAPNestableGroup[5];
        groupList[0] = new SOAPNestableGroup();
        groupList[0].setName(GROUP_NAME_1);
        groupList[0].setGroupMembers(new String[]{GROUP_NAME_3, GROUP_NAME_4});
        groupList[1] = new SOAPNestableGroup();
        groupList[1].setName(GROUP_NAME_2);
        groupList[1].setGroupMembers(new String[]{GROUP_NAME_3});
        groupList[2] = new SOAPNestableGroup();
        groupList[2].setName(GROUP_NAME_3);
        groupList[2].setGroupMembers(new String[]{GROUP_NAME_4});
        groupList[3] = new SOAPNestableGroup();
        groupList[3].setName(GROUP_NAME_4);
        groupList[3].setGroupMembers(new String[]{GROUP_NAME_1});
        groupList[4] = new SOAPNestableGroup();
        groupList[4].setName(GROUP_NAME_5);

        // check list returned from method is correct
        List/*<String>*/ allGroups = NestingHelper.cacheGroupRelationships(groupList, cache);
        assertNotNull(allGroups);
        assertEquals("Should be five groups in total", 5, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_1));
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertTrue(allGroups.contains(GROUP_NAME_3));
        assertTrue(allGroups.contains(GROUP_NAME_4));
        assertTrue(allGroups.contains(GROUP_NAME_5));
        assertFalse(allGroups.contains(GROUP_NAME_6));

        // check list saved in cache is correct
        allGroups = cache.getAllGroupNames();
        assertNotNull(allGroups);
        assertEquals("Should be five groups in total", 5, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_1));
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertTrue(allGroups.contains(GROUP_NAME_3));
        assertTrue(allGroups.contains(GROUP_NAME_4));
        assertTrue(allGroups.contains(GROUP_NAME_5));
        assertFalse(allGroups.contains(GROUP_NAME_6));

        List groupsForUser = NestingHelper.getAllGroupsForUser(Arrays.asList(GROUP_NAME_1), cache.getAncestorsForGroups());
        assertNotNull(groupsForUser);
        assertEquals("Should be four groups for user", 4, groupsForUser.size());
        assertTrue(groupsForUser.contains(GROUP_NAME_1));
        assertTrue(groupsForUser.contains(GROUP_NAME_2));
        assertTrue(groupsForUser.contains(GROUP_NAME_3));
        assertTrue(groupsForUser.contains(GROUP_NAME_4));
        assertFalse(groupsForUser.contains(GROUP_NAME_5));

        groupsForUser = NestingHelper.getAllGroupsForUser(Arrays.asList(GROUP_NAME_2, GROUP_NAME_3), cache.getAncestorsForGroups());
        assertNotNull(groupsForUser);
        assertEquals("Should be four groups for user", 4, groupsForUser.size());
        assertTrue(groupsForUser.contains(GROUP_NAME_1));
        assertTrue(groupsForUser.contains(GROUP_NAME_2));
        assertTrue(groupsForUser.contains(GROUP_NAME_3));
        assertTrue(groupsForUser.contains(GROUP_NAME_4));
        assertFalse(groupsForUser.contains(GROUP_NAME_5));

        groupsForUser = NestingHelper.getAllGroupsForUser(Arrays.asList(GROUP_NAME_2, GROUP_NAME_5), cache.getAncestorsForGroups());
        assertNotNull(groupsForUser);
        assertEquals("Should be two groups for user", 2, groupsForUser.size());
        assertTrue(groupsForUser.contains(GROUP_NAME_2));
        assertTrue(groupsForUser.contains(GROUP_NAME_5));
        assertFalse(groupsForUser.contains(GROUP_NAME_1));
    }

    public void testUnknownChildGroupsAreIgnoredWhenCalculatingGroupRelationships() throws Exception
    {
        SecurityServerClient securityServerClient = Mockito.mock(SecurityServerClient.class);
        BasicCache basicCache = Mockito.mock(BasicCache.class);

        SOAPNestableGroup[] groups = {
                new SOAPNestableGroup("group", new String[]{"unknown-child-group"})
        };

        Mockito.when(basicCache.getAllGroupNames()).thenReturn(null);
        Mockito.when(securityServerClient.findAllGroupRelationships()).thenReturn(groups);

        assertEquals(ImmutableSet.<String>of("group"),
                ImmutableSet.<String>copyOf(NestingHelper.cacheGroupRelationships(groups, basicCache)));

        Map<String, Set<String>> ancestorsByGroup = ImmutableMap.<String, Set<String>>of(
                "group", ImmutableSet.<String>of());

        Mockito.verify(basicCache).cacheAncestorsForGroups(ancestorsByGroup);
    }
}
