package com.atlassian.crowd.util;

import com.atlassian.crowd.service.client.ResourceLocator;
import com.atlassian.crowd.service.client.ClientResourceLocator;
import junit.framework.TestCase;

import java.util.Properties;

public class TestPropertyUtils extends TestCase
{
    private static final String TEST_PROPERTY = "test.property";
    private String propertyFile;
    private static final String TEST_VALUE = "newvalue";
    private PropertyUtils propertyUtils = null;
    private ResourceLocator resourceLocator;
    private String resourceLocation;

    public void setUp() throws Exception
    {
        super.setUp();
        propertyFile = TestPropertyUtils.class.getSimpleName() + ".properties";

        resourceLocator = new ClientResourceLocator(propertyFile);

        propertyUtils = new PropertyUtils();

        // Set a system property for the tests
        resourceLocation = resourceLocator.getResourceLocation();
    }

    public void tearDown() throws Exception
    {
        // Make sure there is always a property in the file
        propertyUtils.updateProperty(resourceLocation, TEST_PROPERTY, TEST_VALUE);

        System.clearProperty(propertyFile);

        propertyUtils = null;
        propertyFile = null;

        super.tearDown();
    }

    public void testGetProperties()
    {
        Properties properties = propertyUtils.getProperties(resourceLocation);

        assertNotNull(properties);

        assertFalse(properties.isEmpty());
    }

    public void testRemoveProperty()
    {
        // Get the current property value
        Properties properties = propertyUtils.getProperties(resourceLocation);
        String currentValue = properties.getProperty(TEST_PROPERTY);

        // Update the property
        propertyUtils.removeProperty(resourceLocation, TEST_PROPERTY);

        // Get the property again
        properties = propertyUtils.getProperties(resourceLocation);
        String newValue = properties.getProperty(TEST_PROPERTY);

        // We should have an updated value
        assertNull(newValue);

        // The new value should not be the same as the old value
        assertNotSame(newValue, currentValue);
    }

    public void testUpdateProperty()
    {
        // Get the current property value
        Properties properties = propertyUtils.getProperties(resourceLocation);
        String currentValue = properties.getProperty(TEST_PROPERTY);

        // Update the property
        propertyUtils.updateProperty(resourceLocation, TEST_PROPERTY, TEST_VALUE);

        // Get the property again
        properties = propertyUtils.getProperties(resourceLocation);
        String newValue = properties.getProperty(TEST_PROPERTY);

        // We should have an updated value
        assertEquals(TEST_VALUE, newValue);

        // The new value should not be the same as the old value
        assertNotSame(newValue, currentValue);
    }
}
