package com.atlassian.crowd.service.cache;

import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class UserCacheTest extends TestCase
{
    private static final String USER_NAME_1 = "MyOtherUserIsABadger";
    private static final String USER_NAME_2 = "I'm_a_Badger!";
    private static final String USER_NAME_3 = "However, I'm actually an echidna";
    private static final String USER_NAME_4 = "Dia dhuit!";
    private static final String DESCRIPTION_1 = "MyDesc";
    private static final String DESCRIPTION_2 = "A.N. Other Desc";

    private BasicCache cache;

    public void tearDown()
    {
        cache.flush();
        cache = null;
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        cache = CachingManagerFactory.getCache();
    }

    public void testAddOrReplace_Add()
    {


        SOAPPrincipal user = new SOAPPrincipal();
        user.setName(USER_NAME_1);

        cache.cacheUser(user);

        SOAPPrincipal retrievedUser = cache.getUser(USER_NAME_1);
        assertNotNull(retrievedUser);               
        assertEquals(user, retrievedUser);
    }

    public void testAddOrReplace_Replace()
    {


        SOAPPrincipal user1 = new SOAPPrincipal();
        user1.setName(USER_NAME_1);
        user1.setDescription(DESCRIPTION_1);
        cache.cacheUser(user1);

        SOAPPrincipal retrievedUser = cache.getUser(USER_NAME_1);
        assertNotNull(retrievedUser);
        assertEquals(user1, retrievedUser);
        assertEquals(DESCRIPTION_1, retrievedUser.getDescription());

        SOAPPrincipal user2 = new SOAPPrincipal();
        user2.setName(USER_NAME_1);
        user2.setDescription(DESCRIPTION_2);
        cache.cacheUser(user2);

        retrievedUser = cache.getUser(USER_NAME_1);
        assertNotNull(retrievedUser);
        assertEquals(user2, retrievedUser);
        assertEquals(DESCRIPTION_2, retrievedUser.getDescription());
    }

    public void testAddOrReplace_AddAnother()
    {


        SOAPPrincipal user1 = new SOAPPrincipal();
        user1.setName(USER_NAME_1);
        user1.setDescription(DESCRIPTION_1);
        cache.cacheUser(user1);

        SOAPPrincipal user2 = new SOAPPrincipal();
        user2.setName(USER_NAME_2);
        user2.setDescription(DESCRIPTION_2);
        cache.cacheUser(user2);

        SOAPPrincipal retrievedUser = cache.getUser(USER_NAME_1);
        assertNotNull(retrievedUser);
        assertEquals(user1, retrievedUser);
        assertEquals(DESCRIPTION_1, retrievedUser.getDescription());

        retrievedUser = cache.getUser(USER_NAME_2);
        assertNotNull(retrievedUser);
        assertEquals(user2, retrievedUser);
        assertEquals(DESCRIPTION_2, retrievedUser.getDescription());
    }

    public void testRemove_AddTwoRemoveOne()
    {


        SOAPPrincipal user1 = new SOAPPrincipal();
        user1.setName(USER_NAME_1);
        user1.setDescription(DESCRIPTION_1);
        cache.cacheUser(user1);

        SOAPPrincipal user2 = new SOAPPrincipal();
        user2.setName(USER_NAME_2);
        user2.setDescription(DESCRIPTION_2);
        cache.cacheUser(user2);

        SOAPPrincipal retrievedUser = cache.getUser(USER_NAME_1);
        assertNotNull(retrievedUser);
        assertEquals(user1, retrievedUser);
        assertEquals(DESCRIPTION_1, retrievedUser.getDescription());

        retrievedUser = cache.getUser(USER_NAME_2);
        assertNotNull(retrievedUser);
        assertEquals(user2, retrievedUser);
        assertEquals(DESCRIPTION_2, retrievedUser.getDescription());

        cache.removeUser(USER_NAME_2);

        retrievedUser = cache.getUser(USER_NAME_1);
        assertNotNull(retrievedUser);
        assertEquals(user1, retrievedUser);
        assertEquals(DESCRIPTION_1, retrievedUser.getDescription());

        assertNull(cache.getUser(USER_NAME_2));
    }

    public void testAddOrReplaceAllUserNames_Add()
    {


        List userNames = new ArrayList(3);
        userNames.add(USER_NAME_1);
        userNames.add(USER_NAME_2);
        userNames.add(USER_NAME_3);
        cache.cacheAllUserNames(userNames);

        List foundUserNames = cache.getAllUserNames();
        assertNotNull(foundUserNames);
        assertEquals("Should be three usernames in list", 3, foundUserNames.size());
        assertTrue(foundUserNames.contains(USER_NAME_1));
        assertTrue(foundUserNames.contains(USER_NAME_2));
        assertTrue(foundUserNames.contains(USER_NAME_3));
    }

    public void testAddOrReplaceAllUserNames_Replace()
    {


        List userNames = new ArrayList(3);
        userNames.add(USER_NAME_1);
        userNames.add(USER_NAME_2);
        userNames.add(USER_NAME_3);
        cache.cacheAllUserNames(userNames);

        List moreUserNames = new ArrayList(1);
        moreUserNames.add(USER_NAME_4);
        cache.cacheAllUserNames(moreUserNames);

        List foundUserNames = cache.getAllUserNames();
        assertNotNull(foundUserNames);
        assertEquals("Should be just one username in list", 1, foundUserNames.size());
        assertTrue(foundUserNames.contains(USER_NAME_4));
        assertFalse(foundUserNames.contains(USER_NAME_1));
        assertFalse(foundUserNames.contains(USER_NAME_2));
        assertFalse(foundUserNames.contains(USER_NAME_3));
    }


    public void testGetAllUserNames_Null()
    {

        List userNames = cache.getAllUserNames();
        assertNull(userNames);
    }
}
