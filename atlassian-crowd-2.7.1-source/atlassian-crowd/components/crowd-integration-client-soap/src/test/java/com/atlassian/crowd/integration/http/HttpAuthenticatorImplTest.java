package com.atlassian.crowd.integration.http;

import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.integration.soap.SOAPCookieInfo;
import com.atlassian.crowd.service.AuthenticationManager;
import com.atlassian.crowd.service.UserManager;
import com.atlassian.crowd.service.cache.CacheAwareAuthenticationManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;
import com.atlassian.crowd.testutils.CookieHeaderAwareMockHttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * HttpAuthenticatorImpl Tester.
 */
public class HttpAuthenticatorImplTest
{
    private HttpAuthenticator httpAuthenticator = null;
    private SoapClientProperties mockClientProperties = null;
    private SecurityServerClient mockSecurityServerClient = null;
    private UserManager mockUserManager = null;
    private AuthenticationManager authenticationManager = null;
    private SOAPCookieInfo soapCookieInfo = null;

    private static final String SESSION_LAST_VALIDATION = "session.lastvalidation";
    private static final String SESSION_TOKENKEY = "session.tokenkey";
    private static final String TOKEN_VALUE = "specialtoken";
    private static final String USER_AGENT_STRING = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US; rv:1.8.1.11) Gecko/20071127 Firefox/2.0.0.11";

    // Not a validation factor. Needed to check the browser is not included in validation.
    private static final String USER_AGENT = "User-Agent";

    @Before
    public void setUp() throws Exception
    {
        soapCookieInfo = new SOAPCookieInfo("localhost", false);
        mockUserManager = mock(UserManager.class);

        mockClientProperties = mock(SoapClientProperties.class);
        when(mockClientProperties.getApplicationName()).thenReturn("Crowd Test");
        when(mockClientProperties.getSessionValidationInterval()).thenReturn(1L);
        when(mockClientProperties.getSessionLastValidation()).thenReturn(SESSION_LAST_VALIDATION);
        when(mockClientProperties.getSessionTokenKey()).thenReturn(SESSION_TOKENKEY);
        when(mockClientProperties.getSecurityServerURL()).thenReturn("http://localhost:8095/crowd/services/");
        when(mockClientProperties.getCookieTokenKey()).thenReturn("somekey");
        when(mockClientProperties.getCookieTokenKey(Mockito.anyString())).thenReturn("somekey");

        mockSecurityServerClient = mock(SecurityServerClient.class);
        when(mockSecurityServerClient.getSoapClientProperties()).thenReturn(mockClientProperties);
        when(mockSecurityServerClient.getCookieInfo()).thenReturn(soapCookieInfo);

        authenticationManager = new CacheAwareAuthenticationManager(mockSecurityServerClient, mockUserManager);
        httpAuthenticator = new HttpAuthenticatorImpl(authenticationManager);
    }

    @After
    public void tearDown() throws Exception
    {
        httpAuthenticator = null;
        authenticationManager = null;
        mockSecurityServerClient = null;
        mockUserManager = null;
        mockClientProperties = null;
        soapCookieInfo = null;
    }

    @Test
    public void testIsAuthenticatedWithNoTokenPresent() throws Exception
    {
        final boolean authenticated = httpAuthenticator.isAuthenticated(new MockHttpServletRequest(), new CookieHeaderAwareMockHttpServletResponse());
        assertFalse(authenticated);
        verify(mockSecurityServerClient).getSoapClientProperties();
    }

    @Test
    public void testIsAuthenticatedWithinValidationInterval() throws Exception
    {
        // set the last validation time on the session
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        servletRequest.getSession().setAttribute(SESSION_LAST_VALIDATION, new Date());

        // set a token on the request
        servletRequest.setAttribute(Constants.COOKIE_TOKEN_KEY, TOKEN_VALUE);

        final boolean authenticated = httpAuthenticator.isAuthenticated(servletRequest, new CookieHeaderAwareMockHttpServletResponse());

        assertTrue(authenticated);
    }

    @Test
    public void testIsAuthenticatedOutsideValidationInterval() throws Exception
    {
        // set the last validation time on the session
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();
        servletRequest.getSession().setAttribute(SESSION_LAST_VALIDATION, new Date());

        // Force validation to occur
        when(mockClientProperties.getSessionValidationInterval()).thenReturn(0L);

        // set a token on the request
        servletRequest.setAttribute(Constants.COOKIE_TOKEN_KEY, TOKEN_VALUE);

        // Make sure that the mock request and validation factor are coming from the same place.
        servletRequest.setRemoteAddr("127.0.0.1");

        final ValidationFactor[] validationFactors = {new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, "127.0.0.1")};

        when(mockSecurityServerClient.isValidToken(TOKEN_VALUE, validationFactors)).thenReturn(true);

        final boolean authenticated = httpAuthenticator.isAuthenticated(servletRequest, new CookieHeaderAwareMockHttpServletResponse());

        assertTrue(authenticated);

        assertEquals(TOKEN_VALUE, servletRequest.getAttribute(Constants.COOKIE_TOKEN_KEY));
    }

    @Test
    public void testIsAuthenticatedWithInvalidToken() throws Exception
    {
        // Expectations of the SecurityServerClient
        MockHttpServletRequest servletRequest = new MockHttpServletRequest();

        // set a token on the request
        servletRequest.setAttribute(Constants.COOKIE_TOKEN_KEY, TOKEN_VALUE);

        // Make sure that the mock request and validation factor are coming from the same place.
        servletRequest.setRemoteAddr("127.0.0.1");

        final ValidationFactor[] validationFactors = {new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, "127.0.0.1")};

        when(mockSecurityServerClient.isValidToken(TOKEN_VALUE, validationFactors)).thenReturn(false);

        final boolean authenticated = httpAuthenticator.isAuthenticated(servletRequest, new CookieHeaderAwareMockHttpServletResponse());

        assertFalse(authenticated);
    }

    @Test
    public void testGetValidationFactors()
    {
        // Add validation factors
        MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.setRemoteAddr("127.0.0.1");
        httpServletRequest.addHeader(ValidationFactor.X_FORWARDED_FOR, "192.168.1.1");
        httpServletRequest.addHeader(USER_AGENT, USER_AGENT_STRING); // not used in validation factors

        final List<ValidationFactor> validationFactors = Arrays.asList(httpAuthenticator.getValidationFactors(httpServletRequest));

        assertEquals(2, validationFactors.size());
        assertTrue(validationFactors.contains(new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, "127.0.0.1")));
        assertTrue(validationFactors.contains(new ValidationFactor(ValidationFactor.X_FORWARDED_FOR, "192.168.1.1")));
    }

    @Test
    public void testSetPrincipalToken() throws Exception
    {
        final MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        final MockHttpServletResponse httpServletResponse = new CookieHeaderAwareMockHttpServletResponse();

        httpAuthenticator.setPrincipalToken(httpServletRequest, httpServletResponse, TOKEN_VALUE);

        assertSetPrincipalToken(httpServletRequest, httpServletResponse);
    }

    private void assertSetPrincipalToken(MockHttpServletRequest httpServletRequest, MockHttpServletResponse httpServletResponse)
    {
        // Assert request and session values
        final HttpSession session = httpServletRequest.getSession();

        final Object lastValidationTime = session.getAttribute(SESSION_LAST_VALIDATION);
        assertNotNull(lastValidationTime);
        assertTrue(lastValidationTime instanceof Date);

        assertEquals(TOKEN_VALUE, httpServletRequest.getAttribute(Constants.COOKIE_TOKEN_KEY));
        assertEquals(Boolean.TRUE, httpServletRequest.getAttribute(Constants.REQUEST_SSO_COOKIE_COMMITTED));

        // Assert response values
        final Cookie cookie = httpServletResponse.getCookie(mockClientProperties.getCookieTokenKey());
        assertEquals(TOKEN_VALUE, cookie.getValue());
    }

    @Test
    public void testGetTokenWhereTokenIsOnRequest() throws Exception
    {
        final MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.setAttribute(Constants.COOKIE_TOKEN_KEY, TOKEN_VALUE);

        String token = httpAuthenticator.getToken(httpServletRequest);

        assertNotNull(token);
        assertEquals(TOKEN_VALUE, token);
    }

    @Test
    public void testGetTokenWhereTokenIsInCookie() throws Exception
    {
        final MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();

        Cookie tokenCookie = new Cookie(mockClientProperties.getCookieTokenKey(), TOKEN_VALUE);
        httpServletRequest.setCookies(new Cookie[]{tokenCookie});

        Cookie someOtherCookie = new Cookie("RandomCookie", "RandomValue");
        httpServletRequest.setCookies(new Cookie[]{someOtherCookie, tokenCookie});

        String token = httpAuthenticator.getToken(httpServletRequest);

        assertNotNull(token);
        assertEquals(TOKEN_VALUE, token);
    }

    @Test
    public void testAuthenticate() throws Exception
    {
        final MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        final MockHttpServletResponse httpServletResponse = new CookieHeaderAwareMockHttpServletResponse();

        final UserAuthenticationContext authenticationContext = httpAuthenticator.getPrincipalAuthenticationContext(httpServletRequest, httpServletResponse, "admin", "password");

        when(mockSecurityServerClient.authenticatePrincipal(authenticationContext)).thenReturn(TOKEN_VALUE);

        httpAuthenticator.authenticate(httpServletRequest, httpServletResponse, "admin", "password");

        assertSetPrincipalToken(httpServletRequest, httpServletResponse);
        verify(mockUserManager, times(1)).getUser("admin");
    }

    @Test
    public void testAuthenticateWithBadCredentials() throws Exception
    {
        final MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.getSession().setAttribute(SESSION_TOKENKEY, TOKEN_VALUE);

        final MockHttpServletResponse httpServletResponse = new CookieHeaderAwareMockHttpServletResponse();

        final UserAuthenticationContext authenticationContext = httpAuthenticator.getPrincipalAuthenticationContext(httpServletRequest, httpServletResponse, "admin", "password");

        when(mockSecurityServerClient.authenticatePrincipal(authenticationContext)).thenThrow(InvalidAuthenticationException.newInstanceWithName("admin"));

        try
        {
            httpAuthenticator.authenticate(httpServletRequest, httpServletResponse, "admin", "password");
            fail("Bad credentials given. Should have thrown an InvalidAuthenticationException.");
        }
        catch (InvalidAuthenticationException e)
        {
            // we should be here
        }

        assertInvalidatedClient(httpServletRequest, httpServletResponse);
        verify(mockUserManager, never()).getUser("admin");
    }

    @Test
    public void testAuthenticateWithSecureCookie() throws Exception
    {
        final MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        final MockHttpServletResponse httpServletResponse = new CookieHeaderAwareMockHttpServletResponse();

        when(mockSecurityServerClient.getCookieInfo()).thenReturn(new SOAPCookieInfo("localhost", true));

        final UserAuthenticationContext authenticationContext = httpAuthenticator.getPrincipalAuthenticationContext(httpServletRequest, httpServletResponse, "admin", "password");

        when(mockSecurityServerClient.authenticatePrincipal(authenticationContext)).thenReturn(TOKEN_VALUE);

        httpAuthenticator.authenticate(httpServletRequest, httpServletResponse, "admin", "password");

        // Assert that the "Secure" flag is set on the cookie
        final Cookie cookie = httpServletResponse.getCookie(mockClientProperties.getCookieTokenKey());
        assertEquals("Secure flag should be set", true, cookie.getSecure());
        verify(mockUserManager, times(1)).getUser("admin");
    }

    @Test
    public void testAuthenticateWithInSecureCookie() throws Exception
    {
        final MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        final MockHttpServletResponse httpServletResponse = new CookieHeaderAwareMockHttpServletResponse();

        final UserAuthenticationContext authenticationContext = httpAuthenticator.getPrincipalAuthenticationContext(httpServletRequest, httpServletResponse, "admin", "password");

        when(mockSecurityServerClient.authenticatePrincipal(authenticationContext)).thenReturn(TOKEN_VALUE);

        httpAuthenticator.authenticate(httpServletRequest, httpServletResponse, "admin", "password");

        // Assert that the "Secure" flag is NOT set on the cookie
        final Cookie cookie = httpServletResponse.getCookie(mockClientProperties.getCookieTokenKey());
        assertEquals("Secure flag should not be set", false, cookie.getSecure());
        verify(mockUserManager, times(1)).getUser("admin");
    }

    private void assertInvalidatedClient(MockHttpServletRequest httpServletRequest, MockHttpServletResponse httpServletResponse)
    {
        // Assert request and session values
        final HttpSession session = httpServletRequest.getSession();

        assertNull(session.getAttribute(SESSION_TOKENKEY));

        assertNull(httpServletRequest.getAttribute(mockClientProperties.getCookieTokenKey()));

        // Assert response values
        final Cookie cookie = httpServletResponse.getCookie(mockClientProperties.getCookieTokenKey());
        assertTrue(StringUtils.isEmpty(cookie.getValue()));
        assertEquals(0, cookie.getMaxAge());
    }

    @Test
    public void testLogOff() throws Exception
    {
        final MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.getSession().setAttribute(SESSION_TOKENKEY, TOKEN_VALUE);

        final MockHttpServletResponse httpServletResponse = new CookieHeaderAwareMockHttpServletResponse();

        httpAuthenticator.logoff(httpServletRequest, httpServletResponse);

        assertInvalidatedClient(httpServletRequest, httpServletResponse);
    }

    @Test
    public void setPrincipalTokenCookieConfigurationUsesDefaultNameWhenNotSpecifiedInClientProperties() throws Exception
    {
        when(mockClientProperties.getCookieTokenKey(null)).thenReturn(null);

        final MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        final MockHttpServletResponse httpServletResponse = new CookieHeaderAwareMockHttpServletResponse();

        httpAuthenticator.setPrincipalToken(httpServletRequest, httpServletResponse, TOKEN_VALUE);

        assertSetPrincipalToken(httpServletRequest, httpServletResponse);
    }

    @Test
    public void testLogOffCookieConfigurationUsesDefaultNameWhenNotSpecifiedInClientProperties() throws Exception
    {
        when(mockClientProperties.getCookieTokenKey(null)).thenReturn(null);

        final MockHttpServletRequest httpServletRequest = new MockHttpServletRequest();
        httpServletRequest.getSession().setAttribute(SESSION_TOKENKEY, TOKEN_VALUE);

        final MockHttpServletResponse httpServletResponse = new CookieHeaderAwareMockHttpServletResponse();

        httpAuthenticator.logoff(httpServletRequest, httpServletResponse);

        assertInvalidatedClient(httpServletRequest, httpServletResponse);
    }
}
