package com.atlassian.crowd.service.cache;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SOAPNestableGroup;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.service.GroupManager;
import com.atlassian.crowd.service.GroupMembershipManager;
import com.atlassian.crowd.service.UserManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CachingGroupMembershipManagerTest
{
    private static final String USER_NAME_1 = "el-useriNo";
    private static final String USER_NAME_2 = "el-useriNa";
    private static final String USER_NAME_3 = "el-useriNx";

    private static final String GROUP_NAME_1 = "me-gustas-tu";
    private static final String GROUP_NAME_2 = "promiscuity";
    private static final String GROUP_NAME_3 = "Bobby Marley Homies";
    private static final String GROUP_NAME_4 = "group4";

    private GroupManager groupManager;
    private GroupMembershipManager groupMembershipManager;
    private UserManager userManager;
    private SecurityServerClient securityServerClient;

    @After
    public void tearDown()
    {
        CachingManagerFactory.getCache().flush();
        groupManager = null;
        groupMembershipManager = null;
    }

    @Before
    public void setUp() throws Exception
    {
        securityServerClient = mock(SecurityServerClient.class);

        userManager = new CachingUserManager(securityServerClient, CachingManagerFactory.getCache());
        groupManager = new CachingGroupManager(securityServerClient, CachingManagerFactory.getCache());
        groupMembershipManager = new CachingGroupMembershipManager(securityServerClient, userManager, groupManager, CachingManagerFactory.getCache());
    }

    @Test
    public void testIsMember_NotAMember() throws Exception
    {
        when(securityServerClient.isGroupMember(GROUP_NAME_1, USER_NAME_1)).thenReturn(false);

        assertFalse("USER_NAME_1 should not be a member of GROUP_NAME_1", groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_1));
        verify(securityServerClient).isGroupMember(GROUP_NAME_1, USER_NAME_1);

        // check that the negative relationship is now in the cache.
        assertFalse("USER_NAME_1 should be cached as not being a member of GROUP_NAME_1", groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_1));
        verify(securityServerClient).isGroupMember(GROUP_NAME_1, USER_NAME_1);
    }

    @Test
    public void testIsMember_IsMember() throws Exception
    {
        when(securityServerClient.isGroupMember(GROUP_NAME_1, USER_NAME_1)).thenReturn(true);

        assertTrue("USER_NAME_1 should be a member of GROUP_NAME_1", groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_1));
        verify(securityServerClient).isGroupMember(GROUP_NAME_1, USER_NAME_1);

        // check that the negative relationship is now in the cache.
        assertTrue("USER_NAME_1 should be cached as not a member of GROUP_NAME_1", groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_1));
        verify(securityServerClient).isGroupMember(GROUP_NAME_1, USER_NAME_1);
    }

    @Test
    public void testIsMember_UseGroupCache_InGroup() throws Exception
    {
        // put group->user information into the cache.
        SOAPGroup group = new SOAPGroup();
        group.setName(GROUP_NAME_1);
        group.setMembers(new String[]{USER_NAME_1, USER_NAME_3});

        when(securityServerClient.findGroupByName(GROUP_NAME_1)).thenReturn(group);

        // force a call to findGroupByName() and load the group members into the cache.
        groupManager.getGroup(GROUP_NAME_1);

        assertTrue(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_1));
        verify(securityServerClient).findGroupByName(GROUP_NAME_1);
    }

    @Test
    public void testIsMember_UseGroupCache_NotInGroup() throws Exception
    {
        // put group->user information into the cache.
        SOAPGroup group = new SOAPGroup();
        group.setName(GROUP_NAME_1);
        group.setMembers(new String[]{USER_NAME_1, USER_NAME_3});

        when(securityServerClient.findGroupByName(GROUP_NAME_1)).thenReturn(group);

        // load the group & its members into the cache
        groupManager.getGroup(GROUP_NAME_1);

        assertFalse(groupMembershipManager.isMember(USER_NAME_2, GROUP_NAME_1));
        verify(securityServerClient).findGroupByName(GROUP_NAME_1);
    }

    @Test
    public void testIsMember_UseGroupCache_GroupNotInCache() throws Exception
    {
        SOAPGroup group = new SOAPGroup();
        group.setName(GROUP_NAME_1);
        group.setMembers(new String[]{USER_NAME_1, USER_NAME_3});

        when(securityServerClient.addGroup(group)).thenReturn(group);
        when(securityServerClient.isGroupMember(GROUP_NAME_2, USER_NAME_1)).thenReturn(true);

        // put group->user information into the cache.

        groupManager.addGroup(group);

        assertTrue(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_2));

        verify(securityServerClient).addGroup(group);
        verify(securityServerClient).isGroupMember(GROUP_NAME_2, USER_NAME_1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddMembership_NullUser() throws Exception
    {
        groupMembershipManager.addMembership(null, "neep");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAddMembership_NullGroup() throws Exception
    {
        groupMembershipManager.addMembership("nark", null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testAddMembership_NonExistentUser() throws Exception
    {
        Mockito.doThrow(new UserNotFoundException(USER_NAME_1)).when(
                securityServerClient).addPrincipalToGroup(USER_NAME_1, GROUP_NAME_1);

        groupMembershipManager.addMembership(USER_NAME_1, GROUP_NAME_1);
    }

    @Test(expected = GroupNotFoundException.class)
    public void testAddMembership_NonExistentGroup() throws Exception
    {
        Mockito.doThrow(new GroupNotFoundException(GROUP_NAME_1)).when(
                securityServerClient).addPrincipalToGroup(USER_NAME_1, GROUP_NAME_1);

        groupMembershipManager.addMembership(USER_NAME_1, GROUP_NAME_1);
    }

    @Test
    public void testAddMembership_AlreadyAMember() throws Exception
    {
        // doesn't throw an error - silently succeeds. Same behaviour as if the user's not a member of the group

//        when(securityServerClient.addPrincipalToGroup(, group)
        SOAPGroup group = new SOAPGroup();
        group.setName(GROUP_NAME_3);
        group.setMembers(new String[]{USER_NAME_1});

        SOAPNestableGroup[] groups = new SOAPNestableGroup[4];
        groups[0] = new SOAPNestableGroup();
        groups[0].setName(GROUP_NAME_1);
        groups[1] = new SOAPNestableGroup();
        groups[1].setName(GROUP_NAME_2);
        groups[2] = new SOAPNestableGroup();
        groups[2].setName(GROUP_NAME_3);
        groups[3] = new SOAPNestableGroup();
        groups[3].setName(GROUP_NAME_4);

        when(securityServerClient.findGroupMemberships(USER_NAME_1)).thenReturn(new String[]{GROUP_NAME_1, GROUP_NAME_2, GROUP_NAME_3});
        when(securityServerClient.findAllGroupRelationships()).thenReturn(groups);
        when(securityServerClient.findGroupByName(GROUP_NAME_3)).thenReturn(new SOAPGroup(GROUP_NAME_3, null)); // executed to prime cache

        when(securityServerClient.findPrincipalByName(toLowerCase(USER_NAME_1))).thenReturn(new SOAPPrincipal(USER_NAME_1)); // needed to map user case

        groupMembershipManager.addMembership(USER_NAME_1, GROUP_NAME_3);

        // check membership is now in cache
        assertTrue(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_3));


        groupMembershipManager.addMembership(USER_NAME_1, GROUP_NAME_3);

        // check membership is still in cache
        assertTrue(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_3));

        // further tests
        assertTrue(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_3));

        List<String> memberships = groupMembershipManager.getMemberships(USER_NAME_1);
        assertEquals(3, memberships.size());
        assertTrue(memberships.contains(GROUP_NAME_1));
        assertTrue(memberships.contains(GROUP_NAME_2));
        assertTrue(memberships.contains(GROUP_NAME_3));

        assertTrue(groupMembershipManager.getMembers(GROUP_NAME_3).contains(USER_NAME_1));

        verify(securityServerClient, Mockito.times(2)).addPrincipalToGroup(USER_NAME_1, GROUP_NAME_3);

        verify(securityServerClient).findGroupMemberships(USER_NAME_1);
        verify(securityServerClient).findAllGroupRelationships();
        verify(securityServerClient).findGroupByName(GROUP_NAME_3);
        verify(securityServerClient).findPrincipalByName(toLowerCase(USER_NAME_1));
    }

    @Test
    public void testAddMembership_CacheNotPopulated() throws Exception
    {
        // user 1 is a member of group1 and group2, and is added to group 3

        SOAPGroup group = new SOAPGroup();
        group.setName(GROUP_NAME_3);
        group.setMembers(new String[]{USER_NAME_1});

        SOAPNestableGroup[] groups = new SOAPNestableGroup[4];
        groups[0] = new SOAPNestableGroup();
        groups[0].setName(GROUP_NAME_1);
        groups[1] = new SOAPNestableGroup();
        groups[1].setName(GROUP_NAME_2);
        groups[2] = new SOAPNestableGroup();
        groups[2].setName(GROUP_NAME_3);
        groups[3] = new SOAPNestableGroup();
        groups[3].setName(GROUP_NAME_4);

        when(securityServerClient.findGroupMemberships(USER_NAME_1)).thenReturn(new String[]{GROUP_NAME_1, GROUP_NAME_2, GROUP_NAME_3});
        when(securityServerClient.findAllGroupRelationships()).thenReturn(groups);
        when(securityServerClient.findGroupByName(GROUP_NAME_3)).thenReturn(new SOAPGroup(GROUP_NAME_3, null));

        when(securityServerClient.findPrincipalByName(toLowerCase(USER_NAME_1))).thenReturn(new SOAPPrincipal(USER_NAME_1));

        groupMembershipManager.addMembership(USER_NAME_1, GROUP_NAME_3);

        // the following have been commented out because the manager only checks the group cache for memberhips (not the user cache)
        //assertTrue(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_1));
        //assertTrue(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_2));
        assertTrue(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_3));

        List<String> memberships = groupMembershipManager.getMemberships(USER_NAME_1);
        assertEquals(3, memberships.size());
        assertTrue(memberships.contains(GROUP_NAME_1));
        assertTrue(memberships.contains(GROUP_NAME_2));
        assertTrue(memberships.contains(GROUP_NAME_3));

        assertTrue(groupMembershipManager.getMembers(GROUP_NAME_3).contains(USER_NAME_1));

        verify(securityServerClient).addPrincipalToGroup(USER_NAME_1, GROUP_NAME_3);
    }

    @Test
    public void testAddMembership_CachePopulated() throws Exception
    {
        // user 1 is a member of group1 and group2, and is added to group 3


        SOAPGroup group = new SOAPGroup();
        group.setName(GROUP_NAME_3);

        CachingManagerFactory.getCache().cacheAllGroupNames(Arrays.asList(GROUP_NAME_1, GROUP_NAME_2, GROUP_NAME_3, GROUP_NAME_4));
        CachingManagerFactory.getCache().cacheGroup(group);
        CachingManagerFactory.getCache().cacheAllMembers(GROUP_NAME_1, Arrays.asList(USER_NAME_1));
        CachingManagerFactory.getCache().cacheAllMembers(GROUP_NAME_2, Arrays.asList(USER_NAME_1));
        CachingManagerFactory.getCache().cacheAllMembers(GROUP_NAME_3, new ArrayList<String>());
        List<String> mutableList = new ArrayList<String>();
        mutableList.add(GROUP_NAME_1);
        mutableList.add(GROUP_NAME_2);
        mutableList.add(GROUP_NAME_3);
        CachingManagerFactory.getCache().cacheAllMemberships(USER_NAME_1, mutableList);

        when(securityServerClient.findPrincipalByName(toLowerCase(USER_NAME_1))).thenReturn(new SOAPPrincipal(USER_NAME_1));

        groupMembershipManager.addMembership(USER_NAME_1, GROUP_NAME_3);

        // the following have been commented out because the manager only checks the group cache for memberships (not the user cache)
        //assertTrue(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_1));
        //assertTrue(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_2));
        assertTrue(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_3));

        List<String> memberships = groupMembershipManager.getMemberships(USER_NAME_1);
        assertEquals(3, memberships.size());
        assertTrue(memberships.contains(GROUP_NAME_1));
        assertTrue(memberships.contains(GROUP_NAME_2));
        assertTrue(memberships.contains(GROUP_NAME_3));

        assertTrue(groupMembershipManager.getMembers(GROUP_NAME_3).contains(USER_NAME_1));

        verify(securityServerClient).addPrincipalToGroup(USER_NAME_1, GROUP_NAME_3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveMembership_NullUser() throws Exception
    {
        groupMembershipManager.removeMembership(null, "neep");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRemoveMembership_NullGroup() throws Exception
    {
        groupMembershipManager.removeMembership("nark", null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testRemoveMembership_NonExistentUser() throws Exception
    {
        Mockito.doThrow(new UserNotFoundException(USER_NAME_1)).when(
                securityServerClient).removePrincipalFromGroup(USER_NAME_1, GROUP_NAME_1);

        groupMembershipManager.removeMembership(USER_NAME_1, GROUP_NAME_1);
    }

    @Test(expected = GroupNotFoundException.class)
    public void testRemoveMembership_NonExistentGroup() throws Exception
    {
        Mockito.doThrow(new GroupNotFoundException(GROUP_NAME_1)).when(
                securityServerClient).removePrincipalFromGroup(USER_NAME_1, GROUP_NAME_1);

        groupMembershipManager.removeMembership(USER_NAME_1, GROUP_NAME_1);
    }

    @Test
    public void testRemoveMembership() throws Exception
    {
        groupMembershipManager.removeMembership(USER_NAME_1, GROUP_NAME_1);

        // check membership is now in cache - securityServerClient will assert() if it's called again.
        assertFalse(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_1));

        verify(securityServerClient).removePrincipalFromGroup(USER_NAME_1, GROUP_NAME_1);
    }

    /**
     * Checks that getMembers(groupName) and getMemberships(userName) will no longer return the membership.
     *
     * @throws Exception
     */
    @Test
    public void testRemoveMemberships_CachesUpdated() throws Exception
    {
        // just preload the users to get the case stuff in there
        when(securityServerClient.findPrincipalByName(USER_NAME_1)).thenReturn(new SOAPPrincipal(USER_NAME_1));
        when(securityServerClient.findPrincipalByName(USER_NAME_2)).thenReturn(new SOAPPrincipal(USER_NAME_2));
        when(securityServerClient.findPrincipalByName(USER_NAME_3)).thenReturn(new SOAPPrincipal(USER_NAME_3));
        userManager.getUser(USER_NAME_1);
        userManager.getUser(USER_NAME_2);
        userManager.getUser(USER_NAME_3);

        SOAPGroup group = new SOAPGroup();
        group.setName(GROUP_NAME_1);
        group.setMembers(new String[]{USER_NAME_1, USER_NAME_2});

        SOAPNestableGroup[] groups = new SOAPNestableGroup[2];
        groups[0] = new SOAPNestableGroup();
        groups[0].setName(GROUP_NAME_1);
        groups[1] = new SOAPNestableGroup();
        groups[1].setName(GROUP_NAME_2);

        when(securityServerClient.findAllGroupRelationships()).thenReturn(groups);
        when(securityServerClient.findGroupByName(GROUP_NAME_1)).thenReturn(group);
        when(securityServerClient.findGroupMemberships(USER_NAME_1)).thenReturn(new String[]{GROUP_NAME_1, GROUP_NAME_2});

        // load group->users into cache
        List/*<String>*/ allUsers = groupMembershipManager.getMembers(GROUP_NAME_1);
        assertNotNull(allUsers);
        assertEquals("Should be two members before remove", 2, allUsers.size());
        assertTrue(allUsers.contains(USER_NAME_1));
        assertTrue(allUsers.contains(USER_NAME_2));
        assertFalse(allUsers.contains(USER_NAME_3));

        // load user->groups into cache
        List/*<String>*/ allGroups = groupMembershipManager.getMemberships(USER_NAME_1);
        assertNotNull(allGroups);
        assertEquals("Should be two members before remove", 2, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_1));
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertFalse(allGroups.contains(GROUP_NAME_3));

        groupMembershipManager.removeMembership(USER_NAME_1, GROUP_NAME_1);

        // check explicit membership is now in cache - securityServerClient will assert() if it's called again.
        assertFalse(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_1));

        // check group->users from cache
        allUsers = groupMembershipManager.getMembers(GROUP_NAME_1);
        assertNotNull(allUsers);
        assertEquals("Should be one member after remove", 1, allUsers.size());
        assertTrue(allUsers.contains(USER_NAME_2));
        assertFalse(allUsers.contains(USER_NAME_1));
        assertFalse(allUsers.contains(USER_NAME_3));

        // check user->groups from cache
        allGroups = groupMembershipManager.getMemberships(USER_NAME_1);
        assertNotNull(allGroups);
        assertEquals("Should be one member after remove", 1, allGroups.size());
        assertTrue(allGroups.contains(GROUP_NAME_2));
        assertFalse(allGroups.contains(GROUP_NAME_3));
        assertFalse(allGroups.contains(GROUP_NAME_1));

        verify(securityServerClient).findAllGroupRelationships();
        verify(securityServerClient).removePrincipalFromGroup(USER_NAME_1, GROUP_NAME_1);
        verify(securityServerClient).findGroupByName(GROUP_NAME_1);
        verify(securityServerClient).findGroupMemberships(USER_NAME_1);
    }

    @Test
    public void testRemoveMembership_NotAMember() throws Exception
    {
        // doesn't throw an error - silently succeeds. Same behaviour as if the user's a member of the group

        groupMembershipManager.removeMembership(USER_NAME_1, GROUP_NAME_1);

        // check membership is now in cache
        assertFalse(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_1));
        verify(securityServerClient, Mockito.never()).isGroupMember(GROUP_NAME_1, USER_NAME_1);


        groupMembershipManager.removeMembership(USER_NAME_1, GROUP_NAME_1);

        // check membership is still in cache
        assertFalse(groupMembershipManager.isMember(USER_NAME_1, GROUP_NAME_1));
        verify(securityServerClient, Mockito.never()).isGroupMember(GROUP_NAME_1, USER_NAME_1);

        verify(securityServerClient, Mockito.times(2)).removePrincipalFromGroup(USER_NAME_1, GROUP_NAME_1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMemberships_Null() throws Exception
    {
        groupMembershipManager.getMemberships(null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testGetMemberships_NonExistentUser() throws Exception
    {
        when(securityServerClient.findGroupMemberships(USER_NAME_1)).thenThrow(new UserNotFoundException(USER_NAME_1));

        groupMembershipManager.getMemberships(USER_NAME_1);
    }

    @Test
    public void testGetMemberships_NoMemberships() throws Exception
    {
        when(securityServerClient.findGroupMemberships(USER_NAME_1)).thenReturn(new String[]{});

        assertTrue(groupMembershipManager.getMemberships(USER_NAME_1).isEmpty());
    }

    @Test
    public void testGetMemberships_NoMemberships_InCache() throws Exception
    {
        when(securityServerClient.findGroupMemberships(USER_NAME_1)).thenReturn(new String[]{});

        assertTrue(groupMembershipManager.getMemberships(USER_NAME_1).isEmpty());
        // check we don't hit the server on every request
        assertTrue(groupMembershipManager.getMemberships(USER_NAME_1).isEmpty());

        verify(securityServerClient).findGroupMemberships(USER_NAME_1);
    }

    @Test
    public void testGetMemberships_NotInCache() throws Exception
    {
        when(securityServerClient.findGroupMemberships(USER_NAME_1)).thenReturn(new String[]{GROUP_NAME_1, GROUP_NAME_2});
        when(securityServerClient.findAllGroupRelationships()).thenReturn(new SOAPNestableGroup[]{});

        when(securityServerClient.findGroupByName(GROUP_NAME_1)).thenReturn(new SOAPGroup(GROUP_NAME_1, null));
        when(securityServerClient.findGroupByName(GROUP_NAME_2)).thenReturn(new SOAPGroup(GROUP_NAME_2, null));

        List/*<String>*/ groupNames = groupMembershipManager.getMemberships(USER_NAME_1);
        assertNotNull(groupNames);
        assertEquals("Should be two groups for USER_NAME_1", 2, groupNames.size());
        assertTrue(groupNames.contains(GROUP_NAME_1));
        assertTrue(groupNames.contains(GROUP_NAME_2));
        assertFalse(groupNames.contains(GROUP_NAME_3));
    }

    @Test
    public void testGetMemberships_InCache() throws Exception
    {
        when(securityServerClient.findGroupMemberships(USER_NAME_1)).thenReturn(new String[]{GROUP_NAME_1, GROUP_NAME_2});
        when(securityServerClient.findAllGroupRelationships()).thenReturn(new SOAPNestableGroup[]{});

        when(securityServerClient.findGroupByName(GROUP_NAME_1)).thenReturn(new SOAPGroup(GROUP_NAME_1, null));
        when(securityServerClient.findGroupByName(GROUP_NAME_2)).thenReturn(new SOAPGroup(GROUP_NAME_2, null));

        List/*<String>*/ groupNames = groupMembershipManager.getMemberships(USER_NAME_1);
        assertNotNull(groupNames);
        assertEquals("Should be two groups for USER_NAME_1", 2, groupNames.size());
        assertTrue(groupNames.contains(GROUP_NAME_1));
        assertTrue(groupNames.contains(GROUP_NAME_2));
        assertFalse(groupNames.contains(GROUP_NAME_3));

        // check the memberships are now in the cache
        List/*<String>*/ moreGroupNames = groupMembershipManager.getMemberships(USER_NAME_1);
        assertNotNull(moreGroupNames);
        assertEquals("Should be two groups for USER_NAME_1", 2, moreGroupNames.size());
        assertTrue(moreGroupNames.contains(GROUP_NAME_1));
        assertTrue(moreGroupNames.contains(GROUP_NAME_2));
        assertFalse(moreGroupNames.contains(GROUP_NAME_3));

        verify(securityServerClient).findGroupMemberships(USER_NAME_1);
        verify(securityServerClient).findAllGroupRelationships();
        verify(securityServerClient).findGroupByName(GROUP_NAME_1);
        verify(securityServerClient).findGroupByName(GROUP_NAME_2);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGetMembers_Null() throws Exception
    {
        groupMembershipManager.getMembers(null);
    }

    @Test(expected = GroupNotFoundException.class)
    public void testGetMembers_NonExistentGroup() throws Exception
    {
        when(securityServerClient.findGroupByName(GROUP_NAME_1)).thenThrow(new GroupNotFoundException(GROUP_NAME_1));

        groupMembershipManager.getMembers(GROUP_NAME_1);
    }

    @Test
    public void testGetMembers_NoMembers_NotInCache() throws Exception
    {
        SOAPGroup group = new SOAPGroup();
        group.setName(GROUP_NAME_1);
        when(securityServerClient.findGroupByName(GROUP_NAME_1)).thenReturn(group);


        assertTrue(groupMembershipManager.getMembers(GROUP_NAME_1).isEmpty());
    }

    @Test
    public void testGetMembers_NoMembers_InCache() throws Exception
    {
        SOAPGroup group = new SOAPGroup();
        group.setName(GROUP_NAME_1);
        when(securityServerClient.findGroupByName(GROUP_NAME_1)).thenReturn(group);

        assertTrue(groupMembershipManager.getMembers(GROUP_NAME_1).isEmpty());
        verify(securityServerClient).findGroupByName(GROUP_NAME_1);

        // second call should not hit server - securityServerClient will assert if it does.
        assertTrue(groupMembershipManager.getMembers(GROUP_NAME_1).isEmpty());
        verify(securityServerClient).findGroupByName(GROUP_NAME_1);
    }

    @Test
    public void testGetMembers_Members_NotInCache() throws Exception
    {

        SOAPGroup group = new SOAPGroup();
        group.setName(GROUP_NAME_1);
        group.setMembers(new String[]{USER_NAME_1, USER_NAME_3});
        when(securityServerClient.findGroupByName(GROUP_NAME_1)).thenReturn(group);
        when(securityServerClient.findPrincipalByName(USER_NAME_1)).thenReturn(new SOAPPrincipal(USER_NAME_1));
        when(securityServerClient.findPrincipalByName(USER_NAME_3)).thenReturn(new SOAPPrincipal(USER_NAME_3));

        List/*<String>*/ users = groupMembershipManager.getMembers(GROUP_NAME_1);
        assertNotNull(users);
        assertEquals("Group should have 2 members", 2, users.size());
        assertTrue(users.contains(USER_NAME_1));
        assertTrue(users.contains(USER_NAME_3));
        assertFalse(users.contains(USER_NAME_2));
    }

    @Test
    public void testGetMembers_Members_InCache() throws Exception
    {
        SOAPGroup group = new SOAPGroup();
        group.setName(GROUP_NAME_1);
        group.setMembers(new String[]{USER_NAME_1, USER_NAME_3});
        when(securityServerClient.findGroupByName(GROUP_NAME_1)).thenReturn(group);
        when(securityServerClient.findPrincipalByName(USER_NAME_1)).thenReturn(new SOAPPrincipal(USER_NAME_1));
        when(securityServerClient.findPrincipalByName(USER_NAME_3)).thenReturn(new SOAPPrincipal(USER_NAME_3));

        List/*<String>*/ users = groupMembershipManager.getMembers(GROUP_NAME_1);
        assertNotNull(users);
        assertEquals("Group should have 2 members", 2, users.size());
        assertTrue(users.contains(USER_NAME_1));
        assertTrue(users.contains(USER_NAME_3));
        assertFalse(users.contains(USER_NAME_2));

        // second call should not hit server - securityServerClient will assert if it does.
        users = groupMembershipManager.getMembers(GROUP_NAME_1);
        assertNotNull(users);
        assertEquals("Group should have 2 members", 2, users.size());
        assertTrue(users.contains(USER_NAME_1));
        assertTrue(users.contains(USER_NAME_3));
        assertFalse(users.contains(USER_NAME_2));

        verify(securityServerClient).findGroupByName(GROUP_NAME_1);
        verify(securityServerClient).findPrincipalByName(USER_NAME_1);
        verify(securityServerClient).findPrincipalByName(USER_NAME_3);
    }
}
