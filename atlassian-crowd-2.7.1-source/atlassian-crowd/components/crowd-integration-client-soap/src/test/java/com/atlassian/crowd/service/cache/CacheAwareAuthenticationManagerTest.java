package com.atlassian.crowd.service.cache;

import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.AuthenticationManager;
import com.atlassian.crowd.service.UserManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Some very basic sanity tests for the interface. As initially written it's a passthrough, so this is just to make sure
 * there's nothing obviously wrong.
 */
@RunWith(MockitoJUnitRunner.class)
public class CacheAwareAuthenticationManagerTest
{
    @Mock
    private SecurityServerClient mockSecurityServerClient = null;
    @Mock
    private UserManager mockUserManager = null;
    @Mock
    private SoapClientProperties mockCP = null;

    private static final String TEST_TOKEN = "ThisIsMYToken!";

    @Test(expected = IllegalArgumentException.class)
    public void testAuthenticate_PAC_Null() throws Exception
    {
        AuthenticationManager am = new CacheAwareAuthenticationManager(null, null);
        am.authenticate(null);
    }

    @Test
    public void testAuthenticate_PAC() throws Exception
    {
        when(mockCP.getApplicationName()).thenReturn("jira");
        when(mockSecurityServerClient.authenticatePrincipal(Mockito.<UserAuthenticationContext>any())).thenReturn(TEST_TOKEN);
        when(mockSecurityServerClient.getSoapClientProperties()).thenReturn(mockCP);

        AuthenticationManager am = new CacheAwareAuthenticationManager(mockSecurityServerClient, mockUserManager);
        UserAuthenticationContext pac = new UserAuthenticationContext();

        String token = am.authenticate(pac);
        assertEquals(TEST_TOKEN, token);

        verify(mockUserManager).getUser(Mockito.anyString());
        verify(mockSecurityServerClient).getSoapClientProperties();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAuthenticate_UP_Null() throws Exception
    {
        AuthenticationManager am = new CacheAwareAuthenticationManager(null, null);
        am.authenticate(null, null);
    }

    @Test
    public void testAuthenticate_UP() throws Exception
    {
        when(mockSecurityServerClient.authenticatePrincipalSimple(Mockito.anyString(), Mockito.anyString())).thenReturn(TEST_TOKEN);

        AuthenticationManager am = new CacheAwareAuthenticationManager(mockSecurityServerClient, mockUserManager);

        String token = am.authenticate("username", "password");
        assertEquals(TEST_TOKEN, token);

        verify(mockUserManager).getUser(Mockito.anyString());
    }

    @Test
    public void testIsAuthenticated_True() throws Exception
    {
        when(mockSecurityServerClient.isValidToken(Mockito.anyString(), Mockito.<ValidationFactor[]>any())).thenReturn(true);
        AuthenticationManager am = new CacheAwareAuthenticationManager(mockSecurityServerClient, mockUserManager);

        ValidationFactor[] validationFactors = new ValidationFactor[0];
        assertTrue(am.isAuthenticated(TEST_TOKEN, validationFactors));
    }

    @Test
    public void testIsAuthenticated_False() throws Exception
    {
        when(mockSecurityServerClient.isValidToken(Mockito.anyString(), Mockito.<ValidationFactor[]>any())).thenReturn(false);
        AuthenticationManager am = new CacheAwareAuthenticationManager(mockSecurityServerClient, mockUserManager);

        ValidationFactor[] validationFactors = new ValidationFactor[0];
        assertFalse(am.isAuthenticated(TEST_TOKEN, validationFactors));
    }
}
