package com.atlassian.crowd.console.action.options;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.opensymphony.webwork.ServletActionContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class UpdateGeneralTest
{
    public static final String LOCALHOST_URL = "http://localhost:8095";

    @Mock
    private PropertyManager propertyManager;

    @Mock
    private HttpServletRequest httpServletRequest;

    @InjectMocks
    private UpdateGeneral updateGeneral;

    @Before
    public void initStatics()
    {
        ServletActionContext.setRequest(httpServletRequest);
    }

    @After
    public void releaseStatics()
    {
        ServletActionContext.setRequest(null);
    }

    @Test
    public void shouldLoadExistingConfiguration() throws Exception
    {
        when(propertyManager.getDeploymentTitle()).thenReturn("My Crowd");
        when(propertyManager.getDomain()).thenReturn("example.test");
        when(propertyManager.isSecureCookie()).thenReturn(true);
        when(propertyManager.isCacheEnabled()).thenReturn(true);
        when(propertyManager.isGzipEnabled()).thenReturn(true);

        assertThat(updateGeneral.doDefault(), is(UpdateGeneral.INPUT));

        assertThat(updateGeneral.getTitle(), is("My Crowd"));
        assertThat(updateGeneral.getDomain(), is("example.test"));
        assertThat(updateGeneral.isSecureCookie(), is(true));
        assertThat(updateGeneral.isCachingEnabled(), is(true));
        assertThat(updateGeneral.isGzip(), is(true));
    }

    @Test
    public void shouldSetSecureSSOCookieWhenAccessedOverSecureChannel() throws Exception
    {
        updateGeneral.setTitle("title");
        updateGeneral.setSecureCookie(true);
        updateGeneral.setDomain(LOCALHOST_URL);

        when(httpServletRequest.getServerName()).thenReturn(LOCALHOST_URL);
        when(httpServletRequest.isSecure()).thenReturn(true);

        updateGeneral.doUpdate();

        assertThat(updateGeneral.isDisableSSOSecureCookie(), is(false));
        verify(propertyManager).setSecureCookie(true);
    }

    @Test
    public void shouldNotSetSecureSSOCookieOptionWhenAccessedOverSecureChannel()
    {
        updateGeneral.setTitle("title");
        updateGeneral.setSecureCookie(true);
        updateGeneral.setDomain(LOCALHOST_URL);

        when(httpServletRequest.getServerName()).thenReturn(LOCALHOST_URL);
        when(httpServletRequest.isSecure()).thenReturn(false);

        updateGeneral.doUpdate();

        assertThat(updateGeneral.isDisableSSOSecureCookie(), is(true));
        verify(propertyManager, times(0)).setSecureCookie(true);
    }
}
