package com.atlassian.crowd.console.action;

import java.util.Collections;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.integration.http.HttpAuthenticator;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.license.CrowdLicenseManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.license.SIDManager;

import com.google.common.collect.ImmutableList;
import com.opensymphony.webwork.ServletActionContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LicenseTest
{
    @Mock
    private PropertyManager propertyManager;

    @Mock
    private CrowdLicenseManager licenseManager;

    @Mock
    private SIDManager sidManager;

    @Mock
    private CrowdLicense currentLicense;

    @Mock
    private CrowdLicense newLicense;

    @Mock
    private JohnsonEventContainer johnsonEventContainer;

    @Mock
    private TokenAuthenticationManager tokenAuthenticationManager;

    @Mock
    private HttpAuthenticator httpAuthenticator;

    @Mock
    private CrowdUserDetailsService userDetailsService;

    @Mock
    private CrowdBootstrapManager crowdBootstrapManager;

    @Mock
    private ActionHelper actionHelper;

    @InjectMocks
    private License action;

    @Before
    public void setUpMocks()
    {
        when(actionHelper.getBootstrapManager()).thenReturn(crowdBootstrapManager);
    }

    @Before
    public void initStatics()
    {
        ServletContext servletContext = mock(ServletContext.class);
        when(servletContext.getAttribute(Johnson.EVENT_CONTAINER_ATTRIBUTE)).thenReturn(johnsonEventContainer);
        ServletActionContext.setServletContext(servletContext);
        Johnson.initialize(servletContext);
    }

    @After
    public void releaseStatics()
    {
        ServletActionContext.setContext(null);
    }

    @Test
    public void shouldRequireLicenseKey() throws Exception
    {
        action.setUsername("admin");
        action.setPassword("password");

        assertEquals(License.ERROR, action.doUpdate());
        assertEquals(ImmutableList.of(action.getText("license.key.error.blank")), action.getFieldErrors().get("key"));
    }

    @Test
    public void shouldNotAllowInstallingAnInvalidLicense() throws Exception
    {
        action.setKey("key");
        action.setUsername("admin");
        action.setPassword("password");

        when(licenseManager.isLicenseKeyValid("key")).thenReturn(false);
        when(licenseManager.isBuildWithinMaintenancePeriod("key")).thenReturn(true);

        assertEquals(License.ERROR, action.doUpdate());
        assertEquals(ImmutableList.of(action.getText("license.key.error.invalid")),
                     action.getFieldErrors().get("key"));

        verify(licenseManager, never()).storeLicense("key");
    }

    @Test
    public void shouldNotAllowInstallingAnExpiredLicense() throws Exception
    {
        action.setKey("key");
        action.setUsername("admin");
        action.setPassword("password");

        when(licenseManager.isLicenseKeyValid("key")).thenReturn(true);
        when(licenseManager.isBuildWithinMaintenancePeriod("key")).thenReturn(false);

        assertEquals(License.ERROR, action.doUpdate());
        assertEquals(ImmutableList.of(action.getText("license.key.error.maintenance.expired")),
                     action.getFieldErrors().get("key"));

        verify(licenseManager, never()).storeLicense("key");
    }

    @Test
    public void shouldRequireUsernameIfCurrentLicenseIsValidAndNotExpired() throws Exception
    {
        when(licenseManager.getLicense()).thenReturn(currentLicense);
        when(licenseManager.isLicenseValid(currentLicense)).thenReturn(true);
        when(licenseManager.isBuildWithinMaintenancePeriod(currentLicense)).thenReturn(true);

        action.setKey("key");
        action.setPassword("password");

        assertEquals(License.ERROR, action.doUpdate());
        assertEquals(ImmutableList.of(action.getText("login.username.error")), action.getFieldErrors().get("username"));

        verify(licenseManager, never()).storeLicense("key");
    }

    @Test
    public void shouldRequirePasswordIfCurrentLicenseIsValidAndNotExpired() throws Exception
    {
        when(licenseManager.getLicense()).thenReturn(currentLicense);
        when(licenseManager.isLicenseValid(currentLicense)).thenReturn(true);
        when(licenseManager.isBuildWithinMaintenancePeriod(currentLicense)).thenReturn(true);

        action.setKey("key");
        action.setUsername("admin");

        assertEquals(License.ERROR, action.doUpdate());
        assertEquals(ImmutableList.of(action.getText("login.password.error")), action.getFieldErrors().get("password"));

        verify(licenseManager, never()).storeLicense("key");
    }

    @Test
    public void shouldInstallLicenseWithoutCredentialsIfCurrentLicenseIsNotValid() throws Exception
    {
        when(licenseManager.getLicense()).thenReturn(currentLicense);
        when(licenseManager.isLicenseValid(currentLicense)).thenReturn(false);
        when(licenseManager.isBuildWithinMaintenancePeriod(currentLicense)).thenReturn(true);

        when(licenseManager.isLicenseKeyValid("key")).thenReturn(true);
        when(licenseManager.isBuildWithinMaintenancePeriod("key")).thenReturn(true);

        when(licenseManager.storeLicense("key")).thenReturn(newLicense);

        action.setKey("key");

        assertEquals(License.SUCCESS, action.doUpdate());

        verify(licenseManager).storeLicense("key");
        verify(johnsonEventContainer).addEvent(any(Event.class));
    }

    @Test
    public void shouldInstallLicenseWithoutCredentialsIfCurrentLicenseIsExpired() throws Exception
    {
        when(licenseManager.getLicense()).thenReturn(currentLicense);
        when(licenseManager.isLicenseValid(currentLicense)).thenReturn(true);
        when(licenseManager.isBuildWithinMaintenancePeriod(currentLicense)).thenReturn(false);

        when(licenseManager.isLicenseKeyValid("key")).thenReturn(true);
        when(licenseManager.isBuildWithinMaintenancePeriod("key")).thenReturn(true);

        when(licenseManager.storeLicense("key")).thenReturn(newLicense);

        action.setKey("key");

        assertEquals(License.SUCCESS, action.doUpdate());

        verify(licenseManager).storeLicense("key");
        verify(johnsonEventContainer).addEvent(any(Event.class));
    }

    @Test
    public void shouldInstallLicenseUsingValidAdminCredentialsEvenWhenCurrentLicenseIsValidAndNotExpired() throws Exception
    {
        when(licenseManager.getLicense()).thenReturn(currentLicense);
        when(licenseManager.isLicenseValid(currentLicense)).thenReturn(true);
        when(licenseManager.isBuildWithinMaintenancePeriod(currentLicense)).thenReturn(true);

        when(licenseManager.isLicenseKeyValid("key")).thenReturn(true);
        when(licenseManager.isBuildWithinMaintenancePeriod("key")).thenReturn(true);

        UserAuthenticationContext userAuthenticationContext = mock(UserAuthenticationContext.class);
        when(httpAuthenticator.getPrincipalAuthenticationContext(any(HttpServletRequest.class),
                                                                 any(HttpServletResponse.class),
                                                                 eq("admin"), eq("password")))
            .thenReturn(userAuthenticationContext);

        CrowdUserDetails crowdUserDetails = mock(CrowdUserDetails.class);
        when(userDetailsService.loadUserByUsername("admin")).thenReturn(crowdUserDetails);
        when(actionHelper.hasAdminRole(crowdUserDetails)).thenReturn(true);

        when(licenseManager.storeLicense("key")).thenReturn(newLicense);

        action.setKey("key");
        action.setUsername("admin");
        action.setPassword("password");

        assertEquals(License.SUCCESS, action.doUpdate());

        verify(licenseManager).storeLicense("key");
        verify(johnsonEventContainer).addEvent(any(Event.class));
        verify(tokenAuthenticationManager).authenticateUser(userAuthenticationContext, TokenLifetime.USE_DEFAULT);
    }

    @Test
    public void shouldRejectRegularUserCredentialsIfCurrentLicenseIsValidAndNotExpired() throws Exception
    {
        when(licenseManager.getLicense()).thenReturn(currentLicense);
        when(licenseManager.isLicenseValid(currentLicense)).thenReturn(true);
        when(licenseManager.isBuildWithinMaintenancePeriod(currentLicense)).thenReturn(true);

        when(licenseManager.isLicenseKeyValid("key")).thenReturn(true);
        when(licenseManager.isBuildWithinMaintenancePeriod("key")).thenReturn(true);

        UserAuthenticationContext userAuthenticationContext = mock(UserAuthenticationContext.class);
        when(httpAuthenticator.getPrincipalAuthenticationContext(any(HttpServletRequest.class),
                                                                 any(HttpServletResponse.class),
                                                                 eq("user"), eq("password")))
            .thenReturn(userAuthenticationContext);

        CrowdUserDetails crowdUserDetails = mock(CrowdUserDetails.class);
        when(userDetailsService.loadUserByUsername("user")).thenReturn(crowdUserDetails);
        when(actionHelper.hasAdminRole(crowdUserDetails)).thenReturn(false);

        when(licenseManager.storeLicense("key")).thenReturn(newLicense);

        action.setKey("key");
        action.setUsername("user");
        action.setPassword("password");

        assertEquals(License.ERROR, action.doUpdate());

        assertEquals(ImmutableList.of(action.getText("login.failed.invalidcredentials")),
                     action.getFieldErrors().get("username"));

        verify(licenseManager, never()).storeLicense("key");
        verify(tokenAuthenticationManager).authenticateUser(userAuthenticationContext, TokenLifetime.USE_DEFAULT);
    }

    @Test
    public void shouldRejectInvalidCredentials() throws Exception
    {
        when(licenseManager.getLicense()).thenReturn(currentLicense);
        when(licenseManager.isLicenseValid(currentLicense)).thenReturn(true);
        when(licenseManager.isBuildWithinMaintenancePeriod(currentLicense)).thenReturn(true);

        when(licenseManager.isLicenseKeyValid("key")).thenReturn(true);
        when(licenseManager.isBuildWithinMaintenancePeriod("key")).thenReturn(true);

        UserAuthenticationContext userAuthenticationContext = mock(UserAuthenticationContext.class);
        when(httpAuthenticator.getPrincipalAuthenticationContext(any(HttpServletRequest.class),
                                                                 any(HttpServletResponse.class),
                                                                 eq("admin"), eq("badpassword")))
            .thenReturn(userAuthenticationContext);
        when(tokenAuthenticationManager.authenticateUser(userAuthenticationContext, TokenLifetime.USE_DEFAULT))
            .thenThrow(new InvalidAuthenticationException("invalid auth"));

        action.setKey("key");
        action.setUsername("admin");
        action.setPassword("badpassword");

        assertEquals(License.ERROR, action.doUpdate());

        assertEquals(ImmutableList.of(action.getText("login.failed.invalidcredentials")),
                     action.getFieldErrors().get("username"));

        verify(licenseManager, never()).storeLicense("key");
        verify(tokenAuthenticationManager).authenticateUser(userAuthenticationContext, TokenLifetime.USE_DEFAULT);
    }
}
