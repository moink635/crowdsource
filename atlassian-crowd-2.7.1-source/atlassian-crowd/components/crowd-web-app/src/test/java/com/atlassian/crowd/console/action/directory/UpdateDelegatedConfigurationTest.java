package com.atlassian.crowd.console.action.directory;

import java.util.Set;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.embedded.api.Directory;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import org.hamcrest.CoreMatchers;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UpdateDelegatedConfigurationTest
{
    Directory directory;
    UpdateDelegatedConfiguration update;

    private final String[] groupFields = {
            "groupDescriptionAttr",
            "groupMemberAttr",
            "groupNameAttr",
            "groupObjectClass",
            "groupObjectFilter"
    };

    @Before
    public void setUp()
    {
        directory = mock(Directory.class);

        update = new UpdateDelegatedConfiguration() {
            public com.atlassian.crowd.embedded.api.Directory getDirectory()
            {
                return directory;
            };
        };
    }

    @Test
    public void groupPropertiesAreRequired()
    {
        when(directory.getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_KEY_IMPORT_GROUPS)).thenReturn("true");

        update.doUpdate();

        assertThat(
                (Set<String>) update.getFieldErrors().keySet(),
                hasItems(groupFields));
    }

    @Test
    public void groupPropertiesAreNotRequiredWhenWeAreNotImportingGroups()
    {
        when(directory.getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_KEY_IMPORT_GROUPS)).thenReturn("false");

        update.doUpdate();

        Set<String> fieldErrors = update.getFieldErrors().keySet();

        assertThat(fieldErrors, CoreMatchers.not(IsEmptyCollection.<String>empty()));

        assertThat(
                Sets.intersection(
                        fieldErrors,
                        ImmutableSet.<String>copyOf(groupFields)),
                IsEmptyCollection.<String>empty());
    }
}
