package com.atlassian.crowd.console.action;

import com.atlassian.crowd.manager.mail.MailManager;
import com.atlassian.extras.api.crowd.CrowdLicense;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConsoleTest
{
    @InjectMocks
    private Console console;

    @Mock
    private MailManager mailManager;

    @Mock
    private CrowdLicense license;

    @Mock
    private ActionHelper actionHelper;

    @Test
    public void mailServerShouldBeConfigured() throws Exception
    {
        when(mailManager.isConfigured()).thenReturn(true);
        when(actionHelper.getLicense()).thenReturn(license);

        assertEquals(Console.SUCCESS, console.execute());

        assertTrue(console.isMailServerConfigured());
    }

    @Test
    public void mailServerShouldBeUnconfigured() throws Exception
    {
        when(mailManager.isConfigured()).thenReturn(false);
        when(actionHelper.getLicense()).thenReturn(license);

        assertEquals(Console.SUCCESS, console.execute());

        assertFalse(console.isMailServerConfigured());
    }
}
