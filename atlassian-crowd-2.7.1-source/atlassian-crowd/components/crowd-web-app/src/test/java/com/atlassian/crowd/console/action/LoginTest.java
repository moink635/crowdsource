package com.atlassian.crowd.console.action;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.integration.springsecurity.RequestToApplicationMapper;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.model.application.Application;

import com.google.common.collect.ImmutableList;
import com.opensymphony.webwork.ServletActionContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.web.WebAttributes;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoginTest
{
    @InjectMocks
    private Login action = new Login();

    @Mock
    private RequestToApplicationMapper requestToApplicationMapper;

    @Mock
    private ApplicationManager applicationManager;

    @Mock
    private Application application;

    @Mock
    private ActionHelper actionHelper;

    private MockHttpServletRequest request;

    @Before
    public void setUp() throws Exception
    {
        when(application.getDescription()).thenReturn("Application description");

        when(applicationManager.findByName("application-name")).thenReturn(application);

        when(requestToApplicationMapper.getApplication(any(HttpServletRequest.class))).thenReturn("application-name");

        request = new MockHttpServletRequest();
        ServletActionContext.setRequest(request);
    }

    @After
    public void tearDown() throws Exception
    {
        ServletActionContext.setRequest(null);
    }

    @Test
    public void shouldLoginSuccessfully() throws Exception
    {
        when(actionHelper.isAuthenticated()).thenReturn(true);

        assertEquals(Login.SUCCESS, action.execute());
        assertTrue(action.getActionErrors().isEmpty());
    }

    @Test
    public void shouldInformAboutFailedLoginAttempt() throws Exception
    {
        action.setError(true);  // parameter set by Spring Security

        assertEquals(Login.INPUT, action.execute());

        assertEquals(ImmutableList.of(action.getText("login.failed.label")), action.getActionErrors());
    }

    @Test
    public void shouldRedirectIfPasswordHasExpired() throws Exception
    {
        action.setError(true);  // parameter set by Spring Security

        CredentialsExpiredException authenticationException = new CredentialsExpiredException("");
        request.getSession().setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, authenticationException);

        assertEquals(Login.EXPIRED_PASSWORD, action.execute());
    }

    @Test
    public void shouldInformAboutFailureToConnectToAuthenticationServer() throws Exception
    {
        action.setError(true);  // parameter set by Spring Security

        // this exception may be due to a wrong Server URL, or bad client credentials
        AuthenticationServiceException authenticationException = new AuthenticationServiceException("");
        request.getSession().setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION, authenticationException);

        assertEquals(Login.INPUT, action.execute());

        assertEquals(ImmutableList.of(action.getText("login.failed.serverexception")), action.getActionErrors());
    }
}
