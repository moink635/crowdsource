package com.atlassian.crowd.console.action.options;

import javax.mail.internet.InternetAddress;

import com.atlassian.crowd.manager.mail.MailManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.util.mail.SMTPServer;

import com.google.common.collect.ImmutableList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdateMailServerTest
{
    @InjectMocks
    private UpdateMailServer updateMailServer;

    @Mock
    private MailManager mailManager;

    @Mock
    private PropertyManager propertyManager;

    @Mock
    private SMTPServer smtpServer;

    @Test
    public void shouldShowMessageIfMailServerIsUnconfigured() throws Exception
    {
        when(mailManager.isConfigured()).thenReturn(false);

        when(propertyManager.getSMTPServer()).thenReturn(smtpServer);

        when(smtpServer.getFrom()).thenReturn(mock(InternetAddress.class));

        updateMailServer.doDefault();

        assertEquals(ImmutableList.of(updateMailServer.getText("mailserver.unconfigured.warning")),
                     updateMailServer.getActionErrors());
    }

    @Test
    public void shouldNotShowMessageIfMailServerIsAlreadyConfigured() throws Exception
    {
        when(mailManager.isConfigured()).thenReturn(true);

        when(propertyManager.getSMTPServer()).thenReturn(smtpServer);

        when(smtpServer.getFrom()).thenReturn(mock(InternetAddress.class));

        updateMailServer.doDefault();

        assertEquals(ImmutableList.of(), updateMailServer.getActionErrors());
    }

    @Test
    public void shouldCheckRequiredFields() throws Exception
    {
        updateMailServer.doUpdate();

        assertEquals(ImmutableList.of(updateMailServer.getText("mailserver.notification.invalid")),
                updateMailServer.getFieldErrors().get("notificationEmail"));
        assertEquals(ImmutableList.of(updateMailServer.getText("mailserver.from.invalid")),
                updateMailServer.getFieldErrors().get("from"));
    }
}
