package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.password.factory.PasswordEncoderFactory;

import com.google.common.base.Functions;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreateInternalTest
{
    @Test
    public void encryptionMethodsAreInAlphabeticalOrder()
    {
        PasswordEncoderFactory passwordEncoderFactory = mock(PasswordEncoderFactory.class);

        when(passwordEncoderFactory.getSupportedInternalEncoders()).thenReturn(ImmutableSet.of("b", "c", "a", "e", "d"));
        CreateInternal ci = new CreateInternal();

        ci.setPasswordEncoderFactory(passwordEncoderFactory);

        assertEquals(ImmutableList.of("a/A", "b/B", "c/C", "d/D", "e/E"),
                ImmutableList.copyOf(Iterables.transform(ci.getUserEncryptionMethods(), Functions.toStringFunction())));
    }
}
