package com.atlassian.crowd.console.action.principal;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;

import com.google.common.collect.ImmutableList;
import com.opensymphony.webwork.ServletActionContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddPrincipalTest
{
    private static final long DIRECTORY_ID = 1L;

    private AddPrincipal addPrincipal;

    @Mock private DirectoryManager directoryManager;
    @Mock private Directory directory;

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @Before
    public void createObjectUnderTest() throws Exception
    {
        addPrincipal = new AddPrincipal();
        addPrincipal.setDirectoryManager(directoryManager);
        addPrincipal.setDirectoryID(DIRECTORY_ID);
    }

    @Before
    public void prepareMocks() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        request = new MockHttpServletRequest();
        ServletActionContext.setRequest(request);

        response = new MockHttpServletResponse();
        ServletActionContext.setResponse(response);
    }

    @After
    public void disposeRequestAndResponse()
    {
        ServletActionContext.setRequest(null);
        ServletActionContext.setResponse(null);
    }

    @Test
    public void addPrincipalSuccessfully() throws Exception
    {
        when(directoryManager.findUserByName(DIRECTORY_ID, "user")).thenThrow(new UserNotFoundException("user"));

        addPrincipal.setName("user");
        addPrincipal.setEmail("john@example.test");
        addPrincipal.setFirstname("John");
        addPrincipal.setLastname("Doe");
        addPrincipal.setPassword("password");
        addPrincipal.setPasswordConfirm("password");

        assertEquals(AddPrincipal.SUCCESS, addPrincipal.doUpdate());

        ArgumentCaptor<UserTemplate> userTemplate = ArgumentCaptor.forClass(UserTemplate.class);
        verify(directoryManager).addUser(eq(DIRECTORY_ID), userTemplate.capture(),
                                         eq(PasswordCredential.unencrypted("password")));

        assertEquals("user", userTemplate.getValue().getName());
        assertEquals("john@example.test", userTemplate.getValue().getEmailAddress());
        assertEquals("John", userTemplate.getValue().getFirstName());
        assertEquals("Doe", userTemplate.getValue().getLastName());
    }

    @Test
    public void emptyUsernameIsInvalid()
    {
        assertEquals(AddPrincipal.INPUT, addPrincipal.doUpdate());

        assertEquals(ImmutableList.of(addPrincipal.getText("principal.name.invalid")),
                     addPrincipal.getFieldErrors().get("name"));
    }

    @Test
    public void usernameWithTrailingSpacesIsInvalid()
    {
        addPrincipal.setName(" username ");

        assertEquals(AddPrincipal.INPUT, addPrincipal.doUpdate());

        assertEquals(ImmutableList.of(addPrincipal.getText("invalid.whitespace")),
                     addPrincipal.getFieldErrors().get("name"));
    }


    @Test
    public void usernameMustBeAvailable() throws Exception
    {
        User user = mock(User.class);
        when(directoryManager.findUserByName(DIRECTORY_ID, "user")).thenReturn(user);

        addPrincipal.setName("user");

        assertEquals(AddPrincipal.INPUT, addPrincipal.doUpdate());

        assertEquals(ImmutableList.of(addPrincipal.getText("invalid.namealreadyexist")),
                     addPrincipal.getFieldErrors().get("name"));
    }

    @Test
    public void emptyEmailIsInvalid() throws Exception
    {
        assertEquals(AddPrincipal.INPUT, addPrincipal.doUpdate());

        assertEquals(ImmutableList.of(addPrincipal.getText("principal.email.invalid")),
                     addPrincipal.getFieldErrors().get("email"));
    }

    @Test
    public void emailWithoutAtIsInvalid() throws Exception
    {
        addPrincipal.setEmail("invalid-email");

        assertEquals(AddPrincipal.INPUT, addPrincipal.doUpdate());

        assertEquals(ImmutableList.of(addPrincipal.getText("principal.email.invalid")),
                     addPrincipal.getFieldErrors().get("email"));
    }

    @Test
    public void emailWithTrailingWhitespaceIsInvalid() throws Exception
    {
        addPrincipal.setEmail(" john@example.test ");

        assertEquals(AddPrincipal.INPUT, addPrincipal.doUpdate());

        assertEquals(ImmutableList.of(addPrincipal.getText("principal.email.whitespace")),
                     addPrincipal.getFieldErrors().get("email"));
    }

    @Test
    public void passwordsMustMatch()
    {
        addPrincipal.setPassword("password1");
        addPrincipal.setPasswordConfirm("password2");

        assertEquals(AddPrincipal.INPUT, addPrincipal.doUpdate());

        assertEquals(ImmutableList.of(addPrincipal.getText("principal.passwordconfirm.nomatch")),
                     addPrincipal.getFieldErrors().get("password"));
    }
}
