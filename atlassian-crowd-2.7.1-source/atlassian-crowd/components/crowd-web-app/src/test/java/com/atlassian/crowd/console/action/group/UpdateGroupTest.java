package com.atlassian.crowd.console.action.group;

import java.util.Collection;
import java.util.List;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;

import com.opensymphony.xwork.ValidationAware;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdateGroupTest
{
    private static final Long DIRECTORY_ID = 1L;
    private static final String GROUP_NAME = "group-name";

    @Mock private DirectoryManager directoryManager;
    @Mock private DirectoryInstanceLoader directoryInstanceLoader;
    @Mock private Directory directory;
    @Mock private RemoteDirectory remoteDirectory;

    private Group group = new GroupTemplate(GROUP_NAME);

    private UpdateGroup updateGroup;

    @Before
    public void setUp() throws Exception
    {
        updateGroup = new UpdateGroup();
        updateGroup.setDirectoryID(DIRECTORY_ID);
        updateGroup.setName(GROUP_NAME);
        updateGroup.setDirectoryManager(directoryManager);
        updateGroup.setDirectoryInstanceLoader(directoryInstanceLoader);
    }

    @Test
    public void shouldBeAbleToSetNormalDescription() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);
        when(directoryManager.findGroupByName(DIRECTORY_ID, GROUP_NAME)).thenReturn(group);

        when(directoryInstanceLoader.getDirectory(directory)).thenReturn(remoteDirectory);

        updateGroup.setDescription("sample description");
        String actionResult = updateGroup.doUpdateGeneral();

        assertEquals(ViewGroup.SUCCESS, actionResult);
        assertNull(getFieldErrorsFor(updateGroup, "description"));
        assertThat(getActionErrorsFor(updateGroup), empty());
    }

    @Test
    public void shouldBeAbleToSetEmptyDescription() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);
        when(directoryManager.findGroupByName(DIRECTORY_ID, GROUP_NAME)).thenReturn(group);

        when(directoryInstanceLoader.getDirectory(directory)).thenReturn(remoteDirectory);

        updateGroup.setDescription("");
        String actionResult = updateGroup.doUpdateGeneral();

        assertEquals(ViewGroup.SUCCESS, actionResult);
        assertNull(getFieldErrorsFor(updateGroup, "description"));
        assertThat(getActionErrorsFor(updateGroup), empty());
    }

    @Test
    public void shouldNotBeAbleToSetWhitespaceDescription() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);
        when(directoryManager.findGroupByName(DIRECTORY_ID, GROUP_NAME)).thenReturn(group);

        when(directoryInstanceLoader.getDirectory(directory)).thenReturn(remoteDirectory);

        updateGroup.setDescription(" ");
        String actionResult = updateGroup.doUpdateGeneral();

        assertEquals(ViewGroup.INPUT, actionResult);
        assertThat(getFieldErrorsFor(updateGroup, "description"), contains(updateGroup.getText("group.description.invalid")));
        assertThat(getActionErrorsFor(updateGroup), empty());
    }

    // Helper methods to allow hamcrest matchers to be used with non-generic legacy code without making OpenJDK7 fall over

    @SuppressWarnings("unchecked")
    private static List<String> getFieldErrorsFor(ValidationAware updateGroup, String fieldName)
    {
        return (List<String>) updateGroup.getFieldErrors().get(fieldName);
    }

    @SuppressWarnings("unchecked")
    private static Collection<String> getActionErrorsFor(ValidationAware validationAware)
    {
        return (Collection<String>) validationAware.getActionErrors();
    }
}
