package com.atlassian.crowd;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import static org.junit.Assert.assertNotSame;

public class TestSpringNamespaceHandlerPresent
{
    BeanFactory beanFactory;

    @Before
    public final void createFactory()
    {
        beanFactory = new XmlBeanFactory(new ClassPathResource("beans.xml", TestSpringNamespaceHandlerPresent.class));
    }

    @Before
    public final void setUpSecurityContext()
    {
        SecurityContext sc = new SecurityContextImpl();

        List<GrantedAuthority> creds = Arrays.<GrantedAuthority>asList(
                new SimpleGrantedAuthority("bread-roll"),
                new SimpleGrantedAuthority("ROLE_USER"));
        sc.setAuthentication(new AnonymousAuthenticationToken("key", "principal", creds));
        SecurityContextHolder.setContext(sc);
    }

    @Test
    public void objectIsWrappedWithInterceptor()
    {
        TestSpringNamespaceHandlerPresent hp = beanFactory.getBean(TestSpringNamespaceHandlerPresent.class);
        assertNotSame(TestSpringNamespaceHandlerPresent.class, hp.getClass());
    }

    @Test
    public void ableToParseBeanDefinitionsWithoutError()
    {
        TestSpringNamespaceHandlerPresent hp = beanFactory.getBean(TestSpringNamespaceHandlerPresent.class);
        hp.sampleMethod();
    }

    @Test(expected = AccessDeniedException.class)
    public void unableToCallMethodSecuredWithOtherRole()
    {
        TestSpringNamespaceHandlerPresent hp = beanFactory.getBean(TestSpringNamespaceHandlerPresent.class);
        hp.otherSampleMethod();
    }

//    @Secured("ROLE_USER")
    public void sampleMethod()
    {
    }

//    @Secured({"ROLE_ADMIN"})
    public void otherSampleMethod()
    {
    }
}
