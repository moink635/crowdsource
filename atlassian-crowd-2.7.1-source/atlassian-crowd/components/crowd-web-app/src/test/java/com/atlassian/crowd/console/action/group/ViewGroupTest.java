package com.atlassian.crowd.console.action.group;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.InternalDirectoryGroup;
import com.atlassian.crowd.service.GroupManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ViewGroupTest
{
    private static final Long DIRECTORY_ID = 1L;
    private static final String GROUP_NAME = "group-name";
    private static final String DIRECTORY_NAME = "directory-name";

    @Mock private DirectoryManager directoryManager;
    @Mock private GroupManager groupManager;
    @Mock private DirectoryInstanceLoader directoryInstanceLoader;
    @Mock private RemoteDirectory remoteDirectory;
    @Mock private InternalDirectoryGroup internalDirectoryGroup;

    private ViewGroup viewGroup;

    @Before
    public void createObjectUnderTest()
    {
        viewGroup = new ViewGroup();
        viewGroup.setDirectoryID(DIRECTORY_ID);
        viewGroup.setName(GROUP_NAME);
        viewGroup.setDirectoryManager(directoryManager);
        viewGroup.setDirectoryInstanceLoader(directoryInstanceLoader);
    }

    @Test
    public void internalGroupDoesNotHaveLocalGroups() throws Exception
    {
        Directory directory = new DirectoryImpl(DIRECTORY_NAME, DirectoryType.INTERNAL, "some.Class");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);
        when(directoryManager.findGroupByName(DIRECTORY_ID, GROUP_NAME)).thenReturn(internalDirectoryGroup);

        when(remoteDirectory.supportsNestedGroups()).thenReturn(false);

        when(directoryInstanceLoader.getDirectory(directory)).thenReturn(remoteDirectory);

        assertEquals(ViewGroup.SUCCESS, viewGroup.doDefault());
        assertNull(viewGroup.getLocationKey());
    }

    @Test
    public void localGroupInDelegatingConnector() throws Exception
    {
        Directory directory = new DirectoryImpl(DIRECTORY_NAME, DirectoryType.DELEGATING, "some.Class");

        when(internalDirectoryGroup.isLocal()).thenReturn(true);

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);
        when(directoryManager.findGroupByName(DIRECTORY_ID, GROUP_NAME)).thenReturn(internalDirectoryGroup);

        when(remoteDirectory.supportsNestedGroups()).thenReturn(false);

        when(directoryInstanceLoader.getDirectory(directory)).thenReturn(remoteDirectory);

        assertEquals(ViewGroup.SUCCESS, viewGroup.doDefault());
        assertEquals("group.location.local", viewGroup.getLocationKey());
    }

    @Test
    public void remoteGroupInDelegatingConnector() throws Exception
    {
        Directory directory = new DirectoryImpl(DIRECTORY_NAME, DirectoryType.DELEGATING, "some.Class");

        when(internalDirectoryGroup.isLocal()).thenReturn(false);

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);
        when(directoryManager.findGroupByName(DIRECTORY_ID, GROUP_NAME)).thenReturn(internalDirectoryGroup);

        when(remoteDirectory.supportsNestedGroups()).thenReturn(false);

        when(directoryInstanceLoader.getDirectory(directory)).thenReturn(remoteDirectory);

        assertEquals(ViewGroup.SUCCESS, viewGroup.doDefault());
        assertEquals("group.location.remote", viewGroup.getLocationKey());
    }
}
