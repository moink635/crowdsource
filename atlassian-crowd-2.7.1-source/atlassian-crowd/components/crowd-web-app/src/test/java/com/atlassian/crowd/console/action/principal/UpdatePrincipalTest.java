package com.atlassian.crowd.console.action.principal;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdatePrincipalTest
{
    private static final long DIRECTORY_ID = 1L;

    private UpdatePrincipal updatePrincipal;

    @Mock private DirectoryManager directoryManager;
    @Mock private Directory directory;

    @Before
    public void createObjectUnderTest() throws Exception
    {
        updatePrincipal = new UpdatePrincipal();
        updatePrincipal.setDirectoryID(DIRECTORY_ID);
        updatePrincipal.setDirectoryManager(directoryManager);
    }

    @Before
    public void prepareMocks() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);
    }

    @Test
    public void renamePrincipalCausesNameClash() throws Exception
    {
        when(directoryManager.renameUser(DIRECTORY_ID, "old-name", "new-name"))
                .thenThrow(new UserAlreadyExistsException(DIRECTORY_ID, "new-name"));

        updatePrincipal.setDirectoryID(DIRECTORY_ID);
        updatePrincipal.setName("old-name");
        updatePrincipal.setNewName("new-name");
        updatePrincipal.setEmail("john@example.test"); // required field
        updatePrincipal.setFirstname("John"); // required field
        updatePrincipal.setLastname("Doe"); // required field

        assertEquals(UpdatePrincipal.INPUT, updatePrincipal.doUpdateGeneral());

        assertEquals(ImmutableList.of(updatePrincipal.getText("invalid.namealreadyexist")),
                updatePrincipal.getFieldErrors().get("newName"));
    }

    @Test
    public void renamePrincipalSuccessfully() throws Exception
    {
        User user = new UserTemplate("new-name");
        when(directoryManager.renameUser(DIRECTORY_ID, "old-name", "new-name")).thenReturn(user);

        updatePrincipal.setName("old-name");
        updatePrincipal.setNewName("new-name");
        updatePrincipal.setEmail("john@example.test"); // required field
        updatePrincipal.setFirstname("John"); // required field
        updatePrincipal.setLastname("Doe"); // required field

        assertEquals(UpdatePrincipal.SUCCESS, updatePrincipal.doUpdateGeneral());

        assertEquals(ImmutableList.of(updatePrincipal.getText("updatesuccessful.label")),
                     updatePrincipal.getActionMessages());

        verify(directoryManager).renameUser(DIRECTORY_ID, "old-name", "new-name");

        ArgumentCaptor<UserTemplate> userTemplate = ArgumentCaptor.forClass(UserTemplate.class);
        verify(directoryManager).updateUser(eq(DIRECTORY_ID), userTemplate.capture());

        assertEquals("new-name", userTemplate.getValue().getName());

        assertEquals("new-name", updatePrincipal.getName());
        assertEquals("new-name", updatePrincipal.getNewName());
    }

    @Test
    public void updatePrincipalSuccessfully() throws Exception
    {
        User user = new UserTemplate("user");
        when(directoryManager.findUserByName(DIRECTORY_ID, "user")).thenReturn(user);

        updatePrincipal.setName("user");
        updatePrincipal.setNewName("user"); // no rename
        updatePrincipal.setEmail("john@example.test");
        updatePrincipal.setFirstname("John");
        updatePrincipal.setLastname("Doe");

        assertEquals(UpdatePrincipal.SUCCESS, updatePrincipal.doUpdateGeneral());

        assertEquals(ImmutableList.of(updatePrincipal.getText("updatesuccessful.label")),
                     updatePrincipal.getActionMessages());

        ArgumentCaptor<UserTemplate> userTemplate = ArgumentCaptor.forClass(UserTemplate.class);
        verify(directoryManager).updateUser(eq(DIRECTORY_ID), userTemplate.capture());

        assertEquals("user", userTemplate.getValue().getName());
        assertEquals("john@example.test", userTemplate.getValue().getEmailAddress());
        assertEquals("John", userTemplate.getValue().getFirstName());
        assertEquals("Doe", userTemplate.getValue().getLastName());

        assertEquals("user", updatePrincipal.getName());
        assertEquals("user", updatePrincipal.getNewName());
    }

    @Test
    public void emptyEmailIsInvalid() throws Exception
    {
        assertEquals(UpdatePrincipal.INPUT, updatePrincipal.doUpdateGeneral());

        assertEquals(ImmutableList.of(updatePrincipal.getText("principal.email.invalid")),
                     updatePrincipal.getFieldErrors().get("email"));
    }

    @Test
    public void emailWithoutAtIsInvalid() throws Exception
    {
        updatePrincipal.setEmail("invalid-email");

        assertEquals(UpdatePrincipal.INPUT, updatePrincipal.doUpdateGeneral());

        assertEquals(ImmutableList.of(updatePrincipal.getText("principal.email.invalid")),
                     updatePrincipal.getFieldErrors().get("email"));
    }

    @Test
    public void emailWithTrailingWhitespaceIsInvalid() throws Exception
    {
        updatePrincipal.setEmail(" john@example.test ");

        assertEquals(UpdatePrincipal.INPUT, updatePrincipal.doUpdateGeneral());

        assertEquals(ImmutableList.of(updatePrincipal.getText("principal.email.whitespace")),
                     updatePrincipal.getFieldErrors().get("email"));
    }

}
