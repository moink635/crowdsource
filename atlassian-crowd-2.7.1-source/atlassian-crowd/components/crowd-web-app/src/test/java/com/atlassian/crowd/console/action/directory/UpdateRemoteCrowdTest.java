package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.directory.DirectoryManager;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UpdateRemoteCrowdTest
{
    @Test
    public void askingAnActionInErrorIfItIsSynchronisingShouldNotCauseANullPointerException() throws Exception
    {
        Directory directory = mock(Directory.class);
        DirectoryManager directoryManager = mock(DirectoryManager.class);
        when(directoryManager.findDirectoryById(1)).thenReturn(directory);
        when(directoryManager.findDirectoryByName("name")).thenReturn(null);

        UpdateRemoteCrowd urc = new UpdateRemoteCrowd();
        urc.setDirectoryInstanceLoader(mock(DirectoryInstanceLoader.class));
        urc.setDirectoryManager(directoryManager);
        urc.setCacheEnabled(true);
        urc.setID(1);
        urc.setName("name");

        urc.execute();

        assertThat((Iterable<String>) urc.getActionErrors(), Matchers.hasItem("The directory name 'name' is not unique."));

        assertTrue(urc.isCacheEnabled());
        assertFalse(urc.isCurrentlySynchronising());

        assertNull(urc.getSynchronisationDuration());
        assertNull(urc.getSynchronisationStartTime());
        assertEquals("Not yet synchronised", urc.getSynchronisationStatus());
    }
}
