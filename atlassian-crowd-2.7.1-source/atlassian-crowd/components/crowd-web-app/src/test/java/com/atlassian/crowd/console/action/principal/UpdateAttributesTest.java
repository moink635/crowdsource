package com.atlassian.crowd.console.action.principal;

import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.service.client.ClientProperties;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.junit.Assert.assertThat;

import static org.junit.Assert.assertEquals;

import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdateAttributesTest
{
    private static final String USER_NAME = "userName";
    private static final String EXISTING_ATTRIBUTE_NAME = "existingAttributeName";
    private static final String EXISTING_ATTRIBUTE_VALUE = "existingAttributeValue";
    private static final String NEW_ATTRIBUTE_NAME = "newAttributeName";
    private static final String NEW_ATTRIBUTE_VALUE = "newAttributeValue";
    private static final String UNDESIRED_ATTRIBUTE_NAME = "undesiredAttributeName";
    private static final String UNDESIRED_ATTRIBUTE_VALUE = "undesiredAttributeValue";
    private static final String ANOTHER_ATTRIBUTE_NAME = "anotherAttributeName";
    private static final String ANOTHER_ATTRIBUTE_VALUE = "anotherAttributeValue";
    private static final long DIRECTORY_ID = 1;
    private static final String APPLICATION_NAME = "applicationName";

    @Mock private DirectoryManager directoryManager;
    @Mock private UserWithAttributes userWithAttributes;
    @Mock private ClientProperties clientProperties;
    @Mock private TokenAuthenticationManager tokenAuthenticationManager;
    private MockHttpServletRequest request = new MockHttpServletRequest();

    @InjectMocks private UpdateAttributes updateAttributes = new UpdateAttributes();

    @Before
    public void setUp() throws Exception
    {
        updateAttributes.setDirectoryID(DIRECTORY_ID);
        updateAttributes.setName(USER_NAME);
        updateAttributes.setServletRequest(request);

        when(directoryManager.findUserWithAttributesByName(DIRECTORY_ID, USER_NAME)).thenReturn(userWithAttributes);
        when(userWithAttributes.getName()).thenReturn(USER_NAME);
        when(clientProperties.getApplicationName()).thenReturn(APPLICATION_NAME);
    }

    @Test
    public void shouldNotAddAttributesWithEmptyName() throws Exception
    {
        updateAttributes.setAttribute("");
        updateAttributes.setValue(NEW_ATTRIBUTE_VALUE);

        updateAttributes.doAddAttribute();

        ArrayList<String> attributeFieldErrors = (ArrayList<String>) updateAttributes.getFieldErrors().get("attribute");
        assertThat(attributeFieldErrors, hasItem(updateAttributes.getText("principal.attributename.invalid")));
        verify(directoryManager, never()).storeUserAttributes(anyLong(), anyString(), Matchers.<Map<String,Set<String>>>anyObject());
    }

    @Test
    public void shouldAddNewAttributeAndPreserveExistingOnes() throws Exception
    {
        updateAttributes.setAttribute(NEW_ATTRIBUTE_NAME);
        updateAttributes.setValue(NEW_ATTRIBUTE_VALUE);

        when(userWithAttributes.getKeys()).thenReturn(ImmutableSet.of(EXISTING_ATTRIBUTE_NAME));
        when(userWithAttributes.getValues(EXISTING_ATTRIBUTE_NAME)).thenReturn(ImmutableSet.of(EXISTING_ATTRIBUTE_VALUE));

        updateAttributes.doAddAttribute();

        Map<String, Set<String>> attributesMap = ImmutableMap.<String, Set<String>>of(EXISTING_ATTRIBUTE_NAME,
                                                                ImmutableSet.of(EXISTING_ATTRIBUTE_VALUE),
                                                                NEW_ATTRIBUTE_NAME,
                                                                ImmutableSet.of(NEW_ATTRIBUTE_VALUE));
        verify(directoryManager).storeUserAttributes(DIRECTORY_ID, USER_NAME, attributesMap);
        assertEquals(attributesMap, updateAttributes.getUserAttributes());
    }

    @Test
    public void shouldAddNewValuesToAnExistingAttribute() throws Exception
    {
        updateAttributes.setAttribute(EXISTING_ATTRIBUTE_NAME);
        updateAttributes.setValue(NEW_ATTRIBUTE_VALUE);

        when(userWithAttributes.getKeys()).thenReturn(ImmutableSet.of(EXISTING_ATTRIBUTE_NAME));
        when(userWithAttributes.getValues(EXISTING_ATTRIBUTE_NAME)).thenReturn(ImmutableSet.of(EXISTING_ATTRIBUTE_VALUE));

        updateAttributes.doAddAttribute();

        Map<String, Set<String>> attributesMap = ImmutableMap.<String, Set<String>>of(EXISTING_ATTRIBUTE_NAME,
                                                                ImmutableSet.of(EXISTING_ATTRIBUTE_VALUE, NEW_ATTRIBUTE_VALUE));
        verify(directoryManager).storeUserAttributes(DIRECTORY_ID, USER_NAME, attributesMap);
        assertEquals(attributesMap, updateAttributes.getUserAttributes());
    }

    @Test
    public void shouldDeleteAttributeAndPreserveTheOtherOnes() throws Exception
    {
        updateAttributes.setAttribute(UNDESIRED_ATTRIBUTE_NAME);

        when(userWithAttributes.getKeys()).thenReturn(ImmutableSet.of(EXISTING_ATTRIBUTE_NAME, UNDESIRED_ATTRIBUTE_NAME));
        when(userWithAttributes.getValues(EXISTING_ATTRIBUTE_NAME)).thenReturn(ImmutableSet.of(EXISTING_ATTRIBUTE_VALUE));
        when(userWithAttributes.getValues(UNDESIRED_ATTRIBUTE_NAME)).thenReturn(ImmutableSet.of(UNDESIRED_ATTRIBUTE_VALUE));

        updateAttributes.doRemoveAttribute();

        verify(directoryManager).removeUserAttributes(DIRECTORY_ID, USER_NAME, UNDESIRED_ATTRIBUTE_NAME);
        Map<String, Set<String>> attributesMap = ImmutableMap.<String, Set<String>>of(EXISTING_ATTRIBUTE_NAME,
                                                                ImmutableSet.of(EXISTING_ATTRIBUTE_VALUE));
        assertEquals(attributesMap, updateAttributes.getUserAttributes());
    }

    @Test
    public void shouldUpdateAttributeValuesWhenAttributeNamesAreValid() throws Exception
    {
        updateAttributes.setAttributes(new String[] { EXISTING_ATTRIBUTE_NAME, ANOTHER_ATTRIBUTE_NAME });
        request.setParameter(EXISTING_ATTRIBUTE_NAME + "1", EXISTING_ATTRIBUTE_VALUE);
        request.setParameter(EXISTING_ATTRIBUTE_NAME + "2", NEW_ATTRIBUTE_VALUE);
        request.setParameter(ANOTHER_ATTRIBUTE_NAME + "1", ANOTHER_ATTRIBUTE_VALUE);

        when(userWithAttributes.getKeys()).thenReturn(ImmutableSet.of(EXISTING_ATTRIBUTE_NAME, ANOTHER_ATTRIBUTE_NAME));
        when(userWithAttributes.getValues(EXISTING_ATTRIBUTE_NAME)).thenReturn(ImmutableSet.of(EXISTING_ATTRIBUTE_VALUE, UNDESIRED_ATTRIBUTE_VALUE));
        when(userWithAttributes.getValues(ANOTHER_ATTRIBUTE_NAME)).thenReturn(ImmutableSet.of(ANOTHER_ATTRIBUTE_VALUE));

        updateAttributes.doUpdateAttributes();

        Map<String, Set<String>> attributesMap = ImmutableMap.<String, Set<String>>of(EXISTING_ATTRIBUTE_NAME,
                                                                ImmutableSet.<String>of(EXISTING_ATTRIBUTE_VALUE, NEW_ATTRIBUTE_VALUE),
                                                                ANOTHER_ATTRIBUTE_NAME,
                                                                ImmutableSet.of(ANOTHER_ATTRIBUTE_VALUE));
        verify(directoryManager).storeUserAttributes(DIRECTORY_ID, USER_NAME, attributesMap);
        assertEquals(attributesMap, updateAttributes.getUserAttributes());
    }
}
