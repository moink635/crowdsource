package com.atlassian.crowd.console.action.setup;

import java.util.Collection;

import javax.naming.InitialContext;

import com.atlassian.config.setup.SetupPersister;
import com.atlassian.crowd.console.action.ActionHelper;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.mail.SMTPServer;
import com.atlassian.extras.api.Contact;
import com.atlassian.extras.api.crowd.CrowdLicense;

import com.google.common.collect.ImmutableList;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MailServerTest
{
    @InjectMocks
    private MailServer mailServer = new MailServer();

    @Mock
    private CrowdBootstrapManager bootstrapManager;

    @Mock
    private SetupPersister setupPersister;

    @Mock
    private PropertyManager propertyManager;

    @Mock
    private InitialContext initialContext;

    @Mock
    private ActionHelper actionHelper;

    @Before
    public void setUp()
    {
        when(actionHelper.getBootstrapManager()).thenReturn(bootstrapManager);
        when(bootstrapManager.isApplicationHomeValid()).thenReturn(true);
        when(setupPersister.getCurrentStep()).thenReturn(MailServer.MAILSERVER_STEP);
    }

    @Test
    public void shouldSetDefaultValuesFromLicenseContact() throws Exception
    {
        Contact contact = mock(Contact.class);
        when(contact.getEmail()).thenReturn("customer@acme.com");

        CrowdLicense license = mock(CrowdLicense.class);
        when(license.getContacts()).thenReturn(ImmutableList.of(contact));
        when(actionHelper.getLicense()).thenReturn(license);

        when(propertyManager.getSMTPServer()).thenReturn(mock(SMTPServer.class));

        mailServer.doDefault();

        assertEquals("customer@acme.com", mailServer.getNotificationEmail());
        assertEquals("customer@acme.com", mailServer.getFrom());
    }

    @Test
    public void shouldDefaultToPostponeConfiguration() throws Exception
    {
        Contact contact = mock(Contact.class);
        when(contact.getEmail()).thenReturn("customer@acme.com");

        CrowdLicense license = mock(CrowdLicense.class);
        when(license.getContacts()).thenReturn(ImmutableList.of(contact));
        when(actionHelper.getLicense()).thenReturn(license);

        when(propertyManager.getSMTPServer()).thenReturn(mock(SMTPServer.class));

        mailServer.doDefault();

        assertEquals("true", mailServer.getPostponeConfiguration());
    }

    @Test
    public void shouldContinueSetupIfConfigurationIsPostponed() throws Exception
    {
        mailServer.setPostponeConfiguration("true");

        assertEquals(MailServer.SELECT_SETUP_STEP, mailServer.doUpdate());

        assertThat((Collection<?>) mailServer.getActionErrors(), Matchers.empty());

        verify(setupPersister).progessSetupStep();

        verify(propertyManager).setProperty(Property.FORGOTTEN_PASSWORD_EMAIL_TEMPLATE,
                mailServer.getText(MailServer.DEFAULT_FORGOTTEN_PASSWORD_EMAIL_TEMPLATE));
        verify(propertyManager).setProperty(Property.FORGOTTEN_USERNAME_EMAIL_TEMPLATE,
                mailServer.getText(MailServer.DEFAULT_FORGOTTEN_USERNAME_EMAIL_TEMPLATE));
        verifyNoMoreInteractions(propertyManager);
    }

    @Test
    public void shouldSaveValidHostMailServerConfiguration() throws Exception
    {
        mailServer.setPostponeConfiguration("false");
        mailServer.setJndiMailActive("false");
        mailServer.setFrom("from@test");
        mailServer.setNotificationEmail("notification@test");
        mailServer.setHost("mail-host");

        assertEquals(MailServer.SELECT_SETUP_STEP, mailServer.doUpdate());

        assertThat((Collection<?>) mailServer.getActionErrors(), Matchers.empty());

        verify(setupPersister).progessSetupStep();

        verify(propertyManager, times(2)).setProperty(anyString(), anyString()); // the mail templates
        verify(propertyManager).setSMTPServer(any(SMTPServer.class));
        verify(propertyManager).setNotificationEmail("notification@test");
    }

    @Test
    public void shouldSaveValidJNDIMailServerConfiguration() throws Exception
    {
        mailServer.setPostponeConfiguration("false");
        mailServer.setJndiMailActive("true");
        mailServer.setFrom("from@test");
        mailServer.setNotificationEmail("notification@test");
        mailServer.setJndiLocation("jndi-location");

        when(initialContext.lookup("jndi-location")).thenReturn("mail-session");

        assertEquals(MailServer.SELECT_SETUP_STEP, mailServer.doUpdate());

        assertThat((Collection<?>) mailServer.getActionErrors(), Matchers.empty());

        verify(setupPersister).progessSetupStep();

        verify(propertyManager, times(2)).setProperty(anyString(), anyString()); // the mail templates
        verify(propertyManager).setSMTPServer(any(SMTPServer.class));
        verify(propertyManager).setNotificationEmail("notification@test");
    }
}
