package com.atlassian.crowd;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;
import org.tuckey.web.filters.urlrewrite.UrlRewriteWrappedResponse;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUrlRewriteFilter
{
    private static UrlRewriteFilter loadFilterFromCrowdUrlRewriteConfig() throws ServletException, FileNotFoundException
    {
        UrlRewriteFilter f = new UrlRewriteFilter();

        File configFile = new File("src/main/webapp/WEB-INF/urlrewrite.xml");

        ServletContext sc = mock(ServletContext.class);
        when(sc.getResourceAsStream("/WEB-INF/urlrewrite.xml")).thenReturn(new FileInputStream(configFile));

        FilterConfig fc = mock(FilterConfig.class);
        when(fc.getServletContext()).thenReturn(sc);

        f.init(fc);

        return f;
    }

    private static HttpServletRequest mockRequestFor(String requestUri)
    {
        HttpServletRequest req = mock(HttpServletRequest.class);

        when(req.getContextPath()).thenReturn("");
        when(req.getRequestURI()).thenReturn(requestUri);

        return req;
    }

    private void assertRequestUrlIsRewrittenTo(String requestUri, String forwardedUri) throws ServletException, IOException
    {
        UrlRewriteFilter f = loadFilterFromCrowdUrlRewriteConfig();

        HttpServletRequest req = mockRequestFor(requestUri);

        RequestDispatcher reqDispatcher = mock(RequestDispatcher.class);

        when(req.getRequestDispatcher(Mockito.anyString())).thenReturn(reqDispatcher);

        HttpServletResponse resp = mock(HttpServletResponse.class);
        FilterChain chain = mock(FilterChain.class);
        f.doFilter(req, resp, chain);

        /* The request is forwarded to the download servlet */
        verify(req).getRequestDispatcher(forwardedUri);
        verify(reqDispatcher).forward(eq(req), Mockito.<UrlRewriteWrappedResponse>any(UrlRewriteWrappedResponse.class));
    }

    @Test
    public void rewriteFilterRewritesAValidDownloadRequest() throws Exception
    {
        assertRequestUrlIsRewrittenTo("/s//_/download/resource-to-download", "/download/resource-to-download");
    }

    @Test
    public void rewriteFilterRewritesAValidDownloadRequestThatIncludesDoubleDots() throws Exception
    {
        assertRequestUrlIsRewrittenTo("/s//_/download/resource..to..download", "/download/resource..to..download");
    }

    @Test
    public void rewriteFilterRewritesAValidDownloadRequestThatIncludesAColon() throws Exception
    {
        assertRequestUrlIsRewrittenTo("/s//_/download/resource:to:download", "/download/resource:to:download");
    }

    @Test
    public void rewriteFilterRewritesAValidDownloadRequestThatIncludesAPercentEncodedDot() throws Exception
    {
        assertRequestUrlIsRewrittenTo("/s//_/download/%2E./resource:to:download", "/download/%2E./resource:to:download");
    }

    @Test
    public void rewriteFilterFailsWhenDownloadRequestIncludesParentPathSegment() throws Exception
    {
        UrlRewriteFilter f = loadFilterFromCrowdUrlRewriteConfig();

        HttpServletRequest req = mock(HttpServletRequest.class);

        when(req.getContextPath()).thenReturn("");

        when(req.getRequestURI()).thenReturn("/s//_/download/../WEB-INF/resource-to-download");

        RequestDispatcher reqDispatcher = mock(RequestDispatcher.class);

        when(req.getRequestDispatcher(Mockito.anyString())).thenReturn(reqDispatcher);

        HttpServletResponse resp = mock(HttpServletResponse.class);
        FilterChain chain = mock(FilterChain.class);
        f.doFilter(req, resp, chain);

        /* The request is passed on to the next filter in the chain */
        verify(req, never()).getRequestDispatcher(anyString());
        verify(chain).doFilter(eq(req), Mockito.<UrlRewriteWrappedResponse>any(UrlRewriteWrappedResponse.class));
    }
}
