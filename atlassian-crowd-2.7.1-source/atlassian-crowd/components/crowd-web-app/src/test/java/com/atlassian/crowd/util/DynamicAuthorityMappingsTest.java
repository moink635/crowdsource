package com.atlassian.crowd.util;

import java.util.Collections;
import java.util.Iterator;
import java.util.Map;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.service.client.ClientProperties;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;

import org.hamcrest.Matchers;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DynamicAuthorityMappingsTest
{
    private static final String APPLICATION_NAME = "crowd";
    private static final String ADMIN_AUTHORITY = "SOME_ROLE_ADMIN";
    private static final String GROUP1 = "group1";
    private static final String GROUP2 = "group2";
    private static final String GROUP3 = "group3";

    private DynamicAuthorityMappings dynamicAuthorityMappings;

    @Mock private ClientProperties clientProperties;
    @Mock private ApplicationManager applicationManager;
    @Mock private Application application;
    @Mock private DirectoryMapping directoryMapping1, directoryMapping2;
    @Mock private GroupMapping groupMapping1, groupMapping2, groupMapping3, groupMapping4;

    @Before
    public void setUp()
    {
        when(clientProperties.getApplicationName()).thenReturn(APPLICATION_NAME);

        dynamicAuthorityMappings = new DynamicAuthorityMappings(clientProperties, applicationManager, ADMIN_AUTHORITY);
    }

    @Test
    public void testApplicationDoesNotExist() throws Exception
    {
        when(applicationManager.findByName(APPLICATION_NAME)).thenThrow(new ApplicationNotFoundException(APPLICATION_NAME));

        assertThat(dynamicAuthorityMappings, Matchers.emptyIterable());
    }

    @Test
    public void testMultipleMappings() throws Exception
    {
        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);
        when(application.getDirectoryMappings()).thenReturn(ImmutableList.of(directoryMapping1, directoryMapping2));
        when(directoryMapping1.getAuthorisedGroups()).thenReturn(ImmutableSet.<GroupMapping>of(groupMapping1, groupMapping2));
        when(directoryMapping2.getAuthorisedGroups()).thenReturn(ImmutableSet.<GroupMapping>of(groupMapping3, groupMapping4));
        when(groupMapping1.getGroupName()).thenReturn(GROUP1);
        when(groupMapping2.getGroupName()).thenReturn(GROUP2);
        when(groupMapping3.getGroupName()).thenReturn(GROUP3);
        when(groupMapping4.getGroupName()).thenReturn(GROUP1); // repeated

        assertThat(dynamicAuthorityMappings, Matchers.<Map.Entry<String, String>>hasItems(
            Maps.immutableEntry(GROUP1, ADMIN_AUTHORITY),
            Maps.immutableEntry(GROUP2, ADMIN_AUTHORITY),
            Maps.immutableEntry(GROUP3, ADMIN_AUTHORITY)));
    }

    @Test
    public void testMapIsDynamic() throws Exception
    {
        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);
        when(application.getDirectoryMappings())
            .thenReturn(ImmutableList.of(directoryMapping1))
            .thenReturn(ImmutableList.of(directoryMapping2));
        when(directoryMapping1.getAuthorisedGroups()).thenReturn(ImmutableSet.<GroupMapping>of(groupMapping1));
        when(directoryMapping2.getAuthorisedGroups()).thenReturn(ImmutableSet.<GroupMapping>of(groupMapping2));
        when(groupMapping1.getGroupName()).thenReturn(GROUP1);
        when(groupMapping2.getGroupName()).thenReturn(GROUP2);

        assertThat(dynamicAuthorityMappings, IsCollectionContaining.<Map.Entry<String, String>>hasItem(
            Maps.immutableEntry(GROUP1, ADMIN_AUTHORITY)));
        // ... the application groups are changed here ...
        assertThat(dynamicAuthorityMappings, IsCollectionContaining.<Map.Entry<String, String>>hasItem(
            Maps.immutableEntry(GROUP2, ADMIN_AUTHORITY)));
        verify(application, times(2)).getDirectoryMappings();
    }

    @Test
    public void testGroupIsRemovedAfterKeySetHasBeenRetrieved() throws Exception
    {
        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);
        when(application.getDirectoryMappings())
            .thenReturn(ImmutableList.of(directoryMapping1))
            .thenReturn(Collections.<DirectoryMapping>emptyList());
        when(directoryMapping1.getAuthorisedGroups()).thenReturn(ImmutableSet.<GroupMapping>of(groupMapping1));
        when(groupMapping1.getGroupName()).thenReturn(GROUP1);

        Iterator<Map.Entry<String, String>> keySetSnapshot = dynamicAuthorityMappings.iterator();
        // ... the application groups are changed here ...
        assertFalse(dynamicAuthorityMappings.iterator().hasNext());
        // but we still can iterate the original iterator
        assertTrue(keySetSnapshot.hasNext());
        verify(application, times(2)).getDirectoryMappings();
    }
}
