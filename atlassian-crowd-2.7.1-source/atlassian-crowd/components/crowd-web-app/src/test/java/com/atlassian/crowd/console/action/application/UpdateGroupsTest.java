package com.atlassian.crowd.console.action.application;

import java.util.Arrays;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.user.UserWithAttributes;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdateGroupsTest
{
    private static final long DIRECTORY_ID = 1L;
    private static final String APPLICATION_NAME = "crowd";
    private static final long APPLICATION_ID = 2L;
    private static final String CROWD_ADMINISTRATORS_GROUP_NAME = "crowd-administrators";
    private static final String ADMIN_USERNAME = "admin";

    @Spy private UpdateGroups updateGroups;
    @Mock private ApplicationManager applicationManager;
    @Mock private Directory directory;
    @Mock private Application application;
    @Mock private DirectoryMapping directoryMapping;
    @Mock private UserWithAttributes admin;

    @Before
    public void setUp() throws Exception
    {
        updateGroups.setApplicationManager(applicationManager);
        when(applicationManager.findById(APPLICATION_ID)).thenReturn(application);
        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);
        when(application.getId()).thenReturn(APPLICATION_ID);
        when(directory.getId()).thenReturn(DIRECTORY_ID);
        when(directoryMapping.getDirectory()).thenReturn(directory);
        when(application.getDirectoryMapping(DIRECTORY_ID)).thenReturn(directoryMapping);
        when(admin.getName()).thenReturn(ADMIN_USERNAME);
        when(admin.getDirectoryId()).thenReturn(DIRECTORY_ID);
    }

    @Test
    public void doRemoveGroupThatDoesNotPreventTheAdminFromBeingLockedOut() throws Exception
    {
        updateGroups.setName(APPLICATION_NAME);
        updateGroups.setRemoveName(CROWD_ADMINISTRATORS_GROUP_NAME);
        updateGroups.setRemoveDirectoryID(DIRECTORY_ID);

        // spy superclass methods
        doReturn(admin).when(updateGroups).getRemoteUser();
        doReturn(true).when(updateGroups).authorisedToAccessCrowdAdminConsole(directoryMapping, DIRECTORY_ID, ADMIN_USERNAME);

        assertEquals(BaseAction.SUCCESS, updateGroups.doRemoveGroup());

        assertTrue("There should be no errors", updateGroups.getActionErrors().isEmpty());
        verify(applicationManager).removeGroupMapping(application, directory, CROWD_ADMINISTRATORS_GROUP_NAME);
        verify(applicationManager, never()).addGroupMapping(application, directory, CROWD_ADMINISTRATORS_GROUP_NAME);
    }

    @Test
    public void doRemoveGroupThatPreventsTheAdminFromBeingLockedOut() throws Exception
    {
        updateGroups.setName(APPLICATION_NAME);
        updateGroups.setRemoveName(CROWD_ADMINISTRATORS_GROUP_NAME);
        updateGroups.setRemoveDirectoryID(DIRECTORY_ID);

        // spy superclass methods
        doReturn(admin).when(updateGroups).getRemoteUser();
        doReturn(false).when(updateGroups).authorisedToAccessCrowdAdminConsole(directoryMapping, DIRECTORY_ID, ADMIN_USERNAME);

        assertEquals(BaseAction.INPUT, updateGroups.doRemoveGroup());

        assertEquals(Arrays.asList(updateGroups.getText("preventlockout.unassigngroup.label",
                                                        Arrays.asList(CROWD_ADMINISTRATORS_GROUP_NAME))),
                     updateGroups.getActionErrors());
        verify(applicationManager).removeGroupMapping(application, directory, CROWD_ADMINISTRATORS_GROUP_NAME);
        verify(applicationManager).addGroupMapping(application, directory, CROWD_ADMINISTRATORS_GROUP_NAME);
    }
}
