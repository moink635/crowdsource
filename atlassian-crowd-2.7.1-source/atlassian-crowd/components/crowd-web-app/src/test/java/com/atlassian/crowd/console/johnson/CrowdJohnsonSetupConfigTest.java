package com.atlassian.crowd.console.johnson;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;

import static org.junit.Assert.assertTrue;

public class CrowdJohnsonSetupConfigTest
{
    CrowdJohnsonSetupConfig config;

    @Before
    public void setUp()
    {
        config = new CrowdJohnsonSetupConfig();
        config.init(Collections.<String, String>emptyMap());
    }

    @Test
    public void nullIsIgnored()
    {
        assertTrue(config.isSetupPage(null));
    }

    @Test
    public void setupJspsAreIgnored()
    {
        assertTrue(config.isSetupPage("/console/setup/test.jsp"));
    }

    @Test
    public void setupActionsAreIgnored()
    {
        assertTrue(config.isSetupPage("/console/setup/test.action"));
    }

    @Test
    public void otherPagesAreNotIgnored()
    {
        assertFalse(config.isSetupPage("/console/setup/test.other-suffix"));
        assertFalse(config.isSetupPage("/other-prefix/test.jsp"));
        assertFalse(config.isSetupPage("/other-prefix/test.action"));
    }
}
