package com.atlassian.crowd.console.action.user;

import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.service.client.ClientProperties;

import com.google.common.collect.ImmutableList;
import com.opensymphony.webwork.ServletActionContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EditProfileTest
{
    private EditProfile editProfile;

    @Mock private CrowdBootstrapManager bootstrapManager;
    @Mock private ApplicationService applicationService;
    @Mock private ApplicationManager applicationManager;
    @Mock private ClientProperties clientProperties;
    @Mock private Application application;

    private MockHttpServletRequest request;

    @Before
    public void createObjectUnderTest() throws Exception
    {
        editProfile = new EditProfile()
        {
            @Override
            protected CrowdBootstrapManager getBootstrapManager()
            {
                return bootstrapManager;
            }

            @Override
            public String getRemoteUsername() throws InvalidUserException
            {
                return "user";
            }
        };
        editProfile.setApplicationService(applicationService);
        editProfile.setApplicationManager(applicationManager);
        editProfile.setClientProperties(clientProperties);
    }

    @Before
    public void prepareMocks() throws Exception
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(true);

        when(clientProperties.getApplicationName()).thenReturn("crowd");

        when(applicationManager.findByName("crowd")).thenReturn(application);

        request = new MockHttpServletRequest();
        ServletActionContext.setRequest(request);
    }

    @After
    public void disposeRequest() throws Exception
    {
        ServletActionContext.setRequest(null);
    }

    @Test
    public void successfullyEditProfile() throws Exception
    {
        editProfile.setUsername("user");
        editProfile.setEmail("john@example.test");
        editProfile.setFirstname("John");
        editProfile.setLastname("Doe");

        assertEquals(EditProfile.SUCCESS, editProfile.doUpdate());

        ArgumentCaptor<UserTemplate> userTemplate = ArgumentCaptor.forClass(UserTemplate.class);
        verify(applicationService).updateUser(eq(application), userTemplate.capture());

        assertEquals("user", userTemplate.getValue().getName());
        assertEquals("john@example.test", userTemplate.getValue().getEmailAddress());
        assertEquals("John", userTemplate.getValue().getFirstName());
        assertEquals("Doe", userTemplate.getValue().getLastName());

        assertEquals("true", request.getAttribute("updateSuccessful"));
    }

    @Test
    public void emptyEmailIsInvalid() throws Exception
    {
        assertEquals(EditProfile.INPUT, editProfile.doUpdate());

        assertEquals(ImmutableList.of(editProfile.getText("principal.email.invalid")),
                     editProfile.getFieldErrors().get("email"));
    }

    @Test
    public void emailWithoutAtIsInvalid() throws Exception
    {
        editProfile.setEmail("invalid-email");

        assertEquals(EditProfile.INPUT, editProfile.doUpdate());

        assertEquals(ImmutableList.of(editProfile.getText("principal.email.invalid")),
                     editProfile.getFieldErrors().get("email"));
    }

    @Test
    public void emailWithTrailingWhitespaceIsInvalid() throws Exception
    {
        editProfile.setEmail(" fred@example.test ");

        assertEquals(EditProfile.INPUT, editProfile.doUpdate());

        assertEquals(ImmutableList.of(editProfile.getText("principal.email.whitespace")),
                     editProfile.getFieldErrors().get("email"));
    }
}
