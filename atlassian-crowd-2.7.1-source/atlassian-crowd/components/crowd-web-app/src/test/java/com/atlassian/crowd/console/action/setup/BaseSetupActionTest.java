package com.atlassian.crowd.console.action.setup;

import com.atlassian.crowd.console.action.ActionHelper;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BaseSetupActionTest
{
    private CrowdBootstrapManager bootstrapManager;
    private BaseSetupAction action;

    @Before
    public void setUp()
    {
        ActionHelper actionHelper = mock(ActionHelper.class);
        bootstrapManager = mock(CrowdBootstrapManager.class);

        action = new BaseSetupAction()
        {
            @Override
            public String getStepName()
            {
                throw new UnsupportedOperationException();
            }
        };

        action.setActionHelper(actionHelper);
        when(actionHelper.getBootstrapManager()).thenReturn(bootstrapManager);
    }

    @Test
    public void buildEnvironmentConsideredOkay()
    {
        when(bootstrapManager.isApplicationHomeValid()).thenReturn(true);
        assertTrue(action.isEverythingOk());
    }

    @Test
    public void buildEnvironmentNotConsideredOkayWhenApplicationHomeNotValid()
    {
        when(bootstrapManager.isApplicationHomeValid()).thenReturn(false);
        assertFalse(action.isEverythingOk());
    }

    @Test
    public void individualRequirementChecksAllTrue()
    {
        assertTrue(action.isJdk15());
        assertTrue(action.isJdk16());
        assertTrue(action.isServlet24());
    }

    @Test
    public void classExistenceCheckFailsForMissingClass()
    {
        assertFalse(action.isClassFound("no.such.class"));
    }

    @Test
    public void classExistenceCheckPassesForRealClass()
    {
        assertTrue(action.isClassFound("java.lang.Object"));
    }
}
