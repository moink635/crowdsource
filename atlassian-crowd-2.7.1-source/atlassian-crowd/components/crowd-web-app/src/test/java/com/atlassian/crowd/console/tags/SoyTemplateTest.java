package com.atlassian.crowd.console.tags;

import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.opensymphony.webwork.WebWorkException;
import com.opensymphony.xwork.util.OgnlValueStack;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.StringWriter;
import java.io.Writer;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyMap;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * Tests for {@link SoyTemplate}
 *
 * @since v2.7
 */
@RunWith(MockitoJUnitRunner.class)
public class SoyTemplateTest
{
    @Mock private SoyTemplateRenderer soyTemplateRenderer;
    @Rule public ExpectedException exceptions = ExpectedException.none();

    @Test
    public void moduleKeyMustNotBeNull() throws Exception
    {
        exceptions.expect(WebWorkException.class);

        Writer writer = new StringWriter();
        final SoyTemplate tag = tag(null, "template");

        tag.start(writer);
        tag.end(writer, "body");
    }

    @Test
    public void moduleKeyMustNotBeEmpty() throws Exception
    {
        exceptions.expect(WebWorkException.class);

        Writer writer = new StringWriter();
        final SoyTemplate tag = tag("", "template");

        tag.start(writer);
        tag.end(writer, "body");
    }

    @Test
    public void templateMustNotBeNull() throws Exception
    {
        exceptions.expect(WebWorkException.class);

        Writer writer = new StringWriter();
        final SoyTemplate tag = tag("moduleKey", null);

        tag.start(writer);
        tag.end(writer, "body");
    }

    @Test
    public void templateMustNotBeEmpty() throws Exception
    {
        exceptions.expect(WebWorkException.class);

        Writer writer = new StringWriter();
        final SoyTemplate tag = tag("moduleKey", "");

        tag.start(writer);
        tag.end(writer, "body");
    }

    @Test
    public void testSuccessfulRenderShouldPrintTemplateContent() throws Exception
    {
        when(soyTemplateRenderer.render(eq("moduleKey"), eq("template"), anyMap())).thenReturn("template content");
        Writer writer = new StringWriter();
        final SoyTemplate tag = tag("moduleKey", "template");

        tag.start(writer);
        tag.end(writer, "body");

        assertThat(writer.toString(), is("template content"));
    }

    @Test
    public void failedRenderShouldThrowAnException() throws Exception
    {
        final SoyException soyException = new SoyException("Something went wrong!");
        exceptions.expect(WebWorkException.class);
        exceptions.expectCause(is(soyException));
        when(soyTemplateRenderer.render(eq("moduleKey"), eq("template"), anyMap())).thenThrow(soyException);
        Writer writer = new StringWriter();
        final SoyTemplate tag = tag("moduleKey", "template");

        tag.start(writer);
        tag.end(writer, "body");
    }

    private SoyTemplate tag(final String moduleKey, final String template)
    {
        return new SoyTemplate(new OgnlValueStack(), moduleKey, template)
        {
            @Override
            protected SoyTemplateRenderer getSoyRenderer() throws SoyException
            {
                return soyTemplateRenderer;
            }
        };
    }
}
