package com.atlassian.crowd.console.filter;

import java.io.IOException;

import javax.servlet.ServletException;

import com.atlassian.crowd.manager.license.CrowdLicenseManager;
import com.atlassian.extras.api.crowd.CrowdLicense;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockFilterChain;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * LicenceFilter Tester.
 */
public class LicenceFilterTest
{
    private LicenceFilter licenceFilter;
    protected CrowdLicense validUnlimitedLicense;
    protected CrowdLicenseManager mockLicenseManager;
    protected MockHttpServletRequest request;
    protected MockFilterChain mockFilterChain;
    protected MockHttpServletResponse response;

    @Before
    public void setUp() throws Exception
    {
        licenceFilter = new LicenceFilter();

        licenceFilter.afterPropertiesSet();

        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        mockFilterChain = new MockFilterChain();

        // Valid Unlimited Commercial license
        validUnlimitedLicense = mock(CrowdLicense.class);

        mockLicenseManager = mock(CrowdLicenseManager.class);

        licenceFilter.setCrowdLicenseManager(mockLicenseManager);
    }

    @Test
    public void testDoFilterInternalWithValidLicense() throws IOException, ServletException
    {
        // Expectations
        when(mockLicenseManager.getLicense()).thenReturn(validUnlimitedLicense);
        when(mockLicenseManager.isLicenseValid(validUnlimitedLicense)).thenReturn(true);

        licenceFilter.doFilterInternal(request, response, mockFilterChain);

        verify(mockLicenseManager).getLicense();
        verify(mockLicenseManager).isLicenseValid(validUnlimitedLicense);
    }

    @Test
    public void testDoFilterInternalWithInvalidLicense() throws IOException, ServletException
    {
        // Expectations
        when(mockLicenseManager.getLicense()).thenReturn(validUnlimitedLicense);
        when(mockLicenseManager.isLicenseValid(validUnlimitedLicense)).thenReturn(false);

        licenceFilter.doFilterInternal(request, response, mockFilterChain);

        verify(mockLicenseManager).getLicense();
        verify(mockLicenseManager).isLicenseValid(validUnlimitedLicense);

        assertEquals(LicenceFilter.UPDATE_LICENSE_DEFAULT_PATH, response.getRedirectedUrl());
    }

    @Test
    public void testShouldNotFilterLicenceURI() throws ServletException
    {
        request.setRequestURI("/console/license.action");

        boolean filterUri = licenceFilter.shouldNotFilter(request);

        assertTrue("We should not filter the license URI", filterUri);
    }

    @Test
    public void testShouldFilterConsoleLoginURI() throws ServletException
    {
        request.setRequestURI("/console/login.action");

        boolean filterUri = licenceFilter.shouldNotFilter(request);

        assertFalse("We should filter the login URI", filterUri);
    }

    @Test
    public void testWeShouldNotFilterTheSetupLicenseURI() throws ServletException
    {
        request.setRequestURI("/console/setup/setuplicense.action");

        boolean filterUri = licenceFilter.shouldNotFilter(request);

        assertTrue("We should filter the login URI", filterUri);
    }

    @After
    public void tearDown() throws Exception
    {
        licenceFilter = null;
        mockLicenseManager = null;
        validUnlimitedLicense = null;
        request = null;
        response = null;
        mockFilterChain = null;
    }
}
