package com.atlassian.crowd.console.action.group;

import java.util.List;
import java.util.Set;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.ReadOnlyGroupException;
import com.atlassian.crowd.manager.directory.BulkAddResult;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.AdminGroupChecker;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdateGroupMembersTest
{
    private static final Long DIRECTORY_ID = 1L;
    private static final String CURRENT_GROUP_NAME = "currentGroup";
    private static final ImmutableList<User> NO_USERS = ImmutableList.of();

    private static final MembershipQuery<Group> CHILD_GROUPS_QUERY =
        QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP))
            .childrenOf(EntityDescriptor.group(GroupType.GROUP))
            .withName(CURRENT_GROUP_NAME)
            .returningAtMost(EntityQuery.ALL_RESULTS);

    private static final MembershipQuery<User> CHILD_USERS_QUERY =
        QueryBuilder.queryFor(User.class, EntityDescriptor.user())
            .childrenOf(EntityDescriptor.group(GroupType.GROUP))
            .withName(CURRENT_GROUP_NAME)
            .returningAtMost(EntityQuery.ALL_RESULTS);

    private static final EntityQuery<Group> ALL_GROUPS_QUERY =
        QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP))
            .returningAtMost(EntityQuery.ALL_RESULTS);
    public static final ImmutableSet<Group> NO_GROUPS = ImmutableSet.<Group>of();

    private UpdateGroupMembers updateGroupMembers;
    @Mock private DirectoryManager directoryManager;
    @Mock private Directory directory;
    @Mock private DirectoryInstanceLoader directoryInstanceLoader;
    @Mock private RemoteDirectory remoteDirectory;
    @Mock private AdminGroupChecker adminGroupChecker;

    private Group currentGroup;
    private Group newChildGroup;
    private Group existingChildGroup;
    private Group nonMemberGroup;
    private User existingUser;
    private User newUser1;
    private User newUser2;

    @Before
    public void createObjectUnderTest() throws Exception
    {
        updateGroupMembers = new UpdateGroupMembers();
        updateGroupMembers.setDirectoryManager(directoryManager);
        updateGroupMembers.setDirectoryInstanceLoader(directoryInstanceLoader);
        updateGroupMembers.setAdminGroupChecker(adminGroupChecker);
    }

    @Before
    public void createTemplates()
    {
        currentGroup = new GroupTemplate("currentGroup");
        newChildGroup = new GroupTemplate("newChildGroup");
        existingChildGroup = new GroupTemplate("existingChildGroup");
        nonMemberGroup = new GroupTemplate("nonMemberGroup");

        existingUser = new UserTemplate("existingUser");
        newUser1 = new UserTemplate("newUser1");
        newUser2 = new UserTemplate("newUser2");
    }

    @Test
    public void addUsersToGroupWhenAllUsersCanBeAdded() throws Exception
    {
        updateGroupMembers.setDirectoryID(DIRECTORY_ID);
        updateGroupMembers.setEntityName(CURRENT_GROUP_NAME);
        updateGroupMembers.setSelectedEntityNames(ImmutableList.of("newUser1", "newUser2"));

        prepareDirectory(false);

        BulkAddResult<String> bulkAddResult = mock(BulkAddResult.class);
        when(directoryManager.addAllUsersToGroup(DIRECTORY_ID, ImmutableList.of("newUser1", "newUser2"),
                                                 CURRENT_GROUP_NAME))
            .thenReturn(bulkAddResult);
        when(bulkAddResult.getFailedEntities()).thenReturn(ImmutableList.<String>of());

        assertEquals(UpdateGroupMembers.SUCCESS, updateGroupMembers.doAddUsers());

        assertInformationRequiredForRedirection(CURRENT_GROUP_NAME, false);
    }

    @Test
    public void addUsersToGroupWhenSomeUsersCannotBeAdded() throws Exception
    {
        updateGroupMembers.setDirectoryID(DIRECTORY_ID);
        updateGroupMembers.setEntityName(CURRENT_GROUP_NAME);
        updateGroupMembers.setSelectedEntityNames(ImmutableList.of("newUser1", "newUser2"));

        prepareDirectory(false);

        BulkAddResult<String> bulkAddResult = mock(BulkAddResult.class);
        when(directoryManager.addAllUsersToGroup(DIRECTORY_ID, ImmutableList.of("newUser1", "newUser2"),
                                                 CURRENT_GROUP_NAME))
            .thenReturn(bulkAddResult);
        when(bulkAddResult.getFailedEntities()).thenReturn(ImmutableList.of("newUser1"));

        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, CHILD_USERS_QUERY))
            .thenReturn(ImmutableList.of(existingUser, newUser2));

        assertEquals(UpdateGroupMembers.ERROR, updateGroupMembers.doAddUsers());

        assertEquals("The following users could not be added to the group: newUser1",
                     Iterables.getOnlyElement(updateGroupMembers.getActionErrors()));

        assertInformationRequiredByNonNestedGroupsView(CURRENT_GROUP_NAME,
                                                       ImmutableList.of(existingUser, newUser2));
    }

    @Test
    public void addUsersToGroupWhenBulkAddFailsForAnotherReason() throws Exception
    {
        updateGroupMembers.setDirectoryID(DIRECTORY_ID);
        updateGroupMembers.setEntityName(CURRENT_GROUP_NAME);
        updateGroupMembers.setSelectedEntityNames(ImmutableList.of("newUser1"));

        prepareDirectory(false);

        BulkAddResult<String> bulkAddResult = mock(BulkAddResult.class);
        when(directoryManager.addAllUsersToGroup(DIRECTORY_ID, ImmutableList.of("newUser1"),
                                                 CURRENT_GROUP_NAME))
            .thenThrow(new OperationFailedException("something went wrong"));
        when(bulkAddResult.getFailedEntities()).thenReturn(ImmutableList.<String>of());

        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, CHILD_USERS_QUERY))
            .thenReturn(ImmutableList.of(existingUser, newUser2));

        assertEquals(UpdateGroupMembers.ERROR, updateGroupMembers.doAddUsers());

        assertEquals("something went wrong", Iterables.getOnlyElement(updateGroupMembers.getActionErrors()));

        assertInformationRequiredByNonNestedGroupsView(CURRENT_GROUP_NAME,
                                                       ImmutableList.of(existingUser, newUser2));
    }

    @Test
    public void removeUsersFromGroupShouldSuceeedWhenItDoesNotAffectTheAdminUser() throws Exception
    {
        updateGroupMembers.setDirectoryID(DIRECTORY_ID);
        updateGroupMembers.setEntityName(CURRENT_GROUP_NAME);
        updateGroupMembers.setSelectedEntityNames(ImmutableList.of("existingUser"));

        prepareDirectory(false);

        when(adminGroupChecker.isRemovingCrowdConsoleAdminMembership("loggedInUser", DIRECTORY_ID,
                                                                     ImmutableList.of("existingUser"), DIRECTORY_ID))
            .thenReturn(false);

        // due to static dependencies in BaseAction, this test method does not
        // use the test instance, but a spy. The spy assumes that BaseAction.getRemoteUser()
        // is correct
        updateGroupMembers = spy(updateGroupMembers);
        doReturn(UserTemplateWithAttributes.ofUserWithNoAttributes(new UserTemplate("loggedInUser", DIRECTORY_ID)))
            .when(updateGroupMembers).getRemoteUser();

        assertEquals(UpdateGroupMembers.SUCCESS, updateGroupMembers.doRemoveUsers());

        assertInformationRequiredForRedirection(CURRENT_GROUP_NAME, false);
    }

    @Test
    public void removeUsersFromGroupShouldSuceeedWhenThereIsAnotherAdminUser() throws Exception
    {
        updateGroupMembers.setDirectoryID(DIRECTORY_ID);
        updateGroupMembers.setEntityName(CURRENT_GROUP_NAME);
        updateGroupMembers.setSelectedEntityNames(ImmutableList.of("existingUser"));

        prepareDirectory(false);

        when(adminGroupChecker.isRemovingCrowdConsoleAdminMembership("loggedInUser", DIRECTORY_ID,
                                                                     ImmutableList.of("existingUser"), DIRECTORY_ID))
            .thenReturn(true);

        when(adminGroupChecker.isRemovingConsoleAdminFromLastAdminGroup(CURRENT_GROUP_NAME,
                                                                        "loggedInUser",
                                                                        DIRECTORY_ID))
            .thenReturn(false);

        // due to static dependencies in BaseAction, this test method does not
        // use the test instance, but a spy. The spy assumes that BaseAction.getRemoteUser()
        // is correct
        updateGroupMembers = spy(updateGroupMembers);
        doReturn(UserTemplateWithAttributes.ofUserWithNoAttributes(new UserTemplate("loggedInUser", DIRECTORY_ID)))
            .when(updateGroupMembers).getRemoteUser();

        assertEquals(UpdateGroupMembers.SUCCESS, updateGroupMembers.doRemoveUsers());

        assertInformationRequiredForRedirection(CURRENT_GROUP_NAME, false);

        verify(directoryManager).removeUserFromGroup(DIRECTORY_ID, "existingUser", CURRENT_GROUP_NAME);
    }

    @Test
    public void removeUsersFromGroupShouldPreventLockOut() throws Exception
    {
        updateGroupMembers.setDirectoryID(DIRECTORY_ID);
        updateGroupMembers.setEntityName(CURRENT_GROUP_NAME);
        updateGroupMembers.setSelectedEntityNames(ImmutableList.of("existingUser", "loggedInUser"));

        prepareDirectory(false);

        when(adminGroupChecker.isRemovingCrowdConsoleAdminMembership("loggedInUser", DIRECTORY_ID,
                                                                     ImmutableList.of("existingUser", "loggedInUser"),
                                                                     DIRECTORY_ID))
            .thenReturn(true);

        when(adminGroupChecker.isRemovingConsoleAdminFromLastAdminGroup(CURRENT_GROUP_NAME, "loggedInUser", DIRECTORY_ID))
            .thenReturn(true);

        // due to static dependencies in BaseAction, this test method does not
        // use the test instance, but a spy. The spy assumes that BaseAction.getRemoteUser()
        // is correct
        updateGroupMembers = spy(updateGroupMembers);
        doReturn(UserTemplateWithAttributes.ofUserWithNoAttributes(new UserTemplate("loggedInUser", DIRECTORY_ID)))
            .when(updateGroupMembers).getRemoteUser();

        assertEquals(UpdateGroupMembers.SUCCESS, updateGroupMembers.doRemoveUsers());

        assertInformationRequiredForRedirection(CURRENT_GROUP_NAME, true);

        verify(directoryManager).removeUserFromGroup(DIRECTORY_ID, "existingUser", CURRENT_GROUP_NAME);
        verify(directoryManager, never()).removeUserFromGroup(DIRECTORY_ID, "loggedInUser", CURRENT_GROUP_NAME);
    }

    @Test
    public void removeUsersFromReadOnlyGroupShouldFail() throws Exception
    {
        updateGroupMembers.setDirectoryID(DIRECTORY_ID);
        updateGroupMembers.setEntityName(CURRENT_GROUP_NAME);
        updateGroupMembers.setSelectedEntityNames(ImmutableList.of("existingUser"));

        prepareDirectory(false);

        when(adminGroupChecker.isRemovingCrowdConsoleAdminMembership("loggedInUser", DIRECTORY_ID,
                                                                     "existingUser", DIRECTORY_ID))
            .thenReturn(false);

        doThrow(new ReadOnlyGroupException(CURRENT_GROUP_NAME))
            .when(directoryManager).removeUserFromGroup(DIRECTORY_ID, "existingUser", CURRENT_GROUP_NAME);

        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, CHILD_USERS_QUERY))
            .thenReturn(ImmutableList.of(existingUser));

        // due to static dependencies in BaseAction, this test method does not
        // use the test instance, but a spy. The spy assumes that BaseAction.getRemoteUser()
        // is correct
        updateGroupMembers = spy(updateGroupMembers);
        doReturn(UserTemplateWithAttributes.ofUserWithNoAttributes(new UserTemplate("loggedInUser", DIRECTORY_ID)))
            .when(updateGroupMembers).getRemoteUser();

        assertEquals(UpdateGroupMembers.ERROR, updateGroupMembers.doRemoveUsers());

        assertEquals("Group <currentGroup> is read-only and cannot be updated",
                     Iterables.getOnlyElement(updateGroupMembers.getActionErrors()));

        assertInformationRequiredByNonNestedGroupsView(CURRENT_GROUP_NAME, ImmutableList.of(existingUser));
    }
    @Test
    public void addGroupsToGroupShouldSucceed() throws Exception
    {
        updateGroupMembers.setDirectoryID(DIRECTORY_ID);
        updateGroupMembers.setEntityName(CURRENT_GROUP_NAME);
        updateGroupMembers.setSelectedEntityNames(ImmutableList.of("newChildGroup"));

        prepareDirectory(true);

        assertEquals(UpdateGroupMembers.SUCCESS, updateGroupMembers.doAddGroups());

        verify(directoryManager).addGroupToGroup(DIRECTORY_ID, "newChildGroup", CURRENT_GROUP_NAME);

        assertInformationRequiredForRedirection(CURRENT_GROUP_NAME, false);
    }

    @Test
    public void addGroupsToAReadOnlyGroupShouldFail() throws Exception
    {
        updateGroupMembers.setDirectoryID(DIRECTORY_ID);
        updateGroupMembers.setEntityName(CURRENT_GROUP_NAME);
        updateGroupMembers.setSelectedEntityNames(ImmutableList.of("newChildGroup"));

        prepareDirectory(true);

        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, CHILD_GROUPS_QUERY))
            .thenReturn(ImmutableList.of(existingChildGroup)); // childGroup was not added

        when(directoryManager.searchGroups(DIRECTORY_ID, ALL_GROUPS_QUERY))
            .thenReturn(ImmutableList.of(currentGroup, nonMemberGroup));

        doThrow(new ReadOnlyGroupException(CURRENT_GROUP_NAME))
            .when(directoryManager).addGroupToGroup(DIRECTORY_ID, "newChildGroup", CURRENT_GROUP_NAME);

        assertEquals(UpdateGroupMembers.ERROR, updateGroupMembers.doAddGroups());

        assertEquals("Group <currentGroup> is read-only and cannot be updated",
                     Iterables.getOnlyElement(updateGroupMembers.getActionErrors()));

        assertInformationRequiredByNestedGroupsView(CURRENT_GROUP_NAME, NO_USERS,
                                                    ImmutableSet.of(existingChildGroup), // childGroup was not added
                                                    ImmutableSet.of(nonMemberGroup));
    }

    @Test
    public void removeGroupsFromGroupShouldSucceed() throws Exception
    {
        updateGroupMembers.setDirectoryID(DIRECTORY_ID);
        updateGroupMembers.setEntityName(CURRENT_GROUP_NAME);
        updateGroupMembers.setSelectedEntityNames(ImmutableList.of("existingChildGroup"));

        prepareDirectory(true);

        assertEquals(UpdateGroupMembers.SUCCESS, updateGroupMembers.doRemoveGroups());

        verify(directoryManager).removeGroupFromGroup(DIRECTORY_ID, "existingChildGroup", CURRENT_GROUP_NAME);

        assertInformationRequiredForRedirection(CURRENT_GROUP_NAME, false);
    }

    @Test
    public void removeGroupsFromReadOnlyGroupShouldFail() throws Exception
    {
        updateGroupMembers.setDirectoryID(DIRECTORY_ID);
        updateGroupMembers.setEntityName(CURRENT_GROUP_NAME);
        updateGroupMembers.setSelectedEntityNames(ImmutableList.of("existingChildGroup"));

        prepareDirectory(true);

        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, CHILD_GROUPS_QUERY))
            .thenReturn(ImmutableList.of(existingChildGroup)); // it was not removed

        when(directoryManager.searchGroups(DIRECTORY_ID, ALL_GROUPS_QUERY))
            .thenReturn(ImmutableList.of(currentGroup, existingChildGroup, nonMemberGroup));

        doThrow(new ReadOnlyGroupException(CURRENT_GROUP_NAME))
            .when(directoryManager).removeGroupFromGroup(DIRECTORY_ID, "existingChildGroup", CURRENT_GROUP_NAME);

        assertEquals(UpdateGroupMembers.ERROR, updateGroupMembers.doRemoveGroups());

        assertEquals("Group <currentGroup> is read-only and cannot be updated",
                     Iterables.getOnlyElement(updateGroupMembers.getActionErrors()));

        assertInformationRequiredByNestedGroupsView(CURRENT_GROUP_NAME, NO_USERS,
                                                    ImmutableSet.of(existingChildGroup), // it was not removed
                                                    ImmutableSet.of(nonMemberGroup));
    }

    private void prepareDirectory(boolean supportsNestedGroups)
        throws DirectoryNotFoundException, DirectoryInstantiationException
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);
        when(directoryInstanceLoader.getDirectory(directory)).thenReturn(remoteDirectory);
        when(remoteDirectory.supportsNestedGroups()).thenReturn(supportsNestedGroups);
    }

    /**
     * On success, assert that the information required for the redirection is there (see xwork.xml).
     *
     * @param currentGroupName
     * @param preventingLockOut
     */
    private void assertInformationRequiredForRedirection(String currentGroupName,
                                                         boolean preventingLockOut)
    {
        assertEquals(DIRECTORY_ID, updateGroupMembers.getDirectoryID());
        assertEquals(currentGroupName, updateGroupMembers.getEntityName());  // note that this is not getGroupName()
        assertEquals(preventingLockOut, updateGroupMembers.isPreventingLockout());
    }

    /**
     * @see ViewGroupMembersTest#assertInformationRequiredByNonNestedGroupsView(String, java.util.List)
     */
    private void assertInformationRequiredByNonNestedGroupsView(String currentGroup, List<User> users)
        throws Exception
    {
        assertEquals(currentGroup, updateGroupMembers.getGroupName());
        assertFalse("Directory should not support nested groups", updateGroupMembers.isSupportsNestedGroups());
        assertEquals("Group members should be provided to the view", users, updateGroupMembers.getPrincipals());
        assertNull("The group should not have nested groups", updateGroupMembers.getSubGroups());
        assertNull("Non-member groups are not necessary", updateGroupMembers.getAllNonMemberGroups());

        // no need to query for subgroups when the directory does not support nested groups
        verify(directoryManager, never()).searchDirectGroupRelationships(DIRECTORY_ID, CHILD_GROUPS_QUERY);

        // no need to calculate non-member groups when the directory does not support nested groups
        verify(directoryManager, never()).searchGroups(anyLong(), any(EntityQuery.class));
    }

    /**
     * @see ViewGroupMembersTest#assertInformationRequiredByNestedGroupsView(String, java.util.List, java.util.Set, java.util.Set)
     */
    private void assertInformationRequiredByNestedGroupsView(String currentGroup,
                                                             List<User> users,
                                                             Set<Group> childGroups,
                                                             Set<Group> nonMemberGroups)
    {
        assertEquals(currentGroup, updateGroupMembers.getGroupName());
        assertTrue("Directory should support nested groups", updateGroupMembers.isSupportsNestedGroups());
        assertEquals("Group members should be provided to the view", users, updateGroupMembers.getPrincipals());
        assertEquals("Subgroups should be provided to the view",
                     childGroups, ImmutableSet.copyOf(updateGroupMembers.getSubGroups()));
        assertEquals("Non-member groups should be provided to the view",
                     nonMemberGroups, ImmutableSet.copyOf(updateGroupMembers.getAllNonMemberGroups()));
    }
}
