package com.atlassian.crowd.console.action.group;

import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.GroupTemplate;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ValidationAware;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AddGroupTest
{
    private static final Long DIRECTORY_ID = 1L;
    private static final String GROUP_NAME = "group name";

    @Mock
    private DirectoryManager directoryManager;

    private AddGroup addGroup;

    @Before
    public void setUp()
    {
        addGroup = new AddGroup();
        addGroup.setDirectoryManager(directoryManager);
        addGroup.setDirectoryID(DIRECTORY_ID);

        ServletActionContext.setRequest(mock(HttpServletRequest.class));
        ServletActionContext.setResponse(mock(HttpServletResponse.class));
    }

    @After
    public void tearDown()
    {
        ServletActionContext.setRequest(null);
        ServletActionContext.setResponse(null);
    }

    @Test
    public void normalDescriptionIsAllowed() throws Exception
    {
        when(directoryManager.findGroupByName(DIRECTORY_ID, GROUP_NAME))
                .thenThrow(new GroupNotFoundException("Group doesn't exist!"));

        when(directoryManager.addGroup(eq(DIRECTORY_ID), any(GroupTemplate.class)))
                .thenReturn(new GroupTemplate(GROUP_NAME));

        addGroup.setName(GROUP_NAME);
        addGroup.setDescription("normal description");

        String actionResult = addGroup.doUpdate();

        assertEquals(ViewGroup.SUCCESS, actionResult);
        assertNull(getFieldErrorsFor(addGroup, "description"));
        assertThat(getActionErrorsFor(addGroup), empty());
    }

    @Test
    public void emptyDescriptionIsAllowed() throws Exception
    {
        when(directoryManager.findGroupByName(DIRECTORY_ID, GROUP_NAME))
                .thenThrow(new GroupNotFoundException("Group doesn't exist!"));

        when(directoryManager.addGroup(eq(DIRECTORY_ID), any(GroupTemplate.class)))
                .thenReturn(new GroupTemplate(GROUP_NAME));

        addGroup.setName(GROUP_NAME);
        addGroup.setDescription("");

        String actionResult = addGroup.doUpdate();

        assertEquals(ViewGroup.SUCCESS, actionResult);
        assertNull(getFieldErrorsFor(addGroup, "description"));
        assertThat(getActionErrorsFor(addGroup), empty());
    }

    @Test
    public void whitespaceDescriptionIsNotAllowed() throws Exception
    {
        when(directoryManager.findGroupByName(DIRECTORY_ID, GROUP_NAME))
                .thenThrow(new GroupNotFoundException("Group doesn't exist!"));

        when(directoryManager.addGroup(eq(DIRECTORY_ID), any(GroupTemplate.class)))
                .thenReturn(new GroupTemplate(GROUP_NAME));

        addGroup.setName(GROUP_NAME);
        addGroup.setDescription(" ");

        String actionResult = addGroup.doUpdate();

        assertEquals(ViewGroup.INPUT, actionResult);
        assertThat(getFieldErrorsFor(addGroup, "description"), contains(addGroup.getText("group.description.invalid")));
        assertThat(getActionErrorsFor(addGroup), empty());
    }

    // Helper methods to allow hamcrest matchers to be used with non-generic legacy code without making OpenJDK7 fall over

    @SuppressWarnings("unchecked")
    private static List<String> getFieldErrorsFor(ValidationAware updateGroup, String fieldName)
    {
        return (List<String>) updateGroup.getFieldErrors().get(fieldName);
    }

    @SuppressWarnings("unchecked")
    private static Collection<String> getActionErrorsFor(ValidationAware validationAware)
    {
        return (Collection<String>) validationAware.getActionErrors();
    }
}
