package com.atlassian.crowd.console.action.group;

import java.util.List;
import java.util.Set;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ViewGroupMembersTest
{
    private static final long DIRECTORY_ID = 1L;

    private ViewGroupMembers viewGroupMembers;

    @Mock private DirectoryManager directoryManager;
    @Mock private DirectoryInstanceLoader directoryInstanceLoader;
    @Mock private Directory directory;
    @Mock private RemoteDirectory remoteDirectory;

    @Before
    public void createObjectUnderTest()
    {
        viewGroupMembers = new ViewGroupMembers();
        viewGroupMembers.setDirectoryManager(directoryManager);
        viewGroupMembers.setDirectoryInstanceLoader(directoryInstanceLoader);
    }

    @Test
    public void testExecuteWithoutNestedGroups() throws Exception
    {
        viewGroupMembers.setGroupName("groupname");
        viewGroupMembers.setDirectoryID(DIRECTORY_ID);

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getDirectory(directory)).thenReturn(remoteDirectory);

        when(remoteDirectory.supportsNestedGroups()).thenReturn(false);

        User user1 = new UserTemplate("user1");
        User user2 = new UserTemplate("user2");
        final MembershipQuery<User> directUserMembersQuery =
            QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                .childrenOf(EntityDescriptor.group(GroupType.GROUP))
                .withName("groupname")
                .returningAtMost(EntityQuery.ALL_RESULTS);
        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, directUserMembersQuery))
            .thenReturn(ImmutableList.of(user2, user1)); // note the immutability of the *unsorted* list

        assertEquals(ViewGroupMembers.SUCCESS, viewGroupMembers.execute());

        assertInformationRequiredByNonNestedGroupsView("groupname",
                                                       ImmutableList.of(user1, user2) /* sorted */);
    }

    @Test
    public void testExecuteWithNestedGroups() throws Exception
    {
        viewGroupMembers.setGroupName("groupname");
        viewGroupMembers.setDirectoryID(DIRECTORY_ID);

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getDirectory(directory)).thenReturn(remoteDirectory);

        when(remoteDirectory.supportsNestedGroups()).thenReturn(true);

        User user1 = new UserTemplate("user1");
        User user2 = new UserTemplate("user2");
        final MembershipQuery<User> directUserMembersQuery =
            QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                .childrenOf(EntityDescriptor.group(GroupType.GROUP))
                .withName("groupname")
                .returningAtMost(EntityQuery.ALL_RESULTS);
        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, directUserMembersQuery))
            .thenReturn(ImmutableList.of(user2, user1)); // note the immutability of the *unsorted* list

        Group group1 = new GroupTemplate("group1");
        Group group2 = new GroupTemplate("group2");
        final MembershipQuery<Group> childGroupsQuery =
            QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP))
                .childrenOf(EntityDescriptor.group(GroupType.GROUP))
                .withName("groupname")
                .returningAtMost(EntityQuery.ALL_RESULTS);
        when(directoryManager.searchDirectGroupRelationships(DIRECTORY_ID, childGroupsQuery))
            .thenReturn(ImmutableList.of(group2, group1)); // note the immutability of the unsorted list

        Group group3 = new GroupTemplate("group3");
        Group group4 = new GroupTemplate("group4");
        final EntityQuery<Group> allGroupsQuery =
            QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP))
                .returningAtMost(EntityQuery.ALL_RESULTS);
        when(directoryManager.searchGroups(DIRECTORY_ID, allGroupsQuery))
            .thenReturn(ImmutableList.of(new GroupTemplate("groupname"), group1, group2, group3, group4));


        assertEquals(ViewGroupMembers.SUCCESS, viewGroupMembers.execute());

        assertInformationRequiredByNestedGroupsView("groupname",
                                                    ImmutableList.of(user1, user2), // sorted
                                                    ImmutableSet.of(group1, group2),
                                                    ImmutableSet.of(group3, group4));
    }

    /**
     * This method asserts that the information required to display the view of a group
     * in a directory that does not support nested groups is set.
     *
     * @param currentGroup
     * @param users
     */
    private void assertInformationRequiredByNonNestedGroupsView(String currentGroup, List<User> users)
        throws Exception
    {
        assertEquals(currentGroup, viewGroupMembers.getGroupName());
        assertFalse("Directory should not support nested groups", viewGroupMembers.isSupportsNestedGroups());
        assertEquals("Group members should be provided to the view", users, viewGroupMembers.getPrincipals());
        assertNull("The group should not have nested groups", viewGroupMembers.getSubGroups());
        assertNull("Non-member groups are not necessary", viewGroupMembers.getAllNonMemberGroups());

        // no need to query for subgroups when the directory does not support nested groups
        final MembershipQuery<Group> childGroupsQuery =
            QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP))
                .childrenOf(EntityDescriptor.group(GroupType.GROUP))
                .withName("groupname")
                .returningAtMost(EntityQuery.ALL_RESULTS);
        verify(directoryManager, never()).searchDirectGroupRelationships(DIRECTORY_ID, childGroupsQuery);

        // no need to calculate non-member groups when the directory does not support nested groups
        verify(directoryManager, never()).searchGroups(anyLong(), any(EntityQuery.class));
    }

    /**
     * This method asserts that the information required to display the view of a group
     * in a directory that supports nested groups is set.
     *
     * @param currentGroup
     * @param users
     * @param childGroups
     */
    private void assertInformationRequiredByNestedGroupsView(String currentGroup,
                                                             List<User> users,
                                                             Set<Group> childGroups,
                                                             Set<Group> nonMemberGroups)
    {
        assertEquals(currentGroup, viewGroupMembers.getGroupName());
        assertTrue("Directory should support nested groups", viewGroupMembers.isSupportsNestedGroups());
        assertEquals("Group members should be provided to the view", users, viewGroupMembers.getPrincipals());
        assertEquals("Subgroups should be provided to the view",
                     childGroups, ImmutableSet.copyOf(viewGroupMembers.getSubGroups()));
        assertEquals("Non-member groups should be provided to the view",
                     nonMemberGroups, ImmutableSet.copyOf(viewGroupMembers.getAllNonMemberGroups()));
    }
}
