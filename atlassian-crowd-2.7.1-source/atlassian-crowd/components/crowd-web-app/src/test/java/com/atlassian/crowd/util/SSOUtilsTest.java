package com.atlassian.crowd.util;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class SSOUtilsTest
{
    @Test
    public void testCookieDomainValidation()
    {
        assertTrue(SSOUtils.isCookieDomainValid(null, "example.com"));
        assertTrue(SSOUtils.isCookieDomainValid("", "example.com"));
        assertTrue(SSOUtils.isCookieDomainValid("example.com", "example.com"));
        assertTrue(SSOUtils.isCookieDomainValid("EXAMPLE.com", "example.COM")); // DNS is case insensitive
        assertTrue(SSOUtils.isCookieDomainValid(".example.com", "example.com"));    // wildcard
        assertTrue(SSOUtils.isCookieDomainValid(".example.com", "sub.example.com"));
        assertTrue(SSOUtils.isCookieDomainValid(".local", "host.local"));
        assertTrue(SSOUtils.isCookieDomainValid("local", "host.local"));

        assertFalse(SSOUtils.isCookieDomainValid("foo.com", "bar.com"));
        assertFalse(SSOUtils.isCookieDomainValid("foo.bar.com", "bar.com"));
        assertFalse(SSOUtils.isCookieDomainValid(".sub.example.com", "example.com"));
        assertTrue(SSOUtils.isCookieDomainValid("com", "foo.com"));
        assertTrue(SSOUtils.isCookieDomainValid(".com", "foo.com"));

        /* IMPLEMENTATION NOTE
         *
         * The following scenarios break the rules of RFC-2965!
         * We have them to maintain backward compatibility.
         *
         * According to the rfc, hostname "localhost" must be interpreted as
         * "localhost.local" and therefore the cookie domain "localhost" is
         * illegal, as it does not match the hostname.
         * All these tests should fail, but we'll make an exception.
         */
        assertTrue(SSOUtils.isCookieDomainValid("localhost", "localhost"));
        assertTrue(SSOUtils.isCookieDomainValid(".localhost", "localhost"));
    }

    @Test
    public void domainNameMatchesWhenIdentical()
    {
        assertTrue(SSOUtils.isCookieDomainValid("an-arbitrary-domain", "an-arbitrary-domain"));
    }

    @Test
    public void domainNameDoesNotMatchWhenCompletelyDifferent()
    {
        assertFalse(SSOUtils.isCookieDomainValid("an-arbitrary-domain", "a-completely-different-domain"));
    }

    @Test
    public void domainMatchesWhenDomainIsSuffixAndLastUnmatchedCharacterIsDot()
    {
        assertTrue(SSOUtils.isCookieDomainValid(".domain", "test.domain"));
        assertTrue(SSOUtils.isCookieDomainValid(".domain", "a.longer.test.domain"));

        assertTrue(SSOUtils.isCookieDomainValid("domain", "test.domain"));
        assertTrue(SSOUtils.isCookieDomainValid("domain", "a.longer.test.domain"));
    }

    @Test
    public void domainDoesNotMatchWhenDomainIsNotSuffix()
    {
        assertFalse(SSOUtils.isCookieDomainValid(".sub.domain", "test.another-sub.domain"));

        assertFalse(SSOUtils.isCookieDomainValid("sub.domain", "test.another-sub.domain"));
    }

    @Test
    public void domainDoesNotMatchWhenPrefixButStringIsNotAHostname()
    {
        assertFalse(SSOUtils.isCookieDomainValid(".1", "192.168.1.1"));
        assertFalse(SSOUtils.isCookieDomainValid("1", "192.168.1.1"));
        assertFalse(SSOUtils.isCookieDomainValid(":0000", "fe80::0000"));
    }

    @Test
    public void domainMatchesWhenIpAddressMatchesExactly()
    {
        assertTrue(SSOUtils.isCookieDomainValid("192.168.1.1", "192.168.1.1"));
        assertTrue(SSOUtils.isCookieDomainValid("::1", "::1"));
    }

    @Test
    public void nestedSubdomainsAccepted()
    {
        assertTrue(SSOUtils.isCookieDomainValid(".xyz.example.com", "abc.xyz.example.com"));
        assertTrue(SSOUtils.isCookieDomainValid(".example.com", "abc.xyz.example.com"));
    }

    @Test
    public void domainsWithAndWithoutTrailingDotsAreDistinct()
    {
        assertFalse(SSOUtils.isCookieDomainValid("domain.", "domain"));
        assertFalse(SSOUtils.isCookieDomainValid("domain", "domain."));

        assertFalse(SSOUtils.isCookieDomainValid("host.domain.", "host.domain"));
        assertFalse(SSOUtils.isCookieDomainValid("host.domain", "host.domain."));

        assertFalse(SSOUtils.isCookieDomainValid(".domain.", "host.domain"));
        assertFalse(SSOUtils.isCookieDomainValid(".domain", "host.domain."));
    }

    @Test
    public void domainNameMatchIsCaseInsensitive()
    {
        assertTrue(SSOUtils.isCookieDomainValid("DOMAIN", "domain"));
        assertTrue(SSOUtils.isCookieDomainValid(".DOMAIN", "test.domain"));
        assertTrue(SSOUtils.isCookieDomainValid(".domain", "a.longer.test.DOMAIN"));
    }

    @Test
    public void ipAddressesAreIdentified()
    {
        assertFalse(SSOUtils.isAnIpAddress("test"));
        assertFalse(SSOUtils.isAnIpAddress("1.2.3"));

        assertTrue(SSOUtils.isAnIpAddress("1.2.3.4"));
        assertTrue(SSOUtils.isAnIpAddress("::1"));
    }

    @Test
    public void asterisksAreNotSupportedInDomainPatterns()
    {
        assertFalse(SSOUtils.isCookieDomainValid("*.example.com", "www.example.com"));
        assertFalse(SSOUtils.isCookieDomainValid("*.example.com", "example.com"));
    }
}
