package com.atlassian.crowd.console.listener;

import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;

import com.atlassian.config.HomeLocator;
import com.atlassian.config.bootstrap.AtlassianBootstrapManager;
import com.atlassian.config.db.HibernateConfig;
import com.atlassian.config.lifecycle.events.ApplicationStartedEvent;
import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.crowd.event.application.ApplicationReadyEvent;
import com.atlassian.crowd.manager.license.CrowdLicenseManager;
import com.atlassian.crowd.manager.upgrade.UpgradeManager;
import com.atlassian.crowd.migration.legacy.database.DatabaseMigrationManager;
import com.atlassian.crowd.plugin.RequiredPluginsStartupCheck;
import com.atlassian.crowd.util.SystemInfoHelper;
import com.atlassian.crowd.util.persistence.hibernate.SchemaHelper;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventType;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;

import com.google.common.collect.ImmutableList;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsCollectionContaining;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.ApplicationContext;

import static com.atlassian.crowd.console.listener.StartupListenerTest.EventMatcher.event;
import static com.google.common.base.Preconditions.checkNotNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StartupListenerTest
{
    private StartupListener startupListener;

    @Mock private ServletContextEvent servletContextEvent;
    @Mock private ServletContext servletContext;
    @Mock private AtlassianBootstrapManager bootstrapManager;
    @Mock private ContainerContext containerContext;
    @Mock private CrowdLicenseManager crowdLicenseManager;
    @Mock private CrowdLicense license;
    @Mock private SystemInfoHelper systemInfoHelper;
    @Mock private JohnsonEventContainer johnsonEventContainer;
    @Mock private EventPublisher eventPublisher;
    @Mock private DatabaseMigrationManager databaseMigrationManager;
    @Mock private SchemaHelper schemaHelper;
    @Mock private UpgradeManager upgradeManager;
    @Mock private RequiredPluginsStartupCheck requiredPluginsStartupCheck;
    @Mock private HomeLocator homeLocator;
    @Mock private ApplicationContext bootstrapContext;

    @Before
    public void setUp() throws Exception
    {
        startupListener = new StartupListener();

        when(servletContextEvent.getServletContext()).thenReturn(servletContext);

        when(servletContext.getAttribute(Johnson.EVENT_CONTAINER_ATTRIBUTE)).thenReturn(johnsonEventContainer);

        when(containerContext.getComponent("crowdLicenseManager")).thenReturn(crowdLicenseManager);
        when(containerContext.getComponent("systemInfoHelper")).thenReturn(systemInfoHelper);
        when(containerContext.getComponent("eventPublisher")).thenReturn(eventPublisher);
        when(containerContext.getComponent("databaseMigrationManager")).thenReturn(databaseMigrationManager);
        when(containerContext.getComponent("schemaHelper")).thenReturn(schemaHelper);
        when(containerContext.getComponent("upgradeManager")).thenReturn(upgradeManager);
        when(containerContext.getComponent("requiredPluginsStartupCheck")).thenReturn(requiredPluginsStartupCheck);
        when(containerContext.isSetup()).thenReturn(true);

        when(bootstrapContext.getBean("homeLocator")).thenReturn(homeLocator);

        Johnson.initialize(servletContext);
        BootstrapUtils.setBootstrapManager(bootstrapManager);
        BootstrapUtils.setBootstrapContext(bootstrapContext);
        ContainerManager.getInstance().setContainerContext(containerContext);
    }

    @After
    public void tearDown()
    {
        ContainerManager.resetInstance();
        BootstrapUtils.setBootstrapManager(null);
        BootstrapUtils.setBootstrapContext(null);
        Johnson.terminate(servletContext);
    }

    @Test
    public void shouldNotDoAnythingIfSetupIsNotComplete() throws Exception
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(false);

        startupListener.contextInitialized(servletContextEvent);
    }

    @Test
    public void shouldLockCrowdIfLicenseIsNotFound() throws Exception
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(true, false, false);

        when(crowdLicenseManager.getLicense()).thenReturn(null);  // license not found

        startupListener.contextInitialized(servletContextEvent);

        verify(bootstrapManager).setSetupComplete(false);
        verify(johnsonEventContainer).addEvent(argThat(
            event(Johnson.getConfig().getEventType("bootstrap"),
                  containsString("Crowd cannot find the license"))));
        verify(bootstrapManager, times(3)).isSetupComplete();
        verifyZeroInteractions(eventPublisher);
    }

    @Test
    public void shouldLockCrowdIfEvaluationLicenseIsExpired() throws Exception
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(true, false, false);

        when(crowdLicenseManager.getLicense()).thenReturn(license);

        when(crowdLicenseManager.isLicenseValid(license)).thenReturn(false);
        when(crowdLicenseManager.isBuildWithinMaintenancePeriod(license)).thenReturn(true);

        when(license.isExpired()).thenReturn(true);
        when(license.isEvaluation()).thenReturn(true);
        when(license.isUnlimitedNumberOfUsers()).thenReturn(true);

        startupListener.contextInitialized(servletContextEvent);

        verify(bootstrapManager).setSetupComplete(false);
        verify(johnsonEventContainer).addEvent(argThat(
            event(Johnson.getConfig().getEventType("license-too-old"),
                  containsString("Crowd cannot be started as your evaluation period ended on"))));
        verify(bootstrapManager, times(3)).isSetupComplete();
        verifyZeroInteractions(eventPublisher);
    }

    @Test
    public void shouldLockCrowdIfUnlimitedLicenseIsExpired() throws Exception
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(true, false, false);

        when(crowdLicenseManager.getLicense()).thenReturn(license);

        when(crowdLicenseManager.isLicenseValid(license)).thenReturn(false);
        when(crowdLicenseManager.isBuildWithinMaintenancePeriod(license)).thenReturn(true);

        when(license.isExpired()).thenReturn(true);
        when(license.isEvaluation()).thenReturn(false);
        when(license.getMaintenanceExpiryDate()).thenReturn(null);
        when(license.isUnlimitedNumberOfUsers()).thenReturn(true);

        startupListener.contextInitialized(servletContextEvent);

        verify(bootstrapManager).setSetupComplete(false);
        verify(johnsonEventContainer).addEvent(argThat(
            event(Johnson.getConfig().getEventType("license-too-old"),
                  containsString("Crowd cannot be started as your support period ended on"))));
        verify(bootstrapManager, times(3)).isSetupComplete();
        verifyZeroInteractions(eventPublisher);
    }

    @Test
    public void shouldLockCrowdIfMaintenancePeriodIsExpired() throws Exception
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(true, false, false);

        when(crowdLicenseManager.getLicense()).thenReturn(license);

        when(crowdLicenseManager.isLicenseValid(license)).thenReturn(false);
        when(crowdLicenseManager.isBuildWithinMaintenancePeriod(license)).thenReturn(false);

        when(license.isExpired()).thenReturn(false);
        when(license.isEvaluation()).thenReturn(false);
        when(license.getMaintenanceExpiryDate()).thenReturn(new Date(0));
        when(license.isUnlimitedNumberOfUsers()).thenReturn(true);

        startupListener.contextInitialized(servletContextEvent);

        verify(bootstrapManager).setSetupComplete(false);
        verify(johnsonEventContainer).addEvent(argThat(
            event(Johnson.getConfig().getEventType("license-too-old"),
                  containsString("Crowd cannot be started as your maintenance period ended on"))));
        verify(bootstrapManager, times(3)).isSetupComplete();
        verifyZeroInteractions(eventPublisher);
    }

    @Test
    public void shouldLockCrowdIfResourceUsageAboveLicenseLimit() throws Exception
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(true, false, false);

        when(crowdLicenseManager.getLicense()).thenReturn(license);

        when(crowdLicenseManager.isLicenseValid(license)).thenReturn(false);
        when(crowdLicenseManager.isBuildWithinMaintenancePeriod(license)).thenReturn(true);

        when(license.isExpired()).thenReturn(false);
        when(license.isEvaluation()).thenReturn(false);
        when(license.getMaintenanceExpiryDate()).thenReturn(null);
        when(license.isUnlimitedNumberOfUsers()).thenReturn(false);

        startupListener.contextInitialized(servletContextEvent);

        verify(bootstrapManager).setSetupComplete(false);
        verify(johnsonEventContainer).addEvent(argThat(
            event(Johnson.getConfig().getEventType("license-too-old"),
                  containsString("Crowd cannot be started as your license has reached its resource limit."))));
        verify(bootstrapManager, times(3)).isSetupComplete();
        verifyZeroInteractions(eventPublisher);
    }

    @Test
    public void shouldLockCrowdIfUpgradeFails() throws Exception
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(true, false, false);

        when(crowdLicenseManager.getLicense()).thenReturn(license);

        when(crowdLicenseManager.isLicenseValid(license)).thenReturn(true);
        when(crowdLicenseManager.isBuildWithinMaintenancePeriod(license)).thenReturn(true);

        when(license.isExpired()).thenReturn(false);
        when(license.isEvaluation()).thenReturn(false);
        when(license.getMaintenanceExpiryDate()).thenReturn(null);
        when(license.isUnlimitedNumberOfUsers()).thenReturn(false);

        when(databaseMigrationManager.isLegacyCrowd()).thenReturn(false);

        when(upgradeManager.doUpgrade()).thenReturn(ImmutableList.of("upgrade error"));

        startupListener.contextInitialized(servletContextEvent);

        verify(bootstrapManager, times(3)).isSetupComplete();

        verify(bootstrapManager).setSetupComplete(false);
        verify(johnsonEventContainer).addEvent(argThat(
            event(Johnson.getConfig().getEventType("bootstrap"),
                  containsString(
                      "Crowd has encountered errors while upgrading. Please resolve these errors and restart Crowd."))));
        verify(bootstrapManager, times(3)).isSetupComplete();
        verifyZeroInteractions(eventPublisher);
    }

    @Test
    public void shouldLockCrowdIfRequiredPluginsAreNotLoaded() throws Exception
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(true);

        when(crowdLicenseManager.getLicense()).thenReturn(license);

        when(crowdLicenseManager.isLicenseValid(license)).thenReturn(true);
        when(crowdLicenseManager.isBuildWithinMaintenancePeriod(license)).thenReturn(true);

        when(license.isExpired()).thenReturn(false);
        when(license.isEvaluation()).thenReturn(false);
        when(license.getMaintenanceExpiryDate()).thenReturn(null);
        when(license.isUnlimitedNumberOfUsers()).thenReturn(false);

        when(databaseMigrationManager.isLegacyCrowd()).thenReturn(false);

        when(requiredPluginsStartupCheck.requiredPluginsNotEnabled()).thenReturn(ImmutableList.of("rebel plugin"));

        startupListener.contextInitialized(servletContextEvent);

        verify(johnsonEventContainer).addEvent(argThat(
            event(Johnson.getConfig().getEventType("bootstrap"),
                  containsString(
                      "Required plugins failed to initialise. Please check the logs for errors and restart Crowd."))));
        verify(bootstrapManager, times(3)).isSetupComplete();
        verify(eventPublisher).publish(any(ApplicationStartedEvent.class));
        verifyNoMoreInteractions(eventPublisher);
    }

    @Test
    public void shouldPublishEventsIfCrowdIsSuccessfullyStarted() throws Exception
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(true);

        when(crowdLicenseManager.getLicense()).thenReturn(license);

        when(crowdLicenseManager.isLicenseValid(license)).thenReturn(true);
        when(crowdLicenseManager.isBuildWithinMaintenancePeriod(license)).thenReturn(true);

        when(license.isExpired()).thenReturn(false);
        when(license.isEvaluation()).thenReturn(false);
        when(license.getMaintenanceExpiryDate()).thenReturn(null);
        when(license.isUnlimitedNumberOfUsers()).thenReturn(false);

        when(databaseMigrationManager.isLegacyCrowd()).thenReturn(false);

        startupListener.contextInitialized(servletContextEvent);

        verify(systemInfoHelper).printSystemInfo(BootstrapLoaderListener.STARTUP_LOG);
        verify(requiredPluginsStartupCheck).requiredPluginsNotEnabled();
        verify(upgradeManager).doUpgrade();
        verify(johnsonEventContainer, never()).addEvent(any(Event.class));
        verify(bootstrapManager, times(3)).isSetupComplete();

        ArgumentCaptor<com.atlassian.crowd.event.Event> publishedEvents =
            ArgumentCaptor.forClass(com.atlassian.crowd.event.Event.class);
        verify(eventPublisher, times(2)).publish(publishedEvents.capture());
        assertThat(publishedEvents.getAllValues(),
                   IsCollectionContaining.<com.atlassian.crowd.event.Event>hasItems(
                       Matchers.<com.atlassian.crowd.event.Event>instanceOf(ApplicationStartedEvent.class),
                       Matchers.<com.atlassian.crowd.event.Event>instanceOf(ApplicationReadyEvent.class)));
    }

    @Test
    public void shouldPublishApplicationStartedEventIfStillInSetup_ContextAvailable_DatabaseAvailable_SetupPastDatabaseConfiguration()
            throws Exception
    {
        when(bootstrapManager.getHibernateConfig()).thenReturn(mock(HibernateConfig.class));
        when(bootstrapManager.isSetupComplete()).thenReturn(false);

        startupListener.contextInitialized(servletContextEvent);

        verify(bootstrapManager, times(4)).isSetupComplete();


        // Validate the the event publisher has received the ApplicationStartedEvent if we are past the hibernate step.
        ArgumentCaptor<com.atlassian.crowd.event.Event> publishedEvents =
                ArgumentCaptor.forClass(com.atlassian.crowd.event.Event.class);
        verify(eventPublisher, times(1)).publish(publishedEvents.capture());
        assertThat(publishedEvents.getAllValues(),
                IsCollectionContaining.<com.atlassian.crowd.event.Event>hasItems(
                        Matchers.<com.atlassian.crowd.event.Event>instanceOf(ApplicationStartedEvent.class)));
    }

    static class EventMatcher extends TypeSafeMatcher<Event>
    {
        private final EventType type;
        private final Matcher<String> messageMatcher;

        private EventMatcher(EventType type, Matcher<String> messageMatcher)
        {
            this.type = checkNotNull(type, "The event type is null, make sure the event is defined in Johnson's config");
            this.messageMatcher = messageMatcher;
        }

        @Factory
        public static Matcher<Event> event(EventType type, Matcher<String> message)
        {
            return new EventMatcher(type, message);
        }

        @Override
        protected boolean matchesSafely(Event item)
        {
            return type.equals(item.getKey()) && messageMatcher.matches(item.getDesc());
        }

        @Override
        public void describeTo(Description description)
        {
            description.appendText("a Johnson event with Key = ").appendText(type.toString())
                .appendText(" and Desc = ").appendDescriptionOf(messageMatcher);
        }
    }
}
