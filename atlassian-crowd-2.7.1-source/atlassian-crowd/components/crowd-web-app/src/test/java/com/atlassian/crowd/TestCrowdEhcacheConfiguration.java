package com.atlassian.crowd;

import java.net.URL;

import com.atlassian.crowd.dao.token.TokenDAOMemory;
import com.atlassian.crowd.exception.ObjectAlreadyExistsException;
import com.atlassian.crowd.manager.cache.CacheManagerEhcache;
import com.atlassian.crowd.model.token.Token;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.config.Configuration;
import net.sf.ehcache.config.ConfigurationFactory;

import static org.junit.Assert.assertEquals;

public class TestCrowdEhcacheConfiguration
{
    @Rule
    public TemporaryFolder persistentEhcacheDir = new TemporaryFolder();

    @Test
    public void tokenCachesForTokenDAOMemoryPersistAcrossCacheManagers() throws Exception
    {
        // load the Ehcache configuration file, but override the diskStore dir to use a temporary folder
        URL configFile = getClass().getResource("/crowd-ehcache.xml");
        Configuration ehcacheConfiguration = ConfigurationFactory.parseConfiguration(configFile);
        ehcacheConfiguration.getDiskStoreConfiguration().setPath(persistentEhcacheDir.newFolder().toString());

        String identifierHash = "identifierHash" + Math.random();
        String randomHash = "randomHash" + Math.random();
        long randomNumber = (long) Math.random();

        Token token = new Token.Builder(1, "name", identifierHash, randomNumber, randomHash).create();

        /* Store in the first CacheManager */
        CacheManager firstCacheManager = new CacheManager(ehcacheConfiguration);
        storeInNewClearedCacheManager(firstCacheManager, token);

        /* Then retrieve in another instance */
        CacheManager secondCacheManager = new CacheManager(ehcacheConfiguration);

        try
        {
            TokenDAOMemory secondTdm = new TokenDAOMemory(new CacheManagerEhcache(secondCacheManager));

            Token retrieved1 = secondTdm.findByIdentifierHash(identifierHash);
            assertEquals(randomNumber, retrieved1.getRandomNumber());

            Token retrieved2 = secondTdm.findByRandomHash(randomHash);
            assertEquals(randomNumber, retrieved2.getRandomNumber());
        }
        finally
        {
            secondCacheManager.shutdown();
        }
    }

    private void storeInNewClearedCacheManager(CacheManager cm, Token token) throws ObjectAlreadyExistsException
    {
        try
        {
            cm.clearAll();

            TokenDAOMemory tdm = new TokenDAOMemory(new CacheManagerEhcache(cm));
            tdm.add(token);
        }
        finally
        {
            cm.shutdown();
        }
    }
}
