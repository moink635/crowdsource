package com.atlassian.crowd.console.action.pickers;

import java.util.List;

import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.DirectoryEntity;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SearchPickerTest
{
    static List<Group> mocksCalled(Iterable<String> names)
    {
        return ImmutableList.copyOf(Iterables.transform(names, new Function<String, Group>() {
            @Override
            public Group apply(String input)
            {
                Group g = mock(Group.class);
                when(g.getName()).thenReturn(input);
                return g;
            }
        }));
    }

    static List<String> namesOf(Iterable<DirectoryEntity> entities)
    {
        return ImmutableList.copyOf(Iterables.transform(entities, new Function<DirectoryEntity, String>() {
            @Override
            public String apply(DirectoryEntity input)
            {
                return input.getName();
            }
        }));
    }

    @Test
    public void getGroupNonMembersOfGroupDoesNotAssumeMutableSearchResults() throws Exception
    {
        DirectoryManager directoryManager = mock(DirectoryManager.class);
        when(directoryManager.searchDirectGroupRelationships(Mockito.eq(1L), Mockito.<MembershipQuery<String>>any())).thenReturn(ImmutableList.of("already-member"));

        List<Group> allGroups = mocksCalled(ImmutableList.of("group", "already-member", "other-group-1", "other-group-2"));

        when(directoryManager.searchGroups(Mockito.eq(1L), Mockito.<EntityQuery<Group>>any())).thenReturn(allGroups);

        SearchPicker picker = new SearchPicker();
        picker.setDirectoryID(1L);
        picker.setEntityName("group");

        picker.setDirectoryManager(directoryManager);
        assertEquals("success", picker.getGroupNonMembersOfGroup());
        assertEquals(ImmutableList.of("other-group-1", "other-group-2"),
                namesOf(picker.getResults()));
    }

    @Test
    public void getGroupNonMembershipsOfUserDoesNotAssumeMutableSearchResults() throws Exception
    {
        DirectoryManager directoryManager = mock(DirectoryManager.class);
        when(directoryManager.searchDirectGroupRelationships(Mockito.eq(1L), Mockito.<MembershipQuery<String>>any())).thenReturn(ImmutableList.<String>of());

        List<Group> allGroups = mocksCalled(ImmutableList.of("group-2", "group-1"));

        when(directoryManager.searchGroups(Mockito.eq(1L), Mockito.<EntityQuery<Group>>any())).thenReturn(allGroups);

        SearchPicker picker = new SearchPicker();
        picker.setDirectoryID(1L);
        picker.setEntityName("user");

        picker.setDirectoryManager(directoryManager);
        assertEquals("success", picker.getGroupNonMembershipsOfUser());
        assertEquals(ImmutableList.of("group-1", "group-2"),
                namesOf(picker.getResults()));
    }
}
