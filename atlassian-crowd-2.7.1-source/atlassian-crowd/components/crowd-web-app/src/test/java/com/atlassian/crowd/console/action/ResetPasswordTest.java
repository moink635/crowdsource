package com.atlassian.crowd.console.action;

import com.atlassian.crowd.directory.AbstractInternalDirectory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.login.ForgottenLoginManagerImpl;
import com.atlassian.crowd.manager.login.exception.InvalidResetPasswordTokenException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.google.common.collect.ImmutableList;
import com.opensymphony.xwork.Action;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ResetPasswordTest
{
    private static final String REGEX_TEST_MESSAGE = "I am testing the regex";
    private static final String REGEX = "[a-z]";
    private static final long DIRECTORY_ID = 10;
    private static final String RESET_TOKEN = "token";
    private static final String USERNAME = "username";

    private ResetPassword resetPassword;

    @Mock private ForgottenLoginManagerImpl loginManager;
    @Mock private DirectoryManager directoryManager;

    @Before
    public void createObjectUnderTest()
    {
        resetPassword = new ResetPassword();
        resetPassword.setForgottenLoginManager(loginManager);
        resetPassword.setDirectoryManager(directoryManager);
    }

    @Test
    public void shouldDisplayTheDirectoryComplexityRequirementIfPresent() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(createDirectory(REGEX_TEST_MESSAGE, REGEX));

        when(loginManager.isValidResetToken(DIRECTORY_ID, USERNAME, RESET_TOKEN)).thenReturn(true);

        resetPassword.setDirectoryId(DIRECTORY_ID);
        resetPassword.setUsername(USERNAME);
        resetPassword.setToken(RESET_TOKEN);

        assertEquals(Action.INPUT, resetPassword.doDefault());
        assertEquals(REGEX_TEST_MESSAGE, resetPassword.getPasswordComplexityMessage());
    }

    @Test
    public void shouldNotDisplayTheComplexityRequirementIfBlank() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(createDirectory("  ", ""));

        when(loginManager.isValidResetToken(DIRECTORY_ID, USERNAME, RESET_TOKEN)).thenReturn(true);

        resetPassword.setDirectoryId(DIRECTORY_ID);
        resetPassword.setUsername(USERNAME);
        resetPassword.setToken(RESET_TOKEN);

        assertEquals(Action.INPUT, resetPassword.doDefault());
        assertNull(resetPassword.getPasswordComplexityMessage());
    }

    @Test
    public void shouldValidateTokenUpfront() throws Exception
    {
        when(loginManager.isValidResetToken(DIRECTORY_ID, USERNAME, RESET_TOKEN)).thenReturn(false);

        resetPassword.setDirectoryId(DIRECTORY_ID);
        resetPassword.setUsername(USERNAME);
        resetPassword.setToken(RESET_TOKEN);

        assertEquals(Action.INPUT, resetPassword.doDefault());
        assertTrue(resetPassword.getIsInvalidToken());
    }

    @Test
    public void shouldDisplayInvalidTokenIfExpiredOrInvalid() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(createDirectory(REGEX_TEST_MESSAGE, REGEX));

        doThrow(new InvalidResetPasswordTokenException())
                .when(loginManager)
                .resetUserCredential(DIRECTORY_ID, USERNAME, new PasswordCredential("password1"), RESET_TOKEN);

        resetPassword.setDirectoryId(DIRECTORY_ID);
        resetPassword.setUsername(USERNAME);
        resetPassword.setPassword("password1");
        resetPassword.setConfirmPassword("password1");
        resetPassword.setToken(RESET_TOKEN);

        assertEquals(Action.INPUT, resetPassword.doUpdate());
        assertTrue(resetPassword.getIsInvalidToken());
    }

    @Test
    public void shouldDisplayPasswordIsInvalidIfPasswordIsEmpty() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(createDirectory(REGEX_TEST_MESSAGE, REGEX));

        resetPassword.setDirectoryId(DIRECTORY_ID);
        resetPassword.setUsername(USERNAME);
        resetPassword.setPassword("");
        resetPassword.setConfirmPassword("");
        resetPassword.setToken(RESET_TOKEN);

        assertEquals(Action.ERROR, resetPassword.doUpdate());
        assertEquals(ImmutableList.of(resetPassword.getText("passwordempty.invalid")),
                resetPassword.getFieldErrors().get("password"));
    }

    @Test
    public void shouldDisplayConfirmPasswordIsEmpty() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(createDirectory(REGEX_TEST_MESSAGE, REGEX));

        resetPassword.setDirectoryId(DIRECTORY_ID);
        resetPassword.setUsername(USERNAME);
        resetPassword.setPassword("password");
        resetPassword.setConfirmPassword("");
        resetPassword.setToken(RESET_TOKEN);

        assertEquals(Action.ERROR, resetPassword.doUpdate());
        assertEquals(ImmutableList.of(resetPassword.getText("passwordempty.invalid")),
            resetPassword.getFieldErrors().get("confirmpassword"));
    }

    @Test
    public void shouldDisplayPasswordShouldMatchIfPasswordAndConfirmPasswordDoNotMatch() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(createDirectory(REGEX_TEST_MESSAGE, REGEX));

        resetPassword.setDirectoryId(DIRECTORY_ID);
        resetPassword.setUsername(USERNAME);
        resetPassword.setPassword("password1");
        resetPassword.setConfirmPassword("password2");
        resetPassword.setToken(RESET_TOKEN);

        assertEquals(Action.ERROR, resetPassword.doUpdate());
        assertEquals(ImmutableList.of(resetPassword.getText("passworddonotmatch.invalid")),
                resetPassword.getFieldErrors().get("password"));
    }

    @Test
    public void shouldDisplayTheDirectoryComplexityRequirementIfTheRequirementIsNotMetWhenReset() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(createDirectory(REGEX_TEST_MESSAGE, REGEX));

        doThrow(new InvalidCredentialException("Generic message", "Password complexity message from exception"))
                .when(loginManager)
                .resetUserCredential(anyLong(), any(String.class), any(PasswordCredential.class), anyString());

        resetPassword.setDirectoryId(DIRECTORY_ID);
        resetPassword.setUsername(USERNAME);
        resetPassword.setPassword("newpassword");
        resetPassword.setConfirmPassword("newpassword");
        resetPassword.setToken(RESET_TOKEN);

        assertEquals(Action.INPUT, resetPassword.doUpdate());
        assertEquals(ImmutableList.of(resetPassword.getText("passwordupdate.policy.error.message")),
                resetPassword.getActionErrors());
        assertEquals(ImmutableList.of("Password complexity message from exception"),
                resetPassword.getFieldErrors().get("password"));
    }

    @Test
    public void shouldResetThePassword() throws Exception
    {

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(createDirectory(REGEX_TEST_MESSAGE, REGEX));

        resetPassword.setDirectoryId(DIRECTORY_ID);
        resetPassword.setUsername(USERNAME);
        resetPassword.setPassword("newpassword");
        resetPassword.setConfirmPassword("newpassword");
        resetPassword.setToken(RESET_TOKEN);

        assertEquals(Action.SUCCESS, resetPassword.doUpdate());
        assertEquals(ImmutableList.of(resetPassword.getText("passwordupdate.message")),
                resetPassword.getActionMessages());

        verify(loginManager).resetUserCredential(DIRECTORY_ID, USERNAME, new PasswordCredential("newpassword"), RESET_TOKEN);
    }

    @Test
    public void doesNotThrowExceptionOrLogAnyComplexityDescriptionWhenDirectoryIsNotFound() throws Exception
    {
        resetPassword.setDirectoryId(DIRECTORY_ID);
        resetPassword.setUsername(USERNAME);
        resetPassword.setToken("token");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenThrow(new DirectoryNotFoundException(DIRECTORY_ID));

        assertEquals(Action.INPUT, resetPassword.doDefault());
        assertThat((Collection<String>)resetPassword.getActionMessages(), IsEmptyCollection.<String>empty());
    }

    private static DirectoryImpl createDirectory(String passwordComplexityMessage, String regex)
    {
        DirectoryImpl directory = new DirectoryImpl();
        directory.setAttribute(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_COMPLEXITY_MESSAGE, passwordComplexityMessage);
        directory.setAttribute(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_REGEX, regex);
        return directory;
    }
}
