package com.atlassian.crowd.console.action.setup;

import java.sql.Connection;

import com.atlassian.config.db.DatabaseDetails;
import com.atlassian.crowd.console.setup.CrowdSetupPersister;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.plugin.RequiredPluginsStartupCheck;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DatabaseTest
{
    private Database action;

    @Mock private CrowdSetupPersister setupPersister;
    @Mock private CrowdBootstrapManager crowdBootstrapManager;
    @Mock private RequiredPluginsStartupCheck requiredPluginsStartupCheck;
    @Mock private Connection connection;

    @Before
    public void createObjectUnderTest() throws Exception
    {
        action = new Database()
        {
            @Override
            protected CrowdBootstrapManager getBootstrapManager()
            {
                return crowdBootstrapManager;
            }
        };
        action.setSetupPersister(setupPersister);
    }

    @Test
    public void cannotMoveForwardIfThereAreMissingRequiredPlugins() throws Exception
    {
        action.setDatabaseOption(Database.JDBC_DB);
        action.setJdbcDriverClassName(String.class.getCanonicalName());  // must be available to the classloader
        action.setJdbcHibernateDialect(String.class.getCanonicalName());   // must be available to the classloader
        action.setJdbcDatabaseType("other");
        action.setJdbcUrl("jdbc url");
        action.setJdbcUsername("user");

        // lazy load
        action.setRequiredPluginsStartupCheck(requiredPluginsStartupCheck);

        ImmutableList<String> missingPlugins = ImmutableList.of("missing required plugin");

        when(crowdBootstrapManager.isApplicationHomeValid()).thenReturn(true);
        when(setupPersister.getCurrentStep()).thenReturn(Database.DATABASE_STEP);
        when(requiredPluginsStartupCheck.requiredPluginsNotEnabled()).thenReturn(missingPlugins);
        when(crowdBootstrapManager.getTestDatabaseConnection(any(DatabaseDetails.class))).thenReturn(connection);

        assertEquals(Database.ERROR, action.doUpdate());

        assertEquals(ImmutableList.of(action.getText("error.required.plugins.missing",
                                                     ImmutableList.of(missingPlugins)) + " "
                                      + action.getText("setupcomplete.error.cannot.continue")),
                     action.getActionErrors());

        verify(setupPersister, never()).progessSetupStep();
    }

    @Test
    public void configureJdbcSuccessfully() throws Exception
    {
        action.setDatabaseOption(Database.JDBC_DB);
        action.setJdbcDriverClassName(String.class.getCanonicalName());  // must be available to the classloader
        action.setJdbcHibernateDialect(String.class.getCanonicalName());   // must be available to the classloader
        action.setJdbcDatabaseType("other");
        action.setJdbcUrl("jdbc url");
        action.setJdbcUsername("user");

        // lazy load
        action.setRequiredPluginsStartupCheck(requiredPluginsStartupCheck);

        when(crowdBootstrapManager.isApplicationHomeValid()).thenReturn(true);
        when(setupPersister.getCurrentStep()).thenReturn(Database.DATABASE_STEP);
        when(crowdBootstrapManager.getTestDatabaseConnection(any(DatabaseDetails.class))).thenReturn(connection);

        assertEquals(Database.SELECT_SETUP_STEP, action.doUpdate());

        verify(setupPersister).progessSetupStep();
        verify(requiredPluginsStartupCheck).requiredPluginsNotEnabled();
        verify(crowdBootstrapManager).bootstrapDatabase(any(DatabaseDetails.class), eq(false));
    }

    @Test
    public void jdbcDatabaseTypeIsRequired() throws Exception
    {
        action.setDatabaseOption(Database.JDBC_DB);

        when(crowdBootstrapManager.isApplicationHomeValid()).thenReturn(true);
        when(setupPersister.getCurrentStep()).thenReturn(Database.DATABASE_STEP);
        when(crowdBootstrapManager.getTestDatabaseConnection(any(DatabaseDetails.class))).thenReturn(connection);

        assertEquals(Database.ERROR, action.doUpdate());

        assertEquals(ImmutableList.of(action.getText("database.select.blank")),
                     action.getFieldErrors().get("jdbcDatabaseType"));

        verify(setupPersister, never()).progessSetupStep();
    }
}
