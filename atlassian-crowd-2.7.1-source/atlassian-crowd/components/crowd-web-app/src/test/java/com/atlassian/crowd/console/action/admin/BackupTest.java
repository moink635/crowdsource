package com.atlassian.crowd.console.action.admin;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.backup.BackupManager;
import com.atlassian.crowd.manager.backup.BackupScheduler;
import com.atlassian.crowd.manager.backup.BackupSummary;
import com.atlassian.crowd.manager.directory.DirectoryManager;

import com.google.common.collect.ImmutableList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BackupTest
{
    private static final long DIRECTORY1 = 1L;
    private static final long DIRECTORY2 = 2L;
    private static final long DIRECTORY3 = 3L;

    @Mock
    private DirectoryManager directoryManager;

    @Mock
    private BackupManager backupManager;

    @Mock
    private BackupScheduler backupScheduler;

    @InjectMocks
    private Backup backup;

    @Test
    public void shouldSetDefaultValuesOnInitialFormDisplay() throws Exception
    {
        when(backupManager.generateManualBackupFileName()).thenReturn("manual-backup.xml");
        when(backupScheduler.isEnabled()).thenReturn(true);
        when(backupScheduler.getHourToRun()).thenReturn(1);

        assertEquals(Backup.INPUT, backup.doDefault());

        assertTrue(backup.isResetDomain());
        assertEquals("manual-backup.xml", backup.getExportFileName());
        assertTrue(backup.isEnableScheduleBackups());
        assertEquals(1, backup.getScheduledBackupTime());
    }

    @Test
    public void shouldSetDefaultValuesAfterSubmittingScheduledBackupForm() throws Exception
    {
        when(backupManager.generateManualBackupFileName()).thenReturn("manual-backup.xml");

        backup.doScheduledBackup();

        assertTrue(backup.isResetDomain());
        assertEquals("manual-backup.xml", backup.getExportFileName());
    }

    @Test
    public void shouldSetDefaultValuesAfterSubmittingManualBackupForm() throws Exception
    {
        when(backupScheduler.isEnabled()).thenReturn(true);
        when(backupScheduler.getHourToRun()).thenReturn(1);

        backup.doExport();

        assertTrue(backup.isEnableScheduleBackups());
        assertEquals(1, backup.getScheduledBackupTime());
    }

    @Test
    public void shouldNotImportBackupIfThereIsDirectorySynchronisationInProgress() throws Exception
    {
        Directory directory1 = createDirectoryMock(DIRECTORY1, "Directory One");
        Directory directory2 = createDirectoryMock(DIRECTORY2, "Directory Two");
        Directory directory3 = createDirectoryMock(DIRECTORY3, "Directory Three");
        when(directoryManager.findAllDirectories()).thenReturn(ImmutableList.of(directory1, directory2, directory3));

        when(directoryManager.isSynchronising(DIRECTORY1)).thenReturn(true);
        when(directoryManager.isSynchronising(DIRECTORY2)).thenReturn(true);
        when(directoryManager.isSynchronising(DIRECTORY3)).thenReturn(false);

        assertEquals(Backup.ERROR, backup.doImport());

        assertEquals(ImmutableList.of(backup.getText("administration.restore.directory.synchronising.error",
                ImmutableList.of("Directory One, Directory Two"))),
                     backup.getActionErrors());
    }

    @Test
    public void shouldGetBackupSummary() throws Exception
    {
        BackupSummary backupSummary = mock(BackupSummary.class);
        when(backupManager.getAutomatedBackupSummary()).thenReturn(backupSummary);

        assertSame(backupSummary, backup.getBackupSummary());
    }

    private Directory createDirectoryMock(long directoryId, String name)
    {
        Directory directory = mock(Directory.class);
        when(directory.getId()).thenReturn(directoryId);
        when(directory.getName()).thenReturn(name);
        return directory;
    }
}
