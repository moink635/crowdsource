package com.atlassian.crowd.console.action.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.crowd.console.action.ActionHelper;
import com.atlassian.crowd.directory.AbstractInternalDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.service.client.ClientProperties;

import com.google.common.collect.ImmutableList;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.Action;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChangePasswordTest
{
    private static final String USER = "user";
    private static final long DIRECTORY_ID = 1L;

    @InjectMocks
    private ChangePassword changePassword = new ChangePassword();

    @Mock private ApplicationService applicationService;
    @Mock private ApplicationManager applicationManager;
    @Mock private DirectoryManager directoryManager;
    @Mock private ClientProperties clientProperties;
    @Mock private Application application;
    @Mock private CrowdUserDetailsService userDetailsService;
    @Mock private ActionHelper actionHelper;

    @Before
    public void initSession() throws Exception
    {
        changePassword.setActionHelper(actionHelper);

        CrowdBootstrapManager bootstrapManager = mock(CrowdBootstrapManager.class);
        when(bootstrapManager.isSetupComplete()).thenReturn(true);
        BootstrapUtils.setBootstrapManager(bootstrapManager);

        HttpSession session = mock(HttpSession.class);
        when(session.getAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY))
            .thenReturn(USER);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);

        ServletActionContext.setRequest(request);

        UserWithAttributes principal = mock(UserWithAttributes.class);
        when(principal.getName()).thenReturn(USER);
        when(principal.getDirectoryId()).thenReturn(DIRECTORY_ID);
        when(actionHelper.getRemoteUser()).thenReturn(principal);

        CrowdUserDetails crowdUserDetails = mock(CrowdUserDetails.class);
        when(crowdUserDetails.getRemotePrincipal()).thenReturn(principal);

        Authentication authentication = mock(Authentication.class);
        when(authentication.getPrincipal()).thenReturn(crowdUserDetails);

        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @After
    public void releaseStatics()
    {
        ServletActionContext.setRequest(null);
        BootstrapUtils.setBootstrapManager(null);
        SecurityContextHolder.clearContext();
    }

    @Test
    public void shouldShowThePasswordComplexityRequirementIfSet() throws Exception
    {
        Directory directory = createDirectory("Password complexity message", "[some regex]");
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        assertEquals(Action.INPUT, changePassword.doDefault());
        assertEquals("Password complexity message", changePassword.getPasswordComplexityMessage());
    }

    @Test
    public void shouldNotShowThePasswordComplexityRequirementIfNotSet() throws Exception
    {
        Directory directory = createDirectory("", "");
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        assertEquals(Action.INPUT,changePassword.doDefault());
        assertNull(changePassword.getPasswordComplexityMessage());
    }

    @Test
    public void shouldNotChangeThePasswordIfTheOldPasswordIsInvalid() throws Exception
    {
        Directory directory = createDirectory("", "");
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        doThrow(new InvalidAuthenticationException("Bad password!"))
            .when(applicationService).authenticateUser(application, USER,
                                                       new PasswordCredential("badoriginalpassword"));

        changePassword.setOriginalPassword("badoriginalpassword");
        changePassword.setPassword("newpassword");
        changePassword.setConfirmPassword("newpassword");

        assertEquals(Action.INPUT, changePassword.doUpdate());
        assertEquals(ImmutableList.of("Password invalid."), changePassword.getFieldErrors().get("originalPassword"));

        verify(applicationService, never()).updateUserCredential(any(Application.class), anyString(),
                                                                 any(PasswordCredential.class));
    }

    @Test
    public void shouldNotChangeThePasswordIfNewPasswordDoesNotMeetRequirements() throws Exception
    {
        Directory directory = createDirectory("Password complexity message", "[some regex]");
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        doThrow(new InvalidCredentialException("Generic message", "Password complexity message from exception"))
            .when(applicationService).updateUserCredential(application, USER, new PasswordCredential("newpassword"));

        changePassword.setOriginalPassword("originalpassword");
        changePassword.setPassword("newpassword");
        changePassword.setConfirmPassword("newpassword");

        assertEquals(Action.INPUT, changePassword.doUpdate());
        assertEquals(ImmutableList.of(changePassword.getText("passwordupdate.policy.error.message")),
                     changePassword.getActionErrors());
        assertEquals(ImmutableList.of("Password complexity message from exception"),
                     changePassword.getFieldErrors().get("password"));
    }

    @Test
    public void shouldChangePassword() throws Exception
    {
        Directory directory = createDirectory("Password complexity message", "[some regex]");
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        changePassword.setOriginalPassword("originalpassword");
        changePassword.setPassword("newpassword");
        changePassword.setConfirmPassword("newpassword");

        assertEquals(Action.SUCCESS, changePassword.doUpdate());
        assertEquals(ImmutableList.of("Password updated."), changePassword.getActionMessages());

        verify(applicationService).authenticateUser(application, USER, new PasswordCredential("originalpassword"));
        verify(applicationService).updateUserCredential(application, USER, new PasswordCredential("newpassword"));
    }

    private static Directory createDirectory(String passwordComplexityMessaage, String regex)
    {
        DirectoryImpl directory = new DirectoryImpl();
        directory.setAttribute(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_REGEX, regex);
        directory.setAttribute(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_COMPLEXITY_MESSAGE, passwordComplexityMessaage);
        return directory;
    }
}
