package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.OpenLDAP;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.validator.ConnectorValidator;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdateConnectorConnectionTest
{
    private static final int DIRECTORY_ID = 10;

    private UpdateConnectorConnection updateConnectorConnection;

    private DirectoryImpl directory;
    @Mock private DirectoryManager directoryManager;
    @Mock private ConnectorValidator connectorValidator;
    @Mock private DirectoryInstanceLoader directoryInstanceLoader;
    @Mock private RemoteDirectory remoteDirectory;

    @Before
    public void createAndWireObjectUnderTest()
    {
        updateConnectorConnection = new UpdateConnectorConnection();
        updateConnectorConnection.setDirectoryManager(directoryManager);
        updateConnectorConnection.setDirectoryInstanceLoader(directoryInstanceLoader);
        updateConnectorConnection.setConnectorValidator(connectorValidator);
    }

    @Before
    public void createAuxiliaryObjects()
    {
        directory = new DirectoryImpl("My Directory", DirectoryType.CONNECTOR, OpenLDAP.class.toString());
    }

    @Test
    public void shouldPerformTrivialUpdate() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        // required fields
        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setURL("http://directory.test/");
        updateConnectorConnection.setBaseDN("dc=example");

        assertEquals(UpdateConnectorConnection.SUCCESS, updateConnectorConnection.doUpdate());

        ArgumentCaptor<Directory> directoryCaptor = ArgumentCaptor.forClass(Directory.class);
        verify(directoryManager).updateDirectory(directoryCaptor.capture());

        assertEquals("http://directory.test/",
                     directoryCaptor.getValue().getAttributes().get(LDAPPropertiesMapper.LDAP_URL_KEY));
        assertEquals("dc=example", directoryCaptor.getValue().getAttributes().get(LDAPPropertiesMapper.LDAP_BASEDN_KEY));

        verify(remoteDirectory, never()).testConnection();
    }

    @Test
    public void shouldNotPerformUpdateIfConnectionTestFails() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getRawDirectory(anyLong(), anyString(), anyMapOf(String.class, String.class)))
            .thenReturn(remoteDirectory);

        doThrow(new OperationFailedException("some error")).when(remoteDirectory).testConnection();

        // required fields
        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setURL("http://directory.test/");
        updateConnectorConnection.setBaseDN("dc=example");

        // only password changes trigger a connection test
        updateConnectorConnection.setLdapPassword("new password");

        assertEquals(UpdateConnectorConnection.ERROR, updateConnectorConnection.doUpdate());

        assertEquals("some error",
                     Iterables.getOnlyElement(updateConnectorConnection.getActionErrors()));

        verify(directoryManager, never()).updateDirectory(any(Directory.class));

        verify(remoteDirectory).testConnection();
    }

    @Test
    public void shouldReUseTheExistingPasswordWhenNoPasswordIsSetAndUserDNIsNotBlank() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_PASSWORD_KEY, "previous password");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        // required fields
        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setURL("http://directory.test/");
        updateConnectorConnection.setBaseDN("dc=example");

        // credentials
        updateConnectorConnection.setUserDN("valid DN");
        updateConnectorConnection.setLdapPassword("");

        assertEquals(UpdateConnectorConnection.SUCCESS, updateConnectorConnection.doUpdate());

        ArgumentCaptor<Directory> directoryCaptor = ArgumentCaptor.forClass(Directory.class);
        verify(directoryManager).updateDirectory(directoryCaptor.capture());

        assertEquals("valid DN", directoryCaptor.getValue().getAttributes().get(LDAPPropertiesMapper.LDAP_USERDN_KEY));
        assertEquals("previous password",
                     directoryCaptor.getValue().getAttributes().get(LDAPPropertiesMapper.LDAP_PASSWORD_KEY));
    }

    @Test
    public void shouldSwitchToAnonymousBindIfUserDNIsBlank() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_PASSWORD_KEY, "previous password");
        updateConnectorConnection.setSavedLdapPassword("previous password");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getRawDirectory(anyLong(), anyString(), anyMapOf(String.class, String.class)))
            .thenReturn(remoteDirectory);

        // required fields
        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setURL("http://directory.test/");
        updateConnectorConnection.setBaseDN("dc=example");

        // credentials
        updateConnectorConnection.setUserDN("");
        updateConnectorConnection.setLdapPassword("");

        assertEquals(UpdateConnectorConnection.SUCCESS, updateConnectorConnection.doUpdate());

        ArgumentCaptor<Directory> directoryCaptor = ArgumentCaptor.forClass(Directory.class);
        verify(directoryManager).updateDirectory(directoryCaptor.capture());

        assertEquals("", directoryCaptor.getValue().getAttributes().get(LDAPPropertiesMapper.LDAP_USERDN_KEY));
        assertEquals("", directoryCaptor.getValue().getAttributes().get(LDAPPropertiesMapper.LDAP_PASSWORD_KEY));
    }

    @Test
    public void shouldUseTheNewPasswordWhenPasswordIsSet() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_PASSWORD_KEY, "previous password");
        updateConnectorConnection.setSavedLdapPassword("previous password");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getRawDirectory(anyLong(), anyString(), anyMapOf(String.class, String.class)))
            .thenReturn(remoteDirectory);

        // required fields
        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setURL("http://directory.test/");
        updateConnectorConnection.setBaseDN("dc=example");

        // credentials
        updateConnectorConnection.setUserDN("valid DN");
        updateConnectorConnection.setLdapPassword("new password");

        assertEquals(UpdateConnectorConnection.SUCCESS, updateConnectorConnection.doUpdate());

        ArgumentCaptor<Directory> directoryCaptor = ArgumentCaptor.forClass(Directory.class);
        verify(directoryManager).updateDirectory(directoryCaptor.capture());

        assertEquals("valid DN", directoryCaptor.getValue().getAttributes().get(LDAPPropertiesMapper.LDAP_USERDN_KEY));
        assertEquals("new password", directoryCaptor.getValue().getAttributes().get(LDAPPropertiesMapper.LDAP_PASSWORD_KEY));

        verify(remoteDirectory).testConnection();
    }

    @Test
    public void shouldUseTheLDAPPasswordFromLdapPasswordKey() throws DirectoryNotFoundException
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_PASSWORD_KEY, "password");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);

        updateConnectorConnection.execute();

        assertThat(updateConnectorConnection.getLdapPassword(), is("password"));
    }

    @Test
    public void shouldInformAboutSuccessfulConfigurationTest() throws Exception
    {
        updateConnectorConnection.setID(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getRawDirectory(anyLong(), anyString(), anyMapOf(String.class, String.class)))
            .thenReturn(remoteDirectory);

        assertEquals(UpdateConnectorConnection.SUCCESS, updateConnectorConnection.doTestUpdateConfiguration());

        assertEquals(updateConnectorConnection.getText("directoryconnector.testconnection.success"),
                     Iterables.getOnlyElement(updateConnectorConnection.getActionMessages()));

        verify(remoteDirectory).testConnection();
    }

    @Test
    public void shouldInformAboutFailedConfigurationTest() throws Exception
    {
        updateConnectorConnection.setID(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getRawDirectory(anyLong(), anyString(), anyMapOf(String.class, String.class)))
            .thenReturn(remoteDirectory);

        doThrow(new OperationFailedException("some error")).when(remoteDirectory).testConnection();

        assertEquals(UpdateConnectorConnection.ERROR, updateConnectorConnection.doTestUpdateConfiguration());

        assertEquals(updateConnectorConnection.getText("directoryconnector.testconnection.invalid") + " some error",
                     Iterables.getOnlyElement(updateConnectorConnection.getActionErrors()));

        verify(remoteDirectory).testConnection();
    }

    @Test
    public void shouldRetainNewLdapPasswordOnSuccessfulTestConfiguration() throws Exception
    {
        updateConnectorConnection.setID(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getRawDirectory(anyLong(), anyString(), anyMapOf(String.class, String.class)))
            .thenReturn(remoteDirectory);

        updateConnectorConnection.setUserDN("userDn");
        updateConnectorConnection.setSavedLdapPassword("previous password");
        updateConnectorConnection.setLdapPassword("new password");

        assertEquals(UpdateConnectorConnection.SUCCESS, updateConnectorConnection.doTestUpdateConfiguration());

        assertEquals(updateConnectorConnection.getLdapPassword(), "new password");
    }

    @Test
    public void shouldRetainNewLdapPasswordOnFailedTestConfiguration() throws Exception
    {
        updateConnectorConnection.setID(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getRawDirectory(anyLong(), anyString(), anyMapOf(String.class, String.class)))
            .thenReturn(remoteDirectory);

        doThrow(new OperationFailedException("some error")).when(remoteDirectory).testConnection();

        updateConnectorConnection.setUserDN("userDn");
        updateConnectorConnection.setSavedLdapPassword("previous password");
        updateConnectorConnection.setLdapPassword("new password");

        assertEquals(UpdateConnectorConnection.ERROR, updateConnectorConnection.doTestUpdateConfiguration());

        assertEquals(updateConnectorConnection.getLdapPassword(), "new password");

        verify(remoteDirectory).testConnection();
    }

    @Test
    public void shouldLoadLocalUserStatusEnabledConfiguration() throws Exception
    {
        directory.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS, "true");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.execute();

        assertTrue(updateConnectorConnection.isLocalUserStatusEnabled());
    }

    @Test
    public void shouldLoadLocalUserStatusDisabledConfiguration() throws Exception
    {
        directory.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS, "false");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.execute();

        assertFalse(updateConnectorConnection.isLocalUserStatusEnabled());
    }

    @Test
    public void shouldEnableLocalUserStatusIfCacheIsEnabled() throws Exception
    {
        directory.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS, "false");
        directory.setAttribute(DirectoryProperties.CACHE_ENABLED, "true");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setLocalUserStatusEnabled(true);
        updateConnectorConnection.updateDirectory(directory);

        assertTrue(Boolean.valueOf(directory.getAttributes().get(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS)));
    }

    @Test
    public void shouldNotEnableLocalUserStatusIfCacheIsDisabled() throws Exception
    {
        directory.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS, "false");
        directory.setAttribute(DirectoryProperties.CACHE_ENABLED, "false");

        updateConnectorConnection.setLocalUserStatusEnabled(true);
        updateConnectorConnection.doValidation(directory);

        assertEquals(ImmutableList.of(updateConnectorConnection.getText("directoryconnector.localuserstatus.withoutcache.message")),
                     updateConnectorConnection.getFieldErrors().get("localUserStatusEnabled"));
    }

    @Test
    public void shouldDisableLocalUserStatus() throws Exception
    {
        directory.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS, "true");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setLocalUserStatusEnabled(false);
        updateConnectorConnection.updateDirectory(directory);

        assertFalse(Boolean.valueOf(directory.getAttributes().get(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS)));
    }

    @Test
    public void shouldLoadLocalGroupsEnabledConfiguration() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LOCAL_GROUPS, "true");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.execute();

        assertTrue(updateConnectorConnection.isLocalGroupsEnabled());
    }

    @Test
    public void shouldLoadLocalGroupsDisabledConfiguration() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LOCAL_GROUPS, "false");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.execute();

        assertFalse(updateConnectorConnection.isLocalGroupsEnabled());
    }

    @Test
    public void shouldEnableLocalGroupsIfCacheIsEnabled() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LOCAL_GROUPS, "false");
        directory.setAttribute(DirectoryProperties.CACHE_ENABLED, "true");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setLocalGroupsEnabled(true);
        updateConnectorConnection.updateDirectory(directory);

        assertTrue(Boolean.valueOf(directory.getAttributes().get(LDAPPropertiesMapper.LOCAL_GROUPS)));
    }

    @Test
    public void shouldDisableLocalGroups() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LOCAL_GROUPS, "true");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setLocalGroupsEnabled(false);
        updateConnectorConnection.updateDirectory(directory);

        assertFalse(Boolean.valueOf(directory.getAttributes().get(LDAPPropertiesMapper.LOCAL_GROUPS)));
    }

    @Test
    public void shouldLoadNestedGroupsEnabledConfiguration() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, "false");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.execute();

        assertTrue("Nested groups support should be enabled",
                updateConnectorConnection.isUseNestedGroups());
    }

    @Test
    public void shouldLoadNestedGroupsDisabledConfiguration() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, "true");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.execute();

        assertFalse("Nested groups support should be disabled",
                updateConnectorConnection.isUseNestedGroups());
    }

    @Test
    public void shouldEnableNestedGroups() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, "true");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setUseNestedGroups(true);
        updateConnectorConnection.updateDirectory(directory);

        assertFalse("Nested groups support should be enabled",
                Boolean.valueOf(directory.getAttributes().get(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED)));
    }

    @Test
    public void shouldDisableNestedGroups() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, "false");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setUseNestedGroups(false);
        updateConnectorConnection.updateDirectory(directory);

        assertTrue("Nested groups support should be disabled",
                Boolean.valueOf(directory.getAttributes().get(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED)));
    }

    @Test
    public void shouldLoadPrimaryGroupSupportConfiguration() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.PRIMARY_GROUP_SUPPORT, "true");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.execute();

        assertTrue("Primary group support should be enabled",
                   updateConnectorConnection.isPrimaryGroupSupport());
    }

    @Test
    public void shouldEnablePrimaryGroupSupport() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.PRIMARY_GROUP_SUPPORT, "false");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setPrimaryGroupSupport(true);
        updateConnectorConnection.updateDirectory(directory);

        assertTrue("Primary group support should be enabled",
                   Boolean.valueOf(directory.getAttributes().get(LDAPPropertiesMapper.PRIMARY_GROUP_SUPPORT)));
    }

    @Test
    public void shouldDisablePrimaryGroupSupport() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.PRIMARY_GROUP_SUPPORT, "true");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setUseNestedGroups(false);
        updateConnectorConnection.updateDirectory(directory);

        assertFalse("Primary group support should be disabled",
                    Boolean.valueOf(directory.getAttributes().get(LDAPPropertiesMapper.PRIMARY_GROUP_SUPPORT)));
    }

    @Test
    public void shouldNotEnableLocalGroupsWithUncachedDirectory() throws Exception
    {
        directory.setAttribute(DirectoryProperties.CACHE_ENABLED, "false");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        // required fields
        updateConnectorConnection.setID(DIRECTORY_ID);
        updateConnectorConnection.setURL("http://directory.test/");
        updateConnectorConnection.setBaseDN("dc=example");

        updateConnectorConnection.setLocalGroupsEnabled(true);

        assertEquals(UpdateConnectorConnection.ERROR, updateConnectorConnection.doUpdate());

        assertEquals(ImmutableList.of(updateConnectorConnection.getText("directoryconnector.update.invalid")),
                     updateConnectorConnection.getActionErrors());

        assertEquals(
            ImmutableList.of(updateConnectorConnection.getText("directoryconnector.localgroups.withoutcache.message")),
            updateConnectorConnection.getFieldErrors().get("localGroupsEnabled"));

        verify(directoryManager, never()).updateDirectory(any(Directory.class));
    }
}
