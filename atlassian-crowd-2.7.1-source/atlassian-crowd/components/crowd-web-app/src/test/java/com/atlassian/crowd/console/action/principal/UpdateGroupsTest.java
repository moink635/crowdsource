package com.atlassian.crowd.console.action.principal;

import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;
import com.atlassian.crowd.util.AdminGroupChecker;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdateGroupsTest
{
    private static final long DIRECTORY_ID = 1L;

    @Spy private UpdateGroups updateGroups;

    @Mock private AdminGroupChecker adminGroupChecker;
    @Mock private DirectoryManager directoryManager;

    @Before
    public void setUp() throws Exception
    {
        updateGroups.setAdminGroupChecker(adminGroupChecker);
        updateGroups.setDirectoryManager(directoryManager);
        updateGroups.setDirectoryID(DIRECTORY_ID);

        UserTemplateWithAttributes principal = UserTemplateWithAttributes.ofUserWithNoAttributes(new UserTemplate("user", DIRECTORY_ID));
        doReturn(principal).when(updateGroups).getRemoteUser();
    }

    @Test
    public void doRemoveGroupsActuallyRemovesGroups() throws Exception
    {
        when(adminGroupChecker.isRemovingCrowdConsoleAdminMembership("user", DIRECTORY_ID, "user", DIRECTORY_ID))
            .thenReturn(false);

        updateGroups.setEntityName("user");
        updateGroups.setSelectedEntityNames(ImmutableList.of("group1", "group2"));

        assertEquals(UpdateGroups.SUCCESS, updateGroups.doRemoveGroups());

        assertTrue(updateGroups.isUpdateSuccessful());
        assertEquals("", updateGroups.getUnremovedGroups());

        verify(directoryManager).removeUserFromGroup(DIRECTORY_ID, "user", "group1");
        verify(directoryManager).removeUserFromGroup(DIRECTORY_ID, "user", "group2");
    }

    @Test
    public void doRemoveGroupsWhenAGroupFailsToBeRemoved() throws Exception
    {
        when(adminGroupChecker.isRemovingCrowdConsoleAdminMembership("user", DIRECTORY_ID, "user", DIRECTORY_ID))
            .thenReturn(false);

        doThrow(new OperationFailedException(""))
            .when(directoryManager).removeUserFromGroup(DIRECTORY_ID, "user", "group1");

        updateGroups.setEntityName("user");
        updateGroups.setSelectedEntityNames(ImmutableList.of("group1", "group2"));

        assertEquals(UpdateGroups.SUCCESS, updateGroups.doRemoveGroups());

        assertFalse(updateGroups.isUpdateSuccessful());
        assertEquals("group1", updateGroups.getUnremovedGroups());

        verify(directoryManager).removeUserFromGroup(DIRECTORY_ID, "user", "group1");
        verify(directoryManager).removeUserFromGroup(DIRECTORY_ID, "user", "group2");
    }
}
