package com.atlassian.crowd.console.action.group;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ViewGroupNestedPrincipalsTest
{
    private static final long DIRECTORY_ID = 1L;

    private ViewGroupNestedPrincipals viewGroupNestedPrincipals;
    @Mock private DirectoryManager directoryManager;
    @Mock private Directory directory;

    @Before
    public void setUp() throws Exception
    {
        viewGroupNestedPrincipals = new ViewGroupNestedPrincipals();
        viewGroupNestedPrincipals.setDirectoryManager(directoryManager);
    }

    @Test
    public void testExecute() throws Exception
    {
        viewGroupNestedPrincipals.setDirectoryID(DIRECTORY_ID);
        viewGroupNestedPrincipals.setGroupName("groupname");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        User user1 = new UserTemplate("user1");
        User user2 = new UserTemplate("user2");
        final MembershipQuery<User> nestedUsersQuery =
            QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                .childrenOf(EntityDescriptor.group(GroupType.GROUP))
                .withName("groupname")
                .returningAtMost(EntityQuery.ALL_RESULTS);
        when(directoryManager.searchNestedGroupRelationships(DIRECTORY_ID, nestedUsersQuery))
            .thenReturn(ImmutableList.of(user2, user1)); // note the immutability of the *unsorted* list

        assertEquals(ViewGroupNestedPrincipals.SUCCESS, viewGroupNestedPrincipals.execute());

        assertThat(viewGroupNestedPrincipals.getPrincipals(), hasItems(user1, user2));
    }
}
