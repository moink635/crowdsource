package com.atlassian.crowd.console.action.directory;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;

import com.google.common.collect.ImmutableList;
import com.opensymphony.webwork.ServletActionContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdateConnectorTest
{
    private static final long DIRECTORY_ID = 1L;
    private static final String DIRECTORY_NAME = "directory-name";

    @InjectMocks
    private UpdateConnector updateConnector;

    private HttpServletRequest request;
    @Mock private DirectoryManager directoryManager;
    @Mock private Directory directory;

    @Before
    public void initStatics() throws Exception
    {
        request = new MockHttpServletRequest();
        ServletActionContext.setRequest(request);
    }

    @After
    public void disposeStatics() throws Exception
    {
        ServletActionContext.setRequest(null);
    }

    @Test
    public void trivialUpdateShouldSucceed() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directory.getName()).thenReturn(DIRECTORY_NAME);
        when(directory.getType()).thenReturn(DirectoryType.CONNECTOR);
        when(directory.getImplementationClass()).thenReturn("implementation.Class");

        updateConnector.setID(DIRECTORY_ID);
        updateConnector.setName(DIRECTORY_NAME);

        assertEquals(UpdateConnector.SUCCESS, updateConnector.execute());

        assertEquals(Boolean.TRUE, request.getAttribute("updateSuccessful"));

        verify(directoryManager).updateDirectory(any(Directory.class));
    }

    @Test
    public void cannotDisableCacheIfLocalUserStatusIsEnabled() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directory.getName()).thenReturn(DIRECTORY_NAME);
        when(directory.getType()).thenReturn(DirectoryType.CONNECTOR);
        when(directory.getImplementationClass()).thenReturn("implementation.Class");
        when(directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS)).thenReturn("true");

        updateConnector.setID(DIRECTORY_ID);
        updateConnector.setName(DIRECTORY_NAME);
        updateConnector.setCacheEnabled(false);

        assertEquals(UpdateConnector.ERROR, updateConnector.execute());

        assertEquals(ImmutableList.of(updateConnector.getText("directoryconnector.localuserstatus.withoutcache.message")),
                     updateConnector.getFieldErrors().get("cacheEnabled"));

        verify(directoryManager, never()).updateDirectory(any(Directory.class));
    }

    @Test
    public void cannotDisableCacheIfLocalGroupsAreEnabled() throws Exception
    {
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directory.getName()).thenReturn(DIRECTORY_NAME);
        when(directory.getType()).thenReturn(DirectoryType.CONNECTOR);
        when(directory.getImplementationClass()).thenReturn("implementation.Class");
        when(directory.getValue(LDAPPropertiesMapper.LOCAL_GROUPS)).thenReturn("true");

        updateConnector.setID(DIRECTORY_ID);
        updateConnector.setName(DIRECTORY_NAME);
        updateConnector.setCacheEnabled(false);

        assertEquals(UpdateConnector.ERROR, updateConnector.execute());

        assertEquals(ImmutableList.of(updateConnector.getText("directoryconnector.localgroups.withoutcache.message")),
                     updateConnector.getFieldErrors().get("cacheEnabled"));

        verify(directoryManager, never()).updateDirectory(any(Directory.class));
    }
}
