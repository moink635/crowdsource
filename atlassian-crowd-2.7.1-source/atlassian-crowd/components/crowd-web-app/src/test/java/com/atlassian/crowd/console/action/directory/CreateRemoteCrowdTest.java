package com.atlassian.crowd.console.action.directory;

import java.util.Map;

import com.atlassian.crowd.directory.RemoteCrowdDirectory;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.directory.DirectoryManager;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CreateRemoteCrowdTest
{
    private static final Map<String, String> DIRECTORY_ATTRIBUTES = ImmutableMap.<String,String>builder()
        .put("crowd.server.url", "url")
        .put("application.name", "appname")
        .put("application.password", "apppassword")
        .put("crowd.server.http.proxy.host", "proxyhost")
        .put("crowd.server.http.proxy.port", "8080")
        .put("crowd.server.http.proxy.username", "proxyuser")
        .put("crowd.server.http.proxy.password", "proxypassword")
        .put("crowd.server.http.timeout", "5000")
        .put("directory.cache.synchronise.interval", "3600")
        .put("crowd.server.http.max.connections", "20")
        .put("com.atlassian.crowd.directory.sync.cache.enabled", "false")
        .put("crowd.sync.incremental.enabled", "false")
        .put("useNestedGroups", "false")
        .build();

    private CreateRemoteCrowd action;

    @Mock private DirectoryManager directoryManager;
    @Mock private DirectoryInstanceLoader directoryInstanceLoader;
    @Mock private RemoteDirectory remoteDirectory;

    @Before
    public void createObjectUnderTest() throws Exception
    {
        action = new CreateRemoteCrowd();
        action.setDirectoryManager(directoryManager);
        action.setDirectoryInstanceLoader(directoryInstanceLoader);
    }

    @Test
    public void isInitialLoadBeforeUpdate() throws Exception
    {
        action.doDefault();

        assertTrue(action.isInitialLoad());
    }

    @Test
    public void noLongerIsInitialLoadAfterUpdate() throws Exception
    {
        action.doUpdate();

        assertFalse(action.isInitialLoad());
    }

    @Test
    public void doDefaultSetsDefaultValues() throws Exception
    {
        assertEquals(CreateRemoteCrowd.INPUT, action.doDefault());

        assertEquals("http://hostname:8095/crowd", action.getUrl());
        assertTrue("Cache should be enabled by default", action.isCacheEnabled());
        assertFalse("Nested groups should be disabled by default", action.isUseNestedGroups());
        assertTrue("Incremental sync should be enabled by default", action.isIncrementalSyncEnabled());
    }

    @Test
    public void doUpdateDoesValidation() throws Exception
    {
        action.setName("");
        action.setUrl("");
        action.setApplicationName("");
        action.setApplicationPassword("");
        action.setPollingIntervalInMin(-1);
        action.setHttpMaxConnections(-1);
        action.setHttpProxyPort(-1);

        assertEquals(CreateRemoteCrowd.INPUT, action.doUpdate());

        assertEquals(ImmutableList.of(action.getText("directorycrowd.update.invalid")), action.getActionErrors());

        // messages for required fields
        assertEquals(ImmutableList.of(action.getText("directoryinternal.name.invalid")),
                     action.getFieldErrors().get("name"));
        assertEquals(ImmutableList.of(action.getText("directorycrowd.url.invalid")),
                     action.getFieldErrors().get("url"));
        assertEquals(ImmutableList.of(action.getText("directorycrowd.applicationname.invalid")),
                     action.getFieldErrors().get("applicationName"));
        assertEquals(ImmutableList.of(action.getText("directorycrowd.applicationpassword.invalid")),
                     action.getFieldErrors().get("applicationPassword"));

        // messages for invalid values
        assertEquals(ImmutableList.of(action.getText("directorycrowd.polling.interval.invalid")),
                     action.getFieldErrors().get("pollingIntervalInMin"));
        assertEquals(ImmutableList.of(action.getText("directorycrowd.http.maxconnections.invalid")),
                     action.getFieldErrors().get("httpMaxConnections"));
        assertEquals(ImmutableList.of(action.getText("directorycrowd.proxy.port.invalid")),
                     action.getFieldErrors().get("httpProxyPort"));
    }

    @Test
    public void shouldFailIfDuplicatedDirectoryName() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directoryManager.findDirectoryByName("dirname")).thenReturn(directory);

        action.setName("dirname");
        action.setUrl("url");
        action.setApplicationName("appname");
        action.setApplicationPassword("apppassword");

        assertEquals(CreateRemoteCrowd.INPUT, action.doUpdate());

        assertEquals(ImmutableList.of(action.getText("directorycrowd.update.invalid")), action.getActionErrors());

        assertEquals(ImmutableList.of(action.getText("invalid.namealreadyexist")),
                     action.getFieldErrors().get("name"));
    }

    @Test
    public void doUpdateDoesNotSaveDirectoryWhenConnectionTestFails() throws Exception
    {
        when(directoryManager.findDirectoryByName("dirname")).thenThrow(new DirectoryNotFoundException("dirname"));

        when(directoryInstanceLoader.getRawDirectory(null, RemoteCrowdDirectory.class.getCanonicalName(), DIRECTORY_ATTRIBUTES))
            .thenReturn(remoteDirectory);

        doThrow(new OperationFailedException("connection failed")).when(remoteDirectory).testConnection();

        _setAllFields();

        assertEquals(CreateRemoteCrowd.INPUT, action.doUpdate());

        assertEquals(ImmutableList.of(action.getText("directorycrowd.testconnection.invalid") + " connection failed"),
                     action.getActionErrors());
    }

    @Test
    public void doUpdateSavesDirectoryWhenConnectionTestSucceeds() throws Exception
    {
        when(directoryManager.findDirectoryByName("dirname")).thenThrow(new DirectoryNotFoundException("dirname"));

        when(directoryInstanceLoader.getRawDirectory(null,
                                                     RemoteCrowdDirectory.class.getCanonicalName(),
                                                     DIRECTORY_ATTRIBUTES))
            .thenReturn(remoteDirectory);

        Directory returnedDirectory = mock(Directory.class);
        when(returnedDirectory.getId()).thenReturn(1L);
        when(directoryManager.addDirectory(any(Directory.class))).thenReturn(returnedDirectory);

        _setAllFields();

        assertEquals(CreateRemoteCrowd.SUCCESS, action.doUpdate());

        assertEquals(1L, action.getID());

        verify(directoryManager).findDirectoryByName("dirname");

        verify(remoteDirectory).testConnection();

        ArgumentCaptor<Directory> directoryCaptor = ArgumentCaptor.forClass(Directory.class);
        verify(directoryManager).addDirectory(directoryCaptor.capture());
        assertNull(directoryCaptor.getValue().getId());
        assertEquals(RemoteCrowdDirectory.class.getCanonicalName(), directoryCaptor.getValue().getImplementationClass());
        assertEquals(DIRECTORY_ATTRIBUTES, directoryCaptor.getValue().getAttributes());
    }

    @Test
    public void doTestConnectionDoesValidation() throws Exception
    {
        action.setName("");
        action.setUrl("");
        action.setApplicationName("");
        action.setApplicationPassword("");
        action.setPollingIntervalInMin(-1);
        action.setHttpMaxConnections(-1);
        action.setHttpProxyPort(-1);

        assertEquals(CreateRemoteCrowd.INPUT, action.doTestConnection());

        // messages for required fields
        assertEquals(ImmutableList.of(action.getText("directoryinternal.name.invalid")),
                     action.getFieldErrors().get("name"));
        assertEquals(ImmutableList.of(action.getText("directorycrowd.url.invalid")),
                     action.getFieldErrors().get("url"));
        assertEquals(ImmutableList.of(action.getText("directorycrowd.applicationname.invalid")),
                     action.getFieldErrors().get("applicationName"));
        assertEquals(ImmutableList.of(action.getText("directorycrowd.applicationpassword.invalid")),
                     action.getFieldErrors().get("applicationPassword"));

        // messages for invalid values
        assertEquals(ImmutableList.of(action.getText("directorycrowd.polling.interval.invalid")),
                     action.getFieldErrors().get("pollingIntervalInMin"));
        assertEquals(ImmutableList.of(action.getText("directorycrowd.http.maxconnections.invalid")),
                     action.getFieldErrors().get("httpMaxConnections"));
        assertEquals(ImmutableList.of(action.getText("directorycrowd.proxy.port.invalid")),
                     action.getFieldErrors().get("httpProxyPort"));
    }

    @Test
    public void doTestConnectionWhenTestFails() throws Exception
    {
        when(directoryManager.findDirectoryByName("dirname")).thenThrow(new DirectoryNotFoundException("dirname"));

        when(directoryInstanceLoader.getRawDirectory(null, RemoteCrowdDirectory.class.getCanonicalName(),
                                                     DIRECTORY_ATTRIBUTES))
            .thenReturn(remoteDirectory);

        doThrow(new OperationFailedException("connection failed")).when(remoteDirectory).testConnection();

        _setAllFields();

        assertEquals(CreateRemoteCrowd.INPUT, action.doTestConnection());

        assertEquals(ImmutableList.of(action.getText("directorycrowd.testconnection.invalid") + " connection failed"),
                     action.getActionErrors());

        assertEquals("Should return to the same tab the button is in", Integer.valueOf(2), action.getTab());
    }

    @Test
    public void doTestConnectionWhenTestSucceeds() throws Exception
    {
        when(directoryManager.findDirectoryByName("dirname")).thenThrow(new DirectoryNotFoundException("dirname"));

        when(directoryInstanceLoader.getRawDirectory(null, RemoteCrowdDirectory.class.getCanonicalName(),
                                                     DIRECTORY_ATTRIBUTES))
            .thenReturn(remoteDirectory);

        _setAllFields();

        assertEquals(CreateRemoteCrowd.SUCCESS, action.doTestConnection());

        assertEquals(ImmutableList.of(action.getText("directorycrowd.testconnection.success")),
                     action.getActionMessages());

        assertEquals(CreateRemoteCrowd.ALERT_BLUE, action.getActionMessageAlertColor());

        assertEquals("Should return to the same tab the button is in", Integer.valueOf(2), action.getTab());

        verify(remoteDirectory).testConnection();
    }

    @Test
    public void doUpdateShouldUpdateTheApplicationPasswordIfEmptyAndAlreadyPresentInSavedApplicationPassword()
    {
        action.setSavedApplicationPassword("saved_password");
        action.setApplicationPassword("");
        action.doUpdate();

        assertEquals("saved_password", action.getApplicationPassword()) ;
    }

    @Test
    public void doUpdateShouldResetTheSavedApplicationPasswordWithTheNewApplicationPasswordIfPresent()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setSavedLdapPassword("saved_password");
        createConnector.setLdapPassword("new_password");
        action.doUpdate();

        assertEquals("new_password", createConnector.getLdapPassword());
    }

    @Test
    public void doTestConnectionShouldUpdateTheApplicationPasswordIfEmptyAndAlreadyPresentInSavedApplicationPassword()
    {
        action.setSavedApplicationPassword("saved_password");
        action.setApplicationPassword("");
        action.doTestConnection();

        assertEquals("saved_password", action.getApplicationPassword()) ;
    }

    @Test
    public void doTestConnectionShouldResetTheSavedApplicationPasswordWithTheNewApplicationPasswordIfPresent()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setSavedLdapPassword("saved_password");
        createConnector.setLdapPassword("new_password");
        action.doTestConnection();

        assertEquals("new_password", createConnector.getLdapPassword());
    }

    private void _setAllFields()
    {
        action.setName("dirname");
        action.setUrl("url");
        action.setApplicationName("appname");
        action.setApplicationPassword("apppassword");
        action.setHttpProxyHost("proxyhost");
        action.setHttpProxyPort(8080);
        action.setHttpProxyUsername("proxyuser");
        action.setHttpProxyPassword("proxypassword");
    }
}
