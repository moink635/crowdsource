package com.atlassian.crowd.console.action.error;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventType;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ErrorActionTest
{
    private static final EventType IMPORT_ERROR_EVENT_TYPE = new EventType("import", "Import event type");
    private static final EventType ANOTHER_ERROR_EVENT_TYPE = new EventType("another", "Import event type");

    private static final Event IMPORT_ERROR = new Event(IMPORT_ERROR_EVENT_TYPE, "");
    private static final Event ANOTHER_ERROR = new Event(ANOTHER_ERROR_EVENT_TYPE, "");

    private ErrorAction errorAction;

    @Mock private HttpServletResponse response;
    @Mock private JohnsonEventContainer johnsonEventContainer;

    @Before
    public void setUp() throws Exception
    {
        errorAction = new ErrorAction();
        errorAction.setServletResponse(response);
    }

    @Test
    public void shouldWorkEvenContextIsNotInitialised() throws Exception
    {
        // no context means no collaboration injection

        assertEquals(ErrorAction.ERROR, errorAction.execute());

        // getters called from view
        assertEquals(ImmutableMap.of(), errorAction.getEventMap());
        assertEquals(ImmutableList.of(), errorAction.getImportErrors());
    }

    @Test
    public void shouldReturnStatusCode503() throws Exception
    {
        errorAction.execute();

        verify(response).setStatus(503);
    }

    @Test
    public void shouldLoadImportErrors() throws Exception
    {
        when(johnsonEventContainer.getEvents()).thenReturn(ImmutableList.of(IMPORT_ERROR, ANOTHER_ERROR));
        when(johnsonEventContainer.hasEvents()).thenReturn(true);

        errorAction.setJohnsonEventContainer(johnsonEventContainer);

        assertEquals(ErrorAction.ERROR, errorAction.execute());

        assertEquals(ImmutableList.of(IMPORT_ERROR), errorAction.getImportErrors());
    }

    @Test
    public void shouldLoadEventMap() throws Exception
    {
        when(johnsonEventContainer.getEvents()).thenReturn(ImmutableList.of(IMPORT_ERROR, ANOTHER_ERROR));
        when(johnsonEventContainer.hasEvents()).thenReturn(true);

        errorAction.setJohnsonEventContainer(johnsonEventContainer);

        assertEquals(ErrorAction.ERROR, errorAction.execute());

        assertEquals(ImmutableMap.of(ANOTHER_ERROR_EVENT_TYPE, ImmutableList.of(ANOTHER_ERROR)), errorAction.getEventMap());
    }

    @Test
    public void shouldRemoveImportEvents() throws Exception
    {
        when(johnsonEventContainer.getEvents()).thenReturn(ImmutableList.of(IMPORT_ERROR, ANOTHER_ERROR));

        errorAction.setJohnsonEventContainer(johnsonEventContainer);

        assertEquals(ErrorAction.SUCCESS, errorAction.removeImportErrorEvent());

        verify(johnsonEventContainer).removeEvent(IMPORT_ERROR);
        verify(johnsonEventContainer, never()).removeEvent(ANOTHER_ERROR);
    }
}
