package com.atlassian.crowd.console.action.setup;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.atlassian.config.setup.SetupPersister;
import com.atlassian.crowd.console.action.ActionHelper;
import com.atlassian.crowd.manager.application.CrowdApplicationPasswordManager;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.upgrade.UpgradeManager;
import com.atlassian.crowd.service.client.ClientProperties;

import com.google.common.collect.ImmutableList;
import com.opensymphony.webwork.ServletActionContext;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CompleteTest
{
    public static final String ADMIN_NAME = "admin";
    @InjectMocks
    private Complete complete = new Complete();

    @Mock
    private CrowdBootstrapManager bootstrapManager;

    @Mock
    private SetupPersister setupPersister;

    @Mock
    private UpgradeManager upgradeManager;

    @Mock
    private ClientProperties clientProperties;

    @Mock
    private CrowdApplicationPasswordManager crowdApplicationPasswordManager;

    @Mock
    private ActionHelper actionHelper;

    @Mock
    private HttpSession session;

    @Before
    public void initStatics()
    {
        when(session.getAttribute(DefaultAdministrator.DEFAULT_ADMIN_NAME_KEY)).thenReturn(ADMIN_NAME);

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);

        ServletActionContext.setRequest(request);
    }

    @After
    public void releaseStatics()
    {
        ServletActionContext.setRequest(null);
    }

    @Before
    public void setUp()
    {
        when(actionHelper.getBootstrapManager()).thenReturn(bootstrapManager);

        when(bootstrapManager.isApplicationHomeValid()).thenReturn(true);

        when(setupPersister.getCurrentStep()).thenReturn(Complete.COMPLETE_STEP);

        when(clientProperties.getApplicationName()).thenReturn("appname");
        when(clientProperties.getApplicationPassword()).thenReturn("secret");
        when(clientProperties.getApplicationAuthenticationURL()).thenReturn("http://example.test/");
    }

    @Test
    public void shouldCompleteSetupAndShowMessageInLoginPage() throws Exception
    {
        assertEquals(Complete.SUCCESS, complete.execute());

        assertEquals(ImmutableList.of(complete.getText("setupcomplete.text", new String[] { ADMIN_NAME })),
                     complete.getActionMessages());
        assertThat((Collection<?>) complete.getActionErrors(), Matchers.empty());

        verify(upgradeManager).doUpgrade();
        verify(crowdApplicationPasswordManager).resetCrowdPasswordIfRequired();
    }

    @Test
    public void shouldFailIfUpgradesFail() throws Exception
    {
        when(upgradeManager.doUpgrade()).thenReturn(ImmutableList.of("upgrade-failed"));

        assertEquals(Complete.ERROR, complete.execute());

        assertEquals(ImmutableList.of("upgrade-failed"), complete.getActionErrors());
    }

    @Test
    public void noMessageIsShownWhenAdminUserIsNoLongerAvailableInSession() throws Exception
    {
        when(session.getAttribute(DefaultAdministrator.DEFAULT_ADMIN_NAME_KEY)).thenReturn(null);

        assertEquals(Complete.SUCCESS, complete.execute());

        assertThat((Collection<String>) complete.getActionMessages(), Matchers.empty());
    }
}
