package com.atlassian.crowd.console.action.directory;

import java.util.HashMap;
import java.util.Map;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.model.directory.DirectoryImpl;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class CreateDelegatedTest
{
    @Mock
    private DirectoryInstanceLoader directoryInstanceLoader;

    @InjectMocks
    private CreateDelegated createDelegated;

    @Test
    public void shouldSetTheInitialLoadFlag()
    {
        createDelegated.doDefault();

        assertTrue("The first request is an initial load", createDelegated.isInitialLoad());
    }

    @Test
    public void shouldClearTheInitialLoadFlagAfterUpdate()
    {
        createDelegated.doUpdate();

        assertFalse("After an update, it is no longer an initial load", createDelegated.isInitialLoad());
    }

    @Test
    public void shouldClearTheInitialLoadFlagAfterConnectionTest()
    {
        createDelegated.doTestConfiguration();

        assertFalse("After a connection test, it is no longer an initial load", createDelegated.isInitialLoad());
    }

    @Test
    public void shouldUseLDAPAttributeForNestedGroupsButNotInternalDirectoryKey()
    {
        createDelegated.setUseNestedGroups(true);

        Map<String, String> attributes = new HashMap<String, String>();
        createDelegated.populateDirectoryAttributesForConnectionTest(attributes);

        assertEquals("false", attributes.get(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED));
        assertFalse("Should not use internal directory key", attributes.containsKey(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS));
    }
}
