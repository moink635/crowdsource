package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.validator.ConnectorValidator;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.directory.DirectoryManager;

import com.google.common.collect.Iterables;
import com.opensymphony.xwork.Action;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdateConnectorConfigurationTest
{
    private static final long DIRECTORY_ID = 1L;

    @Mock
    private DirectoryManager directoryManager;

    @Mock
    private ConnectorValidator connectorValidator;

    @InjectMocks
    private UpdateConnectorConfiguration updateConnectorConfiguration;

    @Test
    public void shouldFetchExternalIdAttribute() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getValue(LDAPPropertiesMapper.LDAP_EXTERNAL_ID)).thenReturn("my-external-id-attribute");
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateConnectorConfiguration.setID(DIRECTORY_ID);
        assertEquals(UpdateConnectorConfiguration.SUCCESS, updateConnectorConfiguration.execute());

        assertEquals("my-external-id-attribute", updateConnectorConfiguration.getUserExternalIdAttr());
    }

    @Test
    public void shouldUpdateDirectoryAttributes() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getName()).thenReturn("my-dir");
        when(directory.getType()).thenReturn(DirectoryType.CONNECTOR);
        when(directory.getImplementationClass()).thenReturn("my.implementation.Class");
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        fillRequiredFields();
        updateConnectorConfiguration.setID(DIRECTORY_ID);
        updateConnectorConfiguration.setUserExternalIdAttr("my-external-id-attr");

        assertEquals(UpdateConnectorConfiguration.SUCCESS, updateConnectorConfiguration.doUpdate());

        ArgumentCaptor<Directory> directoryCaptor = ArgumentCaptor.forClass(Directory.class);
        verify(directoryManager).updateDirectory(directoryCaptor.capture());

        assertEquals("my-external-id-attr", directoryCaptor.getValue().getValue(LDAPPropertiesMapper.LDAP_EXTERNAL_ID));
    }

    private void fillRequiredFields()
    {
        updateConnectorConfiguration.setUserFirstnameAttr("my-user-fname-attr");
        updateConnectorConfiguration.setUserGroupMemberAttr("my-user-group-member-attr");
        updateConnectorConfiguration.setUserLastnameAttr("my-user-lname-attr");
        updateConnectorConfiguration.setUserDisplayNameAttr("my-user-displayname-attr");
        updateConnectorConfiguration.setUserMailAttr("my-user-email-attr");
        updateConnectorConfiguration.setUserNameAttr("my-user-name-attr");
        updateConnectorConfiguration.setUserNameRdnAttr("my-user-name-rdn-attr");
        updateConnectorConfiguration.setUserObjectClass("my-user-object-class");
        updateConnectorConfiguration.setUserObjectFilter("my-user-object-filter");
        updateConnectorConfiguration.setUserPasswordAttr("my-user-password-attr");

        updateConnectorConfiguration.setGroupDescriptionAttr("my-group-description-attr");
        updateConnectorConfiguration.setGroupMemberAttr("my-group-member-attr");
        updateConnectorConfiguration.setGroupNameAttr("my-group-name-attr");
        updateConnectorConfiguration.setGroupObjectClass("my-group-object-class");
        updateConnectorConfiguration.setGroupObjectFilter("my-group-object-filter");
    }

    @Test
    public void doingTestSearchShouldReturnSuccessWhenLdapConfigTesterSucceeds() throws OperationFailedException
    {
        LDAPConfiguration ldapConfig = mock(LDAPConfiguration.class);
        LDAPConfigurationTester ldapConfigTester = mock(LDAPConfigurationTester.class);
        when(ldapConfigTester.canFindLdapObjects(ldapConfig, LDAPConfigurationTester.Strategy.GROUP)).thenReturn(true);

        UpdateConnectorConfiguration updateConnectorConfig = new UpdateConnectorConfiguration();
        updateConnectorConfig.setLdapConfigurationTester(ldapConfigTester);
        String result = updateConnectorConfig.doTestSearch(ldapConfig, LDAPConfigurationTester.Strategy.GROUP);

        assertEquals(Action.SUCCESS, result);
        String actionMessage = (String) Iterables.getOnlyElement(updateConnectorConfig.getActionMessages());
        assertThat(actionMessage, equalTo(updateConnectorConfig.getText("directoryconnector.testsearch.success")));
    }

    @Test
    public void doingTestSearchShouldReturnSuccessWhenLdapConfigTesterFails() throws OperationFailedException
    {
        LDAPConfiguration ldapConfig = mock(LDAPConfiguration.class);
        LDAPConfigurationTester ldapConfigTester = mock(LDAPConfigurationTester.class);
        when(ldapConfigTester.canFindLdapObjects(ldapConfig, LDAPConfigurationTester.Strategy.GROUP)).thenReturn(false);

        UpdateConnectorConfiguration updateConnectorConfig = new UpdateConnectorConfiguration();
        updateConnectorConfig.setLdapConfigurationTester(ldapConfigTester);
        String result = updateConnectorConfig.doTestSearch(ldapConfig, LDAPConfigurationTester.Strategy.GROUP);

        assertEquals(Action.SUCCESS, result);
        String actionMessage = (String) Iterables.getOnlyElement(updateConnectorConfig.getActionErrors());
        assertThat(actionMessage, equalTo(updateConnectorConfig.getText("directoryconnector.testsearch.invalid")));
    }

    @Test
    public void doingTestSearchShouldReturnSuccessWhenLdapConfigTesterThrowsException() throws OperationFailedException
    {
        LDAPConfiguration ldapConfig = mock(LDAPConfiguration.class);
        LDAPConfigurationTester ldapConfigTester = mock(LDAPConfigurationTester.class);
        when(ldapConfigTester.canFindLdapObjects(ldapConfig, LDAPConfigurationTester.Strategy.GROUP))
                .thenThrow(new OperationFailedException("failure checking config"));

        UpdateConnectorConfiguration updateConnectorConfig = new UpdateConnectorConfiguration();
        updateConnectorConfig.setLdapConfigurationTester(ldapConfigTester);
        String result = updateConnectorConfig.doTestSearch(ldapConfig, LDAPConfigurationTester.Strategy.GROUP);

        assertEquals(Action.SUCCESS, result);
        String actionError = (String) Iterables.getOnlyElement(updateConnectorConfig.getActionErrors());
        assertThat(actionError, containsString("failure checking config"));
    }
}
