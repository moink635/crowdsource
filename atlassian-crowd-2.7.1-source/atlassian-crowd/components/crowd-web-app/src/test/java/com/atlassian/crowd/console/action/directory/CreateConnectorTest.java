package com.atlassian.crowd.console.action.directory;

import java.util.ArrayList;

import com.atlassian.crowd.directory.ApacheDS15;
import com.atlassian.crowd.directory.GenericLDAP;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.directory.DirectoryImpl;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.opensymphony.xwork.Action;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CreateConnectorTest
{
    @Test
    public void emptyDnsAreValid()
    {
        assertTrue(CreateConnector.validDn(null));
        assertTrue(CreateConnector.validDn(""));
    }

    @Test
    public void dnWithoutEqualsIsNotValid()
    {
        assertFalse(CreateConnector.validDn("invalid"));
    }

    @Test
    public void dnIsValidAsDn()
    {
        assertTrue(CreateConnector.validDn("dc=example,dc=com"));
    }

    @Test
    public void genericLdapDoesNotRequireABaseDn()
    {
        assertFalse(CreateConnector.missingRequiredDn(GenericLDAP.class.getName(), ""));
    }

    @Test
    public void apacheDsRequiresABaseDn()
    {
        assertTrue(CreateConnector.missingRequiredDn(ApacheDS15.class.getName(), ""));
        assertFalse(CreateConnector.missingRequiredDn(ApacheDS15.class.getName(), "not-blank"));
    }

    @Test
    public void shouldNotRequireAnExternalId()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.doUpdate();

        Iterable<String> fieldErrors = (Iterable<String>) createConnector.getFieldErrors().get("userExternalIdAttr");
        assertNull("An empty external Id should be valid", fieldErrors);
    }

    @Test
    public void shouldNotRequireAPassword()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.doUpdate();

        ArrayList<String> ldapPasswordFieldErrors = (ArrayList<String>) createConnector.getFieldErrors().get("ldapPassword");
        assertNull("An empty password should be valid", ldapPasswordFieldErrors);
    }

    @Test
    public void shouldNotShowAnyFieldErrorsForLdapPasswordIfPresent()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setLdapPassword("ldapPassword");
        createConnector.doUpdate();

        ArrayList<String> ldapPasswordFieldErrors = (ArrayList<String>) createConnector.getFieldErrors().get("ldapPassword");
        assertNull("Any password should be valid", ldapPasswordFieldErrors);
    }

    @Test
    public void shouldNotRequireAUserDN()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.doUpdate();

        ArrayList<String> userDnFieldErrors = (ArrayList<String>) createConnector.getFieldErrors().get("userDN");
        assertNull("An empty User DN should be valid", userDnFieldErrors);
    }

    @Test
    public void shouldNotShowAnyFieldErrorsForUserDNIfPresent()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setUserDN("awesome");
        createConnector.doUpdate();

        ArrayList<String> userDnFieldErrors = (ArrayList<String>) createConnector.getFieldErrors().get("userDN");
        assertNull("Any User DN should be valid", userDnFieldErrors);
    }

    @Test
    public void shouldUpdateTheLdapPasswordIfEmptyAndAlreadyPresentInSavedLdapPassword()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setSavedLdapPassword("saved_password");
        createConnector.setLdapPassword(null);
        createConnector.doUpdate();

        assertThat(createConnector.getLdapPassword(), is("saved_password"));
    }

    @Test
    public void shouldResetTheSavedPasswordWithTheNewLdapPasswordIfPresent()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setSavedLdapPassword("saved_password");
        createConnector.setLdapPassword("new_password");
        createConnector.doUpdate();

        assertEquals("new_password", createConnector.getLdapPassword());
    }

    @Test
    public void shouldSetTheInitialLoadFlag()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.doDefault();

        assertTrue("The first request is an initial load", createConnector.isInitialLoad());
    }

    @Test
    public void shouldClearTheInitialLoadFlagAfterUpdate()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.doUpdate();

        assertFalse("After an update, it is no longer an initial load", createConnector.isInitialLoad());
    }

    @Test
    public void shouldClearTheInitialLoadFlagAfterConnectionTest()
    {
        CreateConnector createConnector = new CreateConnector();
        String result = createConnector.doTestConfiguration();

        assertFalse("After a connection test, it is no longer an initial load", createConnector.isInitialLoad());
        assertEquals(Action.SUCCESS, result);
    }

    @Test
    public void shouldHaveLocalStatusDisabledByDefaultForNewConnectors()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.doDefault();
        assertFalse("New connectors should have local status disabled by default",
                    createConnector.isLocalUserStatusEnabled());
    }

    @Test
    public void shouldNotCreateUncachedConnectorWithLocalUserStatusEnabled()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setName("Directory name");
        createConnector.setConnector("some.Class");

        createConnector.setCacheEnabled(false);
        createConnector.setLocalUserStatusEnabled(true);
        assertEquals(CreateConnector.INPUT, createConnector.doUpdate());

        assertEquals(ImmutableList.of(createConnector.getText("directoryconnector.localuserstatus.withoutcache.message")),
                     createConnector.getFieldErrors().get("localUserStatusEnabled"));
    }

    @Test
    public void shouldBuildDirectoryWithLocalGroupsAttributeEnabledWhenEnabledInCreateAction()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setName("Directory name");
        createConnector.setConnector("some.Class");

        createConnector.setLocalGroupsEnabled(true);
        DirectoryImpl directory = createConnector.buildDirectoryConfiguration();

        assertEquals("true", directory.getAttributes().get(LDAPPropertiesMapper.LOCAL_GROUPS));
    }

    @Test
    public void shouldBuildDirectoryWithLocalGroupsAttributeDisabledWhenDisabledInCreateAction()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setName("Directory name");
        createConnector.setConnector("some.Class");

        createConnector.setLocalGroupsEnabled(false);
        DirectoryImpl directory = createConnector.buildDirectoryConfiguration();

        String attributeValue = directory.getAttributes().get(LDAPPropertiesMapper.LOCAL_GROUPS);
        assertNotNull(attributeValue);
        assertFalse(Boolean.valueOf(attributeValue));
    }

    @Test
    public void shouldBuildDirectoryWithLocalUserStatusAttributeEnabledWhenEnabledInCreateAction()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setName("Directory name");
        createConnector.setConnector("some.Class");

        createConnector.setLocalUserStatusEnabled(true);
        DirectoryImpl directory = createConnector.buildDirectoryConfiguration();

        assertEquals("true", directory.getAttributes().get(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS));
    }

    @Test
    public void shouldBuildDirectoryWithLocalUserStatusAttributeDisabledWhenDisabledInCreateAction()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setName("Directory name");
        createConnector.setConnector("some.Class");

        createConnector.setLocalUserStatusEnabled(false);
        DirectoryImpl directory = createConnector.buildDirectoryConfiguration();

        String attributeValue = directory.getAttributes().get(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS);
        assertNotNull(attributeValue);
        assertFalse(Boolean.valueOf(attributeValue));
    }

    @Test
    public void shouldNotCreateUncachedConnectorWithLocalGroupsEnabled()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setName("Directory name");
        createConnector.setConnector("some.Class");

        createConnector.setCacheEnabled(false);
        createConnector.setLocalGroupsEnabled(true);
        assertEquals(CreateConnector.INPUT, createConnector.doUpdate());

        assertEquals(ImmutableList.of(createConnector.getText("directoryconnector.localgroups.withoutcache.message")),
                     createConnector.getFieldErrors().get("localGroupsEnabled"));
    }

    @Test
    public void shouldBuildDirectoryWithPrimaryGroupSupportEnabledWhenEnabledInCreateAction()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setName("Directory name");
        createConnector.setConnector("some.Class");

        createConnector.setPrimaryGroupSupport(true);
        DirectoryImpl directory = createConnector.buildDirectoryConfiguration();

        String attributeValue = directory.getAttributes().get(LDAPPropertiesMapper.PRIMARY_GROUP_SUPPORT);
        assertNotNull(attributeValue);
        assertTrue(Boolean.valueOf(attributeValue));
    }

    @Test
    public void shouldBuildDirectoryWithExternalId()
    {
        CreateConnector createConnector = new CreateConnector();
        createConnector.setName("Directory name");
        createConnector.setConnector("some.Class");

        createConnector.setUserExternalIdAttr("my-external-id-attr");
        DirectoryImpl directory = createConnector.buildDirectoryConfiguration();

        String attributeValue = directory.getAttributes().get(LDAPPropertiesMapper.LDAP_EXTERNAL_ID);
        assertEquals("my-external-id-attr", attributeValue);
    }

    @Test
    public void doingTestSearchShouldReturnSuccessWhenLdapConfigTesterSucceeds() throws OperationFailedException
    {
        LDAPConfiguration ldapConfig = mock(LDAPConfiguration.class);
        LDAPConfigurationTester ldapConfigTester = mock(LDAPConfigurationTester.class);
        when(ldapConfigTester.canFindLdapObjects(ldapConfig, LDAPConfigurationTester.Strategy.GROUP)).thenReturn(true);

        CreateConnector createConnector = new CreateConnector();
        createConnector.setLdapConfigurationTester(ldapConfigTester);
        String result = createConnector.doTestSearch(ldapConfig, LDAPConfigurationTester.Strategy.GROUP);

        assertEquals(Action.SUCCESS, result);
        String actionMessage = (String) Iterables.getOnlyElement(createConnector.getActionMessages());
        assertThat(actionMessage, equalTo(createConnector.getText("directoryconnector.testsearch.success")));
    }

    @Test
    public void doingTestSearchShouldReturnSuccessWhenLdapConfigTesterFails() throws OperationFailedException
    {
        LDAPConfiguration ldapConfig = mock(LDAPConfiguration.class);
        LDAPConfigurationTester ldapConfigTester = mock(LDAPConfigurationTester.class);
        when(ldapConfigTester.canFindLdapObjects(ldapConfig, LDAPConfigurationTester.Strategy.GROUP)).thenReturn(false);

        CreateConnector createConnector = new CreateConnector();
        createConnector.setLdapConfigurationTester(ldapConfigTester);
        String result = createConnector.doTestSearch(ldapConfig, LDAPConfigurationTester.Strategy.GROUP);

        assertEquals(Action.SUCCESS, result);
        String actionMessage = (String) Iterables.getOnlyElement(createConnector.getActionErrors());
        assertThat(actionMessage, equalTo(createConnector.getText("directoryconnector.testsearch.invalid")));
    }

    @Test
    public void doingTestSearchShouldReturnSuccessWhenLdapConfigTesterThrowsException() throws OperationFailedException
    {
        LDAPConfiguration ldapConfig = mock(LDAPConfiguration.class);
        LDAPConfigurationTester ldapConfigTester = mock(LDAPConfigurationTester.class);
        when(ldapConfigTester.canFindLdapObjects(ldapConfig, LDAPConfigurationTester.Strategy.GROUP))
                .thenThrow(new OperationFailedException("failure checking config"));

        CreateConnector createConnector = new CreateConnector();
        createConnector.setLdapConfigurationTester(ldapConfigTester);
        String result = createConnector.doTestSearch(ldapConfig, LDAPConfigurationTester.Strategy.GROUP);

        assertEquals(Action.SUCCESS, result);
        String actionError = (String) Iterables.getOnlyElement(createConnector.getActionErrors());
        assertThat(actionError, containsString("failure checking config"));
    }
}
