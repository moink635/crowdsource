package com.atlassian.crowd.console.action.application;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AddApplicationConnectionDetailsTest
{
    private Map<Object, Object> session;
    private AddApplicationConnectionDetails details;

    @Before
    public void setUp() throws UnknownHostException
    {
        setupWithLocalAddress(ImmutableSet.of(
                InetAddress.getByName("10.0.0.1"),
                InetAddress.getByName("127.0.0.1")));
    }

    public void setupWithLocalAddress(final Set<InetAddress> addrs) throws UnknownHostException
    {
        InetAddress[] localhostAddresses = {
                InetAddress.getByName("127.0.0.1"),
                Inet6Address.getByName("::1")
        };

        setupWithLocalAddresses(addrs, localhostAddresses);
    }

    /**
     * @param addrs all addresses considered local
     * @param localhost what <code>localhost</code> should resolve to
     */
    public void setupWithLocalAddresses(final Set<InetAddress> addrs, final InetAddress[] localhost)
    {
        /* Wrapper for getAllByName with dual stack behaviour for localhost */
        details = new AddApplicationConnectionDetails()
        {
            @Override
            InetAddress[] getAllByName(String host) throws UnknownHostException
            {
                if (host.equals("localhost"))
                {
                    return localhost;
                }
                else
                {
                    return InetAddress.getAllByName(host);
                }
            }

            @Override
            Set<InetAddress> getLocalAddresses()
            {
                return addrs;
            }
        };

        session = new HashMap<Object, Object>();
        session.put(AddApplicationDetails.APPLICATION_SESSION_CONFIGURATION_SETUP,
                new ApplicationConfiguration());
        details.setSession(session);

        // Needed to pass validation stage
        details.setApplicationURL("http:");
    }

    @Test
    public void resolveIPAddressWithIPv4LiteralWithoutPort()
    {
        details.setApplicationURL("http://127.0.0.2/");
        details.resolveIPAddress();
        assertEquals("127.0.0.2", details.getRemoteIPAddress());
    }

    @Test
    public void resolveIPAddressWithIPv4LiteralWithPort()
    {
        details.setApplicationURL("http://127.0.0.2:8080/");
        details.resolveIPAddress();
        assertEquals("127.0.0.2", details.getRemoteIPAddress());
    }

    @Test
    public void resolveIPAddressWithIPv6LiteralWithoutPort()
    {
        details.setApplicationURL("http://[::1]/");
        details.resolveIPAddress();
        assertEquals("0:0:0:0:0:0:0:1", details.getRemoteIPAddress());
    }

    @Test
    public void resolveIPAddressWithIPv6LiteralWithPort()
    {
        details.setApplicationURL("http://[::1]:8080/");
        details.resolveIPAddress();
        assertEquals("0:0:0:0:0:0:0:1", details.getRemoteIPAddress());
    }

    @Test
    public void badIpv4AddressResultsInError()
    {
        details.setApplicationURL("http://127.0.0.256/");
        details.resolveIPAddress();
        assertEquals("failed to lookup ip address", details.getRemoteIPAddress());
    }

    @Test
    public void badUrlResultsInError()
    {
        details.setApplicationURL(" ");
        details.resolveIPAddress();
        assertEquals("failed to lookup ip address", details.getRemoteIPAddress());
    }

    @Test
    public void nonHierarchicalUrlResultsInError()
    {
        details.setApplicationURL("mailto:user@example.com");
        details.resolveIPAddress();
        assertEquals("failed to lookup ip address", details.getRemoteIPAddress());
    }

    @Test
    public void resolveIPAddressForLocalhostGivesIpv4AndV6Addresses() throws UnknownHostException
    {
        details.setApplicationURL("http://localhost/");
        details.resolveIPAddress();
        assertEquals("127.0.0.1, 0:0:0:0:0:0:0:1", details.getRemoteIPAddress());
    }

    @Test
    public void completeStepUnderstandsCommaSeparatedLists() throws Exception
    {
        details.setRemoteIPAddress(" 127.0.0.1, 127.0.0.2");

        String s = details.completeStep();
        assertEquals("directorydetails", s);

        List<String> expected = Arrays.asList(
                "0:0:0:0:0:0:0:1",
                "127.0.0.1",
                "127.0.0.2"
        );

        assertEquals(expected,
                details.getConfiguration().getRemoteAddresses());
    }

    @Test
    public void completeStepRecognisesLoopbackAddressWhenLocalAddressIsLoopback() throws Exception
    {
        setupWithLocalAddress(Collections.singleton(InetAddress.getByName("127.0.0.1")));

        details.setRemoteIPAddress("127.0.0.1");

        String s = details.completeStep();
        assertEquals("directorydetails", s);

        List<String> expected = Arrays.asList(
                "0:0:0:0:0:0:0:1",
                "127.0.0.1"
        );

        assertEquals(expected,
                details.getConfiguration().getRemoteAddresses());
    }

    @Test
    public void completeStepRecognisesLocalMachineAddress() throws Exception
    {
        details.setRemoteIPAddress("10.0.0.1");

        String s = details.completeStep();
        assertEquals("directorydetails", s);

        List<String> expected = Arrays.asList(
                "0:0:0:0:0:0:0:1",
                "10.0.0.1",
                "127.0.0.1"
        );

        assertEquals(expected,
                details.getConfiguration().getRemoteAddresses());
    }

    @Test
    public void addingLocalMachineIncludesIpv6Localhost() throws UnknownHostException
    {
        details.setRemoteIPAddress("10.0.0.1");

        String s = details.completeStep();
        assertEquals("directorydetails", s);

        Collection<String> remoteAddrs = details.getConfiguration().getRemoteAddresses();

        assertTrue(remoteAddrs.contains("127.0.0.1"));
        assertTrue(remoteAddrs.contains("0:0:0:0:0:0:0:1"));
    }

    @Test
    public void gettingLocalAddressesGivesNonEmptySet()
    {
        Set<InetAddress> localAddrs = new AddApplicationConnectionDetails().getLocalAddresses();

        assertFalse("Expect at least one address on a real machine", localAddrs.isEmpty());
    }

    @Test
    public void scopeIsRemovedWhenResolvingApplicationUrlToAddresses() throws Exception
    {
        /* On MacOS X, resolving localhost includes an IPv6 address with a scope identifier */
        InetAddress[] localhostAddresses = {
                InetAddress.getByName("127.0.0.1"),
                Inet6Address.getByName("::1%1")
        };

        setupWithLocalAddresses(Collections.<InetAddress> emptySet(), localhostAddresses);

        details.setApplicationURL("http://localhost/");

        assertEquals("input", details.resolveIPAddress());

        assertEquals("127.0.0.1, 0:0:0:0:0:0:0:1", details.getRemoteIPAddress());
    }
}
