package com.atlassian.crowd.console.action.setup;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.config.setup.SetupPersister;
import com.atlassian.crowd.console.action.ActionHelper;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.extras.api.Organisation;
import com.atlassian.extras.api.crowd.CrowdLicense;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OptionsTest
{
    @InjectMocks
    private Options options = new Options();

    @Mock
    private CrowdBootstrapManager bootstrapManager;

    @Mock
    private SetupPersister setupPersister;

    @Mock
    private ActionHelper actionHelper;

    @Test
    public void urlWithUnknownHostCausesValidationToFail()
    {
        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getScheme()).thenReturn("http");
        when(req.getServerName()).thenReturn("localhost");
        when(req.getServerPort()).thenReturn(8095);
        when(req.getContextPath()).thenReturn("/");
        
        options.setServletRequest(req);
        options.setBaseURL("http://example.invalid/");
        
        options.doValidation();

        List<?> errors = (List<?>) options.getFieldErrors().get("baseURL");
        
        assertNotNull(errors);
        assertEquals(1, errors.size());
        
        assertEquals(
                "Error while trying to retrieve the supplied base URL: \"example.invalid\". Try http://localhost:8095/ instead.",
                errors.get(0));
    }

    @Test
    public void shouldGenerateDefaultDeploymentName()
    {
        when(actionHelper.getBootstrapManager()).thenReturn(bootstrapManager);
        when(bootstrapManager.isApplicationHomeValid()).thenReturn(true);

        when(setupPersister.getCurrentStep()).thenReturn(Options.OPTIONS_STEP);

        CrowdLicense license = mock(CrowdLicense.class);
        when(actionHelper.getLicense()).thenReturn(license);

        Organisation organisation = mock(Organisation.class);
        when(license.getOrganisation()).thenReturn(organisation);

        when(organisation.getName()).thenReturn("ACME");

        options.setBaseURL("http://example.test/crowd");

        options.doDefault();

        assertEquals("ACME Crowd server", options.getTitle());
    }
}
