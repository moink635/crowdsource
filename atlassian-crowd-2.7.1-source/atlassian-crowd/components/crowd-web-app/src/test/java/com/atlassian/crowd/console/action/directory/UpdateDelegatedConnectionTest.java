package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.directory.OpenLDAP;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.validator.ConnectorValidator;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;

import com.google.common.collect.Iterables;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyMapOf;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpdateDelegatedConnectionTest
{
    private static final int DIRECTORY_ID = 10;

    private UpdateDelegatedConnection updateDelegatedConnection;

    private DirectoryImpl directory;
    @Mock DirectoryManager directoryManager;
    @Mock ConnectorValidator connectorValidator;
    @Mock DirectoryInstanceLoader directoryInstanceLoader;
    @Mock RemoteDirectory remoteDirectory;

    @Before
    public void createAndWireObjectUnderTest()
    {
        updateDelegatedConnection = new UpdateDelegatedConnection();
        updateDelegatedConnection.setDirectoryManager(directoryManager);
        updateDelegatedConnection.setDirectoryInstanceLoader(directoryInstanceLoader);
        updateDelegatedConnection.setConnectorValidator(connectorValidator);
    }

    @Before
    public void createAuxiliaryObjects()
    {
        directory = new DirectoryImpl("My Directory", DirectoryType.DELEGATING, OpenLDAP.class.toString());
    }

    @Test
    public void shouldInformAboutSuccessfulConfigurationTest() throws Exception
    {
        updateDelegatedConnection.setID(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getRawDirectory(anyLong(), anyString(), anyMapOf(String.class, String.class)))
            .thenReturn(remoteDirectory);

        assertEquals(UpdateDelegatedConnection.SUCCESS, updateDelegatedConnection.doTestUpdateConfiguration());

        assertEquals(updateDelegatedConnection.getText("directoryconnector.testconnection.success"),
                     Iterables.getOnlyElement(updateDelegatedConnection.getActionMessages()));

        verify(remoteDirectory).testConnection();
    }

    @Test
    public void shouldInformAboutFailedConfigurationTest() throws Exception
    {
        updateDelegatedConnection.setID(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getRawDirectory(anyLong(), anyString(), anyMapOf(String.class, String.class)))
            .thenReturn(remoteDirectory);

        doThrow(new OperationFailedException("some error")).when(remoteDirectory).testConnection();

        assertEquals(UpdateDelegatedConnection.ERROR, updateDelegatedConnection.doTestUpdateConfiguration());

        assertEquals(updateDelegatedConnection.getText("directoryconnector.testconnection.invalid") + " some error",
                     Iterables.getOnlyElement(updateDelegatedConnection.getActionErrors()));

        verify(remoteDirectory).testConnection();
    }

    @Test
    public void shouldRetainNewLdapPasswordOnSuccessfulTestConfiguration() throws Exception
    {
        updateDelegatedConnection.setID(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getRawDirectory(anyLong(), anyString(), anyMapOf(String.class, String.class)))
            .thenReturn(remoteDirectory);

        updateDelegatedConnection.setUserDN("userDn");
        updateDelegatedConnection.setSavedLdapPassword("previous password");
        updateDelegatedConnection.setLdapPassword("new password");

        assertEquals(UpdateDelegatedConnection.SUCCESS, updateDelegatedConnection.doTestUpdateConfiguration());

        assertEquals(updateDelegatedConnection.getLdapPassword(), "new password");
    }

    @Test
    public void shouldRetainNewLdapPasswordOnFailedTestConfiguration() throws Exception
    {
        updateDelegatedConnection.setID(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        when(directoryInstanceLoader.getRawDirectory(anyLong(), anyString(), anyMapOf(String.class, String.class)))
            .thenReturn(remoteDirectory);

        doThrow(new OperationFailedException("some error")).when(remoteDirectory).testConnection();

        updateDelegatedConnection.setUserDN("userDn");
        updateDelegatedConnection.setSavedLdapPassword("previous password");
        updateDelegatedConnection.setLdapPassword("new password");

        assertEquals(UpdateDelegatedConnection.ERROR, updateDelegatedConnection.doTestUpdateConfiguration());

        assertEquals(updateDelegatedConnection.getLdapPassword(), "new password");

        verify(remoteDirectory).testConnection();
    }

    @Test
    public void shouldLoadNestedGroupsEnabledConfiguration() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, "false");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateDelegatedConnection.setID(DIRECTORY_ID);
        updateDelegatedConnection.execute();

        assertTrue("Nested groups support should be enabled", updateDelegatedConnection.isUseNestedGroups());
    }

    @Test
    public void shouldLoadNestedGroupsDisabledConfiguration() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, "true");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateDelegatedConnection.setID(DIRECTORY_ID);
        updateDelegatedConnection.execute();

        assertFalse("Nested groups support should be disabled", updateDelegatedConnection.isUseNestedGroups());
    }

    @Test
    public void shouldEnableNestedGroups() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, "true");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateDelegatedConnection.setID(DIRECTORY_ID);
        updateDelegatedConnection.setUseNestedGroups(true);
        updateDelegatedConnection.updateDirectory(directory);

        assertFalse("Nested groups support should be enabled",
                Boolean.valueOf(directory.getAttributes().get(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED)));
    }

    @Test
    public void shouldDisableNestedGroups() throws Exception
    {
        directory.setAttribute(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, "false");

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        updateDelegatedConnection.setID(DIRECTORY_ID);
        updateDelegatedConnection.setUseNestedGroups(false);
        updateDelegatedConnection.updateDirectory(directory);

        assertTrue("Nested groups support should be disabled",
                Boolean.valueOf(directory.getAttributes().get(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED)));
    }
}
