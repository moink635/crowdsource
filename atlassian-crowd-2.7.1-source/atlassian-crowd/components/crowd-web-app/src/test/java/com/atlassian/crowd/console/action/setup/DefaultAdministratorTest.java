package com.atlassian.crowd.console.action.setup;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.atlassian.config.setup.SetupPersister;
import com.atlassian.crowd.console.action.ActionHelper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ResourceLocator;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.service.soap.client.SoapClientProperties;
import com.atlassian.crowd.util.PasswordHelper;
import com.atlassian.crowd.util.PropertyUtils;
import com.atlassian.extras.api.Contact;
import com.atlassian.extras.api.crowd.CrowdLicense;

import com.google.common.collect.ImmutableList;
import com.opensymphony.webwork.ServletActionContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DefaultAdministratorTest
{
    private static final long DIRECTORY_ID = 1L;

    @InjectMocks
    private DefaultAdministrator defaultAdministrator = new DefaultAdministrator();

    @Mock private SetupPersister setupPersister;
    @Mock private DirectoryManager directoryManager;
    @Mock private Directory directory;
    @Mock private PasswordHelper passwordHelper;
    @Mock private ClientProperties clientProperties;
    @Mock private ApplicationManager applicationManager;
    @Mock private PropertyUtils propertyUtils;
    @Mock private ResourceLocator resourceLocator;
    @Mock private SecurityServerClient securityServerClient;
    @Mock private SoapClientProperties soapClientProperties;
    @Mock private HttpSession session;
    @Mock private ActionHelper actionHelper;

    @Before
    public void initStatics()
    {
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);

        ServletActionContext.setRequest(request);
    }

    @After
    public void releaseStatics()
    {
        ServletActionContext.setRequest(null);
    }

    @Before
    public void setUpMocks() throws Exception
    {
        when(setupPersister.getCurrentStep()).thenReturn(DefaultAdministrator.DEFAULT_ADMIN_STEP);

        CrowdBootstrapManager bootstrapManager = mock(CrowdBootstrapManager.class);
        when(bootstrapManager.isApplicationHomeValid()).thenReturn(true);
        when(actionHelper.getBootstrapManager()).thenReturn(bootstrapManager);

        when(directory.getId()).thenReturn(DIRECTORY_ID);

        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);
        when(directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class,
                                                                      EntityDescriptor.directory())
                                                    .returningAtMost(EntityQuery.ALL_RESULTS)))
            .thenReturn(ImmutableList.of(directory));

        when(clientProperties.getBaseURL()).thenReturn("http://localhost:1234/base"); // hostname must resolve

        when(resourceLocator.getResourceLocation()).thenReturn("resource-location");
        when(resourceLocator.getProperties()).thenReturn(new Properties());

        when(securityServerClient.getSoapClientProperties()).thenReturn(soapClientProperties);
    }

    @Test
    public void justHostnameLeavesHostnamesUnaffected()
    {
        assertEquals("hostname", DefaultAdministrator.justHostname("hostname"));
    }

    @Test
    public void justHostnameStripsMessages()
    {
        assertEquals("The message that InetAddress.getLocalHost() would add is removed",
                "hostname", DefaultAdministrator.justHostname("hostname: is the hostname"));
    }

    @Test
    public void shouldSetDefaultValuesFromLicense() throws Exception
    {
        Contact contact = mock(Contact.class);
        when(contact.getEmail()).thenReturn("john@acme.com");
        when(contact.getName()).thenReturn("John Doe");

        CrowdLicense license = mock(CrowdLicense.class);
        when(license.getContacts()).thenReturn(ImmutableList.of(contact));
        when(actionHelper.getLicense()).thenReturn(license);

        assertEquals(DefaultAdministrator.INPUT, defaultAdministrator.doDefault());

        assertEquals("john@acme.com", defaultAdministrator.getEmail());
        assertEquals("John", defaultAdministrator.getFirstname());
        assertEquals("Doe", defaultAdministrator.getLastname());
        assertEquals("john", defaultAdministrator.getName());
    }

    @Test
    public void shouldNotSetDefaultValuesFromLicenseWhenNameCannotBeParsed() throws Exception
    {
        Contact contact = mock(Contact.class);
        when(contact.getName()).thenReturn("unparseablename");

        CrowdLicense license = mock(CrowdLicense.class);
        when(license.getContacts()).thenReturn(ImmutableList.of(contact));
        when(actionHelper.getLicense()).thenReturn(license);

        assertEquals(DefaultAdministrator.INPUT, defaultAdministrator.doDefault());

        assertNull(defaultAdministrator.getEmail());
        assertNull(defaultAdministrator.getFirstname());
        assertNull(defaultAdministrator.getLastname());
        assertNull(defaultAdministrator.getName());
    }

    @Test
    public void shouldCreateAdminUser() throws Exception
    {
        defaultAdministrator.setName("admin");
        defaultAdministrator.setEmail("admin@example.test");
        defaultAdministrator.setPassword("password");
        defaultAdministrator.setPasswordConfirm("password");
        defaultAdministrator.setFirstname("A.D.");
        defaultAdministrator.setLastname("Ministrator");

        User adminUser = new UserTemplate("admin");
        when(directoryManager.addUser(eq(DIRECTORY_ID), any(UserTemplate.class),
                                      eq(PasswordCredential.unencrypted("password"))))
            .thenReturn(adminUser);

        final String adminGroupName = defaultAdministrator.getText("default.admin.group");
        Group adminGroup = new GroupTemplate(adminGroupName);
        when(directoryManager.addGroup(eq(DIRECTORY_ID), any(GroupTemplate.class)))
            .thenReturn(adminGroup);

        ApplicationImpl application = ApplicationImpl.newInstanceWithPassword(toLowerCase(defaultAdministrator.getText(
            "application.name")), ApplicationType.CROWD, "random-password");
        application.setDescription(defaultAdministrator.getText("application.description"));
        when(applicationManager.add(any(Application.class))).thenReturn(application);
        assertEquals(DefaultAdministrator.SELECT_SETUP_STEP, defaultAdministrator.doUpdate());

        // admin user is created
        ArgumentCaptor<UserTemplate> userTemplate = ArgumentCaptor.forClass(UserTemplate.class);
        verify(directoryManager).addUser(eq(DIRECTORY_ID), userTemplate.capture(),
                                         eq(PasswordCredential.unencrypted("password")));
        assertEquals("admin", userTemplate.getValue().getName());
        assertEquals("admin@example.test", userTemplate.getValue().getEmailAddress());
        assertEquals("A.D.", userTemplate.getValue().getFirstName());
        assertEquals("Ministrator", userTemplate.getValue().getLastName());

        // admin group is created
        ArgumentCaptor<GroupTemplate> groupTemplate = ArgumentCaptor.forClass(GroupTemplate.class);
        verify(directoryManager).addGroup(eq(DIRECTORY_ID), groupTemplate.capture());

        assertEquals(adminGroupName, groupTemplate.getValue().getName());

        // user is added to admin group
        verify(directoryManager).addUserToGroup(DIRECTORY_ID, "admin",
                                                adminGroupName);

        // directory is mapped to the Crowd app
        verify(applicationManager).addDirectoryMapping(application, directory, false, OperationType.values());

        // admin group is mapped
        applicationManager.addGroupMapping(application, directory, adminGroupName);

        verify(soapClientProperties).updateProperties(new Properties());

        verify(session).setAttribute(DefaultAdministrator.DEFAULT_ADMIN_NAME_KEY, "admin");
    }

    @Test
    public void emptyUsernameIsInvalid()
    {
        assertEquals(DefaultAdministrator.INPUT, defaultAdministrator.doUpdate());

        assertEquals(ImmutableList.of(defaultAdministrator.getText("principal.name.invalid")),
                     defaultAdministrator.getFieldErrors().get("name"));
    }

    @Test
    public void usernameWithTrailingSpacesIsInvalid()
    {
        defaultAdministrator.setName(" username ");

        assertEquals(DefaultAdministrator.INPUT, defaultAdministrator.doUpdate());

        assertEquals(ImmutableList.of(defaultAdministrator.getText("invalid.whitespace")),
                     defaultAdministrator.getFieldErrors().get("name"));
    }

    @Test
    public void emptyEmailIsInvalid()
    {
        assertEquals(DefaultAdministrator.INPUT, defaultAdministrator.doUpdate());

        assertEquals(ImmutableList.of(defaultAdministrator.getText("principal.email.invalid")),
                     defaultAdministrator.getFieldErrors().get("email"));
    }

    @Test
    public void emailWithoutAtIsInvalid()
    {
        defaultAdministrator.setEmail("invalidemail");

        assertEquals(DefaultAdministrator.INPUT, defaultAdministrator.doUpdate());

        assertEquals(ImmutableList.of(defaultAdministrator.getText("principal.email.invalid")),
                     defaultAdministrator.getFieldErrors().get("email"));
    }

    @Test
    public void emailWithTrailingSpacesIsInvalid()
    {
        defaultAdministrator.setEmail(" john@example.test ");

        assertEquals(DefaultAdministrator.INPUT, defaultAdministrator.doUpdate());

        assertEquals(ImmutableList.of(defaultAdministrator.getText("principal.email.whitespace")),
                     defaultAdministrator.getFieldErrors().get("email"));
    }

    @Test
    public void emptyPasswordIsInvalid()
    {
        assertEquals(DefaultAdministrator.INPUT, defaultAdministrator.doUpdate());

        assertEquals(ImmutableList.of(defaultAdministrator.getText("principal.password.invalid")),
                     defaultAdministrator.getFieldErrors().get("password"));
    }

    @Test
    public void passwordsMustMatch()
    {
        defaultAdministrator.setPassword("password1");
        defaultAdministrator.setPasswordConfirm("password2");

        assertEquals(DefaultAdministrator.INPUT, defaultAdministrator.doUpdate());

        assertEquals(ImmutableList.of(defaultAdministrator.getText("principal.passwordconfirm.nomatch")),
                     defaultAdministrator.getFieldErrors().get("password"));
    }
}
