package com.atlassian.crowd.util;

import java.io.File;

import com.atlassian.config.DefaultHomeLocator;
import com.atlassian.config.HomeLocator;
import com.atlassian.crowd.service.client.ResourceLocator;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * ConsoleResourceLocator Tester.
 */
public class ConsoleResourceLocatorTest
{
    private HomeLocator homeLocator;
    private ResourceLocator locator;
    private File tmpFile;
    private static final String TEST_FILE_NAME = "testfile.properties";

    @Before
    public void setUp() throws Exception
    {
        homeLocator = mock(HomeLocator.class);

        tmpFile = new File(TEST_FILE_NAME);
        FileUtils.writeStringToFile(tmpFile, "test=value", "UTF-8");
        tmpFile.deleteOnExit();
    }

    @After
    public void tearDown() throws Exception
    {
        homeLocator = null;
    }

    @Test
    public void testFindPropertyLocationUsingHomeLocator()
    {
        when(homeLocator.getHomePath()).thenReturn(StringUtils.chomp(tmpFile.getAbsolutePath(), TEST_FILE_NAME));

        locator = new ConsoleResourceLocator(TEST_FILE_NAME, homeLocator);

        String location = locator.getResourceLocation();

        assertNotNull(location);
        assertTrue(location.contains("file:"));
        assertTrue(location.contains(tmpFile.getPath()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNullPropertyFileNameFails()
    {
        new ConsoleResourceLocator(null, new DefaultHomeLocator());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testConstructorWithNullHomeLocatorFails()
    {
        new ConsoleResourceLocator("crowd.properties", null);
    }
}
