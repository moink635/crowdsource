package com.atlassian.crowd.console.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.atlassian.crowd.directory.AbstractInternalDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.user.UserWithAttributes;

import com.google.common.collect.ImmutableList;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.Action;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChangeExpiredPasswordTest
{
    private static final String USER = "user";
    private static final long DIRECTORY_ID = 1L;

    @InjectMocks
    private ChangeExpiredPassword action = new ChangeExpiredPassword();

    @Mock private DirectoryManager directoryManager;
    @Mock private ApplicationService applicationService;
    @Mock private Application application;
    @Mock private UserWithAttributes user;
    @Mock private HttpSession session;

    @Before
    public void initExpiredSession() throws Exception
    {
        when(session.getAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY))
            .thenReturn(USER);
        when(session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION))
            .thenReturn(new CredentialsExpiredException("your credentials have expired"));

        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getSession()).thenReturn(session);

        ServletActionContext.setRequest(request);
    }

    @After
    public void releaseStatics()
    {
        ServletActionContext.setRequest(null);
    }

    @Test
    public void shouldShowThePasswordComplexityRequirementIfSet() throws Exception
    {
        Directory directory = createDirectory("Password complexity message", "[some regex]");
        when(applicationService.findUserByName(application, USER)).thenReturn(user);
        when(user.getDirectoryId()).thenReturn(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        assertEquals(ChangeExpiredPassword.INPUT, action.doDefault());
        assertEquals("Password complexity message", action.getPasswordComplexityMessage());
        assertEquals(ImmutableList.of(action.getText("error.changepassword.required")),
                     action.getActionErrors());
    }

    @Test
    public void shouldNotShowThePasswordComplexityRequirementIfNotSet() throws Exception
    {
        Directory directory = createDirectory("", "");
        when(applicationService.findUserByName(application, USER)).thenReturn(user);
        when(user.getDirectoryId()).thenReturn(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        assertEquals(ChangeExpiredPassword.INPUT, action.doDefault());
        assertNull(action.getPasswordComplexityMessage());
        assertEquals(ImmutableList.of(action.getText("error.changepassword.required")),
                     action.getActionErrors());
    }

    @Test
    public void shouldNotChangeThePasswordIfTheOldPasswordIsInvalid() throws Exception
    {
        Directory directory = createDirectory("", "");
        when(applicationService.findUserByName(application, USER)).thenReturn(user);
        when(user.getDirectoryId()).thenReturn(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        doThrow(new InvalidAuthenticationException("Bad password!"))
            .when(applicationService).authenticateUser(application, USER,
                                                       new PasswordCredential("badoriginalpassword"));

        action.setOriginalPassword("badoriginalpassword");
        action.setPassword("newpassword");
        action.setConfirmPassword("newpassword");

        assertEquals(Action.INPUT, action.doUpdate());
        assertEquals(ImmutableList.of("Password invalid."), action.getFieldErrors().get("originalPassword"));

        verify(applicationService).authenticateUser(application, USER, new PasswordCredential("badoriginalpassword"));
        verify(applicationService, never()).updateUserCredential(any(Application.class), anyString(),
                                                                 any(PasswordCredential.class));
    }

    @Test
    public void shouldNotChangeThePasswordIfNewPasswordDoesNotMeetRequirements() throws Exception
    {
        Directory directory = createDirectory("Password complexity message", "[some regex]");
        when(applicationService.findUserByName(application, USER)).thenReturn(user);
        when(user.getDirectoryId()).thenReturn(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        doThrow(new InvalidCredentialException("Generic message", "Password complexity message from exception"))
            .when(applicationService).updateUserCredential(application, USER, new PasswordCredential("newpassword"));

        action.setOriginalPassword("originalpassword");
        action.setPassword("newpassword");
        action.setConfirmPassword("newpassword");

        assertEquals(Action.INPUT, action.doUpdate());
        assertEquals(ImmutableList.of(action.getText("passwordupdate.policy.error.message")), action.getActionErrors());
        assertEquals(ImmutableList.of("Password complexity message from exception"),
                action.getFieldErrors().get("password"));

        verify(applicationService).authenticateUser(application, USER, new PasswordCredential("originalpassword"));
        verify(applicationService).updateUserCredential(application, USER, new PasswordCredential("newpassword"));
    }

    @Test
    public void shouldChangePassword() throws Exception
    {
        Directory directory = createDirectory("Password complexity message", "[some regex]");
        when(applicationService.findUserByName(application, USER)).thenReturn(user);
        when(user.getDirectoryId()).thenReturn(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        action.setOriginalPassword("originalpassword");
        action.setPassword("newpassword");
        action.setConfirmPassword("newpassword");

        assertEquals(Action.SUCCESS, action.doUpdate());
        assertEquals(ImmutableList.of("Password updated."), action.getActionMessages());

        verify(applicationService).authenticateUser(application, USER, new PasswordCredential("originalpassword"));
        verify(applicationService).updateUserCredential(application, USER, new PasswordCredential("newpassword"));
        verify(session).getAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY);
    }

    private static Directory createDirectory(String passwordComplexityMessage, String regex)
    {
        DirectoryImpl directory = new DirectoryImpl();
        directory.setAttribute(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_REGEX, regex);
        directory.setAttribute(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_COMPLEXITY_MESSAGE, passwordComplexityMessage);
        return directory;
    }

    @Test
    public void changedUsernameInSessionIsIgnoredAfterInitialCheck() throws Exception
    {
        when(session.getAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY)).
            thenReturn(USER, "different-user");

        Directory directory = createDirectory("Password complexity message", "[some regex]");
        when(applicationService.findUserByName(application, USER)).thenReturn(user);
        when(user.getDirectoryId()).thenReturn(DIRECTORY_ID);
        when(directoryManager.findDirectoryById(DIRECTORY_ID)).thenReturn(directory);

        action.setOriginalPassword("originalpassword");
        action.setPassword("newpassword");
        action.setConfirmPassword("newpassword");

        assertEquals(Action.SUCCESS, action.doUpdate());

        verify(applicationService).authenticateUser(application, USER, new PasswordCredential("originalpassword"));
        verify(applicationService).updateUserCredential(application, USER, new PasswordCredential("newpassword"));
        assertEquals(USER, action.getUsername());
        verify(session).getAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY);
    }
}
