<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
<head>
    <title>
        <ww:text name="menu.viewapplication.label"/>
    </title>
    <meta name="section" content="applications"/>
    <meta name="pagename" content="view"/>
    <meta name="help.url" content="<ww:text name="help.application.view.remoteaddr"/>"/>

    <script>

        function detectKeyDown(key)
        {
            // If the user presses ENTER (ascii 13)
            // IE uses 'keyCode', FF/Opera uses 'which'
            if (key.keyCode == 13 || key.which == 13)
            {
                addAddress();
                // Prevent the form from submitting twice
                return false;
            }
            return true;
        }

        function addAddress()
        {
            var form = document.addressForm;
            form.action = '<ww:url namespace="/console/secure/application" action="updateaddresses" method="addAddress" includeParams="none" />';
            form.submit();
        }

    </script>
</head>
<body>

<h2 id="application-name">
    <img class="application-icon" style="padding-bottom:3px;" title="<ww:property value="getImageTitle(application.active, application.type)"/>"
         alt="<ww:property value="getImageTitle(application.active, application.type)"/>" src="<ww:property value="getImageLocation(application.active, application.type)" />"/>
    <ww:property value="application.name"/>
</h2>

<div class="page-content">


    <ww:component template="application_tab_headers.jsp">
        <ww:param name="pagekey" value="'application-remoteaddress'"/>
    </ww:component>


    <div class="tabContent static" id="tab6">

        <div class="crowdForm">

            <form method="post"
              action="<ww:url namespace="/console/secure/application" action="updateaddresses" method="update" includeParams="none"/>"
              name="addressForm">

            <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

            <div class="formBodyNoTop">

                <ww:component template="form_tab_messages.jsp">
                    <ww:param name="tabID" value="5"/>
                </ww:component>

                <p>
                    <ww:property value="getText('application.addressmappings.text')"/>
                </p>

                <input type="hidden" name="ID" value="<ww:property value="ID" />"/>
                <input type="hidden" name="tab" value="5"/>

                <table id="addressesTable" class="formTable">
                    <tr>
                        <th width="82%">
                            <ww:property value="getText('browser.address.label')"/>
                        </th>
                        <th width="18%">
                            <ww:property value="getText('browser.action.label')"/>
                        </th>
                    </tr>

                    <ww:iterator value="application.remoteAddresses" status="rowstatus">

                        <ww:if test="#rowstatus.odd == true">
                            <tr class="odd">
                        </ww:if>
                        <ww:else>
                            <tr class="even">
                        </ww:else>

                        <td>
                            <ww:property value="address"/>
                        </td>

                        <td>
                            <a href="<ww:url namespace="/console/secure/application" action="updateaddresses" method="removeAddress" includeParams="none" ><ww:param name="ID" value="ID" /><ww:param name="address" value="address" /><ww:param name="tab" value="5" /><ww:param name="%{xsrfTokenName}" value="%{xsrfToken}" /></ww:url>"
                               title="<ww:property value="getText('remove.label')"/>">
                                <ww:property value="getText('remove.label')"/>
                            </a>
                        </td>

                        </tr>

                    </ww:iterator>

                </table>
            </div>

            <div class="formFooter wizardFooter">
                <div class="buttons">
                    <ww:text name="browser.address.label"/>:&nbsp;<input type="text" name="address" size="20" style="width: 200px;" value="" onkeydown="return detectKeyDown(event);"/>&nbsp;
                    <input id="add-address" type="button" class="button" value="<ww:property value="getText('add.label')"/> &raquo;" onClick="addAddress();"/>
                </div>
            </div>

        </form>

        </div>

    </div>

</div>
</body>
</html>
