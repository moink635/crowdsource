<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>

<html>
<head>
    <title>
        <ww:text name="menu.viewdirectory.label">
            <ww:param><ww:property value="directory.name"/></ww:param>
        </ww:text>
    </title>

    <meta name="section" content="directories"/>
    <meta name="pagename" content="view"/>
    <meta name="help.url" content="<ww:text name="help.directory.crowd.connection"/>"/>

    <script>

        function testConnection()
        {
            document.crowddetails.action = "<ww:url namespace="/console/secure/directory" action="testupdateremotecrowdconnection" method="testUpdateConnection" includeParams="none"/>";

            var tabNumberElement = document.createElement("input");
            tabNumberElement.setAttribute("type", "hidden");
            tabNumberElement.setAttribute("name", "tab");
            tabNumberElement.setAttribute("id", "tab");
            tabNumberElement.setAttribute("value", "2");

            document.crowddetails.appendChild(tabNumberElement);

            document.crowddetails.submit();
        }

    </script>

</head>
<body>
<h2>
    <ww:text name="menu.viewdirectory.label">
        <ww:param><ww:property value="directory.name"/></ww:param>
    </ww:text>
</h2>

<div class="page-content">

<ul class="tabs">

    <li>
        <a id="crowd-general"
           href="<ww:url action="viewremotecrowd" namespace="/console/secure/directory" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text
                name="menu.details.label"/></a>
    </li>

    <li class="on">
        <span class="tab"><ww:text name="menu.connection.label"/></span>
    </li>

    <li>
        <a id="crowd-permissions"
           href="<ww:url namespace="/console/secure/directory" action="updateremotecrowdpermissions" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text
                name="menu.permissions.label"/></a>
    </li>

    <li>
        <a id="crowd-options" href="<ww:url namespace="/console/secure/directory" action="updateremotecrowdoptions" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.optional.label"/></a>
    </li>

</ul>

<div class="tabContent static" id="tab1">

    <div class="crowdForm">
        <form id="crowddetails" name="crowddetails" method="post"
              action="<ww:url namespace="/console/secure/directory" action="updateremotecrowdconnection" method="update" includeParams="none" />">

            <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

            <div class="formBody">

                <ww:component template="form_messages.jsp"/>

                <input type="hidden" name="ID" value="<ww:property value="ID" />"/>

                <ww:textfield name="url" size="50">
                    <ww:param name="label" value="getText('directorycrowd.url.label')"/>
                    <ww:param name="description">
                        <ww:property value="getText('directorycrowd.url.description')"/>
                    </ww:param>
                    <ww:param name="required" value="true" />
                </ww:textfield>

                <ww:textfield name="applicationName" size="50">
                    <ww:param name="label" value="getText('directorycrowd.applicationname.label')"/>
                    <ww:param name="description">
                        <ww:property value="getText('directorycrowd.applicationname.description')"/>
                    </ww:param>
                    <ww:param name="required" value="true" />
                </ww:textfield>

                <ww:password name="applicationPassword" size="50">
                    <ww:param name="label" value="getText('directorycrowd.applicationpassword.label')"/>
                    <ww:param name="description">
                        <ww:property value="getText('directorycrowd.applicationpassword.description')"/>
                    </ww:param>
                    <ww:param name="required" value="true" />
                </ww:password>

                <ww:textfield name="httpTimeout">
                    <ww:param name="label" value="getText('directorycrowd.http.timeout.label')"/>
                    <ww:param name="description">
                        <ww:property value="getText('directorycrowd.http.timeout.description')"/>
                    </ww:param>
                </ww:textfield>

                <ww:textfield name="httpMaxConnections">
                    <ww:param name="label" value="getText('directorycrowd.http.maxconnections.label')"/>
                    <ww:param name="description">
                        <ww:property value="getText('directorycrowd.http.maxconnections.description')"/>
                    </ww:param>
                </ww:textfield>

                <ww:textfield name="httpProxyHost" size="50">
                    <ww:param name="label" value="getText('directorycrowd.proxy.host.label')"/>
                    <ww:param name="description">
                        <ww:property value="getText('directorycrowd.proxy.host.description')"/>
                    </ww:param>
                </ww:textfield>

                <ww:textfield name="httpProxyPort">
                    <ww:param name="label" value="getText('directorycrowd.proxy.port.label')"/>
                    <ww:param name="description">
                        <ww:property value="getText('directorycrowd.proxy.port.description')"/>
                    </ww:param>
                </ww:textfield>

                <ww:textfield name="httpProxyUsername">
                    <ww:param name="label" value="getText('directorycrowd.proxy.username.label')"/>
                    <ww:param name="description">
                        <ww:property value="getText('directorycrowd.proxy.username.description')"/>
                    </ww:param>
                </ww:textfield>

                <ww:password name="httpProxyPassword">
                    <ww:param name="label" value="getText('directorycrowd.proxy.password.label')"/>
                    <ww:param name="description">
                        <ww:property value="getText('directorycrowd.proxy.password.description')"/>
                    </ww:param>
                </ww:password>

                <ww:checkbox name="incrementalSyncEnabled" fieldValue="true">
                    <ww:param name="label" value="getText('directorycrowd.incrementalsync.enable.label')"/>
                    <ww:param name="description">
                        <ww:property value="getText('directorycrowd.incrementalsync.enable.description')"/>
                    </ww:param>
                </ww:checkbox>

                <div id="polling_interval">
                <ww:textfield name="pollingIntervalInMin">
                    <ww:param name="label" value="getText('directorycrowd.polling.interval.label')"/>
                    <ww:param name="description">
                        <ww:property value="getText('directorycrowd.polling.interval.description')"/>
                    </ww:param>
                </ww:textfield>
                </div>

                <div class="textFieldButton buttons" style="">
                    <input id="test-connection" type="button" class="button" style="width: 125px;" value="<ww:property value="getText('directorycrowd.testconnection.label')"/>" onClick="testConnection();"/>
                </div>

            </div>

            <div class="formFooter wizardFooter">
                <div class="buttons">
                    <input type="submit" class="button" value="<ww:property value="getText('update.label')"/> &raquo;"/>
                    <input type="button" class="button" value="<ww:property value="getText('cancel.label')"/>"
                           onClick="window.location='<ww:url namespace="/console/secure/directory" action="viewremotecrowd" method="default" includeParams="none" ><ww:param name="ID" value="ID" /></ww:url>';"/>
                </div>
            </div>

        </form>

    </div>

</div>

</div>
</body>
</html>
