<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
<head>
    <title>
        <ww:text name="menu.viewdirectory.label">
            <ww:param><ww:property value="directory.name"/></ww:param>
        </ww:text>
    </title>
    <meta name="section" content="directories"/>
    <meta name="pagename" content="view"/>
    <meta name="help.url" content="<ww:text name="help.directory.remotecrowd.options"/>"/>

    <ww:property value="webResourceManager.requireResource('com.atlassian.auiplugin:jquery')"/>
    <ww:property value="webResourceManager.requireResource('com.atlassian.auiplugin:ajs')"/>
    <ww:property value="webResourceManager.requiredResources" escape="false"/>

    <script>

        AJS.$(document).ready(function(){

            CROWD_PICKER.attachPicker("addGroups", "<ww:property value="getText('picker.addgroups.label')"/>",
                    "<ww:url namespace="/console/secure/pickers" action="searchPicker" method="getGroupsExcluding" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="ID"/></ww:url><ww:property value="exclusionsAsString" escape="false"/>",
                    "<ww:property value="getText('picker.addselected.groups.label')"/>",
                    "<ww:url namespace="/console/secure/directory" action="updateremotecrowdoptions" method="addDefaultGroups" includeParams="none" escapeAmp="false"><ww:param name="ID" value="ID"/></ww:url>",
                    "<ww:url namespace="/console/secure/pickers" action="displayPicker" includeParams="none" />",
                    "<ww:url namespace="/console/secure/pickers" action="checkLoginStatus" includeParams="none" />",
                    "<ww:property value="getText('picker.addgroups.generic.message')"/>",
                    "<ww:url namespace="/console/secure/directory" action="updateremotecrowdoptions" includeParams="none" escapeAmp="false"><ww:param name="ID" value="ID"/></ww:url>");
        });

    </script>
</head>
<body>
<h2>
    <ww:text name="menu.viewdirectory.label">
        <ww:param><ww:property value="directory.name"/></ww:param>
    </ww:text>
</h2>

<div class="page-content">

    <ul class="tabs">

        <li>
            <a id="crowd-general" href="<ww:url action="viewremotecrowd" namespace="/console/secure/directory" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.details.label"/></a>
        </li>

        <li>
            <a id="crowd-connectiondetails" href="<ww:url namespace="/console/secure/directory" action="updateremotecrowdconnection" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.connection.label"/></a>
        </li>

        <li>
            <a id="crowd-permissions" href="<ww:url namespace="/console/secure/directory" action="updateremotecrowdpermissions" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.permissions.label"/></a>
        </li>

        <li class="on">
            <span class="tab"><ww:text name="menu.optional.label"/></span>
        </li>

    </ul>

    <div class="tabContent static">

        <div class="crowdForm">

            <form name="updateGeneral" method="post"
                  action="<ww:url namespace="/console/secure/directory" action="updateremotecrowd" includeParams="none" />">

                <div class="formBody">

                    <ww:component template="form_messages.jsp"/>

                    <h3><ww:text name="directory.options.autogroupadd.title"/></h3>

                    <ww:if test="defaultGroups.empty">
                        <p><ww:text name="directory.options.autogroupadd.nogroups.text"/></p>
                    </ww:if>
                    <ww:else>
                        <p><ww:text name="directory.options.autogroupadd.groups.text"/>
                            <ul style="margin-left: 40px">
                                <ww:iterator value="defaultGroups">
                                    <li style="list-style-type: square;"><ww:property/> (<a href="<ww:url namespace="/console/secure/directory" action="updateremotecrowdoptions" method="removeDefaultGroup">
                                                                                                      <ww:param name="ID" value="ID"/>
                                                                                                      <ww:param name="groupToRemove" value="top"/>
                                                                                                      <ww:param name="%{xsrfTokenName}" value="%{xsrfToken}"/>
                                                                                                  </ww:url>"><ww:text name="directory.options.autogroupadd.remove.label"/></a>)</li>
                                </ww:iterator>
                            </ul>
                        </p>
                    </ww:else>

                </div>

                <div class="formFooter wizardFooter" style="padding:1em 0 1em 24em">
                    <div class="buttons">
                        <form name="manageGroupsForm" method="post"
                              action="<ww:url namespace="/console/secure/group" action="viewmembers" includeParams="none" />">
                            <input type="hidden" id="directoryID" name="directoryID" value="<ww:property value="directoryID" />"/>
                            <input type="hidden" id="groupName" name="groupName" value="<ww:property value="groupName" />"/>

                            <input id="addGroups" name="addGroups" type="button" class="button" style="width:120px"
                                   value="<ww:property value="getText('picker.addgroups.label')"/>"/>
                        </form>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <script src="<ww:url value="/console/js/entity_picker.js"/>"></script>

</div>

</body>
</html>
