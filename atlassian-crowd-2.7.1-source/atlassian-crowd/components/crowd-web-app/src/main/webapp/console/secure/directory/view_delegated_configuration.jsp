<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>

<html>
<head>
    <title>
        <ww:text name="menu.viewdirectory.label">
            <ww:param><ww:property value="directory.name"/></ww:param>
        </ww:text>
    </title>
    <meta name="section" content="directories"/>
    <meta name="pagename" content="view"/>
    <meta name="help.url" content="<ww:property value="getText('help.directory.delegated.configuration')"/>"/>

    <script>

        function testPrincipalSearch()
        {
            document.configuration_details.action = "<ww:url namespace="/console/secure/directory" action="updatedelegatedconfiguration" method="testUpdatePrincipalSearch" includeParams="none"/>";

            document.configuration_details.submit();
        }

        function testGroupSearch()
        {
            document.configuration_details.action = "<ww:url namespace="/console/secure/directory" action="updatedelegatedconfiguration" method="testUpdateGroupSearch" includeParams="none"/>";

            document.configuration_details.submit();
        }

    </script>


</head>
<body>
    <h2>
        <ww:text name="menu.viewdirectory.label">
            <ww:param><ww:property value="directory.name"/></ww:param>
        </ww:text>
    </h2>

    <div class="page-content">

        <ul class="tabs">

            <li>
                <a id="delegated-general"
                   href="<ww:url namespace="/console/secure/directory" action="viewdelegated" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text
                        name="menu.details.label"/></a>
            </li>

            <li>
                <a id="delegated-connectiondetails" href="<ww:url namespace="/console/secure/directory" action="updatedelegatedconnection" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.connector.label"/></a>
            </li>

            <li class="on">
                <span class="tab"><ww:text name="menu.configuration.label"/></span>
            </li>

            <li>
                <a id="delegated-permissions" href="<ww:url namespace="/console/secure/directory" action="updatedelegatedpermissions" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.permissions.label"/></a>
            </li>

            <li>
                <a id="delegated-options" href="<ww:url namespace="/console/secure/directory" action="updatedelegatedoptions" includeParams="none"><ww:param name="ID" value="ID" /></ww:url>"><ww:text name="menu.optional.label"/></a>
            </li>

        </ul>

        <div class="tabContent static" id="tab1">

            <div class="crowdForm">
                <form id="configuration_details" name="configuration_details" method="post" action="<ww:url namespace="/console/secure/directory" action="updatedelegatedconfiguration" method="update" includeParams="none" />">

                    <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

                    <div class="formBody">

                        <ww:component template="form_messages.jsp"/>

                        <input type="hidden" name="ID" value="<ww:property value="ID" />"/>

                        <h3>
                            <ww:text name="directoryconnector.userconfiguration.label"/>
                        </h3>

                        <ww:textfield name="userDNaddition" size="35">
                            <ww:param name="label" value="getText('directoryconnector.userdnaddition.label')"/>
                            <ww:param name="description">
                                <ww:text name="directoryconnector.userdnaddition.description"/>
                            </ww:param>
                        </ww:textfield>

                        <ww:textfield name="userObjectClass" size="35">
                            <ww:param name="required" value="true" />
                            <ww:param name="label" value="getText('directoryconnector.userobjectclass.label')"/>
                            <ww:param name="description">
                                <ww:text name="directoryconnector.userobjectclass.description"/>
                            </ww:param>
                        </ww:textfield>

                        <ww:textfield name="userObjectFilter" size="35">
                            <ww:param name="required" value="true" />
                            <ww:param name="label" value="getText('directoryconnector.userobjectfilter.label')"/>
                            <ww:param name="description">
                                <ww:text name="directoryconnector.userobjectfilter.description"/>
                            </ww:param>
                        </ww:textfield>

                        <ww:textfield name="userNameAttr" size="35">
                            <ww:param name="required" value="true" />
                            <ww:param name="label" value="getText('directoryconnector.usernameattribute.label')"/>
                            <ww:param name="description">
                                <ww:text name="directoryconnector.usernameattribute.description"/>
                            </ww:param>
                        </ww:textfield>

                        <ww:textfield name="userNameRdnAttr" size="35">
                            <ww:param name="label" value="getText('directoryconnector.usernamerdnattribute.label')"/>
                            <ww:param name="description">
                                <ww:property value="getText('directoryconnector.usernamerdnattribute.description')"/>
                            </ww:param>
                            <ww:param name="required" value="true" />
                        </ww:textfield>

                        <ww:textfield name="userFirstnameAttr" size="35">
                            <ww:param name="required" value="true" />
                            <ww:param name="label" value="getText('directoryconnector.userfirstnameattribute.label')"/>
                            <ww:param name="description">
                                <ww:text name="directoryconnector.userfirstnameattribute.description"/>
                            </ww:param>
                        </ww:textfield>

                        <ww:textfield name="userLastnameAttr" size="35">
                            <ww:param name="required" value="true" />
                            <ww:param name="label" value="getText('directoryconnector.userlastnameattribute.label')"/>
                            <ww:param name="description">
                                <ww:text name="directoryconnector.userlastnameattribute.description"/>
                            </ww:param>
                        </ww:textfield>

                        <ww:textfield name="userDisplayNameAttr" size="35">
                            <ww:param name="label" value="getText('directoryconnector.userdisplaynameattribute.label')"/>
                            <ww:param name="description">
                                <ww:property value="getText('directoryconnector.userdisplaynameattribute.description')"/>
                            </ww:param>
                            <ww:param name="required" value="true" />
                        </ww:textfield>

                        <ww:textfield name="userMailAttr" size="35">
                            <ww:param name="required" value="true" />
                            <ww:param name="label" value="getText('directoryconnector.usermailattribute.label')"/>
                            <ww:param name="description">
                                <ww:text name="directoryconnector.usermailattribute.description"/>
                            </ww:param>
                        </ww:textfield>

                        <ww:textfield name="userGroupMemberAttr" size="35">
                            <ww:param name="required" value="true" />
                            <ww:param name="label" value="getText('directoryconnector.usermemberofattribute.label')"/>
                            <ww:param name="description">
                                <ww:text name="directoryconnector.usermemberofattribute.description"/>
                            </ww:param>
                        </ww:textfield>

                        <ww:textfield name="userExternalIdAttr" size="35">
                            <ww:param name="label" value="getText('directoryconnector.userexternalidattribute.label')"/>
                            <ww:param name="description">
                                <ww:property value="getText('directoryconnector.userexternalidattribute.description')"/>
                            </ww:param>
                            <ww:param name="required" value="false" />
                        </ww:textfield>

                        <div class="textFieldButton buttons" style="">
                            <input id="test-search-principal" type="button" class="button" style="width: 125px;" value="<ww:property value="getText('directoryconnector.testsearch.label')"/>" onClick="testPrincipalSearch();"/>
                        </div>
                        
                    </div>

                    <ww:if test="showGroupsConfiguration">
                    <h3>
                        <ww:property value="getText('directoryconnector.groupconfiguration.label')"/>
                    </h3>

                    <ww:textfield name="groupDNaddition" size="35">
                        <ww:param name="label" value="getText('directoryconnector.groupdnaddition.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directoryconnector.groupdnaddition.description')"/>
                        </ww:param>
                    </ww:textfield>

                    <ww:textfield name="groupObjectClass" size="35">
                        <ww:param name="label" value="getText('directoryconnector.groupobjectclass.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directoryconnector.groupobjectclass.description')"/>
                        </ww:param>
                        <ww:param name="required" value="true" />
                    </ww:textfield>

                    <ww:textfield name="groupObjectFilter" size="35">
                        <ww:param name="label" value="getText('directoryconnector.groupobjectfilter.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directoryconnector.groupobjectfilter.description')"/>
                        </ww:param>
                        <ww:param name="required" value="true" />
                    </ww:textfield>

                    <ww:textfield name="groupNameAttr" size="35">
                        <ww:param name="label" value="getText('directoryconnector.groupname.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directoryconnector.groupname.description')"/>
                        </ww:param>
                        <ww:param name="required" value="true" />
                    </ww:textfield>

                    <ww:textfield name="groupDescriptionAttr" size="35">
                        <ww:param name="label" value="getText('directoryconnector.groupdescription.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directoryconnector.groupdescription.description')"/>
                        </ww:param>
                        <ww:param name="required" value="true" />
                    </ww:textfield>

                    <ww:textfield name="groupMemberAttr" size="35">
                        <ww:param name="label" value="getText('directoryconnector.groupmember.label')"/>
                        <ww:param name="description">
                            <ww:property value="getText('directoryconnector.groupmember.description')"/>
                        </ww:param>
                        <ww:param name="required" value="true" />
                    </ww:textfield>

                    <div class="textFieldButton buttons" style="">
                        <input id="test-search-group" type="button" class="button" style="width: 125px;" value="<ww:property value="getText('directoryconnector.testsearch.label')"/>" onClick="testGroupSearch();"/>
                    </div>
                    </ww:if>

                    <div class="formFooter wizardFooter">
                        <div class="buttons">
                            <input type="submit" class="button" value="<ww:text name="update.label"/> &raquo;"/>
                            <input type="button" class="button" value="<ww:text name="cancel.label"/>"
                                   onClick="window.location='<ww:url namespace="/console/secure/directory" action="viewdelegated" method="default" includeParams="none" ><ww:param name="ID" value="ID" /></ww:url>';"/>
                        </div>
                    </div>

                </form>

            </div>

        </div>

    </div>
</body>
</html>
