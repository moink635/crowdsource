<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
<head>
    <title>
        <ww:property value="getText('menu.viewprincipal.label')"/>
    </title>
    <meta name="section" content="users"/>
    <meta name="pagename" content="viewuser"/>
    <meta name="help.url" content="<ww:property value="getText('help.user.view.details')"/>"/>

    <ww:property value="webResourceManager.requireResource('com.atlassian.auiplugin:jquery')"/>
    <ww:property value="webResourceManager.requireResource('com.atlassian.auiplugin:ajs')"/>
    <ww:property value="webResourceManager.requiredResources" escape="false"/>

    <jsp:include page="../../decorator/javascript_tabs.jsp">
        <jsp:param name="totalTabs" value="5"/>
    </jsp:include>

    <script>

        AJS.$(document).ready(function(){

            CROWD_PICKER.attachPicker("addGroups", "<ww:property value="getText('picker.addgroups.label')"/>",
                    "<ww:url namespace="/console/secure/pickers" action="searchPicker" method="getGroupNonMembershipsOfUser" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="entityName" value="name"/></ww:url>",
                    "<ww:property value="getText('picker.addselected.groups.label')"/>",
                    "<ww:url namespace="/console/secure/user" action="updategroups" method="addGroups" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="entityName" value="name"/><ww:param name="tab" value="3"/></ww:url>",
                    "<ww:url namespace="/console/secure/pickers" action="displayPicker" includeParams="none" />",
                    "<ww:url namespace="/console/secure/pickers" action="checkLoginStatus" includeParams="none" />",
                    "<ww:property value="getText('picker.addgroups.start.message')"/>",
                    "<ww:url namespace="/console/secure/user" action="view" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="name" value="name"/><ww:param name="tab" value="3"/></ww:url>");

            CROWD_PICKER.attachPicker("removeGroups", "<ww:property value="getText('picker.removegroups.label')"/>",
                    "<ww:url namespace="/console/secure/pickers" action="searchPicker" method="getGroupMembershipsOfUser" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="entityName" value="name"/></ww:url>",
                    "<ww:property value="getText('picker.removeselected.groups.label')"/>",
                    "<ww:url namespace="/console/secure/user" action="updategroups" method="removeGroups" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="entityName" value="name"/><ww:param name="tab" value="3"/></ww:url>",
                    "<ww:url namespace="/console/secure/pickers" action="displayPicker" includeParams="none" />",
                    "<ww:url namespace="/console/secure/pickers" action="checkLoginStatus" includeParams="none" />",
                    "<ww:property value="getText('picker.removegroups.start.message')"/>",
                    "<ww:url namespace="/console/secure/user" action="view" includeParams="none" escapeAmp="false"><ww:param name="directoryID" value="directoryID"/><ww:param name="name" value="name"/><ww:param name="tab" value="3"/></ww:url>");

        });

        function addAttribute()
        {
            var form = document.attributesForm;
            form.action = '<ww:url namespace="/console/secure/user" action="updateattributes" method="addAttribute" includeParams="none" />';
            form.submit();
        }

        function removeAlias(ref)
        {
            var field = document.getElementById(ref);
            field.value = '';
            var form = document.applicationsForm;
            form.submit();
        }

        function processTabsAndSetHelpLink(tab) {
            switch (tab) {
            case 1:
                setHelpLink('<ww:property value="getText('help.user.view.details')"/>'); break;
            case 2:
                setHelpLink('<ww:property value="getText('help.user.view.attributes')"/>'); break;
            case 3:
                setHelpLink('<ww:property value="getText('help.user.view.groups')"/>'); break;
            // case 4: No longer present
            case 5:
                setHelpLink('<ww:property value="getText('help.user.view.applications')"/>'); break;
            }
            processTabs(tab);
        }

        function initTabsAndHelpLinks()
        {
            var tab = parseInt('<ww:property value="tab" />');
            processTabsAndSetHelpLink(tab);
        }


    </script>
</head>
<body onload="initTabsAndHelpLinks();">

    <h2>
        <ww:property value="getText('menu.viewprincipal.label')"/>
        &nbsp;&ndash;&nbsp;
        <ww:property value="name"/>
    </h2>

    <div class="page-content">

        <ul class="tabs">
            <li class="on" id="hreftab1">
                <a id="user-details-tab" href="javascript:processTabsAndSetHelpLink(1);">
                    <ww:property value="getText('menu.details.label')"/>
                </a>
            </li>

            <li id="hreftab2">
                <a id="user-attributes-tab" href="javascript:processTabsAndSetHelpLink(2);">
                    <ww:property value="getText('menu.attributes.label')"/>
                </a>
            </li>

            <li id="hreftab3">
                <a id="user-groups-tab" href="javascript:processTabsAndSetHelpLink(3);">
                    <ww:property value="getText('menu.groups.label')"/>
                </a>
            </li>

            <li id="hreftab5">
                <a id="user-applications-tab" href="javascript:processTabsAndSetHelpLink(5);">
                    <ww:property value="getText('menu.applications.label')"/>
                </a>
            </li>

        </ul>

        <div class="tabContent" id="tab1">

            <div class="crowdForm">
                <form name="updateprincipalForm" method="post" action="<ww:url namespace="/console/secure/user" action="update" method="updateGeneral" includeParams="none" />">

                    <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

                    <div class="formBody">

                        <ww:component template="form_tab_messages.jsp">
                            <ww:param name="tabID" value="1"/>
                        </ww:component>

                        <input type="hidden" name="tab" value="1"/>
                        <input type="hidden" name="directoryID" value="<ww:property value="directoryID" />"/>
                        <input type="hidden" name="name" value="<ww:property value="name" />"/>
                        <input type="hidden" name="externalId" value="<ww:property value="externalId" />"/>

                        <ww:set name="typeAsString"><ww:property value="directory.type.name()"/></ww:set>

                        <ww:textfield name="newName">
                            <ww:param name="label" value="getText('principal.name.label')"/>
                            <ww:param name="description">
                                <ww:property value="getText('principal.name.description')"/>
                            </ww:param>
                            <ww:param name="required" value="true"/>
                        </ww:textfield>

                        <ww:component template="form_row.jsp">
                           <ww:param name="label" value="getText('principal.directory.label')" />
                            <ww:param name="value">
                                    <ww:property value="directory.name"/>&nbsp;&mdash;&nbsp;<ww:property value="directoryImplementationDescriptiveName"/>
                            </ww:param>
                            <ww:param name="escapeValue" value="false" />
                        </ww:component>

                        <ww:textfield name="email" size="50">
                            <ww:param name="label" value="getText('principal.email.label')"/>
                            <ww:param name="description">
                                <ww:property value="getText('principal.email.description')"/>
                            </ww:param>
                        </ww:textfield>

                        <ww:checkbox name="active" fieldValue="true">
                            <ww:param name="label" value="getText('principal.active.label')"/>
                        </ww:checkbox>

                        <ww:textfield name="firstname">
                            <ww:param name="label" value="getText('principal.firstname.label')"/>
                        </ww:textfield>

                        <ww:textfield name="lastname">
                            <ww:param name="label" value="getText('principal.lastname.label')"/>
                        </ww:textfield>

                        <ww:if test="#typeAsString == 'DELEGATING'">
                            <ww:password name="password">
                                <ww:param name="label" value="getText('password.label')"/>
                                <ww:param name="disabled" value="true"/>
                                <ww:param name="description">
                                    <ww:property value="getText('principal.editpassword.disabled.description')"/>
                                </ww:param>
                            </ww:password>
                        </ww:if>
                        <ww:else>
                            <ww:password name="password">
                                <ww:param name="label" value="getText('password.label')"/>
                                <ww:param name="description">
                                    <ww:property value="getText('principal.editpassword.description')"/>
                                </ww:param>
                            </ww:password>
                        </ww:else>

                        <ww:if test="#typeAsString == 'DELEGATING'">
                            <ww:password name="passwordConfirm">
                                <ww:param name="label" value="getText('passwordconfirm.label')"/>
                                <ww:param name="disabled" value="true"/>
                            </ww:password>
                        </ww:if>
                        <ww:else>
                            <ww:password name="passwordConfirm">
                                <ww:param name="label" value="getText('passwordconfirm.label')"/>
                            </ww:password>
                        </ww:else>
                    </div>

                    <div class="formFooter wizardFooter">
                        <div class="buttons">
                            <input type="submit" class="button" value="<ww:property value="getText('update.label')"/> &raquo;"/>
                            <input type="button" class="button" value="<ww:property value="getText('cancel.label')"/>"
                                   onClick="window.location='<ww:url namespace="/console/secure/user" action="view" method="default" includeParams="none"><ww:param name="directoryID" value="directoryID"/><ww:param name="name" value="name"/><ww:param name="tab" value="1"/></ww:url>';"/>
                        </div>
                    </div>

                </form>

            </div>

        </div>

        <div class="tabContent" id="tab2">

            <div class="crowdForm">
                <form method="post" action="<ww:url namespace="/console/secure/user" action="updateattributes" method="updateAttributes" includeParams="none" />" name="attributesForm">

                    <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

                    <div class="formBody">

                        <ww:component template="form_tab_messages.jsp">
                            <ww:param name="tabID" value="2"/>
                        </ww:component>

                        <div class="fieldArea">
                            <ww:fielderror>
                                <ww:param value="%{'attribute'}"/>
                            </ww:fielderror>
                        </div>

                        <input type="hidden" name="tab" value="2"/>
                        <input type="hidden" name="directoryID" value="<ww:property value="directoryID" />"/>
                        <input type="hidden" name="name" value="<ww:property value="name" />"/>
                        <input type="hidden" name="externalId" value="<ww:property value="externalId" />"/>

                        <table id="attributesTable" class="formTable">
                            <tr>
                                <th width="40%">
                                    <ww:property value="getText('attribute.label')"/>
                                </th>
                                <th width="40%">
                                    <ww:property value="getText('values.label')"/>
                                </th>
                                <th width="20%">
                                    <ww:property value="getText('browser.action.label')"/>
                                </th>
                            </tr>

                            <ww:iterator value="userAttributes" status="rowstatus">

                                <input type="hidden" name="attributes" value="<ww:property value="key" />"/>

                                <ww:if test="#rowstatus.odd == true"><tr class="odd"></ww:if>
                                <ww:else><tr class="even"></ww:else>
                                <td>
                                    <ww:property value="key"/>
                                </td>
                                <td>
                                    <ww:iterator value="value" status="valuestatus">
                                        <input type="text" name="<ww:property value="key" /><ww:property value="#valuestatus.count" />" value="<ww:property />" size="30"/>
                                    </ww:iterator>
                                </td>
                                <td>
                                    <a id="remove-<ww:property value="key"/>" href="<ww:url namespace="/console/secure/user" action="updateattributes" method="removeAttribute" includeParams="none" >
                                                                                        <ww:param name="name" value="name" />
                                                                                        <ww:param name="attribute" value="key" />
                                                                                        <ww:param name="directoryID" value="directoryID" />
                                                                                        <ww:param name="tab" value="2"/>
                                                                                        <ww:param name="%{xsrfTokenName}" value="%{xsrfToken}"/>
                                                                                    </ww:url>"
                                       title="<ww:property value="getText('remove.label')"/>">
                                        <ww:property value="getText('remove.label')"/>
                                    </a>
                                </td>
                                </tr>

                            </ww:iterator>
                        </table>
                    </div>

                    <div class="formFooter wizardFooter">
                        <div class="buttons">
                            <ww:property value="getText('attribute.label')"/>
                            :&nbsp;<input type="text" name="attribute" size="15" value="<ww:property value="attribute" />"/>&nbsp;
                            <ww:property value="getText('value.label')"/>
                            :&nbsp;<input type="text" name="value" size="15" value="<ww:property value="value" />"/>&nbsp;
                            <input id="add-attribute" type="button" class="button" value="<ww:property value="getText('add.label')"/> &raquo;" onClick="addAttribute();"/>
                            &nbsp;&nbsp;&nbsp;
                            <input type="submit" class="button" value="<ww:property value="getText('update.label')"/> &raquo;"/>
                            <input type="button" class="button" value="<ww:property value="getText('cancel.label')"/>"
                                   onClick="window.location='<ww:url namespace="/console/secure/user" action="view" method="default" includeParams="none" ><ww:param name="directoryID" value="directoryID"/><ww:param name="name" value="name"/><ww:param name="tab" value="2" /></ww:url>';"/>
                        </div>
                    </div>

                </form>
            </div>
        </div>


        <div class="tabContent" id="tab3">

            <div class="crowdForm">
                <form method="post" action="<ww:url namespace="/console/secure/user" action="view" includeParams="none" />" name="groupsForm">
                    <div class="formBody">
                        <ww:component template="form_tab_messages.jsp">
                            <ww:param name="tabID" value="3"/>
                        </ww:component>

                        <ww:if test="!hasUpdateGroupPermission()">
                            <p class="informationBox">
                                <ww:property value="getText('group.modify.disabled')"/>
                            </p>
                        </ww:if>

                        <ww:if test="preventingLockout==true">
                            <p class="noteBox">
                                <ww:text name="preventlockout.removegroupsfromuser.label">
                                    <ww:param name="0"><ww:property value="unremovedGroups"/></ww:param>
                                    <ww:param name="1"><ww:property value="name"/></ww:param>
                                </ww:text>
                            </p>
                        </ww:if>
                        <ww:elseif test="!unremovedGroups.empty">
                            <p class="warningBox">
                                <ww:text name="principal.unremovedgroups.label">
                                    <ww:param name="0"><ww:property value="unremovedGroups"/></ww:param>
                                    <ww:param name="1"><ww:property value="name"/></ww:param>
                                </ww:text>
                            </p>
                        </ww:elseif>

                        <ww:if test="groups.size > 0">
                            <p>
                                <ww:property value="getText('principal.groupmappings.text')"/>
                            </p>

                            <ww:set name="userName" value="[0].name"/>
                            <ww:set name="directory" value="directoryID" />
                            <ww:set name="directoryName" value="directory.name" />

                            <input type="hidden" name="tab" value="3"/>
                            <input type="hidden" name="directoryID" value="<ww:property value="directoryID" />"/>
                            <input type="hidden" name="name" value="<ww:property value="name" />"/>
                            <input type="hidden" name="externalId" value="<ww:property value="externalId" />"/>

                            <table id="groupsTable" class="formTable">
                                <tr>
                                    <th width="30%">
                                        <ww:property value="getText('browser.group.label')"/>
                                    </th>
                                    <th width="60%">
                                        <ww:property value="getText('browser.description.label')"/>
                                    </th>
                                    <th width="10%">
                                        <ww:property value="getText('browser.active.label')"/>
                                    </th>
                                </tr>

                                <ww:iterator value="groups" status="rowstatus">
                                    <ww:if test="#rowstatus.odd == true">
                                        <tr class="odd">
                                    </ww:if>
                                    <ww:else>
                                        <tr class="even">
                                    </ww:else>

                                    <td>
                                        <a id="viewgroup-<ww:property value="name" />-<ww:property value="#directoryName" />" href="<ww:url namespace="/console/secure/group" action="view" includeParams="none" ><ww:param name="directoryID" value="#directory" /><ww:param name="directoryID" value="#directory" /><ww:param name="name" value="name" /></ww:url>">
                                            <ww:property value="name"/>
                                        </a>
                                    </td>

                                    <td>
                                       <ww:property value="description"/>
                                    </td>

                                    <td>
                                        <ww:property value="active" />
                                    </td>

                                    </tr>

                                </ww:iterator>

                            </table>
                        </ww:if>
                        <ww:else>
                            <p>
                                <ww:text name="principal.nogroupmemberships.text">
                                    <ww:param><ww:property value="name"/></ww:param>
                                </ww:text>
                            </p>
                        </ww:else>
                    </div>


                    <ww:if test="hasUpdateGroupPermission()">
                        <div class="formFooter wizardFooter">
                            <div class="buttons">
                                <input id="addGroups" name="addGroups" type="button" class="button"
                                       style="width:120px"
                                       value="<ww:property value="getText('picker.addgroups.label')"/>"/>

                                <ww:if test="groups.size > 0">
                                    <input id="removeGroups" name="removeGroups" type="button" class="button"
                                           style="width:120px"
                                           value="<ww:property value="getText('picker.removegroups.label')"/>"/>
                                </ww:if>
                            </div>
                        </div>
                    </ww:if>

                </form>
            </div>
        </div>

        <div class="tabContent" id="tab5">

            <div class="crowdForm">
                <form method="post" action="<ww:url namespace="/console/secure/user" action="updatealiases" method="update" includeParams="none" />" name="applicationsForm">

                    <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

                    <input type="hidden" name="tab" value="5"/>
                    <input type="hidden" name="directoryID" value="<ww:property value="directoryID" />"/>
                    <input type="hidden" name="name" value="<ww:property value="name" />"/>
                    <input type="hidden" name="externalId" value="<ww:property value="externalId" />"/>

                    <div class="formBody">

                        <ww:component template="form_tab_messages.jsp">
                            <ww:param name="tabID" value="5"/>
                        </ww:component>

                        <p>
                            <ww:property value="getText('principal.applications.text')"/>
                        </p>

                        <table id="applicationsTable" class="formTable">
                            <tr>
                                <th width="40%">
                                    <ww:property value="getText('browser.application.label')"/>
                                </th>
                                <th width="40%">
                                    <ww:property value="getText('browser.alias.label')"/>
                                </th>
                                <th width="20%">
                                    <ww:text name="browser.action.label"/>
                                </th>
                            </tr>

                            <ww:iterator value="applications" status="rowstatus">

                                <ww:if test="#rowstatus.odd == true">
                                    <tr class="odd">
                                </ww:if>
                                <ww:else>
                                    <tr class="even">
                                </ww:else>

                                <td>
                                    <a id="viewapplication-<ww:property value="top" />-<ww:property value="name" />" href="<ww:url namespace="/console/secure/application" action="viewdetails" includeParams="none" ><ww:param name="ID" value="id" /></ww:url>">
                                        <ww:property value="name"/>
                                    </a>
                                </td>

                                <td>
                                    <ww:if test="!aliasingEnabled">
                                        <span style="color: gray;"><ww:text name="alias.disabled.label"/></span>
                                    </ww:if>
                                    <ww:else>
                                        <input id="alias-appid-<ww:property value="id"/>" type="text" name="alias-appid-<ww:property value="id"/>" value="<ww:property value="aliases.get(#rowstatus.index)"/>" size="30"/>
                                        <ww:if test="applicationInError.containsKey(id)"><span class="error" >*</span></ww:if>
                                    </ww:else>
                                </td>

                                <td>
                                    <ww:if test="aliasingEnabled">
                                        <a id="remove-alias-appid-<ww:property value="id"/>" href="javascript:removeAlias('alias-appid-<ww:property value="id"/>');"><ww:text name="alias.remove.label"/></a>
                                    </ww:if>
                                </td>

                                </tr>

                            </ww:iterator>

                        </table>

                    </div>

                    <div class="formFooter wizardFooter">
                        <div class="buttons">
                            <input type="submit" class="button" value="<ww:property value="getText('update.label')"/> &raquo;"/>
                            <input type="button" class="button" value="<ww:property value="getText('cancel.label')"/>"
                                   onClick="window.location='<ww:url namespace="/console/secure/user" action="view" method="default" includeParams="none" ><ww:param name="directoryID" value="directoryID"/><ww:param name="name" value="name"/><ww:param name="tab" value="5" /></ww:url>';"/>
                        </div>
                    </div>
                </form>
            </div>

        </div>

    </div>
    <script src="<ww:url value="/console/js/entity_picker.js"/>"></script>
</body>
</html>
