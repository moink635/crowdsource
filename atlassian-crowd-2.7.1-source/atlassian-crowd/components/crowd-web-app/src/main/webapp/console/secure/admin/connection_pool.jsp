<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/webwork" prefix="ww" %>
<html>
<head>
    <title>
        <ww:property value="getText('connectionpool.title')"/></title>
    <meta name="section" content="administration"/>
    <meta name="pagename" content="connectionpool"/>
    <meta name="help.url" content="<ww:property value="getText('help.admin.connectionpool')"/>"/>
</head>

<body>
<h2><ww:property value="getText('menu.connectionpool.label')"/></h2>

<div class="page-content">
    <div class="crowdForm">
        <form id="general" method="post" action="<ww:url namespace="/console/secure/admin" action="connectionpool" method="update" includeParams="none"/>"
              name="connectionpool">

            <ww:hidden name="%{xsrfTokenName}" value="%{xsrfToken}"/>

            <div class="formBody">

                <p><ww:text name="connectionpool.description.text"/></p>

                <ww:component template="form_messages.jsp"/>
                <!-- Display the system properties that are currently in effect -->
                <h3><ww:property value="getText('connectionpool.current.settings.title')"/></h3>
                <ww:component template="form_row.jsp">
                    <ww:param name="label" value="getText('connectionpool.initialSize.label')"/>
                    <ww:param name="value">
                        <ww:property value="systemInitialSize"/>
                        <ww:if test="systemInitialSize != initialSize">
                            <em>(new value: <ww:property value="initialSize"/>)</em>
                        </ww:if>
                    </ww:param>
                    <ww:param name="escapeValue" value="false" />
                </ww:component>

                <ww:component template="form_row.jsp">
                    <ww:param name="label" value="getText('connectionpool.preferredSize.label')"/>
                    <ww:param name="value">
                        <ww:property value="systemPreferredSize"/>
                        <ww:if test="systemPreferredSize != preferredSize">
                            <em>(new value: <ww:property value="preferredSize"/>)</em>
                        </ww:if>
                    </ww:param>
                    <ww:param name="escapeValue" value="false" />
                </ww:component>

                <ww:component template="form_row.jsp">
                    <ww:param name="label" value="getText('connectionpool.maximumSize.label')"/>
                    <ww:param name="value">
                        <ww:property value="systemMaximumSize"/>
                        <ww:if test="systemMaximumSize != maximumSize">
                            <em>(new value: <ww:property value="maximumSize"/>)</em>
                        </ww:if>
                    </ww:param>
                    <ww:param name="escapeValue" value="false" />
                </ww:component>

                <ww:component template="form_row.jsp">
                    <ww:param name="label" value="getText('connectionpool.timeout.label')"/>
                    <ww:param name="value">
                        <ww:property value="systemTimeoutInSec"/>
                        <ww:if test="systemTimeoutInSec != timeoutInSec">
                            <em>(new value: <ww:property value="timeoutInSec"/>)</em>
                        </ww:if>
                    </ww:param>
                    <ww:param name="escapeValue" value="false" />
                </ww:component>

                <ww:component template="form_row.jsp">
                    <ww:param name="label" value="getText('connectionpool.supportedProtocol.label')"/>
                    <ww:param name="value">
                        <ww:property value="systemSupportedProtocol"/>
                        <ww:if test="systemSupportedProtocol != supportedProtocol">
                            <em>(new value: <ww:property value="supportedProtocol"/>)</em>
                        </ww:if>
                    </ww:param>
                    <ww:param name="escapeValue" value="false" />
                </ww:component>

                <ww:component template="form_row.jsp">
                    <ww:param name="label" value="getText('connectionpool.supportedAuthentication.label')"/>
                    <ww:param name="value">
                        <ww:property value="systemSupportedAuthentication"/>
                        <ww:if test="systemSupportedAuthentication != supportedAuthentication">
                            <em>(new value: <ww:property value="supportedAuthentication"/>)</em>
                        </ww:if>
                    </ww:param>
                    <ww:param name="escapeValue" value="false" />
                </ww:component>

                <!-- Display form to allow users to update the LDAP connection pool properties -->
                <h3><ww:property value="getText('connectionpool.update.settings.title')"/></h3>

                <p class="noteBox"><ww:text name="connectionpool.warning.text"/></p>

                <ww:textfield name="initialSize">
                    <ww:param name="label" value="getText('connectionpool.initialSize.label')"/>
                    <ww:param name="description" value="getText('connectionpool.initialSize.description')"/>
                </ww:textfield>

                <ww:textfield name="preferredSize">
                    <ww:param name="label" value="getText('connectionpool.preferredSize.label')"/>
                    <ww:param name="description" value="getText('connectionpool.preferredSize.description')"/>
                </ww:textfield>

                <ww:textfield name="maximumSize">
                    <ww:param name="label" value="getText('connectionpool.maximumSize.label')"/>
                    <ww:param name="description" value="getText('connectionpool.maximumSize.description')"/>
                </ww:textfield>

                <ww:textfield name="timeoutInSec">
                    <ww:param name="label" value="getText('connectionpool.timeout.label')"/>
                    <ww:param name="description" value="getText('connectionpool.timeout.description')"/>
                </ww:textfield>

                <ww:textfield name="supportedProtocol">
                    <ww:param name="label" value="getText('connectionpool.supportedProtocol.label')"/>
                    <ww:param name="description" value="getText('connectionpool.supportedProtocol.description')"/>
                </ww:textfield>

                <ww:textfield name="supportedAuthentication">
                    <ww:param name="label" value="getText('connectionpool.supportedAuthentication.label')"/>
                    <ww:param name="description" value="getText('connectionpool.supportedAuthentication.description')"/>
                </ww:textfield>

            </div>

            <div class="formFooter wizardFooter">
                <div class="buttons">
                    <input type="submit" class="button" value="<ww:property value="getText('update.label')"/> &raquo;"/>
                    <input type="button" class="button" id="cancel" value="<ww:property value="getText('cancel.label')"/>"
                           onClick="window.location='<ww:url namespace="/console/secure/admin" action="connectionpool" method="default" includeParams="none"></ww:url>';"/>
                </div>
            </div>

        </form>

    </div>
</div>
</body>
</html>
