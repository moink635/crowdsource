var CROWD_SYNC_FEEDBACK =
{
    intervalId : undefined,

    startSyncFeedback : function(id)
    {
        CROWD_SYNC_FEEDBACK.intervalId = window.setInterval(function()
            {
                var contextPath = AJS.$("meta[name=ajs-context-path]").attr("content");
                AJS.$.ajax(
                {
                    url: contextPath + "/rest/syncfeedback/1/directory/" + id,
                    dataType: 'json',
                    cache: false,
                    success: function(data)
                    {
                        var roundInfo = data.activeRound ? data.activeRound : data.lastRound;
                        if (roundInfo)
                        {
                            AJS.$('#startTime').html(roundInfo.startTime);
                            AJS.$('#duration').html(roundInfo.duration);
                            AJS.$('#status').text(roundInfo.status);
                        }
                        if (!data.activeRound) // Sync has finished
                        {
                            // Toggle header
                            AJS.$('#activeSyncLabel').css('display', 'none');
                            AJS.$('#lastSyncLabel').css('display', 'inline');
                            // Stop polling for changes
                            window.clearInterval(CROWD_SYNC_FEEDBACK.intervalId);
                        }
                    }
                });
            }, 1000);
    }
};