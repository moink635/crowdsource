<%@ taglib uri="/webwork" prefix="ww" %>

<ww:component template="form_messages.jsp"/>

<ww:if test="not isAtCorrectStep()">
    <p class="warningBox">
        <ww:property value="getText('setup.not.at.correct.step.error', {'<a href=\"selectsetupstep.action\">','</a>'}, false)" escape="false"/>
    </p>
</ww:if>