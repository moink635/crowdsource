<%@ taglib uri="/webwork" prefix="ww" %>
<ol class="tabs">
    <ww:iterator value="getWebItemsForApplication()">
        <ww:if test="key == parameters.pagekey">
            <li class="on">
        </ww:if>
        <ww:else>
            <li>
        </ww:else>
        <a id="<ww:property value="link.id"/>" href="<ww:property value="getLink(link)"/>"><ww:property value="getText(webLabel.key)"/></a>
        </li>
    </ww:iterator>
</ol>
