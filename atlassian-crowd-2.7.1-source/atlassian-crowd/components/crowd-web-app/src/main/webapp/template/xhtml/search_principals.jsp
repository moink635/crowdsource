<%
    /**
     * Component Usage:
     *
     * %{parameters.webwork.namespace}
     * %{parameters.webwork.action}
     * %{parameters.applicationid}
     *
     *
     * e.g.
     * <ww:param name="webwork.namespace" value="/console/secure/application"/>
     * <ww:param name="webwork.action" value="viewusers"/>
     * <ww:param name="applicationid" value="ID"/>
     *
     * Action must provide:
     * - getResults(): returning List<User>
     * - getAliasForUser(String username): returning String alias or null if there is no alias.
     */
%>

<%@ taglib uri="/webwork" prefix="ww" %>

<form class="crowd2templates" name="searchusers" method="post"
      action="<ww:url includeParams="none"><ww:param name="namespace" value="%{parameters.webwork.namespace}"/><ww:param name="action" value="%{parameters.webwork.action}"/><ww:param name="ID" value="%{parameters.applicationid}"/></ww:url>">
    <ww:component template="form_messages.jsp"/>
    <ww:property value="%{parameters.webwork.namespace}"/>
    <p class="miniform">
        <label>
            <ww:property value="getText('browser.filter.label')"/>
            :&nbsp;<input type="text" name="search" size="35" value="<ww:property value="search"/>"/>
        </label>

        <!-- Search by Application -->
        <ww:if test="%{parameters.applicationid}">

        </ww:if>
        <ww:else>
            <!-- Search by Directories -->
            <label>
                <ww:property value="getText('browser.directory.label')"/>
                :&nbsp;

                <select name="directoryID" id="directoryID" style="width: 130px;">
                    <ww:if test="directoryID == -1">
                        <option value="">
                            <ww:property value="getText('selectdirectory.label')"/>
                        </option>
                    </ww:if>
                    <ww:iterator value="directories">
                        <option value="<ww:property value="id" />"
                                <ww:if test="[1].directoryID == id">selected</ww:if> >
                            <ww:property value="name"/>
                        </option>
                    </ww:iterator>
                </select>
            </label>

            <label>
                <ww:property value="getText('principal.active.label')"/>
                :&nbsp;
                <select name="active" style="width: 75px;">
                    <option value="">
                        <ww:property value="getText('all.label')"/>
                    </option>
                    <option value="true" <ww:if test="active == 'true'">selected</ww:if>>
                        <ww:property value="getText('active.label')"/>
                    </option>
                    <option value="false" <ww:if test="active == 'false'">selected</ww:if>>
                        <ww:property value="getText('inactive.label')"/>
                    </option>
                </select>
            </label>

        </ww:else>

        <input type="submit" name="submit-search" class="button" value="<ww:text name="browser.filter.label"/>"/>

    </p>

</form>

<table class="crowd2templates search-results" id="user-details">
    <tr>
        <th width="45%">
            <ww:property value="getText('principal.actualname.label')"/>
        </th>
        <th width="55%">
            <ww:property value="getText('principal.info.label')"/>
        </th>
    </tr>

    <ww:iterator value="results" status="rowstatus">
        <ww:if test="#rowstatus.count <= resultsPerPage">
            <ww:if test="#rowstatus.odd == true">
                <tr class="odd">
            </ww:if>
            <ww:else>
                <tr class="even">
            </ww:else>

            <td valign="top" class="search-result-name" id="search-result-name-<ww:property value="#rowstatus.count"/>">
                <a href="<ww:url namespace="/console/secure/user" action="view" method="default" includeParams="none" ><ww:param name="name" value="name"/><ww:param name="directoryID" value="directoryId" /></ww:url>"
                   title="<ww:property value="getText('browser.view.label')"/>">
                    <ww:property value="displayName"/>
                </a>
            </td>
            <td valign="top" class="search-result-details" id="search-result-details-<ww:property value="#rowstatus.count"/>">
                <ul>
                    <!-- In here goes aliases & other details relevant to the search made -->
                    <li><b><ww:text name="principal.name.label"/></b>:<em> <ww:property value="name"/></em></li>
                    <li><b><ww:text name="principal.email.label"/></b>:<em> <ww:property value="emailAddress"/></em></li>
                    <ww:if test="getAliasForUser(name) != null">
                        <li><b><ww:text name="principal.alias.label"/></b>:<em> <ww:property value="getAliasForUser(name)"/> </em></li>
                    </ww:if>
                </ul>
            </td>
            </tr>
        </ww:if>
    </ww:iterator>
</table>

<ww:if test="resultsStart != 0 || results.size > resultsPerPage">
    <table class="pager">
        <tr class="pager">
            <td align="left" width="50%" class="pager">
                <ww:if test="resultsStart != 0">
                    <a href="javascript: document.previous.submit()">&laquo;
                        <ww:property value="getText('previous.label')"/>
                    </a>
                </ww:if>
            </td>
            <td align="right" width="50%" class="pager">
                <ww:if test="results.size > resultsPerPage">
                    <a href="javascript: document.next.submit()">
                        <ww:property value="getText('next.label')"/> &raquo;</a>
                </ww:if>
            </td>
        </tr>
    </table>
</ww:if>

<form name="next" method="post"
      action="<ww:url includeParams="none"><ww:param name="namespace" value="%{parameters.webwork.namespace}"/><ww:param name="action" value="%{parameters.webwork.action}"/><ww:param name="ID" value="%{parameters.applicationid}"/></ww:url>">
    <input type="hidden" name="directoryID" value="<ww:property value="directoryID" />"/>
    <input type="hidden" name="search" value="<ww:property value="search" />"/>
    <input type="hidden" name="active" value="<ww:property value="active" />"/>
    <input type="hidden" name="resultsStart" value="<ww:property value="nextResultsStart" />"/>
    <input type="hidden" name="resultsPerPage" value="<ww:property value="resultsPerPage" />"/>
</form>

<form name="previous" method="post"
      action="<ww:url includeParams="none"><ww:param name="namespace" value="%{parameters.webwork.namespace}"/><ww:param name="action" value="%{parameters.webwork.action}"/><ww:param name="ID" value="%{parameters.applicationid}"/></ww:url>">
    <input type="hidden" name="directoryID" value="<ww:property value="directoryID" />"/>
    <input type="hidden" name="search" value="<ww:property value="search" />"/>
    <input type="hidden" name="active" value="<ww:property value="active" />"/>
    <input type="hidden" name="resultsStart" value="<ww:property value="previousResultsStart" />"/>
    <input type="hidden" name="resultsPerPage" value="<ww:property value="resultsPerPage" />"/>
</form>


<!-- This JS is called from the pages that use this component, typically by calling highlightSearchTerms() in body.onload -->
<script>
    <!-- Takes the value of a text node as HTML, and <strong>s any occurrances of "searchTerm", returning the -->
    <!--   HTML to be applied to the containing object. Used by highlightSearchResults() -->
    function highlightTerm(origHTML, searchTerm)
    {
        var newHTML = "";
        var prev = 0, next = 0;
        var searchLen = searchTerm.length;

        next = origHTML.toLowerCase().indexOf(searchTerm.toLowerCase());
        while (next >= 0)
        {
            newHTML += origHTML.substring(prev, next) + "<strong>" + origHTML.substring(next, next + searchLen) + "</strong>";

            prev = next + searchLen;
            next = origHTML.toLowerCase().indexOf(searchTerm.toLowerCase(), prev);
        }
        newHTML += origHTML.substring(prev);

        <!-- only overwrite on change -->
        if (newHTML != "")
        {
            return newHTML;
        }
        else
        {
            return origHTML;
        }
    }

    <!-- Highlights the searched-for term in the results -->
    function highlightSearchResults()
    {
        var searchTerm = "<ww:property value="search"/>";
        if (searchTerm != "")
        {
            var userDetailsTable = document.getElementById('user-details');
            var resultRows = userDetailsTable.getElementsByTagName('td');
            var len = resultRows.length;
            for (var i = 0; i < len; i++)
            {
                var cell = resultRows[i];
                var origHTML = "";
                if (cell.className == 'search-result-name')
                {
                    <!-- name cell - text is within an anchor -->
                    cell = cell.getElementsByTagName('a')[0];
                    cell.innerHTML = highlightTerm(cell.innerHTML, searchTerm);
                }
                else
                {
                    <!-- details cell - a <ul> containing <li>s -->
                    var lis = cell.getElementsByTagName('li');
                    for (var j = 0; j < lis.length; j++)
                    {
                        <!-- items are encased in a <em> -->
                        var items = lis[j].getElementsByTagName('em');
                        if (items.length > 0)
                        {
                            items[0].innerHTML = highlightTerm(items[0].innerHTML, searchTerm);
                        }
                    }
                }
            }
        }
    }
</script>
