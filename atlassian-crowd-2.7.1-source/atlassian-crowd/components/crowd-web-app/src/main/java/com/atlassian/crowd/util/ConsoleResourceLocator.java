package com.atlassian.crowd.util;

import com.atlassian.config.HomeLocator;
import com.atlassian.crowd.service.client.BaseResourceLocator;
import com.atlassian.crowd.service.client.ResourceLocator;

import java.io.File;

/**
 * Will find the location of the given propertyFileName based off a set of rules to
 * locate the given propertyFileName.
 * <p/>
 * Currently:
 * <ol>
 * <li>System resource</li>
 * <li>Crowd Home location</li>
 * <li>Classpath</li>
 * </ol>
 */
public class ConsoleResourceLocator extends BaseResourceLocator implements ResourceLocator
{
    private final HomeLocator homeLocator;

    public ConsoleResourceLocator(String propertyFileName, HomeLocator homeLocator)
    {
        super(propertyFileName);
        Assert.notNull(homeLocator);
        this.homeLocator = homeLocator;
        this.propertyFileLocation = findPropertyFileLocation();
        if (this.propertyFileLocation == null)
        {
            throw new IllegalArgumentException("Cannot locate Crowd Configuration file:" + getResourceName());            
        }
    }

    private String getResourceLocationFromCrowdHome()
    {
        String location = null;
        if (homeLocator != null)
        {
            String homeLocation = homeLocator.getHomePath();

            // Create what 'should' be the location of the resource
            location = new StringBuffer(32).append(homeLocation).append(File.separator).append(getResourceName()).toString();

            location = formatFileLocation(location);

        }
        return location;
    }

    private String findPropertyFileLocation()
    {
        String location = getResourceLocationFromSystemProperty();

        if (location == null)
        {
            location = getResourceLocationFromCrowdHome();
        }

        if (location == null)
        {
            location = getResourceLocationFromClassPath();
        }

        return location;
    }
}
