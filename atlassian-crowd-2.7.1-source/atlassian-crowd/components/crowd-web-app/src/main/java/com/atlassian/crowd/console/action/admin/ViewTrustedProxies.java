package com.atlassian.crowd.console.action.admin;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.manager.proxy.TrustedProxyManager;
import com.google.common.collect.Lists;

import java.util.Collections;
import java.util.List;

public class ViewTrustedProxies extends BaseAction
{
    private TrustedProxyManager trustedProxyManager;
    private List<String> addresses;

    @Override
    public String doDefault()
    {
        addresses = Lists.newArrayList(trustedProxyManager.getAddresses());
        Collections.sort(addresses);
        return INPUT;
    }

    public void setAddresses(List addresses)
    {
        this.addresses = addresses;
    }

    public List getAddresses()
    {
        return addresses;
    }
    
    public void setTrustedProxyManager(TrustedProxyManager trustedProxyManager)
    {
        this.trustedProxyManager = trustedProxyManager;
    }

}
