package com.atlassian.crowd.console.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.atlassian.config.bootstrap.BootstrapException;
import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.crowd.console.event.BootstrapContextInitialisedEvent;
import com.atlassian.crowd.plugin.descriptors.webwork.PluginAwareConfiguration;
import com.atlassian.crowd.util.DependencyVerifier;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.config.JohnsonConfig;
import com.atlassian.johnson.event.Event;
import com.atlassian.plugin.servlet.ServletModuleManager;
import com.atlassian.plugin.servlet.util.ServletContextServletModuleManagerAccessor;

import com.opensymphony.xwork.config.Configuration;
import com.opensymphony.xwork.config.ConfigurationManager;

import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Represents basic added dependencies for the initialization of the web-app context for Crowd.
 * <p/>
 * If the context can be initialized then Crowd has enough resources at its disposal to proceed with a
 * bootstrapManager (see CrowdContextLoaderListener)
 */
public class BootstrapLoaderListener implements ServletContextListener
{
    private static final Logger LOG = LoggerFactory.getLogger(BootstrapLoaderListener.class);
    protected static final Logger STARTUP_LOG = LoggerFactory.getLogger("com.atlassian.crowd.startup");

    public void contextInitialized(ServletContextEvent event)
    {
        final ServletContext servletContext = event.getServletContext();

        try
        {
            setStartupTime(servletContext);

            // verify application server dependencies
            DependencyVerifier dependencyVerifier = new DependencyVerifier("appserver-dependencies.xml");
            dependencyVerifier.verifyDependencies();

            // set the plugin-aware action configuration for XWork
            Configuration xworkConfig = new PluginAwareConfiguration();
            xworkConfig.reload();
            ConfigurationManager.setConfiguration(xworkConfig);

            initialiseBootstrapContext(servletContext);

            if (LogManager.getRootLogger().getLevel().equals(Level.DEBUG) && !"true".equals(System.getProperty("crowd.ignore.debug.logging")))
            {
                STARTUP_LOG.error("***************************************************************************************************************");
                STARTUP_LOG.error("The root log4j logger is set to DEBUG level. This may cause Crowd to run slowly.");
                STARTUP_LOG.error("If you are running Crowd under JBoss, please read http://confluence.atlassian.com/x/PtAB");
                STARTUP_LOG.error("To disable this error message, start your appserver with the system property -Dcrowd.ignore.debug.logging=true");
                STARTUP_LOG.error("***************************************************************************************************************");
            }
        }
        catch (Exception e)
        {
            // if bootstrap could not be configured then put johnson to use
            JohnsonEventContainer agentJohnson = Johnson.getEventContainer(event.getServletContext());
            JohnsonConfig johnsonConfig = Johnson.getConfig();

            agentJohnson.addEvent(new Event(johnsonConfig.getEventType("bootstrap"),
                    "An error was encountered while starting Crowd, please consult the log files",
                    johnsonConfig.getEventLevel("fatal")));

            LOG.error("An error was encountered while starting Crowd (see below): \n" + e.getMessage(), e);

            // an error in bootstrap is an error big enough to stop the context from coming up
            throw new RuntimeException(e);
        }
    }

    private void initialiseBootstrapContext(ServletContext servletContext) throws BootstrapException
    {
        // wire up the bootstrap beans
        ApplicationContext bootstrapContext = new ClassPathXmlApplicationContext(new String[]{"applicationContext-CrowdBootstrap.xml"});
        BootstrapUtils.init(bootstrapContext, servletContext);

        // Terrible way to setup the servlet module manager for plugins... see justin for an explanation.
        ServletContextServletModuleManagerAccessor.setServletModuleManager(servletContext, (ServletModuleManager) bootstrapContext.getBean("servletModuleManager"));

        com.atlassian.config.util.BootstrapUtils.getBootstrapContext().publishEvent(new BootstrapContextInitialisedEvent(this));
    }

    public void contextDestroyed(ServletContextEvent event)
    {
        STARTUP_LOG.info("Stopping Crowd");
        SLF4JBridgeHandler.uninstall();
        com.atlassian.config.util.BootstrapUtils.closeContext();
    }

    /**
     * Stores the system startup time in the context so that system uptime can be computed and displayed on system info
     * page
     *
     * @param context context to add attribute too
     */
    private void setStartupTime(ServletContext context)
    {
        if (context != null)
        {
            context.setAttribute("crowd_startup_time", new Long(System.currentTimeMillis()));
        }
    }
}