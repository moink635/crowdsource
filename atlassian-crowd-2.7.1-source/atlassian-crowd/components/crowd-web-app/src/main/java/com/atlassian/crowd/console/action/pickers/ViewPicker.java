package com.atlassian.crowd.console.action.pickers;

import com.atlassian.crowd.console.action.BaseAction;

public class ViewPicker extends BaseAction
{
    private String initialMessage;

    private int resultsPerPage = 100;

    public int getResultsPerPage()
    {
        return resultsPerPage;
    }

    public void setResultsPerPage(int resultsPerPage)
    {
        this.resultsPerPage = resultsPerPage;
    }

    public String getInitialMessage()
    {
        return initialMessage;
    }

    public void setInitialMessage(String initialMessage)
    {
        this.initialMessage = initialMessage;
    }
}
