package com.atlassian.crowd.console.action.application;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryInstantiationException;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.interceptor.SessionAware;

public class AddApplicationDirectoryDetails extends BaseAction implements SessionAware
{
    private DirectoryInstanceLoader directoryInstanceLoader;

    private List<DirectoryDetails> directories;

    private String name;
    private List<Long> directoryIds;

    private Map session;
    private static final String SELECTEDDIRECTORIES_REQUEST_PARAM = "selecteddirectories";

    private final Function<Directory, DirectoryDetails> getDirectoryForDisplay = new Function<Directory, DirectoryDetails>()
    {
        @Override
        public DirectoryDetails apply(Directory input)
        {
            String implementationDescriptiveName;

            try
            {
                implementationDescriptiveName = directoryInstanceLoader.getDirectory(input).getDescriptiveName();
            }
            catch (DirectoryInstantiationException e)
            {
                implementationDescriptiveName = null;
            }

            return new DirectoryDetails(input.getId(),
                    input.getName(),
                    implementationDescriptiveName);
        }
    };

    @Override
    public String execute()
    {
        ApplicationConfiguration configuration = getConfiguration();

        if (configuration != null)
        {
            name = configuration.getName();
            directoryIds = configuration.getDirectoryids();

        }
        else
        {
            return "start";
        }

        return INPUT;
    }

    public String completeStep()
    {
        String[] parameterValues = ServletActionContext.getRequest().getParameterValues(SELECTEDDIRECTORIES_REQUEST_PARAM);
        doValidation(parameterValues);

        if (hasErrors())
        {
            return ERROR;
        }

        List<Long> newDirectoryIds = new ArrayList<Long>(parameterValues.length);

        for (String parameterValue : parameterValues)
        {
            newDirectoryIds.add(new Long(parameterValue));
        }

        getConfiguration().setDirectoryIds(newDirectoryIds);

        return "authorisationdetails";
    }

    public void doValidation(String[] directories)
    {
        // You must select at least one directory
        if (directories == null || directories.length == 0)
        {
            addActionError(getText("application.directories.invalid", Arrays.asList(getConfiguration().getName())));
        }
    }

    public List<DirectoryDetails> getDirectories()
    {
        if (directories == null)
        {
            directories = ImmutableList.copyOf(Iterables.transform(directoryManager.findAllDirectories(), getDirectoryForDisplay));
        }
        return directories;
    }

    public ApplicationConfiguration getConfiguration()
    {
        return (ApplicationConfiguration) session.get(AddApplicationDetails.APPLICATION_SESSION_CONFIGURATION_SETUP);
    }

    public String getName()
    {
        return name;
    }

    public boolean isSelectedDirectory(Long id)
    {
        boolean selected = false;
        if (directoryIds != null)
        {
            selected = directoryIds.contains(id);
        }

        return selected;
    }

    @Override
    public void setSession(Map session)
    {
        this.session = session;
    }

    public void setDirectoryInstanceLoader(DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }

    public static class DirectoryDetails
    {
        private final Long id;
        private final String name;
        private final String implementationDescriptiveName;

        DirectoryDetails(Long id, String name, String implementationDescriptiveName)
        {
            this.id = id;
            this.name = name;
            this.implementationDescriptiveName = implementationDescriptiveName;
        }

        public Long getId()
        {
            return id;
        }

        public String getName()
        {
            return name;
        }

        public String getImplementationDescriptiveName()
        {
            return implementationDescriptiveName;
        }
    }
}
