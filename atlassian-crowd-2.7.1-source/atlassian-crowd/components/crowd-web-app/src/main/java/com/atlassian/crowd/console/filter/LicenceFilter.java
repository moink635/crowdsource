package com.atlassian.crowd.console.filter;

import com.atlassian.crowd.manager.license.CrowdLicenseManager;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.atlassian.plugin.servlet.util.DefaultPathMapper;
import com.atlassian.plugin.servlet.util.PathMapper;

import com.opensymphony.webwork.RequestUtils;
import org.apache.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * A Filter to check if the license is valid.
 * <p/>
 * Redirect to an update license page if it is not.
 */
public class LicenceFilter extends OncePerRequestFilter
{
    private static final Logger log = Logger.getLogger(LicenceFilter.class);

    private PathMapper ignorePaths;

    private CrowdLicenseManager crowdLicenseManager;

    protected static final String UPDATE_LICENSE_DEFAULT_PATH = "/console/license.action";

    public void afterPropertiesSet() throws ServletException
    {
        ignorePaths = new DefaultPathMapper();
        ignorePaths.put("/console/license*", "/console/license*");
        ignorePaths.put("/services/*", "/services/*");
        ignorePaths.put("/console/error*", "/console/error*");
        ignorePaths.put("/console/style*", "/console/style*");
        ignorePaths.put("/console/images*", "/console/images*");
        ignorePaths.put("/console/setup/*", "/console/setup/*");

        super.afterPropertiesSet();
    }

    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException
    {
        final CrowdLicense license = getCrowdLicenseManager().getLicense();
        if (!getCrowdLicenseManager().isLicenseValid(license))
        {
            log.info("No valid license found.");
            httpServletResponse.sendRedirect(httpServletRequest.getContextPath() + UPDATE_LICENSE_DEFAULT_PATH);
        }
        else
        {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
    }

    protected boolean shouldNotFilter(HttpServletRequest httpServletRequest) throws ServletException
    {
        String uri = RequestUtils.getServletPath(httpServletRequest);
        return ignoreUri(uri);
    }

    private boolean ignoreUri(String uri)
    {
        return (ignorePaths.get(uri) != null);
    }

    ///CLOVER:OFF
    public CrowdLicenseManager getCrowdLicenseManager()
    {
        return crowdLicenseManager;
    }

    public void setCrowdLicenseManager(CrowdLicenseManager crowdLicenseManager)
    {
        this.crowdLicenseManager = crowdLicenseManager;
    }
}