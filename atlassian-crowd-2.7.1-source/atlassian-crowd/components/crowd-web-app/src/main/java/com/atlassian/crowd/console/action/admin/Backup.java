package com.atlassian.crowd.console.action.admin;

import com.atlassian.core.util.DateUtils;
import com.atlassian.core.util.PairType;
import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.manager.backup.BackupManager;
import com.atlassian.crowd.manager.backup.BackupScheduler;
import com.atlassian.crowd.manager.backup.BackupSummary;
import com.atlassian.crowd.manager.backup.ScheduledBackupException;
import com.atlassian.crowd.migration.ExportException;
import com.atlassian.crowd.migration.XmlMigrationManager;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalTime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class Backup extends BaseAction
{
    private static final Logger logger = Logger.getLogger(Backup.class);

    private BackupManager backupManager;
    private BackupScheduler backupScheduler;
    private XmlMigrationManager xmlMigrationManager;

    private String exportFileName;
    private String importFilePath;
    private String importResponseMessage;
    private String exportResponseMessage;

    private boolean resetDomain;
    private boolean enableScheduleBackups;
    private int scheduledBackupTime;

    @Override
    public String doDefault() throws Exception
    {
        loadDefaultsScheduledBackup();
        loadDefaultsManualBackup();

        return INPUT;
    }

    private void loadDefaultsManualBackup()
    {
        resetDomain = true;
        exportFileName = backupManager.generateManualBackupFileName();
    }

    private void loadDefaultsScheduledBackup()
    {
        enableScheduleBackups = backupScheduler.isEnabled();
        scheduledBackupTime = backupScheduler.getHourToRun();
    }

    private void doImportValidation()
    {
        if (StringUtils.isBlank(importFilePath))
        {
            addFieldError("importFilePath", getText("administration.restore.filePath.error"));
        }

        List<String> currentlySynchronisingDirectoryNames = getSynchronisingDirectoryNames();
        if (!currentlySynchronisingDirectoryNames.isEmpty())
        {
            String directoryNames = Joiner.on(", ").join(currentlySynchronisingDirectoryNames);
            addActionError(getText("administration.restore.directory.synchronising.error", Arrays.asList(directoryNames)));
        }
    }

    private void doExportValidation()
    {
        if (StringUtils.isBlank(exportFileName) || exportFileName.contains("/") || exportFileName.contains("\\"))
        {
            addFieldError("exportFileName", getText("administration.backup.fileName.error"));
        }
    }

    @RequireSecurityToken(true)
    public String doExport() throws Exception
    {
        loadDefaultsScheduledBackup();
        doExportValidation();

        if (!hasErrors())
        {
            // export to xml
            long timeTaken;
            try
            {
                timeTaken = backupManager.backup(exportFileName, resetDomain);
            }
            catch (ExportException e)
            {
                addActionError(getText("administration.backup.error", Arrays.asList(e.getMessage())));
                logger.error(e.getMessage(), e);
                return INPUT;
            }

            // notify completion with an indication of time
            DateUtils dateUtils = new DateUtils(ResourceBundle.getBundle(BaseAction.class.getName()));
            setExportResponseMessage(getText("administration.backup.complete", ImmutableList.of(backupManager.getBackupFileFullPath(exportFileName), dateUtils.formatDurationPretty(timeTaken / 1000))));
        }
        else
        {
            return INPUT;
        }

        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doImport()
    {
        doImportValidation();

        if (hasErrors())
        {
            return ERROR;
        }
        else
        {
            try
            {
                long timeTaken = xmlMigrationManager.importXml(importFilePath);
                // notify completion with an indication of time
                DateUtils dateUtils = new DateUtils(ResourceBundle.getBundle(BaseAction.class.getName()));
                setImportResponseMessage(getText("administration.restore.complete", ImmutableList.of(dateUtils.formatDurationPretty(timeTaken / 1000))));
            }
            catch (Exception e)
            {
                // An error occured importing the data, report this error to the user
                logger.error("Failed Import: " + e.getMessage(), e);
                addActionError(e);
                return ERROR;
            }
        }

        return LOGIN;
    }

    @RequireSecurityToken(true)
    public String doScheduledBackup()
    {
        loadDefaultsManualBackup();

        try
        {
            if (enableScheduleBackups)
            {
                backupScheduler.enable();
            }
            else
            {
                backupScheduler.disable();
            }
            backupScheduler.setHourToRun(scheduledBackupTime);
        }
        catch (ScheduledBackupException e)
        {
            logger.error("Error while configuring automated backups: " + e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }

        return SUCCESS;
    }

    /**
     * Checks with directory manager for any directories that are currently synchronising
     * @return A list of the names of directories that are currently synchronising.
     * If no directories are synchronising returns an empty list
     */
    private List<String> getSynchronisingDirectoryNames()
    {
        List<Directory> allDirectories = directoryManager.findAllDirectories();
        List<String> currentlySynchronisingDirectories = new ArrayList<String>();

        for (Directory directory : allDirectories)
        {
            try
            {
                if (directoryManager.isSynchronising(directory.getId()))
                {
                    currentlySynchronisingDirectories.add(directory.getName());
                }
            }
            catch (DirectoryInstantiationException e)
            {
                throw new RuntimeException(e);
            }
            catch (DirectoryNotFoundException e)
            {
                throw new RuntimeException(e);
            }
        }
        return currentlySynchronisingDirectories;
    }

    ///CLOVER:OFF
    public XmlMigrationManager getXmlMigrationManager()
    {
        return xmlMigrationManager;
    }

    public void setXmlMigrationManager(XmlMigrationManager xmlMigrationManager)
    {
        this.xmlMigrationManager = xmlMigrationManager;
    }

    public String getExportFileName()
    {
        return exportFileName;
    }

    public void setExportFileName(String exportFileName)
    {
        this.exportFileName = exportFileName;
    }

    public String getImportFilePath()
    {
        return importFilePath;
    }

    public void setImportFilePath(String importFilePath)
    {
        this.importFilePath = importFilePath;
    }

    public String getImportResponseMessage()
    {
        return importResponseMessage;
    }

    public void setImportResponseMessage(String importResponseMessage)
    {
        this.importResponseMessage = importResponseMessage;
    }

    public String getExportResponseMessage()
    {
        return exportResponseMessage;
    }

    public void setExportResponseMessage(String exportResponseMessage)
    {
        this.exportResponseMessage = exportResponseMessage;
    }

    public boolean isResetDomain()
    {
        return resetDomain;
    }

    public void setResetDomain(boolean resetDomain)
    {
        this.resetDomain = resetDomain;
    }

    public void setBackupManager(BackupManager backupManager)
    {
        this.backupManager = backupManager;
    }

    public List<PairType> getScheduledBackupTimes()
    {
        List<PairType> scheduledBackupTimes = Lists.newArrayList();
        final String pattern = getText("administration.backup.automated.hourToRun.displayPattern");

        for (int i = 0; i < 24; i++)
        {
            final LocalTime hour = new LocalTime(i, 0);
            scheduledBackupTimes.add(new PairType(Integer.toString(i), hour.toString(pattern)));
        }

        return scheduledBackupTimes;
    }

    public void setBackupScheduler(BackupScheduler backupScheduler)
    {
        this.backupScheduler = backupScheduler;
    }

    public boolean isEnableScheduleBackups()
    {
        return enableScheduleBackups;
    }

    public void setEnableScheduleBackups(boolean enableScheduleBackups)
    {
        this.enableScheduleBackups = enableScheduleBackups;
    }

    public int getScheduledBackupTime()
    {
        return scheduledBackupTime;
    }

    public void setScheduledBackupTime(int scheduledBackupTime)
    {
        this.scheduledBackupTime = scheduledBackupTime;
    }

    public BackupSummary getBackupSummary()
    {
        return backupManager.getAutomatedBackupSummary();
    }

    public Date getServerCurrentDate()
    {
        return new Date();
    }
}
