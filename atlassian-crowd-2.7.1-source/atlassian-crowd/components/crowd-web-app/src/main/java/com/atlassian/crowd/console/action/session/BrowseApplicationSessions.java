package com.atlassian.crowd.console.action.session;

import com.atlassian.crowd.console.action.AbstractBrowser;
import com.atlassian.crowd.dao.token.SearchableTokenStorage;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.manager.token.SearchableTokenService;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.TokenTermKeys;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import static com.atlassian.crowd.model.token.Token.APPLICATION_TOKEN_DIRECTORY_ID;

public class BrowseApplicationSessions extends AbstractBrowser<Token>
{
    private static final Logger logger = Logger.getLogger(BrowseApplicationSessions.class);

    private String name;

    private SearchableTokenStorage tokenStorage;

    public String execute()
    {
        try
        {
            SearchRestriction restriction;
            PropertyRestriction<Long> applicationRestriction = Restriction.on(TokenTermKeys.DIRECTORY_ID).exactlyMatching(APPLICATION_TOKEN_DIRECTORY_ID);

            if (StringUtils.isNotBlank(name))
            {
                restriction = Combine.allOf(applicationRestriction, Restriction.on(TokenTermKeys.NAME).startingWith(name));
            }
            else
            {
                restriction = applicationRestriction;
            }

            EntityQuery<Token> query = QueryBuilder.queryFor(Token.class, EntityDescriptor.token())
                                            .with(restriction)
                                            .startingAt(resultsStart)
                                            .returningAtMost(resultsPerPage + 1);

            results = tokenStorage.search(query);
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return SUCCESS;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setTokenManager(SearchableTokenService tokenStorage)
    {
        this.tokenStorage = tokenStorage;
    }
}
