package com.atlassian.crowd.console.action.user;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.model.application.Application;

public class BaseUserAction extends BaseAction
{
    public String getRemoteUsername() throws InvalidUserException
    {
        return getRemoteUser().getName();
    }
}
