package com.atlassian.crowd.console.action.session;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.log4j.Logger;

public class RemoveSession extends BaseAction
{
    private static final Logger LOGGER = Logger.getLogger(RemoveSession.class);

    private String randomHash;

    @RequireSecurityToken(true)
    public String doRemovePrincipalSession()
    {
        try
        {
            tokenAuthenticationManager.invalidateToken(randomHash);
        }
        catch (Exception e)
        {
            addActionError(e);
            LOGGER.error(e.getMessage(), e);
        }

        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doRemoveApplicationSession()
    {
        try
        {
            tokenAuthenticationManager.invalidateToken(randomHash);
        }
        catch (Exception e)
        {
            addActionError(e);
            LOGGER.error(e.getMessage(), e);
        }

        return SUCCESS;
    }

    public String getRandomHash()
    {
        return randomHash;
    }

    public void setRandomHash(String randomHash)
    {
        this.randomHash = randomHash;
    }
}