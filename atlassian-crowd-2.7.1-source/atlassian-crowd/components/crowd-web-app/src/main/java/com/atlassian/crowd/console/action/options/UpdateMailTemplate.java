package com.atlassian.crowd.console.action.options;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class UpdateMailTemplate extends BaseAction
{
    private static final Logger logger = Logger.getLogger(UpdateMailTemplate.class);

    private String forgottenPasswordTemplate;
    private String forgottenUsernamesTemplate;

    private PropertyManager propertyManager;

    public String doDefault()
    {
        try
        {
            processMailTemplates();
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
        }

        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            doValidation();

            if (hasErrors())
            {
                return INPUT;
            }

            propertyManager.setProperty(Property.FORGOTTEN_PASSWORD_EMAIL_TEMPLATE, forgottenPasswordTemplate);
            propertyManager.setProperty(Property.FORGOTTEN_USERNAME_EMAIL_TEMPLATE, forgottenUsernamesTemplate);
        }
        catch (Exception e)
        {
            logger.error("An exception occured updating the mail template", e);
            addActionError(e);
        }

        return SUCCESS;
    }

    protected void doValidation()
    {
        if (StringUtils.isBlank(forgottenPasswordTemplate))
        {
            addFieldError("forgottenPasswordTemplate", getText("mailtemplate.template.invalid"));
        }

        if (StringUtils.isBlank(forgottenUsernamesTemplate))
        {
            addFieldError("forgottenUsernamesTemplate", getText("mailtemplate.template.invalid"));
        }
    }

    protected void processMailTemplates() throws PropertyManagerException, ObjectNotFoundException
    {
        forgottenPasswordTemplate = propertyManager.getProperty(Property.FORGOTTEN_PASSWORD_EMAIL_TEMPLATE);
        forgottenUsernamesTemplate = propertyManager.getProperty(Property.FORGOTTEN_USERNAME_EMAIL_TEMPLATE);
    }

    public String getForgottenPasswordTemplate()
    {
        return forgottenPasswordTemplate;
    }

    public void setForgottenPasswordTemplate(String template)
    {
        this.forgottenPasswordTemplate = template;
    }

    public String getForgottenUsernamesTemplate()
    {
        return forgottenUsernamesTemplate;
    }

    public void setForgottenUsernamesTemplate(String template)
    {
        this.forgottenUsernamesTemplate = template;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}
