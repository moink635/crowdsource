package com.atlassian.crowd.console.action.dataimport;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.xwork.RequireSecurityToken;

public class ImportJive extends ImportType
{
    private String url;
    private String driver;
    private String username;
    private String password;
    protected long groupImportCount = 0;
    protected long userImportCount = 0;
    protected long directoryID = -1;

    public String doDefault()
    {

        url = "jdbc:mysql://localhost/jive_software?autoReconnect=true&useUnicode=true";
        driver = "com.mysql.jdbc.Driver";
        username = "root";

        return INPUT;
    }

    private static final String GET_GROUPS = "SELECT groupID, name, description from jiveGroup order by name";

    private static final String GET_USERS = "SELECT userID, username, name, email FROM jiveUser order by username";

    private static final String GET_MEMBERSHIPS = "SELECT groupID from jiveGroupUser where userID = ?";

    @RequireSecurityToken(true)
    public String doUpdate()
    {

        try
        {

            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                return INPUT;
            }
            Connection conn = null;
            PreparedStatement pstmt = null;
            ResultSet usersRs = null;
            ResultSet groupsRs = null;

            groupImportCount = 0;
            userImportCount = 0;

            try
            {
                Class.forName(driver);

                conn = DriverManager.getConnection(url, username, password);
            }
            catch (Exception e)
            {
                addActionError(e);
                logger.error(e.getMessage(), e);

                return INPUT;
            }

            try
            {
                // import groups
                pstmt = conn.prepareStatement(GET_GROUPS);
                groupsRs = pstmt.executeQuery();

                Map groupIDMappings = importGroups(groupsRs);

                // import users
                pstmt = conn.prepareStatement(GET_USERS);
                usersRs = pstmt.executeQuery();

                importUsers(usersRs, conn, groupIDMappings);
            }
            catch (Exception e)
            {
                logger.error(e.getMessage(), e);
                addActionError(e);

                return INPUT;
            }
            finally
            {

                // close the connection objects here
                closeResultSet(usersRs);

                closeResultSet(groupsRs);

                closePreparedStatment(pstmt);

                closeConnection(conn);
            }

            return SUCCESS;
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
        }

        return INPUT;
    }

    protected void importUsers(ResultSet usersRs, Connection conn, Map groupMemberships) throws Exception
    {

        while (usersRs.next())
        {

            long userID = usersRs.getLong(1);
            String username = usersRs.getString(2);
            String fullname = usersRs.getString(3);
            String email = usersRs.getString(4);

            if (username != null && !username.equals(""))
            {
                // have the security server add it
                UserTemplate principalT = new UserTemplate(username, directoryID);

                principalT.setActive(true);

                principalT.setEmailAddress(email);

                PasswordCredential credential = PasswordCredential.NONE;

                PreparedStatement membershipsPStmt = null;
                ResultSet membershipsRs = null;

                User principal = null;
                try
                {
                    logger.info("Attemping to import: " + principalT.getName());

                    try
                    {
                        // see if the principal already exist
                        principal = directoryManager.findUserByName(directoryID, principalT.getName());
                    }
                    catch (UserNotFoundException e)
                    {
                        // if the principal doesn't already exist, add them
                        principal = directoryManager.addUser(directoryID, principalT, credential);
                    }

                    // add the principal to the groups they are a member of in JIVE
                    membershipsPStmt = conn.prepareStatement(GET_MEMBERSHIPS);
                    membershipsPStmt.setLong(1, userID);
                    membershipsRs = membershipsPStmt.executeQuery();

                    while (membershipsRs.next())
                    {
                        long groupId = membershipsRs.getLong(1);
                        String groupName = (String) groupMemberships.get(new Long(groupId));

                        directoryManager.addUserToGroup(directoryID, principal.getName(), groupName);
                    }

                    logger.info("Successfully imported: " + principal.getName());

                    userImportCount++;
                }
                catch (InvalidCredentialException e)
                {
                    addActionError(getText("directory.importjiveerror.invalid") + " " + username);
                }
                finally
                {

                    closeResultSet(membershipsRs);

                    closePreparedStatment(membershipsPStmt);
                }
            }
        }
    }

    protected Map importGroups(ResultSet groupsRs) throws Exception
    {

        Map groupMappings = new HashMap();

        while (groupsRs.next())
        {

            GroupTemplate group = new GroupTemplate(groupsRs.getString(2), directoryID, GroupType.GROUP);
            group.setActive(true);
            group.setDescription(groupsRs.getString(3));

            try
            {
                directoryManager.findGroupByName(directoryID, group.getName());
            }
            catch (GroupNotFoundException e)
            {
                // if group does not already exist
                directoryManager.addGroup(directoryID, group);
            }

            groupMappings.put(new Long(groupsRs.getLong(1)), group.getName());

            groupImportCount++;
        }

        return groupMappings;
    }

    public Connection getConnection() throws SQLException
    {
        try
        {
            Class.forName(driver);
        }
        catch (Exception e)
        {
            logger.info(e.getMessage(), e);
            addActionError(getText(e.getMessage()));

            throw new SQLException(getText("invalid.classfile"));
        }

        return DriverManager.getConnection(driver, username, password);
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getDriver()
    {
        return driver;
    }

    public void setDriver(String driver)
    {
        this.driver = driver;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    protected void doValidation()
    {
        if (directoryID == -1)
        {
            addFieldError("directoryID", getText("dataimport.directoryid.invalid"));

        }
    }

    public List getDirectories()
    {
        List directories = new ArrayList();

        try
        {

            DirectoryImpl emptyDirectory = new DirectoryImpl();

            directories.add(emptyDirectory);
            directories.addAll(directoryManager.findAllDirectories());

        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
        }

        return directories;

    }

    public long getGroupImportCount()
    {
        return groupImportCount;
    }

    public long getUserImportCount()
    {
        return userImportCount;
    }

    protected static void closeResultSet(ResultSet rs)
    {
        if (rs != null)
        {
            try
            {
                rs.close();
            }
            catch (SQLException e)
            {
                // ignore
            }
        }
    }

    protected static void closePreparedStatment(PreparedStatement stmt)
    {
        if (stmt != null)
        {
            try
            {
                stmt.close();
            }
            catch (SQLException e)
            {
                // ignore
            }
        }
    }

    protected static void closeConnection(Connection conn)
    {
        if (conn != null)
        {
            try
            {
                conn.close();
            }
            catch (SQLException e)
            {
                // ignore
            }
        }
    }

    public long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(long directoryID)
    {
        this.directoryID = directoryID;
    }
}
