package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Update Action specifically for Custom Directory Attributes
 */
public class UpdateCustomAttributes extends BaseAction
{
    private long ID = -1;
    private Directory directory;

    private String attributes[];
    private String attribute;
    private String value;

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        DirectoryImpl updateDirectory = new DirectoryImpl(getDirectory());

        //TODO: fix this up since we only have a 1-1 for directory and application
        for (String attribute : attributes)
        {
            List<String> modifiedValues = getAttributeValues(attribute);

            String newValue = modifiedValues.isEmpty() ? "" : modifiedValues.get(0);
            updateDirectory.setAttribute(attribute, newValue);
        }

        try
        {
            directoryManager.updateDirectory(updateDirectory);
        }
        catch (DirectoryNotFoundException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
        catch (RuntimeException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }

        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doAddAttribute()
    {

        doValidateAttribute();

        if (hasErrors())
        {
            return ERROR;
        }

        try
        {
            DirectoryImpl updatedDirectory = new DirectoryImpl(getDirectory());

            updatedDirectory.setAttribute(attribute, value);

            directoryManager.updateDirectory(updatedDirectory);

            // Reset the form fields
            setAttribute("");
            setValue("");
        }
        catch (DirectoryNotFoundException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
        catch (RuntimeException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }

        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doRemoveAttribute()
    {
        try
        {
            DirectoryImpl updateDirectory = new DirectoryImpl(getDirectory());
            updateDirectory.removeAttribute(attribute);

            directoryManager.updateDirectory(updateDirectory);

            setAttribute("");
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
            return ERROR;
        }

        return SUCCESS;
    }

    private void doValidateAttribute()
    {
        if (StringUtils.isEmpty(attribute) || StringUtils.isEmpty(value))
        {
            addActionError(getText("directorycustom.attribute.invalid"));
        }
    }

    private List<String> getAttributeValues(String attribute)
    {

        // find out how many values there are
        int parameterCount = getParameterCount(attribute);

        // construct the values
        List<String> values = new ArrayList<String>();
        for (int i = 0; i < parameterCount; i++)
        {
            String value = ServletActionContext.getRequest().getParameter(attribute + (i + 1));
            if (StringUtils.isNotEmpty(value))
            {
                values.add(value);
            }
        }

        return values;
    }

    private int getParameterCount(String attribute)
    {
        int count = 0;

        logger.debug("searching attribute: " + attribute);

        Enumeration parameterNames = ServletActionContext.getRequest().getParameterNames();

        while (parameterNames.hasMoreElements())
        {
            String parameterName = (String) parameterNames.nextElement();
            if (parameterName.matches("(" + attribute + "){1}\\d+"))
            {
                String number = parameterName.substring(attribute.length());

                // match the larger value
                int current = Integer.parseInt(number);
                if (current > count)
                {
                    count = current;
                }
            }
        }

        return count;
    }

    public String[] getAttributes()
    {
        return attributes;
    }

    public void setAttributes(String[] attributes)
    {
        this.attributes = attributes;
    }

    public String getAttribute()
    {
        return attribute;
    }

    public void setAttribute(String attribute)
    {
        this.attribute = attribute;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public long getID()
    {
        return ID;
    }

    public void setID(long ID)
    {
        this.ID = ID;
    }

    public Directory getDirectory()
    {
        if (directory == null && ID != -1)
        {
            try
            {
                directory = directoryManager.findDirectoryById(ID);
            }
            catch (DirectoryNotFoundException e)
            {
                addActionError(e);
            }
        }

        return directory;
    }
}
