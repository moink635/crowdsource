/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.principal;

import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.atlassian.crowd.exception.OperationFailedException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class UpdateGroups extends ViewPrincipal
{
    private static final Logger logger = Logger.getLogger(UpdateGroups.class);

    private String entityName;
    private List<String> selectedEntityNames;

    @RequireSecurityToken(true)
    public String doAddGroups()
    {
        try
        {
            for (String groupName : selectedEntityNames)
            {
                // add user (entityName) to group (groupName)
                directoryManager.addUserToGroup(directoryID, entityName, groupName);
            }
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }
        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doRemoveGroups()
    {
        try
        {
            // Check if the selected groups to remove will remove ALL groups that give admin rights to the currently logged in Crowd console admin
            if (adminGroupChecker.isRemovingCrowdConsoleAdminMembership(getRemoteUser().getName(), getRemoteUser().getDirectoryId(), entityName, directoryID))
            {
                Set<String> unsafeAdminGroups = adminGroupChecker.getUnsafeAdminGroups(entityName, directoryID, selectedEntityNames);
                if (!unsafeAdminGroups.isEmpty())
                {
                    unremovedGroups = StringUtils.join(unsafeAdminGroups, ", ");
                    preventingLockout = selectedEntityNames.removeAll(unsafeAdminGroups);
                }
            }

            List<String> failedGroups = new LinkedList<String>();
            for (String groupName : selectedEntityNames)
            {
                try
                {
                    // remove user (entityName) from group (groupName)
                    directoryManager.removeUserFromGroup(directoryID, entityName, groupName);
                }
                catch (Exception e)
                {
                    logger.error(e.getMessage(), e);
                    failedGroups.add(groupName);
                }
            }
            unremovedGroups = StringUtils.join(failedGroups, ", ");
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }
        return SUCCESS;
    }

    public boolean isUpdateSuccessful()
    {
        return !preventingLockout && StringUtils.isEmpty(unremovedGroups);
    }

    public String getEntityName()
    {
        return entityName;
    }

    public void setEntityName(String entityName)
    {
        this.entityName = entityName;
    }

    public List<String> getSelectedEntityNames()
    {
        return selectedEntityNames;
    }

    public void setSelectedEntityNames(List<String> selectedEntityNames)
    {
        this.selectedEntityNames = selectedEntityNames;
    }
}