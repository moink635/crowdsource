package com.atlassian.crowd.console.tags;

import com.opensymphony.webwork.components.Component;
import com.opensymphony.xwork.util.OgnlValueStack;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @see com.opensymphony.webwork.components.Text
 */
public class TextTag extends com.opensymphony.webwork.views.jsp.TextTag
{
    private boolean escape = true;

    public Component getBean(OgnlValueStack stack, HttpServletRequest req, HttpServletResponse res)
    {
        return new Text(stack);
    }

    @Override
    protected void populateParams()
    {
        super.populateParams();

        Text tag = (Text) component;
        tag.setEscape(escape);
    }

    /**
     * Defines whether or not parameters should be HTML encoded
     *
     * @param escape
     */
    public void setEscape(boolean escape)
    {
        this.escape = escape;
    }
}