package com.atlassian.crowd.console.filter;

import com.atlassian.config.util.BootstrapUtils;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * A springified DelagatingFilterProxy which does not
 * attempt to wire up (and hence run) the filter unless
 * Crowd has been setup successfully.
 *
 * See CrowdContextLoaderListener and BootstrappedContextLoaderListener
 * for more details.
 */

public class CrowdDelegatingFilterProxy extends DelegatingFilterProxy
{

    /**
     * Only doFilter if the BootstrapManager asserts that the
     * setup process is complete.
     *
     * @param request servlet request.
     * @param response servlet response.
     * @param filterChain filter chain.
     * @throws ServletException error occured.
     * @throws IOException error occured.
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws ServletException, IOException
    {
        if (BootstrapUtils.getBootstrapManager().isSetupComplete())
        {
            super.doFilter(request, response, filterChain);
        }
        else
        {
            filterChain.doFilter(request, response);
        }
    }
}
