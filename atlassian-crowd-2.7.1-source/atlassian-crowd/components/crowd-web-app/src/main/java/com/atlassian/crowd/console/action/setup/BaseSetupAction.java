package com.atlassian.crowd.console.action.setup;

import com.atlassian.config.setup.SetupPersister;
import com.atlassian.crowd.console.action.BaseAction;
import org.apache.log4j.Logger;

public abstract class BaseSetupAction extends BaseAction
{
    private static final Logger logger = Logger.getLogger(BaseSetupAction.class);

    public static final String SELECT_SETUP_STEP = "select-setup-step";

    private SetupPersister setupPersister;

    public void setSetupPersister(SetupPersister setupPersister)
    {
        this.setupPersister = setupPersister;
    }

    public SetupPersister getSetupPersister()
    {
        if (setupPersister == null)
        {
            setupPersister = getBootstrapManager().getSetupPersister();
        }

        return setupPersister;
    }

    public boolean isAtCorrectStep()
    {
        return getSetupPersister().getCurrentStep().equals(getStepName());
    }

    // standard update behaviour is to display an error if trying to update a step the user is not up to
    public String doUpdate()
    {
        if (!isEverythingOk())
        {
            return SELECT_SETUP_STEP;
        }
        if (!isAtCorrectStep())
        {
            return ERROR;
        }
        else
        {
            return SUCCESS;
        }
    }

    // standard default behaviour is redirect the user to the correct setup page
    public String doDefault()
    {
        if (!isEverythingOk() || !isAtCorrectStep())
        {
            return SELECT_SETUP_STEP;
        }
        else
        {
            return SUCCESS;
        }
    }

    public boolean isEverythingOk()
    {
        return isJdk16() && isServlet24() && isApplicationHomeOk();
    }

    public boolean isJdk15()
    {
        return isClassFound("java.util.Formattable");
    }

    public boolean isJdk16()
    {
        return isClassFound("java.util.Deque");
    }

    public boolean isServlet24()
    {
        return isClassFound("javax.servlet.ServletRequestListener");
    }

    boolean isClassFound(String className)
    {
        Class clazz = null;

        try
        {
            clazz = Class.forName(className);
        }
        catch (ClassNotFoundException e1)
        {
            try
            {
                clazz = Thread.currentThread().getContextClassLoader().loadClass(className);
            }
            catch (ClassNotFoundException e2)
            {
                try
                {
                    clazz = getClass().getClassLoader().loadClass(className);
                }
                catch (ClassNotFoundException e)
                {
                    logger.info("Class not found " + className + ": " + e, e);
                }
            }
        }

        return clazz != null;
    }

    public boolean isApplicationHomeOk()
    {
        return getBootstrapManager().isApplicationHomeValid();
    }

    public abstract String getStepName();
}
