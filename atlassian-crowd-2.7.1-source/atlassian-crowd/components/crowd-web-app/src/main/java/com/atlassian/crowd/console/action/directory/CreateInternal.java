package com.atlassian.crowd.console.action.directory;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.atlassian.core.util.PairType;
import com.atlassian.crowd.directory.AbstractInternalDirectory;
import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.google.common.collect.Ordering;
import com.opensymphony.util.TextUtils;

import org.apache.log4j.Logger;

public class CreateInternal extends CreateDirectory
{
    private static final Logger logger = Logger.getLogger(CreateDirectory.class);

    private String passwordRegex = "";
    private String passwordComplexityMessage = "";
    private long passwordMaxAttempts = 0;
    private long passwordMaxChangeTime = 0;
    private long passwordHistoryCount = 0;

    /* This is presented as the default for new internal directories */
    private String userEncryptionMethod = PasswordEncoderFactory.ATLASSIAN_SECURITY_ENCODER;
    private PasswordEncoderFactory passwordEncoderFactory;
    private boolean useNestedGroups;

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                return INPUT;
            }

            DirectoryImpl directoryToCreate = new DirectoryImpl(name, DirectoryType.INTERNAL, InternalDirectory.class.getName());
            directoryToCreate.setActive(active);
            directoryToCreate.setDescription(description);

            directoryToCreate.setAttribute(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_REGEX, passwordRegex);
            directoryToCreate.setAttribute(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_COMPLEXITY_MESSAGE, passwordComplexityMessage);
            directoryToCreate.setAttribute(InternalDirectory.ATTRIBUTE_PASSWORD_MAX_ATTEMPTS, Long.toString(passwordMaxAttempts));
            directoryToCreate.setAttribute(InternalDirectory.ATTRIBUTE_PASSWORD_MAX_CHANGE_TIME, Long.toString(passwordMaxChangeTime));
            directoryToCreate.setAttribute(InternalDirectory.ATTRIBUTE_PASSWORD_HISTORY_COUNT, Long.toString(passwordHistoryCount));
            directoryToCreate.setAttribute(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, userEncryptionMethod);
            directoryToCreate.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS, Boolean.toString(useNestedGroups));

            directoryToCreate.setAllowedOperations(permissions);

            ID = directoryManager.addDirectory(directoryToCreate).getId();

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    protected void doValidation()
    {
        if (name == null || name.equals(""))
        {
            addFieldError("name", getText("directoryinternal.name.invalid"));
        }
        else
        {
            try
            {
                directoryManager.findDirectoryByName(name);

                addFieldError("name", getText("invalid.namealreadyexist"));
            }
            catch (Exception e)
            {
                // ingore
            }
        }

        try
        {
            if (passwordRegex != null)
            {
                Pattern.compile(passwordRegex);
            }
        }
        catch (PatternSyntaxException e)
        {
            addFieldError("passwordRegex", e.getMessage());
        }

        if (passwordMaxChangeTime < 0)
        {
            addFieldError("passwordMaxChangeTime", getText("directoryinternal.passwordmaxchangetime.invalid"));
        }

        if (passwordMaxAttempts < 0)
        {
            addFieldError("passwordMaxAttempts", getText("directoryinternal.passwordmaxattempts.invalid"));
        }

        if (passwordHistoryCount < 0)
        {
            addFieldError("passwordHistoryCount", getText("directoryinternal.passwordhistorycount.invalid"));
        }

        if (!TextUtils.stringSet(userEncryptionMethod))
        {
            addFieldError("userEncryptionMethod", getText("directoryinternal.userencryptionmethod.invalid"));
        }
    }

    ///CLOVE:OFF
    public String getPasswordRegex()
    {
        return passwordRegex;
    }

    public void setPasswordRegex(String passwordRegex)
    {
        this.passwordRegex = passwordRegex;
    }

    public long getPasswordMaxAttempts()
    {
        return passwordMaxAttempts;
    }

    public void setPasswordMaxAttempts(long passwordMaxAttempts)
    {
        this.passwordMaxAttempts = passwordMaxAttempts;
    }

    public long getPasswordMaxChangeTime()
    {
        return passwordMaxChangeTime;
    }

    public void setPasswordMaxChangeTime(long passwordMaxChangeTime)
    {
        this.passwordMaxChangeTime = passwordMaxChangeTime;
    }

    public long getPasswordHistoryCount()
    {
        return passwordHistoryCount;
    }

    public void setPasswordHistoryCount(long passwordHistoryCount)
    {
        this.passwordHistoryCount = passwordHistoryCount;
    }

    public String getUserEncryptionMethod()
    {
        return userEncryptionMethod;
    }

    public void setUserEncryptionMethod(String userEncryptionMethod)
    {
        this.userEncryptionMethod = userEncryptionMethod;
    }

    public List<PairType> getUserEncryptionMethods()
    {
        List<PairType> encoders = new ArrayList<PairType>();
        for (String encoder : Ordering.<String>natural().immutableSortedCopy(this.passwordEncoderFactory.getSupportedInternalEncoders()))
        {
            encoders.add(new PairType(encoder, encoder.toUpperCase()));
        }
        return encoders;
    }

    public PasswordEncoderFactory getPasswordEncoderFactory()
    {
        return passwordEncoderFactory;
    }

    public void setPasswordEncoderFactory(PasswordEncoderFactory passwordEncoderFactory)
    {
        this.passwordEncoderFactory = passwordEncoderFactory;
    }

    public boolean isUseNestedGroups()
    {
        return useNestedGroups;
    }

    public void setUseNestedGroups(final boolean useNestedGroups)
    {
        this.useNestedGroups = useNestedGroups;
    }

    public String getPasswordComplexityMessage()
    {
        return passwordComplexityMessage;
    }

    public void setPasswordComplexityMessage(String passwordComplexityMessage)
    {
        this.passwordComplexityMessage = passwordComplexityMessage;
    }
}