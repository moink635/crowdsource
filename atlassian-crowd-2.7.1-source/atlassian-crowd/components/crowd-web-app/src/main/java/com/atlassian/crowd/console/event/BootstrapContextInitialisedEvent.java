package com.atlassian.crowd.console.event;

import org.springframework.context.ApplicationEvent;

/**
 *  An event indicating that the bootstrap context has been initialised.
 */
public class BootstrapContextInitialisedEvent extends ApplicationEvent
{
    /**
     * Create a new ApplicationEvent.
     *
     * @param source the component that published the event (never <code>null</code>)
     */
    public BootstrapContextInitialisedEvent(Object source)
    {
        super(source);
    }
}