package com.atlassian.crowd.console.action.principal;

import com.atlassian.crowd.console.action.AbstractBrowser;
import com.atlassian.crowd.console.action.Searcher;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.util.SelectionUtils;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.apache.log4j.Logger;

import java.util.List;

public class BrowsePrincipals extends AbstractBrowser
{
    private static final Logger logger = Logger.getLogger(BrowsePrincipals.class);

    private String active;
    private String search;
    private long directoryID = -1;
    private List<Directory> directories;
    private DirectoryManager directoryManager;
    private Searcher<User> userSearcher;

    public String execute()
    {
        try
        {
            // select only directory if the list has only 1 result
            directories = directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS));
            if (directoryID == -1)
            {
                directoryID = SelectionUtils.getSelectedDirectory(directories);
            }
            else
            {
                SelectionUtils.saveSelectedDirectory(directoryID);
            }

            Boolean activeVal = null;
            if (active != null && active.length() > 0)
            {
                activeVal = Boolean.valueOf(active);
            }

            results = userSearcher.doSearchByDirectory(directoryID, activeVal, search, resultsStart, resultsPerPage);
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return SUCCESS;
    }

    public String getSearch()
    {
        return search;
    }

    public void setSearch(final String search)
    {
        this.search = search;
    }

    public String getActive()
    {
        return active;
    }

    public void setActive(String active)
    {
        this.active = active;
    }

    public long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(long directoryID)
    {
        this.directoryID = directoryID;
    }

    public List getDirectories()
    {
        return directories;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }

    public void setUserSearcher(Searcher userSearcher)
    {
        this.userSearcher = userSearcher;
    }

    public String getAliasForUser(String username)
    {
        // no aliases for users as the users are not tied to a particular application
        return null;
    }
}