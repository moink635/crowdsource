package com.atlassian.crowd.console.action.admin;

import com.atlassian.crowd.console.action.BaseAction;
import org.apache.log4j.Logger;

/**
 * User: Justin
 * Date: 26/02/2007
 */
public class Administer extends BaseAction
{
    private static final Logger logger = Logger.getLogger(Administer.class);

    public String doDefault() throws Exception
    {
        return SUCCESS;
    }

    ///CLOVER:OFF
}
