package com.atlassian.crowd.console.action.directory;

import java.util.Arrays;

import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.ServletActionContext;

import org.apache.commons.lang3.StringUtils;

public class UpdateRemoteCrowd extends ViewConnector
{
    protected boolean useNestedGroups;

    @Override
    public String doDefault() throws Exception
    {
        String nesting = getDirectory().getValue(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS);

        if (nesting == null)
        {
            useNestedGroups = false;
        }
        else
        {
            useNestedGroups = Boolean.parseBoolean(nesting);
        }

        return super.doDefault();
    }

    @RequireSecurityToken(true)
    public String execute()
    {
        //  Validate the inputs
        doValidation();

        // check for errors from the above validation
        if (hasErrors())
        {
            return ERROR;
        }

        DirectoryImpl updatedDirectory = new DirectoryImpl(getDirectory());

        updatedDirectory.setName(name);
        updatedDirectory.setDescription(directoryDescription);
        updatedDirectory.setActive(active);
        updatedDirectory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.toString(isCacheEnabled()));
        updatedDirectory.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS, Boolean.toString(useNestedGroups));

        try
        {
            directoryManager.updateDirectory(updatedDirectory);
        }
        catch (DirectoryNotFoundException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
        catch (RuntimeException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }

        ServletActionContext.getRequest().setAttribute("updateSuccessful", Boolean.TRUE);

        return SUCCESS;

    }

    private void doValidation()
    {
        if (StringUtils.isEmpty(name))
        {
            addFieldError("name", getText("directoryinternal.name.invalid"));
        }

        // do we have a unique directory name?
        if (!name.equals(getDirectory().getName()))
        {
            try
            {
                directoryManager.findDirectoryByName(name);

                addActionError(getText("directory.name.nonunique.invalid", Arrays.asList(name)));
            }
            catch (DirectoryNotFoundException e)
            {
                // we should be here!
            }
        }
    }

    public boolean isUseNestedGroups()
    {
        return useNestedGroups;
    }

    public void setUseNestedGroups(final boolean useNestedGroups)
    {
        this.useNestedGroups = useNestedGroups;
    }

}
