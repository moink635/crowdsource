package com.atlassian.crowd.console.action.directory;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.directory.AbstractInternalDirectory;
import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;

/**
 * Action that handles updating the configuration for an Internal Directory
 */
public class UpdateInternalConfiguration extends BaseAction
{
    private long ID = -1;

    private long passwordMaxAttempts = 0;

    private long passwordMaxChangeTime = 0;

    private long passwordHistoryCount = 0;

    private String passwordRegex = null;

    private String passwordComplexityMessage = "";

    private Directory directory;

    protected boolean useNestedGroups;

    public String execute() throws Exception
    {
        passwordHistoryCount = Long.parseLong(getDirectory().getValue(InternalDirectory.ATTRIBUTE_PASSWORD_HISTORY_COUNT));

        passwordMaxChangeTime = Long.parseLong(getDirectory().getValue(InternalDirectory.ATTRIBUTE_PASSWORD_MAX_CHANGE_TIME));

        passwordMaxAttempts = Long.parseLong(getDirectory().getValue(InternalDirectory.ATTRIBUTE_PASSWORD_MAX_ATTEMPTS));

        passwordRegex = getDirectory().getValue(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_REGEX);

        passwordComplexityMessage = getDirectory().getValue(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_COMPLEXITY_MESSAGE);

        String nesting = getDirectory().getValue(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS);
        if (nesting == null)
        {
            useNestedGroups = false;
        }
        else
        {
            useNestedGroups = Boolean.parseBoolean(nesting);
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        doValidation();

        if (hasErrors())
        {
            return ERROR;
        }

        try
        {
            DirectoryImpl updatedDirectory = new DirectoryImpl(getDirectory());

            updatedDirectory.setAttribute(InternalDirectory.ATTRIBUTE_PASSWORD_HISTORY_COUNT, String.valueOf(passwordHistoryCount));
            updatedDirectory.setAttribute(InternalDirectory.ATTRIBUTE_PASSWORD_MAX_CHANGE_TIME, String.valueOf(passwordMaxChangeTime));
            updatedDirectory.setAttribute(InternalDirectory.ATTRIBUTE_PASSWORD_MAX_ATTEMPTS, String.valueOf(passwordMaxAttempts));
            updatedDirectory.setAttribute(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_REGEX, passwordRegex);
            updatedDirectory.setAttribute(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_COMPLEXITY_MESSAGE, String.valueOf(passwordComplexityMessage));
            updatedDirectory.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS, Boolean.toString(useNestedGroups));

            directoryManager.updateDirectory(updatedDirectory);

            return SUCCESS;
        }
        catch (DirectoryNotFoundException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
        catch (RuntimeException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
    }

    private void doValidation()
    {
        try
        {
            if (passwordRegex != null)
            {
                Pattern.compile(passwordRegex);
            }
        }
        catch (PatternSyntaxException e)
        {
            addFieldError("passwordRegex", e.getMessage());
        }

        if (passwordMaxChangeTime < 0)
        {
            addFieldError("passwordMaxChangeTime", getText("directoryinternal.passwordmaxchangetime.invalid"));
        }

        if (passwordMaxAttempts < 0)
        {
            addFieldError("passwordMaxAttempts", getText("directoryinternal.passwordmaxattempts.invalid"));
        }

        if (passwordHistoryCount < 0)
        {
            addFieldError("passwordHistoryCount", getText("directoryinternal.passwordhistorycount.invalid"));
        }
    }

    ///CLOVER:OFF
    public long getID()
    {
        return ID;
    }

    public void setID(long ID)
    {
        this.ID = ID;
    }

    public Directory getDirectory()
    {
        if (directory == null && ID != -1)
        {
            try
            {
                directory = directoryManager.findDirectoryById(ID);
            }
            catch (DirectoryNotFoundException e)
            {
                addActionError(e);
            }
        }

        return directory;
    }

    public long getPasswordMaxAttempts()
    {
        return passwordMaxAttempts;
    }

    public void setPasswordMaxAttempts(long passwordMaxAttempts)
    {
        this.passwordMaxAttempts = passwordMaxAttempts;
    }

    public long getPasswordMaxChangeTime()
    {
        return passwordMaxChangeTime;
    }

    public void setPasswordMaxChangeTime(long passwordMaxChangeTime)
    {
        this.passwordMaxChangeTime = passwordMaxChangeTime;
    }

    public long getPasswordHistoryCount()
    {
        return passwordHistoryCount;
    }

    public void setPasswordHistoryCount(long passwordHistoryCount)
    {
        this.passwordHistoryCount = passwordHistoryCount;
    }

    public String getPasswordRegex()
    {
        return passwordRegex;
    }

    public void setPasswordRegex(String passwordRegex)
    {
        this.passwordRegex = passwordRegex;
    }

    public String getUserEncryptionMethod()
    {
        return StringUtils.upperCase(getDirectory().getValue(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD));
    }

    public boolean isUseNestedGroups()
    {
        return useNestedGroups;
    }

    public void setUseNestedGroups(final boolean useNestedGroups)
    {
        this.useNestedGroups = useNestedGroups;
    }

    public String getPasswordComplexityMessage()
    {
        return passwordComplexityMessage;
    }

    public void setPasswordComplexityMessage(String passwordComplexityMessage)
    {
        this.passwordComplexityMessage = passwordComplexityMessage;
    }
}
