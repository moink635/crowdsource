package com.atlassian.crowd.util;

import java.util.AbstractMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.service.client.ClientProperties;

import com.google.common.collect.ImmutableSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Provides an unmodifiable, dynamic view of the authority mappings that contains
 * that groups that are assigned to the application. The dynamic mapper automatically
 * reflects the changes in the application group mappings, but the returned iterators
 * capture snapshots of the mappings, i.e., they are not updated even if the
 * mapping change.
 *
 */
public class DynamicAuthorityMappings implements Iterable<Map.Entry<String, String>>
{

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private String adminAuthority;
    private String applicationName;
    private ApplicationManager applicationManager;

    public DynamicAuthorityMappings(ClientProperties clientProperties, ApplicationManager applicationManager, String adminAuthority)
    {
        this.applicationName = clientProperties.getApplicationName();
        this.applicationManager = applicationManager;
        this.adminAuthority = adminAuthority;
    }

    private Application getApplication() throws ApplicationNotFoundException
    {
        return applicationManager.findByName(applicationName);
    }

    private Set<String> getPrivilegedGroups()
    {
        Set<String> privilegedGroups = new HashSet<String>();
        try
        {
            for (DirectoryMapping directoryMapping : getApplication().getDirectoryMappings())
            {
                for (GroupMapping groupMapping : directoryMapping.getAuthorisedGroups())
                {
                    privilegedGroups.add(groupMapping.getGroupName());
                }
            }
        }
        catch (ApplicationNotFoundException e)
        {
            logger.error("When trying to find the groups with authority over application", e);
        }
        return privilegedGroups;
    }

    /**
     * In order make life simpler for users of this class, this method
     * returns a snapshot of the configuration, i.e., the returned iterator will not change
     * even if the dynamic map does. This is intentional, and makes it easier to
     * iterate without caring about concurrent modifications. Note, however, that subsequent
     * calls to this object may find it in a different state.
     * @return A snapshot of the mappings.
     */
    @Override
    public Iterator<Map.Entry<String, String>> iterator()
    {
        ImmutableSet.Builder<Map.Entry<String,String>> builder = ImmutableSet.builder();
        for (String group : getPrivilegedGroups())
        {
            builder.add(new AbstractMap.SimpleImmutableEntry<String, String>(group, adminAuthority));
        }
        return builder.build().iterator();
    }
}
