package com.atlassian.crowd.console.value.directory;

public class ConnectorConnection
{
    // Timeout value are displayed to users as seconds, but stored as milliseconds
    private long readTimeoutInSec = 120;
    private long searchTimeoutInSec = 60;
    private long connectionTimeoutInSec = 10;
    private boolean incrementalSyncEnabled = false;
    // Note 'pollingInterval' is LDAP Sync Interval (displayed to user as minutes, but stored as seconds)
    protected long pollingIntervalInMin = 60;

    public long getReadTimeoutInSec()
    {
        return readTimeoutInSec;
    }

    public void setReadTimeoutInSec(long readTimeoutInSec)
    {
        this.readTimeoutInSec = readTimeoutInSec;
    }

    public long getSearchTimeoutInSec()
    {
        return searchTimeoutInSec;
    }

    public void setSearchTimeoutInSec(long searchTimeoutInSec)
    {
        this.searchTimeoutInSec = searchTimeoutInSec;
    }

    public long getConnectionTimeoutInSec()
    {
        return connectionTimeoutInSec;
    }

    public void setConnectionTimeoutInSec(long connectionTimeoutInSec)
    {
        this.connectionTimeoutInSec = connectionTimeoutInSec;
    }

    public boolean isIncrementalSyncEnabled()
    {
        return incrementalSyncEnabled;
    }

    public void setIncrementalSyncEnabled(boolean incrementalSyncEnabled)
    {
        this.incrementalSyncEnabled = incrementalSyncEnabled;
    }

    public long getPollingIntervalInMin()
    {
        return pollingIntervalInMin;
    }

    public void setPollingIntervalInMin(long pollingIntervalInMin)
    {
        this.pollingIntervalInMin = pollingIntervalInMin;
    }
}
