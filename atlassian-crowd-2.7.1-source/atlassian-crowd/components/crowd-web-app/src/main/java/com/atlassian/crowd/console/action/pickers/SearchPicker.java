package com.atlassian.crowd.console.action.pickers;

import com.atlassian.crowd.console.action.AbstractBrowser;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.model.DirectoryEntity;
import com.atlassian.crowd.model.EntityComparator;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchPicker extends AbstractBrowser
{
    // input (also see AbstractBrowser for paging)
    private Long directoryID;
    private String searchString;
    private List<String> exclusions;
    private String entityName;
    private String activeFlag;

    // output (also see AbstractBrowser for results)
    private String resultEntityType;

    private List<DirectoryEntity> removeExclusionsFromResults(List<DirectoryEntity> results, Iterable<String> exclusionNames)
    {
        if (exclusionNames == null || Iterables.isEmpty(exclusionNames))
        {
            return results;
        }

        Map<String, DirectoryEntity> resultsMap = new HashMap<String, DirectoryEntity>();
        for (DirectoryEntity result : results)
        {
                resultsMap.put(result.getName(), result);
        }

        for (String entityName : exclusionNames)
        {
            // this should be fast (ie. M+N instead of M*N)
            resultsMap.remove(entityName);
        }

        return new ArrayList<DirectoryEntity>(resultsMap.values());
    }

    private List<? extends DirectoryEntity> retainInclusionsInResults(List<DirectoryEntity> results, List<String> inclusionNames)
    {
        if (inclusionNames == null || inclusionNames.isEmpty())
        {
            return Collections.emptyList();
        }

        Map<String, DirectoryEntity> resultsMap = new HashMap<String, DirectoryEntity>();
        for (DirectoryEntity result : results)
        {
                resultsMap.put(result.getName(), result);
        }

        List<DirectoryEntity> retainedEntities = new ArrayList<DirectoryEntity>();
        for (String entityName : inclusionNames)
        {
            // this should be fast (ie. M+N instead of M*N)
            if (resultsMap.containsKey(entityName))
            {
                retainedEntities.add(resultsMap.get(entityName));
            }
        }

        return retainedEntities;
    }

    private boolean isDirectoryIdValid()
    {
        return directoryID != null && directoryID >= 0;
    }

    private List<User> searchUsers(Long directoryID, String searchString, String active, int maxResults) throws Exception
    {
         resultEntityType = Entity.USER.name();

        SearchRestriction keywordRestriction = null;
        SearchRestriction activeRestriction = null;
        SearchRestriction finalRestriction;

        if (StringUtils.isNotBlank(searchString))
        {
            keywordRestriction = Combine.anyOf(
                                    Restriction.on(UserTermKeys.USERNAME).containing(searchString),
                                    Restriction.on(UserTermKeys.EMAIL).containing(searchString),
                                    Restriction.on(UserTermKeys.DISPLAY_NAME).containing(searchString),
                                    Restriction.on(UserTermKeys.FIRST_NAME).containing(searchString),
                                    Restriction.on(UserTermKeys.LAST_NAME).containing(searchString)
                                 );
        }

        if (StringUtils.isNotBlank(active))
        {
            activeRestriction = Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(Boolean.valueOf(active));
        }

        if (keywordRestriction != null && activeRestriction != null)
        {
            finalRestriction = Combine.allOf(keywordRestriction, activeRestriction);
        }
        else if (keywordRestriction != null)
        {
            finalRestriction = keywordRestriction;
        }
        else if (activeRestriction != null)
        {
            finalRestriction = activeRestriction;
        }
        else
        {
            finalRestriction = NullRestrictionImpl.INSTANCE;
        }

        return directoryManager.searchUsers(directoryID, QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                .with(finalRestriction)
                .returningAtMost(maxResults));
    }


    private List<Group> searchGroups(Long directoryID, String searchString, String active, int maxResults) throws Exception
    {
         resultEntityType = Entity.GROUP.name();

        SearchRestriction keywordRestriction = null;
        SearchRestriction activeRestriction = null;
        SearchRestriction finalRestriction;

        if (StringUtils.isNotBlank(searchString))
        {
            keywordRestriction = Restriction.on(GroupTermKeys.NAME).containing(searchString);
        }

        if (StringUtils.isNotBlank(active))
        {
            activeRestriction = Restriction.on(GroupTermKeys.ACTIVE).exactlyMatching(Boolean.valueOf(active));
        }

        if (keywordRestriction != null && activeRestriction != null)
        {
            finalRestriction = Combine.allOf(keywordRestriction, activeRestriction);
        }
        else if (keywordRestriction != null)
        {
            finalRestriction = keywordRestriction;
        }
        else if (activeRestriction != null)
        {
            finalRestriction = activeRestriction;
        }
        else
        {
            finalRestriction = NullRestrictionImpl.INSTANCE;
        }

        return directoryManager.searchGroups(directoryID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP))
                .with(finalRestriction)
                .returningAtMost(maxResults));
    }


    private static <T> List<T> sortAndConstrainResults(List<T> results, int maxResults, Class<T> type)
    {
        List<T> sorted = Ordering.from(EntityComparator.of(type)).immutableSortedCopy(results);
        if (results.size() < maxResults)
        {
            maxResults = results.size();
        }
        return sorted.subList(0, maxResults);
    }

    /**
     * Generates a collection of users in a directory,
     * removing users that are specified as exclusions.
     * <p/>
     * Params:
     * <ul>
     * <li>directoryID</li>
     * <li>exclusions (optional)</li>
     * <li>searchString (optional)</il>
     * <li>active (optional)</li>
     * </ul>
     *
     * @return SUCCESS.
     * @throws Exception error executing search.
     */
    public String getUsersExcluding() throws Exception
    {
        resultEntityType = Entity.USER.name();

        if (!isDirectoryIdValid())
        {
            // invalid directoryID will always return empty results
            results = Collections.emptyList();
            return SUCCESS;
        }

        results = searchUsers(directoryID, searchString, activeFlag, EntityQuery.ALL_RESULTS);

        results = removeExclusionsFromResults(results, exclusions);

        results = sortAndConstrainResults(results, resultsPerPage, User.class);

        return SUCCESS;
    }

    /**
     * Generates a collection of groups in a directory,
     * removing groups that are specified as exclusions.
     * <p/>
     * Params:
     * <ul>
     * <li>directoryID</li>
     * <li>exclusions (optional)</li>
     * <li>searchString (optional)</li>
     * <li>active (optional)</li>
     * </ul>
     *
     * @return SUCCESS.
     */
    public String getGroupsExcluding() throws Exception
    {
        resultEntityType = Entity.GROUP.name();

        if (!isDirectoryIdValid())
        {
            // invalid directoryID will always return empty results
            results = Collections.emptyList();
            return SUCCESS;
        }

        results = searchGroups(directoryID, searchString, activeFlag, EntityQuery.ALL_RESULTS);

        results = removeExclusionsFromResults(results, exclusions);

        results = sortAndConstrainResults(results, resultsPerPage, Group.class);

        return SUCCESS;
    }


    /**
     * Generates a collection of users that are members
     * of a group in a particular directory.
     * <p/>
     * Params:
     * <ul>
     * <li>directoryID</li>
     * <li>entityName (group name)</li>
     * <li>searchString (optional)</il>
     * <li>active (optional)</li>
     * </ul>
     *
     * @return SUCCESS.
     * @throws Exception error executing search.
     */
    public String getUserMembersOfGroup() throws Exception
    {
        if (!isDirectoryIdValid())
        {
            results = Collections.emptyList();
            return SUCCESS;
        }

        // get all users matching search criteria
        results = searchUsers(directoryID, searchString, activeFlag, EntityQuery.ALL_RESULTS);

        List<String> userMembersOfGroup = directoryManager.searchDirectGroupRelationships(directoryID, QueryBuilder.queryFor(String.class, EntityDescriptor.user())
                .childrenOf(EntityDescriptor.group(GroupType.GROUP))
                .withName(entityName)
                .returningAtMost(EntityQuery.ALL_RESULTS));

        // retain only the ones that are members
        results = retainInclusionsInResults(results, userMembersOfGroup);

        results = sortAndConstrainResults(results, resultsPerPage, User.class);

        return SUCCESS;
    }


    /**
     * Generates a collection of users that are not members
     * of a group in a particular directory.
     * <p/>
     * Params:
     * <ul>
     * <li>directoryID</li>
     * <li>entityName (group name)</li>
     * <li>searchString (optional)</il>
     * <li>active (optional)</li>
     * </ul>
     *
     * @return SUCCESS.
     * @throws Exception error executing search.
     */
    public String getUserNonMembersOfGroup() throws Exception
    {
        if (!isDirectoryIdValid())
        {
            results = Collections.emptyList();
            return SUCCESS;
        }

        List<String> existingUsers = directoryManager.searchDirectGroupRelationships(directoryID, QueryBuilder.queryFor(String.class, EntityDescriptor.user())
                .childrenOf(EntityDescriptor.group(GroupType.GROUP))
                .withName(entityName)
                .returningAtMost(EntityQuery.ALL_RESULTS));

        results = searchUsers(directoryID, searchString, activeFlag, EntityQuery.ALL_RESULTS);

        results = removeExclusionsFromResults(results, existingUsers);

        results = sortAndConstrainResults(results, resultsPerPage, User.class);

        return SUCCESS;
    }

    /**
     * Generates a collection of groups that are members
     * of a group in a particular directory.
     * <p/>
     * Params:
     * <ul>
     * <li>directoryID</li>
     * <li>entityName (group name)</li>
     * <li>searchString (optional)</il>
     * <li>active (optional)</li>
     * </ul>
     *
     * @return SUCCESS.
     * @throws Exception error executing search.
     */
    public String getGroupMembersOfGroup() throws Exception
    {
        if (!isDirectoryIdValid())
        {
            results = Collections.emptyList();
            return SUCCESS;
        }

        // get all groups matching search criteria
        results = searchGroups(directoryID, searchString, activeFlag, EntityQuery.ALL_RESULTS);

        List<String> groupMembersOfGroup = directoryManager.searchDirectGroupRelationships(directoryID,
                QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP))
                        .childrenOf(EntityDescriptor.group(GroupType.GROUP))
                        .withName(entityName)
                        .returningAtMost(EntityQuery.ALL_RESULTS));

        // retain only the ones that are members
        results = retainInclusionsInResults(results, groupMembersOfGroup);

        results = sortAndConstrainResults(results, resultsPerPage, Group.class);

        return SUCCESS;
    }

    /**
     * Generates a collection of groups that are not members
     * of a group in a particular directory.
     * <p/>
     * Params:
     * <ul>
     * <li>directoryID</li>
     * <li>entityName (group name)</li>
     * <li>searchString (optional)</li>
     * <li>active (optional)</li>
     * </ul>
     *
     * @return SUCCESS.
     * @throws Exception error executing search.
     */
    public String getGroupNonMembersOfGroup() throws Exception
    {
        if (!isDirectoryIdValid())
        {
            results = Collections.emptyList();
            return SUCCESS;
        }

        Iterable<String> existingGroups = directoryManager.searchDirectGroupRelationships(directoryID,
                QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP))
                        .childrenOf(EntityDescriptor.group(GroupType.GROUP))
                        .withName(entityName)
                        .returningAtMost(EntityQuery.ALL_RESULTS));

        // Cannot add the group as a member of itself.
        Iterable<String> exclusionNames = Iterables.concat(existingGroups, ImmutableList.of(entityName));

        results = searchGroups(directoryID, searchString, activeFlag, EntityQuery.ALL_RESULTS);

        results = removeExclusionsFromResults(results, exclusionNames);

        results = sortAndConstrainResults(results, resultsPerPage, Group.class);

        return SUCCESS;
    }

    /**
     * Generates a collection of groups in a particular directory
     * that the user is a member of.
     * <p/>
     * Params:
     * <ul>
     * <li>directoryID</li>
     * <li>entityName (user name)</li>
     * <li>searchString (optional)</il>
     * <li>active (optional)</li>
     * </ul>
     *
     * @return SUCCESS.
     * @throws Exception error executing search.
     */
    public String getGroupMembershipsOfUser() throws Exception
    {
        if (!isDirectoryIdValid())
        {
            results = Collections.emptyList();
            return SUCCESS;
        }

        // get all groups matching search criteria
        results = searchGroups(directoryID, searchString, activeFlag, EntityQuery.ALL_RESULTS);

        List<String> groupMembershipsOfUser = directoryManager.searchDirectGroupRelationships(directoryID,
                QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP))
                        .parentsOf(EntityDescriptor.user())
                        .withName(entityName)
                        .returningAtMost(EntityQuery.ALL_RESULTS));

        // retain only the ones that are members
        results = retainInclusionsInResults(results, groupMembershipsOfUser);

        results = sortAndConstrainResults(results, resultsPerPage, Group.class);

        return SUCCESS;
    }

    /**
     * Generates a collection of groups in a particular directory
     * that the user is not a member of.
     * <p/>
     * Params:
     * <ul>
     * <li>directoryID</li>
     * <li>entityName (user name)</li>
     * <li>searchString (optional)</il>
     * <li>active (optional)</li>
     * </ul>
     *
     * @return SUCCESS.
     * @throws Exception error executing search.
     */
    public String getGroupNonMembershipsOfUser() throws Exception
    {
        if (!isDirectoryIdValid())
        {
            results = Collections.emptyList();
            return SUCCESS;
        }

        List<String> groupMembershipsOfUser = directoryManager.searchDirectGroupRelationships(directoryID,
                QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP))
                        .parentsOf(EntityDescriptor.user())
                        .withName(entityName)
                        .returningAtMost(EntityQuery.ALL_RESULTS));

        results = searchGroups(directoryID, searchString, activeFlag, EntityQuery.ALL_RESULTS);

        results = removeExclusionsFromResults(results, groupMembershipsOfUser);

        results = sortAndConstrainResults(results, resultsPerPage, Group.class);

        return SUCCESS;
    }

    public Long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(Long directoryID)
    {
        this.directoryID = directoryID;
    }

    public String getEntityName()
    {
        return entityName;
    }

    public void setEntityName(String entityName)
    {
        this.entityName = entityName;
    }

    public List<String> getExclusions()
    {
        return exclusions;
    }

    public void setExclusions(List<String> exclusions)
    {
        this.exclusions = exclusions;
    }

    public String getResultEntityType()
    {
        return resultEntityType;
    }

    public String getSearchString()
    {
        return searchString;
    }

    public void setSearchString(String searchString)
    {
        this.searchString = searchString;
    }

    public String getActiveFlag()
    {
        return activeFlag;
    }

    public void setActiveFlag(String activeFlag)
    {
        this.activeFlag = activeFlag;
    }
}
