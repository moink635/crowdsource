package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class CreateCustom extends CreateDirectory
{
    private static final Logger logger = Logger.getLogger(CreateCustom.class);

    private String implementationClass;

    private DirectoryInstanceLoader directoryInstanceLoader;

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            // check for errors
            doValidation();
            if (hasErrors() || hasActionMessages())
            {
                return INPUT;
            }

            DirectoryImpl directoryToUpdate = new DirectoryImpl(name, DirectoryType.CUSTOM, implementationClass);
            directoryToUpdate.setActive(active);
            directoryToUpdate.setDescription(description);
            directoryToUpdate.setAllowedOperations(permissions);

            ID = directoryManager.addDirectory(directoryToUpdate).getId();

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    protected void doValidation()
    {
        if (StringUtils.isBlank(name))
        {
            addFieldError("name", getText("directoryinternal.name.invalid"));
        }
        else
        {
            try
            {
                directoryManager.findDirectoryByName(name);

                addFieldError("name", getText("invalid.namealreadyexist"));
            }
            catch (Exception e)
            {
                // ingore
            }
        }

        if (StringUtils.isBlank(implementationClass))
        {
            addFieldError("implementationClass", getText("directorycustom.implementationclass.invalid"));
        }
        else
        {

            try
            {
                DirectoryImpl remoteDirectory = new DirectoryImpl();
                remoteDirectory.setImplementationClass(implementationClass);

                directoryInstanceLoader.getRawDirectory(remoteDirectory.getId(), remoteDirectory.getImplementationClass(), remoteDirectory.getAttributes());
            }
            catch (Exception e)
            {
                addFieldError("implementationClass", getText("directorycustom.implementationclass.invalid"));
                addActionError(e);
            }
        }
    }

    public String getImplementationClass()
    {
        return implementationClass;
    }

    public void setImplementationClass(String implementationClass)
    {
        this.implementationClass = implementationClass;
    }

    public void setDirectoryInstanceLoader(DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }
}
