package com.atlassian.crowd.console.action.setup;

import com.atlassian.crowd.console.setup.CrowdSetupPersister;

/**
 * Select and set (in crowd.cfg.xml) the installation type
 * for the setup:
 * <ol>
 *   <li>New Installation</li>
 *   <li>Upgrade from XML Backup</li>
 *   <li>Upgrade from existing Database</li>
 * </ol>
 *
 * See CrowdSetupPersister for more information
 * regarding the flow of the setup process with
 * respect to the installation type.
 */

public class Installation extends BaseSetupAction
{
    public static final String INSTALL_TYPE_STEP = "installtype";

    private String installOption;

    public String getStepName()
    {
        return "installtype";
    }

    public String doDefault()
    {
        // check if i'm at the correct step
        String setupDefault = super.doDefault();
        if (!setupDefault.equals(SUCCESS))
        {
            return setupDefault;
        }

        installOption = getNewInstallValue();
        return INPUT;        
    }

    public String doUpdate()
    {
        // check if i'm at the correct step
        String setupUpdate = super.doUpdate();
        if (!setupUpdate.equals(SUCCESS))
        {
            return setupUpdate;
        }

        if (getNewInstallValue().equals(installOption) || getXmlInstallValue().equals(installOption))
        {
            getSetupPersister().setSetupType(installOption);
            getSetupPersister().progessSetupStep();
            return SELECT_SETUP_STEP;
        }
        else
        {
            addActionError(getText("install.option.error"));
            return ERROR;
        }
    }

    public String getInstallOption()
    {
        return installOption;
    }

    public void setInstallOption(String installOption)
    {
        this.installOption = installOption;
    }

    public String getNewInstallValue()
    {
        return CrowdSetupPersister.SETUP_TYPE_NEW_INSTALL;
    }

    public String getXmlInstallValue()
    {
        return CrowdSetupPersister.SETUP_TYPE_UPGRADE_XML;
    }

    public boolean isNewInstallSelected()
    {
        return getNewInstallValue().equals(installOption);
    }

    public boolean isXmlInstallSelected()
    {
        return getXmlInstallValue().equals(installOption);
    }


}
