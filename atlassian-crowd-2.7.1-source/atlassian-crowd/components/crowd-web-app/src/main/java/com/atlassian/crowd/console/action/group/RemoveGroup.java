/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.group;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.log4j.Logger;

public class RemoveGroup extends BaseAction
{
    private static final Logger logger = Logger.getLogger(RemoveGroup.class);

    private long directoryID;
    private String name;
    private Group group;
    protected Directory directory;

    public String doDefault()
    {
        try
        {
            group = directoryManager.findGroupByName(directoryID, name);
            directory = directoryManager.findDirectoryById(directoryID);
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            group = directoryManager.findGroupByName(directoryID, name);
            directory = directoryManager.findDirectoryById(directoryID);

            directoryManager.removeGroup(directoryID, name);

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    public long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(long directoryID)
    {
        this.directoryID = directoryID;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Group getGroup()
    {
        return group;
    }

    public Directory getDirectory()
    {
        return directory;
    }
}