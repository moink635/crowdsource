package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

public class UpdateApplication extends ViewApplication
{
    private static final Logger logger = Logger.getLogger(UpdateApplication.class);

    public String doDefault()
    {
        super.doDefault();

        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        name = toLowerCase(name);

        try
        {
            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                processGeneral();
                return INPUT;
            }

            Application application = applicationManager.findById(ID);

            ApplicationImpl applicationImpl = ApplicationImpl.newInstance(application);

            applicationImpl.setName(name);
            applicationImpl.setDescription(applicationDescription);

            // can't deactivate the crowd app
            if (!isCrowdApplication())
            {
                applicationImpl.setActive(active);
            }

            applicationManager.update(applicationImpl);

            // Are we updating the password
            if (StringUtils.isNotBlank(password) && StringUtils.isNotBlank(passwordConfirm))
            {
                PasswordCredential credential = new PasswordCredential(password);
                applicationManager.updateCredential(getApplication(), credential);
            }

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdateOptional()
    {
        try
        {
            Application currentApplication = applicationManager.findById(ID);
            ApplicationImpl applicationImpl = ApplicationImpl.newInstance(currentApplication);

            //Mutate the hibernate backed application
            applicationImpl.setLowerCaseOutput(lowerCaseOutput);
            applicationImpl.setAliasingEnabled(aliasingEnabled);

            applicationManager.update(applicationImpl);

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            return INPUT;
        }
    }

    protected void doValidation()
    {
        if (StringUtils.isBlank(name))
        {
            addFieldError("name", getText("application.name.invalid"));
        }
        else
        {
            try
            {
                final Application application = getApplicationByName();
                if (application.getId() != ID)
                {
                    // this isn't good, it's the existing name
                    addFieldError("name", getText("invalid.namealreadyexist"));
                }
            }
            catch (Exception e)
            {
                // ignore
            }
        }

        // Only validate the password if we are attempting to update it
        if (StringUtils.isNotBlank(password))
        {
            if (!password.equals(passwordConfirm))
            {
                addFieldError("password", getText("invalid.passwordmismatch"));
                addFieldError("confirm", "");
            }
        }
    }
}