package com.atlassian.crowd.console.action.principal;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class RemovePrincipal extends BaseAction
{
    private static final Logger logger = Logger.getLogger(RemovePrincipal.class);

    protected User principal;
    protected Directory directory;

    private boolean currentUser = false;

    private String name;
    private long directoryID;
    private String directoryName;

    public String doDefault()
    {
        try
        {
            if (StringUtils.isNotBlank(directoryName))
            {
                directory = directoryManager.findDirectoryByName(directoryName);
                directoryID = directory.getId();
            }
            else
            {
                directory = directoryManager.findDirectoryById(directoryID);
            }

            principal = directoryManager.findUserByName(directoryID, name);
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        // Pre-validate so we show the warning.
        doValidation();

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        doValidation();
        if (hasErrors())
        {
            doDefault();
            return INPUT;
        }

        try
        {
            directory = directoryManager.findDirectoryById(directoryID);
            principal = directoryManager.findUserByName(directoryID, name);

            directoryManager.removeUser(directoryID, name);

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    private void doValidation()
    {
        currentUser = getRemoteUser().getName().equals(name) && directoryID == getRemoteUser().getDirectoryId();
        if (currentUser)
        {
            addActionError(getText("principal.remove.curent.user"));
        }
    }

    public boolean isCurrentUser()
    {
        return currentUser;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public User getPrincipal()
    {
        return principal;
    }

    public Directory getDirectory()
    {
        return directory;
    }

    public long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(long directoryID)
    {
        this.directoryID = directoryID;
    }

    public void setDirectoryName(String directoryName)
    {
        this.directoryName = directoryName;
    }
}
