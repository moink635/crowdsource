/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.directory;

import java.util.Collections;
import java.util.Date;
import java.util.ResourceBundle;

import com.atlassian.core.util.DateUtils;
import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.DirectorySynchronisationInformation;
import com.atlassian.crowd.embedded.api.DirectorySynchronisationRoundInformation;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.model.directory.DirectoryImpl;

public class ViewRemoteCrowd extends AbstractViewDirectory
{
    protected String name;
    protected String directoryDescription;
    protected boolean active;

    protected boolean cacheEnabled;

    private boolean useNestedGroups;
    private DirectorySynchronisationInformation syncInfo;

    public String doDefault() throws Exception
    {
        long directoryId = getDirectory().getId();
        name = getDirectory().getName();
        directoryDescription = getDirectory().getDescription();
        active = getDirectory().isActive();
        syncInfo = directoryManager.getDirectorySynchronisationInformation(directoryId);
        cacheEnabled = syncInfo != null;

        String nesting = getDirectory().getValue(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS);

        if (nesting == null)
        {
            useNestedGroups = false;
        }
        else
        {
            useNestedGroups = Boolean.parseBoolean(nesting);
        }

        return SUCCESS;
    }

    ///CLOVER:OFF
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDirectoryDescription()
    {
        return directoryDescription;
    }

    public void setDirectoryDescription(String directoryDescription)
    {
        this.directoryDescription = directoryDescription;
    }

    public boolean getActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isCurrentlySynchronising()
    {
        return syncInfo.isSynchronising();
    }

    public String getSynchronisationStartTime() throws DirectoryInstantiationException
    {
        final DirectorySynchronisationRoundInformation roundInfo = getRoundToDisplay();
        if (roundInfo == null)
        {
            return null;
        }
        return getText("directory.caching.sync.start.time", Collections.singletonList(new Date(roundInfo.getStartTime())));
    }

    public String getSynchronisationDuration() throws DirectoryInstantiationException
    {
        final DirectorySynchronisationRoundInformation roundInfo = getRoundToDisplay();
        if (roundInfo == null)
        {
            return null;
        }
        final ResourceBundle rb = getTexts(BaseAction.class.getName());
        return DateUtils.getDurationPrettySecondsResolution(roundInfo.getDurationMs() / 1000, rb);
    }

    public String getSynchronisationStatus() throws DirectoryInstantiationException
    {
        final DirectorySynchronisationRoundInformation roundInfo = getRoundToDisplay();
        if (roundInfo == null)
        {
            return getText("directory.caching.sync.never.label");
        }
        return getText(roundInfo.getStatusKey(), roundInfo.getStatusParameters(), false);
    }

    private DirectorySynchronisationRoundInformation getRoundToDisplay()
    {
        if (syncInfo.getActiveRound() != null)
        {
            return syncInfo.getActiveRound();
        }
        else if (syncInfo.getLastRound() != null)
        {
            return syncInfo.getLastRound();
        }
        else
        {
            return null;
        }
    }

    public boolean isCacheEnabled()
    {
        return cacheEnabled;
    }

    public void setCacheEnabled(boolean cacheEnabled)
    {
        this.cacheEnabled = cacheEnabled;
    }

    public boolean isUseNestedGroups()
    {
        return useNestedGroups;
    }

    public void setUseNestedGroups(final boolean useNestedGroups)
    {
        this.useNestedGroups = useNestedGroups;
    }
}
