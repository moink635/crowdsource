package com.atlassian.crowd.console.action.options;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.impl.ConnectionPoolPropertyConstants;
import com.atlassian.crowd.embedded.impl.ConnectionPoolPropertyUtil;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.log4j.Logger;

import java.util.Map;
import java.util.concurrent.TimeUnit;

public class UpdateConnectionPool extends BaseAction
{
    private static final Logger logger = Logger.getLogger(UpdateConnectionPool.class);

    private String initialSize = ConnectionPoolPropertyConstants.DEFAULT_INITIAL_POOL_SIZE;
    private String preferredSize = ConnectionPoolPropertyConstants.DEFAULT_PREFERRED_POOL_SIZE;
    private String maximumSize = ConnectionPoolPropertyConstants.DEFAULT_MAXIMUM_POOL_SIZE;
    private String timeoutInSec = Long.toString(TimeUnit.SECONDS.convert(NumberUtils.toLong(ConnectionPoolPropertyConstants.DEFAULT_POOL_TIMEOUT_MS),TimeUnit.MILLISECONDS));
    private String supportedProtocol = ConnectionPoolPropertyConstants.DEFAULT_POOL_PROTOCOL;
    private String supportedAuthentication = ConnectionPoolPropertyConstants.DEFAULT_POOL_AUTHENTICATION;

    private PropertyManager propertyManager;

    @Override
    public String doDefault()
    {
        try
        {
            processConnectionProperties();
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
        }

        return SUCCESS;
    }

    private void processConnectionProperties()
    {
        try
        {
            initialSize = propertyManager.getProperty(ConnectionPoolPropertyConstants.POOL_INITIAL_SIZE);
            preferredSize = propertyManager.getProperty(ConnectionPoolPropertyConstants.POOL_PREFERRED_SIZE);
            maximumSize = propertyManager.getProperty(ConnectionPoolPropertyConstants.POOL_MAXIMUM_SIZE);
            supportedProtocol = propertyManager.getProperty(ConnectionPoolPropertyConstants.POOL_PROTOCOL);
            supportedAuthentication = propertyManager.getProperty(ConnectionPoolPropertyConstants.POOL_AUTHENTICATION);
            // retrieved timeout value will be in milliseconds - convert to seconds to display to user
            timeoutInSec = Long.toString(TimeUnit.SECONDS.convert(NumberUtils.toLong(propertyManager.getProperty(ConnectionPoolPropertyConstants.POOL_TIMEOUT)), TimeUnit.MILLISECONDS));
        }
        catch (ObjectNotFoundException e)
        {
            throw new RuntimeException(e);
        }
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            doValidation();

            if (hasErrors())
            {
                addActionError(getText("connectionpool.update.error"));
                return INPUT;
            }

            propertyManager.setProperty(ConnectionPoolPropertyConstants.POOL_INITIAL_SIZE, initialSize);
            propertyManager.setProperty(ConnectionPoolPropertyConstants.POOL_PREFERRED_SIZE, preferredSize);
            propertyManager.setProperty(ConnectionPoolPropertyConstants.POOL_MAXIMUM_SIZE, maximumSize);
            propertyManager.setProperty(ConnectionPoolPropertyConstants.POOL_PROTOCOL, supportedProtocol);
            propertyManager.setProperty(ConnectionPoolPropertyConstants.POOL_AUTHENTICATION, supportedAuthentication);
            // We want to store timeout value as milliseconds
            propertyManager.setProperty(ConnectionPoolPropertyConstants.POOL_TIMEOUT, Long.toString(TimeUnit.MILLISECONDS.convert(NumberUtils.toLong(timeoutInSec), TimeUnit.SECONDS)));

            ServletActionContext.getRequest().setAttribute("updateSuccessful", "true");
            
            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    private void doValidation() throws ObjectNotFoundException
    {
        // Some basic validation:
        // Check if the settings that are numeric are indeed numbers
        // If entered value is invalid, add a field error and set the value back to what is stored in the database

        if (!StringUtils.isNumeric(initialSize))
        {
            addFieldError("initialSize", getText("connectionpool.integer.invalid"));
            initialSize = propertyManager.getProperty(ConnectionPoolPropertyConstants.POOL_INITIAL_SIZE);
        }

        if (!StringUtils.isNumeric(preferredSize))
        {
            addFieldError("preferredSize", getText("connectionpool.integer.invalid"));
            preferredSize = propertyManager.getProperty(ConnectionPoolPropertyConstants.POOL_PREFERRED_SIZE);
        }

        if (!StringUtils.isNumeric(maximumSize))
        {
            addFieldError("maximumSize", getText("connectionpool.integer.invalid"));
            maximumSize = propertyManager.getProperty(ConnectionPoolPropertyConstants.POOL_MAXIMUM_SIZE);
        }

        if (!StringUtils.isNumeric(timeoutInSec))
        {
            addFieldError("timeoutInSec", getText("connectionpool.integer.invalid"));
            timeoutInSec = propertyManager.getProperty(ConnectionPoolPropertyConstants.POOL_TIMEOUT);
        }

        if (!ConnectionPoolPropertyUtil.isValidProtocol(supportedProtocol))
        {
            addFieldError("supportedProtocol", getText("connectionpool.supportedProtocol.error"));
            supportedProtocol = propertyManager.getProperty(ConnectionPoolPropertyConstants.POOL_PROTOCOL);
        }

        if (!ConnectionPoolPropertyUtil.isValidAuthentication(supportedAuthentication))
        {
            addFieldError("supportedAuthentication", getText("connectionpool.supportedAuthentication.error"));
            supportedAuthentication = propertyManager.getProperty(ConnectionPoolPropertyConstants.POOL_AUTHENTICATION);
        }
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    public String getInitialSize()
    {
        return initialSize;
    }

    public String getMaximumSize()
    {
        return maximumSize;
    }

    public String getPreferredSize()
    {
        return preferredSize;
    }

    public String getSupportedAuthentication()
    {
        return supportedAuthentication;
    }

    public String getSupportedProtocol()
    {
        return supportedProtocol;
    }

    public String getTimeoutInSec()
    {
        return timeoutInSec;
    }

    public Map<String, String> toPropertiesMap()
    {
        throw new UnsupportedOperationException("Should not need to convert to properties map for view.");
    }

    public void setInitialSize(String initialSize)
    {
        this.initialSize = initialSize;
    }

    public void setMaximumSize(String maximumSize)
    {
        this.maximumSize = maximumSize;
    }

    public void setPreferredSize(String preferredSize)
    {
        this.preferredSize = preferredSize;
    }

    public void setSupportedAuthentication(String supportedAuthentication)
    {
        this.supportedAuthentication = supportedAuthentication;
    }

    public void setSupportedProtocol(String supportedProtocol)
    {
        this.supportedProtocol = supportedProtocol;
    }

    public void setTimeoutInSec(String timeoutInSec)
    {
        this.timeoutInSec = timeoutInSec;
    }

    // Get the system property values to display in the "Current Settings" section
    public String getSystemInitialSize()
    {
        return System.getProperty(ConnectionPoolPropertyConstants.POOL_INITIAL_SIZE, ConnectionPoolPropertyConstants.DEFAULT_INITIAL_POOL_SIZE);
    }

    public String getSystemMaximumSize()
    {
        return System.getProperty(ConnectionPoolPropertyConstants.POOL_MAXIMUM_SIZE, ConnectionPoolPropertyConstants.DEFAULT_MAXIMUM_POOL_SIZE);
    }

    public String getSystemPreferredSize()
    {
        return System.getProperty(ConnectionPoolPropertyConstants.POOL_PREFERRED_SIZE, ConnectionPoolPropertyConstants.DEFAULT_PREFERRED_POOL_SIZE);
    }

    public String getSystemSupportedAuthentication()
    {
        return System.getProperty(ConnectionPoolPropertyConstants.POOL_AUTHENTICATION, ConnectionPoolPropertyConstants.DEFAULT_POOL_AUTHENTICATION);
    }

    public String getSystemSupportedProtocol()
    {
        return System.getProperty(ConnectionPoolPropertyConstants.POOL_PROTOCOL, ConnectionPoolPropertyConstants.DEFAULT_POOL_PROTOCOL);
    }

    public String getSystemTimeoutInSec()
    {
        // System timeout value is stored as milliseconds, but displayed to the users as seconds
        long poolTimeout = NumberUtils.toLong(System.getProperty(ConnectionPoolPropertyConstants.POOL_TIMEOUT, ConnectionPoolPropertyConstants.DEFAULT_POOL_TIMEOUT_MS));
        return Long.toString(TimeUnit.SECONDS.convert(poolTimeout, TimeUnit.MILLISECONDS));
    }
}
