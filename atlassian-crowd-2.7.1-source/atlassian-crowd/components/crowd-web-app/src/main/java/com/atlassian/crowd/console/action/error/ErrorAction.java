package com.atlassian.crowd.console.action.error;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventType;

import com.opensymphony.webwork.interceptor.ServletResponseAware;

import org.apache.commons.httpclient.HttpStatus;

/**
 * Error action that populates a webwork action with Johnson Events
 */
public class ErrorAction extends BaseAction implements ServletResponseAware
{
    private Map<EventType, List<Event>> eventMap;
    private List<Event> importErrors;

    private HttpServletResponse response;  // injected
    private JohnsonEventContainer johnsonEventContainer;
    private static final String IMPORT_EVENT_TYPE = "import";

    public ErrorAction()
    {
        eventMap = Collections.emptyMap();
        importErrors = Collections.emptyList();
    }

    public String execute() throws Exception
    {
        if (johnsonEventContainer != null && johnsonEventContainer.hasEvents())
        {
            this.eventMap = new HashMap<EventType, List<Event>>();
            this.importErrors = new ArrayList<Event>();

            for (Event event : johnsonEventContainer.getEvents())
            {
                EventType eventType = event.getKey();
                if (!IMPORT_EVENT_TYPE.equals(eventType.getType()))
                {
                    List<Event> currentEvents = eventMap.get(eventType);
                    if (currentEvents == null)
                    {
                        currentEvents = new ArrayList<Event>();
                    }

                    currentEvents.add(event);
                    eventMap.put(eventType, currentEvents);
                }
                else
                {
                    importErrors.add(event);
                }
            }
        }

        response.setStatus(HttpStatus.SC_SERVICE_UNAVAILABLE);

        return ERROR;
    }

    public List<Event> getImportErrors()
    {
        return importErrors;
    }

    public String removeImportErrorEvent()
    {
        for (Event event : johnsonEventContainer.getEvents())
        {
            if (IMPORT_EVENT_TYPE.equals(event.getKey().getType()))
            {
                johnsonEventContainer.removeEvent(event);
            }
        }

        return SUCCESS;
    }

    public Map<EventType, List<Event>> getEventMap()
    {
        return eventMap;
    }

    public void setJohnsonEventContainer(JohnsonEventContainer johnsonEventContainer)
    {
        this.johnsonEventContainer = johnsonEventContainer;
    }

    @Override
    public void setServletResponse(HttpServletResponse response)
    {
        this.response = response;
    }
}