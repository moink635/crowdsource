package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class UpdateAddresses extends ViewApplication
{
    private static final Logger logger = Logger.getLogger(UpdateAddresses.class);

    private String address;

    @RequireSecurityToken(true)
    public String doRemoveAddress()
    {
        try
        {
            processGeneral();

            // the the existing application
            Application application = applicationManager.findById(ID);

            if (address != null && address.length() > 0)
            {
                applicationManager.removeRemoteAddress(application, new RemoteAddress(address));
            }

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doAddAddress()
    {
        try
        {
            processGeneral();

            // Get rid of any extraneous whitespace.
            if (address != null)
            {
                address = address.trim();
            }

            // the the existing application
            Application application = applicationManager.findById(ID);

            if (StringUtils.isNotBlank(address))
            {
                RemoteAddress remoteAddress = new RemoteAddress(address);
                if (application.getRemoteAddresses().contains(remoteAddress))
                {
                    addActionError(getText("application.addressmappings.duplicate"));
                    return INPUT;
                }
                else
                {
                    applicationManager.addRemoteAddress(application, remoteAddress);
                }
            }

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }
}