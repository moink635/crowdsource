package com.atlassian.crowd.console.filter;

import com.atlassian.core.filters.cache.AbstractCachingFilter;
import com.atlassian.core.filters.cache.CachingStrategy;
import com.opensymphony.util.TextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class CrowdCachingFilter extends AbstractCachingFilter
{
    private static final CachingStrategy[] strategies = { new NonCachableCachingStrategy() };

    /**
     * Returns a list of all the caching strategies that Crowd cares about.
     */
    protected CachingStrategy[] getCachingStrategies()
    {
        return strategies;
    }


    /**
     * Handles URIs that should not be cached.
     */
    static class NonCachableCachingStrategy implements CachingStrategy
    {
        /**
         * Returns true if this is a non-cachable URI
         */
        public boolean matches(HttpServletRequest request)
        {
            String uri = TextUtils.noNull(request.getRequestURI());
            return uri.indexOf(".jsp") > 0 || uri.indexOf(".action") > 0;
        }

        /**
         * Sets the relevant headers so the response is not cached.
         */
        public void setCachingHeaders(HttpServletResponse response)
        {
            response.setHeader("Cache-Control", "no-cache");    // http 1.1
            response.setHeader("Pragma", "no-cache");           // http 1.0
            response.setDateHeader("Expires", 0);               // prevent proxy caching
        }
    }
}
