package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;

import java.util.Map;

/**
 * Update Action specifically for the Delegated Directory type
 */
public class UpdateDelegatedConfiguration extends UpdateConnectorConfiguration
{
    @Override
    public void populateDirectoryAttributesForConnectionTest(final Map<String, String> attributes)
    {
        super.populateDirectoryAttributesForConnectionTest(attributes);
        attributes.put(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS, getDirectory().getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS));
    }

    @Override
    public boolean isShowGroupsConfiguration()
    {
        return Boolean.valueOf(getDirectory().getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_KEY_IMPORT_GROUPS));
    }

    @Override
    void validateUserPasswordAttr()
    {
        // do nothing, since we don't have this option for a delegated directory
    }
}
