package com.atlassian.crowd.console.action.options;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.util.SSOUtils;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UpdateGeneral extends BaseAction
{
    private static final Logger logger = LoggerFactory.getLogger(UpdateGeneral.class);

    private String title;
    private String domain;
    private boolean secureCookie;
    private boolean cachingEnabled;
    private boolean gzip;

    @Override
    public String doDefault()
    {
        try
        {
            processGeneral();
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                return INPUT;
            }

            propertyManager.setDeploymentTitle(StringUtils.defaultString(title));
            propertyManager.setDomain(StringUtils.defaultString(domain));
            if (isDisableSSOSecureCookie())
            {
                if (secureCookie)
                {
                    logger.warn("Attempted to set secure SSO cookie over a insecure channel.");
                }
            }
            else
            {
                propertyManager.setSecureCookie(secureCookie);
            }
            propertyManager.setGzipEnabled(gzip);
            propertyManager.setCacheEnabled(cachingEnabled);


            ServletActionContext.getRequest().setAttribute("updateSuccessful", "true");

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    protected void processGeneral()
    {
        try
        {
            title = propertyManager.getDeploymentTitle();
            domain = propertyManager.getDomain();
            secureCookie = propertyManager.isSecureCookie();
            gzip = propertyManager.isGzipEnabled();

            cachingEnabled = propertyManager.isCacheEnabled();
        }
        catch (PropertyManagerException e)
        {
            logger.error(e.getMessage(), e);
        }
    }

    protected void doValidation()
    {
        if (StringUtils.isBlank(title))
        {
            addFieldError("title", getText("options.title.invalid"));
        }

        // Check if newly entered domain value is valid or not (in comparison to current domain)
        if (!SSOUtils.isCookieDomainValid(domain, ServletActionContext.getRequest().getServerName()))
        {
            addFieldError("domain", getText("options.domain.invalid"));
        }
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getDomain()
    {
        return domain;
    }

    public void setDomain(String domain)
    {
        this.domain = domain;
    }

    public boolean isSecureCookie()
    {
        return secureCookie;
    }

    public void setSecureCookie(boolean secureCookie)
    {
        this.secureCookie = secureCookie;
    }

    public boolean isGzip()
    {
        return gzip;
    }

    public void setGzip(boolean gzip)
    {
        this.gzip = gzip;
    }

    public boolean isCachingEnabled()
    {
        return cachingEnabled;
    }

    public void setCachingEnabled(boolean cachingEnabled)
    {
        this.cachingEnabled = cachingEnabled;
    }

    public boolean isDisableSSOSecureCookie()
    {
        return !getHttpRequest().isSecure();
    }
}
