/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.options;

import com.atlassian.core.util.PairType;
import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.manager.token.SwitchableTokenManager;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;

public class UpdateSessionConfig extends BaseAction
{
    private static final Logger logger = Logger.getLogger(UpdateSessionConfig.class);

    public static final String DATABASE_STORAGE = "database";
    public static final String MEMORY_STORAGE = "memory";

    private long sessionTime;
    private String storageType;
    private SwitchableTokenManager tokenManager;
    private PropertyManager propertyManager;
    private final List<PairType> tokenStorageOptions = Arrays.asList(new PairType(DATABASE_STORAGE, getText("caching.token.database")), new PairType(MEMORY_STORAGE, getText("caching.token.memory")));

    private boolean includeIpAddressInValidationFactors;

    @Override
    public String doDefault()
    {
        try
        {
            processSession();
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                return INPUT;
            }

            propertyManager.setSessionTime(sessionTime);

            if (includeIpAddressInValidationFactors != propertyManager.isIncludeIpAddressInValidationFactors())
            {
                tokenManager.removeAll();
            }

            propertyManager.setIncludeIpAddressInValidationFactors(includeIpAddressInValidationFactors);

            boolean usingDatabaseStorage = isUsingDatabaseStorage(storageType);

            // Switch this on the DAO
            tokenManager.setUsingDatabaseStorage(usingDatabaseStorage);

            ServletActionContext.getRequest().setAttribute("updateSuccessful","true");

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    private static boolean isUsingDatabaseStorage(String storageType)
    {
        return DATABASE_STORAGE.equals(storageType);
    }

    private void processSession() throws PropertyManagerException
    {
        sessionTime = propertyManager.getSessionTime();
        includeIpAddressInValidationFactors = propertyManager.isIncludeIpAddressInValidationFactors();
        storageType = propertyManager.isUsingDatabaseTokenStorage() ? DATABASE_STORAGE : MEMORY_STORAGE;
    }

    protected void doValidation()
    {
        if (sessionTime < 1)
        {
            addFieldError("sessionTime", getText("session.sessiontime.invalid"));
        }
    }

    public long getSessionTime()
    {
        return sessionTime;
    }

    public void setSessionTime(long sessionTime)
    {
        this.sessionTime = sessionTime;
    }

    public boolean getIncludeIpAddressInValidationFactors()
    {
        return includeIpAddressInValidationFactors;
    }

    public void setIncludeIpAddressInValidationFactors(boolean include)
    {
        this.includeIpAddressInValidationFactors = include;
    }

    public List<PairType> getTokenStorageOptions()
    {
        return tokenStorageOptions;
    }

    public void setStorageType(final String storageType)
    {
        this.storageType = storageType;
    }

    public String getStorageType()
    {
        return storageType;
    }

    public void setTokenManager(SwitchableTokenManager tokenManager)
    {
        this.tokenManager = tokenManager;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}
