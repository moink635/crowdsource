/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.atlassian.crowd.manager.mail.MailManager;

public class Console extends BaseAction
{
    private static final Logger log = Logger.getLogger(Console.class);

    private Boolean isNearExpiredLicense = null;
    protected DateTime expiryDate = null;
    protected String daysLeft = null;
    private boolean mailServerConfigured;

    private MailManager mailManager; // injected

    public String execute() throws Exception
    {
        if (getLicense().getExpiryDate() != null)
        {
            expiryDate = new DateTime(getLicense().getExpiryDate());
        }
        else if (getLicense().getMaintenanceExpiryDate() != null)
        {
            expiryDate = new DateTime(getLicense().getMaintenanceExpiryDate());
        }

        mailServerConfigured = mailManager.isConfigured();

        return SUCCESS;
    }

    public boolean isNearExpiredLicense()
    {
        if (isNearExpiredLicense == null)
        {
            final int days = getLicense().isEvaluation() ?
                    getLicense().getNumberOfDaysBeforeExpiry() : getLicense().getNumberOfDaysBeforeMaintenanceExpiry();

            // Generate the text to display to the user regarding days left.
            daysLeft = days + " " + getText("common.words.day" + (days == 0 || days > 1 ? "s" : "") + ".text");
            // Does this license have less than 30 days left?
            isNearExpiredLicense = Boolean.valueOf(days >= 0 && days <= 30);
        }

        return isNearExpiredLicense.booleanValue();
    }

    public String getDaysLeft()
    {
        return daysLeft;
    }

    public String getExpiryDate()
    {
        SimpleDateFormat DATE_PICKER_FORMAT = new SimpleDateFormat("dd MMM yyyy");
        return DATE_PICKER_FORMAT.format(new Date(expiryDate.getMillis()));
    }

    public void setMailManager(MailManager mailManager)
    {
        this.mailManager = mailManager;
    }

    public boolean isMailServerConfigured()
    {
        return mailServerConfigured;
    }
}