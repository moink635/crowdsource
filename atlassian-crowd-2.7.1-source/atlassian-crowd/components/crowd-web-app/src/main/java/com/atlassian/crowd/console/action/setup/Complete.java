package com.atlassian.crowd.console.action.setup;

import java.util.ArrayList;
import java.util.Collection;

import com.atlassian.config.HomeLocator;
import com.atlassian.config.setup.SetupException;
import com.atlassian.crowd.console.action.Login;
import com.atlassian.crowd.integration.springsecurity.RequestToApplicationMapper;
import com.atlassian.crowd.manager.application.CrowdApplicationPasswordManager;
import com.atlassian.crowd.manager.upgrade.UpgradeManager;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.config.JohnsonConfig;
import com.atlassian.johnson.event.Event;

import com.opensymphony.webwork.ServletActionContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class Complete extends BaseSetupAction
{
    private static final Logger logger = Logger.getLogger(Complete.class);

    public static final String COMPLETE_STEP = "finishsetup";

    private CrowdApplicationPasswordManager crowdApplicationPasswordManager;
    private HomeLocator homeLocator;
    private ClientProperties clientProperties;
    private UpgradeManager upgradeManager;
    private RequestToApplicationMapper requestToApplicationMapper;
    private static final String FATAL = "fatal";

    /**
     * This is required because this action backs login.jsp if the setup has completed successfully
     *
     * @see com.atlassian.crowd.console.action.Login#isDomainValid()
     */
    public boolean isDomainValid()
    {
        return Login.isDomainValid(propertyManager);
    }

    /**
     * This is required because this action backs login.jsp if the setup has completed successfully
     *
     * @see com.atlassian.crowd.console.action.Login#getApplicationName()
     */
    public String getApplicationName()
    {
        return requestToApplicationMapper.getApplication(ServletActionContext.getRequest());
    }

    public String execute()
    {
        // check if i'm at the correct step
        String setupDefault = super.doDefault();
        if (!setupDefault.equals(SUCCESS))
        {
            return setupDefault;
        }

        // Validate the client properties before we go any further
        if (validateClientProperties())
        {
            // Throw a johnson event and alert the user they need to place their crowd.properties from their old install
            // in the home directory
            JohnsonEventContainer agentJohnson = Johnson.getEventContainer(ServletActionContext.getServletContext());
            JohnsonConfig johnsonConfig = Johnson.getConfig();

            agentJohnson.addEvent(new Event(johnsonConfig.getEventType("setup"), getText("setupcomplete.error.properties", new String[]{homeLocator.getHomePath()}), johnsonConfig.getEventLevel("fatal")));

            return FATAL;
        }

        // run upgrade tasks
        performUpgrades();

        if (hasErrors())
        {
            return ERROR;
        }

        // setup is complete now!
        try
        {
            getSetupPersister().progessSetupStep();
            getSetupPersister().finishSetup();
            getBootstrapManager().publishConfiguration();
        }
        catch (SetupException e)
        {
            logger.error(getText("setupcomplete.error.save.config"), e);
            addActionError(new StringBuilder().append(getText("setupcomplete.error.save.config")).append(": ").append(e.getMessage()).toString());
            return ERROR;
        }

        // test crowd application authentication
        resetCrowdPasswordIfRequired();

        if (hasErrors())
        {
            return ERROR;
        }

        String adminUsername = getJ_username();

        if (adminUsername != null)
        {
            addActionMessage("green", getText("setupcomplete.text", new String[] { adminUsername }));
        }

        return SUCCESS;
    }

    private void performUpgrades()
    {
        Collection<String> errors = new ArrayList<String>();
        try
        {
            errors.addAll(upgradeManager.doUpgrade());
        }
        catch (Exception e)
        {
            logger.error(getText("setupcomplete.error.upgrades"), e);
            addActionError(new StringBuilder().append(getText("setupcomplete.error.upgrades")).append(": ").append(e.getMessage()).toString());
        }

        for (String error : errors)
        {
            addActionError(error);
        }
    }

    private boolean validateClientProperties()
    {
        return StringUtils.isBlank(clientProperties.getApplicationName()) || StringUtils.isBlank(clientProperties.getApplicationAuthenticationURL()) || StringUtils.isBlank(clientProperties.getApplicationPassword());
    }

    private void resetCrowdPasswordIfRequired()
    {
        try
        {
            crowdApplicationPasswordManager.resetCrowdPasswordIfRequired();
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
            addActionError(new StringBuilder().append(getText("setupcomplete.error.password.reset")).append(": ").append(e.getMessage()).toString());
        }
    }

    public void setUpgradeManager(UpgradeManager upgradeManager)
    {
        this.upgradeManager = upgradeManager;
    }

    public String getStepName()
    {
        return COMPLETE_STEP;
    }

    public void setCrowdApplicationPasswordManager(final CrowdApplicationPasswordManager crowdApplicationPasswordManager)
    {
        this.crowdApplicationPasswordManager = crowdApplicationPasswordManager;
    }

    public void setClientProperties(ClientProperties clientProperties)
    {
        this.clientProperties = clientProperties;
    }

    public void setHomeLocator(HomeLocator homeLocator)
    {
        this.homeLocator = homeLocator;
    }

    public void setRequestToApplicationMapper(RequestToApplicationMapper requestToApplicationMapper)
    {
        this.requestToApplicationMapper = requestToApplicationMapper;
    }


    public String getJ_username()
    {
        return (String) getSession().getAttribute(DefaultAdministrator.DEFAULT_ADMIN_NAME_KEY);
    }
}
