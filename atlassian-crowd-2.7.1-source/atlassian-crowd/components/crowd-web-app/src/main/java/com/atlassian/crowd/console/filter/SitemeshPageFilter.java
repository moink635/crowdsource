package com.atlassian.crowd.console.filter;

import com.opensymphony.module.sitemesh.Decorator;
import com.opensymphony.module.sitemesh.Page;
import com.opensymphony.module.sitemesh.filter.PageFilter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Currently the main purpose for overwritting the filter is to store the page object in the request so underlying
 * plugin web-items and sections can get access to the page properties and can perform conditional logic
 */
public class SitemeshPageFilter extends PageFilter
{
    public static final String SITEMESH_PAGE = "sitemesh-page-properties";

    @Override
    protected void applyDecorator(final Page page, final Decorator decorator, final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
    {
        request.setAttribute(SITEMESH_PAGE, page.getProperties());
        super.applyDecorator(page, decorator, request, response);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
