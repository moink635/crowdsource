package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.search.builder.QueryBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LDAPConfigurationTesterImpl implements LDAPConfigurationTester
{

    private final DirectoryInstanceLoader directoryInstanceLoader;

    public LDAPConfigurationTesterImpl(final DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }

    public boolean canFindLdapObjects(final LDAPConfiguration configuration, final Strategy strategy) throws OperationFailedException
    {
        final Map<String, String> directoryAttributes = new HashMap<String, String>();
        configuration.populateDirectoryAttributesForConnectionTest(directoryAttributes);
        final RemoteDirectory remoteDirectory = directoryInstanceLoader.getRawDirectory(configuration.getId(), configuration.getImplementationClass(), directoryAttributes);
        final List<String> results = strategy.search(remoteDirectory.getAuthoritativeDirectory(), QueryBuilder.queryFor(String.class, strategy.getEntityDescriptor()).returningAtMost(1));
        return !results.isEmpty();
    }

}
