package com.atlassian.crowd.console.action.directory;

import java.util.Map;

interface LDAPConfiguration
{

    String getGroupObjectFilter();

    String getGroupDNaddition();

    String getGroupDescriptionAttr();

    String getGroupMemberAttr();

    String getGroupNameAttr();

    String getGroupObjectClass();

    String getUserObjectFilter();

    String getUserDNaddition();

    String getUserObjectClass();

    String getUserGroupMemberAttr();

    String getUserFirstnameAttr();

    String getUserLastnameAttr();

    String getUserMailAttr();

    String getUserNameAttr();

    String getUserNameRdnAttr();

    String getUserPasswordAttr();

    String getUserDisplayNameAttr();

    String getUserExternalIdAttr();

    void populateDirectoryAttributesForConnectionTest(Map<String, String> attributes);

    Long getId();

    String getImplementationClass();

}
