/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.group;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.util.SelectionUtils;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.List;

public class AddGroup extends BaseAction
{
    private static final Logger logger = Logger.getLogger(AddGroup.class);

    private boolean active;
    private String description;
    private String name;
    private long directoryID = -1;
    protected Directory directory;

    public String doDefault()
    {
        active = true;

        directoryID = SelectionUtils.getSelectedDirectory(getDirectories());

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                return INPUT;
            }


            GroupTemplate groupT = syncFieldsToGroup();

            directory = directoryManager.findDirectoryById(directoryID);
            Group group = directoryManager.addGroup(directoryID, groupT);
            SelectionUtils.saveSelectedDirectory(directoryID);

            syncFieldsFromGroup(group);

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    protected void doValidation()
    {

        if (directoryID == -1)
        {
            addFieldError("directoryID", getText("group.directory.invalid"));
        }
        else
        {
            try
            {
                directoryManager.findDirectoryById(directoryID);
            }
            catch (DirectoryNotFoundException e)
            {
                //directory not found
                addFieldError("directoryID", getText("group.directory.invalid"));
            }
        }

        if (StringUtils.isBlank(name))
        {
            addFieldError("name", getText("group.name.invalid"));
        }
        else if (IdentifierUtils.hasLeadingOrTrailingWhitespace(name))
        {
            addFieldError("name", getText("invalid.whitespace"));
        }
        else
        {
            try
            {
                directoryManager.findGroupByName(directoryID, name);

                // this isn't good, this name already exist
                addFieldError("name", getText("invalid.namealreadyexist"));
            }
            catch (Exception e)
            {
                // ignore
            }
        }

        if (description == null || (!description.isEmpty() && StringUtils.isWhitespace(description)))
        {
            addFieldError("description", getText("group.description.invalid"));
        }
    }

    /**
     * Ensures that the  <code>group</code> contains the same values as the object fields (eg <code>name</code>, <code>active</code>).
     */
    protected GroupTemplate syncFieldsToGroup()
    {
        GroupTemplate groupT = new GroupTemplate(getName(), directoryID, GroupType.GROUP);
        groupT.setDescription(getDescription());
        groupT.setActive(isActive());
        return groupT;
    }

    /**
     * Ensures that the fields (eg <code>name</code>, <code>active</code>) are the same as the values in the <code>group</code>
     */
    protected void syncFieldsFromGroup(Group group)
    {
        if (group != null)
        {
            setName(group.getName());
            setDescription(group.getDescription());
            setActive(group.isActive());
        }
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(long directoryID)
    {
        this.directoryID = directoryID;
    }

    public List<Directory> getDirectories()
    {
        return directoryManager.findAllDirectories();
    }

    public Directory getDirectory()
    {
        return directory;
    }
}
