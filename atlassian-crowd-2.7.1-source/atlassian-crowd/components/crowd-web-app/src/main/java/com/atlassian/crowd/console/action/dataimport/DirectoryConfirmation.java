package com.atlassian.crowd.console.action.dataimport;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.importer.config.DirectoryConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.manager.ImporterManager;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.interceptor.SessionAware;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Action class to handle the confimation of the Directory mappings
 */
public class DirectoryConfirmation extends BaseImporter implements SessionAware
{
    private Map session;

    private ImporterManager importerManager;

    private DirectoryConfiguration configuration;

    public String execute() throws Exception
    {
        configuration = (DirectoryConfiguration) session.get(BaseImporter.IMPORTER_CONFIGURATION);
        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doExecute()
    {
        // If we are happy with the configuration, get the importer and process the result
        if (getConfiguration() != null)
        {
            try
            {
                result = importerManager.performImport(getConfiguration());
            }
            catch (ImporterException e)
            {
                logger.error(e.getMessage(), e);
                addActionError("An error occured performing import: " + e.getMessage());
            }
        }

        if (!hasErrors())
        {
            // now remove the CSV Configuration from the session
            session.remove(IMPORTER_CONFIGURATION);

            return SUCCESS;
        }
        else
        {
            return ERROR;
        }
    }

    public String getSourceDirectoryName() throws DirectoryNotFoundException
    {
        String directoryName = "";
        if (getConfiguration() != null && getConfiguration().getDirectoryID() != null)
        {
            Directory directory = directoryManager.findDirectoryById(getConfiguration().getSourceDirectoryID());
            directoryName = directory.getName();

        }
        return directoryName;
    }

    public String getTargetDirectoryName() throws DirectoryNotFoundException
    {
        String directoryName = "";
        if (getConfiguration() != null && getConfiguration().getSourceDirectoryID() != null)
        {
            Directory directory = directoryManager.findDirectoryById(getConfiguration().getDirectoryID());
            directoryName = directory.getName();

        }
        return directoryName;
    }

    public boolean getDoNestedGroupImport() throws DirectoryInstantiationException, DirectoryNotFoundException
    {
        if (getConfiguration() != null)
        {
            return getConfiguration().getImportNestedGroups();
        }

        return false;
    }

    public String getOverwriteDirectory()
    {
        String overwrite = "";
        if (getConfiguration() != null && getConfiguration().isOverwriteTarget() != null)
        {
            overwrite = StringUtils.capitalize(getConfiguration().isOverwriteTarget().toString());
        }
        return overwrite;
    }

    public String doImport()
    {
        return SUCCESS;
    }

    public void setSession(Map session)
    {
        this.session = session;
    }

    public DirectoryConfiguration getConfiguration()
    {
        if (configuration == null)
        {
            configuration = (DirectoryConfiguration) session.get(BaseImporter.IMPORTER_CONFIGURATION);
        }
        return configuration;
    }

    public void setConfiguration(DirectoryConfiguration configuration)
    {
        this.configuration = configuration;
    }

    public void setImporterManager(ImporterManager importerManager)
    {
        this.importerManager = importerManager;
    }
}
