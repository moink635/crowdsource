package com.atlassian.crowd.console.logging;

import com.atlassian.crowd.console.event.BootstrapContextInitialisedEvent;
import org.apache.log4j.Appender;
import org.apache.log4j.LogManager;
import org.slf4j.bridge.SLF4JBridgeHandler;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;

import java.util.logging.Handler;

/**
 * If the CrowdHomeLogAppender has been configured, then this listener is responsible for configuring it to
 * redirect logging to the crowd home directory once the bootstrap context is initialised. If the
 * CrowdHomeLogAppender is not being used then this listener does nothing.
 * <p/>
 * This listener is also responsible for passing any required configuration onto the CrowdHomeLogAppender
 * since it will have been created by the log4j framework which limits how it could be configured.
 */
public class CrowdLoggingConfigurationListener implements ApplicationListener
{
    /**
     * The assumed appender name if one is not configured.
     */
    private final static String DEFAULT_APPENDER_NAME = "crowdlog";

    private String logFileName;

    /**
     * The name of the crowd appender being used in logging
     */
    private String appenderName;

    /**
     * Configuration to be passed to the CrowdHomeLogAppender when it switches to file logging.
     *
     * @param file just the log file name (not the full path)
     */
    public void setLogFileName(String file)
    {
        logFileName = file;
    }

    public String getLogFileName()
    {
        return logFileName;
    }

    /**
     * @param name the name of the CrowdLoggingConfigurationListener in the log4j configuration.
     */
    public void setAppenderName(String name)
    {
        appenderName = name;
    }

    public String getAppenderName()
    {
        return appenderName;
    }

    public void onApplicationEvent(ApplicationEvent applicationEvent)
    {

        if (applicationEvent instanceof BootstrapContextInitialisedEvent)
        {
            assert com.atlassian.config.util.BootstrapUtils.getBootstrapManager().isBootstrapped() :
                    "BootstrapContextInitialisedEvent raised before Bootstrap is complete";

            String aname = getAppenderName();
            if (aname == null)
            {
                aname = DEFAULT_APPENDER_NAME;
            }

            Appender appender = LogManager.getRootLogger().getAppender(aname);
            if (appender instanceof CrowdHomeLogAppender)
            {
                CrowdHomeLogAppender homeAppender = (CrowdHomeLogAppender) appender;
                homeAppender.setLogFileName(getLogFileName());
                homeAppender.switchAppender();
            }

            // Attempt to register SLF4JBridgeHandler (necessary for proper plugin integration like gadgets)
            // https://extranet.atlassian.com/display/DEV/Atlassian+Plugins+2.3+Migration+Guide
            initSlf4j();
        }
    }

    private void initSlf4j()
    {
        //check if we've already got the SLF4JBridgeHandler registered.  This can be the case under Tomcat.
        //see the included logging.properties in the webapp for more info.
        final Handler[] handlers = java.util.logging.LogManager.getLogManager().getLogger("").getHandlers();
        if (handlers != null)
        {
            for (Handler handler : handlers)
            {
                if (handler instanceof SLF4JBridgeHandler)
                {
                    return;
                }
            }
        }

        SLF4JBridgeHandler.install();
    }
}