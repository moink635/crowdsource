package com.atlassian.crowd.console.setup;

import com.atlassian.config.ConfigurationException;
import com.atlassian.config.db.DatabaseDetails;
import com.atlassian.config.db.HibernateConfig;
import com.atlassian.config.db.HibernateConfigurator;
import com.atlassian.crowd.util.persistence.hibernate.SchemaHelper;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import org.apache.log4j.Logger;

import java.util.Enumeration;
import java.util.Iterator;
import java.util.Properties;

public class DefaultHibernateConfigurator implements HibernateConfigurator
{
    private static final Logger logger = Logger.getLogger(DefaultHibernateConfigurator.class);

    public synchronized void configureDatabase(DatabaseDetails dbDetails, boolean embedded)
            throws ConfigurationException
    {
        if (embedded)
        {
            String database_name = getHsqlDbName();
            dbDetails.setDatabaseUrl("jdbc:hsqldb:" + database_name);
        }

        Properties properties = new Properties();
        properties.setProperty("hibernate.connection.driver_class", dbDetails.getDriverClassName());
        properties.setProperty("hibernate.connection.url", dbDetails.getDatabaseUrl());
        properties.setProperty("hibernate.connection.username", dbDetails.getUserName());
        properties.setProperty("hibernate.connection.password", dbDetails.getPassword());
        properties.setProperty("hibernate.dialect", dbDetails.getDialect());
        properties.setProperty("hibernate.c3p0.max_size", "" + dbDetails.getPoolSize());
        properties.setProperty("hibernate.c3p0.min_size", "0");
        properties.setProperty("hibernate.c3p0.timeout", "30");
        properties.setProperty("hibernate.c3p0.max_statements", "0");
        properties.setProperty("hibernate.c3p0.acquire_increment", "1");
        properties.setProperty("hibernate.c3p0.idle_test_period", "100");

        if (dbDetails.getExtraHibernateProperties() != null)
        {
            Properties extraHibernateConnectionSettings = dbDetails.getExtraHibernateProperties();
            Enumeration enu = extraHibernateConnectionSettings.keys();

            while (enu.hasMoreElements())
            {
                String key = (String) enu.nextElement();

                properties.setProperty(key, extraHibernateConnectionSettings.getProperty(key));
            }
        }

        refreshSpringContextAndCreateDatabase(properties);
    }

    protected String getHsqlDbName()
    {
        return com.atlassian.config.util.BootstrapUtils.getBootstrapManager().getConfiguredApplicationHome() + "/database/defaultdb";
    }

    public synchronized void configureDatasource(String datasourceName, String dialect) throws ConfigurationException
    {
        Properties properties = new Properties();
        properties.setProperty("hibernate.connection.datasource", datasourceName);
        properties.setProperty("hibernate.dialect", dialect);

        refreshSpringContextAndCreateDatabase(properties);
    }

    public void unconfigureDatabase()
    {
        Properties properties = com.atlassian.config.util.BootstrapUtils.getBootstrapManager().getHibernateProperties();
        for (Iterator ot = properties.keySet().iterator(); ot.hasNext();)
        {
            com.atlassian.config.util.BootstrapUtils.getBootstrapManager().removeProperty((String) ot.next());
        }

        try
        {
            com.atlassian.config.util.BootstrapUtils.getBootstrapManager().save();
        }
        catch (ConfigurationException e)
        {
            logger.error("Unable to unconfigure failed database config: " + e.getMessage(), e);
        }
    }

    public void refreshSpringContextAndCreateDatabase(Properties properties) throws ConfigurationException
    {
        // unconfigure any existing hibernate database
        unconfigureDatabase();

        if (logger.isDebugEnabled())
        {
            logger.debug("Configuring database with properties: " + properties);
        }

        String key;

        for (Iterator iter = properties.keySet().iterator(); iter.hasNext();)
        {
            key = (String) iter.next();
            com.atlassian.config.util.BootstrapUtils.getBootstrapManager().setProperty(key, properties.getProperty(key));
        }

        // If you don't save here, then when we refresh the bootstrap context, we lose
        // all our hibernate config properties, which is a bit bad considering we're going
        // to want to use them to configure the database.
        com.atlassian.config.util.BootstrapUtils.getBootstrapManager().save();

        //refresh the context to reload/load components. we need to load the Local sessionfactory
        //in our container with the mappings now we know what our connection properties are
        ContainerContext ctx = ContainerManager.getInstance().getContainerContext();
        ctx.refresh();
        logger.debug("Spring Context loaded");

        SchemaHelper schemaHelper = (SchemaHelper) ctx.getComponent(SchemaHelper.COMPONENT_REFERENCE);
        schemaHelper.createSchema();

        // Make sure we get a new bootstrap manager here, as any one we may have had before
        // was made defunct by the refresh()
        com.atlassian.config.util.BootstrapUtils.getBootstrapManager().setProperty(HibernateConfig.HIBERNATE_SETUP, "true");
    }

}