package com.atlassian.crowd.console.action.options;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.xwork.RequireSecurityToken;
import com.atlassian.util.profiling.UtilTimerStack;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

/**
 * Allows configuration of logging and profiling. These could be split to separate classes (& actions), but not as long
 * as they're one page in the UI.
 */
public class UpdateLoggingProfiling extends BaseAction
{
    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(UpdateLoggingProfiling.class);

    protected static final String LOGLEVEL_DEFAULT = "Revert to Default";

    /**
     * TODO:  levelNames[i] is the level name for class classNames[i].  Should look for a better way.
     */
    // Fields for logging.
    protected String[] classNames;
    protected String[] levelNames;
    protected LoggingConfigEntry entry;
    protected List entries;
    private String profileName;         // logging profile - not yet properly supported.

    // Fields for profiling.
    boolean profilingOn;



    public String doDefault()
    {
        try
        {
            processLogging();
            processProfiling();
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
        }

        return SUCCESS;
    }


    //Profile names aren't really important yet since we do not allow users to save their own logging profiles.
    // We may do this in the future (like Confluence).
    @RequireSecurityToken(true)
    public String doUpdateLogging()
    {
        if (!updateLogging())
        {
            return ERROR;
        }

        ServletActionContext.getRequest().setAttribute("updateSuccessful","true");
        return doDefault();
    }

    @RequireSecurityToken(true)
    public String doUpdateProfiling()
    {
        if (!updateProfiling())
        {
            return ERROR;
        }

// Not needed, as the change in button text will alert the user:
//        ServletActionContext.getRequest().setAttribute("updateSuccessful","true");
        return doDefault();
    }

    /**
     * Reads current state of logging and updates class fields to match.
     */
    protected void processLogging()
    {
        List<Logger> listLoggers = makeListOfExplicitLoggers();
        listLoggers.add(Logger.getRootLogger());

        entries = new ArrayList(listLoggers.size());
        classNames = new String[listLoggers.size()];
        levelNames = new String[listLoggers.size()];
        for (Logger log : listLoggers)
        {
            entries.add(new UpdateLoggingProfiling.LoggingConfigEntry(log.getName(), log.getLevel()));
        }
    }

    /**
     * Updates the logging rules upon a "submit" of the UI form by the user.
     * @return true if successful, false otherwise.
     */
    protected boolean updateLogging()
    {
        if (LOGLEVEL_DEFAULT.equals(getProfileName()))
        {
            if (getText(LOGLEVEL_DEFAULT).equals(getProfileName()))
            {
                //Keeping profiling name in case we want to allow users to save own logging profiles in the future.
                setProfileName("");

                InputStream propStream = ClassLoaderUtils.getResourceAsStream(getProfileName() + "log4j.properties",
                    UpdateLoggingProfiling.class);
                if (propStream == null)
                {
                    return handleError();
                }
                Properties props = new Properties();
                try
                {
                    props.load(propStream);
                }
                catch (IOException e)
                {
                    return handleError();
                }
                LogManager.resetConfiguration();
                PropertyConfigurator.configure(props);
            }
        }
        else
        {
            for (int i = 0; i < classNames.length; i++)
            {
                setLevelForLogger(classNames[i], levelNames[i]);
            }
            logger.debug("New log configuration saved");
        }

        processLogging();   // load the updated configuration.
        return true;
    }

    private boolean handleError()
    {
        addActionError(getText("loglevel.defautlog4j.failed.load"));
        processLogging();
        return false;
    }

    protected void setLevelForLogger(String className, String levelName)
    {
        LoggingConfigEntry configEntry = new LoggingConfigEntry(className, levelName);
        if (configEntry.isRoot())
        {
            LogManager.getRootLogger().setLevel(configEntry.level);
        }
        else
        {
            LogManager.getLogger(configEntry.getClazz()).setLevel(configEntry.level);
        }
    }


    private List<Logger> makeListOfExplicitLoggers()
    {
        Enumeration loggers = LogManager.getCurrentLoggers();
        List<Logger> listLoggers = new ArrayList<Logger>();
        while (loggers.hasMoreElements())
        {
            Logger log = (Logger) loggers.nextElement();
            if (log.getLevel() != null && log.getName().startsWith("com.atlassian.crowd"))
            {
                listLoggers.add(log);
            }
        }

        Collections.sort(listLoggers, new Comparator<Logger>()
        {
            public int compare(Logger o1, Logger o2)
            {
                return o1.getName().compareTo(o2.getName());
            }
        });
        return listLoggers;
    }

    /**
     * Updates the class profiling fields to reflect the current status of profiling.
     */
    protected void processProfiling()
    {
        profilingOn = UtilTimerStack.isActive();
    }

    protected boolean updateProfiling()
    {
        try
        {
            System.setProperty(UtilTimerStack.MIN_TIME, "1");
            UtilTimerStack.setActive(profilingOn);
            processLogging();
            return true;
        }
        catch (RuntimeException e)
        {
            logger.error("Error trying to update profiling settings", e);
            processLogging();
            return false;
        }
    }

    public List getEntries()
    {
        return entries;
    }

    public void setEntries(List entries)
    {
        this.entries = entries;
    }

    public void setClassNames(String[] classNames)
    {
        this.classNames = classNames;
    }

    public void setLevelNames(String[] levelNames)
    {
        this.levelNames = levelNames;
    }

    public String getProfileName()
    {
        return profileName;
    }

    public void setProfileName(String profileName)
    {
        this.profileName = profileName;
    }

    public boolean isProfilingOn()
    {
        return profilingOn;
    }

    public void setProfilingOn(boolean profilingOn)
    {
        this.profilingOn = profilingOn;
    }


    /**
     * Represents a Log4j Config entry, corresponding to a Class or Package mapped to a Level.
     */
    protected static class LoggingConfigEntry implements Comparable<LoggingConfigEntry>
    {
        private static final String ROOT = "root";

        public LoggingConfigEntry()
        {
        }

        public boolean isRoot()
        {
            return clazz.equals(ROOT);
        }

        /**
         * String as we may need to preserve additivity.
         */
        protected String clazz;
        protected Level level;

        public LoggingConfigEntry(String clazz, Level level)
        {
            this.clazz = clazz;
            this.level = level;
        }

        public LoggingConfigEntry(String clazz, String level)
        {
            this.clazz = clazz;
            this.level = Level.toLevel(level);
        }

        public String getClazz()
        {
            return clazz;
        }

        /**
         * OGNL forces me to match getter and setter types. Possibly this could be avoided by use of a
         * TypeConverter.
         *
         * @return the string
         */
        public String getLevel()
        {
            return level.toString();
        }

        public void setClazz(String clazz)
        {
            this.clazz = clazz;
        }

        public void setLevel(String level)
        {
            this.level = Level.toLevel(level);
        }

        public int compareTo(LoggingConfigEntry o)
        {
            return this.clazz.compareTo(o.getClazz());
        }
    }
}
