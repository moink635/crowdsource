package com.atlassian.crowd.console.interceptor;

import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManagerImpl;
import com.atlassian.crowd.xwork.interceptors.XWorkTransactionInterceptor;
import com.atlassian.spring.container.ContainerManager;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionInvocation;
import org.springframework.transaction.PlatformTransactionManager;

/**
 * An interceptor that wraps the action execution in a single Hibernate transaction.
 */
public class CrowdXWorkTransactionInterceptor extends XWorkTransactionInterceptor
{
    /**
     * Delegate used to create, commit and rollback transactions
     */
    private PlatformTransactionManager transactionManager;


    public PlatformTransactionManager getTransactionManager()
    {
        if (transactionManager == null)
        {
            transactionManager = (PlatformTransactionManager) ContainerManager.getComponent("transactionManager");
        }

        return transactionManager;
    }

    protected boolean shouldIntercept(ActionInvocation actionInvocation)
    {
        return (CrowdBootstrapManagerImpl.isContainterReady(ServletActionContext.getServletContext()))
                && (actionInvocation != null);
    }
}
