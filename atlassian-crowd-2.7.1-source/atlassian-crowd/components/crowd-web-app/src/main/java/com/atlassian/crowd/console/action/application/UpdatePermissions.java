package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.xwork.ActionContext;
import org.apache.commons.lang3.StringUtils;

public class UpdatePermissions extends ViewApplication
{
    @RequireSecurityToken(true)
    public String doUpdate()
    {

        if (StringUtils.isNotBlank(directoryId) && ID != -1L)
        {
            try
            {
                Directory directory = directoryManager.findDirectoryById(new Long(directoryId));

                for (OperationType operationType : OperationType.values())
                {
                    String name = operationType.name();

                    // Get the permission for name, if 'true', add the permission
                    String[] param = (String[]) ActionContext.getContext().getParameters().get(name);
                    if (param != null && param.length > 0 && Boolean.valueOf(param[0]))
                    {
                        permissionManager.addPermission(getApplication(), directory, operationType);
                    }
                    else
                    {
                        permissionManager.removePermission(getApplication(), directory, operationType);
                    }
                }
            }
            catch (Exception e)
            {
                logger.error(e.getMessage(), e);
                addActionError(e);
                return INPUT;
            }
        }
        else
        {
            addActionError(getText("application.permission.directory.error"));
            return INPUT;
        }
        return SUCCESS;
    }


    public void setID(long ID)
    {
        // Overriding setID, so we can force the page to be reloaded correctly on error, via a call to doDefault.
        super.setID(ID);
        doDefault();
    }
}
