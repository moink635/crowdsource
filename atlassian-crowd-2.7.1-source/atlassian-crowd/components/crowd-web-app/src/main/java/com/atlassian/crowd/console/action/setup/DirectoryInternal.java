package com.atlassian.crowd.console.action.setup;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import com.atlassian.core.util.PairType;
import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;

import com.google.common.collect.Ordering;
import com.opensymphony.util.TextUtils;

import org.apache.log4j.Logger;

import static com.atlassian.crowd.directory.InternalDirectory.ATTRIBUTE_PASSWORD_COMPLEXITY_MESSAGE;
import static com.atlassian.crowd.directory.InternalDirectory.ATTRIBUTE_PASSWORD_HISTORY_COUNT;
import static com.atlassian.crowd.directory.InternalDirectory.ATTRIBUTE_PASSWORD_MAX_ATTEMPTS;
import static com.atlassian.crowd.directory.InternalDirectory.ATTRIBUTE_PASSWORD_MAX_CHANGE_TIME;
import static com.atlassian.crowd.directory.InternalDirectory.ATTRIBUTE_PASSWORD_REGEX;
import static com.atlassian.crowd.directory.InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD;
import static com.atlassian.crowd.embedded.api.DirectoryType.INTERNAL;
import static com.atlassian.crowd.embedded.api.OperationType.CREATE_GROUP;
import static com.atlassian.crowd.embedded.api.OperationType.CREATE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.DELETE_GROUP;
import static com.atlassian.crowd.embedded.api.OperationType.DELETE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_GROUP;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_GROUP_ATTRIBUTE;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_USER_ATTRIBUTE;

/**
 * Create a default internal directory during setup.
 */
public class DirectoryInternal extends BaseSetupAction
{
    private static final Logger logger = Logger.getLogger(DirectoryInternal.class);

    public static final String DIRECTORY_INTERNAL_STEP = "directoryinternalsetup";

    protected long ID;
    protected boolean active = true;
    protected String name;
    protected String description;
    private String passwordRegex = "";
    private String passwordComplexityMessage  = "";
    private long passwordMaxAttempts = 0;
    private long passwordMaxChangeTime = 0;
    private long passwordHistoryCount = 0;

    /* This is presented as the default for new internal directories */
    private String userEncryptionMethod = PasswordEncoderFactory.ATLASSIAN_SECURITY_ENCODER;
    private PasswordEncoderFactory passwordEncoderFactory;
    private PropertyManager propertyManager;


    public String doDefault()
    {
        // check if i'm at the correct step
        String setupDefault = super.doDefault();
        if (!setupDefault.equals(SUCCESS))
        {
            return setupDefault;
        }

        try
        {
            name = propertyManager.getDeploymentTitle();
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    public String getStepName()
    {
        return DIRECTORY_INTERNAL_STEP;
    }

    public String doUpdate()
    {
        // check if i'm at the correct step
        String setupUpdate = super.doUpdate();
        if (!setupUpdate.equals(SUCCESS))
        {
            return setupUpdate;
        }

        try
        {
            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                return INPUT;
            }

            DirectoryImpl directoryToUpdate = new DirectoryImpl();

            directoryToUpdate.setActive(active);
            directoryToUpdate.setDescription(description);
            directoryToUpdate.setName(name);
            directoryToUpdate.setType(INTERNAL);

            directoryToUpdate.setAttribute(ATTRIBUTE_PASSWORD_REGEX, passwordRegex);
            directoryToUpdate.setAttribute(ATTRIBUTE_PASSWORD_COMPLEXITY_MESSAGE, passwordComplexityMessage);
            directoryToUpdate.setAttribute(ATTRIBUTE_PASSWORD_MAX_ATTEMPTS, Long.toString(passwordMaxAttempts));
            directoryToUpdate.setAttribute(ATTRIBUTE_PASSWORD_MAX_CHANGE_TIME, Long.toString(passwordMaxChangeTime));
            directoryToUpdate.setAttribute(ATTRIBUTE_PASSWORD_HISTORY_COUNT, Long.toString(passwordHistoryCount));
            directoryToUpdate.setAttribute(ATTRIBUTE_USER_ENCRYPTION_METHOD, userEncryptionMethod);

            directoryToUpdate.setImplementationClass(InternalDirectory.class.getName());

            directoryToUpdate.setAllowedOperations(EnumSet.of(
                    CREATE_GROUP, CREATE_USER,
                    DELETE_GROUP, DELETE_USER,
                    UPDATE_GROUP, UPDATE_USER,
                    UPDATE_GROUP_ATTRIBUTE, UPDATE_USER_ATTRIBUTE));

            Directory directory = directoryManager.addDirectory(directoryToUpdate);

            ID = directory.getId();

            getSetupPersister().progessSetupStep();

            return SELECT_SETUP_STEP;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }


    protected void doValidation()
    {
        if (name == null || name.equals(""))
        {
            addFieldError("name", getText("directoryinternal.name.invalid"));
        }
        else
        {
            try
            {
                directoryManager.findDirectoryByName(name);

                addFieldError("name", getText("invalid.namealreadyexist"));
            }
            catch (Exception e)
            {
                logger.debug(e);
            }
        }

        try
        {
            if (passwordRegex != null)
            {
                Pattern.compile(passwordRegex);
            }
        }
        catch (PatternSyntaxException e)
        {
            addFieldError("passwordRegex", e.getMessage());
        }

        if (passwordMaxChangeTime < 0)
        {
            addFieldError("passwordMaxChangeTime", getText("directoryinternal.passwordmaxchangetime.invalid"));
        }

        if (passwordMaxAttempts < 0)
        {
            addFieldError("passwordMaxAttempts", getText("directoryinternal.passwordmaxattempts.invalid"));
        }

        if (passwordHistoryCount < 0)
        {
            addFieldError("passwordHistoryCount", getText("directoryinternal.passwordhistorycount.invalid"));
        }

        if (!TextUtils.stringSet(userEncryptionMethod))
        {
            addFieldError("userEncryptionMethod", getText("directoryinternal.userencryptionmethod.invalid"));
        }
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getPasswordRegex()
    {
        return passwordRegex;
    }

    public void setPasswordRegex(String passwordRegex)
    {
        this.passwordRegex = passwordRegex;
    }

    public long getPasswordMaxAttempts()
    {
        return passwordMaxAttempts;
    }

    public void setPasswordMaxAttempts(long passwordMaxAttempts)
    {
        this.passwordMaxAttempts = passwordMaxAttempts;
    }

    public long getPasswordMaxChangeTime()
    {
        return passwordMaxChangeTime;
    }

    public void setPasswordMaxChangeTime(long passwordMaxChangeTime)
    {
        this.passwordMaxChangeTime = passwordMaxChangeTime;
    }

    public long getPasswordHistoryCount()
    {
        return passwordHistoryCount;
    }

    public void setPasswordHistoryCount(long passwordHistoryCount)
    {
        this.passwordHistoryCount = passwordHistoryCount;
    }

    public String getUserEncryptionMethod()
    {
        return userEncryptionMethod;
    }

    public void setUserEncryptionMethod(String userEncryptionMethod)
    {
        this.userEncryptionMethod = userEncryptionMethod;
    }

    public PasswordEncoderFactory getPasswordEncoderFactory()
    {
        return passwordEncoderFactory;
    }

    public void setPasswordEncoderFactory(PasswordEncoderFactory passwordEncoderFactory)
    {
        this.passwordEncoderFactory = passwordEncoderFactory;
    }

    public String getPasswordComplexityMessage()
    {
        return passwordComplexityMessage;
    }

    public void setPasswordComplexityMessage(String passwordComplexityMessage)
    {
        this.passwordComplexityMessage = passwordComplexityMessage;
    }

    public List<PairType> getUserEncryptionMethods()
    {
        List<PairType> encoders = new ArrayList<PairType>();
        for (String encoder : Ordering.<String>natural().immutableSortedCopy(this.passwordEncoderFactory.getSupportedInternalEncoders()))
        {
            encoders.add(new PairType(encoder, encoder.toUpperCase()));

        }
        return encoders;
    }

    public long getID()
    {
        return ID;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}