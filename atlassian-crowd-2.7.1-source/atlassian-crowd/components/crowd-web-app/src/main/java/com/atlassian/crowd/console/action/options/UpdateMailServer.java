package com.atlassian.crowd.console.action.options;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.manager.mail.MailManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.util.UserUtils;
import com.atlassian.crowd.util.mail.SMTPServer;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.mail.internet.InternetAddress;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Locale;

public class UpdateMailServer extends BaseAction
{
    private static final Logger logger = Logger.getLogger(UpdateMailServer.class);

    private PropertyManager propertyManager;
    private InitialContext initialContext;
    private MailManager mailManager;

    private String notificationEmail;
    private String from;
    private String prefix;
    private String host;
    private String port = String.valueOf(SMTPServer.DEFAULT_MAIL_PORT);
    private String username;
    private String password;
    private String useSSL;
    private String jndiLocation;

    // Are we using JNDI Mail ... this is really a boolean
    private String jndiMailActive = Boolean.FALSE.toString().toLowerCase(Locale.ENGLISH);

    public String doDefault()
    {
        if (mailManager.isConfigured() == false)
        {
            addActionError(getText("mailserver.unconfigured.warning"));
        }

        try
        {
            SMTPServer smtpServer = propertyManager.getSMTPServer();

            from = smtpServer.getFrom().toString();
            prefix = smtpServer.getPrefix();
            host = smtpServer.getHost();
            port = String.valueOf(smtpServer.getPort());
            username = smtpServer.getUsername();
            password = smtpServer.getPassword();
            useSSL = String.valueOf(smtpServer.getUseSSL());
            jndiLocation = smtpServer.getJndiLocation();
            notificationEmail = propertyManager.getNotificationEmail();
            jndiMailActive = Boolean.toString(smtpServer.isJndiMailActive());

        }
        catch (PropertyManagerException e)
        {
            if (!(e.getCause() instanceof ObjectNotFoundException))
            {
                addActionError(e);
                logger.debug(e.getMessage(), e);
            }
        }

        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            // check for errors
            doValidation();

            if (hasErrors())
            {
                return INPUT;
            }

            SMTPServer server = null;
            if (Boolean.valueOf(jndiMailActive).booleanValue())
            {
                server = new SMTPServer(jndiLocation, new InternetAddress(from), prefix);
            }
            else
            {
                server = new SMTPServer(Integer.parseInt(port), prefix, new InternetAddress(from), password, username, host, Boolean.parseBoolean(useSSL));
            }

            propertyManager.setSMTPServer(server);
            
            propertyManager.setNotificationEmail(notificationEmail);

            ServletActionContext.getRequest().setAttribute("updateSuccessful", "true");

            return SUCCESS;
        }
        catch (Exception e)
        {
            logger.error("An exception occured updating the mail server", e);

            addActionError(StringUtils.isNotBlank(e.getMessage()) ? e.getMessage() : "An exception occured updating the mail server");
        }

        return INPUT;
    }

    protected void doValidation()
    {

        if (StringUtils.isBlank(from))
        {
            addFieldError("from", getText("mailserver.from.invalid"));
        }
        else
        {
            if (!UserUtils.isValidEmail(from))
            {
                addFieldError("from", getText("mailserver.from.invalid"));
            }
        }

        if (StringUtils.isBlank(notificationEmail) || !UserUtils.isValidEmail(notificationEmail))
        {
            addFieldError("notificationEmail", getText("mailserver.notification.invalid"));
        }

        if (Boolean.valueOf(jndiMailActive).booleanValue())
        {
            if (StringUtils.isBlank(jndiLocation))
            {
                addFieldError("jndiLocation", getText("mailserver.jndiLocation.invalid"));
            }
            else
            {
                try
                {
                    initialContext.lookup(jndiLocation);
                }
                catch (NamingException e)
                {
                    addFieldError("jndiLocation", e.getMessage());
                }
            }
        }
        else
        {
            if (StringUtils.isBlank(host))
            {
                addFieldError("host", getText("mailserver.host.invalid"));
            }

            if (StringUtils.isNotBlank(port))
            {
                try
                {
                    Integer.parseInt(port);
                }
                catch (NumberFormatException e)
                {
                    addFieldError("port", getText("mailserver.port.invalid") );
                }
            }
        }
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public void setPrefix(String prefix)
    {
        this.prefix = prefix;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getNotificationEmail()
    {
        return notificationEmail;
    }

    public void setNotificationEmail(String notificationEmail)
    {
        this.notificationEmail = notificationEmail;
    }

    public String getPort()
    {
        return port;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    public String getUseSSL()
    {
        return useSSL;
    }

    public void setUseSSL(String useSSL)
    {
        this.useSSL = useSSL;
    }

    public String getJndiLocation()
    {
        return jndiLocation;
    }

    public void setJndiLocation(String jndiLocation)
    {
        this.jndiLocation = jndiLocation;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    public String getJndiMailActive()
    {
        return jndiMailActive;
    }

    public void setJndiMailActive(String jndiMailActive)
    {
        this.jndiMailActive = jndiMailActive;
    }

    public void setInitialContext(InitialContext initialContext)
    {
        this.initialContext = initialContext;
    }

    public void setMailManager(MailManager mailManager)
    {
        this.mailManager = mailManager;
    }
}