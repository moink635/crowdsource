package com.atlassian.crowd.console.listener;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.atlassian.config.HomeLocator;
import com.atlassian.config.bootstrap.AtlassianBootstrapManager;
import com.atlassian.config.lifecycle.events.ApplicationStartedEvent;
import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.crowd.event.application.ApplicationReadyEvent;
import com.atlassian.crowd.manager.license.CrowdLicenseManager;
import com.atlassian.crowd.manager.upgrade.UpgradeManager;
import com.atlassian.crowd.migration.legacy.database.DatabaseMigrationManager;
import com.atlassian.crowd.plugin.RequiredPluginsStartupCheck;
import com.atlassian.crowd.util.ConsoleResourceLocator;
import com.atlassian.crowd.util.SystemInfoHelper;
import com.atlassian.crowd.util.SystemInfoHelperImpl;
import com.atlassian.crowd.util.build.BuildUtils;
import com.atlassian.crowd.util.persistence.hibernate.SchemaHelper;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.config.JohnsonConfig;
import com.atlassian.johnson.event.Event;
import com.atlassian.spring.container.ContainerManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

/**
 * Performs upgrades to Crowd based on the build in the database and the build number for the current version of Crowd.
 * If startup is successful, notifies listeners that Crowd is up and running.
 */
public class StartupListener implements ServletContextListener
{
    private static final Logger log = Logger.getLogger(StartupListener.class);
    private JohnsonConfig johnsonConfig;

    public void contextInitialized(ServletContextEvent servletContextEvent)
    {
        johnsonConfig = Johnson.getConfig();
        assert johnsonConfig != null;

        Collection<String> errors = new ArrayList<String>();

        JohnsonEventContainer agentJohnson = Johnson.getEventContainer(servletContextEvent.getServletContext());
        AtlassianBootstrapManager bootstrapManager = BootstrapUtils.getBootstrapManager();

        // only consider upgrades if setup is complete (we need db to be up)
        if (bootstrapManager.isSetupComplete())
        {
            if (isLicenseValid(servletContextEvent))
            {
                // Attempt to migrate and upgrade crowd
                errors.addAll(migrateAndUpgradeCrowd(servletContextEvent));

                // check if an error has occurred during the upgrade process, this is bad.
                // report the errors via Johnson
                if (!errors.isEmpty() || agentJohnson.hasEvents())
                {
                    for (Object error : errors)
                    {
                        String errorMsg = (String) error;

                        agentJohnson.addEvent(new Event(johnsonConfig.getEventType("bootstrap"),
                                "Crowd has encountered errors while upgrading. Please resolve these errors and restart Crowd.", errorMsg,
                                johnsonConfig.getEventLevel("fatal")));
                    }

                    bootstrapManager.setSetupComplete(false);
                    log.fatal("Errors experienced during the Crowd upgrade process: " + errors);
                }
            }
            else
            {
                bootstrapManager.setSetupComplete(false); // "lockup" crowd
                log.info("License is invalid. Upgrades were not performed.");
            }
        }

        // Re-check if bootstrap manager is setup - in case an error occurred during migration/upgrade
        if (bootstrapManager.isSetupComplete())
        {
            // write out the system info to the log
            SystemInfoHelper systemInfoHelper = ContainerManager.getComponent("systemInfoHelper", SystemInfoHelper.class);
            systemInfoHelper.printSystemInfo(BootstrapLoaderListener.STARTUP_LOG);
        }
        else
        {
            // don't have spring context, print something out tho
            new SystemInfoHelperImpl().printMinimalSystemInfo(BootstrapLoaderListener.STARTUP_LOG);
            log.info("Upgrades not performed since the application has not been setup yet.");
        }

        if (ContainerManager.isContainerSetup() && bootstrapManager.isSetupComplete())
        {
            EventPublisher eventPublisher = ContainerManager.getComponent("eventPublisher", EventPublisher.class);

            // Let the plugin manager know that we need to initialise
            eventPublisher.publish(new ApplicationStartedEvent(this));

            RequiredPluginsStartupCheck check = ContainerManager.getComponent("requiredPluginsStartupCheck", RequiredPluginsStartupCheck.class);

            Collection<String> missing = check.requiredPluginsNotEnabled();

            if (missing.isEmpty())
            {
                // Publish a custom event to notify listeners that the application is ready
                eventPublisher.publish(new ApplicationReadyEvent(this));

                // Log crowd startup message
                // inject the servlet context into the home locator, so we can look at the servlet context parameters
                HomeLocator homeLocator = (HomeLocator) BootstrapUtils.getBootstrapContext().getBean("homeLocator");
                homeLocator.lookupServletHomeProperty(servletContextEvent.getServletContext());

                printStartupMessage(servletContextEvent.getServletContext(), homeLocator);
            }
            else
            {
                agentJohnson.addEvent(new Event(johnsonConfig.getEventType("bootstrap"),
                        "Required plugins failed to initialise. Please check the logs for errors and restart Crowd.",
                        "Not enabled: " + StringUtils.join(missing, ", "),
                        johnsonConfig.getEventLevel("fatal")));
            }
        }
        else if (ContainerManager.isContainerSetup() && bootstrapManager.getHibernateConfig() != null && !bootstrapManager.isSetupComplete())
        {
            // We have a database and we have a container ... kick up the plugin system
            EventPublisher eventPublisher = ContainerManager.getComponent("eventPublisher", EventPublisher.class);
            eventPublisher.publish(new ApplicationStartedEvent(this));
        }
    }

    private void printStartupMessage(ServletContext context, HomeLocator homeLocator)
    {
        // Print successful start up message if there are no events in the context.
        JohnsonEventContainer eventCont = Johnson.getEventContainer(context);
        if (eventCont == null || !eventCont.hasEvents())
        {
            if (BootstrapLoaderListener.STARTUP_LOG.isInfoEnabled())
            {
                String applicationURL = new ConsoleResourceLocator(com.atlassian.crowd.integration.Constants.PROPERTIES_FILE, homeLocator).getProperties().getProperty("application.login.url");
                BootstrapLoaderListener.STARTUP_LOG.info("Starting Crowd Server, Version: " + BuildUtils.getVersion());
                System.out.println();
                System.out.println("*********************************************************************************************");
                System.out.println("*");
                System.out.println("*  You can now use the Crowd server by visiting " + applicationURL);
                System.out.println("*");
                System.out.println("*********************************************************************************************");
                System.out.println();
            }
        }
    }

    private Collection<String> migrateAndUpgradeCrowd(ServletContextEvent servletContextEvent)
    {
        Collection<String> errors = new ArrayList<String>();
        JohnsonEventContainer agentJohnson = Johnson.getEventContainer(servletContextEvent.getServletContext());

        DatabaseMigrationManager databaseMigrationManager = ContainerManager.getComponent("databaseMigrationManager", DatabaseMigrationManager.class);
        UpgradeManager upgradeManager = ContainerManager.getComponent("upgradeManager", UpgradeManager.class);
        SchemaHelper schemaHelper = ContainerManager.getComponent("schemaHelper", SchemaHelper.class);

        try
        {
            // check if Crowd 2.0+ is being pointed at a pre Crowd 2.0 home directory
            if (databaseMigrationManager.isLegacyCrowd())
            {
                // check if it is safe to migrate the database
                errors.addAll(databaseMigrationManager.verifyDatabaseIsSafeToMigrate());

                if (errors.isEmpty())
                {
                    // We are safe so lets get on with it!
                    // first upgrade the schema if required
                    schemaHelper.updateSchemaIfNeeded();

                    // then migrate data from legacy database to 2.0+ database
                    errors.addAll(databaseMigrationManager.doDatabaseMigration());

                    // now try to run the upgrade tasks
                    errors.addAll(upgradeManager.doUpgrade());
                }
            }
            else
            {
                // no need for database migration, just run the upgrade tasks
                // upgrade the schema if required
                schemaHelper.updateSchemaIfNeeded();

                // run upgrade tasks
                errors.addAll(upgradeManager.doUpgrade());
            }
        }
        catch (Exception e)
        {
            log.fatal("Failed to initialise Crowd container", e);
            agentJohnson.addEvent(new Event(johnsonConfig.getEventType("bootstrap"),
                    "Error while trying to initialise Crowd Container. " + e.getMessage(),
                    johnsonConfig.getEventLevel("fatal")));
        }

        return errors;
    }

    private boolean isLicenseValid(ServletContextEvent servletContextEvent)
    {
        JohnsonEventContainer agentJohnson = Johnson.getEventContainer(servletContextEvent.getServletContext());

        CrowdLicenseManager licenseManager = ContainerManager.getComponent("crowdLicenseManager", CrowdLicenseManager.class);
        final CrowdLicense license = licenseManager.getLicense();

        if (license == null)
        {
            log.fatal("License not found");

            agentJohnson.addEvent(new Event(johnsonConfig.getEventType("bootstrap"),
                                            "Crowd cannot find the license.",
                                            johnsonConfig.getEventLevel("fatal")));
            return false;
        }

        boolean isValid = licenseManager.isLicenseValid(license);
        boolean buildWithinMaintenancePeriod = licenseManager.isBuildWithinMaintenancePeriod(license);

        // verify the license
        if (!isValid || !buildWithinMaintenancePeriod)
        {
            MessageFormat expiredFormat = new MessageFormat("Crowd cannot be started as your {0} period ended on {1,date,d MMMM yyyy}.");

            String msg;

            if (!buildWithinMaintenancePeriod)
            {
                // This Crowd was built after the maintenance period
                msg = expiredFormat.format(new Object[]{"maintenance", license.getMaintenanceExpiryDate()});
            }
            else if (license.isExpired())
            {
                // The license has expired
                msg = expiredFormat.format(new Object[]{license.isEvaluation() ? "evaluation" : "support", license.getExpiryDate()});
            }
            else
            {
                msg = "Crowd cannot be started as your license has reached its resource limit.";
            }

            log.fatal(msg);

            agentJohnson.addEvent(new Event(johnsonConfig.getEventType("license-too-old"),
                    msg,
                    johnsonConfig.getEventLevel("fatal")));

            return false;
        }

        if (license.getSupportEntitlementNumber() != null)
        {
            log.info("License is valid. Support Entitlemenet Number (SEN) is " + license.getSupportEntitlementNumber());
        }
        else
        {
            log.info("License is valid, but does not contain Support Entitlemenet Number (SEN)");
        }

        return true;
    }

    public void contextDestroyed(ServletContextEvent servletContextEvent)
    {

    }
}
