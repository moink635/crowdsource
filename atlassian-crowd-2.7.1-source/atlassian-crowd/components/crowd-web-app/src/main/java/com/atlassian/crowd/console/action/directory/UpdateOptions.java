package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.TreeSet;

public class UpdateOptions extends BaseAction
{
    private static final String EXCLUSIONS_PARAM = "&exclusions=";

    private long ID = -1;

    private Directory directory;
    private TreeSet<String> defaultGroups;

    private String groupToRemove;

    private List<String> selectedEntityNames;

    private DirectoryInstanceLoader directoryInstanceLoader;

    @Override
    public String execute() throws Exception
    {
        directory = directoryManager.findDirectoryById(ID);

        defaultGroups = getDefaultGroups(directory);

        return INPUT;
    }

    private TreeSet<String> getDefaultGroups(Directory directory)
    {
        String concatenatedGroupNames = directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_AUTO_ADD_GROUPS);

        if (StringUtils.isNotBlank(concatenatedGroupNames))
        {
            String[] groupNameArray = StringUtils.split(concatenatedGroupNames, DirectoryImpl.AUTO_ADD_GROUPS_SEPARATOR);
            return new TreeSet<String>(Arrays.asList(groupNameArray));
        }
        else
        {
            return new TreeSet<String>();
        }
    }

    private void performDefaultGroupsUpdate(Directory directory, TreeSet<String> defaultGroups)
            throws DirectoryNotFoundException
    {
        DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
        if (defaultGroups.isEmpty())
        {
            directoryToUpdate.removeAttribute(DirectoryImpl.ATTRIBUTE_KEY_AUTO_ADD_GROUPS);
        }
        else
        {
            final String autoAddGroups = StringUtils.join(defaultGroups, DirectoryImpl.AUTO_ADD_GROUPS_SEPARATOR);
            directoryToUpdate.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_AUTO_ADD_GROUPS, autoAddGroups);
        }

        this.directory = directoryManager.updateDirectory(directoryToUpdate);
    }

    @RequireSecurityToken(true)
    public String removeDefaultGroup()
    {
        try
        {
            directory = directoryManager.findDirectoryById(ID);

            if (StringUtils.isNotBlank(groupToRemove))
            {
                defaultGroups = getDefaultGroups(directory);
                defaultGroups.remove(groupToRemove);
                performDefaultGroupsUpdate(directory, defaultGroups);
            }
            return SUCCESS;
        }
        catch (DirectoryNotFoundException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
        catch (RuntimeException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
    }

    @RequireSecurityToken(true)
    public String addDefaultGroups()
    {
        try
        {
            directory = directoryManager.findDirectoryById(ID);

            if (selectedEntityNames != null)
            {
                defaultGroups = getDefaultGroups(directory);

                RemoteDirectory remoteDirectory = directoryInstanceLoader.getDirectory(directory);

                for (String groupName : selectedEntityNames)
                {
                    try
                    {
                        remoteDirectory.findGroupByName(groupName);
                        defaultGroups.add(groupName);
                    }
                    catch (GroupNotFoundException e)
                    {
                        // skip over
                        logger.info("Group not found", e);
                    }
                }

                performDefaultGroupsUpdate(directory, defaultGroups);
            }
            return SUCCESS;
        }
        catch (DirectoryNotFoundException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
        catch (OperationFailedException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
        catch (RuntimeException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
    }

    public long getID()
    {
        return ID;
    }

    public void setID(final long ID)
    {
        this.ID = ID;
    }

    public Directory getDirectory()
    {
        return directory;
    }

    public TreeSet<String> getDefaultGroups()
    {
        return defaultGroups;
    }

    public void setGroupToRemove(final String groupToRemove)
    {
        this.groupToRemove = groupToRemove;
    }

    public void setSelectedEntityNames(final List<String> selectedEntityNames)
    {
        this.selectedEntityNames = selectedEntityNames;
    }

    public String getExclusionsAsString() throws UnsupportedEncodingException
    {
        StringBuilder sb = new StringBuilder();
        for (String groupName : defaultGroups)
        {
            sb.append(EXCLUSIONS_PARAM);
            sb.append(URLEncoder.encode(groupName, "UTF-8"));
        }

        return sb.toString();
    }

    public void setDirectoryInstanceLoader(DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }
}
