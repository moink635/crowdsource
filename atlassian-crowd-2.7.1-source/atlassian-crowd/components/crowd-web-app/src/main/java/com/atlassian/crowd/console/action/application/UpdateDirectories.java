package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;

public class UpdateDirectories extends ViewApplication
{
    private static final Logger logger = Logger.getLogger(ViewApplication.class);

    private long directoryID = -1;
    private long unsubscribedDirectoriesID = -1;
    private final String KEY_BASE = "directory";
    private final String KEY_MANAGEMENTMODE = "-allowAll";

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            processGeneral();

            // get the number of attributes
            int parameterCount = getParameterCount();

            for (int parameter = 1; parameter <= parameterCount; parameter++)
            {
                // get the directory
                String directoryIDParam = KEY_BASE + parameter;
                String strDirectoryID = ServletActionContext.getRequest().getParameter(directoryIDParam);
                long directoryID = Long.parseLong(strDirectoryID);

                // find the directory off the application we are looking for
                for (int directoryCtr = 0; directoryCtr < getApplication().getDirectoryMappings().size(); directoryCtr++)
                {

                    DirectoryMapping directoryMapping = getApplication().getDirectoryMappings().get(directoryCtr);
                    if (directoryMapping.getDirectory().getId() == directoryID)
                    {
                        // pull the view true/false off the parameter post
                        String allowAllDirectoryIDValueParam = KEY_BASE + parameter + KEY_MANAGEMENTMODE;
                        boolean allowAllToAuthenticate = Boolean.valueOf(ServletActionContext.getRequest().getParameter(allowAllDirectoryIDValueParam));
                        if (allowAllToAuthenticate != directoryMapping.isAllowAllToAuthenticate())
                        {
                            // The value has changed, so update
                            directoryMapping.setAllowAllToAuthenticate(allowAllToAuthenticate);
                            applicationManager.updateDirectoryMapping(directoryMapping.getApplication(), directoryMapping.getDirectory(), allowAllToAuthenticate);
                        }

                        break;
                    }
                }
            }

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doRemoveDirectory()
    {
        try
        {
            processGeneral();

            if (directoryID != -1)
            {
                Directory directory = directoryManager.findDirectoryById(directoryID);
                if (isCrowdApplication() && getRemoteUser().getDirectoryId() == directoryID)
                {
                    addActionError(getText("preventlockout.unassigndirectory.label", Arrays.asList(directory.getName())));
                    return INPUT;
                }
                else
                {
                    // Remove this directory and associated attributes from the application
                    applicationManager.removeDirectoryFromApplication(directory, getApplication());
                    return SUCCESS;
                }
            }
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    private int getParameterCount()
    {
        int count = 0;

        Enumeration parameterNames = ServletActionContext.getRequest().getParameterNames();

        while (parameterNames.hasMoreElements())
        {
            String parameterName = (String) parameterNames.nextElement();

            if (parameterName.matches("(" + KEY_BASE + "){1}\\d+"))
            {
                String number = parameterName.substring(KEY_BASE.length());
                int current = Integer.parseInt(number);
                if (current > count)
                {
                    count = current;
                }
            }
        }

        return count;
    }

    @RequireSecurityToken(true)
    public String doAddDirectory()
    {
        try
        {
            processGeneral();

            if (unsubscribedDirectoriesID != -1)
            {
                Directory directory = directoryManager.findDirectoryById(unsubscribedDirectoriesID);

                DirectoryMapping directoryMapping = new DirectoryMapping(getApplication(), directory, false);

                // update the application
                applicationManager.addDirectoryMapping(getApplication(), directory, false, OperationType.values());
            }

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUp()
    {
        try
        {
            processGeneral();

            List<DirectoryMapping> directoryMappings = getApplication().getDirectoryMappings();

            int currentIndex = 0;
            for (DirectoryMapping directoryMapping : directoryMappings)
            {
                if (directoryMapping.getDirectory().getId() == directoryID)
                {
                    break;
                }
                currentIndex++;
            }

            // insert at higher index if possible
            if (currentIndex < directoryMappings.size() && currentIndex > 0)
            {
                final DirectoryMapping directoryMapping = directoryMappings.get(currentIndex);
                applicationManager.updateDirectoryMapping(directoryMapping.getApplication(), directoryMapping.getDirectory(), currentIndex - 1);
            }


            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doDown()
    {
        try
        {
            processGeneral();

            List<DirectoryMapping> directoryMappings = getApplication().getDirectoryMappings();

            int currentIndex = 0;
            for (DirectoryMapping directoryMapping : directoryMappings)
            {
                if (directoryMapping.getDirectory().getId() == directoryID)
                {
                    break;
                }
                currentIndex++;
            }

            // insert at higher index if possible
            if (currentIndex + 1 < directoryMappings.size())
            {
                final DirectoryMapping directoryMapping = directoryMappings.get(currentIndex);
                applicationManager.updateDirectoryMapping(directoryMapping.getApplication(), directoryMapping.getDirectory(), currentIndex + 1);

            }

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }
        return INPUT;
    }

    public long getUnsubscribedDirectoriesID()
    {
        return unsubscribedDirectoriesID;
    }

    public void setUnsubscribedDirectoriesID(long unsubscribedDirectoriesID)
    {
        this.unsubscribedDirectoriesID = unsubscribedDirectoriesID;
    }

    public long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(long directoryID)
    {
        this.directoryID = directoryID;
    }
}