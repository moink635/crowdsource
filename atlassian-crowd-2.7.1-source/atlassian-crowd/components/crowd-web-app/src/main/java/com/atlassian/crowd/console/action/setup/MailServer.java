/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.setup;

import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.UserUtils;
import com.atlassian.crowd.util.mail.SMTPServer;
import com.atlassian.extras.api.Contact;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.mail.internet.InternetAddress;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Locale;

import com.google.common.collect.Iterables;

public class MailServer extends BaseSetupAction
{
    private static final Logger logger = Logger.getLogger(License.class);
    static final String DEFAULT_FORGOTTEN_PASSWORD_EMAIL_TEMPLATE = "mailtemplate.template.forgotten.password.text";
    static final String DEFAULT_FORGOTTEN_USERNAME_EMAIL_TEMPLATE = "mailtemplate.template.forgotten.username.text";

    public static final String MAILSERVER_STEP = "mailserver";

    private PropertyManager propertyManager;

    private String postponeConfiguration;
    private String notificationEmail;
    private String from;
    private String prefix;
    private String host;
    private String port = Integer.toString(SMTPServer.DEFAULT_MAIL_PORT);
    private String useSSL = Boolean.FALSE.toString();
    private String username;
    private String password;
    private String jndiLocation;
    private InitialContext initialContext;

    // Are we using JNDI Mail ... this is really a boolean
    private String jndiMailActive = Boolean.FALSE.toString().toLowerCase(Locale.ENGLISH);

    public String doDefault()
    {
        // check if i'm at the correct step
        String setupDefault = super.doDefault();

        if (!setupDefault.equals(SUCCESS))
        {
            return setupDefault;
        }

        postponeConfiguration = "true";

        populateDefaultsFromLicense();

        try
        {
            prefix = new StringBuilder(128).
                    append("[").append(propertyManager.getDeploymentTitle()).
                    append(" - ").append(getText("application.title")).append("]").toString();
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
        }

        try
        {
            SMTPServer server = propertyManager.getSMTPServer();
            jndiMailActive = Boolean.toString(server.isJndiMailActive());

        }
        catch (PropertyManagerException e)
        {
            if (!(e.getCause() instanceof ObjectNotFoundException))
            {
                addActionError(e);
                logger.debug(e.getMessage(), e);
            }
        }

        return INPUT;
    }

    private void populateDefaultsFromLicense()
    {
        if (!getLicense().getContacts().isEmpty())
        {
            Contact firstContact = Iterables.get(getLicense().getContacts(), 0);

            if (notificationEmail == null && StringUtils.isNotBlank(firstContact.getEmail()))
            {
                notificationEmail = firstContact.getEmail();
            }

            if (from == null && StringUtils.isNotBlank(firstContact.getEmail()))
            {
                from = firstContact.getEmail();
            }
        }
    }

    public String doUpdate()
    {
        // check if i'm at the correct step
        String setupUpdate = super.doUpdate();
        if (!setupUpdate.equals(SUCCESS))
        {
            return setupUpdate;
        }

        try
        {
            if (Boolean.valueOf(postponeConfiguration) == false)
            {
                // check for errors
                doValidation();

                if (hasErrors() || hasActionMessages())
                {
                    return INPUT;
                }

                SMTPServer server;
                if (Boolean.parseBoolean(jndiMailActive))
                {
                    server = new SMTPServer(jndiLocation, new InternetAddress(from), prefix);
                }
                else
                {
                    server = new SMTPServer(Integer.parseInt(port), prefix, new InternetAddress(from), password, username, host, Boolean.parseBoolean(useSSL));
                }

                propertyManager.setSMTPServer(server);

                // set the notification email
                propertyManager.setNotificationEmail(notificationEmail);
            }

            // save default email templates
            propertyManager.setProperty(Property.FORGOTTEN_PASSWORD_EMAIL_TEMPLATE,
                    getText(DEFAULT_FORGOTTEN_PASSWORD_EMAIL_TEMPLATE));
            propertyManager.setProperty(Property.FORGOTTEN_USERNAME_EMAIL_TEMPLATE,
                    getText(DEFAULT_FORGOTTEN_USERNAME_EMAIL_TEMPLATE));

            getSetupPersister().progessSetupStep();

            return SELECT_SETUP_STEP;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error("An error occurred updating the mail server");
        }

        return INPUT;
    }

    public String getStepName()
    {
        return MAILSERVER_STEP;
    }

    protected void doValidation()
    {
        if (StringUtils.isBlank(from) || !UserUtils.isValidEmail(from))
        {
            addFieldError("from", getText("mailserver.from.invalid"));
        }

        if (StringUtils.isBlank(notificationEmail) || !UserUtils.isValidEmail(notificationEmail))
        {
            addFieldError("notificationEmail", getText("mailserver.notification.invalid"));
        }

        if (Boolean.parseBoolean(jndiMailActive))
        {
            if (StringUtils.isBlank(jndiLocation))
            {
                addFieldError("jndiLocation", getText("mailserver.jndiLocation.invalid"));
            }
            else
            {
                try
                {
                    initialContext.lookup(jndiLocation);
                }
                catch (NamingException e)
                {
                    addFieldError("jndiLocation", e.getMessage());
                }
            }
        }
        else
        {
            if (StringUtils.isBlank(host))
            {
                addFieldError("host", getText("mailserver.host.invalid"));
            }

            if (StringUtils.isNotBlank(port))
            {
                try
                {
                    Integer.parseInt(port);
                }
                catch (NumberFormatException e)
                {
                    addFieldError("port", getText("mailserver.port.invalid") );
                }
            }
        }
    }

    public String getFrom()
    {
        return from;
    }

    public void setFrom(String from)
    {
        this.from = from;
    }

    public String getPrefix()
    {
        return prefix;
    }

    public void setPrefix(String prefix)
    {
        this.prefix = prefix;
    }

    public String getHost()
    {
        return host;
    }

    public void setHost(String host)
    {
        this.host = host;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setNotificationEmail(String notificationEmail)
    {
        this.notificationEmail = notificationEmail;
    }

    public String getNotificationEmail()
    {
        return notificationEmail;
    }

    public String getPort()
    {
        return port;
    }

    public void setPort(String port)
    {
        this.port = port;
    }

    public String getUseSSL()
    {
        return useSSL;
    }

    public void setUseSSL(String useSSL)
    {
        this.useSSL = useSSL;
    }


    public String getJndiLocation()
    {
        return jndiLocation;
    }

    public void setJndiLocation(String jndiLocation)
    {
        this.jndiLocation = jndiLocation;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    public String getJndiMailActive()
    {
        return jndiMailActive;
    }

    public void setJndiMailActive(String jndiMailActive)
    {
        this.jndiMailActive = jndiMailActive;
    }

    public void setInitialContext(InitialContext initialContext)
    {
        this.initialContext = initialContext;
    }

    public String getPostponeConfiguration()
    {
        return postponeConfiguration;
    }

    public void setPostponeConfiguration(String postponeConfiguration)
    {
        this.postponeConfiguration = postponeConfiguration;
    }
}
