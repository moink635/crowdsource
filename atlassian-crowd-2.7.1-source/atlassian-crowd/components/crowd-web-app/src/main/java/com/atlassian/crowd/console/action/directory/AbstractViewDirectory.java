package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;

public class AbstractViewDirectory extends BaseAction
{
    private DirectoryInstanceLoader directoryInstanceLoader;

    private long id = -1;
    private Directory directory;

    public final long getID()
    {
        return id;
    }

    public final void setID(long ID)
    {
        this.id = ID;
    }

    public final Directory getDirectory()
    {
        if (directory == null && id != -1)
        {
            try
            {
                directory = directoryManager.findDirectoryById(id);
            }
            catch (DirectoryNotFoundException e)
            {
                addActionError(e);
            }
        }

        return directory;
    }

    public String getDirectoryImplementationDescriptiveName()
    {
        try
        {
            return directoryInstanceLoader.getDirectory(getDirectory()).getDescriptiveName();
        }
        catch (DirectoryInstantiationException e)
        {
            return null;
        }
    }

    public void setDirectoryInstanceLoader(DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }
}
