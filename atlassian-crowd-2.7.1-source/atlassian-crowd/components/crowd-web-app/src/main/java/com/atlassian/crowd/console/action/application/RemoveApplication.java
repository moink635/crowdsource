/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.log4j.Logger;

public class RemoveApplication extends ViewApplication
{
    private static final Logger logger = Logger.getLogger(RemoveApplication.class);

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            processGeneral();

            applicationManager.remove(getApplication());

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    public String doDefault()
    {
        super.doDefault();
        return INPUT;
    }
}