/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.setup;

import com.atlassian.config.ConfigurationException;
import com.atlassian.config.HomeLocator;
import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.crowd.CrowdConstants;
import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.manager.license.CrowdLicenseStore;
import com.atlassian.crowd.util.build.BuildUtils;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.atlassian.license.SIDManager;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

/**
 * This setup action is not spring-injected.
 */

public class License extends BaseSetupAction
{
    private static final Logger logger = Logger.getLogger(License.class);

    public static final String LICENSE_STEP = "setuplicense";

    // webwork i/o
    private String key;
    private String sid;

    // dependencies
    private SIDManager sidManager;
    private CrowdLicenseStore licenseStore;
    private HomeLocator homeLocator;

    public String getStepName()
    {
        return LICENSE_STEP;
    }

    protected void doValidation()
    {
        if (StringUtils.isEmpty(key))
        {
            addFieldError("key", getText("license.key.error.required"));
            return;
        }

        if (!isSetupLicenseKeyValid(key))
        {
            addFieldError("key", getText("license.key.error.invalid"));
        }

        if (!hasErrors() && !isBuildWithinMaintenancePeriod(key))
        {
            addFieldError("key", getText("license.key.error.maintenance.expired"));
        }
    }

    public String doDefault()
    {
        // check if i'm at the correct step
        String setupDefault = super.doDefault();
        if (!setupDefault.equals(SUCCESS))
        {
            return setupDefault;
        }

        String crowdSid = getBootstrapManager().getServerID();

        if (crowdSid == null || !getSidManager().isValidSID(crowdSid))
        {
            crowdSid = getSidManager().generateSID();
            try
            {
                getBootstrapManager().setServerID(crowdSid);
            }
            catch (ConfigurationException e)
            {
                addActionError(e);
            }
        }

        // set the sid in the action
        setSid(crowdSid);

        return INPUT;
    }

    public String doUpdate()
    {
        // check if i'm at the correct step
        String setupUpdate = super.doUpdate();
        if (!setupUpdate.equals(SUCCESS))
        {
            return setupUpdate;
        }

        // validate the license key
        doValidation();

        if (hasErrors())
        {
            return ERROR;
        }

        try
        {
            getCrowdLicenseStore().storeLicense(key);

            createCrowdPropertiesPlaceHolder();

            getSetupPersister().progessSetupStep();

            return SELECT_SETUP_STEP;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    private void createCrowdPropertiesPlaceHolder() throws IOException
    {
        String homePath = getHomeLocator().getHomePath();

        File crowdPropertiesFile = new File(homePath, Constants.PROPERTIES_FILE);

        // Only write to the file if it does not already exist
        if (!crowdPropertiesFile.exists())
        {
            FileUtils.writeLines(crowdPropertiesFile, CrowdConstants.DEFAULT_CHARACTER_ENCODING, getCrowdProperties(), "\n");
        }
    }

    private List getCrowdProperties()
    {
        return Arrays.asList(
                "session.lastvalidation=session.lastvalidation",
                "session.isauthenticated=session.isauthenticated",
                "application.password=",
                "application.name=" + toLowerCase(getText("application.name")),
                "session.validationinterval=0",
                "crowd.server.url=",
                "session.tokenkey=session.tokenkey",
                "application.login.url="
        );
    }

    private boolean isBuildWithinMaintenancePeriod(String key)
    {
        final CrowdLicense license = CrowdLicenseStore.getCrowdLicense(getCrowdLicenseStore().getAtlassianLicense(key));

        return BuildUtils.getCurrentBuildDate().getTime() < license.getMaintenanceExpiryDate().getTime();
    }

    private boolean isSetupLicenseValid(CrowdLicense license)
    {
        if (license == null)
        {
            return false;
        }
        if (license.isExpired())
        {
            logger.fatal("License has expired.");
            return false;
        }
        return true;
    }

    private boolean isSetupLicenseKeyValid(String key)
    {
        try
        {
            return isSetupLicenseValid(CrowdLicenseStore.getCrowdLicense(getCrowdLicenseStore().getAtlassianLicense(key)));
        }
        catch (Exception e)
        {
            logger.fatal("Failed to validate license: " + e.getMessage());
            return false;
        }
    }


    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getSid()
    {
        return sid;
    }

    public void setSid(String sid)
    {
        this.sid = sid;
    }

    public CrowdLicenseStore getCrowdLicenseStore()
    {
        if (licenseStore == null)
        {
            licenseStore = (CrowdLicenseStore) BootstrapUtils.getBootstrapContext().getBean("licenseStore");
        }
        return licenseStore;
    }

    public SIDManager getSidManager()
    {
        if (sidManager == null)
        {
            sidManager = (SIDManager) BootstrapUtils.getBootstrapContext().getBean("sidManager");
        }

        return sidManager;
    }

    private HomeLocator getHomeLocator()
    {
        if (homeLocator == null)
        {
            homeLocator = (HomeLocator) BootstrapUtils.getBootstrapContext().getBean("homeLocator");
            homeLocator.lookupServletHomeProperty(ServletActionContext.getServletContext());
        }

        return homeLocator;
    }

}