package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.manager.directory.SynchronisationMode;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.ServletActionContext;

public class SynchroniseDirectoryCache extends ViewConnector
{
    /**
     * Synchronises the directory cache, and then returns immediately.
     *
     * Runs the synchronise in a background thread so the page will not appear to hang
     * If the directory is already synchronising, a new sync will NOT be started.
     * @return
     * @throws Exception
     */
    @Override
    @RequireSecurityToken(true)
    public String doDefault() throws Exception
    {
        directoryManager.synchroniseCache(getDirectory().getId(), SynchronisationMode.FULL);
        ServletActionContext.getRequest().setAttribute("updateSuccessful", Boolean.TRUE);
        return SUCCESS;
    }
}
