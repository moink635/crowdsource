package com.atlassian.crowd.console.filter;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.CrowdConstants;
import com.atlassian.gzipfilter.integration.GzipFilterIntegration;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;

public class CrowdGzipFilterIntegration implements GzipFilterIntegration
{
    private static final Logger logger = Logger.getLogger(GzipFilterIntegration.class);

    private PropertyManager propertyManager;

    /**
     * @return Whether this application has gzip enabled or not
     */
    public boolean useGzip()
    {
        boolean gzipEnabled = false;
        try
        {
            gzipEnabled = propertyManager.isGzipEnabled();
        }
        catch (PropertyManagerException e)
        {
            logger.debug("Could not read property 'gzip enabled'", e);
        }

        return gzipEnabled;
    }

    /**
     * What response encoding to use.  This is primarily used when handling the parsing of
     * writers to streams and vice-versa.  Usually this is UTF-8, but depending on your application
     * this may be different.
     *
     * @param request If you wish to parse the request for the request encoding
     * @return An encoding supported by java
     */
    public String getResponseEncoding(HttpServletRequest request)
    {
        return CrowdConstants.DEFAULT_CHARACTER_ENCODING;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}
