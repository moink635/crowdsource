package com.atlassian.crowd.console.tags;

import com.opensymphony.webwork.components.Component;
import com.opensymphony.webwork.views.jsp.ComponentTagSupport;
import com.opensymphony.xwork.util.OgnlValueStack;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

/**
 * Webwork tag to call a soy template.
 *
 * Shamelessly stolen from JIRA.
 *
 * @since v2.7.
 */
public class SoyTemplateTag extends ComponentTagSupport
{
    // Attributes ----------------------------------------------------
    protected String template;
    protected Map<String,Object> params = new HashMap<String,Object>();
    protected String completeModuleKey;

    // Public --------------------------------------------------------

    @Override
    public Component getBean(OgnlValueStack stack, HttpServletRequest req, HttpServletResponse res)
    {
        return new SoyTemplate(stack, completeModuleKey, template);
    }

    public String getTemplate()
    {
        return template;
    }

    public void setTemplate(String template)
    {
        this.template = template;
    }

    public String getModuleKey()
    {
        return completeModuleKey;
    }

    public void setModuleKey(String completeModuleKey)
    {
        this.completeModuleKey = completeModuleKey;
    }
}
