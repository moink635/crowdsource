package com.atlassian.crowd.console.action;

import com.opensymphony.webwork.ServletActionContext;

import org.springframework.security.web.savedrequest.HttpSessionRequestCache;

public class DefaultStartPage extends BaseAction
{
    // Implementation just used for clearing the saved request from the session
    // HttpSessionRequestCache.removeRequest is thread-safe; don't use this for anything else
    private static HttpSessionRequestCache requestCache = new HttpSessionRequestCache();

    public String execute() throws Exception
    {
        if (isAdmin())
        {
            return "admin";
        }
        else if (isAuthenticated())
        {
            return "user";
        }
        else
        {
            // we need to nuke any saved requests from acegi
            // since the user has made a direct request to a
            // default start page (ie. wants to login fresh
            // if they are not authenticated)
            requestCache.removeRequest(ServletActionContext.getRequest(), ServletActionContext.getResponse());

            return "login";
        }
    }
}
