package com.atlassian.crowd.util;

import java.util.Locale;

import com.google.common.net.InetAddresses;

import org.apache.commons.lang3.StringUtils;

public class SSOUtils
{
    /**
     * Given a hostname, this method tests if the supplied cookie domain is legal,
     * following RFC 6265 as closely as possible.
     *
     * This method assumes sane input to the extend that both the hostname and
     * domain string MUST be valid, or the result is unspecified.
     *
     * @param cookieDomain  a cookie domain.
     * @param hostname  the host part of a URL.
     * @return  <code>true</true> if the cookie domain is valid for the given hostname,
     *  <code>false</code> otherwise.
     */
    public static boolean isCookieDomainValid(String cookieDomain, String hostname)
    {
        if (!StringUtils.isEmpty(cookieDomain)) {
            /*
             * 5.2.3: "If the first character of the attribute-value string is %x2E ("."):
             *   Let cookie-domain be the attribute-value without the leading %x2E
             *   (".") character."
             */
            cookieDomain = StringUtils.removeStart(cookieDomain, ".");

            // 5.1.3: "(Note that both
            //        the domain string and the string will have been canonicalized to
            //        lower case at this point.)"

            cookieDomain = cookieDomain.toLowerCase(Locale.ENGLISH);
            hostname = hostname.toLowerCase(Locale.ENGLISH);

            // 5.1.3: "The domain string and the string are identical"
            if (cookieDomain.equals(hostname))
            {
                return true;
            }

            // "The string is a host name (i.e., not an IP address)."
            if (isAnIpAddress(hostname))
            {
                return false;
            }

            // "The domain string is a suffix of the string"
            // "The last character of the string that is not included in the
            // domain string a %x2E (".") character."
            return (hostname.endsWith("." + cookieDomain));
        }
        else
        {
            return true;
        }
    }

    static boolean isAnIpAddress(String s)
    {
        try
        {
            InetAddresses.forString(s);
            return true;
        }
        catch (IllegalArgumentException e)
        {
            return false;
        }
    }
}
