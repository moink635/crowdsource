package com.atlassian.crowd.console.action.admin;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.manager.proxy.TrustedProxyManager;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;

/**
 * Removes an address from the list of trusted proxies.
 */
public class UpdateTrustedProxies extends BaseAction
{
    private TrustedProxyManager trustedProxyManager;
    private String address;
    private List<String> addresses;


    /**
     * Called once <code>address</code> has been set.
     * @return
     */
    @RequireSecurityToken(true)
    public String doRemoveAddress()
    {
        if (StringUtils.isNotBlank(address))
        {
            trustedProxyManager.removeAddress(address);
        }                    
        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doAddAddress()
    {
        if (StringUtils.isNotBlank(address))
        {
            if (trustedProxyManager.addAddress(address))
            {
                return SUCCESS;
            }
        }

        addActionError("Unable to add proxy");
        // Need to provide the view with any existing trusted proxies (otherwise the table will be empty)
        addresses = Lists.newArrayList(trustedProxyManager.getAddresses());
        Collections.sort(addresses);

        return INPUT;
    }

    public String getAddress()
    {
        return this.address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public void setTrustedProxyManager(TrustedProxyManager trustedProxyManager)
    {
        this.trustedProxyManager = trustedProxyManager;
    }

    public List<String> getAddresses()
    {
        return addresses;
    }
}
