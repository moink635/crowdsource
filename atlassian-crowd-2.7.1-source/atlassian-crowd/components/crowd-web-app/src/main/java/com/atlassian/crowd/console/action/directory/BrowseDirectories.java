package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.console.action.AbstractBrowser;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;

import static com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction.BooleanLogic.AND;

import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;

public class BrowseDirectories extends AbstractBrowser<BrowseDirectories.DirectoryForDisplay>
{
    private static final Logger logger = Logger.getLogger(BrowseDirectories.class);

    private DirectoryInstanceLoader directoryInstanceLoader;

    private String active;
    private String name;

    private final Function<Directory, DirectoryForDisplay> getDirectoryForDisplay = new Function<Directory, DirectoryForDisplay>()
    {
        @Override
        public DirectoryForDisplay apply(Directory input)
        {
            String implementationDescriptiveName;

            try
            {
                implementationDescriptiveName = directoryInstanceLoader.getDirectory(input).getDescriptiveName();
            }
            catch (DirectoryInstantiationException e)
            {
                implementationDescriptiveName = null;
            }

            return new DirectoryForDisplay(input.getType(), input.getId(),
                    input.getName(), input.isActive(),
                    implementationDescriptiveName);
        }
    };

    @Override
    public String execute()
    {
        try
        {
            Collection<SearchRestriction> restrictions = new ArrayList<SearchRestriction>();

            if (active != null && active.length() > 0)
            {
                restrictions.add(Restriction.on(DirectoryTermKeys.ACTIVE).exactlyMatching(Boolean.valueOf(active)));
            }

            if (name != null && name.length() > 0)
            {
                restrictions.add(Restriction.on(DirectoryTermKeys.NAME).containing(name));
            }

            SearchRestriction restriction = restrictions.isEmpty() ? NullRestrictionImpl.INSTANCE : new BooleanRestrictionImpl(AND, restrictions.toArray(new SearchRestriction[restrictions.size()]));

            Iterable<Directory> directories = directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(restriction).startingAt(resultsStart).returningAtMost(resultsPerPage + 1));

            results = ImmutableList.copyOf(Iterables.transform(directories, getDirectoryForDisplay));
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return SUCCESS;
    }

    public boolean isCacheEnabled(DirectoryForDisplay directory)
            throws DirectoryInstantiationException, DirectoryNotFoundException
    {
        return directoryManager.isSynchronisable(directory.getId());
    }

    public String getActive()
    {
        return active;
    }

    public void setActive(String active)
    {
        this.active = active;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setDirectoryInstanceLoader(DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }

    public static class DirectoryForDisplay
    {
        private final DirectoryType type;
        private final Long id;
        private final String name;
        private final boolean active;
        private final String implementationDescriptiveName;

        DirectoryForDisplay(DirectoryType type, Long id, String name, boolean active,
                String implementationDescriptiveName)
        {
            this.type = type;
            this.id = id;
            this.name = name;
            this.active = active;
            this.implementationDescriptiveName = implementationDescriptiveName;
        }

        public DirectoryType getType()
        {
            return type;
        }

        public Long getId()
        {
            return id;
        }

        public String getName()
        {
            return name;
        }

        public boolean isActive()
        {
            return active;
        }

        public String getImplementationDescriptiveName()
        {
            return implementationDescriptiveName;
        }
    }
}
