package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.console.action.ApplicationBaseAction;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.permission.PermissionManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.model.application.GroupMappingComparator;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.apache.commons.lang3.BooleanUtils.isFalse;
import static org.apache.commons.lang3.BooleanUtils.toBooleanObject;

public class ViewApplication extends ApplicationBaseAction
{
    protected final Logger logger = Logger.getLogger(this.getClass());

    protected long ID = -1;

    protected String name;
    protected String applicationDescription;
    protected boolean active;
    protected boolean lowerCaseOutput;
    protected boolean aliasingEnabled;

    protected String password;
    protected String passwordConfirm;

    protected String testUsername;
    protected String testPassword;

    protected String directoryId;

    protected PermissionManager permissionManager;

    private Boolean pluginApplication = null;
    private Boolean crowdApplication = null;

    List<GroupMapping> unsubscribedGroups;

    public String doDefault()
    {
        try
        {
            processGeneral();
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return SUCCESS;
    }

    public String doConfigTest()
    {
        return doDefault();
    }

    protected void processGeneral() throws ApplicationNotFoundException
    {
        Application application;
        if (StringUtils.isNotBlank(name) && !hasErrors())
        {
            application = applicationManager.findByName(name);

            ID = application.getId();
        }
        else
        {
            application = applicationManager.findById(ID);
        }

        name = application.getName();
        applicationDescription = application.getDescription();
        active = application.isActive();
        lowerCaseOutput = application.isLowerCaseOutput();
        aliasingEnabled = application.isAliasingEnabled();
    }

    public boolean isTestAuthentication()
    {
        return !StringUtils.isEmpty(testUsername);
    }

    public boolean isValidTestAuthentication() throws ApplicationNotFoundException
    {

        // test if the test username/password are valid for the application
        PasswordCredential credential = new PasswordCredential(testPassword);

        UserAuthenticationContext userAuthenticationContext = new UserAuthenticationContext();

        userAuthenticationContext.setApplication(getApplication().getName());
        userAuthenticationContext.setCredential(credential);
        userAuthenticationContext.setName(testUsername);
        userAuthenticationContext.setValidationFactors(new ValidationFactor[0]);

        try
        {
            tokenAuthenticationManager.authenticateUser(userAuthenticationContext, TokenLifetime.USE_DEFAULT);

            return true;
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
        }

        return false;
    }

    public List<Directory> getUnsubscribedDirectories()
    {
        List<Directory> directories = new ArrayList<Directory>();

        try
        {
            List<Directory> allDirectories = directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS));

            final Application application = getApplication();
            for (Directory directory : allDirectories)
            {
                if (application.getDirectoryMapping(directory.getId()) == null)
                {
                    directories.add(directory);
                }
            }
        }
        catch (Exception e)
        {
            logger.warn(e.getMessage(), e);
        }

        return directories;
    }

    public List<GroupMapping> getUnsubscribedGroups()
    {
        if (unsubscribedGroups == null)
        {
            unsubscribedGroups = new ArrayList<GroupMapping>();
            List<GroupMapping> unsubscribedGroupsForDirectory = new ArrayList<GroupMapping>();  // Temp staging to allow us to sort results by directory, then group name.

            try
            {
                Application application = applicationManager.findById(ID);

                for (DirectoryMapping directoryMapping : application.getDirectoryMappings())
                {
                    List<Group> allGroups = directoryManager.searchGroups(directoryMapping.getDirectory().getId(), QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP)).returningAtMost(EntityQuery.ALL_RESULTS));

                    for (Group group : allGroups)
                    {
                        if (!directoryMapping.isAuthorised(group.getName()))
                        {
                            unsubscribedGroupsForDirectory.add(new GroupMapping(directoryMapping, group.getName()));
                        }
                    }

                    // CWD-342 - sort external groups alphabetically.
                    Collections.sort(unsubscribedGroupsForDirectory, new GroupMappingComparator());
                    unsubscribedGroups.addAll(unsubscribedGroupsForDirectory);
                    unsubscribedGroupsForDirectory.clear();
                }
            }
            catch (Exception e)
            {
                logger.warn(e.getMessage(), e);
            }
        }

        return unsubscribedGroups;
    }

    public boolean hasPermission(OperationType operationType)
    {
        boolean hasPermission = false;
        if (operationType != null && StringUtils.isNotBlank(directoryId))
        {
            try
            {
                Directory directory = directoryManager.findDirectoryById(Long.valueOf(directoryId));
                hasPermission = permissionManager.hasPermission(getApplication() , directory, operationType);
            }
            catch (Exception e)
            {
                logger.error(e.getMessage(), e);
                addActionError(e);
            }
        }
        return hasPermission;
    }

    public boolean permissionEnabledGlobally(OperationType operationType)
    {
        boolean hasPermission = false;
        if (operationType != null && StringUtils.isNotBlank(directoryId))
        {
            try
            {
                Directory directory = directoryManager.findDirectoryById(Long.valueOf(directoryId));
                hasPermission = permissionManager.hasPermission(directory, operationType);
            }
            catch (Exception e)
            {
                logger.error(e.getMessage(), e);
                addActionError(e);
            }
        }
        return hasPermission;
    }

    public List<GroupMapping> getGroupMappingsForApplication() throws ApplicationNotFoundException
    {
        List<GroupMapping> allGroupMappings = new ArrayList<GroupMapping>();
        for (DirectoryMapping directoryMapping : getApplication().getDirectoryMappings())
        {
            allGroupMappings.addAll(directoryMapping.getAuthorisedGroups());
        }
        Collections.sort(allGroupMappings, new GroupMappingComparator());

        return allGroupMappings;
    }

    public boolean getIsIncrementalSyncAvailable() throws ApplicationNotFoundException
    {
        final Application application = applicationManager.findById(ID);

        for (DirectoryMapping directoryMapping : application.getDirectoryMappings())
        {
            final Directory directory = directoryMapping.getDirectory();
            if (isFalse(toBooleanObject(directory.getValue(DirectoryProperties.CACHE_ENABLED))) && directory.isActive())
            {
                return false;
            }
        }

        return true;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setDirectoryId(String directoryId)
    {
        this.directoryId = directoryId;
    }

    public String getDirectoryId()
    {
        return directoryId;
    }

    public long getID()
    {
        return ID;
    }

    public void setID(long ID)
    {
        this.ID = ID;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getApplicationDescription()
    {
        return applicationDescription;
    }

    public void setApplicationDescription(String applicationDescription)
    {
        this.applicationDescription = applicationDescription;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPasswordConfirm()
    {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm)
    {
        this.passwordConfirm = passwordConfirm;
    }

    public String getTestUsername()
    {
        return testUsername;
    }

    public void setTestUsername(String testUsername)
    {
        this.testUsername = testUsername;
    }

    public String getTestPassword()
    {
        return testPassword;
    }

    public void setTestPassword(String testPassword)
    {
        this.testPassword = testPassword;
    }

    public boolean isPluginApplication() throws ApplicationNotFoundException
    {
        if (pluginApplication == null)
        {
            pluginApplication = ApplicationType.PLUGIN.equals(getApplication().getType());
        }
        return pluginApplication;
    }

    public boolean isCrowdApplication() throws ApplicationNotFoundException
    {
        if (crowdApplication == null)
        {
            crowdApplication = ApplicationType.CROWD.equals(getApplication().getType());
        }
        return crowdApplication;
    }

    public boolean isLowerCaseOutput()
    {
        return lowerCaseOutput;
    }

    public void setLowerCaseOutput(boolean lowerCaseOutput)
    {
        this.lowerCaseOutput = lowerCaseOutput;
    }

    public boolean isAliasingEnabled()
    {
        return aliasingEnabled;
    }

    public void setAliasingEnabled(final boolean aliasingEnabled)
    {
        this.aliasingEnabled = aliasingEnabled;
    }

    public Application getApplication() throws ApplicationNotFoundException
    {
        return applicationManager.findById(ID);
    }

    public Application getApplicationByName() throws ApplicationNotFoundException
    {
        return applicationManager.findByName(name);
    }
}
