package com.atlassian.crowd.console.action.setup;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ResourceLocator;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.util.PasswordHelper;
import com.atlassian.crowd.util.PropertyUtils;
import com.atlassian.crowd.util.UserUtils;
import com.atlassian.extras.api.Contact;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Sets;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static org.apache.commons.lang3.StringUtils.isBlank;

public class DefaultAdministrator extends BaseSetupAction
{
    private static final Logger logger = Logger.getLogger(DefaultAdministrator.class);

    public static final String DEFAULT_ADMIN_STEP = "defaultadministrator";
    public static final String DEFAULT_ADMIN_NAME_KEY = "DEFAULT_ADMIN_NAME";

    private long ID;

    private String name;
    private String password;
    private String passwordConfirm;
    private String firstname;

    private String lastname;

    private String email;

    private ResourceLocator resourceLocator;

    private ClientProperties clientProperties;

    private PropertyUtils propertyUtils;

    private SecurityServerClient securityServerClient;

    private PasswordHelper passwordHelper;

    public String doDefault()
    {
        // check if i'm at the correct step
        String setupDefault = super.doDefault();
        if (!setupDefault.equals(SUCCESS))
        {
            return setupDefault;
        }

        populateDefaultsFromLicense();

        return INPUT;
    }

    private void populateDefaultsFromLicense()
    {
        if (!getLicense().getContacts().isEmpty())
        {
            Contact firstContact = getLicense().getContacts().iterator().next();

            if (email == null && StringUtils.isNotBlank(firstContact.getEmail()))
            {
                email = firstContact.getEmail();
            }

            if (name == null && firstContact.getEmail() != null)
            {
                name = Iterables.getFirst(Splitter.on("@").split(firstContact.getEmail()), null);
            }

            if (firstname == null && lastname == null && StringUtils.isNotBlank(firstContact.getName())
                && firstContact.getName().contains(" "))
            {
                // naive name splitter. This is just to set default values. If users are not
                // satisfied, they can change the values
                int cutPoint = firstContact.getName().indexOf(" ");
                firstname = firstContact.getName().substring(0, cutPoint);
                lastname = firstContact.getName().substring(cutPoint + 1);
            }
        }
    }

    /**
     * Remove the message that {@link InetAddress#getLocalHost()} adds.
     */
    static final String justHostname(String message)
    {
        int i = message.indexOf(':');
        if (i >= 0)
        {
            return message.substring(0, i);
        }
        else
        {
            return message;
        }
    }

    public String doUpdate()
    {
        // check if i'm at the correct step
        String setupUpdate = super.doUpdate();
        if (!setupUpdate.equals(SUCCESS))
        {
            return setupUpdate;
        }

        try
        {
            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                return INPUT;
            }

            InetAddress localhost;

            try
            {
                localhost = InetAddress.getLocalHost();
            }
            catch (UnknownHostException e)
            {
                throw new RuntimeException("Unable to resolve this machine's hostname. Please ensure '"
                        + justHostname(e.getMessage()) + "' resolves to an IP address.", e);
            }

            UserTemplate userTemplate = new UserTemplate(name, ID);
            userTemplate.setActive(true);
            userTemplate.setEmailAddress(email);
            userTemplate.setFirstName(firstname);
            userTemplate.setLastName(lastname);

            final User adminUser = directoryManager.addUser(ID, userTemplate, new PasswordCredential(password));

            getSession().setAttribute(DEFAULT_ADMIN_NAME_KEY, name);

            GroupTemplate group = new GroupTemplate(getText("default.admin.group"), ID, GroupType.GROUP);

            final Group adminGroup = directoryManager.addGroup(ID, group);

            directoryManager.addUserToGroup(ID, adminUser.getName(), adminGroup.getName());

            addCrowdApplication(ID, group, localhost);

            if (!hasErrors())
            {
                getSetupPersister().progessSetupStep();
            }

            return SELECT_SETUP_STEP;
        }
        catch (InvalidCredentialException e)
        {
            addFieldError("password", getText("principal.password.invalid"));
            addFieldError("confirmPassword", "");
            logger.debug(e.getMessage(), e);
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    public String getStepName()
    {
        return DEFAULT_ADMIN_STEP;
    }

    protected void doValidation()
    {
        List<Directory> directories = directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS));
        if (directories.isEmpty() || directories.get(0) == null)
        {
            addActionError(getText("setup.directory.notfound"));
        }
        else
        {
            ID = directories.get(0).getId();
        }

        if (isBlank(name))
        {
            addFieldError("name", getText("principal.name.invalid"));
        }
        else if (IdentifierUtils.hasLeadingOrTrailingWhitespace(name))
        {
            addFieldError("name", getText("invalid.whitespace"));
        }

        if (isBlank(password))
        {
            addFieldError("password", getText("principal.password.invalid"));
            addFieldError("confirmPassword", "");
        }
        else if (isBlank(passwordConfirm))
        {
            addFieldError("password", getText("principal.password.invalid"));
            addFieldError("confirmPassword", "");
        }
        else if (!password.equals(passwordConfirm))
        {
            addFieldError("password", getText("principal.passwordconfirm.nomatch"));
            addFieldError("confirmPassword", "");
        }

        if (isBlank(email))
        {
            addFieldError("email", getText("principal.email.invalid"));
        }
        else if (!UserUtils.isValidEmail(email))
        {
            addFieldError("email", getText("principal.email.invalid"));
        }
        else if (IdentifierUtils.hasLeadingOrTrailingWhitespace(email))
        {
            addFieldError("email", getText("principal.email.whitespace"));
        }

        if (isBlank(firstname))
        {
            addFieldError("firstname", getText("principal.firstname.invalid"));
        }

        if (isBlank(lastname))
        {
            addFieldError("lastname", getText("principal.lastname.invalid"));
        }
    }

    private void addCrowdApplication(long directoryID, Group group, InetAddress localhost)
            throws UnknownHostException, MalformedURLException, InvalidCredentialException, ApplicationNotFoundException, DirectoryNotFoundException
    {
        final String crowdApplicationPassword = passwordHelper.generateRandomPassword();
        ApplicationImpl application = ApplicationImpl.newInstanceWithPassword(toLowerCase(getText("application.name")), ApplicationType.CROWD, crowdApplicationPassword);
        application.setDescription(getText("application.description"));

        // configure the client addresses, using a set to protect against duplication
        // adds the address specified in the properties file as a valid remote address
        String serverURLProperty = clientProperties.getBaseURL();
        URL serverURL = new URL(serverURLProperty);
        String serverHost = serverURL.getHost();
        String localHostName = localhost.getHostName();

        Set<RemoteAddress> addresses = Sets.newHashSet(
                new RemoteAddress(localhost.getHostAddress()),
                new RemoteAddress(localHostName),
                new RemoteAddress("localhost"),
                new RemoteAddress("127.0.0.1"),
                new RemoteAddress(serverHost),
                new RemoteAddress(InetAddress.getByName(serverHost).getHostAddress()) // attempt to get the IP address for the name specified in the configuration file
        );
        // Attempt to get IPv6 addresses for the serverHost/localHostName
        addresses.addAll(createRemoteAddresses(serverHost));
        addresses.addAll(createRemoteAddresses(localHostName));

        // Add these addresses to the application
        application.setRemoteAddresses(ImmutableSet.copyOf(addresses));

        // add the application
        final Application crowdApplication = applicationManager.add(application);

        // directory for the application mapping and role
        Directory defaultDirectory = directoryManager.findDirectoryById(directoryID);

        // configure the default directory mapping
        applicationManager.addDirectoryMapping(crowdApplication, defaultDirectory, false, OperationType.values());

        // configure the mapped group
        applicationManager.addGroupMapping(crowdApplication, defaultDirectory, group.getName());

        // update the crowd.properties file with the new password
        propertyUtils.updateProperty(resourceLocator.getResourceLocation(), Constants.PROPERTIES_FILE_APPLICATION_PASSWORD, crowdApplicationPassword);

        securityServerClient.getSoapClientProperties().updateProperties(resourceLocator.getProperties());
    }

    private Set<RemoteAddress> createRemoteAddresses(String hostname) throws UnknownHostException
    {
        ImmutableSet.Builder<RemoteAddress> remoteAddresses = new ImmutableSet.Builder<RemoteAddress>();
        InetAddress[] addresses = InetAddress.getAllByName(hostname);
        for (InetAddress address : addresses)
        {
            remoteAddresses.add(new RemoteAddress(address.getHostAddress()));
        }
        return remoteAddresses.build();
    }

    public void setResourceLocator(ResourceLocator resourceLocator)
    {
        this.resourceLocator = resourceLocator;
    }

    public void setClientProperties(ClientProperties clientProperties)
    {
        this.clientProperties = clientProperties;
    }

    public void setPropertyUtils(PropertyUtils propertyUtils)
    {
        this.propertyUtils = propertyUtils;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPasswordConfirm()
    {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm)
    {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public void setSecurityServerClient(SecurityServerClient securityServerClient)
    {
        this.securityServerClient = securityServerClient;
    }

    public void setPasswordHelper(final PasswordHelper passwordHelper)
    {
        this.passwordHelper = passwordHelper;
    }
}
