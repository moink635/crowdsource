package com.atlassian.crowd.console.action;

import com.atlassian.crowd.console.action.user.BaseUserAction;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidEmailAddressException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.login.ForgottenLoginManager;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.util.TextUtils;

import java.rmi.RemoteException;
import java.util.Locale;

/**
 * Handles the functionality for the user forgetting their login details.
 *
 * @since v2.1.0
 */
public class ForgottenLoginDetails extends BaseUserAction
{
    public enum LoginField { UNKNOWN, PASSWORD, USERNAME };

    private String username;
    private String email;
    private LoginField forgottenDetail = LoginField.UNKNOWN;

    private ForgottenLoginManager forgottenLoginManager;
    
    @Override
    public String doDefault() throws Exception
    {
        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate() throws InvalidAuthorizationTokenException, InvalidCredentialException, RemoteException
    {
        doValidation();

        if (hasErrors())
        {
            return ERROR;
        }

        switch(forgottenDetail)
        {
            case PASSWORD:
                return doForgotPassword();
            case USERNAME:
                return doForgotUsername();
            default:
                addActionError(getText("forgottenlogindetails.error.unknownfield"));
                return ERROR;
        }
    }

    private String doForgotPassword()
    {
        // Call the ApplicationService to reset the users password credential
        try
        {
            forgottenLoginManager.sendResetLink(getCrowdApplication(), username);
            return SUCCESS;
        }
        catch (UserNotFoundException e)
        {
            // Failed to find the principal to reset, however, to avoid username farming we still return SUCCESS
            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(getText("forgottenpassword.error.generic.label"));
            return ERROR;
        }
    }

    private String doForgotUsername()
    {
        try
        {
            forgottenLoginManager.sendUsernames(getCrowdApplication(), email);
            return SUCCESS;
        }
        catch (InvalidEmailAddressException e)
        {
            addActionError(getText("forgottenusername.error.generic.label"));
            return ERROR;
        }
        catch (ApplicationNotFoundException e)
        {
            addActionError(getText("forgottenusername.error.generic.label"));
            return ERROR;
        }
    }

    private void doValidation()
    {
        switch (forgottenDetail)
        {
            case PASSWORD:
                doForgotPasswordValidation();
                break;
            case USERNAME:
                doForgotUsernameValidation();
                break;
            default:
                addActionError(getText("forgottenlogindetails.error.unknownfield"));
        }
    }

    private void doForgotPasswordValidation()
    {
        if (!TextUtils.stringSet(username))
        {
            addFieldError("username", getText("forgottenpassword.error.username.label"));
        }
    }

    private void doForgotUsernameValidation()
    {
        if (!TextUtils.stringSet(email))
        {
            addFieldError("email", getText("forgottenusername.error.email.label"));
        }
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(final String username)
    {
        this.username = username;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(final String email)
    {
        this.email = email;
    }

    public String getForgottenDetail()
    {
        return forgottenDetail.toString().toLowerCase(Locale.ENGLISH);
    }

    public void setForgottenDetail(final String value)
    {
        try
        {
            forgottenDetail = LoginField.valueOf(value.toUpperCase());
        }
        catch (IllegalArgumentException e)
        {
            forgottenDetail = LoginField.UNKNOWN;
        }
    }

    public void setForgottenLoginManager(final ForgottenLoginManager forgottenLoginManager)
    {
        this.forgottenLoginManager = forgottenLoginManager;
    }
}
