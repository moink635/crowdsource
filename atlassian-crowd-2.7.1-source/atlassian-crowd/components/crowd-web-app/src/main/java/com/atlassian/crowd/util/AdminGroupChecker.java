package com.atlassian.crowd.util;

import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;


public class AdminGroupChecker
{
    private static final Logger logger = Logger.getLogger(AdminGroupChecker.class);


    private final DirectoryManager directoryManager;
    private final ApplicationManager applicationManager;
    private final I18nHelper i18nHelper;

    public AdminGroupChecker(DirectoryManager directoryManager, ApplicationManager applicationManager, I18nHelper i18nHelper)
    {
        this.directoryManager = Preconditions.checkNotNull(directoryManager);
        this.applicationManager = Preconditions.checkNotNull(applicationManager);
        this.i18nHelper = Preconditions.checkNotNull(i18nHelper);
    }

    public boolean isRemovingCrowdConsoleAdminMembership(String consoleAdmin, long consoleAdminDirectoryID, String selectedUser, long selectedUserDirectoryID)
    {
        return (consoleAdmin.equalsIgnoreCase(selectedUser) && consoleAdminDirectoryID == selectedUserDirectoryID);
    }

    public boolean isRemovingCrowdConsoleAdminMembership(String consoleAdmin, long consoleAdminDirectoryID, List<String> selectedUsernames, long selectedDirectoryID)
    {
        return (selectedUsernames.contains(consoleAdmin) && (selectedDirectoryID == consoleAdminDirectoryID));
    }

    public Set<String> getUnsafeAdminGroups(String username, long directoryID, List<String> selectedEntityNames)
            throws OperationFailedException, DirectoryNotFoundException
    {
        Set<String> adminGroups = authorisedGroupsForUser(username, directoryID);

        if (!adminGroups.isEmpty() && selectedEntityNames.containsAll(adminGroups))
        {
            logger.info("Did not remove user: <" + username + "> from groups: <" + StringUtils.join(adminGroups, ",") + "> to prevent potential lockout from crowd console.");
            return adminGroups;
        }
        return Collections.emptySet();
    }

    public boolean isRemovingConsoleAdminFromLastAdminGroup(String groupname, String consoleAdmin, long directoryID)
            throws OperationFailedException, DirectoryNotFoundException
    {
        Set<String> adminGroups = authorisedGroupsForUser(consoleAdmin, directoryID);

        // Current user is an admin, so should have at least one admin group.
        // See what happens when we remove user from the group will there sitll be any admin groups left...
        adminGroups.remove(groupname);
        if (adminGroups.isEmpty())
        {
            // If we do remove user from this group, the user will no longer be in any 'admin' groups :(
            logger.info("Did not remove user: <" + consoleAdmin + "> from group: <" + groupname + "> to prevent potential lockout from crowd console.");
            return true;
        }
        return false;
    }

    private DirectoryMapping crowdConsoleDirectoryMappingsForCurrentDirectory(Long directoryID)
            throws OperationFailedException
    {
        String crowdConsoleNameLower = toLowerCase(i18nHelper.getText("application.name"));
        List<DirectoryMapping> directoryMappings = null;
        try
        {
            directoryMappings = applicationManager.findByName(crowdConsoleNameLower).getDirectoryMappings();
        }
        catch (ApplicationNotFoundException e)
        {
            throw new OperationFailedException(e);
        }
        for (DirectoryMapping directoryMapping : directoryMappings)
        {
            if (directoryID.equals(directoryMapping.getDirectory().getId()))
            {
                return directoryMapping;
            }
        }
        return null; // no corresponding directory found
    }

    private Set<String> authorisedGroupsForUser(String username, Long directoryID) throws OperationFailedException, DirectoryNotFoundException
    {
        DirectoryMapping directoryMapping = crowdConsoleDirectoryMappingsForCurrentDirectory(directoryID);

        Set<String> currentUserAuthorisedGroups = new HashSet<String>();
        Set<String> applicationAuthorisedGroups = new HashSet<String>();
        Set<String> applicationNestedAuthorisedGroups = new HashSet<String>();

        if (directoryMapping == null)
        {
            return currentUserAuthorisedGroups;
        }

        // Find all the groups that are given admin access to Crowd console
        for (GroupMapping groupMapping : directoryMapping.getAuthorisedGroups())
        {
            applicationAuthorisedGroups.add(groupMapping.getGroupName());
        }

        // Find all the child groups of the above groups
        for (String groupname : applicationAuthorisedGroups)
        {
            applicationNestedAuthorisedGroups.addAll(directoryManager.searchNestedGroupRelationships(directoryID, QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(groupname).returningAtMost(EntityQuery.ALL_RESULTS)));
        }

        // Combined set of all the groups that have admin access to Crowd console
        currentUserAuthorisedGroups.addAll(applicationAuthorisedGroups);
        currentUserAuthorisedGroups.addAll(applicationNestedAuthorisedGroups);

        // Find all the groups the user belongs to
        List<String> groups = directoryManager.searchDirectGroupRelationships(directoryID, QueryBuilder.queryFor(String.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(username).returningAtMost(EntityQuery.ALL_RESULTS));

        // Only keep the admin groups that the user is a member of.
        currentUserAuthorisedGroups.retainAll(groups);
        return currentUserAuthorisedGroups;
    }
}
