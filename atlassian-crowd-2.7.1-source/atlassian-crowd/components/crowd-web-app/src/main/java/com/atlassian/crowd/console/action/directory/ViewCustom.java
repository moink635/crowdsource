package com.atlassian.crowd.console.action.directory;


public class ViewCustom extends AbstractViewDirectory
{
    protected String name;
    protected String directoryDescription;
    protected boolean active;

    @Override
    public String doDefault() throws Exception
    {
        name = getDirectory().getName();
        directoryDescription = getDirectory().getDescription();
        active = getDirectory().isActive();

        return SUCCESS;
    }

    ///CLOVER:OFF
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDirectoryDescription()
    {
        return directoryDescription;
    }

    public void setDirectoryDescription(String directoryDescription)
    {
        this.directoryDescription = directoryDescription;
    }

    public boolean getActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }
}
