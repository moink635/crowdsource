/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.principal;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class UpdatePrincipal extends ViewPrincipal
{
    private static final Logger logger = Logger.getLogger(UpdatePrincipal.class);

    @RequireSecurityToken(true)
    public String doUpdateGeneral()
    {
        try
        {
            processDirectoryMapping();

            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                processGeneral();
                return INPUT;
            }

            User foundUser;
            if (StringUtils.isBlank(newName) || name.equals(newName))
            {
                // update without rename
                foundUser = directoryManager.findUserByName(directoryID, name);
            }
            else
            {
                // update and rename
                foundUser = directoryManager.renameUser(directoryID, name, newName);
                name = newName;
            }

            UserTemplate mutableUser = new UserTemplate(foundUser);
            mutableUser.setActive(active);
            mutableUser.setEmailAddress(email);
            mutableUser.setFirstName(firstname);
            mutableUser.setLastName(lastname);
            mutableUser.setDisplayName(firstname + " " + lastname);

            user = directoryManager.updateUser(directoryID, mutableUser);

            if (StringUtils.isNotBlank(password) && password.equals(passwordConfirm))
            {
                PasswordCredential credential = new PasswordCredential(password);
                try
                {
                    directoryManager.updateUserCredential(directoryID, name, credential);
                }
                catch (InvalidCredentialException e)
                {
                    addFieldError("password", e.getMessage());
                    return INPUT;
                }
            }

            addActionMessage(ALERT_BLUE, getText("updatesuccessful.label"));

            return SUCCESS;
        }
        catch (UserAlreadyExistsException e)
        {
            addFieldError("newName", getText("invalid.namealreadyexist"));
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }
}
