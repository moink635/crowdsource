package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationAttributeConstants;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.interceptor.SessionAware;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 */
public class AddApplicationConfirmation extends BaseAction implements SessionAware
{
    private Map session;
    private long ID;

    @Override
    public String execute() throws Exception
    {
        if (getConfiguration() == null)
        {
            return "start";
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String completeStep()
    {
        try
        {
            ApplicationConfiguration configuration = getConfiguration();

            ApplicationImpl application = ApplicationImpl.newInstanceWithPassword(configuration.getName(), configuration.getApplicationType(), configuration.getPassword());
            application.setDescription(configuration.getDescription());
            Map<String, String> attributes = Maps.newHashMap();
            attributes.put(ApplicationAttributeConstants.ATTRIBUTE_KEY_APPLICATION_URL, configuration.getApplicationURL());
            application.setAttributes(attributes);

            Application createdApplication = applicationManager.add(application);
            ID = createdApplication.getId();


            for (String remoteAddress : configuration.getRemoteAddresses())
            {
                applicationManager.addRemoteAddress(createdApplication, new RemoteAddress(remoteAddress));
            }

            for (Iterator<Long> iterator = configuration.getDirectoryids().iterator(); iterator.hasNext();)
            {
                Long directoryid = iterator.next();

                Directory directory = directoryManager.findDirectoryById(directoryid);

                Boolean allowAll = configuration.getAllowAllForDirectory().get(directoryid);
                if (allowAll == null)
                {
                    allowAll = false;
                }

                applicationManager.addDirectoryMapping(createdApplication, directory, allowAll, OperationType.values());

                Set<String> groupNames = configuration.getDirectoryGroupMappings().get(directoryid);
                if (groupNames != null)
                {
                    for (String groupName : groupNames)
                    {
                        applicationManager.addGroupMapping(createdApplication, directory, groupName);
                    }
                }
            }
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
            return ERROR;
        }

        // finally clear the configuration from the session
        session.remove(AddApplicationDetails.APPLICATION_SESSION_CONFIGURATION_SETUP);

        return SUCCESS;
    }

    public ApplicationConfiguration getConfiguration()
    {
        return (ApplicationConfiguration) session.get(AddApplicationDetails.APPLICATION_SESSION_CONFIGURATION_SETUP);
    }

    public void setSession(Map session)
    {
        this.session = session;
    }

    public long getID()
    {
        return ID;
    }
}
