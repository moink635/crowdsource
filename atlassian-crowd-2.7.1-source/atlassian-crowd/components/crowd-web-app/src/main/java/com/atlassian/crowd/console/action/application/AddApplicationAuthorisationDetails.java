package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.interceptor.SessionAware;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 *
 */
public class AddApplicationAuthorisationDetails extends BaseAction implements SessionAware
{
    private Map session;
    private String name;
    private Long directoryID;
    private String groupName;
    private String directoryGroup;
    private List<Directory> directories;
    public static final String ALLOW_ALL_TO_AUTHENTICATE_FOR_DIRECTORY = "allowAllToAuthenticateForDirectory-";

    @Override
    public String execute() throws Exception
    {
        if (getConfiguration() == null)
        {
            return "start";
        }

        name = getConfiguration().getName();
        directories = new ArrayList<Directory>();
        List<Long> directoryIds = getConfiguration().getDirectoryids();
        for (Iterator<Long> iterator = directoryIds.iterator(); iterator.hasNext();)
        {
            Long directoryId = iterator.next();
            directories.add(directoryManager.findDirectoryById(directoryId));
        }

        setAllowAllForDirectories();


        return INPUT;
    }

    public String completeStep()
    {
        // Determine if allow all to auth was checked
        setAllowAllForDirectories();

        return "confirmationdetails";
    }

    public boolean isAllowAllForDirectory(Long directoryID)
    {
        Boolean allowAll = getConfiguration().getAllowAllForDirectory().get(directoryID);

        if (allowAll != null)
        {
            return allowAll;
        }
        return false;
    }

    private void setAllowAllForDirectories()
    {
        ApplicationConfiguration configuration = getConfiguration();
        List<Long> directoryIds = configuration.getDirectoryids();
        for (Iterator<Long> directoryIdIterator = directoryIds.iterator(); directoryIdIterator.hasNext();)
        {
            Long directoryID = directoryIdIterator.next();
            String allowAllParam = ServletActionContext.getRequest().getParameter(ALLOW_ALL_TO_AUTHENTICATE_FOR_DIRECTORY + directoryID);
            if (Boolean.valueOf(allowAllParam))
            {
                // Allow all for this directory.
                configuration.setAllowAll(directoryID);

                // Clear any assoicated groups. Set allow all for this directory
                Set<String> groupNames = configuration.getDirectoryGroupMappings().get(directoryID);
                if (groupNames != null)
                {
                    groupNames.clear();
                }
            }
        }
    }

    public List<String> getUnsubscribedGroupsForDirectory(Long directoryId)
            throws OperationFailedException, DirectoryNotFoundException
    {
        List<String> groupNames = directoryManager.searchGroups(directoryId, QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP)).returningAtMost(EntityQuery.ALL_RESULTS));

        // Remove the currently selected groups
        groupNames.removeAll(getSelectedGroupsForDirectory(directoryId));
        return groupNames;

    }

    public Set<String> getSelectedGroupsForDirectory(Long directoryId)
    {
        Map<Long, Set<String>> directoryGroupMappings = getConfiguration().getDirectoryGroupMappings();

        Set<String> groupNames = directoryGroupMappings.get(directoryId);

        if (groupNames == null)
        {
            groupNames = Collections.emptySet();
        }

        return groupNames;
    }

    public String addGroupToDirectory() throws Exception
    {
        if (StringUtils.isNotBlank(directoryGroup))
        {
            // String in the format of directoryId-groupname
            String[] params = directoryGroup.split("-", 2);
            if (params != null && params.length == 2)
            {
                Map<Long, Set<String>> groupMappings = getConfiguration().getDirectoryGroupMappings();
                Long directoryId = new Long(params[0]);
                String groupName = params[1];

                Set<String> groups = groupMappings.get(directoryId);
                if (groups != null && !groups.isEmpty())
                {
                    groups.add(groupName);
                }
                else
                {
                    groupMappings.put(directoryId, new TreeSet<String>(Arrays.asList(groupName)));
                }

                // Remove the allow all flag if it exists
                getConfiguration().removeAllowAll(directoryId);

                getConfiguration().setDirectoryGroupMappings(groupMappings);
            }
        }

        return execute();
    }

    public String removeGroup() throws Exception
    {
        if (StringUtils.isNotBlank(groupName) && (directoryID != null && directoryID > 0))
        {
            Set<String> currentGroupNames = getConfiguration().getDirectoryGroupMappings().get(directoryID);
            currentGroupNames.remove(groupName);
        }

        return execute();
    }

    public void setSession(Map session)
    {
        this.session = session;
    }

    public String getName()
    {
        return name;
    }

    public ApplicationConfiguration getConfiguration()
    {
        return (ApplicationConfiguration) session.get(AddApplicationDetails.APPLICATION_SESSION_CONFIGURATION_SETUP);
    }

    public List<Directory> getDirectories()
    {
        return directories;
    }

    public void setDirectoryGroup(String directoryGroup)
    {
        this.directoryGroup = directoryGroup;
    }

    public void setDirectoryID(Long directoryID)
    {
        this.directoryID = directoryID;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }
}
