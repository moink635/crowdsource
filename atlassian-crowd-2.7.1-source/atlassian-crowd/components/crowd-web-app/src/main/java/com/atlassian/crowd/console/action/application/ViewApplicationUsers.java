package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.console.action.Searcher;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.AliasManager;
import com.atlassian.crowd.model.user.User;

import java.util.List;

/**
 * Adds the ability to search for the users in an application by using the functionality of BrowsePrincipals.
 */
public class ViewApplicationUsers extends ViewApplication
{
    private AliasManager aliasManager;
    private Searcher<User> userSearcher;
    private String active;
    private String search;

    /**
     * Results start.
     */
    protected int resultsStart = 0;

    /**
     * Results per page.
     */
    protected int resultsPerPage = 100;

    /**
     * Results listObjects.
     */
    protected List<User> results = null;


    /**
     * Loads all the basic information needed to populate the form. If there's a directoryID set, performs a search.
     * These two separate pieces of functionality are conflated into one method because I suck at getting webwork to do
     * what I want.
     */
    public String doDefault()
    {
        String retVal = super.doDefault();
        if (!SUCCESS.equals(retVal))
        {
            return retVal;
        }

        try
        {
            // perform a search if a applicationId has been set and the search isn't null.
            // This 'plays' on the fact that 'search' will be null on the initial load of the page.
            if (ID != -1 && search != null)
            {
                Boolean activeVal = null;
                if (active != null && active.length() > 0)
                {
                    activeVal = Boolean.valueOf(active);
                }

                results = userSearcher.doSearchByApplication(ID, activeVal, search, resultsStart, resultsPerPage);
            }

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }
        return ERROR;
    }

    /**
     * Gets the results start.
     *
     * @return the results start.
     */
    public int getResultsStart()
    {
        return resultsStart;
    }

    /**
     * Sets the results start.
     *
     * @param resultsStart the results start.
     */
    public void setResultsStart(int resultsStart)
    {
        this.resultsStart = resultsStart;
    }

    /**
     * Gets the next results start.
     *
     * @return the next results start.
     */
    public int getNextResultsStart()
    {
        return resultsStart + resultsPerPage;
    }

    /**
     * Gets the previous results start.
     *
     * @return the previous results start.
     */
    public int getPreviousResultsStart()
    {
        int result = resultsStart - resultsPerPage;

        if (result < 0)
        {
            return 0;
        }
        else
        {
            return result;
        }
    }

    /**
     * Gets the results per page.
     *
     * @return the results per page.
     */
    public int getResultsPerPage()
    {
        return resultsPerPage;
    }

    /**
     * Sets the results per page.
     *
     * @param resultsPerPage the results per page.
     */
    public void setResultsPerPage(int resultsPerPage)
    {
        this.resultsPerPage = resultsPerPage;
    }

    public List<User> getResults()
    {
        return results;
    }

    public void setResults(List<User> results)
    {
        this.results = results;
    }

    public String getActive()
    {
        return active;
    }

    public void setActive(String active)
    {
        this.active = active;
    }

    public String getSearch()
    {
        return search;
    }

    public void setSearch(String search)
    {
        this.search = search;
    }

    public void setUserSearcher(Searcher userSearcher)
    {
        this.userSearcher = userSearcher;
    }

    public void setAliasManager(final AliasManager aliasManager)
    {
        this.aliasManager = aliasManager;
    }

    public String getAliasForUser(String username) throws ApplicationNotFoundException
    {
        String alias = aliasManager.findAliasByUsername(getApplication(), username);
        if (username.equals(alias))
        {
            // not really an alias
            return null;
        }
        else
        {
            return alias;
        }
    }
}
