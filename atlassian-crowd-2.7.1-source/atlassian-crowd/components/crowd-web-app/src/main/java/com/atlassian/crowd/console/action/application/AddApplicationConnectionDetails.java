package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.ip.Subnet;
import com.opensymphony.util.TextUtils;
import com.opensymphony.webwork.interceptor.SessionAware;
import org.apache.commons.lang3.StringUtils;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Connection details for an application
 */
public class AddApplicationConnectionDetails extends BaseAction implements SessionAware
{
    private static final String GENERIC_LOOKUP_FAILURE = "failed to lookup ip address";
    private Map session;
    private String applicationURL;
    private String name;
    private ApplicationType applicationType;
    private String remoteIPAddress;

    public String execute()
    {
        ApplicationConfiguration configuration = getConfiguration();
        if (configuration != null)
        {
            name = configuration.getName();
            applicationType = configuration.getApplicationType();
            if (StringUtils.isEmpty(applicationURL))
            {
                applicationURL = configuration.getApplicationURL();
            }

            if (StringUtils.isEmpty(remoteIPAddress))
            {
                List<String> addresses = configuration.getRemoteAddresses();
                if (addresses != null && addresses.size() > 0)
                {
                    remoteIPAddress = configuration.getRemoteAddresses().get(0);
                }
            }
        }
        else
        {
            return "start";
        }

        return INPUT;
    }

    /**
     * Abstracted out to allow mocking.
     */
    InetAddress[] getAllByName(String host) throws UnknownHostException
    {
        return InetAddress.getAllByName(host);
    }
    
    /**
     * Get all known addresses for the local machine. Abstracted out to allow mocking.
     */
    Set<InetAddress> getLocalAddresses()
    {
        Set<InetAddress> localAddresses = new HashSet<InetAddress>();
        
        try
        {
            Enumeration<NetworkInterface> ints = NetworkInterface.getNetworkInterfaces();
            
            if (ints != null)
            {
                while (ints.hasMoreElements())
                {
                    Enumeration<InetAddress> addrs = ints.nextElement().getInetAddresses();
                    while (addrs.hasMoreElements())
                    {
                        localAddresses.add(addrs.nextElement());
                    }
                }
            }
            
            return localAddresses;
            
        }
        catch (SocketException uhe)
        {
            return Collections.emptySet();
        }
    }
    
    public String resolveIPAddress()
    {
        try
        {
            URI uri = new URI(applicationURL);
            
            String host = uri.getHost();

            if (host != null)
            {
                Collection<String> addresses = new ArrayList<String>();
                
                for (InetAddress ia : getAllByName(host))
                {
                    if (ia instanceof Inet6Address)
                    {
                        /* Create a new instance with the same name and address but with no scope ID */
                        ia = Inet6Address.getByAddress(ia.getHostName(), ia.getAddress(), null);
                    }
                    addresses.add(ia.getHostAddress());
                }
                remoteIPAddress = StringUtils.join(addresses, ", ");
            }
            else
            {
                remoteIPAddress = GENERIC_LOOKUP_FAILURE;
            }
        }
        catch (URISyntaxException e)
        {
            remoteIPAddress = GENERIC_LOOKUP_FAILURE;
        }
        catch (UnknownHostException e)
        {
            remoteIPAddress = GENERIC_LOOKUP_FAILURE;
        }

        return execute();
    }

    public String completeStep()
    {
        doValidation();

        if (hasErrors())
        {
            return ERROR;
        }

        Set<InetAddress> allLocalAddresses = getLocalAddresses();
        
        // Addresses list
        List<String> remoteAddresses = new ArrayList<String>();
        for (String addr : StringUtils.split(remoteIPAddress, ", "))
        {
            remoteAddresses.add(addr);

            try
            {
                InetAddress[] hostAddrs = getAllByName(addr);
                
                // Check if any of the resolved address are one of our local ones

                boolean wasLocal = false;
                
                for (InetAddress a : hostAddrs)
                {
                    if (allLocalAddresses.contains(a))
                    {
                        wasLocal = true;
                        break;
                    }
                }
                
                if (wasLocal)
                {
                    remoteAddresses.add("127.0.0.1");
                    remoteAddresses.add("0:0:0:0:0:0:0:1");
                }
            }
            catch (UnknownHostException e)
            {
                // Can't find local host...
            }
        }

        remoteAddresses = new ArrayList<String>(new HashSet<String>(remoteAddresses));
        Collections.sort(remoteAddresses);
        
        ApplicationConfiguration configuration = getConfiguration();
        configuration.setIPAddresses(remoteAddresses);
        configuration.setApplicationURL(applicationURL);

        return "directorydetails";
    }

    private void doValidation()
    {
        if (StringUtils.isBlank(remoteIPAddress))
        {
            addFieldError("remoteIPAddress", getText("application.remoteipaddress.invalid"));
        }
        else
        {
            for (String addr : StringUtils.split(remoteIPAddress, ", "))
            {
                if (!Subnet.isValidPattern(addr))
                {
                    addFieldError("remoteIPAddress", getText("application.remoteipaddress.invalid"));
                    break;
                }
            }
        }

        if (!TextUtils.verifyUrl(applicationURL))
        {
            addFieldError("applicationURL", getText("application.url.invalid"));
        }
    }

    public void setSession(Map session)
    {
        this.session = session;
    }

    public ApplicationType getApplicationType()
    {
        return applicationType;
    }

    public void setApplicationURL(String applicationURL)
    {
        this.applicationURL = applicationURL;
    }

    public String getApplicationURL()
    {
        return applicationURL;
    }

    public String getName()
    {
        return name;
    }

    public ApplicationConfiguration getConfiguration()
    {
        return (ApplicationConfiguration) session.get(AddApplicationDetails.APPLICATION_SESSION_CONFIGURATION_SETUP);
    }

    public String getRemoteIPAddress()
    {
        return remoteIPAddress;
    }

    public void setRemoteIPAddress(String remoteIPAddress)
    {
        this.remoteIPAddress = remoteIPAddress;
    }
}
