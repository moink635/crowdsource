/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.principal;

import com.atlassian.crowd.manager.login.ForgottenLoginManager;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.log4j.Logger;

public class ResetPassword extends ViewPrincipal
{
    private static final Logger logger = Logger.getLogger(ResetPassword.class);
    private ForgottenLoginManager forgottenLoginManager;

    public String doDefault()
    {
        try
        {
            directory = directoryManager.findDirectoryById(directoryID);
            user = directoryManager.findUserByName(directoryID, name);
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            processGeneral();
            processDirectoryMapping();

            forgottenLoginManager.sendResetLink(directoryID, name);

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    public void setForgottenLoginManager(final ForgottenLoginManager forgottenLoginManager)
    {
        this.forgottenLoginManager = forgottenLoginManager;
    }
}