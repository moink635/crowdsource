package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.log4j.Logger;

import java.util.Arrays;

public class RemoveDirectory extends BaseAction
{
    private static final Logger logger = Logger.getLogger(RemoveDirectory.class);

    long ID;
    Directory directory;

    public String doDefault()
    {
        try
        {
            directory = directoryManager.findDirectoryById(ID);
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            directory = directoryManager.findDirectoryById(ID);
            if (getRemoteUser().getDirectoryId() == ID)
            {
                addActionMessage(getText("preventlockout.removedirectory.label", Arrays.asList(directory.getName())));
                return ERROR;
            }
            else if (directoryManager.isSynchronising(directory.getId()))
            {
                addActionError(getText("directory.remove.currentlysynchronising.error", Arrays.asList(directory.getName())));
                return ERROR;
            }
            else
            {
                directoryManager.removeDirectory(directory);
                return SUCCESS;
            }
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    public long getID()
    {
        return ID;
    }

    public void setID(long ID)
    {
        this.ID = ID;
    }

    public Directory getDirectory()
    {
        return directory;
    }
}