package com.atlassian.crowd.console.action.user;

import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ViewGroups extends BaseUserAction
{
    private List<String> groups;

    public String doDefault() throws Exception
    {
        try
        {
            List<Group> groupsAndRoles = applicationService.searchNestedGroupRelationships(getCrowdApplication(), QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP)).parentsOf(EntityDescriptor.user()).withName(getRemoteUsername()).returningAtMost(EntityQuery.ALL_RESULTS));

            groups = new ArrayList<String>();

            for (Group container : groupsAndRoles)
            {
                if (container.getType() == GroupType.GROUP)
                {
                    groups.add(container.getName());
                }
            }

            Collections.sort(groups, String.CASE_INSENSITIVE_ORDER);
        }
        catch (Exception e)
        {
            addActionError(getText("user.console.groups.error"));
            logger.error("Failed to get groups and roles", e);
            groups = Collections.emptyList();
        }
        return SUCCESS;
    }

    public List getGroups()
    {
        return groups;
    }
}
