/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
                                                                                                                                        */
package com.atlassian.crowd.console.action.principal;

import com.atlassian.crowd.console.action.group.AddGroup;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.util.SelectionUtils;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.List;

public class AddPrincipal extends ViewPrincipal
{
    private static final Logger logger = Logger.getLogger(AddGroup.class);

    public String doDefault()
    {
        active = true;

        directoryID = SelectionUtils.getSelectedDirectory(getDirectories());

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                return INPUT;
            }

            syncFieldsToPrincipal();

            PasswordCredential credential = new PasswordCredential(password);

            try
            {
                directoryManager.addUser(directoryID, (UserTemplate) user, credential);
                user = directoryManager.findUserWithAttributesByName(directoryID, user.getName());
                SelectionUtils.saveSelectedDirectory(directoryID);
            }
            catch (InvalidCredentialException e)
            {
                addFieldError("password", e.getMessage());

                // set the input page
                return INPUT;
            }

            directory = directoryManager.findDirectoryById(directoryID);

            // Details may have been changed or sanitized (eg whitespace stripped) by lower layers
            syncFieldsFromPrincipal();

            return SUCCESS;

        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    protected void doValidation()
    {
        super.doValidation();

        if (StringUtils.isBlank(password))
        {
            addFieldError("password", getText("principal.password.invalid"));
        }

        if (StringUtils.isBlank(name))
        {
            addFieldError("name", getText("principal.name.invalid"));
        }
        else if (IdentifierUtils.hasLeadingOrTrailingWhitespace(name))
        {
            addFieldError("name", getText("invalid.whitespace"));
        }
        else
        {
            try
            {
                directoryManager.findUserByName(directoryID, name);

                // this isn't good, this name already exists
                addFieldError("name", getText("invalid.namealreadyexist"));
            }
            catch (Exception e)
            {
                // ignore
            }
        }

        try
        {
            directoryManager.findDirectoryById(directoryID).getType();
        }
        catch (DirectoryNotFoundException e)
        {
            // directory not found
            addFieldError("directoryID", getText("principal.directory.invalid"));
        }
    }

    public List<Directory> getDirectories()
    {
        return directoryManager.findAllDirectories();
    }
}