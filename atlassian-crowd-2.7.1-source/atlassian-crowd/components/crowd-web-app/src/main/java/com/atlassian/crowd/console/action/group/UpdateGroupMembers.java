package com.atlassian.crowd.console.action.group;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import com.atlassian.crowd.manager.directory.BulkAddResult;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.google.common.base.Joiner;
import com.google.common.collect.Iterables;

public class UpdateGroupMembers extends ViewGroupMembers
{
    private String entityName;
    private List<String> selectedEntityNames;
    private final static String ERROR_MULTIPLE_ADD_USERS = "group.error.multiple.add.user";

    @RequireSecurityToken(true)
    public String doAddUsers()
    {
        try
        {
            BulkAddResult<String> result = directoryManager.addAllUsersToGroup(directoryID, selectedEntityNames, entityName);
            Collection<String> failedUsers = result.getFailedEntities();
            Collection<String> existingUsers = result.getExistingEntities();
            if (!failedUsers.isEmpty() || !existingUsers.isEmpty())
            {
                String errorMsg = getText(ERROR_MULTIPLE_ADD_USERS) + " " + Joiner.on(", ").join(Iterables.concat(failedUsers, existingUsers));
                addActionError(errorMsg);
                logger.error(errorMsg);
                attemptToFetchGroupInformation();
                return ERROR;
            }
            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error("Failed to add users to group", e);
            attemptToFetchGroupInformation();
            return ERROR;
        }
    }

    @RequireSecurityToken(true)
    public String doRemoveUsers()
    {
        try
        {
            String crowdConsoleAdmin = getRemoteUser().getName();

            // Check if any of the membership removals will affect the currently logged in crowd console admin
            // If it is, check if the removal will be removing the last admin membership for the user.
            if (adminGroupChecker.isRemovingCrowdConsoleAdminMembership(crowdConsoleAdmin, getRemoteUser().getDirectoryId(), selectedEntityNames, directoryID))
            {
                if (adminGroupChecker.isRemovingConsoleAdminFromLastAdminGroup(entityName, crowdConsoleAdmin, directoryID))
                {
                    preventingLockout = true;
                    selectedEntityNames = new LinkedList(selectedEntityNames); // defensive copy before removing
                    selectedEntityNames.remove(crowdConsoleAdmin);
                }
            }

            for (String userMember : selectedEntityNames)
            {
                directoryManager.removeUserFromGroup(directoryID, userMember, entityName);
            }
            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error("Failed to remove users from group", e);
            attemptToFetchGroupInformation();
            return ERROR;
        }
    }

    @RequireSecurityToken(true)
    public String doAddGroups()
    {
        try
        {
            for (String groupMember : selectedEntityNames)
            {
                directoryManager.addGroupToGroup(directoryID, groupMember, entityName);
            }
            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error("Failed to add groups to group", e);
            attemptToFetchGroupInformation();
            return ERROR;
        }
    }

    @RequireSecurityToken(true)
    public String doRemoveGroups()
    {
        try
        {
            for (String groupMember : selectedEntityNames)
            {
                directoryManager.removeGroupFromGroup(directoryID, groupMember, entityName);
            }
            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error("Failed to remove groups from group", e);
            attemptToFetchGroupInformation();
            return ERROR;
        }
    }

    /**
     * When if the primary operation (add/remove) fails, we return to the same view, therefore
     * we make an attempt at fetching the group information, so it can be displayed. Errors are logged
     * but not reported to the user.
     */
    private void attemptToFetchGroupInformation()
    {
        try
        {
            groupName = entityName;
            fetchGroupInformation();
        }
        catch (Exception e)
        {
            logger.error("Failed to fetch group information", e);
        }
    }

    public String getEntityName()
    {
        return entityName;
    }

    public void setEntityName(String entityName)
    {
        this.entityName = entityName;
    }

    public List<String> getSelectedEntityNames()
    {
        return selectedEntityNames;
    }

    public void setSelectedEntityNames(List<String> selectedEntityNames)
    {
        this.selectedEntityNames = selectedEntityNames;
    }
}