package com.atlassian.crowd.console.logging;

import com.atlassian.config.bootstrap.AtlassianBootstrapManager;
import com.atlassian.crowd.CrowdConstants;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Layout;
import org.apache.log4j.Logger;
import org.apache.log4j.RollingFileAppender;
import org.apache.log4j.WriterAppender;
import org.apache.log4j.spi.LoggingEvent;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A Log4J appender which is aware of when the crowd.home property is set and will switch logging from the console
 * to within the crowd.home directory.
 * <p>
 * All the console logging will be buffered so that once the switch to logging in the crowd.home is made, all
 * existing logs can be written to this favoured location.
 * <p>
 * This appender accepts a subset of the RollingFileAppender's configuration, although while it is delegating to the
 * ConsoleAppender it will only use the layout settings.
 */
public class CrowdHomeLogAppender extends WriterAppender
{
    private final static Logger log = Logger.getLogger(CrowdHomeLogAppender.class);

    /**
     * The name of the crowd log file
     */
    private final static String DEFAULT_LOG_NAME = "atlassian-crowd.log";

    /**
     * The Appender used prior to crowd.home being set
     */
    private ConsoleAppender consoleAppender;

    /**
     * The Appender user after crowd.home is set
     */
    private RollingFileAppender crowdHomeAppender;

    /**
     * A reference to the current delegate Appender
     */
    private WriterAppender currentAppender;

    /**
     * A List of the LoggingEvent instances that have been sent to the ConsoleAppender
     */
    private List<LoggingEvent> logBuffer;

    private String logFileName;

    /**
     * Whether or not we've tried to switch to the crowdHomeAppender
     */
    private volatile boolean switchAttempted;

    /**
     * Creates the delegate Appenders although they are not ready for use until <code>activateOptions()</code>
     * is called.
     */
    public CrowdHomeLogAppender()
    {
        consoleAppender = new ConsoleAppender();
        crowdHomeAppender = new RollingFileAppender();
        currentAppender = consoleAppender;
        logBuffer = Collections.synchronizedList(new ArrayList<LoggingEvent>());
    }

    public void setMaxFileSize(String value)
    {
        crowdHomeAppender.setMaxFileSize(value);
    }

    public long getMaximumFileSize()
    {
        return crowdHomeAppender.getMaximumFileSize();
    }

    public void setMaxBackupIndex(int maxBackups)
    {
        crowdHomeAppender.setMaxBackupIndex(maxBackups);
    }

    public int getMaxBackupIndex()
    {
        return crowdHomeAppender.getMaxBackupIndex();
    }

    /**
     * Activate the default Appender (ConsoleAppender). The other appender will be activated once it's configuration
     * is known which is in response to the appropriate event.
     *
     * @see #switchAppender()
     */
    public void activateOptions()
    {
        assert consoleAppender != null;
        consoleAppender.activateOptions();
    }

    public Layout getLayout()
    {
        assert crowdHomeAppender.getLayout() == consoleAppender.getLayout() :
                "The delegate Appenders have different layouts.";
        return consoleAppender.getLayout();
    }

    public void setLayout(Layout layout)
    {
        consoleAppender.setLayout(layout);
        crowdHomeAppender.setLayout(layout);
    }

    /**
     * @param file just the log file name (not the full path)
     */
    public void setLogFileName(String file)
    {
        logFileName = file;
    }

    public String getLogFileName()
    {
        return logFileName;
    }

    /**
     * Append to the current delegate Appender. If this is the ConsoleAppender then also buffer the event for writing
     * when the RollingAppender is delegated to.
     *
     * @param event the information to log.
     */
    public void append(LoggingEvent event)
    {
        // only do this so we can eventually add these events to the log file
        // in the home directory. If we've already tried to switch from the
        // console to the home directory (whether we succeeded or not), then stop
        if (!switchAttempted)
        {
            logBuffer.add(event);
        }

        currentAppender.append(event);
    }

    public void close()
    {
        consoleAppender.close();
        crowdHomeAppender.close();
    }

    /**
     * Switch the delegate appender to the RollingFileAppender.
     * <p/>
     * It should be noted that since we need to avoid synchronization for every log message written, it is possible
     * that log messages being raised concurrently could be lost. This shouldn't be an issue since Crowd should
     * still be single threaded at the point when this happens.
     */
    void switchAppender()
    {
        switchAttempted = true;

        AtlassianBootstrapManager atlassianBootstrapManager = com.atlassian.config.util.BootstrapUtils.getBootstrapManager();
        String logsLocation = atlassianBootstrapManager.getApplicationHome() + File.separator + CrowdConstants.CrowdHome.LOG_FILE_LOCATION;

        File logDirectory = new File(logsLocation);
        // log4j does not create the directory automatically
        if (!logDirectory.exists() && !logDirectory.mkdir())
        {
            log.error("Could not create logs directory " + logsLocation + ". Logging remains directed to the ConsoleAppender.");
            return;
        }

        if (logFileName == null)
        {
            logFileName = DEFAULT_LOG_NAME;
        }

        String logfile = logsLocation + File.separator + logFileName;
        crowdHomeAppender.setFile(logfile);
        crowdHomeAppender.activateOptions();

        currentAppender = crowdHomeAppender;

        // now log and clear the logBuffer
        synchronized (logBuffer)
        {
            for (LoggingEvent event : logBuffer)
            {
                currentAppender.append(event);
            }
        }

        logBuffer.clear();
    }
}