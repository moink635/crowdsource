package com.atlassian.crowd.console.johnson;

import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.johnson.Initable;
import com.atlassian.johnson.setup.SetupConfig;
import com.atlassian.plugin.servlet.util.DefaultPathMapper;
import com.atlassian.plugin.servlet.util.PathMapper;

import java.util.Map;

public class CrowdJohnsonSetupConfig implements SetupConfig, Initable
{
    private PathMapper ignorePaths;

    public void init(Map<String, String> map)
    {
        ignorePaths = new DefaultPathMapper();
        ignorePaths.put("/console/setup/*.action", "/console/setup/*.action");
        ignorePaths.put("/console/setup/*.jsp", "/console/setup/*.jsp");
    }

    public boolean isSetupPage(String uri)
    {
        if (uri == null)
        {
            return true;
        }

        return ignoreUri(uri);
    }

    private boolean ignoreUri(String uri)
    {
        return (ignorePaths.get(uri) != null);
    }

    public boolean isSetup()
    {
        return BootstrapUtils.getBootstrapManager().isSetupComplete();
    }
}
