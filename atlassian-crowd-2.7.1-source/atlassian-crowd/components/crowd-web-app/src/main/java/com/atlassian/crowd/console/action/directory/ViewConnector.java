/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.directory;

import java.util.Collections;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Set;

import com.atlassian.core.util.DateUtils;
import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.ldap.validator.ConnectorValidator;
import com.atlassian.crowd.embedded.api.DirectorySynchronisationInformation;
import com.atlassian.crowd.embedded.api.DirectorySynchronisationRoundInformation;
import com.atlassian.crowd.exception.DirectoryInstantiationException;

public class ViewConnector extends AbstractViewDirectory
{
    protected String name;
    protected String directoryDescription;
    protected boolean active;

    private boolean cacheEnabled;

    private ConnectorValidator connectorValidator;
    private DirectorySynchronisationInformation syncInfo;

    @Override
    public String doDefault() throws Exception
    {
        long directoryId = getDirectory().getId();
        name = getDirectory().getName();
        directoryDescription = getDirectory().getDescription();
        active = getDirectory().isActive();
        syncInfo = directoryManager.getDirectorySynchronisationInformation(directoryId);
        cacheEnabled = Boolean.parseBoolean(getDirectory().getValue(DirectoryProperties.CACHE_ENABLED));

        // Check for any errors in connector configuration
        Set<String> directoryErrors = connectorValidator.getErrors(getDirectory());
        if (!directoryErrors.isEmpty())
        {
            for (String error : directoryErrors)
            {
                addActionError(error);
            }
        }
        return SUCCESS;
    }

    ///CLOVER:OFF
    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDirectoryDescription()
    {
        return directoryDescription;
    }

    public void setDirectoryDescription(String directoryDescription)
    {
        this.directoryDescription = directoryDescription;
    }

    public boolean getActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public final boolean isCurrentlySynchronising()
    {
        return syncInfo != null && syncInfo.isSynchronising();
    }

    public String getSynchronisationStartTime() throws DirectoryInstantiationException
    {
        final DirectorySynchronisationRoundInformation roundInfo = getRoundToDisplay();
        if (roundInfo == null)
        {
            return null;
        }
        return getText("directory.caching.sync.start.time", Collections.singletonList(new Date(roundInfo.getStartTime())));
    }

    public String getSynchronisationDuration() throws DirectoryInstantiationException
    {
        final DirectorySynchronisationRoundInformation roundInfo = getRoundToDisplay();
        if (roundInfo == null)
        {
            return null;
        }
        final ResourceBundle rb = getTexts(BaseAction.class.getName());
        return DateUtils.getDurationPrettySecondsResolution(roundInfo.getDurationMs() / 1000, rb);
    }

    public String getSynchronisationStatus() throws DirectoryInstantiationException
    {
        final DirectorySynchronisationRoundInformation roundInfo = getRoundToDisplay();
        if (roundInfo == null)
        {
            return getText("directory.caching.sync.never.label");
        }
        return getText(roundInfo.getStatusKey(), roundInfo.getStatusParameters(), false);
    }

    private DirectorySynchronisationRoundInformation getRoundToDisplay()
    {
        if (syncInfo == null)
        {
            return null;
        }

        if (syncInfo.getActiveRound() != null)
        {
            return syncInfo.getActiveRound();
        }
        else if (syncInfo.getLastRound() != null)
        {
            return syncInfo.getLastRound();
        }
        else
        {
            return null;
        }
    }

    public void setConnectorValidator(ConnectorValidator connectorValidator)
    {
        this.connectorValidator = connectorValidator;
    }

    public boolean isCacheEnabled()
    {
        return cacheEnabled;
    }

    public void setCacheEnabled(boolean cacheEnabled)
    {
        this.cacheEnabled = cacheEnabled;
    }
}
