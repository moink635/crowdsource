package com.atlassian.crowd.console.action.setup;

import com.atlassian.config.DefaultHomeLocator;
import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManagerImpl;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * Decides which step in the setup process we
 * are up to.
 */
public class SelectSetupStep extends BaseSetupAction
{
    private static final Logger log = Logger.getLogger(SelectSetupStep.class);

    private static final String CHECKLIST = "checklist";

    private String currentStepActionName;

    public String execute()
    {
        if (!isEverythingOk())
        {
            return CHECKLIST;
        }

        currentStepActionName = "/console/setup/" + getSetupPersister().getCurrentStep() + ".action";

        log.info("Current setup step is " + currentStepActionName);

        return SUCCESS;
    }

    /**
     * CurrentStep used in the the xwork.xml to choose what step to go to.
     *
     * @return action name to redirect to
     */
    public String getCurrentStep()
    {
        return currentStepActionName;
    }

    public String getJdkName()
    {
        return System.getProperty("java.vendor") + " - " + System.getProperty("java.version");
    }

    public String getServerName()
    {
        return ServletActionContext.getServletContext().getServerInfo();
    }

    public String getApplicationHome()
    {
        return getBootstrapManager().getApplicationHome();
    }

    public String getCrowdInitPropertiesLocation()
    {
        // TODO can we update the HomeLocator to give me the propertyFileName, or even give me the actual filepath ... please stop me doing this!
        URL url = ClassLoaderUtils.getResource(((DefaultHomeLocator) ((CrowdBootstrapManagerImpl) getBootstrapManager()).getHomeLocator()).getPropertiesFile(), SelectSetupStep.class);

        File file = new File(url.getFile());

        try
        {
            return file.getCanonicalPath();
        }
        catch (IOException e)
        {
            return "";
        }
    }

    public String getStepName()
    {
        return getSetupPersister().getCurrentStep();
    }
}
