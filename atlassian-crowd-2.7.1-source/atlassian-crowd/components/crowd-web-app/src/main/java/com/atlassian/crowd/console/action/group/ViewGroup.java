package com.atlassian.crowd.console.action.group;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalDirectoryGroup;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.rmi.RemoteException;

import com.google.common.collect.ImmutableSet;

public class ViewGroup extends BaseAction
{
    private static final ImmutableSet<DirectoryType> DIRECTORY_TYPES_THAT_SUPPORT_LOCAL_GROUPS =
        ImmutableSet.of(DirectoryType.DELEGATING, DirectoryType.CONNECTOR);
    protected final Logger logger = Logger.getLogger(this.getClass());

    protected Group group;
    protected Directory directory;

    protected long directoryID = -1;
    protected String name;
    protected String description;
    protected boolean active;
    protected String directoryName;
    private String locationKey;

    protected boolean hasSubGroups;
    protected boolean supportsNestedGroups;
    private String directoryImplementationDescriptiveName;

    private DirectoryInstanceLoader directoryInstanceLoader;

    public String doDefault()
    {
        try
        {
            processGeneral();
        } catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return SUCCESS;
    }

    protected void processGeneral()
            throws OperationFailedException, RemoteException, DirectoryNotFoundException, GroupNotFoundException
    {
        if (StringUtils.isNotBlank(directoryName) && directoryID == -1)
        {
            directory = directoryManager.findDirectoryByName(directoryName);
            directoryID = directory.getId();
        }
        else
        {
            directory = directoryManager.findDirectoryById(directoryID);
        }

        group = directoryManager.findGroupByName(directoryID, name);
        initFormFieldValues();

        RemoteDirectory remoteDirectory = directoryInstanceLoader.getDirectory(directory);
        supportsNestedGroups = remoteDirectory.supportsNestedGroups();
        directoryImplementationDescriptiveName = remoteDirectory.getDescriptiveName();

        // used to display the "All Members" tab.
        if (supportsNestedGroups)
        {
            // searches for group members of the group to check if we should display the "All Members" tab. Potential performance issue.
            hasSubGroups = !directoryManager.searchDirectGroupRelationships(directoryID,
                    QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP))
                            .childrenOf(EntityDescriptor.group(GroupType.GROUP))
                            .withName(name)
                            .returningAtMost(1)).isEmpty();
        }
        else
        {
            hasSubGroups = false;
        }

        if (directory != null && DIRECTORY_TYPES_THAT_SUPPORT_LOCAL_GROUPS.contains(directory.getType())
            && group instanceof InternalDirectoryGroup)
        {
            locationKey = ((InternalDirectoryGroup) group).isLocal() ? "group.location.local" : "group.location.remote";
        }
        else
        {
            locationKey = null;
        }
    }

    protected void initFormFieldValues()
    {
        name = group.getName();
        description = group.getDescription();
        active = group.isActive();
    }

    ///CLOVER:OFF
    public Directory getDirectory()
    {
        return directory;
    }

    public long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(long directoryID)
    {
        this.directoryID = directoryID;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public boolean isHasSubGroups()
    {
        return hasSubGroups;
    }

    public String getDirectoryName()
    {
        return directoryName;
    }

    public void setDirectoryName(String directoryName)
    {
        this.directoryName = directoryName;
    }

    public boolean isSupportsNestedGroups()
    {
        return supportsNestedGroups;
    }

    public void setDirectoryInstanceLoader(DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }

    public String getLocationKey()
    {
        return locationKey;
    }

    public String getDirectoryImplementationDescriptiveName()
    {
        return directoryImplementationDescriptiveName;
    }
}
