package com.atlassian.crowd.console.action.group;

import com.atlassian.crowd.console.action.AbstractBrowser;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.util.SelectionUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction.BooleanLogic.AND;

public class BrowseGroups extends AbstractBrowser<Group>
{
    private static final Logger logger = Logger.getLogger(BrowseGroups.class);

    private String active;
    private String name;
    private long directoryID = -1;
    private List directories;

    public String execute()
    {
        try
        {
            // select only directory if the list has only 1 result
            directories = directoryManager.findAllDirectories();
            if (directoryID == -1)
            {
                directoryID = SelectionUtils.getSelectedDirectory(directories);
            }

            // only perform the search if a directory ID was supplied
            if (directoryID != -1)
            {
                Collection<SearchRestriction> restrictions = new ArrayList<SearchRestriction>();

                if (active != null && active.length() > 0)
                {
                    restrictions.add(Restriction.on(GroupTermKeys.ACTIVE).exactlyMatching(Boolean.valueOf(active)));
                }

                if (name != null && name.length() > 0)
                {
                    restrictions.add(Restriction.on(GroupTermKeys.NAME).containing(name));
                }

                SearchRestriction restriction = restrictions.isEmpty() ? NullRestrictionImpl.INSTANCE : new BooleanRestrictionImpl(AND, restrictions.toArray(new SearchRestriction[restrictions.size()]));

                results = directoryManager.searchGroups(directoryID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP)).with(restriction).startingAt(resultsStart).returningAtMost(resultsPerPage + 1));

                SelectionUtils.saveSelectedDirectory(directoryID);
            }
            else
            {
                results = new ArrayList<Group>(0);
            }
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return SUCCESS;
    }

    public String getActive()
    {
        return active;
    }

    public void setActive(String active)
    {
        this.active = active;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(long directoryID)
    {
        this.directoryID = directoryID;
    }

    public List getDirectories()
    {
        return directories;
    }
}