package com.atlassian.crowd.console.filter;

import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManagerImpl;
import org.springframework.orm.hibernate4.support.OpenSessionInViewFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * An OpenSessionInViewFilter which does not
 * attempt to wire up (and hence run) the filter unless
 * Crowd's Spring container has been loaded successfully.
 *
 * See CrowdContextLoaderListener and BootstrappedContextLoaderListener
 * for more details.
 */
public class CrowdOpenSessionInViewFilter extends OpenSessionInViewFilter
{
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException
    {
        if (CrowdBootstrapManagerImpl.isContainterReady(getServletContext()))
        {
            super.doFilterInternal(request, response, filterChain);
        }
        else
        {
            filterChain.doFilter(request, response);
        }
    }
}
