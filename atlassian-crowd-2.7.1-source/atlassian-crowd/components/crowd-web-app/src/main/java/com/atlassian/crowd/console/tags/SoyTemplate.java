package com.atlassian.crowd.console.tags;

import com.atlassian.crowd.plugin.PluginComponentAccessor;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.opensymphony.webwork.WebWorkException;
import com.opensymphony.webwork.components.Component;
import com.opensymphony.xwork.util.OgnlValueStack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Writer;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * @since v2.7
 */
public class SoyTemplate extends Component
{
    private static final Logger log = LoggerFactory.getLogger(SoyTemplate.class);
    private final String moduleKey;
    private final String template;
    /**
     * Constructor.
     *
     * @param stack OGNL value stack.
     * @param moduleKey the complete key of the module containing the Soy template to call, in the form &lt;pluginKey>:&lt;moduleName>
     * @param template the name of the Soy template to call
     */
    public SoyTemplate(OgnlValueStack stack, String moduleKey, String template)
    {
        super(stack);
        this.moduleKey = moduleKey;
        this.template = template;
    }

    @Override
    public boolean end(Writer writer, String body)
    {
        if (isEmpty(moduleKey))
        {
            throw new WebWorkException(String.format("Failed to render soy tag; "
                    + "moduleKey attribute's value of \"%s\" evaluated to an empty string."
                    + "\nPerhaps you need to enclose the value in single quotes?", moduleKey));
        }

        if (isEmpty(template))
        {
            throw new WebWorkException(String.format("Failed to render soy tag; "
                    + "template attribute's value of \"%s\" evaluated to an empty string."
                    + "\nPerhaps you need to enclose the value in single quotes?", template));
        }

        if (!isEmpty(id))
        {
            addParameter("id", id);
        }

        try
        {
            log.debug("Using template '{}' from module '{}'", template, moduleKey);
            final String output = getSoyRenderer().render(moduleKey, template, getParameters());
            writer.write(output);
        }
        catch (SoyException e)
        {
            throw new WebWorkException(String.format("Soy rendering failed for template '%s'.", template), e);
        }
        catch (IOException e)
        {
            throw new WebWorkException(String.format("Failed to render soy tag, using template '%s' from module '%s'.", template, moduleKey), e);
        }

        return super.end(writer, "");
    }

    protected SoyTemplateRenderer getSoyRenderer() throws SoyException
    {
        final SoyTemplateRenderer soyTemplateRenderer = PluginComponentAccessor.getOSGiComponentInstanceOfType(SoyTemplateRenderer.class);

        if (soyTemplateRenderer == null)
        {
            throw new SoyException("Could not get SoyTemplateRenderer. Is the soy-template-plugin installed?");
        }
        return soyTemplateRenderer;
    }

    @Override
    public boolean usesBody()
    {
        return true;
    }
}
