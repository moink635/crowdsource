package com.atlassian.crowd.console.action.dataimport;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.importer.config.DirectoryConfiguration;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.interceptor.SessionAware;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * This action handles the setup of an import from one directory to another.
 */
public class DirectoryImporter extends BaseImporter implements SessionAware
{
    private Long sourceDirectoryID;

    private Long targetDirectoryID;

    private Map session;

    private Boolean overwriteTarget;

    public String execute() throws Exception
    {
        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doImport() throws DirectoryInstantiationException, DirectoryNotFoundException
    {
        // validate input
        doValidation();

        if (hasErrors())
        {
            return ERROR;
        }

        // Everthing is fine, set these values in the Configuration
        getConfiguration().setDirectoryID(targetDirectoryID);
        getConfiguration().setSourceDirectoryID(sourceDirectoryID);
        getConfiguration().setOverwriteTarget(overwriteTarget);
        getConfiguration().setImportNestedGroups(determineIfNestedGroupImport());

        return SUCCESS;
    }

    public boolean determineIfNestedGroupImport() throws DirectoryInstantiationException, DirectoryNotFoundException
    {
        if (getConfiguration() != null &&
            directoryManager.supportsNestedGroups(getConfiguration().getSourceDirectoryID()) &&
            directoryManager.supportsNestedGroups(getConfiguration().getDirectoryID()))
        {
            return true;
        }

        return false;
    }

    private void doValidation()
    {
        // Validate that the source and target directories are not the same
        if (getSourceDirectoryID().equals(getTargetDirectoryID()))
        {
            addFieldError("targetDirectoryID", getText("dataimport.directory.sourcetargetsame.error"));
        }

        if (!hasErrors())
        {
            // get both directories and validate password encoders
            try
            {
                Directory targetDirectory = directoryManager.findDirectoryById(targetDirectoryID);

                Directory sourceDirectory = directoryManager.findDirectoryById(sourceDirectoryID);

                if (!isSameEncryptionType(targetDirectory.getEncryptionType(), sourceDirectory.getEncryptionType()))
                {
                    addActionError(getText("dataimport.directory.same.encryption.error"));
                }
            }
            catch (DirectoryNotFoundException e)
            {
                addActionError(e);
            }
        }

    }

    private boolean isSameEncryptionType(String targetEncryption, String sourceEncryption)
    {
        // Trim encryption types to empty to treat null and empty encryption as the same (CWD-1675)
        return StringUtils.equals(StringUtils.trimToEmpty(targetEncryption), StringUtils.trimToEmpty(sourceEncryption));
    }

    public DirectoryConfiguration getConfiguration()
    {
        DirectoryConfiguration configuration = (DirectoryConfiguration) session.get(BaseImporter.IMPORTER_CONFIGURATION);
        if (configuration == null)
        {
            configuration = constructDefaultConfiguration();
            session.put(IMPORTER_CONFIGURATION, configuration);
        }
        return configuration;
    }

    private DirectoryConfiguration constructDefaultConfiguration()
    {
        return new DirectoryConfiguration(null, null, null);
    }

    public Long getSourceDirectoryID()
    {
        if (sourceDirectoryID == null && getConfiguration().getSourceDirectoryID() != null)
        {
            sourceDirectoryID = getConfiguration().getSourceDirectoryID();
        }
        return sourceDirectoryID;
    }

    public void setSourceDirectoryID(Long sourceDirectoryID)
    {
        this.sourceDirectoryID = sourceDirectoryID;
    }

    public Boolean getOverwriteTarget()
    {
        if (overwriteTarget == null && getConfiguration().isOverwriteTarget() != null)
        {
            overwriteTarget = getConfiguration().isOverwriteTarget();
        }
        return overwriteTarget;
    }

    public void setOverwriteTarget(Boolean overwriteTarget)
    {
        this.overwriteTarget = overwriteTarget;
    }

    public Long getTargetDirectoryID()
    {
        if (targetDirectoryID == null && getConfiguration().getDirectoryID() != null)
        {
            targetDirectoryID = getConfiguration().getDirectoryID();
        }
        return targetDirectoryID;
    }

    public void setTargetDirectoryID(Long targetDirectoryID)
    {
        this.targetDirectoryID = targetDirectoryID;
    }

    public void setSession(Map session)
    {
        this.session = session;
    }
}

