package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import java.util.List;

interface LDAPConfigurationTester
{

    boolean canFindLdapObjects(LDAPConfiguration configuration, Strategy strategy) throws OperationFailedException;

    enum Strategy
    {

        USER
                {
                    @Override
                    EntityDescriptor getEntityDescriptor()
                    {
                        return EntityDescriptor.user();
                    }

                    @Override
                    List<String> search(final RemoteDirectory directory, final EntityQuery<String> query) throws OperationFailedException
                    {
                        return directory.searchUsers(query);
                    }

                },

        GROUP
                {
                    @Override
                    EntityDescriptor getEntityDescriptor()
                    {
                        return EntityDescriptor.group();
                    }

                    @Override
                    List<String> search(final RemoteDirectory directory, final EntityQuery<String> query) throws OperationFailedException
                    {
                        return directory.searchGroups(query);
                    }

                };

        abstract EntityDescriptor getEntityDescriptor();

        abstract List<String> search(RemoteDirectory directory, EntityQuery<String> query) throws OperationFailedException;

    }

}
