package com.atlassian.crowd.console.action;

import com.atlassian.config.ConfigurationException;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.integration.http.HttpAuthenticator;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.manager.application.ApplicationAccessDeniedException;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.crowd.util.SystemInfoHelper;
import com.atlassian.crowd.xwork.RequireSecurityToken;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.event.Event;
import com.atlassian.johnson.event.EventLevel;
import com.atlassian.johnson.event.EventType;
import com.atlassian.license.SIDManager;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import static com.google.common.base.Preconditions.checkNotNull;

public class License extends BaseAction
{
    private static final Logger logger = Logger.getLogger(Login.class);

    private String username;
    private String password;
    private String key;

    private String crowdSid;

    private SIDManager sidManager;

    private SystemInfoHelper systemInfoHelper;

    protected long currentResources;
    private HttpAuthenticator httpAuthenticator;

    public String doDefault()
    {
        processLicenseDetails();

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate() throws Exception
    {
        // Validate that the entered license can be accepted
        doValidation();

        if (hasErrors())
        {
            processLicenseDetails();
            return ERROR;
        }
        else
        {
            try
            {
                // attempt to set the license, if the license can not be set the returned object is null.
                final CrowdLicense license = crowdLicenseManager.storeLicense(key);
                if (license == null)
                {
                    addFieldError("key", getText("license.key.error.invalid"));
                    processLicenseDetails();
                    return ERROR;
                }

                // Remove the Old Licence Event
                JohnsonEventContainer cont = Johnson.getEventContainer(ServletActionContext.getServletContext());
                for (Event event : cont.getEvents())
                {
                    if (event != null && event.getKey().equals(EventType.get("license-too-old")))
                    {
                        cont.removeEvent(event);
                    }
                }

                // Once updating the license a restart of Crowd is required, this will kick off the
                // Upgrade tasks.
                cont.addEvent(new Event(checkNotNull(EventType.get("restart")), getText("error.console.restart.text"), checkNotNull(EventLevel.get(EventLevel.ERROR))));

                return SUCCESS;
            }
            catch (Exception e)
            {
                processLicenseDetails();

                addActionError(getText("invalidlogin.label"));
                logger.error(e.getMessage(), e);
            }
        }

        return INPUT;
    }

    private void doValidation() throws OperationFailedException
    {
        if (StringUtils.isBlank(key))
        {
            addFieldError("key", getText("license.key.error.blank"));
            return;
        }

        if (isAuthenticationRequired())
        {
            if (StringUtils.isBlank(username))
            {
                addFieldError("username", getText("login.username.error"));
                return;
            }

            if (StringUtils.isBlank(password))
            {
                addFieldError("password", getText("login.password.error"));
                return;
            }

            try
            {
                tokenAuthenticationManager.authenticateUser(httpAuthenticator.getPrincipalAuthenticationContext(ServletActionContext.getRequest(), ServletActionContext.getResponse(), username, password),
                                                            TokenLifetime.USE_DEFAULT);
                CrowdUserDetails crowdUserDetails = crowdUserDetailsService.loadUserByUsername(username);

                if (!hasAdminRole(crowdUserDetails))
                {
                    addFieldError("username", getText("login.failed.invalidcredentials"));
                    return;
                }
            }
            catch (InvalidAuthenticationException e)
            {
                addFieldError("username", getText("login.failed.invalidcredentials"));
            }
            catch (InactiveAccountException e)
            {
                addFieldError("username", getText("login.failed.invalidcredentials"));
            }
            catch (ApplicationAccessDeniedException e)
            {
                addFieldError("username", getText("login.failed.invalidcredentials"));
            }
            catch (ExpiredCredentialException e)
            {
                addFieldError("username", getText("login.failed.invalidcredentials"));
            }
            catch (ApplicationNotFoundException e)
            {
                throw new OperationFailedException(e);
            }
        }

        if (!crowdLicenseManager.isLicenseKeyValid(key))
        {
            addFieldError("key", getText("license.key.error.invalid"));
        }

        if (!hasErrors() && !crowdLicenseManager.isBuildWithinMaintenancePeriod(key))
        {
            addFieldError("key", getText("license.key.error.maintenance.expired"));
        }
    }

    protected void processLicenseDetails()
    {
        currentResources = propertyManager.getCurrentLicenseResourceTotal();
        crowdSid = getBootstrapManager().getServerID();

        if (crowdSid == null)
        {
            crowdSid = getSidManager().generateSID();
            try
            {
                getBootstrapManager().setServerID(crowdSid);
            }
            catch (ConfigurationException e)
            {
                addActionError(e);
            }
        }
    }

    public boolean isAtResourceLimit()
    {
        final CrowdLicense license = getLicense();
        return license != null && !license.isUnlimitedNumberOfUsers() && propertyManager.getCurrentLicenseResourceTotal() > license.getMaximumNumberOfUsers();
    }

    /**
     * Returns true if the user needs to be authenticated before updating the license.
     *
     * Unauthenticated updating is allowed when Crowd is locked up because of invalid license or too new Crowd version.
     *
     * @return true if the user needs to be authenticated before updating the license
     */
    public Boolean isAuthenticationRequired()
    {
        final CrowdLicense license = crowdLicenseManager.getLicense();
        return crowdLicenseManager.isLicenseValid(license) && crowdLicenseManager.isBuildWithinMaintenancePeriod(license);
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public SIDManager getSidManager()
    {
        return sidManager;
    }

    public void setSidManager(SIDManager sidManager)
    {
        this.sidManager = sidManager;
    }

    public SystemInfoHelper getSystemInfoHelper()
    {
        return systemInfoHelper;
    }

    public void setSystemInfoHelper(SystemInfoHelper systemInfoHelper)
    {
        this.systemInfoHelper = systemInfoHelper;
    }

    public String getCrowdSid()
    {
        return crowdSid;
    }

    public long getCurrentResources()
    {
        return currentResources;
    }

    public void setHttpAuthenticator(HttpAuthenticator httpAuthenticator)
    {
        this.httpAuthenticator = httpAuthenticator;
    }
}
