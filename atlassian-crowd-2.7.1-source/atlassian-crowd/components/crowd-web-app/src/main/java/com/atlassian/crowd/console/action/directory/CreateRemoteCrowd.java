package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.console.value.directory.RemoteCrowdConnection;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.RemoteCrowdDirectory;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.SynchronisableDirectoryProperties;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class CreateRemoteCrowd extends CreateDirectory
{
    private static final Logger logger = Logger.getLogger(CreateRemoteCrowd.class);

    // basic attributes
    private String url;
    private String applicationName;
    private String applicationPassword;
    private String savedApplicationPassword;
    private boolean cacheEnabled;
    private boolean useNestedGroups;

    private final RemoteCrowdConnection connection = new RemoteCrowdConnection();

    private String httpProxyHost;
    private Integer httpProxyPort;
    private String httpProxyUsername;
    private String httpProxyPassword;

    // Spring injected
    private PasswordEncoderFactory passwordEncoderFactory;
    private DirectoryInstanceLoader directoryInstanceLoader;

    @Override
    public String doDefault()
    {
        super.doDefault();

        // Configure a default address for the remote Crowd server.
        if (StringUtils.isBlank(url))
        {
            url = "http://hostname:8095/crowd";
        }

        cacheEnabled = true;
        useNestedGroups = false;
        connection.setIncrementalSyncEnabled(true);

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            // If update has occurred, it is no longer the initialLoad
            initialLoad = false;

            restoreSavedPassword();

            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                addActionError(getText("directorycrowd.update.invalid"));
                return INPUT;
            }

            Directory directory = buildDirectoryConfiguration();

            // test our connection, as if it was at runtime to see if the connection can be established
            validateConnection(directory);
            if (hasErrors())
            {
                tab = 2;
                return INPUT;
            }

            directory = directoryManager.addDirectory(directory);
            ID = directory.getId();

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    private void validateConnection(final Directory directory)
    {
        try
        {
            RemoteDirectory remoteDirectory = directoryInstanceLoader.getRawDirectory(directory.getId(), directory.getImplementationClass(), directory.getAttributes());
            remoteDirectory.testConnection();
        }
        catch (Throwable e)
        {
            tab = 2;
            addActionError(getText("directorycrowd.testconnection.invalid") + " " + e.getMessage());
        }
    }

    public String doTestConnection()
    {
        try
        {
            restoreSavedPassword();

            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                return INPUT;
            }

            tab = 2; // return to the same tab the button is in

            Directory directory = buildDirectoryConfiguration();

            RemoteDirectory remoteDirectory = directoryInstanceLoader.getRawDirectory(directory.getId(), directory.getImplementationClass(), directory.getAttributes());

            remoteDirectory.testConnection();

            actionMessageAlertColor = ALERT_BLUE;

            addActionMessage(getText("directorycrowd.testconnection.success"));
            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(getText("directorycrowd.testconnection.invalid") + " " + e.getMessage());
            return INPUT;
        }
    }

    /**
     * Restore password from previous request(ex: Test connection) to the ldapPassword to ensure we don't save an empty password - refer CWD-1763
     */
    private void restoreSavedPassword()
    {
        if (StringUtils.isBlank(applicationPassword))
        {
            applicationPassword = savedApplicationPassword;
        }
    }

    protected DirectoryImpl buildDirectoryConfiguration()
    {
        DirectoryImpl directory = new DirectoryImpl(name, DirectoryType.CROWD, RemoteCrowdDirectory.class.getName());

        directory.setActive(active);
        directory.setDescription(description);

        // set connection information
        directory.setAttribute(RemoteCrowdDirectory.CROWD_SERVER_URL, url);
        directory.setAttribute(RemoteCrowdDirectory.APPLICATION_NAME, applicationName);
        directory.setAttribute(RemoteCrowdDirectory.APPLICATION_PASSWORD, applicationPassword);

        directory.setAttribute(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL, Long.toString(getPollingIntervalInMin() * 60));

        directory.setAttribute(RemoteCrowdDirectory.CROWD_HTTP_TIMEOUT, Long.toString(getHttpTimeout() * 1000));
        directory.setAttribute(RemoteCrowdDirectory.CROWD_HTTP_MAX_CONNECTIONS, Long.toString(getHttpMaxConnections()));

        directory.setAttribute(RemoteCrowdDirectory.CROWD_HTTP_PROXY_HOST, StringUtils.isBlank(httpProxyHost)? null:httpProxyHost);
        directory.setAttribute(RemoteCrowdDirectory.CROWD_HTTP_PROXY_PORT, httpProxyPort==null ? null:Integer.toString(httpProxyPort));
        directory.setAttribute(RemoteCrowdDirectory.CROWD_HTTP_PROXY_USERNAME, StringUtils.isBlank(httpProxyUsername)? null:httpProxyUsername);
        directory.setAttribute(RemoteCrowdDirectory.CROWD_HTTP_PROXY_PASSWORD, StringUtils.isBlank(httpProxyPassword)? null:httpProxyPassword);

        directory.setAttribute(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED, Boolean.toString(isIncrementalSyncEnabled()));

        directory.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.toString(cacheEnabled));
        directory.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_USE_NESTED_GROUPS, Boolean.toString(useNestedGroups));

        // set the permissions
        directory.setAllowedOperations(permissions);

        return directory;
    }

    protected void doValidation()
    {
        // the tabs are set from last to first so the user will correct the information on the input page
        // as the results come up.
        // reset the tab, for error results
        tab = null;

        // details tab 1
        validateDetails();

        // connection tab 2
        validateConnection();
    }

    protected void validateDetails()
    {
        if (StringUtils.isBlank(name))
        {
            addFieldError("name", getText("directoryinternal.name.invalid"));
        }
        else
        {
            try
            {
                directoryManager.findDirectoryByName(name);
                addFieldError("name", getText("invalid.namealreadyexist"));
            }
            catch (Exception e)
            {
                // ignore
            }
        }

        if (hasErrors() && tab == null)
        {
            tab = 1;
        }
    }

    protected void validateConnection()
    {
        if (StringUtils.isBlank(url))
        {
            addFieldError("url", getText("directorycrowd.url.invalid"));
        }

        if (StringUtils.isBlank(applicationName))
        {
            addFieldError("applicationName", getText("directorycrowd.applicationname.invalid"));
        }

        if (StringUtils.isEmpty(applicationPassword))
        {
            addFieldError("applicationPassword", getText("directorycrowd.applicationpassword.invalid"));
        }

        if (getPollingIntervalInMin() <= 0)
        {
            addFieldError("pollingIntervalInMin", getText("directorycrowd.polling.interval.invalid"));
        }

        if (getHttpMaxConnections() <= 0)
        {
            addFieldError("httpMaxConnections", getText("directorycrowd.http.maxconnections.invalid"));
        }

        if (httpProxyPort != null && httpProxyPort <= 0)
        {
            addFieldError("httpProxyPort", getText("directorycrowd.proxy.port.invalid"));
        }

        if (hasErrors() && tab == null)
        {
            tab = 2;
        }
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getApplicationName()
    {
        return applicationName;
    }

    public void setApplicationName(String applicationName)
    {
        this.applicationName = applicationName;
    }

    public String getApplicationPassword()
    {
        return applicationPassword;
    }

    public void setApplicationPassword(String applicationPassword)
    {
        this.applicationPassword = applicationPassword;
    }

    public boolean isIncrementalSyncEnabled()
    {
        return connection.isIncrementalSyncEnabled();
    }

    public void setIncrementalSyncEnabled(boolean incrementalSyncEnabled)
    {
        connection.setIncrementalSyncEnabled(incrementalSyncEnabled);
    }

    public long getPollingIntervalInMin()
    {
        return connection.getPollingIntervalInMin();
    }

    public void setPollingIntervalInMin(long pollingIntervalInMin)
    {
        connection.setPollingIntervalInMin(pollingIntervalInMin);
    }

    public boolean isCacheEnabled()
    {
        return cacheEnabled;
    }

    public void setCacheEnabled(boolean cacheEnabled)
    {
        this.cacheEnabled = cacheEnabled;
    }

    public PasswordEncoderFactory getPasswordEncoderFactory()
    {
        return passwordEncoderFactory;
    }

    public void setPasswordEncoderFactory(PasswordEncoderFactory passwordEncoderFactory)
    {
        this.passwordEncoderFactory = passwordEncoderFactory;
    }

    public DirectoryInstanceLoader getDirectoryInstanceLoader()
    {
        return directoryInstanceLoader;
    }

    public void setDirectoryInstanceLoader(DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }

    public boolean isUseNestedGroups()
    {
        return useNestedGroups;
    }

    public void setUseNestedGroups(final boolean useNestedGroups)
    {
        this.useNestedGroups = useNestedGroups;
    }

    public long getHttpTimeout()
    {
        return connection.getHttpTimeout();
    }

    public void setHttpTimeout(long httpTimeout)
    {
        connection.setHttpTimeout(httpTimeout);
    }

    public long getHttpMaxConnections()
    {
        return connection.getHttpMaxConnections();
    }

    public void setHttpMaxConnections(long httpMaxConnections)
    {
        connection.setHttpMaxConnections(httpMaxConnections);
    }

    public String getHttpProxyHost()
    {
        return httpProxyHost;
    }

    public void setHttpProxyHost(String httpProxyHost)
    {
        this.httpProxyHost = httpProxyHost;
    }

    public Integer getHttpProxyPort()
    {
        return httpProxyPort;
    }

    public void setHttpProxyPort(Integer httpProxyPort)
    {
        this.httpProxyPort = httpProxyPort;
    }

    public String getHttpProxyUsername()
    {
        return httpProxyUsername;
    }

    public void setHttpProxyUsername(String httpProxyUsername)
    {
        this.httpProxyUsername = httpProxyUsername;
    }

    public String getHttpProxyPassword()
    {
        return httpProxyPassword;
    }

    public void setHttpProxyPassword(String httpProxyPassword)
    {
        this.httpProxyPassword = httpProxyPassword;
    }

    public void setSavedApplicationPassword(String savedApplicationPassword)
    {
        this.savedApplicationPassword = savedApplicationPassword;
    }
}
