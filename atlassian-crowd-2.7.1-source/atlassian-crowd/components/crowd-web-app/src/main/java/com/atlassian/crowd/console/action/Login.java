package com.atlassian.crowd.console.action;

import com.atlassian.crowd.console.filter.LoginCsrfFilter.LoginCsrfTokenInvalidException;
import com.atlassian.crowd.console.filter.LoginCsrfFilter.LoginCsrfTokenMissingException;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.integration.springsecurity.CrowdAccessDeniedException;
import com.atlassian.crowd.integration.springsecurity.RequestToApplicationMapper;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.util.SSOUtils;

import com.opensymphony.webwork.ServletActionContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class Login extends BaseAction
{
    private static final Logger logger = Logger.getLogger(Login.class);
    static final String EXPIRED_PASSWORD = "expired-password";


    private RequestToApplicationMapper requestToApplicationMapper;

    private String j_username;
    private String j_password;
    private boolean error;
    private String applicationName;
    private boolean showForgotPassword;

    public String execute()
    {
        if (isAuthenticated())
        {
            return SUCCESS;
        }
        if (error)
        {
            // get error information out of spring security (springsec puts this info in session)
            String lastUsername = (String) getSession().getAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY);
            if (lastUsername != null)
            {
                j_username = lastUsername;
            }

            Exception e = (Exception) getSession().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
            if (e == null)
            {
                addActionError(getText("login.failed.label"));
            }
            else if (e instanceof DisabledException)
            {
                logger.info("The user: '" + j_username + "' attempted to login with an inactive account");
                addActionError(getText("login.failed.label"));
            }
            else if (e instanceof BadCredentialsException)
            {
                logger.info("The user: '" + j_username + "' attempted to login with incorrect credentials");
                addActionError(getText("login.failed.label"));
            }
            else if (e instanceof CrowdAccessDeniedException)
            {
                logger.info("The user: '" + j_username + "' attempted to login to an unauthorised application: " + e.getMessage());
                addActionError(getText("login.failed.label"));
            }
            else if (e instanceof CredentialsExpiredException)
            {
                logger.info("The user: '" + j_username + "' attempted to login with expired credentials");
                addActionError(getText("login.failed.label"));
                return EXPIRED_PASSWORD;
            }
            else if (e instanceof AuthenticationServiceException)
            {
                logger.error("Failed to connect to the authentication server, please check your crowd.properties", e);
                addActionError(getText("login.failed.serverexception"));
            }
            else if (e instanceof LoginCsrfTokenMissingException)
            {
                logger.debug("Did not attempt login because CSRF token was missing");
                addActionError(getText("atlassian.xwork.xsrf.notoken"));
            }
            else if (e instanceof LoginCsrfTokenInvalidException)
            {
                logger.debug("Did not attempt login because CSRF token was invalid");
                addActionError(getText("atlassian.xwork.xsrf.badtoken"));
            }
            else
            {
                logger.error(e.getMessage(), e);
                addActionError(getText("login.failed.label"));
            }
        }

        applicationName = requestToApplicationMapper.getApplication(ServletActionContext.getRequest());
        showForgotPassword = "crowd".equals(applicationName);

        // use the description if it exists
        try
        {
            Application application = applicationManager.findByName(applicationName);
            if (StringUtils.isNotBlank(application.getDescription()))
            {
                applicationName = application.getDescription();
            }
        }
        catch (ApplicationNotFoundException e)
        {
            // do nothing
        }

        return INPUT;
    }

    public boolean isDomainValid()
    {
        return isDomainValid(propertyManager);
    }

    public static boolean isDomainValid(PropertyManager propertyManager)
    {
        try
        {
            final String hostname = ServletActionContext.getRequest().getServerName();
            final String cookieDomain = propertyManager.getDomain();
            // Check if domain entry in property is invalid
            if (!SSOUtils.isCookieDomainValid(cookieDomain, hostname))
            {
                // Domain in property manager is invalid
                logger.error("Invalid SSO Domain value: '" + cookieDomain + "' found in settings. Domain currently in use is: '" + hostname + "'.");
                return false;
            }
        }
        catch (PropertyManagerException e)
        {
            logger.error(e.getMessage(), e);
        }

        return true;
    }

    ///CLOVER:OFF
    public String getJ_username()
    {
        return j_username;
    }

    public void setJ_username(String j_username)
    {
        this.j_username = j_username;
    }

    public String getJ_password()
    {
        return j_password;
    }

    public void setJ_password(String j_password)
    {
        this.j_password = j_password;
    }

    public boolean isError()
    {
        return error;
    }

    public void setError(boolean error)
    {
        this.error = error;
    }

    public String getApplicationName()
    {
        return applicationName;
    }

    public void setRequestToApplicationMapper(RequestToApplicationMapper requestToApplicationMapper)
    {
        this.requestToApplicationMapper = requestToApplicationMapper;
    }

    public boolean isShowForgotPassword()
    {
        return showForgotPassword;
    }
}