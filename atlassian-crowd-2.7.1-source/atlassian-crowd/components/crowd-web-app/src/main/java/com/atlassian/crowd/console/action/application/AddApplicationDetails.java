package com.atlassian.crowd.console.action.application;

import com.atlassian.core.util.PairType;
import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.model.application.ApplicationType;
import com.opensymphony.webwork.interceptor.SessionAware;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

/**
 *
 */
public class AddApplicationDetails extends BaseAction implements SessionAware
{
    private String name;
    private String description;
    private String password;
    private String passwordConfirmation;
    private List<PairType> applicationTypes;
    private String applicationType;

    private Map sessionData;
    protected static final String APPLICATION_SESSION_CONFIGURATION_SETUP = "APPLICATION_CONFIGURATION_SETUP";

    public String execute()
    {
        ApplicationConfiguration configuration = getApplicationConfigurationFromSession();

        name = configuration.getName();
        description = configuration.getDescription();
        password = configuration.getName();
        ApplicationType type = configuration.getApplicationType();
        if (type != null)
        {
            applicationType = type.name();
        }

        return INPUT;
    }

    private ApplicationConfiguration getApplicationConfigurationFromSession()
    {
        ApplicationConfiguration configuration = (ApplicationConfiguration) sessionData.get(APPLICATION_SESSION_CONFIGURATION_SETUP);
        if (configuration == null)
        {
            configuration = new ApplicationConfiguration();
        }
        return configuration;
    }

    public String completeStep()
    {
        doValidation();
        if (hasErrors())
        {
            return ERROR;
        }

        ApplicationConfiguration configuration = getApplicationConfigurationFromSession();

        configuration.setName(toLowerCase(name));
        configuration.setDescription(description);
        configuration.setPassword(password);
        configuration.setApplicationType(ApplicationType.valueOf(applicationType));

        sessionData.put(APPLICATION_SESSION_CONFIGURATION_SETUP, configuration);

        return "connectiondetails";

    }

    private void doValidation()
    {
        if (StringUtils.isBlank(name))
        {
            addFieldError("name", getText("application.name.invalid"));
        }
        else
        {
            try
            {
                applicationManager.findByName(toLowerCase(name));
                addFieldError("name", getText("invalid.namealreadyexist"));
            }
            catch (ApplicationNotFoundException e)
            {
                // Great we don't have a duplicate application
            }
        }

        if (StringUtils.isBlank(password))
        {
            addFieldError("password", getText("invalid.mustsupplyapassword"));
        }
        else
        {
            if (!StringUtils.equals(password, passwordConfirmation))
            {
                addFieldError("password", getText("invalid.passwordmismatch"));
            }
        }

        if (StringUtils.isBlank(applicationType))
        {
            addFieldError("applicationType", getText("application.type.invalid"));
        }
    }

    public String cancel()
    {
        sessionData.remove(APPLICATION_SESSION_CONFIGURATION_SETUP);

        return "cancel";
    }


    public String getApplicationType()
    {
        return applicationType;
    }

    public String getDescription()
    {
        return description;
    }

    public String getName()
    {
        return name;
    }

    public String getPassword()
    {
        return password;
    }

    public String getPasswordConfirmation()
    {
        return passwordConfirmation;
    }

    public void setApplicationType(String applicationType)
    {
        this.applicationType = applicationType;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setPasswordConfirmation(String passwordConfirmation)
    {
        this.passwordConfirmation = passwordConfirmation;
    }

    public void setSession(Map session)
    {
        this.sessionData = session;
    }

    public List<PairType> getApplicationTypes()
    {
        if (applicationTypes == null)
        {
            applicationTypes = new ArrayList<PairType>();

            for (Iterator<ApplicationType> iterator = ApplicationType.getCreatableAppTypes().iterator(); iterator.hasNext();)
            {
                ApplicationType type = iterator.next();
                applicationTypes.add(new PairType(type.name(), type.getDisplayName()));
            }
        }

        return applicationTypes;
    }
}
