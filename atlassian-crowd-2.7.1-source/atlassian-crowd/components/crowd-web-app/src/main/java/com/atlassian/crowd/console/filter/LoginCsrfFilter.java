package com.atlassian.crowd.console.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.xwork.XsrfTokenGenerator;
import com.atlassian.crowd.xwork.interceptors.XsrfTokenInterceptor;

import org.springframework.security.web.WebAttributes;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 * Specialized filter to prevent CSRF attacks against the login url. Other pages should be protected by
 * {@link com.atlassian.crowd.xwork.interceptors.XsrfTokenInterceptor}, which can't be used because because the login is
 * handled by {@link com.atlassian.crowd.integration.springsecurity.CrowdSSOAuthenticationProcessingFilter} and
 * interceptors are only applied after filters.
 */
public class LoginCsrfFilter extends OncePerRequestFilter
{
    private  XsrfTokenGenerator tokenGenerator;

    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException
    {
        String token = httpServletRequest.getParameter(XsrfTokenInterceptor.REQUEST_PARAM_NAME);
        boolean validToken = tokenGenerator.validateToken(httpServletRequest, token);

        if (!validToken)
        {
            if (token == null)
            {
                httpServletRequest.getSession().setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION,
                        new LoginCsrfTokenMissingException());
            }
            else
            {
                httpServletRequest.getSession().setAttribute(WebAttributes.AUTHENTICATION_EXCEPTION,
                        new LoginCsrfTokenInvalidException());
            }
            httpServletResponse.sendRedirect("login.action?error=true");
        }
        else
        {
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
    }

    public XsrfTokenGenerator getTokenGenerator()
    {
        return tokenGenerator;
    }

    public void setTokenGenerator(XsrfTokenGenerator tokenGenerator)
    {
        this.tokenGenerator = tokenGenerator;
    }

    public static class LoginCsrfTokenMissingException extends IllegalStateException
    {
    }

    public static class LoginCsrfTokenInvalidException extends IllegalStateException
    {
    }
}