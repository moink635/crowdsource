package com.atlassian.crowd.console.action.directory;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.atlassian.core.util.PairType;
import com.atlassian.crowd.console.value.directory.ConnectorConnection;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.GenericLDAP;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.SpringLDAPConnector;
import com.atlassian.crowd.directory.SynchronisableDirectoryProperties;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelper;
import com.atlassian.crowd.directory.ldap.validator.ConnectorValidator;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Ordering;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class CreateConnector extends CreateDirectory implements LDAPConfiguration
{
    private static final Logger logger = Logger.getLogger(CreateDirectory.class);

    // basic attributes
    private String URL;
    private String baseDN;
    private String ldapPassword;
    private String savedLdapPassword;
    private boolean secure;
    private String userDN;
    private String connector;
    private boolean cacheEnabled;
    protected boolean referral;
    protected boolean pagedResults;
    protected String userEncryptionMethod;
    protected boolean localUserStatusEnabled;
    protected boolean localGroups;
    protected boolean useNestedGroups;
    protected boolean primaryGroupSupport;
    protected boolean useUserMembershipAttribute;
    protected boolean useUserMembershipAttributeForGroupMembership; // for AD, at least initially
    boolean useRelaxedDNStandardisation;

    private final ConnectorConnection connection = new ConnectorConnection();

    private LDAPPropertiesHelper ldapPropertiesHelper;

    // ldap attributes
    String groupDNaddition;
    String groupDescriptionAttr;
    String groupMemberAttr;
    String groupNameAttr;
    String groupObjectClass;
    String groupObjectFilter;

    String userDNaddition;
    String userObjectClass;
    String userGroupMemberAttr;
    String userFirstnameAttr;
    String userLastnameAttr;
    String userDisplayNameAttr;
    String userMailAttr;
    String userNameAttr;
    String userNameRdnAttr;
    String userPasswordAttr;
    String userObjectFilter;
    String userExternalIdAttr;

    int pagedResultsSize = 0;

    // Spring injected
    private PasswordEncoderFactory passwordEncoderFactory;
    private ConnectorValidator connectorValidator;
    private DirectoryInstanceLoader directoryInstanceLoader;
    private LDAPConfigurationTester ldapConfigurationTester;

    public String doDefault()
    {
        super.doDefault();

        // Configure a default address for the ldap server.
        if (StringUtils.isBlank(URL))
        {
            URL = "ldap://localhost:389/";
        }

        if (pagedResultsSize == 0)
        {
            pagedResultsSize = SpringLDAPConnector.DEFAULT_PAGE_SIZE;
        }

        localUserStatusEnabled = false;
        cacheEnabled = true;
        connection.setIncrementalSyncEnabled(true);

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            // If update has occurred, it is no longer the initialLoad
            initialLoad = false;

            restoreSavedPassword();

            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                addActionError(getText("directoryconnector.update.invalid"));
                return INPUT;
            }

            Directory directory = buildDirectoryConfiguration();

            // check for further errors in the directory configuration against the validator stack
            validateAgainstValidatorStack(directory);
            if (hasErrors())
            {
                tab = 3;
                return INPUT;
            }

            // test our connection, as if it was at runtime to see if the connection can be established
            validateConnection(directory);
            if (hasErrors())
            {
                tab = 2;
                return INPUT;
            }

            directory = directoryManager.addDirectory(directory);
            ID = directory.getId();

            return SUCCESS;
        } catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    /**
     * Restore password from previous request(ex: Test connection) to the ldapPassword to ensure we don't save an empty password - refer CWD-1763
     */
    protected void restoreSavedPassword()
    {
        if (StringUtils.isBlank(ldapPassword))
        {
            ldapPassword = savedLdapPassword;
        }
    }

    private void validateAgainstValidatorStack(final Directory directory)
    {
        Set<String> directoryConfigurationErrors = connectorValidator.getErrors(directory);
        if (!directoryConfigurationErrors.isEmpty())
        {
            for (String error : directoryConfigurationErrors)
            {
                addActionError(error);
            }
        }
    }

    private void validateConnection(final Directory directory)
    {
        try
        {
            RemoteDirectory remoteDirectory = directoryInstanceLoader.getRawDirectory(directory.getId(), directory.getImplementationClass(), directory.getAttributes());
            remoteDirectory.testConnection();
        } catch (Throwable e)
        {
            tab = 2;
            logger.info(e.getMessage(), e);
            addActionError(e);
        }
    }

    public String doTestConfiguration()
    {
        try
        {
            // If a test is requested, it is no longer the initialLoad
            initialLoad = false;

            restoreSavedPassword();
            Directory directory = buildDirectoryConfiguration();

            RemoteDirectory remoteDirectory = directoryInstanceLoader.getRawDirectory(directory.getId(), directory.getImplementationClass(), directory.getAttributes());

            remoteDirectory.testConnection();

            actionMessageAlertColor = ALERT_BLUE;

            addActionMessage(getText("directoryconnector.testconnection.success"));
        } catch (Exception e)
        {
            addActionError(getText("directoryconnector.testconnection.invalid") + " " + e.getMessage());
        }

        return SUCCESS;
    }

    public String doTestPrincipalSearch()
    {
        return doTestSearch(this, LDAPConfigurationTester.Strategy.USER);
    }

    public String doTestGroupSearch()
    {
        return doTestSearch(this, LDAPConfigurationTester.Strategy.GROUP);
    }

    @VisibleForTesting
    final String doTestSearch(final LDAPConfiguration configuration, final LDAPConfigurationTester.Strategy strategy)
    {
        initialLoad = false;
        try
        {
            if (ldapConfigurationTester.canFindLdapObjects(configuration, strategy))
            {
                actionMessageAlertColor = ALERT_BLUE;
                addActionMessage(getText("directoryconnector.testsearch.success"));
            }
            else
            {
                addActionError(getText("directoryconnector.testsearch.invalid"));
            }
        }
        catch (final Exception exception)
        {
            addActionError(getText("directoryconnector.testsearch.invalid") + " " + exception.getMessage());
            logger.error(exception.getMessage(), exception);
        }
        return SUCCESS;
    }

    @VisibleForTesting
    public DirectoryImpl buildDirectoryConfiguration()
    {
        DirectoryImpl directory = new DirectoryImpl(name, DirectoryType.CONNECTOR, connector);

        directory.setActive(active);
        directory.setDescription(description);

        populateDirectoryAttributesForConnectionTest(directory.getAttributes());

        // set the permissions
        directory.setAllowedOperations(permissions);

        return directory;
    }

    protected void doValidation()
    {
        // the tabs are set from last to first so the user will correct the information on the input page
        // as the results come up.
        // reset the tab, for error results
        tab = null;

        // details tab 1
        validateDetails();

        // connection tab 2
        validateConnection();

        // configuration tab 3
        validateUserConfiguration();

        // configuration tab 3
        validateGroupConfiguration();
    }

    private void validateUserConfiguration()
    {
        if (StringUtils.isBlank(userFirstnameAttr))
        {
            addFieldError("userFirstnameAttr", getText("directoryconnector.userfirstnameattribute.invalid"));
        }

        if (StringUtils.isBlank(userGroupMemberAttr))
        {
            addFieldError("userGroupMemberAttr", getText("directoryconnector.usermemberofattribute.invalid"));
        }

        if (StringUtils.isBlank(userLastnameAttr))
        {
            addFieldError("userLastnameAttr", getText("directoryconnector.userlastnameattribute.invalid"));
        }

        if (StringUtils.isBlank(userDisplayNameAttr))
        {
            addFieldError("userDisplayNameAttr", getText("directoryconnector.userdisplaynameattribute.invalid"));
        }

        if (StringUtils.isBlank(userMailAttr))
        {
            addFieldError("userMailAttr", getText("directoryconnector.usermailattribute.invalid"));
        }

        if (StringUtils.isBlank(userNameAttr))
        {
            addFieldError("userNameAttr", getText("directoryconnector.usernameattribute.invalid"));
        }

        if (StringUtils.isBlank(userNameRdnAttr))
        {
            addFieldError("userNameRdnAttr", getText("directoryconnector.usernamerdnattribute.invalid"));
        }

        if (StringUtils.isBlank(userObjectFilter))
        {
            addFieldError("userObjectFilter", getText("directoryconnector.userobjectfilter.invalid"));
        }

        if (StringUtils.isBlank(userObjectClass))
        {
            addFieldError("userObjectClass", getText("directoryconnector.userobjectclass.invalid"));
        }

        validateUserPasswordAttr();

        if (hasErrors() && tab == null)
        {
            tab = 3;
        }
    }

    void validateUserPasswordAttr()
    {
        if (StringUtils.isBlank(userPasswordAttr))
        {
            addFieldError("userPasswordAttr", getText("directoryconnector.userpassword.invalid"));
        }
    }

    protected void validateGroupConfiguration()
    {
        if (StringUtils.isBlank(groupDescriptionAttr))
        {
            addFieldError("groupDescriptionAttr", getText("directoryconnector.groupdescription.invalid"));
        }

        if (StringUtils.isBlank(groupMemberAttr))
        {
            addFieldError("groupMemberAttr", getText("directoryconnector.groupmember.invalid"));
        }

        if (StringUtils.isBlank(groupNameAttr))
        {
            addFieldError("groupNameAttr", getText("directoryconnector.groupname.invalid"));
        }

        if (StringUtils.isBlank(groupObjectClass))
        {
            addFieldError("groupObjectClass", getText("directoryconnector.groupobjectclass.invalid"));
        }

        if (StringUtils.isBlank(groupObjectFilter))
        {
            addFieldError("groupObjectFilter", getText("directoryconnector.groupobjectfilter.invalid"));
        }

        if (hasErrors() && tab == null)
        {
            tab = 3;
        }
    }

    /**
     * Check for a missing mandatory DN. The base DN is mandatory for all
     * directories but {@link GenericLDAP}.
     *
     * @return <code>true</code> if the DN is empty and this implementation
     *  requires it to be non-empty.
     */
    static boolean missingRequiredDn(String implementationClass, String dn)
    {
        return StringUtils.isBlank(dn)
                && !GenericLDAP.class.getCanonicalName().equals(implementationClass);
    }

    static boolean validDn(String dn)
    {
        return StringUtils.isBlank(dn) || dn.contains("=");
    }

    protected void validateConnection()
    {
        if (StringUtils.isBlank(URL))
        {
            addFieldError("URL", getText("directoryconnector.url.invalid"));
        }

        if (missingRequiredDn(this.getImplementationClass(), baseDN))
        {
            addFieldError("baseDN", getText("directoryconnector.basedn.invalid.blank"));
        }
        else if (!validDn(baseDN))
        {
            addFieldError("baseDN", getText("directoryconnector.basedn.invalid"));
        }

        if (pagedResults && pagedResultsSize < 100)
        {
            addFieldError("pagedResultsSize", getText("directoryconnector.pagedresultscontrolsize.invalid"));
        }

        if (getPollingIntervalInMin() <= 0)
        {
            addFieldError("pollingInterval", getText("directoryconnector.polling.interval.invalid"));
        }

        if (localUserStatusEnabled && !cacheEnabled)
        {
            addFieldError("localUserStatusEnabled", getText("directoryconnector.localuserstatus.withoutcache.message"));
        }

        if (localGroups && !cacheEnabled)
        {
            addFieldError("localGroupsEnabled", getText("directoryconnector.localgroups.withoutcache.message"));
        }

        if (hasErrors() && tab == null)
        {
            tab = 2;
        }
    }

    protected void validateDetails()
    {
        if (StringUtils.isBlank(name))
        {
            addFieldError("name", getText("directoryinternal.name.invalid"));
        } else
        {
            try
            {
                directoryManager.findDirectoryByName(name);
                addFieldError("name", getText("invalid.namealreadyexist"));
            } catch (Exception e)
            {
                // ingore
            }
        }

        if (hasErrors() && tab == null)
        {
            tab = 1;
        }
    }

    public Map getConnectors()
    {
        return ldapPropertiesHelper.getImplementations();
    }

    public String getURL()
    {
        return URL;
    }

    public void setURL(String URL)
    {
        this.URL = URL;
    }

    public String getBaseDN()
    {
        return baseDN;
    }

    public void setBaseDN(String baseDN)
    {
        this.baseDN = baseDN;
    }

    public String getLdapPassword()
    {
        return ldapPassword;
    }

    public void setLdapPassword(String ldapPassword)
    {
        this.ldapPassword = ldapPassword;
    }

    public boolean isSecure()
    {
        return secure;
    }

    public void setSecure(boolean secure)
    {
        this.secure = secure;
    }

    public String getUserDN()
    {
        return userDN;
    }

    public void setUserDN(String userDN)
    {
        this.userDN = userDN;
    }

    public String getConnector()
    {
        return connector;
    }

    public void setConnector(String connector)
    {
        this.connector = connector;
    }

    public String getGroupDescriptionAttr()
    {
        return groupDescriptionAttr;
    }

    public void setGroupDescriptionAttr(String groupDescriptionAttr)
    {
        this.groupDescriptionAttr = groupDescriptionAttr;
    }

    public String getGroupMemberAttr()
    {
        return groupMemberAttr;
    }

    public void setGroupMemberAttr(String groupMemberAttr)
    {
        this.groupMemberAttr = groupMemberAttr;
    }

    public String getGroupNameAttr()
    {
        return groupNameAttr;
    }

    public void setGroupNameAttr(String groupNameAttr)
    {
        this.groupNameAttr = groupNameAttr;
    }

    public String getGroupObjectClass()
    {
        return groupObjectClass;
    }

    public void setGroupObjectClass(String groupObjectClass)
    {
        this.groupObjectClass = groupObjectClass;
    }

    public String getUserObjectClass()
    {
        return userObjectClass;
    }

    public void setUserObjectClass(String userObjectClass)
    {
        this.userObjectClass = userObjectClass;
    }

    public String getUserGroupMemberAttr()
    {
        return userGroupMemberAttr;
    }

    public void setUserGroupMemberAttr(String userGroupMemberAttr)
    {
        this.userGroupMemberAttr = userGroupMemberAttr;
    }

    public String getUserFirstnameAttr()
    {
        return userFirstnameAttr;
    }

    public void setUserFirstnameAttr(String userFirstnameAttr)
    {
        this.userFirstnameAttr = userFirstnameAttr;
    }

    public String getUserLastnameAttr()
    {
        return userLastnameAttr;
    }

    public void setUserLastnameAttr(String userLastnameAttr)
    {
        this.userLastnameAttr = userLastnameAttr;
    }

    public String getUserMailAttr()
    {
        return userMailAttr;
    }

    public void setUserMailAttr(String userMailAttr)
    {
        this.userMailAttr = userMailAttr;
    }

    public String getUserNameAttr()
    {
        return userNameAttr;
    }

    public void setUserNameAttr(String userNameAttr)
    {
        this.userNameAttr = userNameAttr;
    }

    public String getUserNameRdnAttr()
    {
        return userNameRdnAttr;
    }

    public void setUserNameRdnAttr(String userNameRdnAttr)
    {
        this.userNameRdnAttr = userNameRdnAttr;
    }

    public String getGroupDNaddition()
    {
        return groupDNaddition;
    }

    public void setGroupDNaddition(String groupDNaddition)
    {
        this.groupDNaddition = groupDNaddition;
    }

    public String getUserDNaddition()
    {
        return userDNaddition;
    }

    public void setUserDNaddition(String userDNaddition)
    {
        this.userDNaddition = userDNaddition;
    }

    public String getUserPasswordAttr()
    {
        return userPasswordAttr;
    }

    public void setUserPasswordAttr(String userPasswordAttr)
    {
        this.userPasswordAttr = userPasswordAttr;
    }

    public boolean isReferral()
    {
        return referral;
    }

    public void setReferral(boolean referral)
    {
        this.referral = referral;
    }

    public String getGroupObjectFilter()
    {
        return groupObjectFilter;
    }

    public void setGroupObjectFilter(String groupObjectFilter)
    {
        this.groupObjectFilter = groupObjectFilter;
    }

    public String getUserObjectFilter()
    {
        return userObjectFilter;
    }

    public void setUserObjectFilter(String userObjectFilter)
    {
        this.userObjectFilter = userObjectFilter;
    }

    @Override
    public String getUserExternalIdAttr()
    {
        return userExternalIdAttr;
    }

    public void setUserExternalIdAttr(String userExternalIdAttr)
    {
        this.userExternalIdAttr = userExternalIdAttr;
    }

    /**
     * Gets if paged results are going to used when pulling data from the LDAP server.
     *
     * @return <code>true</code> if and only if directory option to support paged results is enabled, otherwise <code>false</code>.
     */
    public boolean isPagedResults()
    {
        return pagedResults;
    }

    /**
     * Sets if paged results are going to be used when pulling data from the LDAP server.
     *
     * @param pagedResults <code>true</code> if and only if the entity is active, otherwise <code>false</code>.
     */
    public void setPagedResults(boolean pagedResults)
    {
        this.pagedResults = pagedResults;
    }

    public String getUserEncryptionMethod()
    {
        return userEncryptionMethod;
    }

    public void setUserEncryptionMethod(String userEncryptionMethod)
    {
        this.userEncryptionMethod = userEncryptionMethod;
    }

    public PasswordEncoderFactory getPasswordEncoderFactory()
    {
        return passwordEncoderFactory;
    }

    public void setPasswordEncoderFactory(PasswordEncoderFactory passwordEncoderFactory)
    {
        this.passwordEncoderFactory = passwordEncoderFactory;
    }

    public String getUserDisplayNameAttr()
    {
        return userDisplayNameAttr;
    }

    public void setUserDisplayNameAttr(String userDisplayNameAttr)
    {
        this.userDisplayNameAttr = userDisplayNameAttr;
    }

    public List<PairType> getUserEncryptionMethods()
    {
        List<PairType> encoders = new ArrayList<PairType>();
        encoders.add(new PairType("", getText("directoryconnector.userencryptionmethod.select")));
        for (String encoder : Ordering.<String>natural().immutableSortedCopy(this.passwordEncoderFactory.getSupportedLdapEncoders()))
        {
            encoders.add(new PairType(encoder, encoder.toUpperCase()));
        }
        return encoders;
    }

    /**
     * Gets the paging size to use when iterating LDAP results returned from a directory query.
     *
     * @return The paging size to use.
     */
    public int getPagedResultsSize()
    {
        return pagedResultsSize;
    }

    /**
     * Sets the paging size to use when iterating LDAP results returned from a directory query.
     *
     * @param pagedResultsSize The paging size to use.
     */
    public void setPagedResultsSize(int pagedResultsSize)
    {
        this.pagedResultsSize = pagedResultsSize;
    }

    public boolean isLocalUserStatusEnabled()
    {
        return localUserStatusEnabled;
    }

    public void setLocalUserStatusEnabled(boolean localUserStatusEnabled)
    {
        this.localUserStatusEnabled = localUserStatusEnabled;
    }

    public boolean isLocalGroupsEnabled()
    {
        return localGroups;
    }

    public void setLocalGroupsEnabled(boolean localGroups)
    {
        this.localGroups = localGroups;
    }

    public boolean isPrimaryGroupSupport()
    {
        return primaryGroupSupport;
    }

    public void setPrimaryGroupSupport(boolean primaryGroupSupport)
    {
        this.primaryGroupSupport = primaryGroupSupport;
    }

    public boolean isUseNestedGroups()
    {
        return useNestedGroups;
    }

    public void setUseNestedGroups(boolean useNestedGroups)
    {
        this.useNestedGroups = useNestedGroups;
    }

    public boolean isUseUserMembershipAttribute()
    {
        return useUserMembershipAttribute;
    }

    public void setUseUserMembershipAttribute(boolean useUserMembershipAttribute)
    {
        this.useUserMembershipAttribute = useUserMembershipAttribute;
    }

    public boolean isUseUserMembershipAttributeForGroupMembership()
    {
        return useUserMembershipAttributeForGroupMembership;
    }

    public void setUseUserMembershipAttributeForGroupMembership(boolean useUserMembershipAttributeForGroupMembership)
    {
        this.useUserMembershipAttributeForGroupMembership = useUserMembershipAttributeForGroupMembership;
    }

    public boolean isIncrementalSyncEnabled()
    {
        return connection.isIncrementalSyncEnabled();
    }

    public void setIncrementalSyncEnabled(boolean incrementalSyncEnabled)
    {
        connection.setIncrementalSyncEnabled(incrementalSyncEnabled);
    }

    public long getPollingIntervalInMin()
    {
        return connection.getPollingIntervalInMin();
    }

    public void setPollingIntervalInMin(long pollingIntervalInMin)
    {
        connection.setPollingIntervalInMin(pollingIntervalInMin);
    }

    public boolean isUseRelaxedDNStandardisation()
    {
        return useRelaxedDNStandardisation;
    }

    public void setUseRelaxedDNStandardisation(boolean useRelaxedDNStandardisation)
    {
        this.useRelaxedDNStandardisation = useRelaxedDNStandardisation;
    }

    public void setLdapPropertiesHelper(LDAPPropertiesHelper ldapPropertiesHelper)
    {
        this.ldapPropertiesHelper = ldapPropertiesHelper;
    }

    public void setConnectorValidator(ConnectorValidator connectorValidator)
    {
        this.connectorValidator = connectorValidator;
    }

    public void setDirectoryInstanceLoader(DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }

    protected DirectoryInstanceLoader getDirectoryInstanceLoader()
    {
        return directoryInstanceLoader;
    }

    public void setLdapConfigurationTester(LDAPConfigurationTester ldapConfigurationTester)
    {
        this.ldapConfigurationTester = ldapConfigurationTester;
    }

    public long getReadTimeoutInSec()
    {
        return connection.getReadTimeoutInSec();
    }

    public void setReadTimeoutInSec(long readTimeoutInSec)
    {
        connection.setReadTimeoutInSec(readTimeoutInSec);
    }

    public long getSearchTimeoutInSec()
    {
        return connection.getSearchTimeoutInSec();
    }

    public void setSearchTimeoutInSec(long searchTimeoutInSec)
    {
        connection.setSearchTimeoutInSec(searchTimeoutInSec);
    }

    public long getConnectionTimeoutInSec()
    {
        return connection.getConnectionTimeoutInSec();
    }

    public void setConnectionTimeoutInSec(long connectionTimeoutInSec)
    {
        connection.setConnectionTimeoutInSec(connectionTimeoutInSec);
    }

    public boolean isCacheEnabled()
    {
        return cacheEnabled;
    }

    public void setCacheEnabled(boolean cacheEnabled)
    {
        this.cacheEnabled = cacheEnabled;
    }

    public void setSavedLdapPassword(String ldapPassword)
    {
        this.savedLdapPassword = ldapPassword;
    }

    @Override
    public Long getId()
    {
        return null;
    }

    @Override
    public String getImplementationClass()
    {
        return connector;
    }

    @Override
    public void populateDirectoryAttributesForConnectionTest(final Map<String, String> attributes)
    {
        // set connection information
        attributes.put(LDAPPropertiesMapper.LDAP_URL_KEY, URL);
        attributes.put(LDAPPropertiesMapper.LDAP_SECURE_KEY, Boolean.toString(secure));
        attributes.put(LDAPPropertiesMapper.LDAP_REFERRAL_KEY, Boolean.toString(referral));

        // Only set the paged results size if the user has enabled paged results
        attributes.put(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY, Boolean.toString(pagedResults));
        if (pagedResults)
        {
            attributes.put(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE, Integer.toString(pagedResultsSize));
        }

        attributes.put(LDAPPropertiesMapper.LDAP_BASEDN_KEY, baseDN);
        attributes.put(LDAPPropertiesMapper.LDAP_USERDN_KEY, userDN);
        attributes.put(LDAPPropertiesMapper.LDAP_PASSWORD_KEY, ldapPassword);

        if (StringUtils.isNotBlank(userEncryptionMethod))
        {
            attributes.put(LDAPPropertiesMapper.LDAP_USER_ENCRYPTION_METHOD, userEncryptionMethod);
        }

        attributes.put(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS, Boolean.toString(localUserStatusEnabled));
        attributes.put(LDAPPropertiesMapper.LOCAL_GROUPS, Boolean.toString(localGroups));
        attributes.put(LDAPPropertiesMapper.PRIMARY_GROUP_SUPPORT, Boolean.toString(primaryGroupSupport));

        if (useNestedGroups)
        {
            attributes.put(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, Boolean.toString(Boolean.FALSE));
        } else
        {
            attributes.put(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, Boolean.toString(Boolean.TRUE));
        }

        attributes.put(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE, Boolean.toString(useUserMembershipAttribute));
        attributes.put(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE_FOR_GROUP_MEMBERSHIP, Boolean.toString(useUserMembershipAttributeForGroupMembership));

        // set connection pool properties - convert from seconds to milliseconds to store
        attributes.put(LDAPPropertiesMapper.LDAP_READ_TIMEOUT, Long.toString(TimeUnit.MILLISECONDS.convert(getReadTimeoutInSec(), TimeUnit.SECONDS)));
        attributes.put(LDAPPropertiesMapper.LDAP_SEARCH_TIMELIMIT, Long.toString(TimeUnit.MILLISECONDS.convert(getSearchTimeoutInSec(), TimeUnit.SECONDS)));
        attributes.put(LDAPPropertiesMapper.LDAP_CONNECTION_TIMEOUT, Long.toString(TimeUnit.MILLISECONDS.convert(getConnectionTimeoutInSec(), TimeUnit.SECONDS)));

        // set the ldap attributes
        attributes.put(LDAPPropertiesMapper.GROUP_DN_ADDITION, groupDNaddition);
        attributes.put(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY, groupDescriptionAttr);
        attributes.put(LDAPPropertiesMapper.GROUP_NAME_KEY, groupNameAttr);
        attributes.put(LDAPPropertiesMapper.GROUP_OBJECTCLASS_KEY, groupObjectClass);
        attributes.put(LDAPPropertiesMapper.GROUP_OBJECTFILTER_KEY, groupObjectFilter);
        attributes.put(LDAPPropertiesMapper.GROUP_USERNAMES_KEY, groupMemberAttr);

        attributes.put(LDAPPropertiesMapper.USER_DN_ADDITION, userDNaddition);
        attributes.put(LDAPPropertiesMapper.USER_EMAIL_KEY, userMailAttr);
        attributes.put(LDAPPropertiesMapper.USER_FIRSTNAME_KEY, userFirstnameAttr);
        attributes.put(LDAPPropertiesMapper.USER_GROUP_KEY, userGroupMemberAttr);
        attributes.put(LDAPPropertiesMapper.USER_LASTNAME_KEY, userLastnameAttr);
        attributes.put(LDAPPropertiesMapper.USER_DISPLAYNAME_KEY, userDisplayNameAttr);
        attributes.put(LDAPPropertiesMapper.USER_OBJECTCLASS_KEY, userObjectClass);
        attributes.put(LDAPPropertiesMapper.USER_OBJECTFILTER_KEY, userObjectFilter);
        attributes.put(LDAPPropertiesMapper.USER_USERNAME_KEY, userNameAttr);
        attributes.put(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY, userNameRdnAttr);
        attributes.put(LDAPPropertiesMapper.USER_PASSWORD_KEY, userPasswordAttr);
        attributes.put(LDAPPropertiesMapper.LDAP_EXTERNAL_ID, userExternalIdAttr);

        attributes.put(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED, Boolean.toString(isIncrementalSyncEnabled()));
        // convert pollingInterval to seconds to store in database
        attributes.put(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL, Long.toString(getPollingIntervalInMin() * 60));

        attributes.put(LDAPPropertiesMapper.LDAP_RELAXED_DN_STANDARDISATION, Boolean.toString(useRelaxedDNStandardisation));

        attributes.put(DirectoryProperties.CACHE_ENABLED, Boolean.toString(cacheEnabled));
    }
}
