package com.atlassian.crowd.console.action;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.DirectoryEntity;

import java.util.List;

public interface Searcher<T extends DirectoryEntity>
{
    /**
     * Performs a search for DirectoryEntities in the specified directory, returning a list of those that match.
     *
     * @param directoryID       The directory to search
     * @param active            Boolean.TRUE for only active, Boolean.FALSE for disabled, null for both.
     * @param searchText        The text to search for.
     * @param resultsStartIndex The index of the first result to return - used to avoid loading all results.
     * @param resultsPerPage    The number of results to return - used to avoid loading all results.
     * @return a List of users or an empty List
     * @throws OperationFailedException if the directory could not be accessed
     * @throws DirectoryNotFoundException if the directory could not be found
     */
    List<T> doSearchByDirectory(long directoryID, Boolean active, String searchText, int resultsStartIndex, int resultsPerPage) throws OperationFailedException, DirectoryNotFoundException;

    /**
     * Performs a search for DirectoryEntities in the specified directory, returning a list of those that match.
     *
     * @param applicationId     The application to search
     * @param active            Boolean.TRUE for only active, Boolean.FALSE for disabled, null for both.
     * @param searchText        The text to search for.
     * @param resultsStartIndex The index of the first result to return - used to avoid loading all results.
     * @param resultsPerPage    The number of results to return - used to avoid loading all results.
     * @return a List of users or an empty List
     * @throws DirectoryNotFoundException if the directory could not be found
     * @throws ApplicationNotFoundException of the application could not be found
     */
    List<T> doSearchByApplication(long applicationId, Boolean active, String searchText, int resultsStartIndex, int resultsPerPage)
            throws DirectoryNotFoundException, ApplicationNotFoundException;
}