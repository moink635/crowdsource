package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.OperationType;

import java.util.EnumSet;
import java.util.Set;

import static com.atlassian.crowd.embedded.api.OperationType.*;

public class CreateDirectory extends BaseAction
{
    protected long ID;
    protected boolean active;
    protected String name;
    protected String description;

    protected Set<OperationType> permissions = EnumSet.noneOf(OperationType.class);

    protected long tabID;

    public String doDefault()
    {
        // Enable all permissions except role permission by default
        permissions.addAll(EnumSet.allOf(OperationType.class));
        permissions.removeAll(EnumSet.of(CREATE_ROLE, UPDATE_ROLE, UPDATE_ROLE_ATTRIBUTE, DELETE_ROLE));

        active = true;

        return INPUT;
    }

    public long getID()
    {
        return ID;
    }

    public void setID(long ID)
    {
        this.ID = ID;
    }

    public boolean isActive()
    {
        return active;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public boolean isPermissionGroupAdd()
    {
        return permissions.contains(CREATE_GROUP);
    }

    public void setPermissionGroupAdd(boolean permissionGroupAdd)
    {
        setPermission(CREATE_GROUP, permissionGroupAdd);
    }

    public boolean isPermissionGroupModify()
    {
        return permissions.contains(UPDATE_GROUP);
    }

    public void setPermissionGroupModify(boolean permissionGroupModify)
    {
        setPermission(UPDATE_GROUP, permissionGroupModify);
    }

    public boolean isPermissionGroupAttributeModify()
    {
        return permissions.contains(UPDATE_GROUP_ATTRIBUTE);
    }

    public void setPermissionGroupAttributeModify(boolean permissionGroupAttributeModify)
    {
        setPermission(UPDATE_GROUP_ATTRIBUTE, permissionGroupAttributeModify);
    }

    public boolean isPermissionGroupRemove()
    {
        return permissions.contains(DELETE_GROUP);
    }

    public void setPermissionGroupRemove(boolean permissionGroupRemove)
    {
        setPermission(DELETE_GROUP, permissionGroupRemove);
    }

    public boolean isPermissionPrincipalAdd()
    {
        return permissions.contains(CREATE_USER);
    }

    public void setPermissionPrincipalAdd(boolean permissionPrincipalAdd)
    {
        setPermission(CREATE_USER, permissionPrincipalAdd);
    }

    public boolean isPermissionPrincipalModify()
    {
        return permissions.contains(UPDATE_USER);
    }

    public void setPermissionPrincipalModify(boolean permissionPrincipalModify)
    {
        setPermission(UPDATE_USER, permissionPrincipalModify);
    }

    public boolean isPermissionPrincipalAttributeModify()
    {
        return permissions.contains(UPDATE_USER_ATTRIBUTE);
    }

    public void setPermissionPrincipalAttributeModify(boolean permissionPrincipalAttributeModify)
    {
        setPermission(UPDATE_USER_ATTRIBUTE, permissionPrincipalAttributeModify);
    }

    public boolean isPermissionPrincipalRemove()
    {
        return permissions.contains(DELETE_USER);
    }

    public void setPermissionPrincipalRemove(boolean permissionPrincipalRemove)
    {
        setPermission(DELETE_USER, permissionPrincipalRemove);
    }

    public long getTabID()
    {
        return tabID;
    }

    public void setTabID(long tabID)
    {
        this.tabID = tabID;
    }

    private void setPermission(OperationType operation, boolean enabled)
    {
        if (enabled)
        {
            permissions.add(operation);
        }
        else
        {
            permissions.remove(operation);
        }
    }
}