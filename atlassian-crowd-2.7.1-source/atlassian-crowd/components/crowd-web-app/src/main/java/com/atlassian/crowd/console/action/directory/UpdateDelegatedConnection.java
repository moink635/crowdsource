package com.atlassian.crowd.console.action.directory;

import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelperImpl;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.directory.DirectoryImpl;

/**
 * Update Action specifically for the Delegated Directory type
 */
public class UpdateDelegatedConnection extends UpdateConnectorConnection
{
    private LDAPPropertiesHelperImpl ldapPropertiesHelperImpl;

    private boolean updateUsers;

    private boolean importGroups;

    public UpdateDelegatedConnection()
    {
        this.pollingEnabled = false;
    }

    @Override
    public String execute()
    {
        String result = super.execute();
        if (SUCCESS.equals(result))
        {
            setUpdateUsers(getDirectoryAttributeOrFalse(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH));
            setImportGroups(getDirectoryAttributeOrFalse(DelegatedAuthenticationDirectory.ATTRIBUTE_KEY_IMPORT_GROUPS));
        }

        return result;
    }

    private Boolean getDirectoryAttributeOrFalse(String key)
    {
        return Boolean.valueOf(getDirectory().getValue(key));
    }

    @Override
    public void updateDirectory(final DirectoryImpl updatedDirectory)
    {
        super.updateDirectory(updatedDirectory);
        updatedDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_KEY_IMPORT_GROUPS, String.valueOf(isImportGroups()));
        updatedDirectory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, String.valueOf(isUpdateUsers()));
    }

    public String getDelegatedDirectoryType()
    {
        String directoryType = getDirectory().getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS);

        Set<Map.Entry<String, String>> entries = ldapPropertiesHelperImpl.getImplementations().entrySet();
        for (Map.Entry<String, String> entry : entries)
        {
            if (directoryType.equals(entry.getValue()))
            {
                return entry.getKey();
            }
        }

        return directoryType;
    }

    @Override
    public String doTestUpdateConfiguration()
    {
        try
        {
            restoreSavedPassword();
            Map<String, String> testAttributes = buildTestConnectionAttributes();
            testAttributes.put(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS, getDirectory().getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS));

            directoryInstanceLoader.getRawDirectory(getDirectory().getId(), getDirectory().getImplementationClass(), testAttributes).testConnection();

            actionMessageAlertColor = ALERT_BLUE;

            addActionMessage(getText("directoryconnector.testconnection.success"));
            return SUCCESS;
        }
        catch (DirectoryInstantiationException e)
        {
            addActionError(getText("directoryconnector.testconnection.invalid") + " " + e.getMessage());
            logger.error("Failed to instantiate directory for connection test", e);
            return ERROR;
        }
        catch (OperationFailedException e)
        {
            addActionError(getText("directoryconnector.testconnection.invalid") + " " + e.getMessage());
            return ERROR;
        }
    }

    public boolean isUserEncryptionConfigurable()
    {
        for (Class<?> c : LDAPPropertiesHelperImpl.DIRECTORIES_WITH_CONFIGURABLE_USER_ENCRYPTION)
        {
            if (c.getCanonicalName().equals(getDirectory().getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS)))
            {
                return true;
            }
        }

        return false;
    }

    public void setLdapPropertiesHelper(LDAPPropertiesHelperImpl ldapPropertiesHelperImpl)
    {
        this.ldapPropertiesHelperImpl = ldapPropertiesHelperImpl;
    }

    public boolean isImportGroups()
    {
        return importGroups;
    }

    public void setImportGroups(boolean importGroups)
    {
        this.importGroups = importGroups;
    }

    public boolean isUpdateUsers()
    {
        return updateUsers;
    }

    public void setUpdateUsers(boolean updateUsers)
    {
        this.updateUsers = updateUsers;
    }
}
