package com.atlassian.crowd.console.action.directory;

import java.util.EnumSet;
import java.util.Set;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.manager.permission.PermissionManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import static com.atlassian.crowd.embedded.api.OperationType.CREATE_GROUP;
import static com.atlassian.crowd.embedded.api.OperationType.CREATE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.DELETE_GROUP;
import static com.atlassian.crowd.embedded.api.OperationType.DELETE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_GROUP;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_GROUP_ATTRIBUTE;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_USER_ATTRIBUTE;

/**
 * Action to handle the updating of Internal Directory Permissions
 */
public class UpdatePermissions extends BaseAction
{
    private final Set<OperationType> permissions = EnumSet.noneOf(OperationType.class);

    private Directory directory;

    private long id = -1;

    private PermissionManager permissionManager;

    public String execute() throws Exception
    {
        Directory currentDirectory = getDirectory();
        if (currentDirectory != null)
        {
            permissions.clear();
            for (OperationType operationType : OperationType.values())
            {
                if (permissionManager.hasPermission(currentDirectory, operationType))
                {
                    permissions.add(operationType);
                }
            }

            return SUCCESS;
        }
        else
        {
            return ERROR;
        }
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {

        try
        {
            // update the permissions
            final DirectoryImpl directoryToUpdate = new DirectoryImpl(getDirectory());
            directoryToUpdate.setAllowedOperations(permissions);

            directory = directoryManager.updateDirectory(directoryToUpdate);
        }
        catch (DirectoryNotFoundException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
        catch (RuntimeException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }

        return SUCCESS;
    }

    public boolean isPermissionGroupAdd()
    {
        return permissions.contains(CREATE_GROUP);
    }

    public void setPermissionGroupAdd(boolean permissionGroupAdd)
    {
        setPermission(CREATE_GROUP, permissionGroupAdd);
    }

    public boolean isPermissionGroupModify()
    {
        return permissions.contains(UPDATE_GROUP);
    }

    public void setPermissionGroupModify(boolean permissionGroupModify)
    {
        setPermission(UPDATE_GROUP, permissionGroupModify);
    }

    public boolean isPermissionGroupAttributeModify()
    {
        return permissions.contains(UPDATE_GROUP_ATTRIBUTE);
    }

    public void setPermissionGroupAttributeModify(boolean permissionGroupAttributeModify)
    {
        setPermission(UPDATE_GROUP_ATTRIBUTE, permissionGroupAttributeModify);
    }

    public boolean isPermissionGroupRemove()
    {
        return permissions.contains(DELETE_GROUP);
    }

    public void setPermissionGroupRemove(boolean permissionGroupRemove)
    {
        setPermission(DELETE_GROUP, permissionGroupRemove);
    }

    public boolean isPermissionPrincipalAdd()
    {
        return permissions.contains(CREATE_USER);
    }

    public void setPermissionPrincipalAdd(boolean permissionPrincipalAdd)
    {
        setPermission(CREATE_USER, permissionPrincipalAdd);
    }

    public boolean isPermissionPrincipalModify()
    {
        return permissions.contains(UPDATE_USER);
    }

    public void setPermissionPrincipalModify(boolean permissionPrincipalModify)
    {
        setPermission(UPDATE_USER, permissionPrincipalModify);
    }

    public boolean isPermissionPrincipalAttributeModify()
    {
        return permissions.contains(UPDATE_USER_ATTRIBUTE);
    }

    public void setPermissionPrincipalAttributeModify(boolean permissionPrincipalAttributeModify)
    {
        setPermission(UPDATE_USER_ATTRIBUTE, permissionPrincipalAttributeModify);
    }

    public boolean isPermissionPrincipalRemove()
    {
        return permissions.contains(DELETE_USER);
    }

    public void setPermissionPrincipalRemove(boolean permissionPrincipalRemove)
    {
        setPermission(DELETE_USER, permissionPrincipalRemove);
    }

    public Directory getDirectory()
    {
        if (directory == null && id != -1)
        {
            try
            {
                directory = directoryManager.findDirectoryById(id);
            }
            catch (DirectoryNotFoundException e)
            {
                addActionError(e);
            }
        }

        return directory;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public long getID()
    {
        return id;
    }

    public void setID(long ID)
    {
        this.id = ID;
    }

    private void setPermission(OperationType operation, boolean enabled)
    {
        if (enabled)
        {
            permissions.add(operation);
        }
        else
        {
            permissions.remove(operation);
        }
    }
}
