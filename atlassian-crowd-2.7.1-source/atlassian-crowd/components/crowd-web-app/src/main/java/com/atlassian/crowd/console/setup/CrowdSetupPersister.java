package com.atlassian.crowd.console.setup;

import com.atlassian.config.ApplicationConfiguration;
import com.atlassian.config.ConfigurationException;
import com.atlassian.config.setup.SetupException;
import com.atlassian.config.setup.SetupPersister;
import com.atlassian.core.util.PairType;
import com.atlassian.crowd.console.action.setup.Complete;
import com.atlassian.crowd.console.action.setup.Database;
import com.atlassian.crowd.console.action.setup.DefaultAdministrator;
import com.atlassian.crowd.console.action.setup.DirectoryInternal;
import com.atlassian.crowd.console.action.setup.Import;
import com.atlassian.crowd.console.action.setup.Installation;
import com.atlassian.crowd.console.action.setup.Integration;
import com.atlassian.crowd.console.action.setup.License;
import com.atlassian.crowd.console.action.setup.MailServer;
import com.atlassian.crowd.console.action.setup.Options;
import com.opensymphony.util.TextUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EmptyStackException;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public class CrowdSetupPersister implements SetupPersister
{
    private static final Logger log = Logger.getLogger(CrowdSetupPersister.class);

    // installation types (these are specific to Crowd, we do not use the
    // confluence ones that have been unfortunately defined in the interface)
    public static final String SETUP_TYPE_NEW_INSTALL = "install.new";
    public static final String SETUP_TYPE_UPGRADE_XML = "install.xml";

    private ApplicationConfiguration applicationConfig;

    // The stack is used to populate the UI as well as manage the flow
    private Stack<PairType> setupStack;
    private Stack<PairType> finishedStack;

    public CrowdSetupPersister()
    {
    }

    public void setApplicationConfig(ApplicationConfiguration applicationConfig)
    {
        this.applicationConfig = applicationConfig;
    }

    private Stack<PairType> getSetupStack()
    {
        if (setupStack == null)
        {
            configureSetupSequence(getSetupType());
        }

        return setupStack;
    }

    /**
     * Creates a default bootstrapManager sequence based on current state of crowd.config.xml
     */
    protected void configureSetupSequence(String setupType)
    {
        setupStack = new Stack<PairType>();
        finishedStack = new Stack<PairType>();

        // we don't have a variety of installation types, same setup sequence for all installs:
        if (SETUP_TYPE_INITIAL.equals(setupType))
        {
            setupStack.add(new PairType(Installation.INSTALL_TYPE_STEP, "step2.install"));
            setupStack.add(new PairType(License.LICENSE_STEP, "step1.license"));
        }
        if (SETUP_TYPE_NEW_INSTALL.equals(setupType))
        {
            setupStack.add(new PairType(SETUP_STATE_COMPLETE, "step10.complete"));
            setupStack.add(new PairType(Complete.COMPLETE_STEP, "step9.finish"));
            setupStack.add(new PairType(Integration.INTEGRATED_APPS_STEP, "step8.apps"));
            setupStack.add(new PairType(DefaultAdministrator.DEFAULT_ADMIN_STEP, "step7.admin"));
            setupStack.add(new PairType(DirectoryInternal.DIRECTORY_INTERNAL_STEP, "step6.directory"));
            setupStack.add(new PairType(MailServer.MAILSERVER_STEP, "step5.mail"));
            setupStack.add(new PairType(Options.OPTIONS_STEP, "step4.options"));
            setupStack.add(new PairType(Database.DATABASE_STEP, "step3.database"));
            setupStack.add(new PairType(Installation.INSTALL_TYPE_STEP, "step2.install"));
            setupStack.add(new PairType(License.LICENSE_STEP, "step1.license"));
        }
        else if (SETUP_TYPE_UPGRADE_XML.equals(setupType))
        {
            setupStack.add(new PairType(SETUP_STATE_COMPLETE, "step6.complete"));
            setupStack.add(new PairType(Complete.COMPLETE_STEP, "step5.finish"));
            setupStack.add(new PairType(Import.XML_IMPORT_STEP, "step4.import"));
            setupStack.add(new PairType(Database.DATABASE_STEP, "step3.database"));
            setupStack.add(new PairType(Installation.INSTALL_TYPE_STEP, "step2.install"));
            setupStack.add(new PairType(License.LICENSE_STEP, "step1.license"));
        }

        // this legacy 1.2.x database upgrade step is no longer supported in Crowd 2.0+ as you must export all Crowd 1.x data to move to 2.0+.

//        else if (SETUP_TYPE_UPGRADE_DB.equals(setupType))
//        {
//            setupStack.add(new PairType(SETUP_STATE_COMPLETE, "step6.complete"));
//            setupStack.add(new PairType(Complete.COMPLETE_STEP, "step5.finish"));
//            setupStack.add(new PairType(LegacyDatabaseCheck.LEGACY_CHECK_STEP, "step4.legacy"));
//            setupStack.add(new PairType(Database.DATABASE_STEP, "step3.database"));
//            setupStack.add(new PairType(Installation.INSTALL_TYPE_STEP, "step2.install"));
//            setupStack.add(new PairType(License.LICENSE_STEP, "step1.license"));
//        }

        String currentStep = getCurrentStep();
        synchSetupStackWithConfigRecord(currentStep);
    }

    /**
     * Matches a recorded action in crowd.cfg.xml, element setupStep, to what is in the setupStack.
     * Will then pop all previously performed steps.
     *
     * @param currentStep
     */
    private void synchSetupStackWithConfigRecord(String currentStep)
    {
        if (currentStep != null)
        {
            for (Iterator<PairType> iterator = getSetupStack().iterator(); iterator.hasNext();)
            {
                PairType pairType = (PairType) iterator.next();

                if (pairType.getKey().equals(currentStep))
                {
                    while (!((PairType) getSetupStack().peek()).getKey().equals(currentStep))
                    {
                        finishedStack.push(getSetupStack().pop());
                    }
                }
            }
        }
    }

    public List<PairType> getUncompletedSteps()
    {
        Iterator<PairType> iter = getSetupStack().iterator();
        ArrayList<PairType> reversedSteps = new ArrayList<PairType>();

        while (iter.hasNext())
        {
            reversedSteps.add(iter.next());
        }

        Collections.reverse(reversedSteps);

        return reversedSteps;
    }

    public List<PairType> getCompletedSteps()
    {
        return finishedStack.subList(0, finishedStack.size());
    }

    /**
     * @return the bootstrapManager type of the bootstrapManager process - initial, custom or install.
     */
    public String getSetupType()
    {
        return applicationConfig.getSetupType();
    }

    public void setSetupType(String setupType)
    {
        if (!TextUtils.stringSet(setupType))
        {
            setupType = SETUP_TYPE_INITIAL;
        }

        try
        {
            applicationConfig.setSetupType(setupType);
            applicationConfig.save();
            configureSetupSequence(setupType);
        }
        catch (ConfigurationException e)
        {
            log.error("Could not save setupType:" + e);
        }
    }

    /**
     * Ensures that the bootstrapManager is written to a complete state
     * by overriding all other remaining operations.
     */
    public void finishSetup() throws SetupException
    {
        String currentStep = getCurrentStep();

        if (!currentStep.equals(SETUP_STATE_COMPLETE))
        {
            log.warn("Tried to finish setup but had not run through the whole wizard?");
            log.warn("Current step was " + currentStep);
            applicationConfig.setCurrentSetupStep(SETUP_STATE_COMPLETE);
        }

        try
        {
            applicationConfig.setSetupComplete(true);
            applicationConfig.save();
        }
        catch (ConfigurationException e)
        {
            log.fatal("Error writing state to crowd.cfg.xml: " + e.getMessage());
            throw new SetupException(e);
        }
    }

    public void progessSetupStep()
    {
        try //we shouldn't need this try-catch, given the logic, but it's nice to have it here
        {
            PairType completedPair = (PairType) getSetupStack().pop();
            finishedStack.push(completedPair);

            String completedStep = (String) completedPair.getKey();

            if (!getSetupStack().empty()) //write the current bootstrapManager step to crowd.cfg.xml
            {
                String newCurrentStep = (String) ((PairType) getSetupStack().peek()).getKey();
                setCurrentStep(newCurrentStep);

            }
            else
            {
                log.error("setupStack is empty of actions.");
            }
        }
        catch (EmptyStackException e)
        {
            log.error("The setupStack is empty; the last action should always be '" + SETUP_STATE_COMPLETE + "', which will prohibit further setupStack activity! Odds are it wasn't in this case.");
        }
    }

    public String getCurrentStep()
    {
        String currentStep = applicationConfig.getCurrentSetupStep();

        if (!TextUtils.stringSet(currentStep))
        {
            currentStep = (String) ((PairType) getSetupStack().peek()).getKey();
            setCurrentStep(currentStep);
        }

        return currentStep;
    }

    private void setCurrentStep(String newCurrentStep)
    {
        applicationConfig.setCurrentSetupStep(newCurrentStep);

        try
        {
            applicationConfig.save();
        }
        catch (ConfigurationException e)
        {
            log.error("unable to record current bootstrapManager step to crowd.cfg.xml");
        }
    }

    public void setDemonstrationContentInstalled()
    {
        // meaningless
    }

    public boolean isDemonstrationContentInstalled()
    {
        return false;
    }

    public boolean isSetupTypeNewInstall()
    {
        return SETUP_TYPE_NEW_INSTALL.equals(getSetupType());
    }

    public boolean isSetupTypeXmlUpgrade()
    {
        return SETUP_TYPE_UPGRADE_XML.equals(getSetupType());
    }
}
