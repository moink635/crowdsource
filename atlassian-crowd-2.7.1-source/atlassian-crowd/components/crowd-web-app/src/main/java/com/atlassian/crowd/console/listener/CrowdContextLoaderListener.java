package com.atlassian.crowd.console.listener;

import com.atlassian.config.bootstrap.BootstrappedContextLoaderListener;
import com.atlassian.crowd.plugin.PluginUtils;

import javax.servlet.ServletContextEvent;

/**
 * Spring context loader with custom crowd configurarations
 */
public class CrowdContextLoaderListener extends BootstrappedContextLoaderListener
{
    /**
     * On context initialisation, change the {@link com.opensymphony.xwork.ObjectFactory} to the {@link com.opensymphony.webwork.spring.WebWorkSpringObjectFactory} one.
     * This can only be done after the spring bootstrapping has succeeded (since spring hasn't been setup)
     *
     * @param event
     */
    protected void postInitialiseContext(ServletContextEvent event)
    {
        // wire up webwork to use pluginized actions and results
        PluginUtils.initialiseWebworkForPluginSupport(event.getServletContext());
    }
}