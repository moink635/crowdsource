/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.options;

import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.support.SupportInformationService;
import com.atlassian.crowd.util.SystemInfoHelper;

import java.util.Date;

public class SystemInfo extends BaseAction
{
    private SystemInfoHelper systemInfoHelper;
    private SupportInformationService supportInformationService;
    private String supportInformation;

    public String execute() throws Exception
    {
        User user = applicationService.findUserByName(getCrowdApplication(), getRemoteUser().getName());
        supportInformation = supportInformationService.getSupportInformation(user);

        return SUCCESS;
    }

    public String getSupportInformation()
    {
        return supportInformation;
    }

    public String getDatabaseJdbcDriver()
    {
        return getSystemInfoHelper().getDatabaseJdbcDriver();
    }

    public String getDatabaseJdbcUsername()
    {
        return getSystemInfoHelper().getDatabaseJdbcUsername();
    }

    public String getDatabaseJdbcUrl()
    {
        return getSystemInfoHelper().getDatabaseJdbcUrl();
    }

    public String getDatabaseHibernateDialect()
    {
        return getSystemInfoHelper().getDatabaseHibernateDialect();
    }

    public String getDatabaseDatasourceJndiName()
    {
        return getSystemInfoHelper().getDatabaseDatasourceJndiName();
    }

    public boolean isDatabaseDatasource()
    {
        return getSystemInfoHelper().isDatabaseDatasource();
    }

    public String getTimeZone()
    {
        return getSystemInfoHelper().getTimeZone();
    }

    public String getJavaVersion()
    {
        return getSystemInfoHelper().getJavaVersion();
    }

    public String getJavaVendor()
    {
        return getSystemInfoHelper().getJavaVendor();
    }

    public String getJavaVMVersion()
    {
        return getSystemInfoHelper().getJavaVMVersion();
    }

    public String getJavaVMVendor()
    {
        return getSystemInfoHelper().getJavaVMVendor();
    }

    public String getJavaRuntime()
    {
        return getSystemInfoHelper().getJavaRuntime();
    }

    public String getApplicationUsername()
    {
        return getSystemInfoHelper().getApplicationUsername();
    }

    public String getArchitecture()
    {
        return getSystemInfoHelper().getArchitecture();
    }

    public String getOperatingSystem()
    {
        return getSystemInfoHelper().getOperatingSystem();
    }

    public String getFileEncoding()
    {
        return getSystemInfoHelper().getFileEncoding();
    }

    public String getFreeMemory()
    {
        return getSystemInfoHelper().getFreeMemory()+" MB";
    }

    public String getUsedMemory()
    {
        return getSystemInfoHelper().getUsedMemory()+" MB";
    }

    public String getTotalMemory()
    {
        return getSystemInfoHelper().getTotalMemory() + " MB";
    }

    public String getApplicationServer()
    {
        return getSystemInfoHelper().getApplicationServer();
    }

    public String getServerId()
    {
        return getSystemInfoHelper().getServerId();
    }

    public Date getSystemTime()
    {
        return new Date();
    }

    public String getCrowdHome()
    {
        return BootstrapUtils.getBootstrapManager().getApplicationHome();
    }

    public void setSystemInfoHelper(SystemInfoHelper systemInfoHelper)
    {
        this.systemInfoHelper = systemInfoHelper;
    }

    public SystemInfoHelper getSystemInfoHelper()
    {
        return systemInfoHelper;
    }

    public void setSupportInformationService(SupportInformationService supportInformationService)
    {
        this.supportInformationService = supportInformationService;
    }
}