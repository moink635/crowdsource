/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.principal;

import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.google.common.collect.Sets;
import com.opensymphony.webwork.interceptor.ServletRequestAware;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

public class UpdateAttributes extends ViewPrincipal implements ServletRequestAware
{
    private static final Logger logger = Logger.getLogger(UpdateAttributes.class);

    private String attributes[];
    private String attribute;
    private String value;
    private HttpServletRequest request;

    @RequireSecurityToken(true)
    public String doAddAttribute()
    {
        try
        {
            processGeneral();
            processDirectoryMapping();

            doValidationAddAttribute();
            if (hasErrors() || hasActionErrors())
            {
                return INPUT;
            }

            // add the value to the existing attribute if it already exist
            Set<String> attributeValues = userAttributes.get(attribute);
            if (attributeValues == null)
            {
                // create the attribute container if it doesn't exist
                attributeValues = new HashSet<String>();
                userAttributes.put(attribute, attributeValues);
            }
            attributeValues.add(value);


            directoryManager.storeUserAttributes(directoryID, user.getName(), userAttributes);

            // reset the values for the view
            attribute = "";
            value = "";

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    private void doValidationAddAttribute()
    {
        if (StringUtils.isBlank(attribute))
        {
            addFieldError("attribute", getText("principal.attributename.invalid"));
        }
    }

    @RequireSecurityToken(true)
    public String doRemoveAttribute()
    {
        try
        {

            processGeneral();
            processDirectoryMapping();

            userAttributes.remove(attribute);

            directoryManager.removeUserAttributes(directoryID, user.getName(), attribute);

            // reset the values for the view
            attribute = "";

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
            attribute = "";
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdateAttributes()
    {
        try
        {

            processGeneral();
            processDirectoryMapping();

            try
            {
                for (int i = 0; i < attributes.length; i++)
                {
                    String attribute = attributes[i];
                    String modifiedValues[] = getAttributeValues(attribute);

                    userAttributes.put(attribute, Sets.newHashSet(modifiedValues));
                }

                directoryManager.storeUserAttributes(directoryID, user.getName(), userAttributes);

                // still need to continue processing the rest of the principal even though the remove failed
            }
            catch (Exception e)
            {
                addActionError(e);
                logger.debug(e.getMessage(), e);
            }

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
        }

        return INPUT;
    }

    private String[] getAttributeValues(String attribute)
    {
        // find out how many values there are
        int parameterCount = getParameterCount(attribute);

        // construct the values
        String values[] = new String[parameterCount];
        for (int i = 0; i < parameterCount; i++)
        {
            values[i] = request.getParameter(attribute + (i + 1));
        }

        // our return ticket
        return values;
    }

    private int getParameterCount(String attribute)
    {
        int count = 0;

        logger.debug("searching attribute: " + attribute);

        Enumeration parameterNames = request.getParameterNames();

        while (parameterNames.hasMoreElements())
        {
            String parameterName = (String) parameterNames.nextElement();
            if (parameterName.matches("(" + attribute + "){1}\\d+"))
            {
                String number = parameterName.substring(attribute.length());

                // match the larger value
                int current = Integer.parseInt(number);
                if (current > count)
                {
                    count = current;
                }
            }
        }

        return count;
    }

    public String[] getAttributes()
    {
        return attributes;
    }

    public void setAttributes(String[] attributes)
    {
        this.attributes = attributes;
    }

    public String getAttribute()
    {
        return attribute;
    }

    public void setAttribute(String attribute)
    {
        this.attribute = attribute;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    @Override
    public void setServletRequest(HttpServletRequest request)
    {
        this.request = request;
    }
}