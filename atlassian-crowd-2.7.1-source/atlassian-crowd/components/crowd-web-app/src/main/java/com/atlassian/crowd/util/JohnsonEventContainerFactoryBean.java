package com.atlassian.crowd.util;

import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import org.springframework.beans.factory.config.AbstractFactoryBean;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

public class JohnsonEventContainerFactoryBean extends AbstractFactoryBean implements ServletContextAware
{
    private ServletContext servletContext;

    public Class getObjectType()
    {
        return JohnsonEventContainer.class;
    }

    protected Object createInstance() throws Exception
    {
        return Johnson.getEventContainer(servletContext);
    }

    public void setServletContext(ServletContext servletContext)
    {
        this.servletContext = servletContext;
    }
}
