package com.atlassian.crowd.console.action;

import com.atlassian.crowd.console.action.user.BaseUserAction;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.manager.login.ForgottenLoginManager;
import com.atlassian.crowd.manager.login.exception.InvalidResetPasswordTokenException;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.google.common.base.Objects;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ResetPassword extends BaseUserAction
{
    private static final Logger logger = LoggerFactory.getLogger(ResetPassword.class);

    private ForgottenLoginManager forgottenLoginManager;

    private String username;
    private long directoryId;
    private String token;
    private String password;
    private String confirmPassword;
    private String passwordComplexityMessage;

    private boolean invalidToken;

    @Override
    public String doDefault()
    {
        if (forgottenLoginManager.isValidResetToken(directoryId, username, token))
        {
            passwordComplexityMessage = getPasswordComplexityMessage(directoryId);
        }
        else
        {
            logger.info("Invalid reset password token for {}", username);
            setIsInvalidToken(true);
        }
        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        passwordComplexityMessage = getPasswordComplexityMessage(directoryId);
        doValidation();

        if (hasErrors())
        {
            return ERROR;
        }

        try
        {
            forgottenLoginManager.resetUserCredential(directoryId,
                                                      username,
                                                      PasswordCredential.unencrypted(password),
                                                      token);
            addActionMessage(ALERT_BLUE, getText("passwordupdate.message"));
            return SUCCESS;
        }
        catch (InvalidCredentialException e)
        {
            addActionError(getText("passwordupdate.policy.error.message"));
            addFieldError("password", Objects.firstNonNull(e.getPolicyDescription(), e.getMessage()));
        }
        catch (InvalidResetPasswordTokenException e)
        {
            logger.info("Unable to update credentials due to Invalid Token.", e);
            setIsInvalidToken(true);
        }
        catch (Exception e)
        {
            logger.info("Unable to update form", e);
            addActionError(e);
        }

        return INPUT;
    }

    private void doValidation()
    {
        if (StringUtils.isBlank(password))
        {
            addFieldError("password", getText("passwordempty.invalid"));
        }
        if (StringUtils.isBlank(confirmPassword))
        {
            addFieldError("confirmpassword", getText("passwordempty.invalid"));
        }
        else if (!StringUtils.equals(password, confirmPassword))
        {
            addFieldError("password", getText("passworddonotmatch.invalid"));
        }
    }

    public void setPassword(final String password)
    {
        this.password = password;
    }

    public void setConfirmPassword(final String confirmPassword)
    {
        this.confirmPassword = confirmPassword;
    }

    public String getPassword()
    {
        return password;
    }

    public String getConfirmPassword()
    {
        return confirmPassword;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(final String username)
    {
        this.username = username;
    }

    public long getDirectoryId()
    {
        return directoryId;
    }

    public void setDirectoryId(final long directoryId)
    {
        this.directoryId = directoryId;
    }

    public String getToken()
    {
        return token;
    }

    public void setToken(final String token)
    {
        this.token = token;
    }

    public void setForgottenLoginManager(ForgottenLoginManager forgottenLoginManager)
    {
        this.forgottenLoginManager = forgottenLoginManager;
    }

    public boolean getIsInvalidToken()
    {
        return invalidToken;
    }

    public void setIsInvalidToken(boolean invalidToken)
    {
        this.invalidToken = invalidToken;
    }

    public String getPasswordComplexityMessage()
    {
        return passwordComplexityMessage;
    }
}
