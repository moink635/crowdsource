/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.options;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.util.SystemInfoHelper;
import com.atlassian.crowd.xwork.RequireSecurityToken;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.Date;

public class UpdateLicensing extends BaseAction
{
    private static final Logger logger = Logger.getLogger(UpdateLicensing.class);

    private String key;

    protected CrowdLicense license;
    protected long currentResources;
    private SystemInfoHelper systemInfoHelper;
    private PropertyManager propertyManager;

    public String doDefault()
    {
        try
        {
            processLicense();
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
        }

        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {

        doValidation();

        if (hasErrors())
        {
            doDefault();
            return INPUT;            
        }

        license = crowdLicenseManager.storeLicense(key);
        if (license == null)
        {
            addFieldError("key", getText("license.key.invalid"));

            doDefault();

            return INPUT;
        }

        processLicense();

        ServletActionContext.getRequest().setAttribute("updateSuccessful","true");
        return SUCCESS;
    }

    public String doRecalculateUserLimit()
    {
        try
        {
            currentResources = crowdLicenseManager.getCurrentResourceUsageTotal();
            // Store the total consumed resources in the database
            propertyManager.setCurrentLicenseResourceTotal((int)currentResources);

            processLicense();

            ServletActionContext.getRequest().setAttribute("updateSuccessful","true");

        }
        catch (Exception e)
        {
            addActionError("An error occured calculating your user limit: " + e.getMessage());
            logger.error(e.getMessage(), e);
            return ERROR;
        }

        return SUCCESS;
    }

    protected void processLicense()
    {
        license = crowdLicenseManager.getLicense();
        currentResources = propertyManager.getCurrentLicenseResourceTotal();
    }

    private void doValidation()
    {
        if (StringUtils.isBlank(key))
        {
            addFieldError("key", getText("license.key.error.blank"));
        }

        if (!hasErrors() && !crowdLicenseManager.isSetupLicenseKeyValid(key))
        {
            addFieldError("key", getText("license.key.error.invalid"));
        }

        if (!hasErrors() && !crowdLicenseManager.isBuildWithinMaintenancePeriod(key))
        {
            addFieldError("key", getText("license.key.error.maintenance.expired"));
        }
    }

    public String getKey()
    {
        return key;
    }

    public void setKey(String key)
    {
        this.key = key;
    }

    public long getCurrentResources()
    {
        return currentResources;
    }

    public void setCurrentResources(long currentResources)
    {
        this.currentResources = currentResources;
    }

    public CrowdLicense getLicense()
    {
        return license;
    }

    public Date getLicenseExpiryDate()
    {
        return license.isEvaluation() ? license.getExpiryDate() : license.getMaintenanceExpiryDate();
    }

    public void setLicense(CrowdLicense license)
    {
        this.license = license;
    }

    public String getServerId()
    {
        return systemInfoHelper.getServerId();
    }

    public void setSystemInfoHelper(SystemInfoHelper systemInfoHelper)
    {
        this.systemInfoHelper = systemInfoHelper;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}