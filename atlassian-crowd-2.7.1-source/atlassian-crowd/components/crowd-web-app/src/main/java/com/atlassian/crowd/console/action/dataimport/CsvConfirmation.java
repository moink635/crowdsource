package com.atlassian.crowd.console.action.dataimport;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.importer.config.CsvConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.manager.ImporterManager;
import com.opensymphony.webwork.interceptor.SessionAware;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;

/**
 * Action class to handle the confimation of the CSV mappings
 */
public class CsvConfirmation extends BaseImporter implements SessionAware
{
    private ImporterManager importerManager;

    private Map session;

    private CsvConfiguration configuration;

    public String doDefault() throws Exception
    {
        configuration = (CsvConfiguration) session.get(BaseImporter.IMPORTER_CONFIGURATION);
        return INPUT;
    }

    public String doExecute()
    {
        // If we are happy with the configuration, get the importer and process the result
        if (getConfiguration() != null)
        {
            try
            {
                result = importerManager.performImport(getConfiguration());
            }
            catch (ImporterException e)
            {
                logger.error(e.getMessage(), e);
                addActionError("An error occured performing import: " + e.getMessage());
                return ERROR;
            }
        }

        // now remove the CSV Configuration from the session
        session.remove(IMPORTER_CONFIGURATION);

        return SUCCESS;
    }

    public void setSession(Map session)
    {
        this.session = session;
    }

    public CsvConfiguration getConfiguration()
    {
        if (configuration == null)
        {
            configuration = (CsvConfiguration) session.get(BaseImporter.IMPORTER_CONFIGURATION);
        }
        return configuration;
    }

    public String getHeaderRowValue(String indexName, boolean groupheader)
    {
        String headerRowValue = null;
        if (StringUtils.isNotBlank(indexName))
        {
            List headerRow = null;
            if (Boolean.valueOf(groupheader).booleanValue())
            {
                headerRow = getConfiguration().getGroupHeaderRow();
            }
            else
            {
                headerRow = getConfiguration().getUserHeaderRow();
            }

            Integer index = new Integer(StringUtils.substringAfter(indexName, "."));
            headerRowValue = (String) headerRow.get(index.intValue());
        }

        return headerRowValue;
    }

    public String getDirectoryName() throws OperationFailedException, DirectoryNotFoundException
    {
        String directoryName = "";
        if (getConfiguration() != null && getConfiguration().getDirectoryID() != null)
        {
            Directory directory = directoryManager.findDirectoryById(getConfiguration().getDirectoryID().longValue());
            directoryName = directory.getName();

        }
        return directoryName;
    }

    public String getEncryptingPasswords()
    {
        String encryptingPasswords = null;

        // Are we actually mapping passwords?
        if (getConfiguration() != null && getConfiguration().getUserMappingConfiguration().containsValue(CsvConfiguration.USER_PASSWORD))
        {
            if (getConfiguration().isEncryptPasswords().booleanValue())
            {
                encryptingPasswords = getText("no.label");
            }
            else
            {
                encryptingPasswords = getText("yes.label");
            }
        }

        return encryptingPasswords;
    }

    public void setImporterManager(ImporterManager importerManager)
    {
        this.importerManager = importerManager;
    }
}
