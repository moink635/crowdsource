package com.atlassian.crowd.util;

import com.atlassian.crowd.embedded.api.Directory;
import com.opensymphony.webwork.ServletActionContext;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import javax.servlet.http.Cookie;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class SelectionUtils
{
    private final static String SELECTED_DIRECTORY_COOKIE_NAME = "crowd.directory.selected";

    private SelectionUtils()
    {
        // Not to be instantiated.
    }

    public static long getSelectedDirectory(List<Directory> directories)
    {
        long directoryId = -1;
        if (directories.size() == 1)
        {
            directoryId = directories.get(0).getId();
        }
        else
        {
            final Long savedDirectoryId = NumberUtils.createLong(getCookieValue(SELECTED_DIRECTORY_COOKIE_NAME));
            for (Directory directory : directories)
            {
                if (directory.getId().equals(savedDirectoryId))
                {
                    directoryId = savedDirectoryId;
                }
            }
        }
        return directoryId;
    }

    public static void saveSelectedDirectory(long directoryId)
    {
        final Cookie cookie = new Cookie(SELECTED_DIRECTORY_COOKIE_NAME, Long.toString(directoryId));
        final String contextPath = ServletActionContext.getRequest().getContextPath();
        cookie.setPath(StringUtils.isNotEmpty(contextPath) ? contextPath : "/");
        cookie.setMaxAge(Long.valueOf(TimeUnit.DAYS.toSeconds(365)).intValue()); // One year
        ServletActionContext.getResponse().addCookie(cookie);
    }

    private static String getCookieValue(String name)
    {
        final Cookie[] cookies = ServletActionContext.getRequest().getCookies();
        for (Cookie cookie : cookies)
        {
            if (name.equals(cookie.getName()))
            {
                return cookie.getValue();
            }
        }

        return null;
    }
}
