package com.atlassian.crowd.util;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.plugin.servlet.ResourceDownloadUtils;

import com.google.common.base.Predicates;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;

import org.tuckey.web.filters.urlrewrite.extend.RewriteMatch;
import org.tuckey.web.filters.urlrewrite.extend.RewriteRule;

public class ResourceDownloadRewriteRule extends RewriteRule
{
    private static final Pattern DOWNLOAD_URL = Pattern.compile("^/s/(.*)/_/download/([^\\?]*).*");

    @Override
    public RewriteMatch matches(HttpServletRequest request, HttpServletResponse response)
    {
        final String url = request.getRequestURI();

        String context = request.getContextPath();
        String contextUrl = url.substring(context.length());

        Matcher m = DOWNLOAD_URL.matcher(contextUrl);

        if (!m.matches())
        {
            return null;
        }

        String path = m.group(2);

        if (Iterables.any(Splitter.on('/').split(path), Predicates.equalTo("..")))
        {
            return null;
        }

        final String rewrittenContextUrl = "/download/" + path;

        final String rewrittenUrl = context + rewrittenContextUrl;

        return new RewriteMatch() {
            @Override
            public String getMatchingUrl()
            {
                return rewrittenUrl;
            }

            @Override
            public boolean execute(HttpServletRequest request, HttpServletResponse response) throws ServletException,
                    IOException
            {
                ResourceDownloadUtils.addPublicCachingHeaders(request, response);
                request.getRequestDispatcher(rewrittenContextUrl).forward(request, response);

                return true;
            }
        };
    }
}
