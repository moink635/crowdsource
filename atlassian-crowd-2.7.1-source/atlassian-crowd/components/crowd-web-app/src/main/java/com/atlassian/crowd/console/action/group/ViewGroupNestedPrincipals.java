package com.atlassian.crowd.console.action.group;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.EntityComparator;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class ViewGroupNestedPrincipals extends BaseAction
{
    private static final Logger logger = Logger.getLogger(ViewGroupMembers.class);

    protected DirectoryManager directoryManager;
    private Long directoryID;
    private String groupName;
    private Collection<User> principals;

    public String execute()
    {
        if (StringUtils.isNotBlank(groupName) && directoryID != null)
        {
            try
            {
                Directory directory = directoryManager.findDirectoryById(directoryID);

                final List<User> principalsList = new ArrayList(directoryManager.searchNestedGroupRelationships(directoryID, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(groupName).returningAtMost(EntityQuery.ALL_RESULTS)));
                Collections.sort(principalsList, EntityComparator.of(User.class));
                principals = principalsList; // sort the list

            }
            catch (Exception e)
            {
                logger.error(e.getMessage(), e);
                addActionError(e);
                return ERROR;
            }
        }

        return SUCCESS;
    }

    ///CLOVER:OFF
    public Long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(Long directoryID)
    {
        this.directoryID = directoryID;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public Collection<User> getPrincipals()
    {
        return principals;
    }

    public void setPrincipals(Collection<User> principals)
    {
        this.principals = principals;
    }

    public DirectoryManager getDirectoryManager()
    {
        return directoryManager;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }
}
