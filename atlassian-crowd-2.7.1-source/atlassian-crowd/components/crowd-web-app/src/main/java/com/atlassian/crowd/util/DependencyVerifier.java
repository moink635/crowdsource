package com.atlassian.crowd.util;

import com.atlassian.core.util.ClassLoaderUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class DependencyVerifier
{
    private static final Logger logger = Logger.getLogger(DependencyVerifier.class);

    private final List<Dependency> dependecies;

    public DependencyVerifier(String dependencyXmlFile) throws DocumentException
    {
        dependecies = new ArrayList<Dependency>();

        Document xmlDoc = getDocument(dependencyXmlFile);
        Element root = xmlDoc.getRootElement();

        List<Element> depElts = root.elements("dependency");
        for(Element depElt : depElts)
        {
            String jar = depElt.element("jar").getText();
            String clazz = depElt.element("class").getText();
            dependecies.add(new Dependency(jar, clazz));
        }
    }

    public void verifyDependencies() throws ClassNotFoundException
    {
        for (Dependency dependency : dependecies)
        {
            if (!dependency.isPresentOnClasspath())
            {
                throw new ClassNotFoundException("Class " + dependency.getClazz() + " was not found on class path. Install "
                        + dependency.getJar() + " into the common library folder of the application server.");
            }
        }
    }

    private Document getDocument(String fileName) throws DocumentException
    {
        InputStream is = ClassLoaderUtils.getResourceAsStream(fileName, this.getClass());

        SAXReader saxReader = new SAXReader();
        Document xmlDoc = saxReader.read(is);

        try
        {
            is.close();
        }
        catch (Exception e)
        {
            logger.error(e, e);
        }
        return xmlDoc;
    }

    private class Dependency
    {
        private final String jar;
        private final String clazz;

        public Dependency(String jar, String clazz)
        {
            this.jar = jar;
            this.clazz = clazz;
        }

        public String getJar()
        {
            return jar;
        }

        public String getClazz()
        {
            return clazz;
        }

        public boolean isPresentOnClasspath()
        {
            try
            {
                ClassLoaderUtils.loadClass(clazz, this.getClass());
                return true;
            }
            catch (ClassNotFoundException e)
            {
                return false;
            }
        }
    }
}
