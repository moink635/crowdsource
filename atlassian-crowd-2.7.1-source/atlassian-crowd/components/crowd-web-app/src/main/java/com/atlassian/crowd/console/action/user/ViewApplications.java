package com.atlassian.crowd.console.action.user;

import com.atlassian.crowd.manager.application.AliasManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.user.User;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ViewApplications extends BaseUserAction
{
    private AliasManager aliasManager;

    private List<Application> applications;
    private List<String> aliases;

    public String doDefault() throws Exception
    {
        try
        {
            final User user = applicationService.findUserByName(getCrowdApplication(), getRemoteUsername());
            applications = tokenAuthenticationManager.findAuthorisedApplications(user, clientProperties.getApplicationName());

            aliases = new ArrayList<String>();
            for (Application application : applications)
            {
                String alias = aliasManager.findAliasByUsername(application, user.getName());
                if (StringUtils.isBlank(alias) || alias.equals(user.getName()))
                {
                    alias = "";
                }
                aliases.add(alias);
            }
        }
        catch (Exception e)
        {
            addActionError(getText("user.console.applications.error"));
            logger.error("Failed to get applications", e);
            applications = Collections.emptyList();
        }
        return SUCCESS;
    }

    public List getApplications()
    {
        return applications;
    }

    public List<String> getAliases()
    {
        return aliases;
    }

    public void setAliasManager(final AliasManager aliasManager)
    {
        this.aliasManager = aliasManager;
    }
}
