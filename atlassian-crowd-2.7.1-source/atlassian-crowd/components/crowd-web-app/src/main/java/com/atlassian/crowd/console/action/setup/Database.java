package com.atlassian.crowd.console.action.setup;

import com.atlassian.config.ConfigurationException;
import com.atlassian.config.db.DatabaseDetails;
import com.atlassian.config.db.DatabaseList;
import com.atlassian.core.util.PairType;
import com.atlassian.crowd.plugin.RequiredPluginsStartupCheck;
import com.atlassian.spring.container.ContainerManager;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.google.common.collect.ImmutableList;

/**
 * Select the type of database to configure Crowd with:
 * <ol>
 *   <li>Embedded (HSQLDB)</li>
 *   <li>External JDBC (C3P0 pooled)</li>
 *   <li>External Datasource (externally pooled)</li>
 * </ol>
 *
 * Once a database type is correctly selected, this
 * action will configure the database with the appropriate
 * schema.
 */

public class Database extends BaseSetupAction
{
    private static final Logger logger = Logger.getLogger(Database.class);

    public static final String DATABASE_STEP = "setupdatabase";

    private static final int MAX_ERROR_MESSAGE_LENGTH = 300;

    static final String EMBEDDED   = "db.embedded";
    static final String JDBC_DB    = "db.jdbc";
    static final String DATASOURCE = "db.datasource";

    // from atlassian config (cf. supportedDatabases.properties)
    private DatabaseList databases = new DatabaseList();

    private String databaseOption;

    private String jdbcDatabaseType;
    private String jdbcDriverClassName;
    private String jdbcUrl;
    private String jdbcUsername;
    private String jdbcPassword;
    private String jdbcHibernateDialect;
    private boolean jdbcOverwriteData;

    private String datasourceDatabaseType;
    private String datasourceJndiName;
    private String datasourceHibernateDialect;
    private boolean datasourceOverwriteData;

    private DatabaseDetails databaseDetails;
    private RequiredPluginsStartupCheck requiredPluginsStartupCheck;

    public String doDefault()
    {
        // check if i'm at the correct step
        String setupDefault = super.doDefault();
        if (!setupDefault.equals(SUCCESS))
        {
            return setupDefault;
        }

        // configure default option
        if (isUpgradeFromExistingDatabase())
        {
            databaseOption = JDBC_DB;
        }
        else
        {
            databaseOption = EMBEDDED;
        }

        return INPUT;
    }
    
    public String doUpdate()
    {
        // check if i'm at the correct step
        String setupUpdate = super.doUpdate();
        if (!setupUpdate.equals(SUCCESS))
        {
            return setupUpdate;
        }

        if (JDBC_DB.equals(databaseOption))
		{
            validateJdbc();
            if (!hasErrors())
            {
                configureJdbc();
            }
        }
		else if (DATASOURCE.equals(databaseOption))
		{
			validateDatasource();
            if (!hasErrors())
            {
                configureDatasource();
            }
        }
		else
		{
			validateEmbedded();
            if (!hasErrors())
            {
                configureEmbedded();
            }
		}

        if (hasErrors())
        {
            return ERROR;
        }

        // plugin system is initialised after context is restarted, so plugins should be available at this point
        Collection<String> missingRequiredPlugins = getRequiredPluginsStartupCheck().requiredPluginsNotEnabled();

        if (!missingRequiredPlugins.isEmpty())
        {
            logger.fatal("Setup cannot continue because some required plugins are not available: "
                         + missingRequiredPlugins);
            addActionError(getText("error.required.plugins.missing", ImmutableList.of(missingRequiredPlugins)) + " "
                + getText("setupcomplete.error.cannot.continue"));
            return ERROR;
        }
        else
        {
            // progress setup step if there are no errors
            getSetupPersister().progessSetupStep();
            return SELECT_SETUP_STEP;
        }       
    }

    public String getStepName()
    {
        return DATABASE_STEP;
    }

    // convenience method to ensure error messages on the UI aren't massively long (eg. wrapped jdbc driver exceptions)
    private String getShortMessage(Exception e)
    {
        if (e.getMessage() == null)
        {
            return "";
        }
        else if (e.getMessage().length() <= MAX_ERROR_MESSAGE_LENGTH)
        {
            return e.getMessage();
        }
        else
        {
            StringBuffer sb = new StringBuffer(e.getMessage().substring(0, MAX_ERROR_MESSAGE_LENGTH));
            sb.append("... ").append(getText("error.more.info"));
            return sb.toString();
        }
    }

    private void validateEmbedded()
    {
        databaseOption = "hsqldb";
        try
        {
            databaseDetails = DatabaseDetails.getDefaults(databaseOption);
            databaseDetails.setupForDatabase(databaseOption);
            if(!databaseDetails.checkDriver())
            {
                addActionError(getText("database.embedded.driver.notfound"));
            }
        }
        catch (ConfigurationException e)
        {
            addActionError(getText("database.select.invalid"));
        }
    }

    private void configureEmbedded()
    {
        try
        {
            getBootstrapManager().bootstrapDatabase(databaseDetails, true);
        }
        catch (Exception e)
        {
            addActionError("Database configuration failed: " + getShortMessage(e));
            logger.error("Database configuration failed. " + getShortMessage(e), e);
        }
    }
    
    private void validateJdbc()
    {
    	// field-level validation

        if (StringUtils.isBlank(jdbcDatabaseType))
    	{
    		addFieldError("jdbcDatabaseType", getText("database.select.blank"));
            // Return since we haven't chosen a database
            return;
        }
        else
        {
            try
            {
                databaseDetails = DatabaseDetails.getDefaults(jdbcDatabaseType);
                databaseDetails.setupForDatabase(jdbcDatabaseType);
            }
            catch (ConfigurationException e)
            {
                addFieldError("jdbcDatabaseType", getText("database.select.invalid"));
            }
        }
        if (StringUtils.isBlank(jdbcDriverClassName))
    	{
    		addFieldError("jdbcDriverClassName", getText("database.driver.blank"));    		
    	}
        else
        {
            try
            {
                Class.forName(jdbcDriverClassName);
            }
            catch (ClassNotFoundException e)
            {
                addFieldError("jdbcDriverClassName", getText("database.driver.notfound"));
            }
        }
        if (StringUtils.isBlank(jdbcUrl))
    	{
    		addFieldError("jdbcUrl", getText("database.jdbc.url.blank"));    		
    	}
    	if (StringUtils.isBlank(jdbcUsername))
    	{
    		addFieldError("jdbcUsername", getText("database.username.blank"));    		
    	}
    	if (StringUtils.isBlank(jdbcHibernateDialect))
    	{
    		addFieldError("jdbcHibernateDialect", getText("database.dialect.blank"));
        }
        else
        {
            try
            {
                Class.forName(jdbcHibernateDialect);
            }
            catch (ClassNotFoundException e)
            {
                addFieldError("jdbcHibernateDialect", getText("database.dialect.notfound"));
            }
        }
    	
        // connection validation (only done if field validation ok)

        if (!hasErrors())
        {
            databaseDetails.setDriverClassName(jdbcDriverClassName);
            databaseDetails.setDatabaseUrl(jdbcUrl);
            databaseDetails.setUserName(jdbcUsername);
            databaseDetails.setPassword(jdbcPassword);
            databaseDetails.setDialect(jdbcHibernateDialect);

            if (!databaseDetails.checkDriver())
            {
                addFieldError("jdbcDriverClassName", getText("database.driver.notfound"));
            }
            else
            {
                // test database connections and check if there is existing data in there
                Connection connection = null;
                try
                {
                    connection = getBootstrapManager().getTestDatabaseConnection(databaseDetails);

                    // don't let people create a new schema unless they explicitly click "overwrite"
                    if (!isUpgradeFromExistingDatabase() && !jdbcOverwriteData && getBootstrapManager().databaseContainsExistingData(connection))
                    {
                        addActionError(getText("database.overwrite.dataexists"));
                    }
                }
                catch (Exception e)
                {
                    addActionError("Could not connect to the database: " + getShortMessage(e));
                    logger.error("Unable to connect to database: " + e.getMessage(), e);
                }
                finally
                {
                    try
                    {
                        if (connection != null)
                        {
                            connection.close();
                        }
                    }
                    catch (SQLException e)
                    {
                        // ignore exception in finally block
                    }
                }
            }
        }
    }

    private void configureJdbc()
    {
        try
        {
            getBootstrapManager().bootstrapDatabase(databaseDetails, false);
        }
        catch (Exception e)
        {
            addActionError("Database configuration failed: " + getShortMessage(e));
            logger.error("Database configuration failed. " + e.getMessage(), e);
        }
    }

    private void validateDatasource()
    {
        // field-level validation

        if (StringUtils.isBlank(datasourceDatabaseType))
    	{
    		addFieldError("datasourceDatabaseType", getText("database.select.blank"));
            // return since we haven't chosen a database yet
            return;
        }
        else
        {
            try
            {
                databaseDetails = DatabaseDetails.getDefaults(datasourceDatabaseType);
                databaseDetails.setupForDatabase(datasourceDatabaseType);
            }
            catch (ConfigurationException e)
            {
                addFieldError("datasourceDatabaseType", getText("database.select.invalid"));
            }
        }

        if (StringUtils.isBlank(datasourceJndiName))
    	{
    		addFieldError("datasourceJndiName", getText("database.jndi.blank"));
    	}
        if (StringUtils.isBlank(datasourceHibernateDialect))
    	{
    		addFieldError("datasourceHibernateDialect", getText("database.dialect.blank"));
        }
        else
        {
            try
            {
                Class.forName(datasourceHibernateDialect);
            }
            catch (ClassNotFoundException e)
            {
                addFieldError("datasourceHibernateDialect", getText("database.dialect.notfound"));
            }
        }

        // connection validation (only done if field validation ok)

        if (!hasErrors())
        {
            databaseDetails.setDatabaseUrl(datasourceJndiName);
            databaseDetails.setDialect(datasourceHibernateDialect);

            // test connection and test for existing data
            Connection connection = null;
            try
            {
                connection = getBootstrapManager().getTestDatasourceConnection(datasourceJndiName);

                // don't let people create a new schema unless they explicitly click "overwrite"
                if (!isUpgradeFromExistingDatabase() && !datasourceOverwriteData && getBootstrapManager().databaseContainsExistingData(connection))
                {
                    addActionError(getText("database.overwrite.dataexists"));
                }
            }
            catch (Exception e)
            {
                addActionError("Error accessing datasource: " + getShortMessage(e));
                logger.warn("Unable to look up datasource: " + e.getMessage(), e);
            }
            finally
            {
                try
                {
                    if (connection != null)
                    {
                        connection.close();
                    }
                }
                catch (SQLException e)
                {
                    // ignore exception in finally
                }
            }
        }
    }

    private void configureDatasource()
    {
        try
        {
            getBootstrapManager().bootstrapDatasource(datasourceJndiName, databaseDetails.getDialect());
        }
        catch (Exception e)
        {
            addActionError("Database configuration failed: " + getShortMessage(e));
            logger.error("Unable to bootstrap datasource: " + e.getMessage(), e);
        }
    }
    
    public DatabaseDetails getDatabaseDetails(String database)
    {
        try
        {
            DatabaseDetails details = DatabaseDetails.getDefaults(database);
            details.setupForDatabase(database); // required to get dialect
            return details;
        }
        catch (ConfigurationException e)
        {
            logger.debug(e);
            return null;
        }
    }

    public String getDatabaseOption()
    {
        return databaseOption;
    }

    public void setDatabaseOption(String databaseOption)
    {
        this.databaseOption = databaseOption;
    }

    public List<PairType> getDatabaseList()
    {
        PairType select = new PairType("", getText("database.select.dropdown.value"));

        List selectlist = new ArrayList();
        selectlist.add(select);
        selectlist.addAll(databases.getDatabases());
        
        return selectlist;
    }

    public String getEmbeddedValue()
    {
        return EMBEDDED;
    }

    public String getJdbcValue()
    {
        return JDBC_DB;
    }

    public String getDatasourceValue()
    {
        return DATASOURCE;
    }

    public boolean isEmbeddedSelected()
    {
        return getEmbeddedValue().equals(getDatabaseOption());
    }

    public boolean isJdbcSelected()
    {
        return getJdbcValue().equals(getDatabaseOption());
    }

    public boolean isDatasourceSelected()
    {
        return getDatasourceValue().equals(getDatabaseOption());
    }

    public DatabaseList getDatabases()
    {
        return databases;
    }

    public void setDatabases(DatabaseList databases)
    {
        this.databases = databases;
    }

    public String getJdbcDatabaseType()
    {
        return jdbcDatabaseType;
    }

    public void setJdbcDatabaseType(String jdbcDatabaseType)
    {
        this.jdbcDatabaseType = jdbcDatabaseType;
    }

    public String getJdbcDriverClassName()
    {
        return jdbcDriverClassName;
    }

    public void setJdbcDriverClassName(String jdbcDriverClassName)
    {
        this.jdbcDriverClassName = jdbcDriverClassName;
    }

    public String getJdbcUrl()
    {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl)
    {
        this.jdbcUrl = jdbcUrl;
    }

    public String getJdbcUsername()
    {
        return jdbcUsername;
    }

    public void setJdbcUsername(String jdbcUsername)
    {
        this.jdbcUsername = jdbcUsername;
    }

    public String getJdbcPassword()
    {
        return jdbcPassword;
    }

    public void setJdbcPassword(String jdbcPassword)
    {
        this.jdbcPassword = jdbcPassword;
    }

    public String getJdbcHibernateDialect()
    {
        return jdbcHibernateDialect;
    }

    public void setJdbcHibernateDialect(String jdbcHibernateDialect)
    {
        this.jdbcHibernateDialect = jdbcHibernateDialect;
    }

    public String getDatasourceDatabaseType()
    {
        return datasourceDatabaseType;
    }

    public void setDatasourceDatabaseType(String datasourceDatabaseType)
    {
        this.datasourceDatabaseType = datasourceDatabaseType;
    }

    public String getDatasourceJndiName()
    {
        return datasourceJndiName;
    }

    public void setDatasourceJndiName(String datasourceJndiName)
    {
        this.datasourceJndiName = datasourceJndiName;
    }

    public String getDatasourceHibernateDialect()
    {
        return datasourceHibernateDialect;
    }

    public void setDatasourceHibernateDialect(String datasourceHibernateDialect)
    {
        this.datasourceHibernateDialect = datasourceHibernateDialect;
    }

    public boolean isJdbcOverwriteData()
    {
        return jdbcOverwriteData;
    }

    public void setJdbcOverwriteData(boolean jdbcOverwriteData)
    {
        this.jdbcOverwriteData = jdbcOverwriteData;
    }

    public boolean isDatasourceOverwriteData()
    {
        return datasourceOverwriteData;
    }

    public void setDatasourceOverwriteData(boolean datasourceOverwriteData)
    {
        this.datasourceOverwriteData = datasourceOverwriteData;
    }

    public boolean isUpgradeFromExistingDatabase()
    {
        //return ((CrowdSetupPersister)getSetupPersister()).isSetupTypeDatabaseUpgrage();

        return false; // this legacy 1.2.x database upgrade step is no longer supported in Crowd 2.0+ as you must export all Crowd 1.x data to move to 2.0+.
    }

    public void setRequiredPluginsStartupCheck(RequiredPluginsStartupCheck requiredPluginsStartupCheck)
    {
        this.requiredPluginsStartupCheck = requiredPluginsStartupCheck;
    }

    public RequiredPluginsStartupCheck getRequiredPluginsStartupCheck()
    {
        // the first time this action is constructed, the context has not been initialised yet, therefore
        // this bean is not autowired. If this is the case, we load it lazily.
        if (requiredPluginsStartupCheck == null)
        {
            requiredPluginsStartupCheck = ContainerManager.getComponent("requiredPluginsStartupCheck",
                                                                        RequiredPluginsStartupCheck.class);
        }
        return requiredPluginsStartupCheck;
    }
}
