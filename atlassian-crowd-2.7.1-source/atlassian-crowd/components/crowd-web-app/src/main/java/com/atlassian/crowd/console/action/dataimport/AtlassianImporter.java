package com.atlassian.crowd.console.action.dataimport;

import com.atlassian.core.util.PairType;
import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.config.JdbcConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.manager.ImporterManager;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import com.atlassian.crowd.xwork.ParameterSafe;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * This action handles the importing of all Atlassian
 * products into Crowd
 */
public class AtlassianImporter extends BaseImporter
{
    private Long directoryID;

    private Long applicationId;

    private ImporterManager importerManager;

    private List<Directory> internalAtlassianSHA1directories;

    private List<PairType> applications;

    private JdbcConfiguration configuration = new JdbcConfiguration(null, null, null, "jdbc:mysql://localhost/dbname?autoReconnect=true", "com.mysql.jdbc.Driver", "root", "");

    private List sourceDirectories;

    public String doDefault() throws Exception
    {
        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doImport()
    {
        try
        {
            result = importerManager.performImport(configuration);
        }
        catch (ImporterException e)
        {
            logger.error("Import failed", e);
            addActionError(e);
            return ERROR;
        }

        return SUCCESS;
    }

    public String doCheckAdditionalParamsRequired()
    {
        // Set the import passwords to null, if it has not been set
        if (configuration.isImportPasswords() == null)
        {
            configuration.setImportPasswords(Boolean.FALSE);
        }

        // Validate configuration
        doValidation();
        if (hasErrors())
        {
            return ERROR;
        }

        try
        {
            if (importerManager.supportsMultipleDirectories(configuration))
            {
                // retrieve the directories.
                final List<Directory> remoteSourceDirectories = new ArrayList<Directory>(importerManager.retrieveRemoteSourceDirectories(configuration));

                // there is only one internal directory so the it's obvious.
                if (remoteSourceDirectories.size() == 1)
                {
                    configuration.setSourceDirectoryID(remoteSourceDirectories.get(0).getId());
                    return doImport();
                }
                // if no directory at all, something is wrong.
                else if (remoteSourceDirectories.size() == 0)
                {
                    addActionError(getText("dataimport.importdirectory.nodirectory.error"));
                    return ERROR;
                }
                // otherwise, the directory has to be selected.
                else
                {
                    Collections.sort(remoteSourceDirectories, new Comparator<Directory>()
                    {
                        public int compare(Directory o1, Directory o2)
                        {
                            return o1.getName().compareTo(o2.getName());
                        }
                    });
                    setSourceDirectories(remoteSourceDirectories);

                    return "selectdir";
                }
            }
            // older apps, just import.
            else
            {
                return doImport();
            }
        }
        catch (ImporterException e)
        {
            logger.error("Import configuration validation failed", e);
            addActionError(e);
            return ERROR;
        }
    }

    private void doValidation()
    {
        if (StringUtils.isBlank(configuration.getDatabaseDriver()))
        {
            addFieldError("configuration.databaseDriver", getText("dataimport.configuration.databasedriver.error"));
        }

        if (StringUtils.isBlank(configuration.getDatabaseURL()))
        {
            addFieldError("configuration.databaseURL", getText("dataimport.configuration.databaseurl.error"));
        }

        if (StringUtils.isBlank(configuration.getUsername()))
        {
            addFieldError("configuration.username", getText("dataimport.configuration.username.error"));
        }

        if (StringUtils.isBlank(configuration.getApplication()))
        {
            addFieldError("configuration.application", getText("dataimport.configuration.application.error"));
        }

        if (configuration.getDirectoryID() == null)
        {
            addFieldError("configuration.directoryID", getText("dataimport.configuration.directoryid.error"));
        }

        // If there are no errors so far, test the configuration
        if (!hasErrors())
        {
            try
            {
                importerManager.testConfiguration(configuration);
            }
            catch (ImporterException e)
            {
                logger.error("Import configuration test failed", e);
                addActionError(e);
            }
        }

        // If we are going to allow importing into an internal directory AND we have password importing
        // The directory MUST be internal and Atlassian Security
        if (configuration.isImportPasswords() != null && configuration.isImportPasswords().booleanValue() && configuration.getDirectoryID() != null)
        {
            try
            {
                Directory directory = directoryManager.findDirectoryById(configuration.getDirectoryID().longValue());
                String encryptionMethod = directory.getEncryptionType();

                if (!(DirectoryType.INTERNAL.equals(directory.getType()) && PasswordEncoderFactory.ATLASSIAN_SECURITY_ENCODER.equals(encryptionMethod)))
                {
                    addActionError(getText("dataimport.configuration.internal.securityonly"));
                }

            }
            catch (Exception e)
            {
                logger.error("Failed to find directory with ID : " + configuration.getDirectoryID(), e);
            }
        }
    }

    public List<Directory> getInternalAtlassianSecurityDirectories()
    {

        if (internalAtlassianSHA1directories == null || internalAtlassianSHA1directories.isEmpty())
        {
            internalAtlassianSHA1directories = new ArrayList<Directory>();

            final List<Directory> directories = directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.INTERNAL)).returningAtMost(EntityQuery.ALL_RESULTS));
            for (Directory directory : directories)
            {
                final Object userEncryption = directory.getValue(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD);
                if (userEncryption != null && PasswordEncoderFactory.ATLASSIAN_SECURITY_ENCODER.equals(userEncryption))
                {
                    internalAtlassianSHA1directories.add(directory);
                }
            }
        }

        return internalAtlassianSHA1directories;
    }

    public void setInternalAtlassianSecurityDirectories(List<Directory> internalAtlassianSHA1directories)
    {
        this.internalAtlassianSHA1directories = internalAtlassianSHA1directories;
    }

    public Long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(Long directoryID)
    {
        this.directoryID = directoryID;
    }

    public List<PairType> getAtlassianApplications()
    {
        if (this.applications == null)
        {
            this.applications = new ArrayList<PairType>();
            this.applications.add(new PairType("", getText("dataimport.importatlassian.product.select")));

            for (String appName : importerManager.getAtlassianSupportedApplications())
            {
                this.applications.add(new PairType(appName, getText("dataimport." + appName + ".label")));
            }
        }

        return this.applications;
    }

    public Long getApplicationId()
    {
        return applicationId;
    }

    public void setApplicationId(Long applicationId)
    {
        this.applicationId = applicationId;
    }

    public void setImporterManager(ImporterManager importerManager)
    {
        this.importerManager = importerManager;
    }

    @ParameterSafe
    public Configuration getConfiguration()
    {
        return configuration;
    }

    @ParameterSafe
    public void setConfiguration(JdbcConfiguration configuration)
    {
        this.configuration = configuration;
    }

    public List getSourceDirectories()
    {
        return sourceDirectories;
    }

    public void setSourceDirectories(List sourceDirectories)
    {
        this.sourceDirectories = sourceDirectories;
    }
}