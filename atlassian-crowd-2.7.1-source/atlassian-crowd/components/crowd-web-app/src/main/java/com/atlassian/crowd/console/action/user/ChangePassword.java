package com.atlassian.crowd.console.action.user;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.PermissionException;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.google.common.base.Objects;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class ChangePassword extends BaseUserAction
{
    private static final Logger logger = Logger.getLogger(ChangePassword.class);

    private String originalPassword;
    private String password;
    private String confirmPassword;
    private String passwordComplexityMessage;
    private ApplicationService applicationService;

    public String doDefault() throws Exception
    {
        String message = (String) getSession().getAttribute(ERROR);

        getSession().removeAttribute(ERROR);

        if (StringUtils.isNotBlank(message))
        {
            addActionError(message);
        }

        addPasswordComplexityMessage();

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate() throws Exception
    {
        addPasswordComplexityMessage();
        // check input to be there
        doValidation();

        if (hasErrors())
        {
            return INPUT;
        }

        Application crowdApplication;
        try
        {
            crowdApplication = getCrowdApplication();
        }
        catch (ApplicationNotFoundException e)
        {
            logger.error("Cannot find the Crowd application, Crowd is misconfigured: " + e.getApplicationName(), e);
            return ERROR;
        }

        // do they know their original password?
        try
        {
            applicationService.authenticateUser(crowdApplication, getRemoteUsername(),
                                                new PasswordCredential(originalPassword));
        }
        catch (Exception e)
        {
            logger.debug("When verifying the original password", e);

            addFieldError("originalPassword", getText("password.invalid"));

            return INPUT;
        }

        // update their password
        try
        {
            applicationService.updateUserCredential(crowdApplication, getRemoteUsername(),
                                                    new PasswordCredential(password));

            addActionMessage(ALERT_BLUE, getText("passwordupdate.message"));

            return SUCCESS;
        }
        catch (PermissionException e)
        {
            logger.info("When changing the user password", e);
            addActionError(getText("user.console.password.permission.error"));
        }
        catch (InvalidCredentialException e)
        {
            addActionError(getText("passwordupdate.policy.error.message"));
            addFieldError("password", Objects.firstNonNull(e.getPolicyDescription(), e.getMessage()));
        }
        catch (Exception e)
        {
            logger.error("When changing the user password", e);
            addActionError(getText("user.console.password.permission.error"));
        }

        return INPUT;
    }

    private void addPasswordComplexityMessage()
    {
        User remoteUser = getRemoteUser();
        if (remoteUser != null)
        {
            passwordComplexityMessage = getPasswordComplexityMessage(remoteUser.getDirectoryId());
        }
    }

    protected void doValidation()
    {
        if (StringUtils.isEmpty(originalPassword))
        {
            addFieldError("originalPassword", getText("password.invalid"));
        }
        else
        {
            if (StringUtils.isEmpty(password))
            {
                addFieldError("password", getText("passwordempty.invalid"));
            }
            if (StringUtils.isEmpty(confirmPassword))
            {
                addFieldError("confirmpassword", getText("passwordempty.invalid"));
            }
            else if (!StringUtils.equals(password, confirmPassword))
            {
                addFieldError("password", getText("passworddonotmatch.invalid"));
            }
        }
    }

    public String getConfirmPassword()
    {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword)
    {
        this.confirmPassword = confirmPassword;
    }

    public String getOriginalPassword()
    {
        return originalPassword;
    }

    public void setOriginalPassword(String originalPassword)
    {
        this.originalPassword = originalPassword;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public void setApplicationService(ApplicationService applicationService)
    {
        this.applicationService = applicationService;
    }

    public String getPasswordComplexityMessage()
    {
        return passwordComplexityMessage;
    }
}
