package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.console.value.directory.RemoteCrowdConnection;
import com.atlassian.crowd.directory.RemoteCrowdDirectory;
import com.atlassian.crowd.directory.SynchronisableDirectoryProperties;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelper;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Action to handle updating connection details for a remote Crowd directory.
 */
public class UpdateRemoteCrowdConnection extends BaseAction
{
    private long ID = -1;

    private Directory directory;

    private String url;
    private String applicationName;
    private String applicationPassword;

    private final RemoteCrowdConnection connection = new RemoteCrowdConnection();

    private String httpProxyHost;
    private Integer httpProxyPort;
    private String httpProxyUsername;
    private String httpProxyPassword;

    private LDAPPropertiesHelper ldapPropertiesHelper;
    private PasswordEncoderFactory passwordEncoderFactory;
    protected DirectoryInstanceLoader directoryInstanceLoader;

    public String execute()
    {
        url = getDirectory().getValue(RemoteCrowdDirectory.CROWD_SERVER_URL);
        applicationName = getDirectory().getValue(RemoteCrowdDirectory.APPLICATION_NAME);
        applicationPassword = getDirectory().getValue(RemoteCrowdDirectory.APPLICATION_PASSWORD);

        setIncrementalSyncEnabled(Boolean.parseBoolean(getDirectory().getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)));
        setPollingIntervalInMin(getAttributeValueAsLong(directory, SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL) / 60);

        final String httpTimeout = getDirectory().getValue(RemoteCrowdDirectory.CROWD_HTTP_TIMEOUT);
        if (httpTimeout != null)
        {
            setHttpTimeout(Long.parseLong(httpTimeout) / 1000);
        }
        final String httpMaxConnections = getDirectory().getValue(RemoteCrowdDirectory.CROWD_HTTP_MAX_CONNECTIONS);
        if (httpMaxConnections != null)
        {
            setHttpMaxConnections(Long.parseLong(httpMaxConnections));
        }
        httpProxyHost = getDirectory().getValue(RemoteCrowdDirectory.CROWD_HTTP_PROXY_HOST);
        String proxyPort = getDirectory().getValue(RemoteCrowdDirectory.CROWD_HTTP_PROXY_PORT);
        httpProxyPort = proxyPort == null? null: Integer.parseInt(proxyPort);
        httpProxyUsername = getDirectory().getValue(RemoteCrowdDirectory.CROWD_HTTP_PROXY_USERNAME);
        httpProxyPassword = getDirectory().getValue(RemoteCrowdDirectory.CROWD_HTTP_PROXY_PASSWORD);

        return SUCCESS;
    }

    private long getAttributeValueAsLong(Directory directory, String attributeName)
    {
        String value = directory.getValue(attributeName);
        if (value == null)
        {
            return 0;
        }
        else
        {
            return Long.parseLong(value);
        }
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        doValidation();

        if (hasErrors())
        {
            addActionError(getText("directorycrowd.update.invalid"));
            return ERROR;
        }

        try
        {
            DirectoryImpl updatedDirectory = new DirectoryImpl(getDirectory());

            // set connection information
            updatedDirectory.setAttribute(RemoteCrowdDirectory.CROWD_SERVER_URL, url);
            updatedDirectory.setAttribute(RemoteCrowdDirectory.APPLICATION_NAME, applicationName);

            updatedDirectory.setAttribute(RemoteCrowdDirectory.CROWD_HTTP_TIMEOUT, Long.toString(getHttpTimeout() * 1000));
            updatedDirectory.setAttribute(RemoteCrowdDirectory.CROWD_HTTP_MAX_CONNECTIONS, Long.toString(getHttpMaxConnections()));

            updatedDirectory.setAttribute(RemoteCrowdDirectory.CROWD_HTTP_PROXY_HOST, StringUtils.isBlank(httpProxyHost) ? null : httpProxyHost);
            updatedDirectory.setAttribute(RemoteCrowdDirectory.CROWD_HTTP_PROXY_PORT, httpProxyPort == null ? null : Integer.toString(httpProxyPort));
            updatedDirectory.setAttribute(RemoteCrowdDirectory.CROWD_HTTP_PROXY_USERNAME, StringUtils.isBlank(httpProxyUsername) ? null : httpProxyUsername);
            updatedDirectory.setAttribute(RemoteCrowdDirectory.CROWD_HTTP_PROXY_PASSWORD, StringUtils.isBlank(httpProxyPassword) ? null : httpProxyPassword);

            updatedDirectory.setAttribute(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED, Boolean.toString(isIncrementalSyncEnabled()));
            updatedDirectory.setAttribute(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL, Long.toString(getPollingIntervalInMin() * 60));

            // Only set the password attribute if it contains a value
            if (StringUtils.isNotEmpty(applicationPassword))
            {
                updatedDirectory.setAttribute(RemoteCrowdDirectory.APPLICATION_PASSWORD, applicationPassword);
            }

            // Test connection if the password has changed
            if (StringUtils.isNotEmpty(applicationPassword))
            {
                directoryInstanceLoader.getRawDirectory(updatedDirectory.getId(), updatedDirectory.getImplementationClass(), updatedDirectory.getAttributes()).testConnection();
            }

            directoryManager.updateDirectory(updatedDirectory);

            return SUCCESS;
        }
        catch (DirectoryNotFoundException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
        catch (OperationFailedException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
    }

    public String doTestUpdateConnection()
    {
        try
        {
            Map<String, String> testAttributes = buildTestConnectionAttributes();

            directoryInstanceLoader.getRawDirectory(getDirectory().getId(), getDirectory().getImplementationClass(), testAttributes).testConnection();

            actionMessageAlertColor = ALERT_BLUE;

            addActionMessage(getText("directorycrowd.testconnection.success"));
        }
        catch (Exception e)
        {
            addActionError(getText("directorycrowd.testconnection.invalid") + " " + e.getMessage());
        }

        return SUCCESS;
    }

    protected Map<String, String> buildTestConnectionAttributes()
    {
        Map<String, String> testAttributes = new HashMap<String, String>(10);

        // Set values according to form incase it has been changed
        testAttributes.put(RemoteCrowdDirectory.CROWD_SERVER_URL, url);
        testAttributes.put(RemoteCrowdDirectory.APPLICATION_NAME, applicationName);
        testAttributes.put(RemoteCrowdDirectory.APPLICATION_PASSWORD, applicationPassword);

        testAttributes.put(RemoteCrowdDirectory.CROWD_HTTP_TIMEOUT, Long.toString(getHttpTimeout() * 1000));
        testAttributes.put(RemoteCrowdDirectory.CROWD_HTTP_MAX_CONNECTIONS, Long.toString(getHttpMaxConnections()));

        testAttributes.put(RemoteCrowdDirectory.CROWD_HTTP_PROXY_HOST, StringUtils.isBlank(httpProxyHost)? null:httpProxyHost);
        testAttributes.put(RemoteCrowdDirectory.CROWD_HTTP_PROXY_PORT, httpProxyPort==null ? null:Integer.toString(httpProxyPort));
        testAttributes.put(RemoteCrowdDirectory.CROWD_HTTP_PROXY_USERNAME, StringUtils.isBlank(httpProxyUsername)? null:httpProxyUsername);
        testAttributes.put(RemoteCrowdDirectory.CROWD_HTTP_PROXY_PASSWORD, StringUtils.isBlank(httpProxyPassword)? null:httpProxyPassword);

        return testAttributes;
    }


    private void doValidation()
    {
        if (StringUtils.isEmpty(url))
        {
            addFieldError("url", getText("directorycrowd.url.invalid"));
        }

        if (StringUtils.isEmpty(applicationName))
        {
            addFieldError("applicationName", getText("directorycrowd.applicationname.invalid"));
        }

        if (getPollingIntervalInMin() <= 0)
        {
            addFieldError("pollingIntervalInMin", getText("directorycrowd.polling.interval.invalid"));
        }

        if (getHttpMaxConnections() <= 0)
        {
            addFieldError("httpMaxConnections", getText("directorycrowd.http.maxconnections.invalid"));
        }

        if (httpProxyPort != null && httpProxyPort <= 0)
        {
            addFieldError("httpProxyPort", getText("directorycrowd.proxy.port.invalid"));
        }
    }

    ///CLOVER:OFF

    public long getID()
    {
        return ID;
    }

    public void setID(long ID)
    {
        this.ID = ID;
    }

    public Directory getDirectory()
    {
        if (directory == null && ID != -1)
        {
            try
            {
                directory = directoryManager.findDirectoryById(ID);
            }
            catch (DirectoryNotFoundException e)
            {
                addActionError(e);
            }
        }

        return directory;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getApplicationName()
    {
        return applicationName;
    }

    public void setApplicationName(String applicationName)
    {
        this.applicationName = applicationName;
    }

    public String getApplicationPassword()
    {
        return applicationPassword;
    }

    public void setApplicationPassword(String applicationPassword)
    {
        this.applicationPassword = applicationPassword;
    }

    public boolean isIncrementalSyncEnabled()
    {
        return connection.isIncrementalSyncEnabled();
    }

    public void setIncrementalSyncEnabled(boolean incrementalSyncEnabled)
    {
        connection.setIncrementalSyncEnabled(incrementalSyncEnabled);
    }

    public long getPollingIntervalInMin()
    {
        return connection.getPollingIntervalInMin();
    }

    public void setPollingIntervalInMin(long pollingIntervalInMin)
    {
        connection.setPollingIntervalInMin(pollingIntervalInMin);
    }

    public LDAPPropertiesHelper getLdapPropertiesHelper()
    {
        return ldapPropertiesHelper;
    }

    public void setLdapPropertiesHelper(LDAPPropertiesHelper ldapPropertiesHelper)
    {
        this.ldapPropertiesHelper = ldapPropertiesHelper;
    }

    public PasswordEncoderFactory getPasswordEncoderFactory()
    {
        return passwordEncoderFactory;
    }

    public void setPasswordEncoderFactory(PasswordEncoderFactory passwordEncoderFactory)
    {
        this.passwordEncoderFactory = passwordEncoderFactory;
    }

    public DirectoryInstanceLoader getDirectoryInstanceLoader()
    {
        return directoryInstanceLoader;
    }

    public void setDirectoryInstanceLoader(DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }

    public long getHttpTimeout()
    {
        return connection.getHttpTimeout();
    }

    public void setHttpTimeout(long httpTimeout)
    {
        connection.setHttpTimeout(httpTimeout);
    }

    public long getHttpMaxConnections()
    {
        return connection.getHttpMaxConnections();
    }

    public void setHttpMaxConnections(long httpMaxConnections)
    {
        connection.setHttpMaxConnections(httpMaxConnections);
    }

    public String getHttpProxyHost()
    {
        return httpProxyHost;
    }

    public void setHttpProxyHost(String httpProxyHost)
    {
        this.httpProxyHost = httpProxyHost;
    }

    public Integer getHttpProxyPort()
    {
        return httpProxyPort;
    }

    public void setHttpProxyPort(Integer httpProxyPort)
    {
        this.httpProxyPort = httpProxyPort;
    }

    public String getHttpProxyUsername()
    {
        return httpProxyUsername;
    }

    public void setHttpProxyUsername(String httpProxyUsername)
    {
        this.httpProxyUsername = httpProxyUsername;
    }

    public String getHttpProxyPassword()
    {
        return httpProxyPassword;
    }

    public void setHttpProxyPassword(String httpProxyPassword)
    {
        this.httpProxyPassword = httpProxyPassword;
    }
}
