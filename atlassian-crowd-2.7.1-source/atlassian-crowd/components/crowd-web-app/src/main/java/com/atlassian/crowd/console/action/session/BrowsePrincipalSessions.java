package com.atlassian.crowd.console.action.session;

import java.util.List;

import com.atlassian.crowd.console.action.AbstractBrowser;
import com.atlassian.crowd.dao.token.SearchableTokenStorage;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.manager.token.SearchableTokenService;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.TokenTermKeys;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class BrowsePrincipalSessions extends AbstractBrowser<Token>
{
    private static final Logger logger = Logger.getLogger(BrowsePrincipalSessions.class);

    private String name;
    private long directoryID = -1;

    private SearchableTokenStorage tokenStorage;

    public String execute()
    {
        try
        {
            SearchRestriction directoryRestriction;
            if (directoryID == -1)
            {
                // search only for principal tokens (not application tokens)
                directoryRestriction = Restriction.on(TokenTermKeys.DIRECTORY_ID).greaterThan(Token.APPLICATION_TOKEN_DIRECTORY_ID);
            }
            else
            {
                // search for tokens with the specified directory
                directoryRestriction = Restriction.on(TokenTermKeys.DIRECTORY_ID).exactlyMatching(directoryID);
            }

            SearchRestriction restriction = directoryRestriction;
            if (StringUtils.isNotBlank(name))
            {
                restriction = Combine.allOf(directoryRestriction, Restriction.on(TokenTermKeys.NAME).startingWith(name));
            }

            EntityQuery<Token> query = QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).with(restriction).startingAt(resultsStart).returningAtMost(resultsPerPage + 1);

            results = tokenStorage.search(query);
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
        }

        return SUCCESS;
    }

    public List<Directory> getDirectories()
    {
        return directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS));
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public long getSelectedDirectoryID()
    {
        return directoryID;
    }

    public void setSelectedDirectoryID(long directoryID)
    {
        this.directoryID = directoryID;
    }

    public void setTokenManager(SearchableTokenService tokenStorage)
    {
        this.tokenStorage = tokenStorage;
    }
}
