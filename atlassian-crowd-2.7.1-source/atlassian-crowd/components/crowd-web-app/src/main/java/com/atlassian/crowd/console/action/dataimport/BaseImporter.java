package com.atlassian.crowd.console.action.dataimport;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.importer.model.Result;
import org.apache.log4j.Logger;

import java.util.List;

public class BaseImporter extends BaseAction
{
    protected final Logger logger = Logger.getLogger(this.getClass());

    protected Result result;
    private List directories;
    protected static final String IMPORTER_CONFIGURATION = "IMPORTER_CONFIGURATION";

    public Result getResult()
    {
        return result;
    }

    public void setResult(Result result)
    {
        this.result = result;
    }

    public List getDirectories()
    {
        if (directories == null || directories.isEmpty())
        {
            setDirectories(directoryManager.findAllDirectories());
        }

        return directories;
    }

    public void setDirectories(List directories)
    {
        this.directories = directories;
    }
}
