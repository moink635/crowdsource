package com.atlassian.crowd.console.action.dataimport;

import com.atlassian.crowd.console.action.BaseAction;
import org.apache.log4j.Logger;

public class ImportType extends BaseAction
{
    protected final Logger logger = Logger.getLogger(this.getClass());

    public String execute() throws Exception
    {
        getSession().removeAttribute(BaseImporter.IMPORTER_CONFIGURATION);
        
        return INPUT;
    }

    public String doDefault() throws Exception
    {
        getSession().removeAttribute(BaseImporter.IMPORTER_CONFIGURATION);
        
        return INPUT;
    }
}
