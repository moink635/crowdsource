package com.atlassian.crowd.console.action.setup;

import com.atlassian.core.util.DateUtils;
import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.migration.XmlMigrationManager;

import com.google.common.collect.ImmutableList;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.ResourceBundle;

/**
 * Web action to handle the import of Crowd data via XML
 */
public class Import extends BaseSetupAction
{
    private static final Logger logger = Logger.getLogger(Import.class);

    public static final String XML_IMPORT_STEP = "setupimport";

    private XmlMigrationManager xmlMigrationManager;

    private String filePath;

    public String doDefault()
    {
        // check if i'm at the correct step
        String setupDefault = super.doDefault();
        if (!setupDefault.equals(SUCCESS))
        {
            return setupDefault;
        }

        return INPUT;
    }

    public String getStepName()
    {
        return XML_IMPORT_STEP;
    }

    public void doValidation()
    {
        if (StringUtils.isBlank(filePath))
        {
            addFieldError("filePath", getText("setup.import.filepath.error"));
        }
    }

    public String doUpdate()
    {
        // check if i'm at the correct step
        String setupUpdate = super.doUpdate();
        if (!setupUpdate.equals(SUCCESS))
        {
            return setupUpdate;
        }

        doValidation();

        if (hasErrors())
        {
            return ERROR;
        }
        else
        {

            try
            {
                long timeTaken = xmlMigrationManager.importXml(filePath);

                // notify completeion with an indication of time
                DateUtils dateUtils = new DateUtils(ResourceBundle.getBundle(BaseAction.class.getName()));
                logger.info(getText("administration.import.complete", ImmutableList.of(dateUtils.formatDurationPretty(timeTaken / 1000))));
            }
            catch (Exception e)
            {
                logger.error("Error importing XML data", e);
                addActionError(e);
                return ERROR;
            }

            getSetupPersister().progessSetupStep();

            return SELECT_SETUP_STEP;
        }
    }

    public void setXmlMigrationManager(final XmlMigrationManager xmlMigrationManager)
    {
        this.xmlMigrationManager = xmlMigrationManager;
    }

    public String getFilePath()
    {
        return filePath;
    }

    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }
}
