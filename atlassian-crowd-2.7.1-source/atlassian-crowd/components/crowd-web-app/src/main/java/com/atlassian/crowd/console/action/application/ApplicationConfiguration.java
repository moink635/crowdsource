package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.model.application.ApplicationType;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Holder class for Application information collected during
 * the 'Application Wizard'
 */
public class ApplicationConfiguration implements Serializable
{
    private String name;
    private String description;
    private String password;
    private ApplicationType applicationType;
    private List<String> remoteAddresses;
    private String applicationURL;
    private List<Long> directoryids;
    private Map<Long, Set<String>> directoryGroupMappings;
    private Map<Long, Boolean> allowAllForDirectory;

    public void setApplicationType(final ApplicationType applicationType)
    {
        this.applicationType = applicationType;
    }

    public void setDescription(final String description)
    {
        this.description = description;
    }

    public void setName(final String name)
    {
        this.name = name;
    }

    public void setPassword(final String password)
    {
        this.password = password;
    }

    public ApplicationType getApplicationType()
    {
        return applicationType;
    }

    public String getDescription()
    {
        return description;
    }

    public String getName()
    {
        return name;
    }

    public String getPassword()
    {
        return password;
    }

    public void setIPAddresses(final List<String> remoteAddresses)
    {
        this.remoteAddresses = remoteAddresses;
    }

    public List<String> getRemoteAddresses()
    {
        return remoteAddresses;
    }

    public void setApplicationURL(final String applicationURL)
    {
        this.applicationURL = applicationURL;
    }

    public void setDirectoryIds(final List<Long> directoryids)
    {
        this.directoryids = directoryids;
    }

    public String getApplicationURL()
    {
        return applicationURL;
    }

    public List<Long> getDirectoryids()
    {
        return directoryids;
    }

    public Map<Long, Set<String>> getDirectoryGroupMappings()
    {
        if (directoryGroupMappings == null)
        {
            directoryGroupMappings = new HashMap<Long, Set<String>>();
        }

        return directoryGroupMappings;
    }

    public void setDirectoryGroupMappings(final Map<Long, Set<String>> directoryGroupMappings)
    {
        this.directoryGroupMappings = directoryGroupMappings;
    }

    public Map<Long, Boolean> getAllowAllForDirectory()
    {
        if (allowAllForDirectory == null)
        {
            return Collections.emptyMap();
        }
        return allowAllForDirectory;
    }

    public void removeAllowAll(Long directoryID)
    {
        if (allowAllForDirectory != null)
        {
            allowAllForDirectory.remove(directoryID);
        }
    }

    public void setAllowAll(Long directoryID)
    {
        if (allowAllForDirectory == null)
        {
            allowAllForDirectory = new HashMap<Long, Boolean>();
        }
        allowAllForDirectory.put(directoryID, Boolean.TRUE);
    }
}
