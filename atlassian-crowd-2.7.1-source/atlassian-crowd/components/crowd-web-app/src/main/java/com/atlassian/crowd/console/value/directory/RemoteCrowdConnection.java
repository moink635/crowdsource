package com.atlassian.crowd.console.value.directory;

public class RemoteCrowdConnection
{
    private long httpTimeout = 5; // in seconds
    private long httpMaxConnections = 20;
    private boolean incrementalSyncEnabled = false;
    private long pollingIntervalInMin = 60;

    public long getHttpTimeout()
    {
        return httpTimeout;
    }

    public void setHttpTimeout(long httpTimeout)
    {
        this.httpTimeout = httpTimeout;
    }

    public long getHttpMaxConnections()
    {
        return httpMaxConnections;
    }

    public void setHttpMaxConnections(long httpMaxConnections)
    {
        this.httpMaxConnections = httpMaxConnections;
    }

    public boolean isIncrementalSyncEnabled()
    {
        return incrementalSyncEnabled;
    }

    public void setIncrementalSyncEnabled(boolean incrementalSyncEnabled)
    {
        this.incrementalSyncEnabled = incrementalSyncEnabled;
    }

    public long getPollingIntervalInMin()
    {
        return pollingIntervalInMin;
    }

    public void setPollingIntervalInMin(long pollingIntervalInMin)
    {
        this.pollingIntervalInMin = pollingIntervalInMin;
    }
}
