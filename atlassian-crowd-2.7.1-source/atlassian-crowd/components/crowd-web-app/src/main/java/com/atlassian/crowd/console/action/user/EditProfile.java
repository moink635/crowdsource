package com.atlassian.crowd.console.action.user;

import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.PermissionException;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.util.UserUtils;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

public class EditProfile extends BaseUserAction
{
    private static final Logger logger = Logger.getLogger(EditProfile.class);

    protected String username;
    protected String firstname;
    protected String lastname;
    protected String email;

    public String doDefault() throws Exception
    {
        username = getRemoteUser().getName();
        firstname = getRemoteUser().getFirstName();
        lastname = getRemoteUser().getLastName();
        email = getRemoteUser().getEmailAddress();

        return INPUT;
    }

    public void doValidation()
    {
        try
        {
            username = getRemoteUsername();
        }
        catch (InvalidUserException e)
        {
            username = "UNAUTHENTICATED";
            addActionError(getText("user.not.authenticated"));
        }

        if (StringUtils.isEmpty(firstname))
        {
            addFieldError("firstname", getText("principal.firstname.invalid"));
        }
        if (StringUtils.isEmpty(lastname))
        {
            addFieldError("lastname", getText("principal.lastname.invalid"));
        }
        if (StringUtils.isEmpty(email) || !UserUtils.isValidEmail(email))
        {
            addFieldError("email", getText("principal.email.invalid"));
        }
        else if (IdentifierUtils.hasLeadingOrTrailingWhitespace(email))
        {
            addFieldError("email", getText("principal.email.whitespace"));
        }
    }

    @RequireSecurityToken(true)
    public String doUpdate() throws Exception
    {
        doValidation();

        if (!hasErrors())
        {
            try
            {
                final UserTemplate userTemplate = new UserTemplate(username);
                userTemplate.setFirstName(firstname);
                userTemplate.setLastName(lastname);
                userTemplate.setEmailAddress(email);
                userTemplate.setDisplayName(firstname + " " + lastname);
                userTemplate.setActive(true); // see CWD-1728
                applicationService.updateUser(getCrowdApplication(), userTemplate);

            }
            catch (PermissionException e)
            {
                logger.info(e);
                addActionError(getText("user.console.profile.permission.error"));
            }
            catch (Exception e)
            {
                logger.error(e);
                addActionError(getText("user.console.profile.update.error"));
            }
        }

        if (hasErrors())
        {
            return INPUT;
        }
        else
        {
            ServletActionContext.getRequest().setAttribute("updateSuccessful", "true");
            return SUCCESS;
        }
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }
}
