package com.atlassian.crowd.console.action.directory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.atlassian.core.util.PairType;
import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.console.value.directory.ConnectorConnection;
import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.directory.SynchronisableDirectoryProperties;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapperImpl;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelper;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelperImpl;
import com.atlassian.crowd.directory.ldap.validator.ConnectorValidator;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Ordering;

import org.apache.commons.lang3.StringUtils;

import static org.apache.commons.lang3.BooleanUtils.isFalse;
import static org.apache.commons.lang3.BooleanUtils.toBoolean;

/**
 * Action to handle updating connection details for a 'Connector' based directory.
 */
public class UpdateConnectorConnection extends BaseAction
{
    private long ID = -1;

    private Directory directory;

    private String URL;
    private String baseDN;
    private String ldapPassword;
    private String savedLdapPassword;
    private boolean secure;
    private boolean referral;
    private boolean pagedResults;
    private boolean localUserStatusEnabled;
    private boolean localGroups;
    private boolean primaryGroupSupport;
    private boolean useNestedGroups;
    private boolean useUserMembershipAttribute;
    protected boolean useUserMembershipAttributeForGroupMembership;    // for AD, at least initially
    private Integer pagedResultsSize;
    private String userEncryptionMethod;
    private String userDN;
    protected boolean pollingEnabled;
    private boolean useRelaxedDNStandardisation;

    private final ConnectorConnection connection = new ConnectorConnection();

    private LDAPPropertiesHelper ldapPropertiesHelper;
    private PasswordEncoderFactory passwordEncoderFactory;
    protected DirectoryInstanceLoader directoryInstanceLoader;

    private ConnectorValidator connectorValidator;

    public UpdateConnectorConnection()
    {
        pollingEnabled = true;
    }

    public String execute()
    {
        URL = getDirectory().getValue(LDAPPropertiesMapper.LDAP_URL_KEY);
        baseDN = getDirectory().getValue(LDAPPropertiesMapper.LDAP_BASEDN_KEY);
        ldapPassword = getDirectory().getValue(LDAPPropertiesMapper.LDAP_PASSWORD_KEY);
        secure = Boolean.valueOf(getDirectory().getValue(LDAPPropertiesMapper.LDAP_SECURE_KEY));
        referral = Boolean.valueOf(getDirectory().getValue(LDAPPropertiesMapper.LDAP_REFERRAL_KEY));
        pagedResults = Boolean.valueOf(getDirectory().getValue(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY));
        useUserMembershipAttribute = Boolean.valueOf(getDirectory().getValue(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE));
        useUserMembershipAttributeForGroupMembership = Boolean.valueOf(getDirectory().getValue(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE_FOR_GROUP_MEMBERSHIP));

        String pagedResultsSizeAsString = getDirectory().getValue(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE);
        if (pagedResults && StringUtils.isNotEmpty(pagedResultsSizeAsString))
        {
            try
            {
                pagedResultsSize = Integer.valueOf(pagedResultsSizeAsString);
            } catch (NumberFormatException e)
            {
                logger.error("Failed to parse Paged Results Size: " + pagedResultsSizeAsString);
            }
        }
        userEncryptionMethod = getDirectory().getValue(LDAPPropertiesMapper.LDAP_USER_ENCRYPTION_METHOD);
        userDN = getDirectory().getValue(LDAPPropertiesMapper.LDAP_USERDN_KEY);

        setIncrementalSyncEnabled(Boolean.parseBoolean(getDirectory().getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)));
        // value is stored internally as seconds, but we want to display it as minutes to the user
        setPollingIntervalInMin(getAttributeValueAsLong(directory, SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL) / 60);

        LDAPPropertiesMapper ldapPropertiesMapper = new LDAPPropertiesMapperImpl(ldapPropertiesHelper);
        ldapPropertiesMapper.setAttributes(getDirectory().getAttributes());
        useRelaxedDNStandardisation = ldapPropertiesMapper.isRelaxedDnStandardisation();

        localUserStatusEnabled = ldapPropertiesMapper.isLocalUserStatusEnabled();
        localGroups = ldapPropertiesMapper.isLocalGroupsEnabled();
        primaryGroupSupport = ldapPropertiesMapper.isPrimaryGroupSupported();
        useNestedGroups = !ldapPropertiesMapper.isNestedGroupsDisabled();

        // timeout values are stored internally in milliseconds, but we want to display it as seconds to the user
        setReadTimeoutInSec(TimeUnit.SECONDS.convert(getAttributeValueAsLong(directory, LDAPPropertiesMapper.LDAP_READ_TIMEOUT), TimeUnit.MILLISECONDS));
        setSearchTimeoutInSec(TimeUnit.SECONDS.convert(getAttributeValueAsLong(directory, LDAPPropertiesMapper.LDAP_SEARCH_TIMELIMIT), TimeUnit.MILLISECONDS));
        setConnectionTimeoutInSec(TimeUnit.SECONDS.convert(getAttributeValueAsLong(directory, LDAPPropertiesMapper.LDAP_CONNECTION_TIMEOUT), TimeUnit.MILLISECONDS));

        // Check for errors in configuration on display of the connection details
        // If an error is detected, doValidation to display error messages for specific fields
        Set<String> directoryErrors = connectorValidator.getErrors(getDirectory());
        if (!directoryErrors.isEmpty())
        {
            for (String error : directoryErrors)
            {
                addActionError(error);
            }
            doValidation(directory);
        }

        return SUCCESS;
    }

    private long getAttributeValueAsLong(Directory directory, String attributeName)
    {
        String value = directory.getValue(attributeName);
        if (value == null)
        {
            return 0;
        } else
        {
            return Long.parseLong(value);
        }
    }

    protected void updateUseNestedGroupsAttribute(DirectoryImpl directory)
    {
        if (useNestedGroups)
        {
            directory.setAttribute(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, Boolean.toString(Boolean.FALSE));
        } else
        {
            directory.setAttribute(LDAPPropertiesMapper.LDAP_NESTED_GROUPS_DISABLED, Boolean.toString(Boolean.TRUE));
        }
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            restoreSavedPassword();
            DirectoryImpl directory = new DirectoryImpl(getDirectory());
            doValidation(directory);

            if (hasErrors())
            {
                addActionError(getText("directoryconnector.update.invalid"));
                return ERROR;
            }

            updateDirectory(directory);

            // Test connection if the password has changed
            if (StringUtils.isNotEmpty(ldapPassword))
            {
                directoryInstanceLoader.getRawDirectory(directory.getId(), directory.getImplementationClass(), directory.getAttributes()).testConnection();
            }

            directoryManager.updateDirectory(directory);

            return SUCCESS;
        }
        catch (DirectoryNotFoundException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
        catch (DirectoryInstantiationException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
        catch (OperationFailedException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return ERROR;
        }
    }

    @VisibleForTesting
    public void updateDirectory(final DirectoryImpl updatedDirectory)
    {
        // set connection information
        updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_URL_KEY, URL);
        updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_BASEDN_KEY, baseDN);
        updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_SECURE_KEY, Boolean.toString(secure));
        updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_REFERRAL_KEY, Boolean.toString(referral));
        updatedDirectory.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS, Boolean.toString(localUserStatusEnabled));
        updatedDirectory.setAttribute(LDAPPropertiesMapper.LOCAL_GROUPS, Boolean.toString(localGroups));
        updatedDirectory.setAttribute(LDAPPropertiesMapper.PRIMARY_GROUP_SUPPORT,
                                      Boolean.toString(primaryGroupSupport));

        updateUseNestedGroupsAttribute(updatedDirectory);

        updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE, Boolean.toString(useUserMembershipAttribute));
        updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE_FOR_GROUP_MEMBERSHIP, Boolean.toString(useUserMembershipAttributeForGroupMembership));

        updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY, Boolean.toString(pagedResults));

        // set connection pool properties - convert from seconds to milliseconds to store
        updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_READ_TIMEOUT, Long.toString(TimeUnit.MILLISECONDS.convert(getReadTimeoutInSec(), TimeUnit.SECONDS)));
        updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_SEARCH_TIMELIMIT, Long.toString(TimeUnit.MILLISECONDS.convert(getSearchTimeoutInSec(), TimeUnit.SECONDS)));
        updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_CONNECTION_TIMEOUT, Long.toString(TimeUnit.MILLISECONDS.convert(getConnectionTimeoutInSec(), TimeUnit.SECONDS)));

        if (pagedResults)
        {
            updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE, Integer.toString(pagedResultsSize));
        } else
        {
            updatedDirectory.removeAttribute(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE);
        }

        if (isUserEncryptionConfigurable() && StringUtils.isNotBlank(userEncryptionMethod))
        {
            updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_USER_ENCRYPTION_METHOD, userEncryptionMethod);
        } else
        {
            updatedDirectory.removeAttribute(LDAPPropertiesMapper.LDAP_USER_ENCRYPTION_METHOD);
        }

        updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_USERDN_KEY, userDN);

        // Only set the password attribute if it contains a value
        if (ldapPassword != null)
        {
            updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_PASSWORD_KEY, ldapPassword);
        }

        updatedDirectory.setAttribute(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED, Boolean.toString(isIncrementalSyncEnabled()));

        // convert pollingInterval to seconds to store in database
        updatedDirectory.setAttribute(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL, Long.toString(getPollingIntervalInMin() * 60));

        // directories can choose whether or not to use relaxed DN standardisation
        updatedDirectory.setAttribute(LDAPPropertiesMapper.LDAP_RELAXED_DN_STANDARDISATION, Boolean.toString(useRelaxedDNStandardisation));
    }

    public String doTestUpdateConfiguration()
    {
        try
        {
            restoreSavedPassword();
            Map<String, String> testAttributes = buildTestConnectionAttributes();

            directoryInstanceLoader.getRawDirectory(getDirectory().getId(), getDirectory().getImplementationClass(), testAttributes).testConnection();

            actionMessageAlertColor = ALERT_BLUE;

            addActionMessage(getText("directoryconnector.testconnection.success"));
            return SUCCESS;
        }
        catch (DirectoryInstantiationException e)
        {
            addActionError(getText("directoryconnector.testconnection.invalid") + " " + e.getMessage());
            logger.error("Failed to instantiate directory for connection test", e);
            return ERROR;
        }
        catch (OperationFailedException e)
        {
            addActionError(getText("directoryconnector.testconnection.invalid") + " " + e.getMessage());
            return ERROR;
        }
    }

    protected Map<String, String> buildTestConnectionAttributes()
    {
        Map<String, String> testAttributes = new HashMap<String, String>(4);

        // Set values according to form incase it has been changed
        testAttributes.put(LDAPPropertiesMapper.LDAP_URL_KEY, URL);
        testAttributes.put(LDAPPropertiesMapper.LDAP_BASEDN_KEY, baseDN);
        testAttributes.put(LDAPPropertiesMapper.LDAP_USERDN_KEY, userDN);
        testAttributes.put(LDAPPropertiesMapper.LDAP_PASSWORD_KEY, ldapPassword);
        testAttributes.put(LDAPPropertiesMapper.LDAP_SECURE_KEY, Boolean.toString(secure));

        return testAttributes;
    }

    @VisibleForTesting
    void doValidation(Directory directory)
    {
        if (StringUtils.isEmpty(URL))
        {
            addFieldError("URL", getText("directoryconnector.url.invalid"));
        }

        // Get the underlying implementation class
        String ldapClass;
        if (directory.getType() == DirectoryType.DELEGATING)
        {
            ldapClass = directory.getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS);
        }
        else
        {
             ldapClass = directory.getImplementationClass();
        }

        if (CreateConnector.missingRequiredDn(ldapClass, baseDN))
        {
            addFieldError("baseDN", getText("directoryconnector.basedn.invalid.blank"));
        }
        else if (!CreateConnector.validDn(baseDN))
        {
            addFieldError("baseDN", getText("directoryconnector.basedn.invalid"));
        }

        if (pagedResults && (pagedResultsSize == null || pagedResultsSize < 100))
        {
            addFieldError("pagedResultsSize", getText("directoryconnector.pagedresultscontrolsize.invalid"));
        }

        if (pollingEnabled && getPollingIntervalInMin() <= 0)
        {
            addFieldError("pollingInterval", getText("directoryconnector.polling.interval.invalid"));
        }

        if (isFalse(toBoolean(directory.getValue(DirectoryProperties.CACHE_ENABLED))) && localUserStatusEnabled)
        {
            addFieldError("localUserStatusEnabled", getText("directoryconnector.localuserstatus.withoutcache.message"));
        }

        if (isFalse(toBoolean(directory.getValue(DirectoryProperties.CACHE_ENABLED))) && localGroups)
        {
            addFieldError("localGroupsEnabled", getText("directoryconnector.localgroups.withoutcache.message"));
        }
    }

    public boolean isUserEncryptionConfigurable()
    {
        for (Class<?> c : LDAPPropertiesHelperImpl.DIRECTORIES_WITH_CONFIGURABLE_USER_ENCRYPTION)
        {
            if (c.getCanonicalName().equals(getDirectory().getImplementationClass()))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Restore password from previous request(ex: Test connection) to the ldapPassword to ensure we don't save an empty password - refer CWD-1763
     */
    protected void restoreSavedPassword()
    {
        if (!StringUtils.isBlank(userDN) && StringUtils.isBlank(ldapPassword))
        {
            ldapPassword = savedLdapPassword;
        }
    }

    ///CLOVER:OFF

    public long getID()
    {
        return ID;
    }

    public void setID(long ID)
    {
        this.ID = ID;
    }

    public Directory getDirectory()
    {
        if (directory == null && ID != -1)
        {
            try
            {
                directory = directoryManager.findDirectoryById(ID);
            } catch (DirectoryNotFoundException e)
            {
                addActionError(e);
            }
        }

        return directory;
    }

    public String getURL()
    {
        return URL;
    }

    public void setURL(String URL)
    {
        this.URL = URL;
    }

    public String getBaseDN()
    {
        return baseDN;
    }

    public void setBaseDN(String baseDN)
    {
        this.baseDN = baseDN;
    }

    public String getLdapPassword()
    {
        return ldapPassword;
    }

    public void setLdapPassword(String ldapPassword)
    {
        this.ldapPassword = ldapPassword;
    }

    public boolean isSecure()
    {
        return secure;
    }

    public void setSecure(boolean secure)
    {
        this.secure = secure;
    }

    public boolean isReferral()
    {
        return referral;
    }

    public void setReferral(boolean referral)
    {
        this.referral = referral;
    }

    public boolean isPagedResults()
    {
        return pagedResults;
    }

    public void setPagedResults(boolean pagedResults)
    {
        this.pagedResults = pagedResults;
    }

    public Integer getPagedResultsSize()
    {
        return pagedResultsSize;
    }

    public void setPagedResultsSize(Integer pagedResultsSize)
    {
        this.pagedResultsSize = pagedResultsSize;
    }

    public String getUserEncryptionMethod()
    {
        return userEncryptionMethod;
    }

    public void setUserEncryptionMethod(String userEncryptionMethod)
    {
        this.userEncryptionMethod = userEncryptionMethod;
    }

    public String getUserDN()
    {
        return userDN;
    }

    public void setUserDN(String userDN)
    {
        this.userDN = userDN;
    }

    public boolean isPrimaryGroupSupport()
    {
        return primaryGroupSupport;
    }

    public void setPrimaryGroupSupport(boolean primaryGroupSupport)
    {
        this.primaryGroupSupport = primaryGroupSupport;
    }

    public boolean isUseNestedGroups()
    {
        return useNestedGroups;
    }

    public void setUseNestedGroups(boolean useNestedGroups)
    {
        this.useNestedGroups = useNestedGroups;
    }

    public boolean isUseUserMembershipAttribute()
    {
        return useUserMembershipAttribute;
    }

    public void setUseUserMembershipAttribute(boolean useUserMembershipAttribute)
    {
        this.useUserMembershipAttribute = useUserMembershipAttribute;
    }

    public boolean isUseUserMembershipAttributeForGroupMembership()
    {
        return useUserMembershipAttributeForGroupMembership;
    }

    public void setUseUserMembershipAttributeForGroupMembership(boolean useUserMembershipAttributeForGroupMembership)
    {
        this.useUserMembershipAttributeForGroupMembership = useUserMembershipAttributeForGroupMembership;
    }

    public boolean isIncrementalSyncEnabled()
    {
        return connection.isIncrementalSyncEnabled();
    }

    public void setIncrementalSyncEnabled(boolean incrementalSyncEnabled)
    {
        connection.setIncrementalSyncEnabled(incrementalSyncEnabled);
    }

    public long getPollingIntervalInMin()
    {
        return connection.getPollingIntervalInMin();
    }

    public void setPollingIntervalInMin(long pollingIntervalInMin)
    {
        connection.setPollingIntervalInMin(pollingIntervalInMin);
    }

    public boolean isUseRelaxedDNStandardisation()
    {
        return useRelaxedDNStandardisation;
    }

    public void setUseRelaxedDNStandardisation(boolean useRelaxedDNStandardisation)
    {
        this.useRelaxedDNStandardisation = useRelaxedDNStandardisation;
    }

    public List<PairType> getUserEncryptionMethods()
    {
        List<PairType> encoders = new ArrayList<PairType>();
        encoders.add(new PairType("", getText("directoryconnector.userencryptionmethod.select")));
        for (String encoder : Ordering.<String>natural().immutableSortedCopy(this.passwordEncoderFactory.getSupportedLdapEncoders()))
        {
            encoders.add(new PairType(encoder, encoder.toUpperCase()));

        }
        return encoders;
    }

    public void setPasswordEncoderFactory(PasswordEncoderFactory passwordEncoderFactory)
    {
        this.passwordEncoderFactory = passwordEncoderFactory;
    }

    public void setLdapPropertiesHelper(LDAPPropertiesHelper ldapPropertiesHelper)
    {
        this.ldapPropertiesHelper = ldapPropertiesHelper;
    }

    public void setDirectoryInstanceLoader(final DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }

    public void setConnectorValidator(ConnectorValidator connectorValidator)
    {
        this.connectorValidator = connectorValidator;
    }

    public long getReadTimeoutInSec()
    {
        return connection.getReadTimeoutInSec();
    }

    public void setReadTimeoutInSec(long readTimeoutInSec)
    {
        connection.setReadTimeoutInSec(readTimeoutInSec);
    }

    public long getSearchTimeoutInSec()
    {
        return connection.getSearchTimeoutInSec();
    }

    public void setSearchTimeoutInSec(long searchTimeoutInSec)
    {
        connection.setSearchTimeoutInSec(searchTimeoutInSec);
    }

    public long getConnectionTimeoutInSec()
    {
        return connection.getConnectionTimeoutInSec();
    }

    public void setConnectionTimeoutInSec(long connectionTimeoutInSec)
    {
        connection.setConnectionTimeoutInSec(connectionTimeoutInSec);
    }

    public void setSavedLdapPassword(String savedLdapPassword)
    {
        this.savedLdapPassword = savedLdapPassword;
    }

    public boolean isLocalUserStatusEnabled()
    {
        return localUserStatusEnabled;
    }

    public void setLocalUserStatusEnabled(boolean localUserStatusEnabled)
    {
        this.localUserStatusEnabled = localUserStatusEnabled;
    }

    public boolean isLocalGroupsEnabled()
    {
        return localGroups;
    }

    public void setLocalGroupsEnabled(boolean localGroups)
    {
        this.localGroups = localGroups;
    }
}
