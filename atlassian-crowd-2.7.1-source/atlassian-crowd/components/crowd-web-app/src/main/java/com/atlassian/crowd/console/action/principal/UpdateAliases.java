package com.atlassian.crowd.console.action.principal;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.AliasAlreadyInUseException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Map;

public class UpdateAliases extends ViewPrincipal
{
    private static final String TEXTBOX_PREFIX = "alias-appid-";

    private final Logger logger = Logger.getLogger(this.getClass());

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            processGeneral();
            processDirectoryMapping();

            Map<String, String[]> requestParams = ServletActionContext.getRequest().getParameterMap();

            for (Map.Entry<String, String[]> entry : requestParams.entrySet())
            {
                if (entry.getKey().startsWith(TEXTBOX_PREFIX))
                {
                    String appId = entry.getKey().substring(TEXTBOX_PREFIX.length());
                    Long id = new Long(appId);

                    try
                    {
                        Application application = applicationManager.findById(id);

                        String username = user.getName();
                        String alias = null;
                        if (entry.getValue() != null && entry.getValue().length > 0)
                        {
                            alias = entry.getValue()[0];
                        }

                        if (StringUtils.isBlank(alias))
                        {
                            aliasManager.removeAlias(application, username);
                        }
                        else if (username.equals(alias))
                        {
                            applicationInError.put(id, Boolean.TRUE);
                            addActionError(getText("principal.alias.primary.invalid", Arrays.asList(username, application.getName())));
                        }
                        else
                        {
                            aliasManager.storeAlias(application, username, alias);
                        }

                    }
                    catch (AliasAlreadyInUseException e)
                    {
                        logger.error(e.getMessage(), e);
                        applicationInError.put(id, Boolean.TRUE);
                        addActionError(e);
                    }
                    catch (ApplicationNotFoundException e)
                    {
                        logger.error(e.getMessage(), e);
                        addActionError("Could not find application with id: " + id);
                    }
                }
            }

            if (hasErrors())
            {
                // process again so any valid updates are reflected when page is displayed
                processGeneral();
                processDirectoryMapping();
                return INPUT;
            }
            else
            {
                return SUCCESS;
            }
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);

            return INPUT;
        }

    }
}
