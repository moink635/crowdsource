package com.atlassian.crowd.console.tags;

import com.opensymphony.util.TextUtils;
import com.opensymphony.xwork.util.OgnlValueStack;

import java.util.ArrayList;

/**
 * Extended version of the webwork {@link com.opensymphony.webwork.components.Text} component that allows
 * the encoding of {@link java.lang.String} based parameters.
 * <p/>
 * HTML encoding is done using the {@link com.opensymphony.util.TextUtils#htmlEncode(String)} to maintain compatibility
 * with other WebWork tags.
 */
public class Text extends com.opensymphony.webwork.components.Text
{
    private boolean escape = true;

    public Text(OgnlValueStack stack)
    {
        super(stack);
    }

    public void addParameter(Object value)
    {
        if (values.isEmpty())
        {
            values = new ArrayList(4);
        }

        if (escape && value instanceof String)
        {

            values.add(TextUtils.htmlEncode((String) value));
        }
        else
        {
            values.add(value);
        }
    }


    /**
     * Defines whether or not parameters should be HTML encoded
     *
     * @param escape
     */
    public void setEscape(boolean escape)
    {
        this.escape = escape;
    }
}