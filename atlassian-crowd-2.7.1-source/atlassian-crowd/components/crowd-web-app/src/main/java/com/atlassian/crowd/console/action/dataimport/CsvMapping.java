package com.atlassian.crowd.console.action.dataimport;

import com.atlassian.core.util.PairType;
import com.atlassian.crowd.importer.config.CsvConfiguration;

import com.google.common.collect.ImmutableList;
import com.opensymphony.webwork.interceptor.SessionAware;
import com.opensymphony.xwork.ActionContext;
import org.apache.commons.collections.OrderedBidiMap;
import org.apache.commons.collections.OrderedMap;
import org.apache.commons.collections.bidimap.TreeBidiMap;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Mapping action for the CSV importer
 */
public class CsvMapping extends BaseImporter implements SessionAware
{
    private Map session = null;

    private CsvConfiguration configuration = null;

    public String doDefault() throws Exception
    {
        configuration = (CsvConfiguration) session.get(BaseImporter.IMPORTER_CONFIGURATION);
        return INPUT;
    }

    public String doExecute() throws Exception
    {
        Map parameters = ActionContext.getContext().getParameters();

        if (parameters != null)
        {
            OrderedBidiMap principalConfigMapping = buildMappings(parameters, CsvConfiguration.USER_PREFIX);
            if (principalConfigMapping != null && !principalConfigMapping.isEmpty())
            {
                getConfiguration().setUserMappingConfiguration(principalConfigMapping);
            }

            if (getConfiguration().getGroupMemberships() != null)
            {
                OrderedBidiMap groupConfigMapping = buildMappings(parameters, CsvConfiguration.GROUP_PREFIX);
                if (groupConfigMapping != null && !groupConfigMapping.isEmpty())
                {
                    getConfiguration().setGroupMappingConfiguration(groupConfigMapping);
                }
            }
        }

        // Validate that we have all the required attributes mapped (first name, last name, email, username etc.)
        doValidation();

        if (hasErrors())
        {
            return ERROR;
        }

        // Place the configuration back in the session
        session.put(BaseImporter.IMPORTER_CONFIGURATION, getConfiguration());

        return SUCCESS;
    }

    private void doValidation()
    {
        // Check that the required Principal attributes have been set
        OrderedMap principalMappings = getConfiguration().getUserMappingConfiguration();
        validateProperty(principalMappings, "dataimport.csvmapping.missing.user.mapping.error", CsvConfiguration.USER_FIRSTNAME);
        validateProperty(principalMappings, "dataimport.csvmapping.missing.user.mapping.error", CsvConfiguration.USER_LASTNAME);
        validateProperty(principalMappings, "dataimport.csvmapping.missing.user.mapping.error", CsvConfiguration.USER_EMAILADDRESS);
        validateProperty(principalMappings, "dataimport.csvmapping.missing.user.mapping.error", CsvConfiguration.USER_USERNAME);

        // Check that the required Group Mapping attributes have been set if present
        if (getConfiguration().getGroupMemberships() != null)
        {
            OrderedMap groupMappings = getConfiguration().getGroupMappingConfiguration();
            validateProperty(groupMappings, "dataimport.csvmapping.missing.group.mapping.error", CsvConfiguration.GROUP_NAME);
            validateProperty(groupMappings, "dataimport.csvmapping.missing.group.mapping.error", CsvConfiguration.GROUP_USERNAME);
        }
    }

    private void validateProperty(OrderedMap mappings, String errorMessageKey, String propertyKey)
    {
        if (mappings == null || !mappings.containsValue(propertyKey))
        {
            addActionError(getText(errorMessageKey, ImmutableList.of(getText(propertyKey))));
        }
    }

    private OrderedBidiMap buildMappings(Map parameters, String parameterPrefix)
    {
        // The config mapping to return
        OrderedBidiMap configMapping = new TreeBidiMap();

        Set paramaterKeys = new TreeSet();
        paramaterKeys.addAll(parameters.keySet());

        // Iterate over the ordered parameter keys
        for (Iterator iterator = paramaterKeys.iterator(); iterator.hasNext();)
        {
            String parameter = (String) iterator.next();

            // If the mapping starts with the required prefix, add this to the mapping
            if (parameter.startsWith(parameterPrefix))
            {
                String[] value = (String[]) parameters.get(parameter);
                if (value != null && value.length > 0 && StringUtils.isNotBlank(value[0]))
                {
                    configMapping.put(parameter, value[0]);
                }
            }
        }

        return configMapping;
    }

    public void setSession(Map session)
    {
        this.session = session;
    }

    public CsvConfiguration getConfiguration()
    {
        if (configuration == null)
        {
            configuration = (CsvConfiguration) session.get(BaseImporter.IMPORTER_CONFIGURATION);
        }
        return configuration;
    }

    public List<PairType> getUserMappingOptions()
    {
        return Arrays.asList(
                new PairType(CsvConfiguration.USER_NONE, getText(CsvConfiguration.USER_NONE)),
                new PairType(CsvConfiguration.USER_FIRSTNAME, getText(CsvConfiguration.USER_FIRSTNAME)),
                new PairType(CsvConfiguration.USER_LASTNAME, getText(CsvConfiguration.USER_LASTNAME)),
                new PairType(CsvConfiguration.USER_EMAILADDRESS, getText(CsvConfiguration.USER_EMAILADDRESS)),
                new PairType(CsvConfiguration.USER_USERNAME, getText(CsvConfiguration.USER_USERNAME)),
                new PairType(CsvConfiguration.USER_PASSWORD, getText(CsvConfiguration.USER_PASSWORD))
        );
    }

    public List<PairType> getGroupMappingOptions()
    {
        return Arrays.asList(
                new PairType(CsvConfiguration.GROUP_NONE, getText(CsvConfiguration.GROUP_NONE)),
                new PairType(CsvConfiguration.GROUP_NAME, getText(CsvConfiguration.GROUP_NAME)),
                new PairType(CsvConfiguration.GROUP_USERNAME, getText(CsvConfiguration.GROUP_USERNAME))
        );
    }

    public boolean isGroupMappingSelected(String key, String value)
    {
        boolean selected = false;
        OrderedMap groupMappings = getConfiguration().getGroupMappingConfiguration();
        if (groupMappings != null)
        {
            selected = groupMappings.containsKey(key);
            if (selected)
            {
                selected = groupMappings.get(key).equals(value);
            }
        }

        return selected;
    }

    public boolean isUserMappingSelected(String key, String value)
    {
        boolean selected = false;
        OrderedMap principalMappings = getConfiguration().getUserMappingConfiguration();
        if (principalMappings != null)
        {
            selected = principalMappings.containsKey(key);
            if (selected)
            {
                selected = principalMappings.get(key).equals(value);
            }
        }

        return selected;
    }
}
