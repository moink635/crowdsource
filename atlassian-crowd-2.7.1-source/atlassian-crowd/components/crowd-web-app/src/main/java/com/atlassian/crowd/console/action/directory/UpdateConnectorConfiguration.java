package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.validator.ConnectorValidator;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.Set;

import com.google.common.annotations.VisibleForTesting;

/**
 * Action to handle updating configuration details for a 'Connector' based directory.
 */
public class UpdateConnectorConfiguration extends BaseAction implements LDAPConfiguration
{
    private Directory directory;
    protected long ID = -1;

    private String groupObjectFilter;
    private String groupDNaddition;
    private String groupDescriptionAttr;
    private String groupMemberAttr;
    private String groupNameAttr;
    private String groupObjectClass;

    private String userObjectFilter;
    private String userDNaddition;
    private String userObjectClass;
    private String userGroupMemberAttr;
    private String userFirstnameAttr;
    private String userLastnameAttr;
    private String userDisplayNameAttr;
    private String userMailAttr;
    private String userNameAttr;
    private String userNameRdnAttr;
    private String userPasswordAttr;
    private String userExternalIdAttr;

    private ConnectorValidator connectorValidator;

    private LDAPConfigurationTester ldapConfigurationTester;

    public String execute()
    {
        // Group attributes
        groupObjectFilter = getDirectory().getValue(LDAPPropertiesMapper.GROUP_OBJECTFILTER_KEY);
        groupDNaddition = getDirectory().getValue(LDAPPropertiesMapper.GROUP_DN_ADDITION);
        groupDescriptionAttr = getDirectory().getValue(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY);
        groupNameAttr = getDirectory().getValue(LDAPPropertiesMapper.GROUP_NAME_KEY);
        groupObjectClass = getDirectory().getValue(LDAPPropertiesMapper.GROUP_OBJECTCLASS_KEY);
        groupMemberAttr = getDirectory().getValue(LDAPPropertiesMapper.GROUP_USERNAMES_KEY);

        // User attributes
        userObjectFilter = getDirectory().getValue(LDAPPropertiesMapper.USER_OBJECTFILTER_KEY);
        userDNaddition = getDirectory().getValue(LDAPPropertiesMapper.USER_DN_ADDITION);
        userObjectClass = getDirectory().getValue(LDAPPropertiesMapper.USER_OBJECTCLASS_KEY);
        userGroupMemberAttr = getDirectory().getValue(LDAPPropertiesMapper.USER_GROUP_KEY);
        userFirstnameAttr = getDirectory().getValue(LDAPPropertiesMapper.USER_FIRSTNAME_KEY);
        userLastnameAttr = getDirectory().getValue(LDAPPropertiesMapper.USER_LASTNAME_KEY);
        userDisplayNameAttr = getDirectory().getValue(LDAPPropertiesMapper.USER_DISPLAYNAME_KEY);
        userMailAttr = getDirectory().getValue(LDAPPropertiesMapper.USER_EMAIL_KEY);
        userNameAttr = getDirectory().getValue(LDAPPropertiesMapper.USER_USERNAME_KEY);
        userNameRdnAttr = getDirectory().getValue(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY);
        userPasswordAttr = getDirectory().getValue(LDAPPropertiesMapper.USER_PASSWORD_KEY);
        userExternalIdAttr = getDirectory().getValue(LDAPPropertiesMapper.LDAP_EXTERNAL_ID);

        // Check for errors in configuration on display of configuration
        // If an error is detected, doValidation to display error messages for specific fields
        Set<String> directoryConfigurationErrors = connectorValidator.getErrors(getDirectory());
        if (!directoryConfigurationErrors.isEmpty())
        {
            for (String error : directoryConfigurationErrors)
            {
                addActionError(error);
            }
            doValidation();
        }

        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        doValidation();

        if (hasErrors())
        {
            addActionError(getText("directoryconnector.update.invalid"));
            return ERROR;
        }

        try
        {
            DirectoryImpl updateDirectory = new DirectoryImpl(getDirectory());

            updateDirectory.setAttribute(LDAPPropertiesMapper.GROUP_DN_ADDITION, groupDNaddition);
            updateDirectory.setAttribute(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY, groupDescriptionAttr);
            updateDirectory.setAttribute(LDAPPropertiesMapper.GROUP_NAME_KEY, groupNameAttr);
            updateDirectory.setAttribute(LDAPPropertiesMapper.GROUP_OBJECTCLASS_KEY, groupObjectClass);
            updateDirectory.setAttribute(LDAPPropertiesMapper.GROUP_OBJECTFILTER_KEY, groupObjectFilter);
            updateDirectory.setAttribute(LDAPPropertiesMapper.GROUP_USERNAMES_KEY, groupMemberAttr);

            updateDirectory.setAttribute(LDAPPropertiesMapper.USER_DN_ADDITION, userDNaddition);
            updateDirectory.setAttribute(LDAPPropertiesMapper.USER_EMAIL_KEY, userMailAttr);
            updateDirectory.setAttribute(LDAPPropertiesMapper.USER_FIRSTNAME_KEY, userFirstnameAttr);
            updateDirectory.setAttribute(LDAPPropertiesMapper.USER_GROUP_KEY, userGroupMemberAttr);
            updateDirectory.setAttribute(LDAPPropertiesMapper.USER_LASTNAME_KEY, userLastnameAttr);
            updateDirectory.setAttribute(LDAPPropertiesMapper.USER_DISPLAYNAME_KEY, userDisplayNameAttr);
            updateDirectory.setAttribute(LDAPPropertiesMapper.USER_OBJECTCLASS_KEY, userObjectClass);
            updateDirectory.setAttribute(LDAPPropertiesMapper.USER_OBJECTFILTER_KEY, userObjectFilter);
            updateDirectory.setAttribute(LDAPPropertiesMapper.USER_USERNAME_KEY, userNameAttr);
            updateDirectory.setAttribute(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY, userNameRdnAttr);
            updateDirectory.setAttribute(LDAPPropertiesMapper.USER_PASSWORD_KEY, userPasswordAttr);
            updateDirectory.setAttribute(LDAPPropertiesMapper.LDAP_EXTERNAL_ID, userExternalIdAttr);

            // Check for further errors in the configuration of the directory
            Set<String> directoryConfigurationErrors = connectorValidator.getErrors(getDirectory());
            if (!directoryConfigurationErrors.isEmpty())
            {
                for (String error : directoryConfigurationErrors)
                {
                    addActionError(error);
                }
                return ERROR;
            }

            directoryManager.updateDirectory(updateDirectory);

            return SUCCESS;
        } catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
            return ERROR;
        }
    }

    public String doTestUpdatePrincipalSearch()
    {
        return doTestSearch(this, LDAPConfigurationTester.Strategy.USER);
    }

    public String doTestUpdateGroupSearch()
    {
        return doTestSearch(this, LDAPConfigurationTester.Strategy.GROUP);
    }

    @VisibleForTesting
    final String doTestSearch(final LDAPConfiguration configuration, final LDAPConfigurationTester.Strategy strategy)
    {
        try
        {
            if (ldapConfigurationTester.canFindLdapObjects(configuration, strategy))
            {
                actionMessageAlertColor = ALERT_BLUE;
                addActionMessage(getText("directoryconnector.testsearch.success"));
            } else
            {
                addActionError(getText("directoryconnector.testsearch.invalid"));
            }
        } catch (final Exception exception)
        {
            addActionError(getText("directoryconnector.testsearch.invalid") + " " + exception.getMessage());
            logger.error(exception.getMessage(), exception);
        }
        return SUCCESS;
    }

    @Override
    public void populateDirectoryAttributesForConnectionTest(final Map<String, String> attributes)
    {
        attributes.put(LDAPPropertiesMapper.LDAP_URL_KEY, getDirectory().getValue(LDAPPropertiesMapper.LDAP_URL_KEY));
        attributes.put(LDAPPropertiesMapper.LDAP_BASEDN_KEY, getDirectory().getValue(LDAPPropertiesMapper.LDAP_BASEDN_KEY));
        attributes.put(LDAPPropertiesMapper.LDAP_USERDN_KEY, getDirectory().getValue(LDAPPropertiesMapper.LDAP_USERDN_KEY));
        attributes.put(LDAPPropertiesMapper.LDAP_PASSWORD_KEY, getDirectory().getValue(LDAPPropertiesMapper.LDAP_PASSWORD_KEY));
        attributes.put(LDAPPropertiesMapper.LDAP_SECURE_KEY, getDirectory().getValue(LDAPPropertiesMapper.LDAP_SECURE_KEY));
        attributes.put(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY, getDirectory().getValue(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY));
        attributes.put(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE, getDirectory().getValue(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE));
        attributes.put(LDAPPropertiesMapper.LDAP_REFERRAL_KEY, getDirectory().getValue(LDAPPropertiesMapper.LDAP_REFERRAL_KEY));

        attributes.put(LDAPPropertiesMapper.USER_OBJECTFILTER_KEY, getUserObjectFilter());
        attributes.put(LDAPPropertiesMapper.USER_DN_ADDITION, getUserDNaddition());
        attributes.put(LDAPPropertiesMapper.USER_OBJECTCLASS_KEY, getUserObjectClass());
        attributes.put(LDAPPropertiesMapper.USER_GROUP_KEY, getUserGroupMemberAttr());
        attributes.put(LDAPPropertiesMapper.USER_FIRSTNAME_KEY, getUserFirstnameAttr());
        attributes.put(LDAPPropertiesMapper.USER_LASTNAME_KEY, getUserLastnameAttr());
        attributes.put(LDAPPropertiesMapper.USER_DISPLAYNAME_KEY, getUserDisplayNameAttr());
        attributes.put(LDAPPropertiesMapper.USER_EMAIL_KEY, getUserMailAttr());
        attributes.put(LDAPPropertiesMapper.USER_USERNAME_KEY, getUserNameAttr());
        attributes.put(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY, getUserNameRdnAttr());
        attributes.put(LDAPPropertiesMapper.USER_PASSWORD_KEY, getUserPasswordAttr());
        attributes.put(LDAPPropertiesMapper.LDAP_EXTERNAL_ID, getUserExternalIdAttr());

        attributes.put(LDAPPropertiesMapper.GROUP_OBJECTFILTER_KEY, getGroupObjectFilter());
        attributes.put(LDAPPropertiesMapper.GROUP_DN_ADDITION, getGroupDNaddition());
        attributes.put(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY, getGroupDescriptionAttr());
        attributes.put(LDAPPropertiesMapper.GROUP_USERNAMES_KEY, getGroupMemberAttr());
        attributes.put(LDAPPropertiesMapper.GROUP_NAME_KEY, getGroupNameAttr());
        attributes.put(LDAPPropertiesMapper.GROUP_OBJECTCLASS_KEY, getGroupObjectClass());
    }

    private void doValidation()
    {
        validateUserConfiguration();

        if (isShowGroupsConfiguration())
        {
            validateGroupConfiguration();
        }
    }

    protected void validateUserConfiguration()
    {
        if (StringUtils.isEmpty(userFirstnameAttr))
        {
            addFieldError("userFirstnameAttr", getText("directoryconnector.userfirstnameattribute.invalid"));
        }

        if (StringUtils.isEmpty(userGroupMemberAttr))
        {
            addFieldError("userGroupMemberAttr", getText("directoryconnector.usermemberofattribute.invalid"));
        }

        if (StringUtils.isEmpty(userLastnameAttr))
        {
            addFieldError("userLastnameAttr", getText("directoryconnector.userlastnameattribute.invalid"));
        }

        if (StringUtils.isEmpty(userDisplayNameAttr))
        {
            addFieldError("userDisplayNameAttr", getText("directoryconnector.userdisplaynameattribute.invalid"));
        }

        if (StringUtils.isEmpty(userMailAttr))
        {
            addFieldError("userMailAttr", getText("directoryconnector.usermailattribute.invalid"));
        }

        if (StringUtils.isEmpty(userNameAttr))
        {
            addFieldError("userNameAttr", getText("directoryconnector.usernameattribute.invalid"));
        }

        if (StringUtils.isEmpty(userNameRdnAttr))
        {
            addFieldError("userNameRdnAttr", getText("directoryconnector.usernamerdnattribute.invalid"));
        }

        if (StringUtils.isEmpty(userObjectClass))
        {
            addFieldError("userObjectClass", getText("directoryconnector.userobjectclass.invalid"));
        }

        if (StringUtils.isEmpty(userObjectFilter))
        {
            addFieldError("userObjectFilter", getText("directoryconnector.userobjectfilter.invalid"));
        }

        validateUserPasswordAttr();
    }

    void validateUserPasswordAttr()
    {
        if (StringUtils.isBlank(userPasswordAttr))
        {
            addFieldError("userPasswordAttr", getText("directoryconnector.userpassword.invalid"));
        }
    }

    protected void validateGroupConfiguration()
    {
        if (StringUtils.isEmpty(groupDescriptionAttr))
        {
            addFieldError("groupDescriptionAttr", getText("directoryconnector.groupdescription.invalid"));
        }

        if (StringUtils.isEmpty(groupMemberAttr))
        {
            addFieldError("groupMemberAttr", getText("directoryconnector.groupmember.invalid"));
        }

        if (StringUtils.isEmpty(groupNameAttr))
        {
            addFieldError("groupNameAttr", getText("directoryconnector.groupname.invalid"));
        }

        if (StringUtils.isEmpty(groupObjectClass))
        {
            addFieldError("groupObjectClass", getText("directoryconnector.groupobjectclass.invalid"));
        }

        if (StringUtils.isEmpty(groupObjectFilter))
        {
            addFieldError("groupObjectFilter", getText("directoryconnector.groupobjectfilter.invalid"));
        }
    }

    public long getID()
    {
        return ID;
    }

    public void setID(long ID)
    {
        this.ID = ID;
    }

    public Directory getDirectory()
    {
        if (directory == null && ID != -1)
        {
            try
            {
                directory = directoryManager.findDirectoryById(ID);
            } catch (DirectoryNotFoundException e)
            {
                addActionError(e);
            }
        }

        return directory;
    }

    @Override
    public String getGroupObjectFilter()
    {
        return groupObjectFilter;
    }

    public void setGroupObjectFilter(String groupObjectFilter)
    {
        this.groupObjectFilter = groupObjectFilter;
    }

    @Override
    public String getGroupDNaddition()
    {
        return groupDNaddition;
    }

    public void setGroupDNaddition(String groupDNaddition)
    {
        this.groupDNaddition = groupDNaddition;
    }

    @Override
    public String getGroupDescriptionAttr()
    {
        return groupDescriptionAttr;
    }

    public void setGroupDescriptionAttr(String groupDescriptionAttr)
    {
        this.groupDescriptionAttr = groupDescriptionAttr;
    }

    @Override
    public String getGroupMemberAttr()
    {
        return groupMemberAttr;
    }

    public void setGroupMemberAttr(String groupMemberAttr)
    {
        this.groupMemberAttr = groupMemberAttr;
    }

    @Override
    public String getGroupNameAttr()
    {
        return groupNameAttr;
    }

    public void setGroupNameAttr(String groupNameAttr)
    {
        this.groupNameAttr = groupNameAttr;
    }

    @Override
    public String getGroupObjectClass()
    {
        return groupObjectClass;
    }

    public void setGroupObjectClass(String groupObjectClass)
    {
        this.groupObjectClass = groupObjectClass;
    }

    @Override
    public String getUserObjectFilter()
    {
        return userObjectFilter;
    }

    public void setUserObjectFilter(String userObjectFilter)
    {
        this.userObjectFilter = userObjectFilter;
    }

    @Override
    public String getUserDNaddition()
    {
        return userDNaddition;
    }

    public void setUserDNaddition(String userDNaddition)
    {
        this.userDNaddition = userDNaddition;
    }

    @Override
    public String getUserObjectClass()
    {
        return userObjectClass;
    }

    public void setUserObjectClass(String userObjectClass)
    {
        this.userObjectClass = userObjectClass;
    }

    @Override
    public String getUserGroupMemberAttr()
    {
        return userGroupMemberAttr;
    }

    public void setUserGroupMemberAttr(String userGroupMemberAttr)
    {
        this.userGroupMemberAttr = userGroupMemberAttr;
    }

    @Override
    public String getUserFirstnameAttr()
    {
        return userFirstnameAttr;
    }

    public void setUserFirstnameAttr(String userFirstnameAttr)
    {
        this.userFirstnameAttr = userFirstnameAttr;
    }

    @Override
    public String getUserLastnameAttr()
    {
        return userLastnameAttr;
    }

    public void setUserLastnameAttr(String userLastnameAttr)
    {
        this.userLastnameAttr = userLastnameAttr;
    }

    @Override
    public String getUserMailAttr()
    {
        return userMailAttr;
    }

    public void setUserMailAttr(String userMailAttr)
    {
        this.userMailAttr = userMailAttr;
    }

    @Override
    public String getUserNameAttr()
    {
        return userNameAttr;
    }

    public void setUserNameAttr(String userNameAttr)
    {
        this.userNameAttr = userNameAttr;
    }

    @Override
    public String getUserNameRdnAttr()
    {
        return userNameRdnAttr;
    }

    public void setUserNameRdnAttr(String userNameRdnAttr)
    {
        this.userNameRdnAttr = userNameRdnAttr;
    }

    @Override
    public String getUserPasswordAttr()
    {
        return userPasswordAttr;
    }

    public void setUserPasswordAttr(String userPasswordAttr)
    {
        this.userPasswordAttr = userPasswordAttr;
    }

    @Override
    public String getUserExternalIdAttr()
    {
        return userExternalIdAttr;
    }

    public void setUserExternalIdAttr(String userExternalIdAttr)
    {
        this.userExternalIdAttr = userExternalIdAttr;
    }

    @Override
    public String getUserDisplayNameAttr()
    {
        return userDisplayNameAttr;
    }

    public void setUserDisplayNameAttr(String userDisplayNameAttr)
    {
        this.userDisplayNameAttr = userDisplayNameAttr;
    }

    public void setConnectorValidator(ConnectorValidator connectorValidator)
    {
        this.connectorValidator = connectorValidator;
    }

    public boolean isShowGroupsConfiguration()
    {
        return true;
    }

    public void setLdapConfigurationTester(LDAPConfigurationTester ldapConfigurationTester)
    {
        this.ldapConfigurationTester = ldapConfigurationTester;
    }

    @Override
    public Long getId()
    {
        return getDirectory().getId();
    }

    @Override
    public String getImplementationClass()
    {
        return getDirectory().getImplementationClass();
    }
}
