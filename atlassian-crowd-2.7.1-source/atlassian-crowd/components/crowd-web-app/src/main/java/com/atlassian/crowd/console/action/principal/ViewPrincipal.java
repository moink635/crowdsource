package com.atlassian.crowd.console.action.principal;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.application.AliasManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.manager.permission.PermissionManager;
import com.atlassian.crowd.model.EntityComparator;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.AdminGroupChecker;
import com.atlassian.crowd.util.UserUtils;

import com.google.common.collect.Sets;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import java.rmi.RemoteException;
import java.util.*;

public class ViewPrincipal extends BaseAction
{
    private static final Logger logger = Logger.getLogger(ViewPrincipal.class);

    protected ApplicationService applicationService;
    protected TokenAuthenticationManager tokenAuthenticationManager;
    protected AliasManager aliasManager;
    protected PermissionManager permissionManager;
    protected AdminGroupChecker adminGroupChecker;
    private DirectoryInstanceLoader directoryInstanceLoader;

    protected User user;
    protected Map<String, Set<String>> userAttributes;
    protected Directory directory;
    protected long directoryID = -1;
    protected String directoryName;

    protected String name;
    protected String newName;
    protected String password;
    protected String passwordConfirm;
    protected String firstname;
    protected String lastname;
    protected String email;
    protected boolean active;
    protected String externalId;

    Set<String> allGroups;

    Set<String> unsubscribedGroupNames;
    Set<String> subscribedGroupNames;

    Set<Group> subscribedGroups;

    protected List<Application> applications;
    protected List<String> aliases;
    protected Map<Long, Boolean> applicationInError = new HashMap<Long, Boolean>();

    protected String unremovedGroups = "";
    protected boolean preventingLockout;

    @Override
    public String doDefault()
    {
        try
        {
            processGeneral();
            processDirectoryMapping();
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
        }

        return SUCCESS;
    }

    public boolean hasUpdateGroupPermission()
    {
        try
        {
            return permissionManager.hasPermission(directoryManager.findDirectoryById(directoryID), OperationType.UPDATE_GROUP);
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return false;
        }
    }

    private void processMemberships() throws RemoteException, OperationFailedException, DirectoryNotFoundException
    {
        // initalize the available groups & roles
        setAllDirectoryGroups();

        // get the principals group memberships
        subscribedGroupNames = new TreeSet<String>();
        subscribedGroups = new TreeSet<Group>(EntityComparator.of(Group.class));

        List<Group> groups = directoryManager.searchDirectGroupRelationships(directoryID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(name).returningAtMost(EntityQuery.ALL_RESULTS));

        for (Group group : groups)
        {
            if (GroupType.GROUP.equals(group.getType()))
            {
                subscribedGroupNames.add(group.getName());
                subscribedGroups.add(group);
            }
        }

        unsubscribedGroupNames = Sets.difference(allGroups, subscribedGroupNames);
    }

    private void processApplications()
    {
        try
        {
            applications = tokenAuthenticationManager.findAuthorisedApplications(user, clientProperties.getApplicationName());
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
            applications = Collections.emptyList();
        }

        aliases = new ArrayList<String>();
        for (Application application : applications)
        {
            String alias = aliasManager.findAliasByUsername(application, user.getName());
            if (StringUtils.isBlank(alias) || alias.equals(user.getName()))
            {
                alias = "";
            }
            aliases.add(alias);
        }
    }

    protected void processGeneral()
    {
        try
        {
            if (StringUtils.isNotBlank(directoryName) && directoryID == -1)
            {
                directoryID = directoryManager.findDirectoryByName(directoryName).getId();
            }

            UserWithAttributes uwa = directoryManager.findUserWithAttributesByName(directoryID, name);
            userAttributes = readAttributes(uwa);
            user = uwa;

            syncFieldsFromPrincipal();

            processMemberships();
            processApplications();
        }
        catch (Exception e)
        {
            addActionError(e);
        }
    }

    protected Map<String, Set<String>> readAttributes(UserWithAttributes userWithAttributes)
    {
        Map<String, Set<String>> newAttributes = new TreeMap<String, Set<String>>();
        for (String attributeName : userWithAttributes.getKeys())
        {
            newAttributes.put(attributeName, new HashSet<String>(userWithAttributes.getValues(attributeName)));
        }
        return newAttributes;
    }

    /**
     * Copies data from class fields to the <code>user</code>
     */
    protected void syncFieldsToPrincipal()
    {
        UserTemplateWithAttributes userTemplate = new UserTemplateWithAttributes(getName(), getDirectoryID());
        userTemplate.setActive(isActive());
        userTemplate.setEmailAddress(getEmail());
        userTemplate.setFirstName(getFirstname());
        userTemplate.setLastName(getLastname());
        user = userTemplate;
    }

    /**
     * Copies data from <code>user</code> to the various fields (eg <code>name</code>).
     */
    protected void syncFieldsFromPrincipal()
    {
        if (user != null)
        {
            setName(user.getName());
            setNewName(user.getName());
            setActive(user.isActive());

            if (user.getFirstName() != null)
            {
                setFirstname(user.getFirstName());
            }
            if (user.getLastName() != null)
            {
                setLastname(user.getLastName());
            }
            if (user.getEmailAddress() != null)
            {
                setEmail(user.getEmailAddress());
            }
            this.externalId = user.getExternalId();
        }
    }

    private void setAllDirectoryGroups()
            throws RemoteException, OperationFailedException, DirectoryNotFoundException
    {
        if (allGroups == null)
        {
            allGroups = new TreeSet<String>(directoryManager.searchGroups(directoryID, QueryBuilder.queryFor(String.class, EntityDescriptor.group()).returningAtMost(EntityQuery.ALL_RESULTS)));
        }
    }

    protected void processDirectoryMapping()
    {
        try
        {
            directory = directoryManager.findDirectoryById(directoryID);
        }
        catch (DirectoryNotFoundException e)
        {
            addActionError(e);
        }
    }

    public User getUser()
    {
        return user;
    }

    public Directory getDirectory()
    {
        return directory;
    }

    public long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(long directoryID)
    {
        this.directoryID = directoryID;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPasswordConfirm()
    {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm)
    {
        this.passwordConfirm = passwordConfirm;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public void setFirstname(String firstname)
    {
        this.firstname = firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public void setLastname(String lastname)
    {
        this.lastname = lastname;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public boolean isActive()
    {
        return active;
    }

    public String getExternalId()
    {
        return externalId;
    }

    public void setActive(boolean active)
    {
        this.active = active;
    }

    public Set getGroups()
    {
        return subscribedGroups;
    }

    public Set getUnsubscribedGroups()
    {
        return unsubscribedGroupNames;
    }

    public Map getUserAttributes()
    {
        return userAttributes;
    }

    public String getDirectoryName()
    {
        return directoryName;
    }

    public List<Application> getApplications()
    {
        return applications;
    }

    public List<String> getAliases()
    {
        // these aliases are index-aligned with applications
        return aliases;
    }

    public void setDirectoryName(String directoryName)
    {
        this.directoryName = directoryName;
    }

    // called by subclasses that need to perform mutations (add/update)
    protected void doValidation()
    {
        if (StringUtils.isNotBlank(password))
        {

            if (StringUtils.isBlank(passwordConfirm))
            {
                addFieldError("password", getText("principal.password.invalid"));
                addFieldError("confirmPassword", "");
            }
            else if (!password.equals(passwordConfirm))
            {
                addFieldError("password", getText("principal.passwordconfirm.nomatch"));
                addFieldError("confirmPassword", "");
            }
        }

        if (StringUtils.isBlank(email))
        {
            addFieldError("email", getText("principal.email.invalid"));
        }
        else if (!UserUtils.isValidEmail(email))
        {
            addFieldError("email", getText("principal.email.invalid"));
        }
        else if (IdentifierUtils.hasLeadingOrTrailingWhitespace(email))
        {
            addFieldError("email", getText("principal.email.whitespace"));
        }

        if (StringUtils.isBlank(firstname))
        {
            addFieldError("firstname", getText("principal.firstname.invalid"));
        }

        if (StringUtils.isBlank(lastname))
        {
            addFieldError("lastname", getText("principal.lastname.invalid"));
        }

    }

    public void setApplicationService(final ApplicationService applicationService)
    {
        this.applicationService = applicationService;
    }

    public void setAliasManager(final AliasManager aliasManager)
    {
        this.aliasManager = aliasManager;
    }

    public Map<Long, Boolean> getApplicationInError()
    {
        return applicationInError;
    }

    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public void setTokenAuthenticationManager(TokenAuthenticationManager tokenAuthenticationManager)
    {
        this.tokenAuthenticationManager = tokenAuthenticationManager;
    }

    public AdminGroupChecker getAdminGroupChecker()
    {
        return adminGroupChecker;
    }

    public void setAdminGroupChecker(AdminGroupChecker adminGroupChecker)
    {
        this.adminGroupChecker = adminGroupChecker;
    }

    public void setDirectoryInstanceLoader(DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }

    public boolean isPreventingLockout()
    {
        return preventingLockout;
    }

    public void setPreventingLockout(boolean preventingLockout)
    {
        this.preventingLockout = preventingLockout;
    }

    public String getUnremovedGroups()
    {
        return unremovedGroups;
    }

    public void setUnremovedGroups(String unremovedGroups)
    {
        this.unremovedGroups = unremovedGroups;
    }

    public String getNewName()
    {
        return newName;
    }

    public void setNewName(String newName)
    {
        this.newName = newName;
    }

    public String getDirectoryImplementationDescriptiveName()
    {
        try
        {
            return directoryInstanceLoader.getDirectory(directory).getDescriptiveName();
        }
        catch (DirectoryInstantiationException e)
        {
            return null;
        }
    }
}
