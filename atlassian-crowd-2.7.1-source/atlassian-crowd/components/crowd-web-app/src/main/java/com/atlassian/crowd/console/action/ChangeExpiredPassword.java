package com.atlassian.crowd.console.action;


import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.user.User;

import com.google.common.base.Objects;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class ChangeExpiredPassword extends BaseAction
{
    private static final Logger logger = LoggerFactory.getLogger(ChangeExpiredPassword.class);

    private String originalPassword;
    private String password;
    private String confirmPassword;
    private String passwordComplexityMessage;
    private String username;

    @Override
    public String doDefault() throws Exception
    {
        // get the username from the session just once to avoid race conditions
        initUsernameFromSession();

        addPasswordComplexityMessage();

        Exception exception = (Exception) getSession().getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        if (exception instanceof CredentialsExpiredException)
        {
            getSession().removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
            addActionError(getText("error.changepassword.required"));

            return INPUT;
        }

        return LOGIN;
    }


    public String doUpdate() throws Exception
    {
        // get the username from the session just once to avoid race conditions
        initUsernameFromSession();

        // If there is no username in session, redirect back to login page.
        if (StringUtils.isBlank(username))
        {
            return LOGIN;
        }
        addPasswordComplexityMessage();
        // check input to be there
        doValidation();

        if (hasErrors() || hasActionErrors())
        {
            addActionError(getText("error.changepassword.required"));
            return INPUT;
        }

        Application crowdApplication;
        try
        {
            crowdApplication = getCrowdApplication();
        }
        catch (ApplicationNotFoundException e)
        {
            logger.error("Crowd is misconfigured. Cannot find the Crowd application '{}'", e.getApplicationName(), e);
            return ERROR;
        }

        // do they know their original password?
        try
        {
            applicationService.authenticateUser(crowdApplication, username,
                                                new PasswordCredential(originalPassword));
        }
        catch (ExpiredCredentialException e)
        {
            // the only exception we'll let through - since they're resetting their password precisely because it has expired!
        }
        catch (Exception e)
        {
            logger.debug("When verifying the original password", e);

            addActionError(getText("error.changepassword.required"));
            addFieldError("originalPassword", getText("password.invalid"));

            return INPUT;
        }

        // update their password
        try
        {
            applicationService.updateUserCredential(crowdApplication, username, new PasswordCredential(password));

            addActionMessage(ALERT_BLUE, getText("passwordupdate.message"));

            return SUCCESS;
        }
        catch (InvalidCredentialException e)
        {
            addActionError(getText("passwordupdate.policy.error.message"));
            addFieldError("password", Objects.firstNonNull(e.getPolicyDescription(), e.getMessage()));
        }
        catch (Exception e)
        {
            logger.error("Failed to update password", e);

            addActionError(getText("passwordupdate.error.message"));
        }

        return INPUT;
    }

    protected void doValidation()
    {
        if (StringUtils.isEmpty(originalPassword))
        {
            addFieldError("originalPassword", getText("password.invalid"));
        }
        else
        {
            if (StringUtils.isEmpty(password) || StringUtils.isEmpty(confirmPassword))
            {
                addFieldError("password", getText("passwordempty.invalid"));
            }
            else if (!StringUtils.equals(password, confirmPassword))
            {
                addFieldError("password", getText("passworddonotmatch.invalid"));
            }
        }
    }

    private void addPasswordComplexityMessage()
    {
        try
        {
            User user = applicationService.findUserByName(getCrowdApplication(), username);
            passwordComplexityMessage = getPasswordComplexityMessage(user.getDirectoryId());
        }
        catch (UserNotFoundException e)
        {
            logger.warn("Unable to look up user from directory", e);
        }
        catch (ApplicationNotFoundException e)
        {
            logger.warn("Unable to look up application", e);
        }
    }

    public String getConfirmPassword()
    {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword)
    {
        this.confirmPassword = confirmPassword;
    }

    public String getOriginalPassword()
    {
        return originalPassword;
    }

    public void setOriginalPassword(String originalPassword)
    {
        this.originalPassword = originalPassword;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * Get username from the session attributes.
     * @return username
     */
    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        // do not set 'username' with untrusted input data. See initUsernameFromSession()
    }

    private void initUsernameFromSession()
    {
        username = (String) getSession().getAttribute(UsernamePasswordAuthenticationFilter.SPRING_SECURITY_LAST_USERNAME_KEY);
    }

    public String getPasswordComplexityMessage()
    {
        return passwordComplexityMessage;
    }
}
