/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.group;

import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;

public class UpdateGroup extends ViewGroup
{
    @RequireSecurityToken(true)
    public String doUpdateGeneral()
    {
        try
        {
            processGeneral();

            // check for errors
            validate();
            if (hasErrors() || hasActionErrors())
            {
                return INPUT;
            }

            GroupTemplate mutableGroup = new GroupTemplate(directoryManager.findGroupByName(directoryID, name));

            mutableGroup.setDescription(description);
            mutableGroup.setActive(active);

            super.group = directoryManager.updateGroup(directoryID, mutableGroup);

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
            return INPUT;
        }
    }

    @Override
    protected void initFormFieldValues()
    {
        name = group.getName();
        // don't set description and active from the group because we don't want to override user-provided values
    }

    @Override
    public void validate()
    {
        if (description == null || (!description.isEmpty() && StringUtils.isWhitespace(description)))
        {
            addFieldError("description", getText("group.description.invalid"));
        }
    }
}
