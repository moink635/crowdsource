/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.console.action.AbstractBrowser;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.NullRestriction;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.constants.ApplicationTermKeys;

import com.google.common.collect.ImmutableMap;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class BrowseApplications extends AbstractBrowser<Application>
{
    private static final Logger logger = Logger.getLogger(BrowseApplications.class);

    private String active;
    private String name;

    public String execute()
    {
        try
        {
            Collection<SearchRestriction> restrictions = new ArrayList<SearchRestriction>();

            if (active != null && active.length() > 0)
            {
                restrictions.add(Restriction.on(ApplicationTermKeys.ACTIVE).exactlyMatching(Boolean.valueOf(active)));
            }

            if (name != null && name.length() > 0)
            {
                restrictions.add(Restriction.on(ApplicationTermKeys.NAME).containing(name));
            }

            final SearchRestriction restriction = restrictions.isEmpty() ? NullRestrictionImpl.INSTANCE : new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, restrictions.toArray(new SearchRestriction[restrictions.size()]));

            results = applicationManager.search(QueryBuilder.queryFor(Application.class, EntityDescriptor.application()).with(restriction).startingAt(resultsStart).returningAtMost(resultsPerPage + 1));
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return SUCCESS;
    }

    public String getActive()
    {
        return active;
    }

    public void setActive(String active)
    {
        this.active = active;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Map getActiveSelectList()
    {
        return ImmutableMap.of("", getText("all.label"), "true", getText("active.label"), "false", getText("inactive.label"));
    }
}