package com.atlassian.crowd.console.action.group;

import com.atlassian.crowd.console.action.BaseAction;
import com.atlassian.crowd.directory.loader.DirectoryInstanceLoader;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.permission.PermissionManager;
import com.atlassian.crowd.model.EntityComparator;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.AdminGroupChecker;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Action to view the Principals for a given group
 */
public class ViewGroupMembers extends BaseAction
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    protected PermissionManager permissionManager;
    protected AdminGroupChecker adminGroupChecker;

    protected Long directoryID;
    protected String groupName;
    private Collection<User> principals;
    private Collection<Group> subGroups;

    protected boolean supportsNestedGroups;
    private Set<Group> allNonMemberGroups;
    protected boolean preventingLockout;
    private DirectoryInstanceLoader directoryInstanceLoader;

    public String execute()
    {
        if (StringUtils.isNotBlank(groupName) && directoryID != null)
        {
            try
            {
                fetchGroupInformation();
            }
            catch (Exception e)
            {
                logger.error(e.getMessage(), e);
                addActionError(e);
                return ERROR;
            }
        }

        return SUCCESS;
    }

    protected void fetchGroupInformation() throws DirectoryNotFoundException, OperationFailedException, RemoteException
    {
        Directory directory = directoryManager.findDirectoryById(directoryID);
        supportsNestedGroups = directoryInstanceLoader.getDirectory(directory).supportsNestedGroups();

        final MembershipQuery<User> principalsQuery =
            QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                .childrenOf(EntityDescriptor.group(GroupType.GROUP))
                .withName(groupName)
                .returningAtMost(EntityQuery.ALL_RESULTS);
        List<User> principalsList = new ArrayList(directoryManager.searchDirectGroupRelationships(directoryID, principalsQuery));
        Collections.sort(principalsList, EntityComparator.of(User.class));
        principals = principalsList; // sort the list

        if (supportsNestedGroups)
        {
            final MembershipQuery<Group> subGroupsQuery =
                QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP))
                    .childrenOf(EntityDescriptor.group(GroupType.GROUP))
                    .withName(groupName)
                    .returningAtMost(EntityQuery.ALL_RESULTS);
            subGroups = directoryManager.searchDirectGroupRelationships(directoryID, subGroupsQuery);

            fetchNonMemberGroups();
        }
    }

    public boolean hasUpdateGroupPermission()
    {
        try
        {
            return permissionManager.hasPermission(directoryManager.findDirectoryById(directoryID), OperationType.UPDATE_GROUP);
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
            addActionError(e);
            return false;
        }
    }

    private void fetchNonMemberGroups() throws RemoteException, OperationFailedException, DirectoryNotFoundException
    {
        if (allNonMemberGroups == null)
        {
            allNonMemberGroups = new TreeSet<Group>(EntityComparator.of(Group.class));

            List<Group> allGroups = directoryManager.searchGroups(directoryID, QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP)).returningAtMost(EntityQuery.ALL_RESULTS));

            for (Group oneGroup : allGroups)
            {
                if (!subGroups.contains(oneGroup) && !groupName.equals(oneGroup.getName()))
                {
                    allNonMemberGroups.add(oneGroup);
                }
            }
        }
    }

    ///CLOVER:OFF
    public Long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(Long directoryID)
    {
        this.directoryID = directoryID;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName)
    {
        this.groupName = groupName;
    }

    public Collection<User> getPrincipals()
    {
        return principals;
    }

    public void setPrincipals(Collection<User> principals)
    {
        this.principals = principals;
    }

    public Collection<Group> getSubGroups()
    {
        return subGroups;
    }

    public void setSubGroups(Collection<Group> subGroups)
    {
        this.subGroups = subGroups;
    }

    public boolean isSupportsNestedGroups()
    {
        return supportsNestedGroups;
    }

    public Collection<Group> getAllNonMemberGroups()
    {
        return allNonMemberGroups;
    }
    public void setPermissionManager(PermissionManager permissionManager)
    {
        this.permissionManager = permissionManager;
    }

    public AdminGroupChecker getAdminGroupChecker()
    {
        return adminGroupChecker;
    }

    public void setAdminGroupChecker(AdminGroupChecker adminGroupChecker)
    {
        this.adminGroupChecker = adminGroupChecker;
    }

    public boolean isPreventingLockout()
    {
        return preventingLockout;
    }

    public void setPreventingLockout(boolean preventingLockout)
    {
        this.preventingLockout = preventingLockout;
    }

    public void setDirectoryInstanceLoader(DirectoryInstanceLoader directoryInstanceLoader)
    {
        this.directoryInstanceLoader = directoryInstanceLoader;
    }
}

