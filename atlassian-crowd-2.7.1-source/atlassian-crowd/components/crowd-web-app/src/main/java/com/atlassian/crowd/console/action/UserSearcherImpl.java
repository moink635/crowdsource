package com.atlassian.crowd.console.action;

import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class UserSearcherImpl implements Searcher<User>
{
    private final DirectoryManager directoryManager;
    private final ApplicationService applicationService;
    private final ApplicationManager applicationManager;

    public UserSearcherImpl(final DirectoryManager directoryManager, final ApplicationService applicationService, final ApplicationManager applicationManager)
    {
        this.directoryManager = directoryManager;
        this.applicationService = applicationService;
        this.applicationManager = applicationManager;
    }

    public List<User> doSearchByDirectory(long directoryID, Boolean active, String searchText, int resultsStartIndex, int resultsPerPage)
            throws OperationFailedException, DirectoryNotFoundException
    {
        List<User> results;

        // only perform the search if a directory ID was supplied
        if (directoryID != -1)
        {
            SearchRestriction mmt = buildSearchRestrictions(active, searchText);

            results = directoryManager.searchUsers(directoryID, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(mmt).startingAt(resultsStartIndex).returningAtMost(resultsPerPage + 1));
        }
        else
        {
            results = Collections.emptyList();
        }

        return results;
    }

    private SearchRestriction buildSearchRestrictions(final Boolean active, final String searchText)
    {
        final Collection<SearchRestriction> restrictions = new ArrayList<SearchRestriction>();
        if (active != null)
        {
            restrictions.add(Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(Boolean.valueOf(active)));
        }

        // if a search string is provided, add name and email search restrictions
        if (StringUtils.isNotBlank(searchText))
        {
            restrictions.add(Combine.anyOf(
                Restriction.on(UserTermKeys.USERNAME).containing(searchText),
                Restriction.on(UserTermKeys.DISPLAY_NAME).containing(searchText),
                Restriction.on(UserTermKeys.FIRST_NAME).containing(searchText),
                Restriction.on(UserTermKeys.LAST_NAME).containing(searchText),
                Restriction.on(UserTermKeys.EMAIL).containing(searchText)
            ));
        }

        return restrictions.isEmpty() ? NullRestrictionImpl.INSTANCE : Combine.allOf(restrictions);
    }

    public List<User> doSearchByApplication(final long applicationId, final Boolean active, final String searchText, final int resultsStartIndex, final int resultsPerPage)
            throws ApplicationNotFoundException
    {
        List<User> results;

        // only perform the search if a directory ID was supplied
        if (applicationId != -1)
        {
            SearchRestriction multiTermRestriction = buildSearchRestrictions(active, searchText);

            final Application application = applicationManager.findById(applicationId);

            results = applicationService.searchUsers(application, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(multiTermRestriction).startingAt(resultsStartIndex).returningAtMost(resultsPerPage + 1));
        }
        else
        {
            results = Collections.emptyList();
        }

        return results;

    }
}
