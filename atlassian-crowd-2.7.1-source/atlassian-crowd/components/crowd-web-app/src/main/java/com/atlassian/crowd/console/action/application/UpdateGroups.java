package com.atlassian.crowd.console.action.application;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.log4j.Logger;

import java.util.Arrays;

public class UpdateGroups extends ViewApplication
{
    private static final Logger logger = Logger.getLogger(UpdateGroups.class);

    // for removing a mapping
    private String removeName;
    private long removeDirectoryID;

    // for adding a mapping
    private String unsubscribedGroup;

    public String doUpdate()
    {
        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doAddGroup()
    {
        try
        {
            processGeneral();

            if (unsubscribedGroup != null && unsubscribedGroup.length() > 0)
            {
                int index = unsubscribedGroup.indexOf("-");
                long unsubscribedDirectoryID = Long.parseLong(unsubscribedGroup.substring(0, index));
                String unsubscribedGroupName = unsubscribedGroup.substring(index + 1, unsubscribedGroup.length());

                // quick throw ONFE
                final Directory directory = directoryManager.findDirectoryById(unsubscribedDirectoryID);

                if (getApplication().getDirectoryMapping(unsubscribedDirectoryID) != null)
                {
                    applicationManager.addGroupMapping(getApplication(), directory, unsubscribedGroupName);
                }
            }

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doRemoveGroup()
    {
        try
        {
            processGeneral();

            DirectoryMapping directoryMapping = getApplication().getDirectoryMapping(removeDirectoryID);
            if (directoryMapping != null)
            {
                applicationManager.removeGroupMapping(getApplication(), directoryMapping.getDirectory(), removeName);

                // Check if the user can still auth with crowd console. If not re-add the groupMapping
                if (!authorisedToAccessCrowdAdminConsole(directoryMapping, getRemoteUser().getDirectoryId(), getRemoteUser().getName()))
                {
                    applicationManager.addGroupMapping(getApplication(), directoryMapping.getDirectory(), removeName);
                    addActionError(getText("preventlockout.unassigngroup.label", Arrays.asList(removeName)));
                    return INPUT;
                }
            }

            return SUCCESS;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.debug(e.getMessage(), e);
        }

        return INPUT;
    }

    public String getRemoveName()
    {
        return removeName;
    }

    public void setRemoveName(String removeName)
    {
        this.removeName = removeName;
    }

    public long getRemoveDirectoryID()
    {
        return removeDirectoryID;
    }

    public void setRemoveDirectoryID(long removeDirectoryID)
    {
        this.removeDirectoryID = removeDirectoryID;
    }

    public String getUnsubscribedGroup()
    {
        return unsubscribedGroup;
    }

    public void setUnsubscribedGroup(String unsubscribedGroup)
    {
        this.unsubscribedGroup = unsubscribedGroup;
    }
}