package com.atlassian.crowd.console.action.dataimport;

import com.atlassian.core.util.PairType;
import com.atlassian.crowd.importer.config.CsvConfiguration;

import com.google.common.collect.ImmutableList;
import com.opensymphony.webwork.interceptor.SessionAware;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVStrategy;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Where we build our Configuration for the CSV import
 */
public class ImportCsv extends BaseImporter implements SessionAware
{
    // Session-aware map
    private Map session = null;

    // User File attributes
    private String users;

    // Group GroupMembership File attributes
    private String groupMemberships;

    // CSV File delimiter
    private Character delimiter;

    // Are the passwords encrypted?
    private String encryptedPasswords;

    private List passwordsEncrypted = ImmutableList.of(new PairType(Boolean.TRUE, getText("yes.label")), new PairType(Boolean.FALSE, getText("no.label")));

    // The directory to import too
    private Long directoryID;

    public String doDefault() throws Exception
    {
        return INPUT;
    }

    public String doExecute() throws Exception
    {
        // Validate the inputed values
        doValidation();
        if (hasErrors())
        {
            return ERROR;
        }

        // Set the required properties in the session, once validated
        getConfiguration().setDelimiter(delimiter);
        getConfiguration().setUsers(new File(users));


        // Are we encrypting Passwords?
        if (Boolean.valueOf(encryptedPasswords).booleanValue())
        {
            getConfiguration().setEncryptPasswords(Boolean.FALSE);
        }
        else
        {
            getConfiguration().setEncryptPasswords(Boolean.TRUE);
        }

        getConfiguration().setDirectoryID(directoryID);

        if (StringUtils.isNotEmpty(groupMemberships))
        {
            getConfiguration().setGroupMemberships(new File(groupMemberships));
        }

        return SUCCESS;
    }

    private void doValidation()
    {
        // Check that we have a user file, and it is a valid CSV file
        if (StringUtils.isBlank(users))
        {
            addFieldError("users", getText("dataimport.csv.userfile.error"));
        }

        // Check that encrypted passwords option is selected
        if (StringUtils.isBlank(encryptedPasswords))
        {
            addFieldError("encryptedPasswords", getText("dataimport.csv.encryptedpasswords.error"));
        }

        if (delimiter == null || StringUtils.isBlank(delimiter.toString()))
        {
            // We don't have a delimiter defined, so remove it from the session
            // until the user sets one
            getConfiguration().setDelimiter(null);
            addFieldError("delimiter", getText("dataimport.csv.delimiter.error"));
        }

        // If the last two parts of validation were OK
        if (!hasErrors())
        {
            validateFileInput("users", users);

            if (StringUtils.isNotEmpty(groupMemberships))
            {
                validateFileInput("groupMemberships", groupMemberships);
            }
        }

    }

    private void validateFileInput(String fieldName, String fileLocation)
    {
        File csvFile = new File(fileLocation);
        if (csvFile.exists() && csvFile.canRead())
        {
            if (!validateCSVFile(csvFile, delimiter))
            {
                addFieldError(fieldName, getText("dataimport.csv.delimiter.invalid.error", ImmutableList.of(delimiter)));
            }
        }
        else
        {
            addFieldError(fieldName, getText("dataimport.csv.fileinvalid.error", ImmutableList.of(fileLocation)));
        }
    }

    private boolean validateCSVFile(File csvFile, Character delimiter)
    {
        boolean validCsv = true;
        try
        {
            CSVStrategy strategy = CSVStrategy.EXCEL_STRATEGY;
            strategy.setDelimiter(delimiter.charValue());

            String[] line = new CSVParser(new FileReader(csvFile), strategy).getLine();
            if (line == null || line.length <= 1)
            {
                validCsv = false;
            }
        }
        catch (IOException e)
        {
            logger.error(e.getMessage(), e);
            validCsv = false;
        }

        return validCsv;
    }

    ///CLOVER:OFF
    public CsvConfiguration getConfiguration()
    {
        CsvConfiguration configuration = (CsvConfiguration) session.get(BaseImporter.IMPORTER_CONFIGURATION);
        if (configuration == null)
        {
            configuration = constructDefaultConfiguration();
            session.put(IMPORTER_CONFIGURATION, configuration);
        }
        return configuration;
    }

    public void setUsers(String users)
    {
        this.users = users;
    }

    public void setGroupMemberships(String groupMemberships)
    {
        this.groupMemberships = groupMemberships;
    }

    public void setSession(Map session)
    {
        this.session = session;
    }

    public String getUsers()
    {
        if (StringUtils.isBlank(users) && getConfiguration().getUsers() != null)
        {
            return getConfiguration().getUsers().getAbsolutePath();
        }
        return users;
    }

    public String getGroupMemberships()
    {
        if (StringUtils.isBlank(groupMemberships) && getConfiguration().getGroupMemberships() != null)
        {
            return getConfiguration().getGroupMemberships().getAbsolutePath();

        }
        return groupMemberships;
    }

    public Character getDelimiter()
    {
        if (delimiter == null && getConfiguration().getDelimiter() != null)
        {
            return getConfiguration().getDelimiter();
        }
        return delimiter;
    }

    public String getEncryptedPasswords()
    {
        if (StringUtils.isBlank(encryptedPasswords) && getConfiguration().isEncryptPasswords() != null)
        {
            // Invert the configuration option, since the view asks in an inverted way
            if (getConfiguration().isEncryptPasswords().booleanValue())
            {
                encryptedPasswords = Boolean.FALSE.toString();
            }
            else
            {
                encryptedPasswords = Boolean.TRUE.toString();
            }
        }
        return encryptedPasswords;
    }

    public Long getDirectoryID()
    {
        if (directoryID == null && getConfiguration().getDirectoryID() != null)
        {
            return getConfiguration().getDirectoryID();
        }
        return directoryID;
    }

    public void setDirectoryID(Long directoryID)
    {
        this.directoryID = directoryID;
    }

    public void setDelimiter(Character delimiter)
    {
        this.delimiter = delimiter;
    }

    public void setEncryptedPasswords(String encryptedPasswords)
    {
        this.encryptedPasswords = encryptedPasswords;
    }

    private CsvConfiguration constructDefaultConfiguration()
    {
        return new CsvConfiguration(null, "csv", Boolean.TRUE, null, null, new Character(','), Boolean.FALSE);
    }

    public List getPasswordsEncrypted()
    {
        return passwordsEncrypted;
    }

    public void setPasswordsEncrypted(List passwordsEncrypted)
    {
        this.passwordsEncrypted = passwordsEncrypted;
    }
}
