package com.atlassian.crowd.console.interceptor;

import com.atlassian.config.util.BootstrapUtils;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.interceptor.Interceptor;

/**
 * Ensure we don't try to hit the setup step after setup is complete.
 */
public class SetupCheckInterceptor implements Interceptor
{
    public static final String ALREADY_SETUP = "alreadySetup";

    public void destroy()
    {
    }

    public void init()
    {
    }

    public String intercept(ActionInvocation actionInvocation) throws Exception
    {
        if (BootstrapUtils.getBootstrapManager().isSetupComplete())
        {
            return ALREADY_SETUP;
        }

        return actionInvocation.invoke();
    }
}
