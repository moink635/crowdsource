package com.atlassian.crowd.console.action.setup;

import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ResourceLocator;
import com.atlassian.crowd.util.PasswordHelper;
import com.atlassian.crowd.util.PropertyUtils;
import com.opensymphony.webwork.interceptor.ServletRequestAware;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import javax.net.ssl.SSLHandshakeException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class Options extends BaseSetupAction implements ServletRequestAware
{
    private static final Logger logger = Logger.getLogger(Options.class);

    public static final String OPTIONS_STEP = "setupoptions";

    private String title;
    private String sessionTime = "30";
    private String baseURL;
    private HttpServletRequest httpServletRequest;
    private ResourceLocator resourceLocator;
    private PropertyUtils propertyUtils;
    private ClientProperties clientProperties;
    private PasswordHelper passwordHelper;

    public String doUpdate()
    {
        // check if i'm at the correct step
        String setupUpdate = super.doUpdate();
        if (!setupUpdate.equals(SUCCESS))
        {
            return setupUpdate;
        }

        try
        {
            // check for errors
            doValidation();
            if (hasErrors() || hasActionErrors())
            {
                return ERROR;
            }
            propertyManager.setDeploymentTitle(title);
            propertyManager.generateDesEncryptionKey();
            propertyManager.setSessionTime(Long.parseLong(sessionTime));

            // default the caching options
            propertyManager.setCacheEnabled(true);

            // generate a random token key see, this can be configured later through the options menu
            String seed = passwordHelper.generateRandomPassword();
            propertyManager.setTokenSeed(seed);

            // Set the login URL and server URL
            String resourceLocation = resourceLocator.getResourceLocation();
            propertyUtils.updateProperty(resourceLocation, Constants.PROPERTIES_FILE_SECURITY_SERVER_URL, constructSecurityServerURL());
            propertyUtils.updateProperty(resourceLocation, Constants.PROPERTIES_FILE_APPLICATION_LOGIN_URL, baseURL);

            // Refresh the Client properties
            clientProperties.updateProperties(resourceLocator.getProperties());

            getSetupPersister().progessSetupStep();

            return SELECT_SETUP_STEP;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    private String constructSecurityServerURL()
    {
        StringBuffer securityServerURL = new StringBuffer(baseURL);

        if (baseURL.endsWith("/"))
        {
            securityServerURL.append(Constants.CROWD_SERVICE_LOCATION).append("/");
        }
        else
        {
            securityServerURL.append("/").append(Constants.CROWD_SERVICE_LOCATION).append("/");
        }

        return securityServerURL.toString();
    }

    public String getStepName()
    {
        return OPTIONS_STEP;
    }

    public String doDefault()
    {
        // check if i'm at the correct step
        String setupDefault = super.doDefault();
        if (!setupDefault.equals(SUCCESS))
        {
            return setupDefault;
        }

        if (title == null && StringUtils.isNotBlank(getLicense().getOrganisation().getName()));
        {
            title = getText("options.title.defaultvalue", new String[] { getLicense().getOrganisation().getName() });
        }

        if (baseURL == null)
        {
            baseURL = buildBaseURL(httpServletRequest);
        }

        return INPUT;
    }

    private String buildBaseURL(HttpServletRequest request)
    {
        StringBuffer sb = new StringBuffer(32);
        sb.append(request.getScheme());
        sb.append("://");
        sb.append(request.getServerName());
        if (request.getServerPort() != 80)
        {
            sb.append(":");
            sb.append(request.getServerPort());
        }
        sb.append(request.getContextPath());
        return sb.toString();

    }

    protected void doValidation()
    {
        if (StringUtils.isBlank(title))
        {
            addFieldError("title", getText("options.title.invalid"));
        }

        validateUrl(baseURL);

        if (StringUtils.isNotBlank(sessionTime))
        {
            try
            {
                long sessTime = Long.parseLong(sessionTime);
                if (sessTime < 1)
                {
                    addFieldError("sessionTime", getText("session.sessiontime.invalid"));
                }
            }
            catch (NumberFormatException e)
            {
                addFieldError("sessionTime", getText("session.sessiontime.invalid"));
            }
        }
        else
        {
            addFieldError("sessionTime", getText("session.sessiontime.invalid"));
        }
    }

    private void validateUrl(String url)
    {
        final String defaultBaseURL = buildBaseURL(httpServletRequest);

        if (StringUtils.isBlank(url))
        {
            addFieldError("baseURL", getText("options.base.url.invalid", new String[] { defaultBaseURL }));
        }
        else
        {
            try
            {
                final GetMethod method = new GetMethod(url);
                try
                {
                    final HttpClient client = new HttpClient();
                    final int status = client.executeMethod(method);
                    if (status != HttpStatus.SC_OK)
                    {
                        addFieldError("baseURL", getText("options.base.url.unavailable", new String[] { String.valueOf(status), defaultBaseURL }));
                    }
                }
                catch (SSLHandshakeException e)
                {
                    logger.debug(e.getMessage(), e);
                    addFieldError("baseURL", getText("options.base.url.error.ssl"));
                }
                catch (IOException e)
                {
                    logger.debug(e.getMessage(), e);
                    addFieldError("baseURL", getText("options.base.url.error", new String[] { e.getMessage(), defaultBaseURL }));
                }
                finally
                {
                    method.releaseConnection();
                }
            }
            catch (IllegalArgumentException e)
            {
                logger.debug(e.getMessage(), e);
                addFieldError("baseURL", getText("options.base.url.invalid", new String[] { defaultBaseURL }));
            }
        }
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getSessionTime()
    {
        return sessionTime;
    }

    public void setSessionTime(String sessionTime)
    {
        this.sessionTime = sessionTime;
    }

    public String getBaseURL()
    {
        return baseURL;
    }

    public void setBaseURL(String baseURL)
    {
        this.baseURL = baseURL;
    }

    public void setServletRequest(HttpServletRequest request)
    {
        this.httpServletRequest = request;
    }

    public void setResourceLocator(ResourceLocator resourceLocator)
    {
        this.resourceLocator = resourceLocator;
    }

    public void setPropertyUtils(PropertyUtils propertyUtils)
    {
        this.propertyUtils = propertyUtils;
    }

    public void setClientProperties(ClientProperties clientProperties)
    {
        this.clientProperties = clientProperties;
    }

    public void setPasswordHelper(final PasswordHelper passwordHelper)
    {
        this.passwordHelper = passwordHelper;
    }
}