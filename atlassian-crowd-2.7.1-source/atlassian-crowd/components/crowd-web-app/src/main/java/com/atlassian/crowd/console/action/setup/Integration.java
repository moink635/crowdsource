/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action.setup;

import com.atlassian.core.util.PairType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.application.ApplicationManagerException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.RemoteAddress;
import org.apache.log4j.Logger;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

public class Integration extends BaseSetupAction
{
    private static final Logger logger = Logger.getLogger(Integration.class);

    public static final String INTEGRATED_APPS_STEP = "integration";

    private String configureDemoApp;
    private String configureOpenIDServer;
    private List enableDisableOptions;

    public final static String DEMO_APPLICATION_NAME = "demo";
    public final static String DEMO_APPLICATION_DESCRIPTION = "Crowd Demo Application";
    public final static String DEFAULT_APPLICATION_PASSWORD = "password";

    public final static String OPENIDSERVER_APPLICATION_NAME = "crowd-openid-server";
    public final static String OPENIDSERVER_DESCRIPTION_NAME = "Crowd OpenID Server";

    public String doUpdate()
    {
        // check if i'm at the correct step
        String setupUpdate = super.doUpdate();
        if (!setupUpdate.equals(SUCCESS))
        {
            return setupUpdate;
        }

        try
        {
            // if the demo app was checked yes, add it
            if (Boolean.valueOf(configureDemoApp).booleanValue())
            {
                addApplication(getText("demo.application.name"), ApplicationType.GENERIC_APPLICATION, getText("demo.application.description"), DEFAULT_APPLICATION_PASSWORD, true);
            }

            // if the openid server app was checked yes, add it
            if (Boolean.valueOf(configureOpenIDServer).booleanValue())
            {
                addApplication(getText("crowdid.application.name"), ApplicationType.GENERIC_APPLICATION, getText("crowdid.application.description"), DEFAULT_APPLICATION_PASSWORD, true);
            }

            if (!hasErrors())
            {
                getSetupPersister().progessSetupStep();
            }

            return SELECT_SETUP_STEP;
        }
        catch (Exception e)
        {
            addActionError(e);
            logger.error(e.getMessage(), e);
        }

        return INPUT;
    }

    public String getStepName()
    {
        return INTEGRATED_APPS_STEP;
    }

    protected Application addApplication(String appName, ApplicationType applicationType, String description, String password, boolean allowAllToAuthenticate) throws ApplicationManagerException, InvalidCredentialException, UnknownHostException
    {
        // get the default values from the previous step in the setup process
        Application crowdApplication = null;
        try
        {
            crowdApplication = applicationManager.findByName(toLowerCase(getText("application.name")));
        }
        catch (ApplicationNotFoundException e)
        {
            throw new RuntimeException(e);
        }

        ApplicationImpl application = ApplicationImpl.newInstanceWithPassword(appName, applicationType, password);
        application.setDescription(description);
        // configure the client address
        Set<RemoteAddress> addresses = buildAddresses();
        application.setRemoteAddresses(addresses);

                // add the application
        final Application createdApplication = applicationManager.add(application);


        final DirectoryMapping crowdApplicationDirectoryMapping = crowdApplication.getDirectoryMappings().get(0);

        // configure the default directory mapping
        try
        {
            applicationManager.addDirectoryMapping(createdApplication, crowdApplicationDirectoryMapping.getDirectory(), allowAllToAuthenticate, OperationType.values());
        }
        catch (ApplicationNotFoundException e)
        {
            throw new RuntimeException(e);
        }
        catch (DirectoryNotFoundException e)
        {
            throw new RuntimeException(e);
        }

        return createdApplication;
    }

    private Set<RemoteAddress> buildAddresses() throws UnknownHostException
    {
        Set<RemoteAddress> addresses = new HashSet<RemoteAddress>();
        String localhostAddress = null;
        try
        {
            localhostAddress = InetAddress.getLocalHost().getHostAddress();
            addresses.add(new RemoteAddress(localhostAddress));
        }
        catch (UnknownHostException e)
        {
            // do nothing, just don't add this as a host
        }

        addresses.add(new RemoteAddress("localhost"));
        addresses.add(new RemoteAddress("127.0.0.1"));

        return addresses;
    }

    public String doDefault()
    {
        // check if i'm at the correct step
        String setupDefault = super.doDefault();
        if (!setupDefault.equals(SUCCESS))
        {
            return setupDefault;
        }

        actionMessageAlertColor = ALERT_BLUE;

        addActionMessage(getText("integration.warning"));

        // set the integrated apps to be configured by default
        configureDemoApp = Boolean.TRUE.toString();
        configureOpenIDServer = Boolean.TRUE.toString();

        return INPUT;
    }

    public String getConfigureDemoApp()
    {
        return configureDemoApp;
    }

    public void setConfigureDemoApp(String configureDemoApp)
    {
        this.configureDemoApp = configureDemoApp;
    }

    public String getConfigureOpenIDServer()
    {
        return configureOpenIDServer;
    }

    public void setConfigureOpenIDServer(String configureOpenIDServer)
    {
        this.configureOpenIDServer = configureOpenIDServer;
    }

    public List<PairType> getEnableDisableOptions()
    {
        if (enableDisableOptions == null)
        {
            enableDisableOptions = new ArrayList<PairType>(2);

            enableDisableOptions.add(new PairType(Boolean.TRUE.toString(), getText("true.label")));
            enableDisableOptions.add(new PairType(Boolean.FALSE.toString(), getText("false.label")));
        }

        return enableDisableOptions;
    }
}