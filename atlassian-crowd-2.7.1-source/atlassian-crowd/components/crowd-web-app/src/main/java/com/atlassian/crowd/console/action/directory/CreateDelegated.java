package com.atlassian.crowd.console.action.directory;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.model.directory.DirectoryImpl;

import java.util.Map;

/**
 * Extension of the Connector Action to allow for Delegated Directory Creation
 */
public class CreateDelegated extends CreateConnector
{
    private boolean updateUsers;

    private boolean importGroups;

    @Override
    public DirectoryImpl buildDirectoryConfiguration()
    {
        DirectoryImpl directory = super.buildDirectoryConfiguration();
        directory.setType(DirectoryType.DELEGATING);
        directory.setImplementationClass("com.atlassian.crowd.directory.DelegatedAuthenticationDirectory");
        return directory;
    }

    @Override
    public void populateDirectoryAttributesForConnectionTest(final Map<String, String> attributes)
    {
        restoreSavedPassword();
        super.populateDirectoryAttributesForConnectionTest(attributes);
        attributes.put(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS, getConnector());
        attributes.put(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, Boolean.TRUE.toString()); // always enable auto-create-on-auth inside crowd
        attributes.put(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, Boolean.TRUE.toString()); // always enable auto-update-on-auth inside crowd
        attributes.put(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.toString(true)); // Disable roles as per CWD-1549
        attributes.put(DelegatedAuthenticationDirectory.ATTRIBUTE_UPDATE_USER_ON_AUTH, Boolean.toString(updateUsers));
        attributes.put(DelegatedAuthenticationDirectory.ATTRIBUTE_KEY_IMPORT_GROUPS, Boolean.toString(importGroups));
    }

    @Override
    public String doTestConfiguration()
    {
        try
        {
            // If a test is requested, it is no longer the initialLoad
            initialLoad = false;

            DirectoryImpl directory = buildDirectoryConfiguration();

            RemoteDirectory directoryImpl =
                    getDirectoryInstanceLoader().getRawDirectory(directory.getId(),
                            directory.getImplementationClass(), directory.getAttributes());

            directoryImpl.testConnection();

            actionMessageAlertColor = ALERT_BLUE;

            addActionMessage(getText("directoryconnector.testconnection.success"));
        }
        catch (Exception e)
        {
            addActionError(getText("directoryconnector.testconnection.invalid") + " " + e.getMessage());
        }

        return INPUT;
    }

    @Override
    void validateUserPasswordAttr()
    {
        // do nothing, since we don't have this option for a delegated directory
    }

    public boolean isImportGroups()
    {
        return importGroups;
    }

    public void setImportGroups(boolean importGroups)
    {
        this.importGroups = importGroups;
    }

    public boolean isUpdateUsers()
    {
        return updateUsers;
    }

    public void setUpdateUsers(boolean updateUsers)
    {
        this.updateUsers = updateUsers;
    }
}
