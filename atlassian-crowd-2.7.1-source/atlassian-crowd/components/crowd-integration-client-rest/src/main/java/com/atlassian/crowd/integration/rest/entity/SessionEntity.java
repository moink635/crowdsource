package com.atlassian.crowd.integration.rest.entity;

import java.util.Date;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.crowd.model.authentication.Session;

@XmlRootElement(name = "session")
public class SessionEntity implements Session
{
    @XmlElement
    private final String token;

    @XmlElement
    private final UserEntity user;

    @XmlElement (name = "created-date")
    private Date createdDate;

    @XmlElement (name = "expiry-date")
    private Date expiryDate;

    @XmlElement (name = "unaliased-username")
    private String unaliasedUsername;

    private SessionEntity()
    {
        this.token = null;
        this.user = null;
        this.createdDate = null;
        this.expiryDate = null;
    }

    public SessionEntity(String token, UserEntity user, Date createdDate, Date expiryDate)
    {
        this.token = token;
        this.user = user;
        this.createdDate = createdDate;
        this.expiryDate = expiryDate;
    }

    public String getToken()
    {
        return token;
    }

    public UserEntity getUser()
    {
        return user;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public Date getExpiryDate()
    {
        return expiryDate;
    }

    /**
     * If this user has been aliased Crowd may also provide the original
     * unaliased name.
     *
     * @return the original username, or <code>null</code>
     * @since 2.6.2
     */
    @Nullable
    public String getUnaliasedUsername()
    {
        return unaliasedUsername;
    }
}
