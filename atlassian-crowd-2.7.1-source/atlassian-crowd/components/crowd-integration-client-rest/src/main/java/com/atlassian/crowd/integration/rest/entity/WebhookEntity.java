package com.atlassian.crowd.integration.rest.entity;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * REST version of a validation factor (client-side).
 *
 * @since 2.7
 */
@XmlRootElement(name = "webhook")
@XmlAccessorType(XmlAccessType.FIELD)
public class WebhookEntity
{
    @XmlElement(name = "id")
    private final Long id;

    @XmlElement(name = "endpointUrl")
    private String endpointUrl;

    @XmlElement(name = "token")
    private String token;

    public WebhookEntity()
    {
        this.id = null;
    }

    public WebhookEntity(String endpointUrl, @Nullable String token)
    {
        this.id = null;
        this.endpointUrl = endpointUrl;
        this.token = token;
    }

    public long getId()
    {
        return id;
    }

    public String getEndpointUrl()
    {
        return endpointUrl;
    }

    public String getToken()
    {
        return token;
    }
}
