package com.atlassian.crowd.integration.rest.service;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.Properties;
import java.util.concurrent.Executors;

import com.atlassian.crowd.event.Events;
import com.atlassian.crowd.event.IncrementalSynchronisationNotAvailableException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UnsupportedCrowdApiException;
import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.model.authentication.CookieConfiguration;
import com.atlassian.crowd.model.event.OperationEvent;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;

import com.google.common.base.Splitter;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import org.apache.commons.httpclient.HttpStatus;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsIterableWithSize;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

/**
 * Tests the {@link RestCrowdClient}.
 */
public class RestCrowdClientTest
{
    private static final Logger logger = LoggerFactory.getLogger(RestCrowdClientTest.class);

    public static final String ERROR_PAGE = "Unparseable <error> page. Do not try to <parse> it as an entity!";
    private HttpServer server;

    /**
     * Captures the most recent HTTP exchange received by the server
     */
    private HttpExchange mostRecentHttpExchange;

    @Before
    public void resetHttpExchange()
    {
        mostRecentHttpExchange = null;
    }

    @After
    public void stopServer()
    {
        server.stop(0);
        server = null;
    }

    public RestCrowdClient getRestCrowdClient(int port) throws Exception
    {
        final Properties properties = new Properties();
        properties.setProperty(Constants.PROPERTIES_FILE_BASE_URL, "http://localhost:" + port);
        properties.setProperty(Constants.PROPERTIES_FILE_APPLICATION_NAME, "applicationName");

        final ClientProperties clientProperties = ClientPropertiesImpl.newInstanceFromProperties(properties);

        return new RestCrowdClient(clientProperties);
    }

    @Test
    public void addUserToGroupWhenMembershipAlreadyExists() throws Exception
    {
        final String responseXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                   + "<error><reason>MEMBERSHIP_ALREADY_EXISTS</reason></error>";
        server = createMinimalHttpServer("POST", "/rest/usermanagement/1/group/user/direct",
                                         HttpStatus.SC_CONFLICT, responseXml);

        server.start();

        try
        {
            getRestCrowdClient(server.getAddress().getPort()).addUserToGroup("username", "group1");
            fail();
        }
        catch (MembershipAlreadyExistsException e)
        {
            assertEquals("username", e.getChildEntity());
            assertEquals("group1", e.getParentEntity());
        }
    }

    @Test
    public void addGroupToGroupWhenMembershipAlreadyExists() throws Exception
    {
        final String responseXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                   + "<error><reason>MEMBERSHIP_ALREADY_EXISTS</reason></error>";
        server = createMinimalHttpServer("POST", "/rest/usermanagement/1/group/child-group/direct",
                                         HttpStatus.SC_CONFLICT, responseXml);

        server.start();

        try
        {
            getRestCrowdClient(server.getAddress().getPort()).addGroupToGroup("group1", "group2");
        }
        catch (MembershipAlreadyExistsException e)
        {
            assertEquals("group1", e.getChildEntity());
            assertEquals("group2", e.getParentEntity());
        }
    }

    @Test(expected = IncrementalSynchronisationNotAvailableException.class)
    public void testFalseIncrementalSyncFlagFails() throws Exception
    {
        final String responseXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                   + "<events incrementalSynchronisationAvailable=\"false\" newEventToken=\"8047511070899496297:12\"/>";
        server = createMinimalHttpServer("GET", "/rest/usermanagement/1/event", responseXml);

        server.start();
        getRestCrowdClient(server.getAddress().getPort()).getCurrentEventToken();
    }

    @Test(expected = IncrementalSynchronisationNotAvailableException.class)
    public void testMissingIncrementalSyncFlagFails() throws Exception
    {
        final String responseXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                   + "<events newEventToken=\"8047511070899496297:12\"/>";
        server = createMinimalHttpServer("GET", "/rest/usermanagement/1/event", responseXml);

        server.start();
        getRestCrowdClient(server.getAddress().getPort()).getCurrentEventToken();
    }

    @Test
    public void testGetCurrentEventToken() throws Exception
    {
        final String responseXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                   + "<events incrementalSynchronisationAvailable=\"true\" newEventToken=\"8047511070899496297:12\"/>";
        server = createMinimalHttpServer("GET", "/rest/usermanagement/1/event", responseXml);
        server.start();

        String eventToken = getRestCrowdClient(server.getAddress().getPort()).getCurrentEventToken();
        assertEquals("8047511070899496297:12", eventToken);
    }

    @Test(expected = UnsupportedCrowdApiException.class)
    public void testGetCurrentEventTokenIsNotSupportedByTheServer() throws Exception
    {
        server = createMinimalHttpServer("GET", "/rest/usermanagement/1/event", HttpStatus.SC_NOT_FOUND);
        server.start();

        getRestCrowdClient(server.getAddress().getPort()).getCurrentEventToken();
    }

    @Test
    public void testGetNewEvents() throws Exception
    {
        final String responseXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                   + "<events incrementalSynchronisationAvailable=\"true\" newEventToken=\"8047511070899496297:12\">"
                                   + "<userEvent><user/></userEvent>"
                                   + "</events>";
        server = createMinimalHttpServer("GET", "/rest/usermanagement/1/event/event-token", responseXml);
        server.start();

        Events events = getRestCrowdClient(server.getAddress().getPort()).getNewEvents("event-token");
        assertThat(events.getEvents(), IsIterableWithSize.<OperationEvent>iterableWithSize(1));
    }

    @Test(expected = UnsupportedCrowdApiException.class)
    public void testGetNewEventsIsNotSupportedByTheServer() throws Exception
    {
        server = createMinimalHttpServer("GET", "/rest/usermanagement/1/event/event-token",
                                                          HttpStatus.SC_NOT_FOUND);
        server.start();
        getRestCrowdClient(server.getAddress().getPort()).getNewEvents("event-token");
    }

    @Test
    public void testGetMemberships() throws Exception
    {
        final String responseXml = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>"
                                   + "<memberships>"
                                   + "<membership/>"
                                   + "</memberships>";
        server = createMinimalHttpServer("GET", "/rest/usermanagement/1/group/membership", responseXml);
        server.start();

        Iterable<Membership> memberships = getRestCrowdClient(server.getAddress().getPort()).getMemberships();
        assertThat(memberships, IsIterableWithSize.<Membership>iterableWithSize(1));
    }

    @Test(expected = UnsupportedCrowdApiException.class)
    public void testGetMembershipsIsNotSupportedByTheServer() throws Exception
    {
        server = createMinimalHttpServer("GET", "/rest/usermanagement/1/group/membership",
                                                          HttpStatus.SC_NOT_FOUND);
        server.start();

        getRestCrowdClient(server.getAddress().getPort()).getMemberships();
    }

    @Test
    public void testInvalidateSSOTokensForUser() throws Exception
    {
        server = createMinimalHttpServer("DELETE", "/rest/usermanagement/1/session", HttpStatus.SC_NO_CONTENT);
        server.start();

        getRestCrowdClient(server.getAddress().getPort()).invalidateSSOTokensForUser("john");
    }

    @Test(expected = UnsupportedCrowdApiException.class)
    public void testInvalidateSSOTokensForUserIsNotSupportedByTheServer() throws Exception
    {
        server = createMinimalHttpServer("DELETE", "/rest/usermanagement/1/session", HttpStatus.SC_METHOD_NOT_ALLOWED);
        server.start();

        getRestCrowdClient(server.getAddress().getPort()).invalidateSSOTokensForUser("john");
    }

    @Test
    public void testInvalidateSSOTokensForUserWithExcludedToken() throws Exception
    {
        server = createMinimalHttpServer("DELETE", "/rest/usermanagement/1/session", HttpStatus.SC_NO_CONTENT);
        server.start();

        getRestCrowdClient(server.getAddress().getPort()).invalidateSSOTokensForUser("john", "excluded-token-key");
    }

    @Test(expected = UnsupportedCrowdApiException.class)
    public void testInvalidateSSOTokensForUserWithExcludedTokenIsNotSupportedByTheServer() throws Exception
    {
        server = createMinimalHttpServer("DELETE", "/rest/usermanagement/1/session", HttpStatus.SC_METHOD_NOT_ALLOWED);
        server.start();

        getRestCrowdClient(server.getAddress().getPort()).invalidateSSOTokensForUser("john", "excluded-token-key");
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void wrappedRemoteExceptionsIndicateThatTheyAreThrownByCrowd() throws Exception
    {
        server = createMinimalHttpServer("GET",
                "/rest/usermanagement/1/user/group/nested", //?username=user&start-index=0&max-results=-1",
                HttpStatus.SC_INTERNAL_SERVER_ERROR,
                "<status><message>Internal Crowd error</message></status>");
        server.start();

        thrown.expect(OperationFailedException.class);
        thrown.expectMessage(containsString("Error from Crowd server propagated to here via REST API (check the Crowd server logs for details): "));

        getRestCrowdClient(server.getAddress().getPort()).getNamesOfGroupsForNestedUser("user", 0, -1);
    }

    @Test
    public void shouldGetWebhook() throws Exception
    {
        server = createMinimalHttpServer("GET", "/rest/usermanagement/1/webhook/1", HttpStatus.SC_CREATED,
                                         "<webhook><id>1</id><endpointUrl>http://example.test/</endpointUrl></webhook>");
        server.start();

        String webhookEndpointUrl = getRestCrowdClient(server.getAddress().getPort()).getWebhook(1L);

        assertEquals("http://example.test/", webhookEndpointUrl);
    }

    @Test
    public void shouldNotGetWebhookIfItDoesNotExist() throws Exception
    {
        server = createMinimalHttpServer("GET", "/rest/usermanagement/1/webhook/1", HttpStatus.SC_NOT_FOUND,
                                         "<error><reason>WEBHOOK_NOT_FOUND</reason><message>fail</message></error>");
        server.start();

        thrown.expect(WebhookNotFoundException.class);
        thrown.expectMessage(containsString("Webhook <1> not found"));

        getRestCrowdClient(server.getAddress().getPort()).getWebhook(1L);
    }

    @Test
    public void shouldRegisterWebhook() throws Exception
    {
        server = createMinimalHttpServer("POST", "/rest/usermanagement/1/webhook", HttpStatus.SC_CREATED,
                                         "<webhook><id>1</id></webhook>");
        server.start();

        long webhookId =
            getRestCrowdClient(server.getAddress().getPort()).registerWebhook("http://example.test/mywebhook", "secret");

        assertEquals(1, webhookId);
        assertNotNull("A request should have been issued to the server", mostRecentHttpExchange);
    }

    @Test(expected = UnsupportedCrowdApiException.class)
    public void shouldNotRegisterWebhookIfIsNotSupportedByTheServer() throws Exception
    {
        server = createMinimalHttpServer("POST", "/rest/usermanagement/1/webhook", HttpStatus.SC_NOT_FOUND);

        server.start();

        getRestCrowdClient(server.getAddress().getPort()).registerWebhook("http://example.test/mywebhook", "secret");
    }

    @Test
    public void shouldUnregisterWebhookSuccessfully() throws Exception
    {
        server = createMinimalHttpServer("DELETE", "/rest/usermanagement/1/webhook/1", HttpStatus.SC_NO_CONTENT);
        server.start();

        getRestCrowdClient(server.getAddress().getPort()).unregisterWebhook(1);
        assertNotNull("A request should have been issued to the server", mostRecentHttpExchange);
    }

    @Test
    public void shouldNotUnRegisterWebhookWhichDoesNotExist() throws Exception
    {
        server = createMinimalHttpServer("DELETE", "/rest/usermanagement/1/webhook/1", HttpStatus.SC_NOT_FOUND,
                                         "<error><reason>WEBHOOK_NOT_FOUND</reason><message>fail</message></error>");
        server.start();

        thrown.expect(WebhookNotFoundException.class);
        thrown.expectMessage(containsString("Webhook <1> not found"));

        getRestCrowdClient(server.getAddress().getPort()).unregisterWebhook(1);
    }

    @Test
    public void setCookiesAreSentBackWithASubsequentRequest() throws Exception
    {
        server = createMinimalHttpServer();

        addContext(server, "POST", "/rest/usermanagement/1/search",
                HttpStatus.SC_OK, "<x/>", "test-cookie = test-value");

        addContext(server, "GET", "/rest/usermanagement/1/config/cookie", HttpStatus.SC_OK,
                        "<cookie-config/>");

        server.start();

        RestCrowdClient crowdClient = getRestCrowdClient(server.getAddress().getPort());

        crowdClient.testConnection();
        assertNull(mostRecentHttpExchange.getRequestHeaders().get("Cookie"));

        CookieConfiguration cfg = crowdClient.getCookieConfiguration();
        assertNotNull(cfg);

        String cookie = mostRecentHttpExchange.getRequestHeaders().getFirst("Cookie");
        assertNotNull(cookie);

        assertThat(Splitter.on(';').split(cookie),
                Matchers.<String>hasItem(Matchers.equalToIgnoringWhiteSpace("test-cookie=test-value")));
    }

    private HttpServer createMinimalHttpServer(String method, String path, String responseXml)
        throws IOException
    {
        return createMinimalHttpServer(method, path, HttpStatus.SC_OK, responseXml);
    }

    private HttpServer createMinimalHttpServer(String method, String path, int statusCode)
        throws IOException
    {
        return createMinimalHttpServer(method, path, statusCode, ERROR_PAGE);
    }

    private HttpServer createMinimalHttpServer(final String method, String path, final int statusCode,
                                               final String responseBody)
        throws IOException
    {
        HttpServer server = createMinimalHttpServer();

        addContext(server, method, path, statusCode, responseBody);

        return server;
    }

    private HttpServer createMinimalHttpServer()
            throws IOException
    {
        InetSocketAddress addr = new InetSocketAddress(0);
        HttpServer server = HttpServer.create(addr, 0);
        server.setExecutor(Executors.newCachedThreadPool());

        return server;
    }

    private void addContext(HttpServer server, final String method, String path, final int statusCode,
            final String responseBody)
    {
        addContext(server, method, path, statusCode, responseBody, null);
    }

    private void addContext(HttpServer server, final String method, String path, final int statusCode,
            final String responseBody, final String setCookieValue)
    {
        server.createContext(path, new HttpHandler()
        {
            @Override
            public void handle(HttpExchange httpExchange) throws IOException
            {
                logger.debug("Received {} {}", httpExchange.getRequestMethod(), httpExchange.getRequestURI());
                if (!httpExchange.getRequestMethod().equalsIgnoreCase(method))
                {
                    httpExchange.sendResponseHeaders(HttpStatus.SC_METHOD_NOT_ALLOWED, 0);
                    httpExchange.close();
                    return;
                }

                mostRecentHttpExchange = httpExchange;

                final byte[] responseBytes = statusCode == HttpStatus.SC_NO_CONTENT ?
                                             new byte[0] : responseBody.getBytes();

                final Headers responseHeaders = httpExchange.getResponseHeaders();
                responseHeaders.set("X-Embedded-Crowd-Version", "any");
                responseHeaders.set("Content-Type", "application/xml");

                if (setCookieValue != null)
                {
                    responseHeaders.set("Set-Cookie", setCookieValue);
                }

                httpExchange.sendResponseHeaders(statusCode, responseBytes.length);
                final OutputStream responseBody = httpExchange.getResponseBody();
                try
                {
                    responseBody.write(responseBytes);
                }
                finally
                {
                    responseBody.close();
                }
            }
        });
    }
}
