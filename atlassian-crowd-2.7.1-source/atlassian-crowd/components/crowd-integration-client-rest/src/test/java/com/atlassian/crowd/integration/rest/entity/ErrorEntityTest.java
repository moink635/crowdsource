package com.atlassian.crowd.integration.rest.entity;

import java.io.StringReader;

import javax.xml.bind.JAXB;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class ErrorEntityTest
{
    @Test
    public void messageIsExtractedFromXml()
    {
        ErrorEntity errorEntity = JAXB.unmarshal(new StringReader("<status><message>Message in Status</message></status>"), ErrorEntity.class);

        assertEquals("Message in Status", errorEntity.getMessage());
        assertNull(errorEntity.getReason());
    }
}
