package com.atlassian.crowd.integration.rest.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Iterator;

import com.atlassian.crowd.exception.InvalidCrowdServiceException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.integration.rest.entity.MembershipsEntity;
import com.atlassian.crowd.integration.rest.service.RestExecutor.MethodExecutor;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.service.client.ClientProperties;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.io.IOUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RestExecutorTest
{
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void testPathParamCount()
    {
        assertEquals(1, RestExecutor.pathArgumentCount("%s"));
        assertEquals(2, RestExecutor.pathArgumentCount("%s%s"));
        assertEquals(2, RestExecutor.pathArgumentCount("%s%s?%s"));
        assertEquals(2, RestExecutor.pathArgumentCount("%s%s?%s%s"));
        assertEquals(2, RestExecutor.pathArgumentCount("%%"));
    }

    @Test
    public void testBuildUrl()
    {
        assertEquals("base/search/group?start-index=0&max-results=0", RestExecutor.buildUrl("base", "/search/group?start-index=%d&max-results=%d", 0, 0));
    }

    @Test(expected = OperationFailedException.class)
    public void throwErrorCopesWithJaxbFailureToParseEmptyResponseBody() throws Exception
    {
        executorConsumingStream(new ByteArrayInputStream(new byte[0])).throwError(HttpStatus.SC_MOVED_TEMPORARILY);
    }

    @Test(expected = OperationFailedException.class)
    public void throwErrorCopesWithJaxbFailureToParseErrorEntity() throws Exception
    {
        executorConsumingStream(IOUtils.toInputStream("un<parse>able")).throwError(HttpStatus.SC_MOVED_TEMPORARILY);
    }

    @Test
    public void throwErrorDoesAttemptToParseErrorEntityForIgnoredStatusCode() throws Exception
    {
        thrown.expect(CrowdRestException.class);
        thrown.expectMessage("Remote error message");
        executorConsumingStream(IOUtils.toInputStream("<error><message>Remote error message</message></error>"))
            .ignoreErrorEntityForStatusCode(HttpStatus.SC_MOVED_TEMPORARILY)
            .throwError(HttpStatus.SC_BAD_REQUEST); // different status code
    }

    @Test
    public void throwErrorDoesNotAttemptToParseErrorEntityForIgnoredStatusCode() throws Exception
    {
        thrown.expect(CrowdRestException.class);
        thrown.expectMessage(containsString("HTTP error: 302"));
        executorConsumingStream(IOUtils.toInputStream("un<parse>able"))
            .ignoreErrorEntityForStatusCode(HttpStatus.SC_MOVED_TEMPORARILY)
            .throwError(HttpStatus.SC_MOVED_TEMPORARILY);
    }

    private static void assertMembershipsAsExpected(MembershipsEntity entity)
    {
        assertMembershipsAsExpected(entity.getList());
    }

    private static void assertMembershipsAsExpected(Iterable<? extends Membership> ms)
    {
        Iterator<? extends Membership> i = ms.iterator();

        Membership m = i.next();

        assertEquals("parent-group", m.getGroupName());
        assertEquals(Collections.singleton("parent-group-user"), m.getUserNames());
        assertEquals(Collections.singleton("child-group"), m.getChildGroupNames());

        m = i.next();
        assertEquals("child-group", m.getGroupName());
        assertEquals(Collections.singleton("child-group-user"), m.getUserNames());
        assertEquals(Collections.emptySet(), m.getChildGroupNames());

        assertFalse(i.hasNext());
    }

    RestExecutor.MethodExecutor executorConsumingStream(InputStream in) throws Exception
    {
        HttpMethod method = mock(HttpMethod.class);
        when(method.getResponseBodyAsStream()).thenReturn(in);

        ClientProperties fakeProps = mock(ClientProperties.class);
        when(fakeProps.getBaseURL()).thenReturn("http://localhost/");
        when(fakeProps.getApplicationName()).thenReturn("");

        RestExecutor exec = new RestExecutor(fakeProps);
        RestExecutor.MethodExecutor m = exec.new MethodExecutor(method) {
            @Override
            int executeCrowdServiceMethod(HttpMethod method) throws InvalidCrowdServiceException, IOException
            {
                return 200;
            }
        };

        return m;
    }

    @Test
    public void ableToUnmarshalJaxbResponses() throws Exception
    {
        InputStream in = getClass().getResourceAsStream("sample-memberships.xml");

        MethodExecutor m = executorConsumingStream(in);

        MembershipsEntity entity = m.andReceive(MembershipsEntity.class);

        assertMembershipsAsExpected(entity);
    }

    @Test
    public void getExceptionMessageFromResponseReturnsTextPlainAsIs() throws Exception
    {
        String response = "Access denied.";

        HttpMethod method = mock(HttpMethod.class);
        when(method.getResponseBodyAsString()).thenReturn(response);
        when(method.getResponseHeader("Content-Type")).thenReturn(new Header("Content-Type", "text/plain"));

        assertEquals("Access denied.",
                RestExecutor.getExceptionMessageFromResponse(method));
    }

    @Test
    public void getExceptionMessageFromResponseStripsHtml() throws Exception
    {
        String response = "<html><body>Access <i>denied</i>.</body></html>";

        HttpMethod method = mock(HttpMethod.class);
        when(method.getResponseBodyAsString()).thenReturn(response);
        when(method.getResponseHeader("Content-Type")).thenReturn(new Header("Content-Type", "text/html; charset=utf-8"));

        assertEquals("Access denied.",
                RestExecutor.getExceptionMessageFromResponse(method));
    }

    @Test
    public void getExceptionMessageFromResponseRecognisesHtmlContentTypeWithoutParameters() throws Exception
    {
        String response = "<html><body>Access <i>denied</i>.</body></html>";

        HttpMethod method = mock(HttpMethod.class);
        when(method.getResponseBodyAsString()).thenReturn(response);
        when(method.getResponseHeader("Content-Type")).thenReturn(new Header("Content-Type", "text/html"));

        assertEquals("Access denied.",
                RestExecutor.getExceptionMessageFromResponse(method));
    }

    @Test
    public void getExceptionMessageFromResponseDecodesHtmlEntities() throws Exception
    {
        String response = "<html><body>Access &quot;<i>denied</i>&quot;.</body></html>";

        HttpMethod method = mock(HttpMethod.class);
        when(method.getResponseBodyAsString()).thenReturn(response);
        when(method.getResponseHeader("Content-Type")).thenReturn(new Header("Content-Type", "text/html; charset=utf-8"));

        assertEquals("Access \"denied\".",
                RestExecutor.getExceptionMessageFromResponse(method));
    }

    @Test
    public void getExceptionMessageFromResponseOnlyShowsBody() throws Exception
    {
        String response = "<html>\n<title>Page Title</title>\n<body>Access\n&quot;<i>denied</i>&quot;.</body></html>";

        HttpMethod method = mock(HttpMethod.class);
        when(method.getResponseBodyAsString()).thenReturn(response);
        when(method.getResponseHeader("Content-Type")).thenReturn(new Header("Content-Type", "text/html; charset=utf-8"));

        assertEquals("Access \"denied\".",
                RestExecutor.getExceptionMessageFromResponse(method));
    }
}
