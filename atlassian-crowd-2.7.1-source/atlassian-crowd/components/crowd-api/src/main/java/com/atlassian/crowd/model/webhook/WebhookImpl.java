package com.atlassian.crowd.model.webhook;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.Nullable;

import com.atlassian.crowd.model.application.Application;

/**
 * An implementation of Webhook designed to be used with Hibernate.
 *
 * @since v2.7
 */
public class WebhookImpl implements Webhook, Serializable
{
    private Long id;
    private String endpointUrl;
    private Application application;
    @Nullable private String token;
    @Nullable private Date oldestFailureDate;
    private long failuresSinceLastSuccess;

    protected WebhookImpl()
    {
        // empty constructor required by Hibernate
    }

    public WebhookImpl(Webhook other)
    {
        this.id = other.getId();
        this.endpointUrl = other.getEndpointUrl();
        this.application = other.getApplication();
        this.token = other.getToken();
        this.oldestFailureDate = other.getOldestFailureDate();
        this.failuresSinceLastSuccess = other.getFailuresSinceLastSuccess();
    }

    public void updateDetailsFrom(Webhook other)
    {
        // webhookId is not updated
        // endpoint is not updated (part of the business key)
        // application is not updated (part of the business key)
        this.token = other.getToken();
        this.oldestFailureDate = other.getOldestFailureDate();
        this.failuresSinceLastSuccess = other.getFailuresSinceLastSuccess();
    }

    @Override
    public Long getId()
    {
        return id;
    }

    @Override
    public String getEndpointUrl()
    {
        return endpointUrl;
    }

    @Override
    public Application getApplication()
    {
        return application;
    }

    @Nullable
    @Override
    public String getToken()
    {
        return token;
    }

    @Nullable
    @Override
    public Date getOldestFailureDate()
    {
        return oldestFailureDate;
    }

    @Override
    public long getFailuresSinceLastSuccess()
    {
        return failuresSinceLastSuccess;
    }

    // setters, required by Hibernate

    private void setId(Long id)
    {
        this.id = id;
    }

    private void setEndpointUrl(String endpointUrl)
    {
        this.endpointUrl = endpointUrl;
    }

    private void setApplication(Application application)
    {
        this.application = application;
    }

    private void setToken(@Nullable String token)
    {
        this.token = token;
    }

    private void setOldestFailureDate(@Nullable Date oldestFailureDate)
    {
        this.oldestFailureDate = oldestFailureDate;
    }

    private void setFailuresSinceLastSuccess(long failuresSinceLastSuccess)
    {
        this.failuresSinceLastSuccess = failuresSinceLastSuccess;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        WebhookImpl webhook = (WebhookImpl) o;

        if (!application.equals(webhook.application))
        {
            return false;
        }
        if (!endpointUrl.equals(webhook.endpointUrl))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = endpointUrl.hashCode();
        result = 31 * result + application.hashCode();
        return result;
    }
}
