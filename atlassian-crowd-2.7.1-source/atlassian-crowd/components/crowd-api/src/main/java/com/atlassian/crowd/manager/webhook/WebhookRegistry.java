package com.atlassian.crowd.manager.webhook;

import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.model.webhook.Webhook;

/**
 * A service interface that provides persistence for {@link com.atlassian.crowd.model.webhook.Webhook}s.
 *
 * @since v2.7
 */
public interface WebhookRegistry
{
    /**
     * Adds a new Webhook, if it does not exist previously.
     *
     * @param webhook
     * @return the new Webhook, or the existing one, if a Webhook for the same application and endpoint was
     * previously registered.
     */
    Webhook add(Webhook webhook);

    void remove(Webhook webhook) throws WebhookNotFoundException;

    Webhook findById(long webhookId) throws WebhookNotFoundException;

    Iterable<Webhook> findAll();

    /**
     * Updates an existing Webhook.
     *
     * @param webhook a template to modify an existing Webhook. Must have an ID.
     * @return the saved Webhook.
     * @throws WebhookNotFoundException if there is not any Webhook with the ID of the template.
     */
    Webhook update(Webhook webhook) throws WebhookNotFoundException;
}
