package com.atlassian.crowd.util;


/**
 * A record of how long an operation is taking, for logging.
 */
public class TimedOperation
{
    protected final long start = System.currentTimeMillis();

    public String complete(String message)
    {
        long duration = System.currentTimeMillis() - start;

        String formattedTime = String.valueOf(duration) + "ms";

        return message + " in [ " + formattedTime + " ]";
    }
}
