package com.atlassian.crowd.search.query.entity.restriction.constants;

import com.atlassian.crowd.search.query.entity.restriction.Property;
import com.atlassian.crowd.search.query.entity.restriction.PropertyImpl;

import java.util.Date;

public class TokenTermKeys
{
    /**
     * Username or application name associated with token.
     */
    public static final Property<String> NAME = new PropertyImpl<String>("name", String.class);

    /**
     * Date the token was last accessed in milliseconds
     * @see java.util.Date#getTime()
     */
    public static final Property<Long> LAST_ACCESSED_TIME = new PropertyImpl<Long>("lastAccessedTime", Long.class);

    /**
     * Directory ID associated with token.
     */
    public static final Property<Long> DIRECTORY_ID = new PropertyImpl<Long>("directoryId", Long.class);

    /**
     * Random number associated with token.
     */
    public static final Property<Long> RANDOM_NUMBER = new PropertyImpl<Long>("randomNumber", Long.class);
}
