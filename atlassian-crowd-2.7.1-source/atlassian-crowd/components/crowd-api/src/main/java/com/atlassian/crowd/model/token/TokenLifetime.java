package com.atlassian.crowd.model.token;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkState;

/**
 * A value object that describes the requested lifetime of a new Token. Get an instance using the factory method, or use
 * the default lifetime constant.
 */
public class TokenLifetime
{
    /**
     * This constant is internally used to represent the default value
     */
    private static final long DEFAULT_VALUE = -1;

    private final long seconds;

    private TokenLifetime()
    {
        this.seconds = DEFAULT_VALUE;
    }
    
    private TokenLifetime(long seconds)
    {
        checkArgument(seconds >= 0, "The duration %s must be greater or equal to 0", seconds);
        this.seconds = seconds;
    }

    /**
     * @return the token lifetime in seconds
     * @throws IllegalStateException if this is the default lifetime, i.e., it does not prescribe a particular duration
     */
    public long getSeconds()
    {
        checkState(seconds >= 0, "Default lifetime does not prescribe a particular duration");
        return seconds;
    }

    /**
     * @return true if this is the default lifetime, i.e., it does not prescribe a particular duration
     */
    public boolean isDefault()
    {
        return seconds == DEFAULT_VALUE;
    }

    /**
     * Indicates that the token does not specify a concrete lifetime, and will use the default lifetime
     */
    public static final TokenLifetime USE_DEFAULT = new TokenLifetime();

    /**
     * Builds a new instance that represents a requested lifetime of a given number of seconds
     * @param seconds requested token lifetime in seconds
     * @return a new instance of TokenLifetime
     */
    public static TokenLifetime inSeconds(long seconds)
    {
        return new TokenLifetime(seconds);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        TokenLifetime that = (TokenLifetime) o;

        if (seconds != that.seconds)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        return (int) (seconds ^ (seconds >>> 32));
    }
}
