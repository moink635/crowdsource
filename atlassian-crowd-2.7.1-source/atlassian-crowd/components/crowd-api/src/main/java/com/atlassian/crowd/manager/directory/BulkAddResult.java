package com.atlassian.crowd.manager.directory;

import java.util.ArrayList;
import java.util.Collection;

import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Represents the results from an 'addAll' operation.
 */
public class BulkAddResult<T>
{
    private final Collection<T> failedEntities;
    private final Collection<T> existingEntities;
    private final long attemptedToAdd;
    private final boolean overwriteUsed;

    private BulkAddResult(Collection<T> failedEntities, Collection<T> existingEntities, long attemptingToAdd,
                         boolean overwrite)
    {
        this.failedEntities = ImmutableList.copyOf(failedEntities);
        this.existingEntities = ImmutableList.copyOf(existingEntities);
        this.attemptedToAdd = attemptingToAdd;
        this.overwriteUsed = overwrite;

        checkArgument(attemptingToAdd >= failedEntities.size() + existingEntities.size(),
                "Should have attempted to add at least as many entities as we have failed plus existing entities");
    }

    /**
     * Returns the entities which did failed to be
     * added during the bulk add process.
     *
     * @return failed entities.
     */
    public Collection<T> getFailedEntities()
    {
        return failedEntities;
    }

    /**
     * Returns the entities which did not get overwritten
     * during the bulk add process. This collection will
     * be empty if <code>overwrite</code> is <code>true</code>.
     *
     * @return existing entities.
     */
    public Collection<T> getExistingEntities()
    {
        return existingEntities;
    }

    /**
     * @return <code>true</code> iff the overwriting was requested
     * during the bulk add process.
     */
    public boolean isOverwriteUsed()
    {
        return overwriteUsed;
    }

    /**
     * @return the number of entities that was asked to be bulk added.
     */
    public long getAttemptedToAdd()
    {
        return attemptedToAdd;
    }

    /**
     * Calculates the number of entities which were successfully added, which is calculated as the number of entities
     * which were attempted to be added minus both the failed entities (which couldn't be added) and the existing
     * entities (which already existed and hence couldn't be added).
     * @return <code>attemptedToAdd - failedEntities - existingEntities</code>
     */
    public long getAddedSuccessfully()
    {
        return attemptedToAdd - failedEntities.size() - existingEntities.size();
    }

    public static <T> Builder<T> builder(long attemptingToAdd)
    {
        return new Builder<T>(attemptingToAdd);
    }

    public static class Builder<T>
    {
        private Collection<T> failedEntities = new ArrayList<T>();
        private Collection<T> existingEntities = new ArrayList<T>();
        private long attemptingToAdd;
        private boolean overwrite;

        public Builder(long attemptingToAdd)
        {
            this.attemptingToAdd = attemptingToAdd;
        }

        public Builder<T> setOverwrite(boolean overwrite)
        {
            this.overwrite = overwrite;
            return this;
        }

        public Builder<T> addFailedEntities(Collection<T> entities)
        {
            failedEntities.addAll(entities);
            return this;
        }

        public Builder<T> addFailedEntity(T entity)
        {
            failedEntities.add(entity);
            return this;
        }

        public Builder<T> addExistingEntities(Collection<T> entities)
        {
            existingEntities.addAll(entities);
            return this;
        }

        public Builder<T> addExistingEntity(T entity)
        {
            existingEntities.add(entity);
            return this;
        }

        public BulkAddResult<T> build()
        {
            return new BulkAddResult<T>(failedEntities, existingEntities, attemptingToAdd, overwrite);
        }
    }
}
