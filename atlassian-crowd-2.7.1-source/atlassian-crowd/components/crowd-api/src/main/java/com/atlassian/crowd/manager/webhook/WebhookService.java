package com.atlassian.crowd.manager.webhook;

/**
 * Webhook service. It is responsible for delivering notifications to Webhooks.
 *
 * @since v2.7
 */
public interface WebhookService
{
    /**
     * Delivers a notification to all the Webhooks.
     *
     */
    void notifyWebhooks();
}
