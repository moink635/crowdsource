package com.atlassian.crowd.support;

import com.atlassian.crowd.embedded.api.User;


/**
 * Provides information for support about the configured directories. It is used in the Embedded Crowd UI
 * and by the Support Tools Plugin.
 *
 * @since 2.6.2
 */
public interface SupportInformationService
{
    String getSupportInformation(User currentUser);
}
