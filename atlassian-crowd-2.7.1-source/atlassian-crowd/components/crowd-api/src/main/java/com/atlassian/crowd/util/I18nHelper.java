package com.atlassian.crowd.util;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

/**
 * Gets text messages that allow for i18n.
 */
public interface I18nHelper
{
    /**
     * @param key i18n key.
     * @return internationalised text.
     */
    String getText(String key);

    /**
     * @param key i18n key.
     * @param value1 first value to interpolate.
     * @return internationalised text.
     */
    String getText(String key, String value1);

    /**
     * @param key i18n key.
     * @param value1 first value to interpolate.
     * @param value2 second value to interpolate.
     * @return internationalised text.
     */
    String getText(String key, String value1, String value2);

    /**
     * @param key i18n key.
     * @param parameters list, object array or value of parameter(s) to interpolate.
     * @return internationalised text.
     */
    String getText(String key, Object parameters);

    /**
     * @param key i18n key.
     * @return unescaped internationalised text.
     */
    String getUnescapedText(String key);

    /**
     * Return a map of all the translated keys whose keys start with the given prefix, indexed by their key.
     *
     * @param prefix    the prefix
     * @return a Map of keys to translated keys
     * @throws NullPointerException if prefix is <code>null</code>
     */
    Map<String, String> getAllTranslationsForPrefix(String prefix);

    /**
     * @param locale the locale to translate into.
     * @param key the key to translate.
     * @param arguments the arguments to be inserted into the translated string.
     * @return the translated string.
     */
    String getText(Locale locale, String key, Serializable... arguments);
}
