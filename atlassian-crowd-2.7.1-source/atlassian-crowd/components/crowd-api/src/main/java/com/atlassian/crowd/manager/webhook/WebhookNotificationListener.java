package com.atlassian.crowd.manager.webhook;

import com.atlassian.crowd.exception.WebhookNotFoundException;

/**
 * Reacts to the outcome of Webhook notifications.
 *
 * @since v2.7
 */
public interface WebhookNotificationListener
{
    /**
     * Reacts to a successful notification delivery.
     *
     * @param webhookId id of the Webhook
     * @throws com.atlassian.crowd.exception.WebhookNotFoundException if the Webhook with the given id cannot be found
     */
    void onPingSuccess(long webhookId) throws WebhookNotFoundException;

    /**
     * Reacts to a failed notification delivery.
     *
     * @param webhookId id of the Webhook
     * @throws WebhookNotFoundException if the Webhook with the given id cannot be found
     */
    void onPingFailure(long webhookId) throws WebhookNotFoundException;
}
