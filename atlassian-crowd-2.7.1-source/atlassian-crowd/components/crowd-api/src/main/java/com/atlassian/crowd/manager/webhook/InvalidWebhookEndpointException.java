package com.atlassian.crowd.manager.webhook;

public class InvalidWebhookEndpointException extends Exception
{
    /**
     * @param endpointUrl url which is invalid
     * @param reason a reason message which fits into "The URL is invalid _________", such as "because it looks bad"
     */
    public InvalidWebhookEndpointException(String endpointUrl, String reason)
    {
        super("Webhook endpoint url of '" + endpointUrl + "' is invalid " + reason);
    }

    public InvalidWebhookEndpointException(String endpointUrl, Throwable cause)
    {
        super("Webhook endpoint url of '" + endpointUrl + "' is invalid", cause);
    }
}
