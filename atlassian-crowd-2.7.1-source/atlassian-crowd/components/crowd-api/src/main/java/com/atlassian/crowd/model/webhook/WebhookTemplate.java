package com.atlassian.crowd.model.webhook;

import java.util.Date;

import javax.annotation.Nullable;

import com.atlassian.crowd.model.application.Application;

import org.apache.commons.lang3.StringUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A prototype to create Webhooks
 *
 * @since v2.7
 */
public class WebhookTemplate implements Webhook
{
    // immutable fields
    private final Long id;
    private final String endpointUrl;
    private final Application application;
    @Nullable final String token;

    // mutable fields
    @Nullable private Date oldestFailureDate;
    private long failuresSinceLastSuccess;

    private WebhookTemplate(@Nullable Long id, Application application, String endpointUrl, @Nullable String token,
                            @Nullable Date oldestFailureDate, long failuresSinceLastSuccess)
    {
        this.id = id;
        this.endpointUrl = endpointUrl;
        this.application = application;
        this.token = token;
        this.oldestFailureDate = oldestFailureDate;
        this.failuresSinceLastSuccess = failuresSinceLastSuccess;
    }

    public WebhookTemplate(Application application, String endpointUrl, @Nullable String token)
    {
        this(null, application, endpointUrl, token, null, 0);
    }

    public WebhookTemplate(Webhook webhook)
    {
        this(webhook.getId(), webhook.getApplication(), webhook.getEndpointUrl(), webhook.getToken(),
             webhook.getOldestFailureDate(), webhook.getFailuresSinceLastSuccess());
    }

    @Override
    @Nullable
    public Long getId()
    {
        return id;
    }

    @Override
    public String getEndpointUrl()
    {
        return endpointUrl;
    }

    @Override
    public Application getApplication()
    {
        return application;
    }

    @Override
    @Nullable
    public String getToken()
    {
        return token;
    }

    @Nullable
    @Override
    public Date getOldestFailureDate()
    {
        return oldestFailureDate;
    }

    @Override
    public long getFailuresSinceLastSuccess()
    {
        return failuresSinceLastSuccess;
    }

    public void setOldestFailureDate(Date oldestFailureDate)
    {
        this.oldestFailureDate = checkNotNull(oldestFailureDate);
    }

    public void resetOldestFailureDate()
    {
        this.oldestFailureDate = null;
    }

    public void setFailuresSinceLastSuccess(long failuresSinceLastSuccess)
    {
        this.failuresSinceLastSuccess = failuresSinceLastSuccess;
    }

    public void resetFailuresSinceLastSuccess()
    {
        this.failuresSinceLastSuccess = 0;
    }

    @Override
    public String toString()
    {
        return "WebhookTemplate{" +
               "id=" + id +
               ", endpointUrl='" + endpointUrl + '\'' +
               ", applicationID=" + application.getId() +
               ", token=" + StringUtils.isNotEmpty(token) +
               ", oldestFailureDate=" + oldestFailureDate +
               ", failuresSinceLastSuccess=" + failuresSinceLastSuccess +
               '}';
    }
}
