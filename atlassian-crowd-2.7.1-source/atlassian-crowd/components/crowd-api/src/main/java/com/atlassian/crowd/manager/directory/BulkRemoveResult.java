package com.atlassian.crowd.manager.directory;

import java.util.ArrayList;
import java.util.Collection;

import com.google.common.collect.ImmutableList;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Represents the results from a 'removeAll' operation.
 */
public class BulkRemoveResult<T>
{
    private final Collection<T> failedEntities;
    private final Collection<T> missingEntities;
    private final long attemptedToRemove;

    private BulkRemoveResult(Collection<T> failedEntities, Collection<T> missingEntities, long attemptingToRemove)
    {
        this.failedEntities = ImmutableList.copyOf(failedEntities);
        this.missingEntities = ImmutableList.copyOf(missingEntities);
        this.attemptedToRemove = attemptingToRemove;

        checkArgument(attemptingToRemove >= failedEntities.size() + missingEntities.size(),
                "Should have attempted to remove at least as many entities as we have failed plus missing entities");
    }

    /**
     * Returns the entities which failed to be
     * removed during the bulk remove process.
     *
     * @return failed entities.
     */
    public Collection<T> getFailedEntities()
    {
        return failedEntities;
    }

    /**
     * Returns the entities which were missing and hence which were not attempted to be removed.
     *
     * @return missing entities.
     */
    public Collection<T> getMissingEntities()
    {
        return missingEntities;
    }

    /**
     * @return the number of entities that was asked to be bulk removed.
     */
    public long getAttemptedToRemove()
    {
        return attemptedToRemove;
    }

    /**
     * Amount of entities successfully removed, which is calculated as the amount of entities which were attempted to
     * be removed minus both the entities which were missing (and hence could not be removed) and the entities which
     * failed to be removed.
     * @return <code>attemptedToRemove - failedEntities - missingEntities</code>
     */
    public long getRemovedSuccessfully()
    {
        return attemptedToRemove - failedEntities.size() - missingEntities.size();
    }

    public static <T> Builder<T> builder(long attemptingToRemove)
    {
        return new Builder<T>(attemptingToRemove);
    }

    public static class Builder<T>
    {
        private long attemptingToRemove;
        private Collection<T> failedEntities = new ArrayList<T>();
        private Collection<T> missingEntities = new ArrayList<T>();

        public Builder(long attemptingToRemove)
        {
            this.attemptingToRemove = attemptingToRemove;
        }

        public Builder<T> addFailedEntities(Collection<T> entities)
        {
            failedEntities.addAll(entities);
            return this;
        }

        public Builder<T> addFailedEntity(T entity)
        {
            failedEntities.add(entity);
            return this;
        }

        public Builder<T> addMissingEntities(Collection<T> entities)
        {
            missingEntities.addAll(entities);
            return this;
        }

        public Builder<T> addMissingEntity(T entity)
        {
            missingEntities.add(entity);
            return this;
        }

        public BulkRemoveResult<T> build()
        {
            return new BulkRemoveResult<T>(failedEntities, missingEntities, attemptingToRemove);
        }
    }
}
