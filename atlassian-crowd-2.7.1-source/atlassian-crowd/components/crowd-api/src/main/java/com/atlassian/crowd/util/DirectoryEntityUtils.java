package com.atlassian.crowd.util;

import com.atlassian.crowd.model.DirectoryEntity;

import com.google.common.base.Predicate;

import static com.google.common.base.Preconditions.checkNotNull;

public class DirectoryEntityUtils
{
    /**
     * Returns a {@link Predicate} that returns true when its (entity) argument's name is equal to the given name to
     * match.
     * @param name the name that entities should match in order for the returned predicate to return true
     * @param <T> the type of {@link DirectoryEntity} which the predicate should be for
     * @return a {@link Predicate} that returns true when its (entity) argument's name is equal to the given name to
     * match.
     */
    public static <T extends DirectoryEntity> Predicate<T> whereNameEquals(final String name)
    {
        checkNotNull(name, "name to match must not be null");
        return new Predicate<T>()
        {
            @Override
            public boolean apply(T entity)
            {
                return name.equals(entity.getName());
            }
        };
    }
}
