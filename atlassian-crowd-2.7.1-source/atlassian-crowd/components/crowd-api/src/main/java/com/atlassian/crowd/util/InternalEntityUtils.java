package com.atlassian.crowd.util;

import com.atlassian.crowd.model.InternalEntity;

import com.google.common.base.Function;

import org.apache.commons.lang3.StringUtils;

public class InternalEntityUtils
{
    /**
     * Maximum directory entity field size for string fields.
     */
    public static final int MAX_ENTITY_FIELD_LENGTH = 255;

    /**
     * Calls {@link com.atlassian.crowd.model.InternalEntity#getName()} on its argument, which must not be null.
     */
    public static final Function<? super InternalEntity, String> GET_NAME = new Function<InternalEntity, String>()
    {
        @Override
        public String apply(InternalEntity entity)
        {
            return entity.getName();
        }
    };

    private InternalEntityUtils()
    {
        // Not to be instantiated
    }

    /**
     * Truncates the given value so that it will conform with database
     * constraints.
     *
     * Fields are limited to 255 characters. Values longer than 255 characters
     * are truncated to 252 first characters with "..." concatenated in the
     * end.
     *
     * @param value value to be truncated
     * @return value that has maximum of 255 charactersn
     */
    public static String truncateValue(String value)
    {
        return StringUtils.abbreviate(value, MAX_ENTITY_FIELD_LENGTH);
    }

    /**
     * Ensures that the given string is not longer than 255 characters.
     *
     * @param value value to be validated
     */
    public static void validateLength(String value)
    {
        if (value != null && value.length() > MAX_ENTITY_FIELD_LENGTH)
        {
            throw new IllegalArgumentException("Value '" + value + "' exceeds maximum allowed length of " + MAX_ENTITY_FIELD_LENGTH + " characters");
        }
    }

}
