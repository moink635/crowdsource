package com.atlassian.crowd.model.token;

import java.io.Serializable;
import java.util.Date;

import javax.annotation.Nullable;

import org.apache.commons.lang3.Validate;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Holds the token information for an authenticated entity.
 */
public class Token implements Serializable
{
    private static final long serialVersionUID = -6200607622554600683L;

    /**
     * Tokens are used for application clients and principals. If the stored token does not have a valid directory ID,
     * the token is then for an application. The value for an invalid directory ID is the <code>APPLICATION_TOKEN_DIRECTORY_ID</code>
     * value.
     */
    public static final long APPLICATION_TOKEN_DIRECTORY_ID = -1;

    private Long id;
    private String identifierHash;
    private String randomHash;
    private long randomNumber;
    private Date createdDate;
    private Date lastAccessedDate; // scheduled for removal, CWD-3042
    private long lastAccessedTime;
    @Nullable private Long duration;   // seconds
    private String name;     // Username or Application name the token is for.
    private long directoryId = APPLICATION_TOKEN_DIRECTORY_ID;

    private String unaliasedUsername;

    private Token(long directoryId, String name, String identifierHash, long randomNumber, String randomHash,
                  Date createdDate, long lastAccessedTime, @Nullable Long duration, String unaliasedUsername)
    {
        Validate.notNull(directoryId, "directoryId argument cannot be null");
        this.directoryId = directoryId;

        Validate.notNull(name, "name argument cannot be null");
        this.name = name;

        Validate.notNull(identifierHash, "identifierHash argument cannot be null");
        this.identifierHash = identifierHash;

        Validate.notNull(randomNumber, "randomNumber argument cannot be null");
        this.randomNumber = randomNumber;

        Validate.notNull(randomHash, "randomHash argument cannot be null");
        this.randomHash = randomHash;

        Validate.notNull(createdDate, "createdDate argument cannot be null");
        this.createdDate = createdDate;

        Validate.notNull(lastAccessedTime, "lastAccessedTime argument cannot be null");
        this.lastAccessedTime = lastAccessedTime;
        this.lastAccessedDate = new Date(); // scheduled for removal CWD-3042

        // duration can be null, but not negative
        if (duration != null)
        {
            Validate.isTrue(duration >= 0, "The duration cannot be negative: ", duration);
        }
        this.duration = duration;

        this.unaliasedUsername = checkNotNull(unaliasedUsername);
    }

    /**
     * Copy constructor
     *
     * @param other token to copy
     */
    protected Token(Token other)
    {
        this.id = other.getId();
        this.directoryId = other.getDirectoryId();
        this.name = other.getName();
        this.identifierHash = other.getIdentifierHash();
        this.randomNumber = other.getRandomNumber();
        this.randomHash = other.getRandomHash();
        this.createdDate = other.getCreatedDate();
        this.lastAccessedTime = other.getLastAccessedTime();
        this.lastAccessedDate = other.getLastAccessedDate();
        this.duration = other.getDuration();
        this.unaliasedUsername = other.getUnaliasedUsername();
    }

    private Token()
    {
        // Used for Hibernate only
    }

    public Long getId()
    {
        return id;
    }

    private void setId(Long id)
    {
        this.id = id;
    }

    /**
     * Gets the token key.
     *
     * @return The key.
     */
    public String getRandomHash()
    {
        return randomHash;
    }

    /**
     * Sets the token key.
     *
     * @param randomHash The key.
     */
    private void setRandomHash(String randomHash)
    {
        this.randomHash = randomHash;
    }

    /**
     * Gets the name of the entity.
     *
     * @return The name.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets the name of the entity.
     *
     * @param name The name.
     */
    private void setName(String name)
    {
        this.name = name;
    }

    public String getUnaliasedUsername()
    {
        if (unaliasedUsername != null)
        {
            return unaliasedUsername;
        }
        else
        {
            return name;
        }
    }

    /**
     * Directory the {@link com.atlassian.crowd.model.user.User user} originated, -1 if the token is for an
     * {@link com.atlassian.crowd.model.application.ApplicationImpl application}.
     *
     * @return The {@link com.atlassian.crowd.model.directory.DirectoryImpl directory} ID.
     */
    public long getDirectoryId()
    {
        return directoryId;
    }

    /**
     * Diretory the {@link com.atlassian.crowd.model.user.User user} originated, -1 if the token is for an
     * {@link com.atlassian.crowd.model.application.ApplicationImpl application}.
     *
     * @param directoryId The {@link com.atlassian.crowd.model.directory.DirectoryImpl directory} ID.
     */
    private void setDirectoryId(long directoryId)
    {
        this.directoryId = directoryId;
    }

    public long getRandomNumber()
    {
        return randomNumber;
    }

    private void setRandomNumber(final long randomNumber)
    {
        this.randomNumber = randomNumber;
    }

    public boolean isUserToken()
    {
        return !isApplicationToken();
    }

    public boolean isApplicationToken()
    {
        return getDirectoryId() == APPLICATION_TOKEN_DIRECTORY_ID;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    private void setCreatedDate(final Date createdDate)
    {
        this.createdDate = createdDate;
    }

    private Date getLastAccessedDate()
    {
        return lastAccessedDate;
    }

    private void setLastAccessedDate(final Date lastAccessedDate)
    {
        this.lastAccessedDate = lastAccessedDate;
    }

    /**
     * Return the last accessed time. If this object appears to have been loaded
     * without the <code>lastAccessedTime</code> field being set then a
     * legacy value of <code>lastAccessedDate</code> will be used instead.
     */
    public long getLastAccessedTime()
    {
        if (lastAccessedTime == 0 && lastAccessedDate != null)
        {
            return lastAccessedDate.getTime();
        }
        else
        {
            return lastAccessedTime;
        }
    }

    public void setLastAccessedTime(final long lastAccessedTime)
    {
        this.lastAccessedTime = lastAccessedTime;
    }

    /**
     * @return Duration of the token (in seconds). If null, the token uses the default session duration.
     */
    @Nullable
    private Long getDuration()
    {
        return duration;
    }

    /**
     * @param duration Duration of the token (in seconds). If null, the token uses the default session duration.
     */
    private void setDuration(Long duration)
    {
        this.duration = duration;
    }

    /**
     * @return requested lifetime of the token
     */
    public TokenLifetime getLifetime()
    {
        if (getDuration() == null)
        {
            return TokenLifetime.USE_DEFAULT;
        }
        else
        {
            return TokenLifetime.inSeconds(duration);
        }
    }

    public String getIdentifierHash()
    {
        return identifierHash;
    }

    private void setIdentifierHash(final String identifierHash)
    {
        this.identifierHash = identifierHash;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Token token = (Token) o;

        if (getIdentifierHash() != null ? !getIdentifierHash().equals(token.getIdentifierHash()) : token.getIdentifierHash() != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        return getIdentifierHash() != null ? getIdentifierHash().hashCode() : 0;
    }

    @Override
    public String toString()
    {
        return "Token{" +
               "identifierHash='" + identifierHash + '\'' +
               ", lastAccessedTime=" + lastAccessedTime +
               ", createdDate=" + createdDate +
               ", duration=" + duration +
               ", name='" + name + '\'' +
               ", directoryId=" + directoryId +
               '}';
    }

    public static class Builder
    {
        private long directoryId;
        private String name;
        private String identifierHash;
        private long randomNumber;
        private String randomHash;
        private Long duration = null;
        private Date createdDate = new Date();
        private long lastAccessedTime = System.currentTimeMillis();
        private String unaliasedUsername;

        public Builder(long directoryId, String name, String identifierHash, long randomNumber, String randomHash)
        {
            this.directoryId = directoryId;
            this.name = name;
            this.identifierHash = identifierHash;
            this.randomNumber = randomNumber;
            this.randomHash = randomHash;
            this.unaliasedUsername = name;
        }

        public Builder(Token prototype)
        {
            this.directoryId = prototype.directoryId;
            this.name = prototype.name;
            this.identifierHash = prototype.identifierHash;
            this.randomNumber = prototype.randomNumber;
            this.randomHash = prototype.randomHash;
            this.duration = prototype.duration;
            this.createdDate = prototype.createdDate;
            this.lastAccessedTime = prototype.lastAccessedTime;
            this.unaliasedUsername = prototype.getUnaliasedUsername();
        }

        public Builder setDirectoryId(long directoryId)
        {
            this.directoryId = directoryId;
            return this;
        }

        public Builder setName(String name)
        {
            this.name = name;
            return this;
        }

        public Builder setIdentifierHash(String identifierHash)
        {
            this.identifierHash = identifierHash;
            return this;
        }

        public Builder setRandomNumber(long randomNumber)
        {
            this.randomNumber = randomNumber;
            return this;
        }

        public Builder setRandomHash(String randomHash)
        {
            this.randomHash = randomHash;
            return this;
        }

        public Builder setLifetime(TokenLifetime tokenLifetime)
        {
            if (tokenLifetime.isDefault())
            {
                this.duration = null;
            }
            else
            {
                this.duration = tokenLifetime.getSeconds();
            }
            return this;
        }

        public Builder withDefaultDuration()
        {
            setLifetime(TokenLifetime.USE_DEFAULT);
            return this;
        }

        public Builder setCreatedDate(Date createdDate)
        {
            this.createdDate = createdDate;
            return this;
        }

        public Builder setLastAccessedTime(long lastAccessedTime)
        {
            this.lastAccessedTime = lastAccessedTime;
            return this;
        }

        public Token create()
        {
            return new Token(directoryId, name, identifierHash, randomNumber, randomHash,
                             createdDate, lastAccessedTime, duration, unaliasedUsername);
        }
    }
}
