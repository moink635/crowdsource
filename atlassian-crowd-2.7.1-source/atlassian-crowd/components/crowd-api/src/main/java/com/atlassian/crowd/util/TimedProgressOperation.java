package com.atlassian.crowd.util;

import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;

/**
 * A class used for timing log messages. Construct an instance and provide the total number of
 * units of work. Then, call {@link #incrementProgress()} <strong>or</strong> {@link #incrementedProgress()}
 * each time around the loop. If more than <code>gapTime</code> (by default one minute) has passed since
 * the start (or last logging) then progress will be logged at INFO. Otherwise, progress will be logged as TRACE.
 */
public class TimedProgressOperation extends TimedOperation
{
    private final int total;
    private int progress = 0;

    private final String action;

    private final Logger log;

    private final long gapTime;

    long latestLog = start;

    public TimedProgressOperation(String action, int total, Logger log)
    {
        this(action, total, log, TimeUnit.MINUTES.toMillis(1));
    }

    public TimedProgressOperation(String action, int total, Logger log, long gapTime)
    {
        this.action = action;
        this.total = total;
        this.log = log;
        this.gapTime = gapTime;
    }

    /**
     * Indicate that a unit of work is <strong>about</strong> to be done, and log if necessary.
     */
    public void incrementProgress()
    {
        logIfNecessary(false);
        progress++;
    }

    /**
     * Indicate that a unit of work <strong>has</strong> been done, and log if necessary.
     */
    public void incrementedProgress()
    {
        progress++;
        logIfNecessary(false);
    }

    private String getFormatMessage(long now)
    {
        return action + String.format(" - (%d/%d - %s%%) %dms elapsed",
                progress, total,
                Percentage.get(progress, total).toString(),
                (now - start));
    }

    private void logIfNecessary(boolean finished)
    {
        if (!log.isInfoEnabled())
        {
            return;
        }

        // We select the log level of messages if they cross the threshold of time taken to finish.
        // This is because in that case the user should be told about progress as progress is slow.

        long now = System.currentTimeMillis();
        boolean logAtTrace = (now < latestLog + gapTime && !finished);

        if (logAtTrace && !log.isTraceEnabled())
        {
            return;
        }

        latestLog = now;

        String message = getFormatMessage(now);

        if (logAtTrace)
        {
            log.trace(message);
        }
        else
        {
            log.info(message);
        }
    }
}
