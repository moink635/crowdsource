package com.atlassian.crowd.directory;

import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.InternalDirectoryGroup;
import com.atlassian.crowd.model.user.TimestampedUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.util.BatchResult;

import javax.annotation.Nonnull;
import java.util.Set;

/**
 * This interface represents a specialised extension of {@link RemoteDirectory} that is used by InternalDirectories.
 * <p/>
 * In particular, the {@link #findUserByName(String)} and {@link #findGroupByName(String)} have been redefined to return
 * {@link TimestampedUser} and {@link InternalDirectoryGroup}. The {@link InternalDirectoryGroup} allows clients to
 * determine whether the group is "local".
 */
public interface InternalRemoteDirectory extends RemoteDirectory
{
    /**
     * @return {@link TimestampedUser} entity.
     */
    TimestampedUser findUserByName(String name) throws UserNotFoundException;

    /**
     * @return {@link TimestampedUser} entity.
     */
    TimestampedUser findUserByExternalId(String externalId) throws UserNotFoundException;

    /**
     * @return {@link InternalDirectoryGroup} entity.
     */
    InternalDirectoryGroup findGroupByName(String name) throws GroupNotFoundException;

    /**
     * Adds a "local" group to the directory.
     * <p/>
     * This method can be used to store groups that aren't clones of "external" groups. For example, if an LDAP
     * directory is cloned in an internal directory, it's possible to define "local" groups that exist internally but
     * not in LDAP.
     * <p/>
     * This functionality was added to meet the functionality that Confluence provided.
     *
     * @param group template of the group to add.
     * @return the added group retrieved from the underlying store.
     * @throws InvalidGroupException The supplied group is invalid.
     * @throws OperationFailedException underlying directory implementation failed to execute the operation.
     */
    Group addLocalGroup(final GroupTemplate group) throws InvalidGroupException, OperationFailedException;

    /**
     * Adds a collection of users to the directory.
     * <p/>
     * The bulk adding of users may be significantly faster than adding users one-by-one for large collections.
     * <p/>
     * Caller must ensure that the users don't already exist.
     *
     * @param users templates of users to add.
     * @return result containing both successful and failed users
     * @throws IllegalArgumentException if any of the users' directory ID does not match the directory's ID.
     */
    BatchResult<User> addAllUsers(final Set<UserTemplateWithCredentialAndAttributes> users);

    /**
     * Adds a collection of groups to the directory.
     * <p/>
     * The bulk adding of groups may be significantly faster than adding groups one-by-one for large collections.
     * <p/>
     * Caller must ensure that the users don't already exist.
     *
     * @param groups templates of groups to add.
     * @return result containing both successful and failed groups
     * @throws IllegalArgumentException if any of the groups' directory ID does not match the directory's ID.
     */
    BatchResult<Group> addAllGroups(final Set<GroupTemplate> groups);

    /**
     * Adds a collection of users to a group.
     * <p/>
     * Caller must ensure that the memberships don't already exist.
     *
     * @param userNames names of users to add to group.
     * @param groupName name of group to add users to.
     * @return result containing both successful and failed users
     * @throws GroupNotFoundException group with supplied {@code groupName} does not exist.
     */
    BatchResult<String> addAllUsersToGroup(Set<String> userNames, String groupName) throws GroupNotFoundException;

    /**
     * Removes all users from the directory.
     * <p/>
     * If a user with the supplied username does not exist in the directory, the username will be ignored.
     *
     * @param usernames usernames of users to remove.
     * @return batch result containing successes (removed users) and failures (users which were not removed)
     */
    BatchResult<String> removeAllUsers(Set<String> usernames);

    /**
     * Removes all groups from the directory.
     * <p/>
     * If a group with the supplied group name does not exist in the directory, the group name will be ignored.
     *
     * @param groupNames names of groups to remove.
     * @return batch result containing successes (removed groups) and failures (groups which were not removed)
     */
    BatchResult<String> removeAllGroups(Set<String> groupNames);

    /**
     * Returns <code>true</code> if user active status is updated independently in the Crowd cache and the remote
     * directory. Otherwise, user status is synchronised between the cache and the remote directory.
     *
     * @return <code>true</code> if user status in the cache is updated independently of the remote directory.
     */
    boolean isLocalUserStatusEnabled();

    /**
     * Forces a rename on the given user in this directory.
     *
     * This works like {@link #renameUser(String, String)}, except it will still do the rename even if there is an
     * existing user under the newName. In this case, it will first rename that existing user to a name that is known
     * not to exist in this directory.
     *
     * @param oldUser the existing user.
     * @param newName desired name of user.
     * @return renamed user.
     *
     * @throws UserNotFoundException      if the "oldUser" does not exist.
     *
     * @see #renameUser(String, String)
     */
    User forceRenameUser(@Nonnull User oldUser, @Nonnull String newName)
            throws UserNotFoundException;
}
