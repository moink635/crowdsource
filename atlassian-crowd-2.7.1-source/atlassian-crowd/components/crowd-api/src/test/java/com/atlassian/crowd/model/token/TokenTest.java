package com.atlassian.crowd.model.token;

import java.io.ObjectInputStream;
import java.util.Date;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TokenTest
{
    @Test
    public void clonedTokensInheritTheOriginalNameAsUnaliasedName()
    {
        Token token = new Token.Builder(0, "username", "", 0, "").create();

        assertEquals("username", token.getName());
        assertEquals("username", token.getUnaliasedUsername());

        Token aliased = new Token.Builder(token).setName("alias").create();

        assertEquals("alias", aliased.getName());
        assertEquals("username", aliased.getUnaliasedUsername());
    }

    @Test
    public void serialisedTokenFromCrowd2Point5CanBeRead() throws Exception
    {
        Token t;

        ObjectInputStream in = new ObjectInputStream(getClass().getResourceAsStream("TokenTest-serialised-token.ser"));

        try
        {
            t = (Token) in.readObject();
            assertEquals(-1, in.read());
        }
        finally
        {
            in.close();
        }

        assertEquals(123, t.getDirectoryId());
        assertEquals("token-name", t.getName());
        assertEquals("identifier-hash", t.getIdentifierHash());
        assertEquals(456L, t.getRandomNumber());
        assertEquals("random-hash", t.getRandomHash());
        assertEquals(new Date(789), t.getCreatedDate());
        assertEquals(new Date(101112), new Date(t.getLastAccessedTime()));
    }

    @Test
    public void setLastAccessedTimeChangesTokens()
    {
        Token t = new Token.Builder(0, "username", "", 0, "").create();

        t.setLastAccessedTime(123);
        assertEquals(123, t.getLastAccessedTime());
    }
}
