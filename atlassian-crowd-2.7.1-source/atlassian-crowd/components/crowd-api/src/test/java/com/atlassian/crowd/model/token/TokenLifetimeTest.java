package com.atlassian.crowd.model.token;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TokenLifetimeTest
{
    @Test(expected = IllegalArgumentException.class)
    public void cannotCreateANegativeLifetime()
    {
        TokenLifetime.inSeconds(-1);
    }

    @Test(expected = IllegalStateException.class)
    public void defaultLifetimeDoesNotHaveSeconds()
    {
        TokenLifetime.USE_DEFAULT.getSeconds();
    }

    @Test
    public void defaultIsDefault()
    {
        assertTrue(TokenLifetime.USE_DEFAULT.isDefault());
    }

    @Test
    public void specificDurationIsNotDefault()
    {
        assertFalse(TokenLifetime.inSeconds(1L).isDefault());
    }

    @Test
    public void getSecondsForNonDefault()
    {
        assertEquals(1L, TokenLifetime.inSeconds(1L).getSeconds());
    }
}
