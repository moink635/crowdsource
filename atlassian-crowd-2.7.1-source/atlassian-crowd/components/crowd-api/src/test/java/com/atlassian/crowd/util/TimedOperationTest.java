package com.atlassian.crowd.util;

import org.junit.Test;

import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

public class TimedOperationTest
{
    @Test
    public void timeIncludedInResultUnconditionally()
    {
        TimedOperation o = new TimedOperation();
        String s = o.complete("operation completed");

        assertThat(s, startsWith("operation completed in [ "));
        assertThat(s, endsWith("ms ]"));

        String justNumber = s.replaceAll(".*\\[ ", "").replaceAll("ms.*", "");
        assertThat(Long.parseLong(justNumber), greaterThanOrEqualTo(0L));
    }

    @Test
    public void providedMessageIsNotFormatted()
    {
        TimedOperation o = new TimedOperation();
        String s = o.complete("Message '':");

        assertThat(s, startsWith("Message '': in [ "));
    }

    @Test
    public void badMessageFormatsDoNotCauseExceptions()
    {
        assertThat(
                new TimedOperation().complete("{bad-message-format}"),
                startsWith("{bad-message-format} in ["));
    }
}
