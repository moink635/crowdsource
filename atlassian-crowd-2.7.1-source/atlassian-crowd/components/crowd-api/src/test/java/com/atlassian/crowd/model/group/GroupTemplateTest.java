package com.atlassian.crowd.model.group;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GroupTemplateTest
{
    @Test(expected = IllegalArgumentException.class)
    public void cannotConstructGroupTemplateWithNullName()
    {
        new GroupTemplate((String) null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotConstructGroupTemplateWithBlankName()
    {
        new GroupTemplate("");
    }

    @Test
    public void groupTemplateIsActiveAndAGroupByDefault()
    {
        GroupTemplate gt = new GroupTemplate("name");
        assertEquals(GroupType.GROUP, gt.getType());
        assertTrue(gt.isActive());
    }
}
