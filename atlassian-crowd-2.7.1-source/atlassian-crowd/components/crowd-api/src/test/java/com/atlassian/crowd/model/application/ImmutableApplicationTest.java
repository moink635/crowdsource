package com.atlassian.crowd.model.application;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.List;

import org.junit.Test;

import com.atlassian.crowd.embedded.api.Directory;
import com.google.common.collect.Lists;

/**
 * Tests for <tt>ImmutableApplication</tt>.
 *
 * @since 2.2
 */
public class ImmutableApplicationTest
{
    @Test
    public void testGetDirectoryMapping() throws Exception
    {
        List<DirectoryMapping> directoryMappings = Lists.newArrayList(
                createDirectoryMapping(1L),
                createDirectoryMapping(2L),
                createDirectoryMapping(null),
                createDirectoryMapping(3L)
        );

        ImmutableApplication application = ImmutableApplication.builder("Application", ApplicationType.CONFLUENCE)
                                            .setDirectoryMappings(directoryMappings)
                                            .build();

        // remove the expected mapping to ensure that ImmutableApplication makes a copy of the collection
        directoryMappings.remove(directoryMappings.size() - 1);

        DirectoryMapping directoryMapping = application.getDirectoryMapping(3L);
        assertEquals(Long.valueOf(3L), directoryMapping.getDirectory().getId());
    }

    /**
     * Creates a new DirectoryMapping mapping the specified directory.
     *
     * @param directoryId directory ID
     * @return new DirectoryMapping
     */
    private static DirectoryMapping createDirectoryMapping(final Long directoryId)
    {
        Directory directory = mock(Directory.class);
        when(directory.getId()).thenReturn(directoryId);
        return new DirectoryMapping(null, directory, true);
    }
}
