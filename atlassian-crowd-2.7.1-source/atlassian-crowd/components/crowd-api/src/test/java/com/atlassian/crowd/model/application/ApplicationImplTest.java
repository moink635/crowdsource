package com.atlassian.crowd.model.application;

import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.webhook.Webhook;

import com.google.common.collect.ImmutableSet;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static org.junit.Assert.assertSame;

public class ApplicationImplTest
{
    private static final String LONG_STRING = StringUtils.repeat("X", 300);

    @Test(expected=IllegalArgumentException.class)
    public void testApplicationImpl()
    {
        new ApplicationImpl(new InternalEntityTemplate(0L, LONG_STRING, true, null, null));
    }

    @Test
    public void webhookSetterShouldOverrideCollectionInstance()
    {
        ApplicationImpl application = ApplicationImpl.newInstance("name", ApplicationType.GENERIC_APPLICATION);

        ImmutableSet<Webhook> newWebhooks = ImmutableSet.of();
        application.setWebhooks(newWebhooks);

        assertSame(newWebhooks, application.getWebhooks());  // Hibernate requires this - see explanation in setWebhooks()
    }
}
