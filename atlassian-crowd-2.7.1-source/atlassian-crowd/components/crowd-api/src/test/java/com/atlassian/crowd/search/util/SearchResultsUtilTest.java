package com.atlassian.crowd.search.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.atlassian.crowd.model.DirectoryEntity;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.util.SearchResultsUtil;

import com.google.common.collect.ImmutableList;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SearchResultsUtilTest
{
    @Test
    public void convertEntitiesToNamesAcceptsEmptyList()
    {
        assertEquals(Collections.emptyList(), SearchResultsUtil.convertEntitiesToNames(Collections.<DirectoryEntity>emptyList()));
    }

    @Test
    public void convertEntitiesToNamesConvertsInOrder()
    {
        List<UserTemplate> users = new ArrayList<UserTemplate>();

        for (String s : ImmutableList.of("one", "two", "three"))
        {
            users.add(new UserTemplate(s));
        }

        assertEquals(
                ImmutableList.of("one", "two", "three"),
                SearchResultsUtil.convertEntitiesToNames(users));
    }

    @Test
    public void constrainResultsReturnsItemWhenLimitsMatch()
    {
        List<String> l = ImmutableList.of("one", "two");

        assertEquals(l,
                SearchResultsUtil.constrainResults(l, 0, 2));
    }

    @Test
    public void constrainResultsReturnsNumberOfItemsRequested()
    {
        List<String> l = ImmutableList.of("one", "two");

        assertEquals(ImmutableList.of("one"),
                SearchResultsUtil.constrainResults(l, 0, 1));
        assertEquals(ImmutableList.of("two"),
                SearchResultsUtil.constrainResults(l, 1, 1));
    }

    @Test
    public void constrainResultsReturnsAllForAllResults()
    {
        List<String> l = ImmutableList.of("one", "two");

        assertEquals(l,
                SearchResultsUtil.constrainResults(l, 0, EntityQuery.ALL_RESULTS));
    }

    @Test
    public void noOverflowWhenRequestingHugeNumberOfResults()
    {
        List<String> l = ImmutableList.of("one", "two");

        assertEquals(ImmutableList.of("two"),
                SearchResultsUtil.constrainResults(l, 1, Integer.MAX_VALUE));
    }

    @Test
    public void constrainResultsReturnsEmptyListWhenStartIsAfterListEnd()
    {
        List<String> l = ImmutableList.of("one", "two");

        assertEquals(Collections.emptyList(),
                SearchResultsUtil.constrainResults(l, 2, 1));
        assertEquals(Collections.emptyList(),
                SearchResultsUtil.constrainResults(l, 2, EntityQuery.ALL_RESULTS));
    }
}
