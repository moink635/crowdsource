package com.atlassian.crowd.model.webhook;

import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertSame;

public class WebhookImplTest
{
    private static final String ENDPOINT1 = "endpoint";
    private static final String ENDPOINT2 = "endpoint2";
    private static final String APPLICATION1 = "app1";
    private static final String APPLICATION2 = "app2";

    @Test
    public void updateDetailsFromWebhookShouldNotUpdateKeyFields()
    {
        Application application1 = ApplicationImpl.newInstance(APPLICATION1, ApplicationType.GENERIC_APPLICATION);
        Application application2 = ApplicationImpl.newInstance(APPLICATION2, ApplicationType.GENERIC_APPLICATION);

        WebhookTemplate originalTemplate = new WebhookTemplate(application1, ENDPOINT1, null);
        WebhookImpl webhook = new WebhookImpl(originalTemplate);

        WebhookTemplate updatedTemplate = new WebhookTemplate(application2, ENDPOINT2, null);
        webhook.updateDetailsFrom(updatedTemplate);

        assertSame(application1, webhook.getApplication());
        assertEquals(ENDPOINT1, webhook.getEndpointUrl());
    }

    /**
     * Explanation: do not do equals() and hashCode() based on the ID, because the entity may be transient.
     * @link http://docs.jboss.org/hibernate/core/4.0/manual/en-US/html/persistent-classes.html#persistent-classes-equalshashcode
     */
    @Test
    public void equalsShouldUseBusinessKey() throws Exception
    {
        Application application = ApplicationImpl.newInstance(APPLICATION1, ApplicationType.GENERIC_APPLICATION);
        WebhookTemplate webhookTemplate = new WebhookTemplate(application, ENDPOINT1, null);

        WebhookImpl webhook1 = new WebhookImpl(webhookTemplate);
        WebhookImpl webhook2 = new WebhookImpl(webhookTemplate);

        assertEquals(webhook1, webhook2);
    }

    @Test
    public void webhooksWithDifferentEndpointUrlsAreDifferent() throws Exception
    {
        Application application = ApplicationImpl.newInstance(APPLICATION1, ApplicationType.GENERIC_APPLICATION);
        WebhookTemplate webhookTemplate1 = new WebhookTemplate(application, ENDPOINT1, null);
        WebhookTemplate webhookTemplate2 = new WebhookTemplate(application, ENDPOINT2, null);

        WebhookImpl webhook1 = new WebhookImpl(webhookTemplate1);
        WebhookImpl webhook2 = new WebhookImpl(webhookTemplate2);

        assertNotEquals(webhook1, webhook2);
    }

    @Test
    public void webhooksOfDifferentApplicationsAreDifferent() throws Exception
    {
        Application application1 = ApplicationImpl.newInstance(APPLICATION1, ApplicationType.GENERIC_APPLICATION);
        Application application2 = ApplicationImpl.newInstance(APPLICATION2, ApplicationType.GENERIC_APPLICATION);
        WebhookTemplate webhookTemplate1 = new WebhookTemplate(application1, ENDPOINT1, null);
        WebhookTemplate webhookTemplate2 = new WebhookTemplate(application2, ENDPOINT1, null);

        WebhookImpl webhook1 = new WebhookImpl(webhookTemplate1);
        WebhookImpl webhook2 = new WebhookImpl(webhookTemplate2);

        assertNotEquals(webhook1, webhook2);
    }
}
