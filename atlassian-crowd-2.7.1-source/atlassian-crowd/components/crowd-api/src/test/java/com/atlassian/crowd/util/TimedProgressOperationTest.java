package com.atlassian.crowd.util;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class TimedProgressOperationTest
{
    private Logger logger;

    @Before
    public void enableLoggingForMock()
    {
        logger = mock(Logger.class);
        when(logger.isInfoEnabled()).thenReturn(true);
    }

    @Test
    public void doesNotLogProgressAtInfoWhenNotLongHasElapsed()
    {
        when(logger.isTraceEnabled()).thenReturn(false);

        TimedProgressOperation tpo = new TimedProgressOperation("", 1, logger);
        tpo.incrementProgress();

        verify(logger).isInfoEnabled();
        verify(logger).isTraceEnabled();
        verifyNoMoreInteractions(logger);
    }

    @Test
    public void logsAtTraceWhetherOrNotTimeIsElapsed()
    {
        when(logger.isTraceEnabled()).thenReturn(true);

        TimedProgressOperation tpo = new TimedProgressOperation("", 1, logger);
        tpo.incrementProgress();

        verify(logger).isInfoEnabled();
        verify(logger).isTraceEnabled();
        verify(logger).trace(anyString());
    }

    @Test
    public void logsAtInfoWhenMaximumDelayIsExceeded()
    {
        when(logger.isTraceEnabled()).thenReturn(false);

        TimedProgressOperation tpo = new TimedProgressOperation("", 1, logger, 0);
        tpo.incrementProgress();

        verify(logger).info(anyString());
    }

    @Test
    public void formatOfInitialLogMessageIsAsExpected()
    {
        when(logger.isTraceEnabled()).thenReturn(true);

        TimedProgressOperation tpo = new TimedProgressOperation("doing work", 1, logger);
        tpo.incrementProgress();

        verify(logger).trace(Mockito.matches("doing work - \\(0/1 - 00.0%\\) \\d+ms elapsed"));
    }

    @Test
    public void formatOfSubsequentLogMessageCalculatesPercentage()
    {
        TimedProgressOperation tpo = new TimedProgressOperation("doing work", 1, logger);

        when(logger.isTraceEnabled()).thenReturn(false);
        tpo.incrementProgress();

        when(logger.isTraceEnabled()).thenReturn(true);
        tpo.incrementProgress();

        verify(logger).trace(Mockito.matches("doing work - \\(1/1 - 100.0%\\) \\d+ms elapsed"));
    }

    @Test
    public void incrementedProgressLogsWithProgressIncluded()
    {
        when(logger.isTraceEnabled()).thenReturn(true);

        TimedProgressOperation tpo = new TimedProgressOperation("doing work", 1, logger);
        tpo.incrementedProgress();

        verify(logger).trace(Mockito.matches("doing work - \\(1/1 - 100.0%\\) \\d+ms elapsed"));
    }
}
