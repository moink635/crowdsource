INSERT INTO public.cwd_directory (id, directory_name, lower_directory_name, created_date, updated_date, active, description, impl_class, lower_impl_class, directory_type) VALUES (32769, 'Studio Crowd', 'studio crowd', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'T', 'Studio internal directory', 'com.atlassian.crowd.directory.InternalDirectory', 'com.atlassian.crowd.directory.internaldirectory', 'INTERNAL');


INSERT INTO public.cwd_directory (id, directory_name, lower_directory_name, created_date, updated_date, active, description, impl_class, lower_impl_class, directory_type) VALUES (32770, 'bamboo-svn-user-directory', 'bamboo-svn-user-directory', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'T', 'Directory for the user that Bamboo uses to log in to SVN', 'com.atlassian.crowd.directory.InternalDirectory', 'com.atlassian.crowd.directory.internaldirectory', 'INTERNAL');


INSERT INTO public.cwd_user (id, user_name, lower_user_name, active, created_date, updated_date, first_name, lower_first_name, last_name, lower_last_name, display_name, lower_display_name, email_address, lower_email_address, directory_id, credential) VALUES (65537, 'sysadmin', 'sysadmin', 'T', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'System', 'system', 'Administrator', 'administrator', 'System Administrator', 'system administrator', 'noreply@atlassian.com', 'noreply@atlassian.com', 32769, '${horde.initialdata.SYSADMIN_CREDENTIAL}');


INSERT INTO public.cwd_user (id, user_name, lower_user_name, active, created_date, updated_date, first_name, lower_first_name, last_name, lower_last_name, display_name, lower_display_name, email_address, lower_email_address, directory_id, credential) VALUES (65538, '${horde.initialdata.ADMIN_USER_NAME}', '${horde.initialdata.ADMIN_LOWER_USER_NAME}', 'T', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.ADMIN_FIRST_NAME}', '${horde.initialdata.ADMIN_LOWER_FIRST_NAME}', '${horde.initialdata.ADMIN_LOWER_LAST_NAME}', '${horde.initialdata.ADMIN_LOWER_LAST_NAME}', '${horde.initialdata.ADMIN_DISPLAY_NAME}', '${horde.initialdata.ADMIN_LOWER_DISPLAY_NAME}', '${horde.initialdata.ADMIN_EMAIL}', '${horde.initialdata.ADMIN_LOWER_EMAIL}', 32769, '${horde.initialdata.ADMIN_CREDENTIAL}');


INSERT INTO public.cwd_user (id, user_name, lower_user_name, active, created_date, updated_date, first_name, lower_first_name, last_name, lower_last_name, display_name, lower_display_name, email_address, lower_email_address, directory_id, credential) VALUES (65539, '_bamboo-svn-user', '_bamboo-svn-user', 'T', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'Bamboo', 'bamboo', 'SVN User', 'svn user', 'Bamboo SVN User', 'bamboo svn user', 'noreply@atlassian.com', 'noreply@atlassian.com', 32770, '{PKCS5S2}UVahBrctwur7IDzDXCFGkvtOtWkHL9zoCuY4/oEsMs/RdqxdWecjwdogtCYuRHIa');


INSERT INTO public.cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (1, 'google-apps', 'google-apps', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'T', 'Google Applications Connector', 'PLUGIN', '${horde.initialdata.APP_CREDENTIAL_GOOGLE_APPS}');


INSERT INTO public.cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (2, 'crowd', 'crowd', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'T', 'Crowd Console', 'CROWD', '${horde.initialdata.APP_CREDENTIAL_CROWD}');


INSERT INTO public.cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (3, 'jira', 'jira', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'T', 'JIRA', 'JIRA', '${horde.initialdata.APP_CREDENTIAL_JIRA}');


INSERT INTO public.cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (4, 'confluence', 'confluence', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'T', 'Confluence', 'CONFLUENCE', '${horde.initialdata.APP_CREDENTIAL_CONFLUENCE}');


INSERT INTO public.cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (5, 'crucible', 'crucible', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'T', 'Crucible', 'CRUCIBLE', '${horde.initialdata.APP_CREDENTIAL_CRUCIBLE}');


INSERT INTO public.cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (8, 'webdav', 'webdav', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'T', 'WebDAV', 'GENERIC_APPLICATION', '${horde.initialdata.APP_CREDENTIAL_WEBDAV}');


INSERT INTO public.cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (9, 'indra', 'indra', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'T', 'Indra', 'GENERIC_APPLICATION', '${horde.initialdata.APP_CREDENTIAL_INDRA}');


INSERT INTO public.cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (10, 'agni', 'agni', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'T', 'Agni', 'GENERIC_APPLICATION', '${horde.initialdata.APP_CREDENTIAL_AGNI}');


INSERT INTO public.cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (6, 'bamboo', 'bamboo', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'T', 'Bamboo', 'BAMBOO', '${horde.initialdata.APP_CREDENTIAL_BAMBOO}');


INSERT INTO public.cwd_application (id, application_name, lower_application_name, created_date, updated_date, active, description, application_type, credential) VALUES (7, 'subversion', 'subversion', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', 'T', 'Subversion', 'GENERIC_APPLICATION', '${horde.initialdata.APP_CREDENTIAL_SVN}');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'deployment.title', 'Studio Crowd');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'des.encryption.key', '${horde.initialdata.DES_ENCRYPTION_KEY}');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'session.time', '2678400000');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'cache.enabled', 'true');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'gzip.enabled', 'false');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'domain', '.${horde.initialdata.HOSTNAME}');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'current.license.resource.total', '0');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'secure.cookie', 'false');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'email.template.forgotten.username', 'Hello $firstname $lastname,

You have requested the username for your email address: $email.

Your username(s) are: $username

If you think this email was sent incorrectly, please contact one of the administrators at: $admincontact

$deploymenttitle Administrator');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'com.sun.jndi.ldap.connect.pool.initsize', '1');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'com.sun.jndi.ldap.connect.pool.prefsize', '10');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'com.sun.jndi.ldap.connect.pool.maxsize', '0');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'com.sun.jndi.ldap.connect.pool.timeout', '30000');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'com.sun.jndi.ldap.connect.pool.protocol', 'plain ssl');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'com.sun.jndi.ldap.connect.pool.authentication', 'simple');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'build.number', '605');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.username', NULL);


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.null', 'com.atlassian.studio.crowd.plugin:build', '12');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'database.token.storage.enabled', 'false');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.port', '25');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.usessl', 'false');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.jndi', '');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.message.template', 'Hello $firstname $lastname,

Your password has been reset by a $deploymenttitle administrator at $date.

Your username is: $username

Your new password is: $password

$deploymenttitle Administrator');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'notification.email', 'crowd@${horde.initialdata.HOSTNAME}');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'validation.factors.include_ip_address', 'false');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'trusted.proxy.servers', 'j2ee.instance.jirastudio.com-jira,j2ee.instance.jirastudio.com-crowd,127.0.0.101,j2ee.instance.jirastudio.com-bamboo,127.0.0.102,j2ee.instance.jirastudio.com-confluence,127.0.0.106,127.0.0.105,127.0.0.104,127.0.0.103,j2ee.instance.jirastudio.com-fisheye,${horde.initialdata.TRUSTED_PROXIES}');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.RoleMembershipCreatedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.RoleMembershipDeletedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.RoleUpdatedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.UserCreatedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.AutoUserCreatedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.UserDeletedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.UserUpdatedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.UserAttributeDeletedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.GroupCreatedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.GroupDeletedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.GroupMembershipCreatedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.ResetPasswordEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.UserAttributeStoredEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.UserCredentialUpdatedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.null', 'com.atlassian.notifier.crowd:build', '2');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.XMLRestoreFinishedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.GroupMembershipDeletedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.GroupUpdatedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.GroupAttributeStoredEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.GroupAttributeDeletedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.RoleCreatedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('plugin.Studio Crowd', 'notifier.subscriptions.RoleDeletedEvent', '#java.util.Properties
#${horde.initialdata.TIMESTAMP_VERBOSE}
subscription.2.url=https\\://${horde.initialdata.HOSTNAME}/source/plugins/servlet/crowdnotify
subscription.4.url=https\\://${horde.initialdata.HOSTNAME}/builds/plugins/servlet/crowdnotify
subscription.3.authenticationType=NONE
subscription.2.authenticationType=NONE
subscription.1.url=https\\://${horde.initialdata.HOSTNAME}/wiki/plugins/servlet/crowdnotify
subscription.3.url=https\\://${horde.initialdata.HOSTNAME}/plugins/servlet/crowdnotify
subscription.4.authenticationType=NONE
subscription.1.authenticationType=NONE
');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.prefix', '[Crowd]');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.sender', 'jira@${horde.initialdata.HOSTNAME}');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.host', 'smtp');


INSERT INTO public.cwd_property (property_key, property_name, property_value) VALUES ('crowd', 'mailserver.password', NULL);


INSERT INTO public.cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (98305, 65537, 32769, 'requiresPasswordChange', 'false', 'false');


INSERT INTO public.cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (98306, 65537, 32769, 'passwordLastChanged', '${horde.initialdata.TIMESTAMP_UNIX_MS}', '${horde.initialdata.TIMESTAMP_UNIX_MS}');


INSERT INTO public.cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (98307, 65538, 32769, 'requiresPasswordChange', 'false', 'false');


INSERT INTO public.cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (98308, 65538, 32769, 'passwordLastChanged', '${horde.initialdata.TIMESTAMP_UNIX_MS}', '${horde.initialdata.TIMESTAMP_UNIX_MS}');


INSERT INTO public.cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (98309, 65539, 32770, 'requiresPasswordChange', 'false', 'false');


INSERT INTO public.cwd_user_attribute (id, user_id, directory_id, attribute_name, attribute_value, attribute_lower_value) VALUES (98310, 65539, 32770, 'passwordLastChanged', '${horde.initialdata.TIMESTAMP_UNIX_MS}', '${horde.initialdata.TIMESTAMP_UNIX_MS}');


INSERT INTO public.cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (32769, '0', 'password_max_change_time');


INSERT INTO public.cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (32769, '', 'password_regex');


INSERT INTO public.cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (32769, 'atlassian-security', 'user_encryption_method');


INSERT INTO public.cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (32769, '0', 'password_history_count');


INSERT INTO public.cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (32769, '0', 'password_max_attempts');


INSERT INTO public.cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (32770, '0', 'password_max_change_time');


INSERT INTO public.cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (32770, '', 'password_regex');


INSERT INTO public.cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (32770, 'atlassian-security', 'user_encryption_method');


INSERT INTO public.cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (32770, '0', 'password_history_count');


INSERT INTO public.cwd_directory_attribute (directory_id, attribute_value, attribute_name) VALUES (32770, '0', 'password_max_attempts');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (2, '${horde.initialdata.HOSTNAME}');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (2, '127.0.0.1');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (2, 'localhost');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (3, '${horde.initialdata.HOSTNAME}');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (3, '127.0.0.1');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (3, 'localhost');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (4, '${horde.initialdata.HOSTNAME}');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (4, '127.0.0.1');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (4, 'localhost');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (5, '${horde.initialdata.HOSTNAME}');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (5, '127.0.0.1');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (5, 'localhost');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (6, '${horde.initialdata.HOSTNAME}');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (6, '127.0.0.1');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (6, 'localhost');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (7, '${horde.initialdata.HOSTNAME}');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (7, '127.0.0.1');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (7, 'localhost');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (8, '${horde.initialdata.HOSTNAME}');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (8, '127.0.0.1');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (8, 'localhost');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (9, '10.72.5.139');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (9, '${horde.initialdata.HOSTNAME}');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (9, '10.72.4.139');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (9, '10.72.6.139');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (9, '127.0.0.1');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (9, 'localhost');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (10, '${horde.initialdata.HOSTNAME}');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (10, '10.72.1.104');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (10, '127.0.0.1');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (10, 'localhost');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (10, '10.72.1.103');


INSERT INTO public.cwd_application_address (application_id, remote_address) VALUES (10, '10.72.1.102');


INSERT INTO public.hibernate_unique_key (next_hi) VALUES (8);


INSERT INTO public.cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (196609, 2, 32769, 'F', 0);


INSERT INTO public.cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (196610, 3, 32769, 'F', 0);


INSERT INTO public.cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (196611, 4, 32769, 'T', 0);


INSERT INTO public.cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (196612, 5, 32769, 'F', 0);


INSERT INTO public.cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (196613, 6, 32769, 'F', 0);


INSERT INTO public.cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (196614, 7, 32769, 'F', 0);


INSERT INTO public.cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (196615, 8, 32769, 'F', 0);


INSERT INTO public.cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (196616, 9, 32769, 'T', 0);


INSERT INTO public.cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (196617, 10, 32769, 'T', 0);


INSERT INTO public.cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (196618, 6, 32770, 'F', 1);


INSERT INTO public.cwd_app_dir_mapping (id, application_id, directory_id, allow_all, list_index) VALUES (196619, 7, 32770, 'T', 1);


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'CREATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'UPDATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'CREATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'DELETE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'DELETE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'DELETE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'CREATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'UPDATE_GROUP_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'UPDATE_ROLE_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'UPDATE_USER_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'UPDATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196609, 'UPDATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196610, 'CREATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196610, 'UPDATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196610, 'CREATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196610, 'DELETE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196610, 'DELETE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196610, 'DELETE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196610, 'CREATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196610, 'UPDATE_GROUP_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196610, 'UPDATE_ROLE_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196610, 'UPDATE_USER_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196610, 'UPDATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196610, 'UPDATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196611, 'CREATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196611, 'UPDATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196611, 'CREATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196611, 'DELETE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196611, 'DELETE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196611, 'DELETE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196611, 'CREATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196611, 'UPDATE_GROUP_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196611, 'UPDATE_ROLE_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196611, 'UPDATE_USER_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196611, 'UPDATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196611, 'UPDATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196613, 'UPDATE_USER_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196613, 'UPDATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196616, 'CREATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196616, 'UPDATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196616, 'CREATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196616, 'DELETE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196616, 'DELETE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196616, 'DELETE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196616, 'CREATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196616, 'UPDATE_GROUP_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196616, 'UPDATE_ROLE_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196616, 'UPDATE_USER_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196616, 'UPDATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196616, 'UPDATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196617, 'CREATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196617, 'UPDATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196617, 'CREATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196617, 'DELETE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196617, 'DELETE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196617, 'DELETE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196617, 'CREATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196617, 'UPDATE_GROUP_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196617, 'UPDATE_ROLE_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196617, 'UPDATE_USER_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196617, 'UPDATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196617, 'UPDATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196618, 'CREATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196618, 'UPDATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196618, 'CREATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196618, 'DELETE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196618, 'DELETE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196618, 'DELETE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196618, 'CREATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196618, 'UPDATE_GROUP_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196618, 'UPDATE_ROLE_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196618, 'UPDATE_USER_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196618, 'UPDATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196618, 'UPDATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196619, 'CREATE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196619, 'UPDATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196619, 'CREATE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196619, 'DELETE_GROUP');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196619, 'DELETE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196619, 'DELETE_ROLE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196619, 'CREATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196619, 'UPDATE_GROUP_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196619, 'UPDATE_ROLE_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196619, 'UPDATE_USER_ATTRIBUTE');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196619, 'UPDATE_USER');


INSERT INTO public.cwd_app_dir_operation (app_dir_mapping_id, operation_type) VALUES (196619, 'UPDATE_GROUP');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32769, 'CREATE_GROUP');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32769, 'DELETE_USER');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32769, 'DELETE_GROUP');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32769, 'CREATE_USER');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32769, 'UPDATE_USER');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32769, 'UPDATE_GROUP');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32769, 'UPDATE_GROUP_ATTRIBUTE');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32769, 'UPDATE_USER_ATTRIBUTE');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32770, 'UPDATE_ROLE');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32770, 'CREATE_GROUP');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32770, 'DELETE_GROUP');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32770, 'DELETE_USER');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32770, 'CREATE_ROLE');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32770, 'DELETE_ROLE');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32770, 'CREATE_USER');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32770, 'UPDATE_ROLE_ATTRIBUTE');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32770, 'UPDATE_GROUP_ATTRIBUTE');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32770, 'UPDATE_USER_ATTRIBUTE');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32770, 'UPDATE_GROUP');


INSERT INTO public.cwd_directory_operation (directory_id, operation_type) VALUES (32770, 'UPDATE_USER');


INSERT INTO public.cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (1, 'true', 'atlassian_sha1_applied');


INSERT INTO public.cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (2, 'true', 'atlassian_sha1_applied');


INSERT INTO public.cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (3, 'true', 'atlassian_sha1_applied');


INSERT INTO public.cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (4, 'true', 'atlassian_sha1_applied');


INSERT INTO public.cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (5, 'true', 'atlassian_sha1_applied');


INSERT INTO public.cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (6, 'true', 'atlassian_sha1_applied');


INSERT INTO public.cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (7, 'true', 'atlassian_sha1_applied');


INSERT INTO public.cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (8, 'true', 'atlassian_sha1_applied');


INSERT INTO public.cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (9, 'true', 'atlassian_sha1_applied');


INSERT INTO public.cwd_application_attribute (application_id, attribute_value, attribute_name) VALUES (10, 'true', 'atlassian_sha1_applied');


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163841, 131073, 65537, 'GROUP_USER', 'GROUP', 'system-administrators', 'system-administrators', 'sysadmin', 'sysadmin', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163842, 131076, 65537, 'GROUP_USER', 'GROUP', 'users', 'users', 'sysadmin', 'sysadmin', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163843, 131077, 65537, 'GROUP_USER', 'GROUP', '_licensed-jira', '_licensed-jira', 'sysadmin', 'sysadmin', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163844, 131078, 65537, 'GROUP_USER', 'GROUP', '_licensed-confluence', '_licensed-confluence', 'sysadmin', 'sysadmin', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163845, 131079, 65537, 'GROUP_USER', 'GROUP', '_licensed-fecru', '_licensed-fecru', 'sysadmin', 'sysadmin', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163846, 131080, 65537, 'GROUP_USER', 'GROUP', '_licensed-bamboo', '_licensed-bamboo', 'sysadmin', 'sysadmin', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163847, 131074, 65538, 'GROUP_USER', 'GROUP', 'administrators', 'administrators', '${horde.initialdata.ADMIN_USER_NAME}', '${horde.initialdata.ADMIN_LOWER_USER_NAME}', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163848, 131075, 65538, 'GROUP_USER', 'GROUP', 'developers', 'developers', '${horde.initialdata.ADMIN_USER_NAME}', '${horde.initialdata.ADMIN_LOWER_USER_NAME}', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163849, 131076, 65538, 'GROUP_USER', 'GROUP', 'users', 'users', '${horde.initialdata.ADMIN_USER_NAME}', '${horde.initialdata.ADMIN_LOWER_USER_NAME}', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163850, 131077, 65538, 'GROUP_USER', 'GROUP', '_licensed-jira', '_licensed-jira', '${horde.initialdata.ADMIN_USER_NAME}', '${horde.initialdata.ADMIN_LOWER_USER_NAME}', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163851, 131078, 65538, 'GROUP_USER', 'GROUP', '_licensed-confluence', '_licensed-confluence', '${horde.initialdata.ADMIN_USER_NAME}', '${horde.initialdata.ADMIN_LOWER_USER_NAME}', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163852, 131079, 65538, 'GROUP_USER', 'GROUP', '_licensed-fecru', '_licensed-fecru', '${horde.initialdata.ADMIN_USER_NAME}', '${horde.initialdata.ADMIN_LOWER_USER_NAME}', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163853, 131080, 65538, 'GROUP_USER', 'GROUP', '_licensed-bamboo', '_licensed-bamboo', '${horde.initialdata.ADMIN_USER_NAME}', '${horde.initialdata.ADMIN_LOWER_USER_NAME}', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163854, 131081, 65537, 'GROUP_USER', 'GROUP', 'confluence-administrators', 'confluence-administrators', 'sysadmin', 'sysadmin', 32769);


INSERT INTO public.cwd_membership (id, parent_id, child_id, membership_type, group_type, parent_name, lower_parent_name, child_name, lower_child_name, directory_id) VALUES (163855, 131083, 65538, 'GROUP_USER', 'GROUP', 'balsamiq-mockups-editors', 'balsamiq-mockups-editors', '${horde.initialdata.ADMIN_USER_NAME}', '${horde.initialdata.ADMIN_LOWER_USER_NAME}', 32769);


INSERT INTO public.cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (131073, 'system-administrators', 'system-administrators', 'T', 'F', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', NULL, 'GROUP', 32769);


INSERT INTO public.cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (131074, 'administrators', 'administrators', 'T', 'F', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', NULL, 'GROUP', 32769);


INSERT INTO public.cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (131075, 'developers', 'developers', 'T', 'F', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', NULL, 'GROUP', 32769);


INSERT INTO public.cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (131076, 'users', 'users', 'T', 'F', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', NULL, 'GROUP', 32769);


INSERT INTO public.cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (131077, '_licensed-jira', '_licensed-jira', 'T', 'F', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', NULL, 'GROUP', 32769);


INSERT INTO public.cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (131078, '_licensed-confluence', '_licensed-confluence', 'T', 'F', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', NULL, 'GROUP', 32769);


INSERT INTO public.cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (131079, '_licensed-fecru', '_licensed-fecru', 'T', 'F', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', NULL, 'GROUP', 32769);


INSERT INTO public.cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (131080, '_licensed-bamboo', '_licensed-bamboo', 'T', 'F', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', NULL, 'GROUP', 32769);


INSERT INTO public.cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (131081, 'confluence-administrators', 'confluence-administrators', 'T', 'F', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', NULL, 'GROUP', 32769);


INSERT INTO public.cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (131082, '_no-one-at-all', '_no-one-at-all', 'T', 'F', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', NULL, 'GROUP', 32769);


INSERT INTO public.cwd_group (id, group_name, lower_group_name, active, is_local, created_date, updated_date, description, group_type, directory_id) VALUES (131083, 'balsamiq-mockups-editors', 'balsamiq-mockups-editors', 'T', 'F', '${horde.initialdata.TIMESTAMP_ISO8601}', '${horde.initialdata.TIMESTAMP_ISO8601}', NULL, 'GROUP', 32769);


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229377, 196609, 2, 32769, 'system-administrators');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229378, 196610, 3, 32769, 'system-administrators');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229379, 196610, 3, 32769, '_licensed-jira');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229380, 196610, 3, 32769, '_licensed-bamboo');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229381, 196610, 3, 32769, '_licensed-fecru');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229382, 196610, 3, 32769, '_licensed-confluence');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229383, 196612, 5, 32769, 'system-administrators');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229384, 196612, 5, 32769, '_licensed-fecru');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229385, 196613, 6, 32769, 'system-administrators');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229386, 196613, 6, 32769, '_licensed-bamboo');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229387, 196614, 7, 32769, 'system-administrators');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229388, 196614, 7, 32769, '_licensed-fecru');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229389, 196615, 8, 32769, 'system-administrators');


INSERT INTO public.cwd_app_dir_group_mapping (id, app_dir_mapping_id, application_id, directory_id, group_name) VALUES (229390, 196615, 8, 32769, 'administrators');


