package com.atlassian.crowd.horde.tenantsetup.propertygenerators;

public class PropertyGenerationException extends Exception
{
    public PropertyGenerationException(String s)
    {
        super(s);
    }

    public PropertyGenerationException(String s, Throwable throwable)
    {
        super(s, throwable);
    }
}
