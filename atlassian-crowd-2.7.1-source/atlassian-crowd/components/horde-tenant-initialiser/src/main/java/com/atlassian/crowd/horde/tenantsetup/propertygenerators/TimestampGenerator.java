package com.atlassian.crowd.horde.tenantsetup.propertygenerators;

import java.util.Map;

import com.atlassian.crowd.horde.tenantsetup.Config;

import com.google.common.collect.ImmutableMap;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

public class TimestampGenerator implements PropertiesGenerator
{
    // ISO8601: 2013-07-18T13:30:32.143
    private final static DateTimeFormatter ISO8601_FORMATTER = ISODateTimeFormat.dateHourMinuteSecondMillis();

    // Verbose: Thu Jul 18 13:43:45 EST 2013
    private final static DateTimeFormatter VERBOSE_FORMATTER = DateTimeFormat.forPattern("EEE MMM dd HH:mm:ss zzz yyyy");

    @Override
    public Map<String, String> generate(Config config)
    {
        long now = System.currentTimeMillis();
        String isoTimeStamp = ISO8601_FORMATTER.print(now);
        if (config.getDbUrl().contains("hsqldb"))
        {
            // HACK for hsqldb
            isoTimeStamp = isoTimeStamp.replace('T', ' ');
        }
        String verboseTimestamp = VERBOSE_FORMATTER.print(now);

        return ImmutableMap.of("TIMESTAMP_ISO8601", isoTimeStamp,
                               "TIMESTAMP_VERBOSE", verboseTimestamp,
                               "TIMESTAMP_UNIX_MS", Long.toString(now));
    }
}
