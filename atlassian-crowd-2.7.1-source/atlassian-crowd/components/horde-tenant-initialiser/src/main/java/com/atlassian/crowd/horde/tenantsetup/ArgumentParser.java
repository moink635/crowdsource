package com.atlassian.crowd.horde.tenantsetup;

import java.util.Arrays;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.crowd.horde.tenantsetup.Config.DEFAULT_CHANGELOG_FILE;
import static com.atlassian.crowd.horde.tenantsetup.Config.DEFAULT_DB_DRIVER;
import static com.atlassian.crowd.horde.tenantsetup.Config.DEFAULT_DB_URL;
import static com.atlassian.crowd.horde.tenantsetup.Config.DEFAULT_DB_USERNAME;
import static com.atlassian.crowd.horde.tenantsetup.Config.DEFAULT_LIQUIBASE_LOGLEVEL;
import static com.atlassian.crowd.horde.tenantsetup.Config.DEFAULT_SQL_FILE;

@SuppressWarnings("static-access")
public class ArgumentParser
{
    private static final Logger log = LoggerFactory.getLogger(ArgumentParser.class);

    static final String DB_URL_OPTION_NAME = "dbUrl";
    static final String DRY_RUN_OPTION_NAME = "dryRun";
    static final String DRY_RUN_OUTPUT_FILE_OPTION_NAME = "dryRunOutputFile";
    static final String APPLICATION_PASSWORD_OPTION_NAME = "applicationPassword";
    static final String LIQUIBASE_LOG_LEVEL_OPTION_NAME = "liquibaseLogLevel";
    static final String CHANGE_LOG_FILE_OPTION_NAME = "changeLogFile";
    static final String DB_DRIVER_OPTION_NAME = "dbDriver";
    static final String DB_PASSWORD_OPTION_NAME = "dbPassword";
    static final String DB_USERNAME_OPTION_NAME = "dbUsername";
    static final String INIT_XML_PATH_OPTION_NAME = "initXmlPath";
    static final String SQL_FILE_OPTION_NAME = "sqlFile";

    static final String DEFAULT_INIT_XML = "/data/jirastudio/home/init.xml";

    private final CommandLineParser cmdLineParser = new PosixParser();

    private final Option dbUrl = OptionBuilder
        .withArgName(DB_URL_OPTION_NAME)
        .isRequired(false)
        .hasArg()
        .withDescription("Database URL. Default: " + DEFAULT_DB_URL)
        .withLongOpt(DB_URL_OPTION_NAME)
        .create("l");

    private final Option dbUsername = OptionBuilder
        .withArgName(DB_USERNAME_OPTION_NAME)
        .isRequired(false)
        .hasArg()
        .withDescription("Database user name. Default: " + DEFAULT_DB_USERNAME)
        .withLongOpt(DB_USERNAME_OPTION_NAME)
        .create("u");

    private final Option dbPassword = OptionBuilder
        .withArgName(DB_PASSWORD_OPTION_NAME)
        .isRequired(true)
        .hasArg()
        .withDescription("Database password. Required")
        .withLongOpt(DB_PASSWORD_OPTION_NAME)
        .create("p");

    private final Option dbDriver = OptionBuilder
        .withArgName(DB_DRIVER_OPTION_NAME)
        .isRequired(false)
        .hasArg()
        .withDescription("Database driver. Default: " + DEFAULT_DB_DRIVER)
        .withLongOpt(DB_DRIVER_OPTION_NAME)
        .create("r");

    private final Option changeLogFile = OptionBuilder
        .withArgName(CHANGE_LOG_FILE_OPTION_NAME)
        .isRequired(false)
        .hasArg()
        .withDescription("Liquibase changelog file. Default: " + DEFAULT_CHANGELOG_FILE)
        .withLongOpt(CHANGE_LOG_FILE_OPTION_NAME)
        .create("f");

    private final Option liquibaseLogLevel = OptionBuilder
        .withArgName(LIQUIBASE_LOG_LEVEL_OPTION_NAME)
        .isRequired(false)
        .hasArg()
        .withDescription("Liquibase log level. Valid values: "
                         + StringUtils.join(liquibase.logging.LogLevel.values(), ", ")
                         + ". Default: " + DEFAULT_LIQUIBASE_LOGLEVEL)
        .withLongOpt(LIQUIBASE_LOG_LEVEL_OPTION_NAME)
        .create("v");

    private final Option applicationPassword = OptionBuilder
        .withArgName(APPLICATION_PASSWORD_OPTION_NAME)
        .isRequired(true)
        .hasArg()
        .withDescription("Application password. Required")
        .withLongOpt(APPLICATION_PASSWORD_OPTION_NAME)
        .create("a");

    private final Option initXmlPath = OptionBuilder
        .withArgName(INIT_XML_PATH_OPTION_NAME)
        .isRequired(false)
        .hasArg()
        .withDescription("Path to init.xml. Default: ")
        .withLongOpt(INIT_XML_PATH_OPTION_NAME)
        .create("i");

    private final Option dryRun = OptionBuilder
        .withArgName(DRY_RUN_OPTION_NAME)
        .isRequired(false)
        .hasArg(false)
        .withDescription("Dry run: don't update the database, just print sql. Disabled by default.")
        .withLongOpt(DRY_RUN_OPTION_NAME)
        .create("d");

    private final Option dryRunOutputFile = OptionBuilder
        .withArgName(DRY_RUN_OUTPUT_FILE_OPTION_NAME)
        .isRequired(false)
        .hasArg(true)
        .withDescription("Path of file to which send output when in dry run mode. Defaults to stdout.")
        .withLongOpt(DRY_RUN_OUTPUT_FILE_OPTION_NAME)
        .create("o");

    private final Option sqlFile = OptionBuilder
        .withArgName(SQL_FILE_OPTION_NAME)
        .isRequired(false)
        .hasArg()
        .withDescription("Path to initial data SQL file read from classpath. Default: " + DEFAULT_SQL_FILE)
        .withLongOpt(SQL_FILE_OPTION_NAME)
        .create("s");

    Options options()
    {
        return new Options()
            .addOption(dbUrl)
            .addOption(dbUsername)
            .addOption(dbPassword)
            .addOption(dbDriver)
            .addOption(changeLogFile)
            .addOption(liquibaseLogLevel)
            .addOption(applicationPassword)
            .addOption(initXmlPath)
            .addOption(dryRun)
            .addOption(dryRunOutputFile)
            .addOption(sqlFile);
    }

    public Config parse(String[] args) throws ParseException
    {
        log.debug("Parsing Arguments %s", Arrays.toString(args));

        CommandLine parser = cmdLineParser.parse(options(), args);
        return new Config(parser.getOptionValue(dbUrl.getOpt())
                          , parser.getOptionValue(dbUsername.getOpt())
                          , parser.getOptionValue(dbPassword.getOpt())
                          , parser.getOptionValue(dbDriver.getOpt())
                          , parser.getOptionValue(changeLogFile.getOpt())
                          , parser.getOptionValue(liquibaseLogLevel.getOpt())
                          , parser.getOptionValue(applicationPassword.getOpt())
                          , parser.getOptionValue(initXmlPath.getOpt(), DEFAULT_INIT_XML)
                          , parser.hasOption(dryRun.getOpt())
                          , parser.getOptionValue(dryRunOutputFile.getOpt())
                          , parser.getOptionValue(sqlFile.getOpt()));
    }

}
