package com.atlassian.crowd.horde.tenantsetup.propertygenerators;

import java.util.Map;

import com.atlassian.crowd.horde.tenantsetup.Config;

public interface PropertiesGenerator
{
    Map<String, String> generate(Config config) throws PropertyGenerationException;
}
