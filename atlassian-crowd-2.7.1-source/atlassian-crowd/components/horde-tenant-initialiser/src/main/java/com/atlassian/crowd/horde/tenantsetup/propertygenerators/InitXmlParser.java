package com.atlassian.crowd.horde.tenantsetup.propertygenerators;

import java.io.File;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.atlassian.crowd.horde.tenantsetup.Config;
import com.atlassian.crowd.horde.tenantsetup.propertygenerators.initxmlmodel.InitialData;
import com.atlassian.crowd.horde.tenantsetup.propertygenerators.initxmlmodel.InitialUser;

import com.google.common.collect.ImmutableMap;

import org.apache.commons.lang3.StringUtils;

import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

public class InitXmlParser implements PropertiesGenerator
{
    static final String ADMINISTRATOR_DISPLAY_NAME_SUFFFIX = " [Administrator]";
    static final String DEFAULT_ADMIN_DISPLAY_NAME = "Administrator";

    @Override
    public Map<String, String> generate(Config config) throws PropertyGenerationException
    {
        InitialData initialData = loadInitialData(config);

        return ImmutableMap.<String, String>builder()
            .put("SYSADMIN_CREDENTIAL", initialData.getUsers().getSysadmin().getHash())
            .put("TRUSTED_PROXIES", StringUtils.join(initialData.getCrowdTrustedProxies(), ','))
            .putAll(getAdminProperties(initialData))
            .build();
    }

    private InitialData loadInitialData(Config config) throws PropertyGenerationException
    {
        File xml = new File(config.getInitXmlPath());
        // Need to ensure we are in the right context classloader to initialise jaxb
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try
        {
            Thread.currentThread().setContextClassLoader(InitXmlParser.class.getClassLoader());
            JAXBContext context = JAXBContext.newInstance(InitialData.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            Object object = unmarshaller.unmarshal(xml);
            if (object instanceof InitialData)
            {
                return (InitialData) object;
            }
            else
            {
                throw new PropertyGenerationException("Unknown unmarshalled object in init.xml: " + object.toString());
            }
        }
        catch (JAXBException e)
        {
            throw new PropertyGenerationException("Error parsing init.xml file", e);
        }
        finally
        {
            Thread.currentThread().setContextClassLoader(classLoader);
        }
    }

    private static Map<String, String> getAdminProperties(InitialData initialData) throws PropertyGenerationException
    {
        InitialUser admin = initialData.getUsers().getAdmin();

        String adminFirstname = trimToEmpty(admin.getFirstname());
        String adminLastname = trimToEmpty(admin.getLastname());
        if (isBlank(adminFirstname) && isBlank(adminLastname))
        {
            // If both names are blank, use default name as first name and leave last name blank. Don't append suffix.
            adminFirstname = DEFAULT_ADMIN_DISPLAY_NAME;
        }
        else
        {
            // Else append suffix to last name.
            adminLastname += ADMINISTRATOR_DISPLAY_NAME_SUFFFIX;
        }

        String adminDisplayName = trimToEmpty(adminFirstname + ' ' + adminLastname);

        return ImmutableMap.<String, String> builder().put("ADMIN_USER_NAME", getValidatedUsername(admin))
                           .put("ADMIN_LOWER_USER_NAME", getValidatedUsername(admin).toLowerCase())
                           .put("ADMIN_FIRST_NAME", adminFirstname)
                           .put("ADMIN_LOWER_FIRST_NAME", adminFirstname.toLowerCase())
                           .put("ADMIN_LAST_NAME", adminLastname)
                           .put("ADMIN_LOWER_LAST_NAME", adminLastname.toLowerCase())
                           .put("ADMIN_DISPLAY_NAME", adminDisplayName)
                           .put("ADMIN_LOWER_DISPLAY_NAME", adminDisplayName.toLowerCase())
                           .put("ADMIN_CREDENTIAL", admin.getHash())
                           .put("ADMIN_EMAIL", admin.getEmail())
                           .put("ADMIN_LOWER_EMAIL", admin.getEmail().toLowerCase())
                           .build();
    }

    private static String getValidatedUsername(InitialUser admin) throws PropertyGenerationException
    {
        final String adminUsername = trimToEmpty(admin.getUsername());

        if (StringUtils.isBlank(adminUsername))
        {
            throw new PropertyGenerationException("The admin username specified in init.xml cannot be blank.");
        }
        if ("sysadmin".equals(adminUsername))
        {
            throw new PropertyGenerationException("The admin username specified in init.xml cannot be 'sysadmin'.");
        }

        return adminUsername;
    }

}
