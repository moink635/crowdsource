package com.atlassian.crowd.horde.tenantsetup.propertygenerators.initxmlmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The initial data supplied to an on demand instance by HAL
 */
@XmlRootElement(name = "initial-data")
@XmlAccessorType(XmlAccessType.FIELD)
public class InitialData
{

    InitialData()
    {
    }

    public InitialData(InitialUsers users, String crowdTrustedProxies)
    {
        this.users = users;
        this.crowdTrustedProxies = crowdTrustedProxies;
    }

    @XmlAttribute(name = "crowd-trusted-proxies")
    private String crowdTrustedProxies;

    private InitialUsers users;

    public InitialUsers getUsers()
    {
        return users;
    }

    /**
     * HAL will pass the list of crowd trusted proxies to studio as a comma-separated string of one or more addresses.
     * This method splits this string, and returns an array of addresses.
     *
     * @return a list of the crowd trusted proxy addresses (as strings). This may be a list of one or more addresses.
     */
    public String[] getCrowdTrustedProxies()
    {
        if (crowdTrustedProxies == null)
        {
            return new String[0];
        }
        return crowdTrustedProxies.split(",");
    }

}
