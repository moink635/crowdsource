package com.atlassian.crowd.horde.tenantsetup.propertygenerators;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.atlassian.crowd.horde.tenantsetup.Config;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

public class PropertiesManager
{
    // Use this prefix on any sysprops used for Liquibase placeholder substitutions.
    public static final String PLACEHOLDER_SYSPROP_PREFIX = "horde.initialdata.";

    private final List<PropertiesGenerator> propertiesGenerators;

    public PropertiesManager()
    {
        this.propertiesGenerators = new LinkedList<PropertiesGenerator>();
    }

    public PropertiesManager addGenerator(PropertiesGenerator propertiesGenerator)
    {
        propertiesGenerators.add(propertiesGenerator);
        return this;
    }

    public Map<String, String> generateProperties(Config config) throws PropertyGenerationException
    {
        Builder<String, String> allProperties = ImmutableMap.builder();

        for (PropertiesGenerator propertiesGenerator : propertiesGenerators)
        {
            allProperties.putAll(propertiesGenerator.generate(config));
        }

        return allProperties.build();
    }
}
