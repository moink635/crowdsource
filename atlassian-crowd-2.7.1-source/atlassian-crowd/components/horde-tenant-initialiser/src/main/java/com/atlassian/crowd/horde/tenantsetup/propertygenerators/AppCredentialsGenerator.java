package com.atlassian.crowd.horde.tenantsetup.propertygenerators;

import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.horde.tenantsetup.Config;
import com.atlassian.security.password.DefaultPasswordEncoder;
import com.atlassian.security.password.PasswordEncoder;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;
import com.google.common.collect.ImmutableSet;

public class AppCredentialsGenerator implements PropertiesGenerator
{

    private static final Set<String> APP_CREDENTIAL_KEYS = ImmutableSet.of("APP_CREDENTIAL_GOOGLE_APPS",
                                                                           "APP_CREDENTIAL_CROWD",
                                                                           "APP_CREDENTIAL_JIRA",
                                                                           "APP_CREDENTIAL_CONFLUENCE",
                                                                           "APP_CREDENTIAL_CRUCIBLE",
                                                                           "APP_CREDENTIAL_WEBDAV",
                                                                           "APP_CREDENTIAL_INDRA",
                                                                           "APP_CREDENTIAL_AGNI",
                                                                           "APP_CREDENTIAL_BAMBOO",
                                                                           "APP_CREDENTIAL_SVN");

    @Override
    public Map<String, String> generate(Config config)
    {
        final String defaultApplicationPassword = config.getApplicationPassword();
        final PasswordEncoder defaultPasswordEncoder = DefaultPasswordEncoder.getDefaultInstance();

        Builder<String, String> appCredentials = ImmutableMap.<String, String>builder();
        for (String appCredentialKey : APP_CREDENTIAL_KEYS)
        {
            String password = defaultPasswordEncoder.encodePassword(defaultApplicationPassword);
            appCredentials.put(appCredentialKey, password);
        }

        return appCredentials.build();
    }
}
