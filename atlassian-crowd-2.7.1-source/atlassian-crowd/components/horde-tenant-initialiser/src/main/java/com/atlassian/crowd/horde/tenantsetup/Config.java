package com.atlassian.crowd.horde.tenantsetup;

import static org.apache.commons.lang3.StringUtils.defaultIfEmpty;

public class Config
{
    public static final String DEFAULT_DB_URL = "jdbc:postgresql://localhost:5432/crowd";
    public static final String DEFAULT_DB_USERNAME = "crowd";
    public static final String DEFAULT_DB_DRIVER = "org.postgresql.Driver";
    public static final String DEFAULT_CHANGELOG_FILE = "liquibase/master.xml";
    public static final String DEFAULT_LIQUIBASE_LOGLEVEL = "warning";
    public static final int DEFAULT_APPLICATION_PASSWORD_MIN_LENGTH = 6;
    public static final String DEFAULT_SQL_FILE = "data/initial-data.sql";

    private final String dbUrl;
    private final String dbUsername;
    private final String dbPassword;
    private final String dbDriver;
    private final String changeLogFile;
    private final String liquibaseLogLevel;
    private final String applicationPassword;
    private final String initXmlPath;
    private final boolean dryRun;
    private final String dryRunOutputFile;
    private final String sqlFile;

    public Config(String dbUrl, String dbUsername, String dbPassword, String dbDriver, String changeLogFile, String liquibaseLogLevel,
        String applicationPassword, String initXmlPath, boolean dryRun, String dryRunOutputFile, String sqlFile)
    {
        this.dbUrl = defaultIfEmpty(dbUrl, DEFAULT_DB_URL);
        this.dbUsername = defaultIfEmpty(dbUsername, DEFAULT_DB_USERNAME);
        this.dbDriver = defaultIfEmpty(dbDriver, DEFAULT_DB_DRIVER);
        this.changeLogFile = defaultIfEmpty(changeLogFile, DEFAULT_CHANGELOG_FILE);
        this.liquibaseLogLevel = defaultIfEmpty(liquibaseLogLevel, DEFAULT_LIQUIBASE_LOGLEVEL);
        this.sqlFile = defaultIfEmpty(sqlFile, DEFAULT_SQL_FILE);

        this.initXmlPath = initXmlPath;
        this.dbPassword = dbPassword;
        this.applicationPassword = applicationPassword;
        this.dryRun = dryRun;
        this.dryRunOutputFile = dryRunOutputFile;
    }

    public String getDbUrl()
    {
        return dbUrl;
    }

    public String getDbUsername()
    {
        return dbUsername;
    }

    public String getDbPassword()
    {
        return dbPassword;
    }

    public String getDbDriver()
    {
        return dbDriver;
    }

    public String getChangeLogFile()
    {
        return changeLogFile;
    }

    public String getLiquibaseLogLevel()
    {
        return liquibaseLogLevel;
    }

    public String getApplicationPassword()
    {
        return applicationPassword;
    }

    public String getInitXmlPath()
    {
        return initXmlPath;
    }

    public boolean isDryRun()
    {
        return dryRun;
    }

    public String getDryRunOutputFile()
    {
        return dryRunOutputFile;
    }

    public String getSqlFile()
    {
        return sqlFile;
    }
}
