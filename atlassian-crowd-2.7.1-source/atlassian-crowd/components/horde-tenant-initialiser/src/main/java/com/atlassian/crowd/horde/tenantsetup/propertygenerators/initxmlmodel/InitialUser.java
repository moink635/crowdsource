package com.atlassian.crowd.horde.tenantsetup.propertygenerators.initxmlmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * An initial data user
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class InitialUser
{
    InitialUser()
    {
    }

    @XmlAttribute
    private String username;
    @XmlAttribute
    private String email;
    @XmlAttribute
    private String firstname;
    @XmlAttribute
    private String lastname;
    @XmlAttribute
    private String hash;
    @XmlAttribute
    private String openid;

    public String getUsername()
    {
        return username;
    }

    public String getEmail()
    {
        return email;
    }

    public String getFirstname()
    {
        return firstname;
    }

    public String getLastname()
    {
        return lastname;
    }

    public String getHash()
    {
        return hash;
    }

    /**
     * Used for logging in using HAL, only should be on sysadmin
     *
     * @return The users openid
     */
    public String getOpenId()
    {
        return openid;
    }
}
