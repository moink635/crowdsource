package com.atlassian.crowd.horde.tenantsetup.propertygenerators;

import java.security.NoSuchAlgorithmException;
import java.util.Map;

import javax.crypto.KeyGenerator;

import com.atlassian.crowd.horde.tenantsetup.Config;

import com.google.common.collect.ImmutableMap;

import org.apache.commons.codec.binary.Base64;

public class DESKeyGenerator implements PropertiesGenerator
{
    @Override
    public Map<String, String> generate(Config config) throws PropertyGenerationException
    {

        byte[] encodedKey;
        try
        {
            encodedKey = KeyGenerator.getInstance("DES").generateKey().getEncoded();
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new PropertyGenerationException("Could not generate DES encryption key.", e);
        }

        if (encodedKey == null)
        {
            throw new PropertyGenerationException("Failed to generate DES encryption key: encoded key is null (failed to encode).");
        }

        String newDesKey = Base64.encodeBase64String(encodedKey);
        return ImmutableMap.of("DES_ENCRYPTION_KEY", newDesKey);
    }
}
