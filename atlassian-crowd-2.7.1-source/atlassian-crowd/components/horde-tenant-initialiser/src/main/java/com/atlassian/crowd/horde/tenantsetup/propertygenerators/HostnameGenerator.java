package com.atlassian.crowd.horde.tenantsetup.propertygenerators;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

import com.atlassian.crowd.horde.tenantsetup.Config;

import com.google.common.collect.ImmutableMap;

public class HostnameGenerator implements PropertiesGenerator
{

    @Override
    public Map<String, String> generate(Config config) throws PropertyGenerationException
    {
        try
        {
            return ImmutableMap.of("HOSTNAME", InetAddress.getLocalHost().getHostName());
        }
        catch (UnknownHostException e)
        {
            throw new PropertyGenerationException("Could not establish hostname", e);
        }
    }

}
