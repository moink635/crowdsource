package com.atlassian.crowd.horde.tenantsetup;

import java.sql.Connection;
import java.sql.Driver;
import java.util.Properties;

import liquibase.logging.LogFactory;

import liquibase.Liquibase;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import liquibase.resource.CompositeResourceAccessor;
import liquibase.resource.FileSystemResourceAccessor;

public class LiquibaseFactory
{
    public Liquibase newLiquibase(Config config) throws LiquibaseException
    {
        LogFactory.setLoggingLevel(config.getLiquibaseLogLevel());

        try
        {
            Driver driver = (Driver) Class.forName(config.getDbDriver()).newInstance();
            Properties info = new Properties();
            info.put("user", config.getDbUsername());
            info.put("password", config.getDbPassword());
            Connection connection = driver.connect(config.getDbUrl(), info);

            CompositeResourceAccessor resourceAccessor = new CompositeResourceAccessor(new FileSystemResourceAccessor(), new ClassLoaderResourceAccessor());
            JdbcConnection jdbcConnection = new JdbcConnection(connection);
            Liquibase liquibase = new Liquibase(config.getChangeLogFile(), resourceAccessor, jdbcConnection);
            return liquibase;
        }
        catch (Exception e)
        {
            throw new LiquibaseException("Could not initialise Liquibase", e);
        }
    }
}
