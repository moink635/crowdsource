package com.atlassian.crowd.horde.tenantsetup.propertygenerators;

import java.util.Map;

import com.atlassian.crowd.horde.tenantsetup.Config;

import com.google.common.collect.ImmutableMap;

public class SqlFileGenerator implements PropertiesGenerator
{
    @Override
    public Map<String, String> generate(Config config) throws PropertyGenerationException
    {
        return ImmutableMap.of("SQL_FILE", config.getSqlFile());
    }
}
