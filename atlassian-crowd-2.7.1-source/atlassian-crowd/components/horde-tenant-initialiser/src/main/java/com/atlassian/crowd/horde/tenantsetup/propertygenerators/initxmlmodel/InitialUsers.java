package com.atlassian.crowd.horde.tenantsetup.propertygenerators.initxmlmodel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * The initial users configuration
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class InitialUsers
{
    InitialUsers()
    {
    }

    @XmlElement
    private InitialUser admin;
    @XmlElement
    private InitialUser sysadmin;

    public InitialUser getAdmin()
    {
        return admin;
    }

    public InitialUser getSysadmin()
    {
        return sysadmin;
    }
}
