package com.atlassian.crowd.horde.tenantsetup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.Map;

import com.atlassian.crowd.horde.tenantsetup.propertygenerators.AppCredentialsGenerator;
import com.atlassian.crowd.horde.tenantsetup.propertygenerators.DESKeyGenerator;
import com.atlassian.crowd.horde.tenantsetup.propertygenerators.HostnameGenerator;
import com.atlassian.crowd.horde.tenantsetup.propertygenerators.InitXmlParser;
import com.atlassian.crowd.horde.tenantsetup.propertygenerators.PropertiesManager;
import com.atlassian.crowd.horde.tenantsetup.propertygenerators.PropertyGenerationException;
import com.atlassian.crowd.horde.tenantsetup.propertygenerators.SqlFileGenerator;
import com.atlassian.crowd.horde.tenantsetup.propertygenerators.TimestampGenerator;

import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.crowd.horde.tenantsetup.propertygenerators.PropertiesManager.PLACEHOLDER_SYSPROP_PREFIX;
import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;

/**
 * Initialises the data for and/or runs schema upgrades on a tenant.
 */
public class Main
{
    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws LiquibaseException
    {
        ArgumentParser argumentParser = new ArgumentParser();
        try
        {
            Config config = argumentParser.parse(args);
            runWith(config);
        }
        catch (ParseException e)
        {
            HelpFormatter formatter = new HelpFormatter();
            formatter.setWidth(120);
            formatter.printHelp(" ", " ", argumentParser.options(), e.getMessage());
        }
    }

    public static void runWith(Config config) throws LiquibaseException
    {
        log.info("Generating placeholder substitution values for Horde.");
        Map<String, String> hordeProperties = generateProperties(config);

        log.info("Copying placeholder substitution values to system properties, so that they are picked up by Liquibase:");
        for (Map.Entry<String, String> prop : hordeProperties.entrySet())
        {
            String syspropName = PLACEHOLDER_SYSPROP_PREFIX + prop.getKey();
            String value = prop.getValue();
            log.info("  {}={}", syspropName, value);
            System.setProperty(syspropName, value);
        }

        log.info("Running Liquibase to setup the database.");
        Liquibase liquibase = new LiquibaseFactory().newLiquibase(config);
        if (config.isDryRun())
        {
            log.info("Dry run mode: database will not be modified.");
            liquibase.update(null, getDryRunOutput(config));
        }
        else
        {
            liquibase.update(null);
        }

        log.info("Horde tenant initialiser has finished running.");
    }

    private static Map<String, String> generateProperties(Config config)
    {
        final PropertiesManager propertiesManager = new PropertiesManager();

        // Add in the property generators
        propertiesManager.addGenerator(new AppCredentialsGenerator());
        propertiesManager.addGenerator(new DESKeyGenerator());
        propertiesManager.addGenerator(new TimestampGenerator());
        propertiesManager.addGenerator(new HostnameGenerator());
        propertiesManager.addGenerator(new SqlFileGenerator());
        if (StringUtils.isNotEmpty(config.getInitXmlPath()))
        {
            propertiesManager.addGenerator(new InitXmlParser());
        }
        else
        {
            log.info("Skipping extraction of data from init.xml, as the path is unspecified.");
        }

        // Run all of the property generators
        try
        {
            return propertiesManager.generateProperties(config);
        }
        catch (PropertyGenerationException e)
        {
            throw new IllegalStateException("Failed to generate properties for Horde Initial Data.", e);
        }

    }

    private static OutputStreamWriter getDryRunOutput(Config config)
    {
        if (config.getDryRunOutputFile() == null)
        {
            log.info("No output file specified, writing to stdout.");
            return new OutputStreamWriter(System.out);
        }

        try
        {
            return new OutputStreamWriter(new FileOutputStream(new File(config.getDryRunOutputFile())));
        }
        catch (FileNotFoundException e)
        {
            log.warn("Could not open file {}, writing to stdout instead.", config.getDryRunOutputFile(), e);
            return new OutputStreamWriter(System.out);
        }
    }
}
