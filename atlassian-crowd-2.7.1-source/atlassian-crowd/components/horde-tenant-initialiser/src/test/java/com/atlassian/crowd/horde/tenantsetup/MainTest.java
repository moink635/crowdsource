package com.atlassian.crowd.horde.tenantsetup;

import java.io.File;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.common.base.Charsets;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class MainTest
{

    private static final String JDBC_HSQLDB_URL_PREFIX = "jdbc:hsqldb:mem:";
    private static final int MINIMUM_EXPECTED_NEW_TABLES = 11;
    private static final int MINIMUM_EXPECTED_NEW_PROPERTIES = 52;
    private static final Pattern UNSUBSTITUTED_PLACEHOLDER_PATTERN = Pattern.compile("(\\$\\{.*?\\})");
    private static final String DUMMY_INIT_XML_CONTENT = "<initial-data crowd-trusted-proxies=\"192.0.43.128/25\"\n"
                                                         + "               fisheye-admin-password=\"fake-password\"\n"
                                                         + "               hal-server=\"https://example.com\"\n"
                                                         + "               mail-server-password=\"fake-password\"\n"
                                                         + "               bamboo-svn-password=\"fake-password\">\n"
                                                         + "    <license>\n"
                                                         + "        dummy license\n"
                                                         + "    </license>\n"
                                                         + "    <users>\n"
                                                         + "        <admin username=\"admin\"\n"
                                                         + "              email=\"kirk.vanhouten@example.com\"\n"
                                                         + "              hash=\"fake-hash-1\"\n"
                                                         + "              firstname=\"Kirk\"\n"
                                                         + "              lastname=\"Van Houten\"/>\n"
                                                         + "        <sysadmin\n"
                                                         + "            hash=\"fake-hash-2\"\n"
                                                         + "            openid=\"https://example.com/oid\"/>\n"
                                                         + "    </users>\n"
                                                         + "    <trusted-apps>\n"
                                                         + "        <private-key>\n"
                                                         + "            dummy private key\n"
                                                         + "        </private-key>\n"
                                                         + "        <public-key>\n"
                                                         + "            dummy public key\n"
                                                         + "        </public-key>\n"
                                                         + "    </trusted-apps>\n"
                                                         + "</initial-data>";

    @Rule
    public TemporaryFolder outputFolder = new TemporaryFolder();

    private File dryRunOutputFile;
    private File dummyInitXml;


    @Before
    public void setup() throws Exception
    {
        dryRunOutputFile = outputFolder.newFile();
        dummyInitXml = outputFolder.newFile();
        FileUtils.write(dummyInitXml, DUMMY_INIT_XML_CONTENT, Charsets.UTF_8);
    }

    @Test
    public void shouldAddTablesToDatabase() throws Exception
    {
        Config config = createConfigForNewDb();
        int numTablesBeforeRun = countDatabaseTables(config);

        Main.runWith(config);

        int numAddedTables = countDatabaseTables(config) - numTablesBeforeRun;
        assertThat(numAddedTables, greaterThan(MINIMUM_EXPECTED_NEW_TABLES));
    }

    @Test
    public void shouldAddDataToTables() throws Exception
    {
        Config config = createConfigForNewDb();

        Main.runWith(config);

        int numProperties = countResultsForQuery("select * from cwd_property", config);
        assertThat(numProperties, greaterThanOrEqualTo(MINIMUM_EXPECTED_NEW_PROPERTIES));
    }

    @Test
    public void shouldRunMultipleTimesWithoutError() throws Exception
    {
        Config config = createConfigForNewDb();
        Main.runWith(config);
        Main.runWith(config);
    }

    @Test
    public void shouldNotAddTablesOnDryRun() throws Exception
    {
        Config config = createDryRunConfigForNewDb();
        int numTablesBeforeRun = countDatabaseTables(config);

        Main.runWith(config);

        int numAddedTables = countDatabaseTables(config) - numTablesBeforeRun;
        assertThat(numAddedTables, is(equalTo(0)));
    }

    @Test
    public void shouldPrintTableCreationQueriesOnDryRun() throws Exception
    {
        String dryRunSql = doDryRunAndCaptureOutput();

        int numAddedTables = StringUtils.countMatches(dryRunSql, "CREATE TABLE ");
        assertThat(numAddedTables, greaterThanOrEqualTo(MINIMUM_EXPECTED_NEW_TABLES));
    }

    @Test
    public void shouldLeaveNoUnsubstitutedPlaceholdersInExecutedSql() throws Exception
    {
        String dryRunSql = doDryRunAndCaptureOutput();

        Matcher m = UNSUBSTITUTED_PLACEHOLDER_PATTERN.matcher(dryRunSql);
        Set<String> unsubstitutedPlaceholders = new HashSet<String>();
        while (m.find())
        {
            unsubstitutedPlaceholders.add(m.group());
        }

        assertTrue("Found the following unsubstituted placeholders in dry run output: " + unsubstitutedPlaceholders, unsubstitutedPlaceholders.isEmpty());
    }

    private String doDryRunAndCaptureOutput() throws Exception
    {
        Config config = createDryRunConfigForNewDb();
        Main.runWith(config);

        String output = FileUtils.readFileToString(dryRunOutputFile, Charsets.UTF_8);
        return output;
    }

    private int countDatabaseTables(Config config) throws Exception
    {
        Connection c = getConnection(config);
        try
        {
            ResultSet rs = c.getMetaData().getTables(null, null, "%", null);
            rs.last();
            return rs.getRow();
        }
        finally
        {
            c.close();
        }
    }

    private Connection getConnection(Config config) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
    {
        Driver driver = (Driver) Class.forName(config.getDbDriver()).newInstance();
        Properties info = new Properties();
        info.put("user", config.getDbUsername());
        info.put("password", config.getDbPassword());
        Connection c = driver.connect(config.getDbUrl(), info);
        return c;
    }

    private int countResultsForQuery(String query, Config config) throws Exception
    {
        Connection c = getConnection(config);
        try
        {
            PreparedStatement p = c.prepareStatement(query);
            p.execute();
            ResultSet rs = p.getResultSet();
            int count = 0;
            while (rs.next())
            {
                count++;
            }
            return count;

        }
        finally
        {
            c.close();
        }

    }

    private Config createConfigForNewDb()
    {
        return createConfigForNewDb(false);
    }

    private Config createDryRunConfigForNewDb()
    {
        return createConfigForNewDb(true);
    }

    private Config createConfigForNewDb(boolean dryRun)
    {
        // Generate a unique dbUrl to guarantee a clean DB.
        String dbUrl = JDBC_HSQLDB_URL_PREFIX + UUID.randomUUID().toString();
        return new Config(dbUrl, "sa", "", "org.hsqldb.jdbcDriver", null, null, "fake-password", dummyInitXml.getAbsolutePath(), dryRun, dryRunOutputFile.getAbsolutePath(), null);
    }
}
