package com.atlassian.crowd.horde.tenantsetup;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;

import org.apache.commons.cli.MissingOptionException;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.crowd.horde.tenantsetup.ArgumentParser.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class ArgumentParserTest
{

    private static final String TEST_APPLICATION_PASSWORD = "test-applicationPassword";
    private static final String TEST_LIQUIBASE_LOG_LEVEL = "test-liquibaseLogLevel";
    private static final String TEST_CHANGE_LOG_FILE = "test-changeLogFile";
    private static final String TEST_DB_DRIVER = "test-dbDriver";
    private static final String TEST_DB_PASSWORD = "test-dbPassword";
    private static final String TEST_DB_USERNAME = "test-dbUsername";
    private static final String TEST_DB_URL = "test-dbUrl";
    private static final String TEST_INIT_XML_PATH = "test-initXmlPath";
    private static final String TEST_DRY_RUN_OUTPUT_FILE = "test-dryRunOutputFile";
    private static final String TEST_SQL_FILE = "test-sqlFile";

    private static final Map<String, String> FULL_ARG_MAP = ImmutableMap.<String, String>builder()
        .put(DB_URL_OPTION_NAME, TEST_DB_URL)
        .put(DB_USERNAME_OPTION_NAME, TEST_DB_USERNAME)
        .put(DB_PASSWORD_OPTION_NAME, TEST_DB_PASSWORD)
        .put(DB_DRIVER_OPTION_NAME, TEST_DB_DRIVER)
        .put(CHANGE_LOG_FILE_OPTION_NAME, TEST_CHANGE_LOG_FILE)
        .put(APPLICATION_PASSWORD_OPTION_NAME, TEST_APPLICATION_PASSWORD)
        .put(INIT_XML_PATH_OPTION_NAME, TEST_INIT_XML_PATH)
        .put(LIQUIBASE_LOG_LEVEL_OPTION_NAME, TEST_LIQUIBASE_LOG_LEVEL)
        .put(DRY_RUN_OPTION_NAME, "true")
        .put(DRY_RUN_OUTPUT_FILE_OPTION_NAME, TEST_DRY_RUN_OUTPUT_FILE)
        .put(SQL_FILE_OPTION_NAME, TEST_SQL_FILE)
        .build();

    private  static final Map<String, String> MINIMAL_ARG_MAP = ImmutableMap.of(
        DB_PASSWORD_OPTION_NAME, TEST_DB_PASSWORD,
        APPLICATION_PASSWORD_OPTION_NAME, TEST_APPLICATION_PASSWORD);

    private ArgumentParser sut;

    @Before
    public void setup() throws Exception
    {
        sut = new ArgumentParser();
    }

    @Test
    public void shouldGenerateConfigThatHonoursArguments() throws Exception
    {
        String[] args = buildArgsFrom(FULL_ARG_MAP);
        Config config = sut.parse(args);

        assertEquals(config.getDbUrl(), TEST_DB_URL);
        assertEquals(config.getDbUsername(), TEST_DB_USERNAME);
        assertEquals(config.getDbPassword(), TEST_DB_PASSWORD);
        assertEquals(config.getDbDriver(), TEST_DB_DRIVER);
        assertEquals(config.getChangeLogFile(), TEST_CHANGE_LOG_FILE);
        assertEquals(config.getLiquibaseLogLevel(), TEST_LIQUIBASE_LOG_LEVEL);
        assertEquals(config.getApplicationPassword(), TEST_APPLICATION_PASSWORD);
        assertEquals(config.getInitXmlPath(), TEST_INIT_XML_PATH);
        assertEquals(config.getDryRunOutputFile(), TEST_DRY_RUN_OUTPUT_FILE);
        assertEquals(config.getSqlFile(), TEST_SQL_FILE);
        assertTrue(config.isDryRun());
    }

    @Test
    public void shouldUseDefaultsForOptionalArguments() throws Exception
    {
        String[] args = buildArgsFrom(MINIMAL_ARG_MAP);
        Config config = sut.parse(args);

        assertEquals(config.getDbUrl(), Config.DEFAULT_DB_URL);
        assertEquals(config.getDbUsername(), Config.DEFAULT_DB_USERNAME);
        assertEquals(config.getDbPassword(), TEST_DB_PASSWORD);
        assertEquals(config.getDbDriver(), Config.DEFAULT_DB_DRIVER);
        assertEquals(config.getChangeLogFile(), Config.DEFAULT_CHANGELOG_FILE);
        assertEquals(config.getLiquibaseLogLevel(), Config.DEFAULT_LIQUIBASE_LOGLEVEL);
        assertEquals(config.getInitXmlPath(), ArgumentParser.DEFAULT_INIT_XML);
        assertEquals(config.getSqlFile(), Config.DEFAULT_SQL_FILE);
        assertFalse(config.isDryRun());
    }

    @Test(expected = MissingOptionException.class)
    public void shouldRequireDbPassword() throws Exception
    {
        String[] args = buildArgsWithout(DB_PASSWORD_OPTION_NAME);
        sut.parse(args);
    }

    @Test(expected = MissingOptionException.class)
    public void shouldRequireApplicationPassword() throws Exception
    {
        String[] args = buildArgsWithout(APPLICATION_PASSWORD_OPTION_NAME);
        sut.parse(args);
    }

    private static String[] buildArgsFrom(Map<String, String> argsMap)
    {
        List<String> args = new ArrayList<String>();
        for (Entry<String, String> arg : argsMap.entrySet())
        {
            args.add("--" + arg.getKey());
            args.add(arg.getValue());
        }
        return args.toArray(new String[] {});
    }

    private static String[] buildArgsWithout(String argNameToRemove)
    {
        Map<String, String> argMap = Maps.newHashMap(FULL_ARG_MAP);
        argMap.remove(argNameToRemove);
        String[] args = buildArgsFrom(argMap);
        return args;
    }



}
