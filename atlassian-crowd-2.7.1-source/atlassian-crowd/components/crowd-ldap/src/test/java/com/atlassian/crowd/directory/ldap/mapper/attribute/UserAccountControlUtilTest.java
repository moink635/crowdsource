package com.atlassian.crowd.directory.ldap.mapper.attribute;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UserAccountControlUtilTest
{
    @Test
    public void testIsUserEnabled() throws Exception
    {
        assertTrue("User should be enabled", UserAccountControlUtil.isUserEnabled("512"));
        assertTrue("User should be enabled", UserAccountControlUtil.isUserEnabled("544"));

        assertFalse("User should be disabled", UserAccountControlUtil.isUserEnabled("514"));
        assertFalse("User should be disabled", UserAccountControlUtil.isUserEnabled("546"));
    }

    @Test
    public void testEnabledUser() throws Exception
    {
        assertEquals("512", UserAccountControlUtil.enabledUser("512")); // no change
        assertEquals("512", UserAccountControlUtil.enabledUser("514"));

        assertEquals("544", UserAccountControlUtil.enabledUser("544")); // no change
        assertEquals("544", UserAccountControlUtil.enabledUser("546"));
    }

    @Test
    public void testDisabledUser() throws Exception
    {
        assertEquals("514", UserAccountControlUtil.disabledUser("512"));
        assertEquals("514", UserAccountControlUtil.disabledUser("514")); // no change

        assertEquals("546", UserAccountControlUtil.disabledUser("544"));
        assertEquals("546", UserAccountControlUtil.disabledUser("546")); // no change
    }
}
