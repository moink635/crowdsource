package com.atlassian.crowd.directory.ldap;

import java.util.Map;

import javax.naming.Context;

import com.atlassian.crowd.directory.ssl.LdapHostnameVerificationSSLSocketFactory;

import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.google.common.collect.ImmutableMap;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LDAPPropertiesMapperImplTest
{
    @Test
    public void nestedGroupsEnabled()
    {
        LDAPPropertiesMapperImpl pm = new LDAPPropertiesMapperImpl(null);

        pm.setAttributes(ImmutableMap.of(LDAPPropertiesMapperImpl.LDAP_NESTED_GROUPS_DISABLED, "false"));

        assertFalse("Nested groups should be enabled", pm.isNestedGroupsDisabled());
    }

    @Test
    public void nestedGroupsDisabled()
    {
        LDAPPropertiesMapperImpl pm = new LDAPPropertiesMapperImpl(null);

        pm.setAttributes(ImmutableMap.of(LDAPPropertiesMapperImpl.LDAP_NESTED_GROUPS_DISABLED, "true"));

        assertTrue("Nested groups should be disabled", pm.isNestedGroupsDisabled());
    }

    @Test
    public void nestedGroupsDisabledByDefault()
    {
        LDAPPropertiesMapperImpl pm = new LDAPPropertiesMapperImpl(null);

        pm.setAttributes(ImmutableMap.<String, String>of());

        assertTrue("Nested groups should be disabled by default", pm.isNestedGroupsDisabled());
    }

    @Test
    public void usesHostnameVerifierFactoryWhenSecure()
    {
        LDAPPropertiesMapperImpl pm = new LDAPPropertiesMapperImpl(null);
        pm.setAttributes(ImmutableMap.of(LDAPPropertiesMapper.LDAP_SECURE_KEY, "true"));
        assertTrue(pm.isSecureSSL());
        assertEquals("", pm.getConnectionURL());

        Map<String, String> attrs = pm.getEnvironment();

        assertEquals("ssl", attrs.get(Context.SECURITY_PROTOCOL));
        assertEquals(LdapHostnameVerificationSSLSocketFactory.class.getName(),
                     attrs.get("java.naming.ldap.factory.socket"));
    }

    @Test
    public void doesNotSpecifyFactoryWhenNotSecure()
    {
        LDAPPropertiesMapperImpl pm = new LDAPPropertiesMapperImpl(null);
        pm.setAttributes(ImmutableMap.of(LDAPPropertiesMapper.LDAP_SECURE_KEY, "false"));
        assertFalse(pm.isSecureSSL());
        assertEquals("", pm.getConnectionURL());

        Map<String, String> attrs = pm.getEnvironment();

        assertFalse(attrs.containsKey(Context.SECURITY_PROTOCOL));
        assertFalse(attrs.containsKey(LDAPPropertiesMapperImpl.CONNECTION_FACTORY));
    }

    @Test
    public void doesNotSpecifyFactoryWhenLdapsProtocolButNotFlaggedAsSecure()
    {
        LDAPPropertiesMapperImpl pm = new LDAPPropertiesMapperImpl(null);
        pm.setAttributes(ImmutableMap.of(
                LDAPPropertiesMapper.LDAP_SECURE_KEY, "false",
                LDAPPropertiesMapper.LDAP_URL_KEY, "ldaps://test/"
        ));
        assertFalse(pm.isSecureSSL());
        assertEquals("ldaps://test/", pm.getConnectionURL());

        Map<String, String> attrs = pm.getEnvironment();

        assertFalse(attrs.containsKey(Context.SECURITY_PROTOCOL));
        assertFalse(attrs.containsKey(LDAPPropertiesMapperImpl.CONNECTION_FACTORY));
    }

    @Test
    public void localUserStatusIsDisabledByDefault()
    {
        LDAPPropertiesMapperImpl pm = new LDAPPropertiesMapperImpl(null);
        pm.setAttributes(ImmutableMap.<String,String>of());

        assertFalse(pm.isLocalUserStatusEnabled());
    }

    @Test
    public void localUserStatusCanBeEnabled()
    {
        LDAPPropertiesMapperImpl pm = new LDAPPropertiesMapperImpl(null);
        pm.setAttributes(ImmutableMap.of(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS, "true"));

        assertTrue(pm.isLocalUserStatusEnabled());
    }
}
