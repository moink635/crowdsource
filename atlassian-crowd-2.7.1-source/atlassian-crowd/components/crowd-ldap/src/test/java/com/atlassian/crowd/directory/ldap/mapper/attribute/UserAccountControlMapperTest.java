package com.atlassian.crowd.directory.ldap.mapper.attribute;

import java.util.Collections;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Test;
import org.springframework.ldap.core.DirContextAdapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class UserAccountControlMapperTest
{
    @Test
    public void shouldReturnValueWhenAttributeExists() throws Exception
    {
        UserAccountControlMapper mapper = new UserAccountControlMapper();
        DirContextAdapter ctx = new DirContextAdapter();
        ctx.setAttributeValue("userAccountControl", "512");
        assertEquals(Collections.singleton("512"), mapper.getValues(ctx));
    }

    @Test
    public void shouldNotReturnValueWhenAttributeDoesNotExist() throws Exception
    {
        UserAccountControlMapper mapper = new UserAccountControlMapper();
        DirContextAdapter ctx = new DirContextAdapter();
        assertThat(mapper.getValues(ctx), IsEmptyCollection.empty());
    }
}
