package com.atlassian.crowd.directory;

import java.util.Hashtable;

import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;
import com.atlassian.crowd.util.PasswordHelper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SunONETest
{
    private static final String SUN_NAME = "Sun Directory Server Enterprise Edition";
    private static final long DIRECTORY_ID = 1;

    @Mock
    private PasswordHelper passwordHelper;

    @InjectMocks
    private SunONE sunOne;

    @Test
    public void testGetDescriptiveName()
    {
        assertEquals(SUN_NAME, sunOne.getDescriptiveName());
    }

    @Test
    public void testGetStaticDirectoryType()
    {
        assertEquals(SUN_NAME, SunONE.getStaticDirectoryType());
    }

    @Test
    public void testEncodePasswordShouldNotEncryptPassword() throws Exception
    {
        String password = "secret";
        assertEquals(password, sunOne.encodePassword(PasswordCredential.unencrypted(password)));
    }

    @Test
    public void encodePasswordShouldQuoteAndEncodeARandomPasswordWhenAskedToEncodePasswordCredentialNone() throws Exception
    {
        when(passwordHelper.generateRandomPassword()).thenReturn("random-password");

        String passwordUsedByDirectory = sunOne.encodePassword(PasswordCredential.NONE);

        verify(passwordHelper).generateRandomPassword();
        assertEquals("random-password", passwordUsedByDirectory);
    }

    @Test(expected = InvalidCredentialException.class)
    public void encodePasswordShouldFailToEncodeAnEncryptedPasswordIfItIsNotPasswordCredentialNone() throws Exception
    {
        sunOne.encodePassword(PasswordCredential.encrypted("my-encrypted-pass"));
    }

    @Test
    public void testCreateChangeListenerTemplate()
    {
        LDAPPropertiesMapper mockLdapPropertiesMapper = mock(LDAPPropertiesMapper.class);
        when(mockLdapPropertiesMapper.getConnectionURL()).thenReturn("http://url.com");
        when(mockLdapPropertiesMapper.getUsername()).thenReturn("Bob");
        when(mockLdapPropertiesMapper.getPassword()).thenReturn("supersafepassword");
        Hashtable<String, String> dummyEnvironment = new Hashtable<String, String>();
        dummyEnvironment.put("dummy.field.one", "ichi");
        dummyEnvironment.put("dummy.field.two", "ni");
        dummyEnvironment.put("dummy.field.three", "san");
        when(mockLdapPropertiesMapper.getEnvironment()).thenReturn(dummyEnvironment);

        sunOne.ldapPropertiesMapper = mockLdapPropertiesMapper; // mock out the properties mapper

        // all needed attributes set should be able to create template with no error
        sunOne.createChangeListenerTemplate();
    }

    @Test
    public void testGetNewUserDirectorySpecificAttributes() throws NamingException
    {
        LDAPPropertiesMapper mockLdapPropertiesMapper = mock(LDAPPropertiesMapper.class);
        when(mockLdapPropertiesMapper.getUserLastNameAttribute()).thenReturn(LDAPPropertiesMapper.USER_LASTNAME_KEY);

        User user = new UserTemplateWithAttributes("hello", DIRECTORY_ID);
        Attributes attributes = new BasicAttributes();
        attributes.put(LDAPPropertiesMapper.USER_EMAIL_KEY, "hello@byebye.com");

        sunOne.ldapPropertiesMapper = mockLdapPropertiesMapper;

        sunOne.getNewUserDirectorySpecificAttributes(user, attributes);

        // a default sn should be set even though it was not specified
        assertEquals("",attributes.get(LDAPPropertiesMapper.USER_LASTNAME_KEY).get(0));
        assertEquals("hello@byebye.com",attributes.get(LDAPPropertiesMapper.USER_EMAIL_KEY).get(0));

    }

}
