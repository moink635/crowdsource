package com.atlassian.crowd.directory.ldap.mapper.attribute;

import java.util.Collections;

import javax.naming.Name;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;

import com.google.common.collect.ImmutableSet;

import org.junit.Test;
import org.springframework.ldap.core.DirContextAdapter;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class USNChangedMapperTest
{
    @Test
    public void emptySetIsReturnedWhenAttributeIsMissing() throws Exception
    {
        AttributeMapper ogm = new USNChangedMapper();

        Attributes attrs = mock(Attributes.class);
        Name dn = mock(Name.class);

        assertEquals(Collections.<String>emptySet(), ogm.getValues(new DirContextAdapter(attrs, dn)));
    }

    @Test
    public void returnedStringIsPassedThroughDirectly() throws Exception
    {
        AttributeMapper uc = new USNChangedMapper();

        Attribute value = new BasicAttribute(null, "1");

        Attributes attrs = mock(Attributes.class);
        when(attrs.get("uSNChanged")).thenReturn(value);

        Name dn = mock(Name.class);

        assertEquals(ImmutableSet.of("1"), uc.getValues(new DirContextAdapter(attrs, dn)));
    }
}
