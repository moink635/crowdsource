package com.atlassian.crowd.directory.ldap.mapper.attribute;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SIDUtilsTest
{
    @Test
    public void substituteRidInSid() throws Exception
    {
        assertEquals("S-1-5-21-348418773-4108394185-1306204109-513",
                     SIDUtils.substituteLastRidInSid("S-1-5-21-348418773-4108394185-1306204109-353960", "513"));
    }

    @Test
    public void getLastRidFromSid()
    {
        assertEquals("513", SIDUtils.getLastRidFromSid("S-1-5-21-348418773-4108394185-1306204109-513"));
    }

    @Test
    public void getLastRidFromSidWhenSidIsMalformed()
    {
        assertEquals("", SIDUtils.getLastRidFromSid("S-"));
    }

    @Test (expected = IllegalArgumentException.class)
    public void getLastRidFromSidWhenSidIsEmpty()
    {
        SIDUtils.getLastRidFromSid("");
    }
}
