package com.atlassian.crowd.directory.ldap.cache;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.naming.ldap.LdapName;

import com.atlassian.crowd.directory.MicrosoftActiveDirectory;
import com.atlassian.crowd.directory.SynchronisableDirectoryProperties;
import com.atlassian.crowd.model.Tombstone;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplateWithAttributes;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.LDAPGroupWithAttributes;
import com.atlassian.crowd.model.user.LDAPUserWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anySet;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.eq;

@RunWith(MockitoJUnitRunner.class)
public class UsnChangedCacheRefresherTest
{
    private MockDirectoryCache mockDirectoryCache;

    @Mock
    private MicrosoftActiveDirectory activeDirectory;

    private LDAPUserWithAttributes userAdam, userBob, userCath;
    private LDAPGroupWithAttributes lemmings, sheep, fish;

    @Before
    public void setUp() throws Exception
    {
        mockDirectoryCache = new MockDirectoryCache();

        userAdam = createUser("adam", "X01");
        userBob = createUser("bob", "X02");
        userCath = createUser("cath", "X03");
        lemmings = createGroup("lemmings", "X08");
        sheep = createGroup("sheep", "X09");
        fish = createGroup("fish", "X10");

        when(activeDirectory.findDirectMembersOfGroup(Mockito.<LdapName>any())).thenReturn(Collections.<LdapName>emptyList());
    }

    @Test
    public void testCannotSyncChangesUntilFullRefresh() throws Exception
    {
        UsnChangedCacheRefresher cacheRefresher = new UsnChangedCacheRefresher(null);
        assertFalse(cacheRefresher.synchroniseChanges(null));
    }

    @Test
    public void testFullRefresh() throws Exception
    {
        final List<LDAPUserWithAttributes> userList = Arrays.asList(userAdam, userBob);
        when(activeDirectory.searchUsers(any(EntityQuery.class))).thenReturn(userList);
        final List<LDAPGroupWithAttributes> groupList = Arrays.asList(lemmings, sheep);
        when(activeDirectory.searchGroups(any(EntityQuery.class))).thenReturn(groupList);

        // Create the class under test
        UsnChangedCacheRefresher cacheRefresher = new UsnChangedCacheRefresher(activeDirectory);
        // Can't do a delta until the first full refresh
        assertFalse(cacheRefresher.synchroniseChanges(mockDirectoryCache));
        // Do the Cache refresh
        cacheRefresher.synchroniseAll(mockDirectoryCache);

        // Expectations - Users
        assertThat(mockDirectoryCache.getUsers(), hasSize(2));
        assertThat(mockDirectoryCache.getUsers(), hasItem(userAdam));
        assertThat(mockDirectoryCache.getUsers(), hasItem(userBob));
        // Groups
        assertThat(mockDirectoryCache.getGroups(), hasSize(2));
        assertThat(mockDirectoryCache.getGroups(), hasItem(lemmings));
        assertThat(mockDirectoryCache.getGroups(), hasItem(sheep));
    }

    @Test
    public void testCantSynchroniseChangesWithIncrementalSyncDisabled() throws Exception
    {
        // these methods get called on sync all
        when(activeDirectory.searchUsers(any(EntityQuery.class))).thenReturn(Arrays.asList(userAdam, userBob));
        when(activeDirectory.searchGroups(any(EntityQuery.class))).thenReturn(Arrays.asList(lemmings, sheep));
        when(activeDirectory.fetchHighestCommittedUSN()).thenReturn(111L);
        // make sure roles are disabled
        when(activeDirectory.isRolesDisabled()).thenReturn(true);
        // make sure incremental sync is disabled
        when(activeDirectory.getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)).thenReturn("false");

        // Create the class under test
        UsnChangedCacheRefresher cacheRefresher = new UsnChangedCacheRefresher(activeDirectory);
        // Can't do a delta until the first full refresh
        assertFalse(cacheRefresher.synchroniseChanges(mockDirectoryCache));
        // Do the Cache refresh
        cacheRefresher.synchroniseAll(mockDirectoryCache);

        // Expectations - Users
        assertThat(mockDirectoryCache.getUsers(), hasSize(2));
        assertThat(mockDirectoryCache.getUsers(), hasItem(userAdam));
        assertThat(mockDirectoryCache.getUsers(), hasItem(userBob));
        // Groups
        assertThat(mockDirectoryCache.getGroups(), hasSize(2));
        assertThat(mockDirectoryCache.getGroups(), hasItem(lemmings));
        assertThat(mockDirectoryCache.getGroups(), hasItem(sheep));

        // Now try to refresh just changes (incremental sync is disabled - so not allowed)
        assertFalse(cacheRefresher.synchroniseChanges(mockDirectoryCache));
    }

    @Test
    public void testSynchroniseChanges() throws Exception
    {
        // these methods get called on sync all
        when(activeDirectory.searchUsers(any(EntityQuery.class))).thenReturn(Arrays.asList(userAdam, userBob));
        when(activeDirectory.searchGroups(any(EntityQuery.class))).thenReturn(Arrays.asList(lemmings, sheep));
        when(activeDirectory.fetchHighestCommittedUSN()).thenReturn(111L);
        // make sure roles are disabled
        when(activeDirectory.isRolesDisabled()).thenReturn(true);
        // make sure incremental sync is enabled
        when(activeDirectory.getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)).thenReturn("true");
                // these get called on sync changes
                when(activeDirectory.findAddedOrUpdatedUsersSince(111L)).thenReturn(Arrays.asList(userCath));
        when(activeDirectory.findUserTombstonesSince(111L)).thenReturn(Arrays.asList(new Tombstone("X02", "111")));
        when(activeDirectory.findAddedOrUpdatedGroupsSince(111L)).thenReturn(Arrays.asList(fish));
        when(activeDirectory.findGroupTombstonesSince(111L)).thenReturn(Arrays.asList(new Tombstone("X09", "111")));

        // Create the class under test
        UsnChangedCacheRefresher cacheRefresher = new UsnChangedCacheRefresher(activeDirectory);
        // Can't do a delta until the first full refresh
        assertFalse(cacheRefresher.synchroniseChanges(mockDirectoryCache));
        // Do the Cache refresh
        cacheRefresher.synchroniseAll(mockDirectoryCache);

        // Expectations - Users
        assertThat(mockDirectoryCache.getUsers(), hasSize(2));
        assertThat(mockDirectoryCache.getUsers(), hasItem(userAdam));
        assertThat(mockDirectoryCache.getUsers(), hasItem(userBob));
        // Groups
        assertThat(mockDirectoryCache.getGroups(), hasSize(2));
        assertThat(mockDirectoryCache.getGroups(), hasItem(lemmings));
        assertThat(mockDirectoryCache.getGroups(), hasItem(sheep));

        // Now refresh just changes
        assertTrue(cacheRefresher.synchroniseChanges(mockDirectoryCache));

        // Expectations - Users
        assertThat(mockDirectoryCache.getUsers(), hasSize(2));
        assertThat(mockDirectoryCache.getUsers(), hasItem(userAdam));
        assertThat(mockDirectoryCache.getUsers(), hasItem(userCath));
        // Groups
        assertThat(mockDirectoryCache.getGroups(), hasSize(2));
        assertThat(mockDirectoryCache.getGroups(), hasItem(lemmings));
        assertThat(mockDirectoryCache.getGroups(), hasItem(fish));
    }

    @Test
    public void testSynchroniseChangesWithTombstonesOutsideOfScopeIgnored() throws Exception
    {
        // these methods get called on sync all
        when(activeDirectory.searchUsers(any(EntityQuery.class))).thenReturn(Arrays.asList(userAdam, userBob));
        when(activeDirectory.searchGroups(any(EntityQuery.class))).thenReturn(Arrays.asList(lemmings, sheep));
        when(activeDirectory.fetchHighestCommittedUSN()).thenReturn(111L);
        // make sure roles are disabled
        when(activeDirectory.isRolesDisabled()).thenReturn(true);
        // make sure incremental sync is enabled
        when(activeDirectory.getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)).thenReturn("true");
        // these get called on sync changes
        when(activeDirectory.findAddedOrUpdatedUsersSince(111L)).thenReturn(Arrays.asList(userCath));
        when(activeDirectory.findUserTombstonesSince(111L)).thenReturn(Arrays.asList(new Tombstone("X02", "111"), new Tombstone("out-of-scope-1", "112")));
        when(activeDirectory.findAddedOrUpdatedGroupsSince(111L)).thenReturn(Arrays.asList(fish));
        when(activeDirectory.findGroupTombstonesSince(111L)).thenReturn(Arrays.asList(new Tombstone("X09", "111"), new Tombstone("out-of-scope-1", "113")));

        // Create the class under test
        UsnChangedCacheRefresher cacheRefresher = new UsnChangedCacheRefresher(activeDirectory);
        // Can't do a delta until the first full refresh
        assertFalse(cacheRefresher.synchroniseChanges(mockDirectoryCache));
        // Do the Cache refresh
        cacheRefresher.synchroniseAll(mockDirectoryCache);

        // Expectations - Users
        assertThat(mockDirectoryCache.getUsers(), hasSize(2));
        assertThat(mockDirectoryCache.getUsers(), hasItem(userAdam));
        assertThat(mockDirectoryCache.getUsers(), hasItem(userBob));
        // Groups
        assertThat(mockDirectoryCache.getGroups(), hasSize(2));
        assertThat(mockDirectoryCache.getGroups(), hasItem(lemmings));
        assertThat(mockDirectoryCache.getGroups(), hasItem(sheep));

        // Now refresh just changes
        assertTrue(cacheRefresher.synchroniseChanges(mockDirectoryCache));

        // Expectations - Users
        assertThat(mockDirectoryCache.getUsers(), hasSize(2));
        assertThat(mockDirectoryCache.getUsers(), hasItem(userAdam));
        assertThat(mockDirectoryCache.getUsers(), hasItem(userCath));
        // Groups
        assertThat(mockDirectoryCache.getGroups(), hasSize(2));
        assertThat(mockDirectoryCache.getGroups(), hasItem(lemmings));
        assertThat(mockDirectoryCache.getGroups(), hasItem(fish));
    }

    @Test
    public void onlyChangedGroupsHaveMembershipsSynchronised() throws Exception
    {
        /* The directory has three groups, two top-level and one subgroup */
        when(activeDirectory.searchGroups(QueryBuilder
                .queryFor(LDAPGroupWithAttributes.class, EntityDescriptor.group(GroupType.GROUP))
                .returningAtMost(EntityQuery.ALL_RESULTS))).thenReturn(
                        ImmutableList.<LDAPGroupWithAttributes>of(
                                createGroup("changed-group", "xxx"),
                                createGroup("unchanged-group", "yyy"),
                                createGroup("subgroup-in-changed-group", "zzz")));

        /* We need a full list of users for the initial full sync */
        final List<LDAPUserWithAttributes> userList = Arrays.asList(
                createUser("user-in-changed-group", "aaa"),
                createUser("user-in-unchanged-group", "bbb"));
        when(activeDirectory.searchUsers(any(EntityQuery.class))).thenReturn(userList);

        UsnChangedCacheRefresher cacheRefresher = new UsnChangedCacheRefresher(activeDirectory);
        when(activeDirectory.supportsNestedGroups()).thenReturn(true);

        /* Synchronise once to prime the caches */
        cacheRefresher.synchroniseAll(mock(DirectoryCache.class));


        /* The changed group contains a user and a subgroup */
        when(activeDirectory.searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName("changed-group").returningAtMost(EntityQuery.ALL_RESULTS))).thenReturn(
                ImmutableList.of("user-in-changed-group"));
        when(activeDirectory.searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName("changed-group").returningAtMost(EntityQuery.ALL_RESULTS))).thenReturn(
                ImmutableList.of("subgroup-in-changed-group"));

        /* The unchanged group fails the test if its members are queried */
        when(activeDirectory.searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName("unchanged-group").returningAtMost(EntityQuery.ALL_RESULTS)))
            .thenThrow(new RuntimeException("Should not query for unchanged group"));
        when(activeDirectory.searchGroupRelationships(QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName("unchanged-group").returningAtMost(EntityQuery.ALL_RESULTS)))
            .thenThrow(new RuntimeException("Should not query for unchanged group"));

        /* The same expectations as above for the RFC4519DirectoryMembershipsIterable */
        when(activeDirectory.findDirectMembersOfGroup(new LdapName("cn=changed-group"))).thenReturn(
                ImmutableList.<LdapName>of(new LdapName("cn=user-in-changed-group"), new LdapName("cn=subgroup-in-changed-group")));
        when(activeDirectory.findDirectMembersOfGroup(new LdapName("cn=unchanged-group"))).thenThrow(new RuntimeException("Should not query for unchanged group"));

        /* Synchronise, specifying that only one group has changes */
        Group changedGroup = mock(Group.class);
        when(changedGroup.getName()).thenReturn("changed-group");

        DirectoryCache directoryCache = mock(DirectoryCache.class);

        cacheRefresher.synchroniseMemberships(Arrays.asList(changedGroup), directoryCache);

        // The complete set of memberships was not requested
        verify(activeDirectory, Mockito.never()).getMemberships();

        /* Only the groups specified as changed were synchronised */
        verify(directoryCache).syncUserMembersForGroup(changedGroup, ImmutableSet.of("user-in-changed-group"));
        verify(directoryCache).syncGroupMembersForGroup(changedGroup, ImmutableSet.of("subgroup-in-changed-group"));
        Mockito.verifyNoMoreInteractions(directoryCache);

        verify(activeDirectory, Mockito.never()).searchGroupRelationships(Mockito.<MembershipQuery<?>>any());
        verify(activeDirectory).findDirectMembersOfGroup(new LdapName("cn=unchanged-group"));
        verify(activeDirectory, Mockito.times(2)).findDirectMembersOfGroup(new LdapName("cn=changed-group"));
    }

    /**
     * This is a check of order of operations. Users need to be deleted before they can be added or updated.
     * This is required to support LDAP rename feature properly.
     * See: https://extranet.atlassian.com/display/JIRADEV/Detect+rename+user+in+LDAP+Scenarios
     * for rename details
     * @throws Exception
     */
    @Test
    public void deleteUsersBeforeAddOrUpdateToSupportUserRename() throws Exception
    {
        final DirectoryCache mockDirectoryCache = mock(DirectoryCache.class);
        final List<LDAPUserWithAttributes> userList = ImmutableList.of(userAdam, userBob);
        when(activeDirectory.searchUsers(any(EntityQuery.class))).thenReturn(userList);

        // Create the class under test
        UsnChangedCacheRefresher cacheRefresher = new UsnChangedCacheRefresher(activeDirectory);
        cacheRefresher.synchroniseAll(mockDirectoryCache);

        InOrder inOrder = inOrder(mockDirectoryCache);
        inOrder.verify(mockDirectoryCache).deleteCachedUsersNotIn(eq(userList), any(Date.class));
        inOrder.verify(mockDirectoryCache).addOrUpdateCachedUsers(eq(userList), any(Date.class));

    }

    @Test
    public void deleteUsersBeforeAddOrUpdateInIncrementalSynchronization() throws Exception
    {
        final DirectoryCache mockDirectoryCache = mock(DirectoryCache.class);
        final List<LDAPUserWithAttributes> userList = ImmutableList.of(userAdam, userBob);

        when(activeDirectory.searchUsers(any(EntityQuery.class))).thenReturn(userList);
        when(activeDirectory.getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)).thenReturn("true");
        when(activeDirectory.findAddedOrUpdatedUsersSince(anyLong())).thenReturn(userList);

        // Create the class under test
        UsnChangedCacheRefresher cacheRefresher = new UsnChangedCacheRefresher(activeDirectory);
        cacheRefresher.synchroniseAll(mockDirectoryCache);
        cacheRefresher.synchroniseChanges(mockDirectoryCache);

        InOrder inOrder = inOrder(mockDirectoryCache);
        inOrder.verify(mockDirectoryCache).deleteCachedUsers(anySet());
        inOrder.verify(mockDirectoryCache).addOrUpdateCachedUsers(userList, null);
    }



    /**
     *  When we have two user in AD: X  and Y, we synchronize them, than we delete X and rename Y to X
     *  if after delete, we log in as X and then start incremental synchronisation there may be situation
     *  where incremental synch wants to delete the same user it is about to update (because we delete users by name not ObjectGUID)
     */
    @Test
    public void doNotDeleteUserIfHeIsPresentOnUpdateList() throws Exception
    {
        final DirectoryCache mockDirectoryCache = mock(DirectoryCache.class);
        final List<LDAPUserWithAttributes> userList = Lists.newArrayList(userAdam, userBob);
        when(activeDirectory.searchUsers(any(EntityQuery.class))).thenReturn(userList);
        when(activeDirectory.getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)).thenReturn("true");
        when(activeDirectory.findAddedOrUpdatedUsersSince(anyLong())).thenReturn(userList);
        when(activeDirectory.findUserTombstonesSince(anyLong())).thenReturn(Arrays.asList(new Tombstone("X02", "111")));

        // Create the class under test
        UsnChangedCacheRefresher cacheRefresher = new UsnChangedCacheRefresher(activeDirectory);
        cacheRefresher.synchroniseAll(mockDirectoryCache);
        cacheRefresher.synchroniseChanges(mockDirectoryCache);

        InOrder inOrder = inOrder(mockDirectoryCache);
        inOrder.verify(mockDirectoryCache).deleteCachedUsers(new HashSet<String>());
        inOrder.verify(mockDirectoryCache).addOrUpdateCachedUsers(userList, null);
    }

    @Test
    public void deleteUserIfHeIsNotPresentOnUpdateList() throws Exception
    {
        final DirectoryCache mockDirectoryCache = mock(DirectoryCache.class);
        final List<LDAPUserWithAttributes> userList = ImmutableList.of(userAdam, userBob, userCath);
        final List<LDAPUserWithAttributes> updatedUserList = ImmutableList.of(userAdam, userBob);
        when(activeDirectory.searchUsers(any(EntityQuery.class))).thenReturn(userList);
        when(activeDirectory.getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)).thenReturn("true");
        when(activeDirectory.findAddedOrUpdatedUsersSince(anyLong())).thenReturn(updatedUserList);
        when(activeDirectory.findUserTombstonesSince(anyLong())).thenReturn(Arrays.asList(new Tombstone("X03", "111")));

        // Create the class under test
        UsnChangedCacheRefresher cacheRefresher = new UsnChangedCacheRefresher(activeDirectory);
        cacheRefresher.synchroniseAll(mockDirectoryCache);
        cacheRefresher.synchroniseChanges(mockDirectoryCache);

        InOrder inOrder = inOrder(mockDirectoryCache);
        inOrder.verify(mockDirectoryCache).deleteCachedUsers(ImmutableSet.of("cath"));
        inOrder.verify(mockDirectoryCache).addOrUpdateCachedUsers(updatedUserList, null);
    }

    private LDAPUserWithAttributes createUser(final String username, final String objectGUID)
    {
        final UserTemplateWithAttributes user = new UserTemplateWithAttributes(username, 1);
        user.setAttribute("objectGUID", objectGUID);
        user.setExternalId(objectGUID);
        return new LDAPUserWithAttributes("cn="+username, user);
    }

    private LDAPGroupWithAttributes createGroup(final String groupname, final String objectGUID)
    {
        final GroupTemplateWithAttributes group = new GroupTemplateWithAttributes(groupname, 1, GroupType.GROUP);
        group.setAttribute("objectGUID", objectGUID);
        return new LDAPGroupWithAttributes("cn="+groupname, group);
    }



}
