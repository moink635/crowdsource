package com.atlassian.crowd.directory.ldap.mapper.attribute;

import com.google.common.collect.ImmutableSet;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ldap.core.DirContextAdapter;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ObjectSIDMapperTest
{
    @Mock private DirContextAdapter context;

    @Test
    public void shouldReturnEmptyIfAttributeDoesNotExist() throws Exception
    {
        ObjectSIDMapper mapper = new ObjectSIDMapper();

        assertThat(mapper.getValues(context), IsEmptyCollection.empty());

        verify(context).getObjectAttribute(ObjectSIDMapper.ATTRIBUTE_KEY);
    }

    @Test
    public void shouldReturnStringRepresentationOfSid() throws Exception
    {
        ObjectSIDMapper mapper = new ObjectSIDMapper();

        // SID binary format: http://msdn.microsoft.com/en-us/library/gg465313.aspx
        byte[] binarySid = new byte[] { 1,               // revision
                                        5,               // number of subauthorities
                                        0,0,0,0,0,5,     // identifier authority
                                        21,0,0,0,        // subauthority 1
                                        -43,114,-60,20,  // subauthority 2
                                        -55,30,-31,-12,  // subauthority 3
                                        -51,23,-37,77,   // subauthority 4
                                        -88,102,5,0};    // subauthority 5
        when(context.getObjectAttribute(ObjectSIDMapper.ATTRIBUTE_KEY)).thenReturn(binarySid);

        assertEquals(ImmutableSet.of("S-1-5-21-348418773-4108394185-1306204109-353960"), mapper.getValues(context));
    }

    @Test (expected = ArrayIndexOutOfBoundsException.class)
    public void shouldFailIfSidIsInvalid() throws Exception
    {
        ObjectSIDMapper mapper = new ObjectSIDMapper();

        byte[] invalidSid = { 1, 2, 3 };
        when(context.getObjectAttribute(ObjectSIDMapper.ATTRIBUTE_KEY)).thenReturn(invalidSid);

        mapper.getValues(context);
    }
}
