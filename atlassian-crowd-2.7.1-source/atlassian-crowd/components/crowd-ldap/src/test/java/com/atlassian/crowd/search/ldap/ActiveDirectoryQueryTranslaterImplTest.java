package com.atlassian.crowd.search.ldap;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.query.entity.restriction.PropertyImpl;
import com.atlassian.crowd.search.query.entity.restriction.TermRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ldap.filter.Filter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class ActiveDirectoryQueryTranslaterImplTest
{
    @Mock
    private LDAPPropertiesMapper ldapPropertiesMapper;

    private ActiveDirectoryQueryTranslaterImpl translater = new ActiveDirectoryQueryTranslaterImpl();

    @Test
    public void shouldFilterActiveUsers() throws Exception
    {
        TermRestriction<Boolean> activeUsersRestriction = new TermRestriction<Boolean>(UserTermKeys.ACTIVE, true);
        Filter filter = translater.booleanTermRestrictionAsFilter(EntityDescriptor.user(),
                                                                  activeUsersRestriction,
                                                                  ldapPropertiesMapper);
        assertEquals("(!(userAccountControl:1.2.840.113556.1.4.804:=2))", filter.encode());
    }

    @Test
    public void shouldFilterInactiveUsers() throws Exception
    {
        TermRestriction<Boolean> inactiveUsersRestriction = new TermRestriction<Boolean>(UserTermKeys.ACTIVE, false);
        Filter filter = translater.booleanTermRestrictionAsFilter(EntityDescriptor.user(),
                                                                  inactiveUsersRestriction,
                                                                  ldapPropertiesMapper);
        assertEquals("(userAccountControl:1.2.840.113556.1.4.803:=2)", filter.encode());
    }

    @Test
    public void shouldReturnEverythingForActiveGroups() throws Exception
    {
        TermRestriction<Boolean> activeGroupsRestriction = new TermRestriction<Boolean>(GroupTermKeys.ACTIVE, true);
        Filter filter = translater.booleanTermRestrictionAsFilter(EntityDescriptor.group(),
                                                                  activeGroupsRestriction,
                                                                  ldapPropertiesMapper);
        assertTrue(filter instanceof EverythingResult);
    }

    @Test
    public void shouldReturnNothingForInactiveGroups() throws Exception
    {
        TermRestriction<Boolean> inactiveGroupsRestriction = new TermRestriction<Boolean>(GroupTermKeys.ACTIVE, false);
        Filter filter = translater.booleanTermRestrictionAsFilter(EntityDescriptor.group(),
                                                                  inactiveGroupsRestriction,
                                                                  ldapPropertiesMapper);
        assertTrue(filter instanceof NothingResult);
    }

    @Test (expected = IllegalArgumentException.class)
    public void shouldFailForOtherQueries() throws Exception
    {
        TermRestriction<Boolean> activeUsersRestriction = new TermRestriction<Boolean>(
                new PropertyImpl<Boolean>("unknown-property", Boolean.class), true);
        translater.booleanTermRestrictionAsFilter(EntityDescriptor.group(),
                                                  activeUsersRestriction,
                                                  ldapPropertiesMapper);
    }

}
