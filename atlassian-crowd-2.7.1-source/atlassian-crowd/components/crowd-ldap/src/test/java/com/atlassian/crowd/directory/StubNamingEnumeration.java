package com.atlassian.crowd.directory;

import java.util.Iterator;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

public class StubNamingEnumeration<T> implements NamingEnumeration<T>
{
    private final Iterator<T> underlying;

    public StubNamingEnumeration(Iterator<T> underlying)
    {
        this.underlying = underlying;
    }

    @Override
    public void close() throws NamingException
    {
    }

    @Override
    public boolean hasMore() throws NamingException
    {
        return underlying.hasNext();
    }

    @Override
    public boolean hasMoreElements()
    {
        return underlying.hasNext();
    }

    @Override
    public T next() throws NamingException
    {
        return underlying.next();
    }

    @Override
    public T nextElement()
    {
        return underlying.next();
    }
}
