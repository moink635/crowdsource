package com.atlassian.crowd.directory;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;

import com.atlassian.crowd.directory.ldap.mapper.ContextMapperWithRequiredAttributes;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Test;
import org.springframework.ldap.core.DirContextAdapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NamedLdapEntityTest
{
    @Test
    public void mapperRequiresExactlyOneAttribute()
    {
        ContextMapperWithRequiredAttributes<NamedLdapEntity> mapper = NamedLdapEntity.mapperFromAttribute("test");

        assertEquals(ImmutableSet.of("test"), mapper.getRequiredLdapAttributes());
    }

    @Test
    public void mapperGetsNameFromAttribute() throws InvalidNameException
    {
        ContextMapperWithRequiredAttributes<NamedLdapEntity> mapper = NamedLdapEntity.mapperFromAttribute("test");

        DirContextAdapter ctxt = mock(DirContextAdapter.class);
        when(ctxt.getDn()).thenReturn(new LdapName("dc=example,dc=com"));
        when(ctxt.getStringAttribute("test")).thenReturn("Entity Name");

        NamedLdapEntity e = mapper.mapFromContext(ctxt );

        assertEquals(new LdapName("dc=example,dc=com"), e.getDn());
        assertEquals("Entity Name", e.getName());
    }

    @Test
    public void mapperMayReturnNullAttribute() throws InvalidNameException
    {
        ContextMapperWithRequiredAttributes<NamedLdapEntity> mapper = NamedLdapEntity.mapperFromAttribute("test");

        DirContextAdapter ctxt = mock(DirContextAdapter.class);
        when(ctxt.getDn()).thenReturn(new LdapName("dc=example,dc=com"));

        NamedLdapEntity e = mapper.mapFromContext(ctxt );

        assertEquals(new LdapName("dc=example,dc=com"), e.getDn());
        assertNull(e.getName());
    }

    @Test
    public void testNamesOf() throws InvalidNameException
    {
        assertEquals(ImmutableList.of("one", "two"),
                ImmutableList.copyOf(NamedLdapEntity.namesOf(
                        ImmutableList.of(new NamedLdapEntity(new LdapName("dc=com"), "one"),
                                new NamedLdapEntity(new LdapName("dc=com"), "two")
                                ))));
    }

    @Test
    public void mapperAreEqual()
    {
        assertEquals(NamedLdapEntity.mapperFromAttribute("cn"), NamedLdapEntity.mapperFromAttribute("cn"));
    }

    @Test
    public void unequalMappersAreNotEqual()
    {
        assertNotEquals(NamedLdapEntity.mapperFromAttribute("cn"), NamedLdapEntity.mapperFromAttribute("otherthing"));
    }
}
