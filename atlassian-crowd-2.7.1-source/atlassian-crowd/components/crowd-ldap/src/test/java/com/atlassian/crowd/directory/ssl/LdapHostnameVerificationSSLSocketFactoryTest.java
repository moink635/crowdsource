package com.atlassian.crowd.directory.ssl;

import java.lang.reflect.Method;
import java.net.Socket;

import javax.net.SocketFactory;
import javax.net.ssl.SSLParameters;
import javax.net.ssl.SSLSocket;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class LdapHostnameVerificationSSLSocketFactoryTest
{
    @Test
    public void constructsInstanceOfFactoryThroughStaticMethodReflection() throws Exception
    {
        String factory = LdapHostnameVerificationSSLSocketFactory.class.getName();

        Class<?> c = getClass().getClassLoader().loadClass(factory);
        Method m = c.getMethod("getDefault");

        SocketFactory fac = (SocketFactory) m.invoke(null);
        assertNotNull(fac);
    }

    @Test
    public void disconnectedSocketsCreated() throws Exception
    {
        Socket s = LdapHostnameVerificationSSLSocketFactory.getDefault().createSocket();
        assertTrue(LdapHostnameVerificationSSLSocketFactory.isInSunSslImplementationPackage(s.getClass(), "SSLSocketImpl"));

        /* Are we running JDK7? */
        SSLParameters params = ((SSLSocket) s).getSSLParameters();

        try
        {
            Method getter = params.getClass().getMethod("getEndpointIdentificationAlgorithm");

            assertEquals("LDAPS", getter.invoke(params));
        }
        catch (NoSuchMethodException e)
        {
            /* JDK 6 */
            /* Invoke with reflection to allow for regular and Mac OS packages */
            Method m = s.getClass().getMethod("getHostnameVerification");
            assertEquals("ldap", m.invoke(s));
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void failsWhenSocketIsNotDefaultSslImpl() throws Exception
    {
        Socket socket = mock(Socket.class);
        LdapHostnameVerificationSSLSocketFactory.makeUseLdapVerification(socket);
    }

    @Test
    public void testForSunSslImplementationPackageFailsForNonSslClass()
    {
        assertFalse(LdapHostnameVerificationSSLSocketFactory.isInSunSslImplementationPackage(
                Object.class, "Object"));
    }
}
