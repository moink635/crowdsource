package com.atlassian.crowd.search.ldap;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BitwiseFilterTest
{
    @Test
    public void encodeAnd() throws Exception
    {
        assertEquals("(myAttribute:1.2.840.113556.1.4.803:=42)", BitwiseFilter.and("myAttribute", 42).encode());
    }

    @Test
    public void encodeOr() throws Exception
    {
        assertEquals("(myAttribute:1.2.840.113556.1.4.804:=42)", BitwiseFilter.or("myAttribute", 42).encode());
    }
}
