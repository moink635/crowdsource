package com.atlassian.crowd.directory;

import java.io.UnsupportedEncodingException;

import javax.naming.InvalidNameException;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.SearchControls;
import javax.naming.ldap.LdapName;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.LdapTemplateWithClassLoaderWrapper;
import com.atlassian.crowd.directory.ldap.mapper.GroupContextMapper;
import com.atlassian.crowd.directory.ldap.mapper.UserContextMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.ObjectSIDMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.PrimaryGroupIdMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.group.RFC4519MemberDnMapper;
import com.atlassian.crowd.directory.ldap.name.GenericConverter;
import com.atlassian.crowd.directory.ldap.name.SearchDN;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.LDAPGroupWithAttributes;
import com.atlassian.crowd.model.user.LDAPUserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.ldap.ActiveDirectoryQueryTranslaterImpl;
import com.atlassian.crowd.search.ldap.LDAPQuery;
import com.atlassian.crowd.search.ldap.NullResultException;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.crowd.util.PasswordHelper;
import com.atlassian.event.api.EventPublisher;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DirContextProcessor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MicrosoftActiveDirectoryTest extends RFC4519DirectoryTest
{
    private static final LdapName GROUP_DN2;
    private static final LdapName CHILD_DN3;

    static
    {
        try
        {
            GROUP_DN2 = new LdapName("cn=parent2,dc=test");
            CHILD_DN3 = new LdapName("cn=child3,dc=test");
        }
        catch (InvalidNameException e)
        {
            throw new RuntimeException(e);
        }
    }

    private static final EntityQuery<Group> GROUP_ENTITY_QUERY_BY_SID =
            QueryBuilder.queryFor(Group.class, EntityDescriptor.group())
                .with(Restriction.on(MicrosoftActiveDirectory.OBJECT_SID).exactlyMatching("S-object-id-that-ends-with-99"))
                .returningAtMost(1);

    private static final EntityQuery<String> GROUP_NAME_QUERY_BY_SID =
            QueryBuilder.queryFor(String.class, EntityDescriptor.group())
                .with(Restriction.on(MicrosoftActiveDirectory.OBJECT_SID).exactlyMatching("S-object-id-that-ends-with-99"))
                .returningAtMost(1);

    public static final String PRIMARY_GROUP_FILTER = "primary-group-filter";

    @Mock
    private PasswordHelper passwordHelper;

    /**
     * Use this to indicate that primary group support is irrelevant to a given test. (Note that the fact that a test is
     * not using this does not necessarily mean that primary group support does matter for that test, because this method
     * could have been added after those tests were written).
     */
    @Override
    protected MicrosoftActiveDirectory createDirectory() throws InvalidNameException, NullResultException
    {
        return createDirectory(false);
    }

    private MicrosoftActiveDirectory createDirectory(final boolean primaryGroupSupport)
            throws InvalidNameException, NullResultException
    {
        ActiveDirectoryQueryTranslaterImpl queryTranslater = mock(ActiveDirectoryQueryTranslaterImpl.class);
        EventPublisher eventPublisher = mock(EventPublisher.class);
        InstanceFactory instanceFactory = mock(InstanceFactory.class);

        return new MicrosoftActiveDirectory(queryTranslater, eventPublisher, instanceFactory, passwordHelper)
        {
            {
                this.ldapTemplate = mock(LdapTemplateWithClassLoaderWrapper.class);
                this.ldapPropertiesMapper = mock(LDAPPropertiesMapper.class);
                this.searchDN = mock(SearchDN.class);

                when(ldapPropertiesMapper.getUserNameAttribute()).thenReturn("cn");
                when(ldapPropertiesMapper.getUserEmailAttribute()).thenReturn("email");
                when(ldapPropertiesMapper.getUserDisplayNameAttribute()).thenReturn("dn");
                when(ldapPropertiesMapper.getUserFirstNameAttribute()).thenReturn("firstName");
                when(ldapPropertiesMapper.getUserLastNameAttribute()).thenReturn("lastName");
                when(ldapPropertiesMapper.getGroupNameAttribute()).thenReturn("cn");
                when(ldapPropertiesMapper.getGroupMemberAttribute()).thenReturn("member");
                when(ldapPropertiesMapper.getGroupDescriptionAttribute()).thenReturn("description");
                when(ldapPropertiesMapper.getUserGroupMembershipsAttribute()).thenReturn("memberOf");
                when(ldapPropertiesMapper.getUserFilter()).thenReturn("(user-filter)");
                when(ldapPropertiesMapper.getGroupFilter()).thenReturn("(group-filter)");
                when(ldapPropertiesMapper.getExternalIdAttribute()).thenReturn("objectGUID");

                when(this.searchDN.getUser()).thenReturn(BASE_DN);
                when(this.searchDN.getGroup()).thenReturn(BASE_DN);
                when(this.searchDN.getRole()).thenReturn(BASE_DN);  // legacy

                when(ldapQueryTranslater.asLDAPFilter(GROUP_ENTITY_QUERY_BY_SID, ldapPropertiesMapper))
                        .thenReturn(new LDAPQuery(PRIMARY_GROUP_FILTER));
            }

            @Override
            boolean isPrimaryGroupSupportEnabled()
            {
                return primaryGroupSupport;
            }
        };
    }

    private static void verifyPrimaryGroupNameWasLookedUpBySid(MicrosoftActiveDirectory directory)
    {
        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN), eq(PRIMARY_GROUP_FILTER),
                any(SearchControls.class),
                eq(NamedLdapEntity.mapperFromAttribute("cn")),
                any(DirContextProcessor.class), eq(1));
    }

    private static void verifyPrimaryGroupWasLookedUpBySid(MicrosoftActiveDirectory directory)
    {
        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN), eq(PRIMARY_GROUP_FILTER),
                any(SearchControls.class),
                any(GroupContextMapper.class),
                any(DirContextProcessor.class), eq(1));
    }

    @Test
    public void fetchHighestUsnParsesString() throws Exception
    {
        MicrosoftActiveDirectory md = createDirectory(false);

        BasicAttributes attrs = new BasicAttributes();
        attrs.put("highestCommittedUSN", "1");
        DirContextAdapter dca = new DirContextAdapter(attrs, null);

        when(md.ldapTemplate.lookup(GenericConverter.emptyLdapName())).thenReturn(dca);

        assertEquals(1, md.fetchHighestCommittedUSN());
    }

    @Test(expected = OperationFailedException.class)
    public void fetchHighestUsnFailsWithSpecificExceptionWhenAttributeIsMissing() throws Exception
    {
        MicrosoftActiveDirectory md = createDirectory(false);

        BasicAttributes attrs = new BasicAttributes();
        DirContextAdapter dca = new DirContextAdapter(attrs, null);

        when(md.ldapTemplate.lookup(GenericConverter.emptyLdapName())).thenReturn(dca);

        md.fetchHighestCommittedUSN();
    }

    @Test(expected = OperationFailedException.class)
    public void fetchHighestUsnFailsWithSpecificExceptionWhenAttributeCannotBeParsedAsNumber() throws Exception
    {
        MicrosoftActiveDirectory md = createDirectory(false);

        BasicAttributes attrs = new BasicAttributes();
        attrs.put("highestCommittedUSN", "X");
        DirContextAdapter dca = new DirContextAdapter(attrs, null);

        when(md.ldapTemplate.lookup(GenericConverter.emptyLdapName())).thenReturn(dca);


        md.fetchHighestCommittedUSN();
    }

    @Test
    public void isResultsPageFullWhenQueryHasFiniteMaxResults()
    {
        assertFalse(MicrosoftActiveDirectory.isResultPageFull(ImmutableList.of(), 3));
        assertFalse(MicrosoftActiveDirectory.isResultPageFull(ImmutableList.of("a", "b"), 3));
        assertTrue(MicrosoftActiveDirectory.isResultPageFull(ImmutableList.of("a", "b", "c"), 3));
    }

    @Test
    public void isResultsPageFullWhenQueryingForAllResults()
    {
        assertFalse(MicrosoftActiveDirectory.isResultPageFull(ImmutableList.of(), EntityQuery.ALL_RESULTS));
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     * primaryGroupId: 99
     * objectSid: S-object-id-that-ends-with-1   # Because we need a base SID
     *
     * # Group
     * dn: cn=parent,dc=test
     * objectSid: S-object-id-that-ends-with-99
     * </pre>
     * @throws Exception
     */
    @Test
    public void addUserToGroupWhenItIsAlreadyThePrimaryGroup() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        when(user.getValue("objectSid")).thenReturn("S-object-id-that-ends-with-1");
        when(user.getValue("primaryGroupId")).thenReturn("99");
        registerUserInDirectory(user, directory);

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        when(group.getValues("memberDNs")).thenReturn(ImmutableSet.<String>of());  // not a direct member
        when(group.getValue("objectSid")).thenReturn("S-object-id-that-ends-with-99");
        registerGroupInDirectory(group, directory);

        expectedException.expect(MembershipAlreadyExistsException.class);
        expectedException.expectMessage("Membership already exists in directory [0] from child entity [user] to parent entity [group]");

        directory.addUserToGroup("user", "group");
    }


    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     * primaryGroupId: 99
     * objectSid: S-object-id-that-ends-with-1   # Because we need a base SID
     *
     * # Group
     * dn: cn=parent,dc=test
     * objectSid: S-object-id-that-ends-with-99
     * </pre>
     * @throws Exception
     */
    @Test
    public void removeUserFromGroupWhenItIsThePrimaryGroup() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        when(user.getValue("objectSid")).thenReturn("S-object-id-that-ends-with-1");
        when(user.getValue("primaryGroupId")).thenReturn("99");
        registerUserInDirectory(user, directory);

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        when(group.getValues("memberDNs")).thenReturn(ImmutableSet.<String>of());  // not a direct member
        when(group.getValue("objectSid")).thenReturn("S-object-id-that-ends-with-99");
        registerGroupInDirectory(group, directory);

        expectedException.expect(OperationFailedException.class);
        expectedException.expectMessage("Cannot remove user 'user' from group 'group' because it is the primary group of the user");

        directory.removeUserFromGroup("user", "group");
    }


    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     * primaryGroupId: 99
     *
     * # Group
     * dn: cn=parent,dc=test
     * objectSid: S-object-id-that-ends-with-99
     * </pre>
     * @throws Exception
     */
    @Test
    public void removeGroupWhenItIsAPrimaryGroup() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);

        // group

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        when(group.getValue("objectSid")).thenReturn("S-object-id-that-ends-with-99");
        registerGroupInDirectory(group, directory);

        // users

        when(directory.ldapTemplate.searchWithLimitedResults(eq(BASE_DN), eq("(&(user-filter)(primaryGroupId=99))"),
                                                             any(SearchControls.class),
                                                             any(ContextMapper.class),
                                                             any(DirContextProcessor.class), eq(1)))
            .thenReturn(ImmutableList.of(new NamedLdapEntity(CHILD_DN1, "user")));

        // actual invocation

        expectedException.expect(OperationFailedException.class);
        expectedException.expectMessage("Cannot remove group 'group' because it is the primary group of some user(s), "
                + "including 'cn=child1,dc=test'");

        directory.removeGroup("group");
    }

    @Override
    @Test
    public void isUserDirectGroupMember_ViaMemberDNs() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        registerUserInDirectory(user, directory);

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        when(group.getValues("memberDNs")).thenReturn(ImmutableSet.of(CHILD_DN1.toString()));
        registerGroupInDirectory(group, directory);

        assertTrue(directory.isUserDirectGroupMember("user", "group"));

        // verify queries

        verifyUserWasSearchedByName(directory, "user");
        verifyGroupWasSearchedByName(directory, "group");

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     * primaryGroupId: 99
     * objectSid: S-object-id-that-ends-with-1   # Because we need a base SID
     *
     * # Group
     * dn: cn=parent,dc=test
     * objectSid: S-object-id-that-ends-with-99
     * </pre>
     * @throws Exception
     */
    @Test
    public void isUserDirectGroupMemberWhenUsingPrimaryGroup() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        when(user.getValue("objectSid")).thenReturn("S-object-id-that-ends-with-1");
        when(user.getValue("primaryGroupId")).thenReturn("99");
        registerUserInDirectory(user, directory);

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        when(group.getValues("memberDNs")).thenReturn(ImmutableSet.<String>of());  // not a direct member
        when(group.getValue("objectSid")).thenReturn("S-object-id-that-ends-with-99");
        registerGroupInDirectory(group, directory);

        assertTrue(directory.isUserDirectGroupMember("user", "group"));

        // verify queries

        verifyUserWasSearchedByName(directory, "user");
        verifyGroupWasSearchedByName(directory, "group");

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     * primaryGroupId: 99
     * objectSid: S-1-2-3-555
     *
     * # Group 1
     * dn: cn=parent,dc=test
     * objectSid: S-1-2-3-88
     *
     * # Group 2
     * dn: cn=parent2,dc=test
     * objectSid: S-1-2-3-99
     * </pre>
     * @throws Exception
     */
    @Test
    public void isUserMemberOfPrimaryGroupResolvesGroupSidUsingPrimaryGroupIdAttribute() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1);
        when(user.getValue(PrimaryGroupIdMapper.ATTRIBUTE_KEY)).thenReturn("99");
        when(user.getValue(ObjectSIDMapper.ATTRIBUTE_KEY)).thenReturn("S-1-2-3-555");

        LDAPGroupWithAttributes group1 = createLDAPGroupWithAttributes(GROUP_DN);
        LDAPGroupWithAttributes group2 = createLDAPGroupWithAttributes(GROUP_DN2);

        when(group1.getValue(ObjectSIDMapper.ATTRIBUTE_KEY)).thenReturn("S-1-2-3-88");
        when(group2.getValue(ObjectSIDMapper.ATTRIBUTE_KEY)).thenReturn("S-1-2-3-99");

        assertFalse(directory.isUserMemberOfPrimaryGroup(user, group1));
        assertTrue(directory.isUserMemberOfPrimaryGroup(user, group2));
    }

    @Override
    @Test
    public void searchGroupRelationships_QueryGroupsOfUser_ViaMemberDn() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);

        // user

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        when(user.getValue("objectSid")).thenReturn("S-object-id-that-ends-with-1");
        when(user.getValue("primaryGroupId")).thenReturn("99");
        registerUserInDirectory(user, directory);

        // regular group

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN);
        registerGroupInDirectory(group, directory, "(&(group-filter)(member=cn=child1,dc=test))");

        // primary group

        LDAPGroupWithAttributes primaryGroup = createLDAPGroupWithAttributes(GROUP_DN2);
        registerGroupInDirectory(primaryGroup, directory, PRIMARY_GROUP_FILTER);

        // actual invocation

        MembershipQuery<LDAPGroupWithAttributes> queryGroupsOfUser =
                QueryBuilder.createMembershipQuery(10, 0, false, EntityDescriptor.group(), LDAPGroupWithAttributes.class,
                        EntityDescriptor.user(), "user");

        assertThat(directory.searchGroupRelationships(queryGroupsOfUser), hasItems(group, primaryGroup));

        // verify queries

        verify(directory.ldapTemplate, times(2)).searchWithLimitedResults(eq(BASE_DN), eq("&((user-filter)(cn=user))"),
                                                                          any(SearchControls.class),
                                                                          any(UserContextMapper.class),
                                                                          any(DirContextProcessor.class), eq(1));

        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN),
                                                                eq("(&(group-filter)(member=cn=child1,dc=test))"),
                                                                any(SearchControls.class),
                                                                any(GroupContextMapper.class),
                                                                any(DirContextProcessor.class), eq(10));

        verifyPrimaryGroupWasLookedUpBySid(directory);
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     * primaryGroupId: 99
     * objectSid: S-object-id-that-ends-with-1
     *
     * # Group 1
     * dn: cn=parent,dc=test
     *
     * # Group 2
     * dn: cn=parent2,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void searchGroupRelationships_QueryGroupsOfUser_WhenPrimaryGroupCannotBeFound() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);

        // user

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        when(user.getValue("objectSid")).thenReturn("S-object-id-that-ends-with-1");
        when(user.getValue("primaryGroupId")).thenReturn("99");
        registerUserInDirectory(user, directory);

        // regular group

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN);
        registerGroupInDirectory(group, directory, "(&(group-filter)(member=cn=child1,dc=test))");

        // primary group

        when(directory.ldapTemplate.searchWithLimitedResults(eq(BASE_DN), eq(PRIMARY_GROUP_FILTER),
                any(SearchControls.class),
                any(GroupContextMapper.class),
                any(DirContextProcessor.class), eq(1)))
            .thenReturn(ImmutableList.<LDAPGroupWithAttributes>of()); // primary group not found


        // actual invocation

        MembershipQuery<LDAPGroupWithAttributes> queryGroupsOfUser =
                QueryBuilder.createMembershipQuery(10, 0, false, EntityDescriptor.group(), LDAPGroupWithAttributes.class,
                        EntityDescriptor.user(), "user");

        assertThat(directory.searchGroupRelationships(queryGroupsOfUser),
                hasItem(group)); // primary group skipped

        // verify queries

        verify(directory.ldapTemplate, times(2)).searchWithLimitedResults(eq(BASE_DN), eq("&((user-filter)(cn=user))"),
                                                                          any(SearchControls.class),
                                                                          any(UserContextMapper.class),
                                                                          any(DirContextProcessor.class), eq(1));

        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN),
                                                                eq("(&(group-filter)(member=cn=child1,dc=test))"),
                                                                any(SearchControls.class),
                                                                any(GroupContextMapper.class),
                                                                any(DirContextProcessor.class), eq(10));

        verifyPrimaryGroupWasLookedUpBySid(directory);
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    @Override
    @Test
    public void searchGroupRelationships_QueryGroupNamesOfUser_ViaMemberDn() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(false);

        // user

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        when(user.getValue("objectSid")).thenReturn("S-object-id-that-ends-with-1");
        when(user.getValue("primaryGroupId")).thenReturn("99");
        registerUserInDirectory(user, directory);

        // regular group

        when(directory.ldapTemplate.searchWithLimitedResults(eq(BASE_DN),
                                                             eq("(&(group-filter)(member=cn=child1,dc=test))"),
                                                             any(SearchControls.class),
                                                             eq(NamedLdapEntity.mapperFromAttribute("cn")),
                                                             any(DirContextProcessor.class), eq(10)))
            .thenReturn(ImmutableList.of(new NamedLdapEntity(GROUP_DN, "group")));

        // primary group

        when(directory.ldapQueryTranslater.asLDAPFilter(GROUP_NAME_QUERY_BY_SID, directory.ldapPropertiesMapper))
            .thenReturn(new LDAPQuery(PRIMARY_GROUP_FILTER));

        when(directory.ldapTemplate.searchWithLimitedResults(eq(BASE_DN), eq(PRIMARY_GROUP_FILTER),
                                                             any(SearchControls.class),
                                                             eq(NamedLdapEntity.mapperFromAttribute("cn")),
                                                             any(DirContextProcessor.class), eq(1)))
            .thenReturn(ImmutableList.of(new NamedLdapEntity(GROUP_DN2, "primaryGroup")));

        // actual invocation

        MembershipQuery<String> queryGroupNamesOfUser =
            QueryBuilder.createMembershipQuery(10, 0, false, EntityDescriptor.group(), String.class,
                                               EntityDescriptor.user(), "user");

        assertThat(directory.searchGroupRelationships(queryGroupNamesOfUser), hasItems("group", "primaryGroup"));

        // verify queries

        verify(directory.ldapTemplate, times(2)).searchWithLimitedResults(eq(BASE_DN), eq("&((user-filter)(cn=user))"),
                                                                          any(SearchControls.class),
                                                                          any(UserContextMapper.class),
                                                                          any(DirContextProcessor.class), eq(1));

        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN),
                                                                eq("(&(group-filter)(member=cn=child1,dc=test))"),
                                                                any(SearchControls.class),
                                                                eq(NamedLdapEntity.mapperFromAttribute("cn")),
                                                                any(DirContextProcessor.class), eq(10));

        verifyPrimaryGroupNameWasLookedUpBySid(directory);
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     * objectSid: S-object-id-that-ends-with-1
     * primaryGroupId: 99
     *
     * # Regular Group
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     *
     * # Primary Group
     * dn: cn=parent2,dc=test
     * objectSid: 99
     * </pre>
     * @throws Exception
     */
    @Test
    public void searchGroupRelationships_QueryGroupNamesOfUser_WhenPrimaryGroupCannotBeFound() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(false);

        // user

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        when(user.getValue("objectSid")).thenReturn("S-object-id-that-ends-with-1");
        when(user.getValue("primaryGroupId")).thenReturn("99");
        registerUserInDirectory(user, directory);

        // regular group

        when(directory.ldapTemplate.searchWithLimitedResults(eq(BASE_DN),
                                                             eq("(&(group-filter)(member=cn=child1,dc=test))"),
                                                             any(SearchControls.class),
                                                             eq(NamedLdapEntity.mapperFromAttribute("cn")),
                                                             any(DirContextProcessor.class), eq(10)))
            .thenReturn(ImmutableList.of(new NamedLdapEntity(GROUP_DN, "group")));

        // primary group

        when(directory.ldapQueryTranslater.asLDAPFilter(GROUP_NAME_QUERY_BY_SID, directory.ldapPropertiesMapper))
            .thenReturn(new LDAPQuery(PRIMARY_GROUP_FILTER));

        when(directory.ldapTemplate.searchWithLimitedResults(eq(BASE_DN), eq(PRIMARY_GROUP_FILTER),
                                                             any(SearchControls.class),
                                                             eq(NamedLdapEntity.mapperFromAttribute("cn")),
                                                             any(DirContextProcessor.class), eq(1)))
            .thenReturn(ImmutableList.<NamedLdapEntity>of());  // primary group not found


        // actual invocation

        MembershipQuery<String> queryGroupNamesOfUser =
            QueryBuilder.createMembershipQuery(10, 0, false, EntityDescriptor.group(), String.class,
                                               EntityDescriptor.user(), "user");

        assertThat(directory.searchGroupRelationships(queryGroupNamesOfUser), hasItem("group")); // primary group skipped

        // verify queries

        verify(directory.ldapTemplate, times(2)).searchWithLimitedResults(eq(BASE_DN), eq("&((user-filter)(cn=user))"),
                                                                          any(SearchControls.class),
                                                                          any(UserContextMapper.class),
                                                                          any(DirContextProcessor.class), eq(1));

        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN),
                                                                eq("(&(group-filter)(member=cn=child1,dc=test))"),
                                                                any(SearchControls.class),
                                                                eq(NamedLdapEntity.mapperFromAttribute("cn")),
                                                                any(DirContextProcessor.class), eq(10));

        verifyPrimaryGroupNameWasLookedUpBySid(directory);
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    @Override
    @Test
    public void searchGroupRelationships_QueryUsersOfGroup_ViaMemberDN() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(false);

        // group

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        when(group.getValues("memberDNs")).thenReturn(ImmutableSet.of(CHILD_DN1.toString()));
        when(group.getValue("objectSid")).thenReturn("S-object-id-ending-with-99");
        registerGroupInDirectory(group, directory);

        // user who is a member via primary group

        LDAPUserWithAttributes user2 = createLDAPUserWithAttributes(CHILD_DN2);
        registerUserInDirectory(user2, directory, "(&(user-filter)(primaryGroupId=99))");

        // regular user

        LDAPUserWithAttributes user1 = createLDAPUserWithAttributes(CHILD_DN1);
        when(directory.ldapTemplate.search(eq(CHILD_DN1), eq("(user-filter)"), any(SearchControls.class),
                any(UserContextMapper.class)))
            .thenReturn(ImmutableList.of(user1, user2));  // both match the user filter

        // actual invocation

        MembershipQuery<LDAPUserWithAttributes> queryUsersOfGroup =
                QueryBuilder.createMembershipQuery(10, 0, true, EntityDescriptor.user(), LDAPUserWithAttributes.class,
                        EntityDescriptor.group(), "group");

        assertThat(directory.searchGroupRelationships(queryUsersOfGroup), hasItems(user1, user2));

        // verify queries

        verify(directory.ldapTemplate, times(2)).searchWithLimitedResults(eq(BASE_DN), eq("&((group-filter)(cn=group))"),
                                                                          any(SearchControls.class),
                                                                          any(GroupContextMapper.class),
                                                                          any(DirContextProcessor.class), eq(1));

        verify(directory.ldapTemplate).search(eq(CHILD_DN1), eq("(user-filter)"), any(SearchControls.class),
                                              any(UserContextMapper.class));

        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN), eq("(&(user-filter)(primaryGroupId=99))"),
                                                                any(SearchControls.class),
                                                                any(UserContextMapper.class),
                                                                any(DirContextProcessor.class), eq(10));
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    @Override
    @Test
    public void searchGroupRelationships_QueryUsersOfGroup_ViaMemberOf() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(true);

        // group

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        when(group.getType()).thenReturn(GroupType.GROUP);
        when(group.getValue("objectSid")).thenReturn("S-object-id-ending-with-99");
        registerGroupInDirectory(group, directory);

        // regular user

        LDAPUserWithAttributes user1 = createLDAPUserWithAttributes(CHILD_DN1);
        registerUserInDirectory(user1, directory, "(&(user-filter)(memberOf=cn=parent,dc=test))");

        // user who is a member via primary group

        LDAPUserWithAttributes user2 = createLDAPUserWithAttributes(CHILD_DN2);
        registerUserInDirectory(user2, directory, "(&(user-filter)(primaryGroupId=99))");

        // actual invocation

        MembershipQuery<LDAPUserWithAttributes> queryUsersOfGroup =
                QueryBuilder.createMembershipQuery(10, 0, true, EntityDescriptor.user(), LDAPUserWithAttributes.class,
                        EntityDescriptor.group(), "group");

        assertThat(directory.searchGroupRelationships(queryUsersOfGroup), hasItems(user1, user2));

        // verify queries

        verify(directory.ldapTemplate, times(2)).searchWithLimitedResults(eq(BASE_DN), eq("&((group-filter)(cn=group))"),
                                                                          any(SearchControls.class),
                                                                          any(GroupContextMapper.class),
                                                                          any(DirContextProcessor.class), eq(1));

        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN),
                                                                eq("(&(user-filter)(memberOf=cn=parent,dc=test))"),
                                                                any(SearchControls.class),
                                                                any(UserContextMapper.class),
                                                                any(DirContextProcessor.class), eq(10));

        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN), eq("(&(user-filter)(primaryGroupId=99))"),
                                                                any(SearchControls.class),
                                                                any(UserContextMapper.class),
                                                                any(DirContextProcessor.class), eq(10));
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # One user
     * dn: cn=child,dc=test
     *
     * # Another user
     * dn: cn=child3,dc=test
     * primaryGroupId: 123
     *
     * # Primary Group
     * dn: cn=parent,dc=test
     * member: cn=child,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void findDirectMembersOfGroupWhenUsingMemberAttribute() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(false);
        when(directory.getLdapPropertiesMapper().isNestedGroupsDisabled()).thenReturn(false);

        LDAPGroupWithAttributes ldapGroupWithAttributes = createLDAPGroupWithAttributes(GROUP_DN);
        when(ldapGroupWithAttributes.getValues(RFC4519MemberDnMapper.ATTRIBUTE_KEY))
            .thenReturn(ImmutableSet.of(CHILD_DN1.toString()));

        when(directory.ldapTemplate.lookup(eq(GROUP_DN), any(GroupContextMapper.class)))
            .thenReturn(ldapGroupWithAttributes);

        // additional interactions introduced by the overridden method

        when(ldapGroupWithAttributes.getValue(ObjectSIDMapper.ATTRIBUTE_KEY))
            .thenReturn("S-parent-object-sid-terminated-with-123");

        when(directory.ldapTemplate.search(eq(BASE_DN), eq("(&(user-filter)(primaryGroupId=123))"),
                                           any(SearchControls.class), any(ContextMapper.class),
                                           any(DirContextProcessor.class)))
            .thenReturn(ImmutableList.of(new NamedLdapEntity(CHILD_DN3, "child3")));

        assertThat(directory.findDirectMembersOfGroup(GROUP_DN), hasItems(CHILD_DN1, CHILD_DN3));

        // verify queries made to the server

        verify(directory.ldapTemplate, times(2)).lookup(eq(GROUP_DN), any(GroupContextMapper.class));
        verify(directory.ldapTemplate).search(eq(BASE_DN), eq("(&(user-filter)(primaryGroupId=123))"),
                                              any(SearchControls.class), any(ContextMapper.class),
                                              any(DirContextProcessor.class));
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # One user
     * dn: cn=child2,dc=test
     * memberOf=cn=parent,dc=test
     *
     * # Another user
     * dn: cn=child3,dc=test
     * primaryGroupId=123
     *
     * # Primary Group
     * dn: cn=parent,dc=test
     * objectSid: S-parent-object-sid-terminated-with-123
     * </pre>
     * @throws Exception
     */
    @Test
    public void findDirectMembersOfGroupWhenUsingMemberOfAttributeAndNestedGroupsDisabled() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(true);
        when(directory.getLdapPropertiesMapper().isNestedGroupsDisabled()).thenReturn(true);
        when(directory.getLdapPropertiesMapper().isPagedResultsControl()).thenReturn(false);

        when(directory.ldapTemplate.search(eq(BASE_DN), eq("(&(user-filter)(memberOf=cn=parent,dc=test))"),
                                           any(SearchControls.class), any(ContextMapper.class),
                                           any(DirContextProcessor.class)))
            .thenReturn(ImmutableList.of(CHILD_DN2));

        // additional interactions introduced by the overridden method

        LDAPGroupWithAttributes ldapGroupWithAttributes = createLDAPGroupWithAttributes(GROUP_DN);
        when(directory.ldapTemplate.lookup(eq(GROUP_DN), any(GroupContextMapper.class)))
            .thenReturn(ldapGroupWithAttributes);

        when(ldapGroupWithAttributes.getValue(ObjectSIDMapper.ATTRIBUTE_KEY))
            .thenReturn("S-parent-object-sid-terminated-with-123");

        when(directory.ldapTemplate.search(eq(BASE_DN), eq("(&(user-filter)(primaryGroupId=123))"),
                                           any(SearchControls.class), any(ContextMapper.class),
                                           any(DirContextProcessor.class)))
            .thenReturn(ImmutableList.of(new NamedLdapEntity(CHILD_DN3, "child3")));

        // verify queries made to the server

        assertThat(directory.findDirectMembersOfGroup(GROUP_DN), hasItems(CHILD_DN2, CHILD_DN3));

        verify(directory.ldapTemplate).search(eq(BASE_DN), eq("(&(user-filter)(memberOf=cn=parent,dc=test))"),
                                              any(SearchControls.class), any(ContextMapper.class),
                                              any(DirContextProcessor.class));
        verify(directory.ldapTemplate).lookup(eq(GROUP_DN), any(GroupContextMapper.class));
        verify(directory.ldapTemplate).search(eq(BASE_DN), eq("(&(user-filter)(primaryGroupId=123))"),
                                              any(SearchControls.class), any(ContextMapper.class),
                                              any(DirContextProcessor.class));
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # One user
     * dn: cn=child,dc=test
     *
     * # Another user
     * dn: cn=child2,dc=test
     * memberOf=cn=parent,dc=test
     *
     * # Yet another user
     * dn: cn=child3,dc=test
     * primaryGroupId=123
     *
     * # Primary Group
     * dn: cn=parent,dc=test
     * objectSid: S-parent-object-sid-terminated-with-123
     * member: dc=child,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void findDirectMembersOfGroupWhenUsingMemberOfAttributeAndNestedGroupsEnabled() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(true);
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(true);
        when(directory.getLdapPropertiesMapper().isNestedGroupsDisabled()).thenReturn(false);
        when(directory.getLdapPropertiesMapper().isPagedResultsControl()).thenReturn(false);

        LDAPGroupWithAttributes ldapGroupWithAttributes = createLDAPGroupWithAttributes(GROUP_DN);
        when(ldapGroupWithAttributes.getValues(RFC4519MemberDnMapper.ATTRIBUTE_KEY))
            .thenReturn(ImmutableSet.of(CHILD_DN1.toString()));

        when(directory.ldapTemplate.lookup(eq(GROUP_DN), any(GroupContextMapper.class)))
            .thenReturn(ldapGroupWithAttributes);

        when(directory.ldapTemplate.search(eq(BASE_DN), eq("(&(user-filter)(memberOf=cn=parent,dc=test))"),
                                           any(SearchControls.class), any(ContextMapper.class),
                                           any(DirContextProcessor.class)))
            .thenReturn(ImmutableList.of(CHILD_DN2));

        // additional interactions introduced by the overridden method

        when(ldapGroupWithAttributes.getValue(ObjectSIDMapper.ATTRIBUTE_KEY))
            .thenReturn("S-parent-object-sid-terminated-with-123");

        when(directory.ldapTemplate.search(eq(BASE_DN), eq("(&(user-filter)(primaryGroupId=123))"),
                                           any(SearchControls.class), any(ContextMapper.class),
                                           any(DirContextProcessor.class)))
            .thenReturn(ImmutableList.of(new NamedLdapEntity(CHILD_DN3, "child3")));

        assertThat(directory.findDirectMembersOfGroup(GROUP_DN), hasItems(CHILD_DN1, CHILD_DN2, CHILD_DN3));

        // verify queries made to the server

        verify(directory.ldapTemplate, times(2)).lookup(eq(GROUP_DN), any(GroupContextMapper.class));
        verify(directory.ldapTemplate).search(eq(BASE_DN), eq("(&(user-filter)(memberOf=cn=parent,dc=test))"),
                                              any(SearchControls.class), any(ContextMapper.class),
                                              any(DirContextProcessor.class));
        verify(directory.ldapTemplate).search(eq(BASE_DN), eq("(&(user-filter)(primaryGroupId=123))"),
                                              any(SearchControls.class), any(ContextMapper.class),
                                              any(DirContextProcessor.class));
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    @Test
    public void shouldSupportInactiveAccounts() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory(false);
        assertTrue(directory.supportsInactiveAccounts());  // note that the parent class does not
    }

    @Test
    public void encodePasswordShouldQuoteAndEncodeUnencryptedPasswordToUtf16Le() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory();

        assertArrayEquals("\"my-password\"".getBytes("UTF-16LE"),
                directory.encodePassword(PasswordCredential.unencrypted("my-password")));
    }

    @Test
    public void encodePasswordShouldQuoteAndEncodeARandomPasswordWhenAskedToEncodePasswordCredentialNone() throws Exception
    {
        when(passwordHelper.generateRandomPassword()).thenReturn("random-password");

        MicrosoftActiveDirectory directory = createDirectory();

        String passwordUsedByDirectory = decodeEncodedPassword(directory.encodePassword(PasswordCredential.NONE));

        verify(passwordHelper).generateRandomPassword();
        assertEquals("random-password", passwordUsedByDirectory);
    }

    private static String decodeEncodedPassword(byte[] encodedBytes) throws UnsupportedEncodingException
    {
        String decodedQuotedPassword = new String(encodedBytes, "UTF-16LE");
        assertThat(decodedQuotedPassword, allOf(startsWith("\""), endsWith("\"")));

        return decodedQuotedPassword.substring(1, decodedQuotedPassword.length() - 1);
    }

    @Test(expected = InvalidCredentialException.class)
    public void encodePasswordShouldFailToEncodeAnEncryptedPasswordIfItIsNotPasswordCredentialNone() throws Exception
    {
        MicrosoftActiveDirectory directory = createDirectory();

        directory.encodePassword(PasswordCredential.encrypted("my-password"));
    }
}
