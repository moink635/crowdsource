package com.atlassian.crowd.directory;

import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A matcher for {@link ModificationItem}.
 *
 */
class ModificationItemMatcher extends TypeSafeMatcher<ModificationItem>
{
    private final int modificationOp;
    private final Matcher<Attribute> attributeMatcher;

    private ModificationItemMatcher(int modificationOp, Matcher<Attribute> attributeMatcher)
    {
        this.modificationOp = modificationOp;
        this.attributeMatcher = checkNotNull(attributeMatcher);
    }

    @Factory
    public static ModificationItemMatcher addAttribute(Matcher<Attribute> attributeMatcher)
    {
        return new ModificationItemMatcher(DirContext.ADD_ATTRIBUTE, attributeMatcher);
    }

    @Factory
    public static ModificationItemMatcher removeAttribute(Matcher<Attribute> attributeMatcher)
    {
        return new ModificationItemMatcher(DirContext.REMOVE_ATTRIBUTE, attributeMatcher);
    }

    @Factory
    public static ModificationItemMatcher replaceAttribute(Matcher<Attribute> attributeMatcher)
    {
        return new ModificationItemMatcher(DirContext.REPLACE_ATTRIBUTE, attributeMatcher);
    }

    @Override
    protected boolean matchesSafely(ModificationItem item)
    {
        return modificationOp == item.getModificationOp()
                && attributeMatcher.matches(item.getAttribute());
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("a modification item to perform operation ").appendValue(modificationOp)
            .appendText(" on ").appendDescriptionOf(attributeMatcher);
    }
}
