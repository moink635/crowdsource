package com.atlassian.crowd.directory.ldap.util;

import javax.naming.NamingException;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class IncrementalAttributeMapperTest
{
    @Test
    public void rangeInResponseIsParsed() throws NamingException
    {
        AttributeValueProcessor valueProcessor = mock(AttributeValueProcessor.class);
        IncrementalAttributeMapper mapper = new IncrementalAttributeMapper("item", valueProcessor);

        BasicAttributes attributes = new BasicAttributes();
        attributes.put(new BasicAttribute("item;Range=0-500", "value"));

        mapper.mapFromAttributes(attributes);

        verify(valueProcessor).process("value");

        assertTrue(mapper.hasMore());

        assertArrayEquals("The next range should follow on from this one",
                new String[]{"item;Range=501-*"},
                mapper.getAttributesArray());
    }

    @Test
    public void responseWithEntireRangeIsParsed() throws NamingException
    {
        AttributeValueProcessor valueProcessor = mock(AttributeValueProcessor.class);
        IncrementalAttributeMapper mapper = new IncrementalAttributeMapper("item", valueProcessor);

        BasicAttributes attributes = new BasicAttributes();
        attributes.put(new BasicAttribute("item", "value"));

        mapper.mapFromAttributes(attributes);

        verify(valueProcessor).process("value");

        assertFalse(mapper.hasMore());
    }

    @Test
    public void responseWithRangeIndicatingTerminalIsParsed() throws NamingException
    {
        AttributeValueProcessor valueProcessor = mock(AttributeValueProcessor.class);
        IncrementalAttributeMapper mapper = new IncrementalAttributeMapper("item;Range=0-*", valueProcessor);

        BasicAttributes attributes = new BasicAttributes();
        attributes.put(new BasicAttribute("item;Range=0-*", "value"));

        mapper.mapFromAttributes(attributes);

        verify(valueProcessor).process("value");

        assertFalse(mapper.hasMore());
    }
}
