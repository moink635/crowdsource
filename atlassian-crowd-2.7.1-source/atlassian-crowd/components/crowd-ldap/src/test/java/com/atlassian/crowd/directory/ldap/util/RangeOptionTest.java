package com.atlassian.crowd.directory.ldap.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RangeOptionTest
{
    @Test
    public void rangeParsedFromInitialResponseIncludesSpecifiedBounds()
    {
        RangeOption range = RangeOption.parse("Range=0-500");
        assertEquals(0, range.getInitial());
        assertEquals(500, range.getTerminal());
    }

    @Test
    public void nextRangeFollowsOnFromThisOne()
    {
        RangeOption range = new RangeOption(0, 500);
        RangeOption next = range.nextRange(100);
        assertEquals(501, next.getInitial());
        assertEquals(600, next.getTerminal());
    }

    @Test
    public void nextRangeWithEndOfRangeAsTerminalAsUpperFollowsOnFromThisOne()
    {
        RangeOption range = new RangeOption(0, 500);
        RangeOption next = range.nextRange(RangeOption.TERMINAL_END_OF_RANGE);
        assertEquals(501, next.getInitial());
        assertEquals(RangeOption.TERMINAL_END_OF_RANGE, next.getTerminal());
    }

    @Test
    public void stringRepresentationSpecifiesBounds()
    {
        assertEquals("Range=501-600", new RangeOption(501, 600).toString());
    }

    @Test
    public void terminalIsParsedFromFinalRange()
    {
        RangeOption range = RangeOption.parse("Range=501-*");
        assertEquals(501, range.getInitial());
        assertEquals(RangeOption.TERMINAL_END_OF_RANGE, range.getTerminal());
        assertTrue(range.isTerminalEndOfRange());
    }
}
