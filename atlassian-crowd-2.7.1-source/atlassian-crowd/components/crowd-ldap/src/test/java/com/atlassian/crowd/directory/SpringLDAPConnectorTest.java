package com.atlassian.crowd.directory;

import java.util.List;

import javax.naming.InvalidNameException;
import javax.naming.Name;
import javax.naming.directory.SearchControls;
import javax.naming.ldap.LdapName;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.LdapTemplateWithClassLoaderWrapper;
import com.atlassian.crowd.directory.ldap.mapper.ContextMapperWithRequiredAttributes;
import com.atlassian.crowd.directory.ldap.mapper.GroupContextMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.AttributeMapper;
import com.atlassian.crowd.directory.ldap.name.GenericConverter;
import com.atlassian.crowd.directory.ldap.name.SearchDN;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidMembershipException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.ReadOnlyGroupException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.LDAPDirectoryEntity;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplateWithAttributes;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.group.LDAPGroupWithAttributes;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.model.user.LDAPUserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.ldap.LDAPQuery;
import com.atlassian.crowd.search.ldap.LDAPQueryTranslater;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.event.api.EventPublisher;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextProcessor;

import static org.hamcrest.collection.IsEmptyCollection.emptyCollectionOf;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SpringLDAPConnectorTest
{
    private static final long DIRECTORY_ID = 1L;
    private static final LdapName BASE_DN;

    static
    {
        try
        {
            BASE_DN = new LdapName("dc=test");
        }
        catch (InvalidNameException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void nestedGroupsEnabled()
    {
        SpringLDAPConnector slc = mock(SpringLDAPConnector.class, Mockito.CALLS_REAL_METHODS);

        slc.ldapPropertiesMapper = mock(LDAPPropertiesMapper.class);

        when(slc.ldapPropertiesMapper.isNestedGroupsDisabled()).thenReturn(false);

        assertTrue("Nested groups should be enabled", slc.supportsNestedGroups());
    }

    @Test
    public void nestedGroupsDisabled()
    {
        SpringLDAPConnector slc = mock(SpringLDAPConnector.class, Mockito.CALLS_REAL_METHODS);

        slc.ldapPropertiesMapper = mock(LDAPPropertiesMapper.class);

        when(slc.ldapPropertiesMapper.isNestedGroupsDisabled()).thenReturn(false);

        assertTrue("Nested groups should be enabled", slc.supportsNestedGroups());
    }

    @Test
    public void standardiseDnDelegatesBasedOnProperties()
    {
        SpringLDAPConnector slc = mock(SpringLDAPConnector.class);

        slc.ldapPropertiesMapper = mock(LDAPPropertiesMapper.class);

        when(slc.ldapPropertiesMapper.isRelaxedDnStandardisation()).thenReturn(false);
        assertEquals("cn=test", slc.standardiseDN("CN = test"));

        when(slc.ldapPropertiesMapper.isRelaxedDnStandardisation()).thenReturn(true);
        assertEquals("cn = test", slc.standardiseDN("CN = test"));
    }

    @Test
    public void findEntityByDNStandardisesItsSearchDN() throws Exception
    {
        SpringLDAPConnector slc = mock(SpringLDAPConnector.class);
        slc.ldapPropertiesMapper = mock(LDAPPropertiesMapper.class);
        slc.searchDN = mock(SearchDN.class);
        when(slc.findEntityByDN(Mockito.anyString(), Mockito.<Class<LDAPDirectoryEntity>>any())).thenCallRealMethod();

        assertEquals("dn=x", slc.standardiseDN("DN=X"));

        slc.findEntityByDN("DN=X", LDAPUserWithAttributes.class);
        verify(slc).findEntityByDN(
                Mockito.eq("dn=x"),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.<ContextMapper>anyObject(),
                Mockito.eq(LDAPUserWithAttributes.class));
    }

    @Test
    public void testGetStandardisedDNWorksForEmptyDN() throws UserNotFoundException, GroupNotFoundException, OperationFailedException, InvalidNameException
    {
        // We use any implementation. In a more verbosely way, we could as well just stub all methods.
        SpringLDAPConnector slc = new Rfc2307(null, null, null, null);
        slc.ldapPropertiesMapper = mock(LDAPPropertiesMapper.class);
        assertEquals("", slc.getStandardisedDN(GenericConverter.emptyLdapName()));
    }

    @Test
    public void searchControlsAcceptsNullExpectedAttributes()
    {
        SpringLDAPConnector slc = mock(SpringLDAPConnector.class);
        when(slc.getSubTreeSearchControls(Mockito.<ContextMapperWithRequiredAttributes<?>>any())).thenCallRealMethod();

        ContextMapperWithRequiredAttributes<?> mapper = mock(ContextMapperWithRequiredAttributes.class);
        when(mapper.getRequiredLdapAttributes()).thenReturn(null);

        SearchControls controls = slc.getSubTreeSearchControls(mapper);
        assertNull(controls.getReturningAttributes());
    }

    @Test
    public void searchControlsUsesExpectedAttributes()
    {
        SpringLDAPConnector slc = mock(SpringLDAPConnector.class);
        when(slc.getSubTreeSearchControls(Mockito.<ContextMapperWithRequiredAttributes<?>>any())).thenCallRealMethod();

        ContextMapperWithRequiredAttributes<?> mapper = mock(ContextMapperWithRequiredAttributes.class);
        when(mapper.getRequiredLdapAttributes()).thenReturn(ImmutableSet.of("a", "b", "c"));

        SearchControls controls = slc.getSubTreeSearchControls(mapper);
        assertEquals(
                ImmutableSet.of("a", "b", "c"),
                ImmutableSet.copyOf(controls.getReturningAttributes()));
    }

    @Test (expected = GroupNotFoundException.class)
    public void findGroupWithAttributesByNameWhenGroupDoesNotExist() throws Exception
    {
        SpringLDAPConnector slc = createSpringLDAPConnector();

        EntityQuery<Group> query =
            QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP))
                .with(Restriction.on(GroupTermKeys.NAME).exactlyMatching("group"))
                .returningAtMost(1);
        when(slc.ldapQueryTranslater.asLDAPFilter(query, slc.getLdapPropertiesMapper()))
            .thenReturn(new LDAPQuery("filter"));

        when(slc.ldapTemplate.search(eq(BASE_DN), eq("filter"), any(SearchControls.class),
                                     any(GroupContextMapper.class)))
            .thenReturn(ImmutableList.of());

        slc.findGroupWithAttributesByName("group");
    }

    @Test
    public void findGroupWithAttributesByNameReturnsGroup() throws Exception
    {
        SpringLDAPConnector slc = createSpringLDAPConnector();

        EntityQuery<Group> query =
            QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP))
                .with(Restriction.on(GroupTermKeys.NAME).exactlyMatching("group"))
                .returningAtMost(1);
        when(slc.ldapQueryTranslater.asLDAPFilter(query, slc.getLdapPropertiesMapper()))
            .thenReturn(new LDAPQuery("filter"));

        LDAPGroupWithAttributes returnedGroup =
            new LDAPGroupWithAttributes("dn", new GroupTemplateWithAttributes("group", DIRECTORY_ID, GroupType.GROUP));

        when(slc.ldapTemplate.searchWithLimitedResults(eq(BASE_DN), eq("filter"), any(SearchControls.class),
                                     any(GroupContextMapper.class),
                                     any(DirContextProcessor.class), eq(1)))
            .thenReturn(ImmutableList.of(returnedGroup));

        final LDAPGroupWithAttributes actualGroup = slc.findGroupWithAttributesByName("group");

        assertEquals(returnedGroup, actualGroup);
    }

    @Test (expected = GroupNotFoundException.class)
    public void findGroupByNameAndTypeWhenGroupDoesNotExist() throws Exception
    {
        SpringLDAPConnector slc = createSpringLDAPConnector();

        EntityQuery<Group> query =
            QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP))
                .with(Restriction.on(GroupTermKeys.NAME).exactlyMatching("group"))
                .returningAtMost(1);
        when(slc.ldapQueryTranslater.asLDAPFilter(query, slc.getLdapPropertiesMapper()))
            .thenReturn(new LDAPQuery("filter"));

        when(slc.ldapTemplate.search(eq(BASE_DN), eq("filter"), any(SearchControls.class),
                                     any(GroupContextMapper.class)))
            .thenReturn(ImmutableList.of());

        slc.findGroupByNameAndType("group", GroupType.GROUP);
    }

    @Test
    public void findGroupByNameAndTypeWhenGroupTypeIsGroup() throws Exception
    {
        SpringLDAPConnector slc = createSpringLDAPConnector();

        EntityQuery<Group> query =
            QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP))
                .with(Restriction.on(GroupTermKeys.NAME).exactlyMatching("group"))
                .returningAtMost(1);
        when(slc.ldapQueryTranslater.asLDAPFilter(query, slc.getLdapPropertiesMapper()))
            .thenReturn(new LDAPQuery("filter"));

        LDAPGroupWithAttributes returnedGroup =
            new LDAPGroupWithAttributes("dn", new GroupTemplateWithAttributes("group", DIRECTORY_ID, GroupType.GROUP));

        when(slc.ldapTemplate.searchWithLimitedResults(eq(BASE_DN), eq("filter"), any(SearchControls.class),
                                     any(GroupContextMapper.class),
                                     any(DirContextProcessor.class), eq(1)))
            .thenReturn(ImmutableList.of(returnedGroup));

        final LDAPGroupWithAttributes actualGroup = slc.findGroupByNameAndType("group", GroupType.GROUP);

        assertEquals(returnedGroup, actualGroup);
    }

    @Test
    public void findGroupByNameAndTypeWhenGroupTypeIsNull() throws Exception
    {
        SpringLDAPConnector slc = createSpringLDAPConnector();

        EntityQuery<Group> query =
            QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP))
                .with(Restriction.on(GroupTermKeys.NAME).exactlyMatching("group"))
                .returningAtMost(1);
        when(slc.ldapQueryTranslater.asLDAPFilter(query, slc.getLdapPropertiesMapper()))
            .thenReturn(new LDAPQuery("filter"));

        LDAPGroupWithAttributes returnedGroup =
            new LDAPGroupWithAttributes("dn", new GroupTemplateWithAttributes("group", DIRECTORY_ID, GroupType.GROUP));

        when(slc.ldapTemplate.searchWithLimitedResults(eq(BASE_DN), eq("filter"), any(SearchControls.class),
                                     any(GroupContextMapper.class),
                                     any(DirContextProcessor.class), eq(1)
                                     ))
            .thenReturn(ImmutableList.of(returnedGroup));

        final LDAPGroupWithAttributes actualGroup = slc.findGroupByNameAndType("group", null);

        assertEquals(returnedGroup, actualGroup);
    }

    @Test
    public void searchGroupsWhenEntitiesAreRoles() throws Exception
    {
        SpringLDAPConnector slc = createSpringLDAPConnector();

        // testing legacy behaviour
        EntityQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.role()).returningAtMost(2);

        List<String> groupNames = slc.searchGroups(query);

        assertThat(groupNames, emptyCollectionOf(String.class));
    }

    @Test
    public void searchGroupsAsStrings() throws Exception
    {
        SpringLDAPConnector slc = createSpringLDAPConnector();

        EntityQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group()).returningAtMost(2);

        when(slc.ldapQueryTranslater.asLDAPFilter(query, slc.ldapPropertiesMapper))
            .thenReturn(new LDAPQuery("filter"));

        when(slc.ldapTemplate.searchWithLimitedResults(eq(BASE_DN), eq("filter"), any(SearchControls.class),
                                     eq(NamedLdapEntity.mapperFromAttribute("cn")),
                                     any(DirContextProcessor.class), eq(2)
                                     ))
            .thenReturn(ImmutableList.of(new NamedLdapEntity(new LdapName("cn=g1"), "group1"),
                                         new NamedLdapEntity(new LdapName("cn=g2"), "group2")));

        // invoke method under test
        List<String> groupNames = slc.searchGroups(query);

        assertThat(groupNames, containsInAnyOrder("group1", "group2"));
    }

    @Test
    public void searchGroupsAsGroupEntitiesWhenEntityTypeIsGroup() throws Exception
    {
        SpringLDAPConnector slc = createSpringLDAPConnector();

        EntityQuery<GroupWithAttributes> query =
            QueryBuilder.queryFor(GroupWithAttributes.class, EntityDescriptor.group()).returningAtMost(2);

        when(slc.ldapQueryTranslater.asLDAPFilter(query, slc.ldapPropertiesMapper))
            .thenReturn(new LDAPQuery("filter"));


        new GroupContextMapper(DIRECTORY_ID, GroupType.GROUP, slc.getLdapPropertiesMapper(), ImmutableList.<AttributeMapper>of()).getRequiredLdapAttributes();

        when(slc.ldapTemplate.searchWithLimitedResults(eq(BASE_DN), eq("filter"), any(SearchControls.class),
                                     any(GroupContextMapper.class),
                                     any(DirContextProcessor.class), eq(2)
                                     ))
            .thenReturn(ImmutableList.of(new GroupTemplateWithAttributes("group1", DIRECTORY_ID, GroupType.GROUP),
                                         new GroupTemplateWithAttributes("group2", DIRECTORY_ID, GroupType.GROUP)));

        // invoke method under test
        List<GroupWithAttributes> groupNames = slc.searchGroups(query);

        assertThat(groupNames,
                   IsIterableContainingInAnyOrder.<GroupWithAttributes>containsInAnyOrder
                       (new GroupTemplateWithAttributes("group1", DIRECTORY_ID, GroupType.GROUP),
                        new GroupTemplateWithAttributes("group2", DIRECTORY_ID, GroupType.GROUP)));
    }

    private static SpringLDAPConnector createSpringLDAPConnector() throws InvalidNameException
    {
        LDAPQueryTranslater ldapQueryTranslater = mock(LDAPQueryTranslater.class);
        EventPublisher eventPublisher = mock(EventPublisher.class);
        InstanceFactory instanceFactory = mock(InstanceFactory.class);
        return new SpringLDAPConnector(ldapQueryTranslater, eventPublisher, instanceFactory)
        {
            {
                this.setDirectoryId(DIRECTORY_ID);
                this.ldapTemplate = mock(LdapTemplateWithClassLoaderWrapper.class);
                this.ldapPropertiesMapper = mock(LDAPPropertiesMapper.class);
                this.searchDN = mock(SearchDN.class);

                when(ldapPropertiesMapper.getGroupNameAttribute()).thenReturn("cn");
                when(ldapPropertiesMapper.getGroupDescriptionAttribute()).thenReturn("description");

                when(searchDN.getGroup()).thenReturn(BASE_DN);
            }

            @Override
            protected <T> Iterable<T> searchGroupRelationshipsWithGroupTypeSpecified(MembershipQuery<T> query)
                throws OperationFailedException
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            protected Object encodePassword(PasswordCredential passwordCredential) throws InvalidCredentialException
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public String getDescriptiveName()
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public boolean isUserDirectGroupMember(String username, String groupName) throws OperationFailedException
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public boolean isGroupDirectGroupMember(String childGroup, String parentGroup)
                throws OperationFailedException
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public void addUserToGroup(String username, String groupName)
                throws GroupNotFoundException, UserNotFoundException, ReadOnlyGroupException, OperationFailedException
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public void addGroupToGroup(String childGroup, String parentGroup)
                throws GroupNotFoundException, InvalidMembershipException, ReadOnlyGroupException,
                       OperationFailedException
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public void removeUserFromGroup(String username, String groupName)
                throws GroupNotFoundException, UserNotFoundException, MembershipNotFoundException,
                       ReadOnlyGroupException,
                       OperationFailedException
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public void removeGroupFromGroup(String childGroup, String parentGroup)
                throws GroupNotFoundException, InvalidMembershipException, MembershipNotFoundException,
                       ReadOnlyGroupException,
                       OperationFailedException
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public Iterable<Membership> getMemberships() throws OperationFailedException
            {
                throw new UnsupportedOperationException("Not implemented");
            }
        };
    }

    @Test
    public void copyOfSearchControlsCopiesAllFields()
    {
        String[] attrs = {"attr"};

        SearchControls orig = new SearchControls(SearchControls.SUBTREE_SCOPE, 2, 3, attrs, true, true);

        SearchControls copy = SpringLDAPConnector.copyOf(orig);

        assertEquals(
                ToStringBuilder.reflectionToString(orig, ToStringStyle.SHORT_PREFIX_STYLE),
                ToStringBuilder.reflectionToString(copy, ToStringStyle.SHORT_PREFIX_STYLE));
    }

    @Test
    public void searchEntitiesWithRequestControlsConstrainsSearchResults() throws OperationFailedException
    {
        SpringLDAPConnector slc = mock(SpringLDAPConnector.class);

        LdapTemplateWithClassLoaderWrapper template = mock(LdapTemplateWithClassLoaderWrapper.class);
        slc.ldapTemplate = template;
        slc.ldapPropertiesMapper = mock(LDAPPropertiesMapper.class);

        when(slc.ldapPropertiesMapper.getSearchTimeLimit()).thenReturn(1000);

        SearchControls sc = new SearchControls();
        when(slc.searchEntitiesWithRequestControls(null, null, null, sc, null, 10, 5)).thenCallRealMethod();

        /* Perform a search */
        slc.searchEntitiesWithRequestControls(null, null, null, sc, null, 10, 5);

        ArgumentCaptor<SearchControls> captor = ArgumentCaptor.forClass(SearchControls.class);

        verify(template).searchWithLimitedResults(Mockito.<Name>anyObject(), Mockito.anyString(), captor.capture(),
                Mockito.<ContextMapper>anyObject(), Mockito.<DirContextProcessor>any(), Mockito.eq(15));

        assertEquals("Limit is set on SearchControls for underlying query", 15, captor.getValue().getCountLimit());
        assertEquals("Timeout is set on underlying query", 1000, captor.getValue().getTimeLimit());

        assertEquals("Provided SearchControls is not modified", 0, sc.getCountLimit());
    }

    @Test
    public void generalLdapConnectorDoesNotSupportInactiveAccounts() throws Exception
    {
        SpringLDAPConnector directory = createSpringLDAPConnector();
        assertFalse(directory.supportsInactiveAccounts());  // not supported in general. Subclasses may add support
    }

    @Test
    public void authenticationRejectsEncryptedPassword() throws Exception
    {
        LDAPUserWithAttributes user = mock(LDAPUserWithAttributes.class);

        SpringLDAPConnector directory = mock(SpringLDAPConnector.class);

        directory.ldapPropertiesMapper = mock(LDAPPropertiesMapper.class);
        when(directory.findUserByName("name")).thenReturn(user);
        when(directory.authenticate(Mockito.anyString(), Mockito.any(PasswordCredential.class))).thenCallRealMethod();

        expectedException.expect(InvalidAuthenticationException.class);
        expectedException.expectMessage(Matchers.is("You cannot authenticate with an encrypted PasswordCredential"));
        directory.authenticate("name", new PasswordCredential("hash", true));
    }
}
