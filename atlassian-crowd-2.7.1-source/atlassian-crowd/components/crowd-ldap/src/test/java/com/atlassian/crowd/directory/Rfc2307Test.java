package com.atlassian.crowd.directory;

import java.util.Collections;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapperImpl;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.PasswordEncoderNotFoundException;
import com.atlassian.crowd.password.encoder.PasswordEncoder;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.util.InstanceFactory;

import com.google.common.collect.ImmutableMap;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Rfc2307Test
{
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void encryptedCredentialShouldNotBeEncryptedAgain()
    {
        PasswordEncoder dummyPasswordEncoder = mock(PasswordEncoder.class);
        when(dummyPasswordEncoder.encodePassword("pass", null)).thenReturn("encoded");

        PasswordEncoderFactory pef = mock(PasswordEncoderFactory.class);
        when(pef.getLdapEncoder("dummy")).thenReturn(dummyPasswordEncoder);

        InstanceFactory instanceFactory = mock(InstanceFactory.class);
        when(instanceFactory.getInstance(LDAPPropertiesMapperImpl.class)).thenReturn(new LDAPPropertiesMapperImpl(null));

        Rfc2307 ds = new Impl(instanceFactory, pef);
        ds.setAttributes(ImmutableMap.of("ldap.user.encryption", "dummy"));

        assertEquals("pass", ds.encodePassword(PasswordCredential.encrypted("pass")));
    }

    @Test
    public void passwordCredentialNoneShouldNotBeTreatedDifferentlyToOtherEncryptedCredential()
    {
        PasswordEncoder dummyPasswordEncoder = mock(PasswordEncoder.class);
        when(dummyPasswordEncoder.encodePassword("pass", null)).thenReturn("encoded");

        PasswordEncoderFactory pef = mock(PasswordEncoderFactory.class);
        when(pef.getLdapEncoder("dummy")).thenReturn(dummyPasswordEncoder);

        InstanceFactory instanceFactory = mock(InstanceFactory.class);
        when(instanceFactory.getInstance(LDAPPropertiesMapperImpl.class)).thenReturn(new LDAPPropertiesMapperImpl(null));

        Rfc2307 ds = new Impl(instanceFactory, pef);
        ds.setAttributes(ImmutableMap.of("ldap.user.encryption", "dummy"));

        assertEquals("PasswordCredential.NONE should have its value preserved (i.e. not re-encrypted)",
                PasswordCredential.NONE.getCredential(), ds.encodePassword(PasswordCredential.NONE));
    }

    @Test
    public void passwordsArePlaintextIfEncryptionAlgorithmIsUnspecified()
    {
        InstanceFactory instanceFactory = mock(InstanceFactory.class);
        when(instanceFactory.getInstance(LDAPPropertiesMapperImpl.class)).thenReturn(new LDAPPropertiesMapperImpl(null));

        Rfc2307 ds = new Impl(instanceFactory, null);
        ds.setAttributes(Collections.<String, String>emptyMap());

        assertEquals("plain", ds.encodePassword(PasswordCredential.unencrypted("plain")));
    }

    @Test
    public void throwsExceptionIfPasswordEncoderCannotBeFound()
    {
        // Rfc2307 should just be propagating the exception from the password encoder factory, but test for it because
        // it's part of its java-doc'd contract and it would be quite bad if it (e.g.) caught the exception and fell
        // back to plain text encoding.

        PasswordEncoderFactory pef = mock(PasswordEncoderFactory.class);
        when(pef.getLdapEncoder("non-existent-encoder"))
                .thenThrow(new PasswordEncoderNotFoundException("non-existent-encoder does not exist"));

        InstanceFactory instanceFactory = mock(InstanceFactory.class);
        when(instanceFactory.getInstance(LDAPPropertiesMapperImpl.class)).thenReturn(new LDAPPropertiesMapperImpl(null));

        Rfc2307 ds = new Impl(instanceFactory, pef);
        ds.setAttributes(ImmutableMap.of("ldap.user.encryption", "non-existent-encoder"));

        exception.expect(PasswordEncoderNotFoundException.class);
        ds.encodePassword(PasswordCredential.unencrypted("pass"));
    }

    @Test
    public void passwordsUseSpecifiedEncoding()
    {
        PasswordEncoder dummyPasswordEncoder = mock(PasswordEncoder.class);
        when(dummyPasswordEncoder.encodePassword("pass", null)).thenReturn("encoded");

        PasswordEncoderFactory pef = mock(PasswordEncoderFactory.class);
        when(pef.getLdapEncoder("dummy")).thenReturn(dummyPasswordEncoder);

        InstanceFactory instanceFactory = mock(InstanceFactory.class);
        when(instanceFactory.getInstance(LDAPPropertiesMapperImpl.class)).thenReturn(new LDAPPropertiesMapperImpl(null));

        Rfc2307 ds = new Impl(instanceFactory, pef);
        ds.setAttributes(ImmutableMap.of("ldap.user.encryption", "dummy"));

        assertEquals("encoded", ds.encodePassword(PasswordCredential.unencrypted("pass")));
    }

    private static class Impl extends Rfc2307
    {
        public Impl(InstanceFactory instanceFactory, PasswordEncoderFactory passwordEncoderFactory)
        {
            super(null, null, instanceFactory, passwordEncoderFactory);
        }

        @Override
        public String getDescriptiveName()
        {
            throw new UnsupportedOperationException();
        }
    }
}
