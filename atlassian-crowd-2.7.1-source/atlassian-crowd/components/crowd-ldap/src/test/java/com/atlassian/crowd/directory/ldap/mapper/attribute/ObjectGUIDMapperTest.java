package com.atlassian.crowd.directory.ldap.mapper.attribute;

import javax.naming.Name;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;

import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Test;
import org.springframework.ldap.core.DirContextAdapter;

import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ObjectGUIDMapperTest
{
    private static final String OBJECT_GUID_ATTRIBUTE = "objectGUID";
    private static final byte[] ARBITRARY_VALUE1 = new byte[]{(byte) 0xBA};
    private static final byte[] ARBITRARY_VALUE2 = new byte[]{(byte) 0xCA};

    private AttributeMapper ogm = new ObjectGUIDMapper();

    @Test
    public void shouldReturnEmptySetWhenAttributeIsMissing() throws Exception
    {
        Attributes attrs = mock(Attributes.class);
        Name dn = mock(Name.class);

        assertThat(ogm.getValues(new DirContextAdapter(attrs, dn)), IsEmptyCollection.empty());
    }

    @Test
    public void shouldReturnEmptySetWhenAttributeIsMultivalued() throws Exception
    {
        Attribute value = new BasicAttribute(null, ARBITRARY_VALUE1);
        value.add(ARBITRARY_VALUE2);

        Attributes attrs = mock(Attributes.class);
        Name dn = mock(Name.class);

        assertThat(ogm.getValues(new DirContextAdapter(attrs, dn)), IsEmptyCollection.empty());
    }

    @Test
    public void shouldSkipValueIfItIsNotBinary() throws Exception
    {
        Attribute value = new BasicAttribute(null, "not-a-binary-value");

        Attributes attrs = mock(Attributes.class);
        when(attrs.get(OBJECT_GUID_ATTRIBUTE)).thenReturn(value);

        Name dn = mock(Name.class);

        assertThat(ogm.getValues(new DirContextAdapter(attrs, dn)), IsEmptyCollection.empty());
    }

    @Test
    public void shouldEncodeByteArrayAsHexString() throws Exception
    {
        Attribute value = new BasicAttribute(null, ARBITRARY_VALUE1);

        Attributes attrs = mock(Attributes.class);
        when(attrs.get(OBJECT_GUID_ATTRIBUTE)).thenReturn(value);

        Name dn = mock(Name.class);

        assertThat(ogm.getValues(new DirContextAdapter(attrs, dn)), contains("ba"));
    }
}
