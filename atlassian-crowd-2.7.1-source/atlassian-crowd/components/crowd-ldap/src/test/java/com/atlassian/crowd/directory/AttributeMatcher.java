package com.atlassian.crowd.directory;

import java.util.Collections;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.ldap.LdapName;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.hamcrest.TypeSafeMatcher;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.hamcrest.Matchers.equalTo;

/**
 * A matcher for {@link Attribute}.
 *
 */
class AttributeMatcher<T> extends TypeSafeMatcher<Attribute>
{
    private final Matcher<String> idMatcher;
    private final Matcher<Iterable<? extends T>> valuesMatcher;

    private AttributeMatcher(Matcher<String> idMatcher, Matcher<Iterable<? extends T>> valuesMatcher)
    {
        this.idMatcher = checkNotNull(idMatcher);
        this.valuesMatcher = checkNotNull(valuesMatcher);
    }

    @Factory
    public static <T> AttributeMatcher<T> attribute(Matcher<String> idMatcher, Matcher<Iterable<? extends T>> valuesMatcher)
    {
        return new AttributeMatcher<T>(idMatcher, valuesMatcher);
    }

    @Factory
    public static <T> AttributeMatcher<T> singleValuedAttribute(Matcher<String> idMatcher, Matcher<? super T> valueMatcher)
    {
        return attribute(idMatcher, Matchers.<T>contains(valueMatcher));
    }

    @Factory
    public static AttributeMatcher<String> singleValuedStringAttributeWithNameOf(String id, String value)
    {
        return singleValuedAttribute(equalTo(id), equalTo(value));
    }

    @Factory
    public static AttributeMatcher<String> singleValuedStringAttributeWithNameOf(String id, LdapName value)
    {
        return singleValuedAttribute(equalTo(id), equalTo(value.toString()));
    }

    @Override
    protected boolean matchesSafely(Attribute item)
    {
        try
        {
            return idMatcher.matches(item.getID())
                    && valuesMatcher.matches(Collections.list(item.getAll()));
        }
        catch (NamingException e)
        {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("a basic attribute with ID ").appendDescriptionOf(idMatcher)
            .appendText(" and values ").appendDescriptionOf(valuesMatcher);
    }
}
