package com.atlassian.crowd.directory.ldap.cache;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.Membership;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AbstractCacheRefresherTest
{
    @Mock
    private RemoteDirectory remoteDirectory;

    @Mock
    private DirectoryCache directoryCache;

    @Test
    public void testFilterOutDuplicateGroupsNoDuplicates()
    {
        List<Group> group = groups("something", "else");
        assertSame(group, AbstractCacheRefresher.filterOutDuplicateGroups(group));
    }

    @Test
    public void testFilterOutDuplicateGroupsWithDuplicates()
    {
        Group group1 = group("something");
        Group group2 = group("duplicate");
        Group group3 = group("DUPLICATE");
        Group group4 = group("else");
        Group group5 = group("DUPLICate");

        final List<Group> inputGroups = asList(group1, group2, group3, group4, group5);
        final List<Group> expectedOutputGroups = asList(group1, group4);
        assertEquals(expectedOutputGroups, AbstractCacheRefresher.filterOutDuplicateGroups(inputGroups));
    }

    @Test(expected = OperationFailedException.class)
    public void testSynchroniseMembershipsSameGroupDifferentCaseThrowsException() throws OperationFailedException
    {
        List<Group> groups = groups("one", "two", "ONE");

        AbstractCacheRefresher refresher = new MockCacheRefresher(remoteDirectory);
        refresher.synchroniseMemberships(groups, directoryCache);
    }

    @Test
    public void testSynchroniseMembershipsGroupAndMembershipHaveDifferentCase() throws OperationFailedException
    {
        Group one = group("one");

        when(remoteDirectory.getMemberships()).thenReturn(MockMembership.memberships("ONE"));

        AbstractCacheRefresher refresher = new MockCacheRefresher(remoteDirectory);
        refresher.synchroniseMemberships(ImmutableList.of(one), directoryCache);

        verify(directoryCache).syncUserMembersForGroup(one, Collections.<String>emptySet());
    }

    private static List<Group> groups(String...strings)
    {
        List<Group> groups = Lists.newArrayListWithExpectedSize(strings.length);
        for (String name : strings)
        {
            groups.add(group(name));
        }
        return groups;
    }

    private static Group group(String name)
    {
        return new GroupTemplate(name);
    }

    private static class MockCacheRefresher extends AbstractCacheRefresher
    {
        public MockCacheRefresher(final RemoteDirectory remoteDirectory)
        {
            super(remoteDirectory);
        }

        @Override
        protected void synchroniseAllUsers(DirectoryCache directoryCache) throws OperationFailedException
        {
            throw new RuntimeException("Not implemented");
        }

        @Override
        protected List<? extends Group> synchroniseAllGroups(DirectoryCache directoryCache) throws OperationFailedException
        {
            throw new RuntimeException("Not implemented");
        }

        @Override
        public boolean synchroniseChanges(DirectoryCache directoryCache) throws OperationFailedException
        {
            throw new RuntimeException("Not implemented");
        }
    }

    private static class MockMembership implements Membership
    {
        private final String groupName;

        private MockMembership(String groupName)
        {
            this.groupName = groupName;
        }

        @Override
        public String getGroupName()
        {
            return groupName;
        }

        @Override
        public Set<String> getUserNames()
        {
            return Collections.emptySet();
        }

        @Override
        public Set<String> getChildGroupNames()
        {
            return Collections.emptySet();
        }

        public static Iterable<Membership> memberships(String...groups)
        {
            List<Membership> result = Lists.newArrayListWithExpectedSize(groups.length);
            for (String group : groups)
            {
                result.add(new MockMembership(group));
            }
            return result;
        }
    }
}
