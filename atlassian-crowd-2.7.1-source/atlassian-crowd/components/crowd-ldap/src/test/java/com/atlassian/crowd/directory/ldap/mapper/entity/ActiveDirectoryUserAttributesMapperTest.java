package com.atlassian.crowd.directory.ldap.mapper.entity;

import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ActiveDirectoryUserAttributesMapperTest
{
    @Test
    public void userShouldBeActive() throws Exception
    {
        ActiveDirectoryUserAttributesMapper mapper = new ActiveDirectoryUserAttributesMapper(1L, null);

        Attributes attributes = new BasicAttributes();
        attributes.put("userAccountControl", "512");
        assertTrue("User should be active", mapper.getUserActiveFromAttribute(attributes));
    }

    @Test
    public void userShouldBeInactive() throws Exception
    {
        ActiveDirectoryUserAttributesMapper mapper = new ActiveDirectoryUserAttributesMapper(1L, null);

        Attributes attributes = new BasicAttributes();
        attributes.put("userAccountControl", "514");
        assertFalse("User should be inactive", mapper.getUserActiveFromAttribute(attributes));
    }

    @Test
    public void userShouldBeActiveByDefaultIfAttributeIsNotPresent() throws Exception
    {
        ActiveDirectoryUserAttributesMapper mapper = new ActiveDirectoryUserAttributesMapper(1L, null);

        Attributes attributes = new BasicAttributes();
        assertTrue("User should be active by default", mapper.getUserActiveFromAttribute(attributes));
    }
}
