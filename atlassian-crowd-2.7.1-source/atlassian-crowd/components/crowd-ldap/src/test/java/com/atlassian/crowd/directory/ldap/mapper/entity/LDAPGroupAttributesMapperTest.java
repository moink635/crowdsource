package com.atlassian.crowd.directory.ldap.mapper.entity;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttributes;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplateWithAttributes;
import com.atlassian.crowd.model.group.GroupType;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.ldap.UncategorizedLdapException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


public class LDAPGroupAttributesMapperTest
{
    private LDAPPropertiesMapper mockLdapPropertiesMapper;

    private LDAPGroupAttributesMapper groupMapper;

    private static final long DIRECTORY_ID = 1;

    @Before
    public void setUp()
    {
        // mock up properties ldappropertiesMapper
        mockLdapPropertiesMapper = mock(LDAPPropertiesMapper.class);

        when(mockLdapPropertiesMapper.getObjectClassAttribute()).thenReturn("objectClass");

        when(mockLdapPropertiesMapper.getGroupDescriptionAttribute()).thenReturn(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY);
        when(mockLdapPropertiesMapper.getGroupFilter()).thenReturn(LDAPPropertiesMapper.GROUP_OBJECTFILTER_KEY);
        when(mockLdapPropertiesMapper.getGroupMemberAttribute()).thenReturn(LDAPPropertiesMapper.GROUP_USERNAMES_KEY);
        when(mockLdapPropertiesMapper.getGroupNameAttribute()).thenReturn(LDAPPropertiesMapper.GROUP_NAME_KEY);
        when(mockLdapPropertiesMapper.getGroupObjectClass()).thenReturn(LDAPPropertiesMapper.GROUP_OBJECTCLASS_KEY);

        groupMapper = new LDAPGroupAttributesMapper(DIRECTORY_ID, GroupType.GROUP, mockLdapPropertiesMapper);
    }

    @After
    public void tearDown()
    {
        mockLdapPropertiesMapper = null;
        groupMapper = null;
    }

    private String getAttribute(String attributeName, Attributes directoryAttributes) throws NamingException
    {
        Attribute attribute = directoryAttributes.get(attributeName);
        if (attribute == null)
        {
            throw new UncategorizedLdapException(attributeName + " was not found in results");
        }
        try
        {
            return (String) attribute.get(0);
        }
        catch (javax.naming.NamingException e)
        {
            throw new UncategorizedLdapException(e);
        }
    }

    // TEST GROUP STUFF
    @Test(expected = UncategorizedLdapException.class)
    public void testMapAttributesFromGroupNullGroup()
    {
        groupMapper.mapAttributesFromGroup(null);
    }

    @Test
    public void testMapAttributesFromGroupWithDescription() throws NamingException
    {
        // create group
        GroupTemplateWithAttributes group = new GroupTemplateWithAttributes("Chocolate", DIRECTORY_ID, GroupType.GROUP);
        group.setDescription("Chocoholics group");

        Attributes attributes = groupMapper.mapAttributesFromGroup(group);

        assertNotNull(attributes);
        assertEquals("Name should be same", group.getName(), getAttribute(LDAPPropertiesMapper.GROUP_NAME_KEY, attributes));
        assertEquals("Description should be same", group.getDescription(), getAttribute(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY, attributes));
        AttributesMapperTestUtils.assertAttributesValuesNeverEmpty(attributes);
    }

    @Test
    public void testMapAttributesFromGroupWithEmptyDescription() throws NamingException
    {
        // create group
        GroupTemplateWithAttributes group = new GroupTemplateWithAttributes("Chocolate", DIRECTORY_ID, GroupType.GROUP);
        group.setDescription("");

        Attributes attributes = groupMapper.mapAttributesFromGroup(group);

        assertNotNull(attributes);
        assertNull(attributes.get(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY));
        AttributesMapperTestUtils.assertAttributesValuesNeverEmpty(attributes);
    }

    @Test
    public void testMapAttributesFromGroupNoDescription() throws NamingException
    {
        // create group
        GroupTemplateWithAttributes group = new GroupTemplateWithAttributes("Chocolate", DIRECTORY_ID, GroupType.GROUP);

        Attributes attributes = groupMapper.mapAttributesFromGroup(group);

        assertNotNull(attributes);
        assertEquals("Name should be same", group.getName(), getAttribute(LDAPPropertiesMapper.GROUP_NAME_KEY, attributes));
        assertNull(attributes.get(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY));
        AttributesMapperTestUtils.assertAttributesValuesNeverEmpty(attributes);
    }

    @Test(expected = UncategorizedLdapException.class)
    public void testMapGroupFromAttributesNullAttributes()
    {
        groupMapper.mapGroupFromAttributes(null);
    }

    @Test
    public void testMapGroupFromAttributesWithDescription() throws NamingException
    {
        Attributes attributes = new BasicAttributes();
        attributes.put(LDAPPropertiesMapper.GROUP_NAME_KEY, "Chocolate");
        attributes.put(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY, "Chocoholics Anonymous");

        Group group = groupMapper.mapGroupFromAttributes(attributes);

        assertNotNull(group);
        assertEquals("Name should be same", getAttribute(LDAPPropertiesMapper.GROUP_NAME_KEY, attributes), group.getName());
        assertEquals("Description should be same", getAttribute(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY, attributes), group.getDescription());
        assertTrue(group.isActive()); // LDAP directories are active
        assertEquals(GroupType.GROUP, group.getType());
    }

    @Test
    public void testMapGroupFromAttributesNoDescription() throws NamingException
    {
        Attributes attributes = new BasicAttributes();
        attributes.put(LDAPPropertiesMapper.GROUP_NAME_KEY, "Chocolate");

        Group group = groupMapper.mapGroupFromAttributes(attributes);

        assertNotNull(group);
        assertEquals("Name should be same", getAttribute(LDAPPropertiesMapper.GROUP_NAME_KEY, attributes), group.getName());
        assertNull(group.getDescription());
        assertTrue(group.isActive()); // LDAP directories are active
        assertEquals(GroupType.GROUP, group.getType());
    }

    @Test(expected = UncategorizedLdapException.class)
    public void testMapGroupFromAttributesNoName() throws NamingException
    {
        Attributes attributes = new BasicAttributes();
        attributes.put(LDAPPropertiesMapper.GROUP_DESCRIPTION_KEY, "Chocoholics Anonymous");

        groupMapper.mapGroupFromAttributes(attributes);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreationWithRoleFails()
    {
        new LDAPGroupAttributesMapper(0, GroupType.LEGACY_ROLE, mockLdapPropertiesMapper);
    }
}
