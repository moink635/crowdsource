package com.atlassian.crowd.directory.ldap.util;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;

import org.junit.Test;
import org.springframework.ldap.core.DistinguishedName;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DNStandardiserTest
{
    @Test
    public void relaxedStandardisationLeavesDnsWithSpacesUnaffected()
    {
        assertEquals("ou=partners, dc=example, dc=test",
                DNStandardiser.standardise("ou=partners, dc=example, dc=test", false));
    }

    @Test
    public void relaxedStandardisationLeavesDnsWithoutSpaces()
    {
        assertEquals("ou=partners,dc=example,dc=test",
                DNStandardiser.standardise("ou=partners,dc=example,dc=test", false));
    }

    @Test
    public void strictStandardisationRemovesSpaces()
    {
        assertEquals("ou=partners,dc=example,dc=test",
                DNStandardiser.standardise("ou=partners, dc=example, dc=test", true));
    }

    @Test
    public void strictStandardisationLeavesDnsWithoutSpaces()
    {
        assertEquals("ou=partners,dc=example,dc=test",
                DNStandardiser.standardise("ou=partners,dc=example,dc=test", true));
    }

    @Test
    public void strictStandardisationConvertsValuesToLowerCase()
    {
        assertEquals("dc=example,dc=test",
                DNStandardiser.standardise("dc=Example,dc=Test", true));
    }

    @Test
    public void strictStandardisationConvertsDistinguishedNameInstancesToLowerCase()
    {
        DistinguishedName dn = new DistinguishedName("dc=Example,dc=Test");

        assertEquals("dc=example,dc=test",
                DNStandardiser.standardise(dn, true));
    }

    @Test
    public void relaxedStandardisationConvertsDistinguishedNameInstancesToLowerCase()
    {
        DistinguishedName dn = new DistinguishedName("dc=Example,dc=Test");

        assertEquals("dc=example,dc=test",
                DNStandardiser.standardise(dn, false));
    }

    @Test
    public void relaxedStandardisationOfDistinguishedNameInstancesRemovesSpaces()
    {
        DistinguishedName dn = new DistinguishedName("dc = Example, dc = Test");

        assertEquals("dc=example,dc=test",
                DNStandardiser.standardise(dn, false));
    }

    /**
     * Test related behaviour of {@link DistinguishedName}.
     */
    @Test
    public void distinguishedNamesEqualCaseInsensitively()
    {
        DistinguishedName dnLower = new DistinguishedName("dc=example,dc=com");
        DistinguishedName dnUpper = new DistinguishedName("DC=Example,DC=Com");

        assertTrue(dnLower.equals(dnUpper));
    }

    /**
     * Test related behaviour of {@link LdapName}.
     */
    @Test
    public void ldapNamesEqualCaseInsensitively() throws InvalidNameException
    {
        LdapName dnLower = new LdapName("dc=example,dc=com");
        LdapName dnUpper = new LdapName("DC=Example,DC=Com");

        assertTrue(dnLower.equals(dnUpper));
    }
}
