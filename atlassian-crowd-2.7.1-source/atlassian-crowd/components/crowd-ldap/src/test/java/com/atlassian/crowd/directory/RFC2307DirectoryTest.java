package com.atlassian.crowd.directory;

import java.util.Collections;
import java.util.List;

import javax.naming.Name;
import javax.naming.directory.SearchControls;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapperImpl;
import com.atlassian.crowd.directory.ldap.LdapTemplateWithClassLoaderWrapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.AttributeMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.RFC2307GidNumberMapper;
import com.atlassian.crowd.directory.ldap.name.GenericConverter;
import com.atlassian.crowd.directory.ldap.name.SearchDN;
import com.atlassian.crowd.model.user.LDAPUserWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.ldap.LDAPQuery;
import com.atlassian.crowd.search.ldap.LDAPQueryTranslater;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.event.api.EventPublisher;

import com.google.common.collect.ImmutableList;

import org.hamcrest.collection.IsIterableWithSize;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextProcessor;

import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RFC2307DirectoryTest
{
    @Mock
    private LDAPQueryTranslater ldapQueryTranslater;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private InstanceFactory instanceFactory;
    @Mock
    private PasswordEncoderFactory passwordEncoderFactory;

    @Test
    public void customAttributeMapperListsAreConstructedWithExpectedCustomMappers()
    {
        LDAPPropertiesMapper ldapPropertiesMapper = mock(LDAPPropertiesMapper.class);

        when(instanceFactory.getInstance((Class) LDAPPropertiesMapperImpl.class)).thenReturn(ldapPropertiesMapper);
        RFC2307Directory d = new Rfc2307(ldapQueryTranslater, eventPublisher, instanceFactory, passwordEncoderFactory);

        d.setAttributes(Collections.<String, String>emptyMap());

        assertThat(d.getCustomGroupAttributeMappers(), IsIterableWithSize.<AttributeMapper>iterableWithSize(2));
        assertThat(d.getCustomUserAttributeMappers(), IsIterableWithSize.<AttributeMapper>iterableWithSize(1));
    }

    @Test
    public void queryingForGroupMembershipsOfUserDoesNotFail() throws Exception
    {
        final LDAPPropertiesMapper mockLdapPropertiesMapper = mock(LDAPPropertiesMapper.class);
        when(mockLdapPropertiesMapper.getGroupNameAttribute()).thenReturn("cn");
        when(mockLdapPropertiesMapper.getGroupMemberAttribute()).thenReturn("member");

        when(mockLdapPropertiesMapper.getUserNameAttribute()).thenReturn("dummy");
        when(mockLdapPropertiesMapper.getUserEmailAttribute()).thenReturn("dummy");
        when(mockLdapPropertiesMapper.getUserFirstNameAttribute()).thenReturn("dummy");
        when(mockLdapPropertiesMapper.getUserLastNameAttribute()).thenReturn("dummy");
        when(mockLdapPropertiesMapper.getUserDisplayNameAttribute()).thenReturn("dummy");
        when(mockLdapPropertiesMapper.getExternalIdAttribute()).thenReturn("dummy");

        /* SpringLDAPConnector isn't very testable */
        RFC2307Directory d = new Rfc2307(ldapQueryTranslater, eventPublisher, instanceFactory, passwordEncoderFactory) {
            {
                this.ldapTemplate = mock(LdapTemplateWithClassLoaderWrapper.class);
                this.ldapPropertiesMapper = mockLdapPropertiesMapper;
                this.searchDN = mock(SearchDN.class);
            }
        };

        /* A user to be found */
        LDAPUserWithAttributes user = mock(LDAPUserWithAttributes.class);
        when(user.getName()).thenReturn("user");
        when(user.getValue(RFC2307GidNumberMapper.ATTRIBUTE_KEY)).thenReturn("1");

        when(d.ldapTemplate.searchWithLimitedResults(Mockito.<Name>any(), Mockito.eq("expected query"),
                Mockito.<SearchControls>any(), Mockito.<ContextMapper>any(),
                Mockito.<DirContextProcessor>any(), Mockito.eq(1))).thenReturn(ImmutableList.of(user));

        /* The query that will be used to find them */
        EntityQuery<User> expectedQuery = QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.USERNAME).exactlyMatching("user")).returningAtMost(1);
        LDAPQuery value = new LDAPQuery("expected query");

        when(ldapQueryTranslater.asLDAPFilter(expectedQuery, mockLdapPropertiesMapper)).thenReturn(value);

        /* And groups for them to belong to */
        List<NamedLdapEntity> groupsToBeFound = ImmutableList.of(
                new NamedLdapEntity(GenericConverter.emptyLdapName(), "group-one"),
                new NamedLdapEntity(GenericConverter.emptyLdapName(), "group-two")
        );

        when(d.ldapTemplate.search(Mockito.<Name>any(), Mockito.eq("(&(|(member=user)(gidNumber=1)))"),
                Mockito.<SearchControls>any(), Mockito.<ContextMapper>any(),
                Mockito.<DirContextProcessor>any())).thenReturn(groupsToBeFound);

        MembershipQuery<String> query = QueryBuilder.createMembershipQuery(-1, 0, false, EntityDescriptor.group(), String.class, EntityDescriptor.user(), "user");

        assertEquals(0, query.getStartIndex());
        assertEquals(EntityQuery.ALL_RESULTS, query.getMaxResults());

        Iterable<String> results = d.searchGroupRelationshipsWithGroupTypeSpecified(query);

        assertThat(results, contains("group-one", "group-two"));

        verify(d.ldapTemplate).searchWithLimitedResults(Mockito.<Name>any(), Mockito.eq("expected query"),
                Mockito.<SearchControls>any(), Mockito.<ContextMapper>any(),
                Mockito.<DirContextProcessor>any(), Mockito.eq(1));
        verify(d.ldapTemplate).search(Mockito.<Name>any(), Mockito.eq("(&(|(member=user)(gidNumber=1)))"),
                Mockito.<SearchControls>any(), Mockito.<ContextMapper>any(), Mockito.<DirContextProcessor>any());
    }
}
