package com.atlassian.crowd.directory.ldap.cache;

import java.util.Collections;

import com.atlassian.crowd.directory.RemoteCrowdDirectory;
import com.atlassian.crowd.directory.SynchronisableDirectoryProperties;
import com.atlassian.crowd.event.Events;
import com.atlassian.crowd.event.IncrementalSynchronisationNotAvailableException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.event.OperationEvent;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EventTokenChangedCacheRefresherTest
{
    private EventTokenChangedCacheRefresher cacheRefresher;

    @Mock
    private RemoteCrowdDirectory remoteCrowdDirectory;

    @Before
    public void setUp() throws Exception
    {
        when(remoteCrowdDirectory.isRolesDisabled()).thenReturn(true);
        when(remoteCrowdDirectory.getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)).thenReturn("true");
        when(remoteCrowdDirectory.getMemberships()).thenReturn(
                Collections.<Membership>emptyList());

        cacheRefresher = new EventTokenChangedCacheRefresher(remoteCrowdDirectory);
    }

    @Test
    public void testSynchroniseAllSynchronisesWhenEventTokenNotAvailable() throws Exception
    {
        final DirectoryCache directoryCache = mock(DirectoryCache.class);

        when(remoteCrowdDirectory.getCurrentEventToken()).thenThrow(new OperationFailedException());

        cacheRefresher.synchroniseAll(directoryCache);

        verify(remoteCrowdDirectory).searchGroups(any(EntityQuery.class));
    }

    @Test
    public void testSynchroniseAllUpdatesEventToken() throws Exception
    {
        final DirectoryCache directoryCache = mock(DirectoryCache.class);

        when(remoteCrowdDirectory.getCurrentEventToken()).thenReturn("token");

        cacheRefresher.synchroniseAll(directoryCache);

        when(remoteCrowdDirectory.getNewEvents("token")).thenReturn(new Events(Collections.<OperationEvent>emptyList(), "token2"));

        assertTrue(cacheRefresher.synchroniseChanges(directoryCache));
    }

    @Test
    public void testSynchroniseChangesFailsWithNoEventToken() throws Exception
    {
        assertFalse(cacheRefresher.synchroniseChanges(null));
    }

    @Test
    public void failedIncrementalShouldCauseNextSyncToBeFull() throws Exception
    {
        final DirectoryCache directoryCache = mock(DirectoryCache.class);

        when(remoteCrowdDirectory.getCurrentEventToken()).thenReturn("token1", "token2");
        when(remoteCrowdDirectory.getNewEvents("token1"))
            .thenReturn(null); // this causes a NPE during incr sync
        when(remoteCrowdDirectory.getNewEvents("token2"))
            .thenReturn(new Events(ImmutableList.<OperationEvent>of(), "token3"));

        cacheRefresher.synchroniseAll(directoryCache);

        try
        {
            cacheRefresher.synchroniseChanges(directoryCache);
            fail("RuntimeException expected");
        }
        catch (RuntimeException e)
        {
            // ok
        }

        assertFalse("Cannot do incremental sync after a failed incremental sync",
                    cacheRefresher.synchroniseChanges(directoryCache));

        cacheRefresher.synchroniseAll(directoryCache);

        assertTrue("Subsequent incremental syncs should work", cacheRefresher.synchroniseChanges(directoryCache));
    }

    @Test
    public void fullSynchronisationDoesNotFetchTokenWhenIncrementalSyncIsNotEnabled() throws Exception
    {
        when(remoteCrowdDirectory.getCurrentEventToken()).thenThrow(new IncrementalSynchronisationNotAvailableException());
        when(remoteCrowdDirectory.getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)).thenReturn("false");

        final DirectoryCache directoryCache = mock(DirectoryCache.class);
        cacheRefresher.synchroniseAll(directoryCache);

        verify(remoteCrowdDirectory, never()).getCurrentEventToken();
    }

    @Test
    public void fullSynchronisationFetchesTokenWhenIncrementalSyncIsEnabled() throws Exception
    {
        when(remoteCrowdDirectory.getCurrentEventToken()).thenReturn("token");
        when(remoteCrowdDirectory.getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)).thenReturn("true");

        final DirectoryCache directoryCache = mock(DirectoryCache.class);
        cacheRefresher.synchroniseAll(directoryCache);

        verify(remoteCrowdDirectory, atLeastOnce()).getCurrentEventToken();
    }

    @Test
    public void synchroniseChangesReturnsWithoutCheckingEventsWhenIncrementalSyncIsDisabled() throws Exception
    {
        final DirectoryCache directoryCache = mock(DirectoryCache.class);

        when(remoteCrowdDirectory.getCurrentEventToken()).thenReturn("token");

        cacheRefresher.synchroniseAll(directoryCache);

        verify(remoteCrowdDirectory, atLeastOnce()).getCurrentEventToken();

        reset(remoteCrowdDirectory);

        when(remoteCrowdDirectory.getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)).thenReturn("false");

        assertFalse(cacheRefresher.synchroniseChanges(directoryCache));

        verify(remoteCrowdDirectory, never()).getCurrentEventToken();
        verify(remoteCrowdDirectory, never()).getNewEvents(anyString());
    }
}
