package com.atlassian.crowd.directory.ldap.name;

import java.util.Map;

import javax.naming.InvalidNameException;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapperImpl;

import com.google.common.collect.Maps;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class SearchDNTest
{
    private final static String ATTRIBUTE_NAME = "property";
    @Mock private Converter converter;

    @Test
    public void testGetSearchDNReturnsBasePlusAdditionalDN() throws InvalidNameException
    {
        search("ou=basedn,o=sgi", "ou=property,ou=group1");
        verify(converter).getName("ou=property,ou=group1,ou=basedn,o=sgi");
    }

    @Test
    public void testGetSearchDNSucceedsWithEmptyBaseDN() throws InvalidNameException
    {
        search("", "ou=property");
        verify(converter).getName("ou=property");
    }

    @Test
    public void testGetSearchDNSucceedsWithNullBaseDN() throws InvalidNameException
    {
        search(null, "ou=property");
        verify(converter).getName("ou=property");
    }

    @Test
    public void testGetSearchDNSucceedsWithEmptyProperty() throws InvalidNameException
    {
        search("ou=basedn", "");
        verify(converter).getName("ou=basedn");
    }

    private void search(String baseDN, String additionalDN) throws InvalidNameException
    {
        LDAPPropertiesMapper pm = new LDAPPropertiesMapperImpl(null);
        Map<String, String> attributes = Maps.newHashMap();
        if (baseDN != null)
        {
            attributes.put(LDAPPropertiesMapper.LDAP_BASEDN_KEY, baseDN);
        }
        if (additionalDN != null)
        {
            attributes.put(ATTRIBUTE_NAME, additionalDN);
        }

        pm.setAttributes(attributes);

        SearchDN searchDN = new SearchDN(pm, converter);
        searchDN.getSearchDN(ATTRIBUTE_NAME);
    }
}
