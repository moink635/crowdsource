package com.atlassian.crowd.directory.ldap;

import java.util.concurrent.atomic.AtomicBoolean;

import javax.naming.Name;
import javax.naming.NamingEnumeration;
import javax.naming.directory.DirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import com.atlassian.crowd.directory.LimitedNamingEnumeration;
import com.atlassian.crowd.directory.StubNamingEnumeration;

import com.google.common.collect.Iterators;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.NameClassPairCallbackHandler;
import org.springframework.ldap.core.SearchExecutor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LdapTemplateWithClassLoaderWrapperTest
{
    @Test
    public void invokeWithContextClassLoaderUsesClassClassLoader()
    {
        final ClassLoader ctxt = Thread.currentThread().getContextClassLoader();

        ClassLoader dummyClassLoader = new ClassLoader(ctxt)
        {
        };

        try
        {
            Thread.currentThread().setContextClassLoader(dummyClassLoader);

            final ClassLoader expectedClassLoaderDuringInvocation = LdapTemplateWithClassLoaderWrapper.class.getClassLoader();
            final AtomicBoolean wasRun = new AtomicBoolean(false);

            String s = LdapTemplateWithClassLoaderWrapper.invokeWithContextClassLoader(new LdapTemplateWithClassLoaderWrapper.CallableWithoutCheckedException<String>(){
                public String call()
                {
                    assertEquals(expectedClassLoaderDuringInvocation,
                            Thread.currentThread().getContextClassLoader());

                    wasRun.set(true);

                    return "result";
                }
            });

            assertEquals("result", s);
            assertEquals(dummyClassLoader, Thread.currentThread().getContextClassLoader());
            assertTrue(wasRun.get());
        }
        finally
        {
            Thread.currentThread().setContextClassLoader(ctxt);
        }
    }

    @Test
    public void searchWithLimitedResultsUsesALimitNamingEnumeration() throws Exception
    {
        LdapTemplate mockTemplate = mock(LdapTemplate.class);

        LdapTemplateWithClassLoaderWrapper template = new LdapTemplateWithClassLoaderWrapper(mockTemplate);

        ContextMapper contextMapper = mock(ContextMapper.class);

        DirContext context = mock(DirContext.class);

        NamingEnumeration<SearchResult> emptyNe = new StubNamingEnumeration<SearchResult>(Iterators.<SearchResult>emptyIterator());

        when(context.search(Mockito.<Name>any(), Mockito.anyString(), Mockito.<SearchControls>any()))
            .thenReturn(emptyNe);

        template.searchWithLimitedResults(null, null, null, contextMapper, null, 15);

        ArgumentCaptor<SearchExecutor> captor = ArgumentCaptor.forClass(SearchExecutor.class);

        verify(mockTemplate).search(captor.capture(), Mockito.<NameClassPairCallbackHandler>any(), Mockito.<DirContextProcessor>anyObject());

        NamingEnumeration ne = captor.getValue().executeSearch(context);

        assertThat("The SearchExecutor wraps results in a LimitNamingEnumeration", ne, Matchers.isA((Class) LimitedNamingEnumeration.class));
        assertEquals(15, ((LimitedNamingEnumeration) ne).getLimit());
    }
}
