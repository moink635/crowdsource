package com.atlassian.crowd.directory.ldap.mapper;

import javax.naming.Name;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;

import com.atlassian.crowd.directory.ldap.mapper.attribute.ObjectGUIDMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.USNChangedMapper;
import com.atlassian.crowd.model.Tombstone;

import com.google.common.collect.ImmutableSet;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.springframework.ldap.core.DirContextAdapter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

public class TombstoneContextMapperTest
{
    @Test
    public void expectedAttributesAreDeclared()
    {
        TombstoneContextMapper tcm = new TombstoneContextMapper();

        assertEquals(
                ImmutableSet.of(
                        ObjectGUIDMapper.ATTRIBUTE_KEY,
                        USNChangedMapper.ATTRIBUTE_KEY),
                tcm.getRequiredLdapAttributes());
    }

    @Test
    public void failsWithSpecificDescriptionInRuntimeExceptionWhenNoAttributesAreAvailable()
    {
        TombstoneContextMapper tcm = new TombstoneContextMapper();

        Attributes attrs = mock(Attributes.class);
        Name dn = mock(Name.class);

        try
        {
            tcm.mapFromContext(new DirContextAdapter(attrs, dn));
            fail();
        }
        catch (RuntimeException e)
        {
            assertThat(e.getMessage(), CoreMatchers.containsString("due to missing attribute for object"));
        }
    }

    @Test
    public void returnsAValidTombsoneWhenAttributesAreAvailable()
    {
        TombstoneContextMapper tcm = new TombstoneContextMapper();

        Attributes attrs = mock(Attributes.class);
        Name dn = mock(Name.class);

        BasicAttributes ba = new BasicAttributes();

        ba.put(new BasicAttribute(ObjectGUIDMapper.ATTRIBUTE_KEY, new byte[]{1, 2, 3, 4}));
        ba.put(new BasicAttribute(USNChangedMapper.ATTRIBUTE_KEY, "1"));

        Tombstone tombstone = tcm.mapFromContext(new DirContextAdapter(ba, dn));

        assertEquals(Long.valueOf(1), tombstone.getUsnChanged());
        assertEquals("01020304", tombstone.getObjectGUID());
    }
}
