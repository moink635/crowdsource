package com.atlassian.crowd.directory;

import javax.annotation.Nullable;
import javax.naming.InvalidNameException;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.ldap.LdapName;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.LdapTemplateWithClassLoaderWrapper;
import com.atlassian.crowd.directory.ldap.mapper.GroupContextMapper;
import com.atlassian.crowd.directory.ldap.mapper.UserContextMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.group.RFC4519MemberDnMapper;
import com.atlassian.crowd.directory.ldap.name.SearchDN;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplateWithAttributes;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.LDAPGroupWithAttributes;
import com.atlassian.crowd.model.user.LDAPUserWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.ldap.LDAPQuery;
import com.atlassian.crowd.search.ldap.LDAPQueryTranslater;
import com.atlassian.crowd.search.ldap.NullResultException;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.event.api.EventPublisher;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextProcessor;

import static com.atlassian.crowd.directory.AttributeMatcher.singleValuedStringAttributeWithNameOf;
import static com.atlassian.crowd.directory.ModificationItemMatcher.addAttribute;
import static com.atlassian.crowd.directory.ModificationItemMatcher.removeAttribute;
import static com.atlassian.crowd.directory.RFC4519Directory.DN_MAPPER;
import static com.atlassian.crowd.directory.RFC4519Directory.totalResultsSize;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class RFC4519DirectoryTest
{
    protected static final LdapName BASE_DN;
    protected static final LdapName GROUP_DN;
    protected static final LdapName CHILD_DN1;
    protected static final LdapName CHILD_DN2;

    static
    {
        try
        {
            BASE_DN = new LdapName("dc=test");
            GROUP_DN = new LdapName("cn=parent,dc=test");
            CHILD_DN1 = new LdapName("cn=child1,dc=test");
            CHILD_DN2 = new LdapName("cn=child2,dc=test");
        }
        catch (InvalidNameException e)
        {
            throw new RuntimeException(e);
        }
    }

    protected RFC4519Directory createDirectory() throws InvalidNameException, NullResultException
    {
        LDAPQueryTranslater ldapQueryTranslater = mock(LDAPQueryTranslater.class);
        EventPublisher eventPublisher = mock(EventPublisher.class);
        InstanceFactory instanceFactory = mock(InstanceFactory.class);

        return new RFC4519Directory(ldapQueryTranslater, eventPublisher, instanceFactory) {

            {
                this.ldapTemplate = mock(LdapTemplateWithClassLoaderWrapper.class);
                this.ldapPropertiesMapper = mock(LDAPPropertiesMapper.class);
                this.searchDN = mock(SearchDN.class);

                when(ldapPropertiesMapper.getUserNameAttribute()).thenReturn("cn");
                when(ldapPropertiesMapper.getUserEmailAttribute()).thenReturn("email");
                when(ldapPropertiesMapper.getUserDisplayNameAttribute()).thenReturn("dn");
                when(ldapPropertiesMapper.getUserFirstNameAttribute()).thenReturn("firstName");
                when(ldapPropertiesMapper.getUserLastNameAttribute()).thenReturn("lastName");
                when(ldapPropertiesMapper.getGroupNameAttribute()).thenReturn("cn");
                when(ldapPropertiesMapper.getGroupMemberAttribute()).thenReturn("member");
                when(ldapPropertiesMapper.getGroupDescriptionAttribute()).thenReturn("description");
                when(ldapPropertiesMapper.getUserGroupMembershipsAttribute()).thenReturn("memberOf");
                when(ldapPropertiesMapper.getUserFilter()).thenReturn("(user-filter)");
                when(ldapPropertiesMapper.getGroupFilter()).thenReturn("(group-filter)");
                when(ldapPropertiesMapper.getExternalIdAttribute()).thenReturn("entryUUID");

                when(this.searchDN.getUser()).thenReturn(BASE_DN);
                when(this.searchDN.getGroup()).thenReturn(BASE_DN);
                when(this.searchDN.getRole()).thenReturn(BASE_DN);  // legacy
            }

            @Override
            protected Object encodePassword(PasswordCredential passwordCredential) throws InvalidCredentialException
            {
                throw new UnsupportedOperationException("Not implemented");
            }

            @Override
            public String getDescriptiveName()
            {
                throw new UnsupportedOperationException("Not implemented");
            }
        };
    }

    protected LDAPUserWithAttributes createLDAPUserWithAttributes(LdapName dn)
    {
        return createLDAPUserWithAttributes(dn, null);
    }

    protected LDAPUserWithAttributes createLDAPUserWithAttributes(LdapName dn, @Nullable String name)
    {
        LDAPUserWithAttributes user = mock(LDAPUserWithAttributes.class);
        when(user.getDn()).thenReturn(dn.toString());
        when(user.getName()).thenReturn(name);
        return user;
    }

    protected LDAPGroupWithAttributes createLDAPGroupWithAttributes(LdapName dn)
    {
        return createLDAPGroupWithAttributes(dn, null);
    }

    protected LDAPGroupWithAttributes createLDAPGroupWithAttributes(LdapName dn, @Nullable String name)
    {
        LDAPGroupWithAttributes group = mock(LDAPGroupWithAttributes.class);
        when(group.getDn()).thenReturn(dn.toString());
        when(group.getName()).thenReturn(name);
        return group;
    }

    /**
     * Registers a user in the directory.
     *
     * @param user
     * @param directory
     */
    protected static void registerUserInDirectory(LDAPUserWithAttributes user, RFC4519Directory directory)
            throws NullResultException
    {
        registerUserInDirectory(user, directory, null);
    }

    /**
     * Register a user in the directory, assuming it is the only user that matches the provided LDAP filter.
     *
     * @param user
     * @param directory
     * @param filter
     */
    protected static void registerUserInDirectory(LDAPUserWithAttributes user, RFC4519Directory directory,
                                                  @Nullable String filter) throws NullResultException
    {
        if (filter != null)
        {
            // make the user discoverable when looking up using the provided filter
            when(directory.ldapTemplate.searchWithLimitedResults(
                    eq(BASE_DN), eq(filter), any(SearchControls.class),
                    any(UserContextMapper.class),
                    any(DirContextProcessor.class), anyInt()))
                    .thenReturn(ImmutableList.of(user));
        }

        @Nullable String name = user.getName();
        if (name != null)
        {
            // make the user discoverable when looking up by name
            String ldapQuery = "&((user-filter)(cn=" + name + "))";
            EntityQuery<User> queryByName = QueryBuilder.queryFor(User.class, EntityDescriptor.user())
                    .with(Restriction.on(UserTermKeys.USERNAME).exactlyMatching(name))
                    .returningAtMost(1);

            when(directory.ldapQueryTranslater.asLDAPFilter(queryByName, directory.ldapPropertiesMapper))
                    .thenReturn(new LDAPQuery(ldapQuery));

            when(directory.ldapTemplate.searchWithLimitedResults(
                    eq(BASE_DN), eq(ldapQuery), any(SearchControls.class),
                    any(UserContextMapper.class),
                    any(DirContextProcessor.class), anyInt()))
                    .thenReturn(ImmutableList.of(user));
        }
    }

    protected static void verifyUserWasSearchedByName(RFC4519Directory directory, String name)
    {
        verify(directory.ldapTemplate)
                .searchWithLimitedResults(eq(BASE_DN), eq("&((user-filter)(cn=" + name + "))"), any(SearchControls.class),
                        any(UserContextMapper.class), any(DirContextProcessor.class), eq(1));
    }

    /**
     * Registers a group in the directory.
     *
     * @param group
     * @param directory
     */
    protected static void registerGroupInDirectory(LDAPGroupWithAttributes group, RFC4519Directory directory)
            throws NullResultException, InvalidNameException
    {
        registerGroupInDirectory(group, directory, null);
    }

    /**
     * Registers a group in the directory, assuming it is the only group that matches the provided LDAP filter
     *
     * @param group
     * @param directory
     * @param filter
     */
    protected static void registerGroupInDirectory(LDAPGroupWithAttributes group, RFC4519Directory directory,
                                                   @Nullable String filter)
            throws NullResultException, InvalidNameException
    {
        if (filter != null)
        {
            // make the group discoverable when looking up using the provided filter
            when(directory.ldapTemplate.searchWithLimitedResults(
                    eq(BASE_DN), eq(filter), any(SearchControls.class),
                    any(GroupContextMapper.class),
                    any(DirContextProcessor.class), anyInt()))
                .thenReturn(ImmutableList.of(group));
        }

        @Nullable String name = group.getName();
        if (name != null)
        {
            // make the group discoverable when looking up by name
            LdapName dn = new LdapName(group.getDn());
            String ldapQuery = "&((group-filter)(cn=" + name + "))";
            EntityQuery<Group> queryByName = QueryBuilder.queryFor(Group.class, EntityDescriptor.group())
                .with(Restriction.on(GroupTermKeys.NAME).exactlyMatching(name))
                .returningAtMost(1);

            when(directory.ldapQueryTranslater.asLDAPFilter(queryByName, directory.ldapPropertiesMapper))
                    .thenReturn(new LDAPQuery(ldapQuery));

            when(directory.ldapTemplate.searchWithLimitedResults(
                    eq(BASE_DN), eq(ldapQuery), any(SearchControls.class),
                    any(GroupContextMapper.class),
                    any(DirContextProcessor.class), anyInt()))
                    .thenReturn(ImmutableList.of(group));

            when(directory.ldapTemplate.lookup(dn, NamedLdapEntity.mapperFromAttribute("cn")))
                    .thenReturn(new NamedLdapEntity(dn, name));

            when(directory.ldapTemplate.search(eq(dn), eq("(group-filter)"),
                    any(SearchControls.class), any(GroupContextMapper.class)))
                    .thenReturn(ImmutableList.of(group));
        }
    }

    protected static void verifyGroupWasSearchedByName(RFC4519Directory directory, String name)
    {
        verify(directory.ldapTemplate)
                .searchWithLimitedResults(eq(BASE_DN), eq("&((group-filter)(cn=" + name + "))"), any(SearchControls.class),
                        any(GroupContextMapper.class), any(DirContextProcessor.class), eq(1));
    }

    protected static void verifyGroupWasSearchedByDN(RFC4519Directory directory, LdapName dn)
    {
        verify(directory.ldapTemplate).search(eq(dn), eq("(group-filter)"),
                any(SearchControls.class), any(GroupContextMapper.class));
    }

    protected static void verifyGroupWasLookedUpByDN(RFC4519Directory directory, LdapName dn)
    {
        verify(directory.ldapTemplate).lookup(dn, NamedLdapEntity.mapperFromAttribute("cn"));
    }

    @Test
    public void totalResultsSizeAddsCountToInitialOffset()
    {
        assertEquals(0, totalResultsSize(0, 0));
        assertEquals(3, totalResultsSize(1, 2));
        assertEquals(Integer.MAX_VALUE, totalResultsSize(0, Integer.MAX_VALUE));
    }

    @Test
    public void totalResultsSizeIsAllResultsWhenRequestedCountIsAllResults()
    {
        assertEquals(EntityQuery.ALL_RESULTS, totalResultsSize(0, EntityQuery.ALL_RESULTS));
        assertEquals(EntityQuery.ALL_RESULTS, totalResultsSize(Integer.MAX_VALUE, EntityQuery.ALL_RESULTS));
    }

    @Test
    public void totalResultsSizeIsAllResultsWhenRequestedCountWouldOverflow()
    {
        assertEquals(EntityQuery.ALL_RESULTS, totalResultsSize(1, Integer.MAX_VALUE));
        assertEquals(EntityQuery.ALL_RESULTS, totalResultsSize(Integer.MAX_VALUE, 1));
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void isUserDirectGroupMember_ViaMemberDNs() throws Exception
    {
        RFC4519Directory directory = createDirectory();

        // user

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        registerUserInDirectory(user, directory);

        // group

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        when(group.getValues("memberDNs")).thenReturn(ImmutableSet.of(CHILD_DN1.toString()));
        registerGroupInDirectory(group, directory);

        assertTrue(directory.isUserDirectGroupMember("user", "group"));

        // verify queries

        verifyUserWasSearchedByName(directory, "user");
        verifyGroupWasSearchedByName(directory, "group");

        verify(directory.ldapPropertiesMapper, never()).isUsingUserMembershipAttribute(); // this setting is irrelevant
        verify(directory.ldapPropertiesMapper, never()).getUserGroupMembershipsAttribute(); // not used
        verify(user, never()).getValues("memberOf"); // memberOf is ignored

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     * memberOf: cn=parent,dc=test    # This isn't enough, 'member' is required
     *
     * # Group
     * dn: cn=parent,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void isUserDirectGroupMember_DoesNotWorkWhenLdapDoesNotUseMemberDNs() throws Exception
    {
        RFC4519Directory directory = createDirectory();

        // user

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        registerUserInDirectory(user, directory);

        // group

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        registerGroupInDirectory(group, directory);

        assertFalse(directory.isUserDirectGroupMember("user", "group"));

        // verify queries

        verifyUserWasSearchedByName(directory, "user");
        verifyGroupWasSearchedByName(directory, "group");

        verify(directory.ldapPropertiesMapper, never()).isUsingUserMembershipAttribute(); // this setting is irrelevant
        verify(directory.ldapPropertiesMapper, never()).getUserGroupMembershipsAttribute(); // not used
        verify(user, never()).getValues("memberOf"); // memberOf is ignored

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # Child group
     * dn: cn=child1,dc=test
     *
     * # Parent group
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void isGroupDirectGroupMember_ViaMemberDNs() throws Exception
    {
        RFC4519Directory directory = createDirectory();

        LDAPGroupWithAttributes childGroup = createLDAPGroupWithAttributes(CHILD_DN1, "child");
        registerGroupInDirectory(childGroup, directory);

        LDAPGroupWithAttributes parentGroup = createLDAPGroupWithAttributes(GROUP_DN, "parent");
        when(parentGroup.getValues("memberDNs")).thenReturn(ImmutableSet.of(CHILD_DN1.toString()));
        registerGroupInDirectory(parentGroup, directory);

        assertTrue(directory.isGroupDirectGroupMember("child", "parent"));

        // verify queries

        verifyGroupWasSearchedByName(directory, "child");
        verifyGroupWasSearchedByName(directory, "parent");

        verify(directory.ldapPropertiesMapper, never()).isUsingUserMembershipAttribute(); // this setting is irrelevant
        verify(directory.ldapPropertiesMapper, never()).getUserGroupMembershipsAttribute(); // not used
        verify(childGroup, never()).getValues("memberOf"); // memberOf is ignored

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     * </pre>
     * @throws Exception
     */
    @Test (expected = MembershipAlreadyExistsException.class)
    public void addUserToGroupWhenMembershipAlreadyExists() throws Exception
    {
        RFC4519Directory directory = createDirectory();

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        registerUserInDirectory(user, directory);

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        when(group.getValues("memberDNs")).thenReturn(ImmutableSet.of(CHILD_DN1.toString()));
        registerGroupInDirectory(group, directory);

        directory.addUserToGroup("user", "group");
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void addUserToGroupWhenUsingMemberDNs() throws Exception
    {
        RFC4519Directory directory = createDirectory();

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        registerUserInDirectory(user, directory);

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        registerGroupInDirectory(group, directory);

        directory.addUserToGroup("user", "group");

        // verify queries

        verifyUserWasSearchedByName(directory, "user");
        verifyGroupWasSearchedByName(directory, "group");
        verify(directory.ldapTemplate)
                .modifyAttributes(eq(GROUP_DN), argThat(org.hamcrest.Matchers.<ModificationItem>hasItemInArray(
                        addAttribute(singleValuedStringAttributeWithNameOf("member", CHILD_DN1)))));

        verify(directory.ldapPropertiesMapper, never()).isUsingUserMembershipAttribute(); // this setting is irrelevant
        verify(directory.ldapPropertiesMapper, never()).getUserGroupMembershipsAttribute(); // not used
        verify(user, never()).getValues("memberOf"); // memberOf is ignored

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # Child
     * dn: cn=child1,dc=test
     *
     * # Parent
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     * </pre>
     * @throws Exception
     */
    @Test (expected = MembershipAlreadyExistsException.class)
    public void addGroupToGroupWhenMembershipAlreadyExists() throws Exception
    {
        RFC4519Directory directory = createDirectory();

        LDAPGroupWithAttributes childGroup = createLDAPGroupWithAttributes(CHILD_DN1, "child");
        registerGroupInDirectory(childGroup, directory);

        LDAPGroupWithAttributes parentGroup = createLDAPGroupWithAttributes(GROUP_DN, "parent");
        when(parentGroup.getValues("memberDNs")).thenReturn(ImmutableSet.of(CHILD_DN1.toString()));
        registerGroupInDirectory(parentGroup, directory);

        directory.addGroupToGroup("child", "parent");
    }

    /**
     * <pre>
     * # Child
     * dn: cn=child1,dc=test
     *
     * # Parent
     * dn: cn=parent,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void addGroupToGroupWhenUsingMemberDNs() throws Exception
    {
        RFC4519Directory directory = createDirectory();

        LDAPGroupWithAttributes childGroup = createLDAPGroupWithAttributes(CHILD_DN1, "child");
        registerGroupInDirectory(childGroup, directory);

        LDAPGroupWithAttributes parentGroup = createLDAPGroupWithAttributes(GROUP_DN, "parent");
        registerGroupInDirectory(parentGroup, directory);

        directory.addGroupToGroup("child", "parent");

        // verify queries

        verifyGroupWasSearchedByName(directory, "child");
        verifyGroupWasSearchedByName(directory, "parent");
        verify(directory.ldapTemplate)
                .modifyAttributes(eq(GROUP_DN), argThat(org.hamcrest.Matchers.<ModificationItem>hasItemInArray(
                        addAttribute(singleValuedStringAttributeWithNameOf("member", CHILD_DN1)))));

        verify(directory.ldapPropertiesMapper, never()).isUsingUserMembershipAttribute(); // this setting is irrelevant
        verify(directory.ldapPropertiesMapper, never()).getUserGroupMembershipsAttribute(); // not used
        verify(childGroup, never()).getValues("memberOf"); // memberOf is ignored

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * </pre>
     * @throws Exception
     */
    @Test (expected = MembershipNotFoundException.class)
    public void removeUserFromGroup_WhenMembershipDoesNotExist() throws Exception
    {
        RFC4519Directory directory = createDirectory();

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        registerUserInDirectory(user, directory);

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        registerGroupInDirectory(group, directory);

        directory.removeUserFromGroup("user", "group");
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void removeUserFromGroup_WhenUsingMemberDNs() throws Exception
    {
        RFC4519Directory directory = createDirectory();

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        registerUserInDirectory(user, directory);

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        when(group.getValues("memberDNs")).thenReturn(ImmutableSet.of(CHILD_DN1.toString()));
        registerGroupInDirectory(group, directory);

        directory.removeUserFromGroup("user", "group");

        // verify queries

        verifyUserWasSearchedByName(directory, "user");
        verifyGroupWasSearchedByName(directory, "group");
        verify(directory.ldapTemplate)
                .modifyAttributes(eq(GROUP_DN), argThat(org.hamcrest.Matchers.<ModificationItem>hasItemInArray(
                        removeAttribute(singleValuedStringAttributeWithNameOf("member", CHILD_DN1)))));

        verify(directory.ldapPropertiesMapper, never()).isUsingUserMembershipAttribute(); // this setting is irrelevant
        verify(directory.ldapPropertiesMapper, never()).getUserGroupMembershipsAttribute(); // not used
        verify(user, never()).getValues("memberOf"); // memberOf is ignored

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # Child
     * dn: cn=child1,dc=test
     *
     * # Parent
     * dn: cn=parent,dc=test
     * </pre>
     * @throws Exception
     */
    @Test (expected = MembershipNotFoundException.class)
    public void removeGroupFromGroup_WhenMembershipDoesNotExist() throws Exception
    {
        RFC4519Directory directory = createDirectory();

        LDAPGroupWithAttributes child = createLDAPGroupWithAttributes(CHILD_DN1, "child");
        registerGroupInDirectory(child, directory);

        LDAPGroupWithAttributes parent = createLDAPGroupWithAttributes(GROUP_DN, "parent");
        registerGroupInDirectory(parent, directory);

        directory.removeGroupFromGroup("child", "parent");
    }

    /**
     * <pre>
     * # Child
     * dn: cn=child1,dc=test
     *
     * # Parent
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void removeGroupFromGroup_WhenUsingMemberDNs() throws Exception
    {
        RFC4519Directory directory = createDirectory();

        LDAPGroupWithAttributes childGroup = createLDAPGroupWithAttributes(CHILD_DN1, "childGroup");
        registerGroupInDirectory(childGroup, directory);

        LDAPGroupWithAttributes parentGroup = createLDAPGroupWithAttributes(GROUP_DN, "parent");
        when(parentGroup.getValues("memberDNs")).thenReturn(ImmutableSet.of(CHILD_DN1.toString()));
        registerGroupInDirectory(parentGroup, directory);

        directory.removeGroupFromGroup("childGroup", "parent");

        // verify queries

        verifyGroupWasSearchedByName(directory, "childGroup");
        verifyGroupWasSearchedByName(directory, "parent");
        verify(directory.ldapTemplate)
                .modifyAttributes(eq(GROUP_DN), argThat(org.hamcrest.Matchers.<ModificationItem>hasItemInArray(
                        removeAttribute(singleValuedStringAttributeWithNameOf("member", CHILD_DN1)))));

        verify(directory.ldapPropertiesMapper, never()).isUsingUserMembershipAttribute(); // this setting is irrelevant
        verify(directory.ldapPropertiesMapper, never()).getUserGroupMembershipsAttribute(); // not used
        verify(childGroup, never()).getValues("memberOf"); // memberOf is ignored

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    @Test
    public void searchGroupRelationships_OnlyQueriesForGroupNamesWhenQueryingForGroupsContainingSubGroup()
        throws Exception
    {
        RFC4519Directory directory = createDirectory();

        // first query: get the subgroup DN querying by the subgroup name
        LDAPQuery ldapQueryToGetTheDn = new LDAPQuery("filter1");
        when(directory.ldapQueryTranslater.asLDAPFilter(any(EntityQuery.class),
                                                        eq(directory.getLdapPropertiesMapper())))
             .thenReturn(ldapQueryToGetTheDn);
        when(directory.ldapTemplate
                 .searchWithLimitedResults(eq(BASE_DN), eq("filter1"), any(SearchControls.class), eq(DN_MAPPER),
                         Mockito.<DirContextProcessor>any(), Mockito.eq(1)))
             .thenReturn(ImmutableList.of(new LdapName("cn=subgroup")));

        // second query: get the parent groups querying by the subgroup DN
        when(directory.ldapTemplate
                 .search(eq(BASE_DN), eq("(&(group-filter)(member=cn=subgroup))"), any(SearchControls.class),
                         eq(NamedLdapEntity.mapperFromAttribute("cn")), Mockito.<DirContextProcessor>any()))
             .thenReturn(ImmutableList.of(new NamedLdapEntity(new LdapName("cn=g1"), "parentgroup1"),
                     new NamedLdapEntity(new LdapName("cn=g2"), "parentgroup2")));

        // invoke the method under test
        MembershipQuery<String> query =
            QueryBuilder.createMembershipQuery(EntityQuery.ALL_RESULTS, 0, false,
                                               EntityDescriptor.group(), String.class,
                                               EntityDescriptor.group(), "subgroup");
        Iterable<String> groupNames = directory.searchGroupRelationshipsWithGroupTypeSpecified(query);

        assertThat(groupNames, containsInAnyOrder("parentgroup1", "parentgroup2"));

        // verify first & second queries
        verify(directory.ldapTemplate)
                 .searchWithLimitedResults(eq(BASE_DN), eq("filter1"), any(SearchControls.class), eq(DN_MAPPER),
                         Mockito.<DirContextProcessor>any(), Mockito.eq(1));
        verify(directory.ldapTemplate)
                 .search(eq(BASE_DN), eq("(&(group-filter)(member=cn=subgroup))"), any(SearchControls.class),
                         eq(NamedLdapEntity.mapperFromAttribute("cn")), Mockito.<DirContextProcessor>any());
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void searchGroupRelationships_QueryGroupsOfUser_ViaMemberDn() throws Exception
    {
        RFC4519Directory directory = createDirectory();
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(false);

        // user

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        registerUserInDirectory(user, directory);

        // group

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN);
        registerGroupInDirectory(group, directory, "(&(group-filter)(member=cn=child1,dc=test))");

        // actual invocation

        MembershipQuery<LDAPGroupWithAttributes> queryGroupsOfUser =
            QueryBuilder.createMembershipQuery(10, 0, false, EntityDescriptor.group(), LDAPGroupWithAttributes.class,
                    EntityDescriptor.user(), "user");

        assertThat(directory.searchGroupRelationships(queryGroupsOfUser), hasItem(group));

        // verify queries

        verifyUserWasSearchedByName(directory, "user");

        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN),
                eq("(&(group-filter)(member=cn=child1,dc=test))"),
                any(SearchControls.class),
                any(GroupContextMapper.class),
                any(DirContextProcessor.class), eq(10));

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void searchGroupRelationships_QueryGroupsOfUser_ViaMemberDnEvenWhenUserMembershipAttributeFlagIsOn()
            throws Exception
    {
        RFC4519Directory directory = createDirectory();
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(true); // makes no difference

        // user

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        registerUserInDirectory(user, directory);

        // group

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN);
        registerGroupInDirectory(group, directory, "(&(group-filter)(member=cn=child1,dc=test))");

        // actual invocation

        MembershipQuery<LDAPGroupWithAttributes> queryGroupsOfUser =
                QueryBuilder.createMembershipQuery(10, 0, false, EntityDescriptor.group(), LDAPGroupWithAttributes.class,
                        EntityDescriptor.user(), "user");

        assertThat(directory.searchGroupRelationships(queryGroupsOfUser), hasItem(group));

        // verify queries

        verifyUserWasSearchedByName(directory, "user");

        // in spite of having the user membership attribute flag active, this method always queries using
        // the 'member' attribute of the group for performance reasons
        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN),
                eq("(&(group-filter)(member=cn=child1,dc=test))"),
                any(SearchControls.class),
                any(GroupContextMapper.class),
                any(DirContextProcessor.class), eq(10));

        verifyNoMoreInteractions(directory.ldapTemplate); // no query for memberOf

        verify(user, never()).getValues("memberOf"); // memberOf is ignored
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     * memberOf: cn=parent,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void searchGroupRelationships_QueryGroupsOfUser_ViaMemberOf() throws Exception
    {
        RFC4519Directory directory = createDirectory();
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(true); // makes no difference
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttributeForGroupMembership()).thenReturn(true);

        // user

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        when(user.getValues("memberOf")).thenReturn(ImmutableSet.of(GROUP_DN.toString()));
        registerUserInDirectory(user, directory);

        // group
        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        registerGroupInDirectory(group, directory);

        // actual invocation

        MembershipQuery<LDAPGroupWithAttributes> queryGroupsOfUser =
                QueryBuilder.createMembershipQuery(10, 0, false, EntityDescriptor.group(), LDAPGroupWithAttributes.class,
                        EntityDescriptor.user(), "user");

        assertThat(directory.searchGroupRelationships(queryGroupsOfUser), hasItem(group));

        // verify queries

        verifyUserWasSearchedByName(directory, "user");
        verifyGroupWasSearchedByDN(directory, GROUP_DN);

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void searchGroupRelationships_QueryGroupNamesOfUser_ViaMemberDn() throws Exception
    {
        RFC4519Directory directory = createDirectory();
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(false);

        // user

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        registerUserInDirectory(user, directory);

        // group

        when(directory.ldapTemplate.searchWithLimitedResults(eq(BASE_DN),
                                                             eq("(&(group-filter)(member=cn=child1,dc=test))"),
                                                             any(SearchControls.class),
                                                             eq(NamedLdapEntity.mapperFromAttribute("cn")),
                                                             any(DirContextProcessor.class), eq(10)))
            .thenReturn(ImmutableList.of(new NamedLdapEntity(GROUP_DN, "group")));

        // actual invocation

        MembershipQuery<String> queryGroupNamesOfUser =
                QueryBuilder.createMembershipQuery(10, 0, false, EntityDescriptor.group(), String.class,
                                               EntityDescriptor.user(), "user");

        assertThat(directory.searchGroupRelationships(queryGroupNamesOfUser), hasItem("group"));

        // verify queries

        verifyUserWasSearchedByName(directory, "user");

        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN),
                eq("(&(group-filter)(member=cn=child1,dc=test))"),
                any(SearchControls.class),
                eq(NamedLdapEntity.mapperFromAttribute("cn")),
                any(DirContextProcessor.class), eq(10));

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void searchGroupRelationships_QueryGroupNamesOfUser_ViaMemberDnEvenWhenUserMembershipAttributeFlagIsOn()
            throws Exception
    {
        RFC4519Directory directory = createDirectory();
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(true); // makes no difference

        // user

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        registerUserInDirectory(user, directory);

        // group

        when(directory.ldapTemplate.searchWithLimitedResults(eq(BASE_DN),
                eq("(&(group-filter)(member=cn=child1,dc=test))"),
                any(SearchControls.class),
                eq(NamedLdapEntity.mapperFromAttribute("cn")),
                any(DirContextProcessor.class), eq(10)))
                .thenReturn(ImmutableList.of(new NamedLdapEntity(GROUP_DN, "group")));

        // actual invocation

        MembershipQuery<String> queryGroupNamesOfUser =
                QueryBuilder.createMembershipQuery(10, 0, false, EntityDescriptor.group(), String.class,
                        EntityDescriptor.user(), "user");

        assertThat(directory.searchGroupRelationships(queryGroupNamesOfUser), hasItem("group"));

        // verify queries

        verifyUserWasSearchedByName(directory, "user");

        // in spite of having the user membership attribute flag active, this method always queries using
        // the 'member' attribute of the group for performance reasons
        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN),
                eq("(&(group-filter)(member=cn=child1,dc=test))"),
                any(SearchControls.class),
                eq(NamedLdapEntity.mapperFromAttribute("cn")),
                any(DirContextProcessor.class), eq(10));

        verifyNoMoreInteractions(directory.ldapTemplate); // no query for memberOf

        verify(user, never()).getValues("memberOf"); // memberOf is ignored
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     * memberOf: cn=parent,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void searchGroupRelationships_QueryGroupNamesOfUser_ViaMemberOf()
            throws Exception
    {
        RFC4519Directory directory = createDirectory();
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(true); // makes no difference
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttributeForGroupMembership()).thenReturn(true);

        // user

        LDAPUserWithAttributes user = createLDAPUserWithAttributes(CHILD_DN1, "user");
        when(user.getValues("memberOf")).thenReturn(ImmutableSet.of(GROUP_DN.toString()));
        registerUserInDirectory(user, directory);

        // group
        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        registerGroupInDirectory(group, directory);

        // actual invocation

        MembershipQuery<String> queryGroupNamesOfUser =
                QueryBuilder.createMembershipQuery(10, 0, false, EntityDescriptor.group(), String.class,
                        EntityDescriptor.user(), "user");

        assertThat(directory.searchGroupRelationships(queryGroupNamesOfUser), hasItem("group"));

        // verify queries

        verifyUserWasSearchedByName(directory, "user");
        verifyGroupWasLookedUpByDN(directory, GROUP_DN);

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void searchGroupRelationships_QueryUsersOfGroup_ViaMemberDN() throws Exception
    {
        RFC4519Directory directory = createDirectory();
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(false);

        // group

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        when(group.getValues("memberDNs")).thenReturn(ImmutableSet.of(CHILD_DN1.toString()));
        registerGroupInDirectory(group, directory);

        // regular user

        LDAPUserWithAttributes user1 = createLDAPUserWithAttributes(CHILD_DN1);
        when(directory.ldapTemplate.search(eq(CHILD_DN1), eq("(user-filter)"), any(SearchControls.class),
                any(UserContextMapper.class)))
            .thenReturn(ImmutableList.of(user1));

        // actual invocation

        MembershipQuery<LDAPUserWithAttributes> queryUsersOfGroup =
                QueryBuilder.createMembershipQuery(10, 0, true, EntityDescriptor.user(), LDAPUserWithAttributes.class,
                        EntityDescriptor.group(), "group");

        assertThat(directory.searchGroupRelationships(queryUsersOfGroup), hasItem(user1));

        // verify queries

        verifyGroupWasSearchedByName(directory, "group");

        verify(directory.ldapTemplate).search(eq(CHILD_DN1), eq("(user-filter)"), any(SearchControls.class),
                any(UserContextMapper.class));

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child1,dc=test
     * memberOf: cn=parent,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void searchGroupRelationships_QueryUsersOfGroup_ViaMemberOf() throws Exception
    {
        RFC4519Directory directory = createDirectory();
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(true);

        // group

        LDAPGroupWithAttributes group = createLDAPGroupWithAttributes(GROUP_DN, "group");
        when(group.getType()).thenReturn(GroupType.GROUP);
        registerGroupInDirectory(group, directory);

        // regular user

        LDAPUserWithAttributes user1 = createLDAPUserWithAttributes(CHILD_DN1);
        when(directory.ldapTemplate.searchWithLimitedResults(eq(BASE_DN),
                                                             eq("(&(user-filter)(memberOf=cn=parent,dc=test))"),
                                                             any(SearchControls.class),
                                                             any(UserContextMapper.class),
                                                             any(DirContextProcessor.class), eq(10)))
            .thenReturn(ImmutableList.of(user1));

        // actual invocation

        MembershipQuery<LDAPUserWithAttributes> queryUsersOfGroup =
                QueryBuilder.createMembershipQuery(10, 0, true, EntityDescriptor.user(), LDAPUserWithAttributes.class,
                        EntityDescriptor.group(), "group");

        assertThat(directory.searchGroupRelationships(queryUsersOfGroup), hasItem(user1));

        // verify queries

        verifyGroupWasSearchedByName(directory, "group");

        verify(directory.ldapTemplate).searchWithLimitedResults(eq(BASE_DN),
                                                                eq("(&(user-filter)(memberOf=cn=parent,dc=test))"),
                                                                any(SearchControls.class),
                                                                any(UserContextMapper.class),
                                                                any(DirContextProcessor.class), eq(10));

        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    @Test
    public void findDirectMembersOfGroupUsingMemberAttribute() throws Exception
    {
        RFC4519Directory directory = createDirectory();
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(false);
        when(directory.getLdapPropertiesMapper().isNestedGroupsDisabled()).thenReturn(false);

        // group

        LDAPGroupWithAttributes ldapGroupWithAttributes = createLDAPGroupWithAttributes(GROUP_DN);
        when(ldapGroupWithAttributes.getValues(RFC4519MemberDnMapper.ATTRIBUTE_KEY))
            .thenReturn(ImmutableSet.of(CHILD_DN1.toString()));

        when(directory.ldapTemplate.lookup(eq(GROUP_DN), any(GroupContextMapper.class)))
            .thenReturn(ldapGroupWithAttributes);

        assertThat(directory.findDirectMembersOfGroup(GROUP_DN), hasItem(CHILD_DN1));

        // verify only one query was made to the server
        verify(directory.ldapTemplate).lookup(eq(GROUP_DN), any(GroupContextMapper.class));
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: cn=child2,dc=test
     * memberOf: cn=parent,dc=test
     *
     * # Group
     * dn: cn=parent,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void findDirectMembersOfGroupUsingMemberOfAttributeAndNestedGroupsDisabled() throws Exception
    {
        RFC4519Directory directory = createDirectory();
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(true);
        when(directory.getLdapPropertiesMapper().isNestedGroupsDisabled()).thenReturn(true);
        when(directory.getLdapPropertiesMapper().isPagedResultsControl()).thenReturn(false);

        when(directory.ldapTemplate.search(eq(BASE_DN), eq("(&(user-filter)(memberOf=cn=parent,dc=test))"),
                                           any(SearchControls.class), any(ContextMapper.class),
                                           any(DirContextProcessor.class)))
            .thenReturn(ImmutableList.of(CHILD_DN2));

        assertThat(directory.findDirectMembersOfGroup(GROUP_DN), hasItem(CHILD_DN2));

        // verify only one query was made to the server
        verify(directory.ldapTemplate).search(eq(BASE_DN), eq("(&(user-filter)(memberOf=cn=parent,dc=test))"),
                                              any(SearchControls.class), any(ContextMapper.class),
                                              any(DirContextProcessor.class));
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    /**
     * <pre>
     * # User
     * dn: dc=child1,dc=test
     *
     * # Child group
     * dn: cn=child2,dc=test
     * memberOf: cn=parent,dc=test
     *
     * # Parent group
     * dn: cn=parent,dc=test
     * member: cn=child1,dc=test
     * </pre>
     * @throws Exception
     */
    @Test
    public void findDirectMembersOfGroupUsingMemberOfAttributeAndNestedGroupsEnabled() throws Exception
    {
        RFC4519Directory directory = createDirectory();
        when(directory.getLdapPropertiesMapper().isUsingUserMembershipAttribute()).thenReturn(true);
        when(directory.getLdapPropertiesMapper().isNestedGroupsDisabled()).thenReturn(false);
        when(directory.getLdapPropertiesMapper().isPagedResultsControl()).thenReturn(false);

        // group

        LDAPGroupWithAttributes ldapGroupWithAttributes = createLDAPGroupWithAttributes(GROUP_DN);
        when(ldapGroupWithAttributes.getValues(RFC4519MemberDnMapper.ATTRIBUTE_KEY))
            .thenReturn(ImmutableSet.of(CHILD_DN1.toString()));

        when(directory.ldapTemplate.lookup(eq(GROUP_DN), any(GroupContextMapper.class)))
            .thenReturn(ldapGroupWithAttributes);

        when(directory.ldapTemplate.search(eq(BASE_DN), eq("(&(user-filter)(memberOf=cn=parent,dc=test))"),
                                           any(SearchControls.class), any(ContextMapper.class),
                                           any(DirContextProcessor.class)))
            .thenReturn(ImmutableList.of(CHILD_DN2));

        assertThat(directory.findDirectMembersOfGroup(GROUP_DN), hasItems(CHILD_DN1, CHILD_DN2));

        // verify two queries were made to the server
        verify(directory.ldapTemplate).lookup(eq(GROUP_DN), any(GroupContextMapper.class));
        verify(directory.ldapTemplate).search(eq(BASE_DN), eq("(&(user-filter)(memberOf=cn=parent,dc=test))"),
                                              any(SearchControls.class), any(ContextMapper.class),
                                              any(DirContextProcessor.class));
        verifyNoMoreInteractions(directory.ldapTemplate);
    }

    @Test
    public void parseValidEntityDnAsLdapName() throws Exception
    {
        GroupTemplateWithAttributes groupTemplate = new GroupTemplateWithAttributes("group1", 1L, GroupType.GROUP);
        LDAPGroupWithAttributes ldapGroup = new LDAPGroupWithAttributes("cn=g1", groupTemplate);
        assertEquals(new LdapName("cn=g1"), RFC4519Directory.getLdapName(ldapGroup));
    }

    @Test (expected = OperationFailedException.class)
    public void parseInValidEntityDnAsLdapName() throws Exception
    {
        GroupTemplateWithAttributes groupTemplate = new GroupTemplateWithAttributes("group1", 1L, GroupType.GROUP);
        LDAPGroupWithAttributes ldapGroup = new LDAPGroupWithAttributes("invalid-cn", groupTemplate);

        RFC4519Directory.getLdapName(ldapGroup);
    }
}
