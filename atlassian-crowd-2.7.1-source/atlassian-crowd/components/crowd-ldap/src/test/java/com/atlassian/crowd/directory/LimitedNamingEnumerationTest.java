package com.atlassian.crowd.directory;

import java.util.NoSuchElementException;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

import com.google.common.collect.ImmutableList;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

public class LimitedNamingEnumerationTest
{
    @Test
    public void emptyNamingEnumerationIsProxied() throws NamingException
    {
        NamingEnumeration<Object> empty = mock(NamingEnumeration.class);

        NamingEnumeration<?> wrapper = new LimitedNamingEnumeration<Object>(empty, 1);

        assertFalse(wrapper.hasMoreElements());
        assertFalse(wrapper.hasMore());
    }

    @Test
    public void limitOfZeroResultsInAnEmptyEnumeration() throws NamingException
    {
        Iterable<String> realItems = ImmutableList.of("a", "b");

        NamingEnumeration<String> wrapped = new LimitedNamingEnumeration<String>(new StubNamingEnumeration<String>(realItems.iterator()), 0);
        assertFalse(wrapped.hasMore());
    }

    @Test
    public void limitsAreEnforced() throws NamingException
    {
        Iterable<String> realItems = ImmutableList.of("a", "b");

        NamingEnumeration<String> wrapped;

        wrapped = new LimitedNamingEnumeration<String>(new StubNamingEnumeration<String>(realItems.iterator()), 1);
        assertTrue(wrapped.hasMore());
        assertEquals("a", wrapped.next());
        assertFalse(wrapped.hasMore());

        wrapped = new LimitedNamingEnumeration<String>(new StubNamingEnumeration<String>(realItems.iterator()), 2);
        assertTrue(wrapped.hasMore());
        assertEquals("a", wrapped.next());
        assertTrue(wrapped.hasMore());
        assertEquals("b", wrapped.next());
        assertFalse(wrapped.hasMore());
    }

    @Test
    public void limitsAreNotEnforcedWhenUnderlyingEnumerationHasMoreElementsThanLimit() throws NamingException
    {
        Iterable<String> realItems = ImmutableList.of("a", "b");

        NamingEnumeration<String> wrapped;

        wrapped = new LimitedNamingEnumeration<String>(new StubNamingEnumeration<String>(realItems.iterator()), 3);
        assertTrue(wrapped.hasMore());
        assertEquals("a", wrapped.next());
        assertTrue(wrapped.hasMore());
        assertEquals("b", wrapped.next());
        assertFalse(wrapped.hasMore());
    }

    @Test(expected = NoSuchElementException.class)
    public void enumeratingPastLimitThrowsException() throws NamingException
    {
        Iterable<String> realItems = ImmutableList.of("a", "b");

        NamingEnumeration<String> wrapped = new LimitedNamingEnumeration<String>(new StubNamingEnumeration<String>(realItems.iterator()), 0);
        wrapped.next();
    }
}
