package com.atlassian.crowd.directory.ldap.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @since v2.7.0
 */
public class GuidHelperTest
{
    @Test
    public void emptyGuidArrayBecomesEmptyString()
    {
        assertEquals("", GuidHelper.getGUIDAsString(new byte[0]));
    }

    @Test
    public void bytesWithoutHighNibbleArePaddedWithZero()
    {
        assertEquals("01", GuidHelper.getGUIDAsString(new byte[] { 1 }));
    }

    @Test
    public void hexDigitsAreLowercase()
    {
        assertEquals("ba", GuidHelper.getGUIDAsString(new byte[] { (byte) 0xBA }));
    }
}
