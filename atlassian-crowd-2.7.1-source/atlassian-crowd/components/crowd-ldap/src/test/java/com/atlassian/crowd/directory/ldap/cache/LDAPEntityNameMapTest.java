package com.atlassian.crowd.directory.ldap.cache;

import java.util.Collections;
import java.util.Map;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;

import com.atlassian.crowd.model.LDAPDirectoryEntity;

import com.google.common.collect.ImmutableMap;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LDAPEntityNameMapTest
{
    @Test
    public void toLdapNameConvertsEmptyMap() throws InvalidNameException
    {
        LDAPEntityNameMap<LDAPDirectoryEntity> map = new LDAPEntityNameMap<LDAPDirectoryEntity>();
        assertEquals(Collections.emptyMap(), map.toLdapNameKeyedMap());
    }

    @Test(expected = InvalidNameException.class)
    public void toLdapNamePropagatesExceptions() throws InvalidNameException
    {
        LDAPEntityNameMap<LDAPDirectoryEntity> map = new LDAPEntityNameMap<LDAPDirectoryEntity>();

        LDAPDirectoryEntity entity = mock(LDAPDirectoryEntity.class);
        when(entity.getDn()).thenReturn("bad dn");
        map.put(entity);

        map.toLdapNameKeyedMap();
    }

    @Test
    public void toLdapNameParsesLdapNames() throws InvalidNameException
    {
        LDAPEntityNameMap<LDAPDirectoryEntity> map = new LDAPEntityNameMap<LDAPDirectoryEntity>();

        LDAPDirectoryEntity entity1 = mock(LDAPDirectoryEntity.class);
        when(entity1.getDn()).thenReturn("cn=one");
        when(entity1.getName()).thenReturn("One");
        map.put(entity1);

        LDAPDirectoryEntity entity2 = mock(LDAPDirectoryEntity.class);
        when(entity2.getDn()).thenReturn("cn=two");
        when(entity2.getName()).thenReturn("Two");
        map.put(entity2);

        Map<LdapName, String> expected = ImmutableMap.<LdapName, String>of(
                new LdapName("cn=one"), "One",
                new LdapName("cn=two"), "Two"
        );

        assertEquals(expected, map.toLdapNameKeyedMap());
    }
}
