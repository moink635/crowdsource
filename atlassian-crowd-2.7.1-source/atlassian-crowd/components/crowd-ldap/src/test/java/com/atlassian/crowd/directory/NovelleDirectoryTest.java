package com.atlassian.crowd.directory;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.util.PasswordHelper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NovelleDirectoryTest
{
    @Mock
    private PasswordHelper passwordHelper;

    @InjectMocks
    private NovelleDirectory novelleDirectory;

    @Test
    public void testEncodePasswordShouldNotEncryptPassword() throws Exception
    {
        String password = "secret";
        assertEquals(password, novelleDirectory.encodePassword(PasswordCredential.unencrypted(password)));
    }

    @Test
    public void encodePasswordShouldQuoteAndEncodeARandomPasswordWhenAskedToEncodePasswordCredentialNone() throws Exception
    {
        when(passwordHelper.generateRandomPassword()).thenReturn("random-password");

        String passwordUsedByDirectory = novelleDirectory.encodePassword(PasswordCredential.NONE);

        verify(passwordHelper).generateRandomPassword();
        assertEquals("random-password", passwordUsedByDirectory);
    }

    @Test(expected = InvalidCredentialException.class)
    public void encodePasswordShouldFailToEncodeAnEncryptedPasswordIfItIsNotPasswordCredentialNone() throws Exception
    {
        novelleDirectory.encodePassword(PasswordCredential.encrypted("my-encrypted-pass"));
    }
}
