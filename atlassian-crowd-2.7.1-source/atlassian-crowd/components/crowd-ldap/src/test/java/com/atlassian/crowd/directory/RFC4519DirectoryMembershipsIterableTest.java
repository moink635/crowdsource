package com.atlassian.crowd.directory;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;

import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.group.Membership;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertSame;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class RFC4519DirectoryMembershipsIterableTest
{
    @Test
    public void knownGroupsAreQueriedForDirectMembers() throws InvalidNameException, OperationFailedException
    {
        RFC4519Directory springLDAPConnector = mock(RFC4519Directory.class);

        Map<LdapName, String> users = Collections.<LdapName, String>emptyMap();
        Map<LdapName, String> groups = ImmutableMap.of(
                new LdapName("cn=one"), "One",
                new LdapName("cn=two"), "Two");

        when(springLDAPConnector.findDirectMembersOfGroup(Mockito.<LdapName>any())).thenReturn(Collections.<LdapName>emptyList());

        RFC4519DirectoryMembershipsIterable iterable = new RFC4519DirectoryMembershipsIterable(springLDAPConnector, users, groups);

        assertEquals(2, Iterables.size(iterable));

        verify(springLDAPConnector).findDirectMembersOfGroup(new LdapName("cn=one"));
        verify(springLDAPConnector).findDirectMembersOfGroup(new LdapName("cn=two"));
        verifyNoMoreInteractions(springLDAPConnector);
    }

    @Test
    public void onlyKnownGroupsSpecifiedAsGroupsToIncludeAreQueriedForDirectMembers() throws InvalidNameException, OperationFailedException
    {
        RFC4519Directory springLDAPConnector = mock(RFC4519Directory.class);

        Map<LdapName, String> users = Collections.<LdapName, String>emptyMap();
        Map<LdapName, String> groups = ImmutableMap.of(
                new LdapName("cn=one"), "One",
                new LdapName("cn=two"), "Two");

        when(springLDAPConnector.findDirectMembersOfGroup(Mockito.<LdapName>any())).thenReturn(Collections.<LdapName>emptyList());

        Set<String> groupsToInclude = ImmutableSet.of("one", "unknown-group");
        RFC4519DirectoryMembershipsIterable iterable = new RFC4519DirectoryMembershipsIterable(springLDAPConnector, users, groups, groupsToInclude);

        assertEquals(1, Iterables.size(iterable));

        verify(springLDAPConnector).findDirectMembersOfGroup(new LdapName("cn=one"));
        verifyNoMoreInteractions(springLDAPConnector);
    }

    @Test
    public void membershipsIncludeExpectedMembers() throws InvalidNameException, OperationFailedException
    {
        RFC4519Directory springLDAPConnector = mock(RFC4519Directory.class);

        Map<LdapName, String> users = ImmutableMap.of(new LdapName("cn=user-one"), "User One");

        Map<LdapName, String> groups = ImmutableMap.of(
                new LdapName("cn=group-one"), "Group One",
                new LdapName("cn=group-two"), "Parent Group");

        when(springLDAPConnector.findDirectMembersOfGroup(new LdapName("cn=group-one"))).thenReturn(Collections.<LdapName>emptyList());
        when(springLDAPConnector.findDirectMembersOfGroup(new LdapName("cn=group-two"))).thenReturn(ImmutableList.of(
                new LdapName("cn=user-one"),
                new LdapName("cn=group-one")));

        RFC4519DirectoryMembershipsIterable iterable = new RFC4519DirectoryMembershipsIterable(springLDAPConnector, users, groups, ImmutableSet.of("parent group"));

        Membership membership = Iterables.getOnlyElement(iterable);

        assertEquals("Parent Group", membership.getGroupName());
        assertEquals(ImmutableSet.of("User One"), membership.getUserNames());
        assertEquals(ImmutableSet.of("Group One"), membership.getChildGroupNames());

        /* Verify that the memberships are already instances of ImmutableSet. */
        assertSame(membership.getUserNames(), ImmutableSet.copyOf(membership.getUserNames()));
        assertSame(membership.getChildGroupNames(), ImmutableSet.copyOf(membership.getChildGroupNames()));
    }
}
