package com.atlassian.crowd.directory.ldap.name;

import javax.naming.CompositeName;
import javax.naming.InvalidNameException;
import javax.naming.Name;
import javax.naming.ldap.LdapName;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GenericConverterTest
{
    Converter converter;

    @Before
    public void setUp()
    {
        converter = new GenericConverter();
    }

    @After
    public void tearDown()
    {
        converter = null;
    }

    @Test(expected = InvalidNameException.class)
    public void testGetNameWithInvalidEscapeFails() throws Exception
    {
        Name name = converter.getName("cn=Smi\\th,name,dc=example,dc=org");
    }

    @Test
    public void testGetNameWorksWithSingleDN() throws Exception
    {
        Name name = converter.getName("cn=my name, dc=example, dc=org");
        assertNotNull(name);
        assertEquals(new LdapName("cn=my name, dc=example, dc=org"), name);
    }

    @Test
    public void testGetNameWorksWithDNAndBase() throws Exception
    {
        Name name = converter.getName("cn", "my name", new LdapName("dc=example, dc=org"));
        assertNotNull(name);
        assertEquals(new LdapName("cn=my name, dc=example, dc=org"), name);
    }

    @Test
    public void testGetNameWorksWithDNAndBaseWithBackslash() throws Exception
    {
        Name name = converter.getName("cn", "my \\ name", new LdapName("dc=example, dc=org"));
        assertNotNull(name);
        assertEquals(new LdapName("cn=my \\\\ name, dc=example, dc=org"), name);
    }

    @Test
    public void getNameWorksWithDNAndRdnWithForwardSlash() throws Exception
    {
        Name name = converter.getName("cn", "my / name", new LdapName("dc=example, dc=org"));
        assertNotNull(name);
        assertEquals(new LdapName("cn=my / name, dc=example, dc=org"), name);
    }

    @Test
    public void getNameWorksWithCommaInValue() throws Exception
    {
        Name name = converter.getName("cn", "value,value", GenericConverter.emptyLdapName());
        assertNotNull(name);
        assertEquals(new LdapName("cn=value\\,value"), name);
    }

    @Test
    public void testGetNameWorksWithEmptyDN() throws Exception
    {
        Name name = converter.getName("cn", "my name", new LdapName(""));
        assertNotNull(name);
        assertEquals("cn=my name", name.get(0));
        assertEquals(1, name.size());
    }

    @Test
    public void testGetNameWorksWithNullDN() throws Exception
    {
        Name name = converter.getName("cn", "my name", null);
        assertNotNull(name);
        assertEquals("cn=my name", name.get(0));
        assertEquals(1, name.size());
    }

    @Test
    public void demonstrateCompositeNameNotLikingSlashes() throws Exception
    {
        Name name = new CompositeName("cn=my/value=name");
        assertEquals(2, name.size());
        assertEquals("cn=my", name.get(0).toString());
        assertEquals("value=name", name.get(1).toString());

        Name name2 = new LdapName("cn=my/value=name");
        assertEquals(1, name2.size());
        assertEquals("cn=my/value\\=name", name2.get(0));
    }

    @Test
    public void emptyLdapNameIsAvailable() throws Exception
    {
        LdapName empty = GenericConverter.emptyLdapName();
        assertEquals(0, empty.size());
        assertEquals("", empty.toString());
    }
}
