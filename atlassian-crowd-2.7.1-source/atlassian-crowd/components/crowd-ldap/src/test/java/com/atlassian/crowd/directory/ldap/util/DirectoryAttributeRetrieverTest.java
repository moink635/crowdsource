package com.atlassian.crowd.directory.ldap.util;

import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;

import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DirectoryAttributeRetrieverTest
{
    private static final String EXTERNAL_ID_ATTRIBUTE = "external-id-attribute";

    @Test
    public void shouldReturnNullIfAttributeNameIsNotDefined() throws Exception
    {
        Attributes emptyAttributes = mock(Attributes.class);
        assertThat(DirectoryAttributeRetriever.getValueFromExternalIdAttribute(null, emptyAttributes),
                nullValue());
    }

    @Test
    public void shouldReturnEmptyIfAttributeValueIsNotDefined() throws Exception
    {
        Attributes emptyAttributes = mock(Attributes.class);
        assertThat(DirectoryAttributeRetriever.getValueFromExternalIdAttribute(EXTERNAL_ID_ATTRIBUTE, emptyAttributes),
                isEmptyString());
    }

    @Test
    public void shouldReturnEmptyIfAttributeIsMultivalued() throws Exception
    {
        Attribute attribute = new BasicAttribute(EXTERNAL_ID_ATTRIBUTE, "value1");
        attribute.add("value2");

        Attributes attributes = mock(Attributes.class);
        when(attributes.get(EXTERNAL_ID_ATTRIBUTE)).thenReturn(attribute);

        assertThat(DirectoryAttributeRetriever.getValueFromExternalIdAttribute(EXTERNAL_ID_ATTRIBUTE, attributes),
                isEmptyString());
    }

    @Test
    public void shouldReturnExternalIdValueIfValueIsAStringServedAsAString() throws Exception
    {
        Attributes attributes = mockAttributes(EXTERNAL_ID_ATTRIBUTE, "value");
        assertThat(DirectoryAttributeRetriever.getValueFromExternalIdAttribute(EXTERNAL_ID_ATTRIBUTE, attributes),
                equalTo("value"));
    }

    @Test
    public void shouldReturnExternalIdValueIfValueIsAByteArrayServedAsAByteArray() throws Exception
    {
        Attributes atributes = mockAttributes(EXTERNAL_ID_ATTRIBUTE, new byte[]{ (byte) 0xBA});
        assertThat(DirectoryAttributeRetriever.getValueFromExternalIdAttribute(EXTERNAL_ID_ATTRIBUTE, atributes),
                equalTo("ba"));
    }

    /**
     * This may happen if the LDAP server is misconfigured and it is serving a byte array as a String attribute.
     * See CWD-3748.
     */
    @Test
    public void shouldSkipExternalIdValueIfValueContainsNonValidXmlChars() throws Exception
    {
        String binaryValueServedAsAString = "\u0000";
        Attributes directoryAttributes = mockAttributes(EXTERNAL_ID_ATTRIBUTE, binaryValueServedAsAString);
        assertThat(DirectoryAttributeRetriever.getValueFromExternalIdAttribute(EXTERNAL_ID_ATTRIBUTE, directoryAttributes),
                isEmptyString());
    }

    private static Attributes mockAttributes(String name, Object value)
    {
        Attribute attribute = new BasicAttribute(name, value);

        Attributes attributes = mock(Attributes.class);
        when(attributes.get(name)).thenReturn(attribute);

        return attributes;
    }
}
