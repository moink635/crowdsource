package com.atlassian.crowd.directory.ldap.mapper;

import java.util.Collections;
import java.util.Set;

import com.atlassian.crowd.directory.ldap.mapper.attribute.AttributeMapper;

import org.junit.Test;
import org.springframework.ldap.core.DirContextAdapter;

import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ContextMapperWithCustomAttributesTest
{
    @Test
    public void nullPropagatesFromCoreAttributes()
    {
        ContextMapperWithRequiredAttributes<Object> m = new ContextMapperWithCustomAttributes<Object>(Collections.<AttributeMapper>emptyList())
        {
            @Override
            public Object mapFromContext(DirContextAdapter arg0)
            {
                throw new UnsupportedOperationException();
            }

            @Override
            protected Set<String> getCoreRequiredLdapAttributes()
            {
                return null;
            }
        };

        assertNull(m.getRequiredLdapAttributes());
    }

    @Test
    public void nullPropagatesFromCustomMapper()
    {
        AttributeMapper mapperReturningNullForRequiredLdapAttributes = mock(AttributeMapper.class);
        when(mapperReturningNullForRequiredLdapAttributes.getRequiredLdapAttributes()).thenReturn(null);

        ContextMapperWithRequiredAttributes<Object> m = new ContextMapperWithCustomAttributes<Object>(
                Collections.singletonList(mapperReturningNullForRequiredLdapAttributes))
        {
            @Override
            public Object mapFromContext(DirContextAdapter arg0)
            {
                throw new UnsupportedOperationException();
            }

            @Override
            protected Set<String> getCoreRequiredLdapAttributes()
            {
                return Collections.emptySet();
            }
        };

        assertNull(m.getRequiredLdapAttributes());
    }
}
