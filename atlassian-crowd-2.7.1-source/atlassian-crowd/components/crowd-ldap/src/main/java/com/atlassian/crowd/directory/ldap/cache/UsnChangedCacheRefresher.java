package com.atlassian.crowd.directory.ldap.cache;

import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;

import com.atlassian.crowd.directory.MicrosoftActiveDirectory;
import com.atlassian.crowd.directory.RFC4519DirectoryMembershipsIterable;
import com.atlassian.crowd.directory.SynchronisableDirectoryProperties;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.Tombstone;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.LDAPGroupWithAttributes;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.model.user.LDAPUserWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import com.atlassian.util.concurrent.ThreadFactories;
import com.google.common.collect.ImmutableSet;

import com.google.common.collect.Sets;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Retrieves latest changes from MS Active Directory in order to allow "delta" cache refreshes.
 * <p/>
 * See http://msdn.microsoft.com/en-us/library/ms677625.aspx and http://msdn.microsoft.com/en-us/library/ms677627%28VS.85%29.aspx
 * for details on polling Microsoft Active Directory.
 * <p/>
 * This class is guaranteed to be run from a single thread at a time by per directory.  This means it does not need to
 * worry about race-conditions, but still must consider safe publication of variables (per directory).
 */
public class UsnChangedCacheRefresher extends AbstractCacheRefresher implements CacheRefresher
{
    private static final Logger log = LoggerFactory.getLogger(UsnChangedCacheRefresher.class);
    private static final long UNINITIALISED = -1;

    private final MicrosoftActiveDirectory activeDirectory;

    // the following caches should be stored in a cluster-safe manner
    // make volatile for safe-publication
    private volatile long highestCommittedUSN = UNINITIALISED;
    private final LDAPEntityNameMap<LDAPUserWithAttributes> userMap = new LDAPEntityNameMap<LDAPUserWithAttributes>();
    private final LDAPEntityNameMap<LDAPGroupWithAttributes> groupMap = new LDAPEntityNameMap<LDAPGroupWithAttributes>();

    private Future<List<LDAPUserWithAttributes>> userListFuture;
    private Future<List<LDAPGroupWithAttributes>> groupListFuture;

    public UsnChangedCacheRefresher(final MicrosoftActiveDirectory activeDirectory)
    {
        super(activeDirectory);
        this.activeDirectory = activeDirectory;
    }

    public boolean synchroniseChanges(DirectoryCache directoryCache) throws OperationFailedException
    {
        // When restarting the app, we must do a full refresh the first time.
        if (highestCommittedUSN == UNINITIALISED ||
            !Boolean.parseBoolean(activeDirectory.getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED)))
        {
            return false;
        }

        // Find the latest USN-Changed value for the AD server
        long currentHighestCommittedUSN = activeDirectory.fetchHighestCommittedUSN();

        synchroniseUserChanges(directoryCache);
        synchroniseGroupChanges(directoryCache);

        // Remember the USN-Changed value that we were on when we started  (this ensures we don't miss anything, but we may get duplicates)
        this.highestCommittedUSN = currentHighestCommittedUSN;

        return true;
    }

    public void synchroniseAll(DirectoryCache directoryCache) throws OperationFailedException
    {
        ExecutorService queryExecutor = Executors.newFixedThreadPool(3, ThreadFactories.namedThreadFactory("CrowdUsnChangedCacheRefresher"));
        try
        {
            userListFuture = queryExecutor.submit(new Callable<List<LDAPUserWithAttributes>>()
            {
                public List<LDAPUserWithAttributes> call() throws Exception
                {
                    long start = System.currentTimeMillis();
                    log.debug("loading remote users");
                    List<LDAPUserWithAttributes> ldapUsers = activeDirectory.searchUsers(QueryBuilder
                            .queryFor(LDAPUserWithAttributes.class, EntityDescriptor.user())
                            .returningAtMost(EntityQuery.ALL_RESULTS));
                    log.info("found [ " + ldapUsers.size() + " ] remote users in [ " + (System.currentTimeMillis() - start) + "ms ]");
                    return ldapUsers;
                }
            });
            groupListFuture = queryExecutor.submit(new Callable<List<LDAPGroupWithAttributes>>()
            {
                public List<LDAPGroupWithAttributes> call() throws Exception
                {
                    long start = System.currentTimeMillis();
                    log.debug("loading remote groups");
                    List<LDAPGroupWithAttributes> ldapGroups = activeDirectory.searchGroups(QueryBuilder
                            .queryFor(LDAPGroupWithAttributes.class, EntityDescriptor.group(GroupType.GROUP))
                            .returningAtMost(EntityQuery.ALL_RESULTS));
                    log.info("found [ " + ldapGroups.size() + " ] remote groups in [ " + (System.currentTimeMillis() - start) + "ms ]");
                    return ldapGroups;
                }
            });

            // Find the latest USN-Changed value for the AD server
            long currentHighestCommittedUSN = activeDirectory.fetchHighestCommittedUSN();
            // Do standard synchroniseAll
            super.synchroniseAll(directoryCache);
            // Remember the USN-Changed value that we were on when we started  (this ensures we don't miss anything, but we may get duplicates)
            this.highestCommittedUSN = currentHighestCommittedUSN;
        }
        finally
        {
            queryExecutor.shutdown();

            userListFuture = null;
            groupListFuture = null;
        }
    }

    @Override
    protected void synchroniseAllUsers(DirectoryCache directoryCache) throws OperationFailedException
    {
        userMap.clear();

        Date syncStartDate = new Date();

        try
        {
            List<LDAPUserWithAttributes> ldapUsers = userListFuture.get();

            // update the user map
            for (LDAPUserWithAttributes ldapUser : ldapUsers)
            {
                userMap.put(ldapUser);
            }
            //the order of this operations is relevant for rename to work properly
            directoryCache.deleteCachedUsersNotIn(ldapUsers, syncStartDate);
            directoryCache.addOrUpdateCachedUsers(ldapUsers, syncStartDate);
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
            throw new OperationFailedException("background query interrupted", e);
        }
        catch (ExecutionException e)
        {
            throw new OperationFailedException(e);
        }
    }

    @Override
    protected List<? extends Group> synchroniseAllGroups(DirectoryCache directoryCache) throws OperationFailedException
    {
        groupMap.clear();

        Date syncStartDate = new Date();

        try
        {
            List<LDAPGroupWithAttributes> ldapGroups;
            ldapGroups = groupListFuture.get();

            //CWD-2504: We filter out the duplicate groups to stop the runtime exception that will occur during membership
            // synchronization. There is no point registering a group we are not going to use.
            ldapGroups = Collections.unmodifiableList(filterOutDuplicateGroups(ldapGroups));

            for (LDAPGroupWithAttributes ldapGroup : ldapGroups)
            {
                groupMap.put(ldapGroup);
            }

            directoryCache.addOrUpdateCachedGroups(ldapGroups, syncStartDate);
            directoryCache.deleteCachedGroupsNotIn(GroupType.GROUP, ldapGroups, syncStartDate);
            return ldapGroups;
        }
        catch (InterruptedException e)
        {
            Thread.currentThread().interrupt();
            throw new OperationFailedException("background query interrupted", e);
        }
        catch (ExecutionException e)
        {
            throw new OperationFailedException(e);
        }
    }

    private void synchroniseUserChanges(DirectoryCache directoryCache) throws OperationFailedException
    {
        long start = System.currentTimeMillis();
        log.debug("loading changed remote users");
        List<LDAPUserWithAttributes> updatedUsers = activeDirectory.findAddedOrUpdatedUsersSince(highestCommittedUSN);
        List<Tombstone> tombstones = activeDirectory.findUserTombstonesSince(highestCommittedUSN);
        log.info(new StringBuilder("found [ ").append(updatedUsers.size() + tombstones.size()).append(" ] changed remote users in [ ").append(System.currentTimeMillis() - start).append("ms ]").toString());

        final Set<String> updatedUserNames = Sets.newHashSet();
        for (LDAPUserWithAttributes user : updatedUsers)
        {
            userMap.put(user);
            updatedUserNames.add(user.getName());
        }

        // calculate removed principals
        start = System.currentTimeMillis();
        final Set<String> usernames = Sets.newHashSet();
        for (Tombstone tombstone : tombstones)
        {
            final String username = userMap.getByGuid(tombstone.getObjectGUID());
            //user can't be updated and deleted at the same time, we need to swich to remove
            //users using only a ObjectGUID, however for now this workaround should be good enough
            if (username != null && !updatedUserNames.contains(username))
            {
                usernames.add(username);
            }
        }
        log.info(new StringBuilder("scanned and compared [ ").append(tombstones.size()).append(" ] users for delete in DB cache in [ ").append(System.currentTimeMillis() - start).append("ms ]").toString());

        directoryCache.deleteCachedUsers(usernames);

        // Send a null sync date - we want to force this change else we may miss it.
        // If we put a stale value in then it will be fixed on next refresh.
        directoryCache.addOrUpdateCachedUsers(updatedUsers, null);
    }

    private void synchroniseGroupChanges(DirectoryCache directoryCache) throws OperationFailedException
    {
        long start = System.currentTimeMillis();
        log.debug("loading changed remote groups");
        //CWD-2504: We filter out the duplicate groups to stop the runtime exception that will occur during membership
        // synchronization.
        List<LDAPGroupWithAttributes> updatedGroups = filterOutDuplicateGroups(activeDirectory.findAddedOrUpdatedGroupsSince(highestCommittedUSN));
        List<Tombstone> tombstones = activeDirectory.findGroupTombstonesSince(highestCommittedUSN);
        log.info(new StringBuilder("found [ ").append(updatedGroups.size() + tombstones.size()).append(" ] changed remote groups in [ ").append(System.currentTimeMillis() - start).append("ms ]").toString());

        for (LDAPGroupWithAttributes group : updatedGroups)
        {
            groupMap.put(group);
        }

        // Send a null sync date - we want to force this change else we may miss it.
        // If we put a stale value in then it will be fixed on next refresh.
        directoryCache.addOrUpdateCachedGroups(updatedGroups, null);

        // Now update the memberships for the changed groups
        // (After new Groups are added because we may have nested group memberships)
        synchroniseMemberships(updatedGroups, directoryCache);

        // calculate removed groups
        start = System.currentTimeMillis();
        Set<String> groupnames = new HashSet<String>();
        for (Tombstone tombstone : tombstones)
        {
            String groupName = groupMap.getByGuid(tombstone.getObjectGUID());
            if (groupName != null)
            {
                groupnames.add(groupName);
            }
        }
        log.info(new StringBuilder("scanned and compared [ ").append(tombstones.size()).append(" ] groups for delete in DB cache in [ ").append(System.currentTimeMillis() - start).append("ms ]").toString());

        directoryCache.deleteCachedGroups(groupnames);
    }

    @Override
    Iterable<Membership> getMemberships(Iterable<String> names) throws OperationFailedException
    {
        try
        {
            Map<LdapName, String> users = userMap.toLdapNameKeyedMap();
            Map<LdapName, String> groups = groupMap.toLdapNameKeyedMap();

            return new RFC4519DirectoryMembershipsIterable(activeDirectory, users, groups, ImmutableSet.copyOf(names));
        }
        catch (InvalidNameException e)
        {
            throw new OperationFailedException("Failed to get directory memberships due to invalid DN", e);
        }
    }
}
