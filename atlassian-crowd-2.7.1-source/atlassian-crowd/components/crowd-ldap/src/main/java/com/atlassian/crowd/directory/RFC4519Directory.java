package com.atlassian.crowd.directory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.naming.InvalidNameException;
import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.ldap.LdapName;

import com.atlassian.crowd.directory.ldap.mapper.ContextMapperWithRequiredAttributes;
import com.atlassian.crowd.directory.ldap.mapper.attribute.AttributeMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.group.RFC4519MemberDnMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.user.MemberOfOverlayMapper;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidMembershipException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.DirectoryEntity;
import com.atlassian.crowd.model.LDAPDirectoryEntity;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.LDAPGroupWithAttributes;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.model.user.LDAPUserWithAttributes;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.ldap.LDAPQuery;
import com.atlassian.crowd.search.ldap.LDAPQueryTranslater;
import com.atlassian.crowd.search.ldap.NullResultException;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.search.util.SearchResultsUtil;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.event.api.EventPublisher;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.AttributeInUseException;
import org.springframework.ldap.NameAlreadyBoundException;
import org.springframework.ldap.OperationNotSupportedException;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.HardcodedFilter;

/**
 * Read-write, nesting-aware implementation of RFC4519 user-group membership
 * interactions.
 * <p/>
 * A user is a member of a group if either:
 * - the DN of user is present in the collection of member attribute values of the group
 * - the user has a memberOf attribute which contains the DN of the group (must be enabled via LDAPPropertiesMapper)
 *
 * @see com.atlassian.crowd.directory.ldap.mapper.attribute.RFC2307GidNumberMapper
 * @see com.atlassian.crowd.directory.ldap.mapper.attribute.group.RFC2307MemberUidMapper
 */
public abstract class RFC4519Directory extends SpringLDAPConnector
{
    private static final Logger logger = LoggerFactory.getLogger(RFC4519Directory.class);

    public RFC4519Directory(LDAPQueryTranslater ldapQueryTranslater, EventPublisher eventPublisher, InstanceFactory instanceFactory)
    {
        super(ldapQueryTranslater, eventPublisher, instanceFactory);
    }

    @Override
    protected List<AttributeMapper> getCustomGroupAttributeMappers()
    {
        Builder<AttributeMapper> builder = ImmutableList.<AttributeMapper>builder();
        builder.addAll(super.getCustomGroupAttributeMappers());
        builder.addAll(getMemberDnMappers());

        return builder.build();
    }

    protected List<AttributeMapper> getMemberDnMappers()
    {
        return Collections.<AttributeMapper>singletonList(new RFC4519MemberDnMapper(ldapPropertiesMapper.getGroupMemberAttribute(),
                                                                                    ldapPropertiesMapper.isRelaxedDnStandardisation()));
    }

    @Override
    protected List<AttributeMapper> getCustomUserAttributeMappers()
    {
        Builder<AttributeMapper> builder = ImmutableList.<AttributeMapper>builder();
        builder.addAll(super.getCustomUserAttributeMappers());
        if (ldapPropertiesMapper.isUsingUserMembershipAttributeForGroupMembership())
        {
            builder.add(new MemberOfOverlayMapper(ldapPropertiesMapper.getUserGroupMembershipsAttribute(), ldapPropertiesMapper.isRelaxedDnStandardisation()));
        }

        return builder.build();
    }

    private static Set<String> getMemberDNs(LDAPGroupWithAttributes group)
    {
        return group.getValues(RFC4519MemberDnMapper.ATTRIBUTE_KEY);
    }

    private static Set<String> getMemberOfs(LDAPUserWithAttributes user)
    {
        return user.getValues(MemberOfOverlayMapper.ATTRIBUTE_KEY);
    }

    protected boolean isDnDirectGroupMember(String memberDN, LDAPGroupWithAttributes parentGroup)
    {
        boolean isMember = false;

        Set<String> memberDNs = getMemberDNs(parentGroup);
        if (memberDNs != null)
        {
            Set<String> normal = new HashSet<String>();
            for (String dn : memberDNs)
            {
                normal.add(standardiseDN(dn));
            }
            isMember = normal.contains(standardiseDN(memberDN));
        }

        return isMember;
    }

    @Override
    public boolean isUserDirectGroupMember(final String username, final String groupName)
            throws OperationFailedException
    {
        Validate.notEmpty(username, "username argument cannot be null or empty");
        Validate.notEmpty(groupName, "groupName argument cannot be null or empty");

        try
        {
            LDAPGroupWithAttributes group = findGroupByName(groupName);
            LDAPUserWithAttributes user = findUserByName(username);

            return isDnDirectGroupMember(user.getDn(), group);
        }
        catch (UserNotFoundException e)
        {
            return false;
        }
        catch (GroupNotFoundException e)
        {
            return false;
        }
    }

    @Override
    public boolean isGroupDirectGroupMember(final String childGroup, final String parentGroup)
            throws OperationFailedException
    {
        Validate.notEmpty(childGroup, "childGroup argument cannot be null or empty");
        Validate.notEmpty(parentGroup, "parentGroup argument cannot be null or empty");

        try
        {
            LDAPGroupWithAttributes parent = findGroupByName(parentGroup);
            LDAPGroupWithAttributes child = findGroupByName(childGroup);

            return isDnDirectGroupMember(child.getDn(), parent);
        }
        catch (GroupNotFoundException e)
        {
            return false;
        }
    }

    protected void addDnToGroup(final String dn, final LDAPGroupWithAttributes group) throws OperationFailedException
    {
        try
        {
            ModificationItem mods[] = new ModificationItem[1];
            mods[0] = new ModificationItem(DirContext.ADD_ATTRIBUTE, new BasicAttribute(ldapPropertiesMapper.getGroupMemberAttribute(), dn));

            ldapTemplate.modifyAttributes(asLdapGroupName(group.getDn(), group.getName()), mods);
        }
        catch (AttributeInUseException e)   // ApacheDS, OpenLDAP, etc
        {
            // already member, do nothing
        }
        catch (NameAlreadyBoundException e)   // Active Directory
        {
            // already member, do nothing
        }
        catch (GroupNotFoundException e)
        {
            logger.error("Could not modify members of group with DN: " + dn, e);
        }
        catch (org.springframework.ldap.NamingException e)
        {
            throw new OperationFailedException(e);
        }
    }

    @Override
    public void addUserToGroup(final String username, final String groupName)
        throws GroupNotFoundException, OperationFailedException, UserNotFoundException, MembershipAlreadyExistsException
    {
        Validate.notEmpty(username, "username argument cannot be null or empty");
        Validate.notEmpty(groupName, "groupName argument cannot be null or empty");

        LDAPGroupWithAttributes group = findGroupByName(groupName);
        LDAPUserWithAttributes user = findUserByName(username);

        if (isDnDirectGroupMember(user.getDn(), group))
        {
            throw new MembershipAlreadyExistsException(getDirectoryId(), username, groupName);
        }

        addDnToGroup(user.getDn(), group);
    }

    @Override
    public void addGroupToGroup(final String childGroup, final String parentGroup)
        throws GroupNotFoundException, InvalidMembershipException, OperationFailedException,
               MembershipAlreadyExistsException
    {
        Validate.notEmpty(childGroup, "childGroup argument cannot be null or empty");
        Validate.notEmpty(parentGroup, "parentGroup argument cannot be null or empty");

        LDAPGroupWithAttributes parent = findGroupByName(parentGroup);
        LDAPGroupWithAttributes child = findGroupByName(childGroup);

        if (parent.getType() != child.getType())
        {
            throw new InvalidMembershipException("Cannot add group of type " + child.getType().name() + " to group of type " + parent.getType().name());
        }

        if (isDnDirectGroupMember(child.getDn(), parent))
        {
            throw new MembershipAlreadyExistsException(getDirectoryId(), childGroup, parentGroup);
        }

        addDnToGroup(child.getDn(), parent);
    }

    protected void removeDnFromGroup(final String dn, LDAPGroupWithAttributes group) throws OperationFailedException
    {
        try
        {
            ModificationItem mods[] = new ModificationItem[1];

            mods[0] = new ModificationItem(DirContext.REMOVE_ATTRIBUTE, new BasicAttribute(ldapPropertiesMapper.getGroupMemberAttribute(), dn));

            ldapTemplate.modifyAttributes(asLdapGroupName(group.getDn(), group.getName()), mods);
        }
        catch (OperationNotSupportedException e)
        {
            // AD: Thrown when the user is not a member of the group.
            // silently fail
        }
        catch (GroupNotFoundException e)
        {
            logger.error("Could not modify memers of group with DN: " + dn, e);
        }
        catch (org.springframework.ldap.NamingException e)
        {
            throw new OperationFailedException(e);
        }
    }

    @Override
    public void removeUserFromGroup(final String username, final String groupName)
            throws UserNotFoundException, GroupNotFoundException, MembershipNotFoundException, OperationFailedException
    {
        Validate.notEmpty(username, "username argument cannot be null or empty");
        Validate.notEmpty(groupName, "groupName argument cannot be null or empty");

        LDAPGroupWithAttributes group = findGroupByName(groupName);
        LDAPUserWithAttributes user = findUserByName(username);

        if (!isDnDirectGroupMember(user.getDn(), group))
        {
            throw new MembershipNotFoundException(username, groupName);
        }

        removeDnFromGroup(user.getDn(), group);
    }

    @Override
    public void removeGroupFromGroup(final String childGroup, final String parentGroup)
            throws GroupNotFoundException, MembershipNotFoundException, InvalidMembershipException, OperationFailedException
    {
        Validate.notEmpty(childGroup, "childGroup argument cannot be null or empty");
        Validate.notEmpty(parentGroup, "parentGroup argument cannot be null or empty");

        LDAPGroupWithAttributes parent = findGroupByName(parentGroup);
        LDAPGroupWithAttributes child = findGroupByName(childGroup);

        if (!isDnDirectGroupMember(child.getDn(), parent))
        {
            throw new MembershipNotFoundException(childGroup, parentGroup);
        }

        if (parent.getType() != child.getType())
        {
            throw new InvalidMembershipException("Cannot remove group of type " + child.getType().name() + " from group of type " + parent.getType().name());
        }

        removeDnFromGroup(child.getDn(), parent);
    }

    @Override
    public Iterable<Membership> getMemberships() throws OperationFailedException
    {
        Map<LdapName, String> users = getEntityNamesAsMap(searchDN.getUser(), EntityDescriptor.user(), ldapPropertiesMapper.getUserNameAttribute());
        Map<LdapName, String> groups = getEntityNamesAsMap(searchDN.getGroup(), EntityDescriptor.group(GroupType.GROUP), ldapPropertiesMapper.getGroupNameAttribute());

        return new RFC4519DirectoryMembershipsIterable(this, users, groups);
    }

    /**
     * @param entity an LDAP entity
     * @return the LdapName of the entity
     * @throws OperationFailedException if the entity DN cannot be parsed
     */
    protected static LdapName getLdapName(LDAPDirectoryEntity entity) throws OperationFailedException
    {
        try
        {
            return new LdapName(entity.getDn());
        }
        catch (InvalidNameException e)
        {
            throw new OperationFailedException("Unable to parse DN for entity", e);
        }
    }

    private Map<LdapName, String> getEntityNamesAsMap(LdapName baseDn, EntityDescriptor desc, String nameAttribute)
            throws OperationFailedException
    {
        LDAPQuery ldapQueryGroups;

        EntityQuery<String> allGroups = QueryBuilder.queryFor(String.class, desc).returningAtMost(EntityQuery.ALL_RESULTS);

        try
        {
            ldapQueryGroups = ldapQueryTranslater.asLDAPFilter(allGroups, ldapPropertiesMapper);
        }
        catch (NullResultException e)
        {
            return Collections.emptyMap();
        }

        String filter = ldapQueryGroups.encode();

        if (logger.isDebugEnabled())
        {
            logger.debug("Performing " + desc.getEntityType() + " search: baseDN = " + baseDn + " - filter = " + filter);
        }

        ContextMapperWithRequiredAttributes<NamedLdapEntity> mapper = NamedLdapEntity.mapperFromAttribute(nameAttribute);

        return asMap(searchEntities(baseDn, filter, mapper, allGroups.getStartIndex(), allGroups.getMaxResults()));
    }

    static Map<LdapName, String> asMap(Iterable<NamedLdapEntity> entities)
    {
        Map<LdapName, String> named = new HashMap<LdapName, String>();

        for (NamedLdapEntity entity : entities)
        {
            named.put(entity.getDn(), entity.getName());
        }

        return named;
    }

    @Override
    protected <T> Iterable<T> searchGroupRelationshipsWithGroupTypeSpecified(final MembershipQuery<T> query)
            throws OperationFailedException
    {
        Iterable<? extends DirectoryEntity> relations;

        if (query.isFindChildren())
        {
            if (query.getEntityToMatch().getEntityType() == Entity.GROUP)
            {
                if (query.getEntityToReturn().getEntityType() == Entity.USER)
                {
                    // query is to find USER members of GROUP
                    if (ldapPropertiesMapper.isUsingUserMembershipAttribute())
                    {
                        relations = findUserMembersOfGroupViaMemberOf(query.getEntityNameToMatch(), query.getEntityToMatch().getGroupType(), query.getStartIndex(), query.getMaxResults());
                    }
                    else
                    {
                        relations = findUserMembersOfGroupViaMemberDN(query.getEntityNameToMatch(), query.getEntityToMatch().getGroupType(), query.getStartIndex(), query.getMaxResults());
                    }
                }
                else if (query.getEntityToReturn().getEntityType() == Entity.GROUP)
                {
                    // query is to find GROUP members of GROUP (only if nesting is enabled)
                    if (ldapPropertiesMapper.isNestedGroupsDisabled())
                    {
                        relations = Collections.emptyList();
                    }
                    else
                    {
                        relations = findGroupMembersOfGroupViaMemberDN(query.getEntityNameToMatch(), query.getEntityToMatch().getGroupType(), query.getStartIndex(), query.getMaxResults());
                    }
                }
                else
                {
                    throw new IllegalArgumentException("You can only find the GROUP or USER members of a GROUP");
                }
            }
            else
            {
                throw new IllegalArgumentException("You can only find the GROUP or USER members of a GROUP");
            }
        }
        else
        {
            // find memberships
            if (query.getEntityToReturn().getEntityType() == Entity.GROUP)
            {
                if (query.getEntityToReturn().getGroupType() == GroupType.GROUP)
                {
                    // Okay
                }
                else if (query.getEntityToReturn().getGroupType() == GroupType.LEGACY_ROLE)
                {
                    return Collections.emptyList();
                }
                else
                {
                    throw new IllegalArgumentException("Cannot find group memberships of entity via member DN for GroupType: " + query.getEntityToReturn().getGroupType());
                }

                if (query.getReturnType() == String.class) // as name
                {
                    return toGenericIterable(findGroupMembershipNames((MembershipQuery<String>) query));
                }
                else
                {
                    relations = findGroupMemberships((MembershipQuery<LDAPGroupWithAttributes>) query);
                }
            }
            else
            {
                throw new IllegalArgumentException("You can only find the GROUP memberships of USER or GROUP");
            }
        }

        if (query.getReturnType() == String.class) // as name
        {
            return toGenericIterable(SearchResultsUtil.convertEntitiesToNames(relations));
        }
        else
        {
            return toGenericIterable(relations);
        }
    }

    protected List<? extends LDAPGroupWithAttributes> findGroupMemberships(final MembershipQuery<? extends LDAPGroupWithAttributes> query) throws OperationFailedException
    {
        if (query.getEntityToMatch().getEntityType() == Entity.USER)
        {
            // query is to find GROUP memberships of USER
            if (ldapPropertiesMapper.isUsingUserMembershipAttributeForGroupMembership())
            {
                return findGroupMembershipsOfUserViaMemberOf(query.getEntityNameToMatch(), query.getStartIndex(), query.getMaxResults());
            }
            else
            {
                return findGroupMembershipsOfUserViaMemberDN(query.getEntityNameToMatch(), query.getStartIndex(), query.getMaxResults());
            }

        }
        else if (query.getEntityToMatch().getEntityType() == Entity.GROUP)
        {
            // query is to find GROUP memberships of GROUP (only if nesting is enabled)
            if (ldapPropertiesMapper.isNestedGroupsDisabled())
            {
                return Collections.emptyList();
            }
            else
            {
                return findGroupMembershipsOfGroupViaMemberDN(query.getEntityNameToMatch(), query.getStartIndex(), query.getMaxResults());
            }
        }
        else
        {
            throw new IllegalArgumentException("You can only find the GROUP memberships of USER or GROUP");
        }
    }

    protected Iterable<String> findGroupMembershipNames(final MembershipQuery<String> query) throws OperationFailedException
    {
        if (query.getEntityToMatch().getEntityType() == Entity.USER)
        {
            // query is to find GROUP memberships of USER
            if (ldapPropertiesMapper.isUsingUserMembershipAttributeForGroupMembership())
            {
                return findGroupMembershipNamesOfUserViaMemberOf(query.getEntityNameToMatch(), query.getStartIndex(), query.getMaxResults());
            }
            else
            {
                return findGroupMembershipNamesOfUserViaMemberDN(query.getEntityNameToMatch(), query.getStartIndex(), query.getMaxResults());
            }
        }
        else if (query.getEntityToMatch().getEntityType() == Entity.GROUP)
        {
            // query is to find GROUP memberships of GROUP (only if nesting is enabled)
            if (ldapPropertiesMapper.isNestedGroupsDisabled())
            {
                return Collections.emptyList();
            }
            else
            {
                return findGroupMembershipNamesOfGroupViaMemberDN(query.getEntityNameToMatch(), query.getStartIndex(), query.getMaxResults());
            }
        }
        else
        {
            throw new IllegalArgumentException("You can only find the GROUP memberships of USER or GROUP");
        }
    }

    private static interface LookupByDn<T>
    {
        T lookup(LdapName dn) throws OperationFailedException, UserNotFoundException, GroupNotFoundException;
    }

    private final LookupByDn<LDAPGroupWithAttributes> lookupGroupByDn = new LookupByDn<LDAPGroupWithAttributes>()
    {
        @Override
        public LDAPGroupWithAttributes lookup(LdapName groupDN) throws OperationFailedException, UserNotFoundException, GroupNotFoundException
        {
            return findEntityByDN(groupDN.toString(), LDAPGroupWithAttributes.class);
        }
    };

    private final LookupByDn<String> lookupGroupNameByDn = new LookupByDn<String>()
    {
        @Override
        public String lookup(LdapName groupDN) throws OperationFailedException, UserNotFoundException, GroupNotFoundException
        {
            return ldapTemplate.lookup(groupDN, NamedLdapEntity.mapperFromAttribute(ldapPropertiesMapper.getGroupNameAttribute())).getName();
        }
    };

    private List<LDAPGroupWithAttributes> findGroupMembershipsOfUserViaMemberOf(final String username, int startIndex, int maxResults)
            throws OperationFailedException
    {
        return findGroupMembershipsOfUserViaMemberOf(username, startIndex, maxResults, lookupGroupByDn);
    }

    private List<String> findGroupMembershipNamesOfUserViaMemberOf(final String username, int startIndex, int maxResults)
            throws OperationFailedException
    {
        return findGroupMembershipsOfUserViaMemberOf(username, startIndex, maxResults, lookupGroupNameByDn);
    }

    static int totalResultsSize(int startIndex, int maxResults)
    {
        if (maxResults == EntityQuery.ALL_RESULTS)
        {
            return EntityQuery.ALL_RESULTS;
        }
        else
        {
            int totalResults = startIndex + maxResults;
            if (totalResults < 0)
            {
                return EntityQuery.ALL_RESULTS;
            }
            else
            {
                return totalResults;
            }
        }
    }

    protected <T> List<T> findGroupMembershipsOfUserViaMemberOf(final String username, int startIndex, int maxResults,
            LookupByDn<T> mapper)
            throws OperationFailedException
    {
        List<T> results;

        try
        {
            LDAPUserWithAttributes user = findUserByName(username);

            Set<String> memberOfs = getMemberOfs(user);
            if (memberOfs != null)
            {
                results = new ArrayList<T>();

                int totalResultSize = totalResultsSize(startIndex, maxResults);

                for (String groupDN : memberOfs)
                {
                    try
                    {
                        T entity = mapper.lookup(new LdapName(groupDN));
                        results.add(entity);
                    }
                    catch (GroupNotFoundException e)
                    {
                        // entity does not exist at specified DN (or does not match object filter/baseDN)
                    }
                    catch (NamingException e)
                    {
                        logger.info("Invalid group DN {}", groupDN);
                    }
                    catch (IllegalArgumentException e)
                    {
                        logger.info("Invalid group DN {}", groupDN);
                    }

                    // if we have enough results then break out
                    if (totalResultSize != EntityQuery.ALL_RESULTS && results.size() >= totalResultSize)
                    {
                        break;
                    }
                }

                results = SearchResultsUtil.constrainResults(results, startIndex, maxResults);
            }
            else
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("User with name <" + username + "> does not have any memberOf values and therefore has no memberships");
                }
                results = Collections.emptyList();
            }
        }
        catch (UserNotFoundException e)
        {
            // user not found
            if (logger.isDebugEnabled())
            {
                logger.debug("User with name <" + username + "> does not exist and therefore has no memberships");
            }
            results = Collections.emptyList();
        }

        return results;
    }

    private List<LDAPGroupWithAttributes> findGroupMembershipsOfUserViaMemberDN(final String username, int startIndex, int maxResults)
            throws OperationFailedException
    {
        try
        {
            LDAPUserWithAttributes user = findUserByName(username);
            return findGroupMembershipsOfEntityViaMemberDN(getLdapName(user), startIndex, maxResults);
        }
        catch (UserNotFoundException e)
        {
            return Collections.emptyList();
        }
        catch (IllegalArgumentException e)
        {
            return Collections.emptyList();
        }
    }

    private List<LDAPGroupWithAttributes> findGroupMembershipsOfGroupViaMemberDN(final String groupName, int startIndex, int maxResults)
            throws OperationFailedException
    {
        try
        {
            LDAPGroupWithAttributes group = findGroupByNameAndType(groupName, GroupType.GROUP);
            return findGroupMembershipsOfEntityViaMemberDN(getLdapName(group), startIndex, maxResults);
        }
        catch (GroupNotFoundException e)
        {
            return Collections.emptyList();
        }
    }

    private Iterable<String> findGroupMembershipNamesOfUserViaMemberDN(final String username, int startIndex, int maxResults)
            throws OperationFailedException
    {
        try
        {
            LDAPUserWithAttributes user = findUserByName(username);
            return findGroupMembershipNamesOfEntityViaMemberDN(getLdapName(user), startIndex, maxResults);
        }
        catch (UserNotFoundException e)
        {
            return Collections.emptyList();
        }
        catch (IllegalArgumentException e)
        {
            return Collections.emptyList();
        }
    }

    private Iterable<String> findGroupMembershipNamesOfGroupViaMemberDN(final String groupName, int startIndex, int maxResults)
            throws OperationFailedException
    {
        try
        {
            final LdapName groupDn = findGroupDnByName(groupName);
            return findGroupMembershipNamesOfEntityViaMemberDN(groupDn, startIndex, maxResults);
        }
        catch (GroupNotFoundException e)
        {
            return Collections.emptyList();
        }
    }

    private List<LDAPGroupWithAttributes> findGroupMembershipsOfEntityViaMemberDN(final LdapName dn, int startIndex, int maxResults)
            throws OperationFailedException
    {
        return findGroupMembershipsOfEntityViaMemberDN(dn, startIndex, maxResults, getGroupContextMapper(GroupType.GROUP));
    }

    private Iterable<String> findGroupMembershipNamesOfEntityViaMemberDN(final LdapName dn, int startIndex, int maxResults)
            throws OperationFailedException
    {
        ContextMapperWithRequiredAttributes<NamedLdapEntity> mapper =
            NamedLdapEntity.mapperFromAttribute(ldapPropertiesMapper.getGroupNameAttribute());

        return NamedLdapEntity.namesOf(this.<NamedLdapEntity>findGroupMembershipsOfEntityViaMemberDN(dn, startIndex,
                                                                                                     maxResults,
                                                                                                     mapper));
    }

    private <T> List<T> findGroupMembershipsOfEntityViaMemberDN(final LdapName dn, int startIndex, int maxResults,
            ContextMapperWithRequiredAttributes<T> contextMapper)
            throws OperationFailedException
    {
        AndFilter filter = getGroupsByGroupMemberAttributeFilter(dn);
        Name baseDN = searchDN.getGroup();

        if (logger.isDebugEnabled())
        {
            logger.debug("Executing search at DN: <" + searchDN.getGroup() + "> with filter: <" + filter.encode() + ">");
        }

        return searchEntities(baseDN, filter.encode(), contextMapper, startIndex, maxResults);
    }

    private List<LDAPGroupWithAttributes> findGroupMembersOfGroupViaMemberDN(final String groupName, GroupType groupType, int startIndex, int maxResults)
            throws OperationFailedException
    {
        return findMembersOfGroupViaMemberDN(groupName, groupType, LDAPGroupWithAttributes.class, startIndex, maxResults);
    }

    protected List<LDAPUserWithAttributes> findUserMembersOfGroupViaMemberDN(final String groupName, GroupType groupType, int startIndex, int maxResults)
            throws OperationFailedException
    {
        return findMembersOfGroupViaMemberDN(groupName, groupType, LDAPUserWithAttributes.class, startIndex, maxResults);
    }

    /*
     * Executes a user search to find users with memberOf=group dn.
     */
    protected Iterable<LDAPUserWithAttributes> findUserMembersOfGroupViaMemberOf(final String groupName, GroupType groupType, int startIndex, int maxResults)
            throws OperationFailedException
    {
        Iterable<LDAPUserWithAttributes> results;

        try
        {
            LDAPGroupWithAttributes group = findGroupWithAttributesByName(groupName);
            if (group.getType() == groupType)
            {
                AndFilter filter = getUsersByUserGroupMembershipAttributeFilter(getLdapName(group));

                if (logger.isDebugEnabled())
                {
                    logger.debug("Executing search at DN: <" + searchDN.getUser() + "> with filter: <" + filter.encode() + ">");
                }

                results = toGenericIterable(searchEntities(searchDN.getUser(), filter.encode(), getUserContextMapper(), startIndex, maxResults));
            }
            else
            {
                // group exists but is not of desired type
                if (logger.isDebugEnabled())
                {
                    logger.debug("Group with name <" + groupName + "> does exist but is of GroupType <" + group.getType() + "> and not <" + groupType + ">");
                }
                results = Collections.emptyList();
            }
        }
        catch (GroupNotFoundException e)
        {
            // group does not exist
            if (logger.isDebugEnabled())
            {
                logger.debug("Group with name <" + groupName + "> does not exist and therefore has no members");
            }
            results = Collections.emptyList();
        }

        return results;
    }

    /**
     * Finds the group and goes through each memberDN to find the user/group member by executing
     * successive lookups that are required to match both the memberBaseDN (suffix match) and
     * the member filter (during the LDAP lookup).
     */
    private <T extends LDAPDirectoryEntity> List<T> findMembersOfGroupViaMemberDN(final String groupName, GroupType groupType, Class<T> memberClass, int startIndex, int maxResults)
            throws OperationFailedException
    {
        List<T> results;

        try
        {
            LDAPGroupWithAttributes group = findGroupByNameAndType(groupName, groupType);

            Set<String> memberDNs = getMemberDNs(group);
            if (memberDNs != null)
            {
                results = new ArrayList<T>();

                int totalResultSize = totalResultsSize(startIndex, maxResults);

                for (String memberDN : memberDNs)
                {
                    try
                    {
                        T entity = findEntityByDN(memberDN, memberClass);

                        if (entity instanceof LDAPGroupWithAttributes)
                        {
                            // only add results of the requested group type
                            if (((LDAPGroupWithAttributes) entity).getType() == groupType)
                            {
                                results.add(entity);
                            }
                        }
                        else
                        {
                            // all users can get added
                            results.add(entity);
                        }

                    }
                    catch (UserNotFoundException e)
                    {
                        // entity does not exist at specified DN (or does not match object filter/baseDN)
                    }
                    catch (GroupNotFoundException e)
                    {
                        // entity does not exist at specified DN (or does not match object filter/baseDN)
                    }

                    // if we have enough results then break out
                    if (totalResultSize != EntityQuery.ALL_RESULTS && results.size() >= totalResultSize)
                    {
                        break;
                    }
                }

                results = SearchResultsUtil.constrainResults(results, startIndex, maxResults);
            }
            else
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("Group with name <" + groupName + "> does not have any memberDNs and therefore has no members");
                }
                results = Collections.emptyList();
            }
        }
        catch (GroupNotFoundException e)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Group with name <" + groupName + "> does not exist and therefore has no members");
            }
            results = Collections.emptyList();
        }

        return results;
    }

    /**
     * Converts an Iterable to a generic Iterable.
     */
    @SuppressWarnings("unchecked")
    protected static <T> Iterable<T> toGenericIterable(Iterable list) {
        return (Iterable<T>) list;
    }

    /**
     * This method is not part of {@link RemoteDirectory}'s contract. It is introduced by {@link RFC4519Directory} to
     * support {@link RFC4519DirectoryMembershipsIterable}.
     *
     * @param groupDn LDAP name of a group
     * @return the LDAP names of the direct members (users and groups) of the given group
     * @throws OperationFailedException if the operation fails for any reason
     */
    public Iterable<LdapName> findDirectMembersOfGroup(LdapName groupDn) throws OperationFailedException
    {
        Iterable<LdapName> childDns;

        /* If we're using 'memberOf', go ahead and use it to fetch users in this group */
        if (ldapPropertiesMapper.isUsingUserMembershipAttribute())
        {
            AndFilter filter = getUsersByUserGroupMembershipAttributeFilter(groupDn);

            childDns = searchEntities(searchDN.getUser(), filter.encode(), DN_MAPPER, 0, EntityQuery.ALL_RESULTS);
        }
        else
        {
            childDns = Collections.emptyList();
        }

        Iterable<LdapName> names;

        /* If we're not using 'memberOf', or we have nested groups, fetch those group members with 'member'   */
        if (!ldapPropertiesMapper.isUsingUserMembershipAttribute() || !ldapPropertiesMapper.isNestedGroupsDisabled())
        {
            ContextMapperWithRequiredAttributes<LDAPGroupWithAttributes> mapper = getGroupContextMapper(GroupType.GROUP);

            LDAPGroupWithAttributes group = ldapTemplate.lookup(groupDn, mapper);

            names = toLdapNames(getMemberDNs(postprocessGroups(ImmutableList.of(group)).get(0)));
        }
        else
        {
            names = Collections.emptyList();
        }

        return Iterables.concat(childDns, names);
    }

    /**
     * @param groupDn DN of the parent group
     * @return a filter that matches users that have the user group membership attribute equals to the groupDn
     */
    private AndFilter getUsersByUserGroupMembershipAttributeFilter(LdapName groupDn)
    {
        AndFilter filter = new AndFilter();
        filter.and(new HardcodedFilter(ldapPropertiesMapper.getUserFilter()));
        filter.and(new EqualsFilter(ldapPropertiesMapper.getUserGroupMembershipsAttribute(), groupDn.toString()));
        return filter;
    }

    /**
     * @param groupDn DN of the parent group
     * @return a filter that matches groups that have the group membership attribute equals to the groupDn
     */
    private AndFilter getGroupsByGroupMemberAttributeFilter(LdapName groupDn)
    {
        AndFilter filter = new AndFilter();
        filter.and(new HardcodedFilter(ldapPropertiesMapper.getGroupFilter()));
        filter.and(new EqualsFilter(ldapPropertiesMapper.getGroupMemberAttribute(), groupDn.toString()));
        return filter;
    }

    private LdapName findGroupDnByName(String groupName) throws OperationFailedException, GroupNotFoundException
    {
        EntityQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group())
            .with(Restriction.on(GroupTermKeys.NAME).exactlyMatching(groupName)).returningAtMost(1);

        try
        {
            return Iterables.getOnlyElement(searchGroupObjects(query, DN_MAPPER));
        }
        catch (NoSuchElementException e)
        {
            throw new GroupNotFoundException(groupName);
        }
    }

    Iterable<LdapName> toLdapNames(Iterable<String> names)
    {
        return Iterables.transform(names, TO_LDAP_NAME);
    }

    private static final Function<String, ? extends LdapName> TO_LDAP_NAME = new Function<String, LdapName>()
    {
        @Override
        public LdapName apply(String input)
        {
            try
            {
                return new LdapName(input);
            }
            catch (NamingException e)
            {
                throw new RuntimeException(e);
            }
        }
    };

    static final ContextMapperWithRequiredAttributes<LdapName> DN_MAPPER = new DnMapper();

    private static class DnMapper implements ContextMapperWithRequiredAttributes<LdapName>
    {
        @Override
        public LdapName mapFromContext(Object ctx)
        {
            DirContextAdapter context = (DirContextAdapter) ctx;

            try
            {
                return new LdapName(context.getDn().toString());
            }
            catch (InvalidNameException e)
            {
                throw new RuntimeException(e);
            }
        }

        @Override
        public Set<String> getRequiredLdapAttributes()
        {
            return ImmutableSet.of();
        }
    }
}
