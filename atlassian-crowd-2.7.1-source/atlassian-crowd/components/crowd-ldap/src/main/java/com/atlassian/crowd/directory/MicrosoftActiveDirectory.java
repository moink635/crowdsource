/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.directory;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;

import javax.naming.Name;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.ldap.LdapName;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapperImpl;
import com.atlassian.crowd.directory.ldap.control.DeletedResultsControl;
import com.atlassian.crowd.directory.ldap.mapper.ContextMapperWithRequiredAttributes;
import com.atlassian.crowd.directory.ldap.mapper.TombstoneContextMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.ActiveDirectoryUserContextMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.AttributeMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.ObjectGUIDMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.ObjectSIDMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.PrimaryGroupIdMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.SIDUtils;
import com.atlassian.crowd.directory.ldap.mapper.attribute.USNChangedMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.UserAccountControlMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.UserAccountControlUtil;
import com.atlassian.crowd.directory.ldap.mapper.attribute.group.RFC4519MemberDnMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.group.RFC4519MemberDnRangeOffsetMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.group.RFC4519MemberDnRangedMapper;
import com.atlassian.crowd.directory.ldap.name.GenericConverter;
import com.atlassian.crowd.directory.ldap.util.IncrementalAttributeMapper;
import com.atlassian.crowd.directory.ldap.util.ListAttributeValueProcessor;
import com.atlassian.crowd.directory.ldap.util.RangeOption;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.Tombstone;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplateWithAttributes;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.LDAPGroupWithAttributes;
import com.atlassian.crowd.model.user.LDAPUserWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.ldap.ActiveDirectoryQueryTranslaterImpl;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.PropertyImpl;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.search.util.SearchResultsUtil;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.crowd.util.PasswordHelper;
import com.atlassian.event.api.EventPublisher;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.filter.AndFilter;
import org.springframework.ldap.filter.EqualsFilter;
import org.springframework.ldap.filter.GreaterThanOrEqualsFilter;
import org.springframework.ldap.filter.HardcodedFilter;

import static com.atlassian.crowd.directory.NamedLdapEntity.dnsOf;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Microsoft Active Directory connector.
 */
public class MicrosoftActiveDirectory extends RFC4519Directory
{
    private static final Logger logger = LoggerFactory.getLogger(MicrosoftActiveDirectory.class);
    // how to establish a secure connection with AD
    // http://forum.java.sun.com/thread.jspa?threadID=581425&tstart=50

    // some useful constants from lmaccess.h
    public static final int UF_ACCOUNTDISABLE = 0x0002;
    private static final int UF_PASSWD_NOTREQD = 0x0020;
    private static final int UF_NORMAL_ACCOUNT = 0x0200;
    private static final int UF_PASSWORD_EXPIRED = 0x800000;

    /**
     * Unused constants:
     * UF_PASSWD_CANT_CHANGE = 0x0040;
     * UF_DONT_EXPIRE_PASSWD = 0x10000;
     */

    private static final String AD_USER_ACCOUNT_CONTROL = "userAccountControl";
    private static final String AD_SAM_ACCOUNT_NAME = "samAccountName";
    private static final String AD_PASSWORD_ENCODED = "UTF-16LE";
    private static final String AD_HIGHEST_COMMITTED_USN = "highestCommittedUSN";
    private static final String AD_IS_DELETED = "isDeleted";
    private static final String AD_OBJECT_CLASS = "objectClass";

    private static final String DELETED_OBJECTS_DN_ADDITION = "CN=Deleted Objects";
    private static final String ROOT_DOMAIN_NAMING_CONTEXT = "rootDomainNamingContext";

    private static final String GROUP_TYPE_NAME = "groupType";
    private static final String GROUP_TYPE_VALUE = "2";
    public static final PropertyImpl<String> OBJECT_SID =
        new PropertyImpl<String>(ObjectSIDMapper.ATTRIBUTE_KEY, String.class);

    private final PasswordHelper passwordHelper;

    /**
     * @param passwordHelper password helper, which must not be null
     */
    public MicrosoftActiveDirectory(ActiveDirectoryQueryTranslaterImpl activeDirectoryQueryTranslater,
                                    EventPublisher eventPublisher,
                                    InstanceFactory instanceFactory,
                                    PasswordHelper passwordHelper)
    {
        super(activeDirectoryQueryTranslater, eventPublisher, instanceFactory);
        this.passwordHelper = checkNotNull(passwordHelper);
    }

    public static String getStaticDirectoryType()
    {
        return "Microsoft Active Directory";
    }

    @Override
    public String getDescriptiveName()
    {
        return MicrosoftActiveDirectory.getStaticDirectoryType();
    }

    /*
     * Almost a clone of the overridden method, but makes an additional query to prevent
     * removing a group that is the primary group for some user.
     */
    @Override
    public void removeGroup(String name) throws GroupNotFoundException, OperationFailedException
    {
        Validate.notEmpty(name, "name argument cannot be null or empty");

        LDAPGroupWithAttributes group = findGroupByName(name);

        if (isPrimaryGroupSupportEnabled())
        {
            String primaryGroupRid = SIDUtils.getLastRidFromSid(group.getValue(ObjectSIDMapper.ATTRIBUTE_KEY));
            Iterable<LdapName> users =
                findUserMembersNamesOfGroupViaPrimaryGroupId(primaryGroupRid, 0, 1);
            if (!Iterables.isEmpty(users))
            {
                throw new OperationFailedException("Cannot remove group '" + group.getName()
                       + "' because it is the primary group of some user(s), including '"
                       + Iterables.get(users, 0).toString() + "'");
            }
        }

        // remove the object by dn
        try
        {
            ldapTemplate.unbind(asLdapGroupName(group.getDn(), name));
        }
        catch (org.springframework.ldap.NamingException ex)
        {
            // this may happen if primary group is disabled, or if the user who is a member of the group is
            // not under the BaseDN. The LDAP code (LDAP: error code 68) is too generic to guess if the problem
            // is actually caused by a "primary group constraint"
            throw new OperationFailedException(ex);
        }
    }

    @Override
    public boolean isUserDirectGroupMember(final String username, final String groupName)
        throws OperationFailedException
    {
        Validate.notEmpty(username, "username argument cannot be null or empty");
        Validate.notEmpty(groupName, "groupName argument cannot be null or empty");

        try
        {
            LDAPGroupWithAttributes group = findGroupByName(groupName);
            LDAPUserWithAttributes user = findUserByName(username);

            return isDnDirectGroupMember(user.getDn(), group) || isUserMemberOfPrimaryGroup(user, group);
        }
        catch (UserNotFoundException e)
        {
            return false;
        }
        catch (GroupNotFoundException e)
        {
            return false;
        }
    }

    /*
     * Almost a clone of the implementation in the parent class, but with an additional condition. This method
     * is reimplemented (as opposed to decorated) to re-use the 'group' and 'user' instances and avoid making
     * any extra calls to AD if primary group support is enabled.
     */
    @Override
    public void addUserToGroup(final String username, final String groupName)
        throws GroupNotFoundException, OperationFailedException, UserNotFoundException, MembershipAlreadyExistsException
    {
        Validate.notEmpty(username, "username argument cannot be null or empty");
        Validate.notEmpty(groupName, "groupName argument cannot be null or empty");

        LDAPGroupWithAttributes group = findGroupByName(groupName);
        LDAPUserWithAttributes user = findUserByName(username);

        if (isDnDirectGroupMember(user.getDn(), group) || isUserMemberOfPrimaryGroup(user, group))
        {
            throw new MembershipAlreadyExistsException(getDirectoryId(), username, groupName);
        }

        addDnToGroup(user.getDn(), group);
    }

    /*
     * Almost a clone of the implementation in the parent class, but with an additional condition. This method
     * is reimplemented (as opposed to decorated) to re-use the 'group' and 'user' instances and avoid making
     * any extra calls to AD if primary group support is enabled.
     */
    @Override
    public void removeUserFromGroup(String username, String groupName)
        throws UserNotFoundException, GroupNotFoundException, MembershipNotFoundException, OperationFailedException
    {
        Validate.notEmpty(username, "username argument cannot be null or empty");
        Validate.notEmpty(groupName, "groupName argument cannot be null or empty");

        LDAPGroupWithAttributes group = findGroupByName(groupName);
        LDAPUserWithAttributes user = findUserByName(username);

        if (!isDnDirectGroupMember(user.getDn(), group))
        {
            if (isUserMemberOfPrimaryGroup(user, group))
            {
                throw new OperationFailedException("Cannot remove user '" + user.getName()
                                                   + "' from group '" + group.getName()
                                                   + "' because it is the primary group of the user");
            }
            else
            {
                throw new MembershipNotFoundException(username, groupName);
            }
        }

        removeDnFromGroup(user.getDn(), group);
    }

    /**
     * Retrieves a group name querying by its SID.
     *
     * @param sid the SID of a group, in its String format (i.e., not a binary SID)
     * @return the name of the group which objectID is the given SID, if it exists
     * @throws GroupNotFoundException if no group exists with the given SID under the base DN
     * @throws OperationFailedException if the operation fails for any other reason
     */
    private String findGroupNameBySID(String sid) throws GroupNotFoundException, OperationFailedException
    {
        Validate.notNull(sid, "SID argument cannot be null");

        EntityQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group())
            .with(Restriction.on(OBJECT_SID).exactlyMatching(sid))
            .returningAtMost(1);

        ContextMapperWithRequiredAttributes<NamedLdapEntity> mapper =
            NamedLdapEntity.mapperFromAttribute(ldapPropertiesMapper.getGroupNameAttribute());

        try
        {
            // query is constrained to returnAtMost(1)
            return Iterables.getOnlyElement(searchGroupObjects(query, mapper)).getName();
        }
        catch (NoSuchElementException e)
        {
            throw new GroupNotFoundException("objectId = " + sid);
        }
    }

    /**
     * Retrieves a group description querying by its SID.
     *
     * @param sid the SID of a group, in its String format (i.e., not a binary SID)
     * @return the description of the group which objectID is the given SID, if it exists
     * @throws GroupNotFoundException if no group exists with the given SID under the base DN
     * @throws OperationFailedException if the operation fails for any other reason
     */
    private LDAPGroupWithAttributes findGroupWithAttributesBySID(String sid)
        throws GroupNotFoundException, OperationFailedException
    {
        Validate.notNull(sid, "SID argument cannot be null");

        EntityQuery<Group> query = QueryBuilder.queryFor(Group.class, EntityDescriptor.group())
            .with(Restriction.on(OBJECT_SID).exactlyMatching(sid))
            .returningAtMost(1);

        try
        {
            // query is constrained to returnAtMost(1)
            return Iterables.getOnlyElement(searchGroupObjects(query, getGroupContextMapper(GroupType.GROUP)));
        }
        catch (NoSuchElementException e)
        {
            throw new GroupNotFoundException("objectId = " + sid);
        }
    }

    /*
     * This implementation decorates the results of the parent method to add the membership of the user (indicated
     * in the query) to his primary group. In the worst case, this results in three queries to AD, as opposed
     * to the single query performed by the parent method. The two additional queries are made to retrieve the
     * user's description (particularly, his attributes) and then to fetch the primary group description.
     * In theory, these three queries could be reduced to just two for directories using 'memberOf' by using
     * a disjunction in the LDAP filter, but this optimisation hasn't been implemented.
     */
    @Override
    protected List<? extends LDAPGroupWithAttributes> findGroupMemberships(MembershipQuery<? extends LDAPGroupWithAttributes> query)
        throws OperationFailedException
    {
        List<? extends LDAPGroupWithAttributes> memberships = super.findGroupMemberships(query);

        // optimisation: do not consider primary group if the result page is already full
        if (isPrimaryGroupSupportEnabled()
            && query.getEntityToMatch().getEntityType() == Entity.USER
            && !isResultPageFull(memberships, query.getMaxResults()))
        {
            try
            {
                LDAPUserWithAttributes user = findUserWithAttributesByName(query.getEntityNameToMatch());
                LDAPGroupWithAttributes primaryGroup =
                    findGroupWithAttributesBySID(getPrimaryGroupSIDOfUser(user));
                ImmutableList<LDAPGroupWithAttributes> augmentedMemberships =
                    ImmutableList.<LDAPGroupWithAttributes>builder().addAll(memberships).add(primaryGroup).build();
                return SearchResultsUtil.constrainResults(augmentedMemberships, 0, query.getMaxResults());
            }
            catch (UserNotFoundException e)
            {
                return memberships;   // do not modify the behaviour of the overridden method
            }
            catch (GroupNotFoundException e)
            {
                logger.debug("Primary group of user '{}' is not under the base DN", query.getEntityNameToMatch());
                return memberships;   // do not modify the behaviour of the overridden method
            }
        }
        else
        {
            return memberships;
        }
    }

    /*
     * This implementation decorates the results of the parent method to add the membership of the user (indicated
     * in the query) to his primary group. In the worst case, this results in three queries to AD, as opposed
     * to the single query performed by the parent method. The two additional queries are made to retrieve the
     * user's description (particularly, his attributes) and then to fetch the primary group name.
     * In theory, these three queries could be reduced to just two for directories using 'memberOf' by using
     * a disjunction in the LDAP filter, but this optimisation hasn't been implemented.
     */
    @Override
    protected Iterable<String> findGroupMembershipNames(MembershipQuery<String> query)
        throws OperationFailedException
    {
        List<String> membershipNames = ImmutableList.copyOf(super.findGroupMembershipNames(query));

        // optimisation: do not consider primary group if the result page is already full
        if (isPrimaryGroupSupportEnabled()
            && query.getEntityToMatch().getEntityType() == Entity.USER
            && !isResultPageFull(membershipNames, query.getMaxResults()))
        {
            try
            {
                LDAPUserWithAttributes user = findUserWithAttributesByName(query.getEntityNameToMatch());
                String primaryGroupName =
                    findGroupNameBySID(getPrimaryGroupSIDOfUser(user));
                ImmutableList<String> augmentedMembershipNames =
                    ImmutableList.<String>builder().addAll(membershipNames).add(primaryGroupName).build();
                return SearchResultsUtil.constrainResults(augmentedMembershipNames, 0, query.getMaxResults());
            }
            catch (UserNotFoundException e)
            {
                return membershipNames;   // do not modify the behaviour of the overridden method
            }
            catch (GroupNotFoundException e)
            {
                logger.debug("Primary group of user '{}' is not under the base DN", query.getEntityNameToMatch());
                return membershipNames;   // do not modify the behaviour of the overridden method
            }
        }
        else
        {
            return membershipNames;
        }
    }

    /**
     * @param primaryGroupRid RID of a group
     * @return a filter that matches users who have the given group as their primary group
     */
    private AndFilter getUserByPrimaryGroupRidFilter(String primaryGroupRid)
    {
        AndFilter filter = new AndFilter();
        filter.and(new HardcodedFilter(ldapPropertiesMapper.getUserFilter()));
        filter.and(new EqualsFilter(PrimaryGroupIdMapper.ATTRIBUTE_KEY, primaryGroupRid));
        return filter;
    }

    /**
     * Queries AD to find the names of all users that have a certain group (identified by its RID) as their
     * primary group.
     *
     * @param primaryGroupRid the RID (relative identifier) of a group. Note that this is not the group absolute SID
     * @param startIndex paginated results control
     * @param maxResults paginated results control
     * @return the names of users that have the given group as their primary group
     * @throws OperationFailedException if the operation fails for any reason
     */
    private Iterable<LdapName> findUserMembersNamesOfGroupViaPrimaryGroupId(String primaryGroupRid,
                                                                            int startIndex, int maxResults)
        throws OperationFailedException
    {
        AndFilter filter = getUserByPrimaryGroupRidFilter(primaryGroupRid);

        ContextMapperWithRequiredAttributes<NamedLdapEntity> mapper =
            NamedLdapEntity.mapperFromAttribute(ldapPropertiesMapper.getUserNameAttribute());

        if (logger.isDebugEnabled())
        {
            logger.debug("Executing search at DN: <" + searchDN.getUser() + "> with filter: <" + filter.encode() + ">");
        }

        return dnsOf(searchEntities(searchDN.getUser(), filter.encode(), mapper, startIndex, maxResults));
    }

    /**
     * Queries AD to find the users that have a certain group (identified by its RID) as their primary group.
     *
     * @param primaryGroupRid the RID (relative identifier) of a group. Note that this is not the group absolute SID
     * @param startIndex paginated results control
     * @param maxResults paginated results control
     * @return the users that have the given group as their primary group
     * @throws OperationFailedException if the operation fails for any reason
     */
    private Iterable<LDAPUserWithAttributes> findUserMembersOfGroupViaPrimaryGroupId(String primaryGroupRid,
                                                                                     int startIndex, int maxResults)
        throws OperationFailedException
    {
        AndFilter filter = getUserByPrimaryGroupRidFilter(primaryGroupRid);

        if (logger.isDebugEnabled())
        {
            logger.debug("Executing search at DN: <" + searchDN.getUser() + "> with filter: <" + filter.encode() + ">");
        }

        return toGenericIterable(searchEntities(searchDN.getUser(), filter.encode(), getUserContextMapper(),
                                                startIndex, maxResults));
    }

    @Override
    protected List<LDAPUserWithAttributes> findUserMembersOfGroupViaMemberDN(String groupName,
                                                                             GroupType groupType,
                                                                             int startIndex,
                                                                             int maxResults)
        throws OperationFailedException
    {
        List<LDAPUserWithAttributes> users =
            super.findUserMembersOfGroupViaMemberDN(groupName, groupType, startIndex, maxResults);

        return augmentUserMembersOfGroupWithPrimaryGroupMembers(groupName, users, startIndex, maxResults);
    }

    @Override
    protected Iterable<LDAPUserWithAttributes> findUserMembersOfGroupViaMemberOf(String groupName,
                                                                                 GroupType groupType,
                                                                                 int startIndex,
                                                                                 int maxResults)
        throws OperationFailedException
    {
        List<LDAPUserWithAttributes> users =
            ImmutableList.copyOf(super.findUserMembersOfGroupViaMemberOf(groupName, groupType, startIndex, maxResults));

        return augmentUserMembersOfGroupWithPrimaryGroupMembers(groupName, users, startIndex, maxResults);
    }

    /*
     * Decorates the overridden method by adding the users who have the given group as their primary group.
     * Note that since AD assigns a primary group to every user, the default primary group may have a large
     * membership.
     * This implementation makes two additional queries to AD, one to fetch the group description, and the
     * second one to actually find the users. This can be potentially optimised to reduce the number of queries
     * to just two.
     */
    @Override
    public Iterable<LdapName> findDirectMembersOfGroup(LdapName groupDn) throws OperationFailedException
    {
        Iterable<LdapName> members = super.findDirectMembersOfGroup(groupDn);

        if (isPrimaryGroupSupportEnabled())
        {
            ContextMapperWithRequiredAttributes<LDAPGroupWithAttributes> mapper = getGroupContextMapper(GroupType.GROUP);
            LDAPGroupWithAttributes group = ldapTemplate.lookup(groupDn, mapper);
            String primaryGroupRid = SIDUtils.getLastRidFromSid(group.getValue(ObjectSIDMapper.ATTRIBUTE_KEY));
            Iterable<LdapName> additionalUsers =
                findUserMembersNamesOfGroupViaPrimaryGroupId(primaryGroupRid, 0, EntityQuery.ALL_RESULTS);
            return Iterables.concat(members, additionalUsers);
        }
        else
        {
            return members;
        }
    }

    private List<LDAPUserWithAttributes> augmentUserMembersOfGroupWithPrimaryGroupMembers(String groupName,
                                                                                          List<LDAPUserWithAttributes> users,
                                                                                          int startIndex,
                                                                                          int maxResults)
        throws OperationFailedException
    {
        if (isPrimaryGroupSupportEnabled() && !isResultPageFull(users, maxResults))
        {
            try
            {
                LDAPGroupWithAttributes group = findGroupWithAttributesByName(groupName);
                String primaryGroupRid = SIDUtils.getLastRidFromSid(group.getValue(ObjectSIDMapper.ATTRIBUTE_KEY));
                Iterable<LDAPUserWithAttributes> additionalUsers =
                    findUserMembersOfGroupViaPrimaryGroupId(primaryGroupRid, startIndex, maxResults);
                ImmutableList<LDAPUserWithAttributes> augmentedUsers =
                    ImmutableList.<LDAPUserWithAttributes>builder().addAll(users).addAll(additionalUsers).build();
                return SearchResultsUtil.constrainResults(augmentedUsers, 0, maxResults);
            }
            catch (GroupNotFoundException e)
            {
                return ImmutableList.copyOf(users);
            }
        }
        else
        {
            return ImmutableList.copyOf(users);
        }
    }

    /**
     * Helper method to optimise paginated results decoration.
     *
     * @param results the results from a query
     * @param maxResults the maximum number of results, as specified in the query
     * @return true if the current page of results if 'full', or false if the page can be augmented with more
     * results without breaking the pagination constraints.
     */
    @VisibleForTesting
    static boolean isResultPageFull(List results, int maxResults)
    {
        if (maxResults == EntityQuery.ALL_RESULTS)
        {
            return false;
        }
        else
        {
            return results.size() == maxResults;
        }
    }

    /**
     * Checks whether the primary group of the user is the provided one.
     *
     * @param user a user
     * @param group a group
     * @return true if 'group' is the primary group of 'user', false otherwise
     */
    @VisibleForTesting
    boolean isUserMemberOfPrimaryGroup(LDAPUserWithAttributes user, LDAPGroupWithAttributes group)
    {
        if (!isPrimaryGroupSupportEnabled())
        {
            return false;
        }

        String primaryGroupSID = getPrimaryGroupSIDOfUser(user);
        String groupSID = group.getValue(ObjectSIDMapper.ATTRIBUTE_KEY);
        return primaryGroupSID.equals(groupSID);
    }

    /**
     * In AD, users have a primaryGroupId attribute that contains the RID (relative identifier) of their primary
     * group. The SID of the primary group is obtained by resolving the RID with respect to the SID of the user.
     *
     * @param user
     * @return the String representation of the user's primary group SID
     * @throws NullPointerException if the user does not have a primaryGroupId or objectId
     */
    private String getPrimaryGroupSIDOfUser(LDAPUserWithAttributes user)
    {
        String primaryGroupRID = user.getValue(PrimaryGroupIdMapper.ATTRIBUTE_KEY);
        String userSID = user.getValue(ObjectSIDMapper.ATTRIBUTE_KEY);
        return SIDUtils.substituteLastRidInSid(userSID, primaryGroupRID);
    }

    /**
     * @return true if the support for primary groups is enabled in this directory
     */
    @VisibleForTesting
    boolean isPrimaryGroupSupportEnabled()
    {
        return getAttributeAsBoolean(DirectoryImpl.ATTRIBUTE_KEY_USE_PRIMARY_GROUP, false);  // default value
    }

    /**
     * AD does not need a default container member.
     *
     * @return <code>null</code>.
     */
    @Override
    protected String getInitialGroupMemberDN()
    {
        return null;
    }

    /**
     * Converts the clear-text password to the {<code>AD_PASSWORD_ENCODED</code> encoding - currently UTF-16LE
     *
     * @return byte array containing password in UTF-16LE encoding.
     * @throws InvalidCredentialException if {@link com.atlassian.crowd.embedded.api.PasswordCredential#isEncryptedCredential()}
     *                                    returns true for the given passwordCredential or if the encoding
     *                                    {@value MicrosoftActiveDirectory#AD_PASSWORD_ENCODED} does not exist on this
     *                                    system.
     */
    @Override
    protected byte[] encodePassword(PasswordCredential passwordCredential) throws InvalidCredentialException
    {
        if (PasswordCredential.NONE.equals(passwordCredential))
        {
            // active directory doesn't support setting passwords by hash, so the best we can do is generate a random one
            return encodeValueForUnicodePwdAttr(passwordHelper.generateRandomPassword());
        }
        else if (passwordCredential.isEncryptedCredential())
        {
            // active directory doesn't support setting passwords by hash so the most sensible thing to do is fail
            throw new InvalidCredentialException("Password credential must not be encrypted before being encoded");
        }
        else
        {
            return encodeValueForUnicodePwdAttr(passwordCredential.getCredential());
        }
    }

    /**
     * @param unhashedPasswordToEncode raw password (active directory doesn't support setting hashed passwords)
     * @return the password transformed to a format which is suitable for being set as the value of the unicodePwd attr
     * @throws InvalidCredentialException if the encoding {@value MicrosoftActiveDirectory#AD_PASSWORD_ENCODED} does not
     *                                    exist on this system.
     */
    private static byte[] encodeValueForUnicodePwdAttr(String unhashedPasswordToEncode) throws InvalidCredentialException
    {
        try
        {
            //Replace the "unicodePwd" attribute with a new value
            //Password must be both Unicode and a quoted string
            String newQuotedPassword = "\"" + unhashedPasswordToEncode + "\"";
            return newQuotedPassword.getBytes(AD_PASSWORD_ENCODED);
        }
        catch (UnsupportedEncodingException e)  // if the Charset AD_PASSWORD_ENCODED isn't available on this system
        {
            throw new InvalidCredentialException(e.getMessage(), e);
        }
    }

    /**
     * Active Directory needs a couple of additional attributes set - the sAMAccountName (which is the account name
     * you use to log on to Windows), and the account disabled flag.
     *
     * @param user
     * @param attributes
     */
    @Override
    protected void getNewUserDirectorySpecificAttributes(User user, Attributes attributes)
    {
        //These are the mandatory attributes for a user object
        //Note that Win2K3 will automagically create a random
        //samAccountName if it is not present. (Win2K does not)
        attributes.put(AD_SAM_ACCOUNT_NAME, user.getName());

        // Set the account status
        String accountStatus = null;
        if (user.isActive())
        {
            accountStatus = Integer.toString(UF_NORMAL_ACCOUNT + UF_PASSWD_NOTREQD + UF_PASSWORD_EXPIRED);
        }
        else
        {
            accountStatus = Integer.toString(UF_NORMAL_ACCOUNT + UF_PASSWD_NOTREQD + UF_PASSWORD_EXPIRED + UF_ACCOUNTDISABLE);
        }

        attributes.put(new BasicAttribute(AD_USER_ACCOUNT_CONTROL, accountStatus));
    }

    /**
     * If we want to be able to nest groups, we need to create distribution groups rather than security groups.
     * To do this we need to set groupType to 2.
     *
     * @param group
     * @param attributes
     */
    @Override
    protected void getNewGroupDirectorySpecificAttributes(final Group group, final Attributes attributes)
    {
        attributes.put(GROUP_TYPE_NAME, GROUP_TYPE_VALUE);
    }

    @Override
    protected List<AttributeMapper> getCustomUserAttributeMappers()
    {
        Builder<AttributeMapper> builder = ImmutableList.<AttributeMapper>builder();
        builder.addAll(super.getCustomUserAttributeMappers());
        builder.add(new ObjectGUIDMapper());
        builder.add(new USNChangedMapper());
        builder.add(new ObjectSIDMapper());
        builder.add(new PrimaryGroupIdMapper());
        builder.add(new UserAccountControlMapper());

        return builder.build();
    }

    @Override
    protected List<AttributeMapper> getCustomGroupAttributeMappers()
    {
        Builder<AttributeMapper> builder = ImmutableList.<AttributeMapper>builder();
        builder.addAll(super.getCustomGroupAttributeMappers());
        builder.add(new ObjectGUIDMapper());
        builder.add(new USNChangedMapper());
        builder.add(new ObjectSIDMapper());

        return builder.build();
    }

    @Override
    protected List<AttributeMapper> getMemberDnMappers()
    {
        return Arrays.asList(
                new RFC4519MemberDnRangedMapper(ldapPropertiesMapper.getGroupMemberAttribute(), ldapPropertiesMapper.isRelaxedDnStandardisation()),
                new RFC4519MemberDnRangeOffsetMapper(ldapPropertiesMapper.getGroupMemberAttribute()));
    }

    @Override
    protected List<LDAPGroupWithAttributes> postprocessGroups(final List<LDAPGroupWithAttributes> groups)
        throws OperationFailedException
    {
        List<LDAPGroupWithAttributes> result = Lists.newArrayList();
        for (LDAPGroupWithAttributes group : groups)
        {
            if (group.getValue(RFC4519MemberDnRangeOffsetMapper.ATTRIBUTE_KEY) != null)
            {
                ListAttributeValueProcessor valueAggregator = new ListAttributeValueProcessor();
                String rangeStart = group.getValue(RFC4519MemberDnRangeOffsetMapper.ATTRIBUTE_KEY);
                RangeOption range = new RangeOption(Integer.valueOf(rangeStart));
                IncrementalAttributeMapper incrementalAttributeMapper = new IncrementalAttributeMapper(ldapPropertiesMapper.getGroupMemberAttribute(), valueAggregator, range);

                LdapName groupDnName = getLdapName(group);

                while (incrementalAttributeMapper.hasMore())
                {
                    ldapTemplate.lookup(groupDnName, incrementalAttributeMapper.getAttributesArray(), incrementalAttributeMapper);
                }

                // standardise the memberDNs (remember to include the existing members!)
                Set<String> initialMembers = group.getValues(RFC4519MemberDnMapper.ATTRIBUTE_KEY);
                Set<String> standardDNs = new HashSet<String>(initialMembers.size() + valueAggregator.getValues().size());
                standardDNs.addAll(initialMembers);
                for (String memberDN : valueAggregator.getValues())
                {
                    String dn = standardiseDN(memberDN);
                    standardDNs.add(dn);
                }

                // Create the new group template - add existing attributes and set the memberDNs
                GroupTemplateWithAttributes groupTemplate = new GroupTemplateWithAttributes(group);

                groupTemplate.setAttribute(RFC4519MemberDnMapper.ATTRIBUTE_KEY, standardDNs);
                groupTemplate.removeAttribute(RFC4519MemberDnRangeOffsetMapper.ATTRIBUTE_KEY); // remove, we don't need this anymore
                result.add(new LDAPGroupWithAttributes(group.getDn(), groupTemplate));
            }
            else
            {
                result.add(group);
            }
        }
        return result;
    }

    @Override
    protected Map<String, String> getBaseEnvironmentProperties()
    {
        Map<String, String> env = super.getBaseEnvironmentProperties();

        // ensure binary attributes are read as binary (byte array and not string)
        env.put(LDAPPropertiesMapperImpl.CONNECTION_BINARY_ATTRIBUTES,
                Joiner.on(' ').join(ImmutableList.of(ObjectGUIDMapper.ATTRIBUTE_KEY,
                                                     ObjectSIDMapper.ATTRIBUTE_KEY)));

        return env;
    }

    public long fetchHighestCommittedUSN() throws OperationFailedException
    {
        try
        {
            String highestCommittedUSN = ((DirContextAdapter) ldapTemplate.lookup(GenericConverter.emptyLdapName())).getStringAttribute(AD_HIGHEST_COMMITTED_USN);
            if (highestCommittedUSN != null)
            {
                try
                {
                    long usn = Long.parseLong(highestCommittedUSN);
                    if (logger.isDebugEnabled())
                    {
                        logger.debug("Fetched highest committed USN of " + usn);
                    }
                    return usn;
                }
                catch (NumberFormatException e)
                {
                    throw new OperationFailedException("Error parsing highestCommittedUSN as a number", e);
                }
            }
            else
            {
                throw new OperationFailedException("No highestCommittedUSN attribute found for AD root");
            }
        }
        catch (org.springframework.ldap.NamingException e)
        {
            throw new OperationFailedException("Error looking up attributes for highestCommittedUSN", e);
        }
    }

    public List<LDAPUserWithAttributes> findAddedOrUpdatedUsersSince(long usnChange) throws OperationFailedException
    {
        return findAddedOrUpdatedObjectsSince(usnChange, searchDN.getUser(), ldapPropertiesMapper.getUserFilter(), getUserContextMapper());
    }

    public List<LDAPGroupWithAttributes> findAddedOrUpdatedGroupsSince(long usnChanged) throws OperationFailedException
    {
        return findAddedOrUpdatedObjectsSince(usnChanged, searchDN.getGroup(), ldapPropertiesMapper.getGroupFilter(), getGroupContextMapper(GroupType.GROUP));
    }

    public List<Tombstone> findUserTombstonesSince(long usnChange) throws OperationFailedException
    {
        return findTombstonesSince(usnChange, searchDN.getUser(), ldapPropertiesMapper.getUserObjectClass());
    }

    public List<Tombstone> findGroupTombstonesSince(long usnChange) throws OperationFailedException
    {
        return findTombstonesSince(usnChange, searchDN.getGroup(), ldapPropertiesMapper.getGroupObjectClass());
    }

    protected <T> List<T> findAddedOrUpdatedObjectsSince(long usnChange, Name objectBaseDN, String objectFilter, ContextMapperWithRequiredAttributes<T> contextMapper) throws OperationFailedException
    {
        AndFilter filter = new AndFilter();

        // restrict the object type to the role object type
        filter.and(new HardcodedFilter(objectFilter));
        filter.and(new GreaterThanOrEqualsFilter(USNChangedMapper.ATTRIBUTE_KEY, Long.toString(usnChange + 1)));

        logger.debug("Performing polling search: baseDN = " + objectBaseDN + " - filter = " + filter.encode());

        return searchEntities(objectBaseDN, filter.encode(), contextMapper, 0, -1);
    }

    private Name getDeletedObjectsDN()
    {
        try
        {
            // CWD-1339 - Correctly find the root of this domain.
            DirContextAdapter root = (DirContextAdapter) ldapTemplate.lookup(new LdapName(""));
            String rootDN = root.getStringAttribute(ROOT_DOMAIN_NAMING_CONTEXT);

            // we could refactor this out and allow it to be customisable
            String dn = new StringBuffer(DELETED_OBJECTS_DN_ADDITION).append(",").append(rootDN).toString();

            return new LdapName(dn);
        }
        catch (NamingException e)
        {
            // if we can't build the DN, then just search from the base DN
            return searchDN.getNamingContext();
        }
    }

    protected List<Tombstone> findTombstonesSince(long usnChange, Name objectBaseDN, String objectClass) throws OperationFailedException
    {
        ContextMapperWithRequiredAttributes<Tombstone> contextMapper = new TombstoneContextMapper();

        SearchControls searchControls = getSubTreeSearchControls(contextMapper);

        // create a filter for finding deleted objects of the appropriate class
        AndFilter filter = new AndFilter();
        filter.and(new EqualsFilter(AD_IS_DELETED, "TRUE"));
        filter.and(new EqualsFilter(AD_OBJECT_CLASS, objectClass));
        filter.and(new GreaterThanOrEqualsFilter(USNChangedMapper.ATTRIBUTE_KEY, Long.toString(usnChange + 1)));

        // get the deleted objects container DN or the root DN (deleted objects are moved to this container)
        Name deletedObjectsDN = getDeletedObjectsDN();

        logger.debug("Performing tombstones search: baseDN = " + deletedObjectsDN + " - filter = " + filter.encode());

        return searchEntitiesWithRequestControls(deletedObjectsDN, filter.encode(), contextMapper, searchControls,
                                                 new DeletedResultsControl(), 0, -1);
    }

    @Override
    public ContextMapperWithRequiredAttributes<LDAPUserWithAttributes> getUserContextMapper()
    {
        return new ActiveDirectoryUserContextMapper(this.getDirectoryId(), ldapPropertiesMapper,
                                                    getCustomUserAttributeMappers());
    }

    @Override
    protected List<ModificationItem> getUserModificationItems(User userTemplate, LDAPUserWithAttributes currentUser)
    {
        Builder<ModificationItem> modificationItems =
            ImmutableList.<ModificationItem>builder().addAll(super.getUserModificationItems(userTemplate, currentUser));

        String currentValue = currentUser.getValue(UserAccountControlMapper.ATTRIBUTE_KEY);
        String newValue = userTemplate.isActive() ?
                          UserAccountControlUtil.enabledUser(currentValue) :
                          UserAccountControlUtil.disabledUser(currentValue);

        ModificationItem activeModItem = createModificationItem(UserAccountControlMapper.ATTRIBUTE_KEY,
                                                                currentValue, newValue);

        if (activeModItem != null)
        {
            modificationItems.add(activeModItem);
        }

        return modificationItems.build();
    }

    /**
     * This connector supports inactive accounts while, in general, LDAP connector do not.
     *
     * @return <code>true</code>
     */
    @Override
    public boolean supportsInactiveAccounts()
    {
        return true;
    }
}
