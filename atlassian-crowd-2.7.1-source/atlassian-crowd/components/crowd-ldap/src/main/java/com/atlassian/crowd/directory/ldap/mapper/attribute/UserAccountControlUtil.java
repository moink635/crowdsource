package com.atlassian.crowd.directory.ldap.mapper.attribute;

import com.atlassian.crowd.directory.MicrosoftActiveDirectory;

/**
 * Helper class to manipulate the values of Active Directory userAccountControl attribute.
 *
 * @since v2.7
 */
public class UserAccountControlUtil
{
    /**
     * This is called UF_ACCOUNTDISABLE by Microsoft, see http://support.microsoft.com/kb/269181
     */
    private static long USER_ACCOUNT_DISABLED_BITMASK = MicrosoftActiveDirectory.UF_ACCOUNTDISABLE;

    public static boolean isUserEnabled(String userAccountControlValue)
    {
        long attributeValue = Long.parseLong(userAccountControlValue);
        return (attributeValue & USER_ACCOUNT_DISABLED_BITMASK) == 0;
    }

    public static String enabledUser(String currentValue)
    {
        long attributeValue = Long.parseLong(currentValue);
        return Long.toString(attributeValue & (~USER_ACCOUNT_DISABLED_BITMASK));
    }

    public static String disabledUser(String currentValue)
    {
        long attributeValue = Long.parseLong(currentValue);
        return Long.toString(attributeValue | USER_ACCOUNT_DISABLED_BITMASK);
    }
}
