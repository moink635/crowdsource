package com.atlassian.crowd.directory.ldap.mapper.attribute;

import java.util.Collections;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import org.springframework.ldap.core.DirContextAdapter;

/**
 * Maps the primaryGroupId of a user.
 *
 * This concept only applies to Active Directory.
 *
 * @since v2.7
 */
public class PrimaryGroupIdMapper implements AttributeMapper
{
    /**
     * Primary Group ID attribute name.
     */
    public static final String ATTRIBUTE_KEY = "primaryGroupId";

    @Override
    public String getKey()
    {
        return ATTRIBUTE_KEY;
    }

    @Override
    public Set<String> getValues(DirContextAdapter ctx) throws Exception
    {
        String primaryGroupId = ctx.getStringAttribute(getKey());

        if (primaryGroupId != null)
        {
            return ImmutableSet.of(primaryGroupId);
        }
        else
        {
            return Collections.emptySet();
        }
    }

    @Override
    public Set<String> getRequiredLdapAttributes()
    {
        return Collections.singleton(getKey());
    }
}
