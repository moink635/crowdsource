package com.atlassian.crowd.directory;

import java.util.Set;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;

import com.atlassian.crowd.directory.ldap.mapper.ContextMapperWithRequiredAttributes;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.springframework.ldap.core.DirContextAdapter;

class NamedLdapEntity
{
    private final LdapName dn;
    private final String name;

    NamedLdapEntity(LdapName dn, String name)
    {
        this.dn = Preconditions.checkNotNull(dn, "DN may not be null");
        this.name = name;
    }

    LdapName getDn()
    {
        return dn;
    }

    String getName()
    {
        return name;
    }

    public String toString()
    {
        return dn.toString() + " = " + name;
    }


    private static final Function<? super NamedLdapEntity, String> NAMES_OF = new Function<NamedLdapEntity, String>() {
        @Override
        public String apply(NamedLdapEntity input)
        {
            return input.getName();
        }
    };

    static Iterable<String> namesOf(Iterable<? extends NamedLdapEntity> namedEntities)
    {
        return Iterables.transform(namedEntities, NAMES_OF);
    }

    private static final Function<? super NamedLdapEntity, LdapName> DNS_OF = new Function<NamedLdapEntity, LdapName>() {
        @Override
        public LdapName apply(NamedLdapEntity input)
        {
            return input.getDn();
        }
    };

    static Iterable<LdapName> dnsOf(Iterable<? extends NamedLdapEntity> namedEntities)
    {
        return Iterables.transform(namedEntities, DNS_OF);
    }

    /**
     * A {@link ContextMapperWithRequiredAttributes} that captures only the name of an
     * entity from a single attribute.
     */
    static ContextMapperWithRequiredAttributes<NamedLdapEntity> mapperFromAttribute(String attrName)
    {
        return new NamedEntityMapper(attrName);
    }

    static class NamedEntityMapper implements ContextMapperWithRequiredAttributes<NamedLdapEntity>
    {
        private final String nameAttribute;

        NamedEntityMapper(String nameAttribute)
        {
            this.nameAttribute = Preconditions.checkNotNull(nameAttribute, "Attribute name may not be null");
        }

        @Override
        public NamedLdapEntity mapFromContext(Object ctx)
        {
            DirContextAdapter context = (DirContextAdapter) ctx;

            LdapName dn;

            try
            {
                dn = new LdapName(context.getDn().toString());
            }
            catch (InvalidNameException e)
            {
                throw new RuntimeException(e);
            }

            return new NamedLdapEntity(dn,  context.getStringAttribute(nameAttribute));
        }

        @Override
        public Set<String> getRequiredLdapAttributes()
        {
            return ImmutableSet.of(nameAttribute);
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o)
            {
                return true;
            }
            if (o == null || getClass() != o.getClass())
            {
                return false;
            }

            NamedEntityMapper that = (NamedEntityMapper) o;

            return nameAttribute.equals(that.nameAttribute);
        }

        @Override
        public int hashCode()
        {
            return nameAttribute.hashCode();
        }
    }
}
