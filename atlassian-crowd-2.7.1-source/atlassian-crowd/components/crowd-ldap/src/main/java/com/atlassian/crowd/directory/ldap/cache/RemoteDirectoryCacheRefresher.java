package com.atlassian.crowd.directory.ldap.cache;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.List;

/**
 * A simple implementation of CacheRefresher that will only do "Full Refresh".
 * This is used for all LDAP servers other than AD.
 *
 * @since v2.1
 */
public class RemoteDirectoryCacheRefresher extends AbstractCacheRefresher implements CacheRefresher
{
    private static final Logger log = LoggerFactory.getLogger(RemoteDirectoryCacheRefresher.class);

    public RemoteDirectoryCacheRefresher(final RemoteDirectory remoteDirectory)
    {
        super(remoteDirectory);
    }

    public boolean synchroniseChanges(final DirectoryCache directoryCache) throws OperationFailedException
    {
        // We can never do a delta sync
        return false;
    }

    private List<User> findAllRemoteUsers() throws OperationFailedException
    {
        long start = System.currentTimeMillis();
        log.debug("loading remote users");
        List<User> users = remoteDirectory.searchUsers(QueryBuilder.queryFor(User.class, EntityDescriptor.user()).returningAtMost(EntityQuery.ALL_RESULTS));
        log.info(new StringBuilder("found [ ").append(users.size()).append(" ] remote users in [ ").append(System.currentTimeMillis() - start).append("ms ]").toString());
        return users;
    }

    private List<Group> findAllRemoteGroups(GroupType groupType) throws OperationFailedException
    {
        long start = System.currentTimeMillis();
        log.debug("loading remote groups");
        List<Group> groups = remoteDirectory.searchGroups(QueryBuilder.queryFor(Group.class, EntityDescriptor.group(groupType)).returningAtMost(EntityQuery.ALL_RESULTS));
        log.info(new StringBuilder("found [ ").append(groups.size()).append(" ] remote groups in [ ").append(System.currentTimeMillis() - start).append("ms ]").toString());
        return groups;
    }

    @Override
    protected void synchroniseAllUsers(DirectoryCache directoryCache) throws OperationFailedException
    {
        Date syncStartDate = new Date();

        List<? extends User> ldapUsers = findAllRemoteUsers();

        // removes
        directoryCache.deleteCachedUsersNotIn(ldapUsers, syncStartDate);

        // inserts/updates
        directoryCache.addOrUpdateCachedUsers(ldapUsers, syncStartDate);

    }

    @Override
    protected List<? extends Group> synchroniseAllGroups(DirectoryCache directoryCache) throws OperationFailedException
    {
        Date syncStartDate = new Date();

        //CWD-2504: We filter out the duplicate groups. This basically means that these groups don't exist to crowd.
        // Crowd can't currently deal with groups with the same name so rather than throwing a runtime exception
        // which kills the synchronization, we just ignore them and log a message and continue to work.
        // The admins of the LDAP directory will have to do some admin to get the groups to work correctly.
        List<? extends Group> groups = filterOutDuplicateGroups(findAllRemoteGroups(GroupType.GROUP));

        // inserts/updates
        directoryCache.addOrUpdateCachedGroups(groups, syncStartDate);

        // removes
        directoryCache.deleteCachedGroupsNotIn(GroupType.GROUP, groups, syncStartDate);

        return groups;
    }
}
