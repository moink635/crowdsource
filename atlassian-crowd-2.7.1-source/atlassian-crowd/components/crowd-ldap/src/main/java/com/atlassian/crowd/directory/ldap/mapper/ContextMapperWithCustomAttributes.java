package com.atlassian.crowd.directory.ldap.mapper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.crowd.directory.ldap.mapper.attribute.AttributeMapper;

import com.google.common.base.Preconditions;

import org.springframework.ldap.core.DirContextAdapter;

/**
 * An abstract implementation of {@link ContextMapperWithRequiredAttributes} for cases where
 * a mapper requires core attributes and also the attributes for any attached
 * custom {@link AttributeMapper}s.
 */
public abstract class ContextMapperWithCustomAttributes<T> implements ContextMapperWithRequiredAttributes<T>
{
    protected final List<AttributeMapper> customAttributeMappers;

    public ContextMapperWithCustomAttributes(List<AttributeMapper> customAttributeMappers)
    {
        Preconditions.checkNotNull(customAttributeMappers);
        this.customAttributeMappers = customAttributeMappers;
    }

    @Override
    public final T mapFromContext(Object ctx)
    {
        return mapFromContext((DirContextAdapter) ctx);
    }

    public abstract T mapFromContext(DirContextAdapter ctx);

    protected abstract Set<String> getCoreRequiredLdapAttributes();

    @Override
    public Set<String> getRequiredLdapAttributes()
    {
        Set<String> combined = new HashSet<String>();

        Set<String> core = getCoreRequiredLdapAttributes();
        if (core == null)
        {
            return null;
        }

        combined.addAll(getCoreRequiredLdapAttributes());

        for (AttributeMapper m : customAttributeMappers)
        {
            Set<String> attrs = m.getRequiredLdapAttributes();
            if (attrs == null)
            {
                return null;
            }
            combined.addAll(attrs);
        }

        return combined;
    }

    public static Set<String> aggregate(AttributeMapper... mappers)
    {
        Set<String> combined = new HashSet<String>();

        for (AttributeMapper m : mappers)
        {
            Set<String> attrs = m.getRequiredLdapAttributes();
            if (attrs == null)
            {
                return null;
            }
            combined.addAll(attrs);
        }

        return combined;
    }
}
