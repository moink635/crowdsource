package com.atlassian.crowd.directory.ldap;

import java.util.List;
import java.util.Set;

import javax.naming.Name;
import javax.naming.NamingEnumeration;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.ModificationItem;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.LdapName;

import com.atlassian.crowd.directory.LimitedNamingEnumeration;
import com.atlassian.crowd.directory.ldap.mapper.ContextMapperWithRequiredAttributes;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.ContextMapperCallbackHandler;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.NameClassPairCallbackHandler;
import org.springframework.ldap.core.SearchExecutor;

/**
 * Wrap an {@link LdapTemplate} and perform all operations with the context
 * ClassLoader set to this class's ClassLoader.
 * <code>com.sun.naming.internal.NamingManager</code> uses the context
 * ClassLoader so, without this wrapper, calls that originate from plugins and
 * end up using LDAP will fail when they can't see the Spring LDAP
 * implementation classes.
 */
public class LdapTemplateWithClassLoaderWrapper
{
    private final LdapTemplate template;

    public LdapTemplateWithClassLoaderWrapper(LdapTemplate template)
    {
        this.template = template;
    }

    static <T> T invokeWithContextClassLoader(CallableWithoutCheckedException<T> runnable)
    {
        Thread current = Thread.currentThread();
        ClassLoader orig = current.getContextClassLoader();

        try
        {
            ClassLoader classLoaderForThisClass = LdapTemplateWithClassLoaderWrapper.class.getClassLoader();
            current.setContextClassLoader(classLoaderForThisClass);

            return runnable.call();
        }
        finally
        {
            current.setContextClassLoader(orig);
        }
    }

    public List search(final Name base, final String filter, final SearchControls controls, final ContextMapper mapper)
    {
        return invokeWithContextClassLoader(new CallableWithoutCheckedException<List>() {
            public List call()
            {
                return template.search(base, filter, controls, mapper);
            }
        });
    }

    public List search(final Name base, final String filter, final SearchControls controls, final ContextMapper mapper,
            final DirContextProcessor processor)
    {
        return invokeWithContextClassLoader(new CallableWithoutCheckedException<List>() {
            public List call()
            {
                return template.search(base, filter, controls, mapper, processor);
            }
        });
    }

    public Object lookup(final Name dn)
    {
        return invokeWithContextClassLoader(new CallableWithoutCheckedException<Object>() {
            public Object call()
            {
                return template.lookup(dn);
            }
        });
    }

    public void search(final Name base, final String filter, final SearchControls controls,
            final NameClassPairCallbackHandler handler, final DirContextProcessor processor)
    {
        invokeWithContextClassLoader(new CallableWithoutCheckedException<Void>() {
            public Void call()
            {
                template.search(base, filter, controls, handler, processor);
                return null;
            }
        });
    }

    public void unbind(final Name dn)
    {
        invokeWithContextClassLoader(new CallableWithoutCheckedException<Void>() {
            public Void call()
            {
                template.unbind(dn);
                return null;
            }
        });
    }

    public void bind(final Name dn, final Object obj, final Attributes attributes)
    {
        invokeWithContextClassLoader(new CallableWithoutCheckedException<Void>() {
            public Void call()
            {
                template.bind(dn, obj, attributes);
                return null;
            }
        });
    }

    public void modifyAttributes(final Name dn, final ModificationItem[] mods)
    {
        invokeWithContextClassLoader(new CallableWithoutCheckedException<Void>() {
            public Void call()
            {
                template.modifyAttributes(dn, mods);
                return null;
            }
        });
    }

    public void lookup(final LdapName dn, final String[] attributes, final AttributesMapper mapper)
    {
        invokeWithContextClassLoader(new CallableWithoutCheckedException<Void>() {
            public Void call()
            {
                template.lookup(dn, attributes, mapper);
                return null;
            }
        });
    }

    public <T> T lookup(final LdapName dn, final ContextMapperWithRequiredAttributes<T> mapper)
    {
        Set<String> attrSet = mapper.getRequiredLdapAttributes();

        final String[] attributes = attrSet.toArray(new String[attrSet.size()]);

        return invokeWithContextClassLoader(new CallableWithoutCheckedException<T>() {
            @SuppressWarnings("unchecked")
            public T call()
            {
                return (T) template.lookup(dn, attributes, mapper);
            }
        });
    }

    public void setIgnorePartialResultException(boolean ignore)
    {
        /* This doesn't load classes */
        template.setIgnorePartialResultException(ignore);
    }

    public void search(final SearchExecutor se, final NameClassPairCallbackHandler handler, final DirContextProcessor processor)
    {
        invokeWithContextClassLoader(new CallableWithoutCheckedException<Void>() {
            public Void call()
            {
                template.search(se, handler, processor);
                return null;
            }
        });
    }

    static interface CallableWithoutCheckedException<T>
    {
        T call();
    }

    public List searchWithLimitedResults(final Name baseDN, final String filter, final SearchControls searchControls,
            ContextMapper contextMapper, DirContextProcessor processor, final int limit)
    {
        SearchExecutor se = new SearchExecutor() {
            public NamingEnumeration<SearchResult> executeSearch(DirContext ctx) throws javax.naming.NamingException {
                NamingEnumeration<SearchResult> ne = ctx.search(baseDN, filter, searchControls);

                if (limit != EntityQuery.ALL_RESULTS)
                {
                    return new LimitedNamingEnumeration<SearchResult>(ne, limit);
                }
                else
                {
                    return ne;
                }
            }
        };

        ContextMapperCallbackHandler handler = new ContextMapperCallbackHandler(contextMapper);

        search(se, handler, processor);

        return handler.getList();
    }
}
