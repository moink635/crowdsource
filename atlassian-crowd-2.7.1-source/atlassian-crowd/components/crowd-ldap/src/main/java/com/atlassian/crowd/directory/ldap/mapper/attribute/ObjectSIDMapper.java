package com.atlassian.crowd.directory.ldap.mapper.attribute;

import java.util.Collections;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.support.LdapUtils;

/**
 * Maps the primaryGroupId of a user.
 *
 * This concept only applies to Active Directory.
 *
 * @since v2.7
 */
public class ObjectSIDMapper implements AttributeMapper
{
    /**
     * Object SID attribute name.
     */
    public static final String ATTRIBUTE_KEY = "objectSid";

    @Override
    public String getKey()
    {
        return ATTRIBUTE_KEY;
    }

    @Override
    public Set<String> getValues(DirContextAdapter ctx) throws Exception
    {
        byte[] objectSidAsByteArray = (byte[]) ctx.getObjectAttribute(getKey());

        if (objectSidAsByteArray != null)
        {
            String objectSid = LdapUtils.convertBinarySidToString(objectSidAsByteArray);
            return ImmutableSet.of(objectSid);
        }
        else
        {
            return Collections.emptySet();
        }
    }

    @Override
    public Set<String> getRequiredLdapAttributes()
    {
        return Collections.singleton(getKey());
    }
}
