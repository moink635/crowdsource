package com.atlassian.crowd.directory.ldap.util;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.ldap.UncategorizedLdapException;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

public class DirectoryAttributeRetriever
{
    private static final Logger logger = LoggerFactory.getLogger(DirectoryAttributeRetriever.class);
    /**
     * If we need to save an empty valued or null (deleted) attribute, this is what we store instead.
     * The exact reasons why aren't clear (i.e. can't be gleaned from the commit history or referenced issues),
     * but a plausible explanation is that it avoids us violating LDAP schema constraints.
     */
    private static final String NULL_OR_EMPTY_ATTRIBUTE_VALUE_PLACEHOLDER = " ";

    /**
     * Retrieves the first value from the collection of attributes for the
     * supplied name directoryAttributeName. If no value exists or if the value
     * is not safe for XML marshalling, <code>null</code> is returned.
     *
     * @param directoryAttributeName attribute name key.
     * @param directoryAttributes collection of attributes to examine.
     * @return first attribute value.
     */
    public static String getValueFromAttributes(String directoryAttributeName, Attributes directoryAttributes)
    {
        if (StringUtils.isBlank(directoryAttributeName))
        {
            return null;
        }

        String value = null;

        Attribute values = directoryAttributes.get(directoryAttributeName);
        if (values != null && values.size() > 0)
        {
            try
            {
                final Object attributeValue = values.get(0);
                if (attributeValue != null)
                {
                    value = attributeValue.toString();
                    if (value != null && !XmlValidator.isSafe(value))
                    {
                        String currentLdapContext = MDC.get("crowd.ldap.context");
                        String ldapContextMessage = currentLdapContext != null
                            ? "Context: <" + currentLdapContext + ">. " : "";
                        logger.info("Unsafe attribute value <{}> for attribute <{}>. {}. Attribute was skipped.",
                                StringEscapeUtils.escapeJava(value), directoryAttributeName, ldapContextMessage);
                        value = null;
                    }
                }
            }
            catch (javax.naming.NamingException e)
            {
                throw new UncategorizedLdapException(e);
            }
        }

        return fromSavedLDAPValue(value);
    }

    /**
     * Retrieves the first value from the collection of attributes for the
     * supplied name externalIdAttribute. If name of the attribute is empty
     * or no value for attribute exists empty string is returned.
     * <p>
     * Although RFC4530 (http://www.ietf.org/rfc/rfc4530.txt) says that the 'entryUUID' attribute is
     * "encoded using the ASCII character string representation described in RFC4122, for example
     * 597ae2f6-16a6-1027-98f4-d28b5365dc14", it is conceivable that a directory may be configured to
     * use any other binary or text attribute (e.g., the email) as external identifier.
     * Therefore, this method accepts both String values and byte arrays, and makes sure that the Strings
     * are XML-safe.
     *
     * @param externalIdAttribute name of external Id attribute
     * @param directoryAttributes collection of attributes
     * @return null if there is no attribute name,
     *         empty string if there is no value for an attribute name,
     *         value of attribute otherwise
     *
     */
    public static String getValueFromExternalIdAttribute(final String externalIdAttribute, final Attributes directoryAttributes)
    {
        if (StringUtils.isBlank(externalIdAttribute))
        {
            return null;
        }

        Attribute values = directoryAttributes.get(externalIdAttribute);
        if (values == null || values.size() == 0)
        {
            //if the name of attribute is defined, but there is no value
            return StringUtils.EMPTY;
        }
        else if (values.size() > 1)
        {
            logger.info("Skipping attribute {} because it is multi-valued", externalIdAttribute);
            return StringUtils.EMPTY;
        }

        else
        {
            try
            {
                final Object attributeValue = values.get(0);
                if (attributeValue instanceof String)
                {
                    String stringValue = (String) attributeValue;
                    if (XmlValidator.isSafe(stringValue))
                    {
                        return stringValue;
                    }
                    else
                    {
                        logger.info("Skipping attribute {} because its value <{}> is not XML safe",
                                externalIdAttribute, StringEscapeUtils.escapeJava(stringValue));
                        return StringUtils.EMPTY;
                    }
                }
                else if (attributeValue instanceof byte[])
                {
                    return GuidHelper.getGUIDAsString((byte[]) attributeValue);
                }
                else if (attributeValue == null)
                {
                    return StringUtils.EMPTY;
                }
                else
                {
                    logger.info("Skipping attribute {} because its value <{}> is not a String or a byte array",
                            externalIdAttribute, attributeValue.toString());
                    return StringUtils.EMPTY;
                }
            }
            catch (NamingException e)
            {
                throw new UncategorizedLdapException(e);
            }
        }
    }

    /**
     * Transforms attribute values in order to comply with LDAP schemas. Reversing this transformation
     * using {@link #fromSavedLDAPValue(String)} is not lossless; see notes about {@code value} below.
     * @param value the value to save. If null, it will be treated as the empty string.
     * @return a value which is safe for saving to LDAP
     * @throws IllegalArgumentException if the given value can't be saved as the value of an LDAP attribute
     */
    public static String toSaveableLDAPValue(String value)
    {
        if (NULL_OR_EMPTY_ATTRIBUTE_VALUE_PLACEHOLDER.equals(value))
        {
            throw new IllegalArgumentException( "value '" + value
                            + "' conflicts with the placeholder value that is stored for a blank or null (deleted) LDAP attribute");
        }
        if (value == null || value.isEmpty())
        {
            return NULL_OR_EMPTY_ATTRIBUTE_VALUE_PLACEHOLDER;
        }
        else
        {
            return value;
        }
    }

    /**
     * @return the reversed form of the output of {@link #toSaveableLDAPValue(String)}. In the case where a null or
     * empty string can't be disambiguated between, an empty string will be returned.
     */
    private static String fromSavedLDAPValue(String value)
    {
        if (NULL_OR_EMPTY_ATTRIBUTE_VALUE_PLACEHOLDER.equals(value))
        {
            // we can't know whether the original value was null or the empty string, so assume it's an empty string
            return "";
        }
        else
        {
            return value;
        }
    }
}