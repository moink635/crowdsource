package com.atlassian.crowd.directory.ldap.cache;

import com.atlassian.crowd.directory.RemoteCrowdDirectory;
import com.atlassian.crowd.directory.SynchronisableDirectoryProperties;
import com.atlassian.crowd.event.EventTokenExpiredException;
import com.atlassian.crowd.event.Events;
import com.atlassian.crowd.event.IncrementalSynchronisationNotAvailableException;
import com.atlassian.crowd.exception.CrowdException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UnsupportedCrowdApiException;
import com.atlassian.crowd.model.event.GroupEvent;
import com.atlassian.crowd.model.event.GroupMembershipEvent;
import com.atlassian.crowd.model.event.Operation;
import com.atlassian.crowd.model.event.OperationEvent;
import com.atlassian.crowd.model.event.UserEvent;
import com.atlassian.crowd.model.event.UserMembershipEvent;
import com.atlassian.crowd.model.group.Group;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class EventTokenChangedCacheRefresher extends AbstractCacheRefresher
{
    private static final Logger log = LoggerFactory.getLogger(EventTokenChangedCacheRefresher.class);

    private final RemoteDirectoryCacheRefresher fullSyncCacheRefresher;

    private final RemoteCrowdDirectory crowdDirectory;

    private String currentEventToken;

    public EventTokenChangedCacheRefresher(final RemoteCrowdDirectory crowdDirectory)
    {
        super(crowdDirectory);
        this.crowdDirectory = crowdDirectory;
        fullSyncCacheRefresher = new RemoteDirectoryCacheRefresher(remoteDirectory);
    }

    private boolean isIncrementalSyncEnabled()
    {
        return Boolean.parseBoolean(crowdDirectory.getValue(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED));
    }

    @Override
    public void synchroniseAll(DirectoryCache directoryCache) throws OperationFailedException
    {
        String initialEventToken = null;

        if (isIncrementalSyncEnabled())
        {
            try
            {
                initialEventToken = crowdDirectory.getCurrentEventToken();
                this.currentEventToken = initialEventToken;
            }
            catch (UnsupportedCrowdApiException e)
            {
                log.debug("Remote server does not support event based sync.");
            }
            catch (OperationFailedException e)
            {
                log.warn("Could not update event token.", e);
            }
            catch (IncrementalSynchronisationNotAvailableException e)
            {
                log.warn("Incremental synchronisation is not available. Falling back to full synchronisation", e);
            }
        }

        fullSyncCacheRefresher.synchroniseAll(directoryCache);

        if (initialEventToken != null)
        {
            try
            {
                String postEventToken = crowdDirectory.getCurrentEventToken();

                if (initialEventToken.equals(postEventToken))
                {
                    log.warn("Possible events during full synchronisation");
                }
            }
            catch (CrowdException e)
            {
                log.debug("Failed to retrieve event token after full synchronisation", e);
            }
        }
    }

    public boolean synchroniseChanges(DirectoryCache directoryCache) throws OperationFailedException
    {
        // When restarting the app, we must do a full refresh the first time.
        if (currentEventToken == null || !isIncrementalSyncEnabled())
        {
            return false;
        }

        final Events events;
        try
        {
            events = crowdDirectory.getNewEvents(currentEventToken);
        }
        catch (EventTokenExpiredException e)
        {
            currentEventToken = null;
            return false;
        }

        // forget the old token, it is not valid anymore. If something goes wrong with the synchronisation,
        // the database may be in an inconsistent state, and any re-attempt to do an incremental synchronisation
        // is not going to help. The lack of an event token will cause the next synchronisation to be full.
        currentEventToken = null;

        for (OperationEvent event : events.getEvents())
        {
            if (event instanceof UserEvent)
            {
                final UserEvent userEvent = (UserEvent) event;
                if (event.getOperation() == Operation.CREATED || event.getOperation() == Operation.UPDATED)
                {
                    directoryCache.addOrUpdateCachedUser(userEvent.getUser());
                }
                else if (event.getOperation() == Operation.DELETED)
                {
                    directoryCache.deleteCachedUser(userEvent.getUser().getName());
                }
            }
            else if (event instanceof GroupEvent)
            {
                final GroupEvent groupEvent = (GroupEvent) event;
                if (event.getOperation() == Operation.CREATED || event.getOperation() == Operation.UPDATED)
                {
                    directoryCache.addOrUpdateCachedGroup(groupEvent.getGroup());
                }
                else if (event.getOperation() == Operation.DELETED)
                {
                    directoryCache.deleteCachedGroup(groupEvent.getGroup().getName());
                }
            }
            else if (event instanceof UserMembershipEvent)
            {
                UserMembershipEvent membershipEvent = (UserMembershipEvent) event;
                if (event.getOperation() == Operation.CREATED)
                {
                    for (String parentGroupName : membershipEvent.getParentGroupNames())
                    {
                        directoryCache.addUserToGroup(membershipEvent.getChildUsername(), parentGroupName);
                    }
                }
                else if (event.getOperation() == Operation.DELETED)
                {
                    for (String parentGroupName : membershipEvent.getParentGroupNames())
                    {
                        directoryCache.removeUserFromGroup(membershipEvent.getChildUsername(), parentGroupName);
                    }
                }
                else if (event.getOperation() == Operation.UPDATED)
                {
                   directoryCache.syncGroupMembershipsForUser(membershipEvent.getChildUsername(), membershipEvent.getParentGroupNames());
                }
            }
            else if (event instanceof GroupMembershipEvent)
            {
                GroupMembershipEvent membershipEvent = (GroupMembershipEvent) event;
                if (event.getOperation() == Operation.CREATED)
                {
                    for (String parentGroupName : membershipEvent.getParentGroupNames())
                    {
                        directoryCache.addGroupToGroup(membershipEvent.getGroupName(), parentGroupName);
                    }
                }
                else if (event.getOperation() == Operation.DELETED)
                {
                    for (String parentGroupName : membershipEvent.getParentGroupNames())
                    {
                        directoryCache.removeGroupFromGroup(membershipEvent.getGroupName(), parentGroupName);
                    }
                }
                else if (event.getOperation() == Operation.UPDATED)
                {
                    directoryCache.syncGroupMembershipsAndMembersForGroup(membershipEvent.getGroupName(), membershipEvent.getParentGroupNames(), membershipEvent.getChildGroupNames());
                }
            }
            else
            {
                throw new RuntimeException("Unsupported event " + event);
            }
        }

        // only if the incremental sync has completed, we may use the event token for the next incremental sync
        currentEventToken = events.getNewEventToken();

        return true;
    }

    @Override
    protected void synchroniseAllUsers(DirectoryCache directoryCache) throws OperationFailedException
    {
        throw new UnsupportedOperationException();
    }

    @Override
    protected List<? extends Group> synchroniseAllGroups(DirectoryCache directoryCache) throws OperationFailedException
    {
        throw new UnsupportedOperationException();
    }
}
