package com.atlassian.crowd.directory.ldap.mapper;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.AttributeMapper;
import com.atlassian.crowd.directory.ldap.mapper.entity.LDAPGroupAttributesMapper;
import com.atlassian.crowd.directory.ldap.util.DNStandardiser;
import com.atlassian.crowd.model.group.GroupTemplateWithAttributes;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.LDAPGroupWithAttributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.ldap.NamingException;
import org.springframework.ldap.core.DirContextAdapter;
import org.springframework.ldap.core.DistinguishedName;

import javax.naming.directory.Attributes;
import java.util.List;
import java.util.Set;

/**
 * Translates information returned from an LDAP directory into a {@link com.atlassian.crowd.model.group.LDAPGroupWithAttributes}
 * implementation of {@link com.atlassian.crowd.model.group.Group}.
 */
public class GroupContextMapper extends ContextMapperWithCustomAttributes<LDAPGroupWithAttributes>
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected final long directoryId;
    protected final GroupType groupType;
    protected final LDAPPropertiesMapper ldapPropertiesMapper;

    public GroupContextMapper(long directoryId, GroupType groupType, LDAPPropertiesMapper ldapPropertiesMapper, List<AttributeMapper> customAttributeMappers)
    {
        super(customAttributeMappers);
        this.directoryId = directoryId;
        this.groupType = groupType;
        this.ldapPropertiesMapper = ldapPropertiesMapper;
    }

    @Override
    protected Set<String> getCoreRequiredLdapAttributes()
    {
        return getAttributesMapper().getRequiredLdapAttributes();
    }

    /**
     * Called by Spring LDAP on every object fetched from the LDAP directory.
     *
     * @param ctx A {@link org.springframework.ldap.core.DirContextAdapter DirContextAdapter} containing information about the object
     * @return {@link com.atlassian.crowd.model.user.LDAPUserWithAttributes}.
     */
    public LDAPGroupWithAttributes mapFromContext(DirContextAdapter context) throws NamingException
    {
        Attributes attributes = context.getAttributes();
        LDAPGroupAttributesMapper mapper = getAttributesMapper();

        // build group from common attributes
        MDC.put("crowd.ldap.context", context.getDn().toString());
        GroupTemplateWithAttributes groupTemplate;
        try
        {
            groupTemplate = mapper.mapGroupFromAttributes(attributes);
        }
        finally
        {
            MDC.remove("crowd.ldap.context");
        }

        // map custom attributes
        for (AttributeMapper attributeMapper : customAttributeMappers)
        {
            try
            {
                groupTemplate.setAttribute(attributeMapper.getKey(), attributeMapper.getValues(context));
            }
            catch (Exception e)
            {
                logger.error("Failed to map attribute <" + attributeMapper.getKey() + "> from context with DN <" + context.getDn().toString() + ">", e);
            }
        }

        String dn = DNStandardiser.standardise((DistinguishedName) context.getDn(), !ldapPropertiesMapper.isRelaxedDnStandardisation());

        LDAPGroupWithAttributes group = new LDAPGroupWithAttributes(dn, groupTemplate);

        if (logger.isTraceEnabled())
        {
            logger.trace("Created group <" + group + "> from DN <" + context.getDn() + ">");
        }

        return group;
    }

    /**
     * Split out so it can be overridden.
     *
     * @return
     */
    protected LDAPGroupAttributesMapper getAttributesMapper()
    {
        return new LDAPGroupAttributesMapper(directoryId, groupType, ldapPropertiesMapper);
    }
}
