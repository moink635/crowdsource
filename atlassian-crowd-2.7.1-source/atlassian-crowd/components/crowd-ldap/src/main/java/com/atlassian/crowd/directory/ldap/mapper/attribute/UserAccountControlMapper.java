package com.atlassian.crowd.directory.ldap.mapper.attribute;

import java.util.Collections;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import org.springframework.ldap.core.DirContextAdapter;

/**
 * Maps the userAccountControl on a user.
 *
 * This concept only applies to Active Directory.
 *
 * @since 2.7
 */
public class UserAccountControlMapper implements AttributeMapper
{
    public static final String ATTRIBUTE_KEY = "userAccountControl";

    @Override
    public String getKey()
    {
        return ATTRIBUTE_KEY;
    }

    @Override
    public Set<String> getValues(DirContextAdapter ctx) throws Exception
    {
        String value = ctx.getStringAttribute(getKey());

        if (value != null)
        {
            return ImmutableSet.of(value);
        }
        else
        {
            return Collections.emptySet();
        }
    }

    @Override
    public Set<String> getRequiredLdapAttributes()
    {
        return Collections.singleton(getKey());
    }
}
