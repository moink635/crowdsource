package com.atlassian.crowd.directory.ldap.mapper.attribute;

import java.util.Set;

import org.springframework.ldap.core.DirContextAdapter;

/**
 * <p>Maps a single attribute for an entity from an LDAP
 * {@link DirContextAdapter} to a set of string values.
 * This is used for non-core attributes, such as membership for specific
 * directory types and for custom attributes.</p>
 *
 * <p>Mandatory or field-level attributes are mapped via the
 * {@link com.atlassian.crowd.directory.ldap.mapper.entity.LDAPUserAttributesMapper}
 * and the
 * {@link com.atlassian.crowd.directory.ldap.mapper.entity.LDAPGroupAttributesMapper}.</p>
 */
public interface AttributeMapper
{
    /**
     * Get the key to use when storing the attribute on an entity
     * with {@link com.atlassian.crowd.embedded.api.Attributes}.
     *
     * @return non-null key.
     */
    public String getKey();

    /**
     * Map the value of the key from the directory context.
     *
     * @param ctx directory context containing attributes.
     * @return the set of attribute values associated with the key. If no values are present an empty set will be returned
     * @throws Exception error retrieving value. The attribute will not be set.
     */
    public Set<String> getValues(DirContextAdapter ctx) throws Exception;

    /**
     * Returns the LDAP attributes that should be requested in a search where {@link #getValues(DirContextAdapter)} will
     * be called on the results. If an implementation returns <code>null</code> then all attributes will
     * be requested.
     *
     * @return the LDAP attributes that should be requested, or <code>null</code> for all
     */
    Set<String> getRequiredLdapAttributes();
}
