package com.atlassian.crowd.directory.ldap.mapper;

import java.util.NoSuchElementException;
import java.util.Set;

import com.atlassian.crowd.directory.ldap.mapper.attribute.ObjectGUIDMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.USNChangedMapper;
import com.atlassian.crowd.model.Tombstone;
import org.springframework.ldap.core.DirContextAdapter;

/**
 * Specific to Active Directory to map deleted objects.
 */
public class TombstoneContextMapper implements ContextMapperWithRequiredAttributes<Tombstone>
{
    private final ObjectGUIDMapper objectGUIDMapper = new ObjectGUIDMapper();
    private final USNChangedMapper usnChangedMapper = new USNChangedMapper();

    /**
     * Returns an object representation of a deleted object.
     *
     * @param ctx
     * @return tombstone object with objectGUID and uSNChanged.
     */
    public Tombstone mapFromContext(Object ctx)
    {
        try
        {
            DirContextAdapter context = (DirContextAdapter) ctx;

            // npe's get caught and rethrown as re
            String guid = objectGUIDMapper.getValues(context).iterator().next();
            String usnChanged = usnChangedMapper.getValues(context).iterator().next();

            return new Tombstone(guid, usnChanged);
        }
        catch (NoSuchElementException e)
        {
            throw new RuntimeException("Could not retrieve objectGUID/uSNChanged due to missing attribute for object: " + ((DirContextAdapter) ctx).getDn());
        }
        catch (Exception e)
        {
            throw new RuntimeException("Could not retrieve objectGUID/uSNChanged from object: " + ((DirContextAdapter) ctx).getDn(), e);
        }
    }

    @Override
    public Set<String> getRequiredLdapAttributes()
    {
        return ContextMapperWithCustomAttributes.aggregate(objectGUIDMapper, usnChangedMapper);
    }
}
