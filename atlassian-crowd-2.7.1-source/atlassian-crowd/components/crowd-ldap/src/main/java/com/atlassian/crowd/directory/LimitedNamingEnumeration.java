package com.atlassian.crowd.directory;

import java.util.NoSuchElementException;

import javax.naming.NamingEnumeration;
import javax.naming.NamingException;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Preconditions;

/**
 * Wrap a {@link NamingEnumeration} and only return up to a limited number of elements.
 */
public class LimitedNamingEnumeration<T> implements NamingEnumeration<T>
{
    private final NamingEnumeration<T> ne;
    private final int limit;

    private int count = 0;

    public LimitedNamingEnumeration(NamingEnumeration<T> ne, int limit)
    {
        this.ne = Preconditions.checkNotNull(ne);
        this.limit = limit;
    }

    @VisibleForTesting
    public int getLimit()
    {
        return limit;
    }

    @Override
    public void close() throws NamingException
    {
        ne.close();
    }

    @Override
    public boolean hasMore() throws NamingException
    {
        return count < limit && ne.hasMore();
    }

    @Override
    public boolean hasMoreElements()
    {
        return count < limit && ne.hasMoreElements();
    }

    @Override
    public T next() throws NamingException
    {
        if (count < limit)
        {
            count++;
            return ne.next();
        }
        else
        {
            throw new NoSuchElementException(count + " items already yielded");
        }
    }

    @Override
    public T nextElement()
    {
        if (count < limit)
        {
            count++;
            return ne.nextElement();
        }
        else
        {
            throw new NoSuchElementException(count + " items already yielded");
        }
    }
}
