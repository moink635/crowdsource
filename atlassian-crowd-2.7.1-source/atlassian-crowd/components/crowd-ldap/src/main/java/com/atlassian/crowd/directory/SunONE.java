package com.atlassian.crowd.directory;

import javax.naming.directory.Attributes;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.ldap.LDAPQueryTranslater;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.crowd.util.PasswordHelper;
import com.atlassian.event.api.EventPublisher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Sun ONE / Sun DSEE Directory connector.
 */
public class SunONE extends RFC4519Directory
{
    private static final Logger logger = LoggerFactory.getLogger(SunONE.class);

    private final PasswordHelper passwordHelper;

    /**
     * @param passwordHelper password helper, which must not be null
     */
    public SunONE(LDAPQueryTranslater ldapQueryTranslater, EventPublisher eventPublisher, InstanceFactory instanceFactory,
                  PasswordHelper passwordHelper)
    {
        super(ldapQueryTranslater, eventPublisher, instanceFactory);
        this.passwordHelper = checkNotNull(passwordHelper);
    }

    public static String getStaticDirectoryType()
    {
        return "Sun Directory Server Enterprise Edition";
    }

    @Override
    public String getDescriptiveName()
    {
        return SunONE.getStaticDirectoryType();
    }

    /**
     * Sun DSEE doesn't want passwords encoded before they're passed to the directory.
     *
     * @throws InvalidCredentialException if {@link com.atlassian.crowd.embedded.api.PasswordCredential#isEncryptedCredential()}
     *                                    returns true for the given passwordCredential
     */
    @Override
    protected String encodePassword(PasswordCredential passwordCredential) throws InvalidCredentialException
    {
        if (PasswordCredential.NONE.equals(passwordCredential))
        {
            // since we don't support setting passwords by hash, the best we can do is generate a random password instead
            return passwordHelper.generateRandomPassword();
        }
        else if (passwordCredential.isEncryptedCredential())
        {
            throw new InvalidCredentialException("Password credential must not be encrypted before being encoded");
        }
        else
        {
            return passwordCredential.getCredential();
        }
    }

    /**
     * Sun DSEE 6.2 in a default install requires the sn to be set before a user can be created.
     * @param user
     * @param attributes
     */
    @Override
    protected void getNewUserDirectorySpecificAttributes(final User user, final Attributes attributes)
    {
        addDefaultSnToUserAttributes(attributes, "");
    }


    // CHANGE LISTENER STUFF

    protected LdapTemplate createChangeListenerTemplate()
    {
        // create a spring connection context object
        LdapContextSource contextSource = new LdapContextSource();

        contextSource.setUrl(ldapPropertiesMapper.getConnectionURL());
        contextSource.setUserDn(ldapPropertiesMapper.getUsername());
        contextSource.setPassword(ldapPropertiesMapper.getPassword());

        // let spring know of our connection attributes
        contextSource.setBaseEnvironmentProperties(getBaseEnvironmentProperties());

        // create a pool for when doing multiple calls.
        contextSource.setPooled(true);

        // by default, all results are converted to a DirContextAdapter using the
        // dirObjectFactory property of the ContextSource (we need to disable that)
        contextSource.setDirObjectFactory(null);

        try
        {
            // we need to tell the context source to configure up our ldap server
            contextSource.afterPropertiesSet();
        }
        catch (Exception e)
        {
            logger.error(e.getMessage(), e);
        }

        return new LdapTemplate(contextSource);
    }
}