package com.atlassian.crowd.search.ldap;

import org.springframework.ldap.filter.AbstractFilter;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implements a bitwise filter for Active Directory attributes. See http://support.microsoft.com/kb/269181
 *
 * @since v2.7
 */
public class BitwiseFilter extends AbstractFilter
{
    private static final String LDAP_MATCHING_RULE_BIT_AND = "1.2.840.113556.1.4.803";
    private static final String LDAP_MATCHING_RULE_BIT_OR = "1.2.840.113556.1.4.804";

    private final String attribute;
    private final String ruleOID;
    private final int mask;

    private BitwiseFilter(String attribute, String ruleOID, int mask)
    {
        this.attribute = checkNotNull(attribute);
        this.ruleOID = ruleOID;
        this.mask = mask;
    }

    @Override
    public StringBuffer encode(StringBuffer buff)
    {
        buff.append("(");
        buff.append(attribute).append(":").append(ruleOID).append(":=").append(mask);
        buff.append(")");
        return buff;
    }

    // the interface requires implementations to provide equals() and hashCode()
    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        BitwiseFilter that = (BitwiseFilter) o;

        if (mask != that.mask)
        {
            return false;
        }
        if (!attribute.equals(that.attribute))
        {
            return false;
        }
        if (!ruleOID.equals(that.ruleOID))
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = attribute.hashCode();
        result = 31 * result + ruleOID.hashCode();
        result = 31 * result + mask;
        return result;
    }

    /**
     * Factory method
     *
     * @param attribute attribute name to query
     * @param mask bit mask
     * @return a bitwise filter that will match attribute values that contain all the bits in the mask.
     */
    public static BitwiseFilter and(String attribute, int mask)
    {
        return new BitwiseFilter(attribute, LDAP_MATCHING_RULE_BIT_AND, mask);
    }

    /**
     * Factory method
     *
     * @param attribute attribute name to query
     * @param mask bit mask
     * @return a bitwise filter that will match attribute values that contain any of the bits in the mask.
     */
    public static BitwiseFilter or(String attribute, int mask)
    {
        return new BitwiseFilter(attribute, LDAP_MATCHING_RULE_BIT_OR, mask);
    }
}
