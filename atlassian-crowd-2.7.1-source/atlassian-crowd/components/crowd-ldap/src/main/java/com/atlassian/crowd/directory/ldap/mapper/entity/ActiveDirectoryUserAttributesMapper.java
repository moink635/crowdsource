package com.atlassian.crowd.directory.ldap.mapper.entity;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.UserAccountControlMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.UserAccountControlUtil;

/**
 * Specialised {@link LDAPUserAttributesMapper} for Active Directory. It maps the user enabled/disabled flag to
 * Crowd's active/inactive user. If the userAccountControl attribute is missing, the user is assumed to be active.
 *
 * @since v2.7
 */
public class ActiveDirectoryUserAttributesMapper extends LDAPUserAttributesMapper
{
    public ActiveDirectoryUserAttributesMapper(long directoryId,
                                               LDAPPropertiesMapper ldapPropertiesMapper)
    {
        super(directoryId, ldapPropertiesMapper);
    }

    @Override
    protected boolean getUserActiveFromAttribute(Attributes directoryAttributes)
    {
        try
        {
            Attribute attribute = directoryAttributes.get(UserAccountControlMapper.ATTRIBUTE_KEY);
            if (attribute != null)
            {
                String userAccountControlValue = attribute.get().toString();
                return UserAccountControlUtil.isUserEnabled(userAccountControlValue);
            }
            else
            {
                logger.debug("LDAP attribute userAccountControl is not present, user is enabled by default");
                return true;
            }
        }
        catch (NamingException e)
        {
            return true; // this should not happen. Assume user is active (as the parent class does)
        }
    }
}
