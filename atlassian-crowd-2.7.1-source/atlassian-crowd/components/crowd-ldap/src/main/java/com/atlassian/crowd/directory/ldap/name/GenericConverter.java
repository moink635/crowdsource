package com.atlassian.crowd.directory.ldap.name;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;

/**
 * See Converter interface for details.
 */
public class GenericConverter implements Converter
{
    public LdapName getName(String dn) throws InvalidNameException
    {
        try
        {
            return new LdapName(dn);
        }
        catch (IllegalArgumentException e)
        {
            // thrown when parsing a DN that contains an invalid escape sequence
            throw (InvalidNameException) new InvalidNameException("Cannot convert <" + dn + "> to a LDAP name").initCause(e);
        }
    }

    public LdapName getName(String attributeName, String objectName, LdapName baseDN) throws InvalidNameException
    {
        List<Rdn> rdns;

        if (baseDN != null)
        {
            rdns = baseDN.getRdns();
        }
        else
        {
            rdns = Collections.emptyList();
        }

        List<Rdn> newRdns = new ArrayList<Rdn>(rdns);
        newRdns.add(new Rdn(attributeName, objectName));

        return new LdapName(newRdns);
    }

    /**
     * @return an empty {@link LdapName}, with no components
     */
    public static LdapName emptyLdapName()
    {
        return new LdapName(Collections.<Rdn>emptyList());
    }
}
