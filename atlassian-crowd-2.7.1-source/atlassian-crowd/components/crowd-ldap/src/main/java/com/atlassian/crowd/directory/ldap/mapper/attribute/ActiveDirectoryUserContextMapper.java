package com.atlassian.crowd.directory.ldap.mapper.attribute;

import java.util.List;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.mapper.UserContextMapper;
import com.atlassian.crowd.directory.ldap.mapper.entity.ActiveDirectoryUserAttributesMapper;
import com.atlassian.crowd.directory.ldap.mapper.entity.LDAPUserAttributesMapper;

/**
 * A specialisation of {@link UserContextMapper} for Active Directory.
 *
 * @since v2.7
 */
public class ActiveDirectoryUserContextMapper extends UserContextMapper
{
    public ActiveDirectoryUserContextMapper(long directoryId,
                                            LDAPPropertiesMapper ldapPropertiesMapper,
                                            List<AttributeMapper> customAttributeMappers)
    {
        super(directoryId, ldapPropertiesMapper, customAttributeMappers);
    }

    @Override
    protected LDAPUserAttributesMapper getAttributesMapper()
    {
        return new ActiveDirectoryUserAttributesMapper(directoryId, ldapPropertiesMapper);
    }
}
