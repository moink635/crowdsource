package com.atlassian.crowd.directory.ldap.cache;

import com.atlassian.crowd.directory.ldap.mapper.attribute.ObjectGUIDMapper;
import com.atlassian.crowd.model.LDAPDirectoryEntity;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import java.util.HashMap;
import java.util.Map;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;

public class LDAPEntityNameMap<T extends LDAPDirectoryEntity>
{
    private Map<String, String> guidMap = new HashMap<String, String>();
    private Map<String, String> dnMap = new HashMap<String, String>();

    public void put(T ldapEntity)
    {
        guidMap.put(ldapEntity.getValue(ObjectGUIDMapper.ATTRIBUTE_KEY), ldapEntity.getName());
        dnMap.put(ldapEntity.getDn(), ldapEntity.getName());
    }

    public String getByDn(String dn)
    {
        return dnMap.get(dn);
    }

    public String getByGuid(String guid)
    {
        return guidMap.get(guid);
    }

    public void clear()
    {
        guidMap.clear();
        dnMap.clear();
    }

    public Map<LdapName, String> toLdapNameKeyedMap() throws InvalidNameException
    {
        Builder<LdapName, String> builder = ImmutableMap.<LdapName, String>builder();

        for (Map.Entry<String, String> e : dnMap.entrySet())
        {
            builder.put(new LdapName(e.getKey()), e.getValue());
        }

        return builder.build();
    }
}
