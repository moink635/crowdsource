package com.atlassian.crowd.directory.ldap.mapper.attribute;

import org.apache.commons.lang3.Validate;

/**
 * Convert back and forth between the binary and the String representation of SIDs.
 *
 * @since v2.7
 */
public class SIDUtils
{
    /**
     * Replaces the last subauthority of a SID with a new RID.
     *
     * @param baseSid a SID in its String format (i.e., not a binary SID)
     * @param rid a RID
     * @return a SID that has the last subauthority replaced with the new RID.
     */
    public static String substituteLastRidInSid(String baseSid, String rid)
    {
        Validate.notNull(baseSid, "baseSID argument cannot be null");
        Validate.notNull(rid, "RID argument cannot be null");

        return baseSid.substring(0, baseSid.lastIndexOf('-')) + "-" + rid;
    }

    /**
     * @param sid a SID in its String format (i.e, not a binary SID)
     * @return the RID of the last subauthority of the SID
     */
    public static String getLastRidFromSid(String sid)
    {
        Validate.notEmpty(sid, "sid argument cannot be empty");

        return sid.substring(sid.lastIndexOf('-') + 1);
    }
}
