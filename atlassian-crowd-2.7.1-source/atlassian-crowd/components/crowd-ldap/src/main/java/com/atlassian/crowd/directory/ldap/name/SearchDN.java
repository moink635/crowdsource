package com.atlassian.crowd.directory.ldap.name;

import javax.naming.InvalidNameException;
import javax.naming.Name;
import javax.naming.ldap.LdapName;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchDN
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final LDAPPropertiesMapper propertiesMapper;
    private final Converter converter;


    public SearchDN(LDAPPropertiesMapper propertiesMapper, Converter converter)
    {
        this.propertiesMapper = propertiesMapper;
        this.converter = converter;
    }

    /**
     * Returns a Name representing the DN to anchor the search for groups. Reads directory configuration to build the
     * dn.
     * @return
     */
    public LdapName getGroup()
    {
        try
        {
            return getSearchDN(LDAPPropertiesMapper.GROUP_DN_ADDITION);
        }
        catch (InvalidNameException e)
        {
            logger.error("Group Search DN could not be parsed", e);
            return GenericConverter.emptyLdapName();
        }
    }

    /**
     * Returns a Name representing the DN form which to search for roles. Reads directory configuration to build the dn.
     * @return
     * @deprecated
     */
    public LdapName getRole()
    {
        try
        {
            return getSearchDN(LDAPPropertiesMapper.ROLE_DN_ADDITION);
        }
        catch (InvalidNameException e)
        {
            logger.error("Role Search DN could not be parsed", e);
            return GenericConverter.emptyLdapName();
        }
    }

    /**
     * Returns a Name representing the DN to search beneath for users. Reads directory configuration to build the dn.
     * @return
     */
    public LdapName getUser()
    {
        try
        {
            return getSearchDN(LDAPPropertiesMapper.USER_DN_ADDITION);
        }
        catch (InvalidNameException e)
        {
            logger.error("User Search DN could not be parsed", e);
            return GenericConverter.emptyLdapName();
        }
    }

    /**
     * Returns a Name representing the DN beneath which all objects should reside.
     * @return
     * @throws javax.naming.InvalidNameException
     */
    public Name getBase() throws InvalidNameException
    {
        return converter.getName(propertiesMapper.getAttribute(LDAPPropertiesMapper.LDAP_BASEDN_KEY));
    }

    /**
     * Returns a Name representing the DN beneath which all objects should reside OR
     * the blank root DSE if none is specified.
     * @return
     */
    public Name getNamingContext()
    {
        Name baseDN;
        try
        {
            baseDN = getBase();
        }
        catch (InvalidNameException e)
        {
            baseDN = GenericConverter.emptyLdapName();
        }

        return baseDN;
    }

    /**
     * Given an additional DN (eg. "ou=Groups") and a base search DN (eg. "dc=example, dc=org") builds a full search DN
     * "ou=Groups, dc=example, dc=org". Handles if there's no additional DN specified.
     * @param propertyName
     * @return
     * @throws javax.naming.InvalidNameException
     */
    protected LdapName getSearchDN(String propertyName) throws InvalidNameException
    {
        String searchDN = "";

        String additionalDN = propertiesMapper.getAttribute(propertyName);
        if (StringUtils.isNotBlank(additionalDN))
        {
            searchDN = additionalDN;
        }

        String baseDN = propertiesMapper.getAttribute(LDAPPropertiesMapper.LDAP_BASEDN_KEY);
        if (StringUtils.isNotBlank(baseDN))
        {
            if (StringUtils.isNotBlank(searchDN))
            {
                searchDN += ",";
            }
            searchDN += baseDN;
        }

        return converter.getName(searchDN);
    }
}
