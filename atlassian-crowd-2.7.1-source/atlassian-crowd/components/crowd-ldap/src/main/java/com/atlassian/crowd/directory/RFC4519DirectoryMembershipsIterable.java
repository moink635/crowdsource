package com.atlassian.crowd.directory;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.naming.ldap.LdapName;

import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.group.Membership;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.collect.Maps;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * An {@link Iterable} over group {@link Membership}s optimised for the case
 * where we already have the DNs and names of all possible users and
 * sub-groups.
 */
public class RFC4519DirectoryMembershipsIterable implements Iterable<Membership>
{
    private static final Logger logger = LoggerFactory.getLogger(RFC4519DirectoryMembershipsIterable.class);

    private final RFC4519Directory connector;
    private final Map<LdapName, String> users;
    private final Map<LdapName, String> groups;
    private final Set<String> groupsToInclude;

    /**
     * @param connector the remote directory to query
     * @param users all known users
     * @param groups all known groups
     * @param groupsToInclude a set of <strong>lowercased</strong> group names. Restrict membership retrieval to only the known groups that also appear in this set.
     */
    public RFC4519DirectoryMembershipsIterable(RFC4519Directory connector, Map<LdapName, String> users,
            Map<LdapName, String> groups, Set<String> groupsToInclude)
    {
        this.connector = connector;
        this.users = users;
        this.groups = groups;
        this.groupsToInclude = groupsToInclude;
    }

    public RFC4519DirectoryMembershipsIterable(RFC4519Directory springLDAPConnector, Map<LdapName, String> users,
            Map<LdapName, String> groups)
    {
        this(springLDAPConnector, users, groups, ImmutableSet.copyOf(Iterables.transform(groups.values(), IdentifierUtils.TO_LOWER_CASE)));
    }

    private final Predicate<String> groupIsIncluded = new Predicate<String>()
    {
        @Override
        public boolean apply(String name)
        {
            return groupsToInclude.contains(IdentifierUtils.toLowerCase(name));
        }
    };

    private Function<? super Entry<LdapName, String>, ? extends Membership> lookUpMembers = new Function<Entry<LdapName, String>, Membership>()
    {
        @Override
        public Membership apply(Entry<LdapName, String> input)
        {
            Iterable<LdapName> directMembers;

            try
            {
                directMembers = connector.findDirectMembersOfGroup(input.getKey());
            }
            catch (OperationFailedException e)
            {
                throw new Membership.MembershipIterationException(e);
            }

            final String groupName = input.getValue();
            final ImmutableSet.Builder<String> userNames = ImmutableSet.builder();
            final ImmutableSet.Builder<String> childGroupNames = ImmutableSet.builder();

            for (LdapName member : directMembers)
            {
                if (users.containsKey(member))
                {
                    userNames.add(users.get(member));
                }

                if (groups.containsKey(member))
                {
                    childGroupNames.add(groups.get(member));
                }

                if (!users.containsKey(member) && !groups.containsKey(member))
                {
                    logger.debug("Unexpected DN in group '{}': {}", groupName, member);
                }
            }

            return new ImmutableMembership(groupName, userNames.build(), childGroupNames.build());
        }
    };

    @Override
    public Iterator<Membership> iterator()
    {
        Map<LdapName, String> groupsToRetrieve = Maps.filterValues(groups, groupIsIncluded);

        return Iterators.transform(groupsToRetrieve.entrySet().iterator(), lookUpMembers);
    }

    private static final class ImmutableMembership implements Membership
    {
        private final String groupName;
        private final Set<String> userNames;
        private final Set<String> childGroupNames;

        private ImmutableMembership(String groupName, Set<String> userNames, Set<String> childGroupNames)
        {
            this.groupName = groupName;
            this.childGroupNames = childGroupNames;
            this.userNames = userNames;
        }

        @Override
        public String getGroupName()
        {
            return groupName;
        }

        @Override
        public Set<String> getUserNames()
        {
            return userNames;
        }

        @Override
        public Set<String> getChildGroupNames()
        {
            return childGroupNames;
        }
    }
}
