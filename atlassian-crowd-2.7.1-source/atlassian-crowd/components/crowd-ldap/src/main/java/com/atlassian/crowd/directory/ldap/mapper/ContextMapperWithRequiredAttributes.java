package com.atlassian.crowd.directory.ldap.mapper;

import java.util.Set;

import org.springframework.ldap.core.ContextMapper;
import org.springframework.ldap.core.DirContextAdapter;

/**
 * A {@link ContextMapper} for LDAP {@link DirContextAdapter}s that declares which attributes it requires.
 */
public interface ContextMapperWithRequiredAttributes<T> extends ContextMapper
{
    @Override
    public T mapFromContext(Object ctx);

    /**
     * Returns the LDAP attributes that should be requested in a search where {@link #mapFromContext(Object)} will
     * be called on the results. If an implementation returns <code>null</code> then all attributes will
     * be requested.
     *
     * @return the LDAP attributes that should be requested, or <code>null</code> for all
     */
    Set<String> getRequiredLdapAttributes();
}
