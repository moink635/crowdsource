package com.atlassian.crowd.directory.ldap.mapper.attribute;

import java.util.Collections;
import java.util.Set;

import javax.naming.NamingException;
import javax.naming.directory.Attribute;

import com.atlassian.crowd.directory.ldap.util.GuidHelper;

import com.google.common.collect.ImmutableSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ldap.core.DirContextAdapter;

/**
 * Maps the objectGUID on an entity.
 *
 * This concept only applies to Active Directory.
 */
public class ObjectGUIDMapper implements AttributeMapper
{
    private static final Logger logger = LoggerFactory.getLogger(ObjectGUIDMapper.class);

    /**
     * Object GUID attribute name.
     */
    public static final String ATTRIBUTE_KEY = "objectGUID";

    @Override
    public String getKey()
    {
        return ATTRIBUTE_KEY;
    }

    @Override
    public Set<String> getValues(DirContextAdapter ctx) throws NamingException
    {
        Attribute attr = ctx.getAttributes().get(getKey());
        if (attr == null || attr.size() != 1)
        {
            return Collections.emptySet();
        }

        Object attrValue = attr.get(0);
        if (attrValue instanceof byte[])
        {
            return ImmutableSet.of(GuidHelper.getGUIDAsString((byte[]) attrValue));
        }
        else
        {
            logger.debug("Skipped value <{}> for attribute {} because it is not a byte array",
                    attrValue, ATTRIBUTE_KEY);
            return Collections.emptySet();
        }
    }

    @Override
    public Set<String> getRequiredLdapAttributes()
    {
        return Collections.singleton(getKey());
    }
}
