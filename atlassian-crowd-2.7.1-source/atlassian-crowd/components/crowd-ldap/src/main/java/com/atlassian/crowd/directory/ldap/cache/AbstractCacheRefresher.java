package com.atlassian.crowd.directory.ldap.cache;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.embedded.impl.IdentifierMap;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.Groups;
import com.atlassian.crowd.model.group.Membership;
import com.atlassian.crowd.util.TimedProgressOperation;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.collect.ImmutableList.copyOf;

/**
 * @since v2.1
 */
public abstract class AbstractCacheRefresher implements CacheRefresher
{
    private static final Logger log = LoggerFactory.getLogger(AbstractCacheRefresher.class);

    protected final RemoteDirectory remoteDirectory;

    public AbstractCacheRefresher(final RemoteDirectory remoteDirectory)
    {
        this.remoteDirectory = remoteDirectory;
    }

    /**
     * Remove the duplicate groups from the passed list.
     *
     * @param remoteGroups the groups to filter.
     * @param <T> the type of group to filter.
     * @return the list of groups with duplicates removed.
     */
    protected static <T extends Group> List<T> filterOutDuplicateGroups(final List<T> remoteGroups)
    {
        final Map<String, T> groupMap = Maps.newLinkedHashMap();
        final Set<String> badGroups = Sets.newHashSet();
        for (T ldapGroup : remoteGroups)
        {
            final String groupId = IdentifierUtils.toLowerCase(ldapGroup.getName());
            if (!badGroups.contains(groupId))
            {
                final Group origGroup = groupMap.put(groupId, ldapGroup);
                if (origGroup != null)
                {
                    groupMap.remove(groupId);
                    badGroups.add(groupId);
                    if (!origGroup.getName().equals(ldapGroup.getName()))
                    {
                        log.warn("group [{}] duplicated in remote directory by group [{}]. Ignoring group.",
                                origGroup.getName(), ldapGroup.getName());
                    }
                    else
                    {
                        log.warn("group [{}] duplicated in remote directory. Ignoring group.", origGroup.getName());
                    }
                }
            }
        }

        return badGroups.isEmpty() ? remoteGroups : copyOf(groupMap.values());
    }

    public void synchroniseAll(DirectoryCache directoryCache) throws OperationFailedException
    {
        synchroniseAllUsers(directoryCache);
        // Synchronise groups of type GROUP
        List<? extends Group> allGroups = synchroniseAllGroups(directoryCache);
        // We have to finish adding new Groups BEFORE we can add Nested Group memberships.
        synchroniseMemberships(allGroups, directoryCache);
    }

    protected abstract void synchroniseAllUsers(final DirectoryCache directoryCache) throws OperationFailedException;

    protected abstract List<? extends Group> synchroniseAllGroups(final DirectoryCache directoryCache) throws OperationFailedException;

    Iterable<Membership> getMemberships(Iterable<String> names) throws OperationFailedException
    {
        return remoteDirectory.getMemberships();
    }

    protected void synchroniseMemberships(final List<? extends Group> remoteGroups, final DirectoryCache directoryCache)
            throws OperationFailedException
    {
        if (log.isDebugEnabled())
        {
            log.debug("Updating memberships for " + remoteGroups.size() + " groups from " + directoryDescription());
        }

        int total = remoteGroups.size();

        Map<String, Group> groupsByName = new IdentifierMap<Group>();

        for (Group g : remoteGroups)
        {
            String name = g.getName();
            if (null != groupsByName.put(name, g))
            {
                throw new OperationFailedException("Unable to synchronise directory: duplicate groups with name '" + name + "'");
            }
        }

        TimedProgressOperation operation = new TimedProgressOperation("migrated memberships for group", total, log);

        Iterable<Membership> memberships = getMemberships(groupsByName.keySet());

        Iterator<Membership> iter = memberships.iterator();

        while (iter.hasNext())
        {
            Membership membership;

            long start = System.currentTimeMillis();
            membership = iter.next();
            long finish = System.currentTimeMillis();

            long duration = finish - start;

            log.debug("found [ " + membership.getUserNames().size() + " ] remote user-group memberships, "
                    + "[ " +  membership.getChildGroupNames().size() + " ] remote group-group memberships in [ "
                    + duration + "ms ]");

            Group g = groupsByName.get(membership.getGroupName());
            if (g == null)
            {
                log.debug("Unexpected group in response: " + membership.getGroupName());
                continue;
            }

            directoryCache.syncUserMembersForGroup(g, membership.getUserNames());

            if (remoteDirectory.supportsNestedGroups())
            {
                directoryCache.syncGroupMembersForGroup(g, membership.getChildGroupNames());
            }

            operation.incrementedProgress();
        }
    }

    protected String directoryDescription()
    {
        return remoteDirectory.getDescriptiveName() + " Directory " + remoteDirectory.getDirectoryId();
    }
}
