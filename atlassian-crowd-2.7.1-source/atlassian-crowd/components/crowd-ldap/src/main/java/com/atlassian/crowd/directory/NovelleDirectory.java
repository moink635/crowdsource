package com.atlassian.crowd.directory;

import javax.naming.directory.Attributes;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.ldap.LDAPQueryTranslater;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.crowd.util.PasswordHelper;
import com.atlassian.event.api.EventPublisher;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Novell eDirectory LDAP connector.
 *
 */
public class NovelleDirectory extends RFC4519Directory
{
    private final PasswordHelper passwordHelper;

    /**
     * @param passwordHelper password helper, which must not be null
     */
    public NovelleDirectory(LDAPQueryTranslater ldapQueryTranslater,
                            EventPublisher eventPublisher,
                            InstanceFactory instanceFactory,
                            PasswordHelper passwordHelper)
    {
        super(ldapQueryTranslater, eventPublisher, instanceFactory);
        this.passwordHelper = checkNotNull(passwordHelper);
    }

    public static String getStaticDirectoryType()
    {
        return "Novell eDirectory Server";
    }

    @Override
    public String getDescriptiveName()
    {
        return NovelleDirectory.getStaticDirectoryType();
    }
    
     /**
     * Novell eDirectory doesn't want passwords encoded before they're passed to the directory. At least, we don't currently
     * support it.
      *
      * @throws InvalidCredentialException if {@link com.atlassian.crowd.embedded.api.PasswordCredential#isEncryptedCredential()}
      *                                    returns true for the given passwordCredential
     */
    @Override
    protected String encodePassword(PasswordCredential passwordCredential) throws InvalidCredentialException
    {
        if (PasswordCredential.NONE.equals(passwordCredential))
        {
            // since we don't support setting passwords by hash, the best we can do is generate a random password instead
            return passwordHelper.generateRandomPassword();
        }
        else if (passwordCredential.isEncryptedCredential())
        {
            throw new InvalidCredentialException("Password credential must not be encrypted before being encoded");
        }
        else
        {
            return passwordCredential.getCredential();
        }
    }

    /**
     * Novell eDirectory in a default install requires the sn to be set before a user can be created.
     * @param user
     * @param attributes
     */
    @Override
    protected void getNewUserDirectorySpecificAttributes(final User user, final Attributes attributes)
    {
        addDefaultSnToUserAttributes(attributes, user.getName());
    }
}