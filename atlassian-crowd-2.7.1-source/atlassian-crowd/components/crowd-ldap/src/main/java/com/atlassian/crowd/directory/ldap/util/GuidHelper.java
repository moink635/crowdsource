package com.atlassian.crowd.directory.ldap.util;

/**
 * Util for  GUID (externalId) transformations
 *
 * @since v2.7.0
 */
public class GuidHelper
{
    /**
     * The returned representation doesn't match AD's string representation,
     * but it doesn't matter as the GUID should be treated as an opaque
     * identifier. Basically, the method is a byte array to hex string.
     * AD chooses to order the hex string in partial reverse, eg.
     *
     * Normal Hex String:     6797e1e5ecb5154f960f865c28c015fa
     * AD Formatted String:   e5e19767-b5ec-4f15-960f-865c28c015fa
     *
     * This method returns the "normal" hex string.
     *
     * @param inArr
     * @return
     */
    public static String getGUIDAsString(byte[] inArr)
    {
        StringBuffer guid = new StringBuffer();
        for (int i = 0; i < inArr.length; i++)
        {
            StringBuffer dblByte = new StringBuffer(Integer.toHexString(inArr[i] & 0xff));
            if (dblByte.length() == 1)
            {
                guid.append("0");
            }
            guid.append(dblByte);
        }
        return guid.toString();
    }
}
