package com.atlassian.crowd.search.ldap;

import com.atlassian.crowd.directory.MicrosoftActiveDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.mapper.attribute.UserAccountControlMapper;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;

import org.springframework.ldap.filter.Filter;
import org.springframework.ldap.filter.NotFilter;

/**
 * An specialisation of {@link LDAPQueryTranslaterImpl} that can translate Active Directory
 * <a href="http://support.microsoft.com/kb/269181">enabled/disabled user queries</a>.
 *
 * @since v2.7
 */
public class ActiveDirectoryQueryTranslaterImpl extends LDAPQueryTranslaterImpl
{
    private static final int UF_ACCOUNTDISABLE_MASK = MicrosoftActiveDirectory.UF_ACCOUNTDISABLE;

    @Override
    protected Filter booleanTermRestrictionAsFilter(EntityDescriptor entityDescriptor,
                                                    PropertyRestriction<Boolean> termRestriction,
                                                    LDAPPropertiesMapper ldapPropertiesMapper)
    {
        if (entityDescriptor.getEntityType() == Entity.USER && termRestriction.getProperty().equals(UserTermKeys.ACTIVE))
        {
            if (termRestriction.getValue())
            {
                // querying for enabled users
                return new NotFilter(BitwiseFilter.or(UserAccountControlMapper.ATTRIBUTE_KEY, UF_ACCOUNTDISABLE_MASK));
            }
            else
            {
                // querying for disabled users
                return BitwiseFilter.and(UserAccountControlMapper.ATTRIBUTE_KEY, UF_ACCOUNTDISABLE_MASK);
            }
        }
        else if (entityDescriptor.getEntityType() == Entity.GROUP && termRestriction.getProperty().equals(GroupTermKeys.ACTIVE))
        {
            if (termRestriction.getValue())
            {
                // groups are always active = true, so no need to add a restriction
                return new EverythingResult();
            }
            else
            {
                // no group is ever active = false, so no results
                return new NothingResult();
            }
        }
        else
        {
            // if boolean term restrictions are for anything other than the group/user active flag, then throw exception
            throw new IllegalArgumentException("Boolean restrictions for property " + termRestriction.getProperty().getPropertyName() + " are not supported");
        }
    }
}
