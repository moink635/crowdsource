package com.atlassian.crowd.directory;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.password.encoder.PasswordEncoder;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.search.ldap.LDAPQueryTranslater;
import com.atlassian.crowd.util.InstanceFactory;
import com.atlassian.event.api.EventPublisher;

import org.apache.commons.lang3.StringUtils;

abstract class ConfigurablePasswordEncodingDirectory extends RFC4519Directory
{
    private final PasswordEncoderFactory passwordEncoderFactory;

    public ConfigurablePasswordEncodingDirectory(LDAPQueryTranslater ldapQueryTranslater, EventPublisher eventPublisher, InstanceFactory instanceFactory, final PasswordEncoderFactory passwordEncoderFactory)
    {
        super(ldapQueryTranslater, eventPublisher, instanceFactory);
        this.passwordEncoderFactory = passwordEncoderFactory;
    }

    /**
     * Translates a clear-text password into an encrypted one if it isn't already encrypted, using the encryption method
     * specified by the directory settings.
     *
     * @throws com.atlassian.crowd.exception.PasswordEncoderNotFoundException (runtime exception) if the encoder for the
     *                                                                        specified encryption method cannot be found
     */
    @Override
    protected String encodePassword(PasswordCredential passwordCredential)
    {
        String encryptionAlgorithm = ldapPropertiesMapper.getUserEncryptionMethod();
        if (passwordCredential.isEncryptedCredential() || StringUtils.isBlank(encryptionAlgorithm))
        {
            return passwordCredential.getCredential();
        }
        else
        {
            PasswordEncoder passwordEncoder = passwordEncoderFactory.getLdapEncoder(encryptionAlgorithm);
            return passwordEncoder.encodePassword(passwordCredential.getCredential(), null);
        }
    }
}
