package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation;

import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.util.persistence.hibernate.batch.HibernateOperation;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

/**
 * An operation to remove a group and its related information such as members, memberships, and attributes.
 * When given an Object of type {@link InternalGroup}, this should act the same as calling
 * {@link com.atlassian.crowd.dao.group.GroupDAOHibernate#remove(com.atlassian.crowd.model.group.Group)} directly.
 */
public class RemoveGroupOperation implements HibernateOperation<Session>
{
    @Override
    public void performOperation(Object object, Session session) throws HibernateException
    {
        InternalGroup group = (InternalGroup) object;

        // remove group members
        session.getNamedQuery("removeAllEntityMembers")
                .setString("entityName", toLowerCase(group.getName()))
                .setLong("directoryId", group.getDirectoryId())
                .executeUpdate();

        // remove group memberships
        session.getNamedQuery("removeAllEntityMemberships")
                .setString("entityName", toLowerCase(group.getName()))
                .setLong("directoryId", group.getDirectoryId())
                .setParameter("membershipType", MembershipType.GROUP_GROUP)
                .executeUpdate();

        // remove group attributes
        session.getNamedQuery("removeAllInternalGroupAttributes")
                .setEntity("group", group)
                .executeUpdate();

        // remove group
        session.delete(group);
    }
}
