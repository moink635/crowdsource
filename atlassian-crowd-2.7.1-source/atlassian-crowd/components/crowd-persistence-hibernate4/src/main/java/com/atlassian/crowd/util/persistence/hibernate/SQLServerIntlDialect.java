package com.atlassian.crowd.util.persistence.hibernate;

import org.hibernate.dialect.SQLServerDialect;

import java.sql.Types;

/**
 * A dialect for MS SQL Server 2000 & 2005, which supports unicode characters (CWD-1070)
 */
public class SQLServerIntlDialect extends SQLServerDialect
{
    public SQLServerIntlDialect()
    {
        registerColumnType(Types.CHAR, "nchar(1)");
        registerColumnType(Types.VARCHAR, "nvarchar($l)");
        registerColumnType(Types.CLOB, "ntext");
    }
}