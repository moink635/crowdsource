package com.atlassian.crowd.util.persistence.hibernate;

import net.sf.ehcache.CacheManager;
import org.hibernate.cache.ehcache.EhCacheRegionFactory;
import org.hibernate.cfg.Settings;
import org.springframework.core.Conventions;

import java.util.Properties;

/**
 * A simple wrapper around {@code EhCacheRegionFactory}, part of the {@code hibernate-ehcache} module, which extracts
 * the {@code CacheManager} from properties provided to the constructor rather than initialising one on the fly.
 * <p/>
 * This class <i>requires</i> that the {@link ConfigurableLocalSessionFactoryBean} be used to create the Hibernate
 * {@code SessionFactory}, as there is currently no other way to get the {@code CacheManager} set as the required
 * property in order to be pulled out by the constructor. Long term, Crowd needs to move away from this approach as
 * it is not very compatible with how Hibernate deals with caching.
 */
public class EhCacheProvider extends EhCacheRegionFactory
{
    public static final String PROP_CACHE_MANAGER = Conventions.getQualifiedAttributeName(EhCacheProvider.class, "CacheManager");

    public EhCacheProvider(Properties properties)
    {
        manager = (CacheManager) properties.get(PROP_CACHE_MANAGER);
    }

    /**
     * Suppresses the initialisation for the {@code CacheManager} that happens in the base class; the cache manager
     * is provided to the constructor of this region factory, so it doesn't need to be initialised here.
     *
     * @param settings   ignored
     * @param properties ignored
     */
    public void start(Settings settings, Properties properties)
    {
        //This space intentionally left blank
    }
}
