package com.atlassian.crowd.dao.alias;

import com.atlassian.crowd.dao.CriteriaFactory;
import com.atlassian.crowd.model.alias.Alias;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.hibernate.HQLQuery;
import com.atlassian.crowd.search.hibernate.HQLQueryTranslater;
import com.atlassian.crowd.search.hibernate.HibernateSearchResultsTransformer;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.persistence.hibernate.HibernateDao;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.ListIterator;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

public class AliasDAOHibernate extends HibernateDao implements AliasDAO
{
    private HQLQueryTranslater hqlQueryTranslater;

    public String findAliasByUsername(Application application, String username)
    {
        Alias aliasObject = findAliasObjectByUsername(application, username);
        return aliasObject == null ? null : aliasObject.getAlias();
    }

    public List<Alias> findAll()
    {
        return CriteriaFactory.createCriteria(session(), getPersistentClass()).list();
    }

    public String findUsernameByAlias(Application application, String alias)
    {
        Alias aliasObject = findAliasObjectByAlias(application, alias);
        return aliasObject == null ? null : aliasObject.getName();
    }

    public Class getPersistentClass()
    {
        return Alias.class;
    }

    public void removeAlias(Application application, String username)
    {
        Alias aliasObject = findAliasObjectByUsername(application, username);

        if (aliasObject != null)
        {
            remove(aliasObject);
        }
    }

    public void removeAliases(Application application)
    {
        session().getNamedQuery("removeAllAliasesForApplication")
                .setEntity("application", application)
                .executeUpdate();
    }

    public List<String> search(EntityQuery<String> entityQuery)
    {
        if (entityQuery.getEntityDescriptor().getEntityType() != Entity.ALIAS)
        {
            throw new IllegalArgumentException("AliasDAO can only evaluate EntityQueries for Entity.ALIAS");
        }
        HQLQuery hqlQuery = hqlQueryTranslater.asHQL(entityQuery);

        Query hibernateQuery = createHibernateQuery(entityQuery, hqlQuery);

        return HibernateSearchResultsTransformer.transformResults(hibernateQuery.list());
    }

    public void storeAlias(Application application, String username, String alias)
    {
        Validate.notNull(application, "Application cannot be null");

        if (StringUtils.isBlank(username))
        {
            throw new IllegalArgumentException("Username cannot be blank");
        }

        if (StringUtils.isBlank(alias))
        {
            throw new IllegalArgumentException("Alias cannot be blank");
        }

        Alias aliasObject = findAliasObjectByUsername(application, username);

        if (aliasObject != null)
        {
            aliasObject.setAlias(alias);
        }
        else
        {
            aliasObject = new Alias(application, username, alias);
        }

        saveOrUpdate(aliasObject);
    }

    private Alias findAliasObjectByAlias(Application application, String alias)
    {
        return (Alias) CriteriaFactory.createCriteria(session(), getPersistentClass())
                .add(Restrictions.eq("application", application))
                .add(Restrictions.eq("lowerAlias", toLowerCase(alias)))
                .uniqueResult();
    }

    private Alias findAliasObjectByUsername(Application application, String username)
    {
        return (Alias) CriteriaFactory.createCriteria(session(), getPersistentClass())
                .add(Restrictions.eq("application", application))
                .add(Restrictions.eq("lowerName", toLowerCase(username)))
                .uniqueResult();
    }

    @Autowired
    public void setHqlQueryTranslater(HQLQueryTranslater hqlQueryTranslater)
    {
        this.hqlQueryTranslater = hqlQueryTranslater;
    }
}
