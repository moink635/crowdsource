package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation;

import com.atlassian.crowd.util.persistence.hibernate.batch.HibernateOperation;

import org.hibernate.HibernateException;
import org.hibernate.ReplicationMode;
import org.hibernate.Session;

/**
 * Hibernate Operation to replicate entities in batch
 */
public class ReplicateOperation implements HibernateOperation<Session>
{
    private final ReplicationMode replicationMode;

    public ReplicateOperation(ReplicationMode replicationMode)
    {
        this.replicationMode = replicationMode;
    }

    @Override
    public void performOperation(Object object, Session session) throws HibernateException
    {
        session.replicate(object, replicationMode);
    }
}