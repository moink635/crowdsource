package com.atlassian.crowd.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;

public class CriteriaFactory
{
    public static Criteria createCriteria(Session session, Class type)
    {
        return session.createCriteria(type)
                .setCacheable(true);
    }
}
