/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.util.persistence.hibernate;

import java.util.ListIterator;

import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.search.hibernate.HQLQuery;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchFinder;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;

/**
 * Generic persistence class for storing Hibernate persistence objects.
 */
public abstract class HibernateDao
{
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected BatchFinder batchFinder;
    protected BatchProcessor<Session> batchProcessor;
    protected SessionFactory sessionFactory;

    /**
     * All subclasses of HibernateDao <em>must<em> implement this method for {@link #load(long)} to work correctly.
     *
     * @return the entity class for this DAO
     */
    public abstract Class getPersistentClass();

    /**
     * Loads a persisted entity from the persistence store.
     *
     * @param persistentClass The entity type to load
     * @param id              The unique identifier of the object to load from the persistence store.
     * @return The populated object from the database.
     * @throws com.atlassian.crowd.exception.ObjectNotFoundException
     *          when the requested entity is not found.
     */
    public <T> T load(Class<T> persistentClass, long id) throws ObjectNotFoundException
    {
        T obj = persistentClass.cast(session().get(persistentClass, id));
        if (obj == null)
        {
            throw new ObjectNotFoundException(getPersistentClass(), id);
        }
        logger.trace("Loaded object: {}", obj);

        return obj;
    }

    /**
     * Loads a persisted entity from the persistence store.
     *
     * @param id The unique identifier of the object to load from the persistence store.
     * @return The populated object from the database.
     * @throws com.atlassian.crowd.exception.ObjectNotFoundException
     *          when the requested entity is not found.
     */
    public Object load(long id) throws ObjectNotFoundException
    {
        return load(getPersistentClass(), id);
    }

    /**
     * This method calls the <code>session.load</code> method to
     * obtain a proxy (or actual instance if the object is in session)
     * by NOT hitting the database immediately.
     * <p/>
     * Do NOT call this method unless you are SURE that the object
     * with the supplied identifier exists.
     *
     * @param persistentClass the type of the entity to load
     * @param id              unique identifier to load.
     * @return a proxy object (or actual instance if the object is in session)
     */
    public <T> T loadReference(Class<T> persistentClass, long id)
    {
        return persistentClass.cast(session().load(persistentClass, id));
    }

    /**
     * This method calls the <code>session.load</code> method to
     * obtain a proxy (or actual instance if the object is in session)
     * by NOT hitting the database immediately.
     * <p/>
     * Do NOT call this method unless you are SURE that the object
     * with the supplied identifier exists.
     *
     * @param id unique identifier to load.
     * @return a proxy object (or actual instance if the object is in session)
     */
    public Object loadReference(long id)
    {
        return loadReference(getPersistentClass(), id);
    }

    /**
     * Removes the DAO object from the persistence store.
     *
     * @param persistentObject The object to remove.
     * @throws org.springframework.dao.DataAccessException
     *          A persistence exception has occurred.
     */
    public void remove(Object persistentObject) throws DataAccessException
    {
        logger.debug("Deleting object: {}", persistentObject);

        session().delete(persistentObject);
    }

    /**
     * Saves a new DAO object to the persistence store.
     *
     * @param persistentObject The object to save.
     * @throws org.springframework.dao.DataAccessException
     *          A persistence exception has occurred.
     */
    public void save(Object persistentObject) throws DataAccessException
    {
        logger.debug("Saving object: {}", persistentObject);

        session().save(persistentObject);
    }

    /**
     * Saves or updates DAO object to the persistence store.
     *
     * @param persistentObject The object to save or update.
     * @throws org.springframework.dao.DataAccessException
     *          A persistence exception has occurred.
     */
    public void saveOrUpdate(Object persistentObject) throws DataAccessException
    {
        logger.debug("Saving or updating object: {}", persistentObject);

        session().saveOrUpdate(persistentObject);
    }

    @Autowired
    public void setBatchFinder(BatchFinder batchFinder)
    {
        this.batchFinder = batchFinder;
    }

    @Autowired
    public void setBatchProcessor(BatchProcessor batchProcessor)
    {
        this.batchProcessor = batchProcessor;
    }

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    /**
     * Updates an existing DAO object, if the object does not exist it will be added to the persistence store.
     *
     * @param persistentObject The object to update.
     * @throws org.springframework.dao.DataAccessException
     *          A persistence exception has occurred.
     */
    public void update(Object persistentObject) throws DataAccessException
    {
        logger.debug("Updating object: {}", persistentObject);

        session().saveOrUpdate(persistentObject);
    }

    protected Session session()
    {
        return sessionFactory.getCurrentSession();
    }

    /**
     * Creates a Hibernate {@link Query} equivalent to the provided Crowd {@link com.atlassian.crowd.embedded.api.Query}
     * and {@link HQLQuery}.
     *
     * @param crowdQuery
     * @param hqlQuery
     * @return a Hibernate query
     */
    protected Query createHibernateQuery(com.atlassian.crowd.embedded.api.Query crowdQuery, HQLQuery hqlQuery)
    {
        Query hibernateQuery = session().createQuery(hqlQuery.toString())
                .setFirstResult(crowdQuery.getStartIndex());
        if (crowdQuery.getMaxResults() != EntityQuery.ALL_RESULTS)
        {
            hibernateQuery.setMaxResults(crowdQuery.getMaxResults());
        }

        for (ListIterator iterator = hqlQuery.getParameterValues().listIterator(); iterator.hasNext(); )
        {
            Object value = iterator.next();
            hibernateQuery.setParameter(generatedJpaParameterNameForListIndex(iterator), value);
        }
        return hibernateQuery;
    }

    /**
     * Generates a JPA-style positional parameter name for element at the current position of the iterator,
     * i.e., the result of the last call to {@link java.util.ListIterator#next()}.
     *
     * @param iterator the iterator that is being used to iterate over the positional parameter values
     * @return a JPA-style positional parameter name, e.g., ?1, ?2, ?3...
     */
    private static String generatedJpaParameterNameForListIndex(ListIterator iterator)
    {
        return Integer.toString(iterator.nextIndex());   // JPA positional parameters start at index 1
    }
}
