package com.atlassian.crowd.util.persistence.hibernate;

import com.atlassian.config.db.HibernateConfig;
import net.sf.ehcache.CacheManager;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.service.ServiceRegistry;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

/**
 * Extends Spring's Hibernate 4-compatible {@code LocalSessionFactoryBean} with properties to restore various bits of
 * functionality that were present in their Hibernate 3 version, as well as to add new Crowd-specific functionality.
 * <ul>
 *     <li>{@link #setCacheManager(net.sf.ehcache.CacheManager) Setting a CacheManager}, when used in conjunction with
 *     {@link EhCacheProvider}, sets the {@code CacheManager} which should be used by that provider</li>
 *     <li>{@link #setEventListeners(java.util.Map) Setting event listeners}, uses the new Hibernate 4 mechanism to
 *     register event listeners via the {@code ServiceRegistry}</li>
 *     <li>{@link #setHibernateConfig(com.atlassian.config.db.HibernateConfig) Setting a HibernateConfig} merges any
 *     properties defined in the provided configuration into the properties used to initialise Hibernate</li>
 *     <li>{@link #setMappingResourcesBean(MappingResources) Setting mapping resources} retrieves mappings as an array
 *     from the configured {@link MappingResources} and includes them when initialising Hibernate. Note this
 *     property is named differently from the one in the parent class, since the Bean specification does not
 *     allow overloaded properties.</li>
 * </ul>
 */
public class ConfigurableLocalSessionFactoryBean extends LocalSessionFactoryBean
{
    private CacheManager cacheManager;
    private Map<String, Object> eventListeners;
    private HibernateConfig hibernateConfig;
    private MappingResources mappingResourcesBean;

    @Override
    public void afterPropertiesSet() throws IOException
    {
        // Set this to null JIC there is a value held, which the LocalSessionFactoryBean will
        // use to init the config
        setConfigLocations(null);

        // Add any properties from HibernateConfig to any values configured using hibernateProperties
        if (hibernateConfig != null)
        {
            getHibernateProperties().putAll(hibernateConfig.getHibernateProperties());
        }

        if (cacheManager != null)
        {
            //Use the Map<Object, Object> underpinnings of Properties to store the cache manager under the key that
            //is expected by the Crowd EhCacheProvider
            getHibernateProperties().put(EhCacheProvider.PROP_CACHE_MANAGER, cacheManager);
        }

        super.setMappingResources(mappingResourcesBean.getMappingsAsArray());
        super.afterPropertiesSet();
    }

    @Override
    protected SessionFactory buildSessionFactory(LocalSessionFactoryBuilder sfb)
    {
        SessionFactory sessionFactory = super.buildSessionFactory(sfb);

        registerEventListeners((SessionFactoryImplementor) sessionFactory);

        return sessionFactory;
    }

    @SuppressWarnings("unchecked")
    private void registerEventListeners(SessionFactoryImplementor sessionFactory)
    {
        if (!(eventListeners == null || eventListeners.isEmpty()))
        {
            ServiceRegistry registry = sessionFactory.getServiceRegistry();
            EventListenerRegistry listeners = registry.getService(EventListenerRegistry.class);

            for (Map.Entry<String, Object> entry : eventListeners.entrySet())
            {
                EventType type = EventType.resolveEventTypeByName(entry.getKey());

                Object value = entry.getValue();
                if (value instanceof Collection)
                {
                    listeners.setListeners(type, ((Collection<?>) value).toArray());
                }
                else
                {
                    listeners.setListeners(type, value);
                }
            }
        }
    }

    public void setCacheManager(CacheManager cacheManager)
    {
        this.cacheManager = cacheManager;
    }

    public void setEventListeners(Map<String, Object> eventListeners)
    {
        this.eventListeners = eventListeners;
    }

    public void setHibernateConfig(HibernateConfig hibernateConfig)
    {
        this.hibernateConfig = hibernateConfig;
    }

    public void setMappingResourcesBean(MappingResources mappingResourcesBean)
    {
        this.mappingResourcesBean = mappingResourcesBean;
    }
}