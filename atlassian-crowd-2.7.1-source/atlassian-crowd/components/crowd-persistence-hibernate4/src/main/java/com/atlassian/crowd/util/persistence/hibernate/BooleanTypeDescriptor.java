package com.atlassian.crowd.util.persistence.hibernate;

import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.AbstractTypeDescriptor;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

/**
 * An {@code AbstractTypeDescriptor} which maps Java {@code Boolean} values to {@code String} values containing either
 * {@code "true"} or {@code "false"}.
 * <p/>
 * By default, Hibernate includes types which manage {@code Boolean}s as {@code Number}s or {@code char}s (containing
 * {@code "T"}/{@code "Y"} for {@code true} and {@code "F"}/{@code "N"} for {@code false}). This type provides backward
 * compatibility for behaviour applied by previous versions of Crowd.
 */
public class BooleanTypeDescriptor extends AbstractTypeDescriptor<Boolean>
{
    public static final BooleanTypeDescriptor INSTANCE = new BooleanTypeDescriptor();

    private final String stringValueFalse;
    private final String stringValueTrue;

    public BooleanTypeDescriptor()
    {
        super(Boolean.class);

        stringValueTrue = String.valueOf(true);
        stringValueFalse = String.valueOf(false);
    }

    public Boolean fromString(String string)
    {
        return Boolean.valueOf(string);
    }

    public String toString(Boolean value)
    {
        return value == null ? null : value.toString();
    }

    @SuppressWarnings({"unchecked"})
    public <X> X unwrap(Boolean value, Class<X> type, WrapperOptions options)
    {
        if (value == null)
        {
            return null;
        }
        if (Boolean.class.isAssignableFrom(type))
        {
            return (X) value;
        }
        if (String.class.isAssignableFrom(type))
        {
            return (X) (value ? stringValueTrue : stringValueFalse);
        }
        throw unknownUnwrap(type);
    }

    public <X> Boolean wrap(X value, WrapperOptions options)
    {
        if (value == null)
        {
            return null;
        }
        if (value instanceof Boolean)
        {
            return (Boolean) value;
        }
        if (value instanceof String)
        {
            return stringValueTrue.equals(value) ? TRUE : FALSE;
        }
        throw unknownWrap(value.getClass());
    }
}
