package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation;

import com.atlassian.crowd.util.persistence.hibernate.batch.HibernateOperation;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Hibernate Operation to merge entities in batch
 * <p/>
 * NOTE: don't use merge for entities without single IDs.
 */
public class MergeOperation implements HibernateOperation<Session>
{
    private static final Logger log = LoggerFactory.getLogger(MergeOperation.class);

    @Override
    public void performOperation(Object object, Session session) throws HibernateException
    {
        log.debug("Merging {}", object);
        session.merge(object);
    }
}