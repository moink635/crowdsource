package com.atlassian.crowd.util.persistence.hibernate;

import java.util.Properties;

/**
 * This class is now an alias for {@link com.atlassian.hibernate.extras.type.GenericEnumUserType GenericEnumUserType}
 * from the {@code atlassian-hibernate-extras} library. It is retained in Crowd only for backward compatibility.
 * <p/>
 * In addition to providing backward compatibility for the class being referenced, this class also applies the
 * historical Crowd defaults for the {@code identifierMethod} and {@code valueOf} methods. In the base class, the
 * defaults are "getId" and "fromId", respectively; for this class, they are "name" and "valueOf". While the defaults
 * used here are satisfied automatically by every Java enumeration, they are weak for refactoring; renaming an enum
 * constant requires a database upgrade task to adjust previous rows.
 */
public class GenericEnumUserType extends com.atlassian.hibernate.extras.type.GenericEnumUserType
{
    public static final String DEFAULT_IDENTIFIER_METHOD_NAME = "name";
    public static final String DEFAULT_VALUE_OF_METHOD_NAME = "valueOf";

    @Override
    public void setParameterValues(Properties parameters)
    {
        //The default identifierMethod and valueOfMethods for the base class do not match what they were in Crowd, so
        //the defaults need to be adjusted. However, the semantics are the same so if there is already a value set for
        //either property it should not be modified.
        setPropertyIfAbsent(parameters, "identifierMethod", DEFAULT_IDENTIFIER_METHOD_NAME);
        setPropertyIfAbsent(parameters, "valueOfMethod", DEFAULT_VALUE_OF_METHOD_NAME);

        super.setParameterValues(parameters);
    }

    private void setPropertyIfAbsent(Properties properties, String name, String value)
    {
        if (!properties.containsKey(name))
        {
            properties.setProperty(name, value);
        }
    }
}