package com.atlassian.crowd.dao.directory;

import com.atlassian.crowd.dao.CriteriaFactory;
import com.atlassian.crowd.dao.group.InternalGroupDao;
import com.atlassian.crowd.dao.user.InternalUserDao;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.hibernate.HQLQuery;
import com.atlassian.crowd.search.hibernate.HQLQueryTranslater;
import com.atlassian.crowd.search.hibernate.HibernateSearchResultsTransformer;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.persistence.hibernate.HibernateDao;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.ListIterator;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

public class DirectoryDAOHibernate extends HibernateDao implements DirectoryDao
{
    private InternalGroupDao groupDao;
    private HQLQueryTranslater hqlQueryTranslater;
    private InternalUserDao userDao;

    public DirectoryImpl add(final Directory directory)
    {
        DirectoryImpl directoryToPersist = new DirectoryImpl(directory);

        directoryToPersist.setCreatedDateToNow();
        directoryToPersist.setUpdatedDateToNow();

        directoryToPersist.validate();

        super.save(directoryToPersist);

        return directoryToPersist;
    }

    @SuppressWarnings({"unchecked"})
    public List<Directory> findAll()
    {
        return session().createQuery("from DirectoryImpl")
                .list();
    }

    public DirectoryImpl findById(final long id) throws DirectoryNotFoundException
    {
        try
        {
            return (DirectoryImpl) load(id);
        }
        catch (ObjectNotFoundException e)
        {
            throw new DirectoryNotFoundException(id);
        }
    }

    public DirectoryImpl findByName(final String name) throws DirectoryNotFoundException
    {
        Object result = CriteriaFactory.createCriteria(session(), getPersistentClass())
                .add(Restrictions.eq("lowerName", toLowerCase(name)))
                .uniqueResult();
        if (result == null)
        {
            throw new DirectoryNotFoundException(name);
        }

        return (DirectoryImpl) result;
    }

    public Class getPersistentClass()
    {
        return DirectoryImpl.class;
    }

    public void remove(final Directory directory) throws DirectoryNotFoundException
    {
        // remove all groups
        groupDao.removeAll(directory.getId());

        // remove all users
        userDao.removeAll(directory.getId());

        // remove the directory
        super.remove(directory);
    }

    public List<Directory> search(final EntityQuery query)
    {
        if (query.getEntityDescriptor().getEntityType() != Entity.DIRECTORY)
        {
            throw new IllegalArgumentException("DirectoryDAO can only evaluate EntityQueries for Entity.DIRECTORY");
        }
        HQLQuery hqlQuery = hqlQueryTranslater.asHQL(query);

        Query hibernateQuery = createHibernateQuery(query, hqlQuery);

        return HibernateSearchResultsTransformer.transformResults(hibernateQuery.list());
    }

    public DirectoryImpl update(final Directory directory) throws DirectoryNotFoundException
    {
        DirectoryImpl directoryToUpdate = findById(directory.getId());
        directoryToUpdate.setUpdatedDateToNow();
        directoryToUpdate.updateDetailsFrom(directory);
        directoryToUpdate.validate();

        super.update(directoryToUpdate);

        return directoryToUpdate;
    }

    @Autowired
    public void setGroupDao(InternalGroupDao groupDao)
    {
        this.groupDao = groupDao;
    }

    @Autowired
    public void setHqlQueryTranslater(HQLQueryTranslater hqlQueryTranslater)
    {
        this.hqlQueryTranslater = hqlQueryTranslater;
    }

    @Autowired
    public void setUserDao(InternalUserDao userDao)
    {
        this.userDao = userDao;
    }
}
