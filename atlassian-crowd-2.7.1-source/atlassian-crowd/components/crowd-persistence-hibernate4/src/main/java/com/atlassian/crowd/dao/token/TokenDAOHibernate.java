package com.atlassian.crowd.dao.token;

import com.atlassian.crowd.dao.CriteriaFactory;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.exception.ObjectAlreadyExistsException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.hibernate.HQLQuery;
import com.atlassian.crowd.search.hibernate.HQLQueryTranslater;
import com.atlassian.crowd.search.hibernate.HibernateSearchResultsTransformer;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.TokenTermKeys;
import com.atlassian.crowd.util.persistence.hibernate.HibernateDao;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataRetrievalFailureException;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public class TokenDAOHibernate extends HibernateDao implements TokenDAO
{
    private HQLQueryTranslater hqlQueryTranslater;

    @Override
    public Token add(Token token) throws ObjectAlreadyExistsException
    {
        session().flush(); // flush before to ensure a clean state
        try
        {
            super.save(token);
            session().flush();  // to check database constraints
            return token;
        }
        catch (HibernateException e)
        {
            throw new ObjectAlreadyExistsException(token.getIdentifierHash());
        }
    }

    public Token findByID(long ID) throws ObjectNotFoundException
    {
        return (Token) load(ID);
    }

    @Override
    public Token findByIdentifierHash(final String identifierHash) throws ObjectNotFoundException
    {
        Object result = CriteriaFactory.createCriteria(session(), getPersistentClass())
                .add(Restrictions.eq("identifierHash", identifierHash))
                .uniqueResult();
        if (result == null)
        {
            throw new ObjectNotFoundException(getPersistentClass(), identifierHash);
        }
        return (Token) result;
    }

    @Override
    public Token findByRandomHash(final String randomHash) throws ObjectNotFoundException
    {
        Object result = CriteriaFactory.createCriteria(session(), getPersistentClass())
                .add(Restrictions.eq("randomHash", randomHash))
                .uniqueResult();
        if (result == null)
        {
            throw new ObjectNotFoundException(getPersistentClass(), randomHash);
        }
        return (Token) result;
    }

    @Override
    public Class getPersistentClass()
    {
        return Token.class;
    }

    /**
     * {@see TokenDAOPersistence.loadAll()}
     */
    @Override
    public Collection<Token> loadAll()
    {
        return CriteriaFactory.createCriteria(session(), getPersistentClass()).list();
    }

    @Override
    public void remove(final Token token)
    {
        session().getNamedQuery("removeToken")
                .setEntity("token", token)
                .executeUpdate();
        session().flush();
    }

    @Override
    public void remove(final long directoryId, final String name)
    {
        session().getNamedQuery("removeTokensByDirectoryAndName")
                .setLong("directoryId", directoryId)
                .setString("name", name)
                .executeUpdate();
    }

    @Override
    public void removeExcept(long directoryId, String name, String exclusionToken)
    {
        SearchRestriction restriction = Combine.allOf(
                Restriction.on(TokenTermKeys.DIRECTORY_ID).exactlyMatching(directoryId),
                Restriction.on(TokenTermKeys.NAME).exactlyMatching(name)
        );

        EntityQuery<Token> query = QueryBuilder.queryFor(Token.class, EntityDescriptor.token()).with(restriction).startingAt(0).returningAtMost(EntityQuery.ALL_RESULTS);

        for (Token t : search(query))
        {
            if (t.getRandomHash().equals(exclusionToken))
            {
                // Leave it
            }
            else
            {
                remove(t);
            }
        }
    }

    @Override
    public void removeExpiredTokens(final Date currentTime, final long maxLifeInSeconds)
    {
        int removed = session().getNamedQuery("removeExpiredTokens")
                .setLong("maxLifeInSeconds", maxLifeInSeconds)
                .setLong("currentTime", currentTime.getTime())
                .executeUpdate();
        logger.debug("removeExpiredTokens removed tokens: {}", removed);
    }

    @Override
    public void removeAll(final long directoryId)
    {
        session().getNamedQuery("removeTokensByDirectory")
                .setLong("directoryId", directoryId)
                .executeUpdate();
    }

    /**
     * {@see TokenDAOPersistence.removeAll()}
     */
    @Override
    public void removeAll()
    {
        int removed = session().getNamedQuery("removeAllTokens")
                .executeUpdate();
        logger.debug("removeAll removed tokens: {}", removed);
    }

    /**
     * {@see TokenDAOPersistence.saveAll()}
     */
    @Override
    public void saveAll(Collection<Token> tokens)
    {
        if (tokens == null)
        {
            throw new DataRetrievalFailureException("Unable to save an empty collection of tokens");
        }

        for (Token token : tokens)
        {
            save(token);
        }
    }

    @Override
    public List<Token> search(final EntityQuery<? extends Token> query)
    {
        if (query.getEntityDescriptor().getEntityType() != Entity.TOKEN)
        {
            throw new IllegalArgumentException("TokenDAO can only evaluate EntityQueries for Entity.TOKEN");
        }
        HQLQuery hqlQuery = hqlQueryTranslater.asHQL(query);

        Query hibernateQuery = createHibernateQuery(query, hqlQuery);

        return HibernateSearchResultsTransformer.transformResults(hibernateQuery.list());
    }

    @Override
    public Token update(Token token)
    {
        token.setLastAccessedTime(System.currentTimeMillis());

        super.update(token);

        return token;
    }

    @Autowired
    public void setHqlQueryTranslater(HQLQueryTranslater hqlQueryTranslater)
    {
        this.hqlQueryTranslater = hqlQueryTranslater;
    }
}
