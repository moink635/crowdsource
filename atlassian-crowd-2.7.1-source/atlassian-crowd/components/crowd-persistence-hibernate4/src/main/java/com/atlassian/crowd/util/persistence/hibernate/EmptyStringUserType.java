package com.atlassian.crowd.util.persistence.hibernate;

/**
 * This class is now an alias for {@link com.atlassian.hibernate.extras.type.EmptyStringUserType EmptyStringUserType}
 * from the {@code atlassian-hibernate-extras} library. It is retained in Crowd only for backward compatibility.
 */
public class EmptyStringUserType extends com.atlassian.hibernate.extras.type.EmptyStringUserType
{
    //All implementation is supplied by the base class.
}