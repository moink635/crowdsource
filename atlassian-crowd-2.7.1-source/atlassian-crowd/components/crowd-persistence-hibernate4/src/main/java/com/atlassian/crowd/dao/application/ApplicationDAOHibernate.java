package com.atlassian.crowd.dao.application;

import com.atlassian.crowd.dao.CriteriaFactory;
import com.atlassian.crowd.dao.alias.AliasDAO;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.application.*;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.hibernate.HQLQuery;
import com.atlassian.crowd.search.hibernate.HQLQueryTranslater;
import com.atlassian.crowd.search.hibernate.HibernateSearchResultsTransformer;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.persistence.hibernate.HibernateDao;
import org.apache.commons.lang3.Validate;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static com.google.common.base.Preconditions.checkArgument;
import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class ApplicationDAOHibernate extends HibernateDao implements ApplicationDAO
{
    private AliasDAO aliasDao;
    private DirectoryDao directoryDao;
    private HQLQueryTranslater hqlQueryTranslater;

    public ApplicationImpl add(Application application, PasswordCredential passwordCredential)
    {
        ApplicationImpl internalApplication = ApplicationImpl.newInstance(application);
        internalApplication.setCredential(passwordCredential);

        //internalApplication.updateDetailsFromApplication(application);
        // Update dates
        internalApplication.setCreatedDateToNow();
        internalApplication.setUpdatedDateToNow();

        // For the moment assume that we have set the password correctly.
        internalApplication.setAttribute(ApplicationAttributeConstants.ATTRIBUTE_KEY_ATLASSIAN_SHA1_APPLIED, Boolean.TRUE.toString());

        internalApplication.validate();

        super.save(internalApplication);

        return internalApplication;
    }

    public void addDirectoryMapping(long applicationId, long directoryId, boolean allowAllToAuthenticate, OperationType... operationTypes)
            throws DirectoryNotFoundException, ApplicationNotFoundException
    {
        ApplicationImpl application = findById(applicationId);

        Directory directory = directoryDao.findById(directoryId);

        application.addDirectoryMapping(directory, allowAllToAuthenticate, operationTypes);
        application.setUpdatedDateToNow();

        super.update(application);
    }

    public void addGroupMapping(long applicationId, long directoryId, String groupName)
            throws ApplicationNotFoundException
    {
        checkArgument(isNotBlank(groupName));
        ApplicationImpl application = findById(applicationId);
        application.addGroupMapping(directoryId, groupName);

        super.update(application);
    }

    public void addRemoteAddress(long applicationId, RemoteAddress remoteAddress)
            throws ApplicationNotFoundException
    {
        Validate.notNull(remoteAddress);

        ApplicationImpl application = findById(applicationId);
        application.addRemoteAddress(remoteAddress.getAddress());
        application.setUpdatedDateToNow();

        super.update(application);
    }

    public List<Application> findAuthorisedApplications(long directoryId, List<String> groupNames)
    {
        if (groupNames == null || groupNames.isEmpty())
        {
            // we need to special case this because of hibernate's incompetence in dealing with empty 'in' clauses
            return session().getNamedQuery("findAuthorisedApplicationsWithoutGroupNames")
                    .setLong("directoryId", directoryId)
                    .list();
        }
        else
        {
            return session().getNamedQuery("findAuthorisedApplications")
                    .setLong("directoryId", directoryId)
                    .setParameterList("groupNames", groupNames)
                    .list();
        }
    }

    public ApplicationImpl findById(long id) throws ApplicationNotFoundException
    {
        try
        {
            return (ApplicationImpl) load(id);
        }
        catch (ObjectNotFoundException e)
        {
            throw new ApplicationNotFoundException(id);
        }
    }

    public ApplicationImpl findByName(String name) throws ApplicationNotFoundException
    {
        Object result = CriteriaFactory.createCriteria(session(), getPersistentClass())
                .add(Restrictions.eq("lowerName", toLowerCase(name)))
                .uniqueResult();
        if (result == null)
        {
            throw new ApplicationNotFoundException(name);
        }

        return (ApplicationImpl) result;
    }

    public Class getPersistentClass()
    {
        return ApplicationImpl.class;
    }

    public void remove(Application application)
    {
        aliasDao.removeAliases(application);

        super.remove(application);
    }

    public void removeDirectoryMapping(long applicationId, long directoryId) throws ApplicationNotFoundException
    {
        ApplicationImpl application = findById(applicationId);

        application.removeDirectoryMapping(directoryId);

        application.setUpdatedDateToNow();
        super.update(application);
    }

    public void removeDirectoryMappings(long directoryId)
    {
        // can't just do a HQL delete because DirectoryMapping has dependent association "allowedOperations"
        // therefore we need to manually do the delete so that hibernate performs cascade-delete
        List<ApplicationImpl> applications = session().getNamedQuery("findApplicationsWithDirectoryMapping")
                .setLong("directoryId", directoryId)
                .list();

        for (ApplicationImpl application : applications)
        {
            application.removeDirectoryMapping(directoryId);
            application.setUpdatedDateToNow();
            super.update(application);
        }
    }

    public void removeGroupMapping(long applicationId, long directoryId, String groupName)
        throws ApplicationNotFoundException
    {
        ApplicationImpl application = findById(applicationId);
        application.getDirectoryMapping(directoryId).removeGroupMapping(groupName);
        super.update(application);
    }

    public void removeGroupMappings(long directoryId, String groupName)
    {
        session().getNamedQuery("removeAllGroupMappings")
                .setLong("directoryId", directoryId)
                .setString("groupName", groupName)
                .executeUpdate();
    }

    public void removeRemoteAddress(long applicationId, RemoteAddress remoteAddress)
            throws ApplicationNotFoundException
    {
        Validate.notNull(remoteAddress);

        ApplicationImpl application = findById(applicationId);
        application.removeRemoteAddress(remoteAddress.getAddress());

        super.update(application);
    }

    public void renameGroupMappings(long directoryId, String oldGroupName, String newGroupName)
    {
        session().getNamedQuery("renameGroupMappings")
                .setLong("directoryId", directoryId)
                .setString("oldGroupName", oldGroupName)
                .setString("newGroupName", newGroupName)
                .executeUpdate();
    }

    public List<Application> search(EntityQuery<Application> query)
    {
        if (query.getEntityDescriptor().getEntityType() != Entity.APPLICATION)
        {
            throw new IllegalArgumentException("ApplicationDAO can only evaluate EntityQueries for Entity.APPLICATION");
        }
        HQLQuery hqlQuery = hqlQueryTranslater.asHQL(query);

        Query hibernateQuery = createHibernateQuery(query, hqlQuery);

        return HibernateSearchResultsTransformer.transformResults(hibernateQuery.list());
    }

    public ApplicationImpl update(Application application) throws ApplicationNotFoundException
    {
        ApplicationImpl internalApplication = findById(application.getId());

        internalApplication.updateDetailsFromApplication(application);

        internalApplication.setUpdatedDateToNow();

        internalApplication.validate();

        super.update(internalApplication);

        return internalApplication;
    }

    public void updateCredential(Application application, PasswordCredential passwordCredential)
            throws ApplicationNotFoundException
    {
        if (!passwordCredential.isEncryptedCredential())
        {
            throw new IllegalArgumentException("The application password needs to be encrypted before being updated");
        }

        ApplicationImpl internalApplication = findByName(application.getName());
        internalApplication.setCredential(passwordCredential);
        internalApplication.setAttribute(ApplicationAttributeConstants.ATTRIBUTE_KEY_ATLASSIAN_SHA1_APPLIED, Boolean.TRUE.toString());
        internalApplication.setUpdatedDateToNow();

        super.update(internalApplication);
    }

    public void updateDirectoryMapping(long applicationId, long directoryId, int position)
            throws ApplicationNotFoundException, DirectoryNotFoundException
    {
        ApplicationImpl application = findById(applicationId);

        List<DirectoryMapping> directoryMappings = application.getDirectoryMappings();

        DirectoryMapping directoryMapping = application.getDirectoryMapping(directoryId);
        if (directoryMapping == null)
        {
            throw new DirectoryNotFoundException(directoryId);
        }
        int currentIndex = directoryMappings.indexOf(directoryMapping);

        if (currentIndex >= 0 && position >= 0 && position < directoryMappings.size())
        {
            DirectoryMapping directoryMappingToMove = directoryMappings.remove(currentIndex);

            directoryMappings.add(position, directoryMappingToMove);

            application.setUpdatedDateToNow();

            super.update(application);
        }
    }

    public void updateDirectoryMapping(long applicationId, long directoryId, boolean allowAllToAuthenticate)
            throws ApplicationNotFoundException, DirectoryNotFoundException
    {
        ApplicationImpl application = findById(applicationId);

        DirectoryMapping directoryMapping = application.getDirectoryMapping(directoryId);
        if (directoryMapping == null)
        {
            throw new DirectoryNotFoundException(directoryId);
        }
        directoryMapping.setAllowAllToAuthenticate(allowAllToAuthenticate);

        application.setUpdatedDateToNow();

        super.update(application);
    }

    public void updateDirectoryMapping(long applicationId, long directoryId, boolean allowAllToAuthenticate, Set<OperationType> operationTypes)
            throws ApplicationNotFoundException, DirectoryNotFoundException
    {
        ApplicationImpl application = findById(applicationId);

        DirectoryMapping directoryMapping = application.getDirectoryMapping(directoryId);
        if (directoryMapping == null)
        {
            throw new DirectoryNotFoundException(directoryId);
        }

        directoryMapping.setAllowAllToAuthenticate(allowAllToAuthenticate);
        directoryMapping.setAllowedOperations(operationTypes);

        application.setUpdatedDateToNow();

        super.update(application);
    }

    @Autowired
    public void setAliasDao(AliasDAO aliasDao)
    {
        this.aliasDao = aliasDao;
    }

    @Autowired
    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }

    @Autowired
    public void setHqlQueryTranslater(HQLQueryTranslater hqlQueryTranslater)
    {
        this.hqlQueryTranslater = hqlQueryTranslater;
    }
}
