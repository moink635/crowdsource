package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation;

import com.atlassian.crowd.util.persistence.hibernate.batch.HibernateOperation;

import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * Hibernate Operation to delete entities in batch
 */
public class DeleteOperation implements HibernateOperation<Session>
{
    @Override
    public void performOperation(Object object, Session session) throws HibernateException
    {
        session.delete(object);
    }
}
