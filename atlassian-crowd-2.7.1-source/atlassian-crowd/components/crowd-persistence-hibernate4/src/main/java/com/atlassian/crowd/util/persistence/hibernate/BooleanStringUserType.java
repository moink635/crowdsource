package com.atlassian.crowd.util.persistence.hibernate;

import org.hibernate.dialect.Dialect;
import org.hibernate.type.AbstractSingleColumnStandardBasicType;
import org.hibernate.type.DiscriminatorType;
import org.hibernate.type.PrimitiveType;
import org.hibernate.type.StringType;
import org.hibernate.type.descriptor.sql.VarcharTypeDescriptor;

import java.io.Serializable;

/**
 * Boolean implementation that allows us to store Booleans as "true" and "false".
 * <p/>
 * For Hibernate 4 this type has been reimplemented as a {@code AbstractSingleColumnStandardBasicType}, as it is no
 * longer possible to change the true and false strings for {@code CharBooleanType}. Due to the short-sighted way
 * Hibernate's {@code BooleanType} is implemented, this class cannot extend from that, because that class explicitly
 * references Hibernate's {@code BooleanTypeDescriptor}, which only uses "T" or "F" for its value, rather than the
 * whole word as required by this type.
 *
 * @see BooleanTypeDescriptor
 */
public class BooleanStringUserType
        extends AbstractSingleColumnStandardBasicType<Boolean>
        implements PrimitiveType<Boolean>, DiscriminatorType<Boolean>
{
    public BooleanStringUserType()
    {
        super(VarcharTypeDescriptor.INSTANCE, BooleanTypeDescriptor.INSTANCE);
    }

    public Serializable getDefaultValue()
    {
        return Boolean.FALSE;
    }

    public String getName()
    {
        return "boolean";
    }

    public Class getPrimitiveClass()
    {
        return boolean.class;
    }

    public String objectToSQLString(Boolean value, Dialect dialect) throws Exception
    {
        return StringType.INSTANCE.objectToSQLString(value ? "T" : "F", dialect);
    }

    public Boolean stringToObject(String xml) throws Exception
    {
        return fromString(xml);
    }
}