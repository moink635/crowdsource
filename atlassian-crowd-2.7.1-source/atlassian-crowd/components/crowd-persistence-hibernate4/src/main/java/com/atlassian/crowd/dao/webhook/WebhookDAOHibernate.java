package com.atlassian.crowd.dao.webhook;

import com.atlassian.crowd.dao.CriteriaFactory;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.exception.WebhookNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.webhook.Webhook;
import com.atlassian.crowd.model.webhook.WebhookImpl;
import com.atlassian.crowd.util.persistence.hibernate.HibernateDao;

import org.hibernate.criterion.Restrictions;

/**
 * Persists Webhooks using Hibernate.
 *
 * @since v2.7
 */
public class WebhookDAOHibernate extends HibernateDao implements WebhookDAO
{
    @Override
    public Webhook findById(Long webhookId) throws WebhookNotFoundException
    {
        try
        {
            return (WebhookImpl) load(webhookId);
        }
        catch (ObjectNotFoundException e)
        {
            throw new WebhookNotFoundException(webhookId);
        }
    }

    @Override
    public Webhook findByApplicationAndEndpointUrl(Application application, String endpointUrl)
        throws WebhookNotFoundException
    {
        Object result = CriteriaFactory.createCriteria(session(), getPersistentClass())
            .add(Restrictions.eq("application", application))
            .add(Restrictions.eq("endpointUrl", endpointUrl))
            .uniqueResult();
        if (result == null)
        {
            throw new WebhookNotFoundException(application.getId(), endpointUrl);
        }

        return (WebhookImpl) result;
    }

    @Override
    public Webhook add(Webhook webhook)
    {
        WebhookImpl webhookToPersist = new WebhookImpl(webhook);

        super.save(webhookToPersist);

        return webhookToPersist;
    }

    @Override
    public void remove(Webhook webhook) throws WebhookNotFoundException
    {
        if (webhookExists(webhook.getId()))
        {
            super.remove(webhook);
        }
        else
        {
            throw new WebhookNotFoundException(webhook.getId());
        }
    }

    private boolean webhookExists(Long id)
    {
        try
        {
            findById(id);
            return true;
        }
        catch (WebhookNotFoundException e)
        {
            return false;
        }
    }

    @Override
    public Iterable<Webhook> findAll()
    {
        return session().createQuery("from WebhookImpl").list();
    }

    @Override
    public Webhook update(Webhook webhook) throws WebhookNotFoundException
    {
        try
        {
            WebhookImpl webhookToUpdate = (WebhookImpl) load(webhook.getId());
            webhookToUpdate.updateDetailsFrom(webhook);
            super.update(webhookToUpdate);
            return webhookToUpdate;
        }
        catch (ObjectNotFoundException e)
        {
            throw new WebhookNotFoundException(webhook.getId());
        }
    }

    @Override
    public Class getPersistentClass()
    {
        return WebhookImpl.class;
    }
}
