package com.atlassian.crowd.util.persistence.hibernate;

import java.util.Properties;

import com.atlassian.config.ConfigurationException;
import com.atlassian.config.db.HibernateConfig;

import org.hibernate.HibernateException;
import org.hibernate.MappingException;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Migrated from bucket.
 */
public class SchemaHelper
{
    private static final Logger logger = LoggerFactory.getLogger(SchemaHelper.class);
    public static final String COMPONENT_REFERENCE = "schemaHelper";

    private Configuration configuration;
    private HibernateConfig hibernateConfig;
    // If these are not set, we fall back to looking in the ApplicationConfig for anything
    // starting with hibernate.*
    private Properties hibernateConfigProperties;
    private MappingResources mappingResources;

    /**
     * @return Returns the config.
     */
    public Configuration getConfiguration() throws MappingException
    {
        if (configuration == null)
        {
            configuration = makeBaseHibernateConfiguration();

            if (this.mappingResources != null)
            {
                // register given Hibernate mapping definitions
                for (int i = 0, n = mappingResources.getMappings().size(); i < n; i++)
                {
                    configuration.addResource(mappingResources.getMappings().get(i),
                            Thread.currentThread().getContextClassLoader());
                }
            }
        }
        return configuration;
    }

    /**
     * @return Returns the mappings.
     */
    public MappingResources getMappingResources()
    {
        return mappingResources;
    }

    /**
     * @param configuration The config to set.
     */
    public void setConfiguration(Configuration configuration)
    {
        this.configuration = configuration;
    }

    public void setHibernateConfig(HibernateConfig hibernateConfig)
    {
        this.hibernateConfig = hibernateConfig;
    }

    public void setHibernateProperties(Properties props)
    {
        this.hibernateConfigProperties = props;
    }

    /**
     * @param mappings The mappings to set.
     */
    public void setMappingResources(MappingResources mappings)
    {
        this.mappingResources = mappings;
    }

    /**
     * If there are any updates to the Schema needed, do them.
     */
    public void updateSchemaIfNeeded() throws ConfigurationException
    {
        updateSchemaIfNeeded(true);
    }

    /**
     * @param showDDL - false will output DDL to stdout
     * @throws ConfigurationException
     */
    public void updateSchemaIfNeeded(boolean showDDL) throws ConfigurationException
    {
        try
        {
            new SchemaUpdate(getConfiguration()).execute(showDDL, true);
        }
        catch (Exception e)
        {
            throw new ConfigurationException("Cannot update schema", e);
        }
    }

    private Configuration makeBaseHibernateConfiguration()
    {
        if (hibernateConfigProperties != null)
        {
            return new Configuration().setProperties(hibernateConfigProperties);
        }

        return new Configuration().setProperties(hibernateConfig.getHibernateProperties());
    }

    /**
     * Creates the schema in the database.
     *
     * This will drop and create the schema if the setup type
     * is a new install or XML import OR will update the
     * schema if the setup type is database upgrade.
     *
     * @throws com.atlassian.config.ConfigurationException if there was an issue configuring the schema.
     */
    public void createSchema() throws ConfigurationException
    {
        // drop and create the schema
        try
        {
            SchemaExport schemaExport = new SchemaExport(getConfiguration());

            schemaExport.drop(logger.isDebugEnabled(), true); // drop the current schema before creating the new one
            schemaExport.create(logger.isDebugEnabled(), true);
        }
        catch (MappingException e)
        {
            logger.error("While creating the database schema", e);
            throw new ConfigurationException("Error dropping and creating database schema: " + e.getMessage(), e);
        }
        catch (HibernateException e)
        {
            logger.error("While creating the database schema", e);
            throw new ConfigurationException("Error dropping and creating database schema: " + e.getMessage(), e);

        }
    }
}
