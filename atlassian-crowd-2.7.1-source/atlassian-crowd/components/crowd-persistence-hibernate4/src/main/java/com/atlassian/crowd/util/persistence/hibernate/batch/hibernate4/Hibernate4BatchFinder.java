package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4;

import com.atlassian.crowd.dao.CriteriaFactory;
import com.atlassian.crowd.util.persistence.hibernate.batch.AbstractBatchFinder;
import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import java.util.Collection;
import java.util.List;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

/**
 * Hibernate 4 version of the {@link com.atlassian.crowd.util.persistence.hibernate.batch.BatchFinder}.
 */
public class Hibernate4BatchFinder extends AbstractBatchFinder
{
    private static final ThreadLocal<Session> currentSessionHolder = new ThreadLocal<Session>();

    private final SessionFactory sessionFactory;

    public Hibernate4BatchFinder(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    @Override
    protected void afterFind()
    {
        currentSessionHolder.get().close();
        currentSessionHolder.set(null);
    }

    @Override
    protected void beforeFind()
    {
        currentSessionHolder.set(sessionFactory.openSession());
    }

    protected <E> Collection<E> processBatchFind(long directoryID, Collection<String> names, Class<E> persistentClass)
    {
        Collection<String> lowercaseNames = Collections2.transform(names, new Function<String, String>()
        {
            public String apply(String from)
            {
                return toLowerCase(from);
            }
        });

        //noinspection unchecked
        return (List<E>) CriteriaFactory.createCriteria(currentSessionHolder.get(), persistentClass)
                .add(Restrictions.eq("directory.id", directoryID))
                .add(Restrictions.in("lowerName", lowercaseNames))
                .list();
    }
}
