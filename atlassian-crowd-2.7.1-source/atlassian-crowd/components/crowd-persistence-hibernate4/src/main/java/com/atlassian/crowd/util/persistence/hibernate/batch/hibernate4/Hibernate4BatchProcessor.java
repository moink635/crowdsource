package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4;

import com.atlassian.crowd.util.persistence.hibernate.batch.AbstractBatchProcessor;

import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Hibernate 4 implementation of the batch processor runs each collection in a new session, and each batch in
 * a separate transaction.
 */
public class Hibernate4BatchProcessor extends AbstractBatchProcessor<Session>
{
    private static final ThreadLocal<Session> currentSessionHolder = new ThreadLocal<Session>();
    private static final Logger log = LoggerFactory.getLogger(Hibernate4BatchProcessor.class);

    private final SessionFactory sessionFactory;

    public Hibernate4BatchProcessor(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    @Override
    protected Session getSession()
    {
        return currentSessionHolder.get();
    }

    @Override
    protected void afterProcessBatch()
    {
        commitTransaction();
    }

    @Override
    protected void afterProcessCollection()
    {
        closeSession();
    }

    @Override
    protected void afterProcessIndividual()
    {
        commitTransaction();
    }

    @Override
    protected void beforeProcessBatch()
    {
        startTransaction();
    }

    @Override
    protected void beforeProcessCollection()
    {
        openSession();
    }

    @Override
    protected void beforeProcessIndividual()
    {
        startTransaction();
    }

    /**
     * Method is protected to allow subclasses to override the transaction management if they need to (e.g. use a
     * transaction manager instead of manipulating the Hibernate session directly)
     */
    protected void commitTransaction()
    {
        flushSession();
        Transaction transaction = getSession().getTransaction();
        log.debug("commit transaction [ {} ]", transaction);
        transaction.commit();
        clearSession();
    }

    @Override
    protected void rollbackProcessBatch()
    {
        rollbackTransaction();
    }

    @Override
    protected void rollbackProcessIndividual()
    {
        rollbackTransaction();
    }

    /**
     * Method is protected to allow subclasses to override the transaction management if they need to (e.g. use a
     * transaction manager instead of manipulating the Hibernate session directly)
     */
    protected void rollbackTransaction()
    {
        Transaction transaction = getSession().getTransaction();
        transaction.rollback();
        log.debug("rollback transaction [ {} ]", transaction);
        clearSession();
    }

    /**
     * Method is protected to allow subclasses to override the transaction management if they need to (e.g. use a
     * transaction manager instead of manipulating the Hibernate session directly)
     */
    protected void startTransaction()
    {
        Transaction transaction = getSession().beginTransaction();
        log.debug("start transaction [ {} ]", transaction);
    }

    private void clearSession()
    {
        Session session = getSession();
        if (session != null)
        {
            log.debug("clear session [{}]", session);
            session.clear();
        }
    }

    private void closeSession()
    {
        if (currentSessionHolder.get() != null)
        {
            log.debug("close session [{}]", currentSessionHolder.get());
            currentSessionHolder.get().close();
            currentSessionHolder.set(null);
        }
    }

    private void flushSession()
    {
        Session session = getSession();
        if (session != null)
        {
            log.debug("flush session [{}]", session);
            session.flush();
        }
    }

    private Session openSession()
    {
        if (currentSessionHolder.get() != null)
        {
            throw new IllegalStateException("session already open");
        }
        Session session = sessionFactory.openSession();
        session.setFlushMode(FlushMode.MANUAL);
        session.setCacheMode(CacheMode.IGNORE);

        currentSessionHolder.set(session);
        log.debug("open new session [{}]", currentSessionHolder.get());
        return session;
    }
}
