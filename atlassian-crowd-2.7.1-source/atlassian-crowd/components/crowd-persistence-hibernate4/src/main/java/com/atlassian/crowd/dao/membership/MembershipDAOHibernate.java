package com.atlassian.crowd.dao.membership;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.crowd.dao.CriteriaFactory;
import com.atlassian.crowd.dao.group.InternalGroupDao;
import com.atlassian.crowd.dao.user.InternalUserDao;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.impl.IdentifierUtils;
import com.atlassian.crowd.embedded.spi.MembershipDao;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.exception.MembershipNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.membership.InternalMembership;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.search.hibernate.HQLQuery;
import com.atlassian.crowd.search.hibernate.HQLQueryTranslater;
import com.atlassian.crowd.search.hibernate.HibernateSearchResultsTransformer;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.persistence.hibernate.HibernateDao;
import com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation.MergeOperation;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableSet;

import org.hibernate.Query;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.beans.factory.annotation.Autowired;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

public class MembershipDAOHibernate extends HibernateDao implements MembershipDao, InternalMembershipDao
{
    private InternalGroupDao groupDao;
    private HQLQueryTranslater hqlQueryTranslater;
    private InternalUserDao userDao;

    public BatchResult<InternalMembership> addAll(Set<InternalMembership> memberships)
    {
        return batchProcessor.execute(new MergeOperation(), memberships);
    }

    public BatchResult<String> addAllUsersToGroup(long directoryId, Collection<String> userNames, String groupName) throws GroupNotFoundException
    {
        Collection<InternalUser> internalUsers = userDao.findByNames(directoryId, userNames);
        Collection<String> nonFoundUsers = nonFoundUsers(userNames, internalUsers);

        InternalGroup group = groupDao.findByName(directoryId, groupName);

        Set<InternalMembership> memberships = new HashSet<InternalMembership>();
        for (InternalUser user : internalUsers)
        {
            memberships.add(new InternalMembership(group, user));
        }

        BatchResult<InternalMembership> daoResult = addAll(memberships);

        BatchResult<String> result = new BatchResult<String>(daoResult.getTotalSuccessful());
        for (InternalMembership internalMembership : daoResult.getSuccessfulEntities())
        {
            result.addSuccess(internalMembership.getChildName());
        }
        for (InternalMembership internalMembership : daoResult.getFailedEntities())
        {
            result.addFailure(internalMembership.getChildName());
        }

        result.addFailures(nonFoundUsers);

        return result;
    }

    /**
     * Given a collection of internal users and a collection of requested usernames, returns the user names that
     * cannot be matched to an internal user.
     *
     * @param usernames a collection of usernames
     * @param internalUsers a collection of internal users from the database
     * @return a collection of usernames that cannot be matched to an internal user
     */
    private static Collection<String> nonFoundUsers(Collection<String> usernames,
                                                    Collection<InternalUser> internalUsers)
    {

        if (internalUsers.size() == usernames.size())
        {
            return Collections.emptySet(); // optimisation to avoid the computation below
        }

        final Set<String> foundUsernames =   // transformed into a Set to improve worst case performance
            ImmutableSet.copyOf(Collections2.transform(internalUsers, LOWERCASE_NAMES_OF_INTERNAL_USERS));
        return Collections2.filter(usernames, new Predicate<String>()
        {
            @Override
            public boolean apply(String requestedUsername)
            {
                return !foundUsernames.contains(IdentifierUtils.toLowerCase(requestedUsername));
            }
        });
    }

    private static final Function<InternalUser,String> LOWERCASE_NAMES_OF_INTERNAL_USERS =
        new Function<InternalUser, String>()
        {
            @Override
            public String apply(InternalUser input)
            {
                return input.getLowerName();
            }
        };

    public void addGroupToGroup(long directoryId, String childGroup, String parentGroup)
        throws GroupNotFoundException, MembershipAlreadyExistsException
    {
        InternalGroup internalChildGroup = groupDao.findByName(directoryId, childGroup);
        InternalGroup internalParentGroup = groupDao.findByName(directoryId, parentGroup);

        if (isGroupDirectMember(directoryId, childGroup, parentGroup))
        {
            throw new MembershipAlreadyExistsException(directoryId, childGroup, parentGroup);
        }

        super.save(new InternalMembership(internalParentGroup, internalChildGroup));
    }

    public void addUserToGroup(long directoryId, String username, String groupName)
        throws UserNotFoundException, GroupNotFoundException, MembershipAlreadyExistsException
    {
        // Check that the user and group exists
        InternalUser internalUser = (InternalUser) userDao.findByName(directoryId, username);
        InternalGroup internalGroup = groupDao.findByName(directoryId, groupName);

        if (isUserDirectMember(directoryId, username, groupName))
        {
            throw new MembershipAlreadyExistsException(directoryId, username, groupName);
        }

        super.save(new InternalMembership(internalGroup, internalUser));
    }

    /**
     * @param directories directories to search for memberships
     * @return all memberships across all directories (used for export).
     */
    @SuppressWarnings("unchecked")
    public List<InternalMembership> findAll(Collection<Directory> directories)
    {
        if (directories == null || directories.isEmpty())
        {
            return Collections.emptyList();
        }
        return CriteriaFactory.createCriteria(session(), getPersistentClass())
                .add(Restrictions.in("directory", directories))
                .list();
    }

    /**
     * @param directories directories to search for memberships
     * @return all memberships across all directories (used for export).
     */
    @SuppressWarnings("unchecked")
    public List<InternalMembership> findAllLocal(Collection<Directory> directories)
    {
        if (directories == null || directories.isEmpty())
        {
            return Collections.emptyList();
        }
        return CriteriaFactory.createCriteria(session(), getPersistentClass())
                .add(Restrictions.in("directory", directories))
                .add(Subqueries.propertyIn("parentId", DetachedCriteria.forClass(InternalGroup.class)
                        .add(Restrictions.eq("local", true))
                        .setProjection(Projections.id())))
                .list();
    }

    public Class getPersistentClass()
    {
        return InternalMembership.class;
    }

    public boolean isGroupDirectMember(long directoryId, String childGroup, String parentGroup)
    {
        try
        {
            findInternalMembership(directoryId, childGroup, parentGroup, MembershipType.GROUP_GROUP);
            return true;
        }
        catch (MembershipNotFoundException e)
        {
            return false;
        }
    }

    public boolean isUserDirectMember(long directoryId, String username, String groupName)
    {
        try
        {
            findInternalMembership(directoryId, username, groupName, MembershipType.GROUP_USER);
            return true;
        }
        catch (MembershipNotFoundException e)
        {
            return false;
        }
    }

    public void removeAllRelationships(long directoryId)
    {
        session().getNamedQuery("removeAllRelationships")
                .setLong("directoryId", directoryId)
                .executeUpdate();
    }

    public void removeAllUserRelationships(long directoryId)
    {
        session().getNamedQuery("removeAllRelationshipsOfType")
                .setLong("directoryId", directoryId)
                .setParameter("membershipType", MembershipType.GROUP_USER)
                .executeUpdate();
    }

    public void removeGroupFromGroup(long directoryId, String childGroup, String parentGroup) throws MembershipNotFoundException
    {
        InternalMembership internalMembership = findInternalMembership(directoryId, childGroup, parentGroup, MembershipType.GROUP_GROUP);

        super.remove(internalMembership);
    }

    public void removeGroupMembers(long directoryId, String groupName)
    {
        session().getNamedQuery("removeAllEntityMembers")
                .setString("entityName", toLowerCase(groupName))
                .setLong("directoryId", directoryId)
                .executeUpdate();
    }

    public void removeGroupMemberships(long directoryId, String groupName)
    {
        session().getNamedQuery("removeAllEntityMemberships")
                .setString("entityName", toLowerCase(groupName))
                .setLong("directoryId", directoryId)
                .setParameter("membershipType", MembershipType.GROUP_GROUP)
                .executeUpdate();
    }

    public void removeUserFromGroup(long directoryId, String username, String groupName) throws UserNotFoundException, GroupNotFoundException, MembershipNotFoundException
    {
        InternalMembership internalMembership = findInternalMembership(directoryId, username, groupName, MembershipType.GROUP_USER);

        super.remove(internalMembership);
    }

    public void removeUserMemberships(long directoryId, String username)
    {
        session().getNamedQuery("removeAllEntityMemberships")
                .setString("entityName", toLowerCase(username))
                .setLong("directoryId", directoryId)
                .setParameter("membershipType", MembershipType.GROUP_USER)
                .executeUpdate();
    }

    public void renameGroupRelationships(long directoryId, String oldName, String newName)
    {
        session().getNamedQuery("renameChild")
                .setLong("directoryId", directoryId)
                .setString("oldName", toLowerCase(oldName))
                .setString("newName", newName)
                .setString("lowerNewName", toLowerCase(newName))
                .setParameter("membershipType", MembershipType.GROUP_GROUP)
                .executeUpdate();

        session().getNamedQuery("renameParent")
                .setLong("directoryId", directoryId)
                .setString("oldName", toLowerCase(oldName))
                .setString("newName", newName)
                .setString("lowerNewName", toLowerCase(newName))
                .setParameter("membershipType", MembershipType.GROUP_GROUP)
                .executeUpdate();

        session().getNamedQuery("renameParent")
                .setLong("directoryId", directoryId)
                .setString("oldName", toLowerCase(oldName))
                .setString("newName", newName)
                .setString("lowerNewName", toLowerCase(newName))
                .setParameter("membershipType", MembershipType.GROUP_USER)
                .executeUpdate();
    }

    public void renameUserRelationships(long directoryId, String oldName, String newName)
    {
        session().getNamedQuery("renameChild")
                .setLong("directoryId", directoryId)
                .setString("oldName", toLowerCase(oldName))
                .setString("newName", newName)
                .setString("lowerNewName", toLowerCase(newName))
                .setParameter("membershipType", MembershipType.GROUP_USER)
                .executeUpdate();
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> search(long directoryId, MembershipQuery<T> query)
    {
        Query hibernateQuery = createHibernateSearchQuery(directoryId, query);

        return HibernateSearchResultsTransformer.transformResults(hibernateQuery.list());
    }

    @Autowired
    public void setGroupDao(InternalGroupDao groupDao)
    {
        this.groupDao = groupDao;
    }

    @Autowired
    public void setHqlQueryTranslater(HQLQueryTranslater hqlQueryTranslater)
    {
        this.hqlQueryTranslater = hqlQueryTranslater;
    }

    @Autowired
    public void setUserDao(InternalUserDao userDao)
    {
        this.userDao = userDao;
    }

    /**
     * Builds a Hibernate {@link Query query} from a {@link MembershipQuery}. This method is protected to allow
     * subclasses to override it, for instance to configure query caching.
     *
     * @param directoryId the directory being queried
     * @param query the query
     * @return the translated Hibernate query
     */
    protected Query createHibernateSearchQuery(long directoryId, MembershipQuery<?> query)
    {
        HQLQuery hqlQuery = hqlQueryTranslater.asHQL(directoryId, query);

        return createHibernateQuery(query, hqlQuery);
    }

    private InternalMembership findInternalMembership(long directoryId, String childName, String parentName, MembershipType type) throws MembershipNotFoundException
    {
        InternalMembership internalMembership =
                (InternalMembership) CriteriaFactory.createCriteria(session(), getPersistentClass())
                        .add(Restrictions.eq("directory.id", directoryId))
                        .add(Restrictions.eq("lowerParentName", toLowerCase(parentName)))
                        .add(Restrictions.eq("lowerChildName", toLowerCase(childName)))
                        .add(Restrictions.eq("membershipType", type))
                        .uniqueResult();
        if (internalMembership == null)
        {
            throw new MembershipNotFoundException(childName, parentName);
        }

        return internalMembership;
    }
}
