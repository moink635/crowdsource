package com.atlassian.crowd.dao.property;

import com.atlassian.crowd.dao.CriteriaFactory;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.model.property.PropertyId;
import com.atlassian.crowd.util.persistence.hibernate.HibernateDao;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class PropertyDAOHibernate extends HibernateDao implements PropertyDAO
{
    public Property add(Property property)
    {
        super.save(property);
        return property;
    }

    public Property find(String key, String name) throws ObjectNotFoundException
    {
        Object result = session().get(getPersistentClass(), new PropertyId(key, name));
        if (result == null)
        {
            throw new ObjectNotFoundException(getPersistentClass(), name);
        }

        return (Property) result;
    }

    public List<Property> findAll(String key)
    {
        return CriteriaFactory.createCriteria(session(), getPersistentClass())
                .add(Restrictions.eq("propertyId.key", key))
                .list();
    }

    public List<Property> findAll()
    {
        return CriteriaFactory.createCriteria(session(), getPersistentClass()).list();
    }

    public Class getPersistentClass()
    {
        return Property.class;
    }

    public void remove(String key, String name)
    {
        try
        {
            Property property = find(key, name);
            super.remove(property);
        }
        catch (ObjectNotFoundException e)
        {
            // silently pass
        }
    }

    public Property update(Property property)
    {
        super.update(property);
        return property;
    }
}