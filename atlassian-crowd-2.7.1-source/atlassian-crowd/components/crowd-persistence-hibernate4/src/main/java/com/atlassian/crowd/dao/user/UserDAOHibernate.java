package com.atlassian.crowd.dao.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.dao.CriteriaFactory;
import com.atlassian.crowd.dao.membership.InternalMembershipDao;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.exception.UserAlreadyExistsException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.InternalAttributesHelper;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.InternalUserAttribute;
import com.atlassian.crowd.model.user.InternalUserWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.hibernate.HQLQuery;
import com.atlassian.crowd.search.hibernate.HQLQueryTranslater;
import com.atlassian.crowd.search.hibernate.HibernateSearchResultsTransformer;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.InternalEntityUtils;
import com.atlassian.crowd.util.persistence.hibernate.HibernateDao;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchResultWithIdReferences;
import com.atlassian.crowd.util.persistence.hibernate.batch.TransactionGroup;
import com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation.MergeOperation;
import com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation.RemoveUserOperation;

import com.google.common.base.Preconditions;
import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;

import org.apache.commons.lang3.Validate;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

public class UserDAOHibernate extends HibernateDao implements InternalUserDao, UserDao
{
    private InternalAttributesHelper attributesHelper;
    private HQLQueryTranslater hqlQueryTranslater;
    private InternalMembershipDao membershipDao;

    public InternalUser add(User user, PasswordCredential credential) throws DirectoryNotFoundException, UserAlreadyExistsException
    {
        InternalUser existingUser = findByNameInternal(user.getDirectoryId(), user.getName());

        if (existingUser != null)
        {
            throw new UserAlreadyExistsException(existingUser.getDirectoryId(), existingUser.getName());
        }

        InternalUser userToAdd = new InternalUser(user, getDirectory(user), credential);
        userToAdd.setCreatedDateToNow();
        userToAdd.setUpdatedDateToNow();
        super.save(userToAdd);

        return userToAdd;
    }

    public BatchResult<User> addAll(Set<UserTemplateWithCredentialAndAttributes> users)
    {
        Set<TransactionGroup<InternalUser, InternalUserAttribute>> usersToAdd = new HashSet<TransactionGroup<InternalUser, InternalUserAttribute>>();

        Set<User> usersFailedValidation = new HashSet<User>();

        for (UserTemplateWithCredentialAndAttributes user : users)
        {
            try
            {
                // prepare user
                InternalUser userToAdd = new InternalUser(user, getDirectory(user), user.getCredential());
                userToAdd.setCreatedDateToNow();
                userToAdd.setUpdatedDateToNow();

                // prepare attributes
                Set<InternalUserAttribute> attribs = new HashSet<InternalUserAttribute>();
                for (Map.Entry<String, Set<String>> entry : user.getAttributes().entrySet())
                {
                    for (String value : entry.getValue())
                    {
                        attribs.add(new InternalUserAttribute(userToAdd, entry.getKey(), value));
                    }
                }

                // add transaction group
                usersToAdd.add(new TransactionGroup<InternalUser, InternalUserAttribute>(userToAdd, attribs));
            }
            catch (IllegalArgumentException e)
            {
                logger.error("Could not add user <" + user.getName() + ">: " + e.getMessage());
                usersFailedValidation.add(user);
            }
            catch (DirectoryNotFoundException e)
            {
                logger.error("Could not add user <" + user.getName() + ">: " + e.getMessage());
                usersFailedValidation.add(user);
            }
        }

        // process the collection
        BatchResult<TransactionGroup<InternalUser, InternalUserAttribute>> daoResult = batchProcessor.execute(new MergeOperation(), usersToAdd);

        // produce an amalgamated result consisting of successes, dao failures and validation failures
        BatchResult<User> result = new BatchResult<User>(users.size());
        for (TransactionGroup<InternalUser, InternalUserAttribute> transactionGroup : daoResult.getSuccessfulEntities())
        {
            result.addSuccess(transactionGroup.getPrimaryObject());
        }
        for (TransactionGroup<InternalUser, InternalUserAttribute> transactionGroup : daoResult.getFailedEntities())
        {
            result.addFailure(transactionGroup.getPrimaryObject());
        }
        result.addFailures(usersFailedValidation);

        return result;
    }

    // this method is used for legacy imports
    public BatchResultWithIdReferences<User> addAll(Collection<UserTemplateWithCredentialAndAttributes> users)
    {
        Set<TransactionGroup<InternalUser, InternalUserAttribute>> usersToAdd = new HashSet<TransactionGroup<InternalUser, InternalUserAttribute>>();

        Set<User> usersFailedValidation = new HashSet<User>();

        for (UserTemplateWithCredentialAndAttributes user : users)
        {
            try
            {
                // directory better exist!
                DirectoryImpl directory = loadReference(DirectoryImpl.class, user.getDirectoryId());

                // prepare user
                InternalUser userToAdd = new InternalUser(user, directory);

                // prepare attributes
                Set<InternalUserAttribute> attribs = new HashSet<InternalUserAttribute>();
                for (Map.Entry<String, Set<String>> entry : user.getAttributes().entrySet())
                {
                    for (String value : entry.getValue())
                    {
                        attribs.add(new InternalUserAttribute(userToAdd, entry.getKey(), value));
                    }
                }

                // add transaction group
                usersToAdd.add(new TransactionGroup<InternalUser, InternalUserAttribute>(userToAdd, attribs));
            }
            catch (IllegalArgumentException e)
            {
                logger.error("Could not add user <" + user.getName() + ">: " + e.getMessage());
                usersFailedValidation.add(user);
            }
        }

        // process the collection
        BatchResult<TransactionGroup<InternalUser, InternalUserAttribute>> daoResult = batchProcessor.execute(new MergeOperation(), usersToAdd);

        // produce an amalgamated result consisting of dao failures and validation failures
        BatchResultWithIdReferences<User> result = new BatchResultWithIdReferences<User>(users.size());
        for (TransactionGroup<InternalUser, InternalUserAttribute> transactionGroup : daoResult.getFailedEntities())
        {
            result.addFailure(transactionGroup.getPrimaryObject());
        }
        result.addFailures(usersFailedValidation);

        // now add the ID references of the successfully added users
        for (TransactionGroup<InternalUser, InternalUserAttribute> user : usersToAdd)
        {
            result.addIdReference(user.getPrimaryObject());
        }

        return result;
    }

    public InternalUser findByName(long directoryId, String username) throws UserNotFoundException
    {
        InternalUser user = findByNameInternal(directoryId, username);

        if (user == null)
        {
            throw new UserNotFoundException(username);
        }

        return user;
    }

    public InternalUser findByExternalId(long directoryId, String externalId) throws UserNotFoundException
    {
        Preconditions.checkNotNull(externalId, "externalId");

        final InternalUser user = (InternalUser) CriteriaFactory.createCriteria(session(), InternalUser.class)
                .add(Restrictions.eq("directory.id", directoryId))
                .add(Restrictions.eq("externalId", externalId))
                .uniqueResult();

        if (user == null)
        {
            UserNotFoundException.throwNotFoundByExternalId(externalId);
        }

        return user;
    }

    public InternalUserWithAttributes findByNameWithAttributes(long directoryId, String username) throws UserNotFoundException
    {
        InternalUser user = findByName(directoryId, username);

        Set<InternalUserAttribute> attributesList = findUserAttributes(user.getId());

        Map<String, Set<String>> attributesMap = attributesHelper.attributesListToMap(attributesList);

        return new InternalUserWithAttributes(user, attributesMap);
    }

    public Collection<InternalUser> findByNames(long directoryID, Collection<String> usernames)
    {
        return batchFinder.find(directoryID, usernames, InternalUser.class);
    }

    public Set<InternalUserAttribute> findUserAttributes(long userID)
    {
        return Sets.<InternalUserAttribute>newHashSet(
                CriteriaFactory.createCriteria(session(), InternalUserAttribute.class)
                        .add(Restrictions.eq("user.id", userID))
                        .list());
    }

    public PasswordCredential getCredential(long directoryId, String username) throws UserNotFoundException
    {
        return findByName(directoryId, username).getCredential();
    }

    public List<PasswordCredential> getCredentialHistory(long directoryId, String username) throws UserNotFoundException
    {
        return findByName(directoryId, username).getCredentialHistory();
    }

    public Class getPersistentClass()
    {
        return InternalUser.class;
    }

    public void remove(User user) throws UserNotFoundException
    {
        InternalUser userToRemove = findByName(user.getDirectoryId(), user.getName());

        // remove all memberships
        membershipDao.removeUserMemberships(user.getDirectoryId(), user.getName());

        // remove all user attributes manually.
        session().getNamedQuery("removeAllInternalUserAttributes")
                .setEntity("user", userToRemove)
                .executeUpdate();

        // remove the user, hibernate will also remove credential history
        super.remove(userToRemove);
    }

    public void removeAll(long directoryId)
    {
        // remove memberships
        membershipDao.removeAllUserRelationships(directoryId);

        // remove attributes, creds and users
        session().getNamedQuery("removeInternalUserAttributesInDirectory")
                .setLong("directoryId", directoryId)
                .executeUpdate();

        // hibernate does not bulk remove associated collections so we need to manully bulk delete them
        // (see HHH-368 / HHH-1917 as well as http://in.relation.to/Bloggers/BulkOperations)
        session().getNamedQuery("removeCredentialRecordsInDirectory")
                .setLong("directoryId", directoryId)
                .executeUpdate();

        session().getNamedQuery("removeInternalUsersInDirectory")
                .setLong("directoryId", directoryId)
                .executeUpdate();
    }

    public BatchResult<String> removeAllUsers(long directoryId, Set<String> userNames)
    {
        Collection<InternalUser> users = findByNames(directoryId, userNames);
        BatchResult<InternalUser> batchResult = batchProcessor.execute(new RemoveUserOperation(), users);

        BatchResult<String> overallResult = new BatchResult<String>(userNames.size());
        overallResult.addFailures(Collections2.transform(batchResult.getFailedEntities(), InternalEntityUtils.GET_NAME));
        overallResult.addSuccesses(Collections2.transform(batchResult.getSuccessfulEntities(), InternalEntityUtils.GET_NAME));

        return overallResult;
    }

    public void removeAttribute(User user, String attributeName) throws UserNotFoundException
    {
        removeAttributes(findByName(user.getDirectoryId(), user.getName()), attributeName);
    }

    public InternalUser rename(User user, String newUsername) throws UserNotFoundException, UserAlreadyExistsException
    {
        InternalUser userToRename = findByName(user.getDirectoryId(), user.getName());
        if (newUsername.equals(userToRename.getName()))
        {
            return userToRename; // no change if renaming to exactly the same name (case preserving)
        }

        String oldName = userToRename.getName();

        final InternalUser existingUser = findByNameInternal(user.getDirectoryId(), newUsername);

        if (existingUser != null && !userToRename.getId().equals(existingUser.getId()))
        {
            throw new UserAlreadyExistsException(existingUser.getDirectoryId(), existingUser.getName());
        }

        userToRename.renameTo(newUsername);
        userToRename.setUpdatedDateToNow();
        super.update(userToRename);

        membershipDao.renameUserRelationships(userToRename.getDirectoryId(), oldName, userToRename.getName());

        return userToRename;
    }

    public <T> List<T> search(long directoryID, EntityQuery<T> query)
    {
        if (query.getEntityDescriptor().getEntityType() != Entity.USER)
        {
            throw new IllegalArgumentException("UserDAO can only evaluate EntityQueries for Entity.USER");
        }
        HQLQuery hqlQuery = hqlQueryTranslater.asHQL(directoryID, query);

        Query hibernateQuery = createHibernateQuery(query, hqlQuery);

        return HibernateSearchResultsTransformer.transformResults(hibernateQuery.list());
    }

    @Autowired
    public void setAttributesHelper(InternalAttributesHelper attributesHelper)
    {
        this.attributesHelper = attributesHelper;
    }

    @Autowired
    public void setHqlQueryTranslater(HQLQueryTranslater hqlQueryTranslater)
    {
        this.hqlQueryTranslater = hqlQueryTranslater;
    }

    @Autowired
    public void setMembershipDao(InternalMembershipDao internalMembershipDao)
    {
        this.membershipDao = internalMembershipDao;
    }

    public void storeAttributes(User user, Map<String, Set<String>> attributes) throws UserNotFoundException
    {
        InternalUser internalUser = findByName(user.getDirectoryId(), user.getName());

        Set<InternalUserAttribute> attributesList = findUserAttributes(internalUser.getId());

        // convert list to object map
        Map<String, Set<InternalUserAttribute>> existingAttributesMap = new HashMap<String, Set<InternalUserAttribute>>();
        for (InternalUserAttribute existingAttrib : attributesList)
        {
            Set<InternalUserAttribute> existingVals = existingAttributesMap.get(existingAttrib.getName());
            if (existingVals == null)
            {
                existingVals = new HashSet<InternalUserAttribute>();
                existingAttributesMap.put(existingAttrib.getName(), existingVals);
            }
            existingVals.add(existingAttrib);
        }

        // process each new attribute map entry
        for (Map.Entry<String, Set<String>> entry : attributes.entrySet())
        {
            if (existingAttributesMap.containsKey(entry.getKey()))
            {
                // we need to do something about it because the existing and new attributes match on the same key
                Set<String> newVals = entry.getValue();
                Set<InternalUserAttribute> existingVals = existingAttributesMap.get(entry.getKey());

                List<String> valsToAdd = new ArrayList<String>(); // default: add nothing
                Set<InternalUserAttribute> valsToRemove = new HashSet<InternalUserAttribute>(existingVals); // default: remove everything

                // work out what NOOPs we can perform
                for (String newVal : newVals)
                {
                    boolean valueAlreadyExists = false;
                    for (InternalUserAttribute existingVal : existingVals)
                    {
                        if (newVal.equals(existingVal.getValue()))
                        {
                            // we can keep the existing value
                            valsToRemove.remove(existingVal);

                            valueAlreadyExists = true;
                            break;
                        }
                    }

                    if (!valueAlreadyExists)
                    {
                        // we need to add the new value
                        valsToAdd.add(newVal);
                    }
                }

                // try to update the ones to remove into ones to add (ie. SQL UPDATE) or remove them if there are no more to add (ie. SQL DELETE)
                for (InternalUserAttribute existingVal : valsToRemove)
                {
                    if (valsToAdd.size() > 0)
                    {
                        String newVal = valsToAdd.remove(0);
                        existingVal.setValue(newVal);
                        saveOrUpdate(existingVal);
                    }
                    else
                    {
                        remove(existingVal);
                    }
                }

                // incase there are attributes that still need to be added (ie. SQL INSERT) because we could be adding more attributes than there used to be
                for (String newVal : valsToAdd)
                {
                    addAttribute(internalUser, entry.getKey(), newVal);
                }
            }
            else
            {
                // there are no existing attributes on the user matching the given key (so we need to SQL INSERT)
                for (String newVal : entry.getValue())
                {
                    addAttribute(internalUser, entry.getKey(), newVal);
                }
            }
        }
    }

    public InternalUser update(User user) throws UserNotFoundException
    {
        InternalUser userToUpdate = findByName(user.getDirectoryId(), user.getName());
        userToUpdate.updateDetailsFrom(user);
        userToUpdate.setUpdatedDateToNow();
        super.update(userToUpdate);

        return userToUpdate;
    }

    public void updateCredential(User user, PasswordCredential credential, int maxPasswordHistory) throws UserNotFoundException
    {
        InternalUser userToUpdate = findByName(user.getDirectoryId(), user.getName());
        userToUpdate.updateCredentialTo(credential, maxPasswordHistory);
        userToUpdate.setUpdatedDateToNow();
        super.update(userToUpdate);
    }

    private void addAttribute(InternalUser user, String attributeName, String attributeValue)
    {
        InternalUserAttribute attribute = new InternalUserAttribute(user, attributeName, attributeValue);
        session().save(attribute);
    }

    private InternalUser findByNameInternal(long directoryId, String username)
    {
        return (InternalUser) CriteriaFactory.createCriteria(session(), InternalUser.class)
                .add(Restrictions.eq("directory.id", directoryId))
                .add(Restrictions.eq("lowerName", toLowerCase(username)))
                .uniqueResult();
    }

    private DirectoryImpl getDirectory(User user) throws DirectoryNotFoundException
    {
        Validate.notNull(user.getDirectoryId(), "Cannot add user with null directoryId");
        try
        {
            return load(DirectoryImpl.class, user.getDirectoryId());
        }
        catch (ObjectNotFoundException e)
        {
            throw new DirectoryNotFoundException(user.getDirectoryId(), e);
        }
    }

    private void removeAttributes(InternalUser user, String attributeName)
    {
        session().getNamedQuery("removeInternalUserAttributes")
                .setEntity("user", user).setString("attributeName", attributeName)
                .executeUpdate();
    }
}
