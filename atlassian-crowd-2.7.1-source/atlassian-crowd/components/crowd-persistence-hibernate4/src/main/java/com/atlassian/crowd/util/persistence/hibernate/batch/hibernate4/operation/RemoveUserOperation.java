package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation;

import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.util.persistence.hibernate.batch.HibernateOperation;

import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * An operation to remove a user and its related information such as memberships and attributes.
 */
public class RemoveUserOperation implements HibernateOperation<Session>
{
    public void performOperation(Object object, Session session) throws HibernateException
    {
        InternalUser user = (InternalUser) object;

        // remove memberships
        session.getNamedQuery("removeAllEntityMemberships")
                .setString("entityName", user.getName())
                .setLong("directoryId", user.getDirectoryId())
                .setParameter("membershipType", MembershipType.GROUP_USER)
                .executeUpdate();

        // remove attributes
        session.getNamedQuery("removeAllInternalUserAttributes")
                .setEntity("user", user)
                .executeUpdate();

        // remove users
        session.delete(user);
    }
}
