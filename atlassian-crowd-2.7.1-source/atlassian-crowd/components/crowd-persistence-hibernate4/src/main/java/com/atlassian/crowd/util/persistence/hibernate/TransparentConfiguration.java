package com.atlassian.crowd.util.persistence.hibernate;

import org.hibernate.MappingException;
import org.hibernate.cfg.Configuration;
import org.hibernate.mapping.Table;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Helper class to allow visibility of tables in a Hibernate Configuration.
 */
public class TransparentConfiguration extends Configuration
{
    private List<String> mappingFiles = new ArrayList<String>();

    public Map<String, Table> getTables()
    {
        return tables;
    }

    /**
     * Read mappings as a application resourceName (i.e. classpath lookup)
     * trying different classloaders.
     *
     * @param resourceName The resource name
     * @return this (for method chaining purposes)
     * @throws org.hibernate.MappingException Indicates problems locating the resource or
     *                                        processing the contained mapping document.
     */
    public Configuration addResource(String resourceName) throws MappingException
    {
        mappingFiles.add(resourceName);

        return super.addResource(resourceName);
    }

    public List<String> getMappingFiles()
    {
        return mappingFiles;
    }
}
