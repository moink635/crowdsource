package com.atlassian.crowd.util.persistence.hibernate;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Stores a collection of hbm.xml mapping files.
 * <p/>
 * You can either set a list of mappings (hbm.xml file locations) OR a configLocation (a hibernate.cfg.xml).
 * <p/>
 * This class relies on a wrapped Hibernate Configuration object (TransparentConfiguration) to do the parsing work.
 * <p/>
 * If you set both, the configLocation overrides mappings.
 * <p/>
 * This code has been migrated from bucket.
 */
public class MappingResources implements InitializingBean
{
    // either spring injected or not used at all (ie. optional)
    private Resource configLocation;
    // either spring injected or generated from configLocation
    private List<String> mappings = new ArrayList<String>();
    // initialised properties from the mappings
    private List<String> tableNames;

    public void addMapping(String mapping)
    {
        mappings.add(mapping);
    }

    /**
     * Invoked by a BeanFactory after it has set all bean properties supplied (and satisfied BeanFactoryAware and
     * ApplicationContextAware). <p>This method allows the bean instance to perform initialization only possible
     * when all bean properties have been set and to throw an exception in the event of misconfiguration.
     *
     * @throws IllegalStateException if no mappings have been configured, either directly or via configLocation
     * @throws IOException           if the URL for the configLocation cannot be parsed
     */
    public void afterPropertiesSet() throws IllegalStateException, IOException
    {
        // get hibernate to do all the work (parsing hibernate.cfg.xml, mappings.hbm.xml, etc)
        TransparentConfiguration config = new TransparentConfiguration();

        if (configLocation != null)
        {
            config.configure(configLocation.getURL());
            mappings = config.getMappingFiles();
        }
        else if (mappings != null)
        {
            for (String hbmFile : mappings)
            {
                config.addResource(hbmFile);
            }
        }
        config.buildMappings();

        // sanity check for development
        if (mappings == null || mappings.isEmpty())
        {
            throw new IllegalStateException("Misconfigured mappings/configLocation property on MappingResources bean: no hbm.xml mappings found.");
        }

        tableNames = new ArrayList<String>(config.getTables().keySet());
    }

    public Resource getConfigLocation()
    {
        return configLocation;
    }

    public List<String> getMappings()
    {
        return mappings;
    }

    public String[] getMappingsAsArray()
    {
        String[] map = new String[mappings.size()];
        for (int i = 0; i < mappings.size(); i++)
        {
            map[i] = mappings.get(i);
        }
        return map;
    }

    /**
     * Retrieves the table names using a transparent configuration object built from the hbm.xml mappings.
     *
     * @return table names.
     */
    public List<String> getTableNames()
    {
        return tableNames;
    }

    public void setConfigLocation(Resource configLocation)
    {
        this.configLocation = configLocation;
    }

    public void setMappings(List<String> mappings)
    {
        this.mappings.addAll(mappings);
    }
}
