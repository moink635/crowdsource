package com.atlassian.crowd.dao.group;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.crowd.dao.CriteriaFactory;
import com.atlassian.crowd.dao.membership.InternalMembershipDao;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.GroupDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.model.InternalAttributesHelper;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.group.InternalGroupAttribute;
import com.atlassian.crowd.model.group.InternalGroupWithAttributes;
import com.atlassian.crowd.search.Entity;
import com.atlassian.crowd.search.hibernate.HQLQuery;
import com.atlassian.crowd.search.hibernate.HQLQueryTranslater;
import com.atlassian.crowd.search.hibernate.HibernateSearchResultsTransformer;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.InternalEntityUtils;
import com.atlassian.crowd.util.persistence.hibernate.HibernateDao;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchResultWithIdReferences;
import com.atlassian.crowd.util.persistence.hibernate.batch.TransactionGroup;
import com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation.MergeOperation;
import com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation.RemoveGroupOperation;

import com.google.common.collect.Collections2;
import com.google.common.collect.Sets;

import org.apache.commons.lang3.Validate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

public class GroupDAOHibernate extends HibernateDao implements InternalGroupDao, GroupDao
{
    private InternalAttributesHelper attributesHelper;
    private DirectoryDao directoryDao;
    private HQLQueryTranslater hqlQueryTranslater;
    private InternalMembershipDao membershipDao;

    public InternalGroup add(Group group) throws DirectoryNotFoundException, InvalidGroupException
    {
        return add(group, false);
    }

    public BatchResult<Group> addAll(Set<? extends Group> groups) throws DirectoryNotFoundException
    {
        Set<InternalGroup> groupsToAdd = new HashSet<InternalGroup>();

        Set<Group> groupsFailedValidation = new HashSet<Group>();

        for (Group group : groups)
        {
            try
            {
                // prepare group
                InternalGroup groupToAdd = new InternalGroup(group, getDirectory(group));
                groupToAdd.setCreatedDateToNow();
                groupToAdd.setUpdatedDateToNow();

                // add transaction group
                groupsToAdd.add(groupToAdd);
            }
            catch (IllegalArgumentException e)
            {
                logger.error("Could not add group <" + group.getName() + ">: " + e.getMessage());
                groupsFailedValidation.add(group);
            }
        }

        // process the collection
        BatchResult<InternalGroup> daoResult = batchProcessor.execute(new MergeOperation(), groupsToAdd);

        // produce an amalgamated result consisting of successes, dao failures and validation failures
        BatchResult<Group> result = new BatchResult<Group>(groups.size());
        result.addSuccesses(daoResult.getSuccessfulEntities());
        result.addFailures(daoResult.getFailedEntities());
        result.addFailures(groupsFailedValidation);

        return result;
    }

    // this method is used for legacy imports
    public BatchResultWithIdReferences<Group> addAll(Collection<InternalGroupWithAttributes> groups)
    {
        Set<TransactionGroup<InternalGroup, InternalGroupAttribute>> groupsToAdd = new HashSet<TransactionGroup<InternalGroup, InternalGroupAttribute>>();

        Set<Group> groupsFailedValidation = new HashSet<Group>();

        for (InternalGroupWithAttributes group : groups)
        {
            try
            {
                InternalGroup groupToAdd = group.getInternalGroup();

                Set<InternalGroupAttribute> attribs = new HashSet<InternalGroupAttribute>();
                for (String name : group.getKeys())
                {
                    for (String val : group.getValues(name))
                    {
                        InternalGroupAttribute attrib = new InternalGroupAttribute(groupToAdd, name, val);
                        attribs.add(attrib);
                    }
                }

                // add transaction group
                groupsToAdd.add(new TransactionGroup<InternalGroup, InternalGroupAttribute>(groupToAdd, attribs));
            }
            catch (IllegalArgumentException e)
            {
                logger.error("Could not add group <" + group.getName() + ">: " + e.getMessage());
                groupsFailedValidation.add(group);
            }
        }

        // process the collection
        BatchResult<TransactionGroup<InternalGroup, InternalGroupAttribute>> daoResult = batchProcessor.execute(new MergeOperation(), groupsToAdd);

        // produce an amalgamated result consisting of dao failures and validation failures
        BatchResultWithIdReferences<Group> result = new BatchResultWithIdReferences<Group>(groups.size());
        for (TransactionGroup<InternalGroup, InternalGroupAttribute> transactionGroup : daoResult.getFailedEntities())
        {
            result.addFailure(transactionGroup.getPrimaryObject());
        }
        result.addFailures(groupsFailedValidation);

        // now add the ID references of the successfully added users
        for (TransactionGroup<InternalGroup, InternalGroupAttribute> group : groupsToAdd)
        {
            result.addIdReference(group.getPrimaryObject());
        }

        return result;
    }

    public InternalGroup addLocal(Group group) throws DirectoryNotFoundException, InvalidGroupException
    {
        return add(group, true);
    }

    public InternalGroup findByName(long directoryId, String groupName) throws GroupNotFoundException
    {
        InternalGroup result = findByNameInternal(directoryId, groupName);
        if (result == null)
        {
            throw new GroupNotFoundException(groupName);
        }

        return result;
    }

    private InternalGroup findByNameInternal(long directoryId, String groupName)
    {
        return (InternalGroup) CriteriaFactory.createCriteria(session(), InternalGroup.class)
                .add(Restrictions.eq("directory.id", directoryId))
                .add(Restrictions.eq("lowerName", toLowerCase(groupName)))
                .uniqueResult();
    }

    public InternalGroupWithAttributes findByNameWithAttributes(long directoryId, String groupName) throws GroupNotFoundException
    {
        InternalGroup group = findByName(directoryId, groupName);

        Set<InternalGroupAttribute> attributesList = findGroupAttributes(group.getId());

        Map<String, Set<String>> attributesMap = attributesHelper.attributesListToMap(attributesList);

        return new InternalGroupWithAttributes(group, attributesMap);
    }

    public Collection<InternalGroup> findByNames(long directoryID, Collection<String> groupnames)
    {
        return batchFinder.find(directoryID, groupnames, InternalGroup.class);
    }

    @SuppressWarnings("unchecked")
    public Set<InternalGroupAttribute> findGroupAttributes(long groupId)
    {
        return Sets.<InternalGroupAttribute>newHashSet(
                CriteriaFactory.createCriteria(session(), InternalGroupAttribute.class)
                        .add(Restrictions.eq("group.id", groupId))
                        .list());
    }

    public Class getPersistentClass()
    {
        return InternalGroup.class;
    }

    public void remove(Group group) throws GroupNotFoundException
    {
        InternalGroup groupToRemove = findByName(group.getDirectoryId(), group.getName());

        // remove all group relationships
        membershipDao.removeGroupMembers(group.getDirectoryId(), group.getName());
        membershipDao.removeGroupMemberships(group.getDirectoryId(), group.getName());

        // remove all group attributes manually.
        session().getNamedQuery("removeAllInternalGroupAttributes")
                .setEntity("group", groupToRemove)
                .executeUpdate();

        // remove the group
        super.remove(groupToRemove);
    }

    public void removeAll(long directoryId)
    {
        // remove all relationships
        membershipDao.removeAllRelationships(directoryId);

        // remove all group attributes and groups
        session().getNamedQuery("removeAllInternalGroupAttributesInDirectory")
                .setLong("directoryId", directoryId)
                .executeUpdate();
        session().getNamedQuery("removeAllGroupsInDirectory")
                .setLong("directoryId", directoryId)
                .executeUpdate();
    }

    @Override
    public BatchResult<String> removeAllGroups(long directoryId, Set<String> groupNames)
    {
        Collection<InternalGroup> groups = findByNames(directoryId, groupNames);
        BatchResult<InternalGroup> batchResult = batchProcessor.execute(new RemoveGroupOperation(), groups);

        BatchResult<String> overallResult = new BatchResult<String>(groupNames.size());
        overallResult.addFailures(Collections2.transform(batchResult.getFailedEntities(), InternalEntityUtils.GET_NAME));
        overallResult.addSuccesses(Collections2.transform(batchResult.getSuccessfulEntities(), InternalEntityUtils.GET_NAME));

        return overallResult;
    }

    public void removeAttribute(Group group, String attributeName) throws GroupNotFoundException
    {
        removeAttributes(findByName(group.getDirectoryId(), group.getName()), attributeName);
    }

    public InternalGroup rename(Group group, String newName) throws GroupNotFoundException, InvalidGroupException
    {
        InternalGroup groupToRename = findByName(group.getDirectoryId(), group.getName());

        String oldName = groupToRename.getName();

        final InternalGroup existingGroup = findByNameInternal(group.getDirectoryId(), newName);

        if (existingGroup != null)
        {
            throw new InvalidGroupException(group, "Cannot rename group as group with new name already exists: " + group.getName());
        }

        groupToRename.renameTo(newName);
        groupToRename.setUpdatedDateToNow();
        super.update(groupToRename);

        membershipDao.renameGroupRelationships(groupToRename.getDirectoryId(), oldName, groupToRename.getName());

        return groupToRename;
    }

    public <T> List<T> search(long directoryId, EntityQuery<T> query)
    {
        if (query.getEntityDescriptor().getEntityType() != Entity.GROUP)
        {
            throw new IllegalArgumentException("GroupDAO can only evaluate EntityQueries for Entity.GROUP");
        }
        HQLQuery hqlQuery = hqlQueryTranslater.asHQL(directoryId, query);

        Query hibernateQuery = createHibernateQuery(query, hqlQuery);

        return HibernateSearchResultsTransformer.transformResults(hibernateQuery.list());
    }

    public void storeAttributes(Group group, Map<String, Set<String>> attributes) throws GroupNotFoundException
    {
        InternalGroupWithAttributes groupToUpdate = findByNameWithAttributes(group.getDirectoryId(), group.getName());

        // remove all matching attributes
        for (String attributeName : attributes.keySet())
        {
            if (groupToUpdate.getValue(attributeName) != null)
            {
                removeAttributes(groupToUpdate.getInternalGroup(), attributeName);
            }
        }

        // add all provided attributes
        for (Map.Entry<String, Set<String>> entry : attributes.entrySet())
        {
            for (String value : entry.getValue())
            {
                addAttribute(groupToUpdate.getInternalGroup(), entry.getKey(), value);
            }
        }
    }

    public InternalGroup update(Group group) throws GroupNotFoundException
    {
        InternalGroup groupToUpdate = findByName(group.getDirectoryId(), group.getName());

        if (groupToUpdate.getType() != group.getType())
        {
            throw new UnsupportedOperationException("Cannot modify the GroupType for an Internal group");
        }

        groupToUpdate.updateDetailsFrom(group);
        groupToUpdate.setUpdatedDateToNow();
        super.update(groupToUpdate);

        return groupToUpdate;
    }

    private InternalGroup add(Group group, boolean local) throws DirectoryNotFoundException, InvalidGroupException
    {
        final InternalGroup existingGroup = findByNameInternal(group.getDirectoryId(), group.getName());

        if (existingGroup != null)
        {
            throw new InvalidGroupException(group, "Cannot add group as group with new name already exists: " + group.getName());
        }

        InternalGroup groupToAdd = new InternalGroup(group, getDirectory(group));
        groupToAdd.setLocal(local);
        groupToAdd.setCreatedDateToNow();
        groupToAdd.setUpdatedDateToNow();
        super.save(groupToAdd);

        return groupToAdd;
    }

    private void addAttribute(InternalGroup group, String attributeName, String attributeValue)
    {
        InternalGroupAttribute attribute = new InternalGroupAttribute(group, attributeName, attributeValue);
        session().save(attribute);
    }

    private Directory getDirectory(Group group) throws DirectoryNotFoundException
    {
        Validate.notNull(group.getDirectoryId(), "Cannot add a group with null directoryId");
        return directoryDao.findById(group.getDirectoryId());
    }

    private void removeAttributes(InternalGroup group, String attributeName)
    {
        session().getNamedQuery("removeInternalGroupAttributes")
                .setEntity("group", group)
                .setString("attributeName", attributeName)
                .executeUpdate();
    }

    @Autowired
    public void setAttributesHelper(InternalAttributesHelper attributesHelper)
    {
        this.attributesHelper = attributesHelper;
    }

    @Autowired
    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }

    @Autowired
    public void setHqlQueryTranslater(HQLQueryTranslater hqlQueryTranslater)
    {
        this.hqlQueryTranslater = hqlQueryTranslater;
    }

    @Autowired
    public void setMembershipDao(InternalMembershipDao membershipDao)
    {
        this.membershipDao = membershipDao;
    }
}
