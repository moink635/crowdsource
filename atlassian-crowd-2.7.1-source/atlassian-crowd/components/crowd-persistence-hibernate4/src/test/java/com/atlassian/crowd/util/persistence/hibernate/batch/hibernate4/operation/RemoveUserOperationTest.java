package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RemoveUserOperationTest
{
    @Mock
    private Session session;

    @Before
    public void setup()
    {
        Query query = mock(Query.class, Mockito.RETURNS_MOCKS);
        when(session.getNamedQuery(anyString())).thenReturn(query);
    }

    @Test
    public void testPerformOperationShouldRemoveMembershipsAndAttributesAndUser() throws Exception
    {
        InternalUser entity = new InternalUser(
                new UserTemplateWithCredentialAndAttributes("user", 0, new PasswordCredential("", true)),
                mock(Directory.class));

        RemoveUserOperation removeUserOperation = new RemoveUserOperation();
        removeUserOperation.performOperation(entity, session);

        verify(session).getNamedQuery("removeAllEntityMemberships");
        verify(session).getNamedQuery("removeAllInternalUserAttributes");
        verify(session).delete(entity);
    }
}
