package com.atlassian.crowd.dao.group;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.group.InternalGroupAttribute;
import com.atlassian.crowd.model.group.InternalGroupWithAttributes;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchFinder;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.TransactionGroup;
import com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation.MergeOperation;
import com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation.RemoveGroupOperation;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GroupDAOHibernateTest
{
    private static final long DIRECTORY_ID = 0;

    @Mock
    private BatchProcessor batchProcessor;
    @Mock
    private BatchFinder batchFinder;
    @Mock
    private DirectoryDao directoryDao;
    @Mock
    private Directory directory;

    @InjectMocks
    @SuppressWarnings("unused")
    private GroupDAOHibernate groupDAOHibernate;

    @Before
    public void setup() throws Exception
    {
        when(directoryDao.findById(DIRECTORY_ID)).thenReturn(directory);
    }

    @Test
    public void testAddAllShouldUseBatchProcessor() throws Exception
    {
        when(batchProcessor.execute(isA(MergeOperation.class), any(Set.class)))
                .thenReturn(new BatchResult<InternalGroup>(2));

        Set<GroupTemplate> groups = ImmutableSet.of(createGroupTemplate("group 1"), createGroupTemplate("group 2"));
        groupDAOHibernate.addAll(groups);

        verify(batchProcessor).execute(isA(MergeOperation.class), eq(groups));
    }

    @Test
    public void testLegacyAddAllShouldUseBatchProcessor() throws Exception
    {
        when(batchProcessor.execute(isA(MergeOperation.class), any(Set.class)))
                .thenReturn(new BatchResult<TransactionGroup<InternalGroup, InternalGroupAttribute>>(2));

        groupDAOHibernate.addAll(ImmutableList.of(createSampleInternalGroupWithAttributes("group 1"),
                createSampleInternalGroupWithAttributes("group 2")));

        verify(batchProcessor).execute(isA(MergeOperation.class), any(Set.class));
    }

    @Test
    public void testRemoveAllGroupsShouldUseBatchProcessor() throws Exception
    {
        ImmutableSet<String> groupNames = ImmutableSet.of("existing group 1", "existing group 2");
        Collection<InternalGroup> groupCollection = ImmutableList.of(
                createInternalGroup("group 1"),
                createInternalGroup("group 2"));
        when(batchFinder.find(DIRECTORY_ID, groupNames, InternalGroup.class)).thenReturn(groupCollection);

        when(batchProcessor.execute(isA(RemoveGroupOperation.class), eq(groupCollection)))
                .thenReturn(new BatchResult<InternalGroup>(0));

        groupDAOHibernate.removeAllGroups(DIRECTORY_ID, groupNames);

        verify(batchProcessor).execute(isA(RemoveGroupOperation.class), eq(groupCollection));
    }

    private InternalGroupWithAttributes createSampleInternalGroupWithAttributes(String name)
    {
        return new InternalGroupWithAttributes(createInternalGroup(name), Collections.<String, Set<String>>emptyMap());
    }

    private InternalGroup createInternalGroup(String name)
    {
        return new InternalGroup(createGroupTemplate(name), directory);
    }

    private GroupTemplate createGroupTemplate(String name)
    {
        return new GroupTemplate(name, DIRECTORY_ID, GroupType.GROUP);
    }
}
