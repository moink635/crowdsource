package com.atlassian.crowd.dao.user;

import java.util.Collection;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchFinder;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation.MergeOperation;
import com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation.RemoveUserOperation;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDAOHibernateTest
{
    @Mock
    private BatchProcessor batchProcessor;
    @Mock
    private BatchFinder batchFinder;
    @Mock
    private Directory directory;

    private static final long DIRECTORY_ID = 0;

    @InjectMocks
    private UserDAOHibernate userDAOHibernate = new UserDAOHibernate()
    {
        @Override // overridden for testAddAllShouldUseBatchProcessor()
        public <T> T load(Class<T> persistentClass, long id) throws ObjectNotFoundException
        {
            return mock(persistentClass);
        }

        @Override // overridden for testLegacyAddAllShouldUseBatchProcessor()
        public <T> T loadReference(Class<T> persistentClass, long id)
        {
            return mock(persistentClass);
        }
    };

    @Test
    public void testAddAllShouldUseBatchProcessor() throws Exception
    {
        when(batchProcessor.execute(isA(MergeOperation.class), any(Collection.class)))
                .thenReturn(new BatchResult<UserTemplateWithCredentialAndAttributes>(2));

        userDAOHibernate.addAll(ImmutableSet.of(createUserTemplate("user 1"), createUserTemplate("user 2")));

        verify(batchProcessor).execute(isA(MergeOperation.class), any(Collection.class));
    }

    @Test
    public void testLegacyAddAllShouldUseBatchProcessor() throws Exception
    {
        when(batchProcessor.execute(isA(MergeOperation.class), any(Collection.class)))
                .thenReturn(new BatchResult<UserTemplateWithCredentialAndAttributes>(2));

        userDAOHibernate.addAll(ImmutableList.of(createUserTemplate("user 1"), createUserTemplate("user 2")));

        verify(batchProcessor).execute(isA(MergeOperation.class), any(Collection.class));
    }

    @Test
    public void testRemoveAllUsersShouldUseBatchProcessor() throws Exception
    {
        UserTemplateWithCredentialAndAttributes user1 = createUserTemplate("user 1");
        UserTemplateWithCredentialAndAttributes user2 = createUserTemplate("user 2");

        ImmutableSet<String> userNames = ImmutableSet.of(user1.getName(), user2.getName());
        Collection<InternalUser> userCollection = ImmutableList.of(
                new InternalUser(user1, directory), new InternalUser(user2, directory));
        when(batchFinder.find(DIRECTORY_ID, userNames, InternalUser.class)).thenReturn(userCollection);

        when(batchProcessor.execute(isA(RemoveUserOperation.class), eq(userCollection)))
                .thenReturn(new BatchResult<InternalUser>(0));

        userDAOHibernate.removeAllUsers(DIRECTORY_ID, userNames);

        verify(batchProcessor).execute(isA(RemoveUserOperation.class), eq(userCollection));
    }

    private UserTemplateWithCredentialAndAttributes createUserTemplate(String name)
    {
        return new UserTemplateWithCredentialAndAttributes(name, DIRECTORY_ID, new PasswordCredential("", true));
    }
}
