package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.InternalGroup;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RemoveGroupOperationTest
{
    @Mock
    private Session session;

    @Before
    public void setup()
    {
        Query query = mock(Query.class, Mockito.RETURNS_MOCKS);
        when(session.getNamedQuery(anyString())).thenReturn(query);
    }

    @Test
    public void testPerformOperationShouldRemoveMembershipsAndAttributesAndUser() throws Exception
    {
        InternalGroup entity = new InternalGroup(new GroupTemplate("group", 0), mock(Directory.class));

        RemoveGroupOperation removeGroupOperation = new RemoveGroupOperation();
        removeGroupOperation.performOperation(entity, session);

        verify(session).getNamedQuery("removeAllEntityMembers");
        verify(session).getNamedQuery("removeAllEntityMemberships");
        verify(session).getNamedQuery("removeAllInternalGroupAttributes");
        verify(session).delete(entity);
    }
}