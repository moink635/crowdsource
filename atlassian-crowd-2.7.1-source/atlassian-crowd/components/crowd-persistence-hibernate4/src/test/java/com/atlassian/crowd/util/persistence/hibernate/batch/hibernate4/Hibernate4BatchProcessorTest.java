package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.atlassian.crowd.util.persistence.hibernate.batch.HibernateOperation;
import com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.Hibernate4BatchProcessor;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.anyObject;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class Hibernate4BatchProcessorTest
{
    @Mock
    private HibernateOperation<Session> hibernateOperation;
    @Mock
    private Session session;
    @Mock
    private SessionFactory sessionFactory;
    @Mock
    private Transaction transaction;

    private Hibernate4BatchProcessor batchProcessor;
    private int nextSampleDataId;

    @Before
    public void setUp() throws Exception
    {
        when(sessionFactory.openSession()).thenReturn(session);
        when(session.beginTransaction()).thenReturn(transaction);
        when(session.getTransaction()).thenReturn(transaction);

        batchProcessor = new Hibernate4BatchProcessor(sessionFactory);
        batchProcessor.setBatchSize(20);

        nextSampleDataId = 0;
    }

    @After
    public void tearDown() throws Exception
    {
        batchProcessor = null;
        sessionFactory = null;
        hibernateOperation = null;
    }

    @Test
    public void testCallOperationForEachObject()
    {
        batchProcessor.execute(hibernateOperation, makeSampleSet(15));

        verify(hibernateOperation, times(15)).performOperation(anyObject(), eq(session));
    }

    @Test
    public void testOneBatchWithRetry()
    {
        Set<Serializable> data = new HashSet<Serializable>();
        data.add("error");
        data.addAll(makeSampleSet(10));

        doThrow(new RuntimeException()).when(hibernateOperation).performOperation("error", session);

        batchProcessor.execute(hibernateOperation, data);

        verify(sessionFactory, times(1)).openSession();
        verify(transaction, times(10)).commit(); // commit all successes
        verify(transaction, times(2)).rollback(); // rollback all failures
        verify(session, times(12)).clear(); // clear even on failures
        verify(session, times(10)).flush(); // flush only on successful
    }

    @Test
    public void testOneSessionPerCollection()
    {
        batchProcessor.execute(hibernateOperation, makeSampleSet(25));

        verify(sessionFactory, times(1)).openSession();
    }

    @Test
    public void testOneTransactionPerBatch()
    {
        batchProcessor.execute(hibernateOperation, makeSampleSet(25));

        verify(transaction, times(2)).commit();
        verify(session, times(2)).flush();
    }

    private Set<Serializable> makeSampleSet(int size)
    {
        Set<Serializable> objects = new HashSet<Serializable>(size);
        for (int i = 0; i < size; i++)
        {
            objects.add("object" + (nextSampleDataId++));
        }
        return objects;
    }
}
