package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation;

import org.hibernate.Query;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MergeOperationTest
{
    @Mock
    private Session session;

    @Before
    public void setup()
    {
        Query query = mock(Query.class, Mockito.RETURNS_MOCKS);
        when(session.getNamedQuery(anyString())).thenReturn(query);
    }

    @Test
    public void testPerformOperationShouldMergeEntity() throws Exception
    {
        Object entity = new Object();

        MergeOperation mergeOperation = new MergeOperation();
        mergeOperation.performOperation(entity, session);

        verify(session).merge(entity);
    }
}
