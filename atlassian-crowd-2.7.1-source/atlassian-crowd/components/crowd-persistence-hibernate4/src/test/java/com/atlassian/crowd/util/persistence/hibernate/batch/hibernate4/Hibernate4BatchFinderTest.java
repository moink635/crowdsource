package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4;

import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.Hibernate4BatchFinder;
import junit.framework.TestCase;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;

import java.util.ArrayList;
import java.util.Collection;

import static org.mockito.Mockito.*;

public class Hibernate4BatchFinderTest extends TestCase
{
    private Hibernate4BatchFinder batchFinder;
    private Session session;
    private SessionFactory sessionFactory;

    public void testFindWithMultipleBatches()
    {
        Collection<String> names = new ArrayList<String>();
        int sampleSize = 50;
        for (int i = 0; i < sampleSize; i++)
        {
            String username = "user" + (i + 1);
            names.add(username);
        }

        Criteria criteria = mock(Criteria.class);
        doReturn(criteria).when(criteria).add(isA(Criterion.class));
        doReturn(criteria).when(criteria).setCacheable(eq(true));
        doReturn(criteria).when(session).createCriteria(InternalUser.class);
        batchFinder.find(1, names, InternalUser.class);
        verify(session, times(3)).createCriteria(InternalUser.class); // three batches
    }

    public void testFindWithSingleBatch()
    {
        Collection<String> names = new ArrayList<String>();
        names.add("user1");

        final Criteria criteria = mock(Criteria.class);
        doReturn(criteria).when(criteria).add(isA(Criterion.class));
        doReturn(criteria).when(criteria).setCacheable(eq(true));
        doReturn(criteria).when(session).createCriteria(InternalUser.class);
        batchFinder.find(1, names, InternalUser.class);
        verify(session, times(1)).createCriteria(InternalUser.class);
    }

    protected void setUp() throws Exception
    {
        Transaction transaction = mock(Transaction.class);

        session = mock(Session.class);
        doReturn(transaction).when(session).beginTransaction();
        doReturn(transaction).when(session).getTransaction();

        sessionFactory = mock(SessionFactory.class);
        doReturn(session).when(sessionFactory).openSession();

        batchFinder = new Hibernate4BatchFinder(sessionFactory);
        batchFinder.setBatchSize(20);
    }

    @Override
    protected void tearDown() throws Exception
    {
        batchFinder = null;
        sessionFactory = null;
    }
}
