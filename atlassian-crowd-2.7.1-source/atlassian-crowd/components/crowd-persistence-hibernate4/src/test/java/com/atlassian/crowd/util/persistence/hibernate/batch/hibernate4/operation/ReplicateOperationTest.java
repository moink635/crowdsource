package com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation;

import org.hibernate.Query;
import org.hibernate.ReplicationMode;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ReplicateOperationTest
{
    @Mock
    private Session session;

    private final Object entity = new Object();

    @Before
    public void setup()
    {
        Query query = mock(Query.class, Mockito.RETURNS_MOCKS);
        when(session.getNamedQuery(anyString())).thenReturn(query);
    }

    @Test
    public void testPerformOperationShouldReplicateEntity() throws Exception
    {
        performReplicationOperationWithModeOf(ReplicationMode.OVERWRITE);

        verify(session).replicate(eq(entity), any(ReplicationMode.class));
    }

    @Test
    public void testPerformOperationShouldUseGivenReplicationMode() throws Exception
    {
        performReplicationOperationWithModeOf(ReplicationMode.OVERWRITE);

        verify(session).replicate(entity, ReplicationMode.OVERWRITE);

        reset(session);

        performReplicationOperationWithModeOf(ReplicationMode.LATEST_VERSION);

        verify(session).replicate(entity, ReplicationMode.LATEST_VERSION);
    }

    private void performReplicationOperationWithModeOf(ReplicationMode replicationMode)
    {
        ReplicateOperation replicateOperation = new ReplicateOperation(replicationMode);
        replicateOperation.performOperation(entity, session);
    }
}
