package com.atlassian.crowd.apacheds;

import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.directory.server.constants.ServerDNConstants;
import org.apache.directory.server.core.CoreSession;
import org.apache.directory.server.core.DefaultDirectoryService;
import org.apache.directory.server.core.DirectoryService;
import org.apache.directory.server.core.filtering.EntryFilteringCursor;
import org.apache.directory.server.core.partition.Partition;
import org.apache.directory.server.core.partition.impl.btree.jdbm.JdbmIndex;
import org.apache.directory.server.core.partition.impl.btree.jdbm.JdbmPartition;
import org.apache.directory.server.core.partition.ldif.LdifPartition;
import org.apache.directory.server.core.schema.SchemaPartition;
import org.apache.directory.server.ldap.LdapServer;
import org.apache.directory.server.protocol.shared.transport.TcpTransport;
import org.apache.directory.server.xdbm.Index;
import org.apache.directory.shared.ldap.entry.DefaultServerEntry;
import org.apache.directory.shared.ldap.entry.Entry;
import org.apache.directory.shared.ldap.entry.EntryAttribute;
import org.apache.directory.shared.ldap.entry.ServerEntry;
import org.apache.directory.shared.ldap.entry.Value;
import org.apache.directory.shared.ldap.exception.LdapNoSuchObjectException;
import org.apache.directory.shared.ldap.ldif.LdifEntry;
import org.apache.directory.shared.ldap.ldif.LdifReader;
import org.apache.directory.shared.ldap.message.AliasDerefMode;
import org.apache.directory.shared.ldap.name.DN;
import org.apache.directory.shared.ldap.schema.AttributeTypeOptions;
import org.apache.directory.shared.ldap.schema.SchemaManager;
import org.apache.directory.shared.ldap.schema.ldif.extractor.SchemaLdifExtractor;
import org.apache.directory.shared.ldap.schema.loader.ldif.LdifSchemaLoader;
import org.apache.directory.shared.ldap.schema.manager.impl.DefaultSchemaManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Servlet context listener to start and stop ApacheDS.
 *
 * The following environment variables are supported:
 * <ul>
 * <li>apacheds.ldap.port - the LDAP connector port (default: 10389)</li>
 * <li>apacheds.partition.name - the LDAP partition distinguished name (default: dc=example,dc=com)</li>
 * <li>apacheds.ldif.file - the LDIF classpath file to import just after startup (default: default-example.ldif)</li>
 * </ul>
 *
 * The server is both self-contained and light-weight.
 */
public class ApacheDSContextListener implements ServletContextListener
{
    private static final String APACHEDS_LDAP_PORT_KEY = "apacheds.ldap.port";
    private static final String APACHEDS_LDAP_PORT_DEFAULT = "10389";

    private static final String APACHEDS_PARTITION_NAME_KEY = "apacheds.partition.name";
    private static final String APACHEDS_PARTITION_NAME_DEFAULT = "dc=example,dc=com";

    private static final String APACHEDS_LDIF_FILE_KEY = "apacheds.ldif.file";
    private static final String APACHEDS_LDIF_FILE_DEFAULT = "default-example.ldif";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private DirectoryService directoryService;
    private LdapServer ldapService;
    private CoreSession rootDSE;
    private DN baseDn;

    protected String getPartitionDN()
    {
        return System.getProperty(APACHEDS_PARTITION_NAME_KEY, APACHEDS_PARTITION_NAME_DEFAULT);
    }

    protected int getLdapPort()
    {
        String port = System.getProperty(APACHEDS_LDAP_PORT_KEY, APACHEDS_LDAP_PORT_DEFAULT);
        return Integer.parseInt(port);
    }

    protected String getLdifFile()
    {
        return System.getProperty(APACHEDS_LDIF_FILE_KEY, APACHEDS_LDIF_FILE_DEFAULT);
    }

    int replaceLdif(InputStream in, boolean verifyEntries) throws Exception
    {
        if (baseDn != null)
        {
            try
            {
                deleteRecursively(baseDn);
            }
            catch (LdapNoSuchObjectException e)
            {
                logger.warn("Failed to delete " + baseDn, e);
            }
        }

        return loadLdif(in, verifyEntries).size();
    }

    void deleteRecursively(DN dn) throws Exception
    {
        logger.debug("Checking {}", dn);

        EntryFilteringCursor results = rootDSE.list(dn, AliasDerefMode.NEVER_DEREF_ALIASES, Collections.<AttributeTypeOptions>emptySet());

        while (results.next())
        {
            DN childDn = results.get().getDn();
            logger.debug("Found {}", childDn);

            deleteRecursively(results.get().getDn());
        }

        logger.debug("Deleting {}", dn);
        rootDSE.delete(dn);
    }

    /**
     * Loads an LDIF from an input stream and adds the entries it contains to
     * the server.  It appears as though the administrator added these entries
     * to the server.
     *
     * @param in the input stream containing the LDIF entries to load
     * @param verifyEntries whether or not all entry additions are checked
     * to see if they were in fact correctly added to the server
     * @return a list of entries added to the server in the order they were added
     * @throws javax.naming.NamingException of the load fails
     */
    protected List<LdifEntry> loadLdif(InputStream in, boolean verifyEntries) throws Exception
    {
        LdifReader ldifReader = new LdifReader(in);
        List<LdifEntry> entries = new ArrayList<LdifEntry>();

        try
        {
            for (LdifEntry entry : ldifReader)
            {
                rootDSE.add(
                    new DefaultServerEntry(directoryService.getSchemaManager(), entry.getEntry()));

                if (verifyEntries)
                {
                    verify(entry);
                    logger.info("Successfully verified addition of entry: " + entry.getDn());
                }
                else
                {
                    logger.info("Added entry without verification:" + entry.getDn());
                }

                entries.add(entry);
            }
        }
        finally
        {
            ldifReader.close();
        }

        return entries;
    }

    /**
     * Verifies that an entry exists in the directory with the
     * specified attributes.
     *
     * @param entry the entry to verify
     * @throws javax.naming.NamingException if there are problems accessing the entry
     */
    protected void verify(LdifEntry entry) throws Exception
    {
        Entry readEntry = rootDSE.lookup(entry.getDn());

        for (EntryAttribute readAttribute:readEntry)
        {
            String id = readAttribute.getId();
            EntryAttribute origAttribute = entry.getEntry().get(id);

            for (Value<?> value:origAttribute)
            {
                if (! readAttribute.contains(value))
                {
                    logger.error("Failed to verify entry addition of: " + entry.getDn());
                    logger.error(id + " attribute in original entry missing from read entry.");

                    throw new AssertionError("Failed to verify entry addition of " + entry.getDn() );
                }
            }
        }
    }

    private void initSchemaPartition() throws Exception
    {
        File wdf = directoryService.getWorkingDirectory();

        // Extract the schema on disk (a brand new one) and load the registries
        SchemaLdifExtractor extractor = new AppServerFriendlySchemaLdifExtractor(wdf);
        extractor.extractOrCopy(true);

        File schemaRepository = new File(wdf, "schema");

        LdifPartition ldifPartition = new LdifPartition();
        ldifPartition.setWorkingDirectory(schemaRepository.getPath());

        SchemaPartition schemaPartition = directoryService.getSchemaService().getSchemaPartition();
        schemaPartition.setWrappedPartition(ldifPartition);

        SchemaManager schemaManager = new DefaultSchemaManager(new LdifSchemaLoader(schemaRepository));
        schemaManager.enable("nis");

        schemaManager.loadAllEnabled();

        directoryService.setSchemaManager(schemaManager);
        schemaPartition.setSchemaManager(schemaManager);

        List<Throwable> errors = schemaManager.getErrors();

        if (errors.size() != 0)
        {
            throw new Exception("Schema load failed : " + errors);
        }
    }

    private JdbmPartition addPartition(String partitionId, String partitionDn) throws Exception
    {
        JdbmPartition partition = new JdbmPartition();
        partition.setId(partitionId);
        partition.setPartitionDir(new File(directoryService.getWorkingDirectory(), partitionId));
        partition.setSuffix(partitionDn);
        directoryService.addPartition(partition);

        return partition;
    }

    private void addIndex(JdbmPartition partition, String... attrs)
    {
        HashSet<Index<?, ServerEntry, Long>> indexedAttributes = new HashSet<Index<?, ServerEntry, Long>>();

        for (String attribute : attrs)
        {
            indexedAttributes.add(new JdbmIndex<String, ServerEntry>(attribute));
        }

        partition.setIndexedAttributes(indexedAttributes);
    }

    /**
     * Startup ApacheDS embedded.
     *
     * If you get an error message in the logs like:
     *   "The value 'inetOrgPerson' is incorrect, it hasn't been added"
     * Don't stress, it's an ApacheDS bug: https://issues.apache.org/jira/browse/DIRSERVER-1253
     *
     * Also if you see an error message like this:
     *   "OID for name 'monitorcontext' was not found within the OID registry"
     * It's an Apache LDAP Studio bug.
     */
    public void contextInitialized(ServletContextEvent evt)
    {
        int port = getLdapPort();

        logger.info("Initialising ApacheDS on port: " + port);

        try
        {
            directoryService = new DefaultDirectoryService();

            // determine an appropriate working directory
            ServletContext servletContext = evt.getServletContext();

            File workingDir = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
            directoryService.setWorkingDirectory(workingDir);

            directoryService.getChangeLog().setEnabled(false);

            initSchemaPartition();

            Partition systemPartition = addPartition("system", ServerDNConstants.SYSTEM_DN);
            directoryService.setSystemPartition(systemPartition);

            JdbmPartition apachePartition = addPartition("example", getPartitionDN());
            addIndex(apachePartition, "objectClass", "ou", "uid");

            baseDn = apachePartition.getSuffixDn();


            directoryService.setShutdownHookEnabled(true);

            directoryService.startup();

            rootDSE = directoryService.getAdminSession();

            importLdif();

            /* Expose as LDAP */
            ldapService = new LdapServer();
            ldapService.setTransports(new TcpTransport(port));
            ldapService.setDirectoryService(directoryService);

            // startup
            ldapService.start();

            // store directoryService in context to provide it to servlets etc (we don't use it)
            servletContext.setAttribute(DirectoryService.JNDI_KEY, directoryService);
        }
        catch (Exception e)
        {
            logger.error("Failed to start ApacheDS");
            throw new RuntimeException(e);
        }

        logger.info("ApacheDS successfully started.");
        evt.getServletContext().setAttribute(getClass().getName(), this);
    }

    private void importLdif()
    {
        String ldifFile = getLdifFile();
        logger.info("Loading LDIF file: " + ldifFile);
        try
        {
            InputStream ldif;

            URL url = Thread.currentThread().getContextClassLoader().getResource(ldifFile);
            if (url != null)
            {
                ldif = url.openStream();
            }
            else
            {
                ldif = getClass().getResourceAsStream(ldifFile);
                if (ldif == null)
                {
                    throw new RuntimeException("LDIF file not found: " + ldifFile);
                }
            }
            loadLdif(ldif, true);
        }
        catch (Exception e)
        {
            logger.error("Failed to load LDIF");
            throw new RuntimeException(e);
        }

        logger.info("LDIF successfully imported");
    }

    /**
     * Shutdown ApacheDS embedded.
     */
    public void contextDestroyed(ServletContextEvent evt)
    {
        logger.info("Shutting down ApacheDS");
        try
        {
            if (ldapService != null)
            {
                ldapService.stop();
            }

            if (directoryService != null)
            {
                directoryService.shutdown();
            }
        }
        catch (Exception e)
        {
            logger.info("Error shutting down ApacheDS: " + e.getMessage(), e);
            throw new RuntimeException(e);
        }
        logger.info("ApacheDS shutdown successfully");
    }
}
