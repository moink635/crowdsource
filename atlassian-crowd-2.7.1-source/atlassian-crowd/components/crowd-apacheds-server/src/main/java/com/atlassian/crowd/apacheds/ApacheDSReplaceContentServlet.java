package com.atlassian.crowd.apacheds;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A servlet to allow new LDIF to be posted. For example,
 * <pre>
 * curl --data-binary @default-example.ldif http://localhost:8095/apacheds15/content
 * </pre>
 */
public class ApacheDSReplaceContentServlet extends HttpServlet
{
    private ApacheDSContextListener apacheDs;

    @Override
    public void init(ServletConfig config) throws ServletException
    {
        apacheDs = (ApacheDSContextListener) config.getServletContext().getAttribute(ApacheDSContextListener.class.getName());
        if (apacheDs == null)
        {
            throw new ServletException("No ApacheDSContextListener in ServletContext");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        try
        {
            int count = apacheDs.replaceLdif(req.getInputStream(), true);

            resp.setContentType("text/plain");

            resp.getWriter().println("Entries stored: " + count);
            resp.getWriter().close();
        }
        catch (Exception e)
        {
            throw new ServletException(e);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/plain");

        PrintWriter w = resp.getWriter();

        w.println("Post LDIF.");
    }
}
