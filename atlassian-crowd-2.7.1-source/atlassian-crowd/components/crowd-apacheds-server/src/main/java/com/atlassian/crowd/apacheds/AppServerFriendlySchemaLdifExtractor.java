package com.atlassian.crowd.apacheds;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.google.common.base.Preconditions;

import org.apache.commons.io.IOUtils;
import org.apache.directory.shared.ldap.schema.ldif.extractor.SchemaLdifExtractor;
import org.apache.directory.shared.ldap.schema.ldif.extractor.impl.DefaultSchemaLdifExtractor;

/**
 * {@link DefaultSchemaLdifExtractor} makes assumptions about <code>java.class.path</code>. Since we're just copying
 * resources to the filing system from a JAR file, reimplement in a way that works for our testing.
 * This assumes that {@link SchemaLdifExtractor} is in a jar file that also holds the initial schema.
 */
public class AppServerFriendlySchemaLdifExtractor implements SchemaLdifExtractor
{
    private final File outputDirectory;

    public AppServerFriendlySchemaLdifExtractor(File outputDirectory)
    {
        this.outputDirectory = Preconditions.checkNotNull(outputDirectory);
    }

    @Override
    public void extractOrCopy() throws IOException
    {
        extractOrCopy(false);
    }

    @Override
    public boolean isExtracted()
    {
        return outputDirectory.exists();
    }

    public void extractOrCopy(boolean overwrite) throws IOException
    {
        if (!overwrite && outputDirectory.exists())
        {
            return;
        }

        if (!outputDirectory.exists())
        {
            outputDirectory.mkdir();
        }

        URL sl = SchemaLdifExtractor.class.getResource("/schema/ou=schema.ldif");
        JarURLConnection juc = (JarURLConnection) sl.openConnection();

        JarFile jf = juc.getJarFile();

        Enumeration<JarEntry> en = jf.entries();

        while (en.hasMoreElements())
        {
            JarEntry ne = en.nextElement();

            if (ne.getName().startsWith("schema/") && !ne.isDirectory())
            {
                File outputFile = new File(outputDirectory, ne.getName());
                outputFile.getParentFile().mkdirs();

                InputStream source = jf.getInputStream(ne);

                OutputStream out = new FileOutputStream(outputFile);

                IOUtils.copy(source, out);

                out.close();
                source.close();
            }
        }
    }
}
