package com.atlassian.crowd.apacheds;

import org.junit.Test;

public class ApacheDSContextListenerTest
{
    @Test
    public void shutdownOnUnstartedInstanceDoesNotFail()
    {
        new ApacheDSContextListener().contextDestroyed(null);
    }
}
