package com.atlassian.crowd.apacheds;

import java.io.File;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * A test launcher to invoke {@link ApacheDSContextListener} directly.
 */
public class Main
{
    public static void main(String[] args)
    {
        System.setProperty("apacheds.ldap.port", "12389");
        ServletContextListener cl = new ApacheDSContextListener();

        ServletContext source = mock(ServletContext.class);
        when(source.getAttribute("javax.servlet.context.tempdir")).thenReturn(new File("target", "apacheds-temp"));

        ServletContextEvent evt = new ServletContextEvent(source);
        cl.contextInitialized(evt);
    }
}
