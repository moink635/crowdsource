package com.atlassian.crowd.migration.legacy;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.migration.verify.LegacyXmlVerifier;
import com.atlassian.crowd.migration.verify.Verifier;
import junit.framework.TestCase;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.net.URL;

public class LegacyXmlVerifierTest extends TestCase
{
    private Verifier legacyXmlVerifier;

    @Override
    protected void setUp() throws Exception
    {
        legacyXmlVerifier = new LegacyXmlVerifier();
    }

    protected Element getXml(String name) throws DocumentException
    {
        // load xml file
        SAXReader reader = new SAXReader();
        final URL resource = ClassLoaderUtils.getResource(StringUtils.replace(getClass().getCanonicalName(), ".", File.separator) + "-" + name + ".xml", getClass());
        Document document = reader.read(resource);

        return document.getRootElement();
    }

    public void testVerifySuccess() throws DocumentException
    {
        legacyXmlVerifier.verify(getXml("success"));

        assertEquals(0, legacyXmlVerifier.getErrors().size());
    }

    public void testVerifyDuplicateGroup() throws DocumentException
    {
        legacyXmlVerifier.verify(getXml("duplicategroup"));

        assertEquals(1, legacyXmlVerifier.getErrors().size());
    }

    public void testVerifyDuplicateRole() throws DocumentException
    {
        legacyXmlVerifier.verify(getXml("duplicaterole"));

        assertEquals(1, legacyXmlVerifier.getErrors().size());
    }

    public void testVerifySameGroupAndRole() throws DocumentException
    {
        legacyXmlVerifier.verify(getXml("samegrouprole"));

        assertEquals(1, legacyXmlVerifier.getErrors().size());
    }
}