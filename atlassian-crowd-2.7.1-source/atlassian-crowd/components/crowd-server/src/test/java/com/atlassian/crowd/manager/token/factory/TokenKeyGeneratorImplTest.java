package com.atlassian.crowd.manager.token.factory;

import java.util.List;

import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.manager.proxy.TrustedProxyManager;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.security.random.SecureRandomService;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import junit.framework.TestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TokenKeyGeneratorImplTest extends TestCase
{

    // static user details
    private static final long DIRECTORY_ID = 1;
    private static final long DIRECTORY_ID_2 = 2;
    private static final String USERNAME = "test1";
    private static final String USERNAME_MIXEDCASE = "Test1";
    private static final String USERNAME_2 = "test2";

    // User agents for two different browsers
    private static final String IE_USER_AGENT = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322)";
    private static final String FIREFOX_USER_AGENT = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X; en-US; rv:1.8.0.6) Gecko/20060728 Firefox/1.5.0.6";

    // Remote IP address of a application/principal
    private static final String REMOTE_ADDRESS = "127.0.0.1";
    private static final String REMOTE_ADDRESS_2 = "192.168.3.28";
    private static final String REMOTE_ADDRESS_3 = "10.5.6.87";

    // Not a validation factor. Needed to check the browser is not included in validation.
    private static final String USER_AGENT = "User-Agent";

    // Application-specific privilege level
    private static final String WEBSUDO_PRIVILEGE_LEVEL = "WebSudo";

    // what the token should be
    private static final String VALIDATED_TOKEN_RANDOM_HASH = "7speBszspcZvF8Wp56jhQA00";
    private static final String VALIDATED_TOKEN_IDENTIFIER_HASH = "yWx6DrAetcqpk5OCnu5xlg00";

    private static final long NON_RANDOM_NUMBER = 123456789L;
    private static final ImmutableList<ValidationFactor> EMPTY_FACTORS = ImmutableList.of();

    private TokenFactoryImpl tokenFactoryImpl;
    private TrustedProxyManager trustedProxy;
    private ValidationFactor ieuserAgentValidationFactor;
    private ValidationFactor remoteAddressValidationFactor;
    private ValidationFactor firefoxUserAgentValidationFactor;
    private ValidationFactor xForwardedForValidationFactor;
    private TokenKeyGeneratorImpl tokenKeyGenerator;
    private SecureRandomService secureRandomService;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        trustedProxy = mock(TrustedProxyManager.class);

        secureRandomService = mock(SecureRandomService.class);

        tokenKeyGenerator = new TokenKeyGeneratorImpl(trustedProxy, "MD5");
        tokenFactoryImpl = new TokenFactoryImpl(tokenKeyGenerator, secureRandomService);

        ieuserAgentValidationFactor = new ValidationFactor(USER_AGENT, IE_USER_AGENT);
        firefoxUserAgentValidationFactor = new ValidationFactor(USER_AGENT, FIREFOX_USER_AGENT);
        remoteAddressValidationFactor = new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, REMOTE_ADDRESS);
        xForwardedForValidationFactor = new ValidationFactor(ValidationFactor.X_FORWARDED_FOR, REMOTE_ADDRESS_2);

        when(secureRandomService.nextLong()).thenReturn(NON_RANDOM_NUMBER);
    }

    @Override
    protected void tearDown() throws Exception
    {
        tokenFactoryImpl = null;
        trustedProxy = null;
        ieuserAgentValidationFactor = null;
        remoteAddressValidationFactor = null;
        firefoxUserAgentValidationFactor = null;
        xForwardedForValidationFactor = null;
        super.tearDown();
    }

    public void testTokenCreation() throws Exception
    {
        List<ValidationFactor> validationFactors = ImmutableList.of(remoteAddressValidationFactor);

        // run the generation
        Token token = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT, validationFactors);

        assertEquals(VALIDATED_TOKEN_RANDOM_HASH, token.getRandomHash());
        assertEquals(VALIDATED_TOKEN_IDENTIFIER_HASH, token.getIdentifierHash());

    }

    public void testTokenCreationWithDifferentUserAgents() throws Exception
    {
        List<ValidationFactor> validationFactors = ImmutableList.of(firefoxUserAgentValidationFactor, remoteAddressValidationFactor);

        // run the generation
        Token firefoxToken = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                                     validationFactors);

        validationFactors = ImmutableList.of(ieuserAgentValidationFactor, remoteAddressValidationFactor);

        // run the generation
        Token ieToken = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT, validationFactors);

        // Same value since we are ignoring browsers
        assertEquals(firefoxToken.getRandomHash(), ieToken.getRandomHash());
    }

    // Token generation should be unique across creation. We are using a 'real' secure random for this test
    public void testTokenCreationAlwaysCreatesTwoUniqueKeys() throws Exception
    {
        tokenFactoryImpl = new TokenFactoryImpl(tokenKeyGenerator);

        Token token1 = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                               Lists.newArrayList(remoteAddressValidationFactor));
        Token token2 = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                               Lists.newArrayList(remoteAddressValidationFactor));

        assertNotSame(token1.getRandomHash(), token2.getRandomHash());
    }

    // this test will ensure that different users (from the same directory) have unique tokens
    // note: there is a one in gazillion chance that we'll get an md5 hash collision :)
    public void testGenerateDifferentTokensForDifferentUsers() throws Exception
    {
        Token token1 = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                               Lists.newArrayList(remoteAddressValidationFactor));
        Token token2 = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME_2, TokenLifetime.USE_DEFAULT,
                                               Lists.newArrayList(remoteAddressValidationFactor));

        assertNotSame(token1.getRandomHash(), token2.getRandomHash());
    }

    // same as above, except user is constant and directories variant
    public void testGenerateDifferentTokensForSameUserInDifferentDirectories() throws Exception
    {
        Token token1 = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                               Lists.newArrayList(remoteAddressValidationFactor));
        Token token2 = tokenFactoryImpl.create(DIRECTORY_ID_2, USERNAME, TokenLifetime.USE_DEFAULT,
                                               Lists.newArrayList(remoteAddressValidationFactor));

        assertNotSame(token1.getRandomHash(), token2.getRandomHash());
    }

    public void testSameTokenGeneratedWithUpperCaseAndLowerCaseName() throws InvalidTokenException
    {
        Token token1 = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                               Lists.newArrayList(remoteAddressValidationFactor));
        Token token2 = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME_MIXEDCASE, TokenLifetime.USE_DEFAULT,
                                               Lists.newArrayList(remoteAddressValidationFactor));

        assertEquals(token1.getRandomHash(), token2.getRandomHash());
    }

    public void testXForwardedForIsIgnoredWithUntrustedProxy() throws Exception
    {
        when(trustedProxy.isTrusted(REMOTE_ADDRESS)).thenReturn(false);

        // run the generation
        Token token = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                              Lists.newArrayList(ieuserAgentValidationFactor, xForwardedForValidationFactor, remoteAddressValidationFactor));

        assertEquals(VALIDATED_TOKEN_RANDOM_HASH, token.getRandomHash());
        verify(trustedProxy, times(2)).isTrusted(REMOTE_ADDRESS);
    }

    public void testXForwardedForIsUsedForSafeProxies() throws Exception
    {
        when(trustedProxy.isTrusted(REMOTE_ADDRESS_2)).thenReturn(true);

        // run the generation
        Token token = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                              Lists.newArrayList(new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, REMOTE_ADDRESS_2), new ValidationFactor(ValidationFactor.X_FORWARDED_FOR, REMOTE_ADDRESS)));

        verify(trustedProxy, times(2)).isTrusted(REMOTE_ADDRESS_2);
        assertEquals(VALIDATED_TOKEN_RANDOM_HASH, token.getRandomHash());
    }

    /*
     * There can be multiple IP addresses in an X-Forwarded-For. Should use the first one.
     */
    public void testFirstXForwardedForIsUsedForSafeProxies() throws Exception
    {
        when(trustedProxy.isTrusted(REMOTE_ADDRESS_2)).thenReturn(true);
        when(trustedProxy.isTrusted(REMOTE_ADDRESS_3)).thenReturn(true);

        // run the generation
        Token token = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                              Lists.newArrayList(new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, REMOTE_ADDRESS_2), new ValidationFactor(ValidationFactor.X_FORWARDED_FOR, REMOTE_ADDRESS + "," + REMOTE_ADDRESS_3)));

        verify(trustedProxy, times(2)).isTrusted(REMOTE_ADDRESS_2);
        verify(trustedProxy, times(2)).isTrusted(REMOTE_ADDRESS_3);
        assertEquals(VALIDATED_TOKEN_RANDOM_HASH, token.getRandomHash());
    }

    public void testTokenCreationWithDifferentPrivilegeLevels() throws Exception
    {
        Token unprivilegedToken = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                                          EMPTY_FACTORS);

        ValidationFactor webSudoValidationFactor =
            new ValidationFactor(ValidationFactor.PRIVILEGE_LEVEL, WEBSUDO_PRIVILEGE_LEVEL);
        List<ValidationFactor> validationFactors = ImmutableList.of(webSudoValidationFactor);
        Token privilegedToken = tokenFactoryImpl.create(DIRECTORY_ID, USERNAME, TokenLifetime.USE_DEFAULT, validationFactors);

        assertNotSame(unprivilegedToken.getRandomHash(), privilegedToken.getRandomHash());
    }

    public void testTokenCreationIgnoresUnknownValidationFactors() throws Exception
    {
        String tokenWithNoFactors = tokenKeyGenerator.generateKey(DIRECTORY_ID, USERNAME, EMPTY_FACTORS);

        String secondTokenWithNoFactors = tokenKeyGenerator.generateKey(DIRECTORY_ID, USERNAME, EMPTY_FACTORS);

        assertEquals(tokenWithNoFactors, secondTokenWithNoFactors);

        List<ValidationFactor> validationFactors = ImmutableList.of(
            new ValidationFactor("custom-validation-factor", "custom-value"));

        String tokenWithUnknownValidationFactor = tokenKeyGenerator.generateKey(DIRECTORY_ID, USERNAME, validationFactors);

        assertEquals(tokenWithNoFactors, tokenWithUnknownValidationFactor);
    }
}
