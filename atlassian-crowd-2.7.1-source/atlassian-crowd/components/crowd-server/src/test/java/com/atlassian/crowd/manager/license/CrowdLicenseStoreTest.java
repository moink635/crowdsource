package com.atlassian.crowd.manager.license;

import com.atlassian.config.ConfigurationException;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.extras.api.AtlassianLicense;
import com.atlassian.extras.api.LicenseManager;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.crowd.CrowdLicense;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertNull;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CrowdLicenseStoreTest
{
    @Mock
    LicenseManager licenseManager;

    @Mock
    CrowdBootstrapManager bootstrapManager;

    @Test
    public void saveAndRetrieveLicenseParsesAfterSaving() throws ConfigurationException
    {
        CrowdLicense expectedLicense = mock(CrowdLicense.class);
        AtlassianLicense license = mock(AtlassianLicense.class);
        when(license.getProductLicense(Product.CROWD)).thenReturn(expectedLicense);

        when(bootstrapManager.getProperty("license")).thenReturn("returned-license");

        when(licenseManager.getLicense("returned-license")).thenReturn(license);

        CrowdLicenseStore store = new CrowdLicenseStore(licenseManager);
        store.setBootstrapManager(bootstrapManager);

        CrowdLicense crowdLicense = store.storeLicense("license-key");

        assertSame(expectedLicense, crowdLicense);

        verify(bootstrapManager).setProperty("license", "license-key");
        verify(bootstrapManager).save();

        verify(licenseManager).getLicense("returned-license");

        verify(license).getProductLicense(Product.CROWD);
    }

    @Test
    public void invalidLicenseIsStillSaved() throws ConfigurationException
    {
        when(bootstrapManager.getProperty("license")).thenReturn("returned-license");

        when(licenseManager.getLicense("returned-license")).thenThrow(new RuntimeException("Failed to parse license"));

        CrowdLicenseStore store = new CrowdLicenseStore(licenseManager);
        store.setBootstrapManager(bootstrapManager);

        try
        {
            store.storeLicense("license-key");
            fail();
        }
        catch (RuntimeException e)
        {
            assertEquals("Failed to parse license", e.getMessage());
        }

        verify(bootstrapManager).setProperty("license", "license-key");
        verify(licenseManager).getLicense("returned-license");
    }

    @Test
    public void getAtlassianLicensePropagatesNulls()
    {
        CrowdLicenseStore store = new CrowdLicenseStore(licenseManager);

        assertNull(store.getAtlassianLicense(null));
    }
}
