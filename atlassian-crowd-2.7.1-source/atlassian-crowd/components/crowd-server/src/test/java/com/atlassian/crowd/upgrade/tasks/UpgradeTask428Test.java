package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.*;
import com.atlassian.crowd.embedded.api.*;
import com.atlassian.crowd.embedded.spi.*;
import com.atlassian.crowd.model.directory.*;
import com.atlassian.crowd.search.query.entity.*;
import org.junit.*;
import org.mockito.*;

import java.util.*;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Tests that {@link UpgradeTask428} sets the caching enabled attribute in all the connector directories to false.
 *
 * @since v2.1
 */
public class UpgradeTask428Test
{
    @Mock private DirectoryDao mockDirectoryDao;
    private Directory directory1;
    private Directory directory2;

    private UpgradeTask428 upgradeTask;

    @Before
    public void setUp() throws Exception
    {
        initMocks(this);
        upgradeTask = new UpgradeTask428();
        upgradeTask.setDirectoryDao(mockDirectoryDao);

        directory1 = new DirectoryImpl("Directory 1", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());
        directory2 = new DirectoryImpl("Directory 2", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());
        when(mockDirectoryDao.search(any(EntityQuery.class))).thenReturn(Arrays.asList(directory1, directory2));
    }

    @After
    public void tearDown() throws Exception
    {
        upgradeTask = null;
        directory2 = null;
        directory1 = null;
        mockDirectoryDao = null;
    }

    @Test
    public void testDoUpgrade() throws Exception
    {
        upgradeTask.doUpgrade();

        verify(mockDirectoryDao, times(2)).update(argThat(new HasCachingEnabledSetToFalse()));
    }

    static class HasCachingEnabledSetToFalse extends ArgumentMatcher<Directory>
    {
        @Override
        public boolean matches(final Object argument)
        {
            Directory directory = (Directory) argument;
            String strCachingEnabled = directory.getAttributes().get(DirectoryProperties.CACHE_ENABLED);
            if (strCachingEnabled == null)
            {
                return false;
            }

            return !Boolean.parseBoolean(strCachingEnabled);
        }
    }
}
