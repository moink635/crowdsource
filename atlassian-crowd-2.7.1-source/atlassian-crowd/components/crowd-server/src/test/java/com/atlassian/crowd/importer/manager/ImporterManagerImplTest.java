package com.atlassian.crowd.importer.manager;

import java.util.Set;

import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.exceptions.ImporterConfigurationException;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.factory.ImporterFactory;
import com.atlassian.crowd.importer.importers.Importer;
import com.atlassian.crowd.importer.model.Result;

import com.google.common.collect.ImmutableSet;

import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

/**
 * ImporterManager Test
 */
public class ImporterManagerImplTest extends MockObjectTestCase
{
    private ImporterManagerImpl importerManager = null;
    private Mock mockImporterFactory = null;

    protected void setUp() throws Exception
    {
        super.setUp();
        importerManager = new ImporterManagerImpl();

        mockImporterFactory = new Mock(ImporterFactory.class);

        importerManager.setImporterFactory((ImporterFactory) mockImporterFactory.proxy());

    }

    public void testPerformImportWithNullConfiguation() throws ImporterException
    {
        try
        {
            importerManager.performImport(null);
            fail("An IllegalArgumentException should have been thrown");
        }
        catch(IllegalArgumentException e)
        {
            // we should be here
        }
    }

    public void testTestConfigurationIsOK()
    {
        Configuration configuration = new Configuration(new Long(1), "jira", Boolean.TRUE, Boolean.FALSE);

        try
        {
            importerManager.testConfiguration(configuration);
        }
        catch (ImporterConfigurationException e)
        {
            fail("No exception hould be thrown");
        }
    }

    public void testTestConfigurationIsBad()
    {
        Configuration configuration = new Configuration(new Long(1), null, Boolean.TRUE, Boolean.FALSE);

        try
        {
            importerManager.testConfiguration(configuration);
            fail("ImporterConfigurationException should have been thrown");
        }
        catch (ImporterConfigurationException e)
        {
            // We should be here
        }
    }

    public void testPerformImport()
    {
        Configuration configuration = new Configuration(new Long(1), "jira", Boolean.TRUE, Boolean.FALSE);

        // Setup the mock's
        Mock mockImporter = new Mock(Importer.class);
        mockImporter.expects(once()).with(eq(configuration)).will(returnValue(new Result()));

        mockImporterFactory.expects(once()).method("getImporterDAO").with(eq(configuration)).will(returnValue(mockImporter.proxy()));

        Result result = null;
        try
        {
            result = importerManager.performImport(configuration);
        }
        catch (ImporterException e)
        {
            // Using a valid configuration no exceptions should be thrown
            fail();
        }

        // We should return a result obect that contains details from the import
        assertNotNull(result);
        mockImporterFactory.verify();
    }

    public void testPerformImportWithBadConfiguration()
    {
        Configuration configuration = new Configuration(null, null, null, null);

        try
        {
            importerManager.performImport(configuration);
            fail("The configuration is meant to be invalid");
        }
        catch (ImporterException e)
        {
            // Success!
        }
    }

    protected void tearDown() throws Exception
    {
        importerManager = null;
        mockImporterFactory = null;
        super.tearDown();
    }

    public void testGetSupportedApplications() throws Exception
    {
        Set<String> applications = ImmutableSet.of("csv", "directory");

        mockImporterFactory.expects(once()).method("getSupportedImporterApplications").withNoArguments().will(returnValue(applications));

        Set<String> apps = importerManager.getSupportedApplications();
        assertNotNull(apps);
        assertFalse(apps.isEmpty());
    }

    public void testGetSupportedAtlassianApplications() throws Exception
    {
        Set<String> applications = ImmutableSet.of("jira", "Confluence");

        mockImporterFactory.expects(once()).method("getAtlassianSupportedImporterApplications").withNoArguments().will(returnValue(applications));

        Set<String> apps = importerManager.getAtlassianSupportedApplications();
        assertNotNull(apps);
        assertFalse(apps.isEmpty());
    }
}
