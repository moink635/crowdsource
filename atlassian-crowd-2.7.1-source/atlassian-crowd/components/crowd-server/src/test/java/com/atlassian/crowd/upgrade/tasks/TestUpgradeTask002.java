package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.model.property.Property;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

/**
 * UpgradeTask002 Tester.
 */
public class TestUpgradeTask002 extends MockObjectTestCase
{
    private Mock propertyManager = null;

    private UpgradeTask002 upgradeTask = null;

    public void setUp() throws Exception
    {
        super.setUp();
        propertyManager = new Mock(PropertyManager.class);

        upgradeTask = new UpgradeTask002();
        upgradeTask.setPropertyManager((PropertyManager) propertyManager.proxy());
    }

    public void testDoUpgrade() throws Exception
    {
        propertyManager.expects(once()).method("removeProperty").with(eq(Property.CACHE_TIME));

        upgradeTask.doUpgrade();

        assertTrue(upgradeTask.getErrors().isEmpty());
    }

    public void testDoUpgradeWithError() throws Exception
    {
        propertyManager.expects(once()).method("removeProperty").with(eq(Property.CACHE_TIME)).will(throwException(new RuntimeException("something bad happened!")));

        upgradeTask.doUpgrade();

        assertFalse(upgradeTask.getErrors().isEmpty());
    }

    public void tearDown() throws Exception
    {
        super.tearDown();
    }

}
