package com.atlassian.crowd.console.action;

import java.util.Collections;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.license.CrowdLicenseManager;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.atlassian.plugin.web.WebInterfaceManager;

import com.google.common.collect.ImmutableMap;
import com.opensymphony.module.sitemesh.Page;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.WebWorkStatics;
import com.opensymphony.xwork.ActionContext;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ActionHelperImplTest
{
    @InjectMocks
    private ActionHelperImpl actionHelper;

    @Mock
    private CrowdBootstrapManager bootstrapManager;

    @Mock
    private CrowdLicenseManager crowdLicenseManager;

    @Mock
    private CrowdUserDetailsService crowdUserDetailsService;

    @Mock
    private WebInterfaceManager webInterfaceManager;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private CrowdLicense license;

    @Before
    public void setUp()
    {
        when(crowdLicenseManager.getLicense()).thenReturn(license);
    }

    @Test
    public void testLicenseExpiry()
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(false);
        assertThat(actionHelper.isLicenseExpired(), equalTo(false));

        when(bootstrapManager.isSetupComplete()).thenReturn(true);
        when(license.isExpired()).thenReturn(false);
        assertThat(actionHelper.isLicenseExpired(), equalTo(false));

        when(license.isExpired()).thenReturn(true);
        assertThat(actionHelper.isLicenseExpired(), equalTo(true));
    }

    @Test
    public void testEvaluation()
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(false);
        assertThat(actionHelper.isEvaluation(), equalTo(false));

        when(bootstrapManager.isSetupComplete()).thenReturn(true);
        when(license.isEvaluation()).thenReturn(false);
        assertThat(actionHelper.isEvaluation(), equalTo(false));

        when(license.isEvaluation()).thenReturn(true);
        assertThat(actionHelper.isEvaluation(), equalTo(true));
    }

    @Test
    public void testSubscription()
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(false);
        assertThat(actionHelper.isSubscription(), equalTo(false));

        when(bootstrapManager.isSetupComplete()).thenReturn(true);
        when(license.isSubscription()).thenReturn(false);
        assertThat(actionHelper.isSubscription(), equalTo(false));

        when(license.isSubscription()).thenReturn(true);
        assertThat(actionHelper.isSubscription(), equalTo(true));
    }

    @Test
    public void testGracePeriod()
    {
        when(bootstrapManager.isSetupComplete()).thenReturn(false);
        assertThat(actionHelper.isWithinGracePeriod(), equalTo(false));

        when(bootstrapManager.isSetupComplete()).thenReturn(true);
        when(license.isWithinGracePeriod()).thenReturn(false);
        assertThat(actionHelper.isWithinGracePeriod(), equalTo(false));

        when(license.isWithinGracePeriod()).thenReturn(true);
        assertThat(actionHelper.isWithinGracePeriod(), equalTo(true));
    }

    @Test
    public void testHasAdminRole()
    {
        String admin = "admin";
        String user = "user";

        when(crowdUserDetailsService.getAdminAuthority()).thenReturn(admin);
        GrantedAuthority grantedAuthority = mock(GrantedAuthority.class);
        CrowdUserDetails crowdUserDetails = mock(CrowdUserDetails.class);
        when(crowdUserDetails.getAuthorities()).thenReturn(Collections.singleton(grantedAuthority));

        when(grantedAuthority.getAuthority()).thenReturn(user);
        assertThat(actionHelper.hasAdminRole(crowdUserDetails), equalTo(false));

        when(grantedAuthority.getAuthority()).thenReturn(admin);
        assertThat(actionHelper.hasAdminRole(crowdUserDetails), equalTo(true));
    }

    @Test
    public void testIsAdmin()
    {
        String admin = "admin";

        when(crowdUserDetailsService.getAdminAuthority()).thenReturn(admin);

        Authentication authentication = mock(Authentication.class);
        CrowdUserDetails crowdUserDetails = mock(CrowdUserDetails.class);
        GrantedAuthority grantedAuthority = mock(GrantedAuthority.class);

        when(grantedAuthority.getAuthority()).thenReturn(admin);
        when(crowdUserDetails.getAuthorities()).thenReturn(Collections.singleton(grantedAuthority));
        when(authentication.getPrincipal()).thenReturn(crowdUserDetails);

        try
        {
            SecurityContextHolder.getContext().setAuthentication(authentication);
            assertThat(actionHelper.isAdmin(), equalTo(true));
        }
        finally
        {
            SecurityContextHolder.getContext().setAuthentication(null);
        }

    }

    @Test
    public void testGetRemoteUser()
    {
        Authentication authentication = mock(Authentication.class);
        CrowdUserDetails crowdUserDetails = mock(CrowdUserDetails.class);
        UserWithAttributes userWithAttributes = mock(UserWithAttributes.class);

        when(crowdUserDetails.getRemotePrincipal()).thenReturn(userWithAttributes);
        when(authentication.getPrincipal()).thenReturn(crowdUserDetails);

        try
        {
            SecurityContextHolder.getContext().setAuthentication(authentication);
            assertThat(actionHelper.getRemoteUser(), equalTo(userWithAttributes));
        }
        finally
        {
            SecurityContextHolder.getContext().setAuthentication(null);
        }
    }

    @Test
    public void testGetSitemeshPageProperties()
    {
        Map<String, String> emptySitemeshPageProperties = Collections.emptyMap();
        assertThat(actionHelper.getSitemeshPageProperties(), equalTo(emptySitemeshPageProperties));

        try
        {
            PageContext pageContext = mock(PageContext.class);
            ActionContext.getContext().put(WebWorkStatics.PAGE_CONTEXT, pageContext);
            assertThat(actionHelper.getSitemeshPageProperties(), equalTo(emptySitemeshPageProperties));

            Page page = mock(Page.class);
            Map sitemeshPageProperties = mock(Map.class);
            when(page.getProperties()).thenReturn(sitemeshPageProperties);
            when(pageContext.getAttribute("sitemeshPage")).thenReturn(page);
            assertThat(actionHelper.getSitemeshPageProperties(), equalTo(sitemeshPageProperties));
        }
        finally
        {
            ActionContext.getContext().put(WebWorkStatics.PAGE_CONTEXT, null);
        }
    }

    @Test
    public void testGetWebFragmentsContextMapContainsRequestParameters()
    {
        try
        {
            ActionContext.getContext().setParameters(ImmutableMap.of("key", new String[] { "value1", "value2" }));
            assertThat(actionHelper.getWebFragmentsContextMap(),
                    Matchers.<Map<String, Object>>equalTo(ImmutableMap.<String, Object>of("key", "value1,value2")));
        }
        finally
        {
            ActionContext.getContext().setParameters(null);
        }
    }

    @Test
    public void testGetWebFragmentsContextMapContainsSitemeshPageProperties()
    {
        Page page = mock(Page.class);
        PageContext pageContext = mock(PageContext.class);

        when(page.getProperties()).thenReturn(ImmutableMap.of("key", "valueFromPP"));
        when(pageContext.getAttribute("sitemeshPage")).thenReturn(page);

        try
        {
            ActionContext.getContext().setParameters(ImmutableMap.of());
            ActionContext.getContext().put(ServletActionContext.PAGE_CONTEXT, pageContext);
            assertThat(actionHelper.getWebFragmentsContextMap(),
                    Matchers.<Map<String, Object>>equalTo(ImmutableMap.<String, Object>of("key", "valueFromPP")));
        }
        finally
        {
            ActionContext.getContext().setParameters(null);
            ActionContext.getContext().put(ServletActionContext.PAGE_CONTEXT, null);
        }
    }

    @Test
    public void testGetWebFragmentsContextMapShouldOverrideRequestParametersWithSitemeshPageProperties()
    {
        Page page = mock(Page.class);
        PageContext pageContext = mock(PageContext.class);

        when(page.getProperties()).thenReturn(ImmutableMap.of("key", "valueFromPP"));
        when(pageContext.getAttribute("sitemeshPage")).thenReturn(page);

        try
        {
            ActionContext.getContext().setParameters(ImmutableMap.of("key", new String[] { "value1", "value2" }));
            ActionContext.getContext().put(ServletActionContext.PAGE_CONTEXT, pageContext);
            assertThat(actionHelper.getWebFragmentsContextMap(),
                    Matchers.<Map<String, Object>>equalTo(ImmutableMap.<String, Object>of("key", "valueFromPP")));
        }
        finally
        {
            ActionContext.getContext().setParameters(null);
            ActionContext.getContext().put(ServletActionContext.PAGE_CONTEXT, null);
        }
    }
}
