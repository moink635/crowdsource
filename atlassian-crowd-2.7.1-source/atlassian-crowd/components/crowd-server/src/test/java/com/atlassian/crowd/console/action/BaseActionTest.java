package com.atlassian.crowd.console.action;

import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.service.client.ClientProperties;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class BaseActionTest
{
    @Test
    public void normalExceptionsAreNotMarkedAsBeingDueToReferralErrors()
    {
        Exception e = new Exception();
        e.fillInStackTrace();

        assertFalse(BaseAction.isBadReferralException(e));
    }

    @Test
    public void usesCrowdApplicationIfInjected() throws Exception
    {
        Application application = mock(Application.class);
        ApplicationManager applicationManager = mock(ApplicationManager.class);
        ClientProperties clientProperties = mock(ClientProperties.class);

        BaseAction baseAction = new BaseAction();
        baseAction.setCrowdApplication(application);
        baseAction.setApplicationManager(applicationManager);
        baseAction.setClientProperties(clientProperties);

        assertEquals(application, baseAction.getCrowdApplication());

        verifyZeroInteractions(clientProperties);
        verifyZeroInteractions(applicationManager);
    }

    @Test
    public void findsCrowdApplicationIfNotInjected() throws Exception
    {
        Application application = mock(Application.class);
        ApplicationManager applicationManager = mock(ApplicationManager.class);
        ClientProperties clientProperties = mock(ClientProperties.class);

        when(clientProperties.getApplicationName()).thenReturn("crowd");
        when(applicationManager.findByName("crowd")).thenReturn(application);

        BaseAction baseAction = new BaseAction();
        baseAction.setApplicationManager(applicationManager);
        baseAction.setClientProperties(clientProperties);

        assertEquals(application, baseAction.getCrowdApplication());
    }
}
