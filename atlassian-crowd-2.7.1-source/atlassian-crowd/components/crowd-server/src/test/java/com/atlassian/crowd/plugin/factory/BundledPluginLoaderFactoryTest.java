package com.atlassian.crowd.plugin.factory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import javax.servlet.ServletContext;

import com.atlassian.crowd.plugin.PluginDirectoryLocator;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.PluginFactory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BundledPluginLoaderFactoryTest
{
    @Mock
    PluginDirectoryLocator pluginDirectoryLocator;

    @Mock
    List<PluginFactory> pluginFactories;

    @Mock
    PluginEventManager pluginEventManager;

    @Test
    public void getBundledPluginsUrlAcceptsAndUsesAnAbsoluteUrl() throws MalformedURLException
    {
        String url = "file:/plugins.zip";
        BundledPluginLoaderFactory factory = new BundledPluginLoaderFactory(pluginDirectoryLocator, pluginFactories, pluginEventManager, url);

        assertEquals("file:/plugins.zip", factory.getBundledPluginsUrl().toExternalForm());
    }

    @Test(expected = IllegalStateException.class)
    public void nonexistentResourceThrowsAnException() throws MalformedURLException
    {
        String path = "/no-resource-with-this-name";
        BundledPluginLoaderFactory factory = new BundledPluginLoaderFactory(pluginDirectoryLocator, pluginFactories, pluginEventManager, path);

        ServletContext context = mock(ServletContext.class);
        factory.setServletContext(context);

        factory.getBundledPluginsUrl();
    }

    @Test
    public void nonexistentFileIsNotLoaded() throws Exception
    {
        String path = "/filename";
        BundledPluginLoaderFactory factory = new BundledPluginLoaderFactory(pluginDirectoryLocator, pluginFactories, pluginEventManager, path);

        ServletContext context = mock(ServletContext.class);
        when(context.getRealPath("/WEB-INF/classes/filename")).thenReturn("file-that-does-not-exist");
        when(context.getResource("/filename")).thenReturn(new URL("file:/resource"));
        factory.setServletContext(context);

        factory.getBundledPluginsUrl();

        assertEquals("file:/resource", factory.getBundledPluginsUrl().toExternalForm());
    }

    @Test
    public void aResourceUrlIsUsed() throws Exception
    {
        String path = "/a-resource-that-exists"; // Any resource that exists
        BundledPluginLoaderFactory factory = new BundledPluginLoaderFactory(pluginDirectoryLocator, pluginFactories, pluginEventManager, path);

        ServletContext context = mock(ServletContext.class);
        when(context.getResource("/a-resource-that-exists")).thenReturn(new URL("file:/resource"));
        factory.setServletContext(context);

        assertEquals("file:/resource", factory.getBundledPluginsUrl().toExternalForm());
    }
}
