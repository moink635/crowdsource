package com.atlassian.crowd.manager.backup;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.event.api.EventPublisher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.quartz.Scheduler;
import org.quartz.Trigger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v2.7
 */
@RunWith(MockitoJUnitRunner.class)
public class QuartzBackupSchedulerTest
{
    private QuartzBackupScheduler service;
    @Mock
    private Scheduler scheduler;
    @Mock
    private BackupManager backupManager;
    @Mock
    private PropertyManager propertyManager;
    @Mock
    private EventPublisher eventPublisher;
    @Mock
    private BackupFileStore backupFileStore;

    @Before
    public void setUp() throws Exception
    {
        service = new QuartzBackupScheduler(scheduler, propertyManager, backupManager, eventPublisher, backupFileStore);
    }

    @Test
    public void testIsEnabledTrue() throws Exception
    {
        withEnabled(true);

        assertThat(service.isEnabled(), is(true));
    }

    @Test
    public void testIsEnabledFalse() throws Exception
    {
        withEnabled(false);

        assertThat(service.isEnabled(), is(false));
    }

    @Test
    public void testIsEnabledUndefined() throws Exception
    {
        assertThat(service.isEnabled(), is(false));
    }

    @Test
    public void testGetBackupTime() throws Exception
    {
        withTime(12);

        assertThat(service.getHourToRun(), is(12));
    }

    @Test
    public void testEnable() throws Exception
    {
        withEnabled(false);
        withTime(5);

        service.enable();

        verify(scheduler).unscheduleJob("AutomatedBackup", "AutomatedBackup");
        verify(scheduler).scheduleJob(any(Trigger.class));
        verify(propertyManager).setProperty(QuartzBackupScheduler.ENABLED_PROPERTY, "true");
        verifyNoMoreInteractions(scheduler);
    }

    @Test
    public void testEnableAlreadyEnabled() throws Exception
    {
        withEnabled(true);
        service.enable();

        verifyNoMoreInteractions(scheduler);
    }

    @Test
    public void testDisable() throws Exception
    {
        withEnabled(true);
        service.disable();

        verify(scheduler).unscheduleJob("AutomatedBackup", "AutomatedBackup");
        verify(propertyManager).setProperty(QuartzBackupScheduler.ENABLED_PROPERTY, "false");
        verifyNoMoreInteractions(scheduler);
    }

    @Test
    public void testDisableAlreadyDisabled() throws Exception
    {
        withEnabled(false);
        service.disable();

        verifyNoMoreInteractions(scheduler);
    }

    @Test
    public void testSetBackupTimeDisabled() throws Exception
    {
        withEnabled(false);
        service.setHourToRun(5);

        verify(propertyManager).setProperty(QuartzBackupScheduler.HOUR_PROPERTY, "5");
        verifyNoMoreInteractions(scheduler);
    }

    @Test
    public void testSetBackupTimeEnabled() throws Exception
    {
        withEnabled(true);
        service.setHourToRun(5);

        verify(propertyManager).setProperty(QuartzBackupScheduler.HOUR_PROPERTY, "5");
        verify(scheduler).unscheduleJob("AutomatedBackup", "AutomatedBackup");
        verify(scheduler).scheduleJob(any(Trigger.class));
        verifyNoMoreInteractions(scheduler);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetBackupTimeInvalid() throws Exception
    {
        service.setHourToRun(-3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetBackupTimeInvalid2() throws Exception
    {
        service.setHourToRun(24);
    }

    private void withEnabled(boolean enabled) throws Exception
    {
        when(propertyManager.getBoolean(QuartzBackupScheduler.ENABLED_PROPERTY, true)).thenReturn(enabled);
    }

    private void withTime(int time) throws Exception
    {
        when(propertyManager.getInt(QuartzBackupScheduler.HOUR_PROPERTY, 2)).thenReturn(time);
    }
}
