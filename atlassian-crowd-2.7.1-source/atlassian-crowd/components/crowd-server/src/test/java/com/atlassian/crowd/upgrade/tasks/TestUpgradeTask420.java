package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.DataAccessException;

import static org.junit.Assert.assertTrue;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Testing {@link UpgradeTask420}
 */
public class TestUpgradeTask420
{
    private static final String OLD_APACHEDS_IMPLEMENTATION_CLASS = "com.atlassian.crowd.integration.directory.connector.ApacheDS";
    private static final String NEW_APACHEDS_IMPLEMENTATION_CLASS = "com.atlassian.crowd.directory.ApacheDS";

    private static final String OLD_DELEGATED_AUTH_IMPL_CLASS = "com.atlassian.crowd.integration.directory.delegated.DelegatedAuthenticationDirectory";
    private static final String NEW_DELEGATED_AUTH_IMPL_CLASS = "com.atlassian.crowd.directory.DelegatedAuthenticationDirectory";

    private static final String OLD_MICROSOFTAD_IMPLEMENTATION_CLASS = "com.atlassian.crowd.integration.directory.connector.MicrosoftActiveDirectory";
    private static final String NEW_MICROSOFTAD_IMPLEMENTATION_CLASS = "com.atlassian.crowd.directory.MicrosoftActiveDirectory";

    private DirectoryDao mockDirectoryDAO;

    private DirectoryImpl directory;
    private UpgradeTask420 task;

    @Before
    public void setUp()
    {
        directory = new DirectoryImpl("Directory 1", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());

        mockDirectoryDAO = mock(DirectoryDao.class);
        when(mockDirectoryDAO.search(any(EntityQuery.class))).thenReturn(Collections.<Directory>singletonList(directory));

        task = new UpgradeTask420();
        task.setDirectoryDao(mockDirectoryDAO);
    }

    @Test
    public void testDoUpgradeWithUpgradableDirectory() throws Exception
    {
        directory.setImplementationClass(OLD_APACHEDS_IMPLEMENTATION_CLASS);

        DirectoryImpl updatedDirectory = new DirectoryImpl(directory);
        updatedDirectory.setImplementationClass(NEW_APACHEDS_IMPLEMENTATION_CLASS);

        task.doUpgrade();

        verify(mockDirectoryDAO, times(1)).update(updatedDirectory);
        assertTrue(task.getErrors().isEmpty());
    }

    @Test
    public void testDoUpgradeWithNoUpgradableDirectory() throws Exception
    {
        final String someRandomClass = "com.atlassian.crowd.SomeRandomClass";
        directory.setImplementationClass(someRandomClass);

        task.doUpgrade();

        verify(mockDirectoryDAO, never()).update(any(DirectoryImpl.class));
        assertTrue(task.getErrors().isEmpty());
        assertEquals(someRandomClass, directory.getImplementationClass());
    }

    @Test
    public void testDoUpgradeWithIssueUpgradingDirectory() throws Exception
    {
        // Want to test if a DataAccessException is thrown
        when(mockDirectoryDAO.update(any(DirectoryImpl.class))).thenThrow(new DataAccessException(""){});

        directory.setImplementationClass(OLD_APACHEDS_IMPLEMENTATION_CLASS);

        DirectoryImpl updatedDirectory = new DirectoryImpl(directory);
        updatedDirectory.setImplementationClass(NEW_APACHEDS_IMPLEMENTATION_CLASS);

        task.doUpgrade();

        // Won't be successful, but still called
        verify(mockDirectoryDAO, times(1)).update(updatedDirectory);
        assertEquals(1, task.getErrors().size());
    }

    @Test
    public void testDoUpgradeWithDelegatedDirectory() throws Exception
    {
        // Change the directory to a delegated directory
        directory = new DirectoryImpl("Directory 2", DirectoryType.DELEGATING, DelegatedAuthenticationDirectory.class.getCanonicalName());
        directory.setImplementationClass(OLD_DELEGATED_AUTH_IMPL_CLASS);
        directory.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS, OLD_MICROSOFTAD_IMPLEMENTATION_CLASS);
        when(mockDirectoryDAO.search(any(EntityQuery.class))).thenReturn(Collections.<Directory>singletonList(directory));

        DirectoryImpl updatedDirectory = new DirectoryImpl(directory);
        updatedDirectory.setImplementationClass(NEW_DELEGATED_AUTH_IMPL_CLASS);

        task.doUpgrade();

        verify(mockDirectoryDAO, times(1)).update(updatedDirectory);

        assertTrue(task.getErrors().isEmpty());
        verify(mockDirectoryDAO).update(argThat(DirectoryAttributesMatcher.contains(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS, NEW_MICROSOFTAD_IMPLEMENTATION_CLASS)));
    }
}
