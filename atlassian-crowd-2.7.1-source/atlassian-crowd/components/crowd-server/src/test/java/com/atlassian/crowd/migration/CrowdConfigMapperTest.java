package com.atlassian.crowd.migration;

import java.util.HashMap;

import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CrowdConfigMapperTest
{
    private CrowdConfigMapper configMapper = null;
    private Element root;
    private static final String TEST_KEY = "test.key";
    private static final String TEST_VALUE = "test.value";

    protected CrowdBootstrapManager bootstrapManager;

    @Before
    public void setUp() throws Exception
    {
        bootstrapManager = mock(CrowdBootstrapManager.class);
        configMapper = new CrowdConfigMapper(bootstrapManager);

        // create a dummy XML document to import
        Document document = DocumentHelper.createDocument();
        root = document.addElement(XmlMigrationManagerImpl.XML_ROOT);
        Element crowdConfigRoot = DocumentHelper.createElement(CrowdConfigMapper.CONFIG_XML_ROOT);
        Element propertyElement = crowdConfigRoot.addElement(CrowdConfigMapper.CONFIG_XML_ROOT);
        propertyElement.addElement(CrowdConfigMapper.CONFIG_XML_NAME).addText(TEST_KEY);
        propertyElement.addElement(CrowdConfigMapper.CONFIG_XML_VALUE).addText(TEST_VALUE);
        root.add(crowdConfigRoot);
    }

    @Test
    public void testExportCrowdProperties() throws ExportException
    {
        String sid = "SID12345";
        when(bootstrapManager.getString(CrowdBootstrapManager.CROWD_SID)).thenReturn(sid);

        Document document = DocumentHelper.createDocument();
        Element root = document.addElement(XmlMigrationManagerImpl.XML_ROOT);

        Element propertiesElement = configMapper.exportXml(new HashMap());

        // Add the result to the root so we have the right Document structure
        root.add(propertiesElement);

        assertNotNull(propertiesElement);
        assertTrue("Contains content", propertiesElement.hasContent());

        // assert that the properties Element has all known crowd.properties
        assertEquals(((Element) propertiesElement.selectNodes(CrowdConfigMapper.CONFIG_XML_NODE + "/" + CrowdConfigMapper.CONFIG_XML_NAME).get(0)).getText(), CrowdBootstrapManager.CROWD_SID);
        assertEquals(((Element) propertiesElement.selectNodes(CrowdConfigMapper.CONFIG_XML_NODE + "/" + CrowdConfigMapper.CONFIG_XML_VALUE).get(0)).getText(), sid);
    }

    @Test
    public void testImportCrowdProperties() throws Exception
    {
        configMapper.importXml(root);

        verify(bootstrapManager).setProperty(TEST_KEY, TEST_VALUE);
        verify(bootstrapManager).save();
        Mockito.verifyNoMoreInteractions(bootstrapManager);
    }
}
