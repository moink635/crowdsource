package com.atlassian.crowd.migration.legacy;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import junit.framework.TestCase;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.net.URL;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Set;

import static com.atlassian.crowd.embedded.api.OperationType.*;

public abstract class BaseLegacyImporterTest extends TestCase
{
    protected static final Set<OperationType> ALL_PRE_20_OPERATION_TYPES = Collections.unmodifiableSet(EnumSet.of(
            CREATE_GROUP, UPDATE_GROUP, DELETE_GROUP,
            CREATE_USER, UPDATE_USER, DELETE_USER,
            CREATE_ROLE, UPDATE_ROLE, DELETE_ROLE
    ));

    protected Document document;
    protected HashMap<Long, Long> oldToNewDirectoryIds;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        // load xml file
        SAXReader reader = new SAXReader();
        final URL resource = ClassLoaderUtils.getResource(StringUtils.replace(getClass().getCanonicalName(), ".", File.separator) + ".xml", getClass());
        document = reader.read(resource);

        oldToNewDirectoryIds = new HashMap<Long, Long>();
    }

    @Override
    protected void tearDown() throws Exception
    {
        document = null;
        super.tearDown();
    }

    protected DirectoryImpl directoryWithIdAndName(long id, String name)
    {
        final InternalEntityTemplate template = new InternalEntityTemplate();
        template.setId(id);
        template.setName(name);
        return new DirectoryImpl(template);
    }

}
