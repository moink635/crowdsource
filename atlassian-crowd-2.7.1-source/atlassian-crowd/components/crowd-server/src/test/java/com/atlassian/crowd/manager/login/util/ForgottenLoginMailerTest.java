package com.atlassian.crowd.manager.login.util;

import com.atlassian.crowd.manager.mail.MailManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.util.I18nHelper;
import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;

import javax.mail.internet.InternetAddress;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class ForgottenLoginMailerTest
{
    private static final String RESET_TOKEN = "abc123";
    private static final String TEST_USERNAME = "user";
    private static final String TEST_USERNAME2 = "user2";
    private static final String TEST_FIRSTNAME = "FirstName";
    private static final String TEST_LASTNAME = "LastName";
    private static final String TEST_EMAIL = "test@example.com";
    private static final String TEST_INVALID_EMAIL = "invalid-email";
    private static final String TEST_NOTIFICATION_EMAIL = "admin@example.com";
    private static final String RESET_LINK = "http://localhost:8095/resetpassword.action?username=" + TEST_USERNAME + "&token=" + RESET_TOKEN;
    private static final String FORGOTTEN_PASSWORD_SUBJECT = "Forgotten Password";
    private static final String FORGOTTEN_USERNAME_SUBJECT = "Forgotten Username";
    private static final String DEPLOYMENT_TITLE = "Crowd";

    private ForgottenLoginMailer forgottenLoginMailer;
    private PropertyManager propertyManager;
    private MailManager mailManager;
    private I18nHelper i18nHelper;

    @Before
    public void setUp()
    {
        propertyManager = mock(PropertyManager.class);
        mailManager = mock(MailManager.class);
        i18nHelper = mock(I18nHelper.class);
        forgottenLoginMailer = new ForgottenLoginMailer(propertyManager, mailManager, i18nHelper);
    }

    /**
     * Tests that {@link ForgottenLoginMailer#mailResetPasswordLink(com.atlassian.crowd.model.user.User, String)} sends
     * an email with the correct title.
     */
    @Test
    public void testMailResetPasswordLink() throws Exception
    {
        Joiner joiner = Joiner.on(" ");
        List<String> paramList = Lists.newArrayList(
                                    ForgottenLoginMailer.FIRSTNAME_MACRO,
                                    ForgottenLoginMailer.LASTNAME_MACRO,
                                    ForgottenLoginMailer.USERNAME_MACRO,
                                    ForgottenLoginMailer.RESET_LINK_MACRO,
                                    ForgottenLoginMailer.DEPLOYMENTTITLE_MACRO);
        String emailTemplate = joiner.join(paramList);
        List<String> expectedList = Lists.newArrayList(
                TEST_FIRSTNAME,
                TEST_LASTNAME,
                TEST_USERNAME,
                RESET_LINK,
                DEPLOYMENT_TITLE
        );
        String expectedEmailBody = joiner.join(expectedList);

        User user = mock(User.class);
        when(user.getFirstName()).thenReturn(TEST_FIRSTNAME);
        when(user.getLastName()).thenReturn(TEST_LASTNAME);
        when(user.getName()).thenReturn(TEST_USERNAME);
        when(user.getEmailAddress()).thenReturn(TEST_EMAIL);

        when(propertyManager.getProperty(Property.FORGOTTEN_PASSWORD_EMAIL_TEMPLATE)).thenReturn(emailTemplate);
        when(propertyManager.getDeploymentTitle()).thenReturn(DEPLOYMENT_TITLE);

        when(i18nHelper.getText("forgottenpassword.subject")).thenReturn(FORGOTTEN_PASSWORD_SUBJECT);

        forgottenLoginMailer.mailResetPasswordLink(user, RESET_LINK);

        verify(mailManager).sendEmail(new InternetAddress(TEST_EMAIL), FORGOTTEN_PASSWORD_SUBJECT, expectedEmailBody);
    }

    /**
     * Tests that {@link ForgottenLoginMailer#mailResetPasswordLink(com.atlassian.crowd.model.user.User, String)}
     * still sends the e-mail when the e-mail address is invalid.
     * <p>It delegates to the SMTP server the choice of failing the delivery of such emails</p>
     */
    @Test
    public void testMailResetPasswordLink_InvalidEmail() throws Exception
    {
        Joiner joiner = Joiner.on(" ");
        List<String> paramList = Lists.newArrayList(
                                    ForgottenLoginMailer.FIRSTNAME_MACRO,
                                    ForgottenLoginMailer.LASTNAME_MACRO,
                                    ForgottenLoginMailer.USERNAME_MACRO,
                                    ForgottenLoginMailer.RESET_LINK_MACRO,
                                    ForgottenLoginMailer.DEPLOYMENTTITLE_MACRO);
        String emailTemplate = joiner.join(paramList);
        List<String> expectedList = Lists.newArrayList(
                TEST_FIRSTNAME,
                TEST_LASTNAME,
                TEST_USERNAME,
                RESET_LINK,
                DEPLOYMENT_TITLE
        );
        String expectedEmailBody = joiner.join(expectedList);

        User user = mock(User.class);
        when(user.getFirstName()).thenReturn(TEST_FIRSTNAME);
        when(user.getLastName()).thenReturn(TEST_LASTNAME);
        when(user.getName()).thenReturn(TEST_USERNAME);
        when(user.getEmailAddress()).thenReturn(TEST_INVALID_EMAIL);

        when(propertyManager.getProperty(Property.FORGOTTEN_PASSWORD_EMAIL_TEMPLATE)).thenReturn(emailTemplate);
        when(propertyManager.getDeploymentTitle()).thenReturn(DEPLOYMENT_TITLE);

        when(i18nHelper.getText("forgottenpassword.subject")).thenReturn(FORGOTTEN_PASSWORD_SUBJECT);

        forgottenLoginMailer.mailResetPasswordLink(user, RESET_LINK);

        verify(mailManager).sendEmail(new InternetAddress(TEST_INVALID_EMAIL), FORGOTTEN_PASSWORD_SUBJECT, expectedEmailBody);
    }

    @Test
    public void testMailUsernames() throws Exception
    {
        Joiner joiner = Joiner.on(" ");
        List<String> paramList = Lists.newArrayList(
                                    ForgottenLoginMailer.FIRSTNAME_MACRO,
                                    ForgottenLoginMailer.LASTNAME_MACRO,
                                    ForgottenLoginMailer.USERNAME_MACRO,
                                    ForgottenLoginMailer.DEPLOYMENTTITLE_MACRO,
                                    ForgottenLoginMailer.EMAIL_MACRO,
                                    ForgottenLoginMailer.ADMIN_CONTACT_MACRO);
        String emailTemplate = joiner.join(paramList);
        List<String> expectedList = Lists.newArrayList(
                TEST_FIRSTNAME,
                TEST_LASTNAME,
                TEST_USERNAME + ", " + TEST_USERNAME2,
                DEPLOYMENT_TITLE,
                TEST_EMAIL,
                TEST_NOTIFICATION_EMAIL
        );
        String expectedEmailBody = joiner.join(expectedList);

        User user1 = mock(User.class);
        when(user1.getFirstName()).thenReturn(TEST_FIRSTNAME);
        when(user1.getLastName()).thenReturn(TEST_LASTNAME);
        when(user1.getName()).thenReturn(TEST_USERNAME);
        when(user1.getEmailAddress()).thenReturn(TEST_EMAIL);

        User user2 = mock(User.class);
        when(user2.getFirstName()).thenReturn(TEST_FIRSTNAME);
        when(user2.getLastName()).thenReturn(TEST_LASTNAME);
        when(user2.getName()).thenReturn(TEST_USERNAME2);
        when(user2.getEmailAddress()).thenReturn(TEST_EMAIL);

        List<String> usernames = new ArrayList<String>();
        usernames.add(user1.getName());
        usernames.add(user2.getName());

        when(propertyManager.getProperty(Property.FORGOTTEN_USERNAME_EMAIL_TEMPLATE)).thenReturn(emailTemplate);
        when(propertyManager.getDeploymentTitle()).thenReturn(DEPLOYMENT_TITLE);
        when(propertyManager.getProperty(Property.NOTIFICATION_EMAIL)).thenReturn(TEST_NOTIFICATION_EMAIL);

        when(i18nHelper.getText("forgottenusername.subject")).thenReturn(FORGOTTEN_USERNAME_SUBJECT);

        forgottenLoginMailer.mailUsernames(user1, usernames);

        verify(mailManager).sendEmail(new InternetAddress(TEST_EMAIL), FORGOTTEN_USERNAME_SUBJECT, expectedEmailBody);
    }

    @Test
    public void testMailUsernames_NoUsers() throws Exception
    {
        Joiner joiner = Joiner.on(" ");
        List<String> paramList = Lists.newArrayList(
                                    ForgottenLoginMailer.FIRSTNAME_MACRO,
                                    ForgottenLoginMailer.LASTNAME_MACRO,
                                    ForgottenLoginMailer.USERNAME_MACRO,
                                    ForgottenLoginMailer.DEPLOYMENTTITLE_MACRO,
                                    ForgottenLoginMailer.EMAIL_MACRO,
                                    ForgottenLoginMailer.ADMIN_CONTACT_MACRO);
        String emailTemplate = joiner.join(paramList);
        List<String> expectedList = Lists.newArrayList(
                TEST_FIRSTNAME,
                TEST_LASTNAME,
                TEST_USERNAME,
                DEPLOYMENT_TITLE,
                TEST_EMAIL,
                TEST_NOTIFICATION_EMAIL
        );
        String expectedEmailBody = joiner.join(expectedList);

        List<String> usernames = new ArrayList<String>();

        when(propertyManager.getProperty(Property.FORGOTTEN_USERNAME_EMAIL_TEMPLATE)).thenReturn(emailTemplate);
        when(propertyManager.getDeploymentTitle()).thenReturn(DEPLOYMENT_TITLE);
        when(propertyManager.getProperty(Property.NOTIFICATION_EMAIL)).thenReturn(TEST_NOTIFICATION_EMAIL);

        when(i18nHelper.getText("forgottenusername.subject")).thenReturn(FORGOTTEN_USERNAME_SUBJECT);

        try
        {
            forgottenLoginMailer.mailUsernames(null, usernames);
            fail("Should have thrown a NullPointerException");
        }
        catch (NullPointerException e)
        {
            // correct result
        }
    }

    @Test
    public void testMailUsernames_InvalidEmail() throws Exception
    {
        Joiner joiner = Joiner.on(" ");
        List<String> paramList = Lists.newArrayList(
                                    ForgottenLoginMailer.FIRSTNAME_MACRO,
                                    ForgottenLoginMailer.LASTNAME_MACRO,
                                    ForgottenLoginMailer.USERNAME_MACRO,
                                    ForgottenLoginMailer.DEPLOYMENTTITLE_MACRO,
                                    ForgottenLoginMailer.EMAIL_MACRO,
                                    ForgottenLoginMailer.ADMIN_CONTACT_MACRO);
        String emailTemplate = joiner.join(paramList);
        List<String> expectedList = Lists.newArrayList(
                TEST_FIRSTNAME,
                TEST_LASTNAME,
                TEST_USERNAME + ", " + TEST_USERNAME2,
                DEPLOYMENT_TITLE,
                TEST_INVALID_EMAIL,
                TEST_NOTIFICATION_EMAIL
        );
        String expectedEmailBody = joiner.join(expectedList);

        User user1 = mock(User.class);
        when(user1.getFirstName()).thenReturn(TEST_FIRSTNAME);
        when(user1.getLastName()).thenReturn(TEST_LASTNAME);
        when(user1.getName()).thenReturn(TEST_USERNAME);
        when(user1.getEmailAddress()).thenReturn(TEST_INVALID_EMAIL);

        User user2 = mock(User.class);
        when(user2.getFirstName()).thenReturn(TEST_FIRSTNAME);
        when(user2.getLastName()).thenReturn(TEST_LASTNAME);
        when(user2.getName()).thenReturn(TEST_USERNAME2);
        when(user2.getEmailAddress()).thenReturn(TEST_INVALID_EMAIL);

        List<String> usernames = new ArrayList<String>();
        usernames.add(user1.getName());
        usernames.add(user2.getName());

        when(propertyManager.getProperty(Property.FORGOTTEN_USERNAME_EMAIL_TEMPLATE)).thenReturn(emailTemplate);
        when(propertyManager.getDeploymentTitle()).thenReturn(DEPLOYMENT_TITLE);
        when(propertyManager.getProperty(Property.NOTIFICATION_EMAIL)).thenReturn(TEST_NOTIFICATION_EMAIL);

        when(i18nHelper.getText("forgottenusername.subject")).thenReturn(FORGOTTEN_USERNAME_SUBJECT);

        forgottenLoginMailer.mailUsernames(user1, usernames);

        verify(mailManager).sendEmail(new InternetAddress(TEST_INVALID_EMAIL), FORGOTTEN_USERNAME_SUBJECT, expectedEmailBody);
    }
}
