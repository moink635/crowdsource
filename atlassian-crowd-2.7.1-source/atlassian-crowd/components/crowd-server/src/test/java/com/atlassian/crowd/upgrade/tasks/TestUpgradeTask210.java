package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

public class TestUpgradeTask210 extends MockObjectTestCase
{
    private UpgradeTask210 upgradeTask;
    private Mock mockPropertyManager;

    protected void setUp() throws Exception
    {
        super.setUp();

        mockPropertyManager = new Mock(PropertyManager.class);

        upgradeTask = new UpgradeTask210();
        upgradeTask.setPropertyManager((PropertyManager) mockPropertyManager.proxy());
    }

    public void testUpgrade_notRequired() throws Exception
    {
        mockPropertyManager.expects(once()).method("isGzipEnabled").will(returnValue(false));

        upgradeTask.doUpgrade();

        assertEquals(upgradeTask.getErrors().size(), 0);
    }

    public void testUpgrade_successful() throws Exception
    {
        mockPropertyManager.expects(once()).method("isGzipEnabled").will(throwException(new PropertyManagerException()));
        mockPropertyManager.expects(once()).method("setGzipEnabled");

        upgradeTask.doUpgrade();

        assertEquals(upgradeTask.getErrors().size(), 0);
    }
}
