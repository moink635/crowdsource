package com.atlassian.crowd.manager.authentication;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.event.application.ApplicationAuthenticatedEvent;
import com.atlassian.crowd.event.user.UserAuthenticationFailedAccessDeniedEvent;
import com.atlassian.crowd.event.user.UserAuthenticationSucceededEvent;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.ObjectAlreadyExistsException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationAccessDeniedException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.cache.CacheManager;
import com.atlassian.crowd.manager.cache.NotInCacheException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.token.TokenManager;
import com.atlassian.crowd.manager.token.factory.TokenFactory;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.authentication.ApplicationAuthenticationContext;
import com.atlassian.crowd.model.authentication.AuthenticationContext;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.event.api.EventPublisher;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TokenAuthenticationManagerImplTest
{
    private static final String APPLICATION_NAME = "My Test App";
    private static final String GROUP_1_NAME = "Remote Group 1";
    private static final String PASSWORD_TEXT = "my-password";
    private static final String INVALID_PASSWORD_NAME = "my-mother-once-kissed-Santa";
    private static final String INVALID_APPLICATION_NAME = "applications-are-for-lightweights";
    private static final String RANDOM_HASH = "random-hash";
    private static final String USERNAME = "Principal 1";
    private static final Long DIRECTORY1_ID = 1L;
    private static final ValidationFactor[] NO_FACTORS = {};
    private static final ImmutableList<ValidationFactor> EMPTY_FACTOR_LIST = ImmutableList.of();

    private TokenAuthenticationManagerImpl tokenAuthenticationManager;
    @Mock private DirectoryManager directoryManager;
    @Mock private TokenFactory tokenFactory;
    @Mock private ApplicationDAO applicationDAO;
    @Mock private RemoteDirectory remoteDirectory1;
    @Mock private RemoteDirectory remoteDirectory2;
    @Mock private PropertyManager propertyManager;
    @Mock private TokenManager tokenManager;
    @Mock private CacheManager cacheManager;
    @Mock private EventPublisher eventPublisher;
    @Mock private ApplicationService applicationService;
    @Mock private ApplicationManager applicationManager;
    private Token token;
    private ValidationFactor validationFactor;
    private ApplicationImpl application;
    private DirectoryMapping directoryMapping1;
    private DirectoryMapping directoryMapping2;
    private User principal1;
    private final PasswordCredential passwordCredential = new PasswordCredential(PASSWORD_TEXT, false);


    @Before
    public void setUp() throws Exception
    {
        tokenAuthenticationManager = new TokenAuthenticationManagerImpl(tokenManager, applicationDAO, tokenFactory, cacheManager, eventPublisher, propertyManager, directoryManager, applicationManager, applicationService);

        validationFactor = new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, "127.0.0.1");

        token = new Token.Builder(DIRECTORY1_ID, USERNAME, "identifier-hash", 1234L, RANDOM_HASH).create();

        application = ApplicationImpl.newInstanceWithIdAndCredential(APPLICATION_NAME, ApplicationType.GENERIC_APPLICATION, PasswordCredential.unencrypted("myPassword"), 1L);
        application.setActive(true);
        application.setDescription("desc");

        Directory directory1 = new DirectoryImpl("Test Directory 1", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName())
        {
            public RemoteDirectory getImplementation() throws DirectoryInstantiationException
            {
                return remoteDirectory1;
            }

            public Long getId()
            {
                return DIRECTORY1_ID;
            }
        };
        directoryMapping1 = new DirectoryMapping(application, directory1, Boolean.FALSE);

        Directory directory2 = new DirectoryImpl("Test Directory 2", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName())
        {
            public RemoteDirectory getImplementation() throws DirectoryInstantiationException
            {
                return remoteDirectory2;
            }

            public Long getId()
            {
                return 2L;
            }
        };
        directoryMapping2 = new DirectoryMapping(application, directory2, Boolean.FALSE);

        application.addDirectoryMapping(directoryMapping1.getDirectory(), directoryMapping1.isAllowAllToAuthenticate(),
                                        directoryMapping1.getAllowedOperations().toArray(new OperationType[directoryMapping1.getAllowedOperations().size()]));
        application.addDirectoryMapping(directoryMapping2.getDirectory(), directoryMapping2.isAllowAllToAuthenticate(),
                                        directoryMapping2.getAllowedOperations().toArray(new OperationType[directoryMapping2.getAllowedOperations().size()]));

        principal1 = new UserTemplate(USERNAME, DIRECTORY1_ID);
    }

    protected void setupAuthenticateBasicStubsCache() throws Exception
    {
        when(applicationDAO.findByName(APPLICATION_NAME)).thenReturn(application);
        when(remoteDirectory1.authenticate(USERNAME, passwordCredential)).thenReturn(principal1);
        when(remoteDirectory1.getDirectoryId()).thenReturn(DIRECTORY1_ID);

        when(tokenManager.findByIdentifierHash(anyString())).thenThrow(new ObjectNotFoundException());

        when(propertyManager.isCacheEnabled()).thenReturn(true);

        when(cacheManager.get(eq(TokenAuthenticationManagerImpl.CACHE_NAME), anyString())).thenReturn(true);
    }

    protected void setupAuthenticateBasicStubsNoAuthenticate() throws Exception
    {
        when(applicationDAO.findByName(APPLICATION_NAME)).thenReturn(application);
        when(remoteDirectory1.getDirectoryId()).thenReturn(DIRECTORY1_ID);

        when(tokenManager.findByIdentifierHash(anyString())).thenThrow(new ObjectNotFoundException());

        when(propertyManager.isCacheEnabled()).thenReturn(true);

        when(cacheManager.get(eq(TokenAuthenticationManagerImpl.CACHE_NAME), anyString())).thenThrow(new NotInCacheException());
    }

    protected void setupAuthenticateBasicStubs() throws Exception
    {
        when(remoteDirectory1.authenticate(anyString(), Matchers.<PasswordCredential>anyObject())).thenReturn(principal1);
        setupAuthenticateBasicStubsNoAuthenticate();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGenerateTokenWithANullAuthenticationContext() throws Exception
    {
        tokenAuthenticationManager.generateUserToken(1L, null, TokenLifetime.USE_DEFAULT);
    }

    @Test
    public void testGenerateTokenWithAnAuthContextContainingNullValidationFactors() throws Exception
    {
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, EMPTY_FACTOR_LIST))
            .thenReturn(aCopyOf(token));
        when(tokenManager.findByIdentifierHash(anyString())).thenThrow(new ObjectNotFoundException());

        AuthenticationContext context = new UserAuthenticationContext();
        context.setName(USERNAME);
        context.setCredential(passwordCredential);
        context.setValidationFactors(null);

        Token newToken = tokenAuthenticationManager.generateUserToken(1L, context, TokenLifetime.USE_DEFAULT);

        verify(tokenManager, times(1)).add(any(Token.class));
        verify(tokenFactory, times(1)).create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, EMPTY_FACTOR_LIST);

        assertNotNull(newToken);
        assertEquals(USERNAME, newToken.getName());
    }

    @Test
    public void testGenerateToken() throws Exception
    {
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, Arrays.asList(validationFactor)))
            .thenReturn(aCopyOf(token));
        when(tokenManager.findByIdentifierHash(anyString())).thenThrow(new ObjectNotFoundException());

        AuthenticationContext context = new UserAuthenticationContext(USERNAME, passwordCredential, new ValidationFactor[]{validationFactor}, APPLICATION_NAME);

        when(propertyManager.isIncludeIpAddressInValidationFactors()).thenReturn(true);

        Token newToken = tokenAuthenticationManager.generateUserToken(1L, context, TokenLifetime.USE_DEFAULT);

        verify(tokenManager).findByIdentifierHash(anyString());
        verify(tokenManager).add(any(Token.class));
        assertNotNull(newToken);
        assertEquals(USERNAME, newToken.getName());
    }

    @Test
    public void testGenerateTokenWhenAddFails() throws Exception
    {
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, Arrays.asList(validationFactor)))
            .thenReturn(aCopyOf(token));
        when(tokenManager.findByIdentifierHash(anyString())).thenThrow(new ObjectNotFoundException()).thenReturn(token);

        AuthenticationContext context = new UserAuthenticationContext(USERNAME, passwordCredential, new ValidationFactor[]{validationFactor}, APPLICATION_NAME);

        when(propertyManager.isIncludeIpAddressInValidationFactors()).thenReturn(true);

        when(tokenManager.add(any(Token.class))).thenThrow(ObjectAlreadyExistsException.class);

        Token newToken = tokenAuthenticationManager.generateUserToken(1L, context, TokenLifetime.USE_DEFAULT);

        verify(tokenManager, times(2)).findByIdentifierHash(anyString());
        verify(tokenManager).add(any(Token.class));
        assertNotNull(newToken);
        assertEquals(USERNAME, newToken.getName());
    }

    @Test (expected = OperationFailedException.class)
    public void testGenerateTokenWhenAddFailsAndThenFindFails() throws Exception
    {
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, Arrays.asList(validationFactor)))
            .thenReturn(aCopyOf(token));
        when(tokenManager.findByIdentifierHash(anyString())).thenThrow(new ObjectNotFoundException());

        AuthenticationContext context = new UserAuthenticationContext(USERNAME, passwordCredential, new ValidationFactor[]{validationFactor}, APPLICATION_NAME);

        when(propertyManager.isIncludeIpAddressInValidationFactors()).thenReturn(true);

        when(tokenManager.add(any(Token.class))).thenThrow(ObjectAlreadyExistsException.class);

        tokenAuthenticationManager.generateUserToken(1L, context, TokenLifetime.USE_DEFAULT);
    }

    @Test
    public void generateTokenPassesThroughRemoteAddressValidationFactor() throws Exception
    {
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, ImmutableList.<ValidationFactor>of(validationFactor)))
            .thenReturn(aCopyOf(token));

        when(tokenManager.findByIdentifierHash(token.getIdentifierHash())).thenThrow(new ObjectNotFoundException());

        when(propertyManager.isIncludeIpAddressInValidationFactors()).thenReturn(true);

        AuthenticationContext context = new UserAuthenticationContext(USERNAME, passwordCredential, new ValidationFactor[]{validationFactor}, APPLICATION_NAME);
        tokenAuthenticationManager.generateUserToken(DIRECTORY1_ID, context, TokenLifetime.USE_DEFAULT);

        verify(tokenFactory).create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                    ImmutableList.<ValidationFactor>of(validationFactor));
    }

    @Test
    public void generateTokenDoesNotPassThroughRemoteAddressValidationFactorWhenDisabled() throws Exception
    {
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, EMPTY_FACTOR_LIST)).thenReturn(aCopyOf(token));

        when(tokenManager.findByIdentifierHash(token.getIdentifierHash())).thenThrow(new ObjectNotFoundException());

        when(propertyManager.isIncludeIpAddressInValidationFactors()).thenReturn(false);

        AuthenticationContext context = new UserAuthenticationContext(USERNAME, passwordCredential, new ValidationFactor[]{validationFactor}, APPLICATION_NAME);
        tokenAuthenticationManager.generateUserToken(1, context, TokenLifetime.USE_DEFAULT);

        verify(tokenFactory).create(1, USERNAME, TokenLifetime.USE_DEFAULT, EMPTY_FACTOR_LIST);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGenericValidateTokenWithNullTokenKey() throws Exception
    {
        tokenAuthenticationManager.genericValidateToken(null, NO_FACTORS);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testGenericValidateTokenWithNullValidationFactors() throws Exception
    {
        tokenAuthenticationManager.genericValidateToken("secret-token-key", null);
    }

    @Test
    public void testGenericValidateToken() throws Exception
    {
        when(tokenManager.findByRandomHash(RANDOM_HASH)).thenReturn(token);
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, EMPTY_FACTOR_LIST,
                                 token.getRandomNumber()))
            .thenReturn(aCopyOf(token));
        when(propertyManager.getSessionTime()).thenReturn(30L); // 30 minutes
        when(tokenManager.update(any(Token.class))).thenReturn(token);

        Token actualToken = tokenAuthenticationManager.genericValidateToken(RANDOM_HASH, NO_FACTORS);

        assertNotNull(actualToken);
    }

    @Test(expected = InvalidTokenException.class)
    public void testGenericValidateTokenKeysNotEqual() throws Exception
    {
        when(tokenManager.findByRandomHash(RANDOM_HASH)).thenReturn(token);
        Token differentToken = new Token.Builder(token).setRandomHash("new-secret-key").create();
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                 EMPTY_FACTOR_LIST, token.getRandomNumber()))
            .thenReturn(differentToken);

        tokenAuthenticationManager.genericValidateToken(RANDOM_HASH, NO_FACTORS);
    }

    @Test
    public void testApplicationAuthenticationEvent() throws Exception
    {
        when(applicationDAO.findByName(APPLICATION_NAME)).thenReturn(application);
        when(tokenManager.findByIdentifierHash(anyString())).thenThrow(new ObjectNotFoundException());
        when(tokenFactory.create(Token.APPLICATION_TOKEN_DIRECTORY_ID, APPLICATION_NAME,
                                 TokenLifetime.USE_DEFAULT, EMPTY_FACTOR_LIST))
            .thenReturn(aCopyOf(token));
        when(applicationManager.authenticate(application, passwordCredential)).thenReturn(true);

        // auth context
        ApplicationAuthenticationContext authContext = new ApplicationAuthenticationContext();
        authContext.setName(APPLICATION_NAME);
        authContext.setCredential(passwordCredential);

        tokenAuthenticationManager.authenticateApplication(authContext);

        // this event firing is the point of the test...
        verify(eventPublisher, times(1)).publish(any(ApplicationAuthenticatedEvent.class));
        verify(tokenManager, times(1)).add(any(Token.class));
    }

    @Test(expected = InvalidAuthenticationException.class)
    public void testAuthenticate_NullUserAuthenticationContext() throws Exception
    {
        tokenAuthenticationManager.authenticateUser((UserAuthenticationContext) null, TokenLifetime.USE_DEFAULT);
    }

    @Test(expected = InvalidAuthenticationException.class)
    public void testAuthenticate_NonExistentPrincipal() throws Exception
    {
        when(applicationDAO.findByName(APPLICATION_NAME)).thenReturn(application);
        when(remoteDirectory1.authenticate(anyString(), Matchers.<PasswordCredential>anyObject())).thenThrow(new UserNotFoundException(INVALID_PASSWORD_NAME));
        when(remoteDirectory2.authenticate(anyString(), Matchers.<PasswordCredential>anyObject())).thenThrow(new UserNotFoundException(INVALID_PASSWORD_NAME));
        when(applicationService.authenticateUser(application, INVALID_PASSWORD_NAME, PasswordCredential.unencrypted(PASSWORD_TEXT))).thenThrow(InvalidAuthenticationException.newInstanceWithName(INVALID_PASSWORD_NAME));

        UserAuthenticationContext userAuth = new UserAuthenticationContext(INVALID_PASSWORD_NAME, PasswordCredential.unencrypted(PASSWORD_TEXT), NO_FACTORS, APPLICATION_NAME);

        tokenAuthenticationManager.authenticateUser(userAuth, TokenLifetime.USE_DEFAULT);
    }

    @Test(expected = InvalidAuthenticationException.class)
    public void testAuthenticate_NonExistentApplication() throws Exception
    {
        when(applicationDAO.findByName(INVALID_APPLICATION_NAME)).thenThrow(new ApplicationNotFoundException(INVALID_APPLICATION_NAME));

        UserAuthenticationContext userAuth = new UserAuthenticationContext(INVALID_PASSWORD_NAME, passwordCredential, NO_FACTORS, INVALID_APPLICATION_NAME);

        tokenAuthenticationManager.authenticateUser(userAuth, TokenLifetime.USE_DEFAULT);
    }

    @Test
    public void testAuthenticate_NoAccess() throws Exception
    {
        setupAuthenticateBasicStubs();
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, EMPTY_FACTOR_LIST))
            .thenReturn(aCopyOf(token));

        // We have authentication success, just no auth
        when(applicationService.authenticateUser(application, USERNAME, PasswordCredential.unencrypted(PASSWORD_TEXT)))
            .thenReturn(principal1);

        try
        {
            UserAuthenticationContext userAuth = new UserAuthenticationContext(USERNAME, passwordCredential, NO_FACTORS, APPLICATION_NAME);

            tokenAuthenticationManager.authenticateUser(userAuth, TokenLifetime.USE_DEFAULT);

            fail("Should have thrown ApplicationAccessDeniedException on non-existent principal");
        }
        catch (ApplicationAccessDeniedException e)
        {

        }

        verify(eventPublisher, times(1)).publish(any(UserAuthenticationFailedAccessDeniedEvent.class));
    }

    @Test
    public void testAuthenticate_AccessThroughCache() throws Exception
    {
        setupAuthenticateBasicStubsCache();
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, EMPTY_FACTOR_LIST))
            .thenReturn(aCopyOf(token));

        when(applicationService.authenticateUser(application, USERNAME, PasswordCredential.unencrypted(PASSWORD_TEXT))).thenReturn(principal1);
        when(applicationService.isUserAuthorised(application, USERNAME)).thenReturn(true);

        UserAuthenticationContext userAuth = new UserAuthenticationContext(USERNAME, passwordCredential, NO_FACTORS, APPLICATION_NAME);

        Token actualToken = tokenAuthenticationManager.authenticateUser(userAuth, TokenLifetime.USE_DEFAULT);

        assertNotNull(actualToken);
        assertNotNull(actualToken.getRandomHash());
        verify(eventPublisher, times(1)).publish(any(UserAuthenticationSucceededEvent.class));
    }

    @Test
    public void testAuthenticate_AccessThroughApplication() throws Exception
    {
        setupAuthenticateBasicStubs();
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                 EMPTY_FACTOR_LIST))
            .thenReturn(aCopyOf(token));

        application.addDirectoryMapping(directoryMapping1.getDirectory(), true, directoryMapping1.getAllowedOperations().toArray(
            new OperationType[directoryMapping1.getAllowedOperations().size()]));

        when(applicationService.authenticateUser(application, USERNAME, PasswordCredential.unencrypted(PASSWORD_TEXT))).thenReturn(principal1);
        when(applicationService.isUserAuthorised(application, USERNAME)).thenReturn(true);

        //  principal1 is covered by this mapping.
        UserAuthenticationContext userAuth = new UserAuthenticationContext(USERNAME, passwordCredential, NO_FACTORS, APPLICATION_NAME);

        Token actualToken = tokenAuthenticationManager.authenticateUser(userAuth, TokenLifetime.USE_DEFAULT);

        assertNotNull(actualToken);
        assertNotNull(actualToken.getRandomHash());
        verify(eventPublisher, times(1)).publish(any(UserAuthenticationSucceededEvent.class));
    }

    @Test
    public void testAuthenticate_AccessThroughGroup() throws Exception
    {
        setupAuthenticateBasicStubs();
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                 EMPTY_FACTOR_LIST))
            .thenReturn(aCopyOf(token));

        when(applicationService.authenticateUser(application, USERNAME, PasswordCredential.unencrypted(PASSWORD_TEXT))).thenReturn(principal1);
        when(applicationService.isUserAuthorised(application, USERNAME)).thenReturn(true);

        // add groups allowed to auth to the application
        application.getDirectoryMapping(DIRECTORY1_ID).addGroupMapping(GROUP_1_NAME);
        // principal1 is a member of group1 in directory1

        // says that principal1 is a member of group1
        when(directoryManager.isUserNestedGroupMember(DIRECTORY1_ID, USERNAME, GROUP_1_NAME)).thenReturn(true);

        UserAuthenticationContext userAuth = new UserAuthenticationContext(USERNAME, new PasswordCredential(PASSWORD_TEXT), NO_FACTORS, APPLICATION_NAME);

        Token actualToken = tokenAuthenticationManager.authenticateUser(userAuth, TokenLifetime.USE_DEFAULT);

        assertNotNull(actualToken);
        assertNotNull(actualToken.getRandomHash());
        verify(eventPublisher, times(1)).publish(any(UserAuthenticationSucceededEvent.class));
    }

    @Test
    public void testAuthenticate_AccessDeniedThroughGroup() throws Exception
    {
        setupAuthenticateBasicStubs();

        when(applicationService.authenticateUser(application, USERNAME, PasswordCredential.unencrypted(PASSWORD_TEXT))).thenReturn(principal1);
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, EMPTY_FACTOR_LIST))
            .thenReturn(aCopyOf(token));

        // add groups allowed to auth to the application
        application.getDirectoryMapping(DIRECTORY1_ID).addGroupMapping(GROUP_1_NAME);
        // principal1 is a member of group1 in directory1

        // says that principal1 is *not* a member of group1
        when(directoryManager.isUserNestedGroupMember(DIRECTORY1_ID, USERNAME, GROUP_1_NAME)).thenReturn(false);

        try
        {
            UserAuthenticationContext userAuth = new UserAuthenticationContext(USERNAME, new PasswordCredential(PASSWORD_TEXT), NO_FACTORS, APPLICATION_NAME);
            tokenAuthenticationManager.authenticateUser(userAuth, TokenLifetime.USE_DEFAULT);

            fail("Should have thrown ApplicationAccessDeniedException - user is not authorized");
        }
        catch (ApplicationAccessDeniedException e)
        {

        }

        verify(eventPublisher, times(1)).publish(any(UserAuthenticationSucceededEvent.class));
    }

    @Test(expected = InvalidAuthenticationException.class)
    public void testAuthenticate_InvalidPassword() throws Exception
    {
        setupAuthenticateBasicStubsNoAuthenticate();

        when(applicationService.authenticateUser(application, USERNAME, PasswordCredential.unencrypted(PASSWORD_TEXT)))
            .thenThrow(InvalidAuthenticationException.newInstanceWithName("nope!"));

        // add groups allowed to auth to the application
        application.getDirectoryMapping(DIRECTORY1_ID).addGroupMapping(GROUP_1_NAME);
        // principal1 is a member of group1 in directory1

        // says that principal1 is a member of group1
        when(directoryManager.isUserNestedGroupMember(DIRECTORY1_ID, USERNAME, GROUP_1_NAME)).thenReturn(true);

        UserAuthenticationContext userAuth = new UserAuthenticationContext(USERNAME, new PasswordCredential(PASSWORD_TEXT), NO_FACTORS, APPLICATION_NAME);
        tokenAuthenticationManager.authenticateUser(userAuth, TokenLifetime.USE_DEFAULT);
    }

    @Test
    public void testAuthenticate_AccessThroughGroup_IgnoreCache() throws Exception
    {
        setupAuthenticateBasicStubs();
        when(applicationService.authenticateUser(application, USERNAME, PasswordCredential.unencrypted(PASSWORD_TEXT))).thenReturn(principal1);
        when(applicationService.isUserAuthorised(application, USERNAME)).thenReturn(true);
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, EMPTY_FACTOR_LIST))
            .thenReturn(aCopyOf(token));

        // add groups allowed to auth to the application
        application.getDirectoryMapping(DIRECTORY1_ID).addGroupMapping(GROUP_1_NAME);

        // says that principal1 is a member of group1
        when(directoryManager.isUserNestedGroupMember(DIRECTORY1_ID, USERNAME, GROUP_1_NAME)).thenReturn(true);

        UserAuthenticationContext userAuth = new UserAuthenticationContext(USERNAME, new PasswordCredential(PASSWORD_TEXT), NO_FACTORS, APPLICATION_NAME);
        Token actualToken =
            tokenAuthenticationManager.authenticateUser(userAuth, true, true, TokenLifetime.USE_DEFAULT);

        assertNotNull(actualToken);
        assertNotNull(actualToken.getRandomHash());
        verify(eventPublisher, times(1)).publish(any(UserAuthenticationSucceededEvent.class));
    }

    @Test(expected = ApplicationAccessDeniedException.class)
    public void findUserByTokenFailsWhenUserDoesNotHaveAccessToThatApplication() throws Exception
    {
        setupAuthenticateBasicStubs();

        DirectoryMapping dm = application.getDirectoryMapping(1);
        dm.setAllowAllToAuthenticate(false);

        when(applicationService.isUserAuthorised(application, USERNAME)).thenReturn(Boolean.TRUE);

        when(tokenManager.findByRandomHash(RANDOM_HASH)).thenReturn(token);
        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        tokenAuthenticationManager.findUserByToken(RANDOM_HASH, application.getName());
    }

    @Test
    public void findUserByTokenSucceedsWhenUserHasAccessToThatApplication() throws Exception
    {
        setupAuthenticateBasicStubs();

        DirectoryMapping dm = application.getDirectoryMapping(1);
        dm.setAllowAllToAuthenticate(true);

        when(applicationService.isUserAuthorised(application, USERNAME)).thenReturn(Boolean.TRUE);

        when(tokenManager.findByRandomHash(RANDOM_HASH)).thenReturn(token);
        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);
        when(directoryManager.findUserByName(1, USERNAME)).thenReturn(principal1);

        User user = tokenAuthenticationManager.findUserByToken(RANDOM_HASH, application.getName());
        assertSame(principal1, user);
    }

    @Test(expected = ApplicationNotFoundException.class)
    public void findUserByTokenPropagatesApplicationNotFoundException() throws Exception
    {
        setupAuthenticateBasicStubs();

        when(tokenManager.findByRandomHash(RANDOM_HASH)).thenThrow(new ApplicationNotFoundException(""));

        tokenAuthenticationManager.findUserTokenByKey(RANDOM_HASH, application.getName());
    }

    @Test(expected = ApplicationAccessDeniedException.class)
    public void findTokenByKeyFailsWhenUserDoesNotHaveAccessToThatApplication() throws Exception
    {
        setupAuthenticateBasicStubs();

        DirectoryMapping dm = application.getDirectoryMapping(1);
        dm.setAllowAllToAuthenticate(false);

        when(tokenManager.findByRandomHash(RANDOM_HASH)).thenReturn(token);
        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        tokenAuthenticationManager.findUserTokenByKey(RANDOM_HASH, APPLICATION_NAME);
    }

    @Test(expected = InvalidTokenException.class)
    public void findTokenByKeyFailsWhenTokenIsAnApplicationToken() throws Exception
    {
        setupAuthenticateBasicStubs();

        DirectoryMapping dm = application.getDirectoryMapping(1);
        dm.setAllowAllToAuthenticate(true);

        Token applicationToken = new Token.Builder(token)
            .setDirectoryId(Token.APPLICATION_TOKEN_DIRECTORY_ID)
            .create();
        when(tokenManager.findByRandomHash(RANDOM_HASH)).thenReturn(applicationToken);
        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        tokenAuthenticationManager.findUserTokenByKey(RANDOM_HASH, APPLICATION_NAME);
    }

    @Test(expected = InvalidTokenException.class)
    public void findTokenByKeyFailsWhenTokenCannotBeFound() throws Exception
    {
        setupAuthenticateBasicStubs();

        DirectoryMapping dm = application.getDirectoryMapping(1);
        dm.setAllowAllToAuthenticate(true);

        when(tokenManager.findByRandomHash(RANDOM_HASH)).thenThrow(new ObjectNotFoundException("token not found"));
        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        tokenAuthenticationManager.findUserTokenByKey(RANDOM_HASH, APPLICATION_NAME);
    }

    @Test
    public void findTokenByKeySucceedsWhenUserHasAccessToThatApplication() throws Exception
    {
        setupAuthenticateBasicStubs();

        DirectoryMapping dm = application.getDirectoryMapping(1);
        dm.setAllowAllToAuthenticate(true);

        when(tokenManager.findByRandomHash(RANDOM_HASH)).thenReturn(token);
        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);

        Token actualToken = tokenAuthenticationManager.findUserTokenByKey(RANDOM_HASH, APPLICATION_NAME);
        assertSame(token, actualToken);
    }

    @Test
    public void remoteAddressPassedToTokenFactoryForValidationWhenIncluded() throws Exception
    {
        Token activeToken = new Token.Builder(token).setLastAccessedTime(Long.MAX_VALUE).create();

        when(tokenManager.findByRandomHash(RANDOM_HASH)).thenReturn(activeToken);
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                 ImmutableList.<ValidationFactor>of(validationFactor), activeToken.getRandomNumber()))
            .thenReturn(aCopyOf(activeToken));
        when(tokenManager.update(activeToken)).thenReturn(activeToken);

        when(propertyManager.isIncludeIpAddressInValidationFactors()).thenReturn(true);

        Token actualToken = tokenAuthenticationManager.genericValidateToken(RANDOM_HASH, new ValidationFactor[]{validationFactor});
        assertSame(activeToken, actualToken);
        verify(tokenFactory).create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                    ImmutableList.<ValidationFactor>of(validationFactor), activeToken.getRandomNumber());
    }

    @Test
    public void remoteAddressNotPassedToTokenFactoryForValidationWhenProvidedButDisabled() throws Exception
    {
        Token activeToken = new Token.Builder(token).setLastAccessedTime(Long.MAX_VALUE).create();

        when(tokenManager.findByRandomHash(RANDOM_HASH)).thenReturn(activeToken);
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                 EMPTY_FACTOR_LIST, activeToken.getRandomNumber()))
            .thenReturn(aCopyOf(activeToken));
        when(tokenManager.update(activeToken)).thenReturn(activeToken);

        when(propertyManager.isIncludeIpAddressInValidationFactors()).thenReturn(false);

        Token actualToken = tokenAuthenticationManager.genericValidateToken(RANDOM_HASH, new ValidationFactor[]{validationFactor});
        assertSame(activeToken, actualToken);
        verify(tokenFactory).create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT,
                                    EMPTY_FACTOR_LIST, activeToken.getRandomNumber());
    }

    @Test
    public void activeTokensExcludesRemoteAddress()
    {
        assertEquals(Collections.emptyList(), tokenAuthenticationManager.activeValidationFactors(NO_FACTORS));
        assertEquals(Collections.emptyList(), tokenAuthenticationManager.activeValidationFactors(new ValidationFactor[]{validationFactor}));
    }

    @Test
    public void activeTokensNeverExcludesOtherFactors()
    {
        ValidationFactor vf = new ValidationFactor("name", "value");

        when(propertyManager.isIncludeIpAddressInValidationFactors()).thenReturn(true);
        assertEquals(ImmutableList.of(vf), tokenAuthenticationManager.activeValidationFactors(new ValidationFactor[]{vf}));

        when(propertyManager.isIncludeIpAddressInValidationFactors()).thenReturn(false);
        assertEquals(ImmutableList.of(vf), tokenAuthenticationManager.activeValidationFactors(new ValidationFactor[]{vf}));
    }

    @Test
    public void authenticateUserWithoutValidatingPasswordFiresUserAuthenticationSucceededEvent() throws Exception
    {
        UserTemplate user = new UserTemplate(USERNAME);
        user.setActive(true);
        user.setDirectoryId(DIRECTORY1_ID);

        when(applicationDAO.findByName("application")).thenReturn(application);
        when(applicationService.findUserByName(application, USERNAME)).thenReturn(user);
        when(tokenFactory.create(DIRECTORY1_ID, USERNAME, TokenLifetime.USE_DEFAULT, EMPTY_FACTOR_LIST))
            .thenReturn(aCopyOf(token));
        when(tokenManager.findByIdentifierHash(anyString())).thenThrow(new ObjectNotFoundException());
        when(applicationService.isUserAuthorised(application, USERNAME)).thenReturn(true);

        UserAuthenticationContext context = new UserAuthenticationContext();
        context.setApplication("application");
        context.setName(USERNAME);
        context.setValidationFactors(null);

        tokenAuthenticationManager.authenticateUserWithoutValidatingPassword(context);

        ArgumentCaptor<UserAuthenticationSucceededEvent> argument = ArgumentCaptor.forClass(UserAuthenticationSucceededEvent.class);
        verify(eventPublisher).publish(argument.capture());
        assertEquals(user, argument.getValue().getRemotePrincipal());
        assertEquals(application, argument.getValue().getApplication());
    }

    @Test
    public void createdTokenUsesUsernameCaseFromDirectory() throws Exception
    {
        /* The username in the directory is uppercase */
        String realCase = "ADMIN";

        Token tokenWithRealCase = new Token.Builder(token).setName(realCase).create();
        when(tokenFactory.create(DIRECTORY1_ID, realCase, TokenLifetime.USE_DEFAULT,
                                 EMPTY_FACTOR_LIST)).thenReturn(tokenWithRealCase);

        when(tokenManager.findByIdentifierHash(anyString())).thenThrow(new ObjectNotFoundException());

        when(applicationDAO.findByName(APPLICATION_NAME)).thenReturn(application);
        when(applicationService.isUserAuthorised(application, realCase)).thenReturn(true);

        User user = new UserTemplate(realCase, DIRECTORY1_ID);


        /* Authentication is attempted with the lowercase version */
        String providedCase = "admin";
        UserAuthenticationContext context = new UserAuthenticationContext(providedCase, passwordCredential, NO_FACTORS, APPLICATION_NAME);

        when(applicationService.authenticateUser(application, providedCase, passwordCredential)).thenReturn(user);

        Token actualToken =
            tokenAuthenticationManager.authenticateUser(context, true, false, TokenLifetime.USE_DEFAULT);

        verify(tokenManager).add(tokenWithRealCase);
        assertSame(tokenWithRealCase, actualToken);

        /* The TokenFactory is supplied with the directory-stored case */
        verify(tokenFactory).create(1L, realCase, TokenLifetime.USE_DEFAULT, EMPTY_FACTOR_LIST);
    }

    @Test
    public void validationFactorsAreStringifiedReadably()
    {
        ValidationFactor[] factors = {
                new ValidationFactor("name", "value"),
                new ValidationFactor("second-name", "second-value")
        };

        assertEquals("ValidationFactor[name=value], ValidationFactor[second-name=second-value]", TokenAuthenticationManagerImpl.toString(factors));
    }

    @Test
    public void tokenWithDefaultDurationExpiresWithGlobalMaximumSessionDuration()
    {
        when(propertyManager.getSessionTime()).thenReturn(60L); // in minutes

        Token tokenUnderValidation = new Token.Builder(token)
            .withDefaultDuration()
            .setLastAccessedTime(1L) // long time ago, but not unset
            .create();

        assertTrue("Token should expire with the global maximum session duration",
                   tokenAuthenticationManager.isExpired(tokenUnderValidation));
    }

    @Test
    public void shortLivedTokenExpiresWithSpecificSessionDuration()
    {
        when(propertyManager.getSessionTime()).thenReturn(60L); // in minutes

        Token tokenUnderValidation = new Token.Builder(token)
            .setLifetime(TokenLifetime.inSeconds(TimeUnit.MINUTES.toSeconds(10)))
            .setLastAccessedTime(System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(15))  // 15 mins ago
            .create();

        assertTrue("Short-lived token should expire with its specific session duration",
                   tokenAuthenticationManager.isExpired(tokenUnderValidation));
    }

    @Test
    public void shortLivedRecentlyAccessedTokenIsNotExpired()
    {
        when(propertyManager.getSessionTime()).thenReturn(60L); // in minutes

        Token tokenUnderValidation = new Token.Builder(token)
            .setLifetime(TokenLifetime.inSeconds(TimeUnit.MINUTES.toSeconds(10)))
            .setLastAccessedTime(System.currentTimeMillis() - TimeUnit.MINUTES.toMillis(1))  // 1 min ago
            .create();

        assertFalse("Short-lived token should not expire immediately",
                    tokenAuthenticationManager.isExpired(tokenUnderValidation));
    }

    @Test
    public void zeroLivedTokenExpiresImmediately()
    {
        when(propertyManager.getSessionTime()).thenReturn(60L); // in minutes

        Token tokenUnderValidation = new Token.Builder(token)
            .setLifetime(TokenLifetime.inSeconds(0))
            .setLastAccessedTime(System.currentTimeMillis())  // just now
            .create();

        assertTrue("Zero-lived token should expire immediately",
                    tokenAuthenticationManager.isExpired(tokenUnderValidation));
    }

    @Test
    public void recentlyAccessedTokenWithDefaultDurationIsNotExpired()
    {
        when(propertyManager.getSessionTime()).thenReturn(60L); // in minutes

        Token tokenUnderValidation = new Token.Builder(token)
            .withDefaultDuration()
            .setLastAccessedTime(System.currentTimeMillis())  // just now
            .create();

        assertFalse("Recently accessed token should not be expired",
                    tokenAuthenticationManager.isExpired(tokenUnderValidation));
    }

    @Test
    public void removeExpiredTokens()
    {
        when(propertyManager.getSessionTime()).thenReturn(30L); // in minutes
        tokenAuthenticationManager.removeExpiredTokens();
        verify(tokenManager).removeExpiredTokens(any(Date.class), eq(TimeUnit.MINUTES.toSeconds(30)));
    }

    @Test
    public void tokenWithDefaultDurationExpiryDate()
    {
        when(propertyManager.getSessionTime()).thenReturn(30L); // in minutes

        Token tokenUnderValidation = new Token.Builder(token)
            .withDefaultDuration()
            .setLastAccessedTime(3L)  // in millis
            .create();

        assertEquals(new Date(TimeUnit.MINUTES.toMillis(30L) + 3L),
                     tokenAuthenticationManager.getTokenExpiryTime(tokenUnderValidation));
    }

    @Test
    public void expiryDateDoesNotOverflowWhenDefaultDurationIsLargeInMilliseconds()
    {
        when(propertyManager.getSessionTime()).thenReturn(TimeUnit.MILLISECONDS.toMinutes(Long.MAX_VALUE));

        Date lastAccessed = new Date(1357775096259L); // a realistic date, sometime in 2013
        Token tokenUnderValidation = new Token.Builder(token)
            .withDefaultDuration()
            .setLastAccessedTime(lastAccessed.getTime())
            .create();

        Date expiryTime = tokenAuthenticationManager.getTokenExpiryTime(tokenUnderValidation);
        assertTrue("Dates should not overflow", lastAccessed.before(expiryTime));
    }

    @Test
    public void expiryDateDoesNotOverflowWhenDefaultDurationIsLargeInSeconds()
    {
        when(propertyManager.getSessionTime()).thenReturn(Long.MAX_VALUE);

        Date lastAccessed = new Date(1357775096259L); // a realistic date, sometime in 2013
        Token tokenUnderValidation = new Token.Builder(token)
            .withDefaultDuration()
            .setLastAccessedTime(lastAccessed.getTime())
            .create();

        Date expiryTime = tokenAuthenticationManager.getTokenExpiryTime(tokenUnderValidation);
        assertTrue("Dates should not overflow", lastAccessed.before(expiryTime));
    }

    @Test
    public void shortLivedTokenExpiryDate()
    {
        when(propertyManager.getSessionTime()).thenReturn(30L); // 30 minutes

        Token tokenUnderValidation = new Token.Builder(token)
            .setLifetime(TokenLifetime.inSeconds(5L))
            .setLastAccessedTime(3L)  // in millis
            .create();

        assertEquals(new Date(TimeUnit.SECONDS.toMillis(5L) + 3L),
                     tokenAuthenticationManager.getTokenExpiryTime(tokenUnderValidation));
    }

    private static Token aCopyOf(Token token)
    {
        return new Token.Builder(token).create();
    }
}
