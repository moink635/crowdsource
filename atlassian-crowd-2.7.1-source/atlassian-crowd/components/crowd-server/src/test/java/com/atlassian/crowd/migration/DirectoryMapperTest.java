package com.atlassian.crowd.migration;

import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.migration.legacy.XmlMapper;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.apache.commons.lang3.time.DateUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static org.mockito.Mockito.mock;

/**
 * DirectoryMapper Tester.
 */
public class DirectoryMapperTest extends BaseMapperTest
{
    private DirectoryMapper directoryMapper;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        // These xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        BatchProcessor batchProcessor = mock(BatchProcessor.class);
        DirectoryManager directoryManager = mock(DirectoryManager.class);
        directoryMapper = new DirectoryMapper(sessionFactory, batchProcessor, directoryManager);

    }

    public void testImportAndExportDirectories() throws Exception
    {
        Element directoriesNode = (Element) document.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + DirectoryMapper.DIRECTORY_XML_ROOT);
        List<Element> directoryElements = directoriesNode.elements(DirectoryMapper.DIRECTORY_XML_NODE);

        SimpleDateFormat sdf = new SimpleDateFormat(XmlMapper.XML_DATE_FORMAT);

        // test import

        DirectoryImpl directory = directoryMapper.getDirectoryFromXml(directoryElements.get(0));

        assertEquals(new Long(32769), directory.getId());
        assertEquals("Crowd Local", directory.getName());
        assertTrue(directory.isActive());

        assertTrue(DateUtils.isSameInstant(sdf.parse("Fri Apr 03 16:36:58 +1100 2009"), directory.getCreatedDate()));
        assertTrue(DateUtils.isSameInstant(sdf.parse("Fri Apr 03 16:37:29 +1100 2009"), directory.getUpdatedDate()));

        assertEquals(toLowerCase("Crowd Local"), directory.getLowerName());
        assertEquals("com.atlassian.crowd.directory.InternalDirectory", directory.getImplementationClass());
//        assertEquals("com.atlassian.crowd.integration.directory.internal.InternalDirectory", directory.getImplementationClass());
        assertEquals("com.atlassian.crowd.directory.InternalDirectory".toLowerCase(Locale.ENGLISH), directory.getLowerImplementationClass());
//        assertEquals("com.atlassian.crowd.integration.directory.internal.InternalDirectory".toLowerCase(), directory.getLowerImplementationClass());
        assertEquals("", directory.getDescription());
        assertEquals(DirectoryType.INTERNAL, directory.getType());

        Set<OperationType> expectedOperations = new HashSet<OperationType>(Arrays.asList(OperationType.CREATE_USER, OperationType.CREATE_GROUP));
        assertTrue(directory.getAllowedOperations().containsAll(expectedOperations));
        assertEquals(2, directory.getAllowedOperations().size());

        assertEquals(5, directory.getKeys().size());
        assertEquals("0", directory.getValue("password_max_attempts"));
        assertEquals("0", directory.getValue("password_max_change_time"));
        assertEquals("0", directory.getValue("password_history_count"));
        assertEquals("atlassian-security", directory.getValue("user_encryption_method"));
        assertEquals("", directory.getValue("password_regex"));

        // test export

        Document document = DocumentHelper.createDocument();
        Element root = document.addElement(XmlMigrationManagerImpl.XML_ROOT);
        Element directoryRoot = root.addElement(DirectoryMapper.DIRECTORY_XML_ROOT);

        directoryMapper.addDirectoryToXml(directory, directoryRoot);

        String export = exportDocumentToString(document);

        assertEquals(DocumentHelper.parseText(getTestExportXmlString()).asXML(), DocumentHelper.parseText(export).asXML());
    }
}
