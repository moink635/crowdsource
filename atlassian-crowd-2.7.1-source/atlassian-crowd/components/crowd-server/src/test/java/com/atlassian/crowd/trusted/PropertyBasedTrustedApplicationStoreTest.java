package com.atlassian.crowd.trusted;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Iterator;
import java.util.List;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.security.auth.trustedapps.DefaultTrustedApplication;
import com.atlassian.security.auth.trustedapps.EncryptionProvider;
import com.atlassian.security.auth.trustedapps.RequestConditions;
import com.atlassian.security.auth.trustedapps.TrustedApplication;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

/**
 * @since v2.7
 */
@RunWith(MockitoJUnitRunner.class)
public class PropertyBasedTrustedApplicationStoreTest
{
    private static final String[] PUBLIC_KEYS = {"PUBLICKEY1", "PUBLICKEY2"};
    private static final String[] PUBLIC_KEYS_BASE64 = {"UFVCTElDS0VZMQ==", "UFVCTElDS0VZMg=="};

    @InjectMocks
    private PropertyBasedTrustedApplicationStore service;
    @Mock
    private EncryptionProvider encryptionProvider;
    @Mock
    private PropertyManager propertyManager;

    @Test
    public void getTrustedApplicationWithInvalidIdShouldReturnNull() throws Exception
    {
        final TrustedApplication application = service.getTrustedApplication("someId");

        assertThat(application, is(nullValue()));
    }

    @Test
    public void getTrustedApplicationWithValidIdShouldReturnTrustedApp() throws Exception
    {
        setPropsForTrustedApp(0);

        final TrustedApplication application = service.getTrustedApplication("appId0");

        assertThat(application.getID(), is("appId0"));
        assertThat(application.getPublicKey().getFormat(), is(PUBLIC_KEYS[0]));
        assertThat(application.getRequestConditions().getCertificateTimeout(), is(10L));
        assertThat(application.getRequestConditions().getIPPatterns(), containsInAnyOrder("127.0.0.0", "192.168.0.0"));
        assertThat(application.getRequestConditions().getURLPatterns(), containsInAnyOrder("http://url0.com", "http://anotherurl0.com"));
    }

    @Test
    public void trustedApplicationWithOnlyPublicKeySetShouldBeReturnedWithoutError() throws Exception
    {
        final String id = "appId0";
        final PublicKey publicKey = mock(PublicKey.class);
        when(publicKey.getFormat()).thenReturn(PUBLIC_KEYS[0]); // Just for asserting the different keys
        when(propertyManager.getString(eq("trustedapps." + id + ".public.key"), anyString())).thenReturn(PUBLIC_KEYS_BASE64[0]);
        when(propertyManager.getString(eq("trustedapps." + id + ".timeout"), anyString())).thenReturn("1");
        when(encryptionProvider.toPublicKey(PUBLIC_KEYS[0].getBytes(Charsets.UTF_8))).thenReturn(publicKey);

        final TrustedApplication application = service.getTrustedApplication("appId0");

        assertThat(application.getID(), is("appId0"));
        assertThat(application.getPublicKey().getFormat(), is(PUBLIC_KEYS[0]));
        assertThat(application.getRequestConditions().getCertificateTimeout(), is(1L));
        assertThat(application.getRequestConditions().getIPPatterns(), Matchers.emptyIterable());
        assertThat(application.getRequestConditions().getURLPatterns(), Matchers.emptyIterable());
    }

    @Test
    public void getTrustedApplicationsShouldReturnAllTrustedApps() throws Exception
    {
        setPropsForTrustedApp(0);
        setPropsForTrustedApp(1);

        when(propertyManager.getString(eq("trustedapps.keys"), anyString())).thenReturn("appId0\nappId1");

        final Iterable<TrustedApplication> applications = service.getTrustedApplications();
        assertThat(applications, Matchers.<TrustedApplication>iterableWithSize(2));

        final Iterator<TrustedApplication> iterator = applications.iterator();
        TrustedApplication application = iterator.next();
        assertThat(application.getID(), is("appId0"));
        assertThat(application.getPublicKey().getFormat(), is(PUBLIC_KEYS[0]));
        assertThat(application.getRequestConditions().getCertificateTimeout(), is(10L));
        assertThat(application.getRequestConditions().getIPPatterns(), containsInAnyOrder("127.0.0.0", "192.168.0.0"));
        assertThat(application.getRequestConditions().getURLPatterns(), containsInAnyOrder("http://url0.com", "http://anotherurl0.com"));

        application = iterator.next();
        assertThat(application.getID(), is("appId1"));
        assertThat(application.getPublicKey().getFormat(), is(PUBLIC_KEYS[1]));
        assertThat(application.getRequestConditions().getCertificateTimeout(), is(11L));
        assertThat(application.getRequestConditions().getIPPatterns(), containsInAnyOrder("127.0.0.1", "192.168.0.1"));
        assertThat(application.getRequestConditions().getURLPatterns(), containsInAnyOrder("http://url1.com", "http://anotherurl1.com"));
    }

    @Test
    public void addTrustedApplicationShouldPersist() throws Exception
    {
        final PublicKey publicKey = mock(PublicKey.class);
        final RequestConditions requestConditions = RequestConditions.builder()
                .setCertificateTimeout(13L)
                .addIPPattern("12.12.12.12", "13.13.13.13")
                .addURLPattern("someUrl", "anotherUrl")
                .build();
        final TrustedApplication application = new DefaultTrustedApplication(encryptionProvider, publicKey, "appId", requestConditions);

        when(publicKey.getEncoded()).thenReturn(PUBLIC_KEYS[0].getBytes(Charsets.UTF_8));
        when(propertyManager.getString(eq("trustedapps.keys"), anyString())).thenReturn("someId\nanotherId");
        service.addTrustedApplication(application);

        verify(propertyManager).setProperty("trustedapps.keys", "someId\nanotherId\nappId");
        verify(propertyManager).setProperty("trustedapps.appId.public.key", PUBLIC_KEYS_BASE64[0]);
        verify(propertyManager).setProperty("trustedapps.appId.timeout", "13");
        verify(propertyManager).setProperty(eq("trustedapps.appId.urls"), argThat(Matchers.<String>either(is("someUrl\nanotherUrl")).or(is("anotherUrl\nsomeUrl"))));
        verify(propertyManager).setProperty(eq("trustedapps.appId.ips"), argThat(Matchers.<String>either(is("12.12.12.12\n13.13.13.13")).or(is("13.13.13.13\n12.12.12.12"))));
    }

    @Test
    public void deleteTrustedApplicationWithUnknownId() throws Exception
    {
        when(propertyManager.getString(eq("trustedapps.keys"), anyString())).thenReturn("someId\nsomeOtherId");
        service.deleteApplication("unknownId");

        verify(propertyManager).getString(eq("trustedapps.keys"), anyString());
        verifyNoMoreInteractions(propertyManager);
    }

    @Test
    public void deleteTrustedApplicationShouldDeleteProperties() throws Exception
    {
        when(propertyManager.getString(eq("trustedapps.keys"), anyString())).thenReturn("someId\nsomeOtherId");

        service.deleteApplication("someId");

        verify(propertyManager).getString(eq("trustedapps.keys"), anyString());
        verify(propertyManager).setProperty("trustedapps.keys", "someOtherId");
        verify(propertyManager).removeProperty("trustedapps.someId.public.key");
        verify(propertyManager).removeProperty("trustedapps.someId.timeout");
        verify(propertyManager).removeProperty("trustedapps.someId.ips");
        verify(propertyManager).removeProperty("trustedapps.someId.urls");
        verifyNoMoreInteractions(propertyManager);
    }

    @Test
    public void getCurrentApplicationNonExisting() throws Exception
    {
        final InternalCurrentApplication currentApplication = service.getCurrentApplication();

        assertThat(currentApplication, is(nullValue()));
    }

    @Test
    public void getCurrentApplication() throws Exception
    {
        when(propertyManager.getString(eq("trustedapps.currentapp.uid"), anyString())).thenReturn("someId");
        when(propertyManager.getString(eq("trustedapps.currentapp.public.key"), anyString())).thenReturn("publicKey");
        when(propertyManager.getString(eq("trustedapps.currentapp.private.key"), anyString())).thenReturn("privateKey");
        final InternalCurrentApplication currentApplication = service.getCurrentApplication();

        assertThat(currentApplication, is(notNullValue()));
        assertThat(currentApplication.getUid(), is("someId"));
        assertThat(currentApplication.getPrivateKey(), is("privateKey"));
        assertThat(currentApplication.getPublicKey(), is("publicKey"));
    }

    @Test
    public void storeCurrentApplicationShouldPersistProperties() throws Exception
    {
        final InternalCurrentApplication application = new InternalCurrentApplication("someId", "privateKey", "publicKey");

        service.storeCurrentApplication(application);

        verify(propertyManager).setProperty("trustedapps.currentapp.uid", "someId");
        verify(propertyManager).setProperty("trustedapps.currentapp.private.key", "privateKey");
        verify(propertyManager).setProperty("trustedapps.currentapp.public.key", "publicKey");
    }

    @Test
    public void iterableToCommaSeparatedStringShouldEncode() throws Exception
    {
        final ImmutableList<String> list = ImmutableList.of("someValue", "someValue\nwith\nnew lines", "another\nvalue\\with\\backslashes");

        final String result = PropertyBasedTrustedApplicationStore.iterableToCommaSeparatedString(list);

        assertThat(result, is("someValue\nsomeValue\\nwith\\nnew lines\nanother\\nvalue\\\\with\\\\backslashes"));
    }

    @Test
    public void decodeCommaSeparatedStringShouldDecode() throws Exception
    {
        final String encodedList = "someValue\nsomeValue\\nwith\\nnew lines\nanother\\nvalue\\\\with\\\\backslashes";

        final Iterable<String> result = PropertyBasedTrustedApplicationStore.decodeCommaSeparatedString(encodedList);

        assertThat(result, contains("someValue", "someValue\nwith\nnew lines", "another\nvalue\\with\\backslashes"));
    }

    @Test
    public void encodeThenDecodeShouldBeIdemPotent() throws Exception
    {
        final List<String> list = ImmutableList.of("someValue", "someValue\nwith\nnew lines", "another\nvalue\\with\\backslashes");

        final Iterable<String> result = PropertyBasedTrustedApplicationStore.decodeCommaSeparatedString(PropertyBasedTrustedApplicationStore.iterableToCommaSeparatedString(list));

        assertThat(result, contains(list.toArray()));
    }

    private void setPropsForTrustedApp(int index) throws NoSuchAlgorithmException, NoSuchProviderException, InvalidKeySpecException
    {
        final String id = "appId" + index;
        final PublicKey publicKey = mock(PublicKey.class);
        when(publicKey.getFormat()).thenReturn(PUBLIC_KEYS[index]); // Just for asserting the different keys
        when(propertyManager.getString(eq("trustedapps." + id + ".public.key"), anyString())).thenReturn(PUBLIC_KEYS_BASE64[index]);
        when(propertyManager.getString(eq("trustedapps." + id + ".timeout"), anyString())).thenReturn("1" + index);
        when(propertyManager.getString(eq("trustedapps." + id + ".ips"), anyString())).thenReturn("127.0.0." + index + "\n192.168.0." + index);
        when(propertyManager.getString(eq("trustedapps." + id + ".urls"), anyString())).thenReturn("http://url" + index + ".com\nhttp://anotherurl" + index + ".com");
        when(encryptionProvider.toPublicKey(PUBLIC_KEYS[index].getBytes(Charsets.UTF_8))).thenReturn(publicKey);
    }
}
