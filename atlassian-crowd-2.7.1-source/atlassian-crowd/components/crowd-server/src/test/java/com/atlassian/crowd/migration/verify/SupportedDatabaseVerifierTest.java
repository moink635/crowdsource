package com.atlassian.crowd.migration.verify;

import com.atlassian.crowd.util.SystemInfoHelperImpl;
import junit.framework.TestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SupportedDatabaseVerifierTest extends TestCase
{
    private SystemInfoHelperImpl systemInfoHelper;
    private DatabaseVerifier verifier;

    private static final String POSTGRESQL_HIBERNATE_DIALECT = "org.hibernate.dialect.PostgreSQLDialect";
    private static final String ORACLE10G_HIBERNATE_DIALECT = "org.hibernate.dialect.Oracle10gDialect";

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        systemInfoHelper = mock(SystemInfoHelperImpl.class);

    }

    public void testVerifySupportedDatabase() throws Exception
    {
        when(systemInfoHelper.getDatabaseHibernateDialect()).thenReturn(POSTGRESQL_HIBERNATE_DIALECT);

        verifier = new SupportedDatabaseVerifier(systemInfoHelper);
        verifier.verify();

        assertFalse(verifier.hasErrors());

    }

    public void testVerifyUnsupportedDatabase()
    {
        when(systemInfoHelper.getDatabaseHibernateDialect()).thenReturn(ORACLE10G_HIBERNATE_DIALECT);

        verifier = new SupportedDatabaseVerifier(systemInfoHelper);
        verifier.verify();

        assertTrue(verifier.hasErrors());
    }
}
