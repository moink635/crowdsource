package com.atlassian.crowd.migration.legacy;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.dao.membership.MembershipDAOHibernate;
import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.group.InternalGroupWithAttributes;
import com.atlassian.crowd.model.membership.InternalMembership;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchResultWithIdReferences;
import junit.framework.TestCase;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.SessionFactory;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import static com.atlassian.crowd.migration.legacy.GroupMapper.REMOTE_GROUP_XML_ROOT;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

public class GroupMapperTest extends TestCase
{
    private GroupMapper groupMapper;
    private DirectoryDAOHibernate directoryDAOHibernate;
    private GroupDAOHibernate groupDAO;
    private UserDAOHibernate userDAO;
    private MembershipDAOHibernate membershipDAOHibernate;
    private Document document;
    private HashMap<Long, Long> directoryIdOldToNew;
    private DirectoryImpl directory;
    private InternalGroup group;
    private static final String EXPECTED_GROUP_NAME = "Crowd-Users";

    protected void setUp() throws Exception
    {
        super.setUp();

        directoryDAOHibernate = mock(DirectoryDAOHibernate.class);
        groupDAO = mock(GroupDAOHibernate.class);
        userDAO = mock(UserDAOHibernate.class);
        membershipDAOHibernate = mock(MembershipDAOHibernate.class);

        // These legacy xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        BatchProcessor batchProcessor = mock(BatchProcessor.class);
        groupMapper = new GroupMapper(sessionFactory, batchProcessor, groupDAO, membershipDAOHibernate, directoryDAOHibernate);

        directoryIdOldToNew = new HashMap<Long, Long>();
        directoryIdOldToNew.put(6L, 1L);

        final InternalEntityTemplate directoryOne = new InternalEntityTemplate();
        directoryOne.setId(1L);
        directoryOne.setName("Directory One");

        directory = new DirectoryImpl(directoryOne);

        group = new InternalGroup(new GroupTemplate(EXPECTED_GROUP_NAME, directory.getId(), GroupType.GROUP), directory);

        // load xml file
        SAXReader reader = new SAXReader();
        final URL resource = ClassLoaderUtils.getResource(StringUtils.replace(this.getClass().getCanonicalName(), ".", File.separator) + ".xml", this.getClass());
        document = reader.read(resource);
    }

    protected void tearDown() throws Exception
    {
        verifyNoMoreInteractions(membershipDAOHibernate);
        verifyNoMoreInteractions(groupDAO);
        verifyNoMoreInteractions(directoryDAOHibernate);

        super.tearDown();
    }

    public void testGetGroupAndAttributesFromXml() throws Exception
    {
        when(directoryDAOHibernate.loadReference(1L)).thenReturn(directory);

        Element groupsElement = getGroupsElement();

        InternalGroupWithAttributes group = groupMapper.getGroupAndAttributesFromXml((Element) groupsElement.elementIterator().next(), directoryIdOldToNew);

        verify(directoryDAOHibernate).loadReference(1L);
        assertEquals(EXPECTED_GROUP_NAME, group.getName());
        assertEquals(1L, group.getDirectoryId());
        assertEquals(GroupType.GROUP, group.getType());
    }

    private Element getGroupsElement()
    {
        return (Element) document.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + REMOTE_GROUP_XML_ROOT);
    }

    public void testgetGroupAndAttributesFromXmlWithIncorrectOldDirectoryId()
    {

        Element groupsElement = getGroupsElement();

        HashMap<Long, Long> oldIdToNewId = new HashMap<Long, Long>();
        oldIdToNewId.put(99L, 1L);

        try
        {
            groupMapper.getGroupAndAttributesFromXml((Element) groupsElement.elementIterator().next(), oldIdToNewId);
            fail("Should have failed since we have an incorrect old directoryId");
        }
        catch (IllegalArgumentException e)
        {

        }
    }

    public void testImportMemberships() throws Exception
    {
        Element groupsElement = getGroupsElement();

        when(groupDAO.findByName(1L, EXPECTED_GROUP_NAME)).thenReturn(new InternalGroup(new GroupTemplate(EXPECTED_GROUP_NAME, directory.getId(), GroupType.GROUP), directory));
        UserTemplate template = new UserTemplate("james", directory.getId());
        template.setEmailAddress("blah@example.com");
        template.setFirstName("firstname");
        template.setLastName("lastname");
        template.setDisplayName("displayname");
        when(userDAO.findByName(1L, "james")).thenReturn(new InternalUser(template, directory, new PasswordCredential("password", true)));

        LegacyImportDataHolder importData = new LegacyImportDataHolder();
        importData.setOldToNewDirectoryIds(directoryIdOldToNew);

        InternalEntityTemplate directoryTemplate = new InternalEntityTemplate();
        directoryTemplate.setId(1L);
        directoryTemplate.setName("dir");
        DirectoryImpl directory = new DirectoryImpl(directoryTemplate);

        InternalEntityTemplate internalUserTemplate = new InternalEntityTemplate();
        internalUserTemplate.setName("james");
        internalUserTemplate.setId(1111L);
        UserTemplate userTemplate = new UserTemplate("james", 1L);
        userTemplate.setFirstName("first");
        userTemplate.setLastName("last");
        userTemplate.setDisplayName("display");
        userTemplate.setEmailAddress("e@mail.com");
        InternalUser user = new InternalUser(internalUserTemplate, directory, userTemplate, new PasswordCredential("password", true));

        InternalEntityTemplate internalGroupTemplate = new InternalEntityTemplate();
        internalGroupTemplate.setName(EXPECTED_GROUP_NAME);
        internalGroupTemplate.setId(2222L);
        GroupTemplate groupTemplate = new GroupTemplate(EXPECTED_GROUP_NAME, 1L, GroupType.GROUP);
        InternalGroup group = new InternalGroup(internalGroupTemplate, directory, groupTemplate);

        BatchResultWithIdReferences<User> userImportResults = new BatchResultWithIdReferences<User>(1);
        userImportResults.addIdReference(user);
        importData.setUserImportResults(userImportResults);

        BatchResultWithIdReferences<Group> groupImportResults = new BatchResultWithIdReferences<Group>(1);
        groupImportResults.addIdReference(group);

        Set<InternalMembership> memberships = groupMapper.getMemberships((Element) groupsElement.elementIterator().next(), importData, groupImportResults);

        assertEquals(1, memberships.size());
        InternalMembership memebership = new ArrayList<InternalMembership>(memberships).get(0);
        assertNull(memebership.getId());
        assertEquals(memebership.getParentName(), EXPECTED_GROUP_NAME);
        assertEquals(memebership.getChildName(), "james");
        assertEquals(memebership.getParentId(), new Long(2222));
        assertEquals(memebership.getChildId(), new Long(1111));
        assertEquals(memebership.getGroupType(), GroupType.GROUP);
        assertEquals(memebership.getMembershipType(), MembershipType.GROUP_USER);

        verify(directoryDAOHibernate).loadReference(1L);
    }
}
