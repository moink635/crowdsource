package com.atlassian.crowd.importer.importers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.importers.CrowdifiedConfluenceImporter.CrowdifiedConfluenceGroupMapper;
import com.atlassian.crowd.importer.importers.CrowdifiedConfluenceImporter.CrowdifiedConfluenceUserMapper;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.RowMapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CrowdifiedConfluenceImporterTest
{
    private RowMapper userMapperWithPassword;
    private RowMapper userMapperNoPassword;
    private RowMapper groupMapper;

    @Before
    public void setUp() throws Exception
    {
        Configuration confWithImportPasswd = new Configuration(new Long(1), "conf", Boolean.TRUE, Boolean.FALSE);
        Configuration confNoImportPasswd = new Configuration(new Long(1), "conf", Boolean.FALSE, Boolean.FALSE);

        userMapperWithPassword = new CrowdifiedConfluenceUserMapper(confWithImportPasswd);
        userMapperNoPassword = new CrowdifiedConfluenceUserMapper(confNoImportPasswd);
        groupMapper = new CrowdifiedConfluenceGroupMapper(Long.valueOf(1));
    }

    @Test
    public void testMapActiveGroup() throws SQLException
    {
        Group remoteGroup = (Group) groupMapper.mapRow(mockGroupResultSet("group1", "description of group1", "T"), 0);

        assertEquals("group1", remoteGroup.getName());
        assertEquals("description of group1", remoteGroup.getDescription());
        assertEquals(GroupType.GROUP, remoteGroup.getType());
        assertTrue(remoteGroup.isActive());
    }

    @Test
    public void testMapInactiveGroup() throws SQLException
    {
        Group remoteGroup = (Group) groupMapper.mapRow(mockGroupResultSet("group2", "description of group2", "F"), 0);

        assertEquals("group2", remoteGroup.getName());
        assertEquals("description of group2", remoteGroup.getDescription());
        assertEquals(GroupType.GROUP, remoteGroup.getType());
        assertFalse(remoteGroup.isActive());
    }

    @Test
    public void testMappingPrincipalWithNoPassword() throws SQLException
    {
        ResultSet resultSet = mockUserResultSet("user1", "user1@gmail.com", "first1", "last1", "first1 last1", "passwd", "T");
        UserTemplateWithCredentialAndAttributes user = (UserTemplateWithCredentialAndAttributes) userMapperNoPassword.mapRow(resultSet, 0);

        assertNotNull(user);
        assertEquals("user1", user.getName());
        assertTrue(user.isActive());
        assertEquals("user1@gmail.com", user.getEmailAddress());
        assertEquals("first1", user.getFirstName());
        assertEquals("last1", user.getLastName());
        assertEquals("first1 last1", user.getDisplayName());
        PasswordCredential credential = user.getCredential();
        assertNotNull(credential);
        assertNotNull(credential.getCredential());
        assertNotSame("passwd", credential.getCredential());
    }

    @Test
    public void testMappingPrincipalWithPassword() throws SQLException
    {
        ResultSet resultSet = mockUserResultSet("user2", "user2@gmail.com", "first2", "last2", "first2 last2", "passwd", "F");
        UserTemplateWithCredentialAndAttributes user = (UserTemplateWithCredentialAndAttributes) userMapperWithPassword.mapRow(resultSet, 0);

        assertNotNull(user);
        assertEquals("user2", user.getName());
        assertFalse(user.isActive());
        assertEquals("user2@gmail.com", user.getEmailAddress());
        assertEquals("first2", user.getFirstName());
        assertEquals("last2", user.getLastName());
        assertEquals("first2 last2", user.getDisplayName());
        PasswordCredential credential = user.getCredential();
        assertNotNull(credential);
        assertNotNull(credential.getCredential());
        assertEquals("passwd", credential.getCredential());
    }

    private ResultSet mockGroupResultSet(String groupname, String description, String active) throws SQLException
    {
        ResultSet mockResultSet = mock(ResultSet.class);
        when(mockResultSet.getString("group_name")).thenReturn(groupname);
        when(mockResultSet.getString("description")).thenReturn(description);
        when(mockResultSet.getString("active")).thenReturn(active);
        return mockResultSet;
    }

    private ResultSet mockUserResultSet(String username, String emailaddress, String firstname, String lastname,
                                        String fullname, String password, String active) throws SQLException
    {
        ResultSet mockResultSet = mock(ResultSet.class);
        when(mockResultSet.getString("user_name")).thenReturn(username);
        when(mockResultSet.getString("email_address")).thenReturn(emailaddress);
        when(mockResultSet.getString("first_name")).thenReturn(firstname);
        when(mockResultSet.getString("last_name")).thenReturn(lastname);
        when(mockResultSet.getString("display_name")).thenReturn(fullname);
        when(mockResultSet.getString("credential")).thenReturn(password);
        when(mockResultSet.getString("active")).thenReturn(active);

        return mockResultSet;
    }
}
