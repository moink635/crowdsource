package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.config.ConfigurationException;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.license.SIDManager;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

/**
 * UpgradeTask001 Tester.
 */
public class TestUpgradeTask001 extends MockObjectTestCase
{
    private UpgradeTask001 upgradeTask = null;

    Mock sidManager = null;
    Mock bootstrapManager = null;
    private static final String SERVER_ID = "THRN-ENCF-WTFE";

    public void setUp() throws Exception
    {
        super.setUp();

        sidManager = new Mock(SIDManager.class);
        bootstrapManager = new Mock(CrowdBootstrapManager.class);

        upgradeTask = new UpgradeTask001();
        upgradeTask.setSidManager((SIDManager) sidManager.proxy());
        upgradeTask.setBootstrapManager((CrowdBootstrapManager) bootstrapManager.proxy());
    }

    public void tearDown() throws Exception
    {
        super.tearDown();
    }

    public void testDoUpgradeWhereSIDExists() throws Exception
    {
        bootstrapManager.expects(once()).method("getServerID").withNoArguments().will(returnValue(SERVER_ID));

        upgradeTask.doUpgrade();

        assertTrue(upgradeTask.getErrors().isEmpty());
    }

    public void testDoUpgradeWhereSIDDoesNotExist() throws Exception
    {
        bootstrapManager.expects(once()).method("getServerID").withNoArguments().will(returnValue(null));

        sidManager.expects(once()).method("generateSID").withNoArguments().will(returnValue(SERVER_ID));

        bootstrapManager.expects(once()).method("setServerID").with(eq(SERVER_ID));

        upgradeTask.doUpgrade();

        assertTrue(upgradeTask.getErrors().isEmpty());
    }

    public void testDoUpgradeWhereSIDFailsToBeSet() throws Exception
    {
        bootstrapManager.expects(once()).method("getServerID").withNoArguments().will(returnValue(null));

        sidManager.expects(once()).method("generateSID").withNoArguments().will(returnValue(SERVER_ID));

        bootstrapManager.expects(once()).method("setServerID").with(eq(SERVER_ID)).will(throwException(new ConfigurationException("Something bad happened")));

        upgradeTask.doUpgrade();

        assertFalse(upgradeTask.getErrors().isEmpty());
    }

    public void testUpgradeDescription()
    {
        assertNotNull(upgradeTask.getShortDescription());
    }
}