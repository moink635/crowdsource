package com.atlassian.crowd.manager.property;

import com.atlassian.crowd.dao.property.PropertyDAO;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.property.Property;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class MockPropertyDAO implements PropertyDAO
{

    private List<Property> properties = new ArrayList<Property>();

    public Property find(final String key, final String name) throws ObjectNotFoundException
    {
        for(Property p : properties)
        {
            if(p.getKey().equals(key) && p.getName().equals(name))
            {
                return p;
            }
        }
        
        return null;
    }

    public List<Property> findAll(final String key)
    {
        List<Property> result = new ArrayList<Property>();
        
        for(Property p : properties)
        {
            if(p.getKey().equals(key))
            {
                result.add(p);
            }
        }
        
        return result;
    }

    public Property add(final Property p)
    {
        properties.add(p);
        return p;
    }

    public Property update(final Property property)
    {
        final String key = property.getKey();
        final String name = property.getName();
        try
        {
            if (find(key, name) != null)
            {
                remove(key, name);
            }
            else
            {
                add(property);
            }
        }
        catch (ObjectNotFoundException e)
        {
            throw new RuntimeException(e);
        }
        return property;
    }

    public void remove(final String key, final String name)
    {
        for (Iterator<Property> iterator = properties.iterator(); iterator.hasNext();)
        {
            final Property p = iterator.next();
            if(p.getKey().equals(key) && p.getName().equals(name))
            {
                iterator.remove();
                return;
            }
        }
        throw new IllegalStateException("element was not in collection");
    }

    public List<Property> findAll()
    {
        return properties;
    }
}
