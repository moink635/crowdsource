package com.atlassian.crowd.file;

import com.atlassian.crowd.dao.RefreshableDao;

import com.google.common.collect.ImmutableSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DaoRefresherTest
{
    private DaoRefresher daoRefresher;

    @Mock private RefreshableDao refreshableDao;

    @Before
    public void setUp() throws Exception
    {
        daoRefresher = new DaoRefresher();
        daoRefresher.setRefreshableDaos(ImmutableSet.of(refreshableDao));
    }

    @Test
    public void testExecuteInternal() throws Exception
    {
        daoRefresher.executeInternal(null);
        verify(refreshableDao).refresh();
    }
}
