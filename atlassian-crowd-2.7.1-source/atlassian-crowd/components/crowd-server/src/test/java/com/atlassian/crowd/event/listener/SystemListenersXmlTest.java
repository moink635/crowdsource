package com.atlassian.crowd.event.listener;

import java.net.URL;

import javax.xml.parsers.DocumentBuilderFactory;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import static org.junit.Assert.assertNotNull;

public class SystemListenersXmlTest
{
    @Test
    public void ensureAllListedListenerClassesAreAvailable() throws Exception
    {
        URL res = getClass().getResource("/system-listeners.xml");
        assertNotNull(res);
        
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(res.toString()));
        
        NodeList el = doc.getElementsByTagName("listener");
        
        for (int i = 0; i < el.getLength(); i++)
        {
            String className = ((Element) el.item(i)).getAttribute("class");
            Class.forName(className);
        }
    }
}
