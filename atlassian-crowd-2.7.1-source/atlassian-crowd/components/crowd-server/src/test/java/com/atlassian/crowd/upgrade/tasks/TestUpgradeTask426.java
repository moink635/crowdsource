package com.atlassian.crowd.upgrade.tasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.User;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import junit.framework.TestCase;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUpgradeTask426 extends TestCase
{
    private UpgradeTask426 task;
    private DirectoryDao directoryDao;
    private UserDAOHibernate userDao;
    private SessionFactory sessionFactory;

    @Override
    protected void setUp() throws Exception
    {
        task = new UpgradeTask426();

        directoryDao = mock(DirectoryDao.class);
        task.setDirectoryDao(directoryDao);

        userDao = mock(UserDAOHibernate.class);
        task.setUserDao(userDao);

        sessionFactory = mock(SessionFactory.class);
        task.setSessionFactory(sessionFactory);
    }

    public void testDoUpgrade() throws Exception
    {
        DirectoryImpl directory = new DirectoryImpl("Directory 1", DirectoryType.INTERNAL, Directory.class.getCanonicalName())
        {
            public Long getId()
            {
                return -1L;
            }
        };
        when(directoryDao.search(any(EntityQuery.class))).thenReturn(Arrays.<Directory>asList(directory));

        User user = new UserTemplate("user", -1L);
        List<User> users = new ArrayList(40);
        for (int i = 0; i < 40; ++i)
        {
            users.add(user);
        }

        when(userDao.search(eq(-1L), any(EntityQuery.class))).thenReturn(users);

        Session currentSession = mock(Session.class);
        when(sessionFactory.getCurrentSession()).thenReturn(currentSession);

        task.doUpgrade();

        verify(userDao, times(40)).update(any(UserTemplate.class));
        verify(currentSession, times(3)).flush();
        verify(currentSession, times(3)).clear();
    }
}
