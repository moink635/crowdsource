package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.property.PropertyManager;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

/**
 * UpgradeTask211 Tester.
 */
public class TestUpgradeTask211 extends MockObjectTestCase
{
    UpgradeTask upgradeTask = null;
    protected Mock mockPropertyManager;

    public void setUp() throws Exception
    {
        super.setUp();

        mockPropertyManager = new Mock(PropertyManager.class);

        upgradeTask = new UpgradeTask211((PropertyManager) mockPropertyManager.proxy());
    }

    public void testDoUpgradeWithResourceSet() throws Exception
    {
        mockPropertyManager.expects(once()).method("getCurrentLicenseResourceTotal").will(returnValue(10));

        mockPropertyManager.expects(never()).method("setCurrentLicenseResourceTotal");

        upgradeTask.doUpgrade();
    }

    public void testDoUpgradeWithNoResourceSet() throws Exception
    {
        mockPropertyManager.expects(once()).method("getCurrentLicenseResourceTotal").will(returnValue(0));

        mockPropertyManager.expects(once()).method("setCurrentLicenseResourceTotal").with(eq(UpgradeTask211.DEFAULT_TOTAL));

        upgradeTask.doUpgrade();
    }

    public void tearDown() throws Exception
    {
        super.tearDown();
    }
}
