package com.atlassian.crowd.migration;

import com.atlassian.crowd.dao.alias.AliasDAOHibernate;
import com.atlassian.crowd.dao.application.ApplicationDAOHibernate;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.alias.Alias;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AliasMapperTest extends BaseMapperTest
{
    private AliasMapper mapper;
    private ApplicationDAOHibernate applicationDAO;
    private AliasDAOHibernate aliasDAO;

    private Element aliasRoot;

    protected void setUp() throws Exception
    {
        super.setUp();

        applicationDAO = mock(ApplicationDAOHibernate.class);
        aliasDAO = mock(AliasDAOHibernate.class);

        // These xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        BatchProcessor batchProcessor = mock(BatchProcessor.class);
        mapper = new AliasMapper(sessionFactory, batchProcessor, aliasDAO, applicationDAO);


        aliasRoot = (Element) document.getRootElement().selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + AliasMapper.ALIAS_XML_ROOT);
    }

    public void testImportAndExportFromXML() throws Exception
    {
        InternalEntityTemplate template = new InternalEntityTemplate();
        template.setId(2L);
        template.setName("");
        ApplicationImpl application = new ApplicationImpl(template);

        when(applicationDAO.loadReference(2L)).thenReturn(application);

        Alias alias = mapper.getAliasFromXml((Element) this.aliasRoot.elementIterator().next());

        assertNotNull(alias);
        assertEquals(new Long(1), alias.getId());
        assertSame(application, alias.getApplication());
        assertEquals("robert", alias.getName());
        assertEquals("Bob", alias.getAlias());
        assertEquals("bob", alias.getLowerAlias());

        // test export
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement(XmlMigrationManagerImpl.XML_ROOT);
        Element applicationRoot = root.addElement(AliasMapper.ALIAS_XML_ROOT);

        mapper.addAliasToXml(alias, applicationRoot);

        String export = exportDocumentToString(document);

        assertEquals(getTestExportXmlString(), export);
    }
}
