package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.upgrade.util.UpgradeUtilityDAOHibernate;
import junit.framework.TestCase;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class TestUpgradeTask427 extends TestCase
{
    private UpgradeTask427 task;
    private UpgradeUtilityDAOHibernate mockUpgradeUtilityDao;

    @Override
    protected void setUp() throws Exception
    {
        task = new UpgradeTask427();
        mockUpgradeUtilityDao = mock(UpgradeUtilityDAOHibernate.class);
        task.setUpgradeUtilityDao(mockUpgradeUtilityDao);
    }

    public void testUpdateGroupLocalValue() throws Exception
    {
        // Just tests that the methods are actually called...
        task.doUpgrade();

        verify(mockUpgradeUtilityDao, times(1)).executeUpdate(UpgradeTask427.UPDATE_LOCAL_TO_FALSE);

    }
}
