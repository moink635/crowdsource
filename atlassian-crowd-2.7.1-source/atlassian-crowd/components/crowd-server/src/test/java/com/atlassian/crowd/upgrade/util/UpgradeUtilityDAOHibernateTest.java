package com.atlassian.crowd.upgrade.util;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpgradeUtilityDAOHibernateTest
{
    private UpgradeUtilityDAOHibernate dao;

    @Mock SessionFactory sessionFactory;
    @Mock Session session;

    @Before
    public void createObjectUnderTest()
    {
        dao = new UpgradeUtilityDAOHibernate();
        dao.setSessionFactory(sessionFactory);
    }

    @Test
    public void testExecuteUpdate() throws Exception
    {
        when(sessionFactory.getCurrentSession()).thenReturn(session);

        Query query = mock(Query.class);
        when(session.createQuery("query")).thenReturn(query);
        when(query.executeUpdate()).thenReturn(1);

        assertEquals(1, dao.executeUpdate("query"));
    }

    @Test
    public void testExecuteBulkUpdate() throws Exception
    {
        when(sessionFactory.getCurrentSession()).thenReturn(session);

        Query query = mock(Query.class);
        when(session.createQuery("query with parameter ?1")).thenReturn(query);
        when(query.setParameter("1", "parameter")).thenReturn(query);
        when(query.executeUpdate()).thenReturn(1);

        assertEquals(1, dao.executeBulkUpdate("query with parameter ?1", "parameter"));

        verify(session).flush();
        verify(query).setParameter("1", "parameter");
    }
}
