package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.directory.OpenLDAP;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUpgradeTask322
{
    private UpgradeTask322 task;
    @Mock private DirectoryManager mockDirectoryManager;

    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        task = new UpgradeTask322();

        task.setDirectoryManager(mockDirectoryManager);
    }

    @Test
    public void testAllInternalDirectoriesSoNoUpgrade() throws Exception
    {
        DirectoryImpl directory1 = new DirectoryImpl();
        directory1.setType(DirectoryType.INTERNAL);

        DirectoryImpl directory2 = new DirectoryImpl();
        directory2.setType(DirectoryType.INTERNAL);

        when(mockDirectoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(Arrays.<Directory>asList(directory1, directory2));
        // should do no updates
        task.doUpgrade();
        verify(mockDirectoryManager, never()).updateDirectory(any(Directory.class));
    }

    @Test
    public void testUpgradeNonInternalDirectories() throws Exception
    {
        DirectoryImpl directory1 = new DirectoryImpl("1", DirectoryType.UNKNOWN, OpenLDAP.class.getCanonicalName());

        DirectoryImpl directory2 = new DirectoryImpl("2", DirectoryType.INTERNAL, InternalDirectory.class.getCanonicalName());

        DirectoryImpl directory3 = new DirectoryImpl("3", DirectoryType.CONNECTOR, OpenLDAP.class.getCanonicalName());

        DirectoryImpl directory4 = new DirectoryImpl("4", DirectoryType.DELEGATING, DelegatedAuthenticationDirectory.class.getCanonicalName());

        DirectoryImpl directory5 = new DirectoryImpl("5", DirectoryType.CUSTOM, OpenLDAP.class.getCanonicalName());

        when(mockDirectoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(Arrays.<Directory>asList(directory1, directory2, directory3, directory4, directory5));

        task.doUpgrade();

        verify(mockDirectoryManager).updateDirectory(directory1);
        verify(mockDirectoryManager, never()).updateDirectory(directory2);
        verify(mockDirectoryManager).updateDirectory(directory3);
        verify(mockDirectoryManager).updateDirectory(directory4);
        verify(mockDirectoryManager).updateDirectory(directory5);

        verify(mockDirectoryManager, times(4)).updateDirectory(argThat(DirectoryAttributesMatcher.contains(LDAPPropertiesMapper.USER_DISPLAYNAME_KEY)));
    }
}

