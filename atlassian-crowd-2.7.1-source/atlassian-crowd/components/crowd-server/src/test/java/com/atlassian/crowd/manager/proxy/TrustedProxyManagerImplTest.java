package com.atlassian.crowd.manager.proxy;

import java.util.ArrayList;
import java.util.Set;

import com.atlassian.crowd.manager.cache.CacheManager;
import com.atlassian.crowd.manager.cache.NotInCacheException;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;

import com.google.common.collect.Sets;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Matchers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class TrustedProxyManagerImplTest
{
    private TrustedProxyManager tpm;
    private PropertyManager propertyManager;
    private CacheManager cacheManager;

    private static final String REMOTE_ADDRESS_SUBNET_1 = "192.168.0.0/21";

    private static final String REMOTE_ADDRESS_1 = "192.168.3.2";
    private static final String REMOTE_ADDRESS_2 = "192.168.5.6";
    private static final String REMOTE_ADDRESS_3 = "192.168.232.193";

    @Before
    public void setUp() throws Exception
    {
        propertyManager = mock(PropertyManager.class);
        cacheManager = mock(CacheManager.class);
        tpm = new TrustedProxyManagerImpl(propertyManager, cacheManager);
    }

    @Test
    public void testIsTrusted_NoProxies_Direct() throws Exception
    {
        // Nothing in cache or property
        when(cacheManager.get(anyString(), anyString())).thenThrow(new NotInCacheException());
        when(propertyManager.getTrustedProxyServers()).thenThrow(new PropertyManagerException());

        assertFalse(tpm.isTrusted(REMOTE_ADDRESS_1));

        verify(cacheManager).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), eq(REMOTE_ADDRESS_1));
        verify(cacheManager).put(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), eq(REMOTE_ADDRESS_1), eq(Boolean.FALSE));

        verify(propertyManager).getTrustedProxyServers();
    }

    @Test
    public void testIsTrusted_NoProxies_CachedSecondTime() throws Exception
    {
        when(cacheManager.get(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1)).thenThrow(new NotInCacheException()).thenReturn(Boolean.FALSE);
        when(propertyManager.getTrustedProxyServers()).thenThrow(new PropertyManagerException()).thenReturn("");

        assertFalse(tpm.isTrusted(REMOTE_ADDRESS_1));
        assertFalse(tpm.isTrusted(REMOTE_ADDRESS_1));

        verify(cacheManager, times(2)).get(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1);
        verify(cacheManager).put(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1, Boolean.FALSE);

        verify(propertyManager).getTrustedProxyServers();
    }

    @Test
    public void testIsTrusted_NotTrusted_Direct() throws Exception
    {
        // Not in cache, but pretend in properties
        when(cacheManager.get(anyString(), anyString())).thenThrow(new NotInCacheException());
        when(propertyManager.getTrustedProxyServers()).thenReturn(REMOTE_ADDRESS_2 + "," + REMOTE_ADDRESS_3);

        assertFalse(tpm.isTrusted(REMOTE_ADDRESS_1));

        verify(cacheManager).get(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1);
        verify(cacheManager).put(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1, Boolean.FALSE);

        verify(propertyManager).getTrustedProxyServers();
    }

    @Test
    public void testIsTrusted_NotTrusted_CachedSecondTime() throws Exception
    {
        when(cacheManager.get(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1)).thenThrow(new NotInCacheException()).thenReturn(Boolean.FALSE);
        when(propertyManager.getTrustedProxyServers()).thenReturn(REMOTE_ADDRESS_2 + "," + REMOTE_ADDRESS_3);

        assertFalse(tpm.isTrusted(REMOTE_ADDRESS_1));
        assertFalse(tpm.isTrusted(REMOTE_ADDRESS_1));

        verify(cacheManager, times(2)).get(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1);
        verify(cacheManager).put(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1, Boolean.FALSE);

        verify(propertyManager).getTrustedProxyServers();
    }

    @Test
    public void testIsTrusted_Trusted_Direct() throws Exception
    {
        when(cacheManager.get(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1)).thenThrow(new NotInCacheException()).thenReturn(Boolean.TRUE);
        when(propertyManager.getTrustedProxyServers()).thenReturn(REMOTE_ADDRESS_2 + "," + REMOTE_ADDRESS_1 + "," + REMOTE_ADDRESS_3);

        assertTrue(tpm.isTrusted(REMOTE_ADDRESS_1));

        verify(cacheManager).get(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1);
        verify(cacheManager).put(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1, Boolean.TRUE);

        verify(propertyManager).getTrustedProxyServers();
    }

    @Test
    public void testIsTrusted_Trusted_Subnet() throws Exception
    {
        when(cacheManager.get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString())).thenThrow(new NotInCacheException());
        when(propertyManager.getTrustedProxyServers()).thenReturn(REMOTE_ADDRESS_SUBNET_1);

        assertTrue(tpm.isTrusted(REMOTE_ADDRESS_1));
        assertTrue(tpm.isTrusted(REMOTE_ADDRESS_2));
        assertFalse(tpm.isTrusted(REMOTE_ADDRESS_3)); // this address does not fall under the sub-net

        verify(cacheManager, times(3)).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
        // each time a new address is tested, it is added to the requested proxies cache
        verify(cacheManager, times(1)).put(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1, Boolean.TRUE);
        verify(cacheManager, times(1)).put(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_2, Boolean.TRUE);
        verify(cacheManager, times(1)).put(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_3, Boolean.FALSE);

        verify(propertyManager, times(3)).getTrustedProxyServers();
    }

    @Test
    public void testIsTrusted_Trusted_SubnetAndSpecific() throws Exception
    {
        when(cacheManager.get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString())).thenThrow(new NotInCacheException());
        when(propertyManager.getTrustedProxyServers()).thenReturn(REMOTE_ADDRESS_SUBNET_1 + "," + REMOTE_ADDRESS_3);

        assertTrue(tpm.isTrusted(REMOTE_ADDRESS_1));
        assertTrue(tpm.isTrusted(REMOTE_ADDRESS_2));
        assertTrue(tpm.isTrusted(REMOTE_ADDRESS_3)); // this address does not fall under the sub-net

        verify(cacheManager, times(3)).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
        // each time a new address is tested, it is added to the requested proxies cache
        verify(cacheManager, times(1)).put(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1, Boolean.TRUE);
        verify(cacheManager, times(1)).put(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_2, Boolean.TRUE);
        verify(cacheManager, times(1)).put(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_3, Boolean.TRUE);

        verify(propertyManager, times(3)).getTrustedProxyServers();
    }

    @Test
    public void testIsTrusted_Trusted_CachedSecondTime() throws Exception
    {
        when(cacheManager.get(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1)).thenThrow(new NotInCacheException()).thenReturn(Boolean.TRUE);
        when(propertyManager.getTrustedProxyServers()).thenReturn(REMOTE_ADDRESS_1);

        assertTrue(tpm.isTrusted(REMOTE_ADDRESS_1));
        assertTrue(tpm.isTrusted(REMOTE_ADDRESS_1));

        verify(cacheManager, times(2)).get(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1);
        verify(cacheManager, times(1)).put(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE, REMOTE_ADDRESS_1, Boolean.TRUE);

        verify(propertyManager, times(1)).getTrustedProxyServers();
    }

    @Test
    public void testGetList_NoProxies_Direct() throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenThrow(new PropertyManagerException());

        Set<String> proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertTrue(proxies.isEmpty());

        verify(propertyManager, times(1)).getTrustedProxyServers();

        verify(cacheManager, never()).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
    }

    @Test
    public void testGetList_NoProxies_CachedSecondTime() throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenThrow(new PropertyManagerException());

        Set<String> proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(0, proxies.size());

        proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(0, proxies.size());

        verify(propertyManager, times(2)).getTrustedProxyServers();

        verify(cacheManager, never()).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
    }

    @Test
    public void testGetList_Proxies_Direct() throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenReturn(REMOTE_ADDRESS_2 + "," + REMOTE_ADDRESS_3);

        Set<String> proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(2, proxies.size());

        assertTrue(proxies.contains(REMOTE_ADDRESS_2));
        assertTrue(proxies.contains(REMOTE_ADDRESS_3));
        assertFalse(proxies.contains(REMOTE_ADDRESS_1));

        verify(propertyManager, times(1)).getTrustedProxyServers();

        verify(cacheManager, never()).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
    }

    @Test
    public void testGetList_Proxies_CachedSecondTime() throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenReturn(REMOTE_ADDRESS_2 + "," + REMOTE_ADDRESS_3);

        Set<String> proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(2, proxies.size());

        assertTrue(proxies.contains(REMOTE_ADDRESS_2));
        assertTrue(proxies.contains(REMOTE_ADDRESS_3));
        assertFalse(proxies.contains(REMOTE_ADDRESS_1));

        proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(2, proxies.size());

        assertTrue(proxies.contains(REMOTE_ADDRESS_2));
        assertTrue(proxies.contains(REMOTE_ADDRESS_3));
        assertFalse(proxies.contains(REMOTE_ADDRESS_1));

        verify(propertyManager, times(2)).getTrustedProxyServers();

        verify(cacheManager, never()).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
    }

    @Test
    public void testAdd_NoExistingProxies_CachesCorrectlyPopulated() throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenThrow(new PropertyManagerException()).thenReturn(REMOTE_ADDRESS_1);

        assertTrue(tpm.addAddress(REMOTE_ADDRESS_1));

        Set<String> proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(1, proxies.size());
        assertTrue(proxies.contains(REMOTE_ADDRESS_1));

        proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(1, proxies.size());
        assertTrue(proxies.contains(REMOTE_ADDRESS_1));

        // modified trusted proxy list, make sure caches are nuked
        verify(cacheManager, times(1)).removeAll(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE);
        verify(propertyManager, times(1)).setTrustedProxyServers(REMOTE_ADDRESS_1);

        verify(propertyManager, times(3)).getTrustedProxyServers();

        verify(cacheManager, never()).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
    }

    @Test
    public void testAdd_BlankAddress() throws Exception
    {
        assertFalse(tpm.addAddress(""));
        assertFalse(tpm.addAddress(null));
        assertFalse(tpm.addAddress("                             "));

        // make sure adding a blank address yields no effect at all, ever.
        verify(cacheManager, never()).removeAll(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE);
        verify(propertyManager, never()).setTrustedProxyServers(anyString());
    }

    @Test
    public void testAdd_AlreadyAdded() throws Exception
    {
        // assume already in cache (testing for adding duplicate)
        when(propertyManager.getTrustedProxyServers()).thenReturn(REMOTE_ADDRESS_1);

        assertFalse(tpm.addAddress(REMOTE_ADDRESS_1));

        // Make sure caches were not changed
        verify(cacheManager, never()).removeAll(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE);
        verify(propertyManager, never()).setTrustedProxyServers(anyString());
    }

    @Test
    public void testAdd_OtherProxyExists_Success() throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenReturn(REMOTE_ADDRESS_2).thenReturn(REMOTE_ADDRESS_2 + "," + REMOTE_ADDRESS_1);

        Set<String> proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(1, proxies.size());
        assertFalse(proxies.contains(REMOTE_ADDRESS_1));
        assertTrue(proxies.contains(REMOTE_ADDRESS_2));
        assertFalse(proxies.contains(REMOTE_ADDRESS_3));

        assertFalse(tpm.addAddress(REMOTE_ADDRESS_1));

        proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(2, proxies.size());
        assertTrue(proxies.contains(REMOTE_ADDRESS_2));
        assertFalse(proxies.contains(REMOTE_ADDRESS_3));

        verify(propertyManager, times(3)).getTrustedProxyServers();

        verify(cacheManager, never()).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
    }

    @Test
    public void testAdd_Multiple() throws Exception
    {
        String multipleProxies = REMOTE_ADDRESS_1 + "," + REMOTE_ADDRESS_2 + "," + REMOTE_ADDRESS_3;

        when(propertyManager.getTrustedProxyServers()).thenThrow(new PropertyManagerException()).thenReturn(multipleProxies);

        assertTrue(tpm.addAddress(multipleProxies));

        Set<String> proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(3, proxies.size());
        assertTrue(proxies.contains(REMOTE_ADDRESS_1));
        assertTrue(proxies.contains(REMOTE_ADDRESS_2));
        assertTrue(proxies.contains(REMOTE_ADDRESS_3));

        proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(3, proxies.size());
        assertTrue(proxies.contains(REMOTE_ADDRESS_1));
        assertTrue(proxies.contains(REMOTE_ADDRESS_2));
        assertTrue(proxies.contains(REMOTE_ADDRESS_3));

        // modified trusted proxy list, make sure caches are nuked
        verify(cacheManager, times(1)).removeAll(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE);
        // Can only check that PropertyManager#setTrustedProxyServers is called - can't determine the order of the proxies
        verify(propertyManager, times(1)).setTrustedProxyServers(Matchers.anyString());

        verify(propertyManager, times(3)).getTrustedProxyServers();

        verify(cacheManager, never()).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
    }

        @Test
    public void testAdd_MultipleDuplicate() throws Exception
    {
        String multipleProxies = REMOTE_ADDRESS_1 + "," + REMOTE_ADDRESS_1 + "," + REMOTE_ADDRESS_3;

        when(propertyManager.getTrustedProxyServers()).thenThrow(new PropertyManagerException()).thenReturn(REMOTE_ADDRESS_1 + "," + REMOTE_ADDRESS_3);

        assertTrue(tpm.addAddress(multipleProxies));

        Set<String> proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(2, proxies.size());
        assertTrue(proxies.contains(REMOTE_ADDRESS_1));
        assertTrue(proxies.contains(REMOTE_ADDRESS_3));

        proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertEquals(2, proxies.size());
        assertTrue(proxies.contains(REMOTE_ADDRESS_1));
        assertTrue(proxies.contains(REMOTE_ADDRESS_3));

        // modified trusted proxy list, make sure caches are nuked
        verify(cacheManager, times(1)).removeAll(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE);
        // Can only check that PropertyManager#setTrustedProxyServers is called - can't determine the order of the proxies
        verify(propertyManager, times(1)).setTrustedProxyServers(Matchers.anyString());

        verify(propertyManager, times(3)).getTrustedProxyServers();

        verify(cacheManager, never()).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
    }

    @Test
    public void testAdd_Blank() throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenThrow(new PropertyManagerException()).thenReturn("");

        assertFalse(tpm.addAddress(" ,,"));

        Set<String> proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertTrue(proxies.isEmpty());


        proxies = tpm.getAddresses();
        assertNotNull(proxies);
        assertTrue(proxies.isEmpty());

        // modified trusted proxy list, make sure caches are nuked
        verify(cacheManager, never()).removeAll(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE);
        verify(propertyManager, never()).setTrustedProxyServers(Matchers.anyString());

        verify(propertyManager, times(3)).getTrustedProxyServers();

        verify(cacheManager, never()).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
    }

    @Test
    public void testRemove_NoProxies() throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenThrow(new PropertyManagerException());

        tpm.removeAddress(REMOTE_ADDRESS_1);

        // Check proxy list is empty
        Set<String> proxies = tpm.getAddresses();
        assertTrue(proxies.isEmpty());

        verify(propertyManager, times(2)).getTrustedProxyServers();

        verify(cacheManager, never()).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
    }

    @Test
    public void testRemove_ExistingProxies() throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenThrow(new PropertyManagerException()).thenReturn(REMOTE_ADDRESS_1).thenReturn("");

        // add then remove
        assertTrue(tpm.addAddress(REMOTE_ADDRESS_1));
        tpm.removeAddress(REMOTE_ADDRESS_1);

        // Check proxy list is empty
        Set<String> proxies = tpm.getAddresses();
        assertTrue(proxies.isEmpty());

        // propertyManager is called 3 times since add/remove nuke the caches
        verify(propertyManager, times(3)).getTrustedProxyServers();

        // modified trusted proxy list, make sure caches are nuked (once each for add, remove)
        verify(cacheManager, times(2)).removeAll(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE);
        // add then remove the address from the properties
        verify(propertyManager, times(1)).setTrustedProxyServers(REMOTE_ADDRESS_1);
        verify(propertyManager, times(1)).setTrustedProxyServers("");

        verify(cacheManager, never()).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
    }

    @Test
    public void testRemove_NotInList() throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenReturn(REMOTE_ADDRESS_2);
        // nothing really happens here since Address1 is not in the list.
        tpm.removeAddress(REMOTE_ADDRESS_1);

        // Check proxy list has not been changed
        Set<String> proxies = tpm.getAddresses();
        assertEquals(1, proxies.size());
        assertTrue(proxies.contains(REMOTE_ADDRESS_2));

        verify(propertyManager, times(2)).getTrustedProxyServers();

        // the remove essentially does nothing - so cache is untouched
        verifyZeroInteractions(cacheManager);
    }

    @Test
    public void testRemove_Success() throws Exception
    {
        ArrayList<String> trustedProxy = new ArrayList<String>();
        trustedProxy.add(REMOTE_ADDRESS_1);
        trustedProxy.add(REMOTE_ADDRESS_2);
        trustedProxy.add(REMOTE_ADDRESS_3);

        ArrayList<String> trustedProxyAfter = new ArrayList<String>(trustedProxy);
        trustedProxyAfter.remove(REMOTE_ADDRESS_1);

        // assume already in cache
        when(propertyManager.getTrustedProxyServers()).thenReturn(REMOTE_ADDRESS_1 + "," + REMOTE_ADDRESS_2 + "," + REMOTE_ADDRESS_3).thenReturn(REMOTE_ADDRESS_2 + "," + REMOTE_ADDRESS_3);

        tpm.removeAddress(REMOTE_ADDRESS_1);

        // Check proxy list has the remaining 2 proxies
        Set<String> proxies = tpm.getAddresses();
        assertEquals(Sets.<String>newHashSet(REMOTE_ADDRESS_2, REMOTE_ADDRESS_3), proxies);

        verify(propertyManager, times(2)).getTrustedProxyServers();

        // modified trusted proxy list, make sure caches are nuked (once each for add, remove)
        verify(cacheManager).removeAll(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE);
        verify(propertyManager).setTrustedProxyServers(REMOTE_ADDRESS_2 + "," + REMOTE_ADDRESS_3);

        verify(cacheManager, never()).get(eq(TrustedProxyManagerImpl.REQUESTED_PROXIES_CACHE), anyString());
    }

    @Test
    public void isTrustedAcceptsLoopbackIpv6AddressWithScopeForRequestAddress() throws Exception
    {
        when(cacheManager.get(anyString(), anyString())).thenThrow(new NotInCacheException());
        when(propertyManager.getTrustedProxyServers()).thenReturn("0:0:0:0:0:0:0:1");

        ArrayList<String> trustedProxy = new ArrayList<String>();
        trustedProxy.add(REMOTE_ADDRESS_1);

        assertTrue(tpm.isTrusted("0:0:0:0:0:0:0:1%0"));
    }

    @Test
    public void addingAddressWithSpacesTrims() throws Exception
    {
        assertTrue(tpm.addAddress(" 192.168.1.1 "));

        verify(propertyManager).setTrustedProxyServers("192.168.1.1");
    }
}
