package com.atlassian.crowd.importer.password;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.atlassian.crowd.password.encoder.AtlassianSecurityPasswordEncoder;

import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * A test case that uses the techniques of each Atlassian product to
 * generate a password hash.
 * This TestCase is in place to help us know if and when a product changes their encoding and
 * if it will affect Crowd.
 */
public class TestAtlassianPasswordEncoders
{
    @Test
    public void testConfluencePasswordEncoding()
    {
        String encodedConfluenceUserPass = "Vy/FrPnu10DloFXMNA1qvE+br42SuDfPhewKy5KYf2TZb6bWzWvdmlzezwD3jAcp/0K8fQtrCu/uYL7PPAGqtw==";

        assertTrue("Password uses unknown encoding", new AtlassianSecurityPasswordEncoder().isPasswordValid(encodedConfluenceUserPass, "justin", null));
    }

    @Test
    public void testJIRAPasswordEncoding() throws NoSuchAlgorithmException, UnsupportedEncodingException
    {
        String encodedJIRAUserPass = "Vy/FrPnu10DloFXMNA1qvE+br42SuDfPhewKy5KYf2TZb6bWzWvdmlzezwD3jAcp/0K8fQtrCu/uYL7PPAGqtw==";

        // Technique used by JIRA (OSUser, SHA-512)
        MessageDigest md = MessageDigest.getInstance("SHA-512");
        byte[] digested = md.digest("justin".getBytes("us-ascii"));
        byte[] encoded = Base64.encodeBase64(digested);

        String sha1EncodedPass = new String(encoded);

        assertEquals(encodedJIRAUserPass, sha1EncodedPass);
        assertTrue("Password uses unknown encoding", new AtlassianSecurityPasswordEncoder().isPasswordValid(encodedJIRAUserPass, "justin", null));
    }
}
