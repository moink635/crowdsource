package com.atlassian.crowd.trusted;

import java.security.KeyPair;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;

import com.atlassian.security.auth.trustedapps.CurrentApplication;
import com.atlassian.security.auth.trustedapps.EncryptionProvider;
import com.atlassian.util.concurrent.LazyReference;

import com.google.common.base.Charsets;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v2.7
 */
@RunWith(MockitoJUnitRunner.class)
public class CrowdTrustedApplicationsManagerTest
{
    private static final String PUBLIC_KEY = "PUBLICKEY";
    private static final String PUBLIC_KEY_BASE64 = "UFVCTElDS0VZ";
    private static final String PRIVATE_KEY = "PRIVATEKEY";
    private static final String PRIVATE_KEY_BASE64 = "UFJJVkFURUtFWQ==";

    @InjectMocks
    private CrowdTrustedApplicationsManager service;
    @Mock
    private EncryptionProvider encryptionProvider;
    @Mock
    private TrustedApplicationStore trustedApplicationStore;

    @Rule
    public ExpectedException exceptions = ExpectedException.none();

    @Test
    public void getCurrentApplicationWithNoExistingOneShouldCreateOne() throws Exception
    {
        final byte[] publicKeyBytes = PUBLIC_KEY.getBytes(Charsets.UTF_8);
        final byte[] privateKeyBytes = PRIVATE_KEY.getBytes(Charsets.UTF_8);
        final PrivateKey privateKey = mock(PrivateKey.class);
        final PublicKey publicKey = mock(PublicKey.class);
        when(trustedApplicationStore.getCurrentApplication()).thenReturn(null);
        when(encryptionProvider.generateNewKeyPair()).thenReturn(new KeyPair(publicKey, privateKey));
        when(encryptionProvider.generateUID()).thenReturn("someId");

        when(encryptionProvider.toPrivateKey(privateKeyBytes)).thenReturn(privateKey);
        when(privateKey.getEncoded()).thenReturn(privateKeyBytes);

        when(encryptionProvider.toPublicKey(publicKeyBytes)).thenReturn(publicKey);
        when(publicKey.getEncoded()).thenReturn(publicKeyBytes);

        final CurrentApplication currentApplication = service.getCurrentApplication();

        assertThat(currentApplication, notNullValue());
        assertThat(currentApplication.getID(), is("someId"));
        assertThat(currentApplication.getPublicKey().getEncoded(), is(publicKeyBytes));

        verify(trustedApplicationStore).storeCurrentApplication(any(InternalCurrentApplication.class));
    }

    @Test
    public void getCurrentApplicationWithExistingOneShouldReturnIt() throws Exception
    {
        final PrivateKey privateKey = mock(PrivateKey.class);
        final PublicKey publicKey = mock(PublicKey.class);
        final InternalCurrentApplication internalCurrentApplication = new InternalCurrentApplication("someId", PRIVATE_KEY_BASE64, PUBLIC_KEY_BASE64);

        when(trustedApplicationStore.getCurrentApplication()).thenReturn(internalCurrentApplication);
        when(encryptionProvider.toPrivateKey(PRIVATE_KEY.getBytes(Charsets.UTF_8))).thenReturn(privateKey);
        when(encryptionProvider.toPublicKey(PUBLIC_KEY.getBytes(Charsets.UTF_8))).thenReturn(publicKey);

        final CurrentApplication currentApplication = service.getCurrentApplication();

        assertThat(currentApplication, notNullValue());
        assertThat(currentApplication.getID(), is("someId"));
        assertThat(currentApplication.getPublicKey(), is(sameInstance(publicKey)));
    }

    @Test
    public void getCurrentApplicationWithErrors() throws Exception
    {
        when(trustedApplicationStore.getCurrentApplication()).thenReturn(null);
        when(encryptionProvider.generateNewKeyPair()).thenThrow(new NoSuchProviderException());

        // Expect an exception to be thrown
        exceptions.expect(LazyReference.InitializationException.class);
        exceptions.expectCause(Matchers.<IllegalStateException>instanceOf(NoSuchProviderException.class));

        service.getCurrentApplication();
    }
}
