package com.atlassian.crowd.manager.threadlocal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.plugin.web.ExecutingHttpRequest;

import com.google.common.collect.Maps;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

/**
 * @since v2.7
 */
public class CrowdThreadLocalStateAccessorTest
{
    private CrowdThreadLocalStateAccessor service = new CrowdThreadLocalStateAccessor();

    @Before
    public void setUp() throws Exception
    {
        // Make sure all the thread locals are cleared before the tests, to be sure we start in a known state.
        clearThreadLocals();
    }

    @After
    public void tearDown() throws Exception
    {
        // Clear the thread locals after the tests
        clearThreadLocals();
    }

    @Test
    public void getStateShouldReturnAllTheThreadLocals() throws Exception
    {
        final SecurityContext securityContext = securityContext();
        final ActionContext actionContext = new ActionContext(Maps.newHashMap());
        final HttpServletRequest request = new MockHttpServletRequest();
        final HttpServletResponse response = new MockHttpServletResponse();

        setThreadLocals(securityContext, actionContext, request, response);

        final ThreadLocalState state = service.getState();

        assertThat(state.getSecurityContext(), sameInstance(securityContext));
        assertThat(state.getActionContext(), sameInstance(actionContext));
        assertThat(state.getRequest(), sameInstance(request));
        assertThat(state.getResponse(), sameInstance(response));
    }

    @Test
    public void setStateShouldSetAllTheThreadLocals() throws Exception
    {
        final SecurityContext securityContext = securityContext();
        final ActionContext actionContext = new ActionContext(Maps.newHashMap());
        final HttpServletRequest request = new MockHttpServletRequest();
        final HttpServletResponse response = new MockHttpServletResponse();
        final ThreadLocalState state = new ThreadLocalState(securityContext, actionContext, request, response);

        service.setState(state);

        assertThat(SecurityContextHolder.getContext(), sameInstance(securityContext));
        assertThat(ServletActionContext.getContext(), sameInstance(actionContext));
        assertThat(ExecutingHttpRequest.get(), sameInstance(request));
        assertThat(ExecutingHttpRequest.getResponse(), sameInstance(response));
    }

    @Test
    public void clearStateShouldClearAllThreadLocals() throws Exception
    {
        final SecurityContext securityContext = securityContext();
        final ActionContext actionContext = new ActionContext(Maps.newHashMap());
        final HttpServletRequest request = new MockHttpServletRequest();
        final HttpServletResponse response = new MockHttpServletResponse();

        // Set all the ThreadLocals
        setThreadLocals(securityContext, actionContext, request, response);

        service.clearState();

        // SecurityContextHolder creates a new SecurityContext with a null Authentication if none exists
        assertThat(SecurityContextHolder.getContext().getAuthentication(), nullValue());
        // ActionContext.getContext() creates a new ActionContext if none exists... Just make sure it's not the same one that was set
        assertThat(ActionContext.getContext(), not(sameInstance(actionContext)));
        assertThat(ExecutingHttpRequest.get(), nullValue());
        assertThat(ExecutingHttpRequest.getResponse(), nullValue());
    }

    private SecurityContext securityContext()
    {
        final Authentication authentication = mock(Authentication.class);
        final SecurityContext securityContext = new SecurityContextImpl();
        securityContext.setAuthentication(authentication);
        return securityContext;
    }

    private void setThreadLocals(final SecurityContext securityContext, final ActionContext actionContext, final HttpServletRequest request, final HttpServletResponse response)
    {
        SecurityContextHolder.setContext(securityContext);
        ServletActionContext.setContext(actionContext);
        ExecutingHttpRequest.set(request, response);
    }

    private void clearThreadLocals()
    {
        SecurityContextHolder.clearContext();
        ActionContext.setContext(null);
        ExecutingHttpRequest.clear();
    }
}
