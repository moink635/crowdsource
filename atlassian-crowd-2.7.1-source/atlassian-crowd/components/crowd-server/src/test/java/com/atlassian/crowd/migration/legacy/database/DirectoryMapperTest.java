package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.directory.SynchronisableDirectoryProperties;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.database.sql.MySQLLegacyTableQueries;
import com.atlassian.crowd.migration.legacy.database.sql.PostgresLegacyTableQueries;
import com.atlassian.crowd.model.directory.DirectoryImpl;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.crowd.embedded.api.OperationType.CREATE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.DELETE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_USER;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DirectoryMapperTest extends BaseDatabaseTest
{
    private static final String INTERNAL_DIRECTORY_NAME = "Second Internal";
    private static final String INTERNAL_DIRECTORY_DESCRIPTION = "A Second Internal Directory";
    private static final String INTERNAL_DIRECTORY_IMPLEMENTATION_CLASS = "com.atlassian.crowd.integration.directory.internal.InternalDirectory";
    private static final String INTERNAL_DIRECTORY_CREATED_DATE = "2010-02-03 15:08:53.000000000"; // dates from crowd15db.script
    private static final String INTERNAL_DIRECTORY_UPDATED_DATE = "2010-02-05 11:04:55.000000000";
    private static final boolean INTERNAL_DIRECTORY_ACTIVE = true;

    private static final String CONNECTOR_DIRECTORY_NAME = "Testing Active Directory";
    private static final String CONNECTOR_DIRECTORY_IMPLEMENTATION_CLASS = "com.atlassian.crowd.integration.directory.connector.MicrosoftActiveDirectory";

    private DirectoryMapper directoryMapper;
    private final List<Object> addedObjects = new ArrayList<Object>();

    @Override
    protected void onSetUp() throws Exception
    {
        super.onSetUp();

        // We don't want this test to really add to database. So mock it out
        final Directory mockReturnObject = mock(Directory.class);
        when(mockReturnObject.getId()).thenReturn(1L, 2L, 3L).thenReturn(null);

        directoryMapper = new DirectoryMapper(sessionFactory, batchProcessor, jdbcTemplate){
            @Override
            protected Object addEntityViaMerge(Object entityToPersist) throws ImportException
            {
                addedObjects.add(entityToPersist);
                return mockReturnObject;
            }
        };
    }

    public void testMySQLScripts()
    {
        directoryMapper.setLegacyTableQueries(new MySQLLegacyTableQueries());
        _testMigrateDirectory();
    }

    public void testPostgresScripts()
    {
        directoryMapper.setLegacyTableQueries(new PostgresLegacyTableQueries());
        _testMigrateDirectory();
    }

    public void _testMigrateDirectory()
    {
        Map<Long, Long> oldToNewDirectoryIds = new HashMap<Long, Long>();
        List<DirectoryImpl> directories;
        try
        {
            directories = directoryMapper.importDirectoriesFromDatabase(oldToNewDirectoryIds);
            assertEquals(3, addedObjects.size());
            assertEquals(3, directories.size());
            assertEquals(3, oldToNewDirectoryIds.size());
            assertEquals(new Long(1), oldToNewDirectoryIds.get(98305L));
            assertEquals(new Long(2), oldToNewDirectoryIds.get(98306L));
            assertEquals(new Long(3), oldToNewDirectoryIds.get(98308L));

            // Test an Internal directory
            DirectoryImpl directory = directories.get(1);

            // Internal directory basic info
            assertEquals(INTERNAL_DIRECTORY_NAME, directory.getName());
            assertEquals(INTERNAL_DIRECTORY_DESCRIPTION, directory.getDescription());
            assertEquals(INTERNAL_DIRECTORY_IMPLEMENTATION_CLASS, directory.getImplementationClass());
            assertEquals(directoryMapper.getDateFromDatabase(INTERNAL_DIRECTORY_CREATED_DATE), directory.getCreatedDate());
            assertEquals(directoryMapper.getDateFromDatabase(INTERNAL_DIRECTORY_UPDATED_DATE), directory.getUpdatedDate());
            assertEquals(INTERNAL_DIRECTORY_ACTIVE, directory.isActive());
            assertEquals(DirectoryType.INTERNAL, directory.getType());

            // Internal directory allowed operations
            assertEquals(EnumSet.of(CREATE_USER, UPDATE_USER, DELETE_USER), directory.getAllowedOperations());

            // Internal directory attributes
            assertEquals(4, directory.getAttributes().size());
            assertEquals("atlassian-sha1", directory.getAttributes().get("user_encryption_method"));
            assertEquals("0", directory.getAttributes().get("password_max_change_time"));
            assertEquals("0", directory.getAttributes().get("password_history_count"));
            assertEquals("0", directory.getAttributes().get("password_max_attempts"));


            // Test a connector directory - ActiveDirectory (in particular attributes)
            directory = directories.get(2);
            assertEquals(CONNECTOR_DIRECTORY_NAME, directory.getName());
            assertEquals(CONNECTOR_DIRECTORY_IMPLEMENTATION_CLASS, directory.getImplementationClass());
            assertEquals(DirectoryType.CONNECTOR, directory.getType());

            // Test allowed operations
            assertEquals(ALL_PRE_CROWD_2_0_OPERATION_TYPES, directory.getAllowedOperations());
            // Pick a couple of attributes to test
            // Note: Three upgrade tasks, will add 4 extra attributes:
            //            UpgradeTask361 - ldap.roles.disabled
            //            UpgradeTask396 - useMonitoring, useCaching
            //            UpgradeTask424 - ldap.cache.synchronise.interval (it removes useMonitoring and useCaching)
            // So these should not exist yet
            assertEquals(31, directory.getAttributes().size());
            assertEquals("ldap://crowd-ad1:389/", directory.getAttributes().get(LDAPPropertiesMapper.LDAP_URL_KEY));
            assertEquals("dc=sydney,dc=atlassian,dc=com", directory.getAttributes().get(LDAPPropertiesMapper.LDAP_BASEDN_KEY));
            assertEquals("cn=Administrator,cn=Users,dc=sydney,dc=atlassian,dc=com", directory.getAttributes().get(LDAPPropertiesMapper.LDAP_USERDN_KEY));
            assertEquals("atlassian", directory.getAttributes().get(LDAPPropertiesMapper.LDAP_PASSWORD_KEY));
            assertEquals("(&(objectCategory=Person)(sAMAccountName=*))", directory.getAttributes().get(LDAPPropertiesMapper.USER_OBJECTFILTER_KEY));
            assertEquals("999", directory.getAttributes().get(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE));
            assertNull(directory.getAttributes().get(LDAPPropertiesMapper.ROLES_DISABLED));
            assertNull(directory.getAttributes().get("useMonitoring"));
            assertNull(directory.getAttributes().get("useCaching"));
            assertNull(directory.getAttributes().get(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL));
        }
        catch (ImportException e)
        {
            fail("Failed to import Directory data from legacy database");
        }
    }
}
