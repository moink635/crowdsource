package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.dao.membership.MembershipDAOHibernate;
import com.atlassian.crowd.dao.property.PropertyDAOHibernate;
import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.migration.legacy.LegacyImportDataHolder;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchResultWithIdReferences;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;
import org.hibernate.SessionFactory;
import org.springframework.test.AbstractTransactionalDataSourceSpringContextTests;

import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static com.atlassian.crowd.embedded.api.OperationType.CREATE_GROUP;
import static com.atlassian.crowd.embedded.api.OperationType.CREATE_ROLE;
import static com.atlassian.crowd.embedded.api.OperationType.CREATE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.DELETE_GROUP;
import static com.atlassian.crowd.embedded.api.OperationType.DELETE_ROLE;
import static com.atlassian.crowd.embedded.api.OperationType.DELETE_USER;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_GROUP;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_ROLE;
import static com.atlassian.crowd.embedded.api.OperationType.UPDATE_USER;

public abstract class BaseDatabaseTest extends AbstractTransactionalDataSourceSpringContextTests
{
    protected static final Set<OperationType> ALL_PRE_CROWD_2_0_OPERATION_TYPES = Collections.unmodifiableSet(EnumSet.of(
            CREATE_GROUP, UPDATE_GROUP, DELETE_GROUP,
            CREATE_USER, UPDATE_USER, DELETE_USER,
            CREATE_ROLE, UPDATE_ROLE, DELETE_ROLE
    ));

    protected SessionFactory sessionFactory;
    protected BatchProcessor batchProcessor;
    protected ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;

    protected GroupDAOHibernate groupDAO;
    protected MembershipDAOHibernate membershipDAO;
    protected DirectoryDAOHibernate directoryDAO;
    protected UserDAOHibernate userDAO;
    protected PropertyDAOHibernate propertyDAO;

    // Required for UserMapper, GroupMapper and RoleMapper tests (membership mapping between user, group/role)
    protected LegacyImportDataHolder importDataHolder;
    protected BatchResultWithIdReferences<Group> groupImportResults;
    protected DirectoryImpl directory_one;
    protected DirectoryImpl directory_two;

    @Override
    protected String[] getConfigLocations()
    {
        setAutowireMode(AUTOWIRE_BY_TYPE);
        return new String[]{"classpath:/applicationContext-migration-config.xml",
                "classpath:/applicationContext-CrowdDAO.xml",
                "classpath:/applicationContext-CrowdEncryption.xml"};
    }

    @Override
    protected void onSetUp() throws Exception
    {
        super.onSetUp();

        // jdbcTemplate and dataSource should be automagically wired up
        assertNotNull(jdbcTemplate);
        assertEquals(1, countRowsInTable("hibernate_unique_key")); // will always have 1 entry

        assertEquals(0, countRowsInTable("cwd_user"));
        assertEquals(0, countRowsInTable("cwd_property"));
        assertEquals(0, countRowsInTable("cwd_group"));
        assertEquals(0, countRowsInTable("cwd_membership"));
        assertEquals(0, countRowsInTable("cwd_directory"));
        assertEquals(0, countRowsInTable("cwd_application"));

        // Create directories for mapping purposes
        InternalEntityTemplate directoryTemplate = new InternalEntityTemplate();
        directoryTemplate.setId(1L);
        directoryTemplate.setName("Internal Directory");
        directory_one = new DirectoryImpl(directoryTemplate);
        directoryTemplate.setId(2L);
        directoryTemplate.setName("Second Internal");
        directory_two = new DirectoryImpl(directoryTemplate);

        Map<Long, Long> oldToNewDirectoryIds = new HashMap<Long, Long>();
        oldToNewDirectoryIds.put(98305L, 1L);
        oldToNewDirectoryIds.put(98306L, 2L);
        oldToNewDirectoryIds.put(98308L, 3L);

        // setup some common dud values to enable users to be created
        InternalEntityTemplate dudInternalUserTemplate = new InternalEntityTemplate();
        UserTemplate dudUserTemplate = new UserTemplate("dud", 1L);
        dudUserTemplate.setEmailAddress("dud@example.com");
        dudUserTemplate.setFirstName("dudFirstname");
        dudUserTemplate.setLastName("dudLastname");
        dudUserTemplate.setDisplayName("dudDisplayname");

        // user import data - we really just need username and id
        // id obtained from doing an actual xml restore.
        BatchResultWithIdReferences<User> userImportResults = new BatchResultWithIdReferences<User>(4);
        // admin, id: 32770
        // test1, id: 32769 (directory 1)
        // test1, id: 32771 (directory 2)
        // testuser, id: 32772
        userImportResults.addIdReference(createDudUser(dudInternalUserTemplate, dudUserTemplate, "admin", 32770L, directory_one));
        userImportResults.addIdReference(createDudUser(dudInternalUserTemplate, dudUserTemplate, "test1", 32769L, directory_one));
        userImportResults.addIdReference(createDudUser(dudInternalUserTemplate, dudUserTemplate, "test1", 32771L, directory_two));
        userImportResults.addIdReference(createDudUser(dudInternalUserTemplate, dudUserTemplate, "testuser", 32772L, directory_one));

        importDataHolder = new LegacyImportDataHolder();
        importDataHolder.setOldToNewDirectoryIds(oldToNewDirectoryIds);
        importDataHolder.setUserImportResults(userImportResults);

        // group import data - we really just need groupname and id
        groupImportResults = new BatchResultWithIdReferences<Group>(4);
        // crowd-administrators, id: 98305
        // test-group1, id: 98306
        // Another Group, id: 98307
        // testing-role, id: 98308 (GroupType.LEGACY_ROLE)
        groupImportResults.addIdReference(createDudGroup("crowd-administrators", 98305L, GroupType.GROUP, directory_one));
        groupImportResults.addIdReference(createDudGroup("test-group1", 98306L, GroupType.GROUP, directory_one));
        groupImportResults.addIdReference(createDudGroup("Another Group", 98307L, GroupType.GROUP, directory_one));
        groupImportResults.addIdReference(createDudGroup("testing-role", 98308L, GroupType.LEGACY_ROLE, directory_one));
    }

    private InternalGroup createDudGroup(String groupName, Long groupId, GroupType groupType, Directory directory)
    {
        InternalEntityTemplate internalGroupTemplate = new InternalEntityTemplate();
        internalGroupTemplate.setName(groupName);
        internalGroupTemplate.setId(groupId);
        GroupTemplate groupTemplate = new GroupTemplate(groupName, directory.getId(), groupType);
        return new InternalGroup(internalGroupTemplate, directory, groupTemplate);
    }

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    public void setBatchProcessor(BatchProcessor batchProcessor)
    {
        this.batchProcessor = batchProcessor;
    }

    public void setDirectoryDAO(DirectoryDAOHibernate directoryDAO)
    {
        this.directoryDAO = directoryDAO;
    }

    public void setUserDAO(UserDAOHibernate userDAO)
    {
        this.userDAO = userDAO;
    }

    public void setGroupDAO(GroupDAOHibernate groupDAO)
    {
        this.groupDAO = groupDAO;
    }

    public void setMembershipDAO(MembershipDAOHibernate membershipDAO)
    {
        this.membershipDAO = membershipDAO;
    }

    public void setPropertyDAO(PropertyDAOHibernate propertyDAO)
    {
        this.propertyDAO = propertyDAO;
    }

    public void setResetableHiLoGeneratorHelper(ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper)
    {
        this.resetableHiLoGeneratorHelper = resetableHiLoGeneratorHelper;
    }

    private InternalUser createDudUser(InternalEntityTemplate internalUserTemplate, UserTemplate userTemplate, String username, Long userid, Directory directory)
    {
        internalUserTemplate.setId(userid);
        internalUserTemplate.setName(username);
        userTemplate.setName(username);
        userTemplate.setDirectoryId(directory.getId());
        return new InternalUser(internalUserTemplate, directory, userTemplate, new PasswordCredential("password", true));
    }
}
