package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationManagerException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.util.I18nHelper;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;
import org.jmock.core.Constraint;
import org.jmock.core.Stub;
import org.springframework.util.Assert;

public class TestUpgradeTask320 extends MockObjectTestCase
{
    private UpgradeTask320 task;
    private Mock mockApplicationManager;
    private Mock mockI18nHelper;

    private static final String CROWD_NAME = "crowd";
    private static final String CROWD_DESC = "crowd desc";
    private static final String DEMO_NAME = "demo";
    private static final String DEMO_DESC = "demo desc";
    private static final String OPENID_NAME = "openid";
    private static final String OPENID_DESC = "openid desc";

    @Override
    protected void setUp() throws Exception
    {
        task = new UpgradeTask320();

        mockApplicationManager = new Mock(ApplicationManager.class);
        mockI18nHelper = new Mock(I18nHelper.class);

        task.setApplicationManager((ApplicationManager) mockApplicationManager.proxy());
        task.setI18nHelper((I18nHelper) mockI18nHelper.proxy());

        Stub[] stubs = new Stub[6];
        stubs[0] = returnValue(CROWD_NAME);
        stubs[1] = returnValue(CROWD_DESC);
        stubs[2] = returnValue(DEMO_NAME);
        stubs[3] = returnValue(DEMO_DESC);
        stubs[4] = returnValue(OPENID_NAME);
        stubs[5] = returnValue(OPENID_DESC);

        mockI18nHelper.expects(exactly(6)).method("getText").will(onConsecutiveCalls(stubs));
    }

    protected Application createApplication(String name, String desc, ApplicationType type)
    {
        ApplicationImpl app = ApplicationImpl.newInstanceWithCredential(name, type, new PasswordCredential("secret"));
        app.setDescription(desc);

        return app;
    }

    public void testUpradePerformedForAllApps() throws Exception
    {
        Application crowd = createApplication(CROWD_NAME, "bogus", ApplicationType.CROWD);
        Application demo = createApplication(DEMO_NAME, "bogus", ApplicationType.GENERIC_APPLICATION);
        Application openid = createApplication(OPENID_NAME, "bogus", ApplicationType.GENERIC_APPLICATION);

        mockApplicationManager.expects(once()).method("findByName").with(eq(CROWD_NAME)).will(returnValue(crowd));
        mockApplicationManager.expects(once()).method("findByName").with(eq(DEMO_NAME)).will(returnValue(demo));
        mockApplicationManager.expects(once()).method("findByName").with(eq(OPENID_NAME)).will(returnValue(openid));

        mockApplicationManager.expects(once()).method("update").with(applicationMatching(CROWD_NAME, CROWD_DESC, ApplicationType.CROWD));
        mockApplicationManager.expects(once()).method("update").with(applicationMatching(DEMO_NAME, DEMO_DESC, ApplicationType.GENERIC_APPLICATION));
        mockApplicationManager.expects(once()).method("update").with(applicationMatching(OPENID_NAME, OPENID_DESC, ApplicationType.GENERIC_APPLICATION));

        task.doUpgrade();

        assertTrue(task.getErrors().isEmpty());
    }

    public void testUpgradePerformedForCrowd() throws Exception
    {
        Application crowd = createApplication(CROWD_NAME, "bogus", ApplicationType.CROWD);

        mockApplicationManager.expects(once()).method("findByName").with(eq(CROWD_NAME)).will(returnValue(crowd));
        mockApplicationManager.expects(once()).method("findByName").with(eq(DEMO_NAME)).will(throwException(new ApplicationNotFoundException(DEMO_NAME)));
        mockApplicationManager.expects(once()).method("findByName").with(eq(OPENID_NAME)).will(throwException(new ApplicationNotFoundException(OPENID_NAME)));

        mockApplicationManager.expects(once()).method("update").with(applicationMatching(CROWD_NAME, CROWD_DESC, ApplicationType.CROWD));

        task.doUpgrade();

        assertTrue(task.getErrors().isEmpty());
    }

    public void testUpgradePerformedWithErrors() throws Exception
    {
        Application demo = createApplication(DEMO_NAME, "bogus", ApplicationType.GENERIC_APPLICATION);
        Application openid = createApplication(OPENID_NAME, "bogus", ApplicationType.GENERIC_APPLICATION );

        mockApplicationManager.expects(once()).method("findByName").with(eq(CROWD_NAME)).will(throwException(new ApplicationNotFoundException(CROWD_NAME)));
        mockApplicationManager.expects(once()).method("findByName").with(eq(DEMO_NAME)).will(returnValue(demo));
        mockApplicationManager.expects(once()).method("findByName").with(eq(OPENID_NAME)).will(returnValue(openid));

        mockApplicationManager.expects(once()).method("update").with(applicationMatching(DEMO_NAME, DEMO_DESC, ApplicationType.GENERIC_APPLICATION)).will(throwException(new ApplicationManagerException()));
        mockApplicationManager.expects(once()).method("update").with(applicationMatching(OPENID_NAME, OPENID_DESC, ApplicationType.GENERIC_APPLICATION));

        task.doUpgrade();

        assertEquals(2, task.getErrors().toArray().length);
    }


    public ApplicationMatcher applicationMatching(String name, String description, ApplicationType type)
    {
        return new ApplicationMatcher(name, description, type);
    }

    private class ApplicationMatcher implements Constraint
    {
        private String name;
        private String description;
        private ApplicationType type;

        private ApplicationMatcher(String name, String description, ApplicationType type)
        {
            Assert.notNull(name);
            Assert.notNull(description);
            Assert.notNull(type);

            this.name = name;
            this.description = description;
            this.type = type;
        }

        public boolean eval(Object o)
        {
            if (o instanceof Application)
            {
                Application app = (Application) o;
                return name.equals(app.getName()) && description.equals(app.getDescription()) && type == app.getType();
            }
            else
            {
                return false;
            }
        }

        public StringBuffer describeTo(StringBuffer buffer)
        {
            buffer.append("ApplicationMatcher name: ").append(name).append(", description: ").append(description).append(", type: ").append(type);
            return buffer;
        }
    }

}

