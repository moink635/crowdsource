package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.MicrosoftActiveDirectory;
import com.atlassian.crowd.directory.OpenLDAP;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * UpgradeTask215 Tester.
 */
public class TestUpgradeTask215
{
    @Mock DirectoryManager directoryManager;
    private UpgradeTask215 upgradeTask;
    private DirectoryImpl directoryNoUpdate;
    private DirectoryImpl directoryToUpdate;

    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        upgradeTask = new UpgradeTask215();
        upgradeTask.setDirectoryManager(directoryManager);

        directoryNoUpdate = new DirectoryImpl("directory-no-update", DirectoryType.CONNECTOR, MicrosoftActiveDirectory.class.getCanonicalName());
        directoryNoUpdate.setAttribute(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY, "true");
        directoryNoUpdate.setAttribute(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE, "999");

        directoryToUpdate = new DirectoryImpl("directory-to-update", DirectoryType.CONNECTOR, OpenLDAP.class.getCanonicalName());
        directoryToUpdate.setAttribute(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY, "false");
        directoryToUpdate.setAttribute(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE, "999");

    }

    @After
    public void tearDown() throws Exception
    {
        upgradeTask = null;
        directoryNoUpdate = null;
        directoryToUpdate = null;
    }

    @Test
    public void testDoUpgradeWithDirectoryToUpdate() throws Exception
    {
        EntityQuery<Directory> entityQuery = QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS);
        when(directoryManager.searchDirectories(entityQuery)).thenReturn(Arrays.<Directory>asList(directoryToUpdate));
        when(directoryManager.updateDirectory(directoryToUpdate)).thenReturn(null);

        upgradeTask.doUpgrade();

        verify(directoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.notContains(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE)));
        assertFalse(Boolean.valueOf(directoryToUpdate.getValue(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY)));

        assertTrue(upgradeTask.getErrors().isEmpty());
    }

    @Test
    public void testDoUpgradeWithDirectoryToLeaveAlone() throws Exception
    {

        when(directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS))).thenReturn(Arrays.<Directory>asList(directoryNoUpdate));
        upgradeTask.doUpgrade();

        verify(directoryManager, never()).updateDirectory(any(Directory.class));

        assertTrue(Boolean.valueOf(directoryNoUpdate.getValue(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY)));
        assertNotNull(directoryNoUpdate.getValue(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE));

        assertTrue(upgradeTask.getErrors().isEmpty());
    }

    @Test
    public void testGetShortDescription() throws Exception
    {
        assertNotNull(upgradeTask.getShortDescription());
    }
}
