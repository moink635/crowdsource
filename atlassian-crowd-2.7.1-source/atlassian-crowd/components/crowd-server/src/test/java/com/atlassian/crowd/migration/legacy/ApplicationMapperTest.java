package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.crowd.migration.legacy.ApplicationMapper.APPLICATION_XML_NODE;
import static com.atlassian.crowd.migration.legacy.ApplicationMapper.APPLICATION_XML_ROOT;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ApplicationMapperTest extends BaseLegacyImporterTest
{
    private ApplicationMapper mapper;
    private DirectoryDAOHibernate directoryDAOHibernate;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        // These legacy xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        BatchProcessor batchProcessor = mock(BatchProcessor.class);

        directoryDAOHibernate = mock(DirectoryDAOHibernate.class);
        when(directoryDAOHibernate.loadReference(1L)).thenReturn(directoryWithIdAndName(1, "Directory One"));
        when(directoryDAOHibernate.loadReference(2L)).thenReturn(directoryWithIdAndName(2, "Directory Two"));

        oldToNewDirectoryIds = new HashMap<Long, Long>();
        oldToNewDirectoryIds.put(6L, 1L);
        oldToNewDirectoryIds.put(7L, 2L);

        mapper = new ApplicationMapper(sessionFactory, batchProcessor, directoryDAOHibernate);

    }

    @Override
    protected void tearDown() throws Exception
    {
        document = null;
        super.tearDown();
    }

    public void testImportApplicationXML() throws Exception
    {
        Element applicationsNode = (Element) document.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + APPLICATION_XML_ROOT);
        final List<Element> applicationElements = applicationsNode.elements(APPLICATION_XML_NODE);

        final ApplicationImpl applicationFromXml = mapper.getApplicationFromXml(applicationElements.get(0), oldToNewDirectoryIds);

        assertEquals("crowd", applicationFromXml.getName());
        assertEquals("Atlassian Crowd", applicationFromXml.getDescription());
        assertTrue(applicationFromXml.isActive());

        Map<String, String> attributes = new HashMap<String, String>();
        attributes.put("atlassian_sha1_applied", "true");
        assertEquals(attributes, applicationFromXml.getAttributes());

        assertEquals(4, applicationFromXml.getRemoteAddresses().size());
        assertTrue(applicationFromXml.getRemoteAddresses().containsAll(Arrays.<RemoteAddress>asList(new RemoteAddress("moonlight.sydney.atlassian.com"), new RemoteAddress("127.0.0.1"), new RemoteAddress("localhost"), new RemoteAddress("192.168.0.72"))));

        assertEquals("etiRtGdponis/3K/xAm96QtVwWjKYNOfXLNHy28yZL9/lSHAmVfhtbjWWzV/l58bBchohNeQIEQ9eiYcpcJ37A==", applicationFromXml.getCredential().getCredential());

        DirectoryMapping directoryMappingOne = applicationFromXml.getDirectoryMapping(1L);
        assertEquals(1, directoryMappingOne.getAuthorisedGroups().size());
        assertEquals("crowd-administrators", directoryMappingOne.getAuthorisedGroups().iterator().next().getGroupName());

        assertFalse(directoryMappingOne.isAllowAllToAuthenticate());

        assertEquals(ALL_PRE_20_OPERATION_TYPES, directoryMappingOne.getAllowedOperations());

        DirectoryMapping directoryMappingTwo = applicationFromXml.getDirectoryMapping(2L);
        assertEquals(1, directoryMappingTwo.getAuthorisedGroups().size());
        assertEquals("crowd-users", directoryMappingTwo.getAuthorisedGroups().iterator().next().getGroupName());
        assertTrue(directoryMappingTwo.isAllowAllToAuthenticate());
        assertTrue(directoryMappingTwo.getAllowedOperations().isEmpty());


    }


}
