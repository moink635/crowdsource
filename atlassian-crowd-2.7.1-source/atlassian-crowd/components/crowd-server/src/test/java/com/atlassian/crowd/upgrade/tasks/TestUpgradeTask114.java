package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationAttributeConstants;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.password.encoder.AtlassianSHA1PasswordEncoder;
import com.atlassian.crowd.password.encoder.DESPasswordEncoder;
import com.atlassian.crowd.password.encoder.PasswordEncoder;
import org.apache.commons.codec.binary.Base64;
import org.junit.Before;
import org.junit.Test;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestUpgradeTask114
{
    private UpgradeTask114 upgradeTask = null;
    private SecretKey secretKey = null;
    private String applicationCredentialEncryptedDES;
    private String applicationCredentialUnencrypted;
    private ApplicationImpl application1;
    private ApplicationImpl application2;
    private String applicationCredentialEncryptedAtlassianSHA1;
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() throws Exception
    {
        upgradeTask = new UpgradeTask114();

        // Set up a real password encoder + factory for this test
        passwordEncoder = new AtlassianSHA1PasswordEncoder();

        // DES unencrypted password
        applicationCredentialUnencrypted = "HMrPXt2B";

        // DES encrypted password
        applicationCredentialEncryptedDES = "/p3HlzWAkfvpGaw7AV+PLA==";

        applicationCredentialEncryptedAtlassianSHA1 = passwordEncoder.encodePassword(applicationCredentialUnencrypted, null);


        // create a DES key spec, with the 'Secret Key'
        DESKeySpec ks = new DESKeySpec(Base64.decodeBase64("0Dfy7CXfkR8="));

        // generate the key from the DES key spec
        secretKey = SecretKeyFactory.getInstance(DESPasswordEncoder.PASSWORD_ENCRYPTION_ALGORITHM).generateSecret(ks);

        // Set up the Mock property manager to return the secret key
        PropertyManager propertyManager = mock(PropertyManager.class);
        when(propertyManager.getDesEncryptionKey()).thenReturn(secretKey);
        upgradeTask.setPropertyManager(propertyManager);

        // Create an application with a DES encrypted credential
        application1 = ApplicationImpl.newInstanceWithCredential("Test Application 1", ApplicationType.GENERIC_APPLICATION, new PasswordCredential(applicationCredentialEncryptedDES));

        // Set up this application to have already had its internal password encrypted
        application2 = ApplicationImpl.newInstanceWithCredential("Test Application 2", ApplicationType.GENERIC_APPLICATION, new PasswordCredential(applicationCredentialEncryptedAtlassianSHA1));
        application2.setAttribute(ApplicationAttributeConstants.ATTRIBUTE_KEY_ATLASSIAN_SHA1_APPLIED, Boolean.TRUE.toString());

        // Setup the mock application manager, for the search and update
        ApplicationManager applicationManager = mock(ApplicationManager.class);
        when(applicationManager.findAll()).thenReturn(Arrays.<Application>asList(application1, application2));

        when(applicationManager.update(eq(application1))).thenReturn(application1);

        upgradeTask.setApplicationManager(applicationManager);
    }

    @Test
    public void testDecryptCredential() throws GeneralSecurityException, PropertyManagerException, IOException
    {
        String unencryptedCredential = upgradeTask.decryptCredential(applicationCredentialEncryptedDES);

        assertEquals(applicationCredentialUnencrypted, unencryptedCredential);
    }

    @Test
    public void testDoUpgrade() throws Exception
    {
        upgradeTask.doUpgrade();

        // assert the credential is not null
        assertNotNull(application1.getCredential());
        PasswordCredential passwordCredential = application1.getCredential();

        // Did we encode the password correctly
        assertTrue(passwordEncoder.isPasswordValid(passwordCredential.getCredential(), applicationCredentialUnencrypted, null));

        // Make sure that the second password has not been corrupted
        assertNotNull(application2.getCredential());
        passwordCredential = application2.getCredential();

        // Is the password still valid ?
        assertTrue(passwordEncoder.isPasswordValid(passwordCredential.getCredential(), applicationCredentialUnencrypted, null));
    }
}
