package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import junit.framework.TestCase;
import org.mockito.ArgumentMatcher;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;

public class TestUpgradeTask396 extends TestCase
{
    private UpgradeTask396 task;
    private DirectoryManager directoryManager;

    @Override
    protected void setUp() throws Exception
    {
        task = new UpgradeTask396();

        directoryManager = mock(DirectoryManager.class);
        task.setDirectoryManager(directoryManager);
    }

    public void testUpgradeNoLDAPDirectories() throws Exception
    {
        when(directoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(new ArrayList<Directory>());

        task.doUpgrade();

        verify(directoryManager, never()).updateDirectory(any(DirectoryImpl.class));
    }

    public void testUpdateLDAPDirectories() throws Exception
    {
        DirectoryImpl directory1 = new DirectoryImpl("Directory 1", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());
        DirectoryImpl directory2 = new DirectoryImpl("Directory 2", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());
        DirectoryImpl directory3 = new DirectoryImpl("Directory 3", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());

        directory1.setAttribute(UpgradeTask396.ATTRIBUTE_KEY_USE_CACHING, Boolean.TRUE.toString());
        directory1.setAttribute(UpgradeTask396.ATTRIBUTE_KEY_USE_MONITORING, Boolean.TRUE.toString());
        directory1.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.FALSE.toString());

        directory2.setAttribute(UpgradeTask396.ATTRIBUTE_KEY_USE_CACHING, Boolean.TRUE.toString());
        directory2.setAttribute(UpgradeTask396.ATTRIBUTE_KEY_USE_MONITORING, Boolean.TRUE.toString());
        directory2.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.TRUE.toString());

        when(directoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(Arrays.<Directory>asList(directory1, directory2, directory3));

        task.doUpgrade();

        verify(directoryManager, times(2)).updateDirectory(argThat(hasCachingEnabled(false)));
    }

    private HasCachingEnabled hasCachingEnabled(boolean value)
    {
        return new HasCachingEnabled(value);
    }

    class HasCachingEnabled extends ArgumentMatcher<Directory>
    {
        private final boolean enabled;

        public HasCachingEnabled(final boolean enabled)
        {
            this.enabled = enabled;
        }

        public boolean matches(Object directory)
        {
            Directory dir = (Directory) directory;
            boolean cachingEnabled = Boolean.parseBoolean(dir.getValue(UpgradeTask396.ATTRIBUTE_KEY_USE_CACHING));
            boolean monitorEnabled = Boolean.parseBoolean(dir.getValue(UpgradeTask396.ATTRIBUTE_KEY_USE_MONITORING));

            return cachingEnabled == enabled && monitorEnabled == enabled;
        }
    }

}
