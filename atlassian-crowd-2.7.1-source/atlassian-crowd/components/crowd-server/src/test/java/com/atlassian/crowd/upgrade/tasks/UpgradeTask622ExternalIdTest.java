package com.atlassian.crowd.upgrade.tasks;

import java.util.Properties;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpgradeTask622ExternalIdTest
{
    @Mock
    private DirectoryDao directoryDao;

    @Mock
    private LDAPPropertiesHelper ldapPropertiesHelper;

    @InjectMocks
    private UpgradeTask622ExternalId task;

    @Test
    public void shouldOnlyUpgradeLDAPAndDelegatingDirectories() throws Exception
    {
        task.doUpgrade();

        verify(directoryDao).search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                                        .with(Combine.anyOf(Restriction.on(DirectoryTermKeys.TYPE)
                                                                .exactlyMatching(DirectoryType.CONNECTOR),
                                                            Restriction.on(DirectoryTermKeys.TYPE)
                                                                .exactlyMatching(DirectoryType.DELEGATING)))
                                        .returningAtMost(EntityQuery.ALL_RESULTS));

        verifyNoMoreInteractions(directoryDao);
    }

    @Test
    public void shouldNotUpdateDirectoriesThatAlreadyHaveTheSetting() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getValue(LDAPPropertiesMapper.LDAP_EXTERNAL_ID)).thenReturn("value"); // value is irrelevant

        when(directoryDao.search(any(EntityQuery.class))).thenReturn(ImmutableList.of(directory));

        task.doUpgrade();

        verify(directoryDao, never()).update(any(Directory.class));
    }

    @Test
    public void shouldSetDefaultValueForUnconfiguredLDAPConnector() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getName()).thenReturn("directory1");
        when(directory.getType()).thenReturn(DirectoryType.CONNECTOR);
        when(directory.getImplementationClass()).thenReturn("implementation.Class");

        Properties defaultProperties = new Properties();
        defaultProperties.setProperty(LDAPPropertiesMapper.LDAP_EXTERNAL_ID, "default");
        when(ldapPropertiesHelper.getConfigurationDetails())
            .thenReturn(ImmutableMap.of("implementation.Class", defaultProperties));

        when(directoryDao.search(any(EntityQuery.class))).thenReturn(ImmutableList.of(directory));

        task.doUpgrade();

        ArgumentCaptor<Directory> updatedDirectory = ArgumentCaptor.forClass(Directory.class);
        verify(directoryDao).update(updatedDirectory.capture());

        assertThat(updatedDirectory.getValue().getValue(LDAPPropertiesMapper.LDAP_EXTERNAL_ID), is("default"));
    }

    @Test
    public void shouldSetDefaultValueForUnconfiguredDelegatingDirectory() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getName()).thenReturn("directory1");
        when(directory.getType()).thenReturn(DirectoryType.DELEGATING);
        when(directory.getImplementationClass()).thenReturn("delegating");
        when(directory.getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS))
            .thenReturn("implementation.Class");

        Properties defaultProperties = new Properties();
        defaultProperties.setProperty(LDAPPropertiesMapper.LDAP_EXTERNAL_ID, "default");
        when(ldapPropertiesHelper.getConfigurationDetails())
            .thenReturn(ImmutableMap.of("implementation.Class", defaultProperties));

        when(directoryDao.search(any(EntityQuery.class))).thenReturn(ImmutableList.of(directory));

        task.doUpgrade();

        ArgumentCaptor<Directory> updatedDirectory = ArgumentCaptor.forClass(Directory.class);
        verify(directoryDao).update(updatedDirectory.capture());

        assertThat(updatedDirectory.getValue().getValue(LDAPPropertiesMapper.LDAP_EXTERNAL_ID), is("default"));
    }

    @Test
    public void shouldNotSetValueIfDefaultValueIsNotAvailable() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getType()).thenReturn(DirectoryType.CONNECTOR);
        when(directory.getImplementationClass()).thenReturn("implementation.Class");

        when(ldapPropertiesHelper.getConfigurationDetails())
            .thenReturn(ImmutableMap.of("implementation.Class", new Properties()));

        when(directoryDao.search(any(EntityQuery.class))).thenReturn(ImmutableList.of(directory));

        task.doUpgrade();

        verify(directoryDao, never()).update(any(Directory.class));
    }
}
