package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.ApacheDS;
import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.directory.MicrosoftActiveDirectory;
import com.atlassian.crowd.directory.Rfc2307;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelperImpl;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import junit.framework.TestCase;
import org.mockito.ArgumentMatcher;

import java.util.ArrayList;
import java.util.Arrays;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUpgradeTask395 extends TestCase
{
    private UpgradeTask395 task;
    private DirectoryManager directoryManager;


    private final static String LEGACY_APACHE_DS = "com.atlassian.crowd.integration.directory.connector.ApacheDS";
    private final static String LEGACY_MICROSOFT_AD = "com.atlassian.crowd.integration.directory.connector.MicrosoftActiveDirectory";
    private final static String LEGACY_RFC2307 = "com.atlassian.crowd.integration.directory.connector.Rfc2307";
    private final static String LEGACY_INTERNAL = "com.atlassian.crowd.integration.directory.internal.InternalDirectory";


    @Override
    protected void setUp() throws Exception
    {
        task = new UpgradeTask395();

        directoryManager = mock(DirectoryManager.class);
        LDAPPropertiesHelperImpl ldapPropertiesHelper = new LDAPPropertiesHelperImpl();

        task.setDirectoryManager(directoryManager);
        task.setLdapPropertiesHelper(ldapPropertiesHelper);
    }

    public void testUpgradeNoLDAPDirectories() throws Exception
    {
        when(directoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(new ArrayList<Directory>());

        task.doUpgrade();

        verify(directoryManager, never()).updateDirectory(any(DirectoryImpl.class));
    }

    public void testUpdateLDAPDirectories() throws Exception
    {
        // Note: Creating DirectoryImpl with implementation classes using 'LEGACY_XXX" instead of XXX.class.getCanaconicalName()
        // because if the upgrade process runs UpgradeTask395, the directory classes will have the old package structure (CWD-1903) 
        Directory directory1 = new DirectoryImpl(MicrosoftActiveDirectory.class.getSimpleName(), DirectoryType.CONNECTOR, LEGACY_MICROSOFT_AD);
        Directory directory2 = new DirectoryImpl(ApacheDS.class.getSimpleName(), DirectoryType.CONNECTOR, LEGACY_APACHE_DS);
        Directory directory3 = new DirectoryImpl(Rfc2307.class.getSimpleName(), DirectoryType.CONNECTOR, LEGACY_RFC2307);

        when(directoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(Arrays.asList(directory1, directory2, directory3));

        task.doUpgrade();

        verify(directoryManager, times(2)).updateDirectory(argThat(hasRelaxedStandardisation(true)));
        verify(directoryManager, times(1)).updateDirectory(argThat(hasRelaxedStandardisation(false)));
    }

    public void testUpgradeNonLDAPDirectory() throws Exception
    {
        Directory directory1 = new DirectoryImpl(InternalDirectory.class.getSimpleName(), DirectoryType.INTERNAL, LEGACY_INTERNAL);

        when(directoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(Arrays.asList(directory1));

        task.doUpgrade();

        verify(directoryManager, never()).updateDirectory(any(DirectoryImpl.class));
    }

    private HasRelaxedStandardisation hasRelaxedStandardisation(boolean value)
    {
        return new HasRelaxedStandardisation(value);
    }

    class HasRelaxedStandardisation extends ArgumentMatcher<Directory>
    {
        private final boolean relaxed;

        public HasRelaxedStandardisation(final boolean relaxed)
        {
            this.relaxed = relaxed;
        }

        public boolean matches(Object directory)
        {
            String relaxedProperty = ((Directory) directory).getValue(LDAPPropertiesMapper.LDAP_RELAXED_DN_STANDARDISATION);
            return Boolean.parseBoolean(relaxedProperty) == relaxed;
        }
    }

}
