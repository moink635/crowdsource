package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.Arrays;


public class TestUpgradeTask397
{
    private UpgradeTask397 task;
    @Mock private DirectoryManager directoryManager;

    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        task = new UpgradeTask397();

        task.setDirectoryManager(directoryManager);
    }

    @Test
    public void testUpgradeNoLDAPDirectories() throws Exception
    {
        when(directoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(new ArrayList<Directory>());

        task.doUpgrade();

        verify(directoryManager, never()).updateDirectory(any(DirectoryImpl.class));
    }

    @Test
    public void testUpdateLDAPDirectories() throws Exception
    {
        DirectoryImpl directory1 = new DirectoryImpl("Directory 1", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());
        DirectoryImpl directory2 = new DirectoryImpl("Directory 2", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());
        DirectoryImpl directory3 = new DirectoryImpl("Directory 3", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());

        directory1.setType(DirectoryType.DELEGATING);
        directory1.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.FALSE.toString());

        directory2.setType(DirectoryType.DELEGATING);
        directory2.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.TRUE.toString());

        directory3.setType(DirectoryType.DELEGATING);

        when(directoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(Arrays.<Directory>asList(directory1, directory2, directory3));

        task.doUpgrade();

        verify(directoryManager, times(2)).updateDirectory(argThat(hasRolesDisabled(true)));
        verify(directoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.containsForDirectory(directory1.getName(), LDAPPropertiesMapper.ROLES_DISABLED, Boolean.TRUE.toString())));
        verify(directoryManager, never()).updateDirectory(argThat(DirectoryAttributesMatcher.containsForDirectory(directory2.getName(), LDAPPropertiesMapper.ROLES_DISABLED, Boolean.TRUE.toString())));
        verify(directoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.containsForDirectory(directory3.getName(), LDAPPropertiesMapper.ROLES_DISABLED, Boolean.TRUE.toString())));
    }

    private HasRolesDisabled hasRolesDisabled(boolean value)
    {
        return new HasRolesDisabled(value);
    }

    class HasRolesDisabled extends ArgumentMatcher<Directory>
    {
        private final boolean disabled;

        public HasRolesDisabled(final boolean disabled)
        {
            this.disabled = disabled;
        }

        public boolean matches(Object directory)
        {
            Directory dir = (Directory) directory;
            boolean rolesDisabled = Boolean.parseBoolean(dir.getValue(LDAPPropertiesMapper.ROLES_DISABLED));

            return rolesDisabled == disabled;
        }
    }
}
