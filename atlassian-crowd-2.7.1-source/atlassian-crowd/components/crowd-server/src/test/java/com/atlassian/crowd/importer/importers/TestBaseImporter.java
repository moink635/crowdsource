package com.atlassian.crowd.importer.importers;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.config.CsvConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.model.MembershipDTO;
import com.atlassian.crowd.importer.model.Result;
import com.atlassian.crowd.manager.directory.BulkAddResult;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.directory.DirectoryPermissionException;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;

import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

/**
 * Test Case for the BaseImporterDAO
 */
public class TestBaseImporter extends MockObjectTestCase
{
    private static final String TEST_GROUP_1 = "Test-Group-1";
    private static final String TEST_GROUP_2 = "test-group-2";
    private static final String TEST_PRINCIPAL_1 = "Test-Principal-1";
    private static final String TEST_PRINCIPAL_2 = "test-principal-2";
    private static final String TEST_PRINCIPAL_2B = "test-principal-2b";
    private static final String CHILD_GROUP_1 = "childgroup-1";
    private static final String CHILD_GROUP_2 = "childgroup-2";
    private BaseImporter baseImporter = null;
    private Mock mockDirectoryManager;
    private Long directoryID;
    private UserTemplateWithCredentialAndAttributes user1;
    private UserTemplateWithCredentialAndAttributes user2;
    private UserTemplateWithCredentialAndAttributes user2b;

    private GroupTemplate remoteGroup1;
    private GroupTemplate remoteGroup2;
    private GroupTemplate childGroup1;
    private GroupTemplate childGroup2;
    private MembershipDTO groupMembership1;
    private MembershipDTO groupMembership2;
    private MembershipDTO groupMembership2b;
    private MembershipDTO childGroupMembership1;
    private MembershipDTO childGroupMembership2;

    private List<UserTemplateWithCredentialAndAttributes> principals;
    private List<String> principalNames1;
    private List<String> principalNames2;
    private List<GroupTemplate> groups;
    private List<MembershipDTO> userToGroupMemberships;
    private List<MembershipDTO> groupToGroupMemberships;

    private Configuration configuration;
    private Configuration configurationWithNestedGroupOn;
    private List<String> principalNames;

    protected void setUp() throws Exception
    {
        super.setUp();

        directoryID = new Long(1);

        remoteGroup1 = new GroupTemplate(TEST_GROUP_1, directoryID, GroupType.GROUP);
        remoteGroup2 = new GroupTemplate(TEST_GROUP_2, directoryID, GroupType.GROUP);
        childGroup1 = new GroupTemplate(CHILD_GROUP_1, directoryID, GroupType.GROUP);
        childGroup2 = new GroupTemplate(CHILD_GROUP_2, directoryID, GroupType.GROUP);
        groups = Arrays.asList(remoteGroup1, remoteGroup2, childGroup1, childGroup2);

        user1 = new UserTemplateWithCredentialAndAttributes(TEST_PRINCIPAL_1, directoryID, new PasswordCredential("secret"));
        user2 = new UserTemplateWithCredentialAndAttributes(TEST_PRINCIPAL_2, directoryID, new PasswordCredential("secret"));
        user2b = new UserTemplateWithCredentialAndAttributes(TEST_PRINCIPAL_2B, directoryID, new PasswordCredential("secret"));
        principals = Arrays.asList(user2, user1, user2b);
        principalNames = Arrays.asList(user1.getName(), user2.getName(), user2b.getName());
        principalNames1 = Arrays.asList(user1.getName());
        principalNames2 = Arrays.asList(user2.getName(), user2b.getName());

        groupMembership1 = new MembershipDTO(MembershipDTO.ChildType.USER, TEST_PRINCIPAL_1, TEST_GROUP_1);
        groupMembership2 = new MembershipDTO(MembershipDTO.ChildType.USER, TEST_PRINCIPAL_2, TEST_GROUP_2);
        groupMembership2b = new MembershipDTO(MembershipDTO.ChildType.USER, TEST_PRINCIPAL_2B, TEST_GROUP_2);
        childGroupMembership1 = new MembershipDTO(MembershipDTO.ChildType.USER, CHILD_GROUP_1, TEST_GROUP_1);
        childGroupMembership2 = new MembershipDTO(MembershipDTO.ChildType.USER, CHILD_GROUP_2, TEST_GROUP_1);

        userToGroupMemberships = Arrays.asList(groupMembership1, groupMembership2, groupMembership2b);
        groupToGroupMemberships = Arrays.asList(childGroupMembership1, childGroupMembership2);

        mockDirectoryManager = new Mock(DirectoryManager.class);


        configuration = new Configuration(directoryID, "test", Boolean.TRUE, Boolean.FALSE);
        configurationWithNestedGroupOn = new Configuration(directoryID, "test", Boolean.TRUE, Boolean.FALSE);
        configurationWithNestedGroupOn.setImportNestedGroups(true);

        baseImporter = new MockBaseImporter((DirectoryManager) mockDirectoryManager.proxy());
    }

    public void testConstructor()
    {
        try
        {
            baseImporter = new JiraLegacyImporter(null);
            fail("java.lang.IllegalArgumentException should have been thrown");
        }
        catch (IllegalArgumentException e)
        {
            // we should be here
        }
    }

    public void testImportGroupPrincipalMemberships() throws ImporterException
    {
        // test that 3 users in 2 groups import correctly as 2 bulk membership imports (one for each group)

        mockDirectoryManager.expects(once()).method("addAllUsersToGroup").with(eq(directoryID), eq(principalNames1), eq(remoteGroup1.getName()))
                .will(returnValue(BulkAddResult.builder(principalNames1.size()).setOverwrite(configuration.isOverwriteTarget()).build()));

        mockDirectoryManager.expects(once()).method("addAllUsersToGroup").with(eq(directoryID), eq(principalNames2), eq(remoteGroup2.getName()))
                .will(returnValue(BulkAddResult.builder(principalNames2.size()).setOverwrite(configuration.isOverwriteTarget()).build()));

        Result result = baseImporter.importUserMemberships(userToGroupMemberships, configuration);

        assertEquals(new Long(3), result.getGroupMembershipsImported());
    }

    public void testImportGroupPrincipalMembershipsWithSingleFailure() throws ImporterException
    {
        // test if one membership fails in batch, it is reported correctly and the others succeed

        mockDirectoryManager.expects(once()).method("addAllUsersToGroup").with(eq(directoryID), eq(principalNames1), eq(remoteGroup1.getName()))
                .will(returnValue(BulkAddResult.builder(principalNames1.size()).setOverwrite(configuration.isOverwriteTarget()).build()));

        mockDirectoryManager.expects(once()).method("addAllUsersToGroup").with(eq(directoryID), eq(principalNames2), eq(remoteGroup2.getName()))
                .will(returnValue(BulkAddResult.builder(principalNames2.size())
                        .setOverwrite(configuration.isOverwriteTarget())
                        .addFailedEntity(user2b.getName())
                        .build()));

        Result result = baseImporter.importUserMemberships(userToGroupMemberships, configuration);

        assertEquals(1, result.getGroupMembershipsFailedImport().size());
        assertEquals(new Long(2), result.getGroupMembershipsImported());
        assertTrue(result.getGroupMembershipsFailedImport().contains(groupMembership2b.getRelationship()));
    }

    public void testImportGroupPrincipalMembershipsWithSingleGroupNotFound() throws ImporterException
    {
        // test if one group fails, other groups can succeed

        mockDirectoryManager.expects(once()).method("addAllUsersToGroup").with(eq(directoryID), eq(principalNames1), eq(remoteGroup1.getName()))
                .will(returnValue(BulkAddResult.builder(principalNames1.size()).setOverwrite(configuration.isOverwriteTarget()).build()));

        mockDirectoryManager.expects(once()).method("addAllUsersToGroup").with(eq(directoryID), eq(principalNames2), eq(remoteGroup2.getName()))
                .will(throwException(new GroupNotFoundException(remoteGroup2.getName())));

        Result result = baseImporter.importUserMemberships(userToGroupMemberships, configuration);

        assertEquals(2, result.getGroupMembershipsFailedImport().size());
        assertEquals(new Long(1), result.getGroupMembershipsImported());
        assertTrue(result.getGroupMembershipsFailedImport().contains(groupMembership2.getRelationship()));
        assertTrue(result.getGroupMembershipsFailedImport().contains(groupMembership2b.getRelationship()));
    }

    public void testImportGroupPrincipalMembershipsWhereMembershipsIsEmpty() throws ImporterException
    {
        Result resultCount = baseImporter.importUserMemberships(Collections.<MembershipDTO>emptyList(), configuration);

        // Expect that no memberships were imported
        assertEquals(new Long(0), resultCount.getGroupMembershipsImported());
    }

    public void testImportGroupPrincipalMembershipsWithPermissionError() throws ImporterException
    {
        mockDirectoryManager.expects(atMostOnce()).method("addAllUsersToGroup").with(eq(directoryID), eq(principalNames1), eq(remoteGroup1.getName()))
                .will(returnValue(BulkAddResult.builder(principalNames1.size()).setOverwrite(configuration.isOverwriteTarget()).build()));

        mockDirectoryManager.expects(once()).method("addAllUsersToGroup").with(eq(directoryID), eq(principalNames2), eq(remoteGroup2.getName()))
                .will(throwException(new DirectoryPermissionException()));

        try
        {
            baseImporter.importUserMemberships(userToGroupMemberships, configuration);
            fail("ImporterException expected");
        }
        catch (ImporterException e)
        {
            // expected
        }
    }

    public void testImportRemoteGroupThatExists() throws ImporterException
    {
        mockDirectoryManager.expects(once()).method("addAllGroups").with(eq(directoryID), eq(groups), eq(configuration.isOverwriteTarget()))
                .will(returnValue(BulkAddResult.builder(groups.size())
                        .setOverwrite(configuration.isOverwriteTarget())
                        .addExistingEntity(remoteGroup1)
                        .build()));

        Result result = baseImporter.importGroups(groups, configuration);

        assertEquals(1, result.getGroupsAlreadyExist().size());
        assertEquals(new Long(3), result.getGroupsImported());
        assertTrue(result.getGroupsAlreadyExist().contains(remoteGroup1.getName()));
    }

    public void testImportRemoteGroups() throws ImporterException
    {
        mockDirectoryManager.expects(once()).method("addAllGroups").with(eq(directoryID), eq(groups), eq(configuration.isOverwriteTarget()))
                .will(returnValue(BulkAddResult.builder(groups.size()).setOverwrite(configuration.isOverwriteTarget()).build()));

        Result result = baseImporter.importGroups(groups, configuration);

        assertEquals(0, result.getGroupsAlreadyExist().size());
        assertEquals(new Long(4), result.getGroupsImported());
    }

    public void testImportRemoteGroupsWhereGroupsIsEmpty() throws ImporterException
    {
        mockDirectoryManager.expects(once()).method("addAllGroups").with(eq(directoryID), eq(Collections.emptyList()), eq(configuration.isOverwriteTarget()))
                .will(returnValue(BulkAddResult.builder(0).setOverwrite(configuration.isOverwriteTarget()).build()));

        Result result = baseImporter.importGroups(Collections.<GroupTemplate>emptyList(), configuration);

        // Expect that no groups were imported
        assertEquals(new Long(0), result.getGroupsImported());
        assertEquals(0, result.getGroupsAlreadyExist().size());
    }

    public void testImportGroupsWithSingleFailure() throws ImporterException
    {
        mockDirectoryManager.expects(once()).method("addAllGroups").with(eq(directoryID), eq(groups), eq(configuration.isOverwriteTarget()))
                .will(returnValue(BulkAddResult.builder(groups.size())
                        .setOverwrite(configuration.isOverwriteTarget())
                        .addFailedEntity(remoteGroup1)
                        .build()));

        Result result = baseImporter.importGroups(groups, configuration);

        assertEquals(1, result.getGroupsFailedImport().size());
        assertEquals(new Long(3), result.getGroupsImported());
        assertTrue(result.getGroupsFailedImport().contains(remoteGroup1.getName()));
    }

    public void testImportGroupsWithNoPermission() throws ImporterException
    {
        mockDirectoryManager.expects(once()).method("addAllGroups").with(eq(directoryID), eq(groups), eq(configuration.isOverwriteTarget())).will(throwException(new DirectoryPermissionException()));

        try
        {
            baseImporter.importGroups(groups, configuration);
            fail("ImporterException expected");
        }
        catch (ImporterException e)
        {
            // expected
        }
    }

    public void testImportRemotePrincipalThatExists() throws ImporterException
    {
        mockDirectoryManager.expects(once()).method("addAllUsers").with(eq(directoryID), eq(principals), eq(configuration.isOverwriteTarget()))
                .will(returnValue(BulkAddResult.builder(principals.size())
                        .setOverwrite(configuration.isOverwriteTarget())
                        .addExistingEntity(user1)
                        .build()));

        Result result = baseImporter.importUsers(principals, configuration);

        assertEquals(1, result.getUsersAlreadyExist().size());
        assertEquals(new Long(2), result.getUsersImported());
        assertTrue(result.getUsersAlreadyExist().contains(user1.getName()));
    }

    public void testImportRemotePrincipals() throws ImporterException
    {
        mockDirectoryManager.expects(once()).method("addAllUsers").with(eq(directoryID), eq(principals), eq(configuration.isOverwriteTarget()))
                .will(returnValue(BulkAddResult.builder(principals.size()).setOverwrite(configuration.isOverwriteTarget()).build()));

        Result result = baseImporter.importUsers(principals, configuration);

        assertEquals(0, result.getUsersAlreadyExist().size());
        assertEquals(new Long(3), result.getUsersImported());
    }

    public void testImportRemotePrincipalsWherePrincipalsIsEmpty() throws ImporterException
    {
        mockDirectoryManager.expects(once()).method("addAllUsers").with(eq(directoryID), eq(Collections.emptyList()), eq(configuration.isOverwriteTarget()))
                .will(returnValue(BulkAddResult.builder(0).setOverwrite(configuration.isOverwriteTarget()).build()));

        Result result = baseImporter.importUsers(Collections.<UserTemplateWithCredentialAndAttributes>emptyList(), configuration);

        // Expect that no principals were imported
        assertEquals(new Long(0), result.getUsersImported());
        assertEquals(0, result.getUsersAlreadyExist().size());
    }

    public void testImportUsersWithSingleFailure() throws ImporterException
    {
        mockDirectoryManager.expects(once()).method("addAllUsers").with(eq(directoryID), eq(principals), eq(configuration.isOverwriteTarget()))
                .will(returnValue(BulkAddResult.builder(principals.size())
                        .setOverwrite(configuration.isOverwriteTarget())
                        .addFailedEntity(user1)
                        .build()));

        Result result = baseImporter.importUsers(principals, configuration);

        assertEquals(1, result.getUsersFailedImport().size());
        assertEquals(new Long(2), result.getUsersImported());
        assertTrue(result.getUsersFailedImport().contains(user1.getName()));
    }

    public void testImportUsersWithNoPermission() throws ImporterException
    {
        mockDirectoryManager.expects(once()).method("addAllUsers").with(eq(directoryID), eq(principals), eq(configuration.isOverwriteTarget())).will(throwException(new DirectoryPermissionException()));

        try
        {
            baseImporter.importUsers(principals, configuration);
            fail("ImporterException expected");
        }
        catch (ImporterException e)
        {
            // expected
        }
    }

    public void testImportUsersAndGroupsWithIncorrectConfiguration() throws ImporterException
    {
        try
        {
            JdbcImporter importer = new JiraLegacyImporter(null);
            importer.importUsersGroupsAndMemberships(new CsvConfiguration());

            fail("Config should not be allowed for a JDBC (Jira) based importer");
        }
        catch (IllegalArgumentException e)
        {
            // we should be here
        }
    }

    public void testImportUsersAndGroupsWithNullConfiguration() throws ImporterException
    {
        try
        {
            baseImporter.importUsersGroupsAndMemberships(null);
            fail("Null is not allowed as a parameter");
        }
        catch (IllegalArgumentException e)
        {
            // we should be here
        }
    }

    public void testImportUsersGroupsNoNestedGroups() throws ImporterException
    {
        // principals
        mockDirectoryManager.expects(once()).method("addAllUsers").with(eq(directoryID), eq(principals), eq(configuration.isOverwriteTarget()))
                .will(returnValue(BulkAddResult.builder(principals.size()).setOverwrite(configuration.isOverwriteTarget()).build()));

        // groups
        mockDirectoryManager.expects(once()).method("addAllGroups").with(eq(directoryID), eq(groups), eq(configuration.isOverwriteTarget()))
                .will(returnValue(BulkAddResult.builder(groups.size()).setOverwrite(configuration.isOverwriteTarget()).build()));

        // group memberships
        mockDirectoryManager.expects(once()).method("addAllUsersToGroup").with(eq(directoryID), eq(principalNames1), eq(remoteGroup1.getName()))
                .will(returnValue(BulkAddResult.builder(principalNames1.size()).setOverwrite(configuration.isOverwriteTarget()).build()));

        mockDirectoryManager.expects(once()).method("addAllUsersToGroup").with(eq(directoryID), eq(principalNames2), eq(remoteGroup2.getName()))
                .will(returnValue(BulkAddResult.builder(principalNames2.size()).setOverwrite(configuration.isOverwriteTarget()).build()));

        final Result result = baseImporter.importUsersGroupsAndMemberships(configuration);

        assertNotNull(result);
        assertEquals(new Long(3), result.getUsersImported());
        assertEquals(new Long(4), result.getGroupsImported());
        assertEquals(new Long(3), result.getGroupMembershipsImported());
        assertEquals(0, result.getUsersFailedImport().size());
        assertEquals(0, result.getGroupsFailedImport().size());
        assertEquals(0, result.getGroupMembershipsFailedImport().size());
        assertEquals(0, result.getUsersAlreadyExist().size());
        assertEquals(0, result.getGroupsAlreadyExist().size());
    }

    public void testImportUsersGroupsWithNestedGroups() throws ImporterException
    {
        // principals
        mockDirectoryManager.expects(once()).method("addAllUsers").with(eq(directoryID), eq(principals), eq(configurationWithNestedGroupOn.isOverwriteTarget()))
                .will(returnValue(BulkAddResult.builder(principals.size()).setOverwrite(configurationWithNestedGroupOn.isOverwriteTarget()).build()));

        // groups
        mockDirectoryManager.expects(once()).method("addAllGroups").with(eq(directoryID), eq(groups), eq(configurationWithNestedGroupOn.isOverwriteTarget()))
                .will(returnValue(BulkAddResult.builder(groups.size()).setOverwrite(configurationWithNestedGroupOn.isOverwriteTarget()).build()));

        // group memberships
        mockDirectoryManager.expects(once()).method("addAllUsersToGroup").with(eq(directoryID), eq(principalNames1), eq(remoteGroup1.getName()))
                .will(returnValue(BulkAddResult.builder(principalNames1.size()).setOverwrite(configurationWithNestedGroupOn.isOverwriteTarget()).build()));

        mockDirectoryManager.expects(once()).method("addAllUsersToGroup").with(eq(directoryID), eq(principalNames2), eq(remoteGroup2.getName()))
                .will(returnValue(BulkAddResult.builder(principalNames2.size()).setOverwrite(configurationWithNestedGroupOn.isOverwriteTarget()).build()));

        mockDirectoryManager.expects(once()).method("addGroupToGroup").with(eq(directoryID), eq(CHILD_GROUP_1), eq(TEST_GROUP_1));

        mockDirectoryManager.expects(once()).method("addGroupToGroup").with(eq(directoryID), eq(CHILD_GROUP_2), eq(TEST_GROUP_1));

        final Result result = baseImporter.importUsersGroupsAndMemberships(configurationWithNestedGroupOn);

        assertNotNull(result);
        assertEquals(new Long(3), result.getUsersImported());
        assertEquals(new Long(4), result.getGroupsImported());
        assertEquals(new Long(5), result.getGroupMembershipsImported());
        assertEquals(0, result.getUsersFailedImport().size());
        assertEquals(0, result.getGroupsFailedImport().size());
        assertEquals(0, result.getGroupMembershipsFailedImport().size());
        assertEquals(0, result.getUsersAlreadyExist().size());
        assertEquals(0, result.getGroupsAlreadyExist().size());
    }

    private class MockBaseImporter extends BaseImporter
    {
        public MockBaseImporter(DirectoryManager directoryManager)
        {
            super(directoryManager);
        }

        public Collection<MembershipDTO> findUserToGroupMemberships(Configuration configuration) throws ImporterException
        {
            return userToGroupMemberships;
        }

        public Collection<MembershipDTO> findGroupToGroupMemberships(Configuration configuration) throws ImporterException
        {
            return groupToGroupMemberships;
        }

        public Collection<GroupTemplate> findGroups(Configuration configuration) throws ImporterException
        {
            return groups;
        }

        public Collection<UserTemplateWithCredentialAndAttributes> findUsers(Configuration configuration) throws ImporterException
        {
            return principals;
        }

        public void init(Configuration configuration)
        {
            // do nothing
        }

        public Class getConfigurationType()
        {
            return Configuration.class;
        }
    }
}