package com.atlassian.crowd.migration.legacy.database;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.lang3.StringUtils;

public class DatabaseMapperUtil
{
    public String getDbUrl() throws URISyntaxException
    {
        final URL resource = getClass().getResource("crowd15db.script");

        // This will fail if the resource isn't a local file
        String crowddbLocation = StringUtils.removeEnd(new File(resource.toURI()).getPath(), ".script");
        return "jdbc:hsqldb:" + crowddbLocation;
    }
}
