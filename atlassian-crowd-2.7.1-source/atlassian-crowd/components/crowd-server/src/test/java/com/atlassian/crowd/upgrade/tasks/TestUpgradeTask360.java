package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.token.TokenManager;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

public class TestUpgradeTask360 extends MockObjectTestCase
{
    private UpgradeTask360 upgradeTask;
    private Mock mockTokenManager;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        upgradeTask = new UpgradeTask360();

        mockTokenManager = new Mock(TokenManager.class);

        upgradeTask.setTokenManager((TokenManager) mockTokenManager.proxy());

    }

    public void testDoUpgrade() throws Exception
    {
        mockTokenManager.expects(once()).method("removeAll");

        upgradeTask.doUpgrade();

        verify();
    }
}
