package com.atlassian.crowd.service.soap;

import java.io.StringReader;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import com.atlassian.crowd.service.soap.SafeXmlXFireServletControllerAdapter;

import org.codehaus.xfire.MessageContext;
import org.codehaus.xfire.XFire;
import org.codehaus.xfire.service.ServiceRegistry;
import org.codehaus.xfire.transport.TransportManager;
import org.codehaus.xfire.util.STAXUtils;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SafeXmlXFireServletControllerAdapterTest
{
    static String BILLION_LAUGHS = "<?xml version='1.0' encoding='utf-8' ?> <!DOCTYPE lolz [ <!ENTITY lol 'lol'> <!ENTITY lol2 '&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;&lol;'> <!ENTITY lol3 '&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;&lol2;'> <!ENTITY lol4 '&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;&lol3;'> <!ENTITY lol5 '&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;&lol4;'> <!ENTITY lol6 '&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;&lol5;'> <!ENTITY lol7 '&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;&lol6;'> <!ENTITY lol8 '&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;&lol7;'> <!ENTITY lol9 '&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;&lol8;'> ]><x>&lol9;</x>";

    static String NONEXISTENT_FILE_ENTITY = "<!DOCTYPE soapenv:Envelope [<!ENTITY readme SYSTEM '/no-such-file'>]><x>&readme;</x>";

    static String EXTERNAL_DTD = "<!DOCTYPE root SYSTEM '/no-such-file'> <root/>";

    static String NONEXISTENT_FILE_PARAMETER_ENTITY = "<!DOCTYPE soapenv:Envelope [<!ENTITY % readme SYSTEM '/no-such-file'> %readme;]><x/>";

    static XMLInputFactory getInputFactory()
    {
        TransportManager tm = mock(TransportManager.class);
        ServiceRegistry sr = mock(ServiceRegistry.class);

        XFire xfire = mock(XFire.class);
        when(xfire.getTransportManager()).thenReturn(tm);
        when(xfire.getServiceRegistry()).thenReturn(sr);

        SafeXmlXFireServletControllerAdapter a = new SafeXmlXFireServletControllerAdapter(xfire, null, null);

        MessageContext mc = a.createMessageContext(null, null, null);

        return STAXUtils.getXMLInputFactory(mc);
    }

    @Test
    public void inputFactoryIsCreated()
    {
        assertNotNull(getInputFactory());
    }

    private static XMLStreamReader createReader(String content) throws XMLStreamException
    {
        return getInputFactory().createXMLStreamReader(new StringReader(content));
    }

    static String appendCharacters(XMLStreamReader sr) throws XMLStreamException
    {
        StringBuilder text = new StringBuilder();

        while (sr.next() != XMLStreamConstants.END_DOCUMENT)
        {
            if (sr.getEventType() == XMLStreamConstants.CHARACTERS)
            {
                if (sr.hasText())
                {
                    text.append(sr.getText());
                }
            }
        }

        return text.toString();
    }

    @Test
    public void parseDocumentExpandsAmpersand() throws Exception
    {
        XMLStreamReader sr = createReader("<x>&amp;</x>");

        assertEquals("&", appendCharacters(sr));
    }

    /**
     * If the parser spends too long on parsing this document, there's a problem.
     * Different fixes for the case may behave differently.
     */
    @Test(timeout = 5000)
    public void parseBillionLaughsDoesNotExhaustTime() throws Exception
    {
        XMLStreamReader sr = createReader(BILLION_LAUGHS);

        try
        {
            /* It's okay for the parser to ignore the entities */
            assertEquals("", appendCharacters(sr));
        }
        catch (XMLStreamException e)
        {
            /* Or to throw a specific exception */
        }
    }

    @Test(expected = XMLStreamException.class)
    public void externalEntityIsNotRead() throws Exception
    {
        XMLStreamReader sr = createReader(NONEXISTENT_FILE_ENTITY);

        appendCharacters(sr);
    }

    @Test
    public void externalDtdIsNotRead() throws Exception
    {
        XMLStreamReader sr = createReader(EXTERNAL_DTD);

        String text = appendCharacters(sr);

        assertEquals("", text);
    }

    @Test
    public void externalParameterEntityIsNotRead() throws Exception
    {
        XMLStreamReader sr = createReader(NONEXISTENT_FILE_PARAMETER_ENTITY);

        String text = appendCharacters(sr);

        assertEquals("", text);
    }
}
