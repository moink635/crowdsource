package com.atlassian.crowd.migration;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.dao.membership.MembershipDAOHibernate;
import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.membership.InternalMembership;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.HibernateOperation;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hamcrest.core.IsCollectionContaining;
import org.hibernate.SessionFactory;
import org.mockito.ArgumentCaptor;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static com.atlassian.crowd.migration.MembershipMapper.MEMBERSHIP_XML_ROOT;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class MembershipMapperTest extends BaseMapperTest
{
    private DirectoryDAOHibernate directoryDAOHibernate;
    private BatchProcessor batchProcessor;
    private MembershipDAOHibernate membershipDAOHibernate;
    private UserDAOHibernate userDAOHibernate;
    private GroupDAOHibernate groupDAOHibernate;
    private DirectoryManager directoryManager;
    private MembershipMapper mapper;
    private Element membershipRoot;

    protected void setUp() throws Exception
    {
        super.setUp();

        directoryDAOHibernate = mock(DirectoryDAOHibernate.class);

        // These xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        batchProcessor = mock(BatchProcessor.class);
        membershipDAOHibernate = mock(MembershipDAOHibernate.class);
        userDAOHibernate = mock(UserDAOHibernate.class);
        groupDAOHibernate = mock(GroupDAOHibernate.class);
        directoryManager = mock(DirectoryManager.class);
        mapper = new MembershipMapper(sessionFactory, batchProcessor, membershipDAOHibernate, directoryDAOHibernate,
                                      userDAOHibernate, groupDAOHibernate, directoryManager);

        membershipRoot = (Element) document.getRootElement().selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + MEMBERSHIP_XML_ROOT);
    }

    public void testImportAndExportFromXML() throws Exception
    {
        when(directoryDAOHibernate.loadReference(1L)).thenReturn(DIRECTORY1);

        Element groupElement = (Element) this.membershipRoot.elementIterator().next();
        InternalMembership internalMembership = mapper.getMembershipFromXml(groupElement);

        assertNotNull(internalMembership);

        assertEquals("Admin", internalMembership.getChildName());
        assertEquals("Crowd-Administrators", internalMembership.getParentName());
        assertEquals(MembershipType.GROUP_USER, internalMembership.getMembershipType());
        assertEquals(1L, internalMembership.getDirectory().getId().longValue());

        // test export
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement(XmlMigrationManagerImpl.XML_ROOT);
        Element membershipRoot = root.addElement(MEMBERSHIP_XML_ROOT);

        mapper.addMembershipToXml(internalMembership, membershipRoot);

        String export = exportDocumentToString(document);

        assertEquals(getTestExportXmlString(), export);
    }

    public void testExportXml_OnlyNonReplicatedMemberships() throws ExportException
    {
        InternalMembership membership1 = createMembership(10L, DIRECTORY1); // internal
        InternalMembership membership2 = createMembership(20L, DIRECTORY2); // connector
        InternalMembership membership3 = createMembership(30L, DIRECTORY3); // delegating
        EntityQuery<Directory> exportableDirectoryQuery =
            QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                .with(Combine.anyOf(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.INTERNAL),
                                    Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.DELEGATING),
                                    Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.CONNECTOR)))
                .returningAtMost(EntityQuery.ALL_RESULTS);

        when(directoryManager.searchDirectories(exportableDirectoryQuery))
            .thenReturn(ImmutableList.<Directory>of(DIRECTORY1, DIRECTORY2, DIRECTORY3));

        // the matcher type requires a cast due to http://code.google.com/p/mockito/issues/detail?id=297
        when(membershipDAOHibernate.findAll((Collection<Directory>)argThat(IsCollectionContaining.<Directory>hasItem(DIRECTORY1))))
            .thenReturn(ImmutableList.of(membership1));
        when(membershipDAOHibernate.findAllLocal((Collection<Directory>)argThat(
            IsCollectionContaining.<Directory>hasItems(DIRECTORY2, DIRECTORY3))))
            .thenReturn(ImmutableList.of(membership2, membership3));

        final Element root = mapper.exportXml(null);

        assertEquals(ImmutableList.of("1", "2", "3"), projectEntitiesSubelement(root, "membership", "directoryId"));
    }

    public void testImportXml_OnlyNonReplicatedMemberships() throws ImportException
    {
        final InternalMembership membership1 = createMembership(10L, DIRECTORY1); // internal
        final InternalMembership membership2 = createMembership(20L, DIRECTORY2); // connector
        final InternalMembership membership3 = createMembership(30L, DIRECTORY3); // delegating
        final InternalMembership membership4 = createMembership(40L, DIRECTORY4); // remote crowd

        when(directoryDAOHibernate.loadReference(1L)).thenReturn(DIRECTORY1);
        when(directoryDAOHibernate.loadReference(2L)).thenReturn(DIRECTORY2);
        when(directoryDAOHibernate.loadReference(3L)).thenReturn(DIRECTORY3);
        when(directoryDAOHibernate.loadReference(4L)).thenReturn(DIRECTORY4);
        when(batchProcessor.execute(any(HibernateOperation.class), any(List.class))).thenReturn(new BatchResult(0));

        final Element root = DocumentHelper.createDocument().addElement("crowd");
        final Element memberships = root.addElement("memberships");
        memberships.add(createElement(membership1));
        memberships.add(createElement(membership2));
        memberships.add(createElement(membership3));
        memberships.add(createElement(membership4));

        mapper.importXml(root);

        verify(batchProcessor).execute(any(HibernateOperation.class),
                                       eq(ImmutableList.of(membership1, membership2, membership3)));
    }

    public void testImportXml_LdapUserMemberOfLocalGroup() throws Exception
    {
        when(directoryDAOHibernate.loadReference(2L)).thenReturn(DIRECTORY2);

        final InternalGroup localGroup = createGroup(10L, "local-group", DIRECTORY2);
        localGroup.setLocal(true);

        final InternalUser ldapUser = createUser(20L, "ldap-user", DIRECTORY2);

        final InternalMembership membership =
            new InternalMembership(30L, localGroup.getId(), ldapUser.getId(), MembershipType.GROUP_USER,
                                   GroupType.GROUP, "local-group", "ldap-user", DIRECTORY2);

        final Element root = DocumentHelper.createDocument().addElement("crowd");
        final Element memberships = root.addElement("memberships");
        memberships.add(createElement(membership));

        // the user does not exist in the directory yet (it is a cached one from the LDAP directory)
        when(userDAOHibernate.findByName(DIRECTORY2.getId(), "ldap-user"))
            .thenThrow(new UserNotFoundException("ldap-user"));

        when(batchProcessor.execute(any(HibernateOperation.class), any(List.class))).thenReturn(new BatchResult(0));

        mapper.importXml(root);

        ArgumentCaptor<Collection> entityCaptor = ArgumentCaptor.forClass(Collection.class);
        verify(batchProcessor, times(2)).execute(any(HibernateOperation.class), entityCaptor.capture());

        // assert that the first batch inserted a placeholder user
        final Collection placeholderUsers = entityCaptor.getAllValues().get(0);
        InternalUser capturedUser = (InternalUser) Iterables.getOnlyElement(placeholderUsers);
        assertEquals("ldap-user", capturedUser.getName());
        assertEquals(DIRECTORY2.getId().longValue(), capturedUser.getDirectoryId());
        assertTrue("Placeholder user should be active", capturedUser.isActive());
        assertEquals(PasswordCredential.NONE, capturedUser.getCredential());
        assertNull(capturedUser.getDisplayName());
        assertNull(capturedUser.getFirstName());
        assertNull(capturedUser.getLastName());
        assertNull(capturedUser.getEmailAddress());

        // assert that the second batch inserted the actual membership
        assertEquals(ImmutableList.of(membership), entityCaptor.getAllValues().get(1));
    }

    public void testImportXml_localGroupMemberOfLdapGroup() throws Exception
    {
        when(directoryDAOHibernate.loadReference(2L)).thenReturn(DIRECTORY2);

        final InternalGroup localGroup = createGroup(10L, "local-group", DIRECTORY2);
        localGroup.setLocal(true);

        final InternalGroup ldapGroup = createGroup(20L, "ldap-group", DIRECTORY2);

        final InternalMembership membership =
            new InternalMembership(30L, ldapGroup.getId(), localGroup.getId(), MembershipType.GROUP_GROUP,
                                   GroupType.GROUP, "ldap-group", "local-group", DIRECTORY2);

        final Element root = DocumentHelper.createDocument().addElement("crowd");
        final Element memberships = root.addElement("memberships");
        memberships.add(createElement(membership));

        // the ldap group does not exist in the directory yet (it is a cached one from the LDAP directory)
        when(groupDAOHibernate.findByName(DIRECTORY2.getId(), "ldap-group"))
            .thenThrow(new GroupNotFoundException("ldap-group"));
        when(groupDAOHibernate.findByName(DIRECTORY2.getId(), "local-group")).thenReturn(localGroup);

        when(batchProcessor.execute(any(HibernateOperation.class), any(List.class))).thenReturn(new BatchResult(0));

        mapper.importXml(root);

        ArgumentCaptor<Collection> entityCaptor = ArgumentCaptor.forClass(Collection.class);
        verify(batchProcessor, times(2)).execute(any(HibernateOperation.class), entityCaptor.capture());

        // assert that the first batch inserted a placeholder group
        final Collection placeholderGroups = entityCaptor.getAllValues().get(0);
        InternalGroup capturedGroup = (InternalGroup) Iterables.getOnlyElement(placeholderGroups);
        assertEquals(ldapGroup.getId(), capturedGroup.getId());
        assertEquals("ldap-group", capturedGroup.getName());
        assertEquals(DIRECTORY2.getId().longValue(), capturedGroup.getDirectoryId());
        assertTrue("Placeholder group should be active", capturedGroup.isActive());

        // assert that the second batch inserted the actual membership
        assertEquals(ImmutableList.of(membership), entityCaptor.getAllValues().get(1));
    }

    public void testImportXml_ldapGroupMemberOfLocalGroup() throws Exception
    {
        when(directoryDAOHibernate.loadReference(2L)).thenReturn(DIRECTORY2);

        final InternalGroup localGroup = createGroup(10L, "local-group", DIRECTORY2);
        localGroup.setLocal(true);

        final InternalGroup ldapGroup = createGroup(20L, "ldap-group", DIRECTORY2);

        final InternalMembership membership =
            new InternalMembership(30L, localGroup.getId(), ldapGroup.getId(), MembershipType.GROUP_GROUP,
                                   GroupType.GROUP, "local-group", "ldap-group", DIRECTORY2);

        final Element root = DocumentHelper.createDocument().addElement("crowd");
        final Element memberships = root.addElement("memberships");
        memberships.add(createElement(membership));

        // the ldap group does not exist in the directory yet (it is a cached one from the LDAP directory)
        when(groupDAOHibernate.findByName(DIRECTORY2.getId(), "ldap-group"))
            .thenThrow(new GroupNotFoundException("ldap-group"));
        when(groupDAOHibernate.findByName(DIRECTORY2.getId(), "local-group")).thenReturn(localGroup);

        when(batchProcessor.execute(any(HibernateOperation.class), any(List.class))).thenReturn(new BatchResult(0));

        mapper.importXml(root);

        ArgumentCaptor<Collection> entityCaptor = ArgumentCaptor.forClass(Collection.class);
        verify(batchProcessor, times(2)).execute(any(HibernateOperation.class), entityCaptor.capture());

        // assert that the first batch inserted a placeholder group
        final Collection placeholderGroups = entityCaptor.getAllValues().get(0);
        InternalGroup capturedGroup = (InternalGroup) Iterables.getOnlyElement(placeholderGroups);
        assertEquals(ldapGroup.getId(), capturedGroup.getId());
        assertEquals("ldap-group", capturedGroup.getName());
        assertEquals(DIRECTORY2.getId().longValue(), capturedGroup.getDirectoryId());
        assertTrue("Placeholder group should be active", capturedGroup.isActive());

        // assert that the second batch inserted the actual membership
        assertEquals(ImmutableList.of(membership), entityCaptor.getAllValues().get(1));
    }

    private InternalUser createUser(long id, String name, Directory directory)
    {
        final UserTemplate userTemplate = new UserTemplate(name, "Test", "User", "Test User");
        userTemplate.setDirectoryId(directory.getId());
        userTemplate.setEmailAddress("test@example.com");
        final InternalEntityTemplate internalEntityTemplate =
            new InternalEntityTemplate(id, name, true, new Date(), new Date());
        return new InternalUser(internalEntityTemplate, directory, userTemplate,
                                PasswordCredential.encrypted("password"));
    }

    private InternalGroup createGroup(long id, String name, Directory directory)
    {
        GroupTemplate groupTemplate = new GroupTemplate(name, directory.getId());
        groupTemplate.setDescription("Group description");
        InternalGroup localGroup = new InternalGroup(new InternalEntityTemplate(id, name, true, new Date(), new Date()), directory, groupTemplate);
        return localGroup;
    }

    private Element createElement(InternalMembership membership)
    {
        final Element element = DocumentHelper.createElement("membership");
        element.addElement("id").setText(membership.getId().toString());
        element.addElement("directoryId").setText(Long.toString(membership.getDirectory().getId()));
        element.addElement("parentName").setText(membership.getParentName());
        element.addElement("childName").setText(membership.getChildName());
        element.addElement("parentId").setText(membership.getParentId().toString());
        element.addElement("childId").setText(membership.getChildId().toString());
        element.addElement("membershipType").setText(membership.getMembershipType().toString());
        element.addElement("groupType").setText(membership.getGroupType().toString());
        return element;
    }

    private InternalMembership createMembership(Long id, DirectoryImpl directory)
    {
        return new InternalMembership(id, 20L, 30L, MembershipType.GROUP_USER, GroupType.GROUP, "parent", "child", directory);
    }
}
