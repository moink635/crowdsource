package com.atlassian.crowd.event.listener;

import com.atlassian.crowd.event.directory.DirectoryDeletedEvent;
import com.atlassian.crowd.event.user.UserDeletedEvent;
import com.atlassian.crowd.event.user.UserRenamedEvent;
import com.atlassian.crowd.manager.token.TokenManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.user.User;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserDirectoryTokenRemovalListenerTest
{
    private static final Long DIRECTORY_ID = 1L;

    @InjectMocks
    private UserDirectoryTokenRemovalListener listener;

    @Mock
    private TokenManager tokenManager;

    @Test
    public void testHandleDirectoryDeleted()
    {
        DirectoryImpl directory = mock(DirectoryImpl.class);
        when(directory.getId()).thenReturn(DIRECTORY_ID);

        DirectoryDeletedEvent event = new DirectoryDeletedEvent(this, directory);

        listener.handleEvent(event);

        verify(tokenManager).removeAll(DIRECTORY_ID);
    }

    @Test
    public void testHandleUserDeleted()
    {
        DirectoryImpl directory = mock(DirectoryImpl.class);
        when(directory.getId()).thenReturn(DIRECTORY_ID);

        UserDeletedEvent event = new UserDeletedEvent(this, directory, "username");

        listener.handleEvent(event);

        verify(tokenManager).remove(DIRECTORY_ID, "username");
    }

    @Test
    public void testHandleUserRenamed()
    {
        DirectoryImpl directory = mock(DirectoryImpl.class);
        when(directory.getId()).thenReturn(DIRECTORY_ID);

        User user = mock(User.class);
        UserRenamedEvent event = new UserRenamedEvent(this, directory, user, "oldname");

        listener.handleEvent(event);

        verify(tokenManager).remove(DIRECTORY_ID, "oldname");
        verifyZeroInteractions(user);
    }
}
