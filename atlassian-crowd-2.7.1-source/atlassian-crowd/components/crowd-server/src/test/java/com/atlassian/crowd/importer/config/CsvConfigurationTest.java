package com.atlassian.crowd.importer.config;

import com.atlassian.crowd.importer.exceptions.ImporterConfigurationException;
import junit.framework.TestCase;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * CsvConfiguration Tester.
 */
public class CsvConfigurationTest extends TestCase
{
    private CsvConfiguration csvConfiguration = null;
    private static final String TEST_TARGET_ROOT = "target/CsvConfigurationTest";
    private File groupFile;
    private File principalFile;
    private File rootDirectory;

    public void setUp() throws Exception
    {
        super.setUp();

        rootDirectory = new File(TEST_TARGET_ROOT);
        rootDirectory.mkdirs();
        rootDirectory.deleteOnExit();

        groupFile = new File(TEST_TARGET_ROOT + "/csvgrouptest.csv");
        groupFile.deleteOnExit();
        StringBuffer groups = new StringBuffer();
        groups.append("\"Username\", \"Group name\"").append("\n").append("\"justinkoke\",\"administrators\"").append("\n");
        FileUtils.writeStringToFile(groupFile, groups.toString(), "UTF-8");

        principalFile = new File(TEST_TARGET_ROOT + "/csvprincipaltest.csv");
        principalFile.deleteOnExit();
        StringBuffer principals = new StringBuffer();
        principals.append("\"First Name\", \"Last Name\",\"Email Address\",\"Username\",\"Password\"").append("\n").
                append("\"Justin\",\"Koke\",\"justin@atlassian.com\",\"justinkoke\",\"password\"").append("\n");
        FileUtils.writeStringToFile(principalFile, principals.toString(), "UTF-8");

        csvConfiguration = new CsvConfiguration();
        csvConfiguration.setDelimiter(new Character(','));
    }

    public void tearDown() throws Exception
    {
        csvConfiguration = null;
        groupFile = null;
        principalFile = null;

        super.tearDown();
    }

    public void testBuildGroupMappings() throws IOException
    {
        csvConfiguration.buildSampleGroupMapping(groupFile);

        assertNotNull(csvConfiguration.getGroupHeaderRow());

        List groupHeader = csvConfiguration.getGroupHeaderRow();
        assertEquals("Username", groupHeader.get(0));
        assertEquals("Group name", groupHeader.get(1));

        assertNotNull(csvConfiguration.getGroupSampleRow());
        List groupSample = csvConfiguration.getGroupSampleRow();
        assertEquals("justinkoke", groupSample.get(0));
        assertEquals("administrators", groupSample.get(1));
    }

    public void testBuildPrincipalMappings() throws IOException
    {
        csvConfiguration.buildSampleUserMapping(principalFile);

        assertNotNull(csvConfiguration.getUserHeaderRow());

        List header = csvConfiguration.getUserHeaderRow();
        assertEquals("First Name", header.get(0));
        assertEquals("Last Name", header.get(1));
        assertEquals("Email Address", header.get(2));
        assertEquals("Username", header.get(3));
        assertEquals("Password", header.get(4));

        assertNotNull(csvConfiguration.getUserSampleRow());
        List sample = csvConfiguration.getUserSampleRow();
        assertEquals("Justin", sample.get(0));
        assertEquals("Koke", sample.get(1));
        assertEquals("justin@atlassian.com", sample.get(2));
        assertEquals("justinkoke", sample.get(3));
        assertEquals("password", sample.get(4));
    }

    public void testValidity()
    {
        CsvConfiguration configuration = new CsvConfiguration();

        try
        {
            configuration.isValid();
            fail("Should have thrown ImporterConfigurationException");
        }
        catch (ImporterConfigurationException e)
        {

        }

        configuration = new CsvConfiguration(new Long(1), "application", Boolean.TRUE, principalFile,
                groupFile, new Character(','), Boolean.TRUE);

        try
        {
            configuration.isValid();
        }
        catch (ImporterConfigurationException e)
        {
            fail("We should have a valid configuration");
        }
    }
}
