package com.atlassian.crowd.manager.upgrade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.atlassian.crowd.dao.token.TokenDAOMemory;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.upgrade.tasks.UpgradeTask;

import com.google.common.collect.ImmutableList;

import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * UpgradeManagerImpl Tester.
 */
public class UpgradeManagerImplTest
{
    private UpgradeManagerImpl upgradeManager;
    private CrowdBootstrapManager bootstrapManager;
    private Integer newBuildNumber;

    // This list will keep the order that the tests were executed in
    private List<String> taskOrderList;
    private PropertyManager mockPropertyManager;

    @Before
    public void setUp() throws Exception
    {
        bootstrapManager = mock(CrowdBootstrapManager.class);

        // A list of upgrade tasks to test against
        List<UpgradeTask> upgradeTasks = new ArrayList<UpgradeTask>();
        upgradeTasks.add(new MockUpgradeTask1());
        upgradeTasks.add(new MockUpgradeTask3());
        upgradeTasks.add(new MockUpgradeTask5());
        upgradeTasks.add(new MockUpgradeTask101());
        upgradeTasks.add(new MockUpgradeTask120());
        upgradeTasks.add(new MockUpgradeTask121());

        // The taskOrderlist is updated via each TestUpgradeTask's doUpgrade()
        taskOrderList = new ArrayList<String>();

        // 120 is the current build of Crowd, equivalent to BuildUtils.BUILD_NUMBER
        newBuildNumber = 120;

        upgradeManager = new UpgradeManagerImpl()
        {
            @Override
            protected int getApplicationBuildNumber()
            {
                return newBuildNumber;
            }

            @Override
            protected void flushAndClearHibernate()
            {
                // do nothing
            }
        };

        upgradeManager.setBootstrapManager(bootstrapManager);
        upgradeManager.setUpgradeTasks(upgradeTasks);

        mockPropertyManager = mock(PropertyManager.class);
        upgradeManager.setPropertyManager(mockPropertyManager);
    }

    @After
    public void tearDown() throws Exception
    {
        upgradeManager = null;
        taskOrderList = null;
    }

    @Test
    public void testDoUpgradeIsNotRunIfTheBuildNumberIsTheLatest() throws Exception
    {
        // Setup the Mock propertyManager and the expectations
        when(mockPropertyManager.getBuildNumber()).thenReturn(new Integer(121));

        newBuildNumber = 121;


        upgradeManager.doUpgrade();
        verify(mockPropertyManager, never()).setBuildNumber(anyInt());
    }

    @Test
    public void testGetCurrentBuildNumberThatDoesNotExistInDB() throws PropertyManagerException
    {
        when(mockPropertyManager.getBuildNumber()).thenThrow(new PropertyManagerException("Failed to find property number"));

        int currentBuildNumber = upgradeManager.getDataBuildNumber();

        assertEquals(0, currentBuildNumber);
        verify(mockPropertyManager).getBuildNumber();
    }

    @Test
    public void testGetUpgradesForCrowdAtVersion103() throws Exception
    {
        // Setup the Mock propertyManager and the expectations
        when(mockPropertyManager.getBuildNumber()).thenReturn(new Integer(103));

        List<UpgradeTask> upgradeTasks = upgradeManager.getRequiredUpgrades();

        assertNotNull(upgradeTasks);
        assertFalse(upgradeTasks.isEmpty());

        // We should have 1 upgrade tasks from test-upgrade.xml
        assertTrue("The total number of upgrade tasks for an install, from 103 to 120", upgradeTasks.size() == 1);
        verify(mockPropertyManager).getBuildNumber();
    }

    /**
     * Database version is 0, Crowd Version is 101 - Thus a new version of Crowd.
     */
    @Test
    public void testGetUpgradesForCrowdVersionGreaterThanDBVersion() throws Exception
    {
        // Setup the Mock propertyManager and the expectations
        when(mockPropertyManager.getBuildNumber()).thenReturn(new Integer(0));

        List<UpgradeTask> upgradeTasks = upgradeManager.getRequiredUpgrades();

        assertNotNull(upgradeTasks);
        assertFalse(upgradeTasks.isEmpty());

        // We should have 5 upgrade tasks from test-upgrade.xml
        assertTrue("The total number of upgrade tasks for a new install", upgradeTasks.size() == 5);
        verify(mockPropertyManager).getBuildNumber();
    }

    /**
     * Test that upgrade task's do not run that have a build number greater than the current Crowd build number.
     * @throws PropertyManagerException
     */
    @Test
    public void testUpgradeTaskGreaterThanUpgradeBuildDoesNotRun() throws PropertyManagerException
    {
        // Setup the Mock propertyManager and the expectations
        when(mockPropertyManager.getBuildNumber()).thenReturn(new Integer(120));

        List<UpgradeTask> upgradeTasks = upgradeManager.getRequiredUpgrades();

        assertNotNull(upgradeTasks);
        assertTrue(upgradeTasks.isEmpty());

        // We should have no upgrade tasks from test-upgrade.xml
        assertThat("The total number of upgrade tasks for a new install", upgradeTasks, Matchers.empty());
        verify(mockPropertyManager).getBuildNumber();
    }

    /**
     * Test that the builds are run in order of their build number
     */
    @Test
    public void testUpgradeTasksAreRunInOrder() throws Exception
    {
        when(mockPropertyManager.getBuildNumber()).thenReturn(new Integer(1));

        upgradeManager.doUpgrade();

        // We have 4 test upgrade tasks assert that they were each run in order
        assertEquals(ImmutableList.of("3", "5", "101", "120"), taskOrderList);

        verify(mockPropertyManager, times(2)).getBuildNumber();
        verify(mockPropertyManager).setBuildNumber(120);
        verify(bootstrapManager).setBuildNumber("120");
        verify(bootstrapManager).save();
    }

    /**
     * Test that the upgrade process dies if a test fails.
     */
    @Test
    public void testUpgradeTasksAreNotRunIfOneFails() throws Exception
    {
        // Set the current build number to 0, so the MockUpgradeTask1 is run, which has an 'error' in its error collection
        when(mockPropertyManager.getBuildNumber()).thenReturn(new Integer(0));

        upgradeManager.doUpgrade();

        // assert the task order list only has the one record and that all other tests were not run.
        assertEquals("Should only contain one entry", taskOrderList.size(), 1);
        verify(mockPropertyManager, times(2)).getBuildNumber();
    }

    @Test
    public void testUpgradeTaskManagerIncrementsBuildEvenWhenNoTasksAreRun() throws Exception
    {
        when(mockPropertyManager.getBuildNumber()).thenReturn(new Integer(121));

        upgradeManager.doUpgrade();

        // assert the task order list only has the one record and that all other tests were not run.
        assertEquals("No upgrades should have been run", taskOrderList.size(), 0);

        // The Build number in the database should still be set even though no upgrade took place.
        verify(mockPropertyManager, times(2)).getBuildNumber();
        verify(mockPropertyManager).setBuildNumber(120);
        verify(bootstrapManager).setBuildNumber("120");
        verify(bootstrapManager).save();
    }

    @Test
    public void cachesUsedPersistentlyAreNotCleared()
    {
        CacheManager cacheManager = mock(CacheManager.class);
        String[] cacheNames = {
                TokenDAOMemory.IDENTIFIER_HASH_CACHE,
                "other-cache"
        };

        when(cacheManager.getCacheNames()).thenReturn(cacheNames);

        Cache notToBeCleared = mock(Cache.class);
        Cache shouldBeCleared = mock(Cache.class);

        when(cacheManager.getCache(TokenDAOMemory.IDENTIFIER_HASH_CACHE)).thenReturn(notToBeCleared);
        when(cacheManager.getCache("other-cache")).thenReturn(shouldBeCleared);

        UpgradeManagerImpl upgradeManager = new UpgradeManagerImpl();
        upgradeManager.setCacheManager(cacheManager);

        upgradeManager.clearCaches();

        verify(notToBeCleared, never()).removeAll();
        verify(shouldBeCleared).removeAll();
    }

    abstract class MockUpgradeTask implements UpgradeTask
    {
        private final String buildNumber;

        MockUpgradeTask(String buildNumber)
        {
            this.buildNumber = buildNumber;
        }

        public final String getBuildNumber()
        {
            return buildNumber;
        }

        public final String getShortDescription()
        {
            return "Running: " + this.getClass().getName();
        }

        public final void doUpgrade() throws Exception
        {
            // for seeing order

            taskOrderList.add(getBuildNumber());
        }

        public Collection<String> getErrors()
        {
            return Collections.emptyList();
        }
    }

    class MockUpgradeTask1 extends MockUpgradeTask
    {
        MockUpgradeTask1()
        {
            super("1");
        }

        public Collection<String> getErrors()
        {
            return ImmutableList.of("This is a bad test 001");
        }
    }

    public class MockUpgradeTask3 extends MockUpgradeTask
    {
        MockUpgradeTask3()
        {
            super("3");
        }
    }

    public class MockUpgradeTask5 extends MockUpgradeTask
    {
        MockUpgradeTask5()
        {
            super("5");
        }
    }

    public class MockUpgradeTask101 extends MockUpgradeTask
    {
        MockUpgradeTask101()
        {
            super("101");
        }
    }

    public class MockUpgradeTask120 extends MockUpgradeTask
    {
        MockUpgradeTask120()
        {
            super("120");
        }
    }

    public class MockUpgradeTask121 extends MockUpgradeTask
    {
        MockUpgradeTask121()
        {
            super("121");
        }
    }
}
