package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import java.util.Arrays;

public class TestUpgradeTask361 extends MockObjectTestCase
{
    private UpgradeTask361 task;
    private Mock mockDirectoryManager;

    @Override
    protected void setUp() throws Exception
    {
        task = new UpgradeTask361();

        mockDirectoryManager = new Mock(DirectoryManager.class);
        task.setDirectoryManager((DirectoryManager) mockDirectoryManager.proxy());
    }

    public void testAllInternalDirectoriesSoNoUpgrade() throws Exception
    {
        DirectoryImpl directory1 = new DirectoryImpl();
        directory1.setType(DirectoryType.INTERNAL);

        DirectoryImpl directory2 = new DirectoryImpl();
        directory2.setType(DirectoryType.INTERNAL);

        mockDirectoryManager.expects(once()).method("searchDirectories").will(returnValue(Arrays.asList(directory1, directory2)));
        // should do no updates

        task.doUpgrade();
    }

    public void testUpgradeNonInternalDirectories() throws Exception
    {
        DirectoryImpl directory1 = new DirectoryImpl("1", DirectoryType.UNKNOWN, "");

        DirectoryImpl directory2 = new DirectoryImpl("2", DirectoryType.INTERNAL, "");

        DirectoryImpl directory3 = new DirectoryImpl("3", DirectoryType.CONNECTOR, "");
        directory3.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.TRUE.toString());

        DirectoryImpl directory4 = new DirectoryImpl("4", DirectoryType.CONNECTOR, "");
        directory4.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.FALSE.toString());

        // only one that should update
        DirectoryImpl directory5 = new DirectoryImpl("5", DirectoryType.CONNECTOR, "");

        DirectoryImpl directory6 = new DirectoryImpl("6", DirectoryType.DELEGATING, "");

        Directory directory7 = new DirectoryImpl("7", DirectoryType.CUSTOM, "");

        mockDirectoryManager.expects(once()).method("searchDirectories").will(returnValue(Arrays.asList(directory1, directory2, directory3, directory4, directory5, directory6)));
        mockDirectoryManager.expects(once()).method("updateDirectory").with(eq(directory5));

        task.doUpgrade();

        assertNull(directory1.getValue(LDAPPropertiesMapper.ROLES_DISABLED));
        assertNull(directory2.getValue(LDAPPropertiesMapper.ROLES_DISABLED));
        assertTrue(Boolean.parseBoolean(directory3.getValue(LDAPPropertiesMapper.ROLES_DISABLED)));
        assertFalse(Boolean.parseBoolean(directory4.getValue(LDAPPropertiesMapper.ROLES_DISABLED)));
        assertFalse(Boolean.parseBoolean(directory5.getValue(LDAPPropertiesMapper.ROLES_DISABLED)));
        assertNull(directory6.getValue(LDAPPropertiesMapper.ROLES_DISABLED));
        assertNull(directory7.getValue(LDAPPropertiesMapper.ROLES_DISABLED));
    }
}

