package com.atlassian.crowd.migration;

import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.migration.legacy.XmlMapper;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.InternalUserAttribute;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.HibernateOperation;

import com.google.common.collect.ImmutableList;

import org.apache.commons.lang3.time.DateUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UserMapperTest extends BaseMapperTest
{
    private UserMapper userMapper;
    private BatchProcessor batchProcessor;
    private DirectoryDAOHibernate directoryDAOHibernate;
    private UserDAOHibernate userDAOHibernate;
    private DirectoryManager directoryManager;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        directoryDAOHibernate = mock(DirectoryDAOHibernate.class);

        // These xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        batchProcessor = mock(BatchProcessor.class);
        userDAOHibernate = mock(UserDAOHibernate.class);
        directoryManager = mock(DirectoryManager.class);
        userMapper = new UserMapper(sessionFactory, batchProcessor, directoryDAOHibernate, userDAOHibernate, directoryManager);
    }

    public void testImportAndExportUsers() throws Exception
    {
        when(directoryDAOHibernate.loadReference(32769L)).thenReturn(directoryWithIdAndName(32769L, "Directory One"));

        Element usersNode = (Element) document.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + UserMapper.USER_XML_ROOT);
        List<Element> userElements = usersNode.elements(UserMapper.USER_XML_NODE);
        Element userElement = userElements.get(0);

        SimpleDateFormat sdf = new SimpleDateFormat(XmlMapper.XML_DATE_FORMAT);

        // test import

        InternalUser user = userMapper.getUserFromXml(userElement);

        assertEquals(new Long(65537), user.getId());
        assertEquals("Admin", user.getName());
        assertTrue(user.isActive());

        assertTrue(DateUtils.isSameInstant(sdf.parse("Fri Apr 03 16:37:07 +1100 2009"), user.getCreatedDate()));

        assertTrue(DateUtils.isSameInstant(sdf.parse("Fri Apr 03 16:37:08 +1100 2009"), user.getUpdatedDate()));

        assertEquals("Admin", user.getFirstName());
        assertEquals("Man", user.getLastName());
        assertEquals("Admin Man", user.getDisplayName());
        assertEquals("admin@Example.com", user.getEmailAddress());
        assertEquals(toLowerCase("Admin".toLowerCase()), user.getLowerFirstName());
        assertEquals(toLowerCase("Man".toLowerCase()), user.getLowerLastName());
        assertEquals(toLowerCase("Admin Man".toLowerCase()), user.getLowerDisplayName());
        assertEquals("admin@Example.com".toLowerCase(Locale.ENGLISH), user.getLowerEmailAddress());
        assertEquals("admin-external-id", user.getExternalId());

        assertEquals("x61Ey612Kl2gpFL56FT9weDnpSo4AV8j8+qx2AuTHdRyY036xxzTTrw10Wq3+4qQyB+XURPWx1ONxp3Y3pB37A==", user.getCredential().getCredential());
        assertTrue(user.getCredential().isEncryptedCredential());

        assertEquals(1, user.getCredentialRecords().size());
        assertEquals(new Long(101), user.getCredentialRecords().get(0).getId());
        assertEquals("x61Ey612Kl2gpFL56FT9weDnpSo4AV8j8+qx2AuTHdRyY036xxzTTrw10Wq3+4qQyB+XURPWx1ONxp3Y3pB37B==", user.getCredentialRecords().get(0).getPasswordHash());

        List<InternalUserAttribute> attributes = userMapper.getUserAttributesFromXml(userElement, user);
        for (InternalUserAttribute attribute : attributes)
        {
            assertEquals(user, attribute.getUser());
            assertEquals(user.getDirectory(), attribute.getDirectory());
        }

        assertEquals(new Long(294915), attributes.get(0).getId());
        assertEquals("lastAuthenticated", attributes.get(0).getName());
        assertEquals("1239154402209", attributes.get(0).getValue());

        assertEquals(new Long(294914), attributes.get(1).getId());
        assertEquals("requiresPasswordChange", attributes.get(1).getName());
        assertEquals("false", attributes.get(1).getValue());

        assertEquals(new Long(294913), attributes.get(2).getId());
        assertEquals("invalidPasswordAttempts", attributes.get(2).getName());
        assertEquals("0", attributes.get(2).getValue());

        assertEquals(new Long(98306), attributes.get(3).getId());
        assertEquals("passwordLastChanged", attributes.get(3).getName());
        assertEquals("1238737027230", attributes.get(3).getValue());

        // test export

        Document document = DocumentHelper.createDocument();
        Element root = document.addElement(XmlMigrationManagerImpl.XML_ROOT);
        Element userRoot = root.addElement(UserMapper.USER_XML_ROOT);

        userMapper.addUserToXml(user, attributes, userRoot);

        String export = exportDocumentToString(document);

        assertEquals(getTestExportXmlString(), export);
    }

    public void testExportXml_OnlyInternalAndDelegated() throws ExportException
    {
        User user1 = createUser("user1", DIRECTORY1, null); // internal
        User user2 = createUser("user2", DIRECTORY2, null); // connector
        User user3 = createUser("user3", DIRECTORY3, null); // delegating
        User user4 = createUser("user4", DIRECTORY4, null); // remote crowd
        EntityQuery<Directory> exportableDirectoryQuery = QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(Combine.anyOf(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.INTERNAL), Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.DELEGATING))).returningAtMost(
            EntityQuery.ALL_RESULTS);

        when(directoryManager.searchDirectories(exportableDirectoryQuery)).thenReturn(ImmutableList.<Directory>of(DIRECTORY1, DIRECTORY3));
        when(userDAOHibernate.search(eq(1L), any(EntityQuery.class))).thenReturn(ImmutableList.of(user1));
        when(userDAOHibernate.search(eq(3L), any(EntityQuery.class))).thenReturn(ImmutableList.of(user3));

        final Element root = userMapper.exportXml(null);

        assertEquals(ImmutableList.of("1", "3"), projectEntitiesSubelement(root, "user", "directoryId"));

        verify(userDAOHibernate, never()).search(eq(user2.getDirectoryId()), any(EntityQuery.class));
        verify(userDAOHibernate, never()).search(eq(user4.getDirectoryId()), any(EntityQuery.class));

    }

    public void testImportXml_OnlyInternalAndDelegated() throws ImportException
    {
        final InternalUser user1 = createUser("user1", DIRECTORY1, null); // internal
        final InternalUser user2 = createUser("user2", DIRECTORY2, null); // connector
        final InternalUser user3 = createUser("user3", DIRECTORY3, null); // delegating
        final InternalUser user4 = createUser("user4", DIRECTORY4, null); // remote crowd

        when(directoryDAOHibernate.loadReference(1L)).thenReturn(DIRECTORY1);
        when(directoryDAOHibernate.loadReference(2L)).thenReturn(DIRECTORY2);
        when(directoryDAOHibernate.loadReference(3L)).thenReturn(DIRECTORY3);
        when(directoryDAOHibernate.loadReference(4L)).thenReturn(DIRECTORY4);
        when(batchProcessor.execute(any(HibernateOperation.class), any(List.class))).thenReturn(new BatchResult(0));

        final Element root = DocumentHelper.createDocument().addElement("crowd");
        final Element users = root.addElement("users");
        users.add(createElement(user1));
        users.add(createElement(user2));
        users.add(createElement(user3));
        users.add(createElement(user4));

        userMapper.importXml(root);

        verify(batchProcessor).execute(any(HibernateOperation.class), eq(ImmutableList.of(user1, user3)));
    }

    public void testExportXml_NoEmailAddress() throws ExportException
    {
        assertNull(exportUser(createUserWithNoEmailAddress()).element("email"));
    }

    public void testImportXml_NoEmailAddress() throws ImportException
    {
        importUser(createUserWithNoEmailAddress());
    }

    public void testExportXml_NoExternalId() throws ExportException
    {
        assertNull(exportUser(createUserWithNoExternalId()).element("externalId"));
    }

    public void testImportXml_NoExternalId() throws ImportException
    {
        importUser(createUserWithNoExternalId());
    }

    public void testExportXmlForUserWithNullUserAttributes() throws Exception
    {
        InternalUser user = createUserWithNoEmailAddress();

        UserTemplate u2 = new UserTemplate(user.getName());
        u2.setDirectoryId(user.getDirectoryId());
        user.updateDetailsFrom(u2);

        assertNull(user.getFirstName());
        assertNull(user.getLowerFirstName());
        assertNull(user.getLastName());

        Element elem = exportUser(user);

        assertNull(elem.element("firstName"));
        assertNull(elem.element("lastName"));
        assertNull(elem.element("displayName"));
        assertNull(elem.element("email"));
        assertNull(elem.element("externalId"));
    }

    public void testImportXmlUserWithMinimalAttributes() throws Exception
    {
        when(directoryDAOHibernate.loadReference(1L)).thenReturn(DIRECTORY1);
        when(batchProcessor.execute(any(HibernateOperation.class), any(List.class))).thenReturn(new BatchResult(0));

        /* The minimal XML for a user that we will accept */
        final Element root = DocumentHelper.createDocument().addElement("crowd");
        final Element users = root.addElement("users");

        final Element user = users.addElement("user");

        user.addElement("directoryId").setText("1");
        user.addElement("id").setText("1");
        user.addElement("name").setText("name");
        user.addElement("createdDate").setText("");
        user.addElement("updatedDate").setText("");
        user.addElement("active").setText("");

        user.addElement("credentialRecords");
        user.addElement("attributes");

        /* Import and capture */
        userMapper.importXml(root);

        ArgumentCaptor<List> capturedUsers = ArgumentCaptor.forClass(List.class);

        verify(batchProcessor, Mockito.times(2)).execute(any(HibernateOperation.class), capturedUsers.capture());

        List<User> addedUsers = capturedUsers.getAllValues().get(0);
        assertEquals(1, addedUsers.size());

        /* Verify imported values */
        InternalUser addedUser = (InternalUser) addedUsers.get(0);
        assertEquals(1, addedUser.getDirectoryId());
        assertEquals("name", addedUser.getName());
        assertNull(addedUser.getCreatedDate());
        assertNull(addedUser.getUpdatedDate());
        assertFalse(addedUser.isActive());
        assertNull(addedUser.getExternalId());
        assertEquals(Collections.emptyList(), addedUser.getCredentialRecords());

        assertEquals("No user attributes",
                Collections.emptyList(), capturedUsers.getAllValues().get(1));
    }

    private static InternalUser createUserWithNoEmailAddress()
    {
        return createUser("user", DIRECTORY1, new UserCustomiser()
        {
            @Override
            public void customise(UserTemplate userTemplate)
            {
                userTemplate.setEmailAddress(null);
            }

            @Override
            public boolean hasPassword()
            {
                return true;
            }
        });
    }

    public void testExportXml_NoPassword() throws ExportException
    {
        assertNull(exportUser(createUserWithNoPassword()).element("email"));
    }

    public void testImportXml_NoPassword() throws ImportException
    {
        importUser(createUserWithNoPassword());
    }

    private static InternalUser createUserWithNoPassword()
    {
        return createUser("user", DIRECTORY1, new UserCustomiser()
        {
            @Override
            public void customise(UserTemplate userTemplate)
            {
                userTemplate.setEmailAddress(null);
            }

            @Override
            public boolean hasPassword()
            {
                return false;
            }
        });
    }

    private static InternalUser createUserWithNoExternalId()
    {
        return createUser("user", DIRECTORY1, new UserCustomiser()
        {
            @Override
            public void customise(UserTemplate userTemplate)
            {
                userTemplate.setExternalId(null);
            }

            @Override
            public boolean hasPassword()
            {
                return true;
            }
        });
    }

    private Element exportUser(User user) throws ExportException {
        EntityQuery<Directory> exportableDirectoryQuery = QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(Combine.anyOf(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.INTERNAL), Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.DELEGATING))).returningAtMost(EntityQuery.ALL_RESULTS);

        when(directoryManager.searchDirectories(exportableDirectoryQuery)).thenReturn(ImmutableList.<Directory>of(DIRECTORY1));
        when(userDAOHibernate.search(eq(1L), any(EntityQuery.class))).thenReturn(ImmutableList.of(user));

        final Element root = userMapper.exportXml(null);

        final List users = root.elements("user");
        assertEquals(1, users.size());
        return (Element) users.get(0);
    }

    private void importUser(InternalUser user) throws ImportException {
        when(directoryDAOHibernate.loadReference(1L)).thenReturn(DIRECTORY1);
        when(batchProcessor.execute(any(HibernateOperation.class), any(List.class))).thenReturn(new BatchResult(0));

        final Element root = DocumentHelper.createDocument().addElement("crowd");
        final Element users = root.addElement("users");
        users.add(createElement(user));

        userMapper.importXml(root);

        verify(batchProcessor).execute(any(HibernateOperation.class), eq(ImmutableList.of(user)));
    }

    private static Element createElement(InternalUser user)
    {
        final Element element = DocumentHelper.createElement("user");
        element.addElement("id").setText(user.getId().toString());
        element.addElement("name").setText(user.getName());
        element.addElement("firstName").setText(user.getFirstName());
        element.addElement("lastName").setText(user.getLastName());
        element.addElement("displayName").setText(user.getDisplayName());
        if (user.getEmailAddress() != null) {
            element.addElement("email").setText(user.getEmailAddress());
        }
        if (user.getCredential() != null) {
            element.addElement("credential").setText(user.getCredential().getCredential());
        }
        element.addElement("directoryId").setText(Long.toString(user.getDirectoryId()));
        element.addElement("createdDate").setText(user.getCreatedDate().toString());
        element.addElement("updatedDate").setText(user.getUpdatedDate().toString());
        element.addElement("active").setText(Boolean.toString(user.isActive()));
        element.addElement("credentialRecords");
        element.addElement("attributes");
        return element;
    }

    private static InternalUser createUser(String name, Directory directory, UserCustomiser customiser)
    {
        final UserTemplate userTemplate = new UserTemplate(name, "Test", "User", "Test User");
        userTemplate.setDirectoryId(directory.getId());
        userTemplate.setEmailAddress("test@example.com");
        if (customiser != null) {
            customiser.customise(userTemplate);
        }
        return new InternalUser(new InternalEntityTemplate(10L, name, true, new Date(), new Date()), directory, userTemplate,
                customiser == null || customiser.hasPassword() ? PasswordCredential.encrypted("password") : null);
    }

    private interface UserCustomiser {

        void customise(UserTemplate userTemplate);

        boolean hasPassword();
    }
}
