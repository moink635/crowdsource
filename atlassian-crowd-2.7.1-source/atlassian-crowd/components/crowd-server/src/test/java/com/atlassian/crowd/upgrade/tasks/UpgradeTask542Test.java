package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;

import java.util.Arrays;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * Tests that {@link UpgradeTask542} sets the incremental sync enabled attribute in all remote Crowd directories to true.
 *
 * @since v2.3
 */
public class UpgradeTask542Test
{
    @Mock private DirectoryDao mockDirectoryDao;
    private Directory directory1;
    private Directory directory2;

    private UpgradeTask542 upgradeTask;

    @Before
    public void setUp() throws Exception
    {
        initMocks(this);
        upgradeTask = new UpgradeTask542();
        upgradeTask.setDirectoryDao(mockDirectoryDao);

        directory1 = new DirectoryImpl("Directory 1", DirectoryType.CROWD, RemoteDirectory.class.getCanonicalName());
        directory2 = new DirectoryImpl("Directory 2", DirectoryType.CROWD, RemoteDirectory.class.getCanonicalName());
        when(mockDirectoryDao.search(any(EntityQuery.class))).thenReturn(Arrays.asList(directory1, directory2));
    }

    @After
    public void tearDown() throws Exception
    {
        upgradeTask = null;
        directory2 = null;
        directory1 = null;
        mockDirectoryDao = null;
    }

    @Test
    public void testDoUpgrade() throws Exception
    {
        upgradeTask.doUpgrade();

        verify(mockDirectoryDao, times(2)).update(argThat(new HasIncrementalSyncEnabledSetToTrue()));
    }

    static class HasIncrementalSyncEnabledSetToTrue extends ArgumentMatcher<Directory>
    {
        @Override
        public boolean matches(final Object argument)
        {
            Directory directory = (Directory) argument;
            return Boolean.parseBoolean(directory.getAttributes().get("crowd.sync.incremental.enabled"));
        }
    }
}
