package com.atlassian.crowd.importer.importers;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.importers.CrowdifiedJiraImporter.CrowdifiedJiraGroupMapper;
import com.atlassian.crowd.importer.importers.CrowdifiedJiraImporter.CrowdifiedJiraUserMapper;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;

import org.jmock.Mock;
import org.jmock.MockObjectTestCase;
import org.springframework.jdbc.core.RowMapper;

public class CrowdifiedJiraImporterTest extends MockObjectTestCase
{
    private RowMapper userMapperWithPassword;
    private RowMapper userMapperNoPassword;
    private RowMapper groupMapper;

    public void setUp() throws Exception
    {
        super.setUp();

        Configuration confWithImportPasswd = new Configuration(new Long(1), "jira", Boolean.TRUE, Boolean.FALSE);
        Configuration confNoImportPasswd = new Configuration(new Long(1), "jira", Boolean.FALSE, Boolean.FALSE);

        userMapperWithPassword = new CrowdifiedJiraUserMapper(confWithImportPasswd);
        userMapperNoPassword = new CrowdifiedJiraUserMapper(confNoImportPasswd);
        groupMapper = new CrowdifiedJiraGroupMapper(Long.valueOf(1));
    }

    public void testMapActiveGroup() throws SQLException
    {
        Group remoteGroup = (Group) groupMapper.mapRow(mockGroupResultSet("group1", "description of group1", true), 0);

        assertEquals("group1", remoteGroup.getName());
        assertEquals("description of group1", remoteGroup.getDescription());
        assertEquals(GroupType.GROUP, remoteGroup.getType());
        assertTrue(remoteGroup.isActive());
    }

    public void testMapInactiveGroup() throws SQLException
    {
        Group remoteGroup = (Group) groupMapper.mapRow(mockGroupResultSet("group2", "description of group2", false), 0);

        assertEquals("group2", remoteGroup.getName());
        assertEquals("description of group2", remoteGroup.getDescription());
        assertEquals(GroupType.GROUP, remoteGroup.getType());
        assertFalse(remoteGroup.isActive());
    }

    public void testMappingPrincipalWithNoPassword() throws SQLException
    {
        ResultSet resultSet = mockUserResultSet("user1", "user1@gmail.com", "first1", "last1", "first1 last1", "passwd", true);
        UserTemplateWithCredentialAndAttributes user = (UserTemplateWithCredentialAndAttributes) userMapperNoPassword.mapRow(resultSet, 0);

        assertNotNull(user);
        assertEquals("user1", user.getName());
        assertTrue(user.isActive());
        assertEquals("user1@gmail.com", user.getEmailAddress());
        assertEquals("first1", user.getFirstName());
        assertEquals("last1", user.getLastName());
        assertEquals("first1 last1", user.getDisplayName());
        PasswordCredential credential = user.getCredential();
        assertNotNull(credential);
        assertNotNull(credential.getCredential());
        assertNotSame("passwd", credential.getCredential());
    }

    public void testMappingPrincipalWithPassword() throws SQLException
    {
        ResultSet resultSet = mockUserResultSet("user2", "user2@gmail.com", "first2", "last2", "first2 last2", "passwd", false);
        UserTemplateWithCredentialAndAttributes user = (UserTemplateWithCredentialAndAttributes) userMapperWithPassword.mapRow(resultSet, 0);

        assertNotNull(user);
        assertEquals("user2", user.getName());
        assertFalse(user.isActive());
        assertEquals("user2@gmail.com", user.getEmailAddress());
        assertEquals("first2", user.getFirstName());
        assertEquals("last2", user.getLastName());
        assertEquals("first2 last2", user.getDisplayName());
        PasswordCredential credential = user.getCredential();
        assertNotNull(credential);
        assertNotNull(credential.getCredential());
        assertEquals("passwd", credential.getCredential());
    }

    private ResultSet mockGroupResultSet(String groupname, String description, boolean active)
    {
        Mock mockResultSet = new Mock(ResultSet.class);
        mockResultSet.expects(once()).method("getString").with(eq("group_name")).will(returnValue(groupname));
        mockResultSet.expects(once()).method("getString").with(eq("description")).will(returnValue(description));
        mockResultSet.expects(once()).method("getBoolean").with(eq("active")).will(returnValue(active));
        return (ResultSet) mockResultSet.proxy();
    }

    private ResultSet mockUserResultSet(String username, String emailaddress, String firstname, String lastname,
                                        String fullname, String password, boolean active)
    {
        Mock mockResultSet = new Mock(ResultSet.class);
        mockResultSet.expects(once()).method("getString").with(eq("user_name")).will(returnValue(username));
        mockResultSet.expects(once()).method("getString").with(eq("email_address")).will(returnValue(emailaddress));
        mockResultSet.expects(once()).method("getString").with(eq("first_name")).will(returnValue(firstname));
        mockResultSet.expects(once()).method("getString").with(eq("last_name")).will(returnValue(lastname));
        mockResultSet.expects(once()).method("getString").with(eq("display_name")).will(returnValue(fullname));
        mockResultSet.expects(once()).method("getString").with(eq("credential")).will(returnValue(password));
        mockResultSet.expects(once()).method("getBoolean").with(eq("active")).will(returnValue(active));

        return (ResultSet) mockResultSet.proxy();
    }
}
