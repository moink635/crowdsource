package com.atlassian.crowd.migration.legacy;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.dao.property.PropertyDAOHibernate;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import junit.framework.TestCase;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.SessionFactory;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class PropertyMapperTest extends TestCase
{
    private final List<Object> addedObjects = new ArrayList();
    private Object returnObject;
    private PropertyMapper propertyMapper;
    private Document document;
    private PropertyDAOHibernate propertyDao;
    private CrowdBootstrapManager bootstrapManager;

    public void setUp() throws Exception
    {
        super.setUp();

        propertyDao = mock(PropertyDAOHibernate.class);
        bootstrapManager = mock(CrowdBootstrapManager.class);

        // These legacy xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        BatchProcessor batchProcessor = mock(BatchProcessor.class);

        propertyMapper = new PropertyMapper(sessionFactory, batchProcessor, propertyDao, bootstrapManager)
        {
            @Override
            protected Object addEntityViaSave(final Object entityToPersist)
            {
                addedObjects.add(entityToPersist);
                return returnObject;
            }
        };
        
        // load xml file
        SAXReader reader = new SAXReader();
        final URL resource = ClassLoaderUtils.getResource(StringUtils.replace(this.getClass().getCanonicalName(), ".", File.separator) + ".xml", this.getClass());
        document = reader.read(resource);
    }

    public void tearDown() throws Exception
    {
        verifyNoMoreInteractions(propertyDao);
        super.tearDown();
    }

    public void testImportProperties() throws Exception
    {
        Element propertiesNode = (Element) document.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + PropertyMapper.PROPERTIES_XML_ROOT);
        propertyMapper.importXml(propertiesNode, null);

        verify(bootstrapManager).setServerID("A9CK-B4GA-0OKC-94CZ");
        assertEquals(2, addedObjects.size());
    }
}