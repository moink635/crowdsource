package com.atlassian.crowd.trusted;

import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

import com.atlassian.security.auth.trustedapps.EncryptionProvider;

import com.google.common.base.Charsets;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @since v2.7
 */
@RunWith(MockitoJUnitRunner.class)
public class KeyUtilsTest
{
    private static final String KEY = "SOMEKEY";
    public static final byte[] KEY_BYTES = KEY.getBytes(Charsets.UTF_8);
    private static final String KEY_BASE64 = "U09NRUtFWQ==";

    @Mock
    private EncryptionProvider encryptionProvider;

    @Test
    public void encodePrivateKey() throws Exception
    {
        final PrivateKey privateKey = mock(PrivateKey.class);
        when(privateKey.getEncoded()).thenReturn(KEY_BYTES);

        final String encodedKey = KeyUtils.encode(privateKey);

        assertThat(encodedKey, is(KEY_BASE64));
    }

    @Test
    public void encodePublicKey() throws Exception
    {
        final PublicKey publicKey = mock(PublicKey.class);
        when(publicKey.getEncoded()).thenReturn(KEY_BYTES);

        final String encodedKey = KeyUtils.encode(publicKey);

        assertThat(encodedKey, is(KEY_BASE64));
    }

    @Test
    public void decodePrivateKey() throws Exception
    {
        final PrivateKey privateKey = mock(PrivateKey.class);
        when(encryptionProvider.toPrivateKey(KEY_BYTES)).thenReturn(privateKey);

        final PrivateKey key = KeyUtils.decodePrivateKey(encryptionProvider, KEY_BASE64);

        assertThat(key, is(sameInstance(privateKey)));
    }

    @Test(expected = IllegalStateException.class)
    public void decodePrivateKeyWithErrors() throws Exception
    {
        when(encryptionProvider.toPrivateKey(KEY_BYTES)).thenThrow(new NoSuchAlgorithmException());

        KeyUtils.decodePrivateKey(encryptionProvider, KEY_BASE64);
    }

    @Test(expected = IllegalArgumentException.class)
    public void decodeInvalidPrivateKey() throws Exception
    {
        when(encryptionProvider.toPrivateKey(KEY_BYTES)).thenThrow(new InvalidKeySpecException());

        KeyUtils.decodePrivateKey(encryptionProvider, KEY_BASE64);
    }

    @Test
    public void decodePublicKey() throws Exception
    {
        final PublicKey publicKey = mock(PublicKey.class);
        when(encryptionProvider.toPublicKey(KEY_BYTES)).thenReturn(publicKey);

        final PublicKey key = KeyUtils.decodePublicKey(encryptionProvider, KEY_BASE64);

        assertThat(key, is(sameInstance(publicKey)));
    }

    @Test(expected = IllegalStateException.class)
    public void decodePublicKeyWithErrors() throws Exception
    {
        when(encryptionProvider.toPublicKey(KEY_BYTES)).thenThrow(new NoSuchAlgorithmException());

        KeyUtils.decodePublicKey(encryptionProvider, KEY_BASE64);
    }

    @Test(expected = IllegalArgumentException.class)
    public void decodeInvalidPublicKey() throws Exception
    {
        when(encryptionProvider.toPublicKey(KEY_BYTES)).thenThrow(new InvalidKeySpecException());

        KeyUtils.decodePublicKey(encryptionProvider, KEY_BASE64);
    }
}
