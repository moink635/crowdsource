package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.config.ConfigurationException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

public class TestUpgradeTask212 extends MockObjectTestCase
{
    private UpgradeTask212 upgradeTask;
    private Mock mockPropertyManager;
    private Mock mockBootstrapManager;

    protected void setUp() throws Exception
    {
        super.setUp();

        mockPropertyManager = new Mock(PropertyManager.class);
        mockBootstrapManager = new Mock(CrowdBootstrapManager.class);

        upgradeTask = new UpgradeTask212();
        upgradeTask.setPropertyManager((PropertyManager) mockPropertyManager.proxy());
        upgradeTask.setBootstrapManager((CrowdBootstrapManager) mockBootstrapManager.proxy());
    }

    public void testUpgrade_notRequired() throws Exception
    {
        mockPropertyManager.expects(once()).method("getProperty").withAnyArguments().will(throwException(new ObjectNotFoundException()));

        mockPropertyManager.expects(never()).method("removeProperty").with(eq(UpgradeTask212.SERVER_ID_KEY));

        upgradeTask.doUpgrade();

        assertEquals(upgradeTask.getErrors().size(), 0);
    }

    public void testUpgrade_successful() throws Exception
    {
        String sid = "SID12345";
        mockPropertyManager.expects(once()).method("getProperty").with(eq(UpgradeTask212.SERVER_ID_KEY)).will(returnValue(sid));

        mockBootstrapManager.expects(once()).method("setServerID").with(eq(sid));
        mockPropertyManager.expects(once()).method("removeProperty").with(eq(UpgradeTask212.SERVER_ID_KEY));

        upgradeTask.doUpgrade();

        assertEquals(upgradeTask.getErrors().size(), 0);
    }

    public void testUpgrade_fail() throws Exception
    {
        String sid = "SID12345";
        mockPropertyManager.expects(once()).method("getProperty").will(returnValue(sid));
        mockBootstrapManager.expects(once()).method("setServerID").with(eq(sid)).will(throwException(new ConfigurationException("")));


        mockPropertyManager.expects(never()).method("removeProperty").with(eq(UpgradeTask212.SERVER_ID_KEY));

        upgradeTask.doUpgrade();

        assertEquals(upgradeTask.getErrors().size(), 1);
    }
}
