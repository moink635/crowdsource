package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.user.InternalUserDao;
import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.time.DateUtils;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserMapperTest extends BaseLegacyImporterTest
{
    private UserMapper mapper;
    private DirectoryDAOHibernate directoryDAOHibernate;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        directoryDAOHibernate = mock(DirectoryDAOHibernate.class);
        InternalUserDao internalUserDao = mock(InternalUserDao.class);

        // These legacy xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        BatchProcessor batchProcessor = mock(BatchProcessor.class);
        directoryDAOHibernate = mock(DirectoryDAOHibernate.class);

        mapper = new UserMapper(sessionFactory, batchProcessor, directoryDAOHibernate, internalUserDao);

        when(directoryDAOHibernate.loadReference(1L)).thenReturn(directoryWithIdAndName(1L, "Directory One"));
        when(directoryDAOHibernate.loadReference(2L)).thenReturn(directoryWithIdAndName(2L, "Directory Two"));

        oldToNewDirectoryIds.put(32769L, 1L);
        oldToNewDirectoryIds.put(32770L, 2L);
    }

    @Override
    protected void tearDown() throws Exception
    {
        document = null;
        super.tearDown();
    }

    public void testImportApplicationXML() throws Exception
    {
        Element usersNode = (Element) document.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + UserMapper.REMOTE_PRINCIPAL_XML_ROOT);
        List<Element> userElements = usersNode.elements(UserMapper.REMOTE_PRINCIPAL_XML_NODE);

        UserTemplateWithCredentialAndAttributes user1 = mapper.getUserAndAttributesFromXml(userElements.get(0), oldToNewDirectoryIds);
        Map<String, Set<String>> attributes1 = user1.getAttributes();

        SimpleDateFormat sdf = new SimpleDateFormat(GenericLegacyImporter.DATE_FORMAT);

        // check user 1 (mega attributes)

        assertEquals("admin", user1.getName());
        assertTrue(user1.isActive());

        assertTrue(DateUtils.isSameInstant(sdf.parse("Fri Nov 16 14:33:12 +1100 2007"), user1.getCreatedDate()));
        assertTrue(DateUtils.isSameInstant(sdf.parse("Sat Nov 17 14:33:12 +1100 2007"), user1.getUpdatedDate()));

        assertEquals("Super", user1.getFirstName());
        assertEquals("User", user1.getLastName());
        assertEquals("", user1.getDisplayName());
        assertEquals("admin@example.com", user1.getEmailAddress());

        assertEquals("x61Ey612Kl2gpFL56FT9weDnpSo4AV8j8+qx2AuTHdRyY036xxzTTrw10Wq3+4qQyB+XURPWx1ONxp3Y3pB37A==", user1.getCredential().getCredential());
        assertTrue(user1.getCredential().isEncryptedCredential());

        assertEquals(5, attributes1.keySet().size());
        assertEquals(Sets.newHashSet("0"), attributes1.get("invalidPasswordAttempts"));
        assertEquals(Sets.newHashSet("false"), attributes1.get("requiresPasswordChange"));
        assertEquals(Sets.newHashSet("1201149048399"), attributes1.get("lastAuthenticated"));
        assertEquals(Sets.newHashSet("1195183992446"), attributes1.get("passwordLastChanged"));
        assertEquals(Sets.newHashSet("Red", "Blue", "Green"), attributes1.get("colour"));

        assertEquals(2, user1.getCredentialHistory().size());
        assertEquals("abc", user1.getCredentialHistory().get(0).getCredential());
        assertEquals("123", user1.getCredentialHistory().get(1).getCredential());

        // check user 2 (no attributes)

        UserTemplateWithCredentialAndAttributes user2 = mapper.getUserAndAttributesFromXml(userElements.get(1), oldToNewDirectoryIds);
        Map<String, Set<String>> attributes2 = user2.getAttributes();

        assertEquals("user", user2.getName());
        assertTrue(!user2.isActive());

        assertTrue(DateUtils.isSameInstant(sdf.parse("Fri Nov 16 14:33:12 +1100 2007"), user2.getCreatedDate()));
        assertTrue(DateUtils.isSameInstant(sdf.parse("Sat Nov 17 14:33:12 +1100 2007"), user2.getUpdatedDate()));

        assertEquals("", user2.getFirstName());
        assertEquals("", user2.getLastName());
        assertEquals("", user2.getDisplayName());
        assertEquals("", user2.getEmailAddress());

        assertNotNull(user1.getCredential().getCredential());
        assertTrue(user1.getCredential().isEncryptedCredential());

        assertEquals(0, attributes2.keySet().size());

        assertEquals(0, user2.getCredentialHistory().size());
    }
}

