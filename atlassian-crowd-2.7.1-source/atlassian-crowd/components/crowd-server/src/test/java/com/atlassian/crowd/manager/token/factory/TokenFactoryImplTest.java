package com.atlassian.crowd.manager.token.factory;

import java.util.Collections;
import java.util.List;

import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.security.random.SecureRandomService;

import org.hamcrest.number.OrderingComparison;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TokenFactoryImplTest
{
    private TokenFactoryImpl tokenFactory = null;

    @Mock
    private TokenKeyGenerator tokenKeyGenerator;

    @Before
    public void setUp()
    {
        tokenFactory = new TokenFactoryImpl(tokenKeyGenerator);
    }

    @Test
    public void testCreateSecretValidationNumber()
    {
        long secretValidationNumber = tokenFactory.createSecretValidationNumber();

        assertNotNull(secretValidationNumber);
        assertTrue(secretValidationNumber > 0);
    }

    @Test
    public void testCreateSecretValidationNumberReturnsPositiveNumber()
    {
        final long number = 5612345678945678952L;

        SecureRandomService randomService = Mockito.mock(SecureRandomService.class);

        tokenFactory = new TokenFactoryImpl(tokenKeyGenerator, randomService);

        when(randomService.nextLong()).thenReturn(-number);

        long secretValidationNumber = tokenFactory.createSecretValidationNumber();

        assertNotNull(secretValidationNumber);
        assertTrue(secretValidationNumber > 0);
        assertEquals(number, secretValidationNumber);

        verify(randomService).nextLong();
    }

    @Test
    public void testCreateSecretValidationNumberAlwaysReturnsPositiveNumber()
    {
        SecureRandomService randomService = Mockito.mock(SecureRandomService.class);

        when(randomService.nextLong()).thenReturn(Long.MIN_VALUE);

        tokenFactory = new TokenFactoryImpl(null, randomService);

        Assert.assertThat(Long.valueOf(tokenFactory.createSecretValidationNumber()),
                OrderingComparison.greaterThanOrEqualTo(0L));
    }

    @Test
    public void createAcceptsImmutableList() throws Exception
    {
        when(tokenKeyGenerator.generateKey(Mockito.anyLong(), Mockito.anyString(), Mockito.<List<ValidationFactor>>anyObject())).thenReturn("identifier-hash");

        tokenFactory.create(0, "name", TokenLifetime.USE_DEFAULT, Collections.<ValidationFactor>emptyList());
    }
}
