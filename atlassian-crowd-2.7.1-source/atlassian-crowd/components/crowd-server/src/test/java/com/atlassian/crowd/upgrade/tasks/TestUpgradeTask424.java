package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.*;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.google.common.collect.Maps;
import junit.framework.TestCase;

import java.util.Arrays;
import java.util.Map;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUpgradeTask424 extends TestCase
{
    private UpgradeTask424 task;
    private DirectoryManager mockDirectoryManager;

    @Override
    protected void setUp() throws Exception
    {
        task = new UpgradeTask424();
        mockDirectoryManager = mock(DirectoryManager.class);
        task.setDirectoryManager(mockDirectoryManager);
    }


    public void testNoPollingInterval() throws Exception
    {
        DirectoryImpl directory = new DirectoryImpl("Directory 1", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());

        directory.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_USE_CACHING, Boolean.FALSE.toString());
        directory.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_USE_MONITORING, Boolean.FALSE.toString());
        directory.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.FALSE.toString());

        when(mockDirectoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(Arrays.<Directory>asList(directory));

        task.doUpgrade();

        // Only the cache attributes removed - with an additional sync interval attribute added
        Map<String, String> expectedAttributes = Maps.newHashMap();
        expectedAttributes.put(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL, UpgradeTask424.DEFAULT_SYNC_INTERVAL);
        expectedAttributes.put(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.FALSE.toString());
        verify(mockDirectoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.exactlyMatches(expectedAttributes)));
    }

    public void testInvalidPollingInterval() throws Exception
    {
        DirectoryImpl directory = new DirectoryImpl("Directory 1", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());

        directory.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_USE_CACHING, Boolean.TRUE.toString());
        directory.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_USE_MONITORING, Boolean.TRUE.toString());
        directory.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_CACHE_CLASS, "com.atlassian.crowd.integration.directory.cache.MicrosoftActiveDirectoryCache");
        directory.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_CACHE_MAX_ELEMENTS_IN_MEMORY, "1000");
        directory.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_POLLING_INTERVAL, "300");
        directory.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.FALSE.toString());

        when(mockDirectoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(Arrays.<Directory>asList(directory));

        task.doUpgrade();

        // Only the cache attributes removed - with an additional sync interval attribute added
        Map<String, String> expectedAttributes = Maps.newHashMap();
        expectedAttributes.put(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL, UpgradeTask424.DEFAULT_SYNC_INTERVAL);
        expectedAttributes.put(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.FALSE.toString());
        verify(mockDirectoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.exactlyMatches(expectedAttributes)));
    }

    public void testValidPollingInterval() throws Exception
    {
        DirectoryImpl directory = new DirectoryImpl("Directory 1", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());

        directory.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_USE_CACHING, Boolean.TRUE.toString());
        directory.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_USE_MONITORING, Boolean.TRUE.toString());
        directory.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_CACHE_CLASS, "com.atlassian.crowd.integration.directory.cache.MicrosoftActiveDirectoryCache");
        directory.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_CACHE_MAX_ELEMENTS_IN_MEMORY, "1000");
        directory.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_POLLING_INTERVAL, "4200");
        directory.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.FALSE.toString());

        when(mockDirectoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(Arrays.<Directory>asList(directory));

        task.doUpgrade();

        // Only the cache attributes removed - with an additional sync interval attribute added
        Map<String, String> expectedAttributes = Maps.newHashMap();
        expectedAttributes.put(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL, "4200");
        expectedAttributes.put(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.FALSE.toString());
        verify(mockDirectoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.exactlyMatches(expectedAttributes)));
    }


    public void testConnectorAndDelegatingDirectory() throws Exception
    {
        DirectoryImpl directory1 = new DirectoryImpl("Directory 1", DirectoryType.CONNECTOR, RemoteDirectory.class.getCanonicalName());
        DirectoryImpl directory2 = new DirectoryImpl("Directory 2", DirectoryType.DELEGATING, DelegatedAuthenticationDirectory.class.getCanonicalName());

        directory1.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_USE_CACHING, Boolean.TRUE.toString());
        directory1.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_USE_MONITORING, Boolean.TRUE.toString());
        directory1.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_CACHE_CLASS, "com.atlassian.crowd.integration.directory.cache.MicrosoftActiveDirectoryCache");
        directory1.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_CACHE_MAX_ELEMENTS_IN_MEMORY, "1000");
        directory1.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_POLLING_INTERVAL, "4200");
        directory1.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.FALSE.toString());

        // These shouldn't be here but for some reason creating a delegated directory in 2.0.7 they do appear
        directory2.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_USE_CACHING, Boolean.FALSE.toString());
        directory2.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_USE_MONITORING, Boolean.FALSE.toString());
        directory2.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_CACHE_CLASS, "com.atlassian.crowd.integration.directory.cache.MicrosoftActiveDirectoryCache");
        directory2.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_CACHE_MAX_ELEMENTS_IN_MEMORY, "0");
        directory2.setAttribute(UpgradeTask424.ATTRIBUTE_KEY_POLLING_INTERVAL, "0");
        directory2.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, Boolean.TRUE.toString());

        when(mockDirectoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(Arrays.<Directory>asList(directory1, directory2));

        task.doUpgrade();

        // Only the cache attributes removed - with an additional sync interval attribute added
        Map<String, String> expectedAttributes = Maps.newHashMap();
        expectedAttributes.put(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL, "4200");
        expectedAttributes.put(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.FALSE.toString());
        verify(mockDirectoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.exactlyMatchesForDirectory(directory1.getName(), expectedAttributes)));

        // Only the cache attributes removed - additional sync interval attribute NOT added as it is a delegated di
        expectedAttributes.clear();
        expectedAttributes.put(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, Boolean.TRUE.toString());
        verify(mockDirectoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.exactlyMatchesForDirectory(directory2.getName(), expectedAttributes)));
    }
}
