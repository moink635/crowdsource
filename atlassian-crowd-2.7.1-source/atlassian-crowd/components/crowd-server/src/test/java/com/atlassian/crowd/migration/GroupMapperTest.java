package com.atlassian.crowd.migration;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.migration.legacy.XmlMapper;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.group.InternalGroupAttribute;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.HibernateOperation;

import com.google.common.collect.ImmutableList;

import org.apache.commons.lang3.time.DateUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static com.atlassian.crowd.migration.GroupMapper.GROUP_XML_ROOT;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class GroupMapperTest extends BaseMapperTest
{
    private DirectoryDAOHibernate directoryDAOHibernate;
    private BatchProcessor batchProcessor;
    private GroupDAOHibernate groupDAOHibernate;
    private DirectoryManager directoryManager;

    private GroupMapper mapper;
    private Element groupRoot;
    private SimpleDateFormat simpleDateFormat;
    private static final String EXPECTED_GROUP_NAME = "Crowd-Administrators";

    protected void setUp() throws Exception
    {
        super.setUp();

        simpleDateFormat = new SimpleDateFormat(XmlMapper.XML_DATE_FORMAT);

        directoryDAOHibernate = mock(DirectoryDAOHibernate.class);

        // These xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        batchProcessor = mock(BatchProcessor.class);
        groupDAOHibernate = mock(GroupDAOHibernate.class);
        directoryManager = mock(DirectoryManager.class);
        mapper = new GroupMapper(sessionFactory, batchProcessor, groupDAOHibernate, directoryDAOHibernate, directoryManager);

        groupRoot = (Element) document.getRootElement().selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + GROUP_XML_ROOT);
    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    public void testImportAndExportFromXML() throws Exception
    {
        when(directoryDAOHibernate.loadReference(1L)).thenReturn(DIRECTORY1);

        // test import
        Element groupElement = (Element) this.groupRoot.elementIterator().next();
        InternalGroup group = mapper.getGroupFromXml(groupElement);

        assertNotNull(group);

        assertEquals(10L, group.getId().longValue());
        assertEquals(1L, group.getDirectoryId());
        assertEquals(EXPECTED_GROUP_NAME, group.getName());
        assertTrue(group.isActive());
        assertEquals("The Crowd Administrator Group", group.getDescription());
        assertEquals(GroupType.GROUP, group.getType());
        assertTrue(group.isLocal());

        assertTrue(DateUtils.isSameInstant(simpleDateFormat.parse("Fri Apr 03 16:37:07 +1100 2009"), group.getCreatedDate()));
        assertTrue(DateUtils.isSameInstant(simpleDateFormat.parse("Fri Apr 03 16:37:07 +1100 2009"), group.getUpdatedDate()));

        // Test attributes
        Set<InternalGroupAttribute> attributes = mapper.getGroupAttributesFromXml(groupElement, group);

        assertEquals(1, attributes.size());
        assertEquals("wicked_group", attributes.iterator().next().getName());
        assertEquals("true", attributes.iterator().next().getValue());
        assertEquals(EXPECTED_GROUP_NAME, attributes.iterator().next().getGroup().getName());


        // test export
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement(XmlMigrationManagerImpl.XML_ROOT);
        Element groupRoot = root.addElement(GroupMapper.GROUP_XML_ROOT);

        mapper.addGroupToXml(group, attributes, groupRoot);

        String export = exportDocumentToString(document);

        assertEquals(getTestExportXmlString(), export);
    }

    public void testExportXml_OnlyNonReplicatedGroups() throws ExportException
    {
        InternalGroup group1 = createGroup("group1", DIRECTORY1); // internal
        InternalGroup group2 = createGroup("group2", DIRECTORY2); // connector
        InternalGroup group3 = createGroup("group3", DIRECTORY3); // delegating
        InternalGroup group4 = createGroup("group4", DIRECTORY4); // remote crowd

        group2.setLocal(true);

        EntityQuery<Directory> exportableDirectoryQuery =
            QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                .with(Combine.anyOf(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.INTERNAL),
                                    Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.DELEGATING),
                                    Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.CONNECTOR)))
                .returningAtMost(EntityQuery.ALL_RESULTS);
        EntityQuery<InternalGroup> queryAllGroups =
            QueryBuilder.queryFor(InternalGroup.class, EntityDescriptor.group())
                .returningAtMost(EntityQuery.ALL_RESULTS);
        EntityQuery<InternalGroup> queryJustLocalGroups =
            QueryBuilder.queryFor(InternalGroup.class, EntityDescriptor.group())
                .with(Restriction.on(GroupTermKeys.LOCAL).exactlyMatching(true))
                .returningAtMost(EntityQuery.ALL_RESULTS);

        when(directoryManager.searchDirectories(exportableDirectoryQuery))
            .thenReturn(ImmutableList.<Directory>of(DIRECTORY1, DIRECTORY2, DIRECTORY3));
        when(groupDAOHibernate.search(eq(1L), eq(queryAllGroups))).thenReturn(ImmutableList.of(group1));
        when(groupDAOHibernate.search(eq(2L), eq(queryJustLocalGroups))).thenReturn(ImmutableList.of(group2));
        when(groupDAOHibernate.search(eq(3L), eq(queryJustLocalGroups))).thenReturn(ImmutableList.of(group3));

        final Element root = mapper.exportXml(null);

        assertEquals(ImmutableList.of("1", "2", "3"), projectEntitiesSubelement(root, "group", "directoryId"));

        verify(groupDAOHibernate, never()).search(eq(group2.getDirectoryId()), eq(queryAllGroups));
        verify(groupDAOHibernate, never()).search(eq(group3.getDirectoryId()), eq(queryAllGroups));

        verify(groupDAOHibernate, never()).search(eq(group4.getDirectoryId()), any(EntityQuery.class));
    }

    public void testImportXml_OnlyNonReplicatedGroups() throws ImportException
    {
        final InternalGroup group1 = createGroup("group1", DIRECTORY1); // internal
        final InternalGroup group2 = createGroup("group2", DIRECTORY2); // connector
        final InternalGroup group3 = createGroup("group3", DIRECTORY3); // delegating
        final InternalGroup group4 = createGroup("group4", DIRECTORY4); // remote crowd

        when(directoryDAOHibernate.loadReference(1L)).thenReturn(DIRECTORY1);
        when(directoryDAOHibernate.loadReference(2L)).thenReturn(DIRECTORY2);
        when(directoryDAOHibernate.loadReference(3L)).thenReturn(DIRECTORY3);
        when(directoryDAOHibernate.loadReference(4L)).thenReturn(DIRECTORY4);
        when(batchProcessor.execute(any(HibernateOperation.class), any(List.class))).thenReturn(new BatchResult(0));

        final Element root = DocumentHelper.createDocument().addElement("crowd");
        final Element groups = root.addElement("groups");
        groups.add(createElement(group1));
        groups.add(createElement(group2));
        groups.add(createElement(group3));
        groups.add(createElement(group4));

        mapper.importXml(root);

        verify(batchProcessor).execute(any(HibernateOperation.class), eq(ImmutableList.of(group1, group2, group3)));
    }

    private Element createElement(InternalGroup group)
    {
        final Element element = DocumentHelper.createElement("group");
        element.addElement("id").setText(group.getId().toString());
        element.addElement("name").setText(group.getName());
        element.addElement("description").setText(group.getDescription());
        element.addElement("type").setText(group.getType().toString());
        element.addElement("directoryId").setText(Long.toString(group.getDirectoryId()));
        element.addElement("createdDate").setText(group.getCreatedDate().toString());
        element.addElement("updatedDate").setText(group.getUpdatedDate().toString());
        element.addElement("active").setText(Boolean.toString(group.isActive()));
        element.addElement("attributes");
        return element;
    }

    private InternalGroup createGroup(String name, Directory directory)
    {
        final GroupTemplate groupTemplate = new GroupTemplate(name, directory.getId().longValue());
        groupTemplate.setDirectoryId(directory.getId());
        groupTemplate.setDescription("Description");
        return new InternalGroup(new InternalEntityTemplate(10L, name, true, new Date(), new Date()), directory, groupTemplate);
    }
}
