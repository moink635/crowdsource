package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.MicrosoftActiveDirectory;
import com.atlassian.crowd.directory.OpenLDAP;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * UpgradeTask216 Tester.
 */
public class TestUpgradeTask216
{
    @Mock
    DirectoryManager directoryManager;
    private UpgradeTask216 upgradeTask;
    private DirectoryImpl directoryNoUpdate;
    private DirectoryImpl directoryToUpdate;
    private DirectoryImpl directoryToUpdateAD;

    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);
        upgradeTask = new UpgradeTask216();
        upgradeTask.setDirectoryManager(directoryManager);

        directoryNoUpdate = new DirectoryImpl("directory-no-update", DirectoryType.CONNECTOR, MicrosoftActiveDirectory.class.getCanonicalName());
        directoryNoUpdate.setAttribute(LDAPPropertiesMapper.USER_USERNAME_KEY, "beep");
        directoryNoUpdate.setAttribute(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY, "boop");

        directoryToUpdate = new DirectoryImpl("directory-to-update", DirectoryType.CONNECTOR, OpenLDAP.class.getCanonicalName());
        directoryToUpdate.setAttribute(LDAPPropertiesMapper.USER_USERNAME_KEY, "burp");

        directoryToUpdateAD = new DirectoryImpl("directory-to-update-active-directory", DirectoryType.CONNECTOR, MicrosoftActiveDirectory.class.getCanonicalName());
        directoryToUpdateAD.setAttribute(LDAPPropertiesMapper.USER_USERNAME_KEY, "sAMAccountName");
    }

    @After
    public void tearDown() throws Exception
    {
        upgradeTask = null;
        directoryNoUpdate = null;
        directoryToUpdate = null;
        directoryToUpdateAD = null;
    }

    @Test
    public void testDoUpgradeWithDirectoryToUpdate() throws Exception
    {
        when(directoryManager.searchDirectories(buildDirectoryQuery())).thenReturn(Arrays.<Directory>asList(directoryToUpdate));
        when(directoryManager.updateDirectory(directoryToUpdate)).thenReturn(null);

        upgradeTask.doUpgrade();

        verify(directoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.contains(LDAPPropertiesMapper.USER_USERNAME_KEY, "burp")));
        verify(directoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.contains(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY, "burp")));

        assertTrue(upgradeTask.getErrors().isEmpty());
    }

    @Test
    public void testDoUpgradeWithDirectoryToUpdateActiveDirectory() throws Exception
    {
        when(directoryManager.searchDirectories(buildDirectoryQuery())).thenReturn(Arrays.<Directory>asList(directoryToUpdateAD));
        when(directoryManager.updateDirectory(directoryToUpdateAD)).thenReturn(null);

        upgradeTask.doUpgrade();

        verify(directoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.contains(LDAPPropertiesMapper.USER_USERNAME_KEY, "sAMAccountName")));
        verify(directoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.contains(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY, "cn")));

        assertTrue(upgradeTask.getErrors().isEmpty());
    }

    @Test
    public void testDoUpgradeWithDirectoryToLeaveAlone() throws Exception
    {
        when(directoryManager.searchDirectories(buildDirectoryQuery())).thenReturn(Arrays.<Directory>asList(directoryNoUpdate));
        upgradeTask.doUpgrade();
        verify(directoryManager, never()).updateDirectory(any(Directory.class));

        assertTrue(upgradeTask.getErrors().isEmpty());
    }

    private EntityQuery buildDirectoryQuery()
    {
        return QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS);
    }
}
