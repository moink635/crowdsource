package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpgradeTask502Test
{
    private UpgradeTask502 upgradeTask;

    @Mock
    private PropertyManager propertyManager;

    @Before
    public void setUp()
    {
        upgradeTask = new UpgradeTask502();
        upgradeTask.setPropertyManager(propertyManager);
    }

    @Test
    public void testUpgradeWithLeadingNull() throws Exception
    {
        runTestUpgrade(",127.0.0.1", "127.0.0.1");
    }

    @Test
    public void testUpgradeWithMultipleLeadingNulls() throws Exception
    {
        runTestUpgrade(",,,,127.0.0.1", "127.0.0.1");
    }

    @Test
    public void testUpgradeWithTrailingNull() throws Exception
    {
        runTestUpgrade("127.0.0.1,", "127.0.0.1");
    }

    @Test
    public void testUpgradeWithMultipleTrailingNulls() throws Exception
    {
        runTestUpgrade("127.0.0.1,,,,", "127.0.0.1");
    }

    @Test
    public void testUpgradeWithMultipleNulls() throws Exception
    {
        runTestUpgrade(",,,,,127.0.0.1,,,,", "127.0.0.1");
    }

    @Test
    public void testNoProxyEverSet() throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenThrow(new PropertyManagerException("blah"));
        upgradeTask.doUpgrade();
        verify(propertyManager, times(0)).setTrustedProxyServers(Matchers.<String>any());
    }

    @Test
    public void testUpgradeMultipleEntries() throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenReturn(",,,,,127.0.0.1,,,192.168.0.1,,,*.atlassian.com,,,,");
        ArgumentCaptor<String> captured = ArgumentCaptor.forClass(String.class);
        upgradeTask.doUpgrade();
        verify(propertyManager, times(1)).setTrustedProxyServers(captured.capture());
        assertEquals(Sets.newHashSet("127.0.0.1", "192.168.0.1", "*.atlassian.com"),
                     Sets.newHashSet(StringUtils.split(captured.getValue(), ',')));
    }

    private void runTestUpgrade(String beforeUpgrade, String expectedAfterUpgrade) throws Exception
    {
        when(propertyManager.getTrustedProxyServers()).thenReturn(beforeUpgrade);
        upgradeTask.doUpgrade();
        verify(propertyManager, times(1)).setTrustedProxyServers(expectedAfterUpgrade);
    }
}
