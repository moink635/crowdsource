package com.atlassian.crowd.service;

import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;

import com.google.common.collect.ImmutableSet;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v2.7
 */
@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest
{
    @InjectMocks
    private UserServiceImpl service;

    @Mock
    private ApplicationManager applicationManager;
    @Mock
    private ApplicationService applicationService;
    @Mock
    private CrowdUserDetailsService crowdUserDetailsService;
    @Mock
    private TokenAuthenticationManager tokenAuthenticationManager;

    @After
    public void tearDown() throws Exception
    {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void authenticateWithoutPasswordAndWrongUsername() throws Exception
    {
        when(crowdUserDetailsService.loadUserByUsername("foo")).thenThrow(new UsernameNotFoundException(""));
        final boolean result = service.setAuthenticatedUser("foo");
        assertThat(result, is(false));
    }

    @Test
    public void authenticateWithoutPasswordAndError() throws Exception
    {
        when(crowdUserDetailsService.loadUserByUsername("foo")).thenThrow(new DataAccessException("") {});
        final boolean result = service.setAuthenticatedUser("foo");
        assertThat(result, is(false));
    }

    @Test(expected = NullPointerException.class)
    public void authenticateWithoutPasswordAndNullUsername() throws Exception
    {
        service.setAuthenticatedUser(null);
    }

    @Test
    public void authenticateWithoutPasswordShouldSetSecurityContext() throws Exception
    {
        final CrowdUserDetails principal = new CrowdUserDetails(new SOAPPrincipal("foo"), ImmutableSet.<GrantedAuthority>of());
        when(crowdUserDetailsService.loadUserByUsername("foo")).thenReturn(principal);
        final boolean result = service.setAuthenticatedUser("foo");

        assertThat(result, is(true));
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        assertThat(authentication, is(notNullValue()));
        assertThat(authentication.getPrincipal(), sameInstance((Object) principal));
        assertThat(authentication.isAuthenticated(), is(true));
    }
}
