package com.atlassian.crowd.manager.backup;

import com.atlassian.config.HomeLocator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v2.7
 */
@RunWith(MockitoJUnitRunner.class)
public class DefaultBackupFileStoreTest
{
    @Mock
    private HomeLocator homeLocator;

    @Test
    public void testGetBackupFiles() throws Exception
    {
        final List<File> backupFiles = serviceWithFiles().getBackupFiles();
        assertThat(backupFiles, is(notNullValue()));
        assertThat(backupFiles, hasSize(4));
        assertThat(backupFiles, contains(
                new File("atlassian-crowd-2013-05-02-154600-automated-backup-2.7.0.xml"),
                new File("atlassian-crowd-2013-05-02-154700-automated-backup-2.7.0.xml"),
                new File("atlassian-crowd-2013-05-02-154900-automated-backup-2.7.0.xml"),
                new File("atlassian-crowd-2013-05-02-155000-automated-backup-2.7.0.xml")
        ));
    }

    @Test
    public void testGetBackupFilesNoFiles() throws Exception
    {
        final List<File> backupFiles = serviceWithNoFiles().getBackupFiles();
        assertThat(backupFiles, is(notNullValue()));
        assertThat(backupFiles, hasSize(0));
    }

    @Test
    public void testGetBackupDirectory() throws Exception
    {
        File crowdHome = new File("crowdhome");
        when(homeLocator.getHomePath()).thenReturn(crowdHome.getPath());
        assertThat(serviceWithNoFiles().getBackupDirectory().getPath(), is(new File(crowdHome, "backups").getPath()));
    }

    @Test
    public void testCleanUpAutomatedBackup() throws Exception
    {
        List<File> deletedFiles = Lists.newArrayList();

        serviceForDelete(deletedFiles, 60).cleanUpAutomatedBackupFiles();

        // As the max is 50, verify that 10 were deleted
        assertThat(deletedFiles, hasSize(10));
    }

    private DefaultBackupFileStore serviceWithFiles()
    {
        return new DefaultBackupFileStore(homeLocator) {
            @Override
            protected Iterable<File> filesInBackupDirectory()
            {
                return ImmutableList.of(
                        new File("someFile"),
                        new File("atlassian-crowd-2013-05-02-154900-automated-backup-2.7.0.xml"),
                        new File("atlassian-crowd-2013-05-02-154700-automated-backup-2.7.0.xml"),
                        new File("atlassian-crowd-2.7.0-backup-2013-05-02-154800.xml"), // manual backup
                        new File("atlassian-crowd-2013-05-02-155000-automated-backup-2.7.0.xml"),
                        new File("someOtherFile"),
                        new File("atlassian-crowd-2013-05-02-154600-automated-backup-2.7.0.xml")
                );
            }
        };
    }

    private DefaultBackupFileStore serviceWithNoFiles()
    {
        return new DefaultBackupFileStore(homeLocator) {
            @Override
            protected Iterable<File> filesInBackupDirectory()
            {
                return ImmutableList.of();
            }
        };
    }

    private DefaultBackupFileStore serviceForDelete(final List<File> deletedFiles, final int numFiles)
    {
        return new DefaultBackupFileStore(homeLocator) {
            @Override
            protected Iterable<File> filesInBackupDirectory()
            {
                final DateTime startDate = new DateTime();
                final SimpleDateFormat sdf = new SimpleDateFormat(BackupFileConstants.TIMESTAMP_FORMAT);
                final ImmutableList.Builder<File> builder = ImmutableList.builder();
                for (int i = 0; i < numFiles; i++)
                {
                    builder.add(new File("atlassian-crowd-" + sdf.format(startDate.plusHours(i).toDate()) + "-automated-backup-2.7.0.xml"));
                }
                return builder.build();
            }

            @Override
            protected void deleteFile(File file)
            {
                deletedFiles.add(file);
            }
        };
    }
}
