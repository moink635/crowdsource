package com.atlassian.crowd.event.listener;

import java.util.Locale;
import java.util.ResourceBundle;

import com.atlassian.crowd.event.LicenseResourceLimitEvent;
import com.atlassian.crowd.manager.license.CrowdLicenseManager;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.I18nHelperImpl;
import com.atlassian.crowd.util.ResourceBundleProvider;
import com.atlassian.extras.api.crowd.CrowdLicense;

import com.google.common.collect.ImmutableList;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LicenseResourceLimitListenerTest
{
    static I18nHelper i18HelperUsingRealResources()
    {
        final ResourceBundleProvider provider = new ResourceBundleProvider()
        {
            @Override
            public Iterable<ResourceBundle> getResourceBundles()
            {
                return ImmutableList.of(ResourceBundle.getBundle("com.atlassian.crowd.console.action.BaseAction", Locale.getDefault()));
            }
        };

        return new I18nHelperImpl(ImmutableList.of(provider));
    }

    @Test
    public void formattedMessageIncludesCounts()
    {
        LicenseResourceLimitListener lrll = new LicenseResourceLimitListener();

        CrowdLicense license = mock(CrowdLicense.class);
        when(license.getMaximumNumberOfUsers()).thenReturn(500);

        CrowdLicenseManager crowdLicenseManager = mock(CrowdLicenseManager.class);
        when(crowdLicenseManager.getLicense()).thenReturn(license);

        lrll.setCrowdLicenseManager(crowdLicenseManager);

        lrll.setI18nHelper(i18HelperUsingRealResources());

        LicenseResourceLimitEvent event = new LicenseResourceLimitEvent(null, 123);

        String body = lrll.formatBody(event);

        assertThat(body, Matchers.containsString("This is a reminder that you have reached 123 users of your 500 user license."));
    }
}
