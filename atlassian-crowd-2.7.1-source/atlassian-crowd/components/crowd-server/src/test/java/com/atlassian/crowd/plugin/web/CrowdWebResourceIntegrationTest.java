package com.atlassian.crowd.plugin.web;

import java.io.File;

import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceIntegration;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

public class CrowdWebResourceIntegrationTest
{
    @Test
    public void temporaryDirectoryIsInASubdirectoryOfJavaIoTmpdir()
    {
        PluginAccessor pluginAccessor = mock(PluginAccessor.class);
        ClientProperties clientProperties = mock(ClientProperties.class);
        WebResourceIntegration wri = new CrowdWebResourceIntegration(pluginAccessor, clientProperties);

        File wrtd = wri.getTemporaryDirectory();

        File tmpDir = new File(System.getProperty("java.io.tmpdir"));

        assertThat(wrtd.getAbsolutePath(), Matchers.startsWith(tmpDir.getAbsolutePath()));
        assertThat(wrtd.getAbsolutePath(), Matchers.not(tmpDir.getAbsolutePath()));
    }

    @Test
    public void knowsAboutPdl()
    {
        WebResourceIntegration wri = new CrowdWebResourceIntegration(null, null);
        assertNotNull(wri.getResourceSubstitutionVariables().get("pdl.dir"));
    }
}
