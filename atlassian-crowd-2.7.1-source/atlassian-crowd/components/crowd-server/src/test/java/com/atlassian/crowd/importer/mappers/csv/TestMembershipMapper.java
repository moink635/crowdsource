package com.atlassian.crowd.importer.mappers.csv;

import com.atlassian.crowd.importer.config.CsvConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.model.MembershipDTO;

import org.apache.commons.collections.OrderedBidiMap;
import org.apache.commons.collections.bidimap.TreeBidiMap;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class TestMembershipMapper
{
    private MembershipMapper membershipMapper;

    @Before
    public void setUp() throws Exception
    {
        OrderedBidiMap configuration = new TreeBidiMap();

        configuration.put("group.0", CsvConfiguration.GROUP_NAME);
        configuration.put("group.1", CsvConfiguration.GROUP_USERNAME);

        membershipMapper = new MembershipMapper(new Long(1), configuration);
    }

    @Test
    public void testMapRow() throws ImporterException
    {
        MembershipDTO membership = membershipMapper.mapRow(new String[]{"jira-admin", "psmith"});

        assertNotNull(membership);
        assertEquals("jira-admin", membership.getParentName());
        assertEquals("psmith", membership.getChildName());
        assertEquals(MembershipDTO.ChildType.USER, membership.getChildType());
    }

    @Test(expected = NullPointerException.class)
    public void testMapRowWithNullData() throws ImporterException
    {
        membershipMapper.mapRow(null);
    }

    @Test(expected = ImporterException.class)
    public void testMapRowWithIncompleteData() throws ImporterException
    {
        membershipMapper.mapRow(new String[]{"jira-admin"});
    }
}
