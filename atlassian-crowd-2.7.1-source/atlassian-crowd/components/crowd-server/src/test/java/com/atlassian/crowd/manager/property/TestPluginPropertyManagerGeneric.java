package com.atlassian.crowd.manager.property;

import com.atlassian.crowd.dao.property.PropertyDAO;
import junit.framework.TestCase;


public class TestPluginPropertyManagerGeneric extends TestCase
{
    private PropertyDAO propertyDAO;
    
    public void setUp()
    {
        propertyDAO = new MockPropertyDAO();
    }
        
    
    public void testGetSet() throws Exception
    {
        PluginPropertyManagerGeneric pluginPropertyManagerGeneric = new PluginPropertyManagerGeneric(propertyDAO);
        final String value = "c";
        final String name = "b";
        final String key = "a";
        pluginPropertyManagerGeneric.setProperty(key, name, value);
        final String result = pluginPropertyManagerGeneric.getProperty(key, name);
        
        assertEquals(value, result);

    }

}
