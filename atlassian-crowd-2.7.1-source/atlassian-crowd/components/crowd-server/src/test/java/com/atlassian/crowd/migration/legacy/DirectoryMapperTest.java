package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.apache.commons.lang3.time.DateUtils;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.text.SimpleDateFormat;
import java.util.*;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;
import static org.mockito.Mockito.mock;

public class DirectoryMapperTest extends BaseLegacyImporterTest
{
    private final List<Object> addedObjects = new ArrayList();
    private Object returnObject;
    private DirectoryMapper mapper;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        // These legacy xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        BatchProcessor batchProcessor = mock(BatchProcessor.class);
        mapper = new DirectoryMapper(sessionFactory, batchProcessor)
        {
            @Override
            protected Object addEntityViaMerge(final Object entityToPersist)
            {
                addedObjects.add(entityToPersist);
                return returnObject;
            }
        };
    }

    @Override
    protected void tearDown() throws Exception
    {
        document = null;
        super.tearDown();
    }

    public void testImportDirectoryXML() throws Exception
    {
        Directory directory1Mock = directoryWithIdAndName(1, "ApacheDS Delegated Directory");
        returnObject = directory1Mock;

        SimpleDateFormat sdf = new SimpleDateFormat(GenericLegacyImporter.DATE_FORMAT);
        Element directoriesNode = (Element) document.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + DirectoryMapper.DIRECTORY_XML_ROOT);
        final List<Element> directoryElements = directoriesNode.elements(DirectoryMapper.DIRECTORY_XML_NODE);

        // check directory 1 (some perms, some attribs)

        DirectoryImpl directory1 = mapper.importDirectoryFromXml(directoryElements.get(0), oldToNewDirectoryIds);

        assertNotNull(directory1);
        assertNull(directory1.getId());
        assertEquals("ApacheDS Delegated Directory", directory1.getName());
        assertTrue(directory1.isActive());
        assertTrue(DateUtils.isSameInstant(sdf.parse("Wed Feb 13 11:15:53 +1100 2008"), directory1.getCreatedDate()));
        assertTrue(DateUtils.isSameInstant(sdf.parse("Fri Apr 11 14:54:55 +1000 2008"), directory1.getUpdatedDate()));
        assertEquals(toLowerCase("ApacheDS Delegated Directory"), directory1.getLowerName());
        assertEquals("com.atlassian.crowd.directory.DelegatedAuthenticationDirectory", directory1.getImplementationClass());
//        assertEquals("com.atlassian.crowd.integration.directory.delegated.DelegatedAuthenticationDirectory", directory1.getImplementationClass());
        assertEquals("com.atlassian.crowd.directory.DelegatedAuthenticationDirectory".toLowerCase(Locale.ENGLISH), directory1.getLowerImplementationClass());
//        assertEquals("com.atlassian.crowd.integration.directory.delegated.DelegatedAuthenticationDirectory".toLowerCase(), directory1.getLowerImplementationClass());
        assertEquals("", directory1.getDescription());
        assertEquals(DirectoryType.DELEGATING, directory1.getType());

        Set<OperationType> expectedOperations = EnumSet.copyOf(ALL_PRE_20_OPERATION_TYPES);
        expectedOperations.remove(OperationType.CREATE_USER);
        assertEquals(expectedOperations, directory1.getAllowedOperations());

        assertEquals(4, directory1.getKeys().size());
        assertEquals("inetorgperson", directory1.getValue("ldap.user.objectclass"));
        assertEquals("dc=example,dc=com", directory1.getValue("ldap.basedn"));
        assertEquals("givenname", directory1.getValue("ldap.user.firstname"));
        assertEquals("false", directory1.getValue("ldap.pagedresults"));


        Directory directory2Mock = directoryWithIdAndName(2, "ApacheDS Directory");
        returnObject = directory2Mock;

        // check directory 2 (no perms, no attribs)

        DirectoryImpl directory2 = mapper.importDirectoryFromXml(directoryElements.get(1), oldToNewDirectoryIds);

        assertNotNull(directory2);
        assertNull(directory2.getId());
        assertEquals("ApacheDS Directory", directory2.getName());
        assertFalse(directory2.isActive());
        assertTrue(DateUtils.isSameInstant(sdf.parse("Wed Feb 13 11:16:47 +1100 2008"), directory2.getCreatedDate()));
        assertTrue(DateUtils.isSameInstant(sdf.parse("Thu Apr 10 09:56:24 +1000 2008"), directory2.getUpdatedDate()));
        assertEquals(toLowerCase("ApacheDS Directory"), directory2.getLowerName());
        assertEquals("com.atlassian.crowd.directory.ApacheDS", directory2.getImplementationClass());
//        assertEquals("com.atlassian.crowd.integration.directory.connector.ApacheDS", directory2.getImplementationClass());
        assertEquals("com.atlassian.crowd.directory.ApacheDS".toLowerCase(Locale.ENGLISH), directory2.getLowerImplementationClass());
//        assertEquals("com.atlassian.crowd.integration.directory.connector.ApacheDS".toLowerCase(), directory2.getLowerImplementationClass());
        assertEquals("I have a description", directory2.getDescription());
        assertEquals(DirectoryType.CONNECTOR, directory2.getType());

        assertEquals(EnumSet.allOf(OperationType.class), directory2.getAllowedOperations());

        assertEquals(0, directory2.getKeys().size());

        // check the produced map
        assertEquals(new Long(1L), oldToNewDirectoryIds.get(1048577L));
        assertEquals(new Long(2L), oldToNewDirectoryIds.get(1048578L));

        assertEquals(2, addedObjects.size());
    }
}