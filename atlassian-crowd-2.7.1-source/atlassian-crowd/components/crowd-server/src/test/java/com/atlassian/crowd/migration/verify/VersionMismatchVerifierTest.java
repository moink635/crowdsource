package com.atlassian.crowd.migration.verify;

import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.util.build.BuildUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertTrue;

public class VersionMismatchVerifierTest
{
    private Verifier verfier;
    private Document document;

    @Before
    public void setUp()
    {
        verfier = new VersionMismatchVerifier();

        document = DocumentHelper.createDocument();
        Element root = document.addElement(XmlMigrationManagerImpl.XML_ROOT);

        // add the high level root elements that specify just what Crowd version we are exporting.
        root.addElement(XmlMigrationManagerImpl.CROWD_XML_DATE).addText((new Date()).toString());
        root.addElement(XmlMigrationManagerImpl.CROWD_XML_VERSION).addText("10.0.0");
        root.addElement(XmlMigrationManagerImpl.CROWD_XML_BUILD_NUMBER).addText("100000");
        root.addElement(XmlMigrationManagerImpl.CROWD_XML_BUILD_DATE).addText(BuildUtils.BUILD_DATE);
    }

    @Test
    public void testVerify() throws Exception
    {
        verfier.verify(document.getRootElement());

        assertTrue(verfier.hasErrors());
    }
}