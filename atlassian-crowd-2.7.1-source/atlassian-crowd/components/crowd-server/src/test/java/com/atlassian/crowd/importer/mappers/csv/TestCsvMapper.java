package com.atlassian.crowd.importer.mappers.csv;

import com.atlassian.crowd.importer.config.CsvConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;

import org.apache.commons.collections.OrderedBidiMap;
import org.apache.commons.collections.bidimap.TreeBidiMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class TestCsvMapper
{
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    // Implementation of a CsvMapper
    private CsvMapper<?> cvsMapper;

    private static CsvMapper<?> createMapper(long directoryId, OrderedBidiMap configuration)
    {
        return new CsvMapper<Void>(directoryId, configuration)
        {
            @Override
            Void mapRow(String[] resultSet) throws ImporterException
            {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Before
    public void setUp() throws Exception
    {
        OrderedBidiMap configuration = new TreeBidiMap();
        configuration.put("principal.0", CsvConfiguration.USER_FIRSTNAME);

        cvsMapper = createMapper(1, configuration);
    }

    @Test
    public void testGetStringFirstName() throws Exception
    {
        String firstname = cvsMapper.getString(new String[]{"Peter"}, CsvConfiguration.USER_FIRSTNAME);

        assertNotNull(firstname);
        assertEquals("Peter", firstname);
    }

    @Test
    public void testGetStringFirstNameWithSpace() throws Exception
    {
        String firstname = cvsMapper.getString(new String[]{"   Peter   "}, CsvConfiguration.USER_FIRSTNAME);

        assertNotNull(firstname);
        assertEquals("Peter", firstname);
    }

    @Test
    public void testGetStringWithNull() throws Exception
    {
        String firstname = cvsMapper.getString(new String[]{null}, CsvConfiguration.USER_FIRSTNAME);

        assertNull(firstname);
    }

    @Test
    public void requestForAbsentColumnCausesException() throws ImporterException
    {
        expectedException.expect(ImporterException.class);
        expectedException.expectMessage("Missing column for user.firstname: []");
        cvsMapper.getString(new String[]{}, CsvConfiguration.USER_FIRSTNAME);
    }

    @Test
    public void nullLinesFailImmediately() throws ImporterException
    {
        expectedException.expect(NullPointerException.class);
        cvsMapper.getString(null, "key");
    }

    @Test
    public void nullKeyFailImmediately() throws ImporterException
    {
        expectedException.expect(NullPointerException.class);
        cvsMapper.getString(new String[]{}, null);
    }

    @Test
    public void badConfigurationWithoutIndexOnColumnKeyCausesException() throws ImporterException
    {
        OrderedBidiMap configuration = new TreeBidiMap();
        configuration.put("principal", CsvConfiguration.USER_FIRSTNAME);

        CsvMapper<?> mapper = createMapper(1, configuration);


        expectedException.expect(ImporterException.class);
        expectedException.expectMessage("Bad column mapping for user.firstname: principal");
        mapper.getString(new String[]{}, CsvConfiguration.USER_FIRSTNAME);
    }

    @Test
    public void hasColumnForShowsWhichKeysAreConfigured()
    {
        assertTrue(cvsMapper.hasColumnFor(CsvConfiguration.USER_FIRSTNAME));
        assertFalse(cvsMapper.hasColumnFor("unknown-key"));
    }

    @After
    public void tearDown() throws Exception
    {
        cvsMapper = null;
    }
}
