package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

import static com.atlassian.crowd.embedded.api.OperationType.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class UpgradeTask452Test
{
    @Mock private ApplicationDAO applicationDao;
    @Mock private DirectoryDao directoryDao;

    @Mock private Application application;
    @Mock private  Directory directory;

    private UpgradeTask452 upgradeTask;

    @Before
    public void setUp()
    {
        upgradeTask = new UpgradeTask452();
        upgradeTask.setApplicationDao(applicationDao);
        upgradeTask.setDirectoryDao(directoryDao);
    }

    @Test
    public void testDoUpgrade_Directories() throws Exception
    {
        when(applicationDao.search(any(EntityQuery.class))).thenReturn(Collections.<Application>emptyList());

        DirectoryImpl directory1 = new DirectoryImpl("dir", DirectoryType.CUSTOM, "");
        directory1.setAllowedOperations(EnumSet.of(UPDATE_GROUP, UPDATE_USER, UPDATE_ROLE));
        DirectoryImpl directory2 = new DirectoryImpl("dir2", DirectoryType.CUSTOM, "");
        directory2.setAllowedOperations(EnumSet.of(CREATE_GROUP));

        when(directoryDao.findAll()).thenReturn(Arrays.<Directory>asList(directory1, directory2));

        upgradeTask.doUpgrade();

        verify(directoryDao, times(2)).update(argThat(new HasDirectoryOperationsUpdated()));
    }

    private static class HasDirectoryOperationsUpdated extends ArgumentMatcher<Directory>
    {
        @Override
        public boolean matches(final Object argument)
        {
            final Directory directory = (Directory) argument;
            final Set<OperationType> allowedOperations = directory.getAllowedOperations();
            return allowedOperations.contains(UPDATE_GROUP) == allowedOperations.contains(UPDATE_GROUP_ATTRIBUTE) &&
                    allowedOperations.contains(UPDATE_USER) == allowedOperations.contains(UPDATE_USER_ATTRIBUTE) &&
                    allowedOperations.contains(UPDATE_ROLE) == allowedOperations.contains(UPDATE_ROLE_ATTRIBUTE);
        }
    }

    @Test
    public void testDoUpgrade_Applications() throws Exception
    {
        when(directoryDao.findAll()).thenReturn(Collections.<Directory>emptyList());

        DirectoryMapping directoryMapping1 = new DirectoryMapping(application, directory, true, EnumSet.of(UPDATE_GROUP, UPDATE_USER, UPDATE_ROLE));
        DirectoryMapping directoryMapping2 = new DirectoryMapping(application, directory, true, EnumSet.of(CREATE_GROUP));

        when(applicationDao.search(any(EntityQuery.class))).thenReturn(Arrays.asList(application));
        when(application.getId()).thenReturn(0L);
        when(directory.getId()).thenReturn(0L);
        when(application.getDirectoryMappings()).thenReturn(Arrays.asList(directoryMapping1, directoryMapping2));

        upgradeTask.doUpgrade();

        verify(applicationDao, times(2)).updateDirectoryMapping(eq(0L), eq(0L), eq(true), argThat(new HasOperationsUpdated()));
    }

    private static class HasOperationsUpdated extends ArgumentMatcher<Set<OperationType>>
    {
        @Override
        public boolean matches(final Object argument)
        {
            final Set<OperationType> allowedOperations = (Set<OperationType>) argument;
            return allowedOperations.contains(UPDATE_GROUP) == allowedOperations.contains(UPDATE_GROUP_ATTRIBUTE) &&
                    allowedOperations.contains(UPDATE_USER) == allowedOperations.contains(UPDATE_USER_ATTRIBUTE) &&
                    allowedOperations.contains(UPDATE_ROLE) == allowedOperations.contains(UPDATE_ROLE_ATTRIBUTE);
        }
    }
}
