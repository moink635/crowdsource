package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.config.ConfigurationException;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.property.PropertyManagerGeneric;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.database.sql.MySQLLegacyTableQueries;
import com.atlassian.crowd.migration.legacy.database.sql.PostgresLegacyTableQueries;
import com.atlassian.crowd.model.property.Property;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class PropertyMapperTest extends BaseDatabaseTest
{
    private PropertyMapper propertyMapper;
    private CrowdBootstrapManager bootstrapManager;
    private Map<String, String> actualProperties;

    @Override
    protected void onSetUp() throws Exception
    {
        super.onSetUp();
        bootstrapManager = mock(CrowdBootstrapManager.class);
        propertyMapper = new PropertyMapper(sessionFactory, batchProcessor, jdbcTemplate, propertyDAO, bootstrapManager);

        // Use a map for actual properties so can assert each property individually
        actualProperties = new HashMap<String, String>();
        actualProperties.put("cache.enabled", "true");
        actualProperties.put("current.license.resource.total", "0");
        actualProperties.put("database.token.storage.enabled", "true");
        actualProperties.put("deployment.title", "Test Crowd Migration");
        actualProperties.put("des.encryption.key", "6dMZIOr0MZc=");
        actualProperties.put("gzip.enabled", "true");
        actualProperties.put("mailserver.host", "localhost");
        actualProperties.put("mailserver.jndi", "");
        actualProperties.put("mailserver.message.template", "Hello $firstname $lastname, Your password has been reset by a $deploymenttitle administrator at $date. Your new password is: $password $deploymenttitle Administrator");
        actualProperties.put("mailserver.password", "");
        actualProperties.put("mailserver.port", "25");
        actualProperties.put("mailserver.prefix", "[Test Crowd Migration - Atlassian Crowd]");
        actualProperties.put("mailserver.sender", "admin@example.com");
        actualProperties.put("mailserver.username", "");
        actualProperties.put("notification.email", "admin@example.com");
        actualProperties.put("secure.cookie", "false");
        actualProperties.put("session.time", "1800000");
        actualProperties.put("token.seed", "YFWGPzH6");
        actualProperties.put("build.number", "342"); // This is the legacy build number - should be changed when Crowd is actually up and runnning
    }

    // Note: Testing uses HSQLDB and not MySQL or Postgres.
    // Though still running both scripts to catch any typos, possible errors since HSQLDB can handle them.
    // This will not be able to catch MySQL or Postgres specific database quirks.
    public void testMySQLScripts() throws ConfigurationException, ImportException
    {
        propertyMapper.setLegacyTableQueries(new MySQLLegacyTableQueries());
        _testGetProperties();
    }

    public void testPostgresScripts() throws ConfigurationException, ImportException
    {
        propertyMapper.setLegacyTableQueries(new PostgresLegacyTableQueries());
        _testGetProperties();
    }

    public void testMySQLScriptsLegacyProperties() throws ConfigurationException, ImportException
    {
        propertyMapper.setLegacyTableQueries(new MySQLLegacyTableQueries());
        _testWithLegacyProperties();
    }

    public void testPostgresScriptsLegacyProperties() throws ConfigurationException, ImportException
    {
        propertyMapper.setLegacyTableQueries(new PostgresLegacyTableQueries());
        _testWithLegacyProperties();
    }

    public void _testGetProperties() throws ImportException, ConfigurationException
    {
        List<Property> properties = propertyMapper.importPropertiesFromDatabase();
        assertEquals(19, properties.size());
        for (Property property : properties)
        {
            String name = property.getName();
            assertEquals("Property value for <" + name + "> did not match", actualProperties.get(name), property.getValue());
            assertEquals(Property.CROWD_PROPERTY_KEY, property.getKey());
        }

        verify(bootstrapManager, never()).setServerID(anyString());
    }

    public void _testWithLegacyProperties() throws ImportException, ConfigurationException
    {
        // Add some legacy property values
        jdbcTemplate.execute("INSERT INTO SERVERPROPERTY(ID, NAME, VALUE) VALUES (100, 19, 'rRNswPPRnppPrnQMNqSvrmpRrOPmnRqmMWWTXUUUsTwsOVSvvvwSvrWuuTvTVvvMNpNqsmvUUnnvrnmsqmmmmmUUnnvrnmsqmmmmmUU1qiXppfXkWJlcqtXobWJvpqbjpUUnmmmm');");
        jdbcTemplate.execute("INSERT INTO SERVERPROPERTY(ID, NAME, VALUE) VALUES (101, 22, 'A9CK-B4GA-0OKC-94CZ');");

        // everything should be the same - no extra properites added (except bootstrapManager should be called)
        List<Property> properties = propertyMapper.importPropertiesFromDatabase();
        assertEquals(19, properties.size());
        for (Property property : properties)
        {
            String name = property.getName();
            assertEquals("Property value for <" + name + "> did not match", actualProperties.get(name), property.getValue());
            assertEquals(Property.CROWD_PROPERTY_KEY, property.getKey());
        }

        // bootstrapManger setServerID should have been called
        verify(bootstrapManager, times(1)).setServerID("A9CK-B4GA-0OKC-94CZ");
    }
}
