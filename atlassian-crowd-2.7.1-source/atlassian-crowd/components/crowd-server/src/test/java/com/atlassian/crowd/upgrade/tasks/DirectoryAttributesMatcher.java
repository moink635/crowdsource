package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.embedded.api.Directory;
import org.mockito.ArgumentMatcher;

import java.util.Map;

/**
 * Checks if the directory attributes matches the criteria.
 *
 * @since 2.2
 */
public abstract class DirectoryAttributesMatcher extends ArgumentMatcher<Directory>
{
    private final String directoryName;

    private DirectoryAttributesMatcher(final String directoryName)
    {
        this.directoryName = directoryName;
    }

    @Override
    public boolean matches(final Object argument)
    {
        if (!(argument instanceof Directory))
        {
            return false;
        }
        else
        {
            Directory directory = (Directory) argument;
            if (directoryName != null && !directoryName.equals(directory.getName()))
            {
                return false;
            }
            else
            {
                return attributesMatches(directory.getAttributes());
            }
        }
    }

    protected abstract boolean attributesMatches(final Map<String, String> attributes);

    /**
     * Matches attributes containing the specified attribute.
     *
     * @param attributeName name of attribute
     * @return <tt>true</tt> if the directory contains the specified attribute, otherwise <tt>false</tt>
     */
    public static DirectoryAttributesMatcher contains(final String attributeName)
    {
        return containsForDirectory(null, attributeName);
    }

    /**
     * Matches the directory with a name of <tt>directoryName</tt> and attributes containing the specified attribute.
     *
     * @param directoryName name of directory to match
     * @param attributeName name of attribute
     * @return <tt>true</tt> if the directory contains the specified attribute, otherwise <tt>false</tt>
     */
    public static DirectoryAttributesMatcher containsForDirectory(final String directoryName, final String attributeName)
    {
        return new DirectoryAttributesMatcher(directoryName)
        {
            @Override
            protected boolean attributesMatches(final Map<String, String> attributes)
            {
                return attributes.containsKey(attributeName);
            }
        };
    }

    /**
     * Matches attributes containing the specified attribute and value.
     *
     * @param attributeName name of attribute
     * @param attributeValue value of the attribute
     * @return <tt>true</tt> if the directory contains the specified attribute and value, otherwise <tt>false</tt>
     */
    public static DirectoryAttributesMatcher contains(final String attributeName, final String attributeValue)
    {
        return containsForDirectory(null, attributeName, attributeValue);
    }

    /**
     * Matches attributes containing the specified attribute and value.
     *
     * @param directoryName name of the directory
     * @param attributeName name of attribute
     * @param attributeValue value of the attribute
     * @return <tt>true</tt> if the directory contains the specified attribute and value, otherwise <tt>false</tt>
     */
    public static DirectoryAttributesMatcher containsForDirectory(final String directoryName, final String attributeName, final String attributeValue)
    {
        return new DirectoryAttributesMatcher(directoryName)
        {
            @Override
            protected boolean attributesMatches(final Map<String, String> attributes)
            {
                return attributes.get(attributeName).equals(attributeValue);
            }
        };
    }

    /**
     * Matches attributes that does not contain the specified attribute.
     *
     * @param attributeName name of attribute
     * @return <tt>true</tt> if the directory does not contain the specified attribute, otherwise <tt>false</tt>
     */
    public static DirectoryAttributesMatcher notContains(final String attributeName)
    {
        return notContainsForDirectory(null, attributeName);
    }

    /**
     * Matches attributes that does not contain the specified attribute.
     *
     * @param directoryName name of the directory
     * @param attributeName name of attribute
     * @return <tt>true</tt> if the directory does not contain the specified attribute, otherwise <tt>false</tt>
     */
    public static DirectoryAttributesMatcher notContainsForDirectory(final String directoryName, final String attributeName)
    {
        return new DirectoryAttributesMatcher(directoryName)
        {
            @Override
            protected boolean attributesMatches(final Map<String, String> attributes)
            {
                return !attributes.containsKey(attributeName);
            }
        };
    }

    /**
     * Returns <tt>true</tt> if the directory attributes exactly matches given attributes.
     *
     * @param expectedAttributes the expected attributes
     * @return <tt>true</tt> if the directory attributes exactly matches the given attributes, otherwise <tt>false</tt>
     */
    public static DirectoryAttributesMatcher exactlyMatches(final Map<String, String> expectedAttributes)
    {
        return exactlyMatchesForDirectory(null, expectedAttributes);
    }

    /**
     * Returns <tt>true</tt> if the directory attributes exactly matches given attributes.
     *
     * @param directoryName name of the directory
     * @param expectedAttributes the expected attributes
     * @return <tt>true</tt> if the directory attributes exactly matches the given attributes, otherwise <tt>false</tt>
     */
    public static DirectoryAttributesMatcher exactlyMatchesForDirectory(final String directoryName, final Map<String, String> expectedAttributes)
    {
        return new DirectoryAttributesMatcher(directoryName)
        {
            @Override
            protected boolean attributesMatches(final Map<String, String> attributes)
            {
                return expectedAttributes.equals(attributes);
            }
        };
    }
}
