package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.upgrade.util.UpgradeUtilityDAOHibernate;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpgradeTask523Test
{
    private UpgradeTask523 task;

    @Mock private UpgradeUtilityDAOHibernate dao;

    @Test
    public void testDoUpgrade() throws Exception
    {
        task = new UpgradeTask523();
        task.setUpgradeUtilityDao(dao);

        task.doUpgrade();

        verify(dao).executeBulkUpdate("update InternalGroup g set local = true where g.directory in (select d from DirectoryImpl d where type = ?1)",
                                      DirectoryType.DELEGATING);
    }
}
