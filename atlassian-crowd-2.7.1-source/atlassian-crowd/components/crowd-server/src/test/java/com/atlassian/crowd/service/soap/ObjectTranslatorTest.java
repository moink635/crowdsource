package com.atlassian.crowd.service.soap;

import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPEntity;
import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SOAPNestableGroup;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.soap.SOAPRole;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupTemplateWithAttributes;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;
import com.google.common.collect.Sets;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ObjectTranslatorTest extends TestCase
{
    private SOAPGroup group1;
    private SOAPGroup[] soapGroups;
    private SOAPNestableGroup[] soapNestableGroups;
    private ArrayList<SOAPNestableGroup> soapNestableGroupCollection;

    private SOAPRole role1;
    private SOAPRole[] soapRoles;

    private UserTemplate user1;
    private SOAPPrincipal soapPrincipal;
    private ArrayList<User> userCollection;
    private UserTemplateWithAttributes userWithAttributes;

    private static final String[] SINGLE_MEMBER_LIST = new String[]{"John"};
    private static final String[] MULTIPLE_MEMBER_LIST = new String[]{"Bob", "Ben", "David", "Mark", "Jon"};

    private static final String ATTRIBUTE_NAME_1 = "colour";
    private static final String ATTRIBUTE_VALUE_1 = "Azure";
    private static final String ATTRIBUTE_NAME_2 = "food";
    private static final String[] ATTRIBUTE_VALUE_2 = new String[]{"Chocolate", "Coke", "Pie"};
    private static final String NULL_ATTRIBUTE = "null-attribute";

    private static final String FIRST_NAME = "Bob";
    private static final String LAST_NAME = "Smith";
    private static final String EMAIL_ADDRESS = "bob@smith.com";
    private SOAPPrincipal soapPrincipalWithAttributes;

    public void setUp()
    {
        // Set up the groups
        group1 = new SOAPGroup("Group1", SINGLE_MEMBER_LIST);
        group1.setDirectoryId(new Long(1));
        SOAPGroup group2 = new SOAPGroup("Group2", MULTIPLE_MEMBER_LIST);
        group2.setDirectoryId(new Long(1));

        SOAPNestableGroup nestedGroup = new SOAPNestableGroup("NestedGroup", new String[]{"Group1", "Group2"});
        soapGroups = new SOAPGroup[]{group1, group2};
        soapNestableGroups = new SOAPNestableGroup[]{nestedGroup};
        soapNestableGroupCollection = new ArrayList<SOAPNestableGroup>();
        soapNestableGroupCollection.add(nestedGroup);

        // Set up a role
        role1 = new SOAPRole("Role1", SINGLE_MEMBER_LIST);
        SOAPRole role2 = new SOAPRole("Role2", MULTIPLE_MEMBER_LIST);
        soapRoles = new SOAPRole[]{role1, role2};

        // Set up users
        user1 = new UserTemplate("User1", new Long(1));
        user1.setFirstName(FIRST_NAME);
        user1.setLastName(LAST_NAME);
        user1.setEmailAddress(EMAIL_ADDRESS);
        User user2 = new UserTemplate("User2", new Long(1));

        userCollection = new ArrayList<User>();
        userCollection.add(user1);
        userCollection.add(user2);

        soapPrincipal = new SOAPPrincipal(new Long(2), "SoapPrincipal", new Long(1), "Soapy person", true, null, null, null);

        userWithAttributes = UserTemplateWithAttributes.ofUserWithNoAttributes(user1);
        userWithAttributes.setAttribute(ATTRIBUTE_NAME_1, ATTRIBUTE_VALUE_1);
        userWithAttributes.setAttribute(ATTRIBUTE_NAME_2, Sets.newHashSet(ATTRIBUTE_VALUE_2));
        // Try to add a null attribute - should just skip/ignore
        Set<String> nullArray = null;
        userWithAttributes.setAttribute(NULL_ATTRIBUTE, nullArray);

        soapPrincipalWithAttributes = new SOAPPrincipal(new Long(2), "SoapPrincipal", new Long(1), "Soapy person", true, null, null, new SOAPAttribute[]{new SOAPAttribute(ATTRIBUTE_NAME_1, ATTRIBUTE_VALUE_1), new SOAPAttribute(ATTRIBUTE_NAME_2, ATTRIBUTE_VALUE_2), new SOAPAttribute(UserConstants.FIRSTNAME, FIRST_NAME)});
    }

    public void testProcessSOAPGroupAndMemberNamesPreserveCase()
    {
        SOAPGroup[] processedGroups = ObjectTranslator.processSOAPGroupAndMemberNames(soapGroups);

        assertEquals(2, processedGroups.length);
        // Cases for names should be preserved
        assertContainsElements(processedGroups, "Group1", "Group2");
        assertContainsElements(processedGroups[0].getMembers(), SINGLE_MEMBER_LIST);
        assertContainsElements(processedGroups[1].getMembers(), MULTIPLE_MEMBER_LIST);
    }

    public void testProcessSOAPGroupAndMemberNamesForceLowerNestedGroup()
    {
        SOAPNestableGroup[] processedGroups = ObjectTranslator.processSOAPGroupAndMemberNames(soapNestableGroups);

        assertEquals(1, processedGroups.length);
        // Names are now in lower case
        assertContainsElements(processedGroups, "NestedGroup");
        assertContainsElements(processedGroups[0].getGroupMembers(), "Group1", "Group2");
    }

    public void testProcessSOAPRoleAndMemberNamesPreserveCase()
    {
        SOAPRole[] processedRoles = ObjectTranslator.processSOAPRoleAndMemberNames(soapRoles);

        assertEquals(2, processedRoles.length);
        // Cases for names should be preserved
        assertContainsElements(processedRoles, "Role1", "Role2");
        assertContainsElements(processedRoles[0].getMembers(), SINGLE_MEMBER_LIST);
        assertContainsElements(processedRoles[1].getMembers(), MULTIPLE_MEMBER_LIST);
    }

    public void testProcessSOAPNestableGroupAndMemberNames()
    {
        SOAPNestableGroup[] processedGroups = ObjectTranslator.processSOAPNestableGroupAndMemberNames(soapNestableGroupCollection);

        assertEquals(1, processedGroups.length);
        // Cases for names should be preserved
        assertContainsElements(processedGroups, "NestedGroup");
        assertContainsElements(processedGroups[0].getGroupMembers(), "Group1", "Group2");
    }

    public void testProcessUsers()
    {
        SOAPPrincipal[] processedUsers = ObjectTranslator.processUsers(userCollection);

        assertEquals(2, processedUsers.length);
        assertContainsElements(processedUsers, "User1", "User2");
        assertEquals("User1", processedUsers[0].getName());
        assertEquals("User2", processedUsers[1].getName());
    }

    public void testProcessUser()
    {
        SOAPPrincipal processedUsers = ObjectTranslator.processUser(user1);

        assertEquals("User1", processedUsers.getName());

        // Check the attributes - only care about User so only the 3 primary attributes
        assertEquals(3, processedUsers.getAttributes().length);
        assertContainsElements(processedUsers.getAttribute(UserConstants.FIRSTNAME).getValues(), FIRST_NAME);
        assertContainsElements(processedUsers.getAttribute(UserConstants.LASTNAME).getValues(), LAST_NAME);
        assertContainsElements(processedUsers.getAttribute(UserConstants.EMAIL).getValues(), EMAIL_ADDRESS);
    }

    public void testProcessUserSOAPPrincipal()
    {
        UserTemplate userTemplate = ObjectTranslator.processUser(soapPrincipal);
        assertEquals(soapPrincipal.getName(), userTemplate.getName());
        assertTrue(userTemplate.isActive());
    }

    public void testProcessUserWithAttributes()
    {
        SOAPPrincipal processedUsers = ObjectTranslator.processUserWithAttributes(userWithAttributes);

        assertEquals("User1", processedUsers.getName());

        // Check the attributes (3 primary + 2 custom)
        assertEquals(5, processedUsers.getAttributes().length);
        assertContainsElements(processedUsers.getAttribute(UserConstants.FIRSTNAME).getValues(), FIRST_NAME);
        assertContainsElements(processedUsers.getAttribute(UserConstants.LASTNAME).getValues(), LAST_NAME);
        assertContainsElements(processedUsers.getAttribute(UserConstants.EMAIL).getValues(), EMAIL_ADDRESS);
        assertContainsElements(processedUsers.getAttribute(ATTRIBUTE_NAME_1).getValues(), ATTRIBUTE_VALUE_1);
        assertContainsElements(processedUsers.getAttribute(ATTRIBUTE_NAME_2).getValues(), ATTRIBUTE_VALUE_2);

        // Check null attribute was ignored
        assertNull(processedUsers.getAttribute(NULL_ATTRIBUTE));
    }

    public void testProcessGroup()
    {
        GroupTemplate groupTemplate = ObjectTranslator.processGroup(group1);
        assertEquals(group1.getName(), groupTemplate.getName());
        assertEquals(group1.getDescription(), groupTemplate.getDescription());
        assertEquals(group1.isActive(), groupTemplate.isActive());
    }

    public void testProcessNestableGroup()
    {
        GroupTemplate group = new GroupTemplate("NestedGroup", 1l, GroupType.GROUP);
        group.setDescription("Nested Group");
        group.setActive(true);
        SOAPNestableGroup soapGroup = ObjectTranslator.processNestableGroup(group, Arrays.asList("Group1", "Group2"));

        assertEquals("NestedGroup", soapGroup.getName());
        assertEquals(group.getDescription(), soapGroup.getDescription());
        assertTrue(soapGroup.isActive());
        assertEquals(1l, soapGroup.getDirectoryId());
        assertContainsElements(soapGroup.getGroupMembers(), "Group1", "Group2");
        assertEquals(0, soapGroup.getAttributes().length);
    }

    public void testProcessGroupNestedUserMembers()
    {
        GroupTemplate group = new GroupTemplate("SomeGroup", new Long(1), GroupType.GROUP);
        group.setActive(true);
        group.setDescription("Some Odd Group");
        SOAPGroup soapGroup = ObjectTranslator.processGroup(group, Arrays.asList(MULTIPLE_MEMBER_LIST));
        assertEquals(group.getName(), soapGroup.getName());
        assertEquals(group.getDescription(), soapGroup.getDescription());
        assertEquals(group.isActive(), soapGroup.isActive());
        assertContainsElements(soapGroup.getMembers(), MULTIPLE_MEMBER_LIST);
        assertEquals(0, soapGroup.getAttributes().length);
    }

    public void testProcessGroupWithAttributes()
    {
        GroupTemplateWithAttributes group = new GroupTemplateWithAttributes("GroupWithAttribute", new Long(1), GroupType.GROUP);
        group.setActive(true);
        group.setDescription("Group With attributes!");
        group.setAttribute(ATTRIBUTE_NAME_1, ATTRIBUTE_VALUE_1);
        group.setAttribute(ATTRIBUTE_NAME_2, Sets.newHashSet(ATTRIBUTE_VALUE_2));
        SOAPGroup soapGroup = ObjectTranslator.processGroupWithAttributes(group, Arrays.asList(MULTIPLE_MEMBER_LIST));
        assertEquals(group.getName(), soapGroup.getName());
        assertEquals(group.getDescription(), soapGroup.getDescription());
        assertEquals(group.isActive(), soapGroup.isActive());
        assertContainsElements(soapGroup.getMembers(), MULTIPLE_MEMBER_LIST);
        // Check attributes
        assertEquals(2, soapGroup.getAttributes().length);
        assertContainsElements(soapGroup.getAttribute(ATTRIBUTE_NAME_1).getValues(), ATTRIBUTE_VALUE_1);
        assertContainsElements(soapGroup.getAttribute(ATTRIBUTE_NAME_2).getValues(), ATTRIBUTE_VALUE_2);
    }


    public void testProcessRole()
    {
        // same as testProccessGroup (except GroupType = Role)
        GroupTemplate groupTemplate = ObjectTranslator.processRole(role1);
        assertEquals(role1.getName(), groupTemplate.getName());
        assertEquals(role1.getDescription(), groupTemplate.getDescription());
        assertEquals(role1.isActive(), groupTemplate.isActive());
    }

    public void testProcessRoleSOAP()
    {
        GroupTemplate legacyRole = new GroupTemplate("LegacyRole", new Long(1), GroupType.LEGACY_ROLE);
        legacyRole.setActive(true);
        legacyRole.setDescription("This is a Pseudo Group.");
        SOAPRole soapRole = ObjectTranslator.processRole(legacyRole, Arrays.asList(MULTIPLE_MEMBER_LIST));
        assertEquals(legacyRole.getName(), soapRole.getName());
        assertEquals(legacyRole.getDescription(), soapRole.getDescription());
        assertEquals(legacyRole.isActive(), soapRole.isActive());
        assertContainsElements(soapRole.getMembers(), MULTIPLE_MEMBER_LIST);

    }

    public void testBuildUserAttributeMap()
    {
        Map<String, Set<String>> attributes = ObjectTranslator.buildUserAttributeMap(soapPrincipalWithAttributes);

        assertEquals(2, attributes.size());
        assertTrue(attributes.containsKey(ATTRIBUTE_NAME_1));
        assertEquals(1, attributes.get(ATTRIBUTE_NAME_1).size());
        assertEquals(ATTRIBUTE_VALUE_1, attributes.get(ATTRIBUTE_NAME_1).iterator().next());

        assertTrue(attributes.containsKey(ATTRIBUTE_NAME_2));
        assertEquals(3, attributes.get(ATTRIBUTE_NAME_2).size());
        assertTrue(attributes.get(ATTRIBUTE_NAME_2).containsAll(Arrays.asList(ATTRIBUTE_VALUE_2)));
    }

    private static void assertContainsElements(SOAPEntity[] soapEntities, String... names)
    {
        assertEquals(names.length, soapEntities.length);

        List<String> entityNames = new ArrayList<String>();
        for (SOAPEntity entity : soapEntities)
        {
            entityNames.add(entity.getName());
        }

        for (String name : names)
        {
            assertTrue(entityNames.contains(name));
        }
    }

    private static void assertContainsElements(String[] attributes, String... names)
    {
        assertEquals(names.length, attributes.length);

        List<String> attributeValues = Arrays.asList(attributes);

        for (String name : names)
        {
            assertTrue(attributeValues.contains(name));
        }
    }
}
