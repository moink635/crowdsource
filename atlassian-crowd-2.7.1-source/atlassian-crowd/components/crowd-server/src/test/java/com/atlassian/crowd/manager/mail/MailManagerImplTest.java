package com.atlassian.crowd.manager.mail;

import javax.mail.internet.InternetAddress;
import javax.naming.InitialContext;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.mail.SMTPServer;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MailManagerImplTest
{
    @InjectMocks
    private MailManagerImpl mailManager;

    @Mock
    private PropertyManager propertyManager;

    @Mock
    private InitialContext initialContext;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private SMTPServer smtpServer;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldFailToSendEmailWhenNotConfigured() throws Exception
    {
        PropertyManagerException exception = new PropertyManagerException("mail server not configured");
        when(propertyManager.getSMTPServer()).thenThrow(exception);

        this.expectedException.expect(MailSendException.class);
        this.expectedException.expectCause(is(exception));

        mailManager.sendEmail(new InternetAddress("test@test"), "subject", "body");
    }

    @Test
    public void shouldBeConfiguredInHostMode() throws Exception
    {
        when(propertyManager.getSMTPServer()).thenReturn(smtpServer);

        when(smtpServer.isJndiMailActive()).thenReturn(false);
        when(smtpServer.getHost()).thenReturn("test-host");

        assertTrue(mailManager.isConfigured());
    }

    @Test
    public void shouldBeConfiguredInJNDIMode() throws Exception
    {
        when(propertyManager.getSMTPServer()).thenReturn(smtpServer);

        when(smtpServer.isJndiMailActive()).thenReturn(true);
        when(smtpServer.getJndiLocation()).thenReturn("jndi-location");

        assertTrue(mailManager.isConfigured());
    }

    @Test
    public void shouldNotBeConfiguredInHostModeIfHostIsEmpty() throws Exception
    {
        when(propertyManager.getSMTPServer()).thenReturn(smtpServer);

        when(smtpServer.isJndiMailActive()).thenReturn(false);
        when(smtpServer.getHost()).thenReturn("");

        assertFalse(mailManager.isConfigured());
    }

    @Test
    public void shouldNotBeConfiguredInJNDIModeIfJNDILocationIsEmpty() throws Exception
    {
        when(propertyManager.getSMTPServer()).thenReturn(smtpServer);

        when(smtpServer.isJndiMailActive()).thenReturn(true);
        when(smtpServer.getJndiLocation()).thenReturn("");

        assertFalse(mailManager.isConfigured());
    }

    @Test
    public void shouldNotBeConfiguredIfPropertiesAreNotFound() throws Exception
    {
        when(propertyManager.getSMTPServer()).thenThrow(new PropertyManagerException());

        assertFalse(mailManager.isConfigured());
    }
}
