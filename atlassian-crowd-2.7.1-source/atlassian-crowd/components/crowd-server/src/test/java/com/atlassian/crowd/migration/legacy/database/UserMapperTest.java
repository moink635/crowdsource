package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.database.sql.MySQLLegacyTableQueries;
import com.atlassian.crowd.migration.legacy.database.sql.PostgresLegacyTableQueries;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;

import com.google.common.collect.ImmutableSet;

import java.util.List;

public class UserMapperTest extends BaseDatabaseTest
{
    private static final String ADMIN_USER_NAME = "admin";
    private static final String ADMIN_FIRST_NAME = "Super";
    private static final String ADMIN_LAST_NAME = "User";
    private static final String ADMIN_EMAIL = "admin@example.com";
    private static final boolean ADMIN_ACTIVE = true;
    private static final PasswordCredential ADMIN_CREDENTIAL = new PasswordCredential("x61Ey612Kl2gpFL56FT9weDnpSo4AV8j8+qx2AuTHdRyY036xxzTTrw10Wq3+4qQyB+XURPWx1ONxp3Y3pB37A==", true);
    private static final String ADMIN_CREATED_DATE = "2010-02-03 15:06:03.000000000";// dates from crowd15db.script
    private static final String ADMIN_UPDATED_DATE = "2010-02-03 15:16:29.000000000";


    private static final String TEST_USER_NAME = "test1";
    private static final String TEST_FIRST_NAME = "Test";
    private static final String TEST_LAST_NAME = "User1Dir2";
    private static final PasswordCredential TEST_CREDENTIAL = new PasswordCredential("7iaw3Ur350mqGo7jwQrpkj9hiYB3Lkc/iBml1JQODbJ6wYX4oOHV+E+IvIh/1nsUNzLDBMxfqa2Ob1f1ACio/w==", true);

    private UserMapper userMapper;

    @Override
    protected void onSetUp() throws Exception
    {
        super.onSetUp();
        userMapper = new UserMapper(sessionFactory, batchProcessor, jdbcTemplate, userDAO, directoryDAO);
    }

        // Note: Testing uses HSQLDB and not MySQL or Postgres.
    // Though still running both scripts to catch any typos, possible errors since HSQLDB can handle them.
    // This will not be able to catch MySQL or Postgres specific database quirks.
    public void testMySQLScripts()
    {
        userMapper.setLegacyTableQueries(new MySQLLegacyTableQueries());
        _testMigratedDataInDatabase();
    }

    public void testPostgresScripts()
    {
        userMapper.setLegacyTableQueries(new PostgresLegacyTableQueries());
        _testMigratedDataInDatabase();
    }

    public void _testMigratedDataInDatabase()
    {
        try
        {
            List<UserTemplateWithCredentialAndAttributes> users = userMapper.importUsersFromDatabase(importDataHolder.getOldToNewDirectoryIds());
            assertEquals(4, users.size());

            // Get the first user (admin)
            UserTemplateWithCredentialAndAttributes user = getUser(users, "admin", 1L);
            assertNotNull(user);
            assertEquals(ADMIN_USER_NAME, user.getName());
            assertEquals(ADMIN_FIRST_NAME, user.getFirstName());
            assertEquals(ADMIN_LAST_NAME, user.getLastName());
            assertEquals(ADMIN_EMAIL, user.getEmailAddress());
            assertEquals(ADMIN_ACTIVE, user.isActive());
            assertEquals(ADMIN_CREDENTIAL, user.getCredential());
            assertEquals(userMapper.getDateFromDatabase(ADMIN_CREATED_DATE), user.getCreatedDate());
            assertEquals(userMapper.getDateFromDatabase(ADMIN_UPDATED_DATE), user.getUpdatedDate());

            assertEquals(4, user.getAttributes().size());
            assertEquals("0", user.getValue("invalidPasswordAttempts"));
            assertEquals("1265774846524", user.getValue("lastAuthenticated"));
            assertEquals("1265170401218", user.getValue("passwordLastChanged"));
            assertEquals("false", user.getValue("requiresPasswordChange"));

            // Get another user (test1, from directory 2)
            // In particular the attributes
            user = getUser(users, "test1", 2L);
            assertNotNull(user);
            assertEquals(TEST_USER_NAME, user.getName());
            assertEquals(TEST_FIRST_NAME, user.getFirstName());
            assertEquals(TEST_LAST_NAME, user.getLastName());
            assertEquals(TEST_CREDENTIAL, user.getCredential());

            assertEquals(4, user.getAttributes().size());
            assertEquals("1265170186131", user.getValue("passwordLastChanged"));
            assertEquals("false", user.getValue("requiresPasswordChange"));
            assertEquals(ImmutableSet.of("Chocolate"), user.getValues("food"));
            assertEquals(ImmutableSet.of("Black", "grey", "White"), user.getValues("colours"));
        }
        catch (ImportException e)
        {
            fail("Failed to import User data from legacy database");
        }
    }

    private UserTemplateWithCredentialAndAttributes getUser(List<UserTemplateWithCredentialAndAttributes> users, String name, Long directoryId)
    {
        for (UserTemplateWithCredentialAndAttributes user : users)
        {
            if (name.equals(user.getName()) && directoryId.equals(user.getDirectoryId()))
            {
                return user;
            }
        }
        // No user with name/directoryId combination found.
        return null;
    }
}
