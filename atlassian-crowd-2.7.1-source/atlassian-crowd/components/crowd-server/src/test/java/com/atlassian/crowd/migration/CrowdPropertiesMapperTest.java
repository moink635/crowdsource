package com.atlassian.crowd.migration;

import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl;
import com.atlassian.crowd.service.client.ClientResourceLocator;
import com.atlassian.crowd.util.PropertyUtils;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import junit.framework.TestCase;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.HashMap;
import java.util.Properties;

import static org.mockito.Mockito.mock;

public class CrowdPropertiesMapperTest extends TestCase
{
    private CrowdPropertiesMapper propertiesMapper = null;
    private Element root;
    private static final String TEST_KEY = "test.key";
    private static final String TEST_VALUE = "test.value";
    private static final String CROWD_TEST_PROPERTIES = "crowd.test.properties";
    protected ClientProperties clientProperties;
    private ClientResourceLocator crowdResourceLocator;
    private PropertyUtils propertyUtils;

    public void setUp() throws Exception
    {
        super.setUp();
        
        // These xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        BatchProcessor batchProcessor = mock(BatchProcessor.class);
        propertyUtils = new PropertyUtils();
        crowdResourceLocator = new ClientResourceLocator(CROWD_TEST_PROPERTIES);
        clientProperties = SoapClientPropertiesImpl.newInstanceFromResourceLocator(crowdResourceLocator);

        propertiesMapper = new CrowdPropertiesMapper(sessionFactory, batchProcessor, clientProperties, crowdResourceLocator, propertyUtils);

        // Create a dummy XML document to import
        Document document = DocumentHelper.createDocument();
        root = document.addElement(XmlMigrationManagerImpl.XML_ROOT);
        Element crowdPropertiesRoot = DocumentHelper.createElement(CrowdPropertiesMapper.PROPERTIES_XML_ROOT);
        Element propertyElement = crowdPropertiesRoot.addElement(CrowdPropertiesMapper.PROPERTIES_XML_ROOT);
        propertyElement.addElement(CrowdPropertiesMapper.PROPERTY_XML_NAME).addText(TEST_KEY);
        propertyElement.addElement(CrowdPropertiesMapper.PROPERTY_XML_VALUE).addText(TEST_VALUE);
        root.add(crowdPropertiesRoot);
    }

    public void tearDown() throws Exception
    {
        // Remove the test property from crowd.properties
        propertyUtils.removeProperty(crowdResourceLocator.getResourceLocation(), TEST_KEY);

        propertiesMapper = null;
        root = null;

        super.tearDown();
    }

    public void testExportCrowdProperties() throws ExportException
    {
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement(XmlMigrationManagerImpl.XML_ROOT);

        Element propertiesElement = propertiesMapper.exportXml(new HashMap());

        // Add the result to the root so we have the right Document structure
        root.add(propertiesElement);

        assertNotNull(propertiesElement);
        assertTrue("Contains content", propertiesElement.hasContent());

        // assert that the properties Element has all known crowd.properties
        assertEquals("We should have 8 properties in crowd.properties", 8, propertiesElement.nodeCount());
    }

    public void testImportCrowdProperties() throws Exception
    {
        propertiesMapper.importXml(root);

        // Assert that the test crowd.properties had the test element added
        Properties properties = crowdResourceLocator.getProperties();
        String value = properties.getProperty(TEST_KEY);
        
        assertEquals(TEST_VALUE, value);
    }
}
