package com.atlassian.crowd.manager.backup;

import com.atlassian.crowd.file.FileConfigurationExporter;
import com.atlassian.crowd.migration.XmlMigrationManager;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since v2.7
 */
@RunWith(MockitoJUnitRunner.class)
public class DefaultBackupManagerTest
{
    private final SimpleDateFormat TIMESTAMP_FORMATER = new SimpleDateFormat(BackupFileConstants.TIMESTAMP_FORMAT);
    private DefaultBackupManager service;

    @Mock private XmlMigrationManager xmlMigrationManager;
    @Mock private FileConfigurationExporter fileConfigurationExporter;
    @Mock private BackupFileStore backupFileStore;

    @Before
    public void setup() throws Exception
    {
        service = new DefaultBackupManager(xmlMigrationManager, fileConfigurationExporter, backupFileStore);
    }

    @Test
    public void testBackupWithResetDomain() throws Exception
    {
        final File backupDir = new File("/backupdir");
        when(backupFileStore.getBackupDirectory()).thenReturn(backupDir);
        when(xmlMigrationManager.exportXml(new File(backupDir, "somefile").getPath(), ImmutableMap.of("OPTION_RESET_DOMAIN", true))).thenReturn(3L);

        final long time = service.backup("somefile", true);
        assertThat(time, is(3L));
        verify(fileConfigurationExporter).exportDirectories(new File(backupDir, "directories.properties").getPath());
    }

    @Test
    public void testBackupWithoutResetDomain() throws Exception
    {
        final File backupDir = new File("/backupdir");
        when(backupFileStore.getBackupDirectory()).thenReturn(backupDir);
        when(xmlMigrationManager.exportXml(new File(backupDir, "somefile").getPath(), ImmutableMap.of("OPTION_RESET_DOMAIN", false))).thenReturn(3L);

        final long time = service.backup("somefile", false);
        assertThat(time, is(3L));
        verify(fileConfigurationExporter).exportDirectories(new File(backupDir, "directories.properties").getPath());
    }

    @Test
    public void testGetAutomatedBackupSummary() throws Exception
    {
        final DateTime dateTime = new DateTime();
        final Date oldest = dateTime.toDate();
        final Date earliest = dateTime.plusDays(36).toDate();

        final List<File> backupFiles = backupFiles(dateTime, 37);

        when(backupFileStore.getBackupFiles()).thenReturn(backupFiles);
        when(backupFileStore.extractTimestamp(any(File.class))).thenReturn(oldest, earliest);

        final BackupSummary summary = service.getAutomatedBackupSummary();
        assertThat(summary, is(notNullValue()));
        assertThat(summary.getNumBackups(), is(37));
        assertThat(summary.getEarliestDate(), is(oldest));
        assertThat(summary.getLatestDate(), is(earliest));
    }

    private List<File> backupFiles(DateTime startDate, int numFiles)
    {
        final ImmutableList.Builder<File> builder = ImmutableList.builder();
        for (int i = 0; i < numFiles; i++)
        {
            builder.add(new File("atlassian-crowd-" + TIMESTAMP_FORMATER.format(startDate.plusDays(i).toDate()) + "-automated-backup-2.7.0.xml"));
        }

        return builder.build();
    }
}
