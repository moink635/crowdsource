package com.atlassian.crowd.manager.login;

import com.atlassian.crowd.dao.token.*;
import com.atlassian.crowd.embedded.api.*;
import com.atlassian.crowd.event.login.*;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.application.*;
import com.atlassian.crowd.manager.directory.*;
import com.atlassian.crowd.manager.permission.*;
import com.atlassian.crowd.model.application.*;
import com.atlassian.crowd.model.token.*;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.query.entity.*;
import com.atlassian.crowd.service.client.*;
import com.atlassian.event.api.*;
import com.atlassian.security.random.*;
import org.joda.time.*;
import org.junit.*;
import org.mockito.*;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class ForgottenLoginManagerImplTest
{
    private static final String TEST_USERNAME = "username";
    private static final String TEST_EMAIL = "test@example.com";
    private static final String TEST_TOKEN = "abc123";
    private static final long TEST_DIRECTORY_ID = 1L;
    private static final List<String> TEST_USERNAMES;

    private ApplicationService applicationService;
    private DirectoryManager directoryManager;
    private PermissionManager permissionManager;
    private ResetPasswordTokenDao resetPasswordTokenDao;
    private SecureTokenGenerator tokenGenerator;
    private ClientProperties clientProperties;
    private EventPublisher eventPublisher;

    private ForgottenLoginManager forgottenLoginManager;

    static
    {
        TEST_USERNAMES = Arrays.asList("user1", "user2");
    }

    @Before
    public void setUp()
    {
        applicationService = mock(ApplicationService.class);
        directoryManager = mock(DirectoryManager.class);
        permissionManager = mock(PermissionManager.class);
        resetPasswordTokenDao = mock(ResetPasswordTokenDao.class);
        tokenGenerator = mock(SecureTokenGenerator.class);
        clientProperties = mock(ClientProperties.class);
        eventPublisher = mock(EventPublisher.class);

        forgottenLoginManager = new ForgottenLoginManagerImpl(applicationService, directoryManager, permissionManager, resetPasswordTokenDao, tokenGenerator, clientProperties, eventPublisher);
    }

    @After
    public void tearDown()
    {
        applicationService = null;
        directoryManager = null;
        permissionManager = null;
        resetPasswordTokenDao = null;
        tokenGenerator = null;
        clientProperties = null;
        eventPublisher = null;

        forgottenLoginManager = null;
    }

    /**
     * Tests that {@link ForgottenLoginManager#sendResetLink(com.atlassian.crowd.model.application.Application, String)}
     * creates a token and sends the reset link to the user.
     */
    @Test
    public void testSendResetLink() throws Exception
    {
        Application application = mock(Application.class);
        User user = mock(User.class);
        when(user.getName()).thenReturn(TEST_USERNAME);
        when(user.getEmailAddress()).thenReturn(TEST_EMAIL);
        when(tokenGenerator.generateToken()).thenReturn(TEST_TOKEN);

        when(applicationService.findUserByName(application, TEST_USERNAME)).thenReturn(user);
        when(permissionManager.hasPermission(any(Application.class), any(Directory.class), eq(OperationType.UPDATE_USER))).thenReturn(true);
        final Directory directory = mock(Directory.class);
        when(directoryManager.findDirectoryById(anyLong())).thenReturn(directory);

        forgottenLoginManager.sendResetLink(application, TEST_USERNAME);
        verify(resetPasswordTokenDao).addToken(any(ResetPasswordToken.class));
        verify(eventPublisher).publish(any());
    }

    /**
     * Tests that {@link ForgottenLoginManager#isValidResetToken(long, String, String)} returns true for a valid token.
     */
    @Test
    public void testisValidResetToken() throws Exception
    {
        ResetPasswordToken resetPasswordToken = mock(ResetPasswordToken.class);
        when(resetPasswordTokenDao.findTokenByUsername(TEST_USERNAME)).thenReturn(resetPasswordToken);
        when(resetPasswordToken.getToken()).thenReturn(TEST_TOKEN);
        when(resetPasswordToken.getExpiryDate()).thenReturn(new LocalDateTime().plusHours(24).toDateTime().getMillis());
        when(resetPasswordToken.getDirectoryId()).thenReturn(TEST_DIRECTORY_ID);

        boolean valid = forgottenLoginManager.isValidResetToken(TEST_DIRECTORY_ID, TEST_USERNAME, TEST_TOKEN);
        assertTrue(valid);
        verify(resetPasswordTokenDao).findTokenByUsername(TEST_USERNAME);
    }

    /**
     * Tests that {@link ForgottenLoginManager#isValidResetToken(long, String, String)}
     * returns false when: the reset password tokens do not match.
     */
    @Test
    public void testIsValidResetToken_NotMatch() throws Exception
    {
        final String INVALID_TOKEN = "invalid";

        ResetPasswordToken resetPasswordToken = mock(ResetPasswordToken.class);
        when(resetPasswordTokenDao.findTokenByUsername(TEST_USERNAME)).thenReturn(resetPasswordToken);
        when(resetPasswordToken.getToken()).thenReturn(TEST_TOKEN);
        when(resetPasswordToken.getDirectoryId()).thenReturn(TEST_DIRECTORY_ID);
        when(resetPasswordToken.getExpiryDate()).thenReturn(new LocalDateTime().plusHours(24).toDateTime().getMillis());

        // tokens do not match
        boolean valid = forgottenLoginManager.isValidResetToken(TEST_DIRECTORY_ID, TEST_USERNAME, INVALID_TOKEN);
        assertFalse("Reset tokens do not match. isValidResetToken should have returned false.", valid);
    }

    /**
     * Tests that {@link ForgottenLoginManager#isValidResetToken(long, String, String)}
     * returns false when: the token has expired.
     */
    @Test
    public void testIsValidResetToken_ExpiredToken() throws Exception
    {
        ResetPasswordToken resetPasswordToken = mock(ResetPasswordToken.class);
        when(resetPasswordTokenDao.findTokenByUsername(TEST_USERNAME)).thenReturn(resetPasswordToken);
        when(resetPasswordToken.getToken()).thenReturn(TEST_TOKEN);
        when(resetPasswordToken.getDirectoryId()).thenReturn(TEST_DIRECTORY_ID);
        when(resetPasswordToken.getExpiryDate()).thenReturn(new LocalDateTime().minusHours(1).toDateTime().getMillis());

        // token expired
        boolean valid = forgottenLoginManager.isValidResetToken(TEST_DIRECTORY_ID, TEST_USERNAME, TEST_TOKEN);
        assertFalse("Reset token has expired. isValidResetToken should have returned false.", valid);
    }

    /**
     * Tests that {@link ForgottenLoginManager#isValidResetToken(long, String, String)}
     * returns false when: there is no token found for the username.
     */
    @Test
    public void testIsValidResetToken_NoToken() throws Exception
    {
        ResetPasswordToken resetPasswordToken = mock(ResetPasswordToken.class);
        when(resetPasswordTokenDao.findTokenByUsername(anyString())).thenThrow(new ObjectNotFoundException());
        when(resetPasswordToken.getToken()).thenReturn(TEST_TOKEN);
        when(resetPasswordToken.getDirectoryId()).thenReturn(TEST_DIRECTORY_ID);
        when(resetPasswordToken.getExpiryDate()).thenReturn(new LocalDateTime().plusHours(24).toDateTime().getMillis());

        // no token for user
        boolean valid = forgottenLoginManager.isValidResetToken(TEST_DIRECTORY_ID, TEST_USERNAME, TEST_TOKEN);
        assertFalse("No reset token found for username. isValidResetToken should have returned false.", valid);
    }

    /**
     * Tests that {@link ForgottenLoginManager#isValidResetToken(long, String, String)}
     * returns false when: the directories are different.
     */
    @Test
    public void testIsValidResetToken_DirectoriesDifferent() throws Exception
    {
        final long INVALID_DIRECTORY = -1L;
        ResetPasswordToken resetPasswordToken = mock(ResetPasswordToken.class);
        when(resetPasswordTokenDao.findTokenByUsername(TEST_USERNAME)).thenReturn(resetPasswordToken);
        when(resetPasswordToken.getToken()).thenReturn(TEST_TOKEN);
        when(resetPasswordToken.getExpiryDate()).thenReturn(new LocalDateTime().plusHours(24).toDateTime().getMillis());
        when(resetPasswordToken.getDirectoryId()).thenReturn(INVALID_DIRECTORY);

        // no token for user
        boolean valid = forgottenLoginManager.isValidResetToken(TEST_DIRECTORY_ID, TEST_USERNAME, TEST_TOKEN);
        assertFalse("Directories are different. isValidResetToken should have returned false.", valid);
    }

    /**
     * Tests that {@link ForgottenLoginManager#sendUsernames(com.atlassian.crowd.model.application.Application, String)}
     * sends a list of usernames associated with an email.
     */
    @Test
    public void testSendUsernames() throws InvalidEmailAddressException
    {
        Application application = mock(Application.class);
        List<User> users = new ArrayList<User>();
        for (String username : TEST_USERNAMES)
        {
            User user = mock(User.class);
            when(user.getName()).thenReturn(username);
            users.add(user);
        }
        when(applicationService.searchUsers(eq(application), Matchers.<EntityQuery<User>>any())).thenReturn(users);

        forgottenLoginManager.sendUsernames(application, TEST_EMAIL);
        verify(eventPublisher).publish(new RequestUsernamesEvent(users.get(0), TEST_USERNAMES));
    }

    /**
     * Tests that {@link ForgottenLoginManager#sendUsernames(com.atlassian.crowd.model.application.Application, String)}
     * does not send an email if there are no users associated with an email address.
     */
    @Test
    public void testSendUsernames_NoUsernames() throws InvalidEmailAddressException
    {
        Application application = mock(Application.class);
        List<User> users = Collections.emptyList();
        when(applicationService.searchUsers(eq(application), Matchers.<EntityQuery<User>>any())).thenReturn(users);

        forgottenLoginManager.sendUsernames(application, TEST_EMAIL);
        verify(eventPublisher, never()).publish(any());
    }
}
