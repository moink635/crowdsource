package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.migration.legacy.database.sql.MySQLLegacyTableQueries;
import com.atlassian.crowd.migration.legacy.database.sql.PostgresLegacyTableQueries;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroupWithAttributes;
import com.atlassian.crowd.model.membership.InternalMembership;
import com.atlassian.crowd.model.membership.MembershipType;

import java.util.List;
import java.util.Set;

public class RoleMapperTest extends BaseDatabaseTest
{
    private RoleMapper roleMapper;

    @Override
    protected void onSetUp() throws Exception
    {
        super.onSetUp();
        roleMapper = new RoleMapper(sessionFactory, batchProcessor, jdbcTemplate, groupDAO, membershipDAO, directoryDAO);
    }

    // Note: Testing uses HSQLDB and not MySQL or Postgres.
    // Though still running both scripts to catch any typos, possible errors since HSQLDB can handle them.
    // This will not be able to catch MySQL or Postgres specific database quirks.
    public void testMySQLScripts()
    {
        roleMapper.setLegacyTableQueries(new MySQLLegacyTableQueries());
        _testMigrateRoles();
    }

    public void testPostgresScripts()
    {
        roleMapper.setLegacyTableQueries(new PostgresLegacyTableQueries());
        _testMigrateRoles();
    }

    public void testMySQLScriptsMemberships()
    {
        roleMapper.setLegacyTableQueries(new MySQLLegacyTableQueries());
        _testMigratedMembershipDataInDatabase();
    }

    public void testPostgresScriptsMemberships()
    {
        roleMapper.setLegacyTableQueries(new PostgresLegacyTableQueries());
        _testMigratedMembershipDataInDatabase();
    }

    public void _testMigrateRoles()
    {
        List<InternalGroupWithAttributes> roles = roleMapper.importRolesFromDatabase(importDataHolder.getOldToNewDirectoryIds());
        assertEquals(1, roles.size());

        // Only 1 role
        InternalGroupWithAttributes group = getRole(roles, "testing-role", 1L);
        assertNotNull(group);
        assertTrue(group.isActive());
        assertEquals(GroupType.LEGACY_ROLE, group.getType());
        assertEquals("This is a role", group.getDescription());
        assertEquals(0, group.getKeys().size());  // we don't have any attributes in our test
    }

    public void _testMigratedMembershipDataInDatabase()
    {
        Set<InternalMembership> memberships = roleMapper.importMembershipsFromDatabase(importDataHolder, groupImportResults);
        assertEquals(1, memberships.size()); // There is only 1 user/Role membership

        // Ids obatined from actual XML restore
        InternalMembership membership = new InternalMembership(131078L, 98308L, 32772L, MembershipType.GROUP_USER, GroupType.LEGACY_ROLE, "testing-role", "testuser", directory_one);

        assertTrue(memberships.contains(membership));
    }

    private InternalGroupWithAttributes getRole(List<InternalGroupWithAttributes> roles, String name, Long directoryId)
    {
        for (InternalGroupWithAttributes group : roles)
        {
            if (name.equals(group.getName()) && directoryId.equals(group.getDirectoryId()))
            {
                return group;
            }
        }
        // No user with name/directoryId combination found.
        return null;
    }
}
