package com.atlassian.crowd.applinks;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.plugin.PluginAccessor;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * @since v2.7
 */
@RunWith(MockitoJUnitRunner.class)
public class CrowdHostApplicationTest
{
    @InjectMocks
    private CrowdHostApplication service;
    @Mock
    private PropertyManager propertyManager;
    @Mock
    private PluginAccessor pluginAccessor;
    @Mock
    private TypeAccessor typeAccessor;
    @Mock
    private ClientProperties clientProperties;
    @Mock
    private CrowdBootstrapManager crowdBootstrapManager;

    @Test
    public void getNameShouldReturnDeploymentTitleIfDefined() throws Exception
    {
        when(propertyManager.getDeploymentTitle()).thenReturn("My Crowd Server");
        final String name = service.getName();

        assertThat(name, Matchers.is("My Crowd Server"));
    }

    @Test
    public void getNameShouldReturnDefaultValueIfDeploymentTitleNotDefined() throws Exception
    {
        when(propertyManager.getDeploymentTitle()).thenThrow(new PropertyManagerException());
        final String name = service.getName();

        assertThat(name, Matchers.is("Crowd"));
    }

    @Test
    public void getIdShouldDependOnServerId() throws Exception
    {
        when(crowdBootstrapManager.getServerID()).thenReturn("SomeId", "SomeId", "AnotherId");

        final ApplicationId id1 = service.getId();
        final ApplicationId id2 = service.getId();
        final ApplicationId id3 = service.getId();

        // Same SID => Same ApplicationId
        assertThat(id1, equalTo(id2));
        // Different SID => Different ApplicationId
        assertThat(id1, not(equalTo(id3)));
    }
}
