package com.atlassian.crowd.manager.token.reaper;

import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;

import org.junit.Before;
import org.junit.Test;
import org.quartz.JobExecutionException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;


public class TokenReaperTest
{
    private TokenReaper tokenReaper;
    private TokenAuthenticationManager tokenAuthenticationManager;

    @Before
    public void setUp() throws Exception
    {
        tokenReaper = new TokenReaper();
        tokenAuthenticationManager = mock(TokenAuthenticationManager.class);
        tokenReaper.setTokenAuthenticationManager(tokenAuthenticationManager);
    }

    @Test
    public void testTokenReaperRuns() throws JobExecutionException
    {
        tokenReaper.executeInternal(null);

        verify(tokenAuthenticationManager, times(1)).removeExpiredTokens();
    }

    @Test(expected = NullPointerException.class)
    public void nullAuthenticationManagerCausesException() throws Exception
    {
        tokenReaper.setTokenAuthenticationManager(null);
        tokenReaper.executeInternal(null);
    }
}
