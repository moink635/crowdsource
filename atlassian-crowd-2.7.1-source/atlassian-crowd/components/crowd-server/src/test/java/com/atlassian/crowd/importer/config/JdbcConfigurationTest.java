package com.atlassian.crowd.importer.config;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.atlassian.crowd.importer.exceptions.ImporterConfigurationException;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class JdbcConfigurationTest
{
    private JdbcConfiguration configuration = null;

    @Before
    public void setUp() throws Exception
    {
        Class.forName("org.hsqldb.jdbcDriver");

        configuration = new JdbcConfiguration(1L, "JIRA", true, "jdbc:hsqldb:mem:JdbcConfigurationTest",
                                              "org.hsqldb.jdbcDriver", "sa", "");
        configuration.setOverwriteTarget(false);

        try
        {
            DriverManager.getConnection("jdbc:hsqldb:mem:JdbcConfigurationTest", "sa", "");
        }
        catch (SQLException e)
        {
            fail("Failed to load DB: " + e.getMessage());
        }
    }

    @Test
    public void testIsValid()
    {
        try
        {
            configuration.isValid();
        }
        catch (ImporterConfigurationException e)
        {
            fail("We should have been able to load the DB");
        }
    }

    @Test(expected = ImporterConfigurationException.class)
    public void testIsNotValid() throws ImporterConfigurationException
    {
        configuration.setDatabaseURL("");
        configuration.isValid();
    }

    @Test
    public void testIsNotValidWithSQLException()
    {
        configuration.setDatabaseURL("jdbc:hldb:BadURL");
        try
        {
            configuration.isValid();
            fail("We should have an error since there is no DB URL");
        }
        catch (ImporterConfigurationException e)
        {
            assertTrue(e.getCause() instanceof SQLException);
        }
    }


    @Test(expected = ImporterConfigurationException.class)
    public void testIsNotValidWithDriverException() throws ImporterConfigurationException
    {
        configuration.setDatabaseDriver("com.atlassian.jdbc.JdbcDriver");
        configuration.isValid();
    }

    @Test
    public void testFullConstructor()
    {
        JdbcConfiguration localConfiguration = new JdbcConfiguration(1L, "JIRA", Boolean.TRUE, "jdbc:hsqldb:mem:JdbcConfigurationTest","org.hsqldb.jdbcDriver", "sa", "");

        assertEquals(new Long(1), localConfiguration.getDirectoryID());
        assertEquals("JIRA", localConfiguration.getApplication());
        assertEquals(Boolean.TRUE, localConfiguration.isImportPasswords());
        assertEquals(Boolean.FALSE, localConfiguration.isOverwriteTarget());
        assertEquals("jdbc:hsqldb:mem:JdbcConfigurationTest", localConfiguration.getDatabaseURL());
        assertEquals("org.hsqldb.jdbcDriver", localConfiguration.getDatabaseDriver());
        assertEquals("sa", localConfiguration.getUsername());
        assertEquals("", localConfiguration.getPassword());

        assertEquals(configuration, localConfiguration);
    }
}
