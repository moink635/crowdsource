package com.atlassian.crowd.migration;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.migration.legacy.XmlMapper;
import com.atlassian.crowd.model.application.ApplicationAttributeConstants;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.apache.commons.lang3.time.DateUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.text.SimpleDateFormat;
import java.util.Arrays;

import static com.atlassian.crowd.migration.ApplicationMapper.APPLICATION_XML_ROOT;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ApplicationMapperTest extends BaseMapperTest
{
    private ApplicationMapper mapper;
    private Element applicationRoot;
    private DirectoryDAOHibernate directoryDAOHibernate;

    protected void setUp() throws Exception
    {
        super.setUp();

        directoryDAOHibernate = mock(DirectoryDAOHibernate.class);

        // These xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        BatchProcessor batchProcessor = mock(BatchProcessor.class);
        ApplicationManager applicationManager = mock(ApplicationManager.class);
        mapper = new ApplicationMapper(sessionFactory, batchProcessor, applicationManager, directoryDAOHibernate);

        when(directoryDAOHibernate.loadReference(1L)).thenReturn(directoryWithIdAndName(1L, "Directory One"));

        applicationRoot = (Element) document.getRootElement().selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + APPLICATION_XML_ROOT);

    }

    protected void tearDown() throws Exception
    {
        super.tearDown();
    }

    public void testImportAndExportFromXML() throws Exception
    {
        ApplicationImpl application = mapper.getApplicationFromXml((Element) this.applicationRoot.elementIterator().next());

        assertNotNull(application);

        assertEquals("crowd", application.getName());
        assertEquals(new Long(2), application.getId());
        assertTrue(application.isActive());
        assertEquals(ApplicationType.CROWD, application.getType());
        assertEquals("Crowd Console", application.getDescription());

        SimpleDateFormat sdf = new SimpleDateFormat(XmlMapper.XML_DATE_FORMAT);

        assertTrue(DateUtils.isSameInstant(sdf.parse("Fri Apr 03 16:37:07 +1100 2009"), application.getCreatedDate()));
        assertTrue(DateUtils.isSameInstant(sdf.parse("Fri Apr 03 16:37:29 +1100 2009"), application.getUpdatedDate()));

        assertEquals(4, application.getRemoteAddresses().size());
        assertTrue(application.getRemoteAddresses().containsAll(Arrays.<RemoteAddress>asList(new RemoteAddress("localhost"), new RemoteAddress("127.0.0.1"), new RemoteAddress("192.168.3.165"), new RemoteAddress("mocha.sydney.atlassian.com"))));

        assertEquals(1, application.getAttributes().entrySet().size());
        assertEquals("true", application.getAttributes().get(ApplicationAttributeConstants.ATTRIBUTE_KEY_ATLASSIAN_SHA1_APPLIED));

        assertEquals(1, application.getDirectoryMappings().size());

        DirectoryMapping mapping = application.getDirectoryMapping(1L);
        assertEquals(6, mapping.getAllowedOperations().size());
        assertTrue(mapping.getAllowedOperations().containsAll(Arrays.asList(OperationType.CREATE_GROUP, OperationType.CREATE_USER, OperationType.UPDATE_GROUP, OperationType.UPDATE_USER, OperationType.DELETE_USER, OperationType.DELETE_GROUP)));

        assertEquals(new Long(1), mapping.getDirectory().getId());

        assertEquals(1, mapping.getAuthorisedGroups().size());
        assertEquals("crowd-administrators", mapping.getAuthorisedGroups().iterator().next().getGroupName());


        // test export
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement(XmlMigrationManagerImpl.XML_ROOT);
        Element applicationRoot = root.addElement(ApplicationMapper.APPLICATION_XML_ROOT);

        mapper.addApplicationToXml(application, applicationRoot);

        String export = exportDocumentToString(document);

        assertEquals(getTestExportXmlString(), export);
    }
}
