package com.atlassian.crowd.plugin;

import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.osgi.container.OsgiPersistentCache;
import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;

/**
 * Tests for {@link CrowdOsgiContainerManager}
 *
 * @since v2.7
 */
@RunWith(MockitoJUnitRunner.class)
public class CrowdOsgiContainerManagerTest
{
    private final AtomicInteger createdServiceTrackers = new AtomicInteger(0);
    private final AtomicInteger closedServiceTrackers = new AtomicInteger(0);

    @Mock private OsgiPersistentCache osgiPersistentCache;
    @Mock private PackageScannerConfiguration packageScannerConfig;
    @Mock private HostComponentProvider hostComponentProvider;
    @Mock private PluginEventManager eventManager;
    @Mock private BundleContext bundleContext;
    private CrowdOsgiContainerManager service;

    @Before
    public void setUp() throws Exception
    {
        service = new CrowdOsgiContainerManager(osgiPersistentCache, packageScannerConfig, hostComponentProvider, eventManager) {
            @Override
            public ServiceTracker getServiceTracker(String interfaceClassName)
            {
                return new MockServiceTracker(new MyDummyService() {});
            }

            @Override
            public boolean isRunning()
            {
                return true;
            }
        };
    }

    @Test
    public void getOSGiComponentInstanceOfTypeShouldReturnNewInstanceFirstTime() throws Exception
    {
        final MyDummyService instance = service.getOsgiComponentOfType(MyDummyService.class);

        assertThat(instance, notNullValue());
        assertThat(createdServiceTrackers.get(), is(1));
        assertThat(closedServiceTrackers.get(), is(0));
    }

    @Test
    public void getOSGiComponentOfTypeShouldReturnCachedInstanceSecondTime() throws Exception
    {
        final MyDummyService firstInstance = service.getOsgiComponentOfType(MyDummyService.class);
        final MyDummyService secondInstance = service.getOsgiComponentOfType(MyDummyService.class);

        // Same service instance returned
        assertThat(firstInstance, sameInstance(secondInstance));
        // The created ServiceTracker should have been reused
        assertThat(createdServiceTrackers.get(), is(1));
        assertThat(closedServiceTrackers.get(), is(0));
    }

    @Test
    public void trackerAreClosedWhenCacheIsInvalidated() throws Exception
    {
        // Put one service tracker in the cache
        service.getOsgiComponentOfType(MyDummyService.class);

        assertThat(closedServiceTrackers.get(), is(0));
        // Stop forces all the entries from the cache to be evicted
        service.stop();
        assertThat(closedServiceTrackers.get(), is(1));
    }

    private class MockServiceTracker extends ServiceTracker {
        private MyDummyService instance;

        public MockServiceTracker(MyDummyService instance)
        {
            super(bundleContext, "", null);
            this.instance = instance;
            createdServiceTrackers.incrementAndGet();
        }

        @Override
        public void close()
        {
            closedServiceTrackers.incrementAndGet();
        }

        @Override
        public Object getService()
        {
            return instance;
        }
    }

    private static interface MyDummyService {}
}
