package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.migration.legacy.database.sql.MySQLLegacyTableQueries;
import com.atlassian.crowd.migration.legacy.database.sql.PostgresLegacyTableQueries;
import com.atlassian.crowd.model.property.Property;

import java.util.ArrayList;
import java.util.List;


public class SALPropertyMapperTest extends BaseDatabaseTest
{
    private SALPropertyMapper salPropertyMapper;

    @Override
    protected void onSetUp() throws Exception
    {
        super.onSetUp();
        salPropertyMapper = new SALPropertyMapper(sessionFactory, batchProcessor, jdbcTemplate);
    }

    // Note: Testing uses HSQLDB and not MySQL or Postgres.
    // Though still running both scripts to catch any typos, possible errors since HSQLDB can handle them.
    // This will not be able to catch MySQL or Postgres specific database quirks.
    public void testMySQLScripts()
    {
        salPropertyMapper.setLegacyTableQueries(new MySQLLegacyTableQueries());
        _testGetSALProperties();
    }

    public void testPostgresScripts()
    {
        salPropertyMapper.setLegacyTableQueries(new PostgresLegacyTableQueries());
        _testGetSALProperties();
    }

    public void _testGetSALProperties()
    {
        List<Property> salProperties = salPropertyMapper.importSALPropertiesFromDatabase();
        assertEquals(3, salProperties.size());

        List<Property> actualProperties = new ArrayList<Property>();
        actualProperties.add(new Property("plugin.SAL", "dummyName", "<list> <map> <entry> <string>password</string> <null/> </entry> <entry> <string>class</string> <java-class>com.atlassian.notifier.NotificationSubscriptionImpl</java-class> </entry> <entry> <string>username</string> <null/> </entry> <entry> <string>url</string> <string>http://localhost/wiki/plugins/servlet/crowdnotify</string> </entry> <entry> <string>authenticationType</string> <com.atlassian.notifier.AuthenticationType>NONE</com.atlassian.notifier.AuthenticationType> </entry> </map> <map> <entry> <string>password</string> <null/> </entry> <entry> <string>class</string> <java-class>com.atlassian.notifier.NotificationSubscriptionImpl</java-class> </entry> <entry> <string>username</string> <null/> </entry> <entry> <string>url</string> <string>http://localhost:3990/plugins/servlet/crowdnotify</string> </entry> <entry> <string>authenticationType</string> <com.atlassian.notifier.AuthenticationType>NONE</com.atlassian.notifier.AuthenticationType> </entry> </map> </list>"));
        actualProperties.add(new Property("plugin.Some SAL Key", "Some SAL Property Name", "A not so interesting value"));
        actualProperties.add(new Property("plugin.null", "this.is.a.property.name", "(something here)"));

        assertTrue(salProperties.containsAll(actualProperties));
    }
}
