package com.atlassian.crowd.importer.mappers.csv;

import com.atlassian.crowd.importer.config.CsvConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;

import junit.framework.TestCase;

import org.apache.commons.collections.OrderedBidiMap;
import org.apache.commons.collections.bidimap.TreeBidiMap;

/**
 * AbstractCsvMapper Tester.
 */
public class TestGroupMapper extends TestCase
{
    private GroupMapper groupMapper;

    protected void setUp() throws Exception
    {
        super.setUp();

        OrderedBidiMap configuration = new TreeBidiMap();

        // First value in the parsed csv file
        configuration.put("group.0", CsvConfiguration.GROUP_NAME);

        groupMapper = new GroupMapper(new Long(1), configuration);
    }

    public void testMapRow() throws ImporterException
    {
        GroupTemplate group = groupMapper.mapRow(new String[]{"jira-admin"});

        assertNotNull(group);
        assertEquals("jira-admin", group.getName());
        assertEquals("jira-admin", group.getDescription());
        assertTrue(group.isActive());
    }

    public void testMapRowWithNullData() throws ImporterException
    {
        Group remoteGroup = groupMapper.mapRow(null);

        assertNull(remoteGroup);
    }

    public void tearDown() throws Exception
    {
        groupMapper = null;
        super.tearDown();
    }

}