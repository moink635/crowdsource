package com.atlassian.crowd.importer.mappers.csv;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.importer.config.CsvConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;

import org.apache.commons.collections.OrderedBidiMap;
import org.apache.commons.collections.bidimap.TreeBidiMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * PrincipalMapper Tester.
 */
public class UserMapperTest
{
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private UserMapper userMapper;

    @Before
    public void setUp() throws Exception
    {
        OrderedBidiMap configuration = new TreeBidiMap();

        configuration.put(CsvConfiguration.USER_PREFIX + "0", CsvConfiguration.USER_USERNAME);
        configuration.put(CsvConfiguration.USER_PREFIX + "1", CsvConfiguration.USER_EMAILADDRESS);
        configuration.put(CsvConfiguration.USER_PREFIX + "2", CsvConfiguration.USER_FIRSTNAME);
        configuration.put(CsvConfiguration.USER_PREFIX + "3", CsvConfiguration.USER_LASTNAME);
        configuration.put(CsvConfiguration.USER_PREFIX + "4", CsvConfiguration.USER_PASSWORD);

        userMapper = new UserMapper(1L, configuration, Boolean.TRUE, Boolean.TRUE);
    }

    @Test
    public void testMapRow() throws ImporterException
    {
        UserTemplateWithCredentialAndAttributes user = userMapper.mapRow(new String[]{"jira-admin", "jira@atlassian.com", "Peter", "Smith", "secret"});

        assertNotNull(user);
        assertEquals("jira-admin", user.getName());
        assertNull(user.getDisplayName());
        assertEquals("Peter", user.getFirstName());
        assertEquals("Smith", user.getLastName());
        assertEquals("jira@atlassian.com", user.getEmailAddress());
        assertEquals(new PasswordCredential("secret"), user.getCredential());
        assertTrue(user.isActive());
    }

    @Test
    public void mapRowFailsWhenPasswordColumnIsOmitted() throws ImporterException
    {
        expectedException.expect(ImporterException.class);
        expectedException.expectMessage(startsWith("Missing column for user.password:"));
        userMapper.mapRow(new String[]{"jira-admin", "jira@atlassian.com", "Peter", "Smith"});
    }

    @Test
    public void mapRowSucceedsWhenNoPasswordColumnIsRequired() throws ImporterException
    {
        OrderedBidiMap configuration = new TreeBidiMap();

        configuration.put(CsvConfiguration.USER_PREFIX + "0", CsvConfiguration.USER_USERNAME);
        configuration.put(CsvConfiguration.USER_PREFIX + "1", CsvConfiguration.USER_EMAILADDRESS);
        configuration.put(CsvConfiguration.USER_PREFIX + "2", CsvConfiguration.USER_FIRSTNAME);
        configuration.put(CsvConfiguration.USER_PREFIX + "3", CsvConfiguration.USER_LASTNAME);

        UserMapper userMapper = new UserMapper(1L, configuration, Boolean.TRUE, Boolean.TRUE);

        UserTemplateWithCredentialAndAttributes user = userMapper.mapRow(new String[]{"jira-admin", "jira@atlassian.com", "Peter", "Smith"});

        assertNotNull(user);
        assertEquals(PasswordCredential.NONE, user.getCredential());
    }

    @Test
    public void testMapRowWithNullData() throws ImporterException
    {
        UserTemplateWithCredentialAndAttributes user = userMapper.mapRow(null);

        assertNull(user);
    }

    @Test
    public void testMapRowWithIncompleteData() throws ImporterException
    {
        UserTemplateWithCredentialAndAttributes user = userMapper.mapRow(new String[]{});

        assertNull(user);
    }

    @After
    public void tearDown() throws Exception
    {
        userMapper = null;
    }
}
