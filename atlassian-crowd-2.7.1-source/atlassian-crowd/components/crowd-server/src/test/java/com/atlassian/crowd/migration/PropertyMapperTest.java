package com.atlassian.crowd.migration;

import com.atlassian.crowd.dao.property.PropertyDAO;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import junit.framework.TestCase;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.Arrays;
import java.util.HashMap;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * PropertyMapper Tester.
 */
public class PropertyMapperTest extends TestCase
{
    private PropertyMapper propertyMapper;
    private Property property1;
    private Property property2;
    private PropertyDAO mockPropertyDAO;

    public void setUp() throws Exception
    {
        super.setUp();

        // These xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        BatchProcessor batchProcessor = mock(BatchProcessor.class);
        mockPropertyDAO = mock(PropertyDAO.class);
        propertyMapper = new PropertyMapper(sessionFactory, batchProcessor, mockPropertyDAO);
        property1 = new Property(Property.CROWD_PROPERTY_KEY, Property.DOMAIN, "localhost");
        property2 = new Property(Property.CROWD_PROPERTY_KEY, Property.NOTIFICATION_EMAIL, "justin@atlassian.com");
    }

    public void tearDown() throws Exception
    {
        super.tearDown();
    }

    public void testExportImportOfProperties() throws Exception
    {
        // Now make that findAll returns the setup properties
        when(mockPropertyDAO.findAll()).thenReturn(Arrays.asList(property1, property2));

        Element root = createBaseXMLDocument();

        Element result = propertyMapper.exportXml(new HashMap());

        // Add the result to the root so we have the right Document structure
        root.add(result);

        // Assert that some content was returned from the export method
        assertNotNull(result);
        assertTrue("Contains content", result.hasContent());

        // And finally the execution
        propertyMapper.importXml(root);

        verify(mockPropertyDAO, times(1)).findAll();
        // Expectations for import
        verify(mockPropertyDAO, times(1)).add(property1);
        verify(mockPropertyDAO, times(1)).add(property2);
    }

    private Element createBaseXMLDocument()
    {
        Document document = DocumentHelper.createDocument();
        return document.addElement(XmlMigrationManagerImpl.XML_ROOT);
    }
}
