package com.atlassian.crowd.file;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import com.atlassian.crowd.dao.directory.DirectoryPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.directory.DirectoryManager;

import com.google.common.collect.ImmutableList;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileConfigurationExporterTest
{

    private FileConfigurationExporter fileConfigurationExporter;

    @Mock private DirectoryManager directoryManager;
    @Mock private DirectoryPropertiesMapper directoryPropertiesMapper;
    @Mock private Directory directory;

    private File outputDir;

    @Before
    public void setUp() throws Exception
    {
        fileConfigurationExporter = new FileConfigurationExporter(directoryManager, directoryPropertiesMapper);
    }

    @Before
    public void createOutputDir() throws Exception
    {
        outputDir = new File(FileUtils.getTempDirectory(), FileConfigurationExporterTest.class.getCanonicalName());
        outputDir.mkdirs();
    }

    @After
    public void clearOutputDir() throws IOException
    {
        if (outputDir.exists() && outputDir.isDirectory())
        {
            outputDir.deleteOnExit();
            FileUtils.deleteDirectory(outputDir);
        }
    }

    @Test
    public void testExportDirectories() throws Exception
    {
        Properties properties = new Properties();
        properties.setProperty("name", "value");
        when(directoryManager.findAllDirectories()).thenReturn(ImmutableList.of(directory));
        when(directoryPropertiesMapper.exportProperties(ImmutableList.of(directory))).thenReturn(properties);

        File outputFile = new File(outputDir, "output.properties");

        fileConfigurationExporter.exportDirectories(outputFile.toString());

        Properties actualProperties = new Properties();
        FileReader reader = new FileReader(outputFile);
        try
        {
            actualProperties.load(reader);

            assertEquals("value", actualProperties.getProperty("name"));
        }
        finally
        {
            reader.close();
        }
    }


}
