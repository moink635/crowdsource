package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.migration.legacy.database.sql.MySQLLegacyTableQueries;
import com.atlassian.crowd.migration.legacy.database.sql.PostgresLegacyTableQueries;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroupWithAttributes;
import com.atlassian.crowd.model.membership.InternalMembership;
import com.atlassian.crowd.model.membership.MembershipType;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GroupMapperTest extends BaseDatabaseTest
{
    private GroupMapper groupMapper;

    @Override
    protected void onSetUp() throws Exception
    {
        super.onSetUp();
        groupMapper = new GroupMapper(sessionFactory, batchProcessor, jdbcTemplate, groupDAO, membershipDAO, directoryDAO);
    }

    // Note: Testing uses HSQLDB and not MySQL or Postgres.
    // Though still running both scripts to catch any typos, possible errors since HSQLDB can handle them.
    // This will not be able to catch MySQL or Postgres specific database quirks.
    public void testMySQLScripts()
    {
        groupMapper.setLegacyTableQueries(new MySQLLegacyTableQueries());
        _testMigrateGroups();
    }

    public void testPostgresScripts()
    {
        groupMapper.setLegacyTableQueries(new PostgresLegacyTableQueries());
        _testMigrateGroups();
    }

    public void testMySQLScriptsMemberhips()
    {
        groupMapper.setLegacyTableQueries(new MySQLLegacyTableQueries());
        _testMigratedMembershipDataInDatabase();
    }

    public void testPostgresScriptsMemberships()
    {
        groupMapper.setLegacyTableQueries(new PostgresLegacyTableQueries());
        _testMigratedMembershipDataInDatabase();
    }

    public void _testMigrateGroups()
    {
        List<InternalGroupWithAttributes> groups = groupMapper.importGroupsFromDatabase(importDataHolder.getOldToNewDirectoryIds());
        assertEquals(3, groups.size());

        // Check crowd-administrators
        InternalGroupWithAttributes group = getGroup(groups, "crowd-administrators", 1L);
        assertNotNull(group);
        assertTrue(group.isActive());
        assertEquals(GroupType.GROUP, group.getType());
        assertNull(group.getDescription()); // no description
        assertEquals(0, group.getKeys().size());  // we don't have any attributes in our test

        // Check another group - with description
        group = getGroup(groups, "test-group1", 1L);
        assertNotNull(group);
        assertTrue(group.isActive());
        assertEquals(GroupType.GROUP, group.getType());
        assertEquals("Group One", group.getDescription());
        assertEquals(0, group.getKeys().size());  // we don't have any attributes in our test
        assertNull(group.getValue("nonexistent"));
    }

    public void _testMigratedMembershipDataInDatabase()
    {
        Set<InternalMembership> memberships = groupMapper.importMembershipsFromDatabase(importDataHolder, groupImportResults);
        assertEquals(5, memberships.size()); // There are five User/Group memberships

        // Ids obatined from actual XML restore
        Set<InternalMembership> actualMemberships = new HashSet<InternalMembership>();
        actualMemberships.add(new InternalMembership(131073L, 98307L, 32772L, MembershipType.GROUP_USER, GroupType.GROUP, "Another Group", "testuser", directory_one));
        actualMemberships.add(new InternalMembership(131074L, 98307L, 32769L, MembershipType.GROUP_USER, GroupType.GROUP, "Another Group", "test1", directory_one));
        actualMemberships.add(new InternalMembership(131075L, 98307L, 32770L, MembershipType.GROUP_USER, GroupType.GROUP, "Another Group", "admin", directory_one));
        actualMemberships.add(new InternalMembership(131076L, 98305L, 32770L, MembershipType.GROUP_USER, GroupType.GROUP, "crowd-administrators", "admin", directory_one));
        actualMemberships.add(new InternalMembership(131077L, 98306L, 32772L, MembershipType.GROUP_USER, GroupType.GROUP, "test-group1", "testuser", directory_one));

        assertTrue(memberships.containsAll(actualMemberships));
    }

    private InternalGroupWithAttributes getGroup(List<InternalGroupWithAttributes> groups, String name, Long directoryId)
    {
        for (InternalGroupWithAttributes group : groups)
        {
            if (name.equals(group.getName()) && directoryId.equals(group.getDirectoryId()))
            {
                return group;
            }
        }
        // No user with name/directoryId combination found.
        return null;
    }
}
