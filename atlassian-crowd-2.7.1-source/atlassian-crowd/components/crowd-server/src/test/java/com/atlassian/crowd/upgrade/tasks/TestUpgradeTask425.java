package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.springframework.dao.DataAccessException;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

public class TestUpgradeTask425
{
    private DirectoryDao mockDirectoryDAO;

    private DirectoryImpl directory;
    private DirectoryImpl directory2;
    private UpgradeTask425 task;

    @Before
    public void setUp() throws Exception
    {
        directory = new DirectoryImpl("Directory 1", DirectoryType.INTERNAL, RemoteDirectory.class.getCanonicalName());
        directory.setAttribute(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, "atlassian-sha1");
        directory2 = new DirectoryImpl("Directory 2", DirectoryType.INTERNAL, RemoteDirectory.class.getCanonicalName());
        directory2.setAttribute(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, "atlassian-sha1");
        mockDirectoryDAO = mock(DirectoryDao.class);
        task = new UpgradeTask425();
        task.setDirectoryDao(mockDirectoryDAO);

        when(mockDirectoryDAO.search(any(EntityQuery.class))).thenReturn(Arrays.<Directory>asList(directory, directory2));
    }

    @Test
    public void testDoUpgradeWithUpgradableDirectory() throws Exception
    {
        task.doUpgrade();

        verify(mockDirectoryDAO, times(2)).update(directoryWithAttributeValue(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, "atlassian-security"));

        assertTrue(task.getErrors().isEmpty());
    }

    @Test
    public void testDoUpgradeWithNoUpgradableDirectory() throws Exception
    {
        directory.setAttribute(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, "md5");
        directory2.setAttribute(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, "test");

        task.doUpgrade();

        verify(mockDirectoryDAO, never()).update(any(Directory.class));

        assertTrue(task.getErrors().isEmpty());
    }

    @Test
    public void testDoUpgradeWithIssueUpgradingDirectory() throws Exception
    {
        when(mockDirectoryDAO.update(any(Directory.class))).thenThrow(new DataAccessException(""){});

        task.doUpgrade();

        assertEquals(2, task.getErrors().size());
    }

    private static Directory directoryWithAttributeValue(String key, String value)
    {
        return argThat(new HasAttributeValue(key, value));
    }

    static class HasAttributeValue extends ArgumentMatcher<Directory>
    {
        private final String key;
        private final String value;

        public HasAttributeValue(String key, String value)
        {
            this.key = key;
            this.value = value;
        }

        public boolean matches(Object dir)
        {
            String actualValue = ((Directory) dir).getValue(key);

            if (value == null)
            {
                return actualValue == null;
            }
            else
            {
                return value.equals(actualValue);
            }
        }
    }

}
