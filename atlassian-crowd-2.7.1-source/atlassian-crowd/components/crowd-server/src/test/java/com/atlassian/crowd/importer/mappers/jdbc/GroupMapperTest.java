package com.atlassian.crowd.importer.mappers.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.atlassian.crowd.model.group.Group;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.RowMapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * GroupMapper Tester.
 */
public class GroupMapperTest
{
    private RowMapper mapper = null;
    private ResultSet resultSet;

    @Before
    public void setUp() throws Exception
    {
        mapper = new GroupMapper("group_name", "description", new Long(1));

        ResultSet mockResultSet = mock(ResultSet.class);
        when(mockResultSet.getString("group_name")).thenReturn("crowd-administrators");
        when(mockResultSet.getString("description")).thenReturn("The Crowd Administrators Group");
        resultSet = mockResultSet;
    }

    @Test
    public void testMapRow() throws SQLException
    {
        Group remoteGroup = (Group) mapper.mapRow(resultSet, 0);

        assertEquals("crowd-administrators", remoteGroup.getName());
        assertEquals("The Crowd Administrators Group", remoteGroup.getDescription());
        assertTrue(remoteGroup.isActive());
    }

    @After
    public void tearDown() throws Exception
    {
        mapper = null;
        resultSet = null;
    }
}
