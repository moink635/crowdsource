package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.GenericLDAP;
import com.atlassian.crowd.directory.RemoteDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TestUpgradeTask112
{
    private UpgradeTask112 upgradeTask;

    @Mock
    private DirectoryManager mockDirectoryManager;

    private List<Directory> directories;

    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);

        upgradeTask = new UpgradeTask112();

        upgradeTask.setDirectoryManager(mockDirectoryManager);

        directories = new ArrayList<Directory>(1);

        DirectoryImpl directory = new DirectoryImpl("UpgradeTask112 Directory", DirectoryType.CONNECTOR, GenericLDAP.class.getName())
        {

            public RemoteDirectory getImplementation() throws DirectoryInstantiationException
            {
                return new GenericLDAP(null, null, null, null);
            }
        };

        directory.setActive(true);
        directory.setDescription("");
        directory.setName("directoryToUpgrade - 1");
        directory.setType(DirectoryType.CONNECTOR);

        directory.setImplementationClass(GenericLDAP.class.getName());

        directories.add(directory);
    }

    @Test
    public void testUpgrade() throws Exception
    {
        Directory directoryObjecToUpgrade = directories.get(0);
        // for seeing order
        when(mockDirectoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(directories);
        when(mockDirectoryManager.updateDirectory(any(Directory.class))).thenReturn(directoryObjecToUpgrade);

        assertFalse(directoryObjecToUpgrade.getAttributes().containsKey(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY));

        upgradeTask.doUpgrade();
        verify(mockDirectoryManager).searchDirectories(any(EntityQuery.class));
        verify(mockDirectoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.contains(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY)));
    }
}
