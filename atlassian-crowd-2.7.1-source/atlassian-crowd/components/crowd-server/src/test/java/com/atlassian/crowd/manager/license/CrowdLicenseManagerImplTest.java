package com.atlassian.crowd.manager.license;

import java.util.Date;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.extras.api.LicenseManager;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.atlassian.extras.core.AtlassianLicenseFactory;
import com.atlassian.extras.core.DefaultAtlassianLicenseFactory;
import com.atlassian.extras.core.DefaultLicenseManager;
import com.atlassian.extras.core.ProductLicenseFactory;
import com.atlassian.extras.core.crowd.CrowdProductLicenseFactory;
import com.atlassian.extras.decoder.api.DelegatingLicenseDecoder;
import com.atlassian.extras.decoder.api.LicenseDecoder;
import com.atlassian.extras.decoder.v1.Version1LicenseDecoder;
import com.atlassian.extras.decoder.v2.Version2LicenseDecoder;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CrowdLicenseManagerImplTest
{
    private CrowdLicenseManagerImpl crowdLicenseManager;

    @Mock private PropertyManager propertyManager;
    @Mock private DirectoryManager directoryManager;
    @Mock private ApplicationManager applicationManager;

    private String EXPIRED_EVALUATION_LICENSE_V1 = "NOMQmCTQjKEiWjuBRWDonoTPQrnOvAjuQfaOEgGWUCWDOO\n" +
            "mi2KQae5Tj1Y31B3H<ayr7eD2KgmhAs>E5<hrF7UI7jMBe\n" +
            "PnPutPmNONNoqrPRrOvTNmoQOnoNqMnqmtXSvvuuUwuXoS\n" +
            "vSwTsssqwvVsWuxUTMosSsorUUnnsqusmoqstvsUUnnsqu\n" +
            "smoqstvsUU1qiXppfXkWJlcqtXobWJvpqbjpUUnmUU";

    private String EXPIRED_LICENSE_V2 = "AAAA8g0ODAoPeNplUEtqwzAQ3c8pfAEbxU6gG0Eh9iLQOAWnB5jK00RgfRjJbnz7qo5DA93p/XhvV\n" +
            "FNQrH3UzsrOGcoGrcgGyvo/AdrRfBKfvj4CcZBVudmBYvfdF6iinkhGHgn2TPjrrjGSLIV4yUWZi\n" +
            "wr2zsbka9GQPLvo1ujbvec8e1ok5YwhVhqHR6I5oh5kTJFXuqHxAxXJBIm1kSxaRc3Na56fCqtcb\n" +
            "OHEF7Q6LGPkcc4cX4q1NKQLC8/OE8dZLmjCIY3vmvZ+fnpARzwRH+qVWVB26GGdnIRKPMC/Cdtc7\n" +
            "OB9ZHXFQE/8Jn0H/ACGFH4mMCwCFDhm4Kq3Y6KkjIeUytH3ZcmCsCmMAhRdz22RMU4msdyH5P4be\n" +
            "Hmg4ELP+w==X01ck";


    @Before
    public void setUp() throws Exception
    {
        crowdLicenseManager = new CrowdLicenseManagerImpl();
        crowdLicenseManager.setPropertyManager(propertyManager);

        LicenseDecoder licenseDecoder = new DelegatingLicenseDecoder(ImmutableList.<LicenseDecoder>of(
                new Version2LicenseDecoder(),
                new Version1LicenseDecoder()
        ));
        AtlassianLicenseFactory atlassianLicenseFactory = new DefaultAtlassianLicenseFactory(
                ImmutableMap.<Product, ProductLicenseFactory>of(Product.CROWD, new CrowdProductLicenseFactory()));

        LicenseManager licenseManager = new DefaultLicenseManager(licenseDecoder, atlassianLicenseFactory);


        crowdLicenseManager.setLicenseManager(licenseManager);
        crowdLicenseManager.setDirectoryManager(directoryManager);
        crowdLicenseManager.setApplicationManager(applicationManager);
    }

    @Test
    public void licensesCanBeDecoded()
    {
        crowdLicenseManager.getAtlassianLicense(EXPIRED_EVALUATION_LICENSE_V1);
        crowdLicenseManager.getAtlassianLicense(EXPIRED_LICENSE_V2);
    }

    @Test
    public void testAuthenticateLicenseWithBadLicenseString()
    {
        assertFalse("Bad license string should not be valid",
                    crowdLicenseManager.isLicenseKeyValid("bad license string"));
    }

    @Test
    public void testIsLicenseKeyValidWithExpiredEvaluationLicenseV1()
    {
        assertFalse("Expired evaluation license key should not be valid",
                    crowdLicenseManager.isLicenseKeyValid(EXPIRED_EVALUATION_LICENSE_V1));
    }

    @Test
    public void testIsLicenseKeyValidWithExpiredLicenseV2()
    {
        assertFalse("Expired license key should not be valid",
                    crowdLicenseManager.isLicenseKeyValid(EXPIRED_LICENSE_V2));
    }

    @Test
    public void testIsLicenseValidForUnlimitedLicense()
    {
        final CrowdLicense validUnlimitedLicense = mock(CrowdLicense.class);
        when(validUnlimitedLicense.isExpired()).thenReturn(false);
        when(validUnlimitedLicense.getMaximumNumberOfUsers()).thenReturn(-1);
        when(validUnlimitedLicense.isUnlimitedNumberOfUsers()).thenReturn(true);

        assertTrue("Unlimited license should be valid", crowdLicenseManager.isLicenseValid(validUnlimitedLicense));
    }

    @Test
    public void testInvalidLicenseKeyWithNull()
    {
        assertFalse("Null license key should not be valid", crowdLicenseManager.isLicenseKeyValid(null));
    }

    @Test
    public void testEvaluationLicenseValid()
    {
        final CrowdLicense evalTestLicense = mock(CrowdLicense.class);
        when(evalTestLicense.isExpired()).thenReturn(false);
        when(evalTestLicense.getMaximumNumberOfUsers()).thenReturn(500);
        when(evalTestLicense.isUnlimitedNumberOfUsers()).thenReturn(false);

        when(propertyManager.getCurrentLicenseResourceTotal()).thenReturn(30);
        assertTrue(crowdLicenseManager.isLicenseValid(evalTestLicense));
        verify(propertyManager).getCurrentLicenseResourceTotal();
    }

    @Test
    public void testCommercialLicenseInValidByResources()
    {
        final CrowdLicense invalidCommercialLicense = mock(CrowdLicense.class);
        when(invalidCommercialLicense.isExpired()).thenReturn(false);
        when(invalidCommercialLicense.getMaximumNumberOfUsers()).thenReturn(29);
        when(invalidCommercialLicense.isUnlimitedNumberOfUsers()).thenReturn(false);

        when(propertyManager.getCurrentLicenseResourceTotal()).thenReturn(30);  // limit exceeded
        assertFalse(crowdLicenseManager.isLicenseValid(invalidCommercialLicense));
        verify(propertyManager).getCurrentLicenseResourceTotal();
    }

    @Test
    public void testCommercialLicenseValidByResources()
    {
        final CrowdLicense validCommercialLicense = mock(CrowdLicense.class);
        when(validCommercialLicense.isExpired()).thenReturn(false);
        when(validCommercialLicense.getMaximumNumberOfUsers()).thenReturn(31);
        when(validCommercialLicense.isUnlimitedNumberOfUsers()).thenReturn(false);

        when(propertyManager.getCurrentLicenseResourceTotal()).thenReturn(30);  // still within limits
        assertTrue(crowdLicenseManager.isLicenseValid(validCommercialLicense));
        verify(propertyManager).getCurrentLicenseResourceTotal();
    }

    @Test
    public void testExpiredLicenseWithinGracePeriodIsValid()
    {
        final CrowdLicense licenseWithinGracePeriod = mock(CrowdLicense.class);
        when(licenseWithinGracePeriod.isExpired()).thenReturn(true);
        when(licenseWithinGracePeriod.isGracePeriodExpired()).thenReturn(false);
        when(licenseWithinGracePeriod.isUnlimitedNumberOfUsers()).thenReturn(true);

        assertTrue(crowdLicenseManager.isLicenseValid(licenseWithinGracePeriod));
    }

    @Test
    public void testExpiredLicenseOutsideGracePeriodIsNotValid()
    {
        final CrowdLicense licenseOutsideGracePeriod = mock(CrowdLicense.class);
        when(licenseOutsideGracePeriod.isExpired()).thenReturn(true);
        when(licenseOutsideGracePeriod.isGracePeriodExpired()).thenReturn(true);
        when(licenseOutsideGracePeriod.isUnlimitedNumberOfUsers()).thenReturn(true);

        assertFalse(crowdLicenseManager.isLicenseValid(licenseOutsideGracePeriod));
    }

    @Test
    public void licenseWithoutMaintenancePeriodExpiryDateIsWithinMaintenancePeriod()
    {
        final CrowdLicense licenseWithoutMaintenancePeriodExpiryDate = mock(CrowdLicense.class);

        assertTrue(crowdLicenseManager.isBuildWithinMaintenancePeriod(licenseWithoutMaintenancePeriodExpiryDate));
    }

    @Test
    public void licenseIsWithinMaintenancePeriod()
    {
        final CrowdLicense licenseWithinMaintenancePeriod = mock(CrowdLicense.class);
        when(licenseWithinMaintenancePeriod.getMaintenanceExpiryDate()).thenReturn(new Date(Long.MAX_VALUE));

        assertTrue(crowdLicenseManager.isBuildWithinMaintenancePeriod(licenseWithinMaintenancePeriod));
    }

    @Test
    public void licenseIsOutOfMaintenancePeriod()
    {
        final CrowdLicense licenseOutOfMaintenancePeriod = mock(CrowdLicense.class);
        when(licenseOutOfMaintenancePeriod.getMaintenanceExpiryDate()).thenReturn(new Date(0));

        assertFalse(crowdLicenseManager.isBuildWithinMaintenancePeriod(licenseOutOfMaintenancePeriod));
    }

    @Test
    public void licenseIsValidForSetupRegardlessOfNumberOfUsers()
    {
        final CrowdLicense validLimitedLicense = mock(CrowdLicense.class);
        when(validLimitedLicense.isExpired()).thenReturn(false);
        when(validLimitedLicense.getMaximumNumberOfUsers()).thenReturn(1);
        when(validLimitedLicense.isUnlimitedNumberOfUsers()).thenReturn(false);

        when(propertyManager.getCurrentLicenseResourceTotal()).thenReturn(2);

        assertFalse("Limited license should not be valid for use", crowdLicenseManager.isLicenseValid(validLimitedLicense));
        assertTrue("Limited license should be valid for setup", crowdLicenseManager.isSetupLicenseValid(validLimitedLicense));
    }

    @Test
    public void currentResourceUsageTotalWhenAllowAllToAuthenticate() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getId()).thenReturn(1L);
        when(directory.isActive()).thenReturn(true);

        DirectoryMapping directoryMapping = mock(DirectoryMapping.class);
        when(directoryMapping.getDirectory()).thenReturn(directory);
        when(directoryMapping.isAllowAllToAuthenticate()).thenReturn(true);

        Application application = mock(Application.class);
        when(application.getDirectoryMappings()).thenReturn(ImmutableList.of(directoryMapping));

        when(applicationManager.findAll()).thenReturn(ImmutableList.of(application));

        when(directoryManager.searchUsers(1L, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).with(
            Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(true)).returningAtMost(EntityQuery.ALL_RESULTS)))
            .thenReturn(ImmutableList.of("user1"));

        assertEquals(1, crowdLicenseManager.getCurrentResourceUsageTotal());
    }

    @Test
    public void currentResourceUsageTotalWhenNotAllowAllToAuthenticate() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getId()).thenReturn(1L);
        when(directory.isActive()).thenReturn(true);

        GroupMapping groupMapping = mock(GroupMapping.class);
        when(groupMapping.getDirectory()).thenReturn(directory);
        when(groupMapping.getGroupName()).thenReturn("group1");

        DirectoryMapping directoryMapping = mock(DirectoryMapping.class);
        when(directoryMapping.getDirectory()).thenReturn(directory);
        when(directoryMapping.isAllowAllToAuthenticate()).thenReturn(false);
        when(directoryMapping.getAuthorisedGroups()).thenReturn(ImmutableSet.of(groupMapping));

        Application application = mock(Application.class);
        when(application.getDirectoryMappings()).thenReturn(ImmutableList.of(directoryMapping));

        User user1 = mock(User.class);
        when(user1.isActive()).thenReturn(true);
        when(user1.getName()).thenReturn("user1");

        when(applicationManager.findAll()).thenReturn(ImmutableList.of(application));

        when(directoryManager.searchNestedGroupRelationships(1L, QueryBuilder.queryFor(
            User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(
            GroupType.GROUP)).withName("group1").returningAtMost(EntityQuery.ALL_RESULTS)))
            .thenReturn(ImmutableList.of(user1));

        when(directoryManager.searchUsers(1L, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).with(
            Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(true)).returningAtMost(EntityQuery.ALL_RESULTS)))
            .thenReturn(ImmutableList.of("user1"));

        assertEquals(1, crowdLicenseManager.getCurrentResourceUsageTotal());
    }

    @Test
    public void currentResourceUsageTotalShouldIgnoreInactiveDirectories() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getId()).thenReturn(1L);
        when(directory.isActive()).thenReturn(false);

        DirectoryMapping directoryMapping = mock(DirectoryMapping.class);
        when(directoryMapping.getDirectory()).thenReturn(directory);
        when(directoryMapping.isAllowAllToAuthenticate()).thenReturn(true);

        Application application = mock(Application.class);
        when(application.getDirectoryMappings()).thenReturn(ImmutableList.of(directoryMapping));

        when(applicationManager.findAll()).thenReturn(ImmutableList.of(application));

        assertEquals(0, crowdLicenseManager.getCurrentResourceUsageTotal());

        verifyZeroInteractions(directoryManager);
    }
}
