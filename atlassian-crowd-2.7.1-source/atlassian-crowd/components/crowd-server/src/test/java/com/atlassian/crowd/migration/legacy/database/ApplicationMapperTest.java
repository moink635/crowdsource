package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.migration.legacy.database.sql.MySQLLegacyTableQueries;
import com.atlassian.crowd.migration.legacy.database.sql.PostgresLegacyTableQueries;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.model.application.RemoteAddress;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public class ApplicationMapperTest extends BaseDatabaseTest
{
    private ApplicationMapper applicationMapper;
    private static final String CROWD_CREATE_DATE = "2010-02-03 15:06:03.000000000"; // timestamps from crowd15db.script
    private static final String CROWD_UPDATE_DATE = "2010-02-03 15:20:14.000000000";
    private static final String CROWD_CREDENTIAL = "dqW5gPwYpt7Mg+3Xc3Kb3W6xY3fnmp+BDgvazWVEtpHtPTf6/ZuUFwZ5XCm3aFvUw+lmueE1kCW6ul3NykDWYg==";

    private static final String GOOGLE_APP_CREATE_DATE = "2010-02-03 15:05:22.000000000"; // timestamps from crowd15db.script
    private static final String GOOGLE_APP_UPDATE_DATE = "2010-02-10 15:02:19.000000000";
    private static final String GOOGLE_APP_CREDENTIAL = "/iPwhVwwDOlh0cQqlpvtrYdwBLQ78c81Ld7yHhljWuoPT6RURvH1ZoSDYi2lQ9LfoPSyImrqQ0InFcv99NPSRg==";

    private static final String ATLASSIAN_SHA1 = "atlassian_sha1_applied";

    @Override
    protected void onSetUp() throws Exception
    {
        super.onSetUp();
        applicationMapper = new ApplicationMapper(sessionFactory, batchProcessor, jdbcTemplate, directoryDAO);
    }

    public void testMySQLScripts()
    {
        applicationMapper.setLegacyTableQueries(new MySQLLegacyTableQueries());
        _testMigrateApplications();
    }

    public void testPostgresScripts()
    {
        applicationMapper.setLegacyTableQueries(new PostgresLegacyTableQueries());
        _testMigrateApplications();
    }

    public void _testMigrateApplications()
    {
        List<ApplicationImpl> applications = applicationMapper.importApplicationsFromDatabase(importDataHolder.getOldToNewDirectoryIds());
        assertEquals(4, applications.size());

        // Test the 'crowd' application
        ApplicationImpl application = getApplication(applications, "crowd");
        assertNotNull(application);
        // General info
        assertEquals("Crowd Console", application.getDescription());
        assertEquals(applicationMapper.getDateFromDatabase(CROWD_CREATE_DATE), application.getCreatedDate());
        assertEquals(applicationMapper.getDateFromDatabase(CROWD_UPDATE_DATE), application.getUpdatedDate());
        assertEquals(new PasswordCredential(CROWD_CREDENTIAL, true), application.getCredential());
        assertEquals(ApplicationType.CROWD, application.getType());
        assertEquals("true", application.getAttributes().get(ATLASSIAN_SHA1));
        assertTrue(application.isActive());
        //Remote addresses
        assertEquals(4, application.getRemoteAddresses().size());
        assertTrue(application.getRemoteAddresses().containsAll(Arrays.<RemoteAddress>asList(new RemoteAddress("pyko.sydney.atlassian.com"), new RemoteAddress("127.0.0.1"), new RemoteAddress("localhost"), new RemoteAddress("172.20.3.56"))));
        // Directory mappings
        DirectoryMapping directoryMapping = application.getDirectoryMapping(1L);
        assertFalse(directoryMapping.isAllowAllToAuthenticate());
        assertEquals(1, directoryMapping.getAuthorisedGroups().size());
        assertEquals("crowd-administrators", directoryMapping.getAuthorisedGroups().iterator().next().getGroupName());
        assertEquals(9, directoryMapping.getAllowedOperations().size());
        assertEquals(ALL_PRE_CROWD_2_0_OPERATION_TYPES, directoryMapping.getAllowedOperations());

        // Test the 'google-apps' application
        application = getApplication(applications, "google-apps");
        assertNotNull(application);
        // General info
        assertEquals("Google Applications Connector", application.getDescription());
        assertEquals(applicationMapper.getDateFromDatabase(GOOGLE_APP_CREATE_DATE), application.getCreatedDate());
        assertEquals(applicationMapper.getDateFromDatabase(GOOGLE_APP_UPDATE_DATE), application.getUpdatedDate());
        assertEquals(new PasswordCredential(GOOGLE_APP_CREDENTIAL, true), application.getCredential());
        assertEquals(ApplicationType.PLUGIN, application.getType());
        assertEquals("true", application.getAttributes().get(ATLASSIAN_SHA1));
        assertTrue(application.isActive());
        // remote addresses
        assertEquals(0, application.getRemoteAddresses().size());
        // Directory Mapping
        directoryMapping = application.getDirectoryMapping(1L);
        assertTrue(directoryMapping.isAllowAllToAuthenticate());
        assertEquals(2, directoryMapping.getAuthorisedGroups().size());
        assertGroupsInDirectoryMapping(directoryMapping, Arrays.asList("crowd-administrators", "test-group1"));
        assertEquals(5, directoryMapping.getAllowedOperations().size());
        assertTrue(directoryMapping.getAllowedOperations().containsAll(new HashSet<OperationType>(Arrays.asList(OperationType.CREATE_GROUP, OperationType.CREATE_USER, OperationType.UPDATE_GROUP, OperationType.UPDATE_USER, OperationType.DELETE_USER))));
        // another directory mapping
        directoryMapping = application.getDirectoryMapping(2L);
        assertFalse(directoryMapping.isAllowAllToAuthenticate());
        assertEquals(0, directoryMapping.getAuthorisedGroups().size());
        assertEquals(9, directoryMapping.getAllowedOperations().size());
        assertEquals(ALL_PRE_CROWD_2_0_OPERATION_TYPES, directoryMapping.getAllowedOperations());

        // Test for a non-existent application
        application = getApplication(applications, "not-here");
        assertNull(application);
    }

    private ApplicationImpl getApplication(List<ApplicationImpl> applications, String name)
    {
        for (ApplicationImpl application : applications)
        {
            if (name.equals(application.getName()))
            {
                return application;
            }
        }
        // No application with matching name found
        return null;
    }

    private void assertGroupsInDirectoryMapping(DirectoryMapping directoryMapping, List<String> actualGroupNames)
    {
        List<String> mappingGroupNames = new ArrayList<String>();
        for (Iterator<GroupMapping> iterator = directoryMapping.getAuthorisedGroups().iterator(); iterator.hasNext();)
        {
            GroupMapping groupMapping = iterator.next();
            mappingGroupNames.add(groupMapping.getGroupName());
        }
        assertTrue(mappingGroupNames.containsAll(actualGroupNames));
    }
}