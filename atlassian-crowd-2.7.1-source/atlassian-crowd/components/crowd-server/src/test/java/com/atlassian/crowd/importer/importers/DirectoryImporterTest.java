package com.atlassian.crowd.importer.importers;

import java.util.Arrays;
import java.util.Collection;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.importer.config.DirectoryConfiguration;
import com.atlassian.crowd.importer.model.MembershipDTO;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.PasswordHelperImpl;

import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.hamcrest.collection.IsCollectionWithSize;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * DirectoryImporter Tester.
 */
public class DirectoryImporterTest
{
    private DirectoryImporter importer;

    private DirectoryManager directoryManager;
    private Long targetDirectoryId;
    private Long sourceDirectoryId;
    private DirectoryConfiguration configuration;
    private DirectoryConfiguration configurationForNestedGroupImport;
    private static final String TEST_GROUP_NAME = "test-group-1";
    private static final String TEST_PRINCIPAL_NAME = "test-principal-1";
    private GroupTemplate remoteGroup;
    private UserTemplateWithCredentialAndAttributes user;
    private MembershipDTO groupMembership;

    @Before
    public void setUp() throws Exception
    {
        directoryManager = mock(DirectoryManager.class);

        importer = new DirectoryImporter(directoryManager);

        sourceDirectoryId = new Long(1);
        targetDirectoryId = new Long(2);

        configuration = new DirectoryConfiguration(targetDirectoryId, sourceDirectoryId, Boolean.TRUE);
        configurationForNestedGroupImport = new DirectoryConfiguration(targetDirectoryId, sourceDirectoryId, Boolean.TRUE);
        configurationForNestedGroupImport.setImportNestedGroups(true);

        remoteGroup = new GroupTemplate(TEST_GROUP_NAME, sourceDirectoryId, GroupType.GROUP);

        user = new UserTemplateWithCredentialAndAttributes(TEST_PRINCIPAL_NAME, sourceDirectoryId, new PasswordCredential("password"));
        user.setActive(Boolean.TRUE);
        user.setEmailAddress("test@example.com");
        user.setAttribute("test", "testvalue");
        groupMembership = new MembershipDTO(MembershipDTO.ChildType.USER, TEST_PRINCIPAL_NAME, TEST_GROUP_NAME);
    }

    @Test
    public void testFindUserToGroupMemberships() throws Exception
    {
        when(directoryManager.searchGroups(sourceDirectoryId, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).returningAtMost(EntityQuery.ALL_RESULTS))).thenReturn(Arrays.<Group>asList(remoteGroup));
        when(directoryManager.searchDirectGroupRelationships(sourceDirectoryId, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(remoteGroup.getName()).returningAtMost(EntityQuery.ALL_RESULTS))).
                thenReturn(ImmutableList.of(user.getName()));

        final Collection<MembershipDTO> membershipCollection = importer.findUserToGroupMemberships(configuration);

        assertNotNull(membershipCollection);
        assertEquals(1, membershipCollection.size());
        assertTrue(membershipCollection.contains(groupMembership));
    }

    @Test
    public void testFindGroupToGroupMemberships() throws Exception
    {
        when(directoryManager.searchGroups(sourceDirectoryId, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).returningAtMost(EntityQuery.ALL_RESULTS))).thenReturn(ImmutableList.<Group>of(remoteGroup));
        when(directoryManager.searchDirectGroupRelationships(sourceDirectoryId, QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(remoteGroup.getName()).returningAtMost(EntityQuery.ALL_RESULTS))).
                thenReturn(ImmutableList.of("group1", "group2"));

        final Collection<MembershipDTO> membershipCollection = importer.findGroupToGroupMemberships(configurationForNestedGroupImport);

        assertNotNull(membershipCollection);
        assertEquals(2, membershipCollection.size());
        assertTrue(Iterables.any(membershipCollection, new Predicate<MembershipDTO>() {
            public boolean apply(MembershipDTO input)
            {
                return input.getChildName().equals("group1");
            }
        }));
        assertTrue(Iterables.any(membershipCollection, new Predicate<MembershipDTO>() {
            public boolean apply(MembershipDTO input)
            {
                return input.getChildName().equals("group2");
            }
        }));
    }

    @Test
    public void testFindGroups() throws Exception
    {
        when(directoryManager.searchGroups(sourceDirectoryId, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).returningAtMost(EntityQuery.ALL_RESULTS))).thenReturn(ImmutableList.<Group>of(remoteGroup));

        final Collection<GroupTemplate> groups = importer.findGroups(configuration);

        assertNotNull(groups);
        assertEquals(1, groups.size());
        assertTrue(groups.contains(remoteGroup));
    }

    @Test
    public void testFindUsers() throws Exception
    {
        when(directoryManager.searchUsers(sourceDirectoryId, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).returningAtMost(EntityQuery.ALL_RESULTS))).thenReturn(ImmutableList.<User>of(user));

        final Collection<UserTemplateWithCredentialAndAttributes> principals = importer.findUsers(configuration);

        assertNotNull(principals);
        assertEquals(1, principals.size());
        assertTrue(principals.contains(user));

        UserTemplateWithCredentialAndAttributes returnedPrincipal = principals.iterator().next();

        assertEquals(returnedPrincipal.getEmailAddress(), returnedPrincipal.getEmailAddress());
        assertEquals(returnedPrincipal.getCredential(), returnedPrincipal.getCredential());
        assertEquals(returnedPrincipal.getAttributes(), returnedPrincipal.getAttributes());
        assertEquals(returnedPrincipal.isActive(), returnedPrincipal.isActive());
    }

    @Test
    public void importUserWithNullCredentialsResultsInUserWithNoPassword() throws Exception
    {
        InternalUser userWithNullCredentials = mock(InternalUser.class);
        when(userWithNullCredentials.getName()).thenReturn(TEST_PRINCIPAL_NAME);

        assertEquals(TEST_PRINCIPAL_NAME, userWithNullCredentials.getName());
        assertNull(userWithNullCredentials.getCredential());

        when(directoryManager.searchUsers(sourceDirectoryId, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).returningAtMost(EntityQuery.ALL_RESULTS))).thenReturn(ImmutableList.<User>of(userWithNullCredentials));

        final Collection<UserTemplateWithCredentialAndAttributes> principals = importer.findUsers(configuration);

        assertThat(principals, IsCollectionWithSize.hasSize(1));

        UserTemplateWithCredentialAndAttributes found = principals.iterator().next();

        assertEquals(TEST_PRINCIPAL_NAME, found.getName());
        assertEquals(PasswordCredential.NONE, found.getCredential());
    }

    @After
    public void tearDown() throws Exception
    {
        remoteGroup = null;
        user = null;
        groupMembership = null;
        directoryManager = null;
        importer = null;
        configuration = null;
        sourceDirectoryId = null;
        targetDirectoryId = null;
    }
}
