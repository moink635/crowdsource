package com.atlassian.crowd.importer.mappers.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.atlassian.crowd.importer.model.MembershipDTO;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.RowMapper;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * UserMembershipMapper Tester.
 */
public class UserMembershipMapperTest
{
    private RowMapper mapper = null;
    private ResultSet resultSet;

    @Before
    public void setUp() throws Exception
    {
        mapper = new UserMembershipMapper("user_name", "group_name");

        ResultSet mockResultSet = mock(ResultSet.class);
        when(mockResultSet.getString("user_name")).thenReturn("jsmith");
        when(mockResultSet.getString("group_name")).thenReturn("crowd-administrators");
        resultSet = mockResultSet;
    }

    @Test
    public void testMapRow() throws SQLException
    {
        MembershipDTO membership = (MembershipDTO) mapper.mapRow(resultSet, 0);

        assertEquals("jsmith", membership.getChildName());
        assertEquals("crowd-administrators", membership.getParentName());
        assertEquals(MembershipDTO.ChildType.USER, membership.getChildType());
    }

    @After
    public void tearDown() throws Exception
    {
        mapper = null;
        resultSet = null;
    }
}
