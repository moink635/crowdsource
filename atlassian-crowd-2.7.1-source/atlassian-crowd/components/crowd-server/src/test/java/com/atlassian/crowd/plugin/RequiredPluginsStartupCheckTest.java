package com.atlassian.crowd.plugin;

import java.util.Collections;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.metadata.RequiredPluginProvider;

import com.google.common.collect.ImmutableSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RequiredPluginsStartupCheckTest
{
    @Mock
    PluginAccessor accessor;

    @Mock
    RequiredPluginProvider metadata;

    private RequiredPluginsStartupCheck systemPluginsStartupCheck;

    @Before
    public final void createInstance()
    {
        systemPluginsStartupCheck = new RequiredPluginsStartupCheck(accessor, metadata);
    }

    @Test
    public void checkPassesWithNoPlugins()
    {
        when(accessor.getPlugins()).thenReturn(Collections.<Plugin>emptyList());
        assertEquals(Collections.emptyList(), systemPluginsStartupCheck.requiredPluginsNotEnabled());
    }

    @Test
    public void checkFailsWhenRequiredPluginIsNotEnabled()
    {
        Plugin p = mock(Plugin.class);
        when(p.getName()).thenReturn("Required Plugin");
        when(p.getKey()).thenReturn("required");

        when(p.getPluginState()).thenReturn(PluginState.DISABLED);
        when(accessor.getPlugins()).thenReturn(Collections.singleton(p));
        when(metadata.getRequiredPluginKeys()).thenReturn(ImmutableSet.of("required"));

        assertEquals(Collections.singletonList("Required Plugin"),
                systemPluginsStartupCheck.requiredPluginsNotEnabled());
    }

    @Test
    public void checkPassesWhenRequiredPluginIsEnabled()
    {
        Plugin p = mock(Plugin.class);
        when(p.getKey()).thenReturn("required");

        when(p.getPluginState()).thenReturn(PluginState.ENABLED);
        when(accessor.getPlugins()).thenReturn(Collections.singleton(p));
        when(metadata.getRequiredPluginKeys()).thenReturn(ImmutableSet.of("required"));

        assertEquals(Collections.emptyList(), systemPluginsStartupCheck.requiredPluginsNotEnabled());
    }

    @Test
    public void checkPassesWhenOptionalPluginIsDisabled()
    {
        Plugin p = mock(Plugin.class);
        when(p.getKey()).thenReturn("optional");

        when(p.getPluginState()).thenReturn(PluginState.DISABLED);
        when(accessor.getPlugins()).thenReturn(Collections.singleton(p));
        when(metadata.getRequiredPluginKeys()).thenReturn(ImmutableSet.<String>of());

        assertEquals(Collections.emptyList(), systemPluginsStartupCheck.requiredPluginsNotEnabled());
    }

    @Test
    public void checkFailsWhenRequiredPluginIsMissing()
    {
        when(metadata.getRequiredPluginKeys()).thenReturn(ImmutableSet.of("required"));

        assertEquals(Collections.singletonList("required"),
                systemPluginsStartupCheck.requiredPluginsNotEnabled());
    }
}
