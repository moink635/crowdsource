package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.RemoteAddress;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpgradeTask430Test
{
    @Mock
    private ApplicationManager mockApplicationManager;

    private Application application1;
    private Application application2;

    private Set<RemoteAddress> remoteAddresses;

    private UpgradeTask430 upgradeTask;

    @Before
    public void setUp()
    {
        upgradeTask = new UpgradeTask430();
        upgradeTask.setApplicationManager(mockApplicationManager);

        application1 = ApplicationImpl.newInstance("Application 1", ApplicationType.CROWD);
        application2 = ApplicationImpl.newInstance("Application 2", ApplicationType.JIRA);

        // We can't really simulate the 'incomplete' remote addresses, but still want the apps to have remote addresses
        remoteAddresses = new HashSet<RemoteAddress>();
        remoteAddresses.add(new RemoteAddress("localhost"));
        remoteAddresses.add(new RemoteAddress("127.0.0.1"));

        // Give the apps some remote addresses
        ((ApplicationImpl) application1).setRemoteAddresses(remoteAddresses);
        ((ApplicationImpl) application2).setRemoteAddresses(remoteAddresses);
    }

    @Test
    public void testDoUpgrade() throws Exception
    {
        when(mockApplicationManager.findAll()).thenReturn(new ArrayList<Application>(Arrays.asList(application1, application2)));

        upgradeTask.doUpgrade();

        // just a sanity check that both apps were updated
        verify(mockApplicationManager, times(2)).update(any(ApplicationImpl.class));
        assertTrue(application1.getRemoteAddresses().containsAll(remoteAddresses));
        assertTrue(application2.getRemoteAddresses().containsAll(remoteAddresses));
    }
}
