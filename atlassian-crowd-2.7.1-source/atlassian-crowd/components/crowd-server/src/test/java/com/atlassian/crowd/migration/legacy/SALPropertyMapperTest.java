package com.atlassian.crowd.migration.legacy;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.dao.property.PropertyDAOHibernate;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import junit.framework.TestCase;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.hibernate.SessionFactory;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class SALPropertyMapperTest extends TestCase
{

    private final List<Object> addedObjects = new ArrayList();
    private Object returnObject;
    private SALPropertyMapper salPropertyMapper;
    private Document document;
    private PropertyDAOHibernate propertyDao;

    public void setUp() throws Exception
    {
        super.setUp();

        propertyDao = mock(PropertyDAOHibernate.class);

        // These legacy xml migration tests don't seem to require sessionFactory or batchProcessor
        SessionFactory sessionFactory = mock(SessionFactory.class);
        BatchProcessor batchProcessor = mock(BatchProcessor.class);

        salPropertyMapper = new SALPropertyMapper(sessionFactory, batchProcessor, propertyDao)
        {
            @Override
            protected Object addEntityViaSave(final Object entityToPersist)
            {
                addedObjects.add(entityToPersist);
                return returnObject;
            }
        };

        // load xml file
        SAXReader reader = new SAXReader();
        final URL resource = ClassLoaderUtils.getResource(StringUtils.replace(this.getClass().getCanonicalName(), ".", File.separator) + ".xml", this.getClass());
        document = reader.read(resource);
    }

    public void tearDown() throws Exception
    {
        verifyNoMoreInteractions(propertyDao);
        super.tearDown();
    }

    public void testImportSALProperties() throws Exception
    {
        Element salPropertiesNode = (Element) document.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + SALPropertyMapper.SALPROPERTY_XML_ROOT);
        salPropertyMapper.importXml(salPropertiesNode, null);
        assertEquals(2, addedObjects.size());

        // key: Studio Crowd
        Property salProperty = (Property) addedObjects.get(0);
        assertEquals("plugin.Studio Crowd", salProperty.getKey());
        assertEquals("notifier.subscriptions.null", salProperty.getName());
        assertEquals("<list> <map> <entry> <string>password</string> <null/> </entry> <entry> <string>class</string> <java-class>com.atlassian.notifier.NotificationSubscriptionImpl</java-class> </entry> <entry> <string>username</string> <null/> </entry> <entry> <string>url</string> <string>http://localhost/wiki/plugins/servlet/crowdnotify</string> </entry> <entry> <string>authenticationType</string> <com.atlassian.notifier.AuthenticationType>NONE</com.atlassian.notifier.AuthenticationType> </entry> </map> <map> <entry> <string>password</string> <null/> </entry> <entry> <string>class</string> <java-class>com.atlassian.notifier.NotificationSubscriptionImpl</java-class> </entry> <entry> <string>username</string> <null/> </entry> <entry> <string>url</string> <string>http://localhost:3990/plugins/servlet/crowdnotify</string> </entry> <entry> <string>authenticationType</string> <com.atlassian.notifier.AuthenticationType>NONE</com.atlassian.notifier.AuthenticationType> </entry> </map> </list>", salProperty.getValue());

        // key: null
        salProperty = (Property) addedObjects.get(1);
        assertEquals("plugin.null", salProperty.getKey());
        assertEquals("com.atlassian.notifier.crowd:build", salProperty.getName());
        assertEquals("2", salProperty.getValue());
    }
}