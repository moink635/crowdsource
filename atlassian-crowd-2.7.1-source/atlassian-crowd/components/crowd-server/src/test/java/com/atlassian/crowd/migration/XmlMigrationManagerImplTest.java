package com.atlassian.crowd.migration;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.event.migration.XMLRestoreFinishedEvent;
import com.atlassian.crowd.manager.application.CrowdApplicationPasswordManager;
import com.atlassian.crowd.manager.upgrade.UpgradeManager;
import com.atlassian.crowd.migration.legacy.LegacyXmlMigrator;
import com.atlassian.crowd.migration.verify.VerificationManager;
import com.atlassian.event.api.EventPublisher;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class XmlMigrationManagerImplTest
{
    private XmlMigrationManagerImpl xmlMigrationManager;
    private XmlMigrator xmlMigrator;
    private VerificationManager verificationManager;
    private LegacyXmlMigrator legacyXmlMigrator;
    private EventPublisher eventPublisher;

    @Before
    public final void setUp() throws Exception
    {
        xmlMigrationManager = new XmlMigrationManagerImpl()
        {
            @Override
            protected void cleanDatabase()
            {
                // do nothing
            }

            @Override
            protected void resetIdentifierGenerators()
            {
                // do nothing
            }

            @Override
            protected void flushAndClearHibernateSession()
            {
                // do nothing
            }

            @Override
            protected void clearCaches()
            {
                // do nothing
            }
        };

        xmlMigrator = mock(XmlMigrator.class);
        legacyXmlMigrator = mock(LegacyXmlMigrator.class);
        verificationManager = mock(VerificationManager.class);
        eventPublisher = mock(EventPublisher.class);

        setMocks(xmlMigrationManager);
    }

    private void setMocks(XmlMigrationManagerImpl xmlMigrationManager)
    {
        xmlMigrationManager.setXmlMigrator(xmlMigrator);
        xmlMigrationManager.setVerificationManager(verificationManager);
        xmlMigrationManager.setLegacyXmlMigrator(legacyXmlMigrator);
        xmlMigrationManager.setEventPublisher(eventPublisher);
        xmlMigrationManager.setUpgradeManager(mock(UpgradeManager.class));
        xmlMigrationManager.setCrowdApplicationPasswordManager(mock(CrowdApplicationPasswordManager.class));
    }

    @Test
    public void testExportXml() throws Exception
    {
        HashMap options = new HashMap();
        xmlMigrationManager.exportXml("test.xml", options);

        final File testFile = new File("test.xml");
        testFile.deleteOnExit();
        String xml = FileUtils.readFileToString(testFile, "UTF-8");

        assertTrue(xml.contains("<crowd>"));
        assertTrue(xml.contains("<version>"));
        assertTrue(xml.contains("<buildNumber>"));
        assertTrue(xml.contains("<buildDate>"));

        verify(xmlMigrator).exportXml(any(Element.class), eq(options));
    }

    private String getFilePath(String fileSuffix) throws URISyntaxException, IOException
    {
        final URL resource = ClassLoaderUtils.getResource(StringUtils.replace(getClass().getCanonicalName(), ".", File.separator) + fileSuffix, getClass());
        File file = new File(resource.toURI());
        return file.getCanonicalPath();
    }

    @Test
    public void testImportXml() throws Exception
    {
        xmlMigrationManager.importXml(getFilePath("-current.xml"));

        verify(xmlMigrator, times(1)).importXml(any(Element.class));

        verify(verificationManager, times(1)).validate(any(Document.class));
    }

    @Test
    public void testImportXmlIsEncodingAware() throws Exception
    {
        xmlMigrationManager.importXml(getFilePath("-encoding.xml"));

        ArgumentCaptor<Document> parsedDocumentCaptor = ArgumentCaptor.forClass(Document.class);
        verify(verificationManager).validate(parsedDocumentCaptor.capture());
        assertThat(parsedDocumentCaptor.getValue().getRootElement().getStringValue(), containsString("Stránge"));
    }

    @Test
    public void testImportLegacy() throws Exception
    {
        xmlMigrationManager.importXml(getFilePath("-legacy.xml"));

        verify(legacyXmlMigrator, times(1)).importXml(any(Element.class));
        verify(verificationManager, times(1)).validate(any(Document.class));
    }

    @Test
    public void importXmlFiresEventWhenComplete() throws Exception
    {
        xmlMigrationManager.importXml(getFilePath("-current.xml"));

        verify(eventPublisher).publish(Matchers.isA(XMLRestoreFinishedEvent.class));
    }

    @Test
    public void importXmlDoesNotFireEventWhenImportFails() throws Exception
    {
        try
        {
            xmlMigrationManager.importXml("no-such-file");
        }
        catch (ImportException e)
        {
        }

        verify(eventPublisher, never()).publish(Matchers.isA(XMLRestoreFinishedEvent.class));
    }

    @Test
    public void importXmlDoesNotFireEventWhenUpgradesFailAfterImport() throws Exception
    {
        XmlMigrationManagerImpl xmlMigrationManager = new XmlMigrationManagerImpl()
        {
            @Override
            protected void cleanDatabase()
            {
            }

            @Override
            protected void resetIdentifierGenerators()
            {
            }

            @Override
            protected void flushAndClearHibernateSession()
            {
            }

            @Override
            protected void clearCaches()
            {
            }

            @Override
            boolean performUpgrades()
            {
                return false;
            }
        };

        setMocks(xmlMigrationManager);

        xmlMigrationManager.importXml(getFilePath("-current.xml"));

        verify(eventPublisher, never()).publish(Matchers.isA(XMLRestoreFinishedEvent.class));
    }
}
