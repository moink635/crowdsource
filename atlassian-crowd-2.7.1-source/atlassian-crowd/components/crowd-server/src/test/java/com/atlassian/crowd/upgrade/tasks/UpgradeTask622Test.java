package com.atlassian.crowd.upgrade.tasks;

import java.util.Collections;

import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpgradeTask622Test
{
    @Mock
    private DirectoryDao directoryDao;

    @InjectMocks
    private UpgradeTask622 task = new UpgradeTask622();

    @Test
    public void shouldOnlyUpgradeLDAPConnectors() throws Exception
    {
        task.doUpgrade();

        verify(directoryDao).search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                                        .with(Restriction.on(DirectoryTermKeys.TYPE)
                                                  .exactlyMatching(DirectoryType.CONNECTOR))
                                        .returningAtMost(EntityQuery.ALL_RESULTS));

        verifyNoMoreInteractions(directoryDao);
    }

    @Test
    public void shouldNotUpdateUncachedDirectories() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getValue(DirectoryProperties.CACHE_ENABLED)).thenReturn("false");
        when(directory.getAllowedOperations()).thenReturn(ImmutableSet.of(OperationType.UPDATE_USER));

        when(directoryDao.search(any(EntityQuery.class))).thenReturn(ImmutableList.of(directory));

        task.doUpgrade();

        verify(directoryDao, never()).update(any(Directory.class));
    }

    @Test
    public void shouldNotUpdateCachedDirectoriesThatAlreadyHaveTheSetting() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getValue(DirectoryProperties.CACHE_ENABLED)).thenReturn("true");
        when(directory.getAllowedOperations()).thenReturn(ImmutableSet.of(OperationType.UPDATE_USER));
        when(directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS)).thenReturn("false"); // value is irrelevant

        when(directoryDao.search(any(EntityQuery.class))).thenReturn(ImmutableList.of(directory));

        task.doUpgrade();

        verify(directoryDao, never()).update(any(Directory.class));
    }

    @Test
    public void shouldNotEnableLocalUserStatusForUnconfiguredCachedReadOnlyDirectories() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getValue(DirectoryProperties.CACHE_ENABLED)).thenReturn("true");
        when(directory.getAllowedOperations()).thenReturn(Collections.<OperationType>emptySet());
        when(directory.getName()).thenReturn("directory1");
        when(directory.getType()).thenReturn(DirectoryType.CONNECTOR);
        when(directory.getImplementationClass()).thenReturn("implementation.Class");

        when(directoryDao.search(any(EntityQuery.class))).thenReturn(ImmutableList.of(directory));

        task.doUpgrade();

        verify(directoryDao, never()).update(any(Directory.class));
    }

    @Test
    public void shouldEnableLocalUserStatusForUnconfiguredCachedReadWriteDirectories() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getValue(DirectoryProperties.CACHE_ENABLED)).thenReturn("true");
        when(directory.getAllowedOperations()).thenReturn(ImmutableSet.of(OperationType.UPDATE_USER));
        when(directory.getName()).thenReturn("directory1");
        when(directory.getType()).thenReturn(DirectoryType.CONNECTOR);
        when(directory.getImplementationClass()).thenReturn("implementation.Class");

        when(directoryDao.search(any(EntityQuery.class))).thenReturn(ImmutableList.of(directory));

        task.doUpgrade();

        ArgumentCaptor<Directory> updatedDirectory = ArgumentCaptor.forClass(Directory.class);
        verify(directoryDao).update(updatedDirectory.capture());

        assertThat(updatedDirectory.getValue().getValue(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS), is("true"));
    }
}
