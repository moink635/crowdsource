package com.atlassian.crowd.plugin;

import com.atlassian.crowd.manager.property.PluginPropertyManager;
import static com.atlassian.crowd.plugin.CrowdPluginPersistentStateStore.GLOBAL_PLUGIN_STATE_PREFIX;
import com.atlassian.plugin.manager.PluginPersistentState;

import com.google.common.collect.ImmutableMap;

import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.*;

import java.util.Collections;
import java.util.Map;

public class CrowdPluginPersistentStateStoreTest extends TestCase
{
    private CrowdPluginPersistentStateStore persistentStateStore;
    private PluginPropertyManager pluginPropertyManager;
    private static final String PLUGIN_KEY = "directory.default.group.adder";
    private Map<String, String> currentProperty;
    private static final String KEY = "plugin." + GLOBAL_PLUGIN_STATE_PREFIX + PLUGIN_KEY;
    private static final String VALUE = Boolean.TRUE.toString();

    @Before
    public void setUp()
    {
        pluginPropertyManager = mock(PluginPropertyManager.class);

        persistentStateStore = new CrowdPluginPersistentStateStore(pluginPropertyManager);

        currentProperty = ImmutableMap.of(KEY, VALUE);
    }

    @Test
    public void testSave()
    {
        PluginPersistentState state = PluginPersistentState.Builder.create().addState(ImmutableMap.of(PLUGIN_KEY, Boolean.TRUE)).toState();

        persistentStateStore.save(state);

        verify(pluginPropertyManager).setProperty(GLOBAL_PLUGIN_STATE_PREFIX, PLUGIN_KEY, Boolean.TRUE.toString());
    }

    @Test
    public void testLoad()
    {
        when(pluginPropertyManager.findAll(GLOBAL_PLUGIN_STATE_PREFIX)).thenReturn(currentProperty);

        PluginPersistentState persistentState = persistentStateStore.load();

        assertEquals(Collections.singletonMap(KEY, Boolean.TRUE), persistentState.getMap());
    }
}
