package com.atlassian.crowd.service.soap.server;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.MembershipAlreadyExistsException;
import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPGroup;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.soap.SearchRestriction;
import com.atlassian.crowd.manager.application.AliasManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.manager.login.ForgottenLoginManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupTemplateWithAttributes;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.SearchContext;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.UserQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.service.soap.ObjectTranslator;
import com.atlassian.crowd.service.soap.SOAPService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertThat;

import static org.junit.Assert.fail;

import static org.mockito.Mockito.doThrow;

import static org.junit.Assert.assertEquals;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static com.atlassian.crowd.model.user.UserConstants.*;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SecurityServerGenericTest
{
    private SecurityServerGeneric securityServer;
    private com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken;
    private ApplicationImpl application;
    private ApplicationService applicationService;
    private SOAPService soapService;
    private AliasManager aliasManager;
    private PropertyManager propertyManager;
    private static final SearchRestriction USER_NAME_SEARCH_RESTRICTION = new SearchRestriction(SearchContext.PRINCIPAL_NAME, "bob");
    private static final SearchRestriction EMAIL_SEARCH_RESTRICTION = new SearchRestriction(SearchContext.PRINCIPAL_EMAIL, "bob");

    private static final String DISPLAY_NAME_VALUE = "Bob Brown";

    private UserTemplate joe;
    private static final String USERNAME_JOE = "joe";
    private static final String EMAILADDRESS_JOE = "joe@example.com";
    private UserTemplateWithAttributes bob;

    private static final String USERNAME_BOB = "bob";
    private static final String EMAILADDRESS_BOB = "bob@example.com";

    private GroupTemplate group1;
    private static final String GROUP1_NAME = "Group1";

    private GroupTemplateWithAttributes group2;
    private static final String GROUP2_NAME = "Group2";

    private static final String ATTRIBUTE_NAME = "colour";
    private static final String ATTRIBUTE_VALUE = "blue";

    private static final String ATTRIBUTE_NAME2 = "numbers";
    private static final Set<String> ATTRIBUTE_LIST = Sets.newHashSet("one", "two", "three");

    private static final SOAPAttribute[] EXTRA_ATTRIBUTES = new SOAPAttribute[]{
            new SOAPAttribute(ATTRIBUTE_NAME, ATTRIBUTE_VALUE),
            new SOAPAttribute(ATTRIBUTE_NAME2, ATTRIBUTE_LIST.toArray(new String[ATTRIBUTE_LIST.size()]))
    };

    private static final long DIRECTORY_ID = 1L;

    private TokenAuthenticationManager tokenAuthenticationManager;


    @Before
    public void setUp() throws Exception
    {
        soapService = mock(SOAPService.class);
        aliasManager = mock(AliasManager.class);
        propertyManager = mock(PropertyManager.class);

        applicationService = mock(ApplicationService.class);

        tokenAuthenticationManager = mock(TokenAuthenticationManager.class);

        applicationToken = new com.atlassian.crowd.integration.authentication.AuthenticatedToken("test-application", "secret-token");

        ForgottenLoginManager forgottenLoginManager = mock(ForgottenLoginManager.class);

        securityServer = new SecurityServerGeneric(soapService, applicationService, propertyManager, tokenAuthenticationManager, forgottenLoginManager );

        application = ApplicationImpl.newInstanceWithPassword("test-application", ApplicationType.GENERIC_APPLICATION, "secret-password");

        joe = new UserTemplate(USERNAME_JOE, new Long(1));
        joe.setEmailAddress(EMAILADDRESS_JOE);

        bob = new UserTemplateWithAttributes(USERNAME_BOB, new Long(1));
        bob.setAttribute(ATTRIBUTE_NAME, ATTRIBUTE_VALUE);
        bob.setAttribute(ATTRIBUTE_NAME2, ATTRIBUTE_LIST);
        bob.setEmailAddress(EMAILADDRESS_BOB);

        group1 = new GroupTemplate(GROUP1_NAME, new Long(1), GroupType.GROUP);

        group2 = new GroupTemplateWithAttributes(GROUP2_NAME, new Long(1), GroupType.GROUP);
        group2.setAttribute(ATTRIBUTE_NAME, ATTRIBUTE_VALUE);
        group2.setAttribute(ATTRIBUTE_NAME2, ATTRIBUTE_LIST);
    }

    private SearchRestriction[] buildCommonSearchRestrictions()
    {
        return new SearchRestriction[]{USER_NAME_SEARCH_RESTRICTION, EMAIL_SEARCH_RESTRICTION};
    }

    @Test
    public void testFindPrincipalByName() throws Exception
    {
        // Make sure each request is a happy request
        when(soapService.validateSOAPService(applicationToken)).thenReturn(application);

        SOAPPrincipal soapyJoe = new SOAPPrincipal(-1, USERNAME_JOE, new Long(1), null, true, null, null, null);

        when(aliasManager.findUsernameByAlias(application, USERNAME_JOE)).thenReturn(USERNAME_JOE);
        when(applicationService.findUserByName(application, USERNAME_JOE)).thenReturn(joe);

        SOAPPrincipal principal = securityServer.findPrincipalByName(applicationToken, USERNAME_JOE);

        // Only email set, so just 1 attribute
        assertEquals(soapyJoe, principal);
        assertEquals(1, principal.getAttributes().length);
    }

    @Test
    public void testFindPrincipalWithAttributesByName() throws Exception
    {
        // Make sure each request is a happy request
        when(soapService.validateSOAPService(applicationToken)).thenReturn(application);

        SOAPPrincipal soapyBob = new SOAPPrincipal(-1, USERNAME_BOB, new Long(1), null, true, null, null, EXTRA_ATTRIBUTES);

        when(aliasManager.findUsernameByAlias(application, USERNAME_BOB)).thenReturn(USERNAME_BOB);
        when(applicationService.findUserWithAttributesByName(application, USERNAME_BOB)).thenReturn(bob);

        SOAPPrincipal principal = securityServer.findPrincipalWithAttributesByName(applicationToken, USERNAME_BOB);

        // Email set, so total attributes is specified attributes(2) + 1 = 3
        assertEquals(soapyBob, principal);
        assertEquals(3, principal.getAttributes().length);
        assertContainsElements(principal.getAttribute(ATTRIBUTE_NAME).getValues(), "blue");
        assertContainsElements(principal.getAttribute(ATTRIBUTE_NAME2).getValues(), "one", "two", "three");
    }

    @Test
    public void testFindGroupByName() throws Exception
    {
        // Make sure each request is a happy request
        when(soapService.validateSOAPService(applicationToken)).thenReturn(application);

        SOAPGroup soapyGroup = new SOAPGroup(GROUP1_NAME, null);

        when(applicationService.findGroupByName(application, GROUP1_NAME)).thenReturn(group1);

        SOAPGroup group = securityServer.findGroupByName(applicationToken, GROUP1_NAME);

        assertEquals(soapyGroup, group);
        assertEquals(0, group.getAttributes().length);
    }

    @Test
    public void testFindGroupWithAttributesByName() throws Exception
    {
        // Make sure each request is a happy request
        when(soapService.validateSOAPService(applicationToken)).thenReturn(application);

        SOAPGroup soapyGroup = new SOAPGroup(GROUP2_NAME, null);

        when(applicationService.findGroupWithAttributesByName(application, GROUP2_NAME)).thenReturn(group2);

        SOAPGroup group = securityServer.findGroupWithAttributesByName(applicationToken, GROUP2_NAME);

        assertEquals(soapyGroup, group);
        assertEquals(2, group.getAttributes().length);
        assertContainsElements(group.getAttribute(ATTRIBUTE_NAME).getValues(), "blue");
        assertContainsElements(group.getAttribute(ATTRIBUTE_NAME2).getValues(), "one", "two", "three");
    }

    @Test
    public void testPrincipalSearch() throws Exception
    {
        // Make sure each request is a happy request
        when(soapService.validateSOAPService(applicationToken)).thenReturn(application);

        final EntityQuery query = QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Combine.allOf(Restriction.on(UserTermKeys.USERNAME).containing(USERNAME_JOE), Restriction.on(UserTermKeys.EMAIL).containing(EMAILADDRESS_JOE))).returningAtMost(EntityQuery.ALL_RESULTS);

        SOAPPrincipal soapyJoe = new SOAPPrincipal(-1, USERNAME_JOE, new Long(1), null, true, null, null, null);

        final List<UserTemplate> usersToReturn = Collections.singletonList(joe);

        final UserQuery userQuery = new UserQuery<User>(User.class, query.getSearchRestriction(), query.getStartIndex(), query.getMaxResults());

        when(applicationService.searchUsers(application, userQuery)).thenReturn(usersToReturn);

        SearchRestriction[] restrictions = {new SearchRestriction(SearchContext.PRINCIPAL_NAME, USERNAME_JOE), new SearchRestriction(SearchContext.PRINCIPAL_EMAIL, EMAILADDRESS_JOE)};

        final SOAPPrincipal[] principals = securityServer.searchPrincipals(applicationToken, restrictions);

        assertEquals(Lists.newArrayList(soapyJoe), Lists.newArrayList(principals));

        verify(soapService, atMost(1)).validateSOAPService(applicationToken);
        verify(applicationService, atMost(1)).searchUsers(application, userQuery);
    }

    @Test
    public void testBuildUserQueryWithNullRestrictions()
    {
        UserQuery expectedResult = new UserQuery(User.class, NullRestrictionImpl.INSTANCE, 0, EntityQuery.ALL_RESULTS);

        UserQuery userQuery = SecurityServerGeneric.buildUserQuery(User.class, null);

        assertEquals(expectedResult, userQuery);
    }

    @Test
    public void testBuildUserQueryWithAllRestrictionsForUser()
    {
        EntityQuery entityQuery = QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Combine.allOf(Restriction.on(UserTermKeys.USERNAME).containing(USER_NAME_SEARCH_RESTRICTION.getValue()), Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(Boolean.TRUE), Restriction.on(UserTermKeys.EMAIL).containing(EMAIL_SEARCH_RESTRICTION.getValue()), Restriction.on(UserTermKeys.DISPLAY_NAME).containing(DISPLAY_NAME_VALUE.toLowerCase()))).
                startingAt(0).returningAtMost(EntityQuery.ALL_RESULTS);


        UserQuery userQuery = SecurityServerGeneric.buildUserQuery(User.class, USER_NAME_SEARCH_RESTRICTION, EMAIL_SEARCH_RESTRICTION, new SearchRestriction(SearchContext.PRINCIPAL_ACTIVE, Boolean.TRUE.toString()), new SearchRestriction(SearchContext.PRINCIPAL_FULLNAME, DISPLAY_NAME_VALUE));


        assertEquals(new UserQuery(User.class, entityQuery.getSearchRestriction(), entityQuery.getStartIndex(), entityQuery.getMaxResults()), userQuery);
    }

    @Test
    public void testAddPrincipal() throws Exception
    {
        SOAPPrincipal principalToAdd = new SOAPPrincipal(USERNAME_JOE);
        principalToAdd.setDirectoryId(DIRECTORY_ID);
        principalToAdd.setAttributes(new SOAPAttribute[]{new SOAPAttribute(FIRSTNAME, "Joe"), new SOAPAttribute(LASTNAME, "Small"), new SOAPAttribute(EMAIL, EMAILADDRESS_JOE), new SOAPAttribute(ATTRIBUTE_NAME, ATTRIBUTE_VALUE), new SOAPAttribute(ATTRIBUTE_NAME2, ATTRIBUTE_LIST.toArray(new String[ATTRIBUTE_LIST.size()]))});
        PasswordCredential credential = new PasswordCredential("secret");

        UserTemplateWithAttributes userTemplateWithAttributes = new UserTemplateWithAttributes(USERNAME_JOE, DIRECTORY_ID);
        userTemplateWithAttributes.setName(USERNAME_JOE);
        userTemplateWithAttributes.setFirstName("Joe");
        userTemplateWithAttributes.setLastName("Small");
        userTemplateWithAttributes.setEmailAddress(EMAILADDRESS_JOE);
        userTemplateWithAttributes.setAttribute(ATTRIBUTE_NAME, ATTRIBUTE_VALUE);
        userTemplateWithAttributes.setAttribute(ATTRIBUTE_NAME2, ATTRIBUTE_LIST);

        when(soapService.validateSOAPService(applicationToken)).thenReturn(application);
        when(applicationService.findUserWithAttributesByName(application, USERNAME_JOE)).thenReturn(userTemplateWithAttributes);
        when(applicationService.addUser(application, userTemplateWithAttributes, credential)).thenReturn(userTemplateWithAttributes);
        when(aliasManager.findUsernameByAlias(application, USERNAME_JOE)).thenReturn(USERNAME_JOE);

        // The actual test call

        com.atlassian.crowd.integration.authentication.PasswordCredential cred = new com.atlassian.crowd.integration.authentication.PasswordCredential(credential.getCredential(), credential.isEncryptedCredential());
        SOAPPrincipal principal = securityServer.addPrincipal(applicationToken, principalToAdd, cred);

        assertEquals(USERNAME_JOE, principal.getName());
        assertEquals(DIRECTORY_ID, principal.getDirectoryId());
        assertEquals("Joe", principal.getAttribute(FIRSTNAME).getValues()[0]);
        assertEquals("Small", principal.getAttribute(LASTNAME).getValues()[0]);
        assertEquals(EMAILADDRESS_JOE, principal.getAttribute(EMAIL).getValues()[0]);
        assertEquals(ATTRIBUTE_VALUE, principal.getAttribute(ATTRIBUTE_NAME).getValues()[0]);
        assertEquals(ATTRIBUTE_LIST.size(), principal.getAttribute(ATTRIBUTE_NAME2).getValues().length);
        assertTrue(ATTRIBUTE_LIST.containsAll(Lists.newArrayList(principal.getAttribute(ATTRIBUTE_NAME2).getValues())));


        verify(applicationService, times(1)).addUser(application, userTemplateWithAttributes, credential);
        verify(applicationService, times(1)).storeUserAttributes(application, USERNAME_JOE, ObjectTranslator.buildUserAttributeMap(principalToAdd));
    }

    private void assertContainsElements(String[] attributes, String... names)
    {
        assertEquals(names.length, attributes.length);

        List<String> attributeValues = Arrays.asList(attributes);

        for (String name : names)
        {
            assertTrue(attributeValues.contains(name));
        }
    }

    @Test(expected = com.atlassian.crowd.integration.exception.ObjectNotFoundException.class)
    public void findRoleByNameAlwaysFails() throws Exception
    {
        securityServer.findRoleByName(null, null);
    }

    @Test
    public void addPrincipalToGroupWrapsMembershipAlreadyExistsException() throws Exception
    {
        when(soapService.validateSOAPService(applicationToken)).thenReturn(application);
        doThrow(new MembershipAlreadyExistsException("", "")).when(applicationService).addUserToGroup(application, "user", "group");

        try
        {
            securityServer.addPrincipalToGroup(applicationToken, "user", "group");
            fail();
        }
        catch (RemoteException e)
        {
            assertThat(e.getCause(), Matchers.instanceOf(MembershipAlreadyExistsException.class));
        }
    }
}
