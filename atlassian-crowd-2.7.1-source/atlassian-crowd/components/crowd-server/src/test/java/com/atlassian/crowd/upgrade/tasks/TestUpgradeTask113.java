package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;

import com.google.common.collect.ImmutableList;

import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

public class TestUpgradeTask113 extends MockObjectTestCase
{
    private UpgradeTask113 upgradeTask = null;

    private Directory directory1 = null;
    private DirectoryImpl directory2 = null;

    protected void setUp() throws Exception
    {
        super.setUp();

        upgradeTask = new UpgradeTask113();

        directory1 = new DirectoryImpl("Directory One", DirectoryType.INTERNAL, "com.atlassian.crowd.directory1.InternalDirectory");

        directory2 = new DirectoryImpl("Directory Two", DirectoryType.INTERNAL, "com.atlassian.crowd.directory1.InternalDirectory");
        directory2.setAttribute(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, PasswordEncoderFactory.SSHA_ENCODER);

        // The directoryDAO mock
        Mock mockDirectorymanager = new Mock(DirectoryManager.class);

        mockDirectorymanager.expects(once()).method("searchDirectories").with(eq(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.INTERNAL)).returningAtMost(EntityQuery.ALL_RESULTS))).will(returnValue(ImmutableList.of(directory1, directory2)));

        mockDirectorymanager.expects(once()).method("updateDirectory").with(eq(directory1));

        upgradeTask.setDirectoryManager((DirectoryManager) mockDirectorymanager.proxy());
    }

    public void testDoUpgrade() throws Exception
    {
        upgradeTask.doUpgrade();

        // Since we passed in directory1 see if the required encryption attribute was set
        assertEquals(directory1.getValue(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD), PasswordEncoderFactory.DES_ENCODER);
        // Since we passed in directory2 see if the required encryption attribute was NOT set
        assertEquals(directory2.getValue(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD), PasswordEncoderFactory.SSHA_ENCODER);

        assertTrue(upgradeTask.getErrors().isEmpty());
    }

    protected void tearDown() throws Exception
    {
        upgradeTask = null;
        directory1 = null;
        super.tearDown();
    }
}
