package com.atlassian.crowd.importer.importers;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.importer.config.CsvConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.model.MembershipDTO;
import com.atlassian.crowd.importer.model.Result;
import com.atlassian.crowd.manager.directory.BulkAddResult;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.apache.commons.collections.OrderedBidiMap;
import org.apache.commons.collections.bidimap.TreeBidiMap;
import org.apache.commons.io.FileUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.test.matchers.CrowdMatchers.userNamed;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

/**
 * CsvImporter Tester.
 */
@RunWith(MockitoJUnitRunner.class)
public class CsvImporterTest
{
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    private CsvImporter importer;
    private CsvConfiguration configuration;

    private File groupFile;
    private File principalFile;

    private UserTemplateWithCredentialAndAttributes bob;
    private GroupTemplate group;
    private GroupTemplate group2;
    @Mock
    private DirectoryManager mockDirectoryManager;
    private static final String GROUP_1_NAME = "administrators";
    private static final String GROUP_2_NAME = "users";
    private static final String USER_NAME = "BobPrince";

    @Before
    public void setUp() throws Exception
    {
        configuration = new CsvConfiguration();
        configuration.setImportPasswords(true);
        configuration.setDirectoryID(1L);
        configuration.setDelimiter(',');

        bob = new UserTemplateWithCredentialAndAttributes(USER_NAME, configuration.getDirectoryID(), new PasswordCredential("password", false));
        bob.setActive(true);
        bob.setEmailAddress("bobp@atlassian.com");
        bob.setFirstName("Bob");
        bob.setLastName("Percival");

        group = new GroupTemplate(GROUP_1_NAME, configuration.getDirectoryID(), GroupType.GROUP);

        group2 = new GroupTemplate(GROUP_2_NAME, configuration.getDirectoryID(), GroupType.GROUP);

        String groups = buildCsvFrom(new String[][]{
                        {"Username", "Group name"},
                        {bob.getName(), group.getName()},
                        {bob.getName(), group2.getName()},
                });

        groupFile = temporaryFolder.newFile("csvgrouptest.csv");
        FileUtils.writeStringToFile(groupFile, groups, "UTF-8");
        configuration.setGroupMemberships(groupFile);

        String principals = buildCsvFrom( new String[][]{
                        {"First Name", "Last Name", "Email Address", "Username", "Password"},
                        {bob.getFirstName(), bob.getLastName(), bob.getEmailAddress(), bob.getName(), bob.getCredential().getCredential()},
                });

        principalFile = temporaryFolder.newFile("csvprincipaltest.csv");
        FileUtils.writeStringToFile(principalFile, principals, "UTF-8");
        configuration.setUsers(principalFile);

        OrderedBidiMap userMap = new TreeBidiMap();
        userMap.put("principal.0", CsvConfiguration.USER_FIRSTNAME);
        userMap.put("principal.1", CsvConfiguration.USER_LASTNAME);
        userMap.put("principal.2", CsvConfiguration.USER_EMAILADDRESS);
        userMap.put("principal.3", CsvConfiguration.USER_USERNAME);
        configuration.setUserMappingConfiguration(userMap);

        OrderedBidiMap groupMap = new TreeBidiMap();
        groupMap.put("group.0", CsvConfiguration.GROUP_USERNAME);
        groupMap.put("group.1", CsvConfiguration.GROUP_NAME);
        configuration.setGroupMappingConfiguration(groupMap);

        importer = new CsvImporter(mockDirectoryManager);
    }

    @Test
    public void testFindGroupsWithEmptyConfiguration() throws ImporterException
    {
        Collection<GroupTemplate> groups = importer.findGroups(new CsvConfiguration());

        assertThat(groups, allOf(notNullValue(), empty()));
    }

    @Test
    public void testFindUsersWithEmptyEmptyConfiguration() throws ImporterException
    {
        Collection<UserTemplateWithCredentialAndAttributes> users = importer.findUsers(new CsvConfiguration());

        assertThat(users, allOf(notNullValue(), empty()));
    }

    @Test
    public void testFindGroupMembershipsWithEmptyEmptyConfiguration() throws ImporterException
    {
        Collection<MembershipDTO> memberships = importer.findUserToGroupMemberships(new CsvConfiguration());

        assertThat(memberships, allOf(notNullValue(), empty()));
    }

    @Test
    public void testFindGroups() throws ImporterException
    {
        configuration.setGroupMemberships(groupFile);

        OrderedBidiMap groupConfiguration = new TreeBidiMap();

        groupConfiguration.put(CsvConfiguration.GROUP_PREFIX + "0", CsvConfiguration.GROUP_USERNAME);
        groupConfiguration.put(CsvConfiguration.GROUP_PREFIX + "1", CsvConfiguration.GROUP_NAME);

        configuration.setGroupMappingConfiguration(groupConfiguration);

        Collection<GroupTemplate> groups = importer.findGroups(configuration);

        assertThat(groups, allOf(notNullValue(), hasSize(2)));
    }

    @Test
    public void testFindGroupMemberships() throws ImporterException
    {
        configuration.setGroupMemberships(groupFile);

        OrderedBidiMap groupConfiguration = new TreeBidiMap();

        groupConfiguration.put(CsvConfiguration.GROUP_PREFIX + "0", CsvConfiguration.GROUP_USERNAME);
        groupConfiguration.put(CsvConfiguration.GROUP_PREFIX + "1", CsvConfiguration.GROUP_NAME);

        configuration.setGroupMappingConfiguration(groupConfiguration);

        Collection<MembershipDTO> memberships = importer.findUserToGroupMemberships(configuration);

        assertThat(memberships, containsInAnyOrder(
                new MembershipDTO(MembershipDTO.ChildType.USER, USER_NAME, GROUP_1_NAME),
                new MembershipDTO(MembershipDTO.ChildType.USER, USER_NAME, GROUP_2_NAME)));
    }

    @Test
    public void testFindUsers() throws ImporterException
    {
        configuration.setUsers(principalFile);

        OrderedBidiMap userConfiguration = new TreeBidiMap();

        userConfiguration.put(CsvConfiguration.USER_PREFIX + "0", CsvConfiguration.USER_FIRSTNAME);
        userConfiguration.put(CsvConfiguration.USER_PREFIX + "1", CsvConfiguration.USER_LASTNAME);
        userConfiguration.put(CsvConfiguration.USER_PREFIX + "2", CsvConfiguration.USER_EMAILADDRESS);
        userConfiguration.put(CsvConfiguration.USER_PREFIX + "3", CsvConfiguration.USER_USERNAME);
        userConfiguration.put(CsvConfiguration.USER_PREFIX + "4", CsvConfiguration.USER_PASSWORD);

        configuration.setUserMappingConfiguration(userConfiguration);

        Collection<UserTemplateWithCredentialAndAttributes> users = importer.findUsers(configuration);

        assertThat(users, Matchers.<UserTemplateWithCredentialAndAttributes>hasItem(userNamed(USER_NAME)));
    }

    @Test
    public void testConfigurationType()
    {
        assertEquals(CsvConfiguration.class, importer.getConfigurationType());
    }

    @Test
    public void testImportUsersAndGroups() throws Exception
    {
        // principals
        when(mockDirectoryManager.addAllUsers(configuration.getDirectoryID(), ImmutableList.of(bob),
                configuration.isOverwriteTarget()))
                .thenReturn(BulkAddResult.<User>builder(1)
                        .setOverwrite(configuration.isOverwriteTarget())
                        .build());

        // groups
        when(mockDirectoryManager.addAllGroups(configuration.getDirectoryID(), ImmutableSet.of(group, group2),
                configuration.isOverwriteTarget()))
                .thenReturn(BulkAddResult.<Group>builder(2)
                        .setOverwrite(configuration.isOverwriteTarget())
                        .build());

        // group memberships
        when(mockDirectoryManager.addAllUsersToGroup(configuration.getDirectoryID(), ImmutableList.of(bob.getName()),
                group.getName()))
                .thenReturn(BulkAddResult.<String>builder(1)
                        .setOverwrite(configuration.isOverwriteTarget())
                        .build());
        when(mockDirectoryManager.addAllUsersToGroup(configuration.getDirectoryID(), ImmutableList.of(bob.getName()),
                group2.getName()))
                .thenReturn(BulkAddResult.<String>builder(1)
                        .setOverwrite(configuration.isOverwriteTarget())
                        .build());

        Result result = importer.importUsersGroupsAndMemberships(configuration);

        // One user Pete
        assertThat(result.getUsersImported(), is(1L));

        // Two group 'administrators', 'users'
        assertThat(result.getGroupsImported(), is(2L));

        // Two memberships, pete -> administrators, pete -> users
        assertThat(result.getGroupMembershipsImported(), is(2L));

        assertThat(result.getUsersFailedImport(), empty());
        assertThat(result.getGroupsFailedImport(), empty());
        assertThat(result.getGroupMembershipsFailedImport(), empty());
        assertThat(result.getUsersAlreadyExist(), empty());
        assertThat(result.getGroupsAlreadyExist(), empty());
    }

    /**
     * @param table 2d array forming the table to convert to CSV, such that table[r][c] gives the value at
     *              in row r and column c. Values will be double quoted, but double quotes in the value won't be escaped.
     * @return a csv file in string form
     */
    private String buildCsvFrom(String[][] table)
    {
        StringBuilder sb = new StringBuilder();
        for (String[] row : table)
        {
            boolean first = true;
            for (String columnValue : row)
            {
                if (!first)
                {
                    sb.append(",");
                }
                sb.append("\"").append(columnValue).append("\"");
                first = false;
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    @Test
    public void specificFailureForGroupColumnCountMismatch() throws ImporterException, IOException
    {
        CsvConfiguration cfg = new CsvConfiguration();

        cfg.setDirectoryID(0L);
        cfg.setDelimiter(',');

        OrderedBidiMap gmc = new TreeBidiMap();
        gmc.put("group.0", CsvConfiguration.GROUP_USERNAME);
        gmc.put("group.1", CsvConfiguration.GROUP_NAME);
        cfg.setGroupMappingConfiguration(gmc);

        File userFile = temporaryFolder.newFile("users.csv");
        FileUtils.writeStringToFile(userFile, "username\nadmin\n", Charsets.UTF_8);
        cfg.setUsers(userFile);

        File groupFile = temporaryFolder.newFile("members.csv");
        FileUtils.writeStringToFile(groupFile, "username,group,group2\nuser,users,admins\n", Charsets.UTF_8);
        cfg.setGroupMemberships(groupFile);

        assertThat(importer.findUserToGroupMemberships(cfg),
                Matchers.contains(Matchers.hasToString("user(USER) - users")));
    }
}
