package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;

import com.google.common.collect.ImmutableList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UpgradeTask623GenerateExternalIdTest
{
    private static final long DIRECTORY_ID = 1L;

    @Mock
    private DirectoryDao directoryDao;

    @Mock
    private UserDao userDao;

    @InjectMocks
    private UpgradeTask623GenerateExternalId task;

    @Test
    public void shouldOnlyUpgradeUsersInInternalDirectories() throws Exception
    {
        task.doUpgrade();

        verify(directoryDao).search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                                        .with(Restriction.on(DirectoryTermKeys.TYPE)
                                                  .exactlyMatching(DirectoryType.INTERNAL))
                                        .returningAtMost(EntityQuery.ALL_RESULTS));

        verifyNoMoreInteractions(directoryDao);
    }

    @Test
    public void shouldNotUpdateUsersThatAlreadyHaveExternalId() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getId()).thenReturn(DIRECTORY_ID);
        when(directoryDao.search(any(EntityQuery.class))).thenReturn(ImmutableList.of(directory));

        User user = mock(User.class);
        when(user.getExternalId()).thenReturn("already-have-external-id");
        when(userDao.search(eq(DIRECTORY_ID), any(EntityQuery.class))).thenReturn(ImmutableList.of(user));

        task.doUpgrade();

        verify(userDao, never()).update(any(User.class));
    }

    @Test
    public void shouldUpdateUsersThatWereMissingExternalId() throws Exception
    {
        Directory directory = mock(Directory.class);
        when(directory.getId()).thenReturn(DIRECTORY_ID);
        when(directoryDao.search(any(EntityQuery.class))).thenReturn(ImmutableList.of(directory));

        User user = mock(User.class);
        when(user.getName()).thenReturn("username");
        when(user.getExternalId()).thenReturn(null); // user does not have externalId yet
        when(userDao.search(eq(DIRECTORY_ID), any(EntityQuery.class))).thenReturn(ImmutableList.of(user));

        task.doUpgrade();

        ArgumentCaptor<User> updatedUser = ArgumentCaptor.forClass(User.class);
        verify(userDao).update(updatedUser.capture());
        assertNotNull("Should generate externalId", updatedUser.getValue().getExternalId());
    }
}
