package com.atlassian.crowd.importer.mappers.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.RowMapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * UserTemplateWithCredentialAndAttributesMapper Tester.
 */
public class UserMapperTest
{
    private RowMapper userMapperWithPassword = null;
    private ResultSet resultSet;
    private RowMapper userMapperNoPassword;

    @Before
    public void setUp() throws Exception
    {
        Configuration configuration = new Configuration(new Long(1), "jira", Boolean.TRUE, Boolean.FALSE);

        userMapperWithPassword = new UserMapper(configuration, "username", "email_address", "full_name", "password_hash");
        userMapperNoPassword = new UserMapper(configuration, "username", "email_address", "full_name", "password_hash");

        ResultSet mockResultSet = mock(ResultSet.class);
        when(mockResultSet.getString("username")).thenReturn("jsmith");
        when(mockResultSet.getString("email_address")).thenReturn("jsmith@test.com");
        when(mockResultSet.getString("full_name")).thenReturn("John Smith");
        when(mockResultSet.getString("password_hash")).thenReturn("HASHED_SECRET");
        resultSet = mockResultSet;
    }

    @Test
    public void testMappingPrincipal() throws SQLException
    {
        UserTemplateWithCredentialAndAttributes user = (UserTemplateWithCredentialAndAttributes) userMapperWithPassword.mapRow(resultSet, 0);

        assertNotNull(user);
        assertEquals("jsmith", user.getName());
        assertTrue(user.isActive());
        assertEquals("jsmith@test.com", user.getEmailAddress());
        assertEquals("John", user.getFirstName());
        assertEquals("Smith", user.getLastName());
        assertEquals("John Smith", user.getDisplayName());
        assertEquals("HASHED_SECRET", user.getCredential().getCredential());
    }

    @Test
    public void testMappingPrincipalWithNoPassword() throws SQLException
    {
        UserTemplateWithCredentialAndAttributes user = (UserTemplateWithCredentialAndAttributes) userMapperNoPassword.mapRow(resultSet, 0);

        assertNotNull(user);
        assertEquals("jsmith", user.getName());
        assertTrue(user.isActive());
        assertEquals("jsmith@test.com", user.getEmailAddress());
        assertEquals("John", user.getFirstName());
        assertEquals("Smith", user.getLastName());
        assertEquals("John Smith", user.getDisplayName());
        PasswordCredential credential = user.getCredential();
        assertNotNull(credential);
        assertNotNull(credential.getCredential());
        assertNotSame("HASED_SECRET", credential.getCredential());
    }

    @After
    public void tearDown() throws Exception
    {
        userMapperWithPassword = null;
        userMapperNoPassword = null;
        resultSet = null;
    }
}
