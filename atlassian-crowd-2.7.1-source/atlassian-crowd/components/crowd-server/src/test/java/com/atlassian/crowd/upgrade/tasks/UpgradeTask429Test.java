package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.*;

/**
 * Tests that directories having InternalDirectoryWithBatching as an
 * implementation class are updated to use InternalDirectory class as an
 * implementation class.
 *
 * @since v2.1
 */
@RunWith(MockitoJUnitRunner.class)
public class UpgradeTask429Test
{
    @Mock private DirectoryDao mockDirectoryDao;
    private Directory directory1;
    private Directory directory2;

    private UpgradeTask429 upgradeTask;

    @Before
    public void setUp()
    {
        upgradeTask = new UpgradeTask429();
        upgradeTask.setDirectoryDao(mockDirectoryDao);

        directory1 = new DirectoryImpl("Directory 1", DirectoryType.INTERNAL, "com.atlassian.crowd.directory.InternalDirectoryWithBatching");
        directory2 = new DirectoryImpl("Directory 2", DirectoryType.INTERNAL, "com.atlassian.crowd.directory.InternalDirectoryWithBatching");
    }

    @Test
    public void testDoUpgrade() throws Exception
    {
        when(mockDirectoryDao.search(any(EntityQuery.class))).thenReturn(Arrays.asList(directory1, directory2));

        upgradeTask.doUpgrade();

        verify(mockDirectoryDao, times(2)).update(argThat(new HasDirectoryImplementationUpdated()));
    }

    private static class HasDirectoryImplementationUpdated extends ArgumentMatcher<Directory>
    {
        @Override
        public boolean matches(final Object argument)
        {
            Directory directory = (Directory) argument;
            return InternalDirectory.class.getName().equals(directory.getImplementationClass());
        }
    }
}
