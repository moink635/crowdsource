package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

import java.sql.SQLException;
import java.util.List;

/**
 * UpgradeTask003 Tester.
 */
public class TestUpgradeTask003 extends MockObjectTestCase
{
    Mock bootstrapManager = null;

    private TestUpgradeTask003.MockResettableHiLoGenerator resettableHiLoGeneratorHelper;

    private static final String CALLING_FLAG = "You called!";
    UpgradeTask003 upgradeTask = null;

    public void setUp() throws Exception
    {
        super.setUp();

        bootstrapManager = new Mock(CrowdBootstrapManager.class);

        upgradeTask = new UpgradeTask003();
        upgradeTask.setBootstrapManager((CrowdBootstrapManager) bootstrapManager.proxy());
        resettableHiLoGeneratorHelper = new MockResettableHiLoGenerator();
        upgradeTask.setResetableHiLoGeneratorHelper(resettableHiLoGeneratorHelper);
    }

    public void testDoUpgrade() throws Exception
    {
        bootstrapManager.expects(once()).method("isSetupComplete").withNoArguments().will(returnValue(true));

        upgradeTask.doUpgrade();

        assertFalse(upgradeTask.getErrors().isEmpty());

        assertEquals(1, upgradeTask.getErrors().size());

        assertTrue(upgradeTask.getErrors().contains(CALLING_FLAG));
    }

    public void testDoUpgradeWithSQLExceptionBeingThrown() throws Exception
    {
        bootstrapManager.expects(never()).method("isSetupComplete");

        resettableHiLoGeneratorHelper.setThrowSQLException(true);

        upgradeTask.doUpgrade();

        assertFalse(upgradeTask.getErrors().isEmpty());

        assertEquals(1, upgradeTask.getErrors().size());

        assertFalse(upgradeTask.getErrors().contains(CALLING_FLAG));

    }

    public void testDoUpgradeWhereWeAlreadyHaveAHiLoSet() throws Exception
    {
        bootstrapManager.expects(never()).method("isSetupComplete");

        resettableHiLoGeneratorHelper.setHiValue(15L);

        upgradeTask.doUpgrade();

        assertTrue(upgradeTask.getErrors().isEmpty());
    }

    public void testShortDescription()
    {
        assertNotNull(upgradeTask.getShortDescription());
    }

    public void tearDown() throws Exception
    {
        super.tearDown();
    }

    private class MockResettableHiLoGenerator extends ResetableHiLoGeneratorHelper
    {
        private long hiValue = 0L;
        private boolean throwSQLException = false;

        public long getHiValue() throws SQLException
        {
            if (throwSQLException)
            {
                throw new SQLException("Failed to get Hi-Lo value");
            }

            return hiValue;
        }

        public void setNextHiValue(List<String> errors)
        {
            errors.add(CALLING_FLAG);
        }

        public void setHiValue(long hiValue)
        {
            this.hiValue = hiValue;
        }

        public void setThrowSQLException(boolean throwSQLException)
        {
            this.throwSQLException = throwSQLException;
        }
    }
}
