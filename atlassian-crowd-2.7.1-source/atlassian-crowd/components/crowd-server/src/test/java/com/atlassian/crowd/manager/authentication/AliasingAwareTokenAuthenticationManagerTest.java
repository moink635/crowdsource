package com.atlassian.crowd.manager.authentication;

import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.application.AliasManager;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.assertEquals;
import static org.mockito.AdditionalAnswers.returnsSecondArg;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AliasingAwareTokenAuthenticationManagerTest
{
    private static final String USER1_NAME = "user1";
    private static final String USER1_ALIAS = "alias1";

    private static final User USER1 = new UserTemplate(USER1_NAME);

    private static final String APPLICATION_NAME = "application";

    @Mock private TokenAuthenticationManager tokenAuthenticationManager;
    @Mock private ApplicationManager applicationManager;
    @Mock private AliasManager aliasManager;
    @Mock private Application application;
    private Token token;

    private AliasingAwareTokenAuthenticationManager aliasingAwareTokenAuthenticationManager;

    @Before
    public void setup() throws ApplicationNotFoundException
    {
        token = new Token.Builder(1L, USER1_NAME, "identifier-hash", 1234L, "random-hash")
                .setCreatedDate(new Date(0))
                .setLastAccessedTime(1L)
                .setLifetime(TokenLifetime.inSeconds(600L))
                .create();

        when(applicationManager.findByName(APPLICATION_NAME)).thenReturn(application);
        when(aliasManager.findUsernameByAlias(application, USER1_ALIAS)).thenReturn(USER1_NAME);
        when(aliasManager.findAliasByUsername(application, USER1_NAME)).thenReturn(USER1_ALIAS);

        aliasingAwareTokenAuthenticationManager = new AliasingAwareTokenAuthenticationManager(tokenAuthenticationManager, applicationManager, aliasManager);
    }

    @Test
    public void testAuthenticateUserUsingRealUserName() throws Exception
    {
        when(aliasManager.findUsernameByAlias(application, USER1_NAME)).then(returnsSecondArg());

        TokenLifetime tokenLifetime = TokenLifetime.inSeconds(600L);
        final UserAuthenticationContext realAuthContext = new UserAuthenticationContext(USER1_NAME, null, null, APPLICATION_NAME);
        when(tokenAuthenticationManager.authenticateUser(realAuthContext, tokenLifetime)).thenReturn(token);

        assertEquals(token, aliasingAwareTokenAuthenticationManager.authenticateUser(realAuthContext, tokenLifetime));
    }

    @Test
    public void testAuthenticateUserUsingAlias() throws Exception
    {
        TokenLifetime tokenLifetime = TokenLifetime.inSeconds(600L);
        final UserAuthenticationContext realAuthContext = new UserAuthenticationContext(USER1_NAME, null, null, APPLICATION_NAME);
        when(tokenAuthenticationManager.authenticateUser(realAuthContext, tokenLifetime)).thenReturn(token);

        final UserAuthenticationContext aliasedAuthContext = new UserAuthenticationContext(USER1_ALIAS, null, null, APPLICATION_NAME);
        assertEquals(token, aliasingAwareTokenAuthenticationManager.authenticateUser(aliasedAuthContext, tokenLifetime));
    }

    @Test
    public void testAuthenticateUserWithoutValidatingPassword() throws Exception
    {
        final UserAuthenticationContext realAuthContext = new UserAuthenticationContext(USER1_NAME, null, null, APPLICATION_NAME);
        when(tokenAuthenticationManager.authenticateUserWithoutValidatingPassword(realAuthContext)).thenReturn(token);

        final UserAuthenticationContext aliasedAuthContext = new UserAuthenticationContext(USER1_ALIAS, null, null, APPLICATION_NAME);
        assertEquals(token, aliasingAwareTokenAuthenticationManager.authenticateUserWithoutValidatingPassword(aliasedAuthContext));
    }

    @Test
    public void testFindUserByToken_LowerCase() throws Exception
    {
        when(application.isLowerCaseOutput()).thenReturn(true);
        when(tokenAuthenticationManager.findUserByToken("token", "application")).thenReturn(USER1);

        assertEquals(USER1_ALIAS.toLowerCase(Locale.ENGLISH), aliasingAwareTokenAuthenticationManager.findUserByToken("token", "application").getName());
    }

    @Test
    public void testFindTokenByKey_LowerCase() throws Exception
    {
        when(application.isLowerCaseOutput()).thenReturn(true);
        when(tokenAuthenticationManager.findUserTokenByKey("actualToken", "application")).thenReturn(token);

        Token actualToken = aliasingAwareTokenAuthenticationManager.findUserTokenByKey("actualToken", "application");

        assertEquals(USER1_ALIAS.toLowerCase(Locale.ENGLISH), actualToken.getName());
        assertEquals(token.getDirectoryId(), actualToken.getDirectoryId());
        assertEquals(token.getIdentifierHash(), actualToken.getIdentifierHash());
        assertEquals(token.getRandomHash(), actualToken.getRandomHash());
        assertEquals(token.getRandomNumber(), actualToken.getRandomNumber());
        assertEquals(token.getCreatedDate(), actualToken.getCreatedDate());
        assertEquals(token.getLastAccessedTime(), actualToken.getLastAccessedTime());
        assertEquals(token.getLifetime(), actualToken.getLifetime());
    }
}
