package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.property.PropertyManager;
import org.jmock.Mock;
import org.jmock.MockObjectTestCase;

public class TestUpgradeTask321 extends MockObjectTestCase
{
    UpgradeTask upgradeTask = null;
    protected Mock mockPropertyManager;

    public void setUp() throws Exception
    {
        super.setUp();

        mockPropertyManager = new Mock(PropertyManager.class);

        upgradeTask = new UpgradeTask321();
        ((UpgradeTask321) upgradeTask).setPropertyManager((PropertyManager) mockPropertyManager.proxy());
    }

    public void testSetInsecure() throws Exception
    {
        mockPropertyManager.expects(once()).method("isSecureCookie").will(returnValue(false));
        mockPropertyManager.expects(once()).method("setSecureCookie").with(eq(false));

        upgradeTask.doUpgrade();
    }

    public void testDontReset() throws Exception
    {
        mockPropertyManager.expects(once()).method("isSecureCookie").will(returnValue(true));
        mockPropertyManager.expects(never()).method("setSecureCookie");

        upgradeTask.doUpgrade();
    }
}

