package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.MicrosoftActiveDirectory;
import com.atlassian.crowd.directory.OpenLDAP;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * UpgradeTask214 Tester.
 */
public class TestUpgradeTask214
{
    @Mock private DirectoryManager directoryManager;

    private UpgradeTask214 upgradeTask;
    private DirectoryImpl directoryToUpdate;
    private DirectoryImpl openLDAPDirectory;

    @Before
    public void setUp() throws Exception
    {
        MockitoAnnotations.initMocks(this);

        upgradeTask = new UpgradeTask214();
        upgradeTask.setDirectoryManager(directoryManager);

        directoryToUpdate = new DirectoryImpl("directory-to-update", DirectoryType.CONNECTOR, MicrosoftActiveDirectory.class.getCanonicalName());
        directoryToUpdate.setAttribute(LDAPPropertiesMapper.LDAP_USER_ENCRYPTION_METHOD, "ssha");

        openLDAPDirectory = new DirectoryImpl("open-ldap-directory", DirectoryType.CONNECTOR, OpenLDAP.class.getCanonicalName());
        openLDAPDirectory.setAttribute(LDAPPropertiesMapper.LDAP_USER_ENCRYPTION_METHOD, "ssha");
    }

    @After
    public void tearDown() throws Exception
    {
        upgradeTask = null;
        directoryToUpdate = null;
        openLDAPDirectory = null;
    }

    @Test
    public void testDoUpgrade() throws Exception
    {
        when(directoryManager.searchDirectories(buildDirectoryQuery())).thenReturn(Arrays.<Directory>asList(directoryToUpdate));

        upgradeTask.doUpgrade();

        assertTrue(upgradeTask.getErrors().isEmpty());
        verify(directoryManager).updateDirectory(argThat(DirectoryAttributesMatcher.notContains(LDAPPropertiesMapper.LDAP_USER_ENCRYPTION_METHOD)));
    }

    @Test
    public void testDoUpgradeWithOpenLDAPDirectory() throws Exception
    {
        when(directoryManager.searchDirectories(buildDirectoryQuery())).thenReturn(Arrays.<Directory>asList(openLDAPDirectory));

        upgradeTask.doUpgrade();

        verify(directoryManager, never()).updateDirectory(any(Directory.class));

        assertTrue(upgradeTask.getErrors().isEmpty());
    }

    @Test
    public void testUpgradeDescription()
    {
        assertNotNull(upgradeTask.getShortDescription());
    }

    private EntityQuery buildDirectoryQuery()
    {
        return QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS);
    }
}
