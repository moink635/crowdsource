package com.atlassian.crowd.util;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.event.PluginEventManager;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static org.hamcrest.Matchers.emptyIterable;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 *
 * @since v2.7
 */
@RunWith(MockitoJUnitRunner.class)
public class PluginResourceBundleProviderTest
{
    private PluginResourceBundleProvider service;
    @Mock
    private PluginAccessor pluginAccessor;
    private Locale locale = Locale.FRENCH;
    @Mock
    private PluginEventManager pluginEventManager;

    @Before
    public void setUp() throws Exception
    {
        service = new PluginResourceBundleProvider(pluginAccessor, locale, pluginEventManager);
    }

    @Test
    public void noPluginsShouldReturnNoBundles() throws Exception
    {
        when(pluginAccessor.getEnabledPlugins()).thenReturn(ImmutableList.<Plugin>of());

        final Iterable<ResourceBundle> bundles = service.getResourceBundles();
        assertThat(bundles, emptyIterable());
    }

    @Test
    public void onePluginWithNoResourceShouldReturnNoBundle() throws Exception
    {
        final Plugin plugin = plugin();
        when(pluginAccessor.getEnabledPlugins()).thenReturn(ImmutableList.of(plugin));

        final Iterable<ResourceBundle> bundles = service.getResourceBundles();
        assertThat(bundles, emptyIterable());
    }

    @Test
    public void onePluginWithOneTopLevelResourceDescriptorShouldReturnOneBundle() throws Exception
    {
        final Plugin plugin = plugin();
        addTopLevelResourceDescriptor(plugin, "testBundle1");
        when(pluginAccessor.getEnabledPlugins()).thenReturn(ImmutableList.of(plugin));

        final Iterable<ResourceBundle> bundles = service.getResourceBundles();
        assertThat(bundles, Matchers.<ResourceBundle>iterableWithSize(1));
        final ResourceBundle bundle = bundles.iterator().next();
        assertThat(bundle.getString("key1"), is("foo"));
        assertThat(bundle.getString("key2"), is("bar"));
    }

    @Test
    public void onePluginWithOneModuleLevelResourceDescriptorShouldReturnOneBundle() throws Exception
    {
        final Plugin plugin = plugin();
        addModuleLevelResourceDescriptor(plugin, "testBundle1");
        when(pluginAccessor.getEnabledPlugins()).thenReturn(ImmutableList.of(plugin));

        final Iterable<ResourceBundle> bundles = service.getResourceBundles();
        assertThat(bundles, Matchers.<ResourceBundle>iterableWithSize(1));
        final ResourceBundle bundle = bundles.iterator().next();
        assertThat(bundle.getString("key1"), is("foo"));
        assertThat(bundle.getString("key2"), is("bar"));
    }

    @Test
    public void onePluginWithTwoDescriptorsShouldReturnTwoBundles() throws Exception
    {
        final Plugin plugin = plugin();
        addTopLevelResourceDescriptor(plugin, "testBundle1");
        addModuleLevelResourceDescriptor(plugin, "testBundle2");

        when(pluginAccessor.getEnabledPlugins()).thenReturn(ImmutableList.of(plugin));

        final Iterable<ResourceBundle> bundles = service.getResourceBundles();
        assertThat(bundles, Matchers.<ResourceBundle>iterableWithSize(2));

        final Iterator<ResourceBundle> iterator = bundles.iterator();
        final ResourceBundle bundle1 = iterator.next();
        assertThat(bundle1.getString("key1"), is("foo"));
        assertThat(bundle1.getString("key2"), is("bar"));
        assertThat(bundle1.containsKey("key3"), is(false));
        assertThat(bundle1.containsKey("key4"), is(false));

        final ResourceBundle bundle2 = iterator.next();
        assertThat(bundle2.containsKey("key1"), is(false));
        assertThat(bundle2.containsKey("key2"), is(false));
        assertThat(bundle2.getString("key3"), is("wizz"));
        assertThat(bundle2.getString("key4"), is("bang"));
    }

    @Test
    public void twoPluginsWithOneDescriptorShouldReturnTwoBundles() throws Exception
    {
        final Plugin plugin1 = plugin();
        final Plugin plugin2 = plugin();

        addTopLevelResourceDescriptor(plugin1, "testBundle1");
        addModuleLevelResourceDescriptor(plugin2, "testBundle2");

        when(pluginAccessor.getEnabledPlugins()).thenReturn(ImmutableList.of(plugin1, plugin2));

        final Iterable<ResourceBundle> bundles = service.getResourceBundles();
        assertThat(bundles, Matchers.<ResourceBundle>iterableWithSize(2));

        final Iterator<ResourceBundle> iterator = bundles.iterator();
        final ResourceBundle bundle1 = iterator.next();
        assertThat(bundle1.getString("key1"), is("foo"));
        assertThat(bundle1.getString("key2"), is("bar"));
        assertThat(bundle1.containsKey("key3"), is(false));
        assertThat(bundle1.containsKey("key4"), is(false));

        final ResourceBundle bundle2 = iterator.next();
        assertThat(bundle2.containsKey("key1"), is(false));
        assertThat(bundle2.containsKey("key2"), is(false));
        assertThat(bundle2.getString("key3"), is("wizz"));
        assertThat(bundle2.getString("key4"), is("bang"));
    }

    @Test
    public void baseBundleWithTranslatedBundleShouldReturnOneBundle() throws Exception
    {
        final Plugin plugin = plugin();

        addTopLevelResourceDescriptor(plugin, "testBundle3");

        when(pluginAccessor.getEnabledPlugins()).thenReturn(ImmutableList.of(plugin));

        final Iterable<ResourceBundle> bundles = service.getResourceBundles();
        assertThat(bundles, Matchers.<ResourceBundle>iterableWithSize(1));

        final ResourceBundle bundle1 = bundles.iterator().next();
        assertThat(bundle1.getString("key1"), is("foo"));
        assertThat(bundle1.getString("key2"), is("la barre"));
    }

    private Plugin plugin()
    {
        Plugin plugin = mock(Plugin.class);
        when(plugin.getClassLoader()).thenReturn(this.getClass().getClassLoader());

        return plugin;
    }

    private void addTopLevelResourceDescriptor(final Plugin mockPlugin, final String resourceBundleName)
    {
        final ResourceDescriptor descriptor = resourceDescriptor(resourceBundleName);
        when(mockPlugin.getResourceDescriptors()).thenReturn(ImmutableList.of(descriptor));
    }

    private void addModuleLevelResourceDescriptor(final Plugin mockPlugin, final String resourceBundleName)
    {
        final ResourceDescriptor descriptor = resourceDescriptor(resourceBundleName);
        final ModuleDescriptor moduleDescriptor = mock(ModuleDescriptor.class);
        final List<ModuleDescriptor<?>> moduleDescriptors = ImmutableList.<ModuleDescriptor<?>>of(moduleDescriptor);

        when(moduleDescriptor.getResourceDescriptors()).thenReturn(ImmutableList.of(descriptor));
        when(mockPlugin.getModuleDescriptors()).thenReturn(moduleDescriptors);
    }

    private ResourceDescriptor resourceDescriptor(final String resourceBundleName)
    {
        final ResourceDescriptor descriptor = mock(ResourceDescriptor.class);
        when(descriptor.getType()).thenReturn("i18n");
        when(descriptor.getLocation()).thenReturn(resourceBundleName);
        return descriptor;
    }
}
