package com.atlassian.crowd.manager.token;

import java.util.Date;

import com.atlassian.crowd.dao.token.TokenDAO;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import com.google.common.collect.ImmutableList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SimpleTokenManagerImplTest
{
    @Mock
    private TokenDAO tokenDAO;

    @InjectMocks
    private SimpleTokenManagerImpl tokenManager;

    @Test
    public void testFindByRandomHash() throws Exception
    {
        Token token = mock(Token.class);
        when(tokenDAO.findByRandomHash("randomhash")).thenReturn(token);

        assertSame(token, tokenManager.findByRandomHash("randomhash"));
    }

    @Test
    public void testFindByIdentifierHash() throws Exception
    {
        Token token = mock(Token.class);
        when(tokenDAO.findByIdentifierHash("identifierhash")).thenReturn(token);

        assertSame(token, tokenManager.findByIdentifierHash("identifierhash"));
    }

    @Test
    public void testAdd() throws Exception
    {
        Token tokenTemplate = mock(Token.class);
        Token savedToken = mock(Token.class);

        when(tokenDAO.add(tokenTemplate)).thenReturn(savedToken);

        assertSame(savedToken, tokenManager.add(tokenTemplate));
    }

    @Test
    public void testUpdate() throws Exception
    {
        Token tokenTemplate = mock(Token.class);
        Token savedToken = mock(Token.class);

        when(tokenDAO.update(tokenTemplate)).thenReturn(savedToken);

        assertSame(savedToken, tokenManager.update(tokenTemplate));
    }

    @Test
    public void testRemoveByToken() throws Exception
    {
        Token tokenToBeDeleted = mock(Token.class);

        tokenManager.remove(tokenToBeDeleted);

        verify(tokenDAO).remove(tokenToBeDeleted);
    }

    @Test
    public void testRemoveByName() throws Exception
    {
        tokenManager.remove(1L, "name");

        verify(tokenDAO).remove(1L, "name");
    }

    @Test
    public void testRemoveAllByDirectory() throws Exception
    {
        tokenManager.removeAll(1L);

        verify(tokenDAO).removeAll(1L);
    }

    @Test
    public void testRemoveExpiredTokens() throws Exception
    {
        Date currentTime = new Date();
        long maxLifeSeconds = 1L;

        tokenManager.removeExpiredTokens(currentTime, maxLifeSeconds);

        verify(tokenDAO).removeExpiredTokens(currentTime, maxLifeSeconds);
    }

    @Test
    public void testRemoveAll() throws Exception
    {
        tokenManager.removeAll();

        verify(tokenDAO).removeAll();
    }
}
