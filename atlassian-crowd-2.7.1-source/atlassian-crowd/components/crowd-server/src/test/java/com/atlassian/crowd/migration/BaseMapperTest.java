package com.atlassian.crowd.migration;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.util.List;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;

import com.google.common.base.Function;
import com.google.common.collect.Lists;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

import junit.framework.TestCase;

public abstract class BaseMapperTest extends TestCase
{
    protected static final DirectoryImpl DIRECTORY1 = directoryWithIdAndName(1L, "Directory One");
    {
        DIRECTORY1.setType(DirectoryType.INTERNAL);
    }
    protected static final DirectoryImpl DIRECTORY2 = directoryWithIdAndName(2L, "Directory Two");
    {
        DIRECTORY2.setType(DirectoryType.CONNECTOR);
    }
    protected static final DirectoryImpl DIRECTORY3 = directoryWithIdAndName(3L, "Directory Three");
    {
        DIRECTORY3.setType(DirectoryType.DELEGATING);
    }
    protected static final DirectoryImpl DIRECTORY4 = directoryWithIdAndName(4L, "Directory Four");
    {
        DIRECTORY4.setType(DirectoryType.CROWD);
    }

    protected Document document;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        // load xml file
        SAXReader reader = new SAXReader();
        final URL resource = ClassLoaderUtils.getResource(StringUtils.replace(getClass().getCanonicalName(), ".", File.separator) + ".xml", getClass());
        document = reader.read(resource);
    }

    @Override
    protected void tearDown() throws Exception
    {
        document = null;
        super.tearDown();
    }

    protected Element createExportElement(String rootName)
    {
        // create xml for data in hibernate persistence
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement(XmlMigrationManagerImpl.XML_ROOT);

        return root.addElement(rootName);
    }

    protected String exportDocumentToString(Document document) throws IOException
    {
        StringWriter xmlOut = new StringWriter();

        OutputFormat fmt = OutputFormat.createPrettyPrint();
        fmt.setIndentSize(4);

        XMLWriter writer = new XMLWriter(xmlOut, fmt);
        writer.write(document);
        writer.close();
        xmlOut.close();

        return xmlOut.toString();
    }

    protected String getTestExportXmlString() throws IOException
    {
        final URL resource = ClassLoaderUtils.getResource(StringUtils.replace(getClass().getCanonicalName(), ".", File.separator) + ".xml", getClass());

        assertNotNull(resource);

        return IOUtils.toString(resource.openStream(), "UTF-8");
    }

    protected static DirectoryImpl directoryWithIdAndName(long id, String name)
    {
        final InternalEntityTemplate template = new InternalEntityTemplate();
        template.setId(id);
        template.setName(name);
        return new DirectoryImpl(template);
    }

    /**
     * Given the XML root element, the name of the entity element, and the name of a subelement nested to the
     * entity elements, this method returns a list with the trimmed contents of such subelements. This method
     * is intended to simplify assertions on the XML output.
     * @param root root element of the XML document
     * @param entityElementName name of the entities that are defined in the XML document as children of the root
     * @param subElementName name of the elements nested within the entity elements (i.e., an entity property)
     * @return a list of the trimmed contents of the subelements
     */
    protected static List<String> projectEntitiesSubelement(Element root,
                                                            String entityElementName,
                                                            final String subElementName)
    {
        return Lists.transform((List<Element>) root.elements(entityElementName), new Function<Element, String>()
        {
            @Override
            public String apply(Element input)
            {
                return input.element(subElementName).getTextTrim();
            }
        });
    }
}
