package com.atlassian.crowd.migration.legacy;

import java.util.EnumSet;
import java.util.List;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;

import com.google.common.collect.ImmutableList;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PartialXmlMapperTest
{
    private PartialXmlMapper mapper;

    @Mock private SessionFactory sessionFactory;
    @Mock private BatchProcessor batchProcessor;
    @Mock private DirectoryManager directoryManager;

    @Before
    public void setUp()
    {
        mapper = new PartialXmlMapper(sessionFactory, batchProcessor, directoryManager,
                                      EnumSet.of(DirectoryType.INTERNAL, DirectoryType.DELEGATING));
    }

    @Test
    public void onlyInternalAndDelegatingDirectoriesAreImportableByDefault()
    {
        assertTrue(mapper.isImportableDirectory(directoryOfType(DirectoryType.INTERNAL)));
        assertTrue(mapper.isImportableDirectory(directoryOfType(DirectoryType.DELEGATING)));

        assertFalse(mapper.isImportableDirectory(directoryOfType(DirectoryType.CONNECTOR)));
        assertFalse(mapper.isImportableDirectory(directoryOfType(DirectoryType.CUSTOM)));
        assertFalse(mapper.isImportableDirectory(directoryOfType(DirectoryType.CROWD)));
    }

    @Test
    public void findAllExportableDirectoriesShouldReturnInternalAndDelegatingByDefault()
    {
        Directory directory = mock(Directory.class);
        when(directoryManager.searchDirectories(any(EntityQuery.class))).thenReturn(ImmutableList.<Directory>of(directory));

        List<Directory> directories = mapper.findAllExportableDirectories();

        assertEquals(ImmutableList.of(directory), directories);

        EntityQuery<Directory> expectedQuery =
            QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                .with(Combine.anyOf(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.INTERNAL),
                                    Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.DELEGATING)))
                .returningAtMost(EntityQuery.ALL_RESULTS);
        verify(directoryManager).searchDirectories(expectedQuery);
        verifyNoMoreInteractions(directoryManager);
    }

    @Test
    public void directoryTypesThatExportRemoteGroups()
    {
        assertTrue(mapper.isExportOfNonLocalGroupsRequired(DirectoryType.UNKNOWN));
        assertTrue(mapper.isExportOfNonLocalGroupsRequired(DirectoryType.INTERNAL));
        assertTrue(mapper.isExportOfNonLocalGroupsRequired(DirectoryType.CUSTOM));
        assertTrue(mapper.isExportOfNonLocalGroupsRequired(DirectoryType.CROWD));
    }

    @Test
    public void directoryTypesThatDoNotExportRemoteGroups()
    {
        assertFalse(mapper.isExportOfNonLocalGroupsRequired(DirectoryType.CONNECTOR));
        assertFalse(mapper.isExportOfNonLocalGroupsRequired(DirectoryType.DELEGATING));
    }

    private Directory directoryOfType(DirectoryType type)
    {
        Directory directory = mock(Directory.class);
        when(directory.getType()).thenReturn(type);
        return directory;
    }
}
