/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.console.action;

import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.crowd.directory.AbstractInternalDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.license.CrowdLicenseManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.xwork.XsrfTokenGenerator;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;
import com.atlassian.plugin.web.model.WebLink;
import com.atlassian.plugin.webresource.WebResourceManager;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.opensymphony.util.TextUtils;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.Action;
import com.opensymphony.xwork.ActionSupport;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.naming.CommunicationException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

public class BaseAction extends ActionSupport
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected Integer tab = 1;
    protected boolean initialLoad = true;

    /**
     * Webwork action messages color settings. Displays the type of message dressing to use.
     */
    protected String actionMessageAlertColor;

    /**
     * @see BaseAction#actionMessageAlertColor;
     */
    public static final String ALERT_GREEN = "green";

    /**
     * @see BaseAction#actionMessageAlertColor;
     */
    public static final String ALERT_YELLOW = "yellow";

    /**
     * @see BaseAction#actionMessageAlertColor;
     */
    public static final String ALERT_RED = "red";

    /**
     * @see BaseAction#actionMessageAlertColor;
     */
    public static final String ALERT_BLUE = "blue";

    // Spring wired dependencies
    protected PropertyManager propertyManager;
    protected DirectoryManager directoryManager;
    protected ApplicationManager applicationManager;
    protected CrowdLicenseManager crowdLicenseManager;
    private WebResourceManager webResourceManager;
    protected CrowdUserDetailsService crowdUserDetailsService;
    private WebInterfaceManager webInterfaceManager;
    private Integer currentLicenseResourceTotal = new Integer(0);
    private static final String DEFAULT_IMAGE_LOCATION = "/console/images/icons/16x16/16";
    private static final String ICON_IMAGE_TYPE = ".png";
    protected ApplicationService applicationService;
    protected TokenAuthenticationManager tokenAuthenticationManager;
    protected ClientProperties clientProperties;
    private XsrfTokenGenerator xsrfTokenGenerator;
    private Application crowdApplication;
    private ActionHelper actionHelper;

    public String doDefault() throws Exception
    {
        return Action.SUCCESS;
    }

    protected boolean hasAdminRole(CrowdUserDetails userDetails)
    {
        return actionHelper.hasAdminRole(userDetails);
    }

    public boolean isAuthenticated()
    {
        return actionHelper != null && actionHelper.isAuthenticated();
    }

    public boolean isAdmin()
    {
        return actionHelper != null && actionHelper.isAdmin();
    }

    public String getSitemeshPageProperty(String propertyName)
    {
        return actionHelper.getSitemeshPageProperty(propertyName);
    }

    public Map<String, String> getSitemeshPageProperties()
    {
        return actionHelper.getSitemeshPageProperties();
    }

    /**
     * @return username of the current logged in user, or <code>null</code> if the user is not authenticated
     * @throws InvalidUserException
     */
    public String getUserName() throws InvalidUserException
    {
        if (!isAuthenticated())
        {
            return null;
        }

        if (getRemoteUser() == null)
        {
            return StringUtils.EMPTY;
        }

        return StringUtils.isNotBlank(getRemoteUser().getDisplayName()) ?
               getRemoteUser().getDisplayName() : getRemoteUser().getName();
    }

    public UserWithAttributes getRemoteUser()
    {
        return actionHelper != null ? actionHelper.getRemoteUser() : null;
    }

    protected HttpSession getSession()
    {
        return ServletActionContext.getRequest().getSession();
    }

    protected HttpServletRequest getHttpRequest()
    {
        return ServletActionContext.getRequest();
    }

    public Integer getTab()
    {
        return tab;
    }

    public void setTab(Integer tab)
    {
        this.tab = tab;
    }

    public boolean isInitialLoad()
    {
        return initialLoad;
    }

    public CrowdLicense getLicense()
    {
        return actionHelper.getLicense();
    }

    public boolean isEvaluation()
    {
        return actionHelper != null && actionHelper.isEvaluation();
    }

    /**
     * @return {@code true} if the license is a subscription license, {@code false} otherwise.
     */
    public boolean isSubscription()
    {
        return actionHelper != null && actionHelper.isSubscription();
    }

    /**
     * @return {@code true} if the license
     */
    public boolean isWithinGracePeriod()
    {
        return actionHelper != null && actionHelper.isWithinGracePeriod();
    }

    public boolean isLicenseExpired()
    {
        return actionHelper != null && actionHelper.isLicenseExpired();
    }

    /**
     * Gets the action message color dressing to use with the decorator.
     *
     * @return The window dressing color to use.
     */
    public String getActionMessageAlertColor()
    {
        if (StringUtils.isEmpty(actionMessageAlertColor))
        {
            return ALERT_YELLOW;
        }
        else
        {
            return actionMessageAlertColor;
        }
    }

    /**
     * Sets a UI message and the color type for the user.
     *
     * @param color The color to use.
     * @param message The message to display.
     */
    public void addActionMessage(String color, String message)
    {
        actionMessageAlertColor = color;
        addActionMessage(message);
    }

    public List<WebItemModuleDescriptor> getWebItemsForSection(String sectionName)
    {
        return actionHelper.getWebItemsForSection(sectionName);
    }

    public List<WebSectionModuleDescriptor> getWebSectionsForLocation(String location)
    {
        return actionHelper.getWebSectionsForLocation(location);
    }

    public String renderFreemarkerTemplate(String templateString)
    {
        return webInterfaceManager.getWebFragmentHelper().renderVelocityFragment(templateString, getWebFragmentsContextMap());
    }

    public Map<String, Object> getWebFragmentsContextMap()
    {
        return actionHelper != null ? actionHelper.getWebFragmentsContextMap() : Collections.<String, Object>emptyMap();
    }

    public String getDisplayableLink(WebLink link)
    {
        return actionHelper.getDisplayableLink(link);
    }

    public String getLink(WebLink link)
    {
        // this method is to be used when we need to render links
        // from within views as opposed to decorators. if we built
        // the context map before the sitemesh properties were
        // set in context, then we incorrectly cache a context map
        // without sitemesh properties causing web links in the
        // decorator to display incorrectly
        return link.getDisplayableUrl(getHttpRequest(), Collections.EMPTY_MAP);
    }

    public Directory directory(long directoryID)
    {
        // TODO: why the hell do we have such crap usage..let's all put the world into base action..ftl
        try
        {
            return directoryManager.findDirectoryById(directoryID);
        }
        catch (Exception e)
        {
            logger.warn(e.getMessage(), e);
            addActionError(e.getMessage());
        }

        // webwork handles this fine...
        return null;
    }

    protected void addActionError(Throwable t)
    {
        if (isBadReferralException(t))
        {
            addActionError(getText("error.suggestion.badnodereferral"));
        }

        addActionError(t.getMessage());
    }

    static boolean isBadReferralException(Throwable e)
    {
        while (e != null)
        {
            if (isExceptionThrownInLdapReferralContext(e))
            {
                return true;
            }

            e = e.getCause();
        }

        return false;
    }

    private static boolean isExceptionThrownInLdapReferralContext(Throwable e)
    {
        if (e instanceof CommunicationException)
        {
            StackTraceElement[] st = e.getStackTrace();

            return (st.length >= 1 && st[0].getClassName().equals("com.sun.jndi.ldap.LdapReferralContext"));
        }
        else
        {
            return false;
        }
    }

    public boolean containsErrorMessages()
    {
        return getActionErrors() != null && !getActionErrors().isEmpty();
    }

    public boolean containsActionMessages()
    {
        return getActionMessages() != null && !getActionMessages().isEmpty();
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }

    public void setCrowdLicenseManager(CrowdLicenseManager crowdLicenseManager)
    {
        this.crowdLicenseManager = crowdLicenseManager;
    }

    public void setApplicationManager(ApplicationManager applicationManager)
    {
        this.applicationManager = applicationManager;
    }

    public void setCrowdUserDetailsService(CrowdUserDetailsService crowdUserDetailsService)
    {
        this.crowdUserDetailsService = crowdUserDetailsService;
    }

    public void setWebInterfaceManager(WebInterfaceManager webInterfaceManager)
    {
        this.webInterfaceManager = webInterfaceManager;
    }

    public WebInterfaceManager getWebInterfaceManager()
    {
        return webInterfaceManager;
    }

    public WebResourceManager getWebResourceManager()
    {
        return webResourceManager;
    }

    public void setWebResourceManager(WebResourceManager webResourceManager)
    {
        this.webResourceManager = webResourceManager;
    }

    protected CrowdBootstrapManager getBootstrapManager()
    {
        if (actionHelper != null)
        {
            return actionHelper.getBootstrapManager();
        }
        else
        {
            return (CrowdBootstrapManager) BootstrapUtils.getBootstrapManager();
        }
    }

    public boolean isAtResourceLimit()
    {
        return crowdLicenseManager.isResourceTotalOverLimit(90f, getCurrentLicenseResourceTotal().intValue());
    }

    public Integer getCurrentLicenseResourceTotal()
    {
        if (currentLicenseResourceTotal == null || currentLicenseResourceTotal.intValue() == 0)
        {
            // Don't calculate the resource total, just get the value present in the DB.
            currentLicenseResourceTotal = new Integer(propertyManager.getCurrentLicenseResourceTotal());
        }
        return currentLicenseResourceTotal;
    }

    public String getImageTitle(boolean active, ApplicationType applicationType)
    {
        String imageI8n = "application." + applicationType.toString().toLowerCase(Locale.ENGLISH) + (active ? ".enabled" : ".disabled");
        return getText(imageI8n);
    }

    public String getImageLocation(boolean active, ApplicationType applicationType)
    {
        return new StringBuilder().
                append(ServletActionContext.getRequest().getContextPath()).
                append(DEFAULT_IMAGE_LOCATION).
                append(applicationType.toString().toLowerCase(Locale.ENGLISH)).
                append(active ? "" : "_disabled").
                append(ICON_IMAGE_TYPE).toString();
    }

    protected boolean authorisedToAccessCrowdConsole(Long directoryId, String name)
            throws DirectoryNotFoundException, UserNotFoundException
    {
        List<Application> authorisedApplications = null;
        try
        {
            authorisedApplications = tokenAuthenticationManager.findAuthorisedApplications(directoryManager.findUserByName(directoryId, name), clientProperties.getApplicationName());
        }
        catch (OperationFailedException ex)
        {
            // Something horrible somewhere has happened - don't allow admin access.
            logger.error("Underlying directory implementation failed to execute the operation");
            return false;
        }
        catch (ApplicationNotFoundException e)
        {
            logger.error("Could not find the application", e);
            return false;
        }
        String crowdConsoleName = toLowerCase(getText("application.name")); // The name of the Crowd Console
        boolean authorised = false;
        for (int i = 0; i < authorisedApplications.size() && !authorised; i++)
        {
            Application application = authorisedApplications.get(i);
            if (application.getName().equals(crowdConsoleName))
            {
                authorised = true;
            }
        }
        return authorised;
    }

    public boolean authorisedToAccessCrowdAdminConsole(DirectoryMapping directoryMapping, Long directoryId, String username)
            throws DirectoryNotFoundException, UserNotFoundException
    {
        boolean allowAllToAuthenticate = directoryMapping.isAllowAllToAuthenticate();

        // Temporarily change allow all to auth to false to check crowd admin access
        directoryMapping.setAllowAllToAuthenticate(false);

        boolean hasAdminAccess = authorisedToAccessCrowdConsole(directoryId, username);

        // Set allow  all to authenticate back to original value
        directoryMapping.setAllowAllToAuthenticate(allowAllToAuthenticate);

        return hasAdminAccess;
    }

    public String getText(String i18nKey, List args, boolean encode)
    {
        if (encode)
        {
            args = Lists.transform(args, new Function()
            {
                public Object apply(Object from)
                {
                    return from instanceof String ? TextUtils.htmlEncode((String) from) : from;
                }
            });

        }

        return super.getText(i18nKey, args);
    }

    public String getXsrfTokenName()
    {
        return xsrfTokenGenerator.getXsrfTokenName();
    }

    public String getXsrfToken()
    {
        return xsrfTokenGenerator.generateToken(getHttpRequest());
    }

    @Override
    public String getText(String i18nKey, List args)
    {
        return getText(i18nKey, args, true);
    }

    public void setApplicationService(final ApplicationService applicationService)
    {
        this.applicationService = applicationService;
    }

    public void setTokenAuthenticationManager(TokenAuthenticationManager tokenAuthenticationManager)
    {
        this.tokenAuthenticationManager = tokenAuthenticationManager;
    }

    public void setClientProperties(ClientProperties clientProperties)
    {
        this.clientProperties = clientProperties;
    }

    public void setXsrfTokenGenerator(XsrfTokenGenerator xsrfTokenGenerator)
    {
        this.xsrfTokenGenerator = xsrfTokenGenerator;
    }

    /**
     * @param directoryId
     * @return the password complexity message for the specified directory, or null if there is none
     */
    @Nullable
    protected String getPasswordComplexityMessage(long directoryId)
    {
        Directory dir = directory(directoryId);

        if (dir != null)
        {
            String passwordComplexityRequirementMessage = dir.getValue(AbstractInternalDirectory.ATTRIBUTE_PASSWORD_COMPLEXITY_MESSAGE);
            if (!StringUtils.isBlank(passwordComplexityRequirementMessage))
            {
                return passwordComplexityRequirementMessage;
            }
        }
        return null;
    }

    protected Application getCrowdApplication() throws ApplicationNotFoundException
    {
        if (crowdApplication == null)
        {
            crowdApplication = applicationManager.findByName(clientProperties.getApplicationName());
        }
        return crowdApplication;
    }

    @VisibleForTesting
    public void setCrowdApplication(Application crowdApplication)
    {
        this.crowdApplication = crowdApplication;
    }

    public void setActionHelper(final ActionHelper actionHelper)
    {
        this.actionHelper = actionHelper;
    }
}
