package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Upgrades directories having InternalDirectoryWithBatching as an
 * implementation class to use InternalDirectory class as an implementation
 * class.
 *
 * @since v2.1
 */
public class UpgradeTask429 implements UpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask429.class);

    private final Collection<String> errors = new ArrayList<String>();

    private DirectoryDao directoryDao;

    public String getBuildNumber()
    {
        return "429";
    }

    public String getShortDescription()
    {
        return "Upgrading directories having InternalDirectoryWithBatching as an implementation class to use " + InternalDirectory.class.getSimpleName() + " as an implementation class.";
    }

    public void doUpgrade() throws Exception
    {
        for (Directory directory : findAllConnectorDirectories())
        {
            log.debug("Upgrading directory {}", directory);
            try
            {
                updateDirectory(directory);
            }
            catch (DataAccessException e)
            {
                final String errorMessage = "Could not update directory " + directory;
                log.error(errorMessage, e);
                errors.add(errorMessage + ", error is " + e.getMessage());
            }
        }
    }

    private void updateDirectory(Directory directory) throws DataAccessException, DirectoryNotFoundException
    {
        DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
        directoryToUpdate.setImplementationClass(InternalDirectory.class.getName());
        directoryDao.update(directoryToUpdate);
    }

    private List<Directory> findAllConnectorDirectories()
    {
        return directoryDao.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                .with(Restriction.on(DirectoryTermKeys.IMPLEMENTATION_CLASS).exactlyMatching("com.atlassian.crowd.directory.InternalDirectoryWithBatching"))
                .returningAtMost(EntityQuery.ALL_RESULTS));
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }
}
