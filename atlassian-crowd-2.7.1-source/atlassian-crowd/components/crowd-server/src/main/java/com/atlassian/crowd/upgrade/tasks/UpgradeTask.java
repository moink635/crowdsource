package com.atlassian.crowd.upgrade.tasks;

import java.util.Collection;

/**
 * An upgrade task for Crowd                          
 * <note>Please make sure when implementing an upgrade task that you write it in such
 * a way as it can be run again and not corrupt the Crowd data on a second, third run.</note>
 */
public interface UpgradeTask
{
    /**
     * @return The build number that this upgrade is applicable to.
     */
    public String getBuildNumber();

    /**
     * A short (<50 chars) description of the upgrade action
     *
     * @return description of upgrade task.
     */
    public String getShortDescription();

    /**
     * Perform the upgrade.
     *
     * @throws Exception is thrown if any errors occur during the upgrade process.
     */
    public void doUpgrade() throws Exception;

    /**
     * Any errors that occur during the upgrade process will be added to this Collection as Strings.
     *
     * @return a Collection<String> of errors. This may return an empty Collection.
     */
    public Collection<String> getErrors();
}
