package com.atlassian.crowd.manager.backup;

import com.google.common.base.Function;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;

import static com.atlassian.crowd.manager.backup.BackupFileConstants.AUTOMATED_BACKUP_FILENAME_PATTERN;
import static com.atlassian.crowd.manager.backup.BackupFileConstants.TIMESTAMP_FORMAT;

/**
* Transforms a {@link File} into a {@link Date}, or <code>null</code> if the file is not a valid automated backup file.
*
* Note: this class is stateful and not thread safe.
*
* @since v2.7
*/
public class FileToTimestampTransformer implements Function<File, Date>
{
    private static final Logger log = LoggerFactory.getLogger(FileToTimestampTransformer.class);

    private final SimpleDateFormat dateFormat = new SimpleDateFormat(TIMESTAMP_FORMAT);

    @Override
    public Date apply(final File file)
    {
        final String name = file.getName();
        final Matcher matcher = AUTOMATED_BACKUP_FILENAME_PATTERN.matcher(name);

        if (matcher.matches())
        {
            String timeStamp = matcher.group(1);
            try
            {
                return dateFormat.parse(timeStamp);
            }
            catch (ParseException e)
            {
                log.warn("Ignoring backup file with an invalid timestamp {}", name);
            }
        }

        return null;
    }
}
