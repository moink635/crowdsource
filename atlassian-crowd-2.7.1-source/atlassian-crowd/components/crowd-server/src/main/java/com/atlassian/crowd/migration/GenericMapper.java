package com.atlassian.crowd.migration;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.hibernate4.operation.ReplicateOperation;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.ReplicationMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class GenericMapper
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected static final String XML_DATE_FORMAT = "EEE MMM dd HH:mm:ss Z yyyy";
    protected static final String DATABASE_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    protected static final String PLUGIN_KEY_PREFIX = "plugin.";
    
    private final SessionFactory sessionFactory;
    private final BatchProcessor batchProcessor;

    public GenericMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor)
    {
        this.sessionFactory = sessionFactory;
        this.batchProcessor = batchProcessor;
    }

    /**
     * Serialises a date/time to String in a standard format.
     *
     * @param date date to serialise.
     * @return serialised date.
     */
    public String getDateAsFormattedString(Date date)
    {
        // Set all times in the XML to GMT
        SimpleDateFormat dateFormat = new SimpleDateFormat(XML_DATE_FORMAT);
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));

        return dateFormat.format(date);
    }

    /**
     * Obtains a date object from a serialised string.
     *
     * @param dateString serialised string.
     * @return date object.
     */
    public Date getDateFromXml(String dateString)
    {
        return getDateFromFormattedString(dateString, XML_DATE_FORMAT);
    }

    public Date getDateFromDatabase(String dateString)
    {
        return getDateFromFormattedString(dateString, DATABASE_DATE_FORMAT);
    }

    private Date getDateFromFormattedString(String dateString, String dateFormat)
    {
        Date date = null;
        if (StringUtils.isNotBlank(dateString))
        {
            try
            {
                date = new SimpleDateFormat(dateFormat).parse(dateString);
            }
            catch (ParseException e)
            {
                logger.error("Failed to parse date: " + dateString, e);
            }
        }
        return date;
    }

    private void commitUnderlyingConnection(Session session) throws HibernateException
    {
        session.doWork(new Work()
        {
            @Override
            public void execute(Connection connection) throws SQLException
            {
                connection.commit();
            }
        });
    }

    /**
     * Replicates a Hiberanate persistable entity.
     *
     * @param entityToPersist entity to replicate.
     */
    protected void addEntity(Object entityToPersist) throws ImportException
    {
        try
        {
            Session session = sessionFactory.getCurrentSession();
            if (session.isDirty())
            {
                session.flush();
                commitUnderlyingConnection(session);
                session.clear();
            }

            // persist
            session.replicate(entityToPersist, ReplicationMode.OVERWRITE);

            session.flush();
            commitUnderlyingConnection(session);
        }
        catch (HibernateException e)
        {
            throw new ImportException(e);
        }
    }

    /**
     * Replicates a collection of Hiberanate persistable entities using the batch processor.
     *
     * @param entitiesToPersist entity to replicate.
     */
    protected <T extends Serializable> void addEntities(List<T> entitiesToPersist) throws HibernateException
    {
        // use batch replication (w00t, 40 times faster!)
        BatchResult<T> result = batchProcessor.execute(new ReplicateOperation(ReplicationMode.OVERWRITE), entitiesToPersist);

        if (result.hasFailures())
        {
            // log failures
            for (T entity : result.getFailedEntities())
            {
                logger.error("Could not add " + entity.toString());
            }

            // this mimics the existing behaviour that when an
            // entity fails persistence, the entire xml import fails
            throw new HibernateException("Could not replicate the entire batch using the BatchProcessor.");
        }
    }

    /**
     * Replicates a Hiberanate persistable entity.
     *
     * @param entityToPersist entity to replicate.
     * @throws com.atlassian.crowd.migration.ImportException
     *          error persisting.
     */
    protected Object addEntityViaMerge(Object entityToPersist) throws ImportException
    {
        try
        {
            Session session = sessionFactory.getCurrentSession();
            if (session.isDirty())
            {
                session.flush();
                commitUnderlyingConnection(session);
                session.clear();
            }

            // persist (can't replicate because legacy stuff should not have an ID)
            session.merge(entityToPersist);

            session.flush();
            commitUnderlyingConnection(session);
        }
        catch (HibernateException e)
        {
            throw new ImportException(e);
        }

        return entityToPersist;
    }

    /**
     * Replicates a Hiberanate persistable entity.
     *
     * @param entityToPersist entity to replicate.
     * @throws com.atlassian.crowd.migration.ImportException
     *          error persisting.
     */
    protected Object addEntityViaSave(Object entityToPersist) throws ImportException
    {
        try
        {
            Session session = sessionFactory.getCurrentSession();
            if (session.isDirty())
            {
                session.flush();
                commitUnderlyingConnection(session);
                session.clear();
            }

            // persist (can't replicate because legacy stuff should not have an ID)
            session.save(entityToPersist);

            session.flush();
            commitUnderlyingConnection(session);
        }
        catch (HibernateException e)
        {
            throw new ImportException(e);
        }

        return entityToPersist;
    }

    public SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }

    protected OperationType getOperationTypeFromLegacyPermissionName(String name)
    {
        if (name.equals("principal.add"))
        {
            return OperationType.CREATE_USER;
        }
        else if (name.equals("group.add"))
        {
            return OperationType.CREATE_GROUP;
        }
        else if (name.equals("role.add"))
        {
            return OperationType.CREATE_ROLE;
        }
        else if (name.equals("principal.modify"))
        {
            return OperationType.UPDATE_USER;
        }
        else if (name.equals("group.modify"))
        {
            return OperationType.UPDATE_GROUP;
        }
        else if (name.equals("role.modify"))
        {
            return OperationType.UPDATE_ROLE;
        }
        else if (name.equals("principal.remove"))
        {
            return OperationType.DELETE_USER;
        }
        else if (name.equals("group.remove"))
        {
            return OperationType.DELETE_GROUP;
        }
        else if (name.equals("role.remove"))
        {
            return OperationType.DELETE_ROLE;
        }
        else
        {
            // TODO: currently role permissions are skipped
            return null;
        }
    }

    protected String getAttributeValue(String name, Map<String, Set<String>> attributes, String defaultValue)
    {
        Set<String> values = attributes.get(name);
        if (values != null && !values.isEmpty())
        {
            return values.iterator().next();
        }
        else
        {
            return defaultValue;
        }
    }

    protected DirectoryType getDirectoryTypeFromLegacyCode(int code)
    {
        switch (code)
        {
            case 0:
                return DirectoryType.UNKNOWN;
            case 1:
                return DirectoryType.INTERNAL;
            case 2:
                return DirectoryType.CONNECTOR;
            case 3:
                return DirectoryType.CUSTOM;
            case 4:
                return DirectoryType.DELEGATING;
            default:
                throw new IllegalArgumentException("Code is not valid");
        }
    }

    protected String getNameFromLegacyCode(String codeName)
    {
        int code = Integer.parseInt(codeName);

        switch (code)
        {
            case 1: return Property.CACHE_TIME;
            case 2: return null; // legacy SETUP_PROCESS_COMPLETE (unused)
            case 3: return null; // legacy LICENSE_KEY (unused)
            case 4: return Property.TOKEN_SEED;
            case 5: return null; // legacy
            case 6: return Property.DEPLOYMENT_TITLE;
            case 7: return Property.DOMAIN;
            case 8: return Property.CACHE_ENABLED;
            case 9: return Property.SESSION_TIME;
            case 10: return null; // legacy
            case 11: return Property.MAILSERVER_HOST;
            case 12: return Property.MAILSERVER_PREFIX;
            case 13: return Property.MAILSERVER_SENDER;
            case 14: return Property.MAILSERVER_USERNAME;
            case 15: return Property.MAILSERVER_PASSWORD;
            case 16: return Property.DES_ENCRYPTION_KEY;
            case 17: return Property.FORGOTTEN_PASSWORD_EMAIL_TEMPLATE;
            case 18: return null; // legacy LICENSE_HASH (unused)
            case 19: return null; // legacy LICENSE_MESSAGE (unused)
            case 20: return Property.CURRENT_LICENSE_RESOURCE_TOTAL;
            case 21: return Property.NOTIFICATION_EMAIL;
            case 22: return null; // legacy SERVER_ID;
            case 23: return Property.BUILD_NUMBER;
            case 24: return Property.GZIP_ENABLED;
            case 25: return Property.TRUSTED_PROXY_SERVERS;
            case 26: return Property.DATABASE_TOKEN_STORAGE_ENABLED;
            case 27: return Property.MAILSERVER_JNDI_LOCATION;
            case 28: return Property.MAILSERVER_PORT;
            case 29: return Property.SECURE_COOKIE;
            default: return null;
        }
    }
}
