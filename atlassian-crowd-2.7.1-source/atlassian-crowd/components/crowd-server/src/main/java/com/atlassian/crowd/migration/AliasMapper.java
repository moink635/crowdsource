package com.atlassian.crowd.migration;

import com.atlassian.crowd.dao.alias.AliasDAOHibernate;
import com.atlassian.crowd.dao.application.ApplicationDAOHibernate;
import com.atlassian.crowd.migration.legacy.XmlMapper;
import com.atlassian.crowd.model.alias.Alias;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class AliasMapper extends XmlMapper implements Mapper
{
    private final AliasDAOHibernate aliasDAO;
    private final ApplicationDAOHibernate applicationDAO;

    protected static final String ALIAS_XML_ROOT = "aliases";
    protected static final String ALIAS_XML_NODE = "alias";
    protected static final String ALIAS_APPLICATION_ID = "applicationId";
    protected static final String ALIAS_USERNAME = "username";
    protected static final String ALIAS_ALIASNAME = "aliasname";

    public AliasMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, AliasDAOHibernate aliasDAO, ApplicationDAOHibernate applicationDAO)
    {
        super(sessionFactory, batchProcessor);
        this.aliasDAO = aliasDAO;
        this.applicationDAO = applicationDAO;
    }

    public Element exportXml(final Map options) throws ExportException
    {
        Element aliasRoot = DocumentHelper.createElement(ALIAS_XML_ROOT);

        List<Alias> aliases = aliasDAO.findAll();

        for (Alias alias : aliases)
        {
            addAliasToXml(alias, aliasRoot);
        }

        return aliasRoot;
    }

    protected void addAliasToXml(final Alias alias, final Element aliasRoot)
    {
        Element aliasElement = aliasRoot.addElement(ALIAS_XML_NODE);
        aliasElement.addElement(GENERIC_XML_ID).addText(alias.getId().toString());
        aliasElement.addElement(ALIAS_APPLICATION_ID).addText(alias.getApplication().getId().toString());
        aliasElement.addElement(ALIAS_USERNAME).addText(alias.getName());
        aliasElement.addElement(ALIAS_ALIASNAME).addText(alias.getAlias());
    }

    public void importXml(final Element root) throws ImportException
    {
        Element aliasRoot = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + ALIAS_XML_ROOT);
        if (aliasRoot == null)
        {
            logger.error("No aliases were found for importing!");
            return;
        }

        for (Iterator aliases = aliasRoot.elementIterator(); aliases.hasNext();)
        {
            Element aliasElement = (Element) aliases.next();
            Alias alias = getAliasFromXml(aliasElement);
            addEntity(alias);
        }
    }

    protected Alias getAliasFromXml(final Element aliasElement)
    {
        Long id = new Long(aliasElement.element(GENERIC_XML_ID).getText());
        Long applicationId = new Long(aliasElement.element(ALIAS_APPLICATION_ID).getText());
        String username = aliasElement.elementText(ALIAS_USERNAME);
        String alias = aliasElement.elementText(ALIAS_ALIASNAME);

        ApplicationImpl application = (ApplicationImpl) applicationDAO.loadReference(applicationId);

        return new Alias(id, application, username, alias);
    }
}