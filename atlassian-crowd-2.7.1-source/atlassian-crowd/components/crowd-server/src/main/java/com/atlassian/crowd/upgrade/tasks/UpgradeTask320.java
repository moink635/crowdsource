package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationManagerException;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.util.I18nHelper;

import java.util.ArrayList;
import java.util.Collection;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

/**
 * Update the description and application type for the Crowd, Demo and OpenID applications.
 */
public class UpgradeTask320 implements UpgradeTask
{
    private Collection<String> errors = new ArrayList<String>();

    private ApplicationManager applicationManager;
    private I18nHelper i18nHelper;

    public String getBuildNumber()
    {
        return "320";
    }

    public String getShortDescription()
    {
        return "Updating application descriptions and types for Crowd's default applications";
    }

    public void doUpgrade() throws Exception
    {
        String crowdName = toLowerCase(i18nHelper.getText("application.name"));
        String crowdDesc = i18nHelper.getText("application.description");
        String demoName = i18nHelper.getText("demo.application.name");
        String demoDesc = i18nHelper.getText("demo.application.description");
        String openidName = i18nHelper.getText("crowdid.application.name");
        String openidDesc = i18nHelper.getText("crowdid.application.description");

        updateApplication(crowdName, crowdDesc, ApplicationType.CROWD, true);
        updateApplication(demoName, demoDesc, ApplicationType.GENERIC_APPLICATION, false);
        updateApplication(openidName, openidDesc, ApplicationType.GENERIC_APPLICATION, false);
    }

    private boolean updateApplication(String applicationName, String newDescription, ApplicationType newType, boolean required)
    {
        try
        {
            ApplicationImpl applicationImpl = ApplicationImpl.newInstance(applicationManager.findByName(applicationName));
            applicationImpl.setDescription(newDescription);
            applicationImpl.setType(newType);
            applicationManager.update(applicationImpl);

            return true;
        }
        catch (ApplicationNotFoundException e)
        {
            // application does not exist
            if (required)
            {
                errors.add("Could not find application to update: " + applicationName);
            }
            return false;
        }
        catch (ApplicationManagerException e)
        {
            // error updating
            errors.add("Could not update found application: " + applicationName);
            return false;
        }
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setApplicationManager(ApplicationManager applicationManager)
    {
        this.applicationManager = applicationManager;
    }

    public void setI18nHelper(I18nHelper i18nHelper)
    {
        this.i18nHelper = i18nHelper;
    }
}