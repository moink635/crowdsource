package com.atlassian.crowd.upgrade.tasks;

import java.util.ArrayList;
import java.util.Collection;

import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.embedded.spi.UserDao;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;

import com.google.common.collect.ImmutableList;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Updates users in internal directories to assign them an UUID.
 *
 * @since v2.7
 */
public class UpgradeTask623GenerateExternalId implements UpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask623GenerateExternalId.class);

    private final DirectoryDao directoryDao;
    private final UserDao userDao;

    private final Collection<String> errors = new ArrayList<String>();

    public UpgradeTask623GenerateExternalId(DirectoryDao directoryDao, UserDao userDao)
    {
        this.directoryDao = directoryDao;
        this.userDao = userDao;
    }

    @Override
    public String getBuildNumber()
    {
        return "623";
    }

    @Override
    public String getShortDescription()
    {
        return "Generates unique externalIds for users in internal directories";
    }

    private static final EntityQuery<Directory> INTERNAL_DIRECTORIES_QUERY = QueryBuilder
        .queryFor(Directory.class, EntityDescriptor.directory())
        .with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.INTERNAL))
        .returningAtMost(EntityQuery.ALL_RESULTS);

    @Override
    public void doUpgrade() throws Exception
    {
        for (Directory directory : directoryDao.search(INTERNAL_DIRECTORIES_QUERY))
        {
            log.debug("Generating externalIds for users in directory {}", directory.getId());
            EntityQuery<User> allUsersInDirectoryQuery =
                QueryBuilder.queryFor(User.class, EntityDescriptor.user()).returningAtMost(EntityQuery.ALL_RESULTS);
            for (User user : userDao.search(directory.getId(), allUsersInDirectoryQuery))
            {
                if (StringUtils.isBlank(user.getExternalId()))
                {
                    generateExternalId(user);
                }
            }
        }
    }

    private void generateExternalId(User user)
    {
        UserTemplate userTemplate = new UserTemplate(user);
        userTemplate.setExternalId(InternalDirectory.generateUniqueIdentifier());
        try
        {
            // in order to change the externalId, we must use the DAO, not the InternalDirectory
            userDao.update(userTemplate);
        }
        catch (UserNotFoundException e)
        {
            String errorMessage = "Could not update user " + user.getName() + " in directory " + user.getDirectoryId();
            log.error(errorMessage, e);
            errors.add(errorMessage + ", error is " + e.getMessage());
        }
    }

    @Override
    public Collection<String> getErrors()
    {
        return ImmutableList.copyOf(errors);
    }
}
