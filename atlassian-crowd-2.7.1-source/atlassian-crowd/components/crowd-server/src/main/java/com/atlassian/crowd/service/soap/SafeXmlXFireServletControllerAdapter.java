package com.atlassian.crowd.service.soap;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLResolver;

import org.codehaus.xfire.MessageContext;
import org.codehaus.xfire.XFire;
import org.codehaus.xfire.spring.remoting.XFireServletControllerAdapter;

public class SafeXmlXFireServletControllerAdapter extends XFireServletControllerAdapter
{
    public SafeXmlXFireServletControllerAdapter(XFire xfire, ServletContext context, QName name)
    {
        super(xfire, context, name);
    }

    private static final InputStream EMPTY_INPUT_STREAM = new ByteArrayInputStream(new byte[0]);

    private static final XMLResolver EMPTY_RESOLVER = new XMLResolver()
    {
        @Override
        public Object resolveEntity(String publicID, String systemID, String baseURI, String namespace)
        {
            return EMPTY_INPUT_STREAM;
        }
    };

    @Override
    protected MessageContext createMessageContext(HttpServletRequest request, HttpServletResponse response,
            String service)
    {
        MessageContext mc = super.createMessageContext(request, response, service);

        XMLInputFactory fac = XMLInputFactory.newInstance();
        fac.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
        fac.setXMLResolver(EMPTY_RESOLVER);
        mc.setProperty(XFire.STAX_INPUT_FACTORY, fac);

        return mc;
    }
}
