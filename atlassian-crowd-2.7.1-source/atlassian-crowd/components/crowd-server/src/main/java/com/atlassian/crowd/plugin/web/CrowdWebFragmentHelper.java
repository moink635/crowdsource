package com.atlassian.crowd.plugin.web;

import java.util.List;
import java.util.Map;

import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.web.Condition;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.web.WebFragmentHelper;
import com.atlassian.plugin.web.conditions.ConditionLoadingException;

import com.opensymphony.util.TextUtils;
import com.opensymphony.xwork.ActionContext;
import com.opensymphony.xwork.util.LocalizedTextUtil;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * Crowd specific implementation of the Atlassian Plugin {@link WebFragmentHelper}
 */
public class CrowdWebFragmentHelper implements WebFragmentHelper, ApplicationContextAware
{
    private ApplicationContext applicationContext;

    private I18nHelper i18nHelper;

    private TemplateRenderer templateRenderer;

    public Condition loadCondition(String className, Plugin plugin) throws ConditionLoadingException
    {
        return (Condition) loadComponent(className, plugin);
    }

    private Object loadComponent(String className, Plugin plugin) throws ConditionLoadingException
    {
        Class<?> clz;

        try
        {
            clz = plugin.loadClass(className, this.getClass());
        }
        catch (ClassNotFoundException e)
        {
            throw new ConditionLoadingException(e);
        }

        try
        {
            if (plugin instanceof ContainerManagedPlugin)
            {
                return ((ContainerManagedPlugin) plugin).getContainerAccessor().createBean(clz);
            }
            else
            {
                return applicationContext.getAutowireCapableBeanFactory().createBean(clz, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
            }
        }
        catch (Exception e)
        {
            throw new ConditionLoadingException(e);
        }
    }

    public ContextProvider loadContextProvider(String className, Plugin plugin) throws ConditionLoadingException
    {
        return (ContextProvider) loadComponent(className, plugin);
    }

    public String getI18nValue(String key, List arguments, Map context)
    {
        String labelValue = LocalizedTextUtil.findDefaultText(key, ActionContext.getContext().getLocale());

        if (StringUtils.isEmpty(labelValue))
        {
            labelValue = i18nHelper.getText(key, arguments);
        }
        else
        {
            return key;
        }

        return labelValue;
    }

    public String renderVelocityFragment(String fragment, Map context)
    {
        if (!needToRender(fragment))
        {
            return fragment;
        }
        String parsedResult = null;
        parsedResult = templateRenderer.renderText(fragment, context);
        return parsedResult;
    }

    private boolean needToRender(String fragment)
    {
        return (TextUtils.stringSet(fragment) && (fragment.indexOf("$") >= 0 || fragment.indexOf("#") >= 0));
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
        this.applicationContext = applicationContext;
    }

    public void setI18nHelper(I18nHelper i18nHelper)
    {
        this.i18nHelper = i18nHelper;
    }

    public void setTemplateRenderer(TemplateRenderer templateRenderer)
    {
        this.templateRenderer = templateRenderer;
    }
}
