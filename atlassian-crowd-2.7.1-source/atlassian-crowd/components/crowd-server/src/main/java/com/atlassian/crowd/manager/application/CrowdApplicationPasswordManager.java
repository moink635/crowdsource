package com.atlassian.crowd.manager.application;

import com.atlassian.crowd.exception.ApplicationNotFoundException;

public interface CrowdApplicationPasswordManager
{
    /**
     * Resets both the Crowd application's password in the database
     * and application.password in crowd.properties.
     *
     * @throws ApplicationNotFoundException Crowd application does not exist in the database.
     * @throws ApplicationManagerException error updating credentials.
     */
    void resetCrowdPasswordIfRequired() throws ApplicationManagerException, ApplicationNotFoundException;
}
