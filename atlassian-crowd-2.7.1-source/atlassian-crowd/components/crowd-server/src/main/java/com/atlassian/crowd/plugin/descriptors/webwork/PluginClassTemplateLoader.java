package com.atlassian.crowd.plugin.descriptors.webwork;

import com.opensymphony.xwork.ActionContext;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.config.entities.ActionConfig;
import freemarker.cache.URLTemplateLoader;

import java.net.URL;

public class PluginClassTemplateLoader extends URLTemplateLoader
{
    protected URL getURL(String name)
    {
        ActionInvocation inv = ActionContext.getContext().getActionInvocation();
        if (inv != null)
        {
            ActionConfig config = inv.getProxy().getConfig();
            if (config instanceof PluginAwareActionConfig)
            {
                PluginAwareActionConfig cfg = (PluginAwareActionConfig) config;
                return cfg.getPlugin().getResource(name);
            }
        }

        return null;
    }
}
