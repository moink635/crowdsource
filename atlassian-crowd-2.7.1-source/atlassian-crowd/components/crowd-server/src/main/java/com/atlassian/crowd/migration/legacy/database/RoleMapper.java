package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.dao.membership.MembershipDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.LegacyImportDataHolder;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.group.InternalGroupWithAttributes;
import com.atlassian.crowd.model.membership.InternalMembership;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchResultWithIdReferences;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RoleMapper extends DatabaseMapper implements DatabaseImporter
{

    private final GroupDAOHibernate groupDAO;
    private final MembershipDAOHibernate membershipDAO;
    private final DirectoryDAOHibernate directoryDAO;

    public RoleMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, JdbcOperations jdbcTemplate, GroupDAOHibernate groupDAO, MembershipDAOHibernate membershipDAO, DirectoryDAOHibernate directoryDAO)
    {
        super(sessionFactory, batchProcessor, jdbcTemplate);

        this.groupDAO = groupDAO;
        this.membershipDAO = membershipDAO;
        this.directoryDAO = directoryDAO;
    }

    public void importFromDatabase(LegacyImportDataHolder importData) throws ImportException
    {
        List<InternalGroupWithAttributes> roles = importRolesFromDatabase(importData.getOldToNewDirectoryIds());

        BatchResultWithIdReferences<Group> roleImportResults = groupDAO.addAll(roles);
        for (Group role : roleImportResults.getFailedEntities())
        {
            logger.error("Unable to add role <" + role.getName() + "> in directory with id <" + role.getDirectoryId() + ">");
        }

        // die hard if there are any errors
        if (roleImportResults.hasFailures())
        {
            throw new ImportException("Unable to import all roles. See logs for more details.");
        }

        // now import memberships (role by role)
        Set<InternalMembership> memberships = importMembershipsFromDatabase(importData, roleImportResults);
        BatchResult<InternalMembership> membershipResult = membershipDAO.addAll(memberships);
        for (InternalMembership membership : membershipResult.getFailedEntities())
        {
            logger.error("Unable to add user <" + membership.getChildName() + "> to role <" + membership.getParentName() + "> in directory with id <" + membership.getDirectory().getId() + ">");
        }

        if (membershipResult.hasFailures())
        {
            throw new ImportException("Unable to import all memberships. See logs for more details.");
        }

        logger.info("Successfully migrated " + roleImportResults.getTotalSuccessful() + " roles, with " + memberships.size() + " memberships.");
    }

    protected List<InternalGroupWithAttributes> importRolesFromDatabase(final Map<Long, Long> oldToNewDirectoryIds)
    {
        List<InternalGroupWithAttributes> internalRolesWithAttributes = new ArrayList<InternalGroupWithAttributes>();

        RoleTableMapper roleTableMapper = new RoleTableMapper(oldToNewDirectoryIds);
        Map<InternalGroup, Long> roleToOldDirectoryId = roleTableMapper.getRoleToOldDirectoryIds();
        List<InternalGroup> roles = getRoles(roleTableMapper);

        // Get all the role attributes in one database call.
        Map<EntityIdentifier, Map<String, Set<String>>> allRoleAttributes = getRoleAttributes();

        for (InternalGroup role : roles)
        {
            EntityIdentifier currentRole = new EntityIdentifier(roleToOldDirectoryId.get(role), role.getName());
            Map<String, Set<String>> attributes = allRoleAttributes.get(currentRole);

            if (attributes == null)
            {
                attributes = new HashMap<String, Set<String>>();
            }
            internalRolesWithAttributes.add(new InternalGroupWithAttributes(role, attributes));
        }
        return internalRolesWithAttributes;
    }

    private List<InternalGroup> getRoles(RoleTableMapper roleTableMapper)
    {
        return jdbcTemplate.query(legacyTableQueries.getRolesSQL(), roleTableMapper);
    }

    private Map<EntityIdentifier, Map<String, Set<String>>> getRoleAttributes()
    {
        AttributeMapper roleAttributeMapper = new AttributeMapper("REMOTEROLEDIRECTORYID", "REMOTEROLENAME");
        jdbcTemplate.query(legacyTableQueries.getRoleAttributesSQL(), roleAttributeMapper);
        roleAttributeMapper.getEntityAttributes();

        Map<EntityIdentifier, List<Map<String, String>>> roleAttributes = roleAttributeMapper.getEntityAttributes();

        // Convert attributes from list of maps to a map.
        Map<EntityIdentifier, Map<String, Set<String>>> roleAttributeMap = new HashMap<EntityIdentifier, Map<String, Set<String>>>();
        for (Map.Entry<EntityIdentifier, List<Map<String, String>>> entry : roleAttributes.entrySet())
        {
            roleAttributeMap.put(entry.getKey(), attributeListToMultiAttributeMap(entry.getValue()));
        }
        return roleAttributeMap;
    }

    protected Set<InternalMembership> importMembershipsFromDatabase(LegacyImportDataHolder importData, BatchResultWithIdReferences<Group> roleImportResults)
    {
        MembershipTableMapper membershipTableMapper = new MembershipTableMapper(importData, roleImportResults);
        List<InternalMembership> memberships = jdbcTemplate.query(legacyTableQueries.getRoleMembershipsSQL(), membershipTableMapper);
        return new HashSet<InternalMembership>(memberships);
    }

    private class RoleTableMapper implements RowMapper
    {
        private final Map<Long, Long> oldToNewDirectoryIds;
        private final Map<InternalGroup, Long> roleToOldDirectoryIds = new HashMap<InternalGroup, Long>();

        public RoleTableMapper(Map<Long, Long> oldToNewDirectoryIds)
        {
            this.oldToNewDirectoryIds = oldToNewDirectoryIds;
        }

        public Object mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            Long oldDirectoryId = rs.getLong("DIRECTORYID");
            String roleName = rs.getString("NAME");
            boolean active = rs.getBoolean("ACTIVE");
            Date createDate = getDateFromDatabase(rs.getString("CONCEPTION"));
            Date updatedDate = getDateFromDatabase(rs.getString("LASTMODIFIED"));
            String description = rs.getString("DESCRIPTION");
            GroupType groupType = GroupType.LEGACY_ROLE;

            // check the old directoryId
            Long directoryId = oldToNewDirectoryIds.get(oldDirectoryId);
            if (directoryId == null)
            {
                throw new IllegalArgumentException("Role belongs to an unknown old directory with ID: " + directoryId);
            }

            Directory directory = (Directory) directoryDAO.loadReference(directoryId);

            // put in the default values
            InternalEntityTemplate internalEntityTemplate = createInternalEntityTemplate(directoryId, roleName, createDate, updatedDate, active);
            internalEntityTemplate.setId(null);

            // create the role
            GroupTemplate roleTemplate = new GroupTemplate(internalEntityTemplate.getName(), directoryId, groupType);
            roleTemplate.setActive(internalEntityTemplate.isActive());
            roleTemplate.setDescription(description);

            // Save the role
            InternalGroup role = new InternalGroup(internalEntityTemplate, directory, roleTemplate);
            roleToOldDirectoryIds.put(role, oldDirectoryId);

            return role;
        }

        public Map<InternalGroup, Long> getRoleToOldDirectoryIds()
        {
            return roleToOldDirectoryIds;
        }
    }

    private class MembershipTableMapper implements RowMapper
    {
        private final LegacyImportDataHolder importData;
        private final BatchResultWithIdReferences roleImportResults;

        public MembershipTableMapper(LegacyImportDataHolder importData, BatchResultWithIdReferences roleImportResults)
        {
            this.importData = importData;
            this.roleImportResults = roleImportResults;
        }

        public Object mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            String parentName = rs.getString("REMOTEROLENAME");
            String childName = rs.getString("REMOTEPRINCIPALNAME");
            Long oldDirectoryId = rs.getLong("REMOTEROLEDIRECTORYID");

            Long directoryId = importData.getOldToNewDirectoryIds().get(oldDirectoryId);
            if (directoryId == null)
            {
                throw new IllegalArgumentException("Role belongs to an unknown old directory with ID: " + directoryId);
            }

            DirectoryImpl directory = (DirectoryImpl) directoryDAO.loadReference(directoryId);
            Long roleId = roleImportResults.getIdReference(directoryId, parentName);
            Long userId = importData.getUserImportResults().getIdReference(directoryId, childName);

            return new InternalMembership(null, roleId, userId, MembershipType.GROUP_USER, GroupType.LEGACY_ROLE, parentName, childName, directory);
        }
    }
}
