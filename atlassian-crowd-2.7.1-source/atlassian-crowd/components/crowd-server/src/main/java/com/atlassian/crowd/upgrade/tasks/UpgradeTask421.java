package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This upgrade task will enable auto-create on auth for existing delegated authentication directories.
 */
public class UpgradeTask421 implements UpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask421.class);

    private final Collection<String> errors = new ArrayList<String>();

    private DirectoryDao directoryDao;

    public String getBuildNumber()
    {
        return "421";
    }

    public String getShortDescription()
    {
        return "Upgrading Delegating Authentication Directories that don't have auto-create-on-auth flags";
    }

    public void doUpgrade() throws Exception
    {
        for (Directory directory : findAllDelegatingAuthenticationDirectories())
        {
            if (needsUpgrade(directory))
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Upgrading directory " + directory);
                }
                try
                {
                    updateDirectory(directory);
                }
                catch (DataAccessException e)
                {
                    final String errorMessage = "Could not update directory " + directory;
                    log.error(errorMessage, e);
                    errors.add(errorMessage + ", error is " + e.getMessage());
                }
            }
            else
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Directory " + directory + " will not be upgraded.");
                }
            }
        }
    }

    private void updateDirectory(Directory directory) throws DataAccessException, DirectoryNotFoundException
    {
        DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
        directoryToUpdate.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH, Boolean.TRUE.toString());
        directoryDao.update(directoryToUpdate);
    }

    private boolean needsUpgrade(Directory directory)
    {
        return directory.getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_CREATE_USER_ON_AUTH) == null;
    }

    public List<Directory> findAllDelegatingAuthenticationDirectories()
    {
        return directoryDao.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                .with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.DELEGATING))
                .returningAtMost(EntityQuery.ALL_RESULTS));
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }
}
