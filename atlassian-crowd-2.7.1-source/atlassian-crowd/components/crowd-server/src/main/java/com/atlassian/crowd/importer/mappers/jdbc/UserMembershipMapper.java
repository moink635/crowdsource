package com.atlassian.crowd.importer.mappers.jdbc;

import com.atlassian.crowd.importer.model.MembershipDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Will map a row from a JDBC {@link ResultSet} to a {@link MembershipDTO}.
 */
public class UserMembershipMapper implements RowMapper
{
    private final String username;
    private final String groupname;

    public UserMembershipMapper(final String username, final String groupname)
    {
        this.username = username;
        this.groupname = groupname;
    }

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        String user = rs.getString(username);
        String group = rs.getString(groupname);

        // jdbc import currently does not support nested groups.
        return new MembershipDTO(MembershipDTO.ChildType.USER, user, group);
    }
}
