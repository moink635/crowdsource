package com.atlassian.crowd.plugin;

import com.atlassian.crowd.util.build.BuildUtils;
import com.atlassian.plugin.Application;

/**
 * Required by the Plugin System
 */
public class CrowdApplication implements Application
{
    private final String key;

    public CrowdApplication(String key)
    {
        this.key = key;
    }

    @Override
    public String getKey()
    {
        return key;
    }

    @Override
    public String getVersion()
    {
        return BuildUtils.BUILD_VERSION;
    }

    @Override
    public String getBuildNumber()
    {
        return BuildUtils.BUILD_NUMBER;
    }
}
