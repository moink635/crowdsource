package com.atlassian.crowd.service;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.manager.application.ApplicationAccessDeniedException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.crowd.model.user.UserTemplate;

import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static com.google.common.base.Preconditions.checkNotNull;

public class UserServiceImpl implements UserService
{
    private final ApplicationManager applicationManager;
    private final ApplicationService applicationService;
    private final TokenAuthenticationManager tokenAuthenticationManager;
    private final CrowdUserDetailsService crowdUserDetailsService;

    public UserServiceImpl(final ApplicationManager applicationManager, final ApplicationService applicationService, final CrowdUserDetailsService crowdUserDetailsService, TokenAuthenticationManager tokenAuthenticationManager)
    {
        this.applicationManager = applicationManager;
        this.applicationService = applicationService;
        this.crowdUserDetailsService = crowdUserDetailsService;
        this.tokenAuthenticationManager = tokenAuthenticationManager;
    }

    @Override
    public String getRemoteUsername()
    {
        String username = null;

        CrowdUserDetails userDetails = getCrowdUserDetails();
        if (userDetails != null)
        {
            username = userDetails.getUsername();
        }

        return username;
    }

    private CrowdUserDetails getCrowdUserDetails()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null && !(auth instanceof AnonymousAuthenticationToken) && auth.getPrincipal() != null && (auth.getPrincipal() instanceof CrowdUserDetails))
        {
            return (CrowdUserDetails) auth.getPrincipal();
        }
        return null;
    }

    @Override
    public String getAuthenticatedUsername(final HttpServletRequest request)
    {
        return getRemoteUsername();
    }

    @Override
    public boolean isUserInGroup(final String username, final String group)
    {

        Application application = getCrowdApplication();

        return applicationService.isUserNestedGroupMember(application, username, group);
    }

    private Application getCrowdApplication()
    {
        Application application;
        try
        {
            application = applicationManager.findByName(ApplicationType.CROWD.getDisplayName());
        }
        catch (ApplicationNotFoundException e)
        {
            throw new RuntimeException("Failed to find Crowd application for service, with name: <" + ApplicationType.CROWD.getDisplayName() + ">", e);
        }
        return application;
    }

    @Override
    public boolean isSystemAdmin(final String username) throws DataAccessException
    {
        final CrowdUserDetails userDetails;
        try
        {
            userDetails = crowdUserDetailsService.loadUserByUsername(username);

            String adminRole = crowdUserDetailsService.getAdminAuthority();
            for (GrantedAuthority grantedAuthority : userDetails.getAuthorities())
            {
                if (grantedAuthority.getAuthority().equals(adminRole))
                {
                    return true;
                }
            }

        }
        catch (UsernameNotFoundException e)
        {
            // do nothing just return false;
        }

        return false;
    }

    @Override
    public boolean authenticate(final String username, final String password)
    {
        UserAuthenticationContext context = buildUserAuthenticationContext(username, password);

        try
        {
            final Token token =
                tokenAuthenticationManager.authenticateUser(context, TokenLifetime.USE_DEFAULT);

            //TODO why was I checking this for null? Justin.
            if (token.getRandomHash() != null)
            {
                return true;
            }
        }
        catch (InvalidAuthenticationException e)
        {
            return false;
        }
        catch (OperationFailedException e)
        {
            throw new RuntimeException("Failed to authenticate user due to: " + e.getMessage(), e);
        }
        catch (InactiveAccountException e)
        {
            return false;
        }
        catch (ApplicationAccessDeniedException e)
        {
            return false;
        }
        catch (ExpiredCredentialException e)
        {
            return false;
        }
        catch (ApplicationNotFoundException e)
        {
            return false;
        }

        return false;
    }

    @Override
    public Principal resolve(final String username) throws DataAccessException
    {
        final CrowdUserDetails userDetails = crowdUserDetailsService.loadUserByUsername(username);

        if (userDetails != null)
        {
            final UserTemplate userTemplate = new UserTemplate(userDetails.getUsername(), userDetails.getFirstName(), userDetails.getLastName(), userDetails.getFullName());
            userTemplate.setEmailAddress(userDetails.getEmail());
            return userTemplate;
        }
        else
        {
            // As per interface
            return null;
        }
    }

    @Override
    public boolean setAuthenticatedUser(final String username) throws UsernameNotFoundException, DataAccessException
    {
        checkNotNull(username, "username should not be null");

        try
        {
            final CrowdUserDetails userDetails = crowdUserDetailsService.loadUserByUsername(username);
            // Use the 3-argument constructor to generate an authenticated token
            final Authentication token = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(token);

            return true;
        }
        catch (UsernameNotFoundException e)
        {
            return false;
        }
        catch (DataAccessException e)
        {
            return false;
        }
    }

    private UserAuthenticationContext buildUserAuthenticationContext(final String username, final String password)
    {
        UserAuthenticationContext context = new UserAuthenticationContext();
        context.setApplication(ApplicationType.CROWD.getDisplayName());
        context.setName(username);
        context.setCredential(new PasswordCredential(password));
        context.setValidationFactors(new ValidationFactor[0]);
        return context;
    }
}
