package com.atlassian.crowd.plugin.web.conditions;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Will return true if a context contains any of the available property values based on a
 * given property name.
 */
public class SitemeshPropertiesCondition implements Condition
{
    private List<String> propertyValues = null;
    private String propertyName = null;

    public void init(Map params) throws PluginParseException
    {
        this.propertyName = (String) params.get("propertyName");
        this.propertyValues = Arrays.asList(((String) params.get("propertyValues")).split(","));

    }

    public boolean shouldDisplay(Map context)
    {
        String value = (String) context.get(propertyName);

        return propertyValues.contains(value);
    }
}
