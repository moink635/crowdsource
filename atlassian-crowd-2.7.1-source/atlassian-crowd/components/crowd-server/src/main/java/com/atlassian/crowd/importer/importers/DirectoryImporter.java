package com.atlassian.crowd.importer.importers;

import java.util.Collection;
import java.util.List;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.config.DirectoryConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.model.MembershipDTO;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import com.google.common.base.Function;
import com.google.common.collect.Collections2;
import com.google.common.collect.ImmutableSet;

/**
 * An Importer that manages moving users, groups and roles from one directory to another.
 */
public class DirectoryImporter extends BaseImporter
{
    public DirectoryImporter(DirectoryManager directoryManager)
    {
        super(directoryManager);
    }

    @Override
    public Collection<MembershipDTO> findUserToGroupMemberships(final Configuration configuration) throws ImporterException
    {
        final DirectoryConfiguration directoryConfiguration = (DirectoryConfiguration) configuration;
        final Collection<GroupTemplate> groups = findGroups(configuration);
        final ImmutableSet.Builder<MembershipDTO> setBuilder = new ImmutableSet.Builder<MembershipDTO>();

        try
        {
            for (final Group group : groups)
            {
                List<String> members = directoryManager.searchDirectGroupRelationships(directoryConfiguration.getSourceDirectoryID(), QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group()).withName(group.getName()).returningAtMost(EntityQuery.ALL_RESULTS));
                setBuilder.addAll(Collections2.transform(members, new MemberToMembershipMapper(group.getName(), MembershipDTO.ChildType.USER)));
            }
        }
        catch (Exception e)
        {
            throw new ImporterException(e);
        }

        return setBuilder.build();
    }

    @Override
    public Collection<MembershipDTO> findGroupToGroupMemberships(Configuration configuration) throws ImporterException
    {
        if (!configuration.getImportNestedGroups())
        {
            throw new IllegalStateException("findGroupToGroupMemberships cannot be called given nested group is not supported");
        }

        final DirectoryConfiguration directoryConfiguration = (DirectoryConfiguration) configuration;
        final Collection<GroupTemplate> groups = findGroups(configuration);
        final ImmutableSet.Builder<MembershipDTO> setBuilder = new ImmutableSet.Builder<MembershipDTO>();

        try
        {
            for (final Group group : groups)
            {
                List<String> members = directoryManager.searchDirectGroupRelationships(directoryConfiguration.getSourceDirectoryID(), QueryBuilder.queryFor(String.class, EntityDescriptor.group()).childrenOf(EntityDescriptor.group()).withName(group.getName()).returningAtMost(EntityQuery.ALL_RESULTS));
                setBuilder.addAll(Collections2.transform(members, new MemberToMembershipMapper(group.getName(), MembershipDTO.ChildType.GROUP)));
            }
        }
        catch (Exception e)
        {
            throw new ImporterException(e);
        }

        return setBuilder.build();
    }

    @Override
    public Collection<GroupTemplate> findGroups(final Configuration configuration) throws ImporterException
    {
        final DirectoryConfiguration directoryConfiguration = (DirectoryConfiguration) configuration;

        final ImmutableSet.Builder<GroupTemplate> setBuilder = new ImmutableSet.Builder<GroupTemplate>();

        try
        {
            Collection<Group> groups = directoryManager.searchGroups(directoryConfiguration.getSourceDirectoryID(), QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).returningAtMost(EntityQuery.ALL_RESULTS));

            // Disassociate the group to the current directory
            for (Group group : groups)
            {
                GroupTemplate groupTemplate = new GroupTemplate(group);
                setBuilder.add(groupTemplate);
            }
        }
        catch (Exception e)
        {
            throw new ImporterException(e);
        }


        return setBuilder.build();
    }

    @Override
    public Collection<UserTemplateWithCredentialAndAttributes> findUsers(final Configuration configuration) throws ImporterException
    {
        final DirectoryConfiguration directoryConfiguration = (DirectoryConfiguration) configuration;

        final ImmutableSet.Builder<UserTemplateWithCredentialAndAttributes> setBuilder = new ImmutableSet.Builder<UserTemplateWithCredentialAndAttributes>();

        try
        {
            Collection<User> users = directoryManager.searchUsers(directoryConfiguration.getSourceDirectoryID(), QueryBuilder.queryFor(User.class, EntityDescriptor.user()).returningAtMost(EntityQuery.ALL_RESULTS));

            for (User user : users)
            {
                PasswordCredential credential = null;
                if (user instanceof InternalUser)
                {
                    // can extract password
                    credential = ((InternalUser) user).getCredential();
                }

                if (credential == null)
                {
                    credential = PasswordCredential.NONE;
                }

                setBuilder.add(new UserTemplateWithCredentialAndAttributes(user, credential));
            }
        }
        catch (Exception e)
        {
            throw new ImporterException(e);
        }

        return setBuilder.build();
    }

    ///CLOVER:OFF
    @Override
    public void init(Configuration configuration)
    {
        // do nothing, nothing to initialise
    }

    @Override
    public Class getConfigurationType()
    {
        return DirectoryConfiguration.class;
    }

    private class MemberToMembershipMapper implements Function<String, MembershipDTO>
    {
        private final String parent;
        private final MembershipDTO.ChildType childType;

        MemberToMembershipMapper(String parent, MembershipDTO.ChildType childType)
        {
            this.parent = parent;
            this.childType = childType;
        }

        // maps member to membership
        @Override
        public MembershipDTO apply(String member)
        {
            return new MembershipDTO(childType, member, parent);
        }
    }
}
