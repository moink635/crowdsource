package com.atlassian.crowd.importer.config;

import com.atlassian.crowd.importer.exceptions.ImporterConfigurationException;
import org.apache.commons.collections.OrderedBidiMap;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

/**
 * This configuration type will encapsulate all required elements
 * to import users/groups and their memberships into Crowd from
 * two CSV files
 */
public class CsvConfiguration extends Configuration implements Serializable
{
    // User mappings
    public static final String USER_PREFIX = "user.";
    public static final String USER_FIRSTNAME = USER_PREFIX + "firstname";
    public static final String USER_LASTNAME = USER_PREFIX + "lastname";
    public static final String USER_EMAILADDRESS = USER_PREFIX + "emailaddress";
    public static final String USER_USERNAME = USER_PREFIX + "username";
    public static final String USER_PASSWORD = USER_PREFIX + "password";
    public static final String USER_NONE = USER_PREFIX + "none";

    // Group Mappings
    public static final String GROUP_PREFIX = "group.";
    public static final String GROUP_NAME = GROUP_PREFIX + "name";
    public static final String GROUP_USERNAME = GROUP_PREFIX + "username";
    public static final String GROUP_NONE = GROUP_PREFIX + "none";

    private File users;
    private File groupMemberships;
    private Character delimiter;
    private Boolean encryptPasswords = Boolean.TRUE;
    private OrderedBidiMap userMappingConfiguration;
    private OrderedBidiMap groupMappingConfiguration;
    private List userHeaderRow;
    private List userSampleRow;
    private List groupHeaderRow;
    private List groupSampleRow;

    private static final Logger logger = LoggerFactory.getLogger(CsvConfiguration.class);

    public CsvConfiguration()
    {
    }

    public CsvConfiguration(Long directoryID, String application, Boolean importPasswords, File users, File groupMemberships, Character delimiter, Boolean encryptPasswords)
    {
        super(directoryID, application, importPasswords, Boolean.FALSE);
        this.users = users;
        this.groupMemberships = groupMemberships;
        this.delimiter = delimiter;
        this.encryptPasswords = encryptPasswords;

        // Build the user sample mappings
        buildSampleUserMapping(users);

        // Build the group sample mappings
        buildSampleGroupMapping(groupMemberships);

    }

    protected void buildSampleUserMapping(File csvDataFile)
    {
        if (csvDataFile != null && getDelimiter() != null)
        {
            try
            {
                CSVStrategy strategy = CSVStrategy.EXCEL_STRATEGY;
                strategy.setDelimiter(getDelimiter().charValue());
                CSVParser csvParser = new CSVParser(new FileReader(csvDataFile), strategy);

                // Populate the Header and sample first row

                // The header Row
                String[] dataRow = csvParser.getLine();
                if (dataRow != null && dataRow.length > 0)
                {
                    userHeaderRow = Arrays.asList(dataRow);
                }

                //The sample row
                dataRow = csvParser.getLine();
                if (dataRow != null && dataRow.length > 0)
                {
                    userSampleRow = Arrays.asList(dataRow);
                }
            }
            catch (IOException e)
            {
                logger.error(e.getMessage(), e);
            }
        }
    }

    protected void buildSampleGroupMapping(File csvDataFile)
    {
        if (csvDataFile != null && getDelimiter() != null)
        {
            try
            {
                CSVStrategy strategy = CSVStrategy.EXCEL_STRATEGY;
                strategy.setDelimiter(getDelimiter().charValue());
                CSVParser csvParser = new CSVParser(new FileReader(csvDataFile), strategy);

                // Populate the Header and sample first row
                
                // The header Row
                String[] dataRow = csvParser.getLine();
                if (dataRow != null && dataRow.length > 0)
                {
                    groupHeaderRow = Arrays.asList(dataRow);
                }

                //The sample row
                dataRow = csvParser.getLine();
                if (dataRow != null && dataRow.length > 0)
                {
                    groupSampleRow = Arrays.asList(dataRow);
                }
            }
            catch (IOException e)
            {
                logger.error(e.getMessage(), e);
            }
        }
    }

    public void isValid() throws ImporterConfigurationException
    {
        super.isValid();

        if (users == null || delimiter == null || encryptPasswords == null)
        {
            throw new ImporterConfigurationException("A required Configuration value was null: " + this);
        }
    }

    ///CLOVER:OFF
    public File getUsers()
    {
        return users;
    }

    public void setUsers(File users)
    {
        this.users = users;
        buildSampleUserMapping(users);
    }

    public File getGroupMemberships()
    {
        return groupMemberships;
    }

    public void setGroupMemberships(File groupMemberships)
    {
        this.groupMemberships = groupMemberships;
        buildSampleGroupMapping(groupMemberships);
    }

    public Character getDelimiter()
    {
        return delimiter;
    }

    public void setDelimiter(Character delimiter)
    {
        this.delimiter = delimiter;
    }

    public Boolean isEncryptPasswords()
    {
        return encryptPasswords;
    }

    public void setEncryptPasswords(Boolean encryptPasswords)
    {
        this.encryptPasswords = encryptPasswords;
    }

    public List getUserHeaderRow()
    {
        if (userHeaderRow == null)
        {
            buildSampleUserMapping(users);
        }
        return userHeaderRow;
    }

    public List getUserSampleRow()
    {
        if (userSampleRow == null)
        {
            buildSampleUserMapping(users);
        }
        return userSampleRow;
    }

    public List getGroupHeaderRow()
    {
        if (groupHeaderRow == null)
        {
            buildSampleGroupMapping(groupMemberships);
        }
        return groupHeaderRow;
    }

    public List getGroupSampleRow()
    {
        if (groupSampleRow == null)
        {
            buildSampleGroupMapping(groupMemberships);
        }
        return groupSampleRow;
    }

    public OrderedBidiMap getUserMappingConfiguration()
    {
        return userMappingConfiguration;
    }

    public void setUserMappingConfiguration(OrderedBidiMap userMappingConfiguration)
    {
        this.userMappingConfiguration = userMappingConfiguration;
    }

    public OrderedBidiMap getGroupMappingConfiguration()
    {
        return groupMappingConfiguration;
    }

    public void setGroupMappingConfiguration(OrderedBidiMap groupMappingConfiguration)
    {
        this.groupMappingConfiguration = groupMappingConfiguration;
    }
}
