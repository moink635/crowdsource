package com.atlassian.crowd.manager.property;

import com.atlassian.crowd.exception.ObjectNotFoundException;

import java.util.Map;

/**
 * Manages plugin properties.
 *
 * All properties are inserted with a "plugin." prefix.
 */
public interface PluginPropertyManager
{
    String getProperty(String key, String name) throws ObjectNotFoundException;

    void setProperty(String key, String name, String value);

    void removeProperty(String key, String name);

    Map<String, String> findAll(String key);
}
