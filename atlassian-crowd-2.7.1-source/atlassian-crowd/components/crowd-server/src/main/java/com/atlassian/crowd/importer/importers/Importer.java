package com.atlassian.crowd.importer.importers;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.model.Result;

import java.util.Set;

/**
 * Classes that extend this interface will manage the import of users, groups and their memberships
 * from a given application (or file) into Crowd.
 */
public interface Importer
{
    /**
     * Responsible for the import of both users and groups into Crowd using the given {@see Configuration}
     *
     * @param configuration the Configuration, this could be a JDBC configuration or a CSVConfiguration
     * @return the result of the import
     * @throws com.atlassian.crowd.importer.exceptions.ImporterException
     *          general exception if anything happened during the import
     */
    Result importUsersGroupsAndMemberships(final Configuration configuration) throws ImporterException;

    /**
     * Initialise anything required for the import process.
     *
     * @param configuration can contain any setup params such as database settings that need init'ing
     */
    void init(final Configuration configuration);

    /**
     * The type of configuration being used for this Import.
     *
     * @return Class that should be a subclass of Configuration
     */
    Class getConfigurationType();

    /**
     * Some sources might be capable of handling more than one directory.
     *
     * @param configuration the Importer Configuration
     * @return true if the source can have multiple directories.
     * @throws ImporterException if there is an error figuring out source directories.
     */
    boolean supportsMultipleDirectories(Configuration configuration) throws ImporterException;

    /**
     * The retrieve the set of remote source directories.
     *
     * @param configuration the import configuration.
     * @return the set of source directories.
     * @throws com.atlassian.crowd.importer.exceptions.ImporterException will be thrown if an error occurs connecting,
     * or if the provided <code>configuration</code> is invalid.
     */
    Set<Directory> retrieveRemoteSourceDirectory(Configuration configuration) throws ImporterException;
}
