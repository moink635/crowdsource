package com.atlassian.crowd.migration;

import org.dom4j.Element;

import java.util.Map;

/**
 * A Mapper that will handle the import and export of a Domain object, or any object from XML into the datastore
 * and vice-versa.
 */
public interface Mapper
{
    /**
     * Exports to V2 XML.
     *
     * @param options map of arbitrarty options.
     * @return XML element, or null if you are not mapping
     * @throws ExportException error producing XML.
     */
    Element exportXml(Map options) throws ExportException;

    /**
     * Imports V2 XML.
     *
     * @param root root XML element.
     * @throws ImportException error importing data.
     */
    void importXml(Element root) throws ImportException;
}

