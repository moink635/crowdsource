package com.atlassian.crowd.migration;

import com.atlassian.config.ConfigurationException;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Mapper implementation that will take the values from a Crowd instances <code>crowd.cfg.xml</code>
 * and place this into Crowd XML export/import.
 * At the moment, this only manages the import/export of the serverID property.
 * The Server ID is exported and imported since a Server ID is tied to a dataset and not a physical server.
 */
public class CrowdConfigMapper implements Mapper
{
    protected static final String CONFIG_XML_ROOT = "crowdcfg";
    protected static final String CONFIG_XML_NODE = "property";
    protected static final String CONFIG_XML_NAME = "name";
    protected static final String CONFIG_XML_VALUE = "value";

    protected List<String> propertiesToMap;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final CrowdBootstrapManager bootstrapManager;

    public CrowdConfigMapper(CrowdBootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
        propertiesToMap = new ArrayList<String>();

        // only cfg propery we want to export is the server id
        propertiesToMap.add(CrowdBootstrapManager.CROWD_SID);
    }

    public Element exportXml(Map options) throws ExportException
    {
        Element crowdPropertiesRoot = DocumentHelper.createElement(CONFIG_XML_ROOT);

        for (final String name : propertiesToMap)
        {
            String value = bootstrapManager.getString(name);

            if (value != null)
            {
                Element propertyElement = crowdPropertiesRoot.addElement(CONFIG_XML_NODE);
                propertyElement.addElement(CONFIG_XML_NAME).addText(name);
                propertyElement.addElement(CONFIG_XML_VALUE).addText(value);
            }
            else
            {
                logger.error("Failed to find value for property: " + name + " in crowd.cfg.xml");
            }
        }

        return crowdPropertiesRoot;
    }

    public void importXml(Element root) throws ImportException
    {
        Element propertiesElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + CONFIG_XML_ROOT);
        if (propertiesElement == null)
        {
            logger.info("No config properties were found for importing.");
            return;
        }

        for (Iterator properties = propertiesElement.elementIterator(); properties.hasNext();)
        {
            Element propertyElemet = (Element) properties.next();
            String name = propertyElemet.element(CONFIG_XML_NAME).getText();
            String value = propertyElemet.element(CONFIG_XML_VALUE).getText();

            bootstrapManager.setProperty(name, value);
        }

        try
        {
            bootstrapManager.save();
        }
        catch (ConfigurationException e)
        {
            throw new ImportException(e);
        }
    }

    public CrowdBootstrapManager getBootstrapManager()
    {
        return bootstrapManager;
    }
}
