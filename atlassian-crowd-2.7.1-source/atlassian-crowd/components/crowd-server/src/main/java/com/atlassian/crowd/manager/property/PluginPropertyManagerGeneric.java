package com.atlassian.crowd.manager.property;

import com.atlassian.crowd.dao.property.PropertyDAO;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.property.Property;
import org.apache.commons.lang3.Validate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PluginPropertyManagerGeneric implements PluginPropertyManager
{
    public static final String KEY_PREFIX = "plugin.";

    private final PropertyDAO propertyDAO;

    public PluginPropertyManagerGeneric(final PropertyDAO propertyDAO)
    {
        this.propertyDAO = propertyDAO;
    }

    private String generatePropertyKey(String key)
    {
        return key.startsWith(KEY_PREFIX) ? key : KEY_PREFIX + key;
    }

    public String getProperty(final String key, final String name) throws ObjectNotFoundException
    {
        Validate.notEmpty(key, "You cannot find an entry with a null/empty key");
        Validate.notEmpty(name, "You cannot find an entry with a null/empty name");

        return propertyDAO.find(generatePropertyKey(key), name).getValue();
    }
    
    public void setProperty(final String key, final String name, final String value)
    {
        Validate.notEmpty(key, "You cannot create an entry with a null/empty key");
        Validate.notEmpty(name, "You cannot create an entry with a null/empty name");

        final String propertyKey = generatePropertyKey(key);
        
        Property property = null;

        try
        {
            property = propertyDAO.find(propertyKey, name);
        }
        catch (ObjectNotFoundException e)
        {
            // ignore, we just want to update if the property already exist
        }

        if (property == null)
        {
            property = new Property(propertyKey, name, value);
        }
        else
        {
            property.setValue(value);
        }

        // add the property to the database
        propertyDAO.update(property);
    }

    public void removeProperty(final String key, final String name)
    {
        Validate.notEmpty(key, "You cannot remove an entry with a null/empty key");
        Validate.notEmpty(name, "You cannot remove an entry with a null/empty name");

        if (key.startsWith(KEY_PREFIX))
        {
            propertyDAO.remove(key, name);
        }
        else
        {
            propertyDAO.remove(KEY_PREFIX + key, name);
        }
    }

    public Map<String, String> findAll(final String key)
    {
        Validate.notEmpty(key, "You cannot find an entries with a null/empty key");

        if (key.startsWith(KEY_PREFIX))
        {
            return buildPropertyMap(propertyDAO.findAll(key));
        }
        else
        {
            return buildPropertyMap(propertyDAO.findAll(KEY_PREFIX + key));
        }
    }

    private Map<String, String> buildPropertyMap(List<Property> foundProperties)
    {
        Map<String, String> convertedProperties = new HashMap<String, String>();
        for (Property property : foundProperties)
        {
            convertedProperties.put(property.getKey(), property.getValue());
        }

        return convertedProperties;
    }
}
