package com.atlassian.crowd.console.action;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;

import java.util.ArrayList;
import java.util.List;

/**
 * Needed so application plugins can get the correct list of web-items without having to depend too heavily on the
 * internals of the way Crowd lays out the page.
 */
public abstract class ApplicationBaseAction extends BaseAction
{
    /**
     * Gets a list of web-items that should be used as tabs in the view of an application. UI elements with a
     * <web-section> of "application-tabs" are part of the system, and defined in system-ui-plugin.xml. Plugins
     * should define their app-specific web-items as "application-tabs:app-name" (eg "application-tabs:google-apps").
     * Any <web-items> defined for all applications can be defined as part of <web-section> "application-tabs".
     *
     * @return
     */
    public List<WebItemModuleDescriptor> getWebItemsForApplication() throws ApplicationNotFoundException
    {
        List<WebItemModuleDescriptor> appItems = new ArrayList<WebItemModuleDescriptor>();
        appItems.addAll(getWebInterfaceManager().getDisplayableItems("application-tabs", getWebFragmentsContextMap()));
        appItems.addAll(getWebInterfaceManager().getDisplayableItems("application-tabs:" + getApplication().getName(), getWebFragmentsContextMap()));

        return appItems;
    }


    public abstract Application getApplication() throws ApplicationNotFoundException;
}
