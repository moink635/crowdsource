package com.atlassian.crowd.migration.verify;

import com.atlassian.crowd.migration.legacy.database.sql.LegacyTableQueries;

import java.util.ArrayList;
import java.util.List;

/**
 * This manager will look after verifiers that need to run against the legacy database <strong>before</strong> migration
 * to validate that the migration can proceed.
 */
public class DatabaseVerificationManager
{
    private final List<DatabaseVerifier> verifiers;
    private LegacyTableQueries legacyTableQueries;

    public DatabaseVerificationManager(List<DatabaseVerifier> verifiers)
    {
        this.verifiers = verifiers;
    }

    /**
     * Will validate the given document against a list of {@link Verifier}'s
     *
     * @return A combined list of errors from all the verifiers. If this list is empty, then all verifiers passed.
     */
    public List<String> validate()
    {
        List<String> errors = new ArrayList<String>();
        for (DatabaseVerifier verifier : verifiers)
        {
            // Only run this verifier if the previous verifiers have passed
            if (errors.isEmpty())
            {
                // Clear any state
                verifier.clearState();

                verifier.setLegacyTableQueries(legacyTableQueries);
                verifier.verify();

                if (verifier.hasErrors())
                {
                    errors.addAll(verifier.getErrors());
                }
            }
        }
        return errors;
    }

    public void setLegacyTableQueries(LegacyTableQueries legacyTableQueries)
    {
        this.legacyTableQueries = legacyTableQueries;
    }
}
