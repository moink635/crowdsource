package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Disabling roles for delegating directories
 */
public class UpgradeTask397 implements UpgradeTask
{
    private DirectoryManager directoryManager;

    private final Collection<String> errors = new ArrayList<String>();

    public String getBuildNumber()
    {
        return "397";
    }

    public String getShortDescription()
    {
        return "Disabling roles for delegating directories";
    }

    public void doUpgrade() throws Exception
    {
        List<Directory> ldapDirectories = directoryManager.searchDirectories(
                                            QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                                                        .with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.DELEGATING))
                                                        .returningAtMost(EntityQuery.ALL_RESULTS));

        for (Directory directory : ldapDirectories)
        {
            boolean isRolesDisabled = Boolean.parseBoolean(directory.getValue(LDAPPropertiesMapper.ROLES_DISABLED));
            if (!isRolesDisabled)
            {
                DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);

                // Disabled roles for delegating directories
                directoryToUpdate.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.toString(true));
                directoryManager.updateDirectory(directoryToUpdate);
            }
        }
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryManager(final DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }
}
