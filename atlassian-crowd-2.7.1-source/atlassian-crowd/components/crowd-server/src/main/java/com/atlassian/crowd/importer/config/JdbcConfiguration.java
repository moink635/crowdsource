package com.atlassian.crowd.importer.config;

import com.atlassian.crowd.importer.exceptions.ImporterConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * This class represents the properties required to connect with a database,
 * specifically the details for a JIRA, Confluence or Bamboo database.
 */
public class JdbcConfiguration extends Configuration
{
    private String databaseURL;
    private String databaseDriver;
    private String username;
    private String password;

    /**
     * The source directory of groups/users/memberships
     */
    private Long sourceDirectoryID;

    public JdbcConfiguration(Long directoryID, String application, Boolean importPasswords, String databaseURL, String databaseDriver, String username, String password)
    {
        super(directoryID, application, importPasswords, Boolean.FALSE);
        this.databaseURL = databaseURL;
        this.databaseDriver = databaseDriver;
        this.username = username;
        this.password = password;
    }

    public Long getSourceDirectoryID()
    {
        return sourceDirectoryID;
    }

    public void setSourceDirectoryID(Long sourceDirectoryID)
    {
        this.sourceDirectoryID = sourceDirectoryID;
    }

    public void isValid() throws ImporterConfigurationException
    {
        super.isValid();
        
        if (StringUtils.isNotBlank(databaseURL) && StringUtils.isNotBlank(databaseDriver) &&
                StringUtils.isNotBlank(username))
        {

            // Test connecting to the datasource
            Connection connection = null;

            try
            {
                DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource(
                        this.getDatabaseDriver(),
                        this.getDatabaseURL(),
                        this.getUsername(),
                        this.getPassword()
                );

                connection = driverManagerDataSource.getConnection(this.getUsername(), this.getPassword());

            }
            catch (Exception e)
            {
                throw new ImporterConfigurationException("Could not load database connection: " + e.getMessage(), e);
            }
            finally
            {
                if (connection != null)
                {
                    try
                    {
                        connection.close();
                    }
                    catch (SQLException e)
                    {
                        // Too late now
                    }
                }
            }
        }
        else
        {
            throw new ImporterConfigurationException("Missing attributes for a JdbcConfiguration: Database URL: " + databaseURL + " Database Driver: " + databaseDriver + " Database Username: " + username);
        }
    }

    public String getDatabaseURL()
    {
        return databaseURL;
    }

    public void setDatabaseURL(String databaseURL)
    {
        this.databaseURL = databaseURL;
    }

    public String getDatabaseDriver()
    {
        return databaseDriver;
    }

    public void setDatabaseDriver(String databaseDriver)
    {
        this.databaseDriver = databaseDriver;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }
        if (!super.equals(o))
        {
            return false;
        }

        JdbcConfiguration that = (JdbcConfiguration) o;

        if (databaseDriver != null ? !databaseDriver.equals(that.databaseDriver) : that.databaseDriver != null)
        {
            return false;
        }
        if (databaseURL != null ? !databaseURL.equals(that.databaseURL) : that.databaseURL != null)
        {
            return false;
        }
        if (password != null ? !password.equals(that.password) : that.password != null)
        {
            return false;
        }
        if (username != null ? !username.equals(that.username) : that.username != null)
        {
            return false;
        }

        return true;
    }

    public int hashCode()
    {
        int result = super.hashCode();
        result = 31 * result + (databaseURL != null ? databaseURL.hashCode() : 0);
        result = 31 * result + (databaseDriver != null ? databaseDriver.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        return result;
    }
}
