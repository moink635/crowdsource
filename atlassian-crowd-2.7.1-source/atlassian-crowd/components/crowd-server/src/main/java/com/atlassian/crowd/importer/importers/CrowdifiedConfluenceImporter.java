package com.atlassian.crowd.importer.importers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.config.JdbcConfiguration;
import com.atlassian.crowd.importer.mappers.jdbc.UserMembershipMapper;
import com.atlassian.crowd.importer.model.MembershipDTO;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;

import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.RowMapper;

/**
 * Importer specific to the Confluence's Embedded Crowd format
 */
class CrowdifiedConfluenceImporter extends JdbcImporter
{
    private static final String CROWD_SOURCE_DIRECTORY_COUNT = "SELECT COUNT(id) FROM cwd_directory WHERE directory_type = 'INTERNAL'";

    private static final String CROWD_INTERNAL_DIRECTORIES_SQL = "SELECT id, directory_name FROM cwd_directory WHERE directory_type = 'INTERNAL'";

    private static final String FIND_GROUPS_SQL = "SELECT group_name, description, active FROM cwd_group WHERE directory_id = ?";

    private static final String FIND_USERS_SQL = "SELECT user_name, email_address, credential, first_name, last_name, display_name, active FROM cwd_user WHERE directory_id = ?";

    private static final String FIND_USER_GROUP_MEMBERSHIPS = "SELECT usr.user_name AS user_name, grp.group_name AS group_name FROM cwd_membership mem INNER JOIN cwd_group grp ON mem.parent_id = grp.id INNER JOIN cwd_user usr ON mem.child_user_id = usr.id WHERE usr.directory_id = ?";

    public CrowdifiedConfluenceImporter(DirectoryManager directoryManager)
    {
        super(directoryManager);
    }

    @Override
    public Collection<GroupTemplate> findGroups(final Configuration configuration)
    {
        return jdbcTemplate.query(FIND_GROUPS_SQL, new Object[] {((JdbcConfiguration)configuration).getSourceDirectoryID()}, getGroupMapper(configuration));
    }

    @Override
    public List<MembershipDTO> findUserToGroupMemberships(final Configuration configuration)
    {
        return jdbcTemplate.query(FIND_USER_GROUP_MEMBERSHIPS, new Object[] {((JdbcConfiguration)configuration).getSourceDirectoryID()}, getMembershipMapper());
    }

    @Override
    public List<UserTemplateWithCredentialAndAttributes> findUsers(final Configuration configuration)
    {
        return jdbcTemplate.query(FIND_USERS_SQL, new Object[] {((JdbcConfiguration)configuration).getSourceDirectoryID()}, getUserMapper(configuration));
    }

    @Override
    public String getSelectAllUserGroupMembershipsSQL()
    {
        throw new IllegalStateException("Should not be called");
    }

    @Override
    public String getSelectAllGroupsSQL()
    {
        throw new IllegalStateException("Should not be called");
    }

    @Override
    public String getSelectAllUsersSQL()
    {
        throw new IllegalStateException("Should not be called");
    }

    @Override
    public RowMapper getGroupMapper(Configuration configuration)
    {
        return new CrowdifiedConfluenceGroupMapper(configuration.getDirectoryID());
    }

    @Override
    public RowMapper getUserMapper(Configuration configuration)
    {
        return new CrowdifiedConfluenceUserMapper(configuration);
    }

    @Override
    public RowMapper getMembershipMapper()
    {
        return new UserMembershipMapper("user_name", "group_name");
    }

    @Override
    public boolean supportsMultipleDirectories(Configuration configuration)
    {
        init(configuration);

        // if the table doesn't exist or there is nothing inside then it's not crowd.
        try
        {
            return jdbcTemplate.queryForInt(CROWD_SOURCE_DIRECTORY_COUNT) > 0;
        }
        catch (BadSqlGrammarException bsge)
        {
            return false;
        }
    }

    @Override
    public Set<Directory> retrieveRemoteSourceDirectory(Configuration configuration)
    {
        init(configuration);

        return new HashSet<Directory>(jdbcTemplate.query(CROWD_INTERNAL_DIRECTORIES_SQL, new MinimalistConfluenceDirectoryMapper()));
    }

    private static class MinimalistConfluenceDirectoryMapper implements RowMapper
    {
        @Override
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            return new DirectoryImpl(new InternalEntityTemplate(rs.getLong("id"),
                                                                rs.getString("directory_name"),
                                                                true, null, null));
        }
    }

    protected static class CrowdifiedConfluenceGroupMapper implements RowMapper
    {
        private final Long directoryId;

        public CrowdifiedConfluenceGroupMapper(final Long directoryId)
        {
            this.directoryId = directoryId;
        }

        @Override
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            String groupName = rs.getString("group_name");
            GroupTemplate group = new GroupTemplate(groupName, directoryId, GroupType.GROUP);

            String activeStr = rs.getString("active").trim();
            group.setActive(activeStr != null && activeStr.equals("T"));

            group.setDescription(rs.getString("description"));

            return group;
        }
    }

    protected static class CrowdifiedConfluenceUserMapper implements RowMapper
    {
        private final Configuration configuration;

        public CrowdifiedConfluenceUserMapper(final Configuration configuration)
        {
            this.configuration = configuration;
        }

        @Override
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            String usernameStr = rs.getString("user_name");

            final PasswordCredential credential;
            if (configuration.isImportPasswords())
            {
                credential = new PasswordCredential(rs.getString("credential"), true);
            }
            else
            {
                credential = PasswordCredential.NONE;
            }

            UserTemplate user = new UserTemplate(usernameStr, configuration.getDirectoryID());

            String activeStr = rs.getString("active").trim();
            user.setActive(activeStr != null && activeStr.equals("T"));

            user.setEmailAddress(rs.getString("email_address"));
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));
            user.setDisplayName(rs.getString("display_name"));

            return new UserTemplateWithCredentialAndAttributes(user, credential);
        }
    }
}