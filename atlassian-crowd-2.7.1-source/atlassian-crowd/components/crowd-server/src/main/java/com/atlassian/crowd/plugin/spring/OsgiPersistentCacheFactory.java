package com.atlassian.crowd.plugin.spring;

import com.atlassian.crowd.plugin.PluginDirectoryLocator;
import com.atlassian.plugin.osgi.container.OsgiPersistentCache;
import com.atlassian.plugin.osgi.container.impl.DefaultOsgiPersistentCache;
import org.springframework.beans.factory.FactoryBean;

public class OsgiPersistentCacheFactory implements FactoryBean
{
    private final PluginDirectoryLocator pluginDirectoryLocator;

    public OsgiPersistentCacheFactory(PluginDirectoryLocator pluginDirectoryLocator)
    {
        this.pluginDirectoryLocator = pluginDirectoryLocator;
    }

    public Object getObject() throws Exception
    {
        return new DefaultOsgiPersistentCache(pluginDirectoryLocator.getPluginCachesDirectory());
    }

    public Class getObjectType()
    {
        return OsgiPersistentCache.class;
    }

    public boolean isSingleton()
    {
        return true;
    }
}
