package com.atlassian.crowd.plugin;

import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.spring.container.ContainerManager;

import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;

/**
 * Utility class to access OSGi components publicly exported by plugins.
 *
 * @since v2.7
 */
public class PluginComponentAccessor
{
    private static final String OSGI_CONTAINER_KEY = "osgiContainerManager";

    /**
     * Returns an instance of the service identified by the given interface, as exported by a plugin module.
     * <p>
     * <em>Warning:</em> as the instance returned comes from OSGi-land, it can become invalid at anytime (if the plugin
     * is disabled/enabled for instance). For this reason, it should <strong>NEVER</strong> be cached (e.g stored in a field variable).
     *
     * @param clazz the interface of the service
     * @param <T>   the interface type
     * @return an instance of the service, or <code>null</code> if no plugin publicly exports this particular service.
     */
    @Nullable
    public static <T> T getOSGiComponentInstanceOfType(final Class<T> clazz)
    {
        checkNotNull(clazz, "class");

        final OsgiContainerManager osgiContainerManager = ContainerManager.getComponent(OSGI_CONTAINER_KEY, OsgiContainerManager.class);
        checkState(osgiContainerManager != null, "Could not find a bean with key '%s'", OSGI_CONTAINER_KEY);

        if (osgiContainerManager instanceof CrowdOsgiContainerManager)
        {
            // If this is our custom implementation, let it handle the caching and automatic closing of ServiceTrackers
            return ((CrowdOsgiContainerManager) osgiContainerManager).getOsgiComponentOfType(clazz);
        }
        else
        {
            throw new IllegalStateException("Expected an instance of CrowdOsgiContainerManager but got " + osgiContainerManager.getClass().getName() + " instead");
        }
    }
}
