package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import org.springframework.dao.DataAccessException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * This upgrade task will set all current Internal Directories
 * to be set to DES
 */
public class UpgradeTask113 implements UpgradeTask
{
    private DirectoryManager directoryManager;

    private List<String> errors = new ArrayList<String>();

    public String getBuildNumber()
    {
        return "113";
    }

    public String getShortDescription()
    {
        return "Set encryption method for all current Internal Directories to be DES";
    }

    public void doUpgrade() throws Exception
    {
        List directories = directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.INTERNAL)).returningAtMost(EntityQuery.ALL_RESULTS));

        for (Iterator iterator = directories.iterator(); iterator.hasNext();)
        {
            DirectoryImpl directory = (DirectoryImpl) iterator.next();

            // Check to see if the directory has a user encryption property set
            String userEncryption = directory.getValue(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD);
            if (userEncryption == null)
            {
                // If there is no property set, set it to DES
                directory.setAttribute(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, PasswordEncoderFactory.DES_ENCODER);

                // Call update for this directory
                try
                {
                    directoryManager.updateDirectory(directory);
                }
                catch (DataAccessException e)
                {
                    // add the exception to the errors and throw the exception up
                    errors.add(e.getMessage());
                    throw e;
                }
            }
        }
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }
}
