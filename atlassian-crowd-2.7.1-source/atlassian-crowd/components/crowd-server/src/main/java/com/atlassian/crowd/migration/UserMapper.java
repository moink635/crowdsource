package com.atlassian.crowd.migration;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.migration.legacy.PartialXmlMapper;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.InternalUserAttribute;
import com.atlassian.crowd.model.user.InternalUserCredentialRecord;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.*;

/**
 * This mapper will handle the mapping of a {@link com.atlassian.crowd.model.user.User}.
 */
public class UserMapper extends PartialXmlMapper implements Mapper
{
    /** Types of directories for which users will need to be backed up */
    private static final Set<DirectoryType> INCLUDED_DIRECTORY_TYPES =
        EnumSet.of(DirectoryType.INTERNAL, DirectoryType.DELEGATING);

    private final UserDAOHibernate userDAO;
    private final DirectoryDAOHibernate directoryDAO;

    // crowd v 2.0+ elements
    protected static final String USER_XML_ROOT = "users";
    protected static final String USER_XML_NODE = "user";
    private static final String USER_XML_DIRECTORY_ID = "directoryId";
    private static final String USER_XML_FIRST_NAME = "firstName";
    private static final String USER_XML_LAST_NAME = "lastName";
    private static final String USER_XML_DISPLAY_NAME = "displayName";
    private static final String USER_XML_EMAIL = "email";
    private static final String USER_XML_EXTERNAL_ID = "externalId";
    private static final String USER_XML_CREDENTIAL = "credential";
    private static final String USER_XML_CREDENTIAL_RECORDS = "credentialRecords";
    private static final String USER_XML_CREDENTIAL_RECORD = "credentialRecord";
    private static final String USER_XML_ATTRIBUTES = "attributes";
    private static final String USER_XML_ATTRIBUTE = "attribute";
    private static final String USER_XML_ATTRIBUTE_NAME = "name";
    private static final String USER_XML_ATTRIBUTE_VALUE = "value";


    public UserMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, DirectoryDAOHibernate directoryDAO,
                      UserDAOHibernate userDAO, DirectoryManager directoryManager)
    {
        super(sessionFactory, batchProcessor, directoryManager, INCLUDED_DIRECTORY_TYPES);
        this.directoryDAO = directoryDAO;
        this.userDAO = userDAO;
    }

    public Element exportXml(Map options) throws ExportException
    {
        Element userRoot = DocumentHelper.createElement(USER_XML_ROOT);

        // only export users from internal/delegated
        List<Directory> directories = findAllExportableDirectories();

        for (Directory directory : directories)
        {
            List<InternalUser> users = userDAO.search(directory.getId(), QueryBuilder.queryFor(InternalUser.class, EntityDescriptor.user()).returningAtMost(EntityQuery.ALL_RESULTS));

            for (InternalUser user : users)
            {
                Collection<InternalUserAttribute> attributes = userDAO.findUserAttributes(user.getId());

                addUserToXml(user, attributes, userRoot);
            }
        }

        return userRoot;
    }

    private static void addIfNotNull(Element parent, String name, String text)
    {
        if (text != null)
        {
            parent.addElement(name).setText(text);
        }
    }

    protected void addUserToXml(InternalUser user, Collection<InternalUserAttribute> attributes, Element userRoot)
    {
        Element userElement = userRoot.addElement(USER_XML_NODE);
        userElement.addElement(USER_XML_DIRECTORY_ID).addText(String.valueOf(user.getDirectoryId()));
        exportInternalEntity(user, userElement);
        addIfNotNull(userElement, USER_XML_FIRST_NAME, user.getFirstName());
        addIfNotNull(userElement, USER_XML_LAST_NAME, user.getLastName());
        addIfNotNull(userElement, USER_XML_DISPLAY_NAME, user.getDisplayName());
        addIfNotNull(userElement, USER_XML_EMAIL, user.getEmailAddress());
        addIfNotNull(userElement, USER_XML_EXTERNAL_ID, user.getExternalId());

        if (user.getCredential() != null) {
            userElement.addElement(USER_XML_CREDENTIAL).addText(user.getCredential().getCredential());
        }

        addUserCredentialRecordsToXml(user.getCredentialRecords(), userElement);

        addUserAttributesToXml(attributes, userElement);
    }

    private void addUserCredentialRecordsToXml(List<InternalUserCredentialRecord> records, Element userElement)
    {
        Element credentialRecordsElement = userElement.addElement(USER_XML_CREDENTIAL_RECORDS);
        for (InternalUserCredentialRecord record : records)
        {
            Element recordElement = credentialRecordsElement.addElement(USER_XML_CREDENTIAL_RECORD);
            recordElement.addElement(GENERIC_XML_ID).addText(record.getId().toString());
            recordElement.addElement(USER_XML_CREDENTIAL).addText(record.getPasswordHash());
        }
    }

    private void addUserAttributesToXml(Collection<InternalUserAttribute> attributes, Element userElement)
    {
        Element attributesElement = userElement.addElement(USER_XML_ATTRIBUTES);
        for (InternalUserAttribute attribute : attributes)
        {
            Element attributeElement = attributesElement.addElement(USER_XML_ATTRIBUTE);
            attributeElement.addElement(GENERIC_XML_ID).addText(attribute.getId().toString());
            attributeElement.addElement(USER_XML_ATTRIBUTE_NAME).addText(attribute.getName());
            attributeElement.addElement(USER_XML_ATTRIBUTE_VALUE).addText(attribute.getValue());
        }
    }

    public void importXml(final Element root) throws ImportException
    {
        Element usersElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + USER_XML_ROOT);
        if (usersElement == null)
        {
            logger.error("No users were found for importing!");
            return;
        }

        // TODO: we should stream the XML and chunkinise this process rather than beg for OOMEs
        List<InternalUser> usersToReplicate = new ArrayList<InternalUser>();
        List<InternalUserAttribute> userAttributesToReplicate = new ArrayList<InternalUserAttribute>();

        for (Iterator userElementIterator = usersElement.elementIterator(); userElementIterator.hasNext();)
        {
            Element userElement = (Element) userElementIterator.next();

            InternalUser user = getUserFromXml(userElement);
            if (isImportableDirectory(user.getDirectory()))
            {
                usersToReplicate.add(user);
            }

            List<InternalUserAttribute> attributes = getUserAttributesFromXml(userElement, user);
            userAttributesToReplicate.addAll(attributes);
        }

        logger.info("About to replicate " + usersToReplicate.size() + " users");
        addEntities(usersToReplicate);
        logger.info("User replication complete");

        logger.info("About to replicate " + userAttributesToReplicate.size() + " user attributes");
        addEntities(userAttributesToReplicate);
        logger.info("User attribute replication complete");
    }

    private static String textOrNull(Element elem)
    {
        if (elem != null)
        {
            return elem.getText();
        }
        else
        {
            return null;
        }
    }

    protected InternalUser getUserFromXml(Element userElement)
    {
        Long directoryId = Long.parseLong(userElement.element(USER_XML_DIRECTORY_ID).getText());
        DirectoryImpl directory = (DirectoryImpl) directoryDAO.loadReference(directoryId);

        InternalEntityTemplate internalEntityTemplate = getInternalEntityTemplateFromXml(userElement);

        UserTemplate userTemplate = new UserTemplate(internalEntityTemplate.getName(), directoryId);
        userTemplate.setActive(internalEntityTemplate.isActive());
        userTemplate.setFirstName(textOrNull(userElement.element(USER_XML_FIRST_NAME)));
        userTemplate.setLastName(textOrNull(userElement.element(USER_XML_LAST_NAME)));
        userTemplate.setDisplayName(textOrNull(userElement.element(USER_XML_DISPLAY_NAME)));
        userTemplate.setEmailAddress(textOrNull(userElement.element(USER_XML_EMAIL)));
        userTemplate.setExternalId(textOrNull(userElement.element(USER_XML_EXTERNAL_ID)));

        final Element credentialElement = userElement.element(USER_XML_CREDENTIAL);
        final PasswordCredential credential;
        if (credentialElement == null) {
            credential = null;
        } else {
            credential = new PasswordCredential(credentialElement.getText(), true);
        }

        InternalUser user = new InternalUser(internalEntityTemplate, directory, userTemplate, credential);

        List<InternalUserCredentialRecord> records = getUserCredentialRecordsFromXml(userElement, user);
        user.getCredentialRecords().addAll(records);

        return user;
    }

    private List<InternalUserCredentialRecord> getUserCredentialRecordsFromXml(Element userElement, InternalUser userReference)
    {
        List<InternalUserCredentialRecord> records = new ArrayList<InternalUserCredentialRecord>();

        Element credentialRecordsElement = userElement.element(USER_XML_CREDENTIAL_RECORDS);
        for (Iterator recordsIter = credentialRecordsElement.elementIterator(); recordsIter.hasNext();)
        {
            Element recordElement = (Element) recordsIter.next();

            Long id = Long.parseLong(recordElement.element(GENERIC_XML_ID).getText());
            String password = recordElement.element(USER_XML_CREDENTIAL).getText();

            InternalUserCredentialRecord record = new InternalUserCredentialRecord(id, userReference, password);
            records.add(record);
        }

        return records;
    }

    protected List<InternalUserAttribute> getUserAttributesFromXml(Element userElement, InternalUser userReference)
    {
        List<InternalUserAttribute> attributes = new ArrayList<InternalUserAttribute>();

        Element attributesElement = userElement.element(USER_XML_ATTRIBUTES);
        for (Iterator attribsIterator = attributesElement.elementIterator(); attribsIterator.hasNext();)
        {
            Element attributeElement = (Element) attribsIterator.next();
            Long id = Long.parseLong(attributeElement.element(GENERIC_XML_ID).getText());
            String name = attributeElement.element(USER_XML_ATTRIBUTE_NAME).getText();
            String value = attributeElement.element(USER_XML_ATTRIBUTE_VALUE).getText();

            InternalUserAttribute attribute = new InternalUserAttribute(id, userReference, name, value);
            attributes.add(attribute);
        }

        return attributes;
    }
}
