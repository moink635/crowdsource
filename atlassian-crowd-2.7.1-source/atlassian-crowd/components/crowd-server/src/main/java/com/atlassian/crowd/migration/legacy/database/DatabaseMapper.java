package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.migration.GenericMapper;
import com.atlassian.crowd.migration.legacy.database.sql.LegacyTableQueries;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DatabaseMapper extends GenericMapper
{
    protected JdbcOperations jdbcTemplate;
    protected LegacyTableQueries legacyTableQueries;

    public DatabaseMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, JdbcOperations jdbcTemplate)
    {
        super(sessionFactory, batchProcessor);
        this.jdbcTemplate = jdbcTemplate;
    }

    protected InternalEntityTemplate createInternalEntityTemplate(Long id, String name, Date createDate, Date updatedDate, boolean active)
    {
        InternalEntityTemplate template = new InternalEntityTemplate();
        template.setId(id);
        template.setName(name);
        template.setCreatedDate(createDate);
        template.setUpdatedDate(updatedDate);
        template.setActive(active);
        return template;
    }

    public Map<String, String> attributeListToMap(List<Map<String, String>> attributes)
    {
        if (attributes == null)
        {
            return new HashMap<String, String>();
        }

        Map<String, String> attributeMap = new HashMap<String, String>(attributes.size());
        for (Map<String, String> attribute : attributes)
        {
            attributeMap.putAll(attribute);
        }
        return attributeMap;
    }

    public Map<String, Set<String>> attributeListToMultiAttributeMap(List<Map<String, String>> attributes)
    {
        if (attributes == null)
        {
            return new HashMap<String, Set<String>>();
        }

        Map<String, Set<String>> multiAttributeMap = new HashMap<String, Set<String>>(attributes.size());

        for (Map<String, String> attribute : attributes)
        {
            for (Map.Entry<String, String> entry : attribute.entrySet())
            {
                String entryKey = entry.getKey();
                String entryValue = entry.getValue();
                Set<String> attributeSet = new HashSet<String>();

                if (multiAttributeMap.containsKey(entryKey))
                {
                    attributeSet.addAll(multiAttributeMap.get(entryKey));
                }
                attributeSet.add(entryValue);
                multiAttributeMap.put(entryKey, attributeSet);
            }
        }

        return multiAttributeMap;
    }

    protected class AttributeMapper implements RowCallbackHandler
    {
        private final Map<EntityIdentifier, List<Map<String, String>>> entityAttributes = new HashMap<EntityIdentifier, List<Map<String, String>>>();

        private String directoryIdColumnName;
        private String entityNameColumnName;

        public AttributeMapper(String directoryIdColumnName, String entityNameColumnName)
        {
            this.directoryIdColumnName = directoryIdColumnName;
            this.entityNameColumnName = entityNameColumnName;
        }

        public void processRow(ResultSet rs) throws SQLException
        {
            String attributeName = rs.getString("ATTRIBUTE");
            String attributeValue = rs.getString("VALUE");
            Long directoryId = rs.getLong(directoryIdColumnName);
            String userName = rs.getString(entityNameColumnName);

            EntityIdentifier currentUser = new EntityIdentifier(directoryId, userName);

            Map<String, String> attributes = new HashMap<String, String>();
            attributes.put(attributeName, attributeValue);

            if (!entityAttributes.containsKey(currentUser))
            {
                entityAttributes.put(currentUser, new ArrayList<Map<String, String>>());
            }
            entityAttributes.get(currentUser).add(attributes);
        }

        public Map<EntityIdentifier, List<Map<String, String>>> getEntityAttributes()
        {
            return entityAttributes;
        }
    }

    protected class EntityIdentifier
    {
        private final String name;
        private final Long directoryId;

        public EntityIdentifier(Long directoryId, String name)
        {
            this.directoryId = directoryId;
            this.name = name;
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            EntityIdentifier that = (EntityIdentifier) o;

            if (directoryId != null ? !directoryId.equals(that.directoryId) : that.directoryId != null) return false;
            if (name != null ? !name.equals(that.name) : that.name != null) return false;

            return true;
        }

        @Override
        public int hashCode()
        {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (directoryId != null ? directoryId.hashCode() : 0);
            return result;
        }
    }

    public void setJdbcTemplate(JdbcOperations jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setLegacyTableQueries(LegacyTableQueries legacyTableQueries)
    {
        this.legacyTableQueries = legacyTableQueries;
    }
}
