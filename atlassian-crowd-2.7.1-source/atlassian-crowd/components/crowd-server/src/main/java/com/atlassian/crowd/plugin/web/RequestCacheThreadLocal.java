package com.atlassian.crowd.plugin.web;

import java.util.Map;

/**
 * Threadlocal for caching objects that must survive the whole HTTP request. The threadlocal is initialised in
 * the {@link com.atlassian.crowd.plugin.web.filter.RequestCacheThreadLocalFilter}
 */
public class RequestCacheThreadLocal
{
    public static final String CONTEXT_PATH_KEY = "crowd.context.path";

    private static final ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal<Map<String, Object>>();

    public static Map<String, Object> getRequestCache()
    {
        return threadLocal.get();
    }

    public static void setRequestCache(Map<String, Object> requestCache)
    {
        threadLocal.set(requestCache);
    }

    public static void clearRequestCache()
    {
        threadLocal.remove();
    }

    public static String getContextPath()
    {
        return (String) (getRequestCache() != null ? getRequestCache().get(CONTEXT_PATH_KEY) : null);
    }
}
