package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationManagerException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.RemoteAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Updating values for application remote addresses to allow support for IPv6
 */
public class UpgradeTask430 implements UpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask430.class);

    private ApplicationManager applicationManager;

    private final Collection<String> errors = new ArrayList<String>();


    public String getBuildNumber()
    {
        return "430";
    }

    public String getShortDescription()
    {
        return "Updating values for application remote addresses to allow support for IPv6";
    }

    public void doUpgrade() throws Exception
    {
        for (Application application : applicationManager.findAll())
        {
            try
            {
                log.debug("Updating application {}", application);
                updateApplication(application);
            }
            catch (ApplicationManagerException e)
            {
                final String errorMessage = "Could not update application " + application;
                log.error(errorMessage, e);
                errors.add(errorMessage + ", error is " + e.getMessage());
            }
            catch (ApplicationNotFoundException e)
            {
                final String errorMessage = "Could not find application " + application;
                log.error(errorMessage, e);
                errors.add(errorMessage + ", error is " + e.getMessage());
            }
        }
    }

    private void updateApplication(Application application) throws ApplicationManagerException, ApplicationNotFoundException
    {
        Set<RemoteAddress> updatedAddresses = new HashSet<RemoteAddress>();

        // We only need the address to create the new RemoteAddress that has the info required for IPv6
        Set<RemoteAddress> originalAddresses = application.getRemoteAddresses();
        for (RemoteAddress address : originalAddresses)
        {
            updatedAddresses.add(new RemoteAddress(address.getAddress()));
        }

        // Update the application with the updated RemoteAddresses
        ApplicationImpl applicationImpl = (ApplicationImpl) application;
        applicationImpl.setRemoteAddresses(updatedAddresses);
        applicationManager.update(applicationImpl);
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setApplicationManager(ApplicationManager applicationManager)
    {
        this.applicationManager = applicationManager;
    }
}
