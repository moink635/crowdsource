package com.atlassian.crowd.util;

import org.slf4j.Logger;


public interface SystemInfoHelper
{
    static final long MEGABYTE = 1048576;

    String getApplicationServer();

    long getTotalMemory();

    long getFreeMemory();

    long getUsedMemory();

    String getOperatingSystem();

    String getJavaVersion();

    String getJavaVendor();

    String getJavaRuntime();

    String getApplicationUsername();

    String getArchitecture();

    String getFileEncoding();

    String getCrowdVersion();

    String getTimeZone();

    String getJavaVMVersion();

    String getJavaVMVendor();

    String getServerId();

    void printSystemInfo(final Logger logger);

    void printMinimalSystemInfo(final Logger logger);

    String getDatabaseJdbcDriver();

    String getDatabaseJdbcUsername();

    String getDatabaseJdbcUrl();

    String getDatabaseHibernateDialect();

    String getDatabaseDatasourceJndiName();

    boolean isDatabaseDatasource();
}
