package com.atlassian.crowd.importer.exceptions;

/**
 * An exception that will represent an error thrown via the Importer code.
 */
public class ImporterException extends Exception
{
    /**
     * Default constructor.
     */
    public ImporterException()
    {
    }

    /**
     * Default constructor.
     *
     * @param s the message.
     */
    public ImporterException(String s)
    {
        super(s);
    }

    /**
     * Default constructor.
     *
     * @param s         the message.
     * @param throwable the {@link Exception Exception}.
     */
    public ImporterException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    /**
     * Default constructor.
     *
     * @param throwable the {@link Exception Exception}.
     */
    public ImporterException(Throwable throwable)
    {
        super(throwable);
    }

}
