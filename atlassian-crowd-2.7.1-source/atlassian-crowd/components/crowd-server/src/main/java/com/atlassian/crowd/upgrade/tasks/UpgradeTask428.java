package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.*;
import com.atlassian.crowd.embedded.api.*;
import com.atlassian.crowd.embedded.spi.*;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.model.directory.*;
import com.atlassian.crowd.search.*;
import com.atlassian.crowd.search.builder.*;
import com.atlassian.crowd.search.query.entity.*;
import com.atlassian.crowd.search.query.entity.restriction.constants.*;
import org.slf4j.*;
import org.springframework.dao.*;

import java.util.*;

/**
 * Upgrades the connector directories to include a {@link DirectoryProperties#CACHE_ENABLED} properties set to
 * <tt>true</tt>.
 *
 * @since v2.1
 */
public class UpgradeTask428 implements UpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask428.class);

    private final Collection<String> errors = new ArrayList<String>();

    private DirectoryDao directoryDao;

    public String getBuildNumber()
    {
        return "428";
    }

    public String getShortDescription()
    {
        return "Upgrading connector directories to include the attribute " + DirectoryProperties.CACHE_ENABLED + " set to false.";
    }

    public void doUpgrade() throws Exception
    {
        for (Directory directory : findAllConnectorDirectories())
        {
            log.debug("Upgrading directory {}", directory);
            try
            {
                updateDirectory(directory);
            }
            catch (DataAccessException e)
            {
                final String errorMessage = "Could not update directory " + directory;
                log.error(errorMessage, e);
                errors.add(errorMessage + ", error is " + e.getMessage());
            }
        }
    }

    private void updateDirectory(Directory directory) throws DataAccessException, DirectoryNotFoundException
    {
        DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
        directoryToUpdate.setAttribute(DirectoryProperties.CACHE_ENABLED, Boolean.FALSE.toString());
        directoryDao.update(directoryToUpdate);
    }

    private List<Directory> findAllConnectorDirectories()
    {
        return directoryDao.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                .with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.CONNECTOR))
                .returningAtMost(EntityQuery.ALL_RESULTS));
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }
}
