package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.dao.membership.MembershipDAOHibernate;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.group.InternalGroupWithAttributes;
import com.atlassian.crowd.model.membership.InternalMembership;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchResultWithIdReferences;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GroupMapper extends GenericLegacyImporter implements LegacyImporter
{
    public static final String REMOTE_GROUP_XML_ROOT = "groups";
    public static final String REMOTE_GROUP_XML_PRINCIPAL_DIRECTORY_ID = "directoryId";
    public static final String REMOTE_GROUP_XML_DIRECTORY_ID = REMOTE_GROUP_XML_PRINCIPAL_DIRECTORY_ID;
    public static final String REMOTE_GROUP_XML_DESCRIPTION = "description";
    public static final String REMOTE_GROUP_XML_PRINCIPAL_NODE = "principals";
    public static final String REMOTE_GROUP_XML_PRINCIPAL = "principal";

    private final GroupDAOHibernate groupDAO;
    private final MembershipDAOHibernate membershipDAO;
    private final DirectoryDAOHibernate directoryDAO;

    public GroupMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, GroupDAOHibernate groupDAO, MembershipDAOHibernate membershipDAO, DirectoryDAOHibernate directoryDAO)
    {
        super(sessionFactory, batchProcessor);
        this.groupDAO = groupDAO;
        this.membershipDAO = membershipDAO;
        this.directoryDAO = directoryDAO;
    }

    public void importXml(final Element root, final LegacyImportDataHolder importData) throws ImportException
    {
        Element groupsElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + REMOTE_GROUP_XML_ROOT);
        if (groupsElement == null)
        {
            logger.error("No groups were found for importing!");
            return;
        }

        List<InternalGroupWithAttributes> groups = new ArrayList<InternalGroupWithAttributes>();
        for (Iterator remoteGroups = groupsElement.elementIterator(); remoteGroups.hasNext();)
        {
            Element groupElement = (Element) remoteGroups.next();

            InternalGroupWithAttributes group = getGroupAndAttributesFromXml(groupElement, importData.getOldToNewDirectoryIds());

            groups.add(group);
        }

        BatchResultWithIdReferences<Group> groupImportResults = groupDAO.addAll(groups);
        for (Group group : groupImportResults.getFailedEntities())
        {
            logger.error("Unable to add group <" + group.getName() + "> in directory with id <" + group.getDirectoryId() + ">");
        }

        // die hard if there are any errors
        if (groupImportResults.hasFailures())
        {
            throw new ImportException("Unable to import all groups. See logs for more details.");
        }

        // now import memberships (group by group)
        for (Iterator remoteGroups = groupsElement.elementIterator(); remoteGroups.hasNext();)
        {
            Element groupElement = (Element) remoteGroups.next();
            Set<InternalMembership> memberships = getMemberships(groupElement, importData, groupImportResults);

            BatchResult<InternalMembership> membershipResult = membershipDAO.addAll(memberships);

            for (InternalMembership membership : membershipResult.getFailedEntities())
            {
                logger.error("Unable to add user <" + membership.getChildName() + "> to group <" + membership.getParentName() + "> in directory with id <" + membership.getDirectory().getId() + ">");
            }

            if (membershipResult.hasFailures())
            {
                throw new ImportException("Unable to import all memberships. See logs for more details.");
            }
        }
    }

    protected InternalGroupWithAttributes getGroupAndAttributesFromXml(Element groupElement, Map<Long, Long> oldToNewDirectoryIds)
    {
        // pull directoryId
        Long oldDirectoryId = Long.parseLong(groupElement.element(REMOTE_GROUP_XML_DIRECTORY_ID).getText());
        Long directoryId = oldToNewDirectoryIds.get(oldDirectoryId);
        if (directoryId == null)
        {
            throw new IllegalArgumentException("Group belongs to an unknown old directory with ID: " + directoryId);
        }
        DirectoryImpl directory = (DirectoryImpl) directoryDAO.loadReference(directoryId);

        // pull all the default values
        InternalEntityTemplate internalEntityTemplate = getInternalEntityTemplateFromLegacyXml(groupElement);
        internalEntityTemplate.setName(internalEntityTemplate.getName());
        internalEntityTemplate.setId(null);

        GroupTemplate groupTemplate = new GroupTemplate(internalEntityTemplate.getName(), directory.getId(), GroupType.GROUP);
        groupTemplate.setActive(internalEntityTemplate.isActive());
        String description = "";
        Element descriptionElement = groupElement.element(REMOTE_GROUP_XML_DESCRIPTION);
        if (descriptionElement != null && descriptionElement.hasContent())
        {
            description = descriptionElement.getText();
        }
        groupTemplate.setDescription(description);

        // save the group
        InternalGroup group = new InternalGroup(internalEntityTemplate, directory, groupTemplate);

        Map<String, Set<String>> attributes = getMultiValuedAttributesMapFromXml(groupElement);

        return new InternalGroupWithAttributes(group, attributes);
    }

    protected Set<InternalMembership> getMemberships(Element groupElement, LegacyImportDataHolder importData, BatchResultWithIdReferences<Group> groupImportResults)
    {
        String groupName = groupElement.element(GENERIC_XML_NAME).getText();
        Long oldDirectoryId = Long.parseLong(groupElement.element(REMOTE_GROUP_XML_DIRECTORY_ID).getText());
        Long directoryId = importData.getOldToNewDirectoryIds().get(oldDirectoryId);
        if (directoryId == null)
        {
            throw new IllegalArgumentException("Group belongs to an unknown old directory with ID: " + directoryId);
        }

        DirectoryImpl directory = (DirectoryImpl) directoryDAO.loadReference(directoryId);
        Long groupId = groupImportResults.getIdReference(directoryId, groupName);

        Set<InternalMembership> memberships = new HashSet<InternalMembership>();

        Element membersElement = groupElement.element(REMOTE_GROUP_XML_PRINCIPAL_NODE);

        if (membersElement != null && membersElement.hasContent())
        {
            for (Iterator iterator = membersElement.elementIterator(REMOTE_GROUP_XML_PRINCIPAL); iterator.hasNext();)
            {
                Element memberElement = (Element) iterator.next();
                String memberName = memberElement.attributeValue(GENERIC_XML_NAME);
                Long userId = importData.getUserImportResults().getIdReference(directoryId, memberName);

                InternalMembership membership = new InternalMembership(null, groupId, userId, MembershipType.GROUP_USER, GroupType.GROUP, groupName, memberName, directory);
                memberships.add(membership);
            }
        }

        return memberships;
    }
}
