package com.atlassian.crowd.manager.backup;

/**
 * Service to handle the scheduling of automated backup.
 *
 * @since v2.7
 */
public interface BackupScheduler
{
    /**
     * Return whether automated backup is enabled
     * @return true if automated backup is enabled
     */
    boolean isEnabled();

    /**
     * Enable automated backups.
     * @throws ScheduledBackupException if there was a problem scheduling the automated backup job
     */
    void enable() throws ScheduledBackupException;

    /**
     * Disable automated backups.
     * @throws ScheduledBackupException if there was a problem scheduling the automated backup job
     */
    void disable() throws ScheduledBackupException;

    /**
     * Set the time of day at which the automated backups are run.
     * @param hourToRun    a valid time (must be between 0 and 23)
     * @throws ScheduledBackupException if there was a problem re-scheduling the automated backup job
     */
    void setHourToRun(int hourToRun) throws ScheduledBackupException;

    /**
     * Return the time of day at which the automated backups are run
     * @return a number between 0 and 23
     */
    int getHourToRun();
}
