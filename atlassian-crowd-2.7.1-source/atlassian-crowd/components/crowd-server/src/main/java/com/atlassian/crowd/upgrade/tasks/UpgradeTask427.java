package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.upgrade.util.UpgradeUtilityDAOHibernate;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Updating the local column in cwd_group from null to false. This change is needed so hibernate doesn't complain about the null values when searching for groups.
 */
public class UpgradeTask427 implements UpgradeTask
{
    private UpgradeUtilityDAOHibernate upgradeUtilityDao;

    private final Collection<String> errors = new ArrayList<String>();
    
    protected static final String UPDATE_LOCAL_TO_FALSE = "update InternalGroup g set g.local = false where g.local is null";

    public String getBuildNumber()
    {
        return "427";
    }

    public String getShortDescription()
    {
        return "Updating the local column in cwd_group from null to false";
    }

    public void doUpgrade() throws Exception
    {
        // Executes HSQL to update the null values to false in the local column that was introduced to cwd_group in 2.1
        // If crowd was upgrade from 2.0.x to 2.1, this column will be pre-populated with null.
        // Unable to update via directoryManager or DAO since executing any queries with the null values causes hibernate to complain.
        upgradeUtilityDao.executeUpdate(UPDATE_LOCAL_TO_FALSE);
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setUpgradeUtilityDao(UpgradeUtilityDAOHibernate upgradeUtilityDao)
    {
        this.upgradeUtilityDao = upgradeUtilityDao;
    }
}
