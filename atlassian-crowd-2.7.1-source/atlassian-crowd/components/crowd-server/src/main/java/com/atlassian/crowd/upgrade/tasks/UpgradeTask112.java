package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UpgradeTask112 implements UpgradeTask
{
    private static final String BUILD_NUMBER = "112";

    private List<String> errors = new ArrayList<String>();

    private DirectoryManager directoryManager;

    public String getBuildNumber()
    {
        return BUILD_NUMBER;
    }

    public String getShortDescription()
    {
        return "Adds missing paged results option to the LDAP connectors.";
    }

    public void doUpgrade() throws Exception
    {
        // find all of the directories
        final List<Directory> directories = directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS));

        for (Directory directory : directories)
        {
            // if the directory is one of our LDAP connectors, we need to check it
            if (DirectoryType.CONNECTOR.equals(directory.getType()))
            {
                // if the paged results key does not exist, add it with a default value of false
                if (!directory.getAttributes().containsKey(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY))
                {

                    DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
                    directoryToUpdate.setAttribute(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY, Boolean.toString(false));
                    // store the update to the database
                    directoryManager.updateDirectory(directoryToUpdate);
                }
            }
        }
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }
}
