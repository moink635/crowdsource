package com.atlassian.crowd.manager.bootstrap;

import com.atlassian.config.ConfigurationException;
import com.atlassian.config.bootstrap.AtlassianBootstrapManager;

public interface CrowdBootstrapManager extends AtlassianBootstrapManager
{
    public static final String CONFIG_DIRECTORY_PARAM = "crowd.config.directory";
    public static final String CROWD_SID = "crowd.server.id";
    
    public String getServerID();

    public void setServerID(String sid) throws ConfigurationException;    
}
