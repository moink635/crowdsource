package com.atlassian.crowd.event.listener;

import com.atlassian.crowd.event.EventJobExecutionException;
import com.atlassian.crowd.event.LicenseResourceLimitEvent;
import com.atlassian.crowd.manager.license.CrowdLicenseManager;
import com.atlassian.crowd.manager.mail.MailManager;
import com.atlassian.crowd.manager.mail.MailSendException;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.util.I18nHelper;

import com.google.common.collect.ImmutableList;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 * Will send an email to the Crowd Administrator letting them know that they are reaching their
 * license limit (within 90%).
 */
public class LicenseResourceLimitListener
{
    private PropertyManager propertyManager;
    private CrowdLicenseManager crowdLicenseManager;
    private I18nHelper i18nHelper;
    private MailManager mailManager;

    private static final String MAILER_SUBJECT = "license.resource.limit.subject";
    private static final String LICENSE_RESOURCE_LIMIT_MESSAGE = "license.resource.limit.message";

    @com.atlassian.event.api.EventListener
    public void handleEvent(LicenseResourceLimitEvent event)
    {
        try
        {
            String email = propertyManager.getNotificationEmail();
            String subject = i18nHelper.getText(MAILER_SUBJECT);
            String body = formatBody(event);

            mailManager.sendEmail(new InternetAddress(email), subject, body);
        }
        catch (MailSendException e)
        {
            throw new EventJobExecutionException(e.getMessage(), e);
        }
        catch (PropertyManagerException e)
        {
            throw new EventJobExecutionException(e.getMessage(), e);
        }
        catch (AddressException e)
        {
            throw new EventJobExecutionException(e.getMessage(), e);
        }
    }

    String formatBody(LicenseResourceLimitEvent event)
    {
        return i18nHelper.getText(LICENSE_RESOURCE_LIMIT_MESSAGE, ImmutableList.of(crowdLicenseManager.getLicense().getMaximumNumberOfUsers(), event.getCurrentUserCount()));
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    public void setI18nHelper(I18nHelper i18nHelper)
    {
        this.i18nHelper = i18nHelper;
    }

    public void setCrowdLicenseManager(CrowdLicenseManager crowdLicenseManager)
    {
        this.crowdLicenseManager = crowdLicenseManager;
    }

    public void setMailManager(MailManager mailManager)
    {
        this.mailManager = mailManager;
    }
}
