package com.atlassian.crowd.importer.importers;

import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.mappers.jdbc.GroupMapper;
import com.atlassian.crowd.importer.mappers.jdbc.UserMapper;
import com.atlassian.crowd.importer.mappers.jdbc.UserMembershipMapper;
import com.atlassian.crowd.manager.directory.DirectoryManager;

import org.springframework.jdbc.core.RowMapper;

/**
 * Specific Confluence Hibernate/Atlassian-User implementation
 */
class ConfluenceHibernateImporter extends JdbcImporter
{
    // Atlassian-User SQL
    private static final String FIND_GROUPS_SQL = "SELECT groupname FROM groups ORDER BY groupname";
    private static final String FIND_USERS_SQL = "SELECT name, password, email, fullname FROM users";

    private static final String FIND_USER_GROUP_MEMBERSHIPS = "SELECT " + "(SELECT name from users where id=userid) as username, " + "(SELECT groupname from groups where id=groupid) as groupname " + "from local_members";

    public ConfluenceHibernateImporter(DirectoryManager directoryManager)
    {
        super(directoryManager);
    }

    @Override
    public String getSelectAllGroupsSQL()
    {
        return FIND_GROUPS_SQL;
    }

    @Override
    public String getSelectAllUsersSQL()
    {
        return FIND_USERS_SQL;
    }

    @Override
    public String getSelectAllUserGroupMembershipsSQL()
    {
        return FIND_USER_GROUP_MEMBERSHIPS;
    }

    @Override
    public RowMapper getGroupMapper(Configuration configuration)
    {
        return new GroupMapper("groupname", "groupname", configuration.getDirectoryID());
    }

    @Override
    public RowMapper getMembershipMapper()
    {
        return new UserMembershipMapper("username", "groupname");
    }

    @Override
    public RowMapper getUserMapper(Configuration configuration)
    {
        return new UserMapper(configuration, "name", "email", "fullname", "password");
    }
}