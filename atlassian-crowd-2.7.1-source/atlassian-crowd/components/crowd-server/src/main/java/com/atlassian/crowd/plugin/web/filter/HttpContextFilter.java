package com.atlassian.crowd.plugin.web.filter;

import com.atlassian.crowd.plugin.web.ExecutingHttpRequest;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet filter responsible for putting the current HTTP request and session in a thread local.
 *
 * @since v2.7
 */
public class HttpContextFilter implements Filter
{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        // NO-OP
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException
    {
        try
        {
            ExecutingHttpRequest.set((HttpServletRequest) request, (HttpServletResponse) response);
            chain.doFilter(request, response);
        }
        finally
        {
            ExecutingHttpRequest.clear();
        }

    }

    @Override
    public void destroy()
    {
        // NO-OP
    }
}
