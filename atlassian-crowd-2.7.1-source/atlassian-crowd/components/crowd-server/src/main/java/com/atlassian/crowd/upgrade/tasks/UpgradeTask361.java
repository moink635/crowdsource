package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Set "ldap.roles.disabled" - LDAPPropertiesMapper.ROLES_DISABLED - to false if it's not set.
 */
public class UpgradeTask361 implements UpgradeTask
{
    private DirectoryManager directoryManager;
    private final Collection<String> errors = new ArrayList<String>();


    public String getBuildNumber()
    {
        return "361";
    }

    public String getShortDescription()
    {
        return "Setting ldap.roles.disabled=false for directories without the property set";
    }

    public void doUpgrade() throws Exception
    {
        List<Directory> directories = directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS));
        for (Directory directory : directories)
        {
            if (DirectoryType.CONNECTOR == directory.getType())
            {
                String disabled = directory.getValue(LDAPPropertiesMapper.ROLES_DISABLED);
                if (disabled == null)
                {
                    DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
                    // ldap.roles.disabled property is not present, so set to false.
                    directoryToUpdate.setAttribute(LDAPPropertiesMapper.ROLES_DISABLED, Boolean.FALSE.toString());
                    directoryManager.updateDirectory(directoryToUpdate);
                }
            }
        }
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }
}
