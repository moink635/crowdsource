package com.atlassian.crowd.manager.authentication;

import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.application.AliasManager;
import com.atlassian.crowd.manager.application.ApplicationAccessDeniedException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.authentication.ApplicationAuthenticationContext;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import java.util.Date;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

@Transactional
public class AliasingAwareTokenAuthenticationManager implements TokenAuthenticationManager
{
    private final TokenAuthenticationManager tokenAuthenticationManager;

    private final ApplicationManager applicationManager;

    private final AliasManager aliasManager;

    public AliasingAwareTokenAuthenticationManager(TokenAuthenticationManager tokenAuthenticationManager, ApplicationManager applicationManager, AliasManager aliasManager)
    {
        this.tokenAuthenticationManager = tokenAuthenticationManager;
        this.applicationManager = applicationManager;
        this.aliasManager = aliasManager;
    }

    @Override
    public Token authenticateApplication(ApplicationAuthenticationContext authenticationContext)
            throws InvalidAuthenticationException
    {
        return tokenAuthenticationManager.authenticateApplication(authenticationContext);
    }

    @Override
    public Token authenticateUser(UserAuthenticationContext authenticateContext)
            throws InvalidAuthenticationException, OperationFailedException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException, ApplicationNotFoundException
    {
        return authenticateUser(authenticateContext, null);
    }

    @Override
    public Token authenticateUser(UserAuthenticationContext authenticateContext, TokenLifetime tokenLifetime)
        throws InvalidAuthenticationException, OperationFailedException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException, ApplicationNotFoundException
    {
        final Application application = applicationManager.findByName(authenticateContext.getApplication());
        final String username = aliasManager.findUsernameByAlias(application, authenticateContext.getName());
        final UserAuthenticationContext aliasedAuthenticateContext = new UserAuthenticationContext(username, authenticateContext.getCredential(), authenticateContext.getValidationFactors(), authenticateContext.getApplication());

        return useAliasForToken(application, tokenAuthenticationManager.authenticateUser(aliasedAuthenticateContext, tokenLifetime));
    }

    @Override
    public Token authenticateUserWithoutValidatingPassword(UserAuthenticationContext authenticateContext)
            throws InvalidAuthenticationException, OperationFailedException, InactiveAccountException, ApplicationAccessDeniedException, ApplicationNotFoundException
    {
        final Application application = applicationManager.findByName(authenticateContext.getApplication());
        final String username = aliasManager.findUsernameByAlias(application, authenticateContext.getName());
        final UserAuthenticationContext aliasedAuthenticateContext = new UserAuthenticationContext(username, authenticateContext.getCredential(), authenticateContext.getValidationFactors(), authenticateContext.getApplication());

        return useAliasForToken(application, tokenAuthenticationManager.authenticateUserWithoutValidatingPassword(aliasedAuthenticateContext));
    }

    @Override
    public Token validateApplicationToken(String tokenKey, ValidationFactor[] validationFactors)
            throws InvalidTokenException
    {
        return tokenAuthenticationManager.validateApplicationToken(tokenKey, validationFactors);
    }

    @Override
    public Token validateUserToken(String userTokenKey, ValidationFactor[] validationFactors, String application)
            throws InvalidTokenException, ApplicationAccessDeniedException, OperationFailedException
    {
        try
        {
            return useAliasForToken(application, tokenAuthenticationManager.validateUserToken(userTokenKey, validationFactors, application));
        }
        catch (ApplicationNotFoundException e)
        {
            throw new com.atlassian.crowd.exception.runtime.OperationFailedException(e);
        }
    }

    @Override
    public void invalidateToken(String token)
    {
        tokenAuthenticationManager.invalidateToken(token);
    }

    @Override
    public void removeExpiredTokens()
    {
        tokenAuthenticationManager.removeExpiredTokens();
    }

    @Override
    public User findUserByToken(String key, String applicationName)
            throws InvalidTokenException, OperationFailedException, ApplicationNotFoundException, ApplicationAccessDeniedException
    {
        final Application application = applicationManager.findByName(applicationName);
        final UserTemplate user = new UserTemplate(tokenAuthenticationManager.findUserByToken(key, applicationName));

        // Replace username with alias if the user has an alias.
        final String alias = aliasManager.findAliasByUsername(application, user.getName());

        // Lower case the username if required
        final String username = application.isLowerCaseOutput() ? toLowerCase(alias) : alias;

        user.setName(username);
        return user;
    }

    Token useAliasForToken(String applicationName, Token originalToken) throws ApplicationNotFoundException
    {
        // Replace username with alias if the user has an alias
        final Application application = applicationManager.findByName(applicationName);
        return useAliasForToken(application, originalToken);
    }

    Token useAliasForToken(Application application, Token originalToken) throws ApplicationNotFoundException
    {
        final String alias = aliasManager.findAliasByUsername(application, originalToken.getName());

        // Lower case the username if required
        final String username = application.isLowerCaseOutput() ? toLowerCase(alias) : alias;

        return new Token.Builder(originalToken).setName(username).create();
    }

    @Override
    public Token findUserTokenByKey(String tokenKey, String applicationName)
        throws InvalidTokenException, ApplicationAccessDeniedException, OperationFailedException,
               ApplicationNotFoundException
    {
        Token originalToken = tokenAuthenticationManager.findUserTokenByKey(tokenKey, applicationName);

        // Replace username with alias if the user has an alias
        return useAliasForToken(applicationName, originalToken);
    }

    @Override
    public List<Application> findAuthorisedApplications(User user, String applicationName)
            throws OperationFailedException, DirectoryNotFoundException, ApplicationNotFoundException
    {
        final Application application = applicationManager.findByName(applicationName);
        final UserTemplate userTemplate = new UserTemplate(user);
        userTemplate.setName(aliasManager.findUsernameByAlias(application, user.getName()));
        return tokenAuthenticationManager.findAuthorisedApplications(userTemplate, applicationName);
    }

    @Override
    public void invalidateTokensForUser(String username, String exclusionToken, String applicationName)
            throws UserNotFoundException, ApplicationNotFoundException
    {
        final Application application = applicationManager.findByName(applicationName);
        String realUsername = aliasManager.findUsernameByAlias(application, username);
        tokenAuthenticationManager.invalidateTokensForUser(realUsername, exclusionToken, applicationName);
    }

    @Override
    public Date getTokenExpiryTime(Token token)
    {
        return tokenAuthenticationManager.getTokenExpiryTime(token);
    }
}
