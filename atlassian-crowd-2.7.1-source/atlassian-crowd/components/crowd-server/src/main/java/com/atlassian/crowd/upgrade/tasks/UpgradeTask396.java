package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Disables caching/monitoring if roles have been enabled.
 */
public class UpgradeTask396 implements UpgradeTask
{
    private DirectoryManager directoryManager;

    private final Collection<String> errors = new ArrayList<String>();
    /** ATTRIBUTE_KEY_USE_CACHING constant used to live in DirectoryImpl, but is not used in production any more */
    public static final String ATTRIBUTE_KEY_USE_CACHING = "useCaching";
    /** ATTRIBUTE_KEY_USE_MONITORING constant used to live in DirectoryImpl, but is not used in production any more */
    public static final String ATTRIBUTE_KEY_USE_MONITORING = "useMonitoring";

    public String getBuildNumber()
    {
        return "396";
    }

    public String getShortDescription()
    {
        return "Disabling caching for LDAP directories with roles enabled";
    }

    public void doUpgrade() throws Exception
    {
        List<Directory> ldapDirectories = directoryManager.searchDirectories(
                                            QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                                                        .with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.CONNECTOR))
                                                        .returningAtMost(EntityQuery.ALL_RESULTS));

        for (Directory directory : ldapDirectories)
        {
            if (!Boolean.parseBoolean(directory.getValue(LDAPPropertiesMapper.ROLES_DISABLED)))
            {
                // disable caching for ldap directory with roles enabled
                DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
                directoryToUpdate.setAttribute(ATTRIBUTE_KEY_USE_CACHING, Boolean.FALSE.toString());
                directoryToUpdate.setAttribute(ATTRIBUTE_KEY_USE_MONITORING, Boolean.FALSE.toString());
                directoryManager.updateDirectory(directoryToUpdate);
            }
        }
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryManager(final DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }
}
