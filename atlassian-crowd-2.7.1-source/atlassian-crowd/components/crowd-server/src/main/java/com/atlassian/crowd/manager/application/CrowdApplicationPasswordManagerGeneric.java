package com.atlassian.crowd.manager.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.authentication.ApplicationAuthenticationContext;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ResourceLocator;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.PasswordHelper;
import com.atlassian.crowd.util.PropertyUtils;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

public class CrowdApplicationPasswordManagerGeneric implements CrowdApplicationPasswordManager
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private TokenAuthenticationManager tokenAuthenticationManager;
    private ApplicationManager applicationManager;
    private PasswordHelper passwordHelper;
    private PropertyUtils propertyUtils;
    private ClientProperties clientProperties;
    private I18nHelper i18nHelper;
    private ResourceLocator resourceLocator;

    public void resetCrowdPasswordIfRequired() throws ApplicationManagerException, ApplicationNotFoundException
    {
        try
        {
            ApplicationAuthenticationContext applicationAuthContext = new ApplicationAuthenticationContext();
            applicationAuthContext.setName(clientProperties.getApplicationName());
            applicationAuthContext.setCredential(new PasswordCredential(clientProperties.getApplicationPassword()));

            tokenAuthenticationManager.authenticateApplication(applicationAuthContext);

            // everything is fine as crowd authed ok
        }
        catch (InvalidAuthenticationException e)
        {
            // this happens for crowd 1.1.x and 1.0.x xml backups where the crowd.properties aren't in the database
            logger.info("Password in crowd.properties does not match imported data. Resetting the Crowd application's password in the database.");

            Application crowdApp = applicationManager.findByName(toLowerCase(i18nHelper.getText("application.name")));

            final String password = passwordHelper.generateRandomPassword();
            final PasswordCredential credential = new PasswordCredential(password);

            applicationManager.updateCredential(crowdApp, credential);

            propertyUtils.updateProperty(resourceLocator.getResourceLocation(), com.atlassian.crowd.integration.Constants.PROPERTIES_FILE_APPLICATION_PASSWORD, password);

            logger.info("Password for Crowd app has been reset in the database and crowd.properties");
        }

        // get a fresh reload of crowd.properties
        clientProperties.updateProperties(resourceLocator.getProperties());
    }

    public void setApplicationManager(final ApplicationManager applicationManager)
    {
        this.applicationManager = applicationManager;
    }

    public void setPasswordHelper(final PasswordHelper passwordHelper)
    {
        this.passwordHelper = passwordHelper;
    }

    public void setPropertyUtils(final PropertyUtils propertyUtils)
    {
        this.propertyUtils = propertyUtils;
    }

    public void setI18nHelper(final I18nHelper i18nHelper)
    {
        this.i18nHelper = i18nHelper;
    }

    public void setResourceLocator(final ResourceLocator resourceLocator)
    {
        this.resourceLocator = resourceLocator;
    }

    public void setTokenAuthenticationManager(TokenAuthenticationManager tokenAuthenticationManager)
    {
        this.tokenAuthenticationManager = tokenAuthenticationManager;
    }

    public void setClientProperties(final ClientProperties clientProperties)
    {
        this.clientProperties = clientProperties;
    }
}
