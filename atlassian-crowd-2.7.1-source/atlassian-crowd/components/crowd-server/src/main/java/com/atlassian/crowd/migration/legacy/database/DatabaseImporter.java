package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.LegacyImportDataHolder;
import com.atlassian.crowd.migration.legacy.database.sql.LegacyTableQueries;

/**
 * A mapper that will handle migrating the data from a legacy Crowd (pre 2.0) database to the Crowd 2.0+ database schema.
 */
public interface DatabaseImporter
{
    /**
     * Migrates data from legacy Crowd database (pre 2.0) to current database (Crowd 2.0+) schema.
     *
     * @param importData stores significant import results
     */
    public void importFromDatabase(LegacyImportDataHolder importData) throws ImportException;

    /**
     * The set of SQL queries that is compatible with the database in use (eg. postgres, mysql).
     *
     * @param legacyTableQueries SQL queries for the database in use
     */
    public void setLegacyTableQueries(LegacyTableQueries legacyTableQueries);
}
