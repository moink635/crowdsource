package com.atlassian.crowd.plugin.descriptors;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.spring.container.SpringContainerContext;
import com.opensymphony.util.TextUtils;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;

public class ComponentModuleDescriptor extends AbstractModuleDescriptor implements ApplicationContextAware
{
    private static final Logger log = LoggerFactory.getLogger(ComponentModuleDescriptor.class);
    String alias;

    private ApplicationContext applicationContext;

    public ComponentModuleDescriptor(ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    public void init(Plugin plugin, Element element) throws PluginParseException
    {
        super.init(plugin, element);

        alias = element.attributeValue("alias");

        if (!TextUtils.stringSet(alias))
        {
            alias = getKey();
        }
    }

    public Object getModule()
    {
        throw new UnsupportedOperationException("You cannot retrieve a component instance - Spring-ified");
    }

    public void enabled()
    {
        super.enabled();
        DefaultListableBeanFactory beanFactory = getGlobalBeanFactory();

        if (beanFactory != null)
        {
            // We should not overwrite an existing definition unless we were able to replace it when we uninstall this
            // module. Since this is currently not supported, disallowing the overwrite will have to do for now.
            // NOTE: It is not as simple as maintaining a reference to the previous value. Multiple plugins could
            //       accidentally overwrite the same value creating a potential mess when we start uninstalling them.
            if (beanFactory.containsBean(alias))
            {
                throw new IllegalStateException("Can not overwrite an existing bean definition: " + alias);
            }

            log.debug("Creating bean definition for " + alias + " with class " + getModuleClass().getName());
            final RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(getModuleClass(), AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
            log.debug("Registering bean definition for " + alias + " with class " + getModuleClass().getName());
            beanFactory.registerBeanDefinition(alias, rootBeanDefinition);

            Object o = beanFactory.getBean(alias);

            if (o instanceof StateAware)
            {
                StateAware sa = (StateAware) o;
                sa.enabled();
            }
        }
    }

    public void disabled()
    {
        if (log.isDebugEnabled())
        {
            log.debug("Disabling component module " + getKey());
        }

        DefaultListableBeanFactory beanFactory = getGlobalBeanFactory();

        if (beanFactory != null)
        {
            Object o = null;
            try
            {
                o = beanFactory.getBean(alias);
            }
            catch (NoSuchBeanDefinitionException nsbe)
            {
                // ignore this
            }
            if (o != null && o instanceof StateAware)
            {
                StateAware sa = (StateAware) o;
                sa.disabled();
            }

            log.debug("Removing bean definition for " + alias);
            beanFactory.removeBeanDefinition(alias);
        }
        super.disabled();
    }

    private DefaultListableBeanFactory getGlobalBeanFactory()
    {
        if (applicationContext instanceof ConfigurableApplicationContext)
        {
            ConfigurableApplicationContext configurableApplicationContext = (ConfigurableApplicationContext) applicationContext;
            if (configurableApplicationContext.getBeanFactory() instanceof DefaultListableBeanFactory)
            {
                return (DefaultListableBeanFactory) configurableApplicationContext.getBeanFactory();
            }
            else
            {
                log.error("Failed to lookup global bean factory - BeanFactory was not a DefaultListableBeanFactory?");
            }
        }
        else
        {
            log.error("Failed to lookup global bean factory - ApplicationContext was not a ConfigurableApplicationContext?");
        }
        return null;
    }

    /**
     * Support autowire of application context
     *
     * @param applicationContext
     * @throws org.springframework.beans.BeansException
     *
     */
    public void setApplicationContext(ApplicationContext applicationContext)
    {
        this.applicationContext = applicationContext;
    }

    private ApplicationContext getApplicationContextWithHack()
    {
        if (this.applicationContext != null)
        {
            return applicationContext;
        }

        /*
        TODO: Now that the ApplicationContextAware interface gets processed properly by the module factory this should no longer be necessary
        I'll add this warning for now and if we don't see it we should axe this code
        */
        log.warn("Using hacks to get application context");

        // application context has not been wired, so we need to dig for it ourselves.
        ContainerContext containerContext = ContainerManager.getInstance().getContainerContext();
        if (containerContext instanceof SpringContainerContext)
        {
            SpringContainerContext springContainerContext = (SpringContainerContext) containerContext;
            ServletContext servletContext = springContainerContext.getServletContext();
            if (servletContext != null)
            {
                return WebApplicationContextUtils.getWebApplicationContext(servletContext);
            }
        }
        return null;
    }
}