package com.atlassian.crowd.manager.token;

import com.atlassian.crowd.dao.token.SessionTokenStorage;

/**
 * A marker interface for a {@link SessionTokenStorage} that should be
 * used for token management.
 */
public interface TokenManager extends SessionTokenStorage
{
}
