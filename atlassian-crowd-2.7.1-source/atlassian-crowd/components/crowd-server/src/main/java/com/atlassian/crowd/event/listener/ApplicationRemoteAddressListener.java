package com.atlassian.crowd.event.listener;

import com.atlassian.crowd.event.application.ApplicationRemoteAddressAddedEvent;
import com.atlassian.crowd.event.application.ApplicationRemoteAddressRemovedEvent;
import com.atlassian.crowd.util.InetAddressCacheUtil;

/**
 * Listens to events affecting the Application's list of permitted remote addresses.
 */
public class ApplicationRemoteAddressListener
{
    private InetAddressCacheUtil cacheUtil;

    @com.atlassian.event.api.EventListener
    public void handleEvent(ApplicationRemoteAddressAddedEvent event)
    {
        cacheUtil.clearCache();
    }

    @com.atlassian.event.api.EventListener
    public void handleEvent(ApplicationRemoteAddressRemovedEvent event)
    {
        cacheUtil.clearCache();
    }

    public void setRemoteAddressCacheUtil(InetAddressCacheUtil cacheUtil)
    {
        this.cacheUtil = cacheUtil;
    }
}
