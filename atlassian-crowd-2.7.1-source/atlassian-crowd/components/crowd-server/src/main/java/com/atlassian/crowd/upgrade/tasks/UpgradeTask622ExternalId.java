package com.atlassian.crowd.upgrade.tasks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Properties;

import javax.annotation.Nullable;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Updates LDAP and delegating directories configuration by adding the User Unique Id attribute if absent.
 * Default value is a function of the directory type (example: entryUUID for OpenLDAP).
 * Updating configuration allows to track user renames from LDAP servers (after full sync).
 *
 * @since 2.7
 */
public class UpgradeTask622ExternalId implements UpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask622ExternalId.class);

    private final DirectoryDao directoryDao;
    private final LDAPPropertiesHelper ldapPropertiesHelper;

    private final Collection<String> errors = new ArrayList<String>();

    public UpgradeTask622ExternalId(DirectoryDao directoryDao, LDAPPropertiesHelper ldapPropertiesHelper)
    {
        this.directoryDao = directoryDao;
        this.ldapPropertiesHelper = ldapPropertiesHelper;
    }

    @Override
    public String getBuildNumber()
    {
        return "622";
    }

    @Override
    public String getShortDescription()
    {
        return "Upgrades directories configuration with the User Unique Id attribute";
    }

    private static final EntityQuery<Directory> LDAP_CONNECTORS_AND_DELEGATING_DIRECTORIES_QUERY = QueryBuilder
        .queryFor(Directory.class, EntityDescriptor.directory())
        .with(Combine.anyOf(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.CONNECTOR),
                            Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.DELEGATING)))
        .returningAtMost(EntityQuery.ALL_RESULTS);

    @Override
    public void doUpgrade() throws Exception
    {
        for (Directory directory : directoryDao.search(LDAP_CONNECTORS_AND_DELEGATING_DIRECTORIES_QUERY))
        {
            if (StringUtils.isBlank(directory.getValue(LDAPPropertiesMapper.LDAP_EXTERNAL_ID)))
            {
                log.debug("Upgrading directory {}", directory);
                upgradeDirectory(directory);
            }
        }
    }

    private void upgradeDirectory(Directory directory)
    {
        Properties properties = getPropertiesForDirectory(getClassNameForDirectory(directory));
        if (properties != null && properties.containsKey(LDAPPropertiesMapper.LDAP_EXTERNAL_ID))
        {
            String defaultValue = properties.getProperty(LDAPPropertiesMapper.LDAP_EXTERNAL_ID);
            if (StringUtils.isNotBlank(defaultValue))
            {
                DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
                directoryToUpdate.setAttribute(LDAPPropertiesMapper.LDAP_EXTERNAL_ID, defaultValue);
                try
                {
                    directoryDao.update(directoryToUpdate);
                }
                catch (DirectoryNotFoundException e)
                {
                    final String errorMessage = "Could not update directory " + directory;
                    log.error(errorMessage, e);
                    errors.add(errorMessage + ", error is " + e.getMessage());
                }
            }
        }
    }

    @Nullable
    private Properties getPropertiesForDirectory(String ldapClass)
    {
        return (ldapClass != null) ? ldapPropertiesHelper.getConfigurationDetails().get(ldapClass) : null;
    }

    @Nullable
    private static String getClassNameForDirectory(Directory directory)
    {
        switch (directory.getType())
        {
            case CONNECTOR:
                return directory.getImplementationClass();
            case DELEGATING:
                return directory.getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS);
            default:
                // we do not provide external id in other types of directories
                return null;
        }
    }

    @Override
    public Collection<String> getErrors()
    {
        return Collections.unmodifiableCollection(errors);
    }
}
