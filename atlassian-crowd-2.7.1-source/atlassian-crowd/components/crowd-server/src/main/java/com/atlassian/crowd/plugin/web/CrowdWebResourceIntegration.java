package com.atlassian.crowd.plugin.web;

import java.io.File;
import java.util.Map;

import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.util.build.BuildUtils;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceIntegration;

import com.google.common.collect.ImmutableMap;

/**
 * Crowd specific implementation of the Atlassian Plugin WebResourceIntegration
 * <b>Note: </b>Bamboo, Confluence and Crowd are all using the same implementation here!
 */
public class CrowdWebResourceIntegration implements WebResourceIntegration
{
    private final PluginAccessor pluginAccessor;
    private final ClientProperties clientProperties;

    public CrowdWebResourceIntegration(PluginAccessor pluginAccessor, ClientProperties clientProperties)
    {
        this.pluginAccessor = pluginAccessor;
        this.clientProperties = clientProperties;
    }

    public PluginAccessor getPluginAccessor()
    {
        return pluginAccessor;
    }

    public Map<String, Object> getRequestCache()
    {
        return RequestCacheThreadLocal.getRequestCache();
    }

    public String getSystemCounter()
    {
        // currently, the system counter is unused and hardwired to return 1. If we implement a "big red button"
        // cache reset, it will have to be changed.

        if (Boolean.getBoolean("atlassian.disable.caches"))
        {
            return (String.valueOf((int) (Math.random() * 100000) + 1));
        }

        return "1";
    }

    public String getSystemBuildNumber()
    {
        return BuildUtils.BUILD_NUMBER;
    }

    public String getBaseUrl()
    {
        return getBaseUrl(UrlMode.AUTO);
    }

    public String getBaseUrl(UrlMode urlMode)
    {
        switch (urlMode)
        {
            case AUTO:
            case RELATIVE:
                return getRelativeBaseUrl();

            case ABSOLUTE:
                return clientProperties.getBaseURL();

            default:
                return null;
        }
    }

    private String getRelativeBaseUrl()
    {
        String baseURL = null;

        if (ExecutingHttpRequest.get() != null)
        {
            baseURL = ExecutingHttpRequest.get().getContextPath();
        }
        else if (RequestCacheThreadLocal.getContextPath() != null)
        {
            baseURL = RequestCacheThreadLocal.getContextPath();
        }

        // TODO we should have a default defined in Crowd.

        return baseURL;
    }

    public String getSuperBatchVersion()
    {
        return "1"; // super batch is not used in Crowd.
    }

    @Override
    public String getStaticResourceLocale()
    {
        return null;
    }

    @Override
    public File getTemporaryDirectory()
    {
        return new File(System.getProperty("java.io.tmpdir"), "webresources");
    }

    private static final Map<String, String> PDL = ImmutableMap.of("pdl.dir", "");

    @Override
    public Map<String, String> getResourceSubstitutionVariables()
    {
        return PDL;
    }
}
