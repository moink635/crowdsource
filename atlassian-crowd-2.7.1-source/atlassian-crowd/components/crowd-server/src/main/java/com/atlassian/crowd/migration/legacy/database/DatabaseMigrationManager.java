package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.config.ApplicationConfig;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.database.sql.HSQLLegacyTableQueries;
import com.atlassian.crowd.migration.legacy.database.sql.LegacyTableQueries;
import com.atlassian.crowd.migration.legacy.database.sql.MySQLLegacyTableQueries;
import com.atlassian.crowd.migration.legacy.database.sql.PostgresLegacyTableQueries;
import com.atlassian.crowd.migration.legacy.database.sql.SqlServerLegacyTableQueries;
import com.atlassian.crowd.migration.verify.DatabaseVerificationManager;
import com.atlassian.crowd.migration.verify.SupportedDatabaseVerifier;
import com.atlassian.crowd.util.SystemInfoHelper;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public class DatabaseMigrationManager
{
    private static final Logger logger = LoggerFactory.getLogger(DatabaseMigrationManager.class);
    private final LegacyTableQueries legacyTableQueries;

    private final DatabaseMigrator legacyDatabaseMigrator;
    private final ApplicationConfig applicationConfig;
    private final DatabaseVerificationManager verificationManager;

    public DatabaseMigrationManager(DatabaseMigrator legacyDatabaseMigrator, ApplicationConfig applicationConfig, DatabaseVerificationManager verificationManager, SystemInfoHelper systemInfoHelper)
    {
        this.legacyDatabaseMigrator = legacyDatabaseMigrator;
        this.applicationConfig = applicationConfig;
        this.verificationManager = verificationManager;

        this.legacyTableQueries = getTableQueryForDriver(systemInfoHelper.getDatabaseHibernateDialect());
    }

    public Collection<String> doDatabaseMigration() throws ImportException
    {
        logger.info("Migrating existing data from legacy database into new schema.");

        return Lists.newArrayList(legacyDatabaseMigrator.importDatabase(legacyTableQueries));
    }

    /**
     * Will validate if we are safe to upgrade or not. If we are not a non-empty Collection of errors will
     * be returned
     * @return An empty Collection of errors if everything is safe, or a collection of error strings if things are not safe.
     */
    public Collection<String> verifyDatabaseIsSafeToMigrate()
    {
        final Collection<String> errors = Lists.newArrayList();

        if (isLegacyCrowd())
        {
            // Crowd is pre 2.0 - migration can be attempted. Check if it is going to be safe to migrate
            logger.info("Legacy Crowd version detected <" +applicationConfig.getBuildNumber() + ">.");

            verificationManager.setLegacyTableQueries(legacyTableQueries);

            // Validate we are safe for in-place upgrade
            errors.addAll(verificationManager.validate());

            if (!errors.isEmpty())
            {
                logger.error("An in-place upgrade is not possible for your database, for the following reasons: " + StringUtils.join(errors, "\n"));
            }
        }

        return errors;
    }

    public boolean isLegacyCrowd()
    {
        final int buildNumber = NumberUtils.toInt(applicationConfig.getBuildNumber());

        return buildNumber > 0 && buildNumber < 395;
    }

    private LegacyTableQueries getTableQueryForDriver(String hibernateDialect)
    {
        if (SupportedDatabaseVerifier.isPostgreSQL(hibernateDialect))
        {
            return new PostgresLegacyTableQueries();
        }

        if (SupportedDatabaseVerifier.isMySQL(hibernateDialect))
        {
            return new MySQLLegacyTableQueries();
        }

        if (SupportedDatabaseVerifier.isHSQLDB(hibernateDialect))
        {
            return new HSQLLegacyTableQueries();
        }

        if (SupportedDatabaseVerifier.isMsSQLServer(hibernateDialect))
        {
            return new SqlServerLegacyTableQueries();
        }
        
        logger.warn("No LegacyTablesQuery available for: " + hibernateDialect);
        
        // Didn't match any of the dialects we support
        return null;
    }
}
