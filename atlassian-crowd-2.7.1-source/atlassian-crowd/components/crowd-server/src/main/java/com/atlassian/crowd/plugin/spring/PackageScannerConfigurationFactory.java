package com.atlassian.crowd.plugin.spring;

import com.atlassian.crowd.util.build.BuildUtils;
import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.spring.SpringAwarePackageScannerConfiguration;
import org.springframework.beans.factory.FactoryBean;

import java.util.List;
import java.util.Map;

/**
 * Factory for the PackageScannerConfiguration to provide the scanner with Crowd's version number on startup.
 */
public class PackageScannerConfigurationFactory implements FactoryBean
{
    private final List<String> packageIncludes;
    private final List<String> packageExcludes;
    private final Map<String, String> packageVersions;
    private final SpringAwarePackageScannerConfiguration config;

    public PackageScannerConfigurationFactory(List<String> packageIncludes,
                                              List<String> packageExcludes,
                                              Map<String, String> packageVersions)
    {
        this.packageIncludes = packageIncludes;
        this.packageExcludes = packageExcludes;
        this.packageVersions = packageVersions;

        config = new SpringAwarePackageScannerConfiguration(BuildUtils.BUILD_VERSION);
        config.setPackageIncludes(packageIncludes);
        config.setPackageExcludes(packageExcludes);
        config.setPackageVersions(packageVersions);
    }

    public Object getObject() throws Exception
    {
        return config;
    }

    public Class getObjectType()
    {
        return PackageScannerConfiguration.class;
    }

    public boolean isSingleton()
    {
        return true;
    }
}
