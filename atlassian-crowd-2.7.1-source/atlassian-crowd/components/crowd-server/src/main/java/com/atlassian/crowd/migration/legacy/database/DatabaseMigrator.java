package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.LegacyImportDataHolder;
import com.atlassian.crowd.migration.legacy.database.sql.LegacyTableQueries;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class DatabaseMigrator
{
    private static final Logger logger = LoggerFactory.getLogger(DatabaseMigrator.class);

    private List<DatabaseImporter> mappers;

    /**
     * Will import a database with the given legacyTableQueries
     * @param legacyTableQueries the queries to use for import
     * @return any errors that occurred during import
     */
    public List<String> importDatabase(LegacyTableQueries legacyTableQueries)
    {
        // The order the mappers are run are specified in the applicationContext-CrowdMappers.xml file
        // They have to be run in that specific order!
        LegacyImportDataHolder importData = new LegacyImportDataHolder();
        List<String> errors = new ArrayList<String>();

        if (legacyTableQueries != null)
        {
            long startTime = System.currentTimeMillis();
            for (DatabaseImporter mapper : mappers)
            {
                logger.info("Migrating legacy database using: <" + mapper.getClass().getName() + ">");
                try
                {
                    mapper.setLegacyTableQueries(legacyTableQueries);
                    mapper.importFromDatabase(importData);
                }
                catch (ImportException e)
                {
                    logger.error("When migrating database", e);
                    errors.add("Error occurred when migrating database");
                }
            }

            logger.info("Time taken to migrate legacy database (millis): " + String.valueOf(System.currentTimeMillis() - startTime));
        }
        else
        {
            errors.add("Unable to find the appropriate queries for the database. Automatic migration for this database may not be supported.");
        }

        return errors;
    }

    public void setMappers(List<DatabaseImporter> mappers)
    {
        this.mappers = mappers;
    }
}
