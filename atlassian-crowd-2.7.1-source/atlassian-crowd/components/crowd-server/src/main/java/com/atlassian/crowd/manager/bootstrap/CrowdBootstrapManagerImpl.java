package com.atlassian.crowd.manager.bootstrap;

import com.atlassian.config.ConfigurationException;
import com.atlassian.config.bootstrap.BootstrapException;
import com.atlassian.config.bootstrap.DefaultAtlassianBootstrapManager;
import com.atlassian.config.lifecycle.events.ApplicationStartedEvent;
import com.atlassian.crowd.plugin.PluginUtils;
import com.atlassian.crowd.util.persistence.hibernate.MappingResources;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.spring.container.ContainerManager;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.config.Configuration;
import com.opensymphony.xwork.config.ConfigurationManager;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletContext;

/**
 * BootstrapManager is responsible for initializing the dependencies of Crowd environment.
 * <ol>
 * <li> Initializing the Crowd environment, beginning with loading crowd.cfg.xml from the crowd.home
 * property specified in crowd-init.properties.
 * <li> Delegating, monitoring, and answering all client requests concerning Crowd initialization routines.
 * <li> Providing the SetupPersister with whatever resources it needs to let a user bootstrapManager Crowd.
 * </ol>
 */
public class CrowdBootstrapManagerImpl extends DefaultAtlassianBootstrapManager implements CrowdBootstrapManager
{
    public String getConfigDirectory()
    {
        return (String) getProperty(CONFIG_DIRECTORY_PARAM);
    }

    public void setConfigDirectory(String configurationDirectory)
    {
        setProperty(CONFIG_DIRECTORY_PARAM, configurationDirectory);
    }

    @Override
    protected void postBootstrapDatabase() throws BootstrapException
    {
        super.postBootstrapDatabase();

        // wire up webwork to use pluginized actions and results
        PluginUtils.initialiseWebworkForPluginSupport(ServletActionContext.getServletContext());

        // Forces xwork to be reloaded after setup
        Configuration config = ConfigurationManager.getConfiguration();
        config.reload();

        // If we are here, everything hopefully went ok, so throw an Application Started Event
        EventPublisher eventPublisher = (EventPublisher) ContainerManager.getComponent("eventPublisher");
        eventPublisher.publish(new ApplicationStartedEvent(this));
    }

    /**
     * @param servletContext servlet context.
     * @return true if the bootstrap succeeded and Hibernate is set up.
     *         This means the WebApplicationContext has been set up for Spring.
     */
    public static boolean isContainterReady(ServletContext servletContext)
    {
        return (WebApplicationContextUtils.getWebApplicationContext(servletContext) != null);
    }

    @Override
    public String getServerID()
    {
        return getString(CROWD_SID);
    }

    @Override
    public void setServerID(String sid) throws ConfigurationException
    {
        setProperty(CROWD_SID, sid);
        save();
    }

    public void setMappingResources(MappingResources mappingResources)
    {
        setTables(mappingResources.getTableNames());
    }
}

