package com.atlassian.crowd.plugin;

import com.atlassian.config.HomeLocator;
import com.atlassian.crowd.plugin.descriptors.webwork.PluginAwareObjectFactory;
import com.atlassian.crowd.plugin.descriptors.webwork.PluginClassTemplateLoader;
import com.atlassian.plugin.AutowireCapablePlugin;
import com.atlassian.plugin.Plugin;
import com.opensymphony.webwork.spring.WebWorkSpringObjectFactory;
import com.opensymphony.webwork.views.freemarker.FreemarkerManager;
import com.opensymphony.xwork.ObjectFactory;
import freemarker.cache.MultiTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.template.TemplateException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletContext;
import java.io.File;

public class PluginUtils
{
    private static final Logger log = LoggerFactory.getLogger(PluginUtils.class);

    public static final String CROWD_APPLICATION_KEY = "crowd";

    public static final String PLUGIN_DESCRIPTOR_FILENAME = "atlassian-plugin.xml";

    public static void initialiseWebworkForPluginSupport(ServletContext servletContext)
    {
        // Forces WebWork to use the spring object factory
        WebWorkSpringObjectFactory objectFactory = new PluginAwareObjectFactory();
        objectFactory.init(servletContext);
        ObjectFactory.setObjectFactory(objectFactory);
        try
        {
            freemarker.template.Configuration fmConfig = FreemarkerManager.getInstance().getConfiguration(servletContext);

            MultiTemplateLoader loader = new MultiTemplateLoader(new TemplateLoader[] {
                    fmConfig.getTemplateLoader(),
                    new PluginClassTemplateLoader()
            });

            fmConfig.setTemplateLoader(loader);
        }
        catch (TemplateException e)
        {
            log.error("Error updating Freemarker configuration. WebWork plugins may have issues rendering Freemarker results." + e.getMessage(), e);
        }
    }

    public static <T> T instantiateModule(Plugin plugin, Class<T> moduleClass, ApplicationContext applicationContext)
    {
        if (plugin instanceof AutowireCapablePlugin)
        {
            return (T) ((AutowireCapablePlugin) plugin).autowire(moduleClass);
        }
        else
        {
            try
            {

                T listenerObject = moduleClass.newInstance();
                applicationContext.getAutowireCapableBeanFactory().autowireBeanProperties(listenerObject, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
                return listenerObject;
            }
            catch (InstantiationException e)
            {
                log.error(e.getMessage());
            }
            catch (IllegalAccessException e)
            {
                log.error(e.getMessage());
            }
        }

        return null;
    }

    public static File getHomeSubDirectory(HomeLocator homeLocator, String subDirectory)
    {
        File homePath = new File(homeLocator.getHomePath());
        File directory = new File(homePath, subDirectory);

        if (!directory.exists())
        {
            directory.mkdir();
        }

        return directory;
    }
}
