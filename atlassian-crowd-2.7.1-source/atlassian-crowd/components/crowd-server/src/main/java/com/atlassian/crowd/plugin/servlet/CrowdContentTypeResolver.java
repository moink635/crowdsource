package com.atlassian.crowd.plugin.servlet;

import com.atlassian.plugin.servlet.ContentTypeResolver;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

public class CrowdContentTypeResolver implements ContentTypeResolver, ServletContextAware
{
    private ServletContext servletContext;

    public String getContentType(final String requestUrl)
    {
        String contentType = servletContext.getMimeType(requestUrl);
        if (contentType == null)
        {
            return "application/octet-stream";
        }
        else
        {
            return contentType;
        }
    }

    public void setServletContext(final ServletContext servletContext)
    {
        this.servletContext = servletContext;

    }
}
