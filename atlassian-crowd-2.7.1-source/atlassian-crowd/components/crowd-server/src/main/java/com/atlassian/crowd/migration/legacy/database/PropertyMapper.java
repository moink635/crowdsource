package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.config.ConfigurationException;
import com.atlassian.crowd.dao.property.PropertyDAOHibernate;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.property.PropertyManagerGeneric;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.LegacyImportDataHolder;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PropertyMapper extends DatabaseMapper implements DatabaseImporter
{
    private final PropertyDAOHibernate propertyDAO;
    private final CrowdBootstrapManager bootstrapManager;

    public PropertyMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, JdbcOperations jdbcTemplate, PropertyDAOHibernate propertyDAO, CrowdBootstrapManager bootstrapManager)
    {
        super(sessionFactory, batchProcessor, jdbcTemplate);
        this.propertyDAO = propertyDAO;
        this.bootstrapManager = bootstrapManager;
    }

    public void importFromDatabase(LegacyImportDataHolder importData) throws ImportException
    {
        List<Property> properties = importPropertiesFromDatabase();
        for (Property property : properties)
        {
            addEntityViaSave(property);
        }
        logger.info("Successfully migrated " + properties.size() + " properties");
    }

    protected List<Property> importPropertiesFromDatabase() throws ImportException
    {
        List<Property> properties = new ArrayList<Property>();
        PropertyTableMapper propertyTableMapper = new PropertyTableMapper();
        Map<String, String> propertiesMap = attributeListToMap(jdbcTemplate.query(legacyTableQueries.getPropertiesSQL(), propertyTableMapper));
        for (Map.Entry<String, String> propertyMap : propertiesMap.entrySet())
        {
            // hack: need to migrate server id from db to cfg.xml
            String codeName = propertyMap.getKey();
            String value = propertyMap.getValue();

            if (codeName.equals("22") && StringUtils.isNotBlank(value))
            {
                try
                {
                    bootstrapManager.setServerID(value);
                }
                catch (ConfigurationException e)
                {
                    throw new ImportException(e);
                }
            }
            // return the new name from the code
            String name = getNameFromLegacyCode(codeName);
            if (name != null)
            {
                Property property = new Property(Property.CROWD_PROPERTY_KEY, name, value);
                properties.add(property);
            }
        }

        return properties;
    }

    private class PropertyTableMapper implements RowMapper
    {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            Map<String, String> propertyMap = new HashMap<String, String>();
            String codeName = rs.getString("NAME");
            String value = rs.getString("VALUE");
            propertyMap.put(codeName, value);
            return propertyMap;
        }
    }
}