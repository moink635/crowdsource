package com.atlassian.crowd.manager.mail;

import com.atlassian.crowd.CrowdConstants;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.mail.SMTPServer;
import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class MailManagerImpl implements MailManager
{
    private static final String SMTP_PROTOCOL = "smtp";
    private static final String SECURE_SMTP_PROTOCOL = "smtps";

    private static final String FORMAT_MAILER_SMTP_HOST = "mail.%s.host";
    private static final String FORMAT_MAILER_SMTP_PORT = "mail.%s.port";
    private static final String FORMAT_MAILER_SMTP_AUTH = "mail.%s.auth";

    // dependencies
    private PropertyManager propertyManager;
    private InitialContext initialContext;

    I18nHelper i18nHelper;

    public MailManagerImpl(PropertyManager propertyManager, InitialContext initialContext, I18nHelper i18nHelper)
    {
        this.propertyManager = propertyManager;
        this.initialContext = initialContext;
        this.i18nHelper = i18nHelper;
    }

    public void sendEmail(InternetAddress emailAddress, String subject, String body) throws MailSendException
    {
        try
        {
            // build the mail server information
            final SMTPServer smtpServer = propertyManager.getSMTPServer();
            Session session = getSession(smtpServer);

            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.setFrom(smtpServer.getFrom());
            mimeMessage.setSentDate(new Date());
            mimeMessage.addRecipient(MimeMessage.RecipientType.TO, emailAddress);
            mimeMessage.setSubject(smtpServer.getPrefix() + " " + subject);
            mimeMessage.setText(body, CrowdConstants.DEFAULT_CHARACTER_ENCODING);

            String protocol = getProtocol(smtpServer.getUseSSL());
            Transport t = session.getTransport(protocol);
            try
            {
                if (hasAuthentication(smtpServer))
                {
                    t.connect(smtpServer.getHost(), smtpServer.getUsername(), smtpServer.getPassword());
                }
                else
                {
                    t.connect();
                }
                t.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
            }
            finally
            {
                t.close();
            }

        }
        catch (Exception e)
        {
            throw new MailSendException(i18nHelper.getText("principal.resetpassword.error.invalid", emailAddress) + e.getMessage(), e);
        }
    }

    private Session getSession(SMTPServer smtpServer) throws NamingException
    {
        if (smtpServer.isJndiMailActive())
        {
            return getJndiMailSession(smtpServer);
        }
        else
        {
            return getHostMailSession(smtpServer);
        }
    }

    @Override
    public boolean isConfigured()
    {
        try
        {
            SMTPServer smtpServer = propertyManager.getSMTPServer();

            if (smtpServer.isJndiMailActive())
            {
                return StringUtils.isNotBlank(smtpServer.getJndiLocation());
            }
            else
            {
                return StringUtils.isNotBlank(smtpServer.getHost());
            }
        }
        catch (PropertyManagerException e)
        {
            return false;
        }
    }

    private Session getHostMailSession(final SMTPServer smtpServer) throws IllegalStateException
    {
        Session session;
        Authenticator auth = null;

        if (hasAuthentication(smtpServer))
        {
            auth = new Authenticator()
            {
                protected PasswordAuthentication getPasswordAuthentication()
                {
                    return new PasswordAuthentication(smtpServer.getUsername(), smtpServer.getPassword());
                }
            };
        }

        if (StringUtils.isBlank(smtpServer.getHost()))
        {
            throw new IllegalStateException("Mail server configured in Remote Host mode, but no remote host is specified in the database.");
        }

        String prot = getProtocol(smtpServer.getUseSSL());
        Properties properties = new Properties();
        properties.put(String.format(FORMAT_MAILER_SMTP_HOST, prot), smtpServer.getHost());
        properties.put(String.format(FORMAT_MAILER_SMTP_PORT, prot), Integer.toString(smtpServer.getPort()));

        if (auth == null)
        {
            session = Session.getInstance(properties);
        }
        else
        {
            properties.put(String.format(FORMAT_MAILER_SMTP_AUTH, prot), Boolean.TRUE.toString());
            session = Session.getInstance(properties, auth);
        }

        return session;
    }

    private Session getJndiMailSession(SMTPServer smtpServer) throws IllegalStateException, NamingException
    {
        Session session;
        if (StringUtils.isNotBlank(smtpServer.getJndiLocation()))
        {
            session = (Session) initialContext.lookup(smtpServer.getJndiLocation());
        }
        else
        {
            throw new IllegalStateException("Mail server configured in JNDI mode, but no JNDI location is specified in the database.");
        }
        return session;
    }

    private static String getProtocol(boolean useSSL)
    {
        return useSSL ? SECURE_SMTP_PROTOCOL : SMTP_PROTOCOL;
    }

    private static boolean hasAuthentication(SMTPServer smtpServer)
    {
        return StringUtils.isNotBlank(smtpServer.getUsername());
    }
}
