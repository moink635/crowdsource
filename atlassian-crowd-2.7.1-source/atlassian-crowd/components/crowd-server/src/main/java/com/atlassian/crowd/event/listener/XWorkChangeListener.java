package com.atlassian.crowd.event.listener;

import com.atlassian.crowd.event.XWorkStateChangeEvent;
import com.atlassian.event.api.EventPublisher;
import com.opensymphony.xwork.config.ConfigurationManager;

/**
 * Event listener that is interested in changes to the Xwork configuration
 */
public class XWorkChangeListener
{
    private final EventPublisher eventPublisher;

    public XWorkChangeListener(EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }

    @com.atlassian.event.api.EventListener
    public void handleEvent(XWorkStateChangeEvent event)
    {
        ConfigurationManager.getConfiguration().reload();
    }

    /**
     * This method can be removed in favour of an atlassian-event Spring based scanner for EventListeners
     */
    public void registerListener()
    {
        eventPublisher.register(this);
    }
}