package com.atlassian.crowd.applinks;

import java.net.URI;
import java.util.UUID;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.EntityType;
import com.atlassian.applinks.api.application.crowd.CrowdApplicationType;
import com.atlassian.applinks.host.spi.AbstractInternalHostApplication;
import com.atlassian.applinks.host.spi.EntityReference;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.spi.util.TypeAccessor;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.plugin.PluginAccessor;

import com.google.common.base.Charsets;
import com.google.common.collect.ImmutableList;

import static com.atlassian.crowd.model.application.ApplicationType.CROWD;

/**
 * Implementation of {@link InternalHostApplication} for Crowd
 *
 * @since v2.7
 */
public class CrowdHostApplication extends AbstractInternalHostApplication implements InternalHostApplication
{
    private final PropertyManager propertyManager;
    private final TypeAccessor typeAccessor;
    private final ClientProperties applicationProperties;
    private final CrowdBootstrapManager crowdBootstrapManager;

    public CrowdHostApplication(final PropertyManager propertyManager, final PluginAccessor pluginAccessor, final TypeAccessor typeAccessor, final ClientProperties applicationProperties, final CrowdBootstrapManager crowdBootstrapManager)
    {
        super(pluginAccessor);
        this.propertyManager = propertyManager;
        this.typeAccessor = typeAccessor;
        this.applicationProperties = applicationProperties;
        this.crowdBootstrapManager = crowdBootstrapManager;
    }

    @Override
    public URI getDocumentationBaseUrl()
    {
        return URI.create("http://confluence.atlassian.com/display/APPLINKS");
    }

    @Override
    public String getName()
    {
        try
        {
            return propertyManager.getDeploymentTitle();
        }
        catch (PropertyManagerException e)
        {
            return CROWD.getDisplayName();
        }
    }

    @Override
    public ApplicationType getType()
    {
        return typeAccessor.getApplicationType(CrowdApplicationType.class);
    }

    @Override
    public Iterable<EntityReference> getLocalEntities()
    {
        return ImmutableList.of();
    }

    @Override
    public boolean doesEntityExist(final String s, final Class<? extends EntityType> aClass)
    {
        return false;
    }

    @Override
    public boolean doesEntityExistNoPermissionCheck(final String s, final Class<? extends EntityType> aClass)
    {
        return false;
    }

    @Override
    public EntityReference toEntityReference(final Object o)
    {
        return null;
    }

    @Override
    public EntityReference toEntityReference(final String s, final Class<? extends EntityType> aClass)
    {
        return null;
    }

    @Override
    public boolean canManageEntityLinksFor(final EntityReference entityReference)
    {
        return false;
    }

    @Override
    public boolean hasPublicSignup()
    {
        return false;
    }

    @Override
    public ApplicationId getId()
    {
        final String serverId = crowdBootstrapManager.getServerID();
        return new ApplicationId(UUID.nameUUIDFromBytes(serverId.getBytes(Charsets.UTF_8)).toString());
    }

    @Override
    public URI getBaseUrl()
    {
        return URI.create(applicationProperties.getBaseURL());
    }
}
