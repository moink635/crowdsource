package com.atlassian.crowd.service.soap;

/*
Copyright (c) 2005 Envoi Solutions LLC

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
 */

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.codehaus.xfire.spring.ServiceBean;
import org.codehaus.xfire.spring.remoting.XFireServletControllerAdapter;
import org.springframework.aop.framework.ProxyFactory;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;


/**
 * Web controller that exports the specified service bean as a XFire Soap service endpoint.
 *
 * @author <a href="mailto:dan@envoisolutions.com">Dan Diephouse </a>
 * @author <a href="mailto:poutsma@mac.com">Arjen Poutsma</a>
 */
public class SafeXmlXFireExporter
        extends ServiceBean
        implements Controller, ServletContextAware
{
    private XFireServletControllerAdapter delegate;
    private ServletContext context;

    public void afterPropertiesSet()
            throws Exception
    {
        super.afterPropertiesSet();

        delegate = new SafeXmlXFireServletControllerAdapter(getXfire(),
                                                     context,
                                                     getXFireService().getName());
    }

    /**
     * Process the incoming SOAP request and create a SOAP response.
     *
     * @param request  current HTTP request
     * @param response current HTTP response
     * @return <code>null</code>
     * @throws Exception in case of errors
     */
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response)
            throws Exception
    {
        return delegate.handleRequest(request, response);
    }

    /**
     * @return
     */
    protected Object getProxyForService()
    {
        ProxyFactory proxyFactory = new ProxyFactory();
        proxyFactory.addInterface(getXFireService().getServiceInfo().getServiceClass());

        proxyFactory.setTarget(getServiceBean());
        return proxyFactory.getProxy();
    }

    /**
     * This is just a convenience method which delegates to setServiceClass().
     * @param intf
     */
    public void setServiceInterface(Class intf)
    {
        setServiceClass(intf);
    }

    public void setServletContext(ServletContext context)
    {
        this.context = context;
    }
}
