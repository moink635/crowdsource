package com.atlassian.crowd.manager.license;

import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.util.build.BuildUtils;
import com.atlassian.extras.api.AtlassianLicense;
import com.atlassian.extras.api.LicenseException;
import com.atlassian.extras.api.LicenseManager;
import com.atlassian.extras.api.crowd.CrowdLicense;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public class CrowdLicenseManagerImpl implements CrowdLicenseManager
{
    private static final Logger log = LoggerFactory.getLogger(CrowdLicenseManagerImpl.class);

    private PropertyManager propertyManager;

    private ApplicationManager applicationManager;
    private DirectoryManager directoryManager;

    private CrowdLicenseStore licenseStore;
    private LicenseManager licenseManager;

    /**
     * @see CrowdLicenseManager#getLicense()
     */
    public CrowdLicense getLicense()
    {
        return licenseStore.getLicense();
    }

    public CrowdLicense storeLicense(final String license)
    {
        return licenseStore.storeLicense(license);
    }

    /**
     * @see CrowdLicenseManager#isLicenseValid()
     */
    public boolean isLicenseValid()
    {
        return isLicenseValid(getLicense());
    }

    boolean isSetupLicenseValid(CrowdLicense license)
    {
        if (license == null)
        {
            return false;
        }
        if (license.isExpired() && license.isGracePeriodExpired())
        {
            log.error("License has expired.");
            return false;
        }
        return true;
    }

    public boolean isSetupLicenseKeyValid(String key)
    {
        try
        {
            return isSetupLicenseValid(CrowdLicenseStore.getCrowdLicense(getAtlassianLicense(key)));
        }
        catch (com.atlassian.extras.api.LicenseException e)
        {
            log.error("Failed to validate license: " + e.getMessage());
            return false;
        }
    }

    /**
     */
    public boolean isLicenseValid(final CrowdLicense license)
    {
        if (license == null)
        {
            return false;
        }

        if (license.isExpired() && license.isGracePeriodExpired())
        {
            log.error("License has expired.");
            return false;
        }

        if (!license.isUnlimitedNumberOfUsers() && license.getMaximumNumberOfUsers() < propertyManager.getCurrentLicenseResourceTotal())
        {
            log.error("License resource limit has been reached.");
            return false;
        }

        return true;
    }

    public boolean isLicenseKeyValid(String key)
    {
        try
        {
            return isLicenseValid(CrowdLicenseStore.getCrowdLicense(getAtlassianLicense(key)));
        }
        catch (LicenseException e)
        {
            log.error("Failed to validate license: " + e.getMessage());
            return false;
        }
    }

    AtlassianLicense getAtlassianLicense(final String license)
    {
        if (license != null)
        {
            return licenseManager.getLicense(license);
        }
        else
        {
            return null;
        }
    }

    /**
     * @see CrowdLicenseManager#getCurrentResourceUsageTotal()
     */
    public int getCurrentResourceUsageTotal() throws CrowdLicenseManagerException
    {
        Set<Group> processedGroups = new HashSet<Group>();
        Set<Long> processedDirectories = new HashSet<Long>();
        Set<LicenseResourceEntity> licenseResourceEntities = new HashSet<LicenseResourceEntity>();

        try
        {
            // find all of the applications and the groups that are allowed to login
            List<Application> applications = applicationManager.findAll();
            for (Application application : applications)
            {
                // loop all of the allow unlimited directories
                for (DirectoryMapping directoryMapping : application.getDirectoryMappings())
                {
                    // only active directories count towards the license resources limit
                    if (directoryMapping.getDirectory().isActive())
                    {
                        final Long directoryId = directoryMapping.getDirectory().getId();

                        // if the directory allows all to authenticate, all principals count towards the license resources limit
                        if (directoryMapping.isAllowAllToAuthenticate())
                        {

                            // check if we've processed this directory already
                            if (!processedDirectories.contains(directoryId))
                            {
                                // get ALL active
                                List<String> activeUsers = directoryManager.searchUsers(directoryId, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(true)).returningAtMost(EntityQuery.ALL_RESULTS));

                                licenseResourceEntities.addAll(buildResourceEntitiesFromUsernames(directoryId, activeUsers));
                            }

                            // we don't want to waste resources processing an already processed directory
                            processedDirectories.add(directoryId);
                        }
                        else
                        {
                            for (GroupMapping groupMapping : directoryMapping.getAuthorisedGroups())
                            {
                                Group group = new GroupTemplate(groupMapping.getGroupName(), groupMapping.getDirectory().getId(), GroupType.GROUP);

                                // check if we've processed this group mapping already
                                if (!processedGroups.contains(group))
                                {
                                    // find the members of group
                                    final List<User> users = directoryManager.searchNestedGroupRelationships(directoryMapping.getDirectory().getId(), QueryBuilder.queryFor(User.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(group.getName()).returningAtMost(EntityQuery.ALL_RESULTS));

                                    licenseResourceEntities.addAll(buildResourceEntitiesFromUsers(directoryId, users));

                                    // we don't want to waste resources processing an already processed group
                                    processedGroups.add(group);
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            throw new CrowdLicenseManagerException("Failed to calculate the current number of active user resources.", e);
        }
        return licenseResourceEntities.size();
    }

    private List<LicenseResourceEntity> buildResourceEntitiesFromUsernames(final Long directoryId, final List<String> usernames)
    {
        final List<LicenseResourceEntity> userResourceList = new ArrayList<LicenseResourceEntity>();
        for (String username : usernames)
        {
            userResourceList.add(new LicenseResourceEntity(directoryId, username));
        }

        return userResourceList;
    }

    private List<LicenseResourceEntity> buildResourceEntitiesFromUsers(final Long directoryId, final List<User> users)
    {
        final List<LicenseResourceEntity> userResourceList = new ArrayList<LicenseResourceEntity>();
        for (User user : users)
        {
            if (user.isActive())
            {
                userResourceList.add(new LicenseResourceEntity(directoryId, user.getName()));
            }
        }

        return userResourceList;
    }

    /**
     * @see CrowdLicenseManager#isResourceTotalOverLimit(float,int)
     */
    public boolean isResourceTotalOverLimit(float limit, int currentResourceCount)
    {
        if (getLicense().isUnlimitedNumberOfUsers())
        {
            return false;
        }

        // Is there a difference?
        int userLimit = getLicense().getMaximumNumberOfUsers();
        float percentage = ((float) currentResourceCount / userLimit) * 100;
        log.debug("Percentage: " + percentage + "%");
        return percentage >= limit;
    }

    public boolean isBuildWithinMaintenancePeriod(CrowdLicense license)
    {
        return license.getMaintenanceExpiryDate() == null ||
               BuildUtils.getCurrentBuildDate().getTime() < license.getMaintenanceExpiryDate().getTime();
    }

    public boolean isBuildWithinMaintenancePeriod(String key)
    {
        return isBuildWithinMaintenancePeriod(CrowdLicenseStore.getCrowdLicense(getAtlassianLicense(key)));
    }

    /**
     * Sets the property manager which is used to get and set the server license resource totals. This method is set by Spring.
     *
     * @param propertyManager Property manager implementation.
     */
    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    /**
     * Sets the application manager which is used to get the configurated applications. This method is set by Spring.
     *
     * @param applicationManager APplications manager implementation.
     */
    public void setApplicationManager(ApplicationManager applicationManager)
    {
        this.applicationManager = applicationManager;
    }

    /**
     * Sets the directory manager which is used to lookup principals. This method is set by Spring.
     *
     * @param directoryManager Directory manager implementation.
     */
    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }

    public void setLicenseStore(final CrowdLicenseStore licenseStore)
    {
        this.licenseStore = licenseStore;
    }

    public void setLicenseManager(LicenseManager licenseManager)
    {
        this.licenseManager = licenseManager;
    }
}
