package com.atlassian.crowd.manager.backup;

import com.atlassian.crowd.migration.ExportException;

/**
 * Service to deal with with backups
 *
 * @since v2.7
 */
public interface BackupManager
{
    /**
     * Perform an XML export of crowd, as well as of the directory configuration.
     *
     * @param exportFileName    the name of the file to export to (not the full path)
     * @param resetDomain       whether to activate the RESET_DOMAIN option or not
     * @return the time it took to execute the export, in milliseconds
     * @throws ExportException in case an error happens while exporting the data
     */
    long backup(String exportFileName, boolean resetDomain) throws ExportException;

    /**
     * Generate the full path of the given backup filename.
     *
     * @param fileName    a filename
     * @return a full path
     */
    String getBackupFileFullPath(String fileName);

    /**
     * Return a summary of how many automated backups are present, and how old are the oldest and most recent backups.
     *
     * @return a {@link BackupSummary}
     */
    BackupSummary getAutomatedBackupSummary();

    /**
     * Generate a filename for a manual backup, that can then be used with {@link #backup(String, boolean)}
     *
     * @return a filename
     */
    String generateManualBackupFileName();

    /**
     * Generate a filename for an automated backup, that can then be used with {@link #backup(String, boolean)}.
     *
     * @return a filename
     */
    String generateAutomatedBackupFileName();
}
