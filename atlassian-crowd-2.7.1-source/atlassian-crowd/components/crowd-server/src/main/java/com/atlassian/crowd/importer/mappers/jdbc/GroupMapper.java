package com.atlassian.crowd.importer.mappers.jdbc;

import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Will map a row from a JDBC {@link ResultSet} to a {@link com.atlassian.crowd.model.group.Group}
 */
public class GroupMapper implements RowMapper
{
    private final String groupname;
    private final String description;
    private final Long directoryId;

    public GroupMapper(final String groupname, final String description, final Long directoryId)
    {
        this.groupname = groupname;
        this.description = description;
        this.directoryId = directoryId;
    }

    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        String groupName = rs.getString(groupname);
        GroupTemplate group = new GroupTemplate(groupName, directoryId, GroupType.GROUP);
        group.setActive(true);
        group.setDescription(rs.getString(description));
        return group;
    }
}