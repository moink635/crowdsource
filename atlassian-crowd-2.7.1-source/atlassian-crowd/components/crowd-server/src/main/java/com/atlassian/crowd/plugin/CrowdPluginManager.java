package com.atlassian.crowd.plugin;

import com.atlassian.config.lifecycle.events.ApplicationStartedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.ModuleDescriptorFactory;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.loaders.SinglePluginLoader;
import com.atlassian.plugin.manager.DefaultPluginManager;
import com.atlassian.plugin.manager.PluginPersistentStateStore;
import com.atlassian.plugin.repositories.FilePluginInstaller;

import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CrowdPluginManager extends DefaultPluginManager implements PluginController, PluginAccessor
{
    private static final Logger log = LoggerFactory.getLogger(CrowdPluginManager.class);

    private boolean initialised = false;

    /**
     * If you pass a String in the list of pluginLoaders, it will automagically be converted into a SinglePluginLoader
     * for a file with that string's name.
     */
    public CrowdPluginManager(PluginPersistentStateStore pluginStateStore, List pluginLoaders, ModuleDescriptorFactory moduleDescriptorFactory, PluginEventManager pluginEventManager, PluginDirectoryLocator pluginDirectoryLocator, final EventPublisher eventPublisher)
    {
        super(pluginStateStore, replaceStringsWithPluginLoaders(pluginLoaders), moduleDescriptorFactory, pluginEventManager);

        // this allows dynamic plugin installation when {@link com.atlassian.plugin.manager.DefaultPluginManager#installPlugin(com.atlassian.plugin.PluginArtifact)} is called.
        // this is designed for use with the PDK plugin
        final FilePluginInstaller installer = new FilePluginInstaller(pluginDirectoryLocator.getPluginsDirectory());
        setPluginInstaller(installer);
        eventPublisher.register(this);
    }

    private static List replaceStringsWithPluginLoaders(List list)
    {
        for (ListIterator it = list.listIterator(); it.hasNext();)
        {
            Object o = it.next();
            if (o instanceof String)
            {
                it.remove();
                it.add(new SinglePluginLoader((String) o));
            }
        }

        return list;
    }

    public void init() throws PluginParseException
    {
        if (initialised)
        {
            // We don't want to call init twice here as plugin loaders use referential equality against known plugins
            // Initialising again at the present time makes the plugin loader internal state get out of sync
            // This is mainly a guard against receiving more than one ConfluenceReadyEvent
            log.error("Init() called on an already initialised plugin manager. Ignoring.");
            return;
        }

        super.init();
        initialised = true;
    }

    /**
     * Initialised the plugin system on receipt of a {@link com.atlassian.config.lifecycle.events.ApplicationStartedEvent}
     * @param event
     */
    @EventListener
    public void onApplicationStartedEvent(ApplicationStartedEvent event)
    {
        try
        {
            init();
        }
        catch (PluginParseException e)
        {
            throw new RuntimeException("Error initialising plugin manager: " + e.getMessage(), e);
        }
    }
}
