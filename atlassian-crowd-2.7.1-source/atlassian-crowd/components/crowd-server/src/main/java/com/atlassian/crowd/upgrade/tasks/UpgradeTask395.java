package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.directory.ldap.util.LDAPPropertiesHelper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import com.atlassian.crowd.upgrade.util.DirectoryImplementationsHelper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Updates the 'use relaxed DN standardisation' flag for Directory Connnectors
 * to their defaults as defined in the standard property files.
 */
public class UpgradeTask395 implements UpgradeTask
{
    private final Map<String, String> directoryImplementations = DirectoryImplementationsHelper.getDirectoryImplementations();
    private DirectoryManager directoryManager;
    private LDAPPropertiesHelper ldapPropertiesHelper;

    private final Collection<String> errors = new ArrayList<String>();

    public String getBuildNumber()
    {
        return "395";
    }

    public String getShortDescription()
    {
        return "Setting use.relaxed.dn.stardisation to default values for all directory connectors";
    }

    public void doUpgrade() throws Exception
    {
        List<Directory> ldapDirectories = directoryManager.searchDirectories(
                                            QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                                                        .with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.CONNECTOR))
                                                        .returningAtMost(EntityQuery.ALL_RESULTS));

        for (Directory directory : ldapDirectories)
        {
            Properties defaults = ldapPropertiesHelper.getConfigurationDetails().get(directoryImplementations.get(directory.getImplementationClass()));
            if (defaults != null)
            {
                String relaxed = defaults.getProperty(LDAPPropertiesMapper.LDAP_RELAXED_DN_STANDARDISATION);
                if (relaxed != null)
                {
                    DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
                    directoryToUpdate.setAttribute(LDAPPropertiesMapper.LDAP_RELAXED_DN_STANDARDISATION, relaxed);
                    directoryManager.updateDirectory(directoryToUpdate);
                }
            }
        }
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryManager(final DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }

    public void setLdapPropertiesHelper(final LDAPPropertiesHelper ldapPropertiesHelper)
    {
        this.ldapPropertiesHelper = ldapPropertiesHelper;
    }
}
