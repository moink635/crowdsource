package com.atlassian.crowd.manager.token.factory;

import java.util.List;

import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.security.random.DefaultSecureRandomService;
import com.atlassian.security.random.SecureRandomService;

import com.google.common.collect.ImmutableList;

/**
 * The <code>TokenGenerator</code> is responsible for generating tokens when a successful authentication has occurred.
 */
public class TokenFactoryImpl implements TokenFactory
{
    private final TokenKeyGenerator tokenKeyGenerator;
    private final SecureRandomService secureRandomService;

    public TokenFactoryImpl(TokenKeyGenerator tokenKeyGenerator, SecureRandomService secureRandomService)
    {
        this.tokenKeyGenerator = tokenKeyGenerator;
        this.secureRandomService = secureRandomService;
    }

    public TokenFactoryImpl(TokenKeyGenerator tokenKeyGenerator)
    {
        this(tokenKeyGenerator, DefaultSecureRandomService.getInstance());
    }

    public Token create(long directoryID, String name, TokenLifetime tokenLifetime, List<ValidationFactor> validationFactors)
        throws InvalidTokenException
    {
        return create(directoryID, name, tokenLifetime, validationFactors, createSecretValidationNumber());
    }

    public Token create(long directoryID, String name, TokenLifetime tokenLifetime, List<ValidationFactor> validationFactors,
                        long secretNumber) throws InvalidTokenException
    {
        String identifierHash = tokenKeyGenerator.generateKey(directoryID, name, validationFactors);

        List<ValidationFactor> withRandomNumber = ImmutableList.<ValidationFactor>builder().addAll(validationFactors).add(
                new ValidationFactor(ValidationFactor.RANDOM_NUMBER, Long.toString(secretNumber))).build();
        String randomHash = tokenKeyGenerator.generateKey(directoryID, name, withRandomNumber);

        return new Token.Builder(directoryID, name, identifierHash, secretNumber, randomHash)
            .setLifetime(tokenLifetime)
            .create();
    }

    protected long createSecretValidationNumber()
    {
        long randomLong;

        // this is threadsafe
        randomLong = secureRandomService.nextLong();

        // force positive long
        if (randomLong < 0)
        {
            if (randomLong == Long.MIN_VALUE)
            {
                return 0;
            }
            else
            {
                return -randomLong;
            }
        }
        else
        {
            return randomLong;
        }
   }
}
