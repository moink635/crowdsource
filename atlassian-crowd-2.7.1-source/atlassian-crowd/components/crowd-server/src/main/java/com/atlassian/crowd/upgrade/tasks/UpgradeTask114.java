package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.model.application.ApplicationAttributeConstants;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.password.encoder.AtlassianSHA1PasswordEncoder;
import com.atlassian.crowd.password.encoder.DESPasswordEncoder;
import com.atlassian.crowd.password.encoder.PasswordEncoder;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;

import org.apache.commons.codec.binary.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.Key;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * This upgrade task will migrate all current Applications
 * configured in Crowd from using DES to using the Atlassian SHA1
 * encryption algorithm
 */
public class UpgradeTask114 implements UpgradeTask
{
    private static final Logger logger = LoggerFactory.getLogger(UpgradeTask114.class);
    private Collection<String> errors = new ArrayList<String>();

    private ApplicationManager applicationManager;

    private PropertyManager propertyManager;

    public String getBuildNumber()
    {
        return "114";
    }

    public String getShortDescription()
    {
        return "Upgrading application passwords from DES to Atlassian SHA-1";
    }

    public void doUpgrade() throws Exception
    {
        // Find all applications in Crowd
        List applications = applicationManager.findAll();

        for (Iterator iterator = applications.iterator(); iterator.hasNext();)
        {
            ApplicationImpl application = (ApplicationImpl) iterator.next();

            // Check if the application has had the Atlassian SHA-1 encryption applied
            Object sha1Applied = application.getValue(ApplicationAttributeConstants.ATTRIBUTE_KEY_ATLASSIAN_SHA1_APPLIED);
            if (sha1Applied == null)
            {
                // Get the encrypted Credentials for the current application
                PasswordCredential credential = application.getCredential();
                String credentialValue = credential.getCredential();
                PasswordCredential unencryptedCredential = new PasswordCredential(decryptCredential(credentialValue));

                logger.info("Re-encrypting credentials for application: " + application.getName());

                // Now re-encrypt these credentials as Atlassian SHA-1
                PasswordCredential encryptedCredential = encryptCredential(unencryptedCredential);

                // Set these credentials on the Application and update the database
                application.setCredential(encryptedCredential);

                // Set the sha1 applied attribute
                application.setAttribute(ApplicationAttributeConstants.ATTRIBUTE_KEY_ATLASSIAN_SHA1_APPLIED, Boolean.TRUE.toString());

                applicationManager.update(application);
            }
        }
    }

    private PasswordCredential encryptCredential(PasswordCredential passwordCredential)
    {
        PasswordEncoder encoder = new AtlassianSHA1PasswordEncoder();

        String encryptedPassword = encoder.encodePassword(passwordCredential.getCredential(), null);
        return new PasswordCredential(encryptedPassword, true);
    }

    protected String decryptCredential(String encryptedCredential) throws PropertyManagerException, IOException, GeneralSecurityException
    {
        // The unencrypted password to return
        String unencryptedCredential = null;

        // Get the encryption key from the datastore
        Key encryptionKey = null;
        try
        {
            encryptionKey = propertyManager.getDesEncryptionKey();
        }
        catch (PropertyManagerException e)
        {
            // We failed to find the encryption key, we can't continue
            errors.add("Failed to find encryption key due too: " + e.getMessage());
            throw e;
        }

        Cipher dcipher = null;
        ByteArrayInputStream bis = null;
        InputStream in = null;
        ByteArrayOutputStream bos = null;
        try
        {
            try
            {
                dcipher = Cipher.getInstance(DESPasswordEncoder.PASSWORD_ENCRYPTION_ALGORITHM);
                dcipher.init(Cipher.DECRYPT_MODE, encryptionKey);
            }
            catch (GeneralSecurityException e)
            {
                errors.add("Failed to create Cipher: " + e.getMessage());
                throw e;
            }

            bis = new ByteArrayInputStream(Base64.decodeBase64(encryptedCredential));

            in = new CipherInputStream(bis, dcipher);

            int numRead = 0;
            bos = new ByteArrayOutputStream();

            // Read in the decrypted bytes and write the cleartext to out
            byte[] buf = new byte[1024];
            while ((numRead = in.read(buf)) >= 0)
            {
                bos.write(buf, 0, numRead);
            }

            unencryptedCredential = new String(bos.toByteArray());
        }
        catch (IOException e)
        {
            errors.add(e.getMessage());
            throw e;
        }
        finally
        {
            try
            {
                if (bis != null)
                {
                    bis.close();
                }

                if (in != null)
                {
                    in.close();
                }

                if (bos != null)
                {
                    bos.close();
                }
            }
            catch (IOException e)
            {
                // Failed to close the input stream
            }
        }

        return unencryptedCredential;
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setApplicationManager(ApplicationManager applicationManager)
    {
        this.applicationManager = applicationManager;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}
