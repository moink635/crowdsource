package com.atlassian.crowd.migration.verify;

import org.dom4j.Element;

import java.util.List;

/**
 * Responsible for verifying the correctness of an XML backup.
 */
public interface Verifier
{
    void verify(Element root);

    /**
     * This method will be called between runs of the verifier to make sure that state can be purged between runs if required.
     */
    void clearState();

    boolean hasErrors();

    List<String> getErrors();
}