package com.atlassian.crowd.plugin.descriptors.webwork;

import com.opensymphony.xwork.config.ConfigurationException;
import com.opensymphony.xwork.config.RuntimeConfiguration;
import com.opensymphony.xwork.config.entities.ActionConfig;
import com.opensymphony.xwork.config.entities.PackageConfig;
import com.opensymphony.xwork.config.entities.ResultTypeConfig;
import com.opensymphony.xwork.config.impl.DefaultConfiguration;
import com.opensymphony.xwork.config.providers.InterceptorBuilder;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * An XWork Configuration implementation that allows for
 * PluginAware XWork actions.
 *
 * This class is essentially a copy of the DefaultConfiguration
 * class, except that it creates PluginAwareActionConfig objects
 * when required, when rebuilding the runtime configuration.
 * 
 */

public class PluginAwareConfiguration extends DefaultConfiguration
{
    /**
     * Create an ActionConfig from a base ActionConfig and supplied
     * configuration parameters.
     *
     * This method ensures that base PluginAwareActionConfig's are
     * created as PluginAwareActionConfigs.
     *
     * @param baseConfig base configuration object. If this object is
     * an instance of PluginAwareActionConfig, a PluginAwareActionConfig
     * will be returned.
     * @param params config params.
     * @param results config results.
     * @param interceptors config interceptors.
     * @param externalRefs config externalRefs.
     * @param exceptionMappings config exceptionMappings.
     * @param packageName config packageName.
     * @return ActionConfig generated from config parameters.
     */
    protected ActionConfig createActionConfig(ActionConfig baseConfig, Map params, Map results, List interceptors, List externalRefs, List exceptionMappings, String packageName)
    {
        if (baseConfig instanceof PluginAwareActionConfig)
        {
            PluginAwareActionConfig actionConfig = new PluginAwareActionConfig(baseConfig.getMethodName(), baseConfig.getClassName(), params, results, interceptors, externalRefs, exceptionMappings, packageName);
            actionConfig.setPlugin(((PluginAwareActionConfig)baseConfig).getPlugin());
            return actionConfig;
        }
        else
        {
            return new ActionConfig(baseConfig.getMethodName(), baseConfig.getClassName(), params, results, interceptors, externalRefs, exceptionMappings, packageName);
        }
    }

    // modified to from the DefaultConfiguration implementation to allow PluginAwareActionConfigs
    private ActionConfig buildFullActionConfig(PackageConfig packageContext, ActionConfig baseConfig) throws ConfigurationException
    {
        Map params = new TreeMap(baseConfig.getParams());

        Map results = new TreeMap();
        if (baseConfig.getPackageName().equals(packageContext.getName()))
        {
            results.putAll(packageContext.getAllGlobalResults());
            results.putAll(baseConfig.getResults());
        }
        else
        {
            PackageConfig baseConfigPackageConfig = (PackageConfig) getPackageConfigs().get(baseConfig.getPackageName());
            if (baseConfigPackageConfig != null)
            {
                results.putAll(baseConfigPackageConfig.getAllGlobalResults());
            }
            results.putAll(baseConfig.getResults());
        }

        setDefaultResults(results, packageContext);

        List interceptors = new ArrayList(baseConfig.getInterceptors());

        if (interceptors.size() <= 0)
        {
            String defaultInterceptorRefName = packageContext.getFullDefaultInterceptorRef();

            if (defaultInterceptorRefName != null)
            {
                interceptors.addAll(InterceptorBuilder.constructInterceptorReference(packageContext, defaultInterceptorRefName, new LinkedHashMap()));
            }
        }

        List externalRefs = baseConfig.getExternalRefs();

        List exceptionMappings = baseConfig.getExceptionMappings();
        exceptionMappings.addAll(packageContext.getAllExceptionMappingConfigs());

        // single line of modified code in this method:
        ActionConfig config = createActionConfig(baseConfig, params, results, interceptors, externalRefs, exceptionMappings, packageContext.getName());

        return config;
    }

    // ---- EVERYTHING BELOW HERE HAS BEEN CUT AND PASTED FROM DefaultConfiguration AS THE METHODS DID NOT HAVE VISIBILITY IN THE SUBCLASS ----

    // cut-and-pasted from DefaultConfiguration
    protected synchronized RuntimeConfiguration buildRuntimeConfiguration() throws ConfigurationException
    {
        Map namespaceActionConfigs = new LinkedHashMap();
        Map namespaceConfigs = new LinkedHashMap();

        for (Iterator iterator = getPackageConfigs().values().iterator(); iterator.hasNext();) {
            PackageConfig packageContext = (PackageConfig) iterator.next();

            if (!packageContext.isAbstract()) {
                String namespace = packageContext.getNamespace();
                Map configs = (Map) namespaceActionConfigs.get(namespace);

                if (configs == null) {
                    configs = new LinkedHashMap();
                }

                Map actionConfigs = packageContext.getAllActionConfigs();

                for (Iterator actionIterator = actionConfigs.keySet().iterator();
                     actionIterator.hasNext();) {
                    String actionName = (String) actionIterator.next();
                    ActionConfig baseConfig = (ActionConfig) actionConfigs.get(actionName);
                    configs.put(actionName, buildFullActionConfig(packageContext, baseConfig));
                }

                namespaceActionConfigs.put(namespace, configs);
                if (packageContext.getFullDefaultActionRef() != null) {
                	namespaceConfigs.put(namespace, packageContext.getFullDefaultActionRef());
                }
            }
        }

        return new RuntimeConfigurationImpl(namespaceActionConfigs, namespaceConfigs);
    }

    // cut-and-pasted from DefaultConfiguration
    private void setDefaultResults(Map results, PackageConfig packageContext) {
        String defaultResult = packageContext.getFullDefaultResultType();

        for (Iterator iterator = results.entrySet().iterator();
             iterator.hasNext();) {
            Map.Entry entry = (Map.Entry) iterator.next();

            if (entry.getValue() == null) {
                ResultTypeConfig resultTypeConfig = (ResultTypeConfig) packageContext.getAllResultTypeConfigs().get(defaultResult);
                entry.setValue(resultTypeConfig.getClazz());
            }
        }
    }

    // cut-and-pasted from DefaultConfiguration
    private class RuntimeConfigurationImpl implements RuntimeConfiguration {
        private Map namespaceActionConfigs;
        private Map namespaceConfigs;

        public RuntimeConfigurationImpl(Map namespaceActionConfigs, Map namespaceConfigs) {
            this.namespaceActionConfigs = namespaceActionConfigs;
            this.namespaceConfigs = namespaceConfigs;
        }



        /**
         * Gets the configuration information for an action name, or returns null if the
         * name is not recognized.
         *
         * @param name      the name of the action
         * @param namespace the namespace for the action or null for the empty namespace, ""
         * @return the configuration information for action requested
         */
        public synchronized ActionConfig getActionConfig(String namespace, String name) {
            ActionConfig config = null;
            Map actions = (Map) namespaceActionConfigs.get((namespace == null) ? "" : namespace);

            if (actions != null) {
                config = (ActionConfig) actions.get(name);
                // fail over to default action
                if (config == null) {
                	String defaultActionRef = (String) namespaceConfigs.get((namespace == null) ? "" : namespace);
                	if (defaultActionRef != null) {
                		config = (ActionConfig) actions.get(defaultActionRef);
                	}
                }
            }

            // fail over to empty namespace
            if ((config == null) && (namespace != null) && (!namespace.trim().equals(""))) {
                actions = (Map) namespaceActionConfigs.get("");

                if (actions != null) {
                    config = (ActionConfig) actions.get(name);
                    // fail over to default action
                    if (config == null) {
                    	String defaultActionRef = (String) namespaceConfigs.get("");
                    	if (defaultActionRef != null) {
                    		config = (ActionConfig) actions.get(defaultActionRef);
                    	}
                    }
                }
            }


            return config;
        }

        /**
         * Gets the configuration settings for every action.
         *
         * @return a Map of namespace - > Map of ActionConfig objects, with the key being the action name
         */
        public synchronized Map getActionConfigs() {
            return namespaceActionConfigs;
        }

        public String toString() {
            StringBuffer buff = new StringBuffer("RuntimeConfiguration - actions are\n");

            for (Iterator iterator = namespaceActionConfigs.keySet().iterator();
                 iterator.hasNext();) {
                String namespace = (String) iterator.next();
                Map actionConfigs = (Map) namespaceActionConfigs.get(namespace);

                for (Iterator iterator2 = actionConfigs.keySet().iterator();
                     iterator2.hasNext();) {
                    buff.append(namespace + "/" + iterator2.next() + "\n");
                }
            }

            return buff.toString();
        }
    }
}
