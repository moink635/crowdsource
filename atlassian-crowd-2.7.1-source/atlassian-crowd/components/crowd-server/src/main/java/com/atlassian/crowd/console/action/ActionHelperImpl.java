package com.atlassian.crowd.console.action;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.PageContext;

import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetailsService;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.license.CrowdLicenseManager;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.atlassian.plugin.web.WebInterfaceManager;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;
import com.atlassian.plugin.web.model.WebLink;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.opensymphony.module.sitemesh.Page;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionContext;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

class ActionHelperImpl implements ActionHelper
{
    private final CrowdBootstrapManager bootstrapManager;
    private final CrowdLicenseManager crowdLicenseManager;
    private final CrowdUserDetailsService crowdUserDetailsService;
    private final WebInterfaceManager webInterfaceManager;
    private final I18nHelper i18nHelper;

    public ActionHelperImpl(final CrowdBootstrapManager bootstrapManager, final CrowdLicenseManager crowdLicenseManager, final CrowdUserDetailsService crowdUserDetailsService, final WebInterfaceManager webInterfaceManager, final I18nHelper i18nHelper)
    {
        this.bootstrapManager = Preconditions.checkNotNull(bootstrapManager);
        this.crowdLicenseManager = Preconditions.checkNotNull(crowdLicenseManager);
        this.crowdUserDetailsService = Preconditions.checkNotNull(crowdUserDetailsService);
        this.webInterfaceManager = Preconditions.checkNotNull(webInterfaceManager);
        this.i18nHelper = Preconditions.checkNotNull(i18nHelper);
    }

    private static CrowdUserDetails getUser()
    {
        final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth != null && !(auth instanceof AnonymousAuthenticationToken) && auth.getPrincipal() instanceof CrowdUserDetails ? (CrowdUserDetails) auth.getPrincipal() : null;
    }

    @Override
    public CrowdLicense getLicense()
    {
        return crowdLicenseManager.getLicense();
    }

    @Override
    public boolean isLicenseExpired()
    {
        return isSetupComplete() && getLicense().isExpired();
    }

    @Override
    public boolean isEvaluation()
    {
        return isSetupComplete() && getLicense().isEvaluation();
    }

    @Override
    public boolean isSubscription()
    {
        return isSetupComplete() && getLicense().isSubscription();
    }

    @Override
    public boolean isWithinGracePeriod()
    {
        return isSetupComplete() && getLicense().isWithinGracePeriod();
    }

    @Override
    public boolean hasAdminRole(final CrowdUserDetails userDetails)
    {
        final String adminRole = crowdUserDetailsService.getAdminAuthority();
        for (GrantedAuthority grantedAuthority : userDetails.getAuthorities())
        {
            if (grantedAuthority.getAuthority().equals(adminRole))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isAdmin()
    {
        final CrowdUserDetails user = getUser();
        return user != null && hasAdminRole(user);
    }

    @Override
    public List<WebSectionModuleDescriptor> getWebSectionsForLocation(final String location)
    {
        return webInterfaceManager.getDisplayableSections(location, getWebFragmentsContextMap());
    }

    @Override
    public List<WebItemModuleDescriptor> getWebItemsForSection(final String sectionName)
    {
        return webInterfaceManager.getDisplayableItems(sectionName, getWebFragmentsContextMap());
    }

    @Override
    public String getDisplayableLink(final WebLink link)
    {
        return link.getDisplayableUrl(ServletActionContext.getRequest(), getWebFragmentsContextMap());
    }

    @Override
    public String getSitemeshPageProperty(final String propertyName)
    {
        return getSitemeshPageProperties().get(propertyName);
    }

    @Override
    public String getText(final String key)
    {
        return i18nHelper.getText(key);
    }

    @Override
    public boolean isAuthenticated()
    {
        return isSetupComplete() && getRemoteUser() != null;
    }

    @Override
    public CrowdBootstrapManager getBootstrapManager()
    {
        return bootstrapManager;
    }

    @Override
    public UserWithAttributes getRemoteUser()
    {
        final CrowdUserDetails user = getUser();
        return user != null ? user.getRemotePrincipal() : null;
    }

    @Override
    public Map<String, Object> getWebFragmentsContextMap()
    {
        final Map<String, String[]> parameters = (Map<String, String[]>) ActionContext.getContext().getParameters();
        final Map<String, String> transformedParameters = Maps.transformValues(parameters, new Function<String[], String>()
        {
            @Override
            public String apply(final String[] input)
            {
                return Joiner.on(',').join(input);
            }
        });
        final Map<String, String> sitemeshPageProperties = getSitemeshPageProperties();

        // use a HashMap instead an ImmutableMap.Builder to allow entry override
        final Map<String, Object> result = Maps.newHashMap();
        result.putAll(transformedParameters);
        result.putAll(sitemeshPageProperties); // potentially override transformedParameters
        return Collections.unmodifiableMap(result);
    }

    @Override
    public Map<String, String> getSitemeshPageProperties()
    {
        final PageContext pageContext = ServletActionContext.getPageContext();
        if (pageContext != null)
        {
            // get the sitemesh page out of the page context
            final Page page = (Page) pageContext.getAttribute("sitemeshPage");
            if (page != null)
            {
                return page.getProperties();
            }
        }
        return Collections.emptyMap();
    }

    private boolean isSetupComplete()
    {
        return bootstrapManager.isSetupComplete();
    }
}
