package com.atlassian.crowd.migration;

public class ImportExportException extends Exception
{
    /**
     * Default constructor.
     */
    public ImportExportException()
    {
    }

    /**
     * Default constructor.
     *
     * @param s the message.
     */
    public ImportExportException(String s)
    {
        super(s);
    }

    /**
     * Default constructor.
     *
     * @param s         the message.
     * @param throwable the {@link Exception Exception}.
     */
    public ImportExportException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    /**
     * Default constructor.
     *
     * @param throwable the {@link Exception Exception}.
     */
    public ImportExportException(Throwable throwable)
    {
        super(throwable);
    }
}
