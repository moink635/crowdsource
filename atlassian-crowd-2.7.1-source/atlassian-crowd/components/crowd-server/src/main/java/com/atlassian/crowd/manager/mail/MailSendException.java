package com.atlassian.crowd.manager.mail;

public class MailSendException extends Exception
{
    public MailSendException()
    {
    }

    public MailSendException(String message)
    {
        super(message);
    }

    public MailSendException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MailSendException(Throwable cause)
    {
        super(cause);
    }
}
