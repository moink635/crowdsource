package com.atlassian.crowd.manager.authentication;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nullable;

import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.dao.token.SessionTokenStorage;
import com.atlassian.crowd.event.application.ApplicationAuthenticatedEvent;
import com.atlassian.crowd.event.token.TokenInvalidatedEvent;
import com.atlassian.crowd.event.user.UserAuthenticationFailedAccessDeniedEvent;
import com.atlassian.crowd.event.user.UserAuthenticationSucceededEvent;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.ObjectAlreadyExistsException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.TokenExpiredException;
import com.atlassian.crowd.exception.TokenNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationAccessDeniedException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.cache.CacheManager;
import com.atlassian.crowd.manager.cache.CacheManagerException;
import com.atlassian.crowd.manager.cache.NotInCacheException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.token.factory.TokenFactory;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.model.authentication.ApplicationAuthenticationContext;
import com.atlassian.crowd.model.authentication.AuthenticationContext;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.membership.MembershipQuery;
import com.atlassian.event.api.EventPublisher;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

// use an isolation level that allows non-repeatable reads so we can recover from the race condition when
// two transactions attempt to create a token with the same identifier
// (see CWD-3688, which fixes the problem created by the fix of CWD-3568).
@Transactional(isolation = Isolation.READ_COMMITTED)
public class TokenAuthenticationManagerImpl implements TokenAuthenticationManager
{
    static final String CACHE_NAME = TokenAuthenticationManagerImpl.class.getName() + "_cache";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final SessionTokenStorage tokenManager;
    private final ApplicationDAO applicationDao;
    private final TokenFactory tokenFactory;
    private final CacheManager cacheManager;
    private final EventPublisher eventPublisher;
    private final PropertyManager propertyManager;
    private final DirectoryManager directoryManager;
    private final ApplicationManager applicationManager;
    private final ApplicationService applicationService;


    public TokenAuthenticationManagerImpl(SessionTokenStorage tokenManager, ApplicationDAO applicationDao,
            TokenFactory tokenFactory, CacheManager cacheManager, EventPublisher eventPublisher,
            PropertyManager propertyManager, DirectoryManager directoryManager, ApplicationManager applicationManager,
            ApplicationService applicationService)
    {
        this.tokenManager = tokenManager;
        this.applicationDao = applicationDao;
        this.tokenFactory = tokenFactory;
        this.cacheManager = cacheManager;
        this.eventPublisher = eventPublisher;
        this.propertyManager = propertyManager;
        this.directoryManager = directoryManager;
        this.applicationManager = applicationManager;
        this.applicationService = applicationService;
    }

    @Override
    public void invalidateToken(final String tokenKey)
    {
        try
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("InvalidateToken: token " + tokenKey);
            }

            Token token = tokenManager.findByRandomHash(tokenKey);

            logger.debug("Removing token from the database");

            tokenManager.remove(token);
            eventPublisher.publish(new TokenInvalidatedEvent(this, token));
        }
        catch (ObjectNotFoundException e)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Token does not exist", e);
            }
        }
    }

    @Override
    public void removeExpiredTokens()
    {
        long expirationInMinutes = propertyManager.getSessionTime();
        long maxLifeInSeconds = TimeUnit.MINUTES.toSeconds(expirationInMinutes);
        Date currentDate = new Date();

        logger.debug("Removing expired tokens as of {} with a maximum lifetime of {} seconds", currentDate, maxLifeInSeconds);
        tokenManager.removeExpiredTokens(currentDate, maxLifeInSeconds);
    }

    @Override
    public User findUserByToken(final String tokenKey, final String applicationName) throws InvalidTokenException, OperationFailedException, ApplicationAccessDeniedException
    {
        try
        {
            Token token = tokenManager.findByRandomHash(tokenKey);

            if (token.isApplicationToken())
            {
                throw new InvalidTokenException("Found token was for an application, not a user");
            }
            else
            {
                User user = directoryManager.findUserByName(token.getDirectoryId(), token.getName());

                if (isAllowedToAuthenticate(token, applicationManager.findByName(applicationName)))
                {
                    return user;
                }
                else
                {
                    throw new ApplicationAccessDeniedException(applicationName);
                }
            }
        }
        catch (UserNotFoundException e)
        {
            throw new InvalidTokenException(e.getMessage(), e);
        }
        catch (ObjectNotFoundException e)
        {
            throw new InvalidTokenException(e.getMessage(), e);
        }
        catch (DirectoryNotFoundException e)
        {
            throw new InvalidTokenException(e.getMessage(), e);
        }
    }

    @Override
    public Token findUserTokenByKey(final String tokenKey, String applicationName)
        throws InvalidTokenException, ApplicationAccessDeniedException, OperationFailedException,
               ApplicationNotFoundException
    {
        try
        {
            Token token = tokenManager.findByRandomHash(tokenKey);

            if (token.isApplicationToken())
            {
                throw new InvalidTokenException("Found token was for an application, not a user");
            }
            else if (isAllowedToAuthenticate(token, applicationManager.findByName(applicationName)))
            {
                return token;
            }
            else
            {
                throw new ApplicationAccessDeniedException(applicationName);
            }
        }
        catch (ApplicationNotFoundException e)
        {
            throw e; // rethrow to avoid being subsumed by the more general ObjectNotFoundException below
        }
        catch (ObjectNotFoundException e)
        {
            throw new InvalidTokenException(e.getMessage(), e);
        }
        catch (DirectoryNotFoundException e)
        {
            throw new InvalidTokenException(e.getMessage(), e);
        }
    }

    @Override
    public List<Application> findAuthorisedApplications(final User user, final String applicationName)
            throws OperationFailedException, DirectoryNotFoundException
    {
        MembershipQuery<String> query = QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP))
                .parentsOf(EntityDescriptor.user())
                .withName(user.getName())
                .returningAtMost(EntityQuery.ALL_RESULTS);

        List<String> groupMemberships = directoryManager.searchNestedGroupRelationships(user.getDirectoryId(), query);

        return applicationDao.findAuthorisedApplications(user.getDirectoryId(), groupMemberships);
    }

    @Override
    public Token authenticateApplication(final ApplicationAuthenticationContext authenticationContext) throws InvalidAuthenticationException
    {
        try
        {
            Application application = applicationDao.findByName(authenticationContext.getName());

            if (!application.isActive())
            {
                throw new InvalidAuthenticationException("Application is not active");
            }

            if (!applicationManager.authenticate(application, authenticationContext.getCredential()))
            {
                throw new InvalidAuthenticationException("The password in the application's crowd.properties file does not match the password in Crowd.");
            }

            Token token = generateApplicationToken(authenticationContext);
            eventPublisher.publish(new ApplicationAuthenticatedEvent(this, application, token));
            return token;
        }
        catch (ApplicationNotFoundException e)
        {
            throw new InvalidAuthenticationException(authenticationContext.getName(), e);
        }
        catch (InvalidTokenException e)
        {
            throw new InvalidAuthenticationException(authenticationContext.getName(), e);
        }
        catch (OperationFailedException e)
        {
            throw new InvalidAuthenticationException(authenticationContext.getName(), e);
        }
    }

    public Token authenticateUser(UserAuthenticationContext authenticationContext, boolean validatePassword,
                                  boolean ignoreCache, TokenLifetime tokenLifetime)
            throws InvalidAuthenticationException, OperationFailedException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {
        checkNotNull(tokenLifetime);
        if (authenticationContext == null)
        {
            throw new InvalidAuthenticationException("Unable to authenticate with null context");
        }

        if (logger.isDebugEnabled())
        {
            logger.debug("Authenticating user: " + authenticationContext.getName());
        }

        try
        {
            // load up the application we're going to do the checks on
            Application application = applicationDao.findByName(authenticationContext.getApplication());

            User user;

            // do we need to validate the password?
            if (validatePassword)
            {
                // iteratively call authenticate on each of the assigned directories (skips ONFE, dies on invalid auth)
                user = applicationService.authenticateUser(application, authenticationContext.getName(), authenticationContext.getCredential());
            }
            else
            {
                user = applicationService.findUserByName(application, authenticationContext.getName());
                if (!user.isActive())
                {
                    throw new InactiveAccountException(user.getName());
                }
            }

            authenticationContext = authenticationContext.withName(user.getName());

            // generate a token for the authentication
            Token token = generateUserToken(user.getDirectoryId(), authenticationContext, tokenLifetime);

            // validate they have access to the application before generating a token
            if (applicationService.isUserAuthorised(application, authenticationContext.getName()))
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("User <" + authenticationContext.getName() + "> has access to the application <" + application.getName() + ">");
                }

                eventPublisher.publish(new UserAuthenticationSucceededEvent(this, user, application, token));

                return token;
            }
            else
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("User <" + authenticationContext.getName() + "> does NOT have access to the application <" + application.getName() + ">");
                }

                eventPublisher.publish(new UserAuthenticationFailedAccessDeniedEvent(this, user, application));
                throw new ApplicationAccessDeniedException(authenticationContext.getApplication());
            }
        }
        catch (ApplicationNotFoundException e)
        {
            // application not found
            throw new InvalidAuthenticationException(authenticationContext.getName(), e);
        }
        catch (InvalidTokenException e)
        {
            // token could not be generated because of encoder issue
            throw new InvalidAuthenticationException(authenticationContext.getName(), e);
        }
        catch (CacheManagerException e)
        {
            // random cache manager runtime exception will probably never be thrown when checking if a user has access (cached)
            throw new InvalidAuthenticationException(authenticationContext.getName(), e);
        }
        catch (UserNotFoundException e)
        {
            throw new InvalidAuthenticationException(authenticationContext.getName(), e);
        }
    }

    @Override
    @Deprecated
    public Token authenticateUser(final UserAuthenticationContext authenticateContext) throws InvalidAuthenticationException, OperationFailedException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {
        return authenticateUser(authenticateContext, true, false, null);
    }

    @Override
    public Token authenticateUser(final UserAuthenticationContext authenticateContext, TokenLifetime tokenLifetime)
        throws InvalidAuthenticationException, OperationFailedException, InactiveAccountException, ApplicationAccessDeniedException, ExpiredCredentialException
    {
        return authenticateUser(authenticateContext, true, false, tokenLifetime);
    }

    @Override
    public Token authenticateUserWithoutValidatingPassword(final UserAuthenticationContext authenticateContext) throws InvalidAuthenticationException, OperationFailedException, InactiveAccountException, ApplicationAccessDeniedException
    {
        try
        {
            return authenticateUser(authenticateContext, false, false, TokenLifetime.USE_DEFAULT);
        }
        catch (ExpiredCredentialException e)
        {
            logger.error("This should never happen! This user is not authenticated by validating the password.");
            throw InvalidAuthenticationException.newInstanceWithName(authenticateContext.getName());
        }
    }

    @Override
    public Token validateApplicationToken(String tokenKey, ValidationFactor[] clientValidationFactors) throws InvalidTokenException
    {
        return genericValidateToken(tokenKey, clientValidationFactors);
    }

    @Override
    public Token validateUserToken(final String userTokenKey, ValidationFactor[] validationFactors, String applicationName) throws InvalidTokenException, ApplicationAccessDeniedException, OperationFailedException
    {
        if (logger.isDebugEnabled())
        {
            logger.debug("validateUserToken: " + userTokenKey);
        }

        Token validatedToken = genericValidateToken(userTokenKey, validationFactors);

        Application application;
        try
        {
            application = applicationDao.findByName(applicationName);
        }
        catch (ObjectNotFoundException e)
        {
            throw new ApplicationAccessDeniedException(applicationName);
        }

        try
        {
            if (isAllowedToAuthenticate(validatedToken, application))
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("user has access to the application <" + application.getName() + ">");
                }

                return validatedToken;
            }
            else
            {
                if (logger.isDebugEnabled())
                {
                    logger.debug("user does NOT have access to the application <" + application.getName() + ">");
                }

                throw new ApplicationAccessDeniedException(applicationName);
            }
        }
        catch (DirectoryNotFoundException e)
        {
            throw new ConcurrentModificationException("Directory mapping removed while validating the user token");
        }
    }

    private static final Predicate<ValidationFactor> NOT_REMOTE_ADDRESS = new Predicate<ValidationFactor>()
    {
        @Override
        public boolean apply(ValidationFactor input)
        {
            return !input.getName().equals(ValidationFactor.REMOTE_ADDRESS);
        }
    };

    protected List<ValidationFactor> activeValidationFactors(ValidationFactor[] factors)
    {
        Predicate<ValidationFactor> activeFactors;

        if (propertyManager.isIncludeIpAddressInValidationFactors())
        {
            activeFactors = Predicates.alwaysTrue();
        }
        else
        {
            activeFactors = NOT_REMOTE_ADDRESS;
        }

        return ImmutableList.copyOf(Iterables.filter(Arrays.asList(factors), activeFactors));
    }

    /**
     * This method will return a {@link com.atlassian.crowd.model.token.Token} based on the passed in parameters.
     * If a token already exists in the datastore, this token will be returned with an updated lastAccessed time.
     * If a token is not found based on the passed in parameters a new {@link com.atlassian.crowd.model.token.Token}
     * will be generated an stored in the datastore.
     * @param directoryID the directoryID you wish to generate a Token for
     * @param authenticationContext holder for the required attributes to authenticate against the Crowd server
     * @param tokenLifetime requested lifetime of the token
     * @return a {@link com.atlassian.crowd.model.token.Token}
     * @throws InvalidTokenException if there was an issue generating the key for a token.
     * @throws OperationFailedException if adding the new token failed
     */
    protected Token generateUserToken(long directoryID, AuthenticationContext authenticationContext,
                                      TokenLifetime tokenLifetime)
            throws InvalidTokenException, OperationFailedException
    {
        Assert.notNull(authenticationContext);

        if (logger.isDebugEnabled())
        {
            logger.debug("generateUserToken: user " + authenticationContext.getName());
        }

        ValidationFactor[] factors = authenticationContext.getValidationFactors();

        // If we do not have any validation factors, create an empty array
        if (factors == null)
        {
            factors = new ValidationFactor[0];
        }

        // Create a new Token based on the passed in parameters.
        Token newToken = tokenFactory.create(directoryID, authenticationContext.getName(),
                                             tokenLifetime, activeValidationFactors(factors));

        // check to see if the identifierHash exists in the DB (if so return that one instead of creating a new one, cf. CWD-1372)
        try
        {
            return reuseExistingToken(newToken.getIdentifierHash());
        }
        catch (ObjectNotFoundException e)
        {
            return saveNewToken(newToken);
        }
    }

    private Token reuseExistingToken(String identifierHash) throws ObjectNotFoundException
    {
        final Token existingToken = tokenManager.findByIdentifierHash(identifierHash);
        if (isExpired(existingToken))
        {
            TokenValidationFailure.expired(existingToken.getRandomHash(), existingToken.getName());
            logger.debug("Token exists matching identifierHash ({}), but is expired, so need to create a new token",
                    existingToken.getIdentifierHash());
            tokenManager.remove(existingToken);
            throw new ObjectNotFoundException(Token.class, existingToken.getIdentifierHash());
        }
        else
        {
            logger.debug("Returning existing token that matched identifierHash");
            return tokenManager.update(existingToken);
        }
    }

    private Token saveNewToken(Token token) throws OperationFailedException
    {
        logger.debug("Saving and returning newly created token");
        try
        {
            tokenManager.add(token);
        }
        catch (ObjectAlreadyExistsException e)
        {
            // someone was faster than us
            logger.debug("A token with the same identifier hash was already created");

            try
            {
                token = tokenManager.findByIdentifierHash(token.getIdentifierHash());
            }
            catch (ObjectNotFoundException e2)
            {
                // this can only happen if the token expiry time is too short and the token expires before the
                // transaction completes. This is considered an error.
                throw new OperationFailedException("A token with the same identifier hash was created and immediately expired by another thread");
            }
        }
        return token;
    }

    protected Token generateApplicationToken(final ApplicationAuthenticationContext authenticationContext)
            throws InvalidTokenException, OperationFailedException
    {
        return generateUserToken(-1L, authenticationContext, TokenLifetime.USE_DEFAULT);
    }


    static String toString(ValidationFactor[] validationFactors)
    {
        return StringUtils.join(validationFactors, ", ");
    }

    /**
     * Will validate a token key with the given {@link com.atlassian.crowd.model.authentication.ValidationFactor}'s
     * against one (if it exists) in the datastore.
     * @param token the key of a {@link com.atlassian.crowd.model.token.Token}
     * @param validationFactors the {@link com.atlassian.crowd.model.authentication.ValidationFactor}'s that are being used for authentication
     * @return the existing token if there is a match (with an updated lastAccessed time)
     * @throws InvalidTokenException thrown if the token keys are not equal,  or the token has expired, or the token does not exist
     */
    protected Token genericValidateToken(String token, ValidationFactor[] validationFactors) throws InvalidTokenException
    {
        Assert.notNull(token, "A token key cannot be null");
        Assert.notNull(validationFactors, "The array of ValidationFactors cannot be null");

        logger.debug("genericValidateToken");

        try
        {
            // get the existing token and build a copy of it from scratch
            Token existingToken = tokenManager.findByRandomHash(token);

            if (isExpired(existingToken))
            {
                logger.debug("token has expired. removing from db");
                tokenManager.remove(existingToken);
                throw new TokenExpiredException("Token has expired.");
            }

            Token validationToken = tokenFactory.create(existingToken.getDirectoryId(), existingToken.getName(),
                                                        existingToken.getLifetime(),
                                                        activeValidationFactors(validationFactors),
                                                        existingToken.getRandomNumber());

            if (logger.isDebugEnabled())
            {
                logger.debug("Current Validation Factors: \n" + toString(validationFactors));

                logger.debug("comparing existing token " + existingToken + " with a validation token " + validationToken);
            }

            // make sure they validate, ie the keys are equal
            if (!existingToken.getRandomHash().equals(validationToken.getRandomHash()))
            {
                TokenValidationFailure.mismatch(existingToken.getRandomHash(), existingToken.getName(), validationToken.getRandomHash(), validationFactors);
                logger.debug("The token keys don't match");
                throw new InvalidTokenException("Token doesn't match the existing token.");
            }

            logger.debug("returning validated token, with updated last accessed time");
            return tokenManager.update(existingToken);
        }
        catch (ObjectNotFoundException e)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("The token " + token + " was not found in db");
            }

            throw new TokenNotFoundException("Token does not validate.");

        }
    }

    protected boolean isExpired(Token token)
    {
        Date now = new Date();
        Date expiryTime = getTokenExpiryTime(token);

        if (logger.isDebugEnabled())
        {
            logger.debug("checking if the token is expired:");
            logger.debug("\tnow: \t\t\t" + now);
            logger.debug("\tlast accessed: \t" + new Date(token.getLastAccessedTime()));
            logger.debug("\texpiry time: \t" + expiryTime);
            logger.debug("\tallowed session time (seconds): " + getEffectiveTokenSessionTime(token));
        }

        // tokens with a duration of 0 must expire immediately, therefore expiryTime.before(now) is not an option
        return !expiryTime.after(now);
    }

    /**
     * @param token token which effective duration after the last update is to be calculated
     * @return the default session time unless the token specifies a shorter duration, values in seconds
     */
    private long getEffectiveTokenSessionTime(Token token)
    {
        long defaultMaxLifeInSeconds = TimeUnit.MINUTES.toSeconds(propertyManager.getSessionTime());
        TokenLifetime tokenLifetime = token.getLifetime();
        if (tokenLifetime.isDefault())
        {
            return defaultMaxLifeInSeconds;
        }
        else
        {
            return Math.min(tokenLifetime.getSeconds(), defaultMaxLifeInSeconds);
        }
    }

    /**
     * Determines if a user is authorised to authenticate
     * with a given application.
     * <p/>
     * For a a user to have access to an application:
     * <ol>
     * <li>the Application must be active.
     * </ol>
     * <p/>
     * And either:
     * <ul>
     * <li>the User is stored in a directory which
     * is associated to the Application and the "allow all
     * to authenticate" flag is true.
     * <li>the User is a member of a Group that
     * is allowed to authenticate with the Application and both
     * the User and Group are from the same
     * RemoteDirectory.
     * </ul>
     * <p/>
     * Note that this call is not cached and does not affect the
     * cache.
     * @param application application the user wants to authenticate with.
     * @param username the username of the user that wants to authenticate with the application.
     * @param directoryId the directoryId of the user that wants to authenticate with the application.
     * @return <code>true</code> iff the user is authorised to authenticate with the application.
     * @throws com.atlassian.crowd.exception.OperationFailedException if the directory implementation could not be loaded when performing a membership check.
     */
    public boolean isAllowedToAuthenticate(String username, long directoryId, Application application)
            throws OperationFailedException, DirectoryNotFoundException
    {
        // first make sure the application is not inactive
        if (!application.isActive())
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("User does not have access to application '" + application.getName() + "' as the application is inactive");
            }

            return false;
        }

        // see if the application has a mapping for the users's directory
        DirectoryMapping directoryMapping = application.getDirectoryMapping(directoryId);
        if (directoryMapping != null)
        {
            if (directoryMapping.isAllowAllToAuthenticate())
            {
                // all users of this directory can authenticate
                return true;
            }
            else
            {
                // check individual group mappings
                for (GroupMapping groupMapping : directoryMapping.getAuthorisedGroups())
                {
                    if (directoryManager.isUserNestedGroupMember(directoryId, username, groupMapping.getGroupName()))
                    {
                        return true;
                    }
                }
            }
        }

        // did not have allow all on directory OR meet group membership requirements
        if (logger.isDebugEnabled())
        {
            logger.debug("User does not have access to application '" + application.getName() + "' as the directory is not allow all to authenticate and the user is not a member of any of the authorised groups");
        }
        return false;
    }

    public boolean isAllowedToAuthenticate(Token token, Application application, boolean ignoreCache)
            throws OperationFailedException, DirectoryNotFoundException
    {
        if (ignoreCache || !propertyManager.isCacheEnabled())
        {
            return isAllowedToAuthenticate(token.getName(), token.getDirectoryId(), application);
        }
        else
        {
            try
            {
                Boolean hasAccess = (Boolean) cacheManager.get(CACHE_NAME, application.getName() + token.getRandomHash());

                // crowd check has already been performed, return status if positive, otherwise try
                // to check crowd results again
                if (hasAccess)
                {
                    return true;
                }

            }
            catch (NotInCacheException e)
            {
                // not cached
            }

            // if successful access not cached then actual perform the lookups
            boolean hasAccess = isAllowedToAuthenticate(token.getName(), token.getDirectoryId(), application);

            // cache the result
            cacheManager.put(CACHE_NAME, application.getName() + token.getRandomHash(), hasAccess);

            return hasAccess;
        }
    }

    public boolean isAllowedToAuthenticate(Token token, Application application)
            throws OperationFailedException, DirectoryNotFoundException
    {
        return isAllowedToAuthenticate(token, application, false);
    }

    @Override
    public void invalidateTokensForUser(String username, @Nullable String exclusionToken, String applicationName)
            throws UserNotFoundException, ApplicationNotFoundException
    {
        User user = applicationService.findUserByName(applicationManager.findByName(applicationName), username);

        if (StringUtils.isBlank(exclusionToken))
        {
            tokenManager.remove(user.getDirectoryId(), user.getName());
        }
        else
        {
            tokenManager.removeExcept(user.getDirectoryId(), user.getName(), exclusionToken);
        }
    }

    @Override
    public Date getTokenExpiryTime(Token token)
    {
        long effectiveSessionTime = TimeUnit.SECONDS.toMillis(getEffectiveTokenSessionTime(token));
        long lastAccessed = token.getLastAccessedTime();
        // check for overflow
        checkArgument(effectiveSessionTime >= 0);
        checkArgument(lastAccessed >= 0);
        long expiryTime = lastAccessed + effectiveSessionTime;
        if (expiryTime < 0) // overflow
        {
            return new Date(Long.MAX_VALUE);
        }
        else
        {
            return new Date(expiryTime);
        }
    }

    /**
     * This class provides context for a specific logger that will only be
     * used for validation failure. Admins may wish to set this logger's level
     * to DEBUG to catch these messages when debugging authentication issues.
     */
    static class TokenValidationFailure
    {
        private static final Logger logger = LoggerFactory.getLogger(TokenValidationFailure.class);

        static void mismatch(String existingRandomHash, String username, String newRandomHash, ValidationFactor[] newValidationFactors)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Existing token '{}' for user '{}' does not match new token '{}' with validation factors '{}'",
                        new String[] {
                            existingRandomHash,
                            username,
                            newRandomHash,
                            TokenAuthenticationManagerImpl.toString(newValidationFactors)});
            }
        }

        static void expired(String randomHash, String username)
        {
            if (logger.isDebugEnabled())
            {
                logger.debug("Existing token '{}' for user '{}' has expired", randomHash, username);
            }
        }
    }
}
