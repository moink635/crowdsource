package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.migration.ImportException;
import org.dom4j.Element;

/**
 * A Mapper that will handle the import a Domain object, or any object from
 * legacy (Crowd v1.x) XML into the datastore.
 */
public interface LegacyImporter
{
    /**
     * Imports V1 (legacy Crowd 0.x.x and 1.x.x) XML.
     *
     * @param root root XML element.
     * @param importData stores significant import results.
     */
    void importXml(Element root, LegacyImportDataHolder importData) throws ImportException;
}
