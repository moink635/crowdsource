package com.atlassian.crowd.manager.backup;

/**
 * Exception thrown for scheduling errors for automated backups.
 *
 * @since v2.7
 */
public class ScheduledBackupException extends Exception
{
    public ScheduledBackupException(String message)
    {
        super(message);
    }

    public ScheduledBackupException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
