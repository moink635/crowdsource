package com.atlassian.crowd.importer.importers;

import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.mappers.jdbc.GroupMapper;
import com.atlassian.crowd.importer.mappers.jdbc.UserMapper;
import com.atlassian.crowd.importer.mappers.jdbc.UserMembershipMapper;
import com.atlassian.crowd.manager.directory.DirectoryManager;

import org.springframework.jdbc.core.RowMapper;

/**
 * This class handles the import of JIRA Groups, Users and their memberships into Crowd.
 */
class JiraLegacyImporter extends JdbcImporter
{
    private static final String FIND_GROUPS_SQL = "SELECT groupname from groupbase order by groupname";

    private static final String FIND_USERS_SQL = "SELECT ID, username, password_hash," + "(select ps.propertyvalue from propertyentry pe, propertystring ps where pe.ENTITY_NAME='OSUser' and pe.ENTITY_ID = ub.ID and pe.PROPERTY_KEY = 'fullName' and pe.ID=ps.ID" + ") as fullname," + "(select ps.propertyvalue from propertyentry pe, propertystring ps where pe.ENTITY_NAME='OSUser' and pe.ENTITY_ID = ub.ID and pe.PROPERTY_KEY = 'email' and pe.ID=ps.ID" + ") as email " + "FROM userbase ub order by ub.ID";

    private static final String FIND_USER_GROUP_MEMBERSHIPS = "SELECT USER_NAME, GROUP_NAME FROM membershipbase";

    public JiraLegacyImporter(DirectoryManager directoryManager)
    {
        super(directoryManager);
    }

    @Override
    public String getSelectAllGroupsSQL()
    {
        return FIND_GROUPS_SQL;
    }

    @Override
    public String getSelectAllUsersSQL()
    {
        return FIND_USERS_SQL;
    }

    @Override
    public String getSelectAllUserGroupMembershipsSQL()
    {
        return FIND_USER_GROUP_MEMBERSHIPS;
    }

    @Override
    public RowMapper getGroupMapper(Configuration configuration)
    {
        return new GroupMapper("groupname", "groupname", configuration.getDirectoryID());
    }

    @Override
    public RowMapper getMembershipMapper()
    {
        return new UserMembershipMapper("USER_NAME", "GROUP_NAME");
    }

    @Override
    public RowMapper getUserMapper(Configuration configuration)
    {
        return new UserMapper(configuration, "username", "email", "fullname", "password_hash");
    }
}
