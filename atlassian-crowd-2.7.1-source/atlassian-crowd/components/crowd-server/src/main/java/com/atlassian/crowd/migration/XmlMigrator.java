package com.atlassian.crowd.migration;

import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

/**
 * This class manages the import and export of Crowd's entity type's via {@link com.atlassian.crowd.migration.Mapper}'s
 * Each mapper has an <code>exportXML()</code> that process's each entity and writes all required relationship to an
 * XML element that is then returned to this class to be finally written to disk (on export).
 * The mapper's <code>importXML()</code> will take the XML document produced from an export and create the entities it is
 * responsible for and create each entity in the datastore.
 * <p/>
 * User: Justin
 * Date: 16/02/2007
 */
public class XmlMigrator
{
    private final Logger logger = LoggerFactory.getLogger(XmlMigrator.class);

    private final List<Mapper> mappers;

    public XmlMigrator(List<Mapper> mappers)
    {
        this.mappers = mappers;
    }

    public void exportXml(Element root, Map options) throws ExportException
    {
        // iterate over the mappers (injected via Spring) and run export on each
        for (Mapper mapper : mappers)
        {
            Element mapping = mapper.exportXml(options);
            if (mapping != null)
            {
                // add each mapping element to the root XML doc
                root.add(mapping);
            }
        }
    }

    public void importXml(Element root) throws ImportException
    {
        for (Mapper mapper : mappers)
        {
            logger.info("Using " + mapper.getClass().getName() + " to import.");
            mapper.importXml(root);
        }
    }
}
