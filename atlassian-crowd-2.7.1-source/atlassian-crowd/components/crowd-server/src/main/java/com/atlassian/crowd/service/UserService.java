package com.atlassian.crowd.service;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

public interface UserService
{
    /**
     * Returns the username of the currently logged in user or null if no user can be found.
     *
     * @return The user name of the logged in user or null
     * @deprecated use {@link com.atlassian.crowd.service.UserService#getAuthenticatedUsername(javax.servlet.http.HttpServletRequest)}
     */
    @Deprecated
    String getRemoteUsername();

    /**
     * Returns the username of the currently logged in user or null if no user can be found.
     *
     * @param request The request to retrieve the username from
     * @return The user name of the logged in user or null
     */
    String getAuthenticatedUsername(HttpServletRequest request);

    /**
     * Returns whether the user is in the specify group
     *
     * @param username The username to check
     * @param group    The group to check
     * @return True if the user is in the specified group
     */
    boolean isUserInGroup(String username, String group);

    /**
     * Returns true or false depending on if a user has been granted the system admin permission within Crowd.
     * This will usually mean that the user is a member of the crowd-administrators group OR
     * the user is a member of a group assigned to the <b>crowd</b> application.
     *
     * @param username The username of the user to check
     * @return true or false depending on if a user has been granted the system admin permission.
     * @throws org.springframework.dao.DataAccessException
     *          if there was an error retrieving the username
     */
    boolean isSystemAdmin(String username) throws DataAccessException;

    /**
     * Given a usernamen & password, this method checks, whether or not the provided user can
     * be authenticated
     *
     * @param username Username of the user
     * @param password Password of the user
     * @return True if the user can be authenticated, false otherwise
     */
    boolean authenticate(String username, String password);

    /**
     * Returns the user that made this request or {@code null} if this application does not have such a user.
     *
     * @param username Username of the user a consumer is making a request on behalf of
     * @return {@code Principal} corresponding to the username, {@code null} if the user does not exist
     * @throws org.springframework.dao.DataAccessException
     *          if there was an error retrieving the username
     */
    Principal resolve(String username) throws DataAccessException;

    /**
     * Authenticate the current user by setting an appropriate authentication token in Spring Security's SecurityContext.
     * This method should only be called if the user has been authenticated via some other mean (e.g OAuth or Trusted Apps).
     *
     * @param username    the username of the user to authenticate
     * @return true if the username was correctly resolved and authenticated, false otherwise
     */
    boolean setAuthenticatedUser(String username);
}
