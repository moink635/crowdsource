package com.atlassian.crowd.plugin.descriptors.webwork;

import com.atlassian.plugin.Plugin;
import com.opensymphony.xwork.config.entities.ActionConfig;

import java.util.List;
import java.util.Map;

/**
 * ActionConfig that contains a reference to its parent
 * plugin. This allows the action to access the classes
 * of the plugin (provided by the OSGI class loader).
 */

public class PluginAwareActionConfig extends ActionConfig
{
    private Plugin plugin;

    public PluginAwareActionConfig()
    {
    }

    public PluginAwareActionConfig(String methodName, Class clazz, Map parameters, Map results, List interceptors)
    {
        super(methodName, clazz, parameters, results, interceptors);
    }

    public PluginAwareActionConfig(String methodName, Class clazz, Map parameters, Map results, List interceptors, List exceptionMappings)
    {
        super(methodName, clazz, parameters, results, interceptors, exceptionMappings);
    }

    public PluginAwareActionConfig(String methodName, String className, Map parameters, Map results, List interceptors)
    {
        super(methodName, className, parameters, results, interceptors);
    }

    public PluginAwareActionConfig(String methodName, String className, Map parameters, Map results, List interceptors, List exceptionMappings)
    {
        super(methodName, className, parameters, results, interceptors, exceptionMappings);
    }

    public PluginAwareActionConfig(String methodName, String className, Map parameters, Map results, List interceptors, List externalRefs, String packageName)
    {
        super(methodName, className, parameters, results, interceptors, externalRefs, packageName);
    }

    public PluginAwareActionConfig(String methodName, String className, Map parameters, Map results, List interceptors, List externalRefs, List exceptionMappings, String packageName)
    {
        super(methodName, className, parameters, results, interceptors, externalRefs, exceptionMappings, packageName);
    }

    public Plugin getPlugin()
    {
        return plugin;
    }

    public void setPlugin(Plugin plugin)
    {
        this.plugin = plugin;
    }
}
