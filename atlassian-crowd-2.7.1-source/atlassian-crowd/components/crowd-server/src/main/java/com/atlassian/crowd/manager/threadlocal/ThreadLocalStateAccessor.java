package com.atlassian.crowd.manager.threadlocal;

/**
 * Service to access the application's ThreadLocal state.
 *
 * @since v2.7
 */
public interface ThreadLocalStateAccessor
{
    /**
     * Return the state of all the thread locals for the current thread.
     *
     * @return a {@link ThreadLocalState}
     */
    ThreadLocalState getState();

    /**
     * Set the thread locals of the current thread with the values contained in the supplied state.
     *
     * @param state    a {@link ThreadLocalState}
     */
    void setState(ThreadLocalState state);

    /**
     * Clear the all the thread locals for the current thread.
     */
    void clearState();
}
