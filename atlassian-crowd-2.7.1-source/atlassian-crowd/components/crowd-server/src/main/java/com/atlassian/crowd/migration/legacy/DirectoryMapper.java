package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * This mapper is expected to import legacy Directories AND populate the oldToNewDirectoryIds map.
 */
public class DirectoryMapper extends GenericLegacyImporter implements LegacyImporter
{
    protected static final String DIRECTORY_XML_ROOT = "directories";
    protected static final String DIRECTORY_XML_NODE = "directory";
    private static final String DIRECTORY_XML_DESCRIPTION = "description";
    private static final String DIRECTORY_XML_IMPLEMENTATION_CLASS = "implementationClass";
    private static final String DIRECTORY_XML_TYPE = "type";
    private static final String DIRECTORY_XML_PERMISSIONS_NODE = "permissions";
    private static final String DIRECTORY_XML_PERMISSION = "permission";
    private static final String DIRECTORY_XML_PERMISSION_KEY = "key";
    private static final String DIRECTORY_XML_PERMISSION_VALUE = "value";

    public DirectoryMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor)
    {
        super(sessionFactory, batchProcessor);
    }

    public void importXml(final Element root, final LegacyImportDataHolder importData) throws ImportException
    {
        Element directoriesElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + DIRECTORY_XML_ROOT);
        if (directoriesElement == null)
        {
            logger.error("No applications were found for importing!");
            return;
        }

        for (Iterator directories = directoriesElement.elementIterator(); directories.hasNext();)
        {
            Element directoryElement = (Element) directories.next();

            importDirectoryFromXml(directoryElement, importData.getOldToNewDirectoryIds());
        }
    }

    protected DirectoryImpl importDirectoryFromXml(final Element directoryElement, final Map<Long, Long> oldToNewDirectoryIds) throws ImportException
    {
        InternalEntityTemplate template = getInternalEntityTemplateFromLegacyXml(directoryElement);
        Long oldDirectoryId = template.getId();
        template.setId(null);

        DirectoryImpl directory = new DirectoryImpl(template);
        directory.setDescription(directoryElement.element(DIRECTORY_XML_DESCRIPTION).getText());
        directory.setType(getDirectoryTypeFromLegacyCode(Integer.parseInt(directoryElement.element(DIRECTORY_XML_TYPE).getText())));
        directory.setImplementationClass(directoryElement.element(DIRECTORY_XML_IMPLEMENTATION_CLASS).getText());

        Map<String, String> attributes = getSingleValuedAttributesMapFromXml(directoryElement);
        directory.setAttributes(attributes);

        Set<OperationType> permissions = getPermissions(directoryElement);
        directory.setAllowedOperations(permissions);

        DirectoryImpl addedDirectory = (DirectoryImpl) addEntityViaMerge(directory);

        Long newDirectoryId = addedDirectory.getId();
        oldToNewDirectoryIds.put(oldDirectoryId, newDirectoryId);

        return directory;
    }

    private Set<OperationType> getPermissions(Element element)
    {
        Set<OperationType> permissions = new HashSet<OperationType>();
        Element permissionsNode = element.element(DIRECTORY_XML_PERMISSIONS_NODE);

        if (permissionsNode != null && permissionsNode.hasContent())
        {
            for (Iterator iterator = permissionsNode.elementIterator(DIRECTORY_XML_PERMISSION); iterator.hasNext();)
            {
                Element permissionElement = (Element) iterator.next();

                if (Boolean.valueOf(permissionElement.attributeValue(DIRECTORY_XML_PERMISSION_VALUE)))
                {
                    // if the permission is allowed then add it as an allowed operation type
                    OperationType operationType = getOperationTypeFromLegacyPermissionName(permissionElement.attributeValue(DIRECTORY_XML_PERMISSION_KEY));
                    if (operationType != null)
                    {
                        permissions.add(operationType);
                    }
                }
            }
        }
        else
        {
            // if there are no permission types, then this must be a really old XML file so allow all operations
            permissions.addAll(Arrays.asList(OperationType.values()));
        }

        return permissions;
    }
}
