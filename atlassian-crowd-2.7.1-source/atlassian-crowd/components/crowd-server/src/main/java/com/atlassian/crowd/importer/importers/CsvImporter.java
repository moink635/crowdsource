package com.atlassian.crowd.importer.importers;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.config.CsvConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.mappers.csv.GroupMapper;
import com.atlassian.crowd.importer.mappers.csv.MembershipMapper;
import com.atlassian.crowd.importer.mappers.csv.UserMapper;
import com.atlassian.crowd.importer.model.MembershipDTO;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;

import org.apache.commons.collections.OrderedBidiMap;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVStrategy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class is responsible for importing users, groups and their
 * memberships from two CSV files.
 */
public class CsvImporter extends BaseImporter implements Importer
{
    private static final Logger logger = LoggerFactory.getLogger(CsvImporter.class);

    public CsvImporter(DirectoryManager directoryManager)
    {
        super(directoryManager);
    }

    @Override
    public void init(Configuration configuration)
    {
        // nothing to do here
    }

    @Override
    public Class getConfigurationType()
    {
        return CsvConfiguration.class;
    }

    @Override
    public Collection<MembershipDTO> findUserToGroupMemberships(Configuration configuration) throws ImporterException
    {
        final CsvConfiguration csvconfiguration = (CsvConfiguration) configuration;
        Collection<MembershipDTO> memberships = new ArrayList<MembershipDTO>();

        // Check if we are importing memberships, as this is optional for CSV.
        if (csvconfiguration.getGroupMemberships() == null)
        {
            return Collections.emptyList();
        }

        OrderedBidiMap groupMapping = csvconfiguration.getGroupMappingConfiguration();

        try
        {
            // Setup the strategy for CSV parser
            CSVStrategy strategy = CSVStrategy.EXCEL_STRATEGY;
            strategy.setDelimiter(csvconfiguration.getDelimiter().charValue());

            // Setup the parser with the above stategy
            CSVParser csvParser = new CSVParser(new FileReader(csvconfiguration.getGroupMemberships()), strategy);

            // Create an instance of the mapper that will be used to create Memberships
            MembershipMapper membershipMapper = new MembershipMapper(csvconfiguration.getDirectoryID(), groupMapping);

            // Skip the first line as this is a header
            csvParser.getLine();

            // Prime the first relationship value
            String[] membershipLine = csvParser.getLine();

            while (membershipLine != null)
            {
                // Fetch the membership
                MembershipDTO membership = membershipMapper.mapRow(membershipLine);
                memberships.add(membership);

                // Get the next line
                membershipLine = csvParser.getLine();
            }
        }
        catch (IOException e)
        {
            logger.error(e.getMessage(), e);
        }

        return memberships;
    }

    @Override
    public Collection<MembershipDTO> findGroupToGroupMemberships(Configuration configuration) throws ImporterException
    {
        throw new UnsupportedOperationException("Nested group not supported yet for CSV imports");
    }

    @Override
    public Collection<GroupTemplate> findGroups(Configuration configuration) throws ImporterException
    {
        final CsvConfiguration csvconfiguration = (CsvConfiguration) configuration;
        Set<GroupTemplate> groups = new HashSet<GroupTemplate>();

        OrderedBidiMap groupMapping = csvconfiguration.getGroupMappingConfiguration();

        if (groupMapping == null || csvconfiguration.getGroupMemberships() == null)
        {
            return Collections.emptyList();
        }

        try
        {
            // Setup the strategy for CSV parser
            CSVStrategy strategy = CSVStrategy.EXCEL_STRATEGY;
            strategy.setDelimiter(csvconfiguration.getDelimiter().charValue());

            // Setup the parser with the above stategy
            CSVParser csvParser = new CSVParser(new FileReader(csvconfiguration.getGroupMemberships()), strategy);

            // Create an instance of the mapper that will be used to create RemoteGroups
            GroupMapper groupMapper = new GroupMapper(csvconfiguration.getDirectoryID(), groupMapping);

            // Skip the first line as this is a header
            csvParser.getLine();

            String[] membershipLine = null;
            do
            {
                try
                {
                    membershipLine = csvParser.getLine();

                    // Fetch the group
                    GroupTemplate group = groupMapper.mapRow(membershipLine);
                    if (group != null)
                    {
                        groups.add(group);
                    }
                }
                catch (IOException ioe)
                {
                    // just log the error and keep trying to read the lines
                    logger.error("Failed to parse line, due too: " + ioe.getMessage());
                }
            } while (membershipLine != null);

        }
        catch (IOException e)
        {
            logger.error(e.getMessage(), e);
        }

        return groups;
    }

    @Override
    public Collection<UserTemplateWithCredentialAndAttributes> findUsers(Configuration configuration) throws ImporterException
    {
        final CsvConfiguration csvconfiguration = (CsvConfiguration) configuration;

        if (csvconfiguration.getUserMappingConfiguration() == null || csvconfiguration.getUsers() == null)
        {
            return Collections.emptyList();
        }

        Collection<UserTemplateWithCredentialAndAttributes> users = new ArrayList<UserTemplateWithCredentialAndAttributes>();
        OrderedBidiMap userMapping = csvconfiguration.getUserMappingConfiguration();

        try
        {
            // Setup the strategy for CSV parser
            CSVStrategy strategy = CSVStrategy.EXCEL_STRATEGY;
            strategy.setDelimiter(csvconfiguration.getDelimiter().charValue());

            // Setup the parser with the above stategy
            CSVParser csvParser = new CSVParser(new FileReader(csvconfiguration.getUsers()), strategy);

            // Create an instance of the mapper that will be used to create Principals
            UserMapper mapper = new UserMapper(csvconfiguration.getDirectoryID(), userMapping, csvconfiguration.isImportPasswords(), csvconfiguration.isEncryptPasswords());

            // Skip the first line as this is a header
            csvParser.getLine();

            // Prime the first principal value
            String[] userline = csvParser.getLine();

            while (userline != null)
            {
                UserTemplateWithCredentialAndAttributes user = mapper.mapRow(userline);
                users.add(user);

                // Get the next line
                userline = csvParser.getLine();
            }
        }
        catch (IOException e)
        {
            logger.error(e.getMessage(), e);
        }

        return users;
    }
}
