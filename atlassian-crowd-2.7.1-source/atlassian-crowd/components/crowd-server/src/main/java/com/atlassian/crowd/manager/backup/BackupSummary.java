package com.atlassian.crowd.manager.backup;

import javax.annotation.Nullable;
import java.util.Date;

/**
 * Summary of automated backup files.
 *
 * @since v2.7
 */
public class BackupSummary
{
    private final int numBackups;
    private final Date earliestDate;
    private final Date latestDate;

    public BackupSummary(final int numBackups, @Nullable final Date earliestDate, @Nullable final Date latestDate)
    {
        this.numBackups = numBackups;
        this.earliestDate = earliestDate;
        this.latestDate = latestDate;
    }

    /**
     * Return the number of backups found.
     * @return the number of backups found
     */
    public int getNumBackups()
    {
        return numBackups;
    }

    /**
     * Return the date of the most recent backup.
     * @return the date of the most recent backup, or <code>null</code> if there are no backups
     */
    @Nullable public Date getEarliestDate()
    {
        return earliestDate;
    }

    /**
     * Return the date of the oldest backup.
     * @return the date of the oldest backup, or <code>null</code> if there are no backups
     */
    @Nullable public Date getLatestDate()
    {
        return latestDate;
    }
}
