package com.atlassian.crowd.manager.license;

import com.atlassian.crowd.event.LicenseResourceLimitEvent;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.extras.api.crowd.CrowdLicense;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * System event that runs calculating the total number of resources are being consumed verses the license resource
 * limit. If the system is nearing 90% usage, an email is sent to the administrator.
 */
public class LicenseResourceJob extends QuartzJobBean implements StatefulJob
{
    private static final Logger log = LoggerFactory.getLogger(LicenseResourceJob.class);

    private PropertyManager propertyManager;
    private CrowdLicenseManager crowdLicenseManager;
    private EventPublisher eventPublisher;

    /**
     * Runs the job of notifying the administrator if the current license resource limit is above 90%.
     * @param jobExecutionContext Information about the current event request.
     * @throws JobExecutionException A problem occured running the event.
     */
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException
    {
        final CrowdLicense license = crowdLicenseManager.getLicense();

        if (license != null && !license.isUnlimitedNumberOfUsers())
        {
            // NOTE: This call will traverse the underlying configured LDAP repositories and is rather expensive. 
            int currentResourceCount = getCurrentResourceTotal();

            // Find what the previous number of resources 'were'
            int oldResourceTotal = propertyManager.getCurrentLicenseResourceTotal();

            log.debug("Current number of resources: " + currentResourceCount + ". Old resource Count: " + oldResourceTotal);

            // Is there a difference?
            if (currentResourceCount > oldResourceTotal)
            {
                // Is the resource limit above 90% ?
                if (crowdLicenseManager.isResourceTotalOverLimit(90f, currentResourceCount))
                {
                    // Send a mail to the user.
                    // send the event
                    eventPublisher.publish(new LicenseResourceLimitEvent(this, currentResourceCount));
                }
            }

            // Store this value every time just so it is always up-to-date.
            updateCurrentResourceTotal(currentResourceCount);
        }
    }

    private void updateCurrentResourceTotal(int currentResourceCount) throws JobExecutionException
    {
        try
        {
            // Store the total consumed resources in the database
            propertyManager.setCurrentLicenseResourceTotal(currentResourceCount);
        }
        catch (DataAccessException e)
        {
            log.error("Failed to update license resource totals", e);
            throw new JobExecutionException("Failed to update license resource totals", e, false);
        }
    }

    /**
     * Gets the current number of resources being used across all applications in Crowd.
     * @return the current number of resources 'used'
     */
    private int getCurrentResourceTotal() throws JobExecutionException
    {
        try
        {
            return crowdLicenseManager.getCurrentResourceUsageTotal();
        }
        catch (CrowdLicenseManagerException e)
        {
            log.error("Failed to get current resource usage total", e);
            throw new JobExecutionException("Failed to calculate the current number of 'used' resources.", e, false);
        }
    }

    /**
     * Sets the property manager which is used to get the server license. This method is set by Spring.
     * @param propertyManager Property manager implementation.
     */
    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    /**
     * Sets the license manager which is used to get and set the license information. This method is set by Spring.
     * @param crowdLicenseManager License manager implementation.
     */
    public void setCrowdLicenseManager(CrowdLicenseManager crowdLicenseManager)
    {
        this.crowdLicenseManager = crowdLicenseManager;
    }

    /**
     * Sets the event manager which is used to trigger server events. This method is set by Spring.
     * @param eventPublisher Event manager implementation.
     */
    public void setEventPublisher(EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }
}
