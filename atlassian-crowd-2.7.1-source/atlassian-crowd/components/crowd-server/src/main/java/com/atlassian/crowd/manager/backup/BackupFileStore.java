package com.atlassian.crowd.manager.backup;

import java.io.File;
import java.util.Date;
import java.util.List;

/**
 * Service to deal with access to backup files
 *
 * @since v2.7
 */
public interface BackupFileStore
{
    /**
     * Returns the directory where backup files are stored
     * @return the directory where backup files are stored
     */
    File getBackupDirectory();

    /**
     * Return a list of automated backup files, sorted by ascending date.
     *
     * Note: the date is taken from the filename, not from the file itself.
     *
     * @return a list of {@link File}
     */
    List<File> getBackupFiles();

    /**
     * Extract the timestamp from the name of the given backup file.
     *
     * @param file    an automated backup file
     * @return a {@link Date}, or <code>null</code> if the file is not a valid backup file
     */
    Date extractTimestamp(File file);

    /**
     * If there are more than the maximum number of automated backup files, delete the oldest ones
     */
    void cleanUpAutomatedBackupFiles();
}
