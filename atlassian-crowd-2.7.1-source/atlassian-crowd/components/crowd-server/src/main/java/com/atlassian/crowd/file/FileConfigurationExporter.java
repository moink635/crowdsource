package com.atlassian.crowd.file;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import com.atlassian.crowd.dao.directory.DirectoryPropertiesMapper;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.migration.ExportException;

public class FileConfigurationExporter
{

    private final DirectoryManager directoryManager;
    private final DirectoryPropertiesMapper directoryPropertiesMapper;

    public FileConfigurationExporter(DirectoryManager directoryManager,
                                     DirectoryPropertiesMapper directoryPropertiesMapper)
    {
        this.directoryManager = directoryManager;
        this.directoryPropertiesMapper = directoryPropertiesMapper;
    }

    public void exportDirectories(String path) throws ExportException
    {
        Properties properties = directoryPropertiesMapper.exportProperties(directoryManager.findAllDirectories());
        FileWriter fileWriter = null;
        try
        {
            fileWriter = new FileWriter(path);
            properties.store(fileWriter, null);
        }
        catch (IOException e)
        {
            throw new ExportException(e);
        }
        finally
        {
            if (fileWriter != null)
            {
                try
                {
                    fileWriter.close();
                }
                catch (IOException e)
                {
                    // there is nothing we can do
                }
            }
        }
    }
}
