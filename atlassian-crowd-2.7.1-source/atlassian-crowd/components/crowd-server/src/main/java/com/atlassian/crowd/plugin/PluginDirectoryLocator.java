package com.atlassian.crowd.plugin;

import com.atlassian.config.HomeLocator;

import java.io.File;

public class PluginDirectoryLocator
{
    private static final String BUNDLED_PLUGINS_DIRECTORY = "bundled-plugins";
    private static final String DIRECTORY_PLUGINS_DIRECTORY = "plugins";
    private static final String PLUGIN_CACHES_DIR_NAME = "caches";

    private final HomeLocator homeLocator;

    public PluginDirectoryLocator(final HomeLocator homeLocator)
    {
        this.homeLocator = homeLocator;
    }

    public File getPluginsDirectory()
    {
        return PluginUtils.getHomeSubDirectory(homeLocator, DIRECTORY_PLUGINS_DIRECTORY);
    }

    public File getBundledPluginsDirectory()
    {
        return PluginUtils.getHomeSubDirectory(homeLocator, BUNDLED_PLUGINS_DIRECTORY);
    }

    public File getPluginCachesDirectory()
    {
        return PluginUtils.getHomeSubDirectory(homeLocator, PLUGIN_CACHES_DIR_NAME);
    }
}
