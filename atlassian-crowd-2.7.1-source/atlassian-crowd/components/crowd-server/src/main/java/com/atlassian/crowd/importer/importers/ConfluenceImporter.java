package com.atlassian.crowd.importer.importers;

import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.manager.directory.DirectoryManager;

import org.springframework.jdbc.BadSqlGrammarException;

/**
 * This class handles the <b>delegation</b> of the import of Confluence Groups, Users and their memberships into Crowd.
 */
public class ConfluenceImporter extends BaseDelegatingJdbcImporter
{
    public ConfluenceImporter(DirectoryManager directoryManager)
    {
        super(directoryManager);
    }

    private static final String CROWD_USER_COUNT = "SELECT COUNT(id)  FROM cwd_user";
    private static final String HIBERNATE_USER_COUNT = "SELECT COUNT(id) FROM users";

    @Override
    public Importer determineImporter(Configuration configuration) throws ImporterException
    {
        if (configuration == null || !(getConfigurationType().isInstance((configuration))))
        {
            throw new IllegalArgumentException("The supplied configuration was of the incorrect type for this Importer, should have been: " + getConfigurationType().getCanonicalName());
        }

        init(configuration);

        Importer confluenceImporter;
        if (isUsingCrowdUser())
        {
            confluenceImporter = new CrowdifiedConfluenceImporter(directoryManager);
        }
        else if (isUsingHibernateUser())
        {
            confluenceImporter = new ConfluenceHibernateImporter(directoryManager);
        }
        else
        {
            confluenceImporter = new ConfluenceOSUserImporter(directoryManager);
        }

        return confluenceImporter;
    }

    private boolean isUsingCrowdUser()
    {
        try
        {
            return jdbcTemplate.queryForInt(CROWD_USER_COUNT) > 0;
        }
        catch (BadSqlGrammarException bsge)
        {
            // This indicates that this confluence does not have crowd embedded.
            return false;
        }
    }

    private boolean isUsingHibernateUser()
    {
        return jdbcTemplate.queryForInt(HIBERNATE_USER_COUNT) > 0;
    }
}