package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ApplicationMapper extends GenericLegacyImporter implements LegacyImporter
{
    protected static final String APPLICATION_XML_ROOT = "applications";
    protected static final String APPLICATION_XML_NODE = "application";
    private static final String APPLICATION_XML_NAME = "name";
    private static final String APPLICATION_XML_DESCRIPTION = "description";
    private static final String APPLICATION_XML_DIRECTORY_ID = "directoryId";
    private static final String APPLICATION_XML_ACTIVE = "active";
    private static final String APPLICATION_XML_REMOTE_ADDRESSES_NODE = "remoteAddresses";
    private static final String APPLICATION_XML_REMOTE_ADDRESSES_VALUE = "address";

    private static final String APPLICATION_XML_DIRECTORY_NODE = "directories";
    private static final String APPLICATION_XML_DIRECTORY_ALLOW_ALL = "allowAll";

    private static final String APPLICATION_XML_GROUPS_NODE = "groups";

    private static final String APPLICATION_XML_PERMISSIONS_NODE = "permissions";
    private static final String APPLICATION_XML_PERMISSION = "permission";
    private static final String APPLICATION_XML_PERMISSION_TYPE = "type";
    private static final String APPLICATION_XML_PERMISSION_ALLOWED = "allowed";

    public static final String OPTION_DEFAULT_PASSWORD = "password";
    private static final String APPLICATION_XML_APPLICATION_TYPE_ATTRIBUTE_NAME = "applicationType";

    private final DirectoryDAOHibernate directoryDAO;


    public ApplicationMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, DirectoryDAOHibernate directoryDAO)
    {
        super(sessionFactory, batchProcessor);
        this.directoryDAO = directoryDAO;
    }

    public void importXml(final Element root, final LegacyImportDataHolder importData) throws ImportException
    {
        Element applicationRoot = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + APPLICATION_XML_ROOT);
        if (applicationRoot == null)
        {
            logger.error("No applications were found for importing!");
            return;
        }

        for (Iterator applications = applicationRoot.elementIterator(); applications.hasNext();)
        {
            Element applicationElement = (Element) applications.next();

            Application application = getApplicationFromXml(applicationElement, importData.getOldToNewDirectoryIds());

            addEntityViaMerge(application);
        }
    }

    protected ApplicationImpl getApplicationFromXml(final Element applicationElement, final Map<Long, Long> oldToNewDirectoryIds)
    {
        // add application basics
        InternalEntityTemplate template = getInternalEntityTemplateFromLegacyXml(applicationElement);

        template.setId(null); // We will be creating the application with a new id

        ApplicationImpl application = new ApplicationImpl(template);

        final Map<String, Set<String>> applicationAttributeValues = getMultiValuedAttributesMapFromXml(applicationElement);

        application.setType(getApplicationType(applicationAttributeValues));

        application.setDescription(applicationElement.element(APPLICATION_XML_DESCRIPTION).getText());

        application.setCredential(getPasswordCredentialFromXml(applicationElement));

        application.setRemoteAddresses(buildRemoteAddresses(applicationElement));

        // add attributes
        Map<String, String> attributes = getSingleValuedAttributesMapFromXml(applicationElement);
        application.setAttributes(attributes);

        // add directory mappings
        Element directoryMappingsElement = applicationElement.element(APPLICATION_XML_DIRECTORY_NODE);
        if (directoryMappingsElement != null)
        {
            for (Iterator directoryMappingsIter = directoryMappingsElement.elementIterator(); directoryMappingsIter.hasNext();)
            {
                Element directoryMappingElement = (Element) directoryMappingsIter.next();

                Long oldDirectoryId = Long.parseLong(directoryMappingElement.attributeValue(APPLICATION_XML_DIRECTORY_ID));
                DirectoryImpl directoryReference = (DirectoryImpl) directoryDAO.loadReference(oldToNewDirectoryIds.get(oldDirectoryId));

                boolean allowAll = Boolean.parseBoolean(directoryMappingElement.attributeValue(APPLICATION_XML_DIRECTORY_ALLOW_ALL));

                DirectoryMapping directoryMapping = new DirectoryMapping(application, directoryReference, allowAll);

                application.getDirectoryMappings().add(directoryMapping);
            }

        }

        // add group mappings
        Element groupMappingsElement = applicationElement.element(APPLICATION_XML_GROUPS_NODE);
        if (groupMappingsElement != null)
        {
            for (Iterator groupMappingsIter = groupMappingsElement.elementIterator(); groupMappingsIter.hasNext();)
            {
                Element groupMappingElement = (Element) groupMappingsIter.next();

                String groupName = groupMappingElement.attributeValue(APPLICATION_XML_NAME);

                // is the group mapping active?
                final boolean activeGroupMapping = Boolean.parseBoolean(groupMappingElement.attributeValue(APPLICATION_XML_ACTIVE));
                if (activeGroupMapping)
                {
                    Long oldDirectoryId = Long.parseLong(groupMappingElement.attributeValue(APPLICATION_XML_DIRECTORY_ID));
                    final DirectoryMapping mapping = application.getDirectoryMapping(oldToNewDirectoryIds.get(oldDirectoryId));
                    if (mapping != null)
                    {
                        GroupMapping groupMapping = new GroupMapping(mapping, groupName);
                        mapping.getAuthorisedGroups().add(groupMapping);
                    }
                }
            }
        }

        // add allowed permissions
        if (applicationElement.element(APPLICATION_XML_PERMISSIONS_NODE) != null)
        {
            for (Iterator permissionsIter = applicationElement.elementIterator(APPLICATION_XML_PERMISSIONS_NODE); permissionsIter.hasNext();)
            {
                Element permissionsElement = (Element) permissionsIter.next();
                Long oldDirectoryId = Long.valueOf(permissionsElement.attributeValue(APPLICATION_XML_DIRECTORY_ID));

                List<OperationType> allowedOperations = new ArrayList<OperationType>();
                for (Iterator permIter = permissionsElement.elementIterator(APPLICATION_XML_PERMISSION); permIter.hasNext();)
                {
                    Element permissionElement = (Element) permIter.next();
                    if (Boolean.valueOf(permissionElement.attributeValue(APPLICATION_XML_PERMISSION_ALLOWED)))
                    {
                        final OperationType operationType = getOperationTypeFromLegacyPermissionName(permissionElement.attributeValue(APPLICATION_XML_PERMISSION_TYPE));
                        if (operationType != null)
                        {
                            allowedOperations.add(operationType);
                        }
                    }

                }
                // Add the allowed operations to the directoryMapping
                application.getDirectoryMapping(oldToNewDirectoryIds.get(oldDirectoryId)).getAllowedOperations().addAll(allowedOperations);
            }
        }
        else
        {
            // no permissions element present (ie. import from 1.0.x or 1.1.x) so allow all permissions
            for (DirectoryMapping directoryMapping : application.getDirectoryMappings())
            {
                directoryMapping.addAllowedOperations(OperationType.values());
            }
        }

        return application;
    }

    private Set<RemoteAddress> buildRemoteAddresses(final Element applicationElement)
    {
        Set<RemoteAddress> addresses = new HashSet<RemoteAddress>();

        // add remote addresses
        Element remoteAddressesElement = applicationElement.element(APPLICATION_XML_REMOTE_ADDRESSES_NODE);
        if (remoteAddressesElement != null && remoteAddressesElement.hasContent())
        {
            for (Iterator iterator = remoteAddressesElement.elementIterator(); iterator.hasNext();)
            {
                Element remoteAddressElement = (Element) iterator.next();

                final boolean active = Boolean.parseBoolean(remoteAddressElement.attributeValue(APPLICATION_XML_ACTIVE));
                if (active)
                {
                    addresses.add(new RemoteAddress(remoteAddressElement.attributeValue(APPLICATION_XML_REMOTE_ADDRESSES_VALUE)));
                }
            }
        }

        return addresses;
    }

    private ApplicationType getApplicationType(final Map<String, Set<String>> applicationAttributeValues)
    {
        ApplicationType applicationType;
        final Set<String> values = applicationAttributeValues.get(APPLICATION_XML_APPLICATION_TYPE_ATTRIBUTE_NAME);
        if (values != null && !values.isEmpty())
        {
            try
            {
                applicationType = ApplicationType.valueOf(values.iterator().next());
            }
            catch (IllegalArgumentException e)
            {
                // value wasn't valid, default to generic
                applicationType = ApplicationType.GENERIC_APPLICATION;

            }
        }
        else
        {
            applicationType = ApplicationType.GENERIC_APPLICATION;
        }

        return applicationType;
    }
}
