package com.atlassian.crowd.event.listener;

import com.atlassian.crowd.event.directory.DirectoryDeletedEvent;
import com.atlassian.crowd.event.user.UserDeletedEvent;
import com.atlassian.crowd.event.user.UserRenamedEvent;
import com.atlassian.crowd.manager.token.TokenManager;
import com.atlassian.event.api.EventListener;

/**
 * Deletes the appropriate Tokens when a User or Directory is deleted, or when a user is renamed.
 */
public class UserDirectoryTokenRemovalListener
{
    private TokenManager tokenManager;

    @EventListener
    public void handleEvent(final UserDeletedEvent event)
    {
        tokenManager.remove(event.getDirectory().getId(), event.getUsername());
    }

    @EventListener
    public void handleEvent(final DirectoryDeletedEvent event)
    {
        tokenManager.removeAll(event.getDirectory().getId());
    }

    @EventListener
    public void handleEvent(final UserRenamedEvent event)
    {
        tokenManager.remove(event.getDirectory().getId(), event.getOldUsername());
    }

    public void setTokenManager(TokenManager tokenManager)
    {
        this.tokenManager = tokenManager;
    }
}
