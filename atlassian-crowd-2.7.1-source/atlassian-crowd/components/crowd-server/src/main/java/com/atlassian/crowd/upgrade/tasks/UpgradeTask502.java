package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import org.apache.commons.lang3.StringUtils;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class UpgradeTask502 implements UpgradeTask
{
    private static final char DELIMITER = ',';

    private PropertyManager propertyManager;

    public String getBuildNumber()
    {
        return "502";
    }

    public String getShortDescription()
    {
        return "Clean up null trusted proxy entries if they exist";
    }

    public void doUpgrade() throws Exception
    {
        String proxiesString;
        try
        {
            proxiesString = propertyManager.getTrustedProxyServers();
        }
        catch (PropertyManagerException e)
        {
            // means there is no trusted proxies to fix.
            return;
        }

        if (!StringUtils.isBlank(proxiesString))
        {
            String[] proxies = StringUtils.split(proxiesString, DELIMITER);
            Set<String> resultSet = new HashSet<String>();
            for(String proxy:proxies)
            {
                // get rid of nulls here.
                if (!StringUtils.isBlank(proxy))
                {
                    resultSet.add(proxy);
                }
            }
            propertyManager.setTrustedProxyServers(StringUtils.join(resultSet, DELIMITER));
        }
    }

    public Collection<String> getErrors()
    {
        // no error here.
        // any exception during upgrade would just pop to the upgrade manager.
        return Collections.emptySet();
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}