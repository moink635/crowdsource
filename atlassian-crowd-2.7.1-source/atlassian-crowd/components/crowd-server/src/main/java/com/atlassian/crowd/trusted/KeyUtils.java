package com.atlassian.crowd.trusted;

import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

import com.atlassian.security.auth.trustedapps.EncryptionProvider;

import org.apache.commons.codec.binary.Base64;

/**
 * Utility methods for dealing with keys
 *
 * @since 2.7
 */
public class KeyUtils
{
    /**
     * Encode the given key into a Base64 encoded string representation.
     *
     * @param key a {@link PrivateKey} or {@link PublicKey}
     * @return a Base64-encoded String representation of the key
     */
    public static String encode(Key key)
    {
        return Base64.encodeBase64String(key.getEncoded());
    }

    /**
     * Decode a Base64-encoded String representing a {@link PrivateKey}.
     *
     * @param keyStr                the String representation of a private key
     * @return a {@link PrivateKey}
     * @throws IllegalStateException if we can't decode the key
     */
    public static PrivateKey decodePrivateKey(EncryptionProvider encryptionProvider, String keyStr)
    {
        final byte[] data = decodeKey(keyStr);
        try
        {
            return encryptionProvider.toPrivateKey(data);
        }
        catch (NoSuchProviderException e)
        {
            throw new IllegalStateException(e);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new IllegalStateException(e);
        }
        catch (InvalidKeySpecException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Decode a Base64-encoded String representing a {@link PublicKey}.
     *
     * @param keyStr                the String representation of a public key
     * @return a {@link PublicKey}
     * @throws IllegalStateException if we can't decode the key
     */
    public static PublicKey decodePublicKey(EncryptionProvider encryptionProvider, String keyStr)
    {
        final byte[] data = decodeKey(keyStr);
        try
        {
            return encryptionProvider.toPublicKey(data);
        }
        catch (NoSuchProviderException e)
        {
            throw new IllegalStateException(e);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new IllegalStateException(e);
        }
        catch (InvalidKeySpecException e)
        {
            throw new IllegalArgumentException(e);
        }
    }

    private static byte[] decodeKey(final String keyStr)
    {
        return Base64.decodeBase64(keyStr);
    }
}
