package com.atlassian.crowd.plugin.web.conditions;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

public class PermanentApplicationCondition implements Condition
{
    private String applicationIDKey = null;

    private ApplicationManager applicationManager;

    public void init(Map params) throws PluginParseException
    {
        this.applicationIDKey = (String) params.get("applicationIDKey");

    }

    public boolean shouldDisplay(Map context)
    {
        String id = (String) context.get(applicationIDKey);
        if (id != null)
        {
            try
            {
                Application application = applicationManager.findById(Integer.valueOf(id));

                if (application.isPermanent() || application.getType() == ApplicationType.PLUGIN)
                {
                    return false;
                }
            }
            catch (ApplicationNotFoundException e)
            {
                // failed to find application
            }
        }

        return true;
    }

    public void setApplicationManager(ApplicationManager applicationManager)
    {
        this.applicationManager = applicationManager;
    }
}
