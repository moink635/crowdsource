package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.migration.ImportException;
import org.dom4j.Element;

/**
 * This class extends the current 2.x CrowdConfigMapper because configuration mapping is 100% backwards-compatible in XML.
 */
public class CrowdConfigMapper extends com.atlassian.crowd.migration.CrowdConfigMapper implements LegacyImporter
{
    public CrowdConfigMapper(CrowdBootstrapManager bootstrapManager)
    {
        super(bootstrapManager);
    }

    public void importXml(final Element root, final LegacyImportDataHolder importData) throws ImportException
    {
        super.importXml(root);
    }
}
