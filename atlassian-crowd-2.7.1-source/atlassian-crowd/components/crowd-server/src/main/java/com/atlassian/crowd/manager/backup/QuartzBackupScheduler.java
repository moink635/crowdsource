package com.atlassian.crowd.manager.backup;

import com.atlassian.config.lifecycle.events.ApplicationStartedEvent;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.google.common.collect.ImmutableMap;
import org.quartz.CronTrigger;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;

import static com.google.common.base.Preconditions.checkArgument;

/**
 * Quartz-specific implementation of {@link BackupScheduler}
 *
 * @since v2.7
 */
public class QuartzBackupScheduler implements BackupScheduler
{
    private static final Logger log = LoggerFactory.getLogger(QuartzBackupScheduler.class);

    public static final String ENABLED_PROPERTY = "backup.scheduled.enabled";
    public static final String HOUR_PROPERTY = "backup.scheduled.time.hour";
    public static final String JOB_NAME = "AutomatedBackup";
    public static final String GROUP_NAME = "AutomatedBackup";
    private final Scheduler scheduler;
    private final PropertyManager propertyManager;
    private final BackupManager backupManager;
    private final BackupFileStore backupFileStore;

    public QuartzBackupScheduler(final Scheduler scheduler, final PropertyManager propertyManager, final BackupManager backupManager, final EventPublisher eventPublisher, BackupFileStore backupFileStore)
    {
        this.scheduler = scheduler;
        this.propertyManager = propertyManager;
        this.backupManager = backupManager;
        this.backupFileStore = backupFileStore;
        eventPublisher.register(this);
    }

    @EventListener
    public synchronized void onApplicationStart(final ApplicationStartedEvent event)
    {
        log.debug("Got ApplicationStarted event");
        final int hourToRun = getHourToRun();

        try
        {
            // Create the job
            scheduler.addJob(buildJobDetail(), true);
            if (isEnabled())
            {
                // Create the trigger
                scheduleJob(hourToRun);
            }
        } catch (Exception e)
        {
            log.error("Unable to schedule the Automated Backup job. The automated backups will not run", e);
        }
    }

    @Override
    public boolean isEnabled()
    {
        return propertyManager.getBoolean(ENABLED_PROPERTY, true);
    }

    @Override
    public synchronized void enable() throws ScheduledBackupException
    {
        if (!isEnabled())
        {
            try
            {
                scheduleJob(getHourToRun());
                propertyManager.setProperty(ENABLED_PROPERTY, Boolean.TRUE.toString());
            } catch (Exception e)
            {
                throw new ScheduledBackupException("Error while enabling automated backups");
            }
        }
    }

    @Override
    public void disable() throws ScheduledBackupException
    {
        if (isEnabled())
        {
            try
            {
                scheduler.unscheduleJob(JOB_NAME, JOB_NAME);
                propertyManager.setProperty(ENABLED_PROPERTY, Boolean.FALSE.toString());
            } catch (SchedulerException e)
            {
                throw new ScheduledBackupException("Error while disabling automated backups", e);
            }
        }
    }

    @Override
    public int getHourToRun()
    {
        return propertyManager.getInt(HOUR_PROPERTY, 2); // Default to 2am
    }

    @Override
    public synchronized void setHourToRun(int timeOfDay) throws ScheduledBackupException
    {
        checkArgument(timeOfDay >= 0 && timeOfDay <= 23, "The time of day must be between 0 and 23");
        try
        {
            if (isEnabled())
            {
                scheduleJob(timeOfDay);
            }
            propertyManager.setProperty(HOUR_PROPERTY, Integer.toString(timeOfDay));
        }
        catch (Exception e)
        {
            throw new ScheduledBackupException("Error while scheduling the automated backup job", e);
        }
    }

    protected void scheduleJob(int timeOfDay) throws SchedulerException, ParseException
    {
        final String cron = getCronExpression(timeOfDay);
        log.info("Registering automated backup Quartz job with trigger schedule {}", cron);
        Trigger trigger = buildTrigger(cron);
        // try to remove the existing trigger
        scheduler.unscheduleJob(JOB_NAME, GROUP_NAME);
        // Add the new trigger
        scheduler.scheduleJob(trigger);
    }

    protected JobDetail buildJobDetail()
    {
        final JobDetail jobDetail = new JobDetail();
        jobDetail.setName(JOB_NAME);
        jobDetail.setGroup(GROUP_NAME);
        jobDetail.setJobClass(BackupJob.class);
        // Don't need to persist the Job - it will get created again on restart.
        jobDetail.setVolatility(true);
        // Keep the job around even if there are no triggers
        jobDetail.setDurability(true);

        jobDetail.setJobDataMap(new JobDataMap(ImmutableMap.of("backupManager", backupManager, "backupFileStore", backupFileStore)));

        return jobDetail;
    }

    protected Trigger buildTrigger(String cron) throws ParseException
    {
        return new CronTrigger(JOB_NAME, GROUP_NAME, JOB_NAME, GROUP_NAME, cron);
    }

    private String getCronExpression(int hourOfDay)
    {
        return "0 0 " + hourOfDay + " * * ?";
    }


}
