package com.atlassian.crowd.migration.legacy;

import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;

import com.google.common.collect.ImmutableSet;

import org.hibernate.SessionFactory;

/**
 * Specialises XmlMapper for mappers that take into account just a subset of the directory types. Subclasses
 * may override getFullBackupDirectoryTypes to specify which directory types participate in the backup, otherwise
 * a default Set is used containing internal and delegating directory types.
 */
public class PartialXmlMapper extends XmlMapper
{
    private static Set<DirectoryType> DIRECTORY_TYPES_THAT_EXPORT_REMOTE_GROUPS =
        EnumSet.of(DirectoryType.UNKNOWN, DirectoryType.INTERNAL, DirectoryType.CUSTOM, DirectoryType.CROWD);

    private DirectoryManager directoryManager;

    /**
     * Directory types that participate in the backup.
     */
    private final Set<DirectoryType> includedDirectoryTypes;

    public PartialXmlMapper(SessionFactory sessionFactory,
                            BatchProcessor batchProcessor,
                            DirectoryManager directoryManager,
                            Set<DirectoryType> includedDirectoryTypes)
    {
        super(sessionFactory, batchProcessor);
        this.directoryManager = directoryManager;
        this.includedDirectoryTypes = ImmutableSet.copyOf(includedDirectoryTypes);
    }

    /**
     * @param directory the directory to be imported
     * @return true if the directory data is importable
     */
    protected boolean isImportableDirectory(Directory directory)
    {
        return includedDirectoryTypes.contains(directory.getType());
    }

    /**
     * Finds all directories that have user/group/membership data that
     * need to be exported (ie. Internal + Delegating).
     *
     * @return list of directories that require exporting internal data.
     */
    protected List<Directory> findAllExportableDirectories()
    {
        final Collection<SearchRestriction> restrictions = new ArrayList(includedDirectoryTypes.size());
        for (DirectoryType type : includedDirectoryTypes)
        {
            restrictions.add(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(type));
        }
        return directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class,
                                                                        EntityDescriptor.directory()).with(
            Combine.anyOf(restrictions)).returningAtMost(EntityQuery.ALL_RESULTS));
    }

    /**
     * Determines whether the backup for a directory type includes all groups (both local
     * and non-local), or just local groups, excluding non-local (remote) groups.
     *
     * @param directoryType a directory type
     * @return false if backups must be limited to just local groups for this directory type, or
     * true if backups must include all groups, local and non-local as well.
     */
    protected boolean isExportOfNonLocalGroupsRequired(DirectoryType directoryType)
    {
        return DIRECTORY_TYPES_THAT_EXPORT_REMOTE_GROUPS.contains(directoryType);
    }
}
