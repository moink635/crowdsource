package com.atlassian.crowd.manager.token;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.concurrent.GuardedBy;
import javax.annotation.concurrent.ThreadSafe;

import com.atlassian.crowd.dao.token.TokenDAO;
import com.atlassian.crowd.exception.ObjectAlreadyExistsException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Proxies the concrete TokenDAO implementations, and allows runtime swapping between implementations, along with copying
 * of data between during swap.
 */
@ThreadSafe
@Transactional
public class SwitchableTokenManagerImpl implements SwitchableTokenManager, SearchableTokenService
{
    private final Logger logger = LoggerFactory.getLogger(SwitchableTokenManagerImpl.class);

    // read lock acquired when calling a method; write when changing active implementation
    private final ReentrantReadWriteLock readWriteLock;

    /**
     * the current, in-use implementation
     */
    @GuardedBy("readWriteLock")
    private TokenDAO implementation;

    /**
     * the in-memory token manager
     */
    @GuardedBy("readWriteLock")
    private final TokenDAO daoMemory;

    /**
     * the in-database token manager
     */
    @GuardedBy("readWriteLock")
    private final TokenDAO daoHibernate;

    private final PropertyManager propertyManager;

    public SwitchableTokenManagerImpl(boolean initialUseIsMemory,
                                      TokenDAO daoMemory,
                                      TokenDAO daoHibernate,
                                      PropertyManager propertyManager)
    {
        this.daoMemory = checkNotNull(daoMemory);
        this.daoHibernate = checkNotNull(daoHibernate);
        this.propertyManager = checkNotNull(propertyManager);

        if (initialUseIsMemory)
        {
            this.implementation = daoMemory;
        }
        else
        {
            this.implementation = daoHibernate;
        }
        this.readWriteLock = new ReentrantReadWriteLock();
    }

    public SwitchableTokenManagerImpl(TokenDAO daoMemory,
                                      TokenDAO daoHibernate,
                                      PropertyManager propertyManager)
    {
        this(Boolean.FALSE, daoMemory, daoHibernate, propertyManager);
    }

    private void lockRead()
    {
        readWriteLock.readLock().lock();
    }

    private void unlockRead()
    {
        readWriteLock.readLock().unlock();
    }

    private void lockWrite()
    {
        readWriteLock.writeLock().lock();
    }

    private void unlockWrite()
    {
        readWriteLock.writeLock().unlock();
    }


    @Override
    public Token findByRandomHash(String randomHash) throws DataAccessException, ObjectNotFoundException
    {
        lockRead();

        try
        {
            return implementation.findByRandomHash(randomHash);
        }
        finally
        {
            unlockRead();
        }
    }

    @Override
    public Token findByIdentifierHash(final String identifierHash) throws ObjectNotFoundException
    {
        lockRead();

        try
        {
            return implementation.findByIdentifierHash(identifierHash);
        }
        finally
        {
            unlockRead();
        }
    }

    // if the insertion fails because the ObjectAlreadyExists, then rollback the nested transaction to
    // give the caller a chance to recover from the problem and continue using the outer transaction (CWD-3688)
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = ObjectAlreadyExistsException.class)
    public Token add(Token token) throws DataAccessException, ObjectAlreadyExistsException
    {
        lockWrite();

        try
        {
            return implementation.add(token);
        }
        finally
        {
            unlockWrite();
        }
    }

    @Override
    public Token update(Token token) throws ObjectNotFoundException
    {
        lockWrite();

        try
        {
            return implementation.update(token);
        }
        finally
        {
            unlockWrite();
        }
    }

    @Override
    public void remove(Token token) throws DataAccessException
    {
        lockWrite();

        try
        {
            implementation.remove(token);
        }
        finally
        {
            unlockWrite();
        }
    }

    @Override
    public List<Token> search(final EntityQuery<? extends Token> query)
    {
        lockRead();

        try
        {
            return implementation.search(query);
        }
        finally
        {
            unlockRead();
        }
    }

    @Override
    public void remove(long directoryID, String name) throws DataAccessException
    {
        lockWrite();

        try
        {
            implementation.remove(directoryID, name);
        }
        finally
        {
            unlockWrite();
        }
    }

    @Override
    public void removeExcept(long directoryId, String name, String exclusionToken)
    {
        lockWrite();

        try
        {
            implementation.removeExcept(directoryId, name, exclusionToken);
        }
        finally
        {
            unlockWrite();
        }
    }

    @Override
    public void removeAll(final long directoryId)
    {
        lockWrite();

        try
        {
            implementation.removeAll(directoryId);
        }
        finally
        {
            unlockWrite();
        }
    }

    @Override
    public void removeExpiredTokens(final Date currentTime, final long maxLifeSeconds)
    {
        lockWrite();

        try
        {
            implementation.removeExpiredTokens(currentTime, maxLifeSeconds);
        }
        finally
        {
            unlockWrite();
        }
    }

    /**
     * Removes all tokens from all implementations.
     */
    @Override
    public void removeAll()
    {
        lockWrite();

        try
        {
            daoMemory.removeAll();
            daoHibernate.removeAll();
        }
        finally
        {
            unlockWrite();
        }
    }

    /**
     * Returns true if the memory token manager is in use. False if the Hibernate manager is being used.
     *
     * @return
     */
    @Override
    public boolean isUsingDatabaseStorage()
    {
        return (implementation == daoHibernate);
    }

    /**
     * Performs the switch, if necessary.
     *
     * @param useDatabaseStorage
     */
    @Override
    public void setUsingDatabaseStorage(boolean useDatabaseStorage) throws PropertyManagerException
    {
        if (useDatabaseStorage)
        {
            if (!isUsingDatabaseStorage())
            {
                switchToHibernate();
            }
        }
        else
        {
            if (isUsingDatabaseStorage())
            {
                switchToMemory();
            }
        }
    }

    /**
     * If the Hibernate DAO is in use, transparently switches to In-memory DAO. Does nothing if the In-memory DAO is
     * already in use. Acquires write lock for safety.
     */
    public void switchToMemory()
    {
        logger.info("About to switch to in-memory token storage");
        lockWrite();
        try
        {
            if (implementation != daoHibernate)
            {
                throw new IllegalStateException("Cannot switch; already in memory caching mode");
            }
            else
            {
                move(daoMemory, daoHibernate);
                implementation = daoMemory;

                // update configuration after completing the switch, just in case problems arise during the switch
                propertyManager.setUsingDatabaseTokenStorage(false);
            }
        }
        finally
        {
            unlockWrite();
        }
        logger.info("Finished switch to in-memory token storage");
    }

    /**
     * If the In-memory DAO is in use, transparently switches to Hibernate DAO. Does nothing if the hibernate DAO is
     * already in use. Acquires write lock for safety.
     */
    public void switchToHibernate()
    {
        logger.info("About to switch to database token storage");
        lockWrite();

        try
        {
            if (implementation != daoMemory)
            {
                throw new IllegalStateException("Cannot switch; already in database caching mode");
            }
            else
            {
                move(daoHibernate, daoMemory);
                implementation = daoHibernate;

                // update configuration after completing the switch, just in case problems arise during the switch
                propertyManager.setUsingDatabaseTokenStorage(true);
            }
        }
        finally
        {
            unlockWrite();
        }
        logger.info("Finished switch to database token storage");
    }

    /**
     * Copies tokens from hibernate to in-memory DAO or vice versa, and upon success deletes the tokens from the source.
     * Acquires write lock, so make sure the lock you use is reentrant!
     */
    protected void move(TokenDAO to, TokenDAO from)
    {
        lockWrite();

        try
        {
            Collection<Token> tokens = from.loadAll();
            if (tokens != null)
            {
                to.saveAll(tokens);
                from.removeAll();
            }
        }
        finally
        {
            unlockWrite();
        }
    }

    @Override
    public void updateTokenStorage()
    {
        try
        {
            setUsingDatabaseStorage(propertyManager.isUsingDatabaseTokenStorage());
        }
        catch (PropertyManagerException e)
        {
            logger.error("Failed to set token storage", e);
        }
    }
}
