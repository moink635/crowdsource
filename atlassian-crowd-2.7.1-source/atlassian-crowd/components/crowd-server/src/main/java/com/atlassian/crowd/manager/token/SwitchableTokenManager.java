package com.atlassian.crowd.manager.token;

import com.atlassian.crowd.manager.property.PropertyManagerException;

/**
 * Extends {@link TokenManager} to allow switching between two TokenDAO delegates (in-memory and database).
 *
 * @since v2.7
 */
public interface SwitchableTokenManager extends TokenManager
{
    boolean isUsingDatabaseStorage();

    void setUsingDatabaseStorage(boolean useDatabaseStorage) throws PropertyManagerException;

    /**
     * Updates the switchable token manager to match the current configuration. This method must be called
     * when the configuration has been changed externally (i.e., not through {@link #setUsingDatabaseStorage(boolean)}),
     * for instance, after a backup restore.
     */
    void updateTokenStorage();
}
