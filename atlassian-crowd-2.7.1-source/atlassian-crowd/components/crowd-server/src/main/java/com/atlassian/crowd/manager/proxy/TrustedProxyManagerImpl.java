package com.atlassian.crowd.manager.proxy;

import com.atlassian.crowd.manager.cache.CacheManager;
import com.atlassian.crowd.manager.cache.NotInCacheException;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.ip.IPMatcher;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

/**
 * Handles the list of proxy servers whose X-Forwarded-For headers we trust. Deals with persisting and querying the list.
 */
public class TrustedProxyManagerImpl implements TrustedProxyManager
{
    private static final Logger logger = LoggerFactory.getLogger(TrustedProxyManagerImpl.class);

    private final PropertyManager propertyManager;
    private final CacheManager cacheManager;

    private static final char DELIMITER = ',';
    protected static final String REQUESTED_PROXIES_CACHE = TrustedProxyManagerImpl.class.getName() + "_requested_proxies";

    public TrustedProxyManagerImpl(final PropertyManager propertyManager, final CacheManager cacheManager)
    {
        this.propertyManager = propertyManager;
        this.cacheManager = cacheManager;
    }

    /**
     * Returns <tt>true</tt> if the proxy server is trusted, <tt>false</tt> otherwise.
     *
     * @param remoteAddress address of the proxy
     * @return <tt>true</tt> if the remote address is a trusted proxy
     */
    public boolean isTrusted(String remoteAddress)
    {
        try
        {
            return (Boolean) cacheManager.get(REQUESTED_PROXIES_CACHE, remoteAddress);
        }
        catch (NotInCacheException e)
        {
            boolean trusted;
            try
            {
                trusted = match(getTrustedProxies(), remoteAddress);
            }
            catch (IllegalArgumentException iae)
            {
                logger.warn("Received invalid IP address: " + remoteAddress);
                trusted = false;
            }

            cacheManager.put(REQUESTED_PROXIES_CACHE, remoteAddress, trusted);
            return trusted;
        }
    }

    /**
     * Returns the addresses of the trusted proxy servers, or an empty dry if there are none. Tries to load the
     * addresses from the cache first. If not in the cache, goes to the PropertyManager and loads from there.
     *
     * @return addresses of the trusted proxies
     */
    public Set<String> getAddresses()
    {
        return getAddressesFromPropertyManager();
    }

    /**
     * Adds to the list of servers we trust. Pass an IP address in decimal dot format (eg. 192.168.3.2)
     * Allows IP addresses with subnets as well (eg. 192.168.3.0/24)
     *
     * @param remoteAddress host name or IP address
     * @return true if the addition was successful, false otherwise
     */
    public boolean addAddress(String remoteAddress)
    {
        // never add blank address.
        if (StringUtils.isBlank(remoteAddress))
        {
            return false;
        }

        Set<String> currentAddresses = getAddressesFromPropertyManager();
        int originalNumAddresses = currentAddresses.size();

        // User can potentially have entered a DELIMITER separated list of addresses
        // Split and check validity of each address (in most cases it should only be one address)
        Set<String> newAddresses = Sets.newHashSet(currentAddresses);
        for (String address : StringUtils.split(remoteAddress, DELIMITER))
        {
            if (!StringUtils.isBlank(address))
            {
                newAddresses.add(address.trim());
            }
        }

        boolean addressesModified = (originalNumAddresses != newAddresses.size());
        if (addressesModified)
        {
            return saveAddresses(ImmutableSet.copyOf(newAddresses));
        }
        else
        {
            return false;
        }
    }

    /**
     * Removes from the list of trusted servers.
     *
     * @param remoteAddress hostname or IP address
     */
    public void removeAddress(String remoteAddress)
    {
        Set<String> trustedProxies = getAddressesFromPropertyManager();
        if (trustedProxies.contains(remoteAddress))
        {
            ImmutableSet<String> newTrustedProxies =
                ImmutableSet.copyOf(Sets.difference(trustedProxies, Collections.singleton(remoteAddress)));
            saveAddresses(newTrustedProxies);
        }
    }

    /**
     * Loads the server property that contains the list of trusted proxies. If there are none, returns null.
     *
     * @return list of proxies from the property manager
     */
    private Set<String> getAddressesFromPropertyManager()
    {
        try
        {
            String proxies = propertyManager.getTrustedProxyServers();
            if (!StringUtils.isBlank(proxies))
            {
                String[] proxyStrings = StringUtils.split(proxies, DELIMITER);
                return ImmutableSet.copyOf(proxyStrings);
            }
        }
        catch (PropertyManagerException e)
        {
            // fine - just means there are no trusted proxies in the database
            logger.debug("No proxies loaded", e);
        }
        return Collections.emptySet();
    }

    /**
     * Sets the server property with the specified list of trusted proxies.
     *
     * @param value list of trusted proxies as a comma delimited string
     * @return true if property successfully set; false otherwise.
     */
    private boolean setProperty(String value)
    {
        try
        {
            propertyManager.setTrustedProxyServers(value);
            return true;
        }
        catch (RuntimeException e)
        {
            logger.warn("Unable to save list of trusted proxy servers", e);
            return false;
        }
    }

    static String omitScopeForIpv6Loopback(String requestAddress)
    {
        if (requestAddress.equals("0:0:0:0:0:0:0:1%0"))
        {
            return "0:0:0:0:0:0:0:1";
        }
        else
        {
            return requestAddress;
        }
    }

    private boolean match(Iterable<RemoteAddress> allowedAddresses, String requestAddress)
    {
        final IPMatcher.Builder ipMatcherBuilder = IPMatcher.builder();
        for (RemoteAddress remoteAddress : allowedAddresses)
        {
            ipMatcherBuilder.addPatternOrHost(remoteAddress.getAddress());
        }
        return ipMatcherBuilder.build().matches(omitScopeForIpv6Loopback(requestAddress));
    }

    /**
     * Returns the remote addresses of trusted proxies. This method will first attempt to load the addresses from the
     * cache. If the addresses are not found in the cache, it will load the addresses from the PropertyManager.
     *
     * @return A list of the remote addresses of trusted proxies.
     */
    private Set<RemoteAddress> getTrustedProxies()
    {
        Set<String> proxies = getAddressesFromPropertyManager();
        ImmutableSet.Builder<RemoteAddress> proxyListBuilder = ImmutableSet.builder();
        for (String proxy : proxies)
        {
            proxyListBuilder.add(new RemoteAddress(proxy));
        }

        return proxyListBuilder.build();
    }

    /**
     * Saves the updated list and reloads it. Clears the trusted proxies caches.
     *
     * @param trustedProxies list of trusted proxies
     * @return <tt>true</tt> if the list of trusted proxies was saved
     */
    private boolean saveAddresses(final Collection<String> trustedProxies)
    {
        String proxies = StringUtils.join(trustedProxies, DELIMITER);

        final boolean propertySet = setProperty(proxies);
        // Clear the trusted proxies caches as list has been updated
        cacheManager.removeAll(REQUESTED_PROXIES_CACHE);
        return propertySet;
    }
}
