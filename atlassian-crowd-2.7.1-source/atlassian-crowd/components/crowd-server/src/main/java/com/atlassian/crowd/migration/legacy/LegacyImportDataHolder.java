package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchResultWithIdReferences;

import java.util.HashMap;
import java.util.Map;

/**
 * This class holds references that can be shared across other importers should the need arise.
 *
 * This does, in some way, break the independance of the mappers - but the reality is that the
 * mappers are interdependant and NEED to share data to allow for fast legacy imports.
 */
public class LegacyImportDataHolder
{
    private Map<Long, Long> oldToNewDirectoryIds = new HashMap<Long, Long>();
    private BatchResultWithIdReferences<User> userImportResults = null;

    /**
     * @return a map of directory IDs present in the XML file mapped to actual directory IDs saved in the database.
     */
    public Map<Long, Long> getOldToNewDirectoryIds()
    {
        return oldToNewDirectoryIds;
    }

    /**
     * @param oldToNewDirectoryIds a map of directory IDs present in the XML file mapped to actual directory IDs saved in the database.
     */
    public void setOldToNewDirectoryIds(final Map<Long, Long> oldToNewDirectoryIds)
    {
        this.oldToNewDirectoryIds = oldToNewDirectoryIds;
    }

    /**
     * @return results from the batch user import.
     */
    public BatchResultWithIdReferences<User> getUserImportResults()
    {
        return userImportResults;
    }

    /**
     * @param userImportResults results from the batch user import.
     */
    public void setUserImportResults(final BatchResultWithIdReferences<User> userImportResults)
    {
        this.userImportResults = userImportResults;
    }
}
