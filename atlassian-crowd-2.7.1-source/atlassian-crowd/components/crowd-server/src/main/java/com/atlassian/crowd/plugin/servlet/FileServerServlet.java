package com.atlassian.crowd.plugin.servlet;

import com.atlassian.plugin.servlet.AbstractFileServerServlet;
import com.atlassian.plugin.servlet.DownloadStrategy;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import java.util.List;

/**
 * Used to serve File from crowd, for a given number of strategies. e.g. plugin web-resources
 */
public class FileServerServlet extends AbstractFileServerServlet
{
    private List<DownloadStrategy> downloadStrategies;

    @Override
    public void init(final ServletConfig config) throws ServletException
    {
        super.init(config);
        downloadStrategies = (List<DownloadStrategy>) WebApplicationContextUtils.getWebApplicationContext(config.getServletContext()).getBean("downloadStrategies");
    }

    protected List<DownloadStrategy> getDownloadStrategies()
    {
        return downloadStrategies;
    }
}
