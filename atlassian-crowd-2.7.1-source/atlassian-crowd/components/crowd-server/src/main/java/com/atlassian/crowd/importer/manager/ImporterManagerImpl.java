package com.atlassian.crowd.importer.manager;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.exceptions.ImporterConfigurationException;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.factory.ImporterFactory;
import com.atlassian.crowd.importer.importers.Importer;
import com.atlassian.crowd.importer.model.Result;

import java.util.Set;

/**
 * The main implementation of the ImporterManager.
 *
 * @see com.atlassian.crowd.importer.manager.ImporterManager
 */
public class ImporterManagerImpl implements ImporterManager
{
    private ImporterFactory importerFactory;

    public Result performImport(Configuration configuration) throws ImporterException
    {
        // Protect against a bad configuration
        if (configuration == null)
        {
            throw new IllegalArgumentException("You cannot supply a null Configuration");
        }

        // perform validation check on Configuration
        configuration.isValid();

        // We have a valid configuration so delegate to the correct importer
        Importer importer = importerFactory.getImporterDAO(configuration);

        return importer.importUsersGroupsAndMemberships(configuration);
    }

    public boolean supportsMultipleDirectories(Configuration configuration) throws ImporterException
    {
        // Protect against a bad configuration
        if (configuration == null)
        {
            throw new IllegalArgumentException("You cannot supply a null Configuration");
        }

        // perform validation check on Configuration
        configuration.isValid();

        // We have a valid configuration so delegate to the correct importer
        Importer importer = importerFactory.getImporterDAO(configuration);

        return importer.supportsMultipleDirectories(configuration);
    }

    public Set<Directory> retrieveRemoteSourceDirectories(Configuration configuration) throws ImporterException
    {
        // Protect against a bad configuration
        if (configuration == null)
        {
            throw new IllegalArgumentException("You cannot supply a null Configuration");
        }

        // perform validation check on Configuration
        configuration.isValid();

        // We have a valid configuration so delegate to the correct importer
        Importer importer = importerFactory.getImporterDAO(configuration);

        return importer.retrieveRemoteSourceDirectory(configuration);
    }

    public void testConfiguration(Configuration configuration) throws ImporterConfigurationException
    {
        configuration.isValid();
    }

    public Set<String> getSupportedApplications()
    {
        return importerFactory.getSupportedImporterApplications();
    }

    public Set<String> getAtlassianSupportedApplications()
    {
        return importerFactory.getAtlassianSupportedImporterApplications();
    }

    public void setImporterFactory(ImporterFactory importerFactory)
    {
        this.importerFactory = importerFactory;
    }
}
