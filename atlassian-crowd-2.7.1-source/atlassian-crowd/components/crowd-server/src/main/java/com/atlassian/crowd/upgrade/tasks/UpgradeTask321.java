package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.property.PropertyManager;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Adds "SSO cookie is NOT secure" preference to the database.
 */
public class UpgradeTask321 implements UpgradeTask
{
    private Collection errors = new ArrayList();

    private PropertyManager propertyManager;

    public String getBuildNumber()
    {
        return "321";
    }

    public String getShortDescription()
    {
        return "Setting SSO Cookie to insecure for non-SSL connections";
    }

    public void doUpgrade() throws Exception
    {
        if (!propertyManager.isSecureCookie())              // "not set" will default to false.
        {
            propertyManager.setSecureCookie(false);         // force to false.
        }
    }


    public Collection getErrors()
    {
        return errors;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}
