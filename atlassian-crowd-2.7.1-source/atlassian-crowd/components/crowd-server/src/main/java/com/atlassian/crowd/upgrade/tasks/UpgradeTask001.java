package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.config.ConfigurationException;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.license.SIDManager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This upgrade task will add a server id to crowd.cfg.xml if one has not already been set
 */
public class UpgradeTask001 implements UpgradeTask
{
    private static final String TASK_NUMBER = "1";

    private static final Logger log = LoggerFactory.getLogger(UpgradeTask001.class);

    private List errors = new ArrayList();

    // Spring Managed deps, the UpgradeManager will auto-wire these.
    private CrowdBootstrapManager bootstrapManager;
    private SIDManager sidManager;

    public String getBuildNumber()
    {
        return TASK_NUMBER;
    }

    public String getShortDescription()
    {
        return "Sets the server id in the crowd.cfg.xml if one has not been set";
    }

    public void doUpgrade() throws Exception
    {
        String crowdSid = bootstrapManager.getServerID();

        if (crowdSid == null)
        {
            crowdSid = sidManager.generateSID();
            try
            {
                bootstrapManager.setServerID(crowdSid);
            }
            catch (ConfigurationException e1)
            {
                log.error("Failed to set server id property", e1);
                errors.add("Failed to set server id property, please see logs for more information");
            }
        }
    }

    public Collection getErrors()
    {
        return errors;
    }

    public void setBootstrapManager(CrowdBootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
    }

    public void setSidManager(SIDManager sidManager)
    {
        this.sidManager = sidManager;
    }
}
