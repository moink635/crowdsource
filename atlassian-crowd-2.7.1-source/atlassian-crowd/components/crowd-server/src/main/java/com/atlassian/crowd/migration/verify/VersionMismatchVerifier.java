package com.atlassian.crowd.migration.verify;

import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.util.build.BuildUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.List;

/**
 * This class will validate the correctness of an XML file to be imported into the currently running version of Crowd.
 * <p/>
 * This will stop XML files created from 'newer' versions of crowd, e.g. 2.0.3 being imported into 2.0.2 versions.
 */
public class VersionMismatchVerifier implements Verifier
{
    private final List<String> errors = new ArrayList<String>();

    public void verify(Element root)
    {
        Element buildNumberElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + XmlMigrationManagerImpl.CROWD_XML_BUILD_NUMBER);
        int buildNumber = NumberUtils.toInt(buildNumberElement.getTextTrim());

        if (buildNumber > NumberUtils.toInt(BuildUtils.BUILD_NUMBER))
        {
            Element buildVersionElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + XmlMigrationManagerImpl.CROWD_XML_VERSION);
            errors.add(new StringBuilder().
                    append("You cannot import XML data from a newer version of Crowd ").
                    append(buildVersionElement.getTextTrim()).
                    append(" (").append(buildNumber).
                    append(") into an older version ").
                    append(BuildUtils.BUILD_VERSION).append(" (").
                    append(BuildUtils.BUILD_NUMBER).append(")").toString());
        }
    }

    public void clearState()
    {
        errors.clear();
    }

    public boolean hasErrors()
    {
        return !errors.isEmpty();
    }

    public List<String> getErrors()
    {
        return errors;
    }
}
