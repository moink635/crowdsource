package com.atlassian.crowd.manager.token;

import com.atlassian.crowd.dao.token.SearchableTokenStorage;

/**
 * A marker interface for {@link SearchableTokenStorage} that provides
 * transaction-wrapped access.
 */
public interface SearchableTokenService extends SearchableTokenStorage
{
}
