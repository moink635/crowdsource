package com.atlassian.crowd.migration;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.dao.group.InternalGroupDao;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.migration.legacy.PartialXmlMapper;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.group.InternalGroupAttribute;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.*;

/**
 * This mapper will handle the mapping of a {@link com.atlassian.crowd.model.group.Group}.
 */
public class GroupMapper extends PartialXmlMapper implements Mapper
{
    /** Types of directories for which users will need to be backed up */
    private static final Set<DirectoryType> INCLUDED_DIRECTORY_TYPES =
        EnumSet.of(DirectoryType.INTERNAL, DirectoryType.DELEGATING, DirectoryType.CONNECTOR);

    private final InternalGroupDao groupDao;
    private final DirectoryDAOHibernate directoryDao;

    protected static final String GROUP_XML_ROOT = "groups";
    private static final String GROUP_XML_NODE = "group";
    private static final String GROUP_XML_DIRECTORY_ID = "directoryId";
    private static final String GROUP_XML_DESCRIPTION = "description";
    private static final String GROUP_XML_TYPE = "type";
    private static final String GROUP_XML_ATTRIBUTES = "attributes";
    private static final String GROUP_XML_ATTRIBUTE = "attribute";
    private static final String GROUP_XML_ATTRIBUTE_NAME = "name";
    private static final String GROUP_XML_ATTRIBUTE_VALUE = "value";

    public static final String REMOTE_GROUP_XML_ROOT = "groups";
    public static final String REMOTE_GROUP_XML_NODE = "group";
    public static final String REMOTE_GROUP_XML_PRINCIPAL_NAME = "name";
    public static final String REMOTE_GROUP_XML_NAME = REMOTE_GROUP_XML_PRINCIPAL_NAME;
    public static final String REMOTE_GROUP_XML_PRINCIPAL_DIRECTORY_ID = "directoryId";
    public static final String REMOTE_GROUP_XML_DIRECTORY_ID = REMOTE_GROUP_XML_PRINCIPAL_DIRECTORY_ID;
    public static final String REMOTE_GROUP_XML_ACTIVE = "active";
    public static final String REMOTE_GROUP_XML_DESCRIPTION = "description";
    public static final String REMOTE_GROUP_XML_PRINCIPAL_NODE = "principals";
    public static final String REMOTE_GROUP_XML_PRINCIPAL = "principal";

    public GroupMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, GroupDAOHibernate groupDao,
                       DirectoryDAOHibernate directoryDao, DirectoryManager directoryManager)
    {
        super(sessionFactory, batchProcessor, directoryManager, INCLUDED_DIRECTORY_TYPES);
        this.groupDao = groupDao;
        this.directoryDao = directoryDao;
    }

    public Element exportXml(Map options) throws ExportException
    {
        Element groupRoot = DocumentHelper.createElement(GROUP_XML_ROOT);

        // only export users from certain directory types
        List<Directory> directories = findAllExportableDirectories();

        for (Directory directory : directories)
        {
            for (InternalGroup group : findExportableGroups(directory))
            {
                Set<InternalGroupAttribute> attributes = groupDao.findGroupAttributes(group.getId());

                addGroupToXml(group, attributes, groupRoot);
            }
        }

        return groupRoot;
    }

    private List<InternalGroup> findExportableGroups(Directory directory)
    {
        final QueryBuilder.PartialEntityQuery<InternalGroup> partialQuery = QueryBuilder.queryFor(InternalGroup.class, EntityDescriptor.group());
        final EntityQuery<InternalGroup> query;
        if (isExportOfNonLocalGroupsRequired(directory.getType()))
        {
            // export all groups, local and non-local
            query = partialQuery.returningAtMost(EntityQuery.ALL_RESULTS);
        }
        else
        {
            // export just local groups, skip non-local groups
            query = partialQuery.with(Restriction.on(GroupTermKeys.LOCAL).exactlyMatching(true)).returningAtMost(EntityQuery.ALL_RESULTS);
        }
        return groupDao.search(directory.getId(), query);
    }

    protected void addGroupToXml(final InternalGroup group, final Set<InternalGroupAttribute> attributes, final Element groupRoot)
    {
        Element groupElement = groupRoot.addElement(GROUP_XML_NODE);
        groupElement.addElement(GROUP_XML_DIRECTORY_ID).addText(String.valueOf(group.getDirectoryId()));
        exportInternalEntity(group, groupElement);
        groupElement.addElement(GROUP_XML_DESCRIPTION).addText(StringUtils.defaultString(group.getDescription()));
        groupElement.addElement(GROUP_XML_TYPE).addText(group.getType().name());

        addGroupAttributesToXml(attributes, groupElement);
    }

    private void addGroupAttributesToXml(final Set<InternalGroupAttribute> attributes, final Element groupElement)
    {
        Element attributesElement = groupElement.addElement(GROUP_XML_ATTRIBUTES);
        for (InternalGroupAttribute attribute : attributes)
        {
            Element attributeElement = attributesElement.addElement(GROUP_XML_ATTRIBUTE);
            attributeElement.addElement(GENERIC_XML_ID).addText(attribute.getId().toString());
            attributeElement.addElement(GROUP_XML_ATTRIBUTE_NAME).addText(attribute.getName());
            attributeElement.addElement(GROUP_XML_ATTRIBUTE_VALUE).addText(attribute.getValue());
        }
    }

    public void importXml(final Element root) throws ImportException
    {
        Element groupsElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + GROUP_XML_ROOT);
        if (groupsElement == null)
        {
            logger.error("No groups were found for importing!");
            return;
        }

        // TODO: we should stream the XML and chunkinise this process rather than beg for OOMEs
        List<InternalGroup> groupsToReplicate = new ArrayList<InternalGroup>();
        List<InternalGroupAttribute> groupAttributesToReplicate = new ArrayList<InternalGroupAttribute>();

        for (Iterator groupElementIterator = groupsElement.elementIterator(); groupElementIterator.hasNext(); )
        {
            Element groupElement = (Element) groupElementIterator.next();

            InternalGroup group = getGroupFromXml(groupElement);

            // Only import groups from internal/delegated directories
            if (isImportableDirectory(group.getDirectory()))
            {
                groupsToReplicate.add(group);
            }

            Set<InternalGroupAttribute> attributes = getGroupAttributesFromXml(groupElement, group);
            groupAttributesToReplicate.addAll(attributes);
        }

        logger.info("About to replicate " + groupsToReplicate.size() + " groups");
        addEntities(groupsToReplicate);
        logger.info("Group replication complete");

        logger.info("About to replicate " + groupAttributesToReplicate.size() + " group attributes");
        addEntities(groupAttributesToReplicate);
        logger.info("Group attribute replication complete");
    }

    protected InternalGroup getGroupFromXml(Element groupElement)
    {
        Long directoryId = Long.parseLong(groupElement.element(GROUP_XML_DIRECTORY_ID).getText());
        DirectoryImpl directory = (DirectoryImpl) directoryDao.loadReference(directoryId);

        InternalEntityTemplate internalEntityTemplate = getInternalEntityTemplateFromXml(groupElement);

        GroupType groupType = GroupType.valueOf(groupElement.element(GROUP_XML_TYPE).getText());

        GroupTemplate groupTemplate = new GroupTemplate(internalEntityTemplate.getName(), directoryId, groupType);
        groupTemplate.setActive(internalEntityTemplate.isActive());
        groupTemplate.setDescription(groupElement.element(GROUP_XML_DESCRIPTION).getText());

        final InternalGroup group = new InternalGroup(internalEntityTemplate, directory, groupTemplate);
        group.setLocal(true);  // all groups from the XML are local
        return group;
    }

    protected Set<InternalGroupAttribute> getGroupAttributesFromXml(Element groupElement, InternalGroup groupReference)
    {
        Set<InternalGroupAttribute> attributes = new HashSet<InternalGroupAttribute>();

        Element attributesElement = groupElement.element(GROUP_XML_ATTRIBUTES);
        for (Iterator attribsIterator = attributesElement.elementIterator(); attribsIterator.hasNext(); )
        {
            Element attributeElement = (Element) attribsIterator.next();
            Long id = Long.parseLong(attributeElement.element(GENERIC_XML_ID).getText());
            String name = attributeElement.element(GROUP_XML_ATTRIBUTE_NAME).getText();
            String value = attributeElement.element(GROUP_XML_ATTRIBUTE_VALUE).getText();

            InternalGroupAttribute attribute = new InternalGroupAttribute(id, groupReference, name, value);
            attributes.add(attribute);
        }

        return attributes;
    }
}
