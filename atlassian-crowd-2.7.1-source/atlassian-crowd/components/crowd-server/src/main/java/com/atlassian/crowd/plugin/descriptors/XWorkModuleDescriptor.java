package com.atlassian.crowd.plugin.descriptors;

import com.atlassian.crowd.event.XWorkStateChangeEvent;
import com.atlassian.crowd.plugin.descriptors.webwork.PluginAwareActionConfig;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import com.opensymphony.util.ClassLoaderUtil;
import com.opensymphony.util.TextUtils;
import com.opensymphony.xwork.ObjectFactory;
import com.opensymphony.xwork.config.Configuration;
import com.opensymphony.xwork.config.ConfigurationException;
import com.opensymphony.xwork.config.ConfigurationManager;
import com.opensymphony.xwork.config.ConfigurationProvider;
import com.opensymphony.xwork.config.ConfigurationUtil;
import com.opensymphony.xwork.config.ExternalReferenceResolver;
import com.opensymphony.xwork.config.entities.ExternalReference;
import com.opensymphony.xwork.config.entities.InterceptorConfig;
import com.opensymphony.xwork.config.entities.InterceptorStackConfig;
import com.opensymphony.xwork.config.entities.PackageConfig;
import com.opensymphony.xwork.config.entities.ResultConfig;
import com.opensymphony.xwork.config.entities.ResultTypeConfig;
import com.opensymphony.xwork.config.providers.InterceptorBuilder;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class XWorkModuleDescriptor extends AbstractModuleDescriptor implements StateAware, ConfigurationProvider
{
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * The list of PackageConfig instances, as defined in the xwork module descriptor.
     */
    private List packages;

    /**
     * The element within the plugin xml file that contains the xwork module definition.
     */
    private Element element;

    /**
     * Maintains the enabled/disabled state of this xwork module. see StateAware
     */
    private boolean enabled;

    private final EventPublisher eventPublisher;

    public XWorkModuleDescriptor(ModuleFactory moduleFactory, EventPublisher eventPublisher)
    {
        super(moduleFactory);
        this.eventPublisher = eventPublisher;
    }

    public void init(Plugin plugin, Element element) throws PluginParseException
    {
        super.init(plugin, element);

        try
        {
            this.enabled = false;
            this.element = element;
            // This initial call to getConfigurationProviders() ensures that the default provider
            // XmlConfigurationProvider is registered. This is critical to ensure that the xwork.xml file has a handler.
            ConfigurationManager.getConfigurationProviders();
            ConfigurationManager.addConfigurationProvider(this);
        }
        catch (Throwable t)
        {
            log.error("Failed to add configuration provider", t);
        }
    }

    public void enabled()
    {
        super.enabled();

        if (!enabled)
        {
            enabled = true;
            eventPublisher.publish(new XWorkStateChangeEvent(this));
        }
    }

    public void disabled()
    {
        if (enabled)
        {
            enabled = false;
            eventPublisher.publish(new XWorkStateChangeEvent(this));
        }
        super.disabled();
    }

    public boolean isEnabled()
    {
        return enabled;
    }

    private Configuration getXWorkConfiguration()
    {
        return ConfigurationManager.getConfiguration();
    }

    private List getPackages(Element element, Configuration configuration)
    {
        List configuredPackages = new ArrayList();
        Iterator it = element.elementIterator("package");
        while (it.hasNext())
        {
            Element packageElement = (Element) it.next();
            PackageConfig newPackage = buildPackageContext(packageElement, configuration);
            log.debug("Loading {}", newPackage);

            try
            {
                addResultTypes(newPackage, packageElement);
                loadInterceptors(newPackage, packageElement);
                loadDefaultInterceptorRef(newPackage, packageElement);
                loadGlobalResults(newPackage, packageElement);
                Iterator actionIt = packageElement.elementIterator("action");
                while (actionIt.hasNext())
                {
                    Element actionElement = (Element) actionIt.next();
                    addAction(actionElement, newPackage);
                }
                configuredPackages.add(newPackage);
            }
            catch (ConfigurationException e)
            {
                log.error("Could not add package: " + newPackage + " because: " + e, e);
            }
            catch (ClassNotFoundException e)
            {
                log.error("Could not load action: " + e.getMessage(), e);
            }
        }

        return configuredPackages;
    }

    protected void loadInterceptors(PackageConfig context, Element element) throws ConfigurationException
    {
        List interceptorList = element.elements("interceptor");
        for (int i = 0; i < interceptorList.size(); i++)
        {
            Element interceptorElement = (Element) interceptorList.get(i);
            String name = interceptorElement.attributeValue("name");
            String className = interceptorElement.attributeValue("class");
            Map params = getParams(interceptorElement);
            InterceptorConfig config;
            try
            {
                config = new InterceptorConfig(name, className, params);
                ObjectFactory.getObjectFactory().buildInterceptor(config, new HashMap());
                context.addInterceptorConfig(config);
            }
            catch (ConfigurationException e)
            {
                String s = "Unable to load class " + className + " for interceptor name " + name + ". This interceptor will not be available.";
                log.error(s);
                throw e;
            }
            context.addInterceptorConfig(config);
        }

        loadInterceptorStacks(element, context);
    }

    protected void loadDefaultInterceptorRef(PackageConfig packageContext, Element element)
    {
        List resultTypeList = element.elements("default-interceptor-ref");
        if (resultTypeList.size() > 0)
        {
            Element defaultRefElement = (Element) resultTypeList.get(0);
            packageContext.setDefaultInterceptorRef(defaultRefElement.attributeValue("name"));
        }
    }

    protected void loadGlobalResults(PackageConfig packageContext, Element packageElement) throws ClassNotFoundException
    {
        List globalResultList = packageElement.elements("global-results");
        if (globalResultList.size() > 0)
        {
            Element globalResultElement = (Element) globalResultList.get(0);
            Map results = buildResults(globalResultElement, packageContext);
            packageContext.addGlobalResultConfigs(results);
        }
    }

    protected Map buildResults(Element element, PackageConfig packageContext) throws ClassNotFoundException
    {
        List resultEls = element.elements("result");
        Map<String, ResultConfig> results = new HashMap<String, ResultConfig>();
        for (int i = 0; i < resultEls.size(); i++)
        {
            Element resultElement = (Element) resultEls.get(i);
            if (!resultElement.getParent().equals(element))
            {
                continue;
            }
            String resultName = resultElement.attributeValue("name");
            String resultType = resultElement.attributeValue("type");
            if (!TextUtils.stringSet(resultType))
            {
                resultType = packageContext.getFullDefaultResultType();
            }
            ResultTypeConfig config = (ResultTypeConfig) packageContext.getAllResultTypeConfigs().get(resultType);
            if (config == null)
            {
                throw new ConfigurationException(
                        "There is no result type defined for type '" + resultType + "' mapped with name '" + resultName + "'");
            }
            Class resultClass = ClassLoaderUtil.loadClass(config.getClazz(), XWorkModuleDescriptor.class);
            if (resultClass == null)
            {
                throw new ConfigurationException(
                        "Result type '" + resultType + "' is invalid. Modify your xwork.xml file.");
            }
            Map<String, String> params = getParams(resultElement);
            if (params.size() == 0 && resultElement.elements().size() == 0 && resultElement.getText() != null)
            {
                params = new HashMap<String, String>();
                try
                {
                    String paramName = (String) resultClass.getField("DEFAULT_PARAM").get(null);
                    params.put(paramName, resultElement.getText());
                }
                catch (Throwable t)
                {
                    //noop.
                }
            }
            ResultConfig resultConfig = new ResultConfig(resultName, resultClass.getCanonicalName(), params);
            results.put(resultConfig.getName(), resultConfig);
        }

        return results;
    }

    private List lookupInterceptorReference(PackageConfig context, Element interceptorRefElement) throws ConfigurationException
    {
        String refName = interceptorRefElement.attributeValue("name");
        Map refParams = getParams(interceptorRefElement);
        return InterceptorBuilder.constructInterceptorReference(context, refName, refParams);
    }

    protected InterceptorStackConfig loadInterceptorStack(Element element, PackageConfig context) throws ConfigurationException
    {
        String name = element.attributeValue("name");
        InterceptorStackConfig config = new InterceptorStackConfig(name);
        List interceptorRefList = element.elements("interceptor-ref");
        for (int j = 0; j < interceptorRefList.size(); j++)
        {
            Element interceptorRefElement = (Element) interceptorRefList.get(j);
            List interceptors = lookupInterceptorReference(context, interceptorRefElement);
            config.addInterceptors(interceptors);
        }

        return config;
    }

    protected void loadInterceptorStacks(Element element, PackageConfig context) throws ConfigurationException
    {
        List interceptorStackList = element.elements("interceptor-stack");
        for (int i = 0; i < interceptorStackList.size(); i++)
        {
            Element interceptorStackElement = (Element) interceptorStackList.get(i);
            InterceptorStackConfig config = loadInterceptorStack(interceptorStackElement, context);
            context.addInterceptorStackConfig(config);
        }

    }

    protected void addAction(Element actionElement, PackageConfig packageContext) throws ConfigurationException, ClassNotFoundException
    {
        String name = actionElement.attributeValue("name");
        String className = actionElement.attributeValue("class");
        String methodName = actionElement.attributeValue("method");

        methodName = (methodName == null || methodName.trim().length() <= 0) ? null : methodName.trim();
        try
        {
            PluginAwareActionConfig actionConfig = new PluginAwareActionConfig(null, className, null, null, null);
            actionConfig.setPlugin(plugin);
            ObjectFactory.getObjectFactory().buildAction(name, packageContext.getNamespace(), actionConfig, null);
        }
        catch (Exception e)
        {
            log.error("Action class [" + className + "] not found, skipping action [" + name + "]", e);
            throw new RuntimeException(e);
        }
        Map<String, String> actionParams = getParams(actionElement);
        Map results;
        try
        {
            results = buildResults(actionElement, packageContext);
        }
        catch (ConfigurationException e)
        {
            throw new ConfigurationException("Error building results for action " + name + " in namespace " + packageContext.getNamespace(), e);
        }
        List interceptorList = buildInterceptorList(actionElement, packageContext);
        List externalrefs = buildExternalRefs(actionElement, packageContext);
        PluginAwareActionConfig actionConfig = new PluginAwareActionConfig(methodName, className, actionParams, results, interceptorList, externalrefs, packageContext.getName());
        actionConfig.setPlugin(plugin);
        packageContext.addActionConfig(name, actionConfig);
        if (log.isDebugEnabled())
        {
            log.debug("Loaded " + (TextUtils.stringSet(
                    packageContext.getNamespace()) ? packageContext.getNamespace() + "/" : "") + name + " in '" + packageContext.getName() + "' package:" + actionConfig);
        }
    }

    protected List buildExternalRefs(Element element, PackageConfig context) throws ConfigurationException
    {
        List refs = new ArrayList();
        List externalRefList = element.elements("external-ref");
        String refName;
        String refValue = null;
        String requiredTemp;
        for (int i = 0; i < externalRefList.size(); i++)
        {
            Element refElement = (Element) externalRefList.get(i);
            if (!refElement.getParent().equals(element))
            {
                continue;
            }
            refName = refElement.attributeValue("name");
            if (refElement.elements().size() > 0)
            {
                refValue = ((Element) refElement.elements().get(0)).getText();
            }
            requiredTemp = refElement.attributeValue("required");
            boolean required;
            if (requiredTemp == null || "".equals(requiredTemp))
            {
                required = true;
            }
            else
            {
                required = Boolean.valueOf(requiredTemp).booleanValue();
            }
            refs.add(new ExternalReference(refName, refValue, required));
        }

        return refs;
    }

    protected void addResultTypes(PackageConfig packageContext, Element element)
    {
        List resultTypeList = element.elements("result-type");
        for (int i = 0; i < resultTypeList.size(); i++)
        {
            Element resultTypeElement = (Element) resultTypeList.get(i);
            String name = resultTypeElement.attributeValue("name");
            String className = resultTypeElement.attributeValue("class");
            String def = resultTypeElement.attributeValue("default");
            try
            {
                Class clazz = ClassLoaderUtil.loadClass(className, com.opensymphony.xwork.config.providers.XmlConfigurationProvider.class);
                ResultTypeConfig resultType = new ResultTypeConfig(name, clazz.getCanonicalName(), null);
                packageContext.addResultTypeConfig(resultType);
                if ("true".equals(def))
                {
                    packageContext.setDefaultResultType(name);
                }
            }
            catch (ClassNotFoundException e)
            {
                log.error("Result class [" + className + "] doesn't exist, ignoring");
            }
        }

    }

    public Object getModule()
    {
        return null;
    }

    protected PackageConfig buildPackageContext(Element packageElement, Configuration configuration)
    {
        String name = TextUtils.noNull(packageElement.attributeValue("name"));
        String namespace = TextUtils.noNull(packageElement.attributeValue("namespace"));
        String parent = packageElement.attributeValue("extends");
        String abstractVal = packageElement.attributeValue("abstract");
        boolean isAbstract = Boolean.valueOf(abstractVal).booleanValue();
        ExternalReferenceResolver erResolver = null;
        String externalReferenceResolver = TextUtils.noNull(packageElement.attributeValue("externalReferenceResolver"));
        if (!"".equals(externalReferenceResolver))
        {
            try
            {
                Class erResolverClazz = ClassLoaderUtil.loadClass(externalReferenceResolver,
                        com.opensymphony.xwork.config.ExternalReferenceResolver.class);
                erResolver = (ExternalReferenceResolver) erResolverClazz.newInstance();
            }
            catch (ClassNotFoundException e)
            {
                String msg = "Could not find External Reference Resolver: " + externalReferenceResolver + ". " + e.getMessage();
                log.error(msg);
                throw new ConfigurationException(msg, e);
            }
            catch (Exception e)
            {
                String msg = "Could not create External Reference Resolver: " + externalReferenceResolver + ". " + e.getMessage();
                log.error(msg);
                throw new ConfigurationException(msg, e);
            }
        }
        PackageConfig pkgConfig = null;
        if (!TextUtils.stringSet(TextUtils.noNull(parent)))
        {
            pkgConfig = new PackageConfig(name, namespace, isAbstract, erResolver);
        }
        List parents = ConfigurationUtil.buildParentsFromString(configuration, parent);
        if (parents.size() <= 0)
        {
            log.error("Unable to find parent packages " + parent);
            pkgConfig = new PackageConfig(name, namespace, isAbstract, erResolver);
        }
        else
        {
            pkgConfig = new PackageConfig(name, namespace, isAbstract, erResolver, parents);
        }
        return pkgConfig;
    }


    protected List buildInterceptorList(Element element, PackageConfig context) throws ConfigurationException
    {
        List interceptorList = new ArrayList();
        List interceptorRefList = element.elements("interceptor-ref");
        for (int i = 0; i < interceptorRefList.size(); i++)
        {
            Element interceptorRefElement = (Element) interceptorRefList.get(i);
            if (interceptorRefElement.getParent().equals(element))
            {
                List interceptors = lookupInterceptorReference(context, interceptorRefElement);
                interceptorList.addAll(interceptors);
            }
        }

        return interceptorList;
    }

    public static Map<String, String> getParams(Element paramsElement)
    {
        Map<String, String> params = new HashMap<String, String>();
        if (paramsElement == null)
        {
            return params;
        }
        List<?> childNodes = paramsElement.elements();
        for (int i = 0; i < childNodes.size(); i++)
        {
            Element childNode = (Element) childNodes.get(i);
            if (childNode.getNodeType() != 1 || !"param".equals(childNode.getName()))
            {
                continue;
            }

            String paramName = childNode.attributeValue("name");
            if (childNode.getText() != null)
            {
                String paramValue = childNode.getText();
                params.put(paramName, paramValue);
            }
        }
        return params;
    }

    //---( implementation of the ConfigurationProvider interface)---

    public void destroy()
    {

    }

    public void init(Configuration configuration) throws ConfigurationException
    {
        if (enabled)
        {
            try
            {
                Iterator i = getPackages(configuration).iterator();
                while (i.hasNext())
                {
                    PackageConfig packageConfig = (PackageConfig) i.next();
                    configuration.addPackageConfig(packageConfig.getName(), packageConfig);
                }
            }
            catch (RuntimeException e)
            {
                enabled = false;
                throw e;
            }
        }
    }

    public boolean needsReload()
    {
        return enabled;
    }

    /**
     * Lazily convert the element into its xwork package definitions.
     */
    private List getPackages(Configuration configuration)
    {
        packages = getPackages(element, configuration);

        return packages;
    }
}
