package com.atlassian.crowd.manager.upgrade;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.atlassian.config.ConfigurationException;
import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.crowd.dao.token.ResetPasswordTokenDaoMemory;
import com.atlassian.crowd.dao.token.TokenDAOMemory;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.upgrade.tasks.UpgradeTask;
import com.atlassian.crowd.util.build.BuildUtils;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import net.sf.ehcache.CacheManager;

@Transactional
public class UpgradeManagerImpl implements UpgradeManager
{
    private final Logger logger = LoggerFactory.getLogger(UpgradeManagerImpl.class);

    private CrowdBootstrapManager bootstrapManager;

    // spring wired deps
    private List<UpgradeTask> upgradeTasks = new ArrayList<UpgradeTask>();

    private PropertyManager propertyManager;
    private SessionFactory sessionFactory;
    private CacheManager cacheManager;

    /**
     * @return the build number of the currently executing version of Crowd.
     *         The Crowd data will be upgraded to this version.
     */
    protected int getApplicationBuildNumber()
    {
        return Integer.parseInt(BuildUtils.BUILD_NUMBER);
    }

    /**
     * @return the build number of the data in the database.
     *         The Crowd data will be upgraded from this version.
     */
    public int getDataBuildNumber()
    {
        int buildNumber;
        try
        {
            buildNumber = propertyManager.getBuildNumber();
        }
        catch (PropertyManagerException e)
        {
            // Using 0 here since we did not originally store this value in the DB
            buildNumber = 0;
        }

        return buildNumber;
    }

    public List<UpgradeTask> getRequiredUpgrades()
    {
        List<UpgradeTask> requiredUpgrades = new ArrayList<UpgradeTask>();

        int currentBuildNumber = getDataBuildNumber();
        int finalBuildNumber = getApplicationBuildNumber();

        try
        {
            Collection<UpgradeTask> upgradeTasks = getUpgradeTasks();

            for (UpgradeTask upgradeTask : upgradeTasks)
            {
                String build = upgradeTask.getBuildNumber();

                int upgradeTaskBuildNumber = Integer.parseInt(build);

                if (upgradeTaskBuildNumber > currentBuildNumber && upgradeTaskBuildNumber <= finalBuildNumber)
                {
                    // need to process upgrade task so construct and add to list
                    requiredUpgrades.add(upgradeTask);
                }
            }
        }
        catch (Throwable e)
        {
            logger.error("Failed to get required upgrades: " + e.getMessage(), e);
        }

        return requiredUpgrades;
    }

    public boolean needUpgrade()
    {
        // this is a '!=' and not a '>' because we need to "doUpgrades" to always put the applicationBuildNumber into the database (kind of retarded logic)
        return getApplicationBuildNumber() != getDataBuildNumber();
    }

    public Collection<String> doUpgrade() throws Exception
    {
        String lastSuccessfullyRanUpgrade = null;
        Collection<String> errors = new ArrayList<String>();

        if (needUpgrade())
        {
            for (UpgradeTask task : getRequiredUpgrades())
            {
                logger.info("Running upgrade task for build - " + task.getBuildNumber() + ": " + task.getShortDescription());

                try
                {
                    task.doUpgrade();
                }
                catch (Exception e)
                {
                    logger.error(e.getMessage(), e);
                    errors.add("Upgrade task for build " + task.getBuildNumber() + " failed with exception: " + e.getMessage());
                }

                if (!task.getErrors().isEmpty())
                {
                    errors.addAll(task.getErrors());

                    // if one task fails, we don't want to keep going with the others
                    break;
                }

                if (errors.isEmpty())
                {
                    // a successful task - now we update the number to this build number as the last task
                    // which ran successfully.
                    lastSuccessfullyRanUpgrade = task.getBuildNumber();
                }
            }

            // we have now done all the upgrades, but the current build may be
            // greater than the last upgrade build number (for e.g. current build is build 20, but only
            // build 15 had an upgrade, 16-20 didn't. we still need to update build number to 20).
            if (errors.isEmpty())
            {
                setCurrentBuildNumber(getApplicationBuildNumber());
            }
            else if (lastSuccessfullyRanUpgrade != null)
            {
                // set the current build number to the last successful upgrade task run.
                setCurrentBuildNumber(Integer.parseInt(lastSuccessfullyRanUpgrade));
            }

            flushAndClearHibernate();
        }

        return errors;
    }

    private static final Set<String> RETAINED_CACHES_ON_UPGRADE = ImmutableSet.of(
            TokenDAOMemory.IDENTIFIER_HASH_CACHE,
            TokenDAOMemory.RANDOM_HASH_CACHE,
            ResetPasswordTokenDaoMemory.CACHE_NAME
    );


    protected void flushAndClearHibernate()
    {
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
        clearCaches();
    }

    void clearCaches()
    {
        logger.info("Clearing non-persisted caches after upgrade");

        Set<String> cacheNames = ImmutableSet.copyOf(cacheManager.getCacheNames());

        for (String cacheName : Sets.difference(cacheNames, RETAINED_CACHES_ON_UPGRADE))
        {
            logger.debug("Clearing cache: {}", cacheName);
            cacheManager.getCache(cacheName).removeAll();
        }

        logger.info("These persisted caches have been left uncleared: {}", Sets.intersection(cacheNames, RETAINED_CACHES_ON_UPGRADE));
    }

    private void setCurrentBuildNumber(int buildNumber)
    {
        try
        {
            propertyManager.setBuildNumber(buildNumber);

            // also store in crowd.cfg.xml for neatness
            // (note: we actually never use this value, required by atlassian-config)
            getBootstrapManager().setBuildNumber(Integer.toString(buildNumber));
            getBootstrapManager().save();
        }
        catch (ConfigurationException e)
        {
            logger.error(e.getMessage(), e);
        }
    }

    public PropertyManager getPropertyManager()
    {
        return propertyManager;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    public CrowdBootstrapManager getBootstrapManager()
    {
        if (bootstrapManager == null)
        {
            bootstrapManager = (CrowdBootstrapManager) BootstrapUtils.getBootstrapManager();
        }
        return bootstrapManager;
    }

    public void setBootstrapManager(CrowdBootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
    }

    public void setSessionFactory(final SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    public void setCacheManager(final CacheManager cacheManager)
    {
        this.cacheManager = cacheManager;
    }

    public List<UpgradeTask> getUpgradeTasks()
 	{
 	    return upgradeTasks;
 	}

 	public void setUpgradeTasks(List<UpgradeTask> upgradeTasks)
 	{
 	    this.upgradeTasks = upgradeTasks;
 	}
}
