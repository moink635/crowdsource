package com.atlassian.crowd.plugin.web.conditions;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

/**
 * Returns true if the Application is of type {@link com.atlassian.crowd.model.application.ApplicationType#PLUGIN}
 */
public class ApplicationPluginCondition implements Condition
{
    private String applicationID = null;

    private ApplicationManager applicationManager;

    public void init(Map params) throws PluginParseException
    {
        this.applicationID = (String) params.get("applicationID");
    }

    public boolean shouldDisplay(Map context)
    {
        String id = (String) context.get(applicationID);
        if (id != null)
        {
            try
            {
                Application application = applicationManager.findById(Integer.valueOf(id));

                if (application.getType() == ApplicationType.PLUGIN)
                {
                    return false;
                }
            }
            catch (ApplicationNotFoundException e)
            {
                // failed to find application
            }
        }

        return true;
    }

    public void setApplicationManager(ApplicationManager applicationManager)
    {
        this.applicationManager = applicationManager;
    }
}
