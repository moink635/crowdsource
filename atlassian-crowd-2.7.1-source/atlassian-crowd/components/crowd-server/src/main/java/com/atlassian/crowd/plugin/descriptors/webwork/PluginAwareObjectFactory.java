package com.atlassian.crowd.plugin.descriptors.webwork;

import com.atlassian.plugin.AutowireCapablePlugin;
import com.opensymphony.webwork.spring.WebWorkSpringObjectFactory;
import com.opensymphony.xwork.config.entities.ActionConfig;

import java.util.Map;

/**
 * An Spring autowiring object factory that uses the classloader
 * of the of PluginAware action configs to load and wire up
 * the action class.
 *
 * Non-plugin actions are wired up using the standard
 * WebWorkSpringObjectFactory mechanism.
 */

public class PluginAwareObjectFactory extends WebWorkSpringObjectFactory
{
    public Object buildAction(String s, String s1, ActionConfig actionConfig, Map map) throws Exception
    {
        Object action = null;
        if (actionConfig instanceof PluginAwareActionConfig)
        {
            PluginAwareActionConfig cfg = (PluginAwareActionConfig) actionConfig;
            if (cfg.getPlugin() instanceof AutowireCapablePlugin)
            {
                Class cls = cfg.getPlugin().loadClass(cfg.getClassName(), getClass());
                action = ((AutowireCapablePlugin) cfg.getPlugin()).autowire(cls);
            }
        }
        
        if (action == null)
        {
            action = super.buildAction(s, s1, actionConfig, map);
        }

        return action;
    }
}
