package com.atlassian.crowd.plugin;

import com.atlassian.plugin.hostcontainer.HostContainer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.lang.reflect.Constructor;
import java.util.Arrays;

public class CrowdHostContainer implements HostContainer, ApplicationContextAware
{
    private ApplicationContext applicationContext;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public <T> T create(Class<T> tClass) throws IllegalArgumentException
    {
        // Looks like AUTOWIRE_AUTODETECT falls back to autowiring by type, not name, which is THE WRONG THING.
        // So we fall back on some reasonably nasty manual reflection hackery.
        for (Constructor constructor : tClass.getConstructors())
        {
            if (constructor.getParameterTypes().length == 0 && (constructor.getAnnotation(Deprecated.class) == null))
                return tClass.cast(applicationContext.getAutowireCapableBeanFactory().createBean(tClass, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false));
        }

        return tClass.cast(applicationContext.getAutowireCapableBeanFactory().createBean(tClass, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, false));
    }

    public <T> T getInstance(Class<T> tClass)
    {
        String[] componentNames = applicationContext.getBeanNamesForType(tClass);

        if (componentNames.length == 1)
        {
            return tClass.cast(applicationContext.getBean(componentNames[0]));
        }
        else
        {
            log.warn("Unable to determine best bean of type " + tClass.getName() + " from beans: " + Arrays.toString(componentNames));
            return null;
        }
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
        this.applicationContext = applicationContext;
    }
}
