package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.token.TokenManager;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Removes all existing Tokens.
 *
 * Prior to 2.0, this upgrade task added the "secret number" field to Tokens.
 */
public class UpgradeTask360 implements UpgradeTask
{
    private final Collection<String> errors = new ArrayList<String>();

    private TokenManager tokenManager;

    public String getBuildNumber()
    {
        return "360";
    }

    public String getShortDescription()
    {
        return "Removing all existing Tokens";
    }

    public void doUpgrade() throws Exception
    {
        // removes ALL tokens from all DAO implementations (hibernate + memory)
        tokenManager.removeAll();
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setTokenManager(TokenManager tokenManager)
    {
        this.tokenManager = tokenManager;
    }
}
