package com.atlassian.crowd.service.soap.server;

import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.integration.exception.ApplicationPermissionException;
import com.atlassian.crowd.integration.soap.*;
import com.atlassian.crowd.manager.application.ApplicationAccessDeniedException;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.manager.login.ForgottenLoginManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.model.authentication.AuthenticatedToken;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.crowd.model.user.*;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.SearchContext;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.GroupQuery;
import com.atlassian.crowd.search.query.entity.UserQuery;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestriction;
import com.atlassian.crowd.search.query.entity.restriction.BooleanRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.PropertyRestriction;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.service.soap.ObjectTranslator;
import com.atlassian.crowd.service.soap.SOAPService;
import com.atlassian.crowd.util.SoapExceptionTranslator;
import com.atlassian.crowd.util.SoapObjectTranslator;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.rmi.RemoteException;
import java.util.*;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

/**
 * <p>Crowd XFire SOAP service implementation.</p>
 *
 * @see com.atlassian.crowd.service.soap.client.SecurityServerClient
 */
public class SecurityServerGeneric implements SecurityServer
{
    private static final Logger logger = LoggerFactory.getLogger(SecurityServerGeneric.class);
    private static final String SHOULD_NEVER_REACH_HERE_MSG = "Should never reach here.";

    private final SOAPService soapService;
    private final ApplicationService applicationService;
    private final TokenAuthenticationManager tokenAuthenticationManager;
    private final PropertyManager propertyManager;
    private final ForgottenLoginManager forgottenLoginManager;

    public SecurityServerGeneric(final SOAPService soapService, final ApplicationService applicationService,
            final PropertyManager propertyManager, TokenAuthenticationManager tokenAuthenticationManager,
            ForgottenLoginManager forgottenLoginManager)
    {
        this.soapService = soapService;
        this.applicationService = applicationService;
        this.propertyManager = propertyManager;
        this.tokenAuthenticationManager = tokenAuthenticationManager;
        this.forgottenLoginManager = forgottenLoginManager;
    }

    @Override
    public String createPrincipalToken(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String username, com.atlassian.crowd.integration.authentication.ValidationFactor[] validationFactors)
            throws RemoteException, com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.InactiveAccountException, com.atlassian.crowd.integration.exception.InvalidAuthenticationException
    {
        try
        {
            // validate the service
            soapService.validateSOAPService(applicationToken);

            UserAuthenticationContext userAuthenticationContext = new UserAuthenticationContext();

            userAuthenticationContext.setApplication(applicationToken.getName());

            userAuthenticationContext.setCredential(null);

            userAuthenticationContext.setValidationFactors(SoapObjectTranslator.fromSoapValidationFactors(validationFactors));

            userAuthenticationContext.setName(username);

            // authenticate the principal
            Token principalToken = tokenAuthenticationManager.authenticateUserWithoutValidatingPassword(userAuthenticationContext);

            return principalToken.getRandomHash();
        }
        catch (com.atlassian.crowd.exception.OperationFailedException e)
        {
            logger.info(e.getMessage(), e);
            throw new RemoteException(e.getMessage(), e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.manager.application.ApplicationAccessDeniedException e)
        {
            throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InactiveAccountException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthenticationException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationNotFoundException e)
        {
            throw new com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public String authenticatePrincipalSimple(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String username, String password) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthenticationException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.InactiveAccountException, com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException, com.atlassian.crowd.integration.exception.ExpiredCredentialException
    {
        try
        {
            // validate the service
            soapService.validateSOAPService(applicationToken);

            UserAuthenticationContext userAuthenticationContext = new UserAuthenticationContext();

            userAuthenticationContext.setApplication(applicationToken.getName());

            com.atlassian.crowd.embedded.api.PasswordCredential passwordCredential = new com.atlassian.crowd.embedded.api.PasswordCredential(password);
            userAuthenticationContext.setCredential(passwordCredential);
            userAuthenticationContext.setValidationFactors(new ValidationFactor[0]);

            userAuthenticationContext.setName(username);

            // authenticate the principal
            Token principalToken =
                tokenAuthenticationManager.authenticateUser(userAuthenticationContext, TokenLifetime.USE_DEFAULT);

            return principalToken.getRandomHash();
        }
        catch (com.atlassian.crowd.exception.OperationFailedException e)
        {
            logger.info(e.getMessage(), e);
            throw new RemoteException(e.getMessage(), e);
        }
        catch (com.atlassian.crowd.manager.application.ApplicationAccessDeniedException e)
        {
            throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InactiveAccountException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ExpiredCredentialException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthenticationException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationNotFoundException e)
        {
            throw new com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public com.atlassian.crowd.integration.authentication.AuthenticatedToken authenticateApplication(com.atlassian.crowd.integration.authentication.ApplicationAuthenticationContext authenticationContext) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthenticationException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // set the validation factors of the client
            com.atlassian.crowd.integration.authentication.ValidationFactor[] validationFactors = soapService.getApplicationClientValidationFactors(authenticationContext.getName());
            authenticationContext.setValidationFactors(validationFactors);

            // authenticateApplication
            Token token = tokenAuthenticationManager.authenticateApplication(SoapObjectTranslator.fromSoapApplicationAuthenticationContext(authenticationContext));

            com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken = new com.atlassian.crowd.integration.authentication.AuthenticatedToken(token.getName(), token.getRandomHash());

            // validate the service
            soapService.validateSOAPService(applicationToken);

            // give the client their key
            return applicationToken;
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthenticationException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public String authenticatePrincipal(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, com.atlassian.crowd.integration.authentication.UserAuthenticationContext authenticateContext) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthenticationException, com.atlassian.crowd.integration.exception.InactiveAccountException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException, com.atlassian.crowd.integration.exception.ExpiredCredentialException
    {
        try
        {
            // validate the service
            /*final Application application = */soapService.validateSOAPService(applicationToken);

            Token principalToken = tokenAuthenticationManager
                .authenticateUser(SoapObjectTranslator.fromSoapUserAuthenticationContext(authenticateContext),
                                  TokenLifetime.USE_DEFAULT);

            return principalToken.getRandomHash();
        }
        catch (com.atlassian.crowd.exception.OperationFailedException e)
        {
            logger.info(e.getMessage(), e);
            throw new RemoteException(e.getMessage(), e);
        }
        catch (com.atlassian.crowd.manager.application.ApplicationAccessDeniedException e)
        {
            throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InactiveAccountException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ExpiredCredentialException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthenticationException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationNotFoundException e)
        {
            throw new com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public boolean isValidPrincipalToken(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principalToken, com.atlassian.crowd.integration.authentication.ValidationFactor[] validationFactors) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException
    {
        try
        {
            // validate the service
            Application application = soapService.validateSOAPService(applicationToken);

            // check the token
            tokenAuthenticationManager.validateUserToken(principalToken, SoapObjectTranslator.fromSoapValidationFactors(validationFactors), application.getName());

            return true;
        }
        catch (com.atlassian.crowd.exception.OperationFailedException e)
        {
            logger.info(e.getMessage(), e);
            throw new RemoteException(e.getMessage(), e);
        }
        catch (com.atlassian.crowd.exception.InvalidTokenException e)
        {
            return false;
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.manager.application.ApplicationAccessDeniedException e)
        {
            throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public void invalidatePrincipalToken(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String token) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            soapService.validateSOAPService(applicationToken);

            tokenAuthenticationManager.invalidateToken(token);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
    }

    @Override
    public SOAPGroup[] searchGroups(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SearchRestriction[] searchRestrictions) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            SearchContext.PopulateMemberships population = SearchContext.PopulateMemberships.ALL;
            String groupNameRestriction = null;
            String usernameRestriction = null;
            Boolean activeRestriction = null;
            int maxResults = EntityQuery.ALL_RESULTS;
            int startIndex = 0;
            if (searchRestrictions != null)
            {
                for (SearchRestriction restriction : searchRestrictions)
                {
                    // if the restriction attributes are not null, add...
                    if (StringUtils.isNotBlank(restriction.getName()) && StringUtils.isNotBlank(restriction.getValue()))
                    {

                        // HACK: converting from string to enum because of lameo enum comparisons in ASG
                        if (restriction.getName().equals(SearchContext.GROUP_POPULATE_DIRECT_SUBGROUPS) || restriction.getName().equals(SearchContext.GROUP_POPULATE_MEMBERSHIPS) || restriction.getName().equals(SearchContext.ROLE_POPULATE_MEMBERSHIPS))
                        {
                            population = SearchContext.PopulateMemberships.parseString(restriction.getValue());
                        }
                        else if (restriction.getName().equals(SearchContext.GROUP_NAME))
                        {
                            groupNameRestriction = toLowerCase(restriction.getValue());
                        }
                        else if (restriction.getName().equals(SearchContext.GROUP_ACTIVE))
                        {
                            activeRestriction = Boolean.valueOf(toLowerCase(restriction.getValue()));
                        }
                        else if (restriction.getName().equals(SearchContext.GROUP_PRINCIPAL_MEMBER))
                        {
                            usernameRestriction = restriction.getValue();
                        }
                    }
                }
            }

            // Groups to return from search.
            List<SOAPGroup> soapGroups = new ArrayList<SOAPGroup>();

            // Are we searching for groups that a user is a memberOf or are we doing a search for groups?
            List<Group> groups;
            if (usernameRestriction != null)
            {
                groups = applicationService.searchNestedGroupRelationships(application, QueryBuilder.queryFor(Group.class, EntityDescriptor.group()).parentsOf(EntityDescriptor.user()).withName(usernameRestriction).startingAt(startIndex).returningAtMost(maxResults));

                for (Group group : groups)
                {
                    final SOAPGroup soapGroup = ObjectTranslator.processGroup(group, Collections.<String>emptyList());
                    soapGroups.add(soapGroup);
                }
            }
            else
            {
                // make query
                List<PropertyRestriction> restrictions = new ArrayList<PropertyRestriction>();
                if (groupNameRestriction != null)
                {
                    restrictions.add(Restriction.on(GroupTermKeys.NAME).containing(groupNameRestriction));
                }
                if (activeRestriction != null)
                {
                    restrictions.add(Restriction.on(GroupTermKeys.ACTIVE).exactlyMatching(activeRestriction));
                }

                GroupQuery<Group> query;
                if (restrictions.isEmpty())
                {
                    query = new GroupQuery<Group>(Group.class, GroupType.GROUP, NullRestrictionImpl.INSTANCE, startIndex, maxResults);
                }
                else
                {
                    final BooleanRestriction multiRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, restrictions.toArray(new PropertyRestriction[restrictions.size()]));
                    query = new GroupQuery<Group>(Group.class, GroupType.GROUP, multiRestriction, startIndex, maxResults);
                }

                groups = applicationService.searchGroups(application, query);

                for (Group group : groups)
                {
                    List<String> members;

                    switch (population)
                    {
                        case NONE:
                            members = Collections.emptyList();
                            break;

                        case DIRECT:
                            members = applicationService.searchDirectGroupRelationships(application, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(group.getName()).returningAtMost(EntityQuery.ALL_RESULTS));
                            break;

                        default:
                            members = applicationService.searchNestedGroupRelationships(application, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(group.getName()).returningAtMost(EntityQuery.ALL_RESULTS));
                            break;
                    }

                    final SOAPGroup soapGroup = ObjectTranslator.processGroup(group, members);
                    soapGroups.add(soapGroup);

                }
            }

            return ObjectTranslator.processSOAPGroupAndMemberNames(soapGroups.toArray(new SOAPGroup[soapGroups.size()]));
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public SOAPPrincipal[] searchPrincipals(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SearchRestriction[] searchRestrictions) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            final UserQuery<User> userQuery = buildUserQuery(User.class, searchRestrictions);

            final List<User> finalMatchingUsers = applicationService.searchUsers(application, userQuery);
            return ObjectTranslator.processUsers(finalMatchingUsers);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @VisibleForTesting
    static <T> UserQuery<T> buildUserQuery(final Class<T> returnType, final SearchRestriction... searchRestrictions)
    {
        String nameRestriction = null;
        String emailRestriction = null;
        String fullNameRestriction = null;
        Boolean activeRestriction = null;
        int maxResults = EntityQuery.ALL_RESULTS;
        int startIndex = 0;

        if (searchRestrictions != null)
        {
            for (SearchRestriction restriction : searchRestrictions)
            {
                // if the restriction attributes are not null, add...
                if (StringUtils.isNotBlank(restriction.getName()) && StringUtils.isNotBlank(restriction.getValue()))
                {
                    if (restriction.getName().equals(SearchContext.PRINCIPAL_NAME) || restriction.getName().equals(UserConstants.USERNAME))
                    {
                        nameRestriction = toLowerCase(restriction.getValue());
                    }
                    else if (restriction.getName().equals(SearchContext.PRINCIPAL_ACTIVE))
                    {
                        activeRestriction = Boolean.valueOf(toLowerCase(restriction.getValue()));
                    }
                    else if (restriction.getName().equals(SearchContext.PRINCIPAL_EMAIL) || restriction.getName().equals(UserConstants.EMAIL))
                    {
                        emailRestriction = restriction.getValue().toLowerCase(Locale.ENGLISH);
                    }
                    else if (restriction.getName().equals(SearchContext.PRINCIPAL_FULLNAME) || restriction.getName().equals(UserConstants.DISPLAYNAME))
                    {
                        fullNameRestriction = toLowerCase(restriction.getValue());
                    }
                }
            }
        }

        // make query
        List<PropertyRestriction> restrictions = new ArrayList<PropertyRestriction>();
        if (nameRestriction != null)
        {
            restrictions.add(Restriction.on(UserTermKeys.USERNAME).containing(nameRestriction));
        }
        if (activeRestriction != null)
        {
            restrictions.add(Restriction.on(UserTermKeys.ACTIVE).exactlyMatching(activeRestriction));
        }
        if (emailRestriction != null)
        {
            restrictions.add(Restriction.on(UserTermKeys.EMAIL).containing(emailRestriction));
        }
        if (fullNameRestriction != null)
        {
            restrictions.add(Restriction.on(UserTermKeys.DISPLAY_NAME).containing(fullNameRestriction));
        }

        UserQuery<T> query;
        if (restrictions.isEmpty())
        {
            query = new UserQuery<T>(returnType, NullRestrictionImpl.INSTANCE, startIndex, maxResults);
        }
        else
        {
            final BooleanRestriction multiRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, restrictions.toArray(new PropertyRestriction[restrictions.size()]));
            query = new UserQuery<T>(returnType, multiRestriction, startIndex, maxResults);
        }
        return query;
    }

    @Override
    public SOAPRole[] searchRoles(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SearchRestriction[] searchRestrictions) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            Application application = soapService.validateSOAPService(applicationToken);

            SearchContext.PopulateMemberships population = SearchContext.PopulateMemberships.ALL;
            String groupNameRestriction = null;
            Boolean activeRestriction = null;
            int maxResults = EntityQuery.ALL_RESULTS;
            int startIndex = 0;
            if (searchRestrictions != null)
            {
                for (SearchRestriction restriction : searchRestrictions)
                {
                    // if the restriction attributes are not null, add...
                    if (StringUtils.isNotBlank(restriction.getName()) && StringUtils.isNotBlank(restriction.getValue()))
                    {

                        // HACK: converting from string to enum because of lameo enum comparisons in ASG
                        if (restriction.getName().equals(SearchContext.GROUP_POPULATE_DIRECT_SUBGROUPS) || restriction.getName().equals(SearchContext.GROUP_POPULATE_MEMBERSHIPS) || restriction.getName().equals(SearchContext.ROLE_POPULATE_MEMBERSHIPS))
                        {
                            population = SearchContext.PopulateMemberships.parseString(restriction.getValue());
                        }
                        else if (restriction.getName().equals(SearchContext.ROLE_NAME))
                        {
                            groupNameRestriction = toLowerCase(restriction.getValue());
                        }
                        else if (restriction.getName().equals(SearchContext.ROLE_ACTIVE))
                        {
                            activeRestriction = Boolean.valueOf(toLowerCase(restriction.getValue()));
                        }
                    }
                }

            }

            // make query
            List<PropertyRestriction> restrictions = new ArrayList<PropertyRestriction>();
            if (groupNameRestriction != null)
            {
                restrictions.add(Restriction.on(GroupTermKeys.NAME).containing(groupNameRestriction));
            }
            if (activeRestriction != null)
            {
                restrictions.add(Restriction.on(GroupTermKeys.ACTIVE).exactlyMatching(activeRestriction));
            }

            GroupQuery<Group> query;
            if (restrictions.isEmpty())
            {
                query = new GroupQuery<Group>(Group.class, GroupType.LEGACY_ROLE, NullRestrictionImpl.INSTANCE, startIndex, maxResults);
            }
            else
            {
                final BooleanRestriction multiRestriction = new BooleanRestrictionImpl(BooleanRestriction.BooleanLogic.AND, restrictions.toArray(new PropertyRestriction[restrictions.size()]));
                query = new GroupQuery<Group>(Group.class, GroupType.LEGACY_ROLE, multiRestriction, startIndex, maxResults);
            }

            List<Group> groups = applicationService.searchGroups(application, query);

            List<SOAPRole> soapRoles = new ArrayList<SOAPRole>(groups.size());

            for (Group group : groups)
            {
                List<String> members;

                switch (population)
                {
                    case NONE:
                        members = Collections.emptyList();
                        break;

                    default:
                        members = applicationService.searchDirectGroupRelationships(application, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.LEGACY_ROLE)).withName(group.getName()).returningAtMost(EntityQuery.ALL_RESULTS));
                        break;
                }

                final SOAPRole soapRole = new SOAPRole(group.getName(), members.toArray(new String[members.size()]));
                soapRoles.add(soapRole);
            }

            return ObjectTranslator.processSOAPRoleAndMemberNames(soapRoles.toArray(new SOAPRole[soapRoles.size()]));
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    @Transactional // because it makes multiple calls to ApplicationService
    public SOAPGroup addGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SOAPGroup soapGroup) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.InvalidGroupException, com.atlassian.crowd.integration.exception.ApplicationPermissionException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            Group remoteGroup = applicationService.addGroup(application, ObjectTranslator.processGroup(soapGroup));

            @SuppressWarnings({"deprecation"}) final List<String> nestedMemberNames = applicationService.searchNestedGroupRelationships(application, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(remoteGroup.getName()).returningAtMost(EntityQuery.ALL_RESULTS));

            return ObjectTranslator.processGroup(remoteGroup, nestedMemberNames);
        }
        catch (com.atlassian.crowd.exception.OperationFailedException e)
        {
            logger.info(e.getMessage(), e);
            throw new RemoteException(e.getMessage(), e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidGroupException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public void updateGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken authenticatedToken, String groupName, String description, boolean active)
            throws RemoteException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            Application application = soapService.validateSOAPService(authenticatedToken);

            final GroupTemplate groupTemplate = new GroupTemplate(groupName);
            groupTemplate.setActive(active);
            groupTemplate.setDescription(description);

            try
            {
                applicationService.updateGroup(application, groupTemplate);
            }
            catch (com.atlassian.crowd.exception.InvalidGroupException e)
            {
                // can't change api
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }
            catch (com.atlassian.crowd.exception.OperationFailedException e)
            {
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException e)
        {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    @Override
    @Transactional // because it makes multiple calls to ApplicationService
    public SOAPGroup findGroupByName(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String name)
            throws RemoteException, com.atlassian.crowd.integration.exception.ObjectNotFoundException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            Application application = soapService.validateSOAPService(applicationToken);

            Group remoteGroup = applicationService.findGroupByName(application, name);

            if (remoteGroup.getType() != GroupType.GROUP)
            {
                throw new com.atlassian.crowd.exception.GroupNotFoundException(name);
            }

            final List<String> nestedMemberNames = applicationService.searchNestedGroupRelationships(application, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(remoteGroup.getName()).returningAtMost(EntityQuery.ALL_RESULTS));

            return ObjectTranslator.processGroup(remoteGroup, nestedMemberNames);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    @Transactional // because it makes multiple calls to ApplicationService
    public SOAPGroup findGroupWithAttributesByName(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String name) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            Application application = soapService.validateSOAPService(applicationToken);

            GroupWithAttributes remoteGroup = applicationService.findGroupWithAttributesByName(application, name);

            if (remoteGroup.getType() != GroupType.GROUP)
            {
                throw new com.atlassian.crowd.exception.GroupNotFoundException(name);
            }

            final List<String> nestedMemberNames = applicationService.searchNestedGroupRelationships(application, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(remoteGroup.getName()).returningAtMost(EntityQuery.ALL_RESULTS));

            return ObjectTranslator.processGroupWithAttributes(remoteGroup, nestedMemberNames);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public SOAPRole addRole(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SOAPRole soapRole) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.InvalidRoleException, com.atlassian.crowd.integration.exception.ApplicationPermissionException
    {
        throw new ApplicationPermissionException("Roles are no longer supported by Crowd");
    }

    @Override
    public SOAPRole findRoleByName(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String name) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        throw new com.atlassian.crowd.integration.exception.ObjectNotFoundException(GroupNotFoundException.class.getName() + " " + name, null);
    }

    @Override
    public SOAPPrincipal findPrincipalByToken(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String key) throws com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, RemoteException, com.atlassian.crowd.integration.exception.InvalidTokenException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            User user;
            try
            {
                user = tokenAuthenticationManager.findUserByToken(key, application.getName());
            }
            catch (com.atlassian.crowd.exception.OperationFailedException e)
            {
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }

            return ObjectTranslator.processUser(user);
        }
        catch (com.atlassian.crowd.exception.InvalidTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationNotFoundException e)
        {
            throw new com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException(e);
        }
        catch (ApplicationAccessDeniedException e)
        {
            throw new com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    @Transactional // because it makes multiple (indirect) calls to ApplicationService
    public void updatePrincipalAttribute(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String name, SOAPAttribute attribute)
            throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            storeUserAttribute(application, name, attribute);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.integration.exception.InvalidUserException e)
        {
            throw new RemoteException(e.getMessage(), e);
        }
    }

    @Override
    @Transactional // because it makes multiple (indirect) calls to ApplicationService
    public void updateGroupAttribute(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String group, SOAPAttribute attribute) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            String groupName = applicationService.findGroupByName(application, group).getName();
            storeCustomGroupAttribute(application, groupName, attribute);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
    }

    @Override
    public SOAPPrincipal findPrincipalByName(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String name) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            User remotePrincipal = applicationService.findUserByName(application, name);

            return ObjectTranslator.processUser(remotePrincipal);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.UserNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public SOAPPrincipal findPrincipalWithAttributesByName(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String name) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            UserWithAttributes remotePrincipal = applicationService.findUserWithAttributesByName(application, name);

            return ObjectTranslator.processUserWithAttributes(remotePrincipal);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.UserNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public void addAllPrincipals(final com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, final SOAPPrincipalWithCredential[] principals)
            throws com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, RemoteException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.BulkAddFailedException
    {
        try
        {
            // validate the service
            Application application = soapService.validateSOAPService(applicationToken);

            final Collection<UserTemplateWithCredentialAndAttributes> userTemplates = new ArrayList<UserTemplateWithCredentialAndAttributes>();
            for (SOAPPrincipalWithCredential principalWithCredential : principals)
            {
                final SOAPPrincipal principal = principalWithCredential.getPrincipal();
                UserTemplate user = ObjectTranslator.processUser(principal);
                Map<String, Set<String>> attributes = ObjectTranslator.buildUserAttributeMap(principal);
                com.atlassian.crowd.integration.authentication.PasswordCredential credential = principalWithCredential.getPasswordCredential();
                UserTemplateWithCredentialAndAttributes userTemplate = new UserTemplateWithCredentialAndAttributes(user, attributes, SoapObjectTranslator.fromSoapPasswordCredential(credential));
                userTemplates.add(userTemplate);
            }
            try
            {
                applicationService.addAllUsers(application, userTemplates);
            }
            catch (com.atlassian.crowd.exception.OperationFailedException e)
            {
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }
        }
        catch (com.atlassian.crowd.exception.BulkAddFailedException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
    }

    @Override
    @Transactional // because it makes multiple calls to ApplicationService
    public SOAPPrincipal addPrincipal(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, SOAPPrincipal principal, com.atlassian.crowd.integration.authentication.PasswordCredential credential)
            throws com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, RemoteException, com.atlassian.crowd.integration.exception.InvalidCredentialException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.InvalidUserException
    {
        try
        {
            // validate the service
            Application application = soapService.validateSOAPService(applicationToken);

            SOAPPrincipal soapPrincipal;

            try
            {
                User user = applicationService.addUser(application, ObjectTranslator.processUser(principal), SoapObjectTranslator.fromSoapPasswordCredential(credential));

                soapPrincipal = ObjectTranslator.processUser(user);

                Map<String, Set<String>> attributes = ObjectTranslator.buildUserAttributeMap(principal);
                if (!attributes.isEmpty())
                {
                    applicationService.storeUserAttributes(application, user.getName(), attributes);

                    // If we managed to store attributes, re-fetch the user with attributes
                    UserWithAttributes userWithAttributes = applicationService.findUserWithAttributesByName(application, user.getName());

                    soapPrincipal = ObjectTranslator.processUserWithAttributes(userWithAttributes);
                }
            }
            catch (com.atlassian.crowd.exception.InvalidUserException e)
            {
                logger.info(e.getMessage(), e);
                SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
                throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
            }
            catch (com.atlassian.crowd.exception.OperationFailedException e)
            {
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }
            catch (com.atlassian.crowd.exception.UserNotFoundException e)
            {
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }
            catch (com.atlassian.crowd.exception.InvalidCredentialException e)
            {
                SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
                throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
            }

            return soapPrincipal;
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public void addPrincipalToGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, String group)
            throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            try
            {
                applicationService.addUserToGroup(application, principal, group);
            }
            catch (com.atlassian.crowd.exception.OperationFailedException e)
            {
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.UserNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.MembershipAlreadyExistsException e)
        {
            // previously to CWD-3215, the DAOs were throwing a random runtime exception. With CWD-3215,
            // they throw MembershipAlreadyExistsException, which it is wrapped as a RemoteException to
            // preserve the old behaviour
            throw new RemoteException(e.getMessage(), e);
        }
    }

    @Override
    public void updatePrincipalCredential(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, com.atlassian.crowd.integration.authentication.PasswordCredential credential)
            throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.InvalidCredentialException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            try
            {
                applicationService.updateUserCredential(application, principal, SoapObjectTranslator.fromSoapPasswordCredential(credential));
            }
            catch (com.atlassian.crowd.exception.OperationFailedException e)
            {
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.UserNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidCredentialException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
    }

    @Override
    @Transactional // because it makes multiple calls to ApplicationService/ForgottenLoginManager
    public void resetPrincipalCredential(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal)
            throws RemoteException, com.atlassian.crowd.integration.exception.InvalidEmailAddressException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.InvalidCredentialException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            /* First reset the password so the current one no longer works */
            try
            {
                applicationService.resetUserCredential(application, principal);
            }
            catch (com.atlassian.crowd.exception.OperationFailedException e)
            {
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }

            /* Then send a reset link to the user */
            forgottenLoginManager.sendResetLink(application, principal);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidEmailAddressException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.UserNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidCredentialException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
    }

    @Override
    public void removeGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String group)
            throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            Application application = soapService.validateSOAPService(applicationToken);

            try
            {
                applicationService.removeGroup(application, group);
            }
            catch (com.atlassian.crowd.exception.OperationFailedException e)
            {
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
    }

    @Override
    public void removeRole(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String role)
            throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        removeGroup(applicationToken, role);
    }

    @Override
    public void removePrincipal(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal)
            throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            try
            {
                applicationService.removeUser(application, principal);
            }
            catch (com.atlassian.crowd.exception.OperationFailedException e)
            {
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.UserNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
    }

    @Override
    public void addPrincipalToRole(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, String role)
            throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        addPrincipalToGroup(applicationToken, principal, role);
    }

    @Override
    public boolean isGroupMember(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String group, String principal) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            return applicationService.isUserNestedGroupMember(application, principal, group);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public boolean isRoleMember(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String role, String principal) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        return isGroupMember(applicationToken, principal, role);
    }

    @Override
    public void removePrincipalFromGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, String group)
            throws com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, RemoteException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            try
            {
                applicationService.removeUserFromGroup(application, principal, group);
            }
            catch (com.atlassian.crowd.exception.OperationFailedException e)
            {
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.UserNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.MembershipNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
    }

    @Override
    public void removePrincipalFromRole(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, String role)
            throws com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, RemoteException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        removePrincipalFromGroup(applicationToken, principal, role);
    }

    private static boolean isPrimaryUserAttribute(String attributeName)
    {
        return UserConstants.FIRSTNAME.equals(attributeName) || UserConstants.LASTNAME.equals(attributeName) || UserConstants.DISPLAYNAME.equals(attributeName) || UserConstants.EMAIL.equals(attributeName) || UserConstants.ACTIVE.equals(attributeName);
    }

    private void updatePrimaryUserAttribute(Application application, String username, SOAPAttribute attribute)
            throws RemoteException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.InvalidUserException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            final User user = applicationService.findUserByName(application, username);

            UserTemplate userTemplate = new UserTemplate(user);

            if (attribute.getValues() == null || attribute.getValues().length > 0)
            {
                String value = attribute.getValues()[0];

                if (value != null)
                {
                    if (UserConstants.FIRSTNAME.equals(attribute.getName()))
                    {
                        userTemplate.setFirstName(value);
                    }
                    else if (UserConstants.LASTNAME.equals(attribute.getName()))
                    {
                        userTemplate.setLastName(value);
                    }
                    else if (UserConstants.DISPLAYNAME.equals(attribute.getName()))
                    {
                        userTemplate.setDisplayName(value);
                    }
                    else if (UserConstants.EMAIL.equals(attribute.getName()))
                    {
                        userTemplate.setEmailAddress(value);
                    }
                    else if (UserConstants.ACTIVE.equals(attribute.getName()))
                    {
                        userTemplate.setActive(Boolean.valueOf(value));
                    }

                    applicationService.updateUser(application, userTemplate);
                }
            }
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.UserNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.InvalidUserException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.OperationFailedException e)
        {
            logger.info(e.getMessage(), e);
            throw new RemoteException(e.getMessage(), e);
        }
    }

    private void storeCustomUserAttribute(Application application, String username, SOAPAttribute attribute)
            throws RemoteException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            Map<String, Set<String>> attribs = new HashMap<String, Set<String>>();
            attribs.put(attribute.getName(), Sets.newHashSet(attribute.getValues()));

            applicationService.storeUserAttributes(application, username, attribs);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.UserNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.OperationFailedException e)
        {
            logger.info(e.getMessage(), e);
            throw new RemoteException(e.getMessage(), e);
        }
    }

    private void storeCustomGroupAttribute(Application application, String group, SOAPAttribute attribute)
            throws RemoteException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            Map<String, Set<String>> attribs = new HashMap<String, Set<String>>();
            attribs.put(attribute.getName(), Sets.newHashSet(attribute.getValues()));
            applicationService.storeGroupAttributes(application, group, attribs);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.OperationFailedException e)
        {
            logger.info(e.getMessage(), e);
            throw new RemoteException(e.getMessage(), e);
        }
    }

    private void storeUserAttribute(Application application, String username, SOAPAttribute attribute)
            throws RemoteException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.InvalidUserException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        if (isPrimaryUserAttribute(attribute.getName()))
        {
            updatePrimaryUserAttribute(application, username, attribute);
        }
        else
        {
            storeCustomUserAttribute(application, username, attribute);
        }
    }

    private void removeUserAttribute(Application application, String username, String attributeName)
            throws RemoteException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        if (isPrimaryUserAttribute(attributeName))
        {
            throw new RemoteException("Cannot remove the attribute <" + attributeName + "> as it is a primary attribute");
        }
        else
        {
            try
            {
                applicationService.removeUserAttributes(application, username, attributeName);
            }
            catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
            {
                SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
            }
            catch (com.atlassian.crowd.exception.UserNotFoundException e)
            {
                SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
            }
            catch (com.atlassian.crowd.exception.OperationFailedException e)
            {
                logger.info(e.getMessage(), e);
                throw new RemoteException(e.getMessage(), e);
            }
        }
    }

    private void removeGroupAttribute(Application application, String groupName, String attributeName)
            throws RemoteException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            applicationService.removeGroupAttributes(application, groupName, attributeName);
        }
        catch (com.atlassian.crowd.exception.ApplicationPermissionException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.OperationFailedException e)
        {
            logger.info(e.getMessage(), e);
            throw new RemoteException(e.getMessage(), e);
        }
    }

    @Override
    @Transactional // because it makes multiple (indirect) calls to ApplicationService
    public void addAttributeToPrincipal(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, SOAPAttribute attribute)
            throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        // Same as calling updatePrincipalAttribute
        updatePrincipalAttribute(applicationToken, principal, attribute);
    }

    @Override
    @Transactional // because it makes multiple (indirect) calls to ApplicationService
    public void addAttributeToGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String group, SOAPAttribute attribute)
            throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        // Same as calling updateGroupAttribute
        updateGroupAttribute(applicationToken, group, attribute);
    }

    @Override
    public void removeAttributeFromPrincipal(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principal, String attribute)
            throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            removeUserAttribute(application, principal, attribute);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
    }

    @Override
    @Transactional // because it makes multiple (indirect) calls to ApplicationService
    public void removeAttributeFromGroup(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String group, String attribute)
            throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException, com.atlassian.crowd.integration.exception.ApplicationPermissionException, com.atlassian.crowd.integration.exception.ObjectNotFoundException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            final String groupName = applicationService.findGroupByName(application, group).getName();
            removeGroupAttribute(application, groupName, attribute);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        catch (com.atlassian.crowd.exception.GroupNotFoundException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
    }

    /**
     * @deprecated since 1.0.2 All caching configuration has moved to the crowd-ehcache.xml
     */
    @Override
    public long getCacheTime(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            soapService.validateSOAPService(applicationToken);

            return propertyManager.getCacheTime();
        }
        catch (PropertyManagerException e)
        {
            logger.info(e.getMessage(), e);
            throw new RemoteException(e.getMessage(), e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public boolean isCacheEnabled(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            soapService.validateSOAPService(applicationToken);

            return propertyManager.isCacheEnabled();
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public String getDomain(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            soapService.validateSOAPService(applicationToken);

            return propertyManager.getDomain();
        }
        catch (PropertyManagerException e)
        {
            throw new RemoteException(e.getMessage(), e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public String[] findAllPrincipalNames(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            Collection<String> usernames = applicationService.searchUsers(application, QueryBuilder.queryFor(String.class, EntityDescriptor.user()).returningAtMost(EntityQuery.ALL_RESULTS));

            return usernames.toArray(new String[usernames.size()]);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public String[] findAllGroupNames(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            Collection<String> groupNames = applicationService.searchGroups(application, QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP)).returningAtMost(EntityQuery.ALL_RESULTS));

            return groupNames.toArray(new String[groupNames.size()]);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    @Transactional // because it makes multiple calls to ApplicationService
    public SOAPNestableGroup[] findAllGroupRelationships(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            Application application = soapService.validateSOAPService(applicationToken);

            final List<Group> groups = applicationService.searchGroups(application, QueryBuilder.queryFor(Group.class, EntityDescriptor.group(GroupType.GROUP)).returningAtMost(EntityQuery.ALL_RESULTS));

            List<SOAPNestableGroup> soapNestableGroups = new ArrayList<SOAPNestableGroup>();

            for (Group group : groups)
            {
                final List<String> childGroups = applicationService.searchDirectGroupRelationships(application, QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP)).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(group.getName()).returningAtMost(EntityQuery.ALL_RESULTS));
                final SOAPNestableGroup nestableGroup = ObjectTranslator.processNestableGroup(group, childGroups);
                soapNestableGroups.add(nestableGroup);
            }

            return ObjectTranslator.processSOAPNestableGroupAndMemberNames(soapNestableGroups);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public String[] findAllRoleNames(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        return findAllGroupNames(applicationToken);
    }

    @Override
    public String[] findGroupMemberships(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principalName) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            final Application application = soapService.validateSOAPService(applicationToken);

            // finds DIRECT group memberships
            List<String> memberships = applicationService.searchDirectGroupRelationships(application, QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.GROUP)).parentsOf(EntityDescriptor.user()).withName(principalName).returningAtMost(EntityQuery.ALL_RESULTS));

            // convert list of group names to array of group names
            return memberships.toArray(new String[memberships.size()]);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public String[] findRoleMemberships(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken, String principalName) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            final Application application = soapService.validateSOAPService(applicationToken);

            List<String> memberships;

            // finds DIRECT role memberships
            memberships = applicationService.searchDirectGroupRelationships(application, QueryBuilder.queryFor(String.class, EntityDescriptor.group(GroupType.LEGACY_ROLE)).parentsOf(EntityDescriptor.user()).withName(principalName).returningAtMost(EntityQuery.ALL_RESULTS));

            // convert list of group names to array of group names
            return memberships.toArray(new String[memberships.size()]);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    @Override
    public String[] getGrantedAuthorities(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            Application application = soapService.validateSOAPService(applicationToken);

            Set<String> groupNames = new HashSet<String>();

            for (DirectoryMapping directoryMapping : application.getDirectoryMappings())
            {
                for (GroupMapping groupMapping : directoryMapping.getAuthorisedGroups())
                {
                    groupNames.add(groupMapping.getGroupName());
                }
            }

            // convert list of group names to array of group names
            return groupNames.toArray(new String[groupNames.size()]);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }
        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    /**
     * Returns information needed to set the SSO cookie correctly.
     *
     * @param applicationToken application token
     * @return An object with lots of tasty configuration information
     * @throws RemoteException An unknown remote exception occurred.
     * @throws com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
     *                         The calling application's {@link AuthenticatedToken applicationToken} is invalid.
     */
    @Override
    public SOAPCookieInfo getCookieInfo(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken) throws RemoteException, com.atlassian.crowd.integration.exception.InvalidAuthorizationTokenException
    {
        try
        {
            // validate the service
            soapService.validateSOAPService(applicationToken);
            SOAPCookieInfo cookieInfo = new SOAPCookieInfo();
            cookieInfo.setDomain(propertyManager.getDomain());
            cookieInfo.setSecure(propertyManager.isSecureCookie());
            return cookieInfo;
        }
        catch (PropertyManagerException e)
        {
            throw new RemoteException(e.getMessage(), e);
        }
        catch (com.atlassian.crowd.exception.InvalidAuthorizationTokenException e)
        {
            SoapExceptionTranslator.throwSoapEquivalentCheckedException(e);
        }

        throw new AssertionError(SHOULD_NEVER_REACH_HERE_MSG);
    }

    /**
     * Throws the equivalent SOAP version of ApplicationAccessDeniedException.
     *
     * <p>This method is not in {@link com.atlassian.crowd.util.SoapExceptionTranslator} because the
     * <tt>crowd-integration-client-soap</tt> module does not see <tt>crowd-api</tt> module.
     *
     * @param e ApplicationAccessException from <tt>crowd-api</tt>
     * @throws com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException when successful
     */
    private static void throwSoapEquivalentCheckedException(final com.atlassian.crowd.manager.application.ApplicationAccessDeniedException e)
            throws com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException
    {
        throw new com.atlassian.crowd.integration.exception.ApplicationAccessDeniedException(e.getMessage(), e.getCause());
    }
}
