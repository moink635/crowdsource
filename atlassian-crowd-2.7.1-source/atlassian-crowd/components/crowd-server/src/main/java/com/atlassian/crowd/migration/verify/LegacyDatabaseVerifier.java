package com.atlassian.crowd.migration.verify;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowCallbackHandler;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;


/**
 * Pretty much a copy/paste of LegacyXmlVerifier - just that info is taken from database instead of XML document
 * <p/>
 * Responsible for verifying the correctness of the legacy database
 * <p/>
 * Currently this verifies that roles and groups within a directory have
 * different names (case-insensitive comparison).
 * <p/>
 */

public class LegacyDatabaseVerifier extends DatabaseVerifier
{
    private static final Logger logger = LoggerFactory.getLogger(LegacyDatabaseVerifier.class);

    // wow, this will kill memory for large imports (but so will all our existing mappers)
    private final Map<Long, Set<String>> directoryToRoleNames = new HashMap<Long, Set<String>>();
    private final Map<Long, Set<String>> directoryToGroupNames = new HashMap<Long, Set<String>>();

    private final JdbcOperations jdbcTemplate;

    public LegacyDatabaseVerifier(JdbcOperations jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Verifies a legacy database to ensure:
     * 1. no group name matches any other group name in the same directory
     * 2. no role name matches any other role name in the same directory
     * 3. no group name matches any role name in the same directory
     * <p/>
     * Errors can be obtained by called <code>getErrors</code>.
     */
    public void verify()
    {
        logger.info("Checking legacy database for data validity.");
        try
        {
            // process groups
            GroupTableVerifier groupTableVerifier = new GroupTableVerifier();
            jdbcTemplate.query(legacyTableQueries.getVerifyGroupsSQL(), groupTableVerifier);

            // process roles
            RoleTableVerifier roleTableVerifier = new RoleTableVerifier();
            jdbcTemplate.query(legacyTableQueries.getVerifyRolesSQL(), roleTableVerifier);
        }
        catch (DataAccessException e)
        {
            errors.add("An error occurred when querying the database <"+e.getMostSpecificCause().getMessage()+">");
        }
    }

    public void clearState()
    {
        getErrors().clear();
        directoryToRoleNames.clear();
        directoryToGroupNames.clear();
    }

    private class GroupTableVerifier implements RowCallbackHandler
    {
        public void processRow(ResultSet rs) throws SQLException
        {
            Long directoryId = rs.getLong("ID"); // In the legacy database, 'ID' is for the directoryID
            String groupName = rs.getString("NAME");

            String standardGroupName = toLowerCase(groupName);

            Set<String> roleNames = directoryToRoleNames.get(directoryId);
            Set<String> groupNames = directoryToGroupNames.get(directoryId);

            if (groupNames == null)
            {
                groupNames = new HashSet<String>();
                directoryToGroupNames.put(directoryId, groupNames);
            }

            if (groupNames.contains(standardGroupName))
            {
                errors.add("Could not add group with name '" + groupName + "' as it matches another group name in the same directory with id: " + directoryId);
            }
            else if (roleNames != null && roleNames.contains(standardGroupName))
            {
                errors.add("Could not add group with name '" + groupName + "' as it matches another role name in the same directory with id: " + directoryId);
            }
            else
            {
                groupNames.add(standardGroupName);
            }
        }
    }

    private class RoleTableVerifier implements RowCallbackHandler
    {
        public void processRow(ResultSet rs) throws SQLException
        {
            Long directoryId = rs.getLong("ID"); // In the legacy database, 'ID' is for the directoryID
            String roleName = rs.getString("NAME");

            String standardRoleName = toLowerCase(roleName);

            Set<String> roleNames = directoryToRoleNames.get(directoryId);
            Set<String> groupNames = directoryToGroupNames.get(directoryId);

            if (roleNames == null)
            {
                roleNames = new HashSet<String>();
                directoryToRoleNames.put(directoryId, roleNames);
            }

            if (roleNames.contains(standardRoleName))
            {
                errors.add("Could not add role with name '" + roleName + "' as it matches another role name in the same directory with id: " + directoryId);
            }
            else if (groupNames != null && groupNames.contains(standardRoleName))
            {
                errors.add("Could not add role with name '" + roleName + "' as it matches another group name in the same directory with id: " + directoryId);
            }
            else
            {
                roleNames.add(standardRoleName);
            }
        }
    }

}
