package com.atlassian.crowd.importer.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * This class represents and tracks just what happened via an import.
 * It can be used to answer question like:< br/>
 * <ol>
 * <li>How many users were imported?</li>
 * <li>How many groups were imported?</li>
 * <li>How many memberships were imported?</li>
 * <li>Which users are already in Crowd?</li>
 * <li>Which users failed to import?</li>
 * <li>Which groups failed to import?</li>
 * <li>Which group already existsin Crowd?</li>
 * <li>Which memberships failed to import?</li>
 * <ol>
 */
public class Result implements Serializable
{
    private Long usersImported = new Long(0);
    private Long groupsImported = new Long(0);
    private Long groupMembershipsImported = new Long(0);
    private Set<String> usersFailedImport = new HashSet<String>();
    private Set<String> usersAlreadyExist = new HashSet<String>();
    private Set<String> groupsFailedImport = new HashSet<String>();
    private Set<String> groupsAlreadyExist = new HashSet<String>();
    private Set<String> groupMembershipsFailedImport = new HashSet<String>();
    private String message;

    public Result()
    {
    }

    public Long getUsersImported()
    {
        return usersImported;
    }

    public void setUsersImported(Long usersImported)
    {
        this.usersImported = usersImported;
    }

    public Long getGroupsImported()
    {
        return groupsImported;
    }

    public void setGroupsImported(Long groupsImported)
    {
        this.groupsImported = groupsImported;
    }

    public Long getGroupMembershipsImported()
    {
        return groupMembershipsImported;
    }

    public void setGroupMembershipsImported(Long groupMembershipsImported)
    {
        this.groupMembershipsImported = groupMembershipsImported;
    }

    public Set<String> getUsersFailedImport()
    {
        return usersFailedImport;
    }

    public void setUsersFailedImport(Set<String> usersFailedImport)
    {
        this.usersFailedImport = usersFailedImport;
    }

    public void addUsersFailedImport(String userName)
    {
        usersFailedImport.add(userName);
    }

    public void addExistingUser(String userName)
    {
        usersAlreadyExist.add(userName);
    }

    public void addExistingGroup(String groupName)
    {
        groupsAlreadyExist.add(groupName);
    }

    public Set<String> getGroupsFailedImport()
    {
        return groupsFailedImport;
    }

    public void setGroupsFailedImport(Set<String> groupsFailedImport)
    {
        this.groupsFailedImport = groupsFailedImport;
    }

    public void addGroupFailedImport(String groupName)
    {
        groupsFailedImport.add(groupName);
    }

    public Set<String> getGroupMembershipsFailedImport()
    {
        return groupMembershipsFailedImport;
    }

    public void setGroupMembershipsFailedImport(Set<String> groupMembershipsFailedImport)
    {
        this.groupMembershipsFailedImport = groupMembershipsFailedImport;
    }

    public void addGroupMembershipsImported(long memberships)
    {
        this.groupMembershipsImported += memberships;
    }

    public void addFailedGroupMembershipImport(MembershipDTO membership)
    {
        groupMembershipsFailedImport.add(membership.getRelationship());
    }

    public void addFailedUserMembershipImports(String groupName, Collection<String> usernames)
    {
        for (String username : usernames)
        {
            groupMembershipsFailedImport.add(MembershipDTO.getRelationshipKey(MembershipDTO.ChildType.USER, username, groupName));
        }
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Set<String> getUsersAlreadyExist()
    {
        return usersAlreadyExist;
    }

    public void setUsersAlreadyExist(Set<String> usersAlreadyExist)
    {
        this.usersAlreadyExist = usersAlreadyExist;
    }

    public Set<String> getGroupsAlreadyExist()
    {
        return groupsAlreadyExist;
    }

    public void setGroupsAlreadyExist(Set<String> groupsAlreadyExist)
    {
        this.groupsAlreadyExist = groupsAlreadyExist;
    }

    public void incrementGroupsImported()
    {
        groupsImported = groupsImported + 1L;
    }

    public void incrementUsersImported()
    {
        usersImported = usersImported + 1L;
    }

    public void incrementGroupMembershipsImported()
    {
        groupMembershipsImported = groupMembershipsImported + 1L;
    }
}
