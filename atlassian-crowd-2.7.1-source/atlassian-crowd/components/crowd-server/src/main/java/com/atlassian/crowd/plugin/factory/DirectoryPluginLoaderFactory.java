package com.atlassian.crowd.plugin.factory;

import com.atlassian.crowd.plugin.PluginDirectoryLocator;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.loaders.DirectoryPluginLoader;
import org.springframework.beans.factory.FactoryBean;

import java.io.File;
import java.util.List;

public class DirectoryPluginLoaderFactory implements FactoryBean
{
    private final PluginDirectoryLocator pluginDirectoryLocator;
    private final List<PluginFactory> pluginFactories;
    private final PluginEventManager pluginEventManager;

    public DirectoryPluginLoaderFactory(PluginDirectoryLocator pluginDirectoryLocator, List<PluginFactory> pluginFactories, PluginEventManager pluginEventManager)
    {
        this.pluginDirectoryLocator = pluginDirectoryLocator;
        this.pluginFactories = pluginFactories;
        this.pluginEventManager = pluginEventManager;
    }

    public Object getObject() throws Exception
    {
        File pluginsDirectory = pluginDirectoryLocator.getPluginsDirectory();
        return new DirectoryPluginLoader(pluginsDirectory, pluginFactories, pluginEventManager);
    }

    public Class getObjectType()
    {
        return DirectoryPluginLoader.class;
    }

    public boolean isSingleton()
    {
        return true;
    }
}
