package com.atlassian.crowd.migration.legacy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.user.InternalUserDao;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchResultWithIdReferences;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.config.JohnsonConfig;
import com.atlassian.johnson.event.Event;

import org.dom4j.Element;
import org.hibernate.SessionFactory;
import org.springframework.web.context.ServletContextAware;

public class UserMapper extends GenericLegacyImporter implements LegacyImporter, ServletContextAware
{
    protected static final String REMOTE_PRINCIPAL_XML_ROOT = "principals";
    protected static final String REMOTE_PRINCIPAL_XML_NODE = "principal";
    private static final String REMOTE_PRINCIPAL_XML_DIRECTORY_ID = "directoryId";
    private static final String REMOTE_PRINCIPAL_XML_CREDENTIAL_HISTORY_NODE = "credentialHistories";
    private static final String REMOTE_PRINCIPAL_XML_CREDENTIAL = "credential";

    private final DirectoryDAOHibernate directoryDAO;
    private final InternalUserDao userDAO;
    private ServletContext servletContext;

    public UserMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, DirectoryDAOHibernate directoryDAO, InternalUserDao userDAO)
    {
        super(sessionFactory, batchProcessor);
        this.directoryDAO = directoryDAO;
        this.userDAO = userDAO;
    }

    public void importXml(final Element root, final LegacyImportDataHolder importData) throws ImportException
    {
        Element principalsElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + REMOTE_PRINCIPAL_XML_ROOT);
        if (principalsElement == null)
        {
            logger.error("No users were found for importing!");
            return;
        }

        // yes, yes..hello OOME for large imports (fix it when we have streaming imports)
        List<UserTemplateWithCredentialAndAttributes> users = new ArrayList<UserTemplateWithCredentialAndAttributes>();
        for (Iterator userIter = principalsElement.elementIterator(); userIter.hasNext();)
        {
            Element userElement = (Element) userIter.next();

            UserTemplateWithCredentialAndAttributes user;
            try
            {
                user = getUserAndAttributesFromXml(userElement, importData.getOldToNewDirectoryIds());
                users.add(user);
            }
            catch (IllegalArgumentException iae)
            {
                // Cannot add this user, report the problem
                JohnsonEventContainer agentJohnson = Johnson.getEventContainer(servletContext);
                JohnsonConfig johnsonConfig = Johnson.getConfig();
                try
                {
                    InternalEntityTemplate internalEntityTemplate = getInternalEntityTemplateFromLegacyXml(userElement);
                    Long directoryId = getUserDirectoryId(userElement);

                    String errorMessage = "Cannot import user <" + internalEntityTemplate.toFriendlyString() + "> into Directory <" + directoryId + ">, due to: " + iae.getMessage();

                    logger.error(errorMessage, iae);

                    agentJohnson.addEvent(new Event(johnsonConfig.getEventType("import"),
                                                    errorMessage, iae.getMessage(),
                                                    johnsonConfig.getEventLevel("warning")));
                }
                catch (Exception e)
                {

                    // I can't report it, damn :(
                    logger.error("An error occured trying to parse user data during import", e);
                }
            }
        }

        // now do the actual import as a batch
        BatchResultWithIdReferences<User> result = userDAO.addAll(users);
        for (User user : result.getFailedEntities())
        {
            logger.error("Unable to add user <" + user.getName() + "> in directory with id <" + user.getDirectoryId() + ">");
        }

        // die hard if there are any errors
        if (result.hasFailures())
        {
            throw new ImportException("Unable to import all users. See logs for more details.");
        }

        importData.setUserImportResults(result);
    }

    protected UserTemplateWithCredentialAndAttributes getUserAndAttributesFromXml(Element userElement, Map<Long, Long> oldToNewDirectoryIds)
    {
        // pull directoryId
        Long oldDirectoryId = getUserDirectoryId(userElement);
        Long directoryId = oldToNewDirectoryIds.get(oldDirectoryId);
        if (directoryId == null)
        {
            throw new IllegalArgumentException("User belongs to an unknown old directory with ID: " + directoryId);
        }
        Directory directory = (Directory) directoryDAO.loadReference(directoryId);

        // pull all the default values
        InternalEntityTemplate internalEntityTemplate = getInternalEntityTemplateFromLegacyXml(userElement);
        internalEntityTemplate.setName(internalEntityTemplate.getName());
        internalEntityTemplate.setId(null);

        Map<String, Set<String>> attributes = getMultiValuedAttributesMapFromXml(userElement);

        // pull field values from attributes
        String firstName = getAttributeValue(UserConstants.FIRSTNAME, attributes, "");
        String lastName = getAttributeValue(UserConstants.LASTNAME, attributes, "");
        String displayName = getAttributeValue(UserConstants.DISPLAYNAME, attributes, "");
        String email = getAttributeValue(UserConstants.EMAIL, attributes, "");

        // pull password
        PasswordCredential credential = getPasswordCredentialFromXml(userElement);

        // create template
        UserTemplateWithCredentialAndAttributes userTemplate = new UserTemplateWithCredentialAndAttributes(internalEntityTemplate.getName(), directoryId, credential);
        userTemplate.setCreatedDate(internalEntityTemplate.getCreatedDate());
        userTemplate.setUpdatedDate(internalEntityTemplate.getUpdatedDate());
        userTemplate.setActive(internalEntityTemplate.isActive());
        userTemplate.setFirstName(firstName);
        userTemplate.setLastName(lastName);
        userTemplate.setDisplayName(displayName);
        userTemplate.setEmailAddress(email);

        // pull password history
        List<PasswordCredential> records = getCredentialHistory(userElement);
        userTemplate.getCredentialHistory().addAll(records);

        // save all custom attributes
        attributes.remove(UserConstants.FIRSTNAME);
        attributes.remove(UserConstants.LASTNAME);
        attributes.remove(UserConstants.DISPLAYNAME);
        attributes.remove(UserConstants.EMAIL);
        userTemplate.getAttributes().putAll(attributes);

        return userTemplate;
    }

    private long getUserDirectoryId(Element userElement)
    {
        return Long.parseLong(userElement.element(REMOTE_PRINCIPAL_XML_DIRECTORY_ID).getText());
    }

    private List<PasswordCredential> getCredentialHistory(Element userElement)
    {
        List<PasswordCredential> history = new ArrayList<PasswordCredential>();

        Element credentialHistoryNode = userElement.element(REMOTE_PRINCIPAL_XML_CREDENTIAL_HISTORY_NODE);
        if (credentialHistoryNode != null && credentialHistoryNode.hasContent())
        {
            for (Iterator iterator = credentialHistoryNode.elementIterator(REMOTE_PRINCIPAL_XML_CREDENTIAL); iterator.hasNext();)
            {
                Element credentialElement = (Element) iterator.next();
                PasswordCredential credential = new PasswordCredential(credentialElement.getText(), true);
                history.add(credential);
            }
        }

        return history;
    }

    public void setServletContext(ServletContext servletContext)
    {
        this.servletContext = servletContext;
    }
}
