package com.atlassian.crowd.trusted;

import com.atlassian.security.auth.trustedapps.TrustedApplication;

/**
 * Persistence service for trusted apps
 *
 * @since v2.7
 */
public interface TrustedApplicationStore
{
    /**
     * Return all the currently stored Trusted Applications.
     *
     * @return a collection of TrustedApplications (potentially empty).
     */
    Iterable<TrustedApplication> getTrustedApplications();

    /**
     * Delete the {@link TrustedApplication} with the given ID.
     *
     * @param id    the ID of the application to delete.
     * @return true if the application was deleted.
     */
    boolean deleteApplication(String id);

    /**
     * Store the current {@link TrustedApplication}. If the application already exists, update it.
     *
     * @param app    the {@link TrustedApplication} to store.
     */
    void addTrustedApplication(TrustedApplication app);

    /**
     * Fetch the {@link TrustedApplication} with the given ID.
     *
     * @param id    the ID of the TrustedApplication to get
     * @return a {@link TrustedApplication} or <code>null</code> if no application exists with this ID.
     */
    TrustedApplication getTrustedApplication(String id);

    /**
     * Store the given current application.
     *
     * @param currentApplication    the current application to persist.
     */
    void storeCurrentApplication(InternalCurrentApplication currentApplication);

    /**
     * Gets the current application.
     *
     * @return a {@link InternalCurrentApplication}, or null if no current application currently exists
     */
    InternalCurrentApplication getCurrentApplication();
}
