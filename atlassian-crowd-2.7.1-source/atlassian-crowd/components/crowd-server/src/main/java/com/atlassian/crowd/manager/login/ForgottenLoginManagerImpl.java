package com.atlassian.crowd.manager.login;

import com.atlassian.crowd.dao.token.ResetPasswordTokenDao;
import com.atlassian.crowd.embedded.api.*;
import com.atlassian.crowd.event.login.RequestResetPasswordEvent;
import com.atlassian.crowd.event.login.RequestUsernamesEvent;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.directory.*;
import com.atlassian.crowd.manager.login.exception.InvalidResetPasswordTokenException;
import com.atlassian.crowd.manager.permission.PermissionManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.token.ResetPasswordToken;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.security.random.SecureTokenGenerator;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.transaction.annotation.Transactional;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import static com.atlassian.crowd.model.user.Users.namesOf;
import static com.google.common.base.Preconditions.checkNotNull;

@Transactional
public class ForgottenLoginManagerImpl implements ForgottenLoginManager
{
    private static final int TOKEN_EXPIRY_HOURS = 24;
    private static final String UTF8_ENCODING = "UTF-8";
    private static final String RESET_PASSWORD_ACTION = "/console/resetpassword.action";

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ApplicationService applicationService;
    private final DirectoryManager directoryManager;
    private final PermissionManager permissionManager;
    private final ResetPasswordTokenDao resetPasswordTokenDao;
    private final SecureTokenGenerator tokenGenerator;
    private final ClientProperties clientProperties;
    private final EventPublisher eventPublisher;

    public ForgottenLoginManagerImpl(final ApplicationService applicationService, final DirectoryManager directoryManager, final PermissionManager permissionManager, final ResetPasswordTokenDao resetPasswordTokenDao, final SecureTokenGenerator tokenGenerator, final ClientProperties clientProperties, final EventPublisher eventPublisher)
    {
        this.applicationService = checkNotNull(applicationService);
        this.directoryManager = checkNotNull(directoryManager);
        this.permissionManager = checkNotNull(permissionManager);
        this.resetPasswordTokenDao = checkNotNull(resetPasswordTokenDao);
        this.tokenGenerator = checkNotNull(tokenGenerator);
        this.clientProperties = checkNotNull(clientProperties);
        this.eventPublisher = checkNotNull(eventPublisher);
    }

    public void sendResetLink(final Application application, final String username)
            throws UserNotFoundException, InvalidEmailAddressException, ApplicationPermissionException
    {
        final User user = applicationService.findUserByName(application, username);
        Directory directory;
        try
        {
            directory = directoryManager.findDirectoryById(user.getDirectoryId());
        }
        catch (DirectoryNotFoundException e)
        {
            throw new ConcurrentModificationException("Directory " + user.getDirectoryId() + " no longer exists.");
        }

        if (permissionManager.hasPermission(application, directory, OperationType.UPDATE_USER))
        {
            sendResetLink(user, directory);
        }
        else
        {
            // Permission denied
            throw new ApplicationPermissionException("Not allowed to update user '" + user.getName() + "' in directory '" + directory.getName() + "'.");
        }
    }

    public boolean sendUsernames(final Application application, final String email) throws InvalidEmailAddressException
    {
        List<User> users = applicationService.searchUsers(application, QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(Restriction.on(UserTermKeys.EMAIL).exactlyMatching(email)).returningAtMost(EntityQuery.ALL_RESULTS));
        if (!users.isEmpty())
        {
            User user = users.get(0);
            List<String> usernames = ImmutableList.copyOf(namesOf(users));

            eventPublisher.publish(new RequestUsernamesEvent(user, usernames));
            return true;
        }
        else
        {
            logger.info("No usernames found for email address: " + email);
            return false;
        }
    }

    public void sendResetLink(final long directoryId, final String username)
            throws DirectoryNotFoundException, InvalidEmailAddressException, UserNotFoundException, OperationFailedException
    {
        final User user = directoryManager.findUserByName(directoryId, username);
        sendResetLink(user, directoryManager.findDirectoryById(directoryId));
    }

    public boolean isValidResetToken(final long directoryId, final String username, final String token)
    {
        if (StringUtils.isBlank(username) || StringUtils.isBlank(token))
        {
            return false;
        }

        try
        {
            ResetPasswordToken resetPasswordToken = resetPasswordTokenDao.findTokenByUsername(username);
            LocalDateTime now = new LocalDateTime();
            if (!resetPasswordToken.getToken().equals(token) || now.isAfter(new LocalDateTime(resetPasswordToken.getExpiryDate())) || resetPasswordToken.getDirectoryId() != directoryId)
            {
                return false;
            }
        }
        catch (ObjectNotFoundException e)
        {
            return false;
        }
        return true;
    }

    public void resetUserCredential(final long directoryId, final String username, final PasswordCredential credential, final String token)
            throws DirectoryNotFoundException, UserNotFoundException, InvalidResetPasswordTokenException, OperationFailedException, InvalidCredentialException, DirectoryPermissionException
    {
        if (isValidResetToken(directoryId, username, token))
        {
            directoryManager.updateUserCredential(directoryId, username, credential);
            resetPasswordTokenDao.removeTokenByUsername(username);
        }
        else
        {
            throw new InvalidResetPasswordTokenException("No valid reset token found for user");
        }
    }

    private void validateUser(final User user) throws InvalidEmailAddressException
    {
        if (StringUtils.isBlank(user.getEmailAddress()))
        {
            throw new InvalidEmailAddressException("Cannot email a reset password link; user's email address is blank.");
        }
        else
        {
            try
            {
                new InternetAddress(user.getEmailAddress()).validate();
            }
            catch (AddressException e)
            {
                throw new InvalidEmailAddressException(e);
            }
        }
    }

    private ResetPasswordToken createAndStoreResetToken(final String username, final long directoryId)
    {
        // generate a random token
        String token = tokenGenerator.generateToken();

        LocalDateTime expiryDate = new LocalDateTime().plusHours(TOKEN_EXPIRY_HOURS);
        ResetPasswordToken resetPasswordToken = new ResetPasswordToken(expiryDate.toDateTime().getMillis(), token, username, directoryId);
        resetPasswordTokenDao.addToken(resetPasswordToken);

        return resetPasswordToken;
    }

    private void sendResetLink(final User user, final Directory directory)
            throws InvalidEmailAddressException
    {
        validateUser(user);

        logger.info("\"" + user.getName() + "\" in \"" + directory.getName() + "\" is being e-mailed a password reset link.");

        ResetPasswordToken resetToken = createAndStoreResetToken(user.getName(), directory.getId());
        emailResetToken(resetToken, user);
    }

    private void emailResetToken(final ResetPasswordToken resetToken, final User user)
            throws InvalidEmailAddressException
    {
        String baseURL = clientProperties.getBaseURL();
        String encUsername = "";
        String encToken = "";
        String encDirectoryId = "";
        try
        {
            encUsername = URLEncoder.encode(resetToken.getUsername(), UTF8_ENCODING);
            encToken = URLEncoder.encode(resetToken.getToken(), UTF8_ENCODING);
            encDirectoryId = URLEncoder.encode(Long.toString(resetToken.getDirectoryId()), UTF8_ENCODING);
        }
        catch (UnsupportedEncodingException e)
        {
            throw new RuntimeException("Could not encode username and token: this Java VM does not support " + UTF8_ENCODING, e);
        }
        String resetLink = baseURL + RESET_PASSWORD_ACTION + "?username=" + encUsername + "&directoryId=" + encDirectoryId + "&token=" + encToken;
        eventPublisher.publish(new RequestResetPasswordEvent(user, resetLink));
    }
}
