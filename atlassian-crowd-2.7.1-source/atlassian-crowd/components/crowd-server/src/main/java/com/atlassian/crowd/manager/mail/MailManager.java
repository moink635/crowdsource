package com.atlassian.crowd.manager.mail;

import javax.mail.internet.InternetAddress;

public interface MailManager
{
    /**
     * Sends an email using the configured
     * mail server (remote host / JNDI).
     *
     * @param emailAddress address of recipient.
     * @param subject subject header.
     * Will be appended with the configured mail subject prefix in the final email.
     * @param body email body text.
     * @throws MailSendException an error occured sending the email.
     */
    void sendEmail(InternetAddress emailAddress, String subject, String body) throws MailSendException;

    /**
     * @return <code>true</code> if the mail server is fully configured (does not check if the configuration is correct)
     */
    boolean isConfigured();
}
