/*
 * Copyright (c) 2006 Atlassian Software Systems. All Rights Reserved.
 */
package com.atlassian.crowd.manager.token.reaper;

import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * The token reaper is a scheduled processes that removes expired {@link com.atlassian.crowd.model.token.Token tokens}
 * from the crowd server.
 */
public class TokenReaper extends QuartzJobBean
{
    private static final Logger logger = LoggerFactory.getLogger(TokenReaper.class);
    private TokenAuthenticationManager tokenAuthenticationManager;

    /**
     * Processes the removal of the expired {@link com.atlassian.crowd.model.token.Token tokens} from the crowd
     * server.
     *
     * @param jobExecutionContext Runtime information used when executing the process at runtime.
     * @throws JobExecutionException Unable to process the request.
     */
    public void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException
    {
        checkNotNull(tokenAuthenticationManager);

        try
        {
            logger.debug("Executing token reaper");
            tokenAuthenticationManager.removeExpiredTokens();
        }
        catch (Exception e)
        {
            throw new JobExecutionException(e.getMessage(), e, false);
        }
    }

    public void setTokenAuthenticationManager(TokenAuthenticationManager tokenAuthenticationManager)
    {
        this.tokenAuthenticationManager = tokenAuthenticationManager;
    }
}
