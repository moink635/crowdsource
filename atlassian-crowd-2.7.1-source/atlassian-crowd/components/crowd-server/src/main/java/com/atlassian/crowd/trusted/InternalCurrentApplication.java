package com.atlassian.crowd.trusted;

/**
 * Class to hold the properties to persist for the Current Application.
 *
 * @since v2.7
 */
public class InternalCurrentApplication
{
    private final String uid;
    private final String privateKey;
    private final String publicKey;

    public InternalCurrentApplication(final String uid, final String privateKey, final String publicKey)
    {
        this.uid = uid;
        this.privateKey = privateKey;
        this.publicKey = publicKey;
    }

    public String getUid()
    {
        return uid;
    }

    public String getPrivateKey()
    {
        return privateKey;
    }

    public String getPublicKey()
    {
        return publicKey;
    }
}
