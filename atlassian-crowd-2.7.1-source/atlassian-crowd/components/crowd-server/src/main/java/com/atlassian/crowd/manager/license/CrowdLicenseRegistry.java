package com.atlassian.crowd.manager.license;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.config.ConfigurationException;
import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.license.LicenseRegistry;

/**
 * Stores license/hash in crowd.cfg.xml.
 */

public class CrowdLicenseRegistry implements LicenseRegistry
{
    private static final Logger log = LoggerFactory.getLogger(CrowdLicenseRegistry.class);

    // dependencies
    private CrowdBootstrapManager bootstrapManager = null;

    // constants
    public static final String LICENSE_HASH = "license.hash";
    public static final String LICENSE_MESSAGE = "license.message";

    public void setLicenseHash(String string)
    {
        getBootstrapManager().setProperty(LICENSE_HASH, string);
        try
        {
            getBootstrapManager().save();
        }
        catch (ConfigurationException e)
        {
            log.error("Failed to set licenseHash", e);
        }
    }

    public void setLicenseMessage(String string)
    {
        getBootstrapManager().setProperty(LICENSE_MESSAGE, string);
        try
        {
            getBootstrapManager().save();
        }
        catch (ConfigurationException e)
        {
            log.error("Failed to set licenseMessage", e);
        }
    }

    public String getLicenseMessage()
    {
        return (String) getBootstrapManager().getProperty(LICENSE_MESSAGE);
    }

    public String getLicenseHash()
    {
        return (String) getBootstrapManager().getProperty(LICENSE_HASH);
    }

    ///CLOVER:OFF

    public CrowdBootstrapManager getBootstrapManager()
    {
        if (bootstrapManager == null)
            bootstrapManager = (CrowdBootstrapManager) BootstrapUtils.getBootstrapManager();
        return bootstrapManager;
    }

    public void setBootstrapManager(CrowdBootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
    }
}
