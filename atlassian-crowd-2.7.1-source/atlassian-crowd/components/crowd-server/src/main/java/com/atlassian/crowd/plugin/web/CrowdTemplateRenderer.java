package com.atlassian.crowd.plugin.web;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.views.freemarker.FreemarkerManager;
import com.opensymphony.xwork.ActionContext;
import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.LocaleProvider;
import freemarker.template.Configuration;
import freemarker.template.SimpleHash;
import freemarker.template.Template;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Locale;
import java.util.Map;

public class CrowdTemplateRenderer implements TemplateRenderer, ServletContextAware
{
    private FreemarkerManager freemarkerManager;

    private ServletContext servletContext;

    private static final Logger log = LoggerFactory.getLogger(CrowdTemplateRenderer.class);

    public String render(String templatePath, Map contextParams)
    {
        try
        {
            // Statics statics everywhere...
            ActionContext ctx = ServletActionContext.getContext();
            HttpServletRequest req = ServletActionContext.getRequest();
            HttpServletResponse res = ServletActionContext.getResponse();

            Configuration config = freemarkerManager.getConfiguration(servletContext);

            // get the configuration and template
            ActionInvocation invocation = ctx.getActionInvocation();
            Locale locale;
            if (invocation.getAction() instanceof LocaleProvider)
            {
                locale = ((LocaleProvider) invocation.getAction()).getLocale();
            }
            else
            {
                locale = config.getLocale();
            }
            freemarker.template.Template template = config.getTemplate(templatePath, locale); // WW-1181

            // get the main hash
            SimpleHash model = freemarkerManager.buildTemplateModel(ctx.getValueStack(), null, servletContext, req, res, config.getObjectWrapper());

            // populate the hash with the properties
            if (contextParams != null)
            {
                model.putAll(contextParams);
            }

            // finally, render it
            StringWriter writer = new StringWriter();
            template.process(model, writer);
            return writer.toString();
        }
        catch (Exception e)
        {
            log.error("Error revolving template", e);
            return ExceptionUtils.getStackTrace(e);
        }
    }

    public String renderText(String text, Map params)
    {
        try
        {
            // Statics statics everywhere...
            ActionContext ctx = ServletActionContext.getContext();
            HttpServletRequest req = ServletActionContext.getRequest();
            HttpServletResponse res = ServletActionContext.getResponse();

            Configuration config = freemarkerManager.getConfiguration(servletContext);
            config.setNumberFormat("0.######");

            // get the configuration and template
            freemarker.template.Template template = new Template("URLTemplate", new StringReader(text), config);

            // get the main hash
            SimpleHash model = freemarkerManager.buildTemplateModel(ctx.getValueStack(), null, servletContext, req, res, config.getObjectWrapper());

            // populate the hash with the properties
            if (params != null)
            {
                model.putAll(params);
            }

            // finally, render it
            StringWriter writer = new StringWriter();
            template.process(model, writer);
            return writer.toString();
        }
        catch (Exception e)
        {
            log.error("Error revolving template", e);
            return ExceptionUtils.getStackTrace(e);
        }
    }

    public void setFreemarkerManager(FreemarkerManager freemarkerManager)
    {
        this.freemarkerManager = freemarkerManager;
    }

    public void setServletContext(ServletContext servletContext)
    {
        this.servletContext = servletContext;
    }
}
