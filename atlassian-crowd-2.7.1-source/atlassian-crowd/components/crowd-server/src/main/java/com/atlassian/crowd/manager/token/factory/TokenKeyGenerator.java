package com.atlassian.crowd.manager.token.factory;

import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.model.authentication.ValidationFactor;

import java.util.List;

/**
 * Handles the creation of a 'random' token key
 */
public interface TokenKeyGenerator
{
    /**
     * Creates a String that will be used by a {@link com.atlassian.crowd.model.token.Token} as its key.
     * This 'key' will be based on the passed in parameters.
     * @param directoryID
     * @param name
     * @param validationFactors
     * @return a base 64 encoded String reprentation of the passed in attributes
     * @throws InvalidTokenException
     */
    String generateKey(long directoryID, String name, List<ValidationFactor> validationFactors) throws InvalidTokenException;
}
