package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.config.ConfigurationException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.property.PropertyManager;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Upgrade task to migrate the serverID from the
 * database to crowd.cfg.xml.
 */

public class UpgradeTask212 implements UpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask212.class);

    public static final String SERVER_ID_KEY = "server.id";

    private PropertyManager propertyManager;
    private CrowdBootstrapManager bootstrapManager;

    private Collection<String> errors = new ArrayList<String>();

    public String getBuildNumber()
    {
        return "212";
    }

    public String getShortDescription()
    {
        return "Moving bootstrap serverID property from database to crowd.cfg.xml";
    }

    public void doUpgrade()
    {

        String dbServerId = null;
        try
        {
            dbServerId = propertyManager.getProperty(SERVER_ID_KEY);
        }
        catch (ObjectNotFoundException e)
        {
            // server id does not exist in DB, no need to move it to the crowd.cfg.xml
        }

        if (dbServerId != null)
        {
            try
            {
                bootstrapManager.setServerID(dbServerId);

                propertyManager.removeProperty(SERVER_ID_KEY);
            }
            catch (ConfigurationException e)
            {
                log.error("Failed to write 'server id' property to crowd.cfg.xml", e);
                errors.add("Failed to write 'server id' property to crowd.cfg.xml");
            }
        }
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    public void setBootstrapManager(CrowdBootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
    }
}
