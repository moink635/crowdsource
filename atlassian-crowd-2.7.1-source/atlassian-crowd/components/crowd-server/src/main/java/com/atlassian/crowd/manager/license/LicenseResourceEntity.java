package com.atlassian.crowd.manager.license;

public class LicenseResourceEntity
{
    private final long directoryId;
    private final String username;

    public LicenseResourceEntity(final long directoryId, final String username)
    {
        this.directoryId = directoryId;
        this.username = username;
    }

    @Override
    public boolean equals(final Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LicenseResourceEntity that = (LicenseResourceEntity) o;

        if (directoryId != that.directoryId) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = (int) (directoryId ^ (directoryId >>> 32));
        result = 31 * result + (username != null ? username.hashCode() : 0);
        return result;
    }
}
