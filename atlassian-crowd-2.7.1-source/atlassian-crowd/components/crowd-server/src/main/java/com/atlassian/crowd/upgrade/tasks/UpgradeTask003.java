package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * UpgradeTask Setting the Hi-Lo value in the database if it has not already been set
 */
public class UpgradeTask003 implements UpgradeTask
{
    private static final String BUILD_NUMBER = "3";

    private static final Logger log = LoggerFactory.getLogger(UpgradeTask003.class);

    private List<String> errors = new ArrayList<String>();

    // Spring managed deps, these will be auto-wired by the UpgradeManager
    private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;
    private CrowdBootstrapManager bootstrapManager;

    public String getBuildNumber()
    {
        return BUILD_NUMBER;
    }

    public String getShortDescription()
    {
        return "Setting the Hi-Lo value in the database if it has not already been set";
    }

    public void doUpgrade() throws Exception
    {
        try
        {
            long result = resetableHiLoGeneratorHelper.getHiValue();

            // If the hi-lo value is less than 1, and crowd has been setup we must have an old version of Crowd.
            if (result <= 1L && bootstrapManager.isSetupComplete())
            {
                resetableHiLoGeneratorHelper.setNextHiValue(errors);
            }
        }
        catch (SQLException e)
        {
            log.error("You cannot start Crowd, since there is an issue with the hibernate_unique_key", e);
            errors.add("You cannot start Crowd, since there is an issue with the hibernate_unique_key, please see the log for more details");
        }
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setResetableHiLoGeneratorHelper(ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper)
    {
        this.resetableHiLoGeneratorHelper = resetableHiLoGeneratorHelper;
    }

    public void setBootstrapManager(CrowdBootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
    }
}
