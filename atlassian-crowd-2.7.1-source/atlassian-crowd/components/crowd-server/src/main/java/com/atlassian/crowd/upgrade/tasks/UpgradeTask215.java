package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * An upgrade task that removes the ldap.user.encryption attribute from directories that do not
 * need this attribute, currently this is everything that is not OpenLDAP.
 */
public class UpgradeTask215 implements UpgradeTask
{
    private Collection<String> errors = new ArrayList<String>();

    private DirectoryManager directoryManager;

    public String getBuildNumber()
    {
        return "215";
    }

    public String getShortDescription()
    {
        return "Removing superfluous paged results size attribute(ldap.pagedresults.size) from directories that don't paged results configured.";
    }

    public void doUpgrade() throws Exception
    {
        // find all of the directories
        List<Directory> directories = directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS));

        for (Directory directory : directories)
        {
            Boolean usingPagedResults = Boolean.valueOf(directory.getValue(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_KEY));

            // If the directory is not using paged results, delete the paged results size
            if (!usingPagedResults)
            {
                DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
                directoryToUpdate.removeAttribute(LDAPPropertiesMapper.LDAP_PAGEDRESULTS_SIZE);

                // store the update to the database
                directoryManager.updateDirectory(directoryToUpdate);
            }
        }
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }
}
