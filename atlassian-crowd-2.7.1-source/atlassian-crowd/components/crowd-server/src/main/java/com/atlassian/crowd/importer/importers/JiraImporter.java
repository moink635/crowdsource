package com.atlassian.crowd.importer.importers;

import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.manager.directory.DirectoryManager;

import org.springframework.jdbc.BadSqlGrammarException;

public class JiraImporter extends BaseDelegatingJdbcImporter
{
    private static final String CROWD_USER_COUNT = "SELECT COUNT(id)  FROM cwd_user";

    public JiraImporter(DirectoryManager directoryManager)
    {
        super(directoryManager);
    }

    private boolean isUsingCrowdUser()
    {
        try
        {
            return jdbcTemplate.queryForInt(CROWD_USER_COUNT) > 0;
        }
        catch (BadSqlGrammarException bsge)
        {
            // This indicates that this jira does not have crowd embedded.
            return false;
        }
    }

    @Override
    public Importer determineImporter(Configuration configuration) throws ImporterException
    {
        if (configuration == null || !(getConfigurationType().isInstance((configuration))))
        {
            throw new IllegalArgumentException("The supplied configuration was of the incorrect type for this Importer, should have been: " + getConfigurationType().getCanonicalName());
        }

        init(configuration);

        Importer importer;

        if (isUsingCrowdUser())
        {
            importer = new CrowdifiedJiraImporter(directoryManager);
        }
        else
        {
            importer = new JiraLegacyImporter(directoryManager);
        }

        return importer;
    }

}