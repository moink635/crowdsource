package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.directory.MicrosoftActiveDirectory;
import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Default existing AD directories to useMemberOfForGroupMembership = true, and all to useMemberOfAttribute = false.
 */
public class UpgradeTask342 implements UpgradeTask
{
    private DirectoryManager directoryManager;

    public String getBuildNumber()
    {
        return "342";
    }

    public String getShortDescription()
    {
        return "Adding 'useUserMembershipAttribute' and 'useUserMembershipAttributeForGroupMembership' defaults to non-internal directories";
    }

    public void doUpgrade() throws Exception
    {
        List<Directory> directories = directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS));
        for (Directory directory : directories)
        {
            if (directory.getType() != DirectoryType.INTERNAL)
            {
                DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);

                directoryToUpdate.setAttribute(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE, Boolean.FALSE.toString());
                directoryToUpdate.setAttribute(LDAPPropertiesMapper.LDAP_USING_USER_MEMBERSHIP_ATTRIBUTE_FOR_GROUP_MEMBERSHIP, Boolean.toString(isActiveDirectory(directory)));
                directoryManager.updateDirectory(directoryToUpdate);
            }
        }
    }

    private boolean isActiveDirectory(Directory directory)
    {
        return MicrosoftActiveDirectory.class.getCanonicalName().equals(directory.getImplementationClass()) || MicrosoftActiveDirectory.class.getCanonicalName().equals(directory.getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS));
    }

    public Collection getErrors()
    {
        return Collections.EMPTY_LIST;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }
}
