package com.atlassian.crowd.importer.mappers.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.util.UserUtils;

import org.springframework.jdbc.core.RowMapper;

/**
 * Will map a row from a JDBC {@link ResultSet} to a {@link com.atlassian.crowd.model.user.User}.
 * Populates missing name information as required.
 *
 * @see UserUtils#populateNames(com.atlassian.crowd.model.user.User)
 */
public class UserMapper implements RowMapper
{
    private final Configuration configuration;
    private final String name;
    private final String email;
    private final String fullname;
    private final String password;

    public UserMapper(final Configuration configuration, final String name, final String email, final String fullname, final String password)
    {
        this.configuration = configuration;
        this.name = name;
        this.email = email;
        this.fullname = fullname;
        this.password = password;
    }

    @Override
    public Object mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        String username = rs.getString(name);

        final PasswordCredential credential;
        if (configuration.isImportPasswords())
        {
            credential = new PasswordCredential(rs.getString(password), true);
        }
        else
        {
            credential = PasswordCredential.NONE;
        }

        UserTemplate user = new UserTemplate(username, configuration.getDirectoryID());
        user.setActive(true);
        user.setEmailAddress(rs.getString(email));
        user.setDisplayName(rs.getString(fullname));

        // need to populate firstName and lastName
        User populatedUser = UserUtils.populateNames(user);

        return new UserTemplateWithCredentialAndAttributes(populatedUser, credential);
    }
}
