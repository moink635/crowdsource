package com.atlassian.crowd.importer.exceptions;

/**
 * This exception represents that a configuration to a given Import was
 * invalid. This will most likely mean that it is of the incorrect type
 */
public class ImporterConfigurationException extends ImporterException
{
    /**
     * Default constructor.
     */
    public ImporterConfigurationException()
    {
    }

    /**
     * Default constructor.
     *
     * @param s the message.
     */
    public ImporterConfigurationException(String s)
    {
        super(s);
    }

    /**
     * Default constructor.
     *
     * @param s         the message.
     * @param throwable the {@link Exception Exception}.
     */
    public ImporterConfigurationException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    /**
     * Default constructor.
     *
     * @param throwable the {@link Exception Exception}.
     */
    public ImporterConfigurationException(Throwable throwable)
    {
        super(throwable);
    }
}
