package com.atlassian.crowd.migration.legacy.database.sql;

public class PostgresLegacyTableQueries implements LegacyTableQueries
{
    // ------------------------
    // Verifiers
    // ------------------------
    public String getVerifyGroupsSQL()
    {
        return "SELECT \"ID\", \"NAME\" FROM \"REMOTEGROUP\"";
    }

    public String getVerifyRolesSQL()
    {
        return "SELECT \"ID\", \"NAME\" FROM \"REMOTEROLE\"";
    }

    // ------------------------
    // Directory
    // ------------------------
    public String getDirectoriesSQL()
    {
        return new StringBuilder()
                .append("SELECT \"ID\", \"NAME\", \"ACTIVE\", \"CONCEPTION\", \"LASTMODIFIED\", \"DESCRIPTION\", \"IMPLEMENTATIONCLASS\", \"CODE\" ")
                .append("FROM \"DIRECTORY\"").toString();
    }

    public String getDirectoryOperationsSQL()
    {
        return new StringBuilder()
                .append("SELECT \"DIRECTORYID\", \"DIRECTORYPERMISSIONS\".\"KEY\" ")
                .append("FROM \"DIRECTORYPERMISSIONS\" ")
                .append("WHERE \"VALUE\" = 'true'").toString();
    }

    public String getDirectoryAttributesSQL()
    {
        return new StringBuilder()
                .append("SELECT \"ATTRIBUTES\".\"DIRECTORYID\", \"ATTRIBUTES\".\"ATTRIBUTE\", \"ATTRIBUTEVALUES\".\"VALUE\" ")
                .append("FROM \"ATTRIBUTES\", \"ATTRIBUTEVALUES\" ")
                .append("WHERE \"ATTRIBUTES\".\"ID\" = \"ATTRIBUTEVALUES\".\"ATTRIBUTEVALUEID\" ").toString();
    }

    // ------------------------
    // User
    // ------------------------
    public String getInternalEntityTemplatesForUsersSQL()
    {
        return new StringBuilder()
                .append("SELECT \"DIRECTORYID\", \"NAME\", \"CONCEPTION\", \"LASTMODIFIED\", \"ACTIVE\" ")
                .append("FROM \"REMOTEPRINCIPAL\"").toString();
    }

    public String getUserCredentialsSQL()
    {
        return new StringBuilder()
                .append("SELECT \"REMOTEPRINCIPALDIRECTORYID\", \"REMOTEPRINCIPALNAME\", \"CREDENTIAL\" ")
                .append("FROM \"REMOTEPRINCIPALCREDENTIALS\"").toString();
    }

    public String getAllUserAttributesSQL()
    {
        return new StringBuilder()
                .append("SELECT \"ATTRIBUTES\".\"ATTRIBUTE\", \"ATTRIBUTEVALUES\".\"VALUE\", \"ATTRIBUTES\".\"REMOTEPRINCIPALDIRECTORYID\", \"ATTRIBUTES\".\"REMOTEPRINCIPALNAME\" ")
                .append("FROM \"ATTRIBUTES\", \"ATTRIBUTEVALUES\" ")
                .append("WHERE \"ATTRIBUTES\".\"ID\" = \"ATTRIBUTEVALUES\".\"ATTRIBUTEVALUEID\" ")
                .append("AND \"ATTRIBUTES\".\"REMOTEPRINCIPALDIRECTORYID\" IS NOT NULL ")
                .append("AND \"ATTRIBUTES\".\"REMOTEPRINCIPALNAME\" IS NOT NULL").toString();
    }

    public String getUserCredentialsHistorySQL()
    {
        return new StringBuilder()
                .append("SELECT \"REMOTEPRINCIPALDIRECTORYID\", \"REMOTEPRINCIPALNAME\", \"CREDENTIAL\" ")
                .append("FROM \"PRINCIPALCREDENTIALHISTORY\" ")
                .append("ORDER BY \"PRINCIPALCREDENTIALHISTORY\".\"INDEX\"").toString();
    }


    // ------------------------
    // Group
    // ------------------------
    public String getGroupsSQL()
    {
        return new StringBuilder()
                .append("SELECT \"NAME\", \"ACTIVE\", \"CONCEPTION\", \"LASTMODIFIED\", \"DESCRIPTION\", DIRECTORYID ")
                .append("FROM \"REMOTEGROUP\"").toString();
    }

    public String getGroupAttributesSQL()
    {
        return new StringBuilder()
                .append("SELECT \"ATTRIBUTES\".\"REMOTEGROUPDIRECTORYID\", \"ATTRIBUTES\".\"REMOTEGROUPNAME\", \"ATTRIBUTES\".\"ATTRIBUTE\", \"ATTRIBUTEVALUES\".\"VALUE\" ")
                .append("FROM \"ATTRIBUTES\", \"ATTRIBUTEVALUES\" ")
                .append("WHERE \"ATTRIBUTES\".\"ID\" = \"ATTRIBUTEVALUES\".\"ATTRIBUTEVALUEID\" ")
                .append("AND \"ATTRIBUTES\".\"REMOTEGROUPDIRECTORYID\" IS NOT NULL ")
                .append("AND \"ATTRIBUTES\".\"REMOTEGROUPNAME\" IS NOT NULL ").toString();
    }

    public String getGroupMembershipsSQL()
    {
        return new StringBuilder()
                .append("SELECT \"REMOTEGROUPNAME\", \"REMOTEPRINCIPALNAME\", \"REMOTEGROUPDIRECTORYID\" ")
                .append("FROM \"REMOTEGROUPMEMBERS\"").toString();
    }

    // ------------------------
    // Role
    // ------------------------
    public String getRolesSQL()
    {
        return new StringBuilder()
                .append("SELECT \"NAME\", \"ACTIVE\", \"CONCEPTION\", \"LASTMODIFIED\", \"DESCRIPTION\", DIRECTORYID ")
                .append("FROM \"REMOTEROLE\"").toString();
    }

    public String getRoleAttributesSQL()
    {
        return new StringBuilder()
                .append("SELECT \"ATTRIBUTES\".\"REMOTEROLEDIRECTORYID\", \"ATTRIBUTES\".\"REMOTEROLENAME\", \"ATTRIBUTES\".\"ATTRIBUTE\", \"ATTRIBUTEVALUES\".\"VALUE\" ")
                .append("FROM \"ATTRIBUTES\", \"ATTRIBUTEVALUES\" ")
                .append("WHERE \"ATTRIBUTES\".\"ID\" = \"ATTRIBUTEVALUES\".\"ATTRIBUTEVALUEID\" ")
                .append("AND \"ATTRIBUTES\".\"REMOTEROLEDIRECTORYID\" IS NOT NULL ")
                .append("AND \"ATTRIBUTES\".\"REMOTEROLENAME\" IS NOT NULL ").toString();
    }

    public String getRoleMembershipsSQL()
    {
        return new StringBuilder()
                .append("SELECT \"REMOTEROLENAME\", \"REMOTEPRINCIPALNAME\", \"REMOTEROLEDIRECTORYID\" ")
                .append("FROM \"REMOTEROLEMEMBERS\"").toString();
    }

    // ------------------------
    // Application
    // ------------------------
    public String getApplicationsSQL()
    {
        return new StringBuilder()
                .append("SELECT \"ID\", \"NAME\", \"CONCEPTION\", \"LASTMODIFIED\", \"ACTIVE\", \"DESCRIPTION\" ")
                .append("FROM \"APPLICATION\"").toString();
    }

    public String getDirectoryMappingDataSQL()
    {
        return new StringBuilder()
                .append("SELECT \"APPLICATIONID\", \"DIRECTORYID\", \"ALLOWALLTOAUTHENTICATE\" ")
                .append("FROM \"APPLICATIONDIRECTORIES\"").toString();
    }

    public String getRemoteAddressesSQL()
    {
        return "SELECT \"APPLICATIONID\", \"ADDRESS\" FROM \"APPLICATIONADDRESSES\"";
    }

    public String getApplicationPasswordCredentialsSQL()
    {
        return "SELECT \"APPLICATIONID\", \"CREDENTIAL\" FROM \"APPLICATIONCREDENTIALS\"";
    }

    public String getApplicationAttributesSQL()
    {
        return new StringBuilder()
                .append("SELECT \"ATTRIBUTES\".\"APPLICATIONID\", \"ATTRIBUTES\".\"ATTRIBUTE\", \"ATTRIBUTEVALUES\".\"VALUE\" ")
                .append("FROM \"ATTRIBUTES\", \"ATTRIBUTEVALUES\" ")
                .append("WHERE \"ATTRIBUTES\".\"ID\" = \"ATTRIBUTEVALUES\".\"ATTRIBUTEVALUEID\"").toString();
    }

    public String getAllowedOperationsSQL()
    {
        return new StringBuilder()
                .append("SELECT APPLICATIONID, DIRECTORYID, PERMISSION_TYPE ")
                .append("FROM APPLICATIONDIRECTORYPERMISSION ")
                .append("WHERE ALLOWED = TRUE").toString();
    }

    public String getAssociatedGroupsSQL()
    {
        return new StringBuilder()
                .append("SELECT \"APPLICATIONID\", \"DIRECTORYID\", \"NAME\" ")
                .append("FROM \"APPLICATIONGROUPS\" ")
                .append("WHERE \"ACTIVE\" = TRUE").toString();
    }

    // ------------------------
    // Property
    // ------------------------
    public String getPropertiesSQL()
    {
        return "SELECT \"NAME\", \"VALUE\" FROM \"SERVERPROPERTY\"";
    }

    // ------------------------
    // SAL Property
    // ------------------------
    public String getSALPropertiesSQL()
    {
        return "SELECT \"SALPROPERTY\".\"KEY\", \"PROPERTYNAME\", \"STRINGVALUE\" FROM \"SALPROPERTY\"";
    }
}

