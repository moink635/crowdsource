package com.atlassian.crowd.event.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.embedded.impl.ConnectionPoolPropertyConstants;
import com.atlassian.crowd.event.application.ApplicationReadyEvent;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.event.api.EventListener;

/**
 * Will update the LDAP connection pool system settings after the application started up.
 */
public class SetLdapConnectionSettingsListener
{
    private static final Logger log = LoggerFactory.getLogger(SetLdapConnectionSettingsListener.class);

    private PropertyManager propertyManager;

    @EventListener
    public void handleEvent(final ApplicationReadyEvent event) throws ObjectNotFoundException
    {
        // Update the LDAP connection system properties with values from the database
        setLdapSystemProperty(ConnectionPoolPropertyConstants.POOL_INITIAL_SIZE);
        setLdapSystemProperty(ConnectionPoolPropertyConstants.POOL_PREFERRED_SIZE);
        setLdapSystemProperty(ConnectionPoolPropertyConstants.POOL_MAXIMUM_SIZE);
        setLdapSystemProperty(ConnectionPoolPropertyConstants.POOL_TIMEOUT);
        setLdapSystemProperty(ConnectionPoolPropertyConstants.POOL_PROTOCOL);
        setLdapSystemProperty(ConnectionPoolPropertyConstants.POOL_AUTHENTICATION);
    }

    private void setLdapSystemProperty(String propertyKey) throws ObjectNotFoundException
    {
        log.debug("Setting system-wide LDAP connection pool property: <" + propertyKey + "> with value: <" + propertyManager.getProperty(propertyKey) + ">");
        System.setProperty(propertyKey, propertyManager.getProperty(propertyKey));
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}
