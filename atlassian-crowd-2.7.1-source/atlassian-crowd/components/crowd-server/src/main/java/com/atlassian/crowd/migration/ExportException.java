package com.atlassian.crowd.migration;

public class ExportException extends Exception
{
    public ExportException(Exception exception)
    {
        super(exception);
    }
}
