package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ResourceLocator;
import com.atlassian.crowd.util.PropertyUtils;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

/**
 * This class extends the current 2.x CrowdPropertiesMapper because crowd.properties mapping is 100% backwards-compatible in XML.
 */
public class CrowdPropertiesMapper extends com.atlassian.crowd.migration.CrowdPropertiesMapper implements LegacyImporter
{
    public CrowdPropertiesMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, ClientProperties clientProperties , ResourceLocator resourceLocator, PropertyUtils propertyUtils)
 	{
 	    super(sessionFactory, batchProcessor, clientProperties, resourceLocator, propertyUtils);
 	}

    public void importXml(final Element root, final LegacyImportDataHolder importData) throws ImportException
    {
        super.importXml(root);
    }
}
