/*
 * Copyright (c) 2005 Your Corporation. All Rights Reserved.
 */
package com.atlassian.crowd.event;

/**
 * Unable to process the event.
 */
public class EventJobExecutionException extends RuntimeException
{
    /**
     * Default constructor.
     */
    public EventJobExecutionException()
    {
    }

    /**
     * Default constructor.
     *
     * @param s the message.
     */
    public EventJobExecutionException(String s)
    {
        super(s);
    }

    /**
     * Default constructor.
     *
     * @param s         the message.
     * @param throwable the {@link Exception Exception}.
     */
    public EventJobExecutionException(String s, Throwable throwable)
    {
        super(s, throwable);
    }

    /**
     * Default constructor.
     *
     * @param throwable the {@link Exception Exception}.
     */
    public EventJobExecutionException(Throwable throwable)
    {
        super(throwable);
    }
}