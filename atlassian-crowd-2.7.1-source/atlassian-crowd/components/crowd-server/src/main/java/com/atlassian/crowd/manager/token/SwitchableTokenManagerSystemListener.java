package com.atlassian.crowd.manager.token;

import com.atlassian.config.lifecycle.events.ApplicationStartedEvent;
import com.atlassian.crowd.event.migration.XMLRestoreFinishedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventListenerRegistrar;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to application events and updates the SwitchableTokenManager if necessary.
 *
 * @since v2.7
 */
public class SwitchableTokenManagerSystemListener
{
    private final SwitchableTokenManager switchableTokenManager;
    private final EventListenerRegistrar eventPublisher;

    public SwitchableTokenManagerSystemListener(EventListenerRegistrar eventPublisher,
                                                SwitchableTokenManager switchableTokenManager)
    {
        this.eventPublisher = checkNotNull(eventPublisher);
        this.switchableTokenManager = checkNotNull(switchableTokenManager);
    }

    // init-method
    void registerWithEventPublisher()
    {
        eventPublisher.register(this);
    }

    /**
     * On the application started event, switch to the correct token storage
     * @param event the application started event
     */
    @EventListener
    public void handleEvent(ApplicationStartedEvent event)
    {
        switchableTokenManager.updateTokenStorage();
    }

    @EventListener
    public void handleEvent(XMLRestoreFinishedEvent event)
    {
        switchableTokenManager.updateTokenStorage();
    }
}
