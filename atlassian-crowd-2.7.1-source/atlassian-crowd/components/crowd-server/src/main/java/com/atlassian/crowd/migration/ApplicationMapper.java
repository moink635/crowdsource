package com.atlassian.crowd.migration;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.migration.legacy.XmlMapper;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * This mapper will handle the mapping of a {@see com.atlassian.crowd.model.application.Application}
 */
public class ApplicationMapper extends XmlMapper implements Mapper
{
    public static final String APPLICATION_XML_ROOT = "applications";
    public static final String APPLICATION_XML_NODE = "application";
    public static final String APPLICATION_XML_TYPE = "type";
    public static final String APPLICATION_XML_DESCRIPTION = "description";
    public static final String APPLICATION_XML_CREDENTIAL = "credential";

    public static final String APPLICATION_XML_REMOTE_ADDRESSES = "remoteAddresses";
    public static final String APPLICATION_XML_REMOTE_ADDRESS = "remoteAddress";

    public static final String APPLICATION_XML_DIRECTORY_MAPPINGS = "directoryMappings";
    public static final String APPLICATION_XML_DIRECTORY_MAPPING = "directoryMapping";
    public static final String APPLICATION_XML_DIRECTORY_ID = "directoryId";
    public static final String APPLICATION_XML_DIRECTORY_ALLOW_ALL = "allowAll";

    public static final String APPLICATION_XML_PERMISSIONS = "permissions";
    public static final String APPLICATION_XML_PERMISSION = "permission";

    public static final String APPLICATION_XML_GROUP_MAPPINGS = "groupMappings";
    public static final String APPLICATION_XML_GROUP_MAPPING = "groupMapping";
    public static final String APPLICATION_XML_GROUP_NAME = "groupName";

    public static final String OPTION_DEFAULT_PASSWORD = "password";

    private final ApplicationManager applicationManager;
    private final DirectoryDAOHibernate directoryDAO;

    public ApplicationMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, ApplicationManager applicationManager, DirectoryDAOHibernate directoryDAO)
    {
        super(sessionFactory, batchProcessor);
        this.applicationManager = applicationManager;
        this.directoryDAO = directoryDAO;
    }

    public Element exportXml(Map options) throws ExportException
    {
        Element applicationRoot = DocumentHelper.createElement(APPLICATION_XML_ROOT);

        List<Application> applications = applicationManager.findAll();

        for (Application application : applications)
        {
            addApplicationToXml((ApplicationImpl) application, applicationRoot);
        }

        return applicationRoot;
    }

    public void importXml(Element root) throws ImportException
    {
        Element applicationRoot = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + APPLICATION_XML_ROOT);
        if (applicationRoot == null)
        {
            logger.error("No applications were found for importing!");
            return;
        }

        for (Iterator applications = applicationRoot.elementIterator(); applications.hasNext();)
        {
            Element applicationElement = (Element) applications.next();
            Application application = getApplicationFromXml(applicationElement);
            addEntity(application);
        }
    }

    protected void addApplicationToXml(ApplicationImpl application, Element directoryRoot)
    {
        Element applicationElement = directoryRoot.addElement(APPLICATION_XML_NODE);
        exportInternalEntity(application, applicationElement);
        applicationElement.addElement(APPLICATION_XML_TYPE).addText(application.getType().name());
        applicationElement.addElement(APPLICATION_XML_DESCRIPTION).addText(StringUtils.defaultString(application.getDescription()));
        applicationElement.addElement(APPLICATION_XML_CREDENTIAL).addText(application.getCredential().getCredential());

        addRemoteAddressesToXml(new TreeSet<RemoteAddress>(application.getRemoteAddresses()), applicationElement);

        exportSingleValuedAttributes(application, applicationElement);

        addDirectoryMappingsToXml(application.getDirectoryMappings(), applicationElement);
    }

    private void addRemoteAddressesToXml(final SortedSet<RemoteAddress> remoteAddresses, final Element applicationElement)
    {
        Element remoteAddressesElement = applicationElement.addElement(APPLICATION_XML_REMOTE_ADDRESSES);
        for (RemoteAddress remoteAddress : remoteAddresses)
        {
            remoteAddressesElement.addElement(APPLICATION_XML_REMOTE_ADDRESS).addText(remoteAddress.getAddress());
        }
    }

    private void addDirectoryMappingsToXml(List<DirectoryMapping> directoryMappings, Element applicationElement)
    {
        Element directoryMappingsElement = applicationElement.addElement(APPLICATION_XML_DIRECTORY_MAPPINGS);

        for (DirectoryMapping directoryMapping : directoryMappings)
        {
            Element directoryMappingElement = directoryMappingsElement.addElement(APPLICATION_XML_DIRECTORY_MAPPING);
            directoryMappingElement.addElement(GENERIC_XML_ID).addText(directoryMapping.getId().toString());
            directoryMappingElement.addElement(APPLICATION_XML_DIRECTORY_ID).addText(directoryMapping.getDirectory().getId().toString());
            directoryMappingElement.addElement(APPLICATION_XML_DIRECTORY_ALLOW_ALL).addText(Boolean.toString(directoryMapping.isAllowAllToAuthenticate()));

            TreeSet<GroupMapping> groupMappings = new TreeSet<GroupMapping>(new GroupMapping.COMPARATOR());
            groupMappings.addAll(directoryMapping.getAuthorisedGroups());
            addGroupMappingsToXml(groupMappings, directoryMappingElement);

            addDirectoryPermissionsToXml(new TreeSet<OperationType>(directoryMapping.getAllowedOperations()), directoryMappingElement);
        }
    }

    private void addDirectoryPermissionsToXml(final SortedSet<OperationType> operations, final Element directoryMappingElement)
    {
        Element permissionsElement = directoryMappingElement.addElement(APPLICATION_XML_PERMISSIONS);
        for (OperationType operation : operations)
        {
            permissionsElement.addElement(APPLICATION_XML_PERMISSION).addText(operation.name());
        }
    }

    private void addGroupMappingsToXml(final SortedSet<GroupMapping> groupMappings, final Element directoryMappingElement)
    {
        Element groupMappingsElement = directoryMappingElement.addElement(APPLICATION_XML_GROUP_MAPPINGS);

        for (GroupMapping groupMapping : groupMappings)
        {
            Element groupMappingElement = groupMappingsElement.addElement(APPLICATION_XML_GROUP_MAPPING);
            groupMappingElement.addElement(GENERIC_XML_ID).addText(groupMapping.getId().toString());
            groupMappingElement.addElement(APPLICATION_XML_GROUP_NAME).addText(groupMapping.getGroupName());
        }
    }

    protected ApplicationImpl getApplicationFromXml(final Element applicationElement)
    {
        // add application basics
        InternalEntityTemplate template = getInternalEntityTemplateFromXml(applicationElement);
        ApplicationImpl application = new ApplicationImpl(template);

        application.setType(ApplicationType.valueOf(applicationElement.element(APPLICATION_XML_TYPE).getText()));
        application.setDescription(applicationElement.element(APPLICATION_XML_DESCRIPTION).getText());
        application.setCredential(new PasswordCredential(applicationElement.element(APPLICATION_XML_CREDENTIAL).getText(), true));

        // add remote addresses
        Element remoteAddressesElement = applicationElement.element(APPLICATION_XML_REMOTE_ADDRESSES);
        for (Iterator addresses = remoteAddressesElement.elementIterator(); addresses.hasNext();)
        {
            Element remoteAddressElement = (Element) addresses.next();
            application.addRemoteAddress(remoteAddressElement.getText());
        }

        // add attributes
        Map<String, String> attributes = getSingleValuedAttributesMapFromXml(applicationElement);
        application.setAttributes(attributes);

        // add directory mappings
        Element directoryMappingsElement = applicationElement.element(APPLICATION_XML_DIRECTORY_MAPPINGS);
        for (Iterator directoryMappingsIter = directoryMappingsElement.elementIterator(); directoryMappingsIter.hasNext();)
        {
            Element directoryMappingElement = (Element) directoryMappingsIter.next();

            Long directoryMappingId = Long.parseLong(directoryMappingElement.element(GENERIC_XML_ID).getText());
            Long directoryId = Long.parseLong(directoryMappingElement.element(APPLICATION_XML_DIRECTORY_ID).getText());
            Directory directoryReference = (Directory) directoryDAO.loadReference(directoryId);
            boolean allowAll = Boolean.parseBoolean(directoryMappingElement.element(APPLICATION_XML_DIRECTORY_ALLOW_ALL).getText());

            DirectoryMapping directoryMapping = new DirectoryMapping(directoryMappingId, application, directoryReference, allowAll);

            // add group mappings
            Element groupMappingsElement = directoryMappingElement.element(APPLICATION_XML_GROUP_MAPPINGS);
            for (Iterator groupMappingsIter = groupMappingsElement.elementIterator(); groupMappingsIter.hasNext();)
            {
                Element groupMappingElement = (Element) groupMappingsIter.next();

                Long groupMappingId = Long.parseLong(groupMappingElement.element(GENERIC_XML_ID).getText());
                String groupName = groupMappingElement.element(APPLICATION_XML_GROUP_NAME).getText();

                GroupMapping groupMapping = new GroupMapping(groupMappingId, directoryMapping, groupName);
                directoryMapping.getAuthorisedGroups().add(groupMapping);
            }

            // add allowed operations
            Element permissionsElement = directoryMappingElement.element(APPLICATION_XML_PERMISSIONS);
            for (Iterator permissionsIter = permissionsElement.elementIterator(); permissionsIter.hasNext();)
            {
                Element permissionElement = (Element) permissionsIter.next();

                OperationType operation = OperationType.valueOf(permissionElement.getText());
                directoryMapping.addAllowedOperations(operation);
            }

            application.getDirectoryMappings().add(directoryMapping);
        }

        return application;
    }
}
