package com.atlassian.crowd.manager.login.util;

import com.atlassian.crowd.exception.InvalidEmailAddressException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.manager.mail.MailManager;
import com.atlassian.crowd.manager.mail.MailSendException;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.util.I18nHelper;
import com.google.common.base.Joiner;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.Validate;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Helper class to perform mailing tasks for forgotten login.
 */
public class ForgottenLoginMailer
{
    /**
     * Forgotten login username macro key.
     */
    public static final String USERNAME_MACRO = "$username";

    /**
     * Forgotten login firstname macro key.
     */
    public static final String FIRSTNAME_MACRO = "$firstname";

    /**
     * Forgotten login lastname macro key.
     */
    public static final String LASTNAME_MACRO = "$lastname";

    /**
     * Forgotten login deployment title macro key.
     */
    public static final String DEPLOYMENTTITLE_MACRO = "$deploymenttitle";

    /**
     * Forgotten login date macro key.
     */
    public static final String DATE_MACRO = "$date";

    /**
     * Forgotten login key. For backwards compatibility only.
     *
     * @deprecated
     */
    public static final String PASSWORD_MACRO = "$password";

    /**
     * Forgotten login link macro key.
     */
    public static final String RESET_LINK_MACRO = "$resetlink";

    /**
     * Forgotten login email macro key.
     */
    public static final String EMAIL_MACRO = "$email";

    /**
     * Forgotten login administrator contact macro key.
     */
    public static final String ADMIN_CONTACT_MACRO = "$admincontact";

    /**
     * Forgotten login active status
     */
    public static final String ACTIVE_MACRO = "$active";

    private static final String MAILER_FORGOT_PASSWORD_SUBJECT = "forgottenpassword.subject";
    private static final String MAILER_FORGOT_USERNAME_SUBJECT = "forgottenusername.subject";

    private static final String AVAILABLE_AT_TEXT = "mailtemplate.template.forgotten.password.availableat.text";

    private final PropertyManager propertyManager;
    private final MailManager mailManager;
    private final I18nHelper i18nHelper;

    /**
     * Constructs a new instance of ForgottenLoginMailer.
     *
     * @param propertyManager   manager used to retrieve SMTP and site configurations
     * @param mailManager       manager used to send emails
     * @param i18nHelper        internationalisation helper
     */
    public ForgottenLoginMailer(final PropertyManager propertyManager, final MailManager mailManager, final I18nHelper i18nHelper)
    {
        this.propertyManager = checkNotNull(propertyManager);
        this.mailManager = checkNotNull(mailManager);
        this.i18nHelper = checkNotNull(i18nHelper);
    }

    /**
     * Mails the reset password link to the <code>user</code>.
     * 
     * @param user      user to email
     * @param resetLink link to set a new password
     * @throws IllegalArgumentException     if <code>user</code> or <code>resetLink</code> is null
     * @throws InvalidEmailAddressException if the email address returned by <code>user.getEmailAddress()</code> is not a valid email address
     * @throws MailSendException            if the email cannot be sent
     */
    public void mailResetPasswordLink(final User user, final String resetLink)
            throws InvalidEmailAddressException, MailSendException
    {
        Validate.notNull(user, "User cannot be null");
        Validate.notNull(resetLink, "Reset link cannot be null");
        Preconditions.checkNotNull(user.getEmailAddress(), "User's email cannot be null");

        try
        {
            String messageTemplate;

            try
            {
                messageTemplate = propertyManager.getProperty(Property.FORGOTTEN_PASSWORD_EMAIL_TEMPLATE);
            }
            catch (ObjectNotFoundException e)
            {
                throw new MailSendException(e);
            }

            String email = user.getEmailAddress();
            InternetAddress emailAddress = new InternetAddress(email);

            Map<String, String> macroReplacementMap = new LinkedHashMap<String, String>();
            macroReplacementMap.put(FIRSTNAME_MACRO, user.getFirstName());
            macroReplacementMap.put(LASTNAME_MACRO, user.getLastName());
            macroReplacementMap.put(USERNAME_MACRO, user.getName());
            macroReplacementMap.put(ACTIVE_MACRO, checkActiveUser(user));
            macroReplacementMap.put(RESET_LINK_MACRO, resetLink);
            // replacing password macro for backwards compatibility only
            macroReplacementMap.put(PASSWORD_MACRO, i18nHelper.getText(AVAILABLE_AT_TEXT) + " " + resetLink);
            macroReplacementMap.put(DEPLOYMENTTITLE_MACRO, propertyManager.getDeploymentTitle());
            macroReplacementMap.put(DATE_MACRO, new Date().toString());
            messageTemplate = replaceMacros(messageTemplate, macroReplacementMap);

            mailManager.sendEmail(emailAddress, i18nHelper.getText(MAILER_FORGOT_PASSWORD_SUBJECT), messageTemplate);
        }
        catch (AddressException e)
        {
            throw new InvalidEmailAddressException(e.getMessage(), e);
        }
        catch (PropertyManagerException e)
        {
            throw new MailSendException(e);
        }
    }

    /**
     * Mails the list of usernames to the <code>user</code>.  The list of <code>usernames</code> must not be empty.
     *
     * @param user      user to email
     * @param usernames list of usernames associated with the email
     * @throws IllegalArgumentException     if <code>user</code> or <code>usernames</code> is null or empty.
     * @throws InvalidEmailAddressException if the email address returned by <code>user.getEmailAddress()</code> is not a valid email address
     * @throws MailSendException            if the email cannot be sent
     */
    public void mailUsernames(final User user, final List<String> usernames)
            throws InvalidEmailAddressException, MailSendException
    {
        Validate.notNull(user, "User cannot be null");
        Validate.notEmpty(usernames);
        Preconditions.checkNotNull(user.getEmailAddress(), "User's email cannot be null");
        
        try
        {
            String email = user.getEmailAddress();
            InternetAddress emailAddress = new InternetAddress(email);

            String messageTemplate = propertyManager.getProperty(Property.FORGOTTEN_USERNAME_EMAIL_TEMPLATE);

            String formattedUsernames = Joiner.on(", ").join(usernames);

            Map<String, String> macroReplacementMap = new LinkedHashMap<String, String>();
            macroReplacementMap.put(FIRSTNAME_MACRO, user.getFirstName());
            macroReplacementMap.put(LASTNAME_MACRO, user.getLastName());
            macroReplacementMap.put(USERNAME_MACRO, formattedUsernames);
            macroReplacementMap.put(DEPLOYMENTTITLE_MACRO, propertyManager.getDeploymentTitle());
            macroReplacementMap.put(DATE_MACRO, new Date().toString());
            macroReplacementMap.put(EMAIL_MACRO, email);
            macroReplacementMap.put(ADMIN_CONTACT_MACRO, propertyManager.getProperty(Property.NOTIFICATION_EMAIL));
            messageTemplate = replaceMacros(messageTemplate, macroReplacementMap);

            mailManager.sendEmail(emailAddress, i18nHelper.getText(MAILER_FORGOT_USERNAME_SUBJECT), messageTemplate);
        }
        catch (AddressException e)
        {
            throw new InvalidEmailAddressException(e);
        }
        catch (PropertyManagerException e)
        {
            throw new MailSendException(e);
        }
        catch (ObjectNotFoundException e)
        {
            throw new MailSendException(e);
        }
    }

    private static String replaceMacros(String messageTemplate, Map<String, String> macroReplacementMap)
    {
        Validate.notNull(messageTemplate);

        for (Map.Entry<String, String> replacementEntry : macroReplacementMap.entrySet())
        {
            // if there is no value to replace the macro with (i.e. value is null)
            // replace macro (and preceding space) with nothing
            String extra = "";
            if (replacementEntry.getValue() == null)
            {
                extra = "\\s";
            }
            String toFind = "(" + extra + "\\" + replacementEntry.getKey() + "){1}";
            messageTemplate = messageTemplate.replaceAll(toFind, replacementEntry.getValue());
        }

        return messageTemplate;
    }

    public String checkActiveUser(User user)
    {
        if (user.isActive())
        {
            return i18nHelper.getText("mailtemplate.template.user.active");
        }
        else
        {
            return i18nHelper.getText("mailtemplate.template.user.inactive");
        }
    }
}
