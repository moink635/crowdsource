package com.atlassian.crowd.plugin.web;

import com.atlassian.plugin.util.PluginUtils;
import com.atlassian.plugin.webresource.DefaultResourceBatchingConfiguration;

public class CrowdResourceBatchingConfiguration extends DefaultResourceBatchingConfiguration
{
    @Override
    public boolean isContextBatchingEnabled()
    {
        return !Boolean.getBoolean(PluginUtils.ATLASSIAN_DEV_MODE);
    }
}
