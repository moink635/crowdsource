package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Makes sure that 1.3 and earlier directories, on upgrade, have an RDN added. Handles the special case of Active Directory by
 * setting the RDN to "cn= ", a value that was formerly hard-coded.
 */
public class UpgradeTask216 implements UpgradeTask
{
    private Collection errors = new ArrayList();

    private DirectoryManager directoryManager;

    public String getBuildNumber()
    {
        return "216";
    }

    public String getShortDescription()
    {
        return "Adding an ldap.user.username.rdn to directories that lack it. Will be set the same as ldap.user.username except for Active Directory.";
    }

    public void doUpgrade() throws Exception
    {
        // find all of the directories
        List<Directory> directories = directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS));

        for (Directory directory : directories)
        {
            if (directory.getValue(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY) == null)
            {
                String rdnValue;

                // special case of Active Directory, because USER_USERNAME_KEY will be "sAMAccountName"
                if ("com.atlassian.crowd.integration.directory.connector.MicrosoftActiveDirectory".equals(directory.getImplementationClass())
                        || "com.atlassian.crowd.directory.MicrosoftActiveDirectory".equals(directory.getImplementationClass()))
                {
                    rdnValue = "cn";
                }
                else
                {
                    rdnValue = directory.getValue(LDAPPropertiesMapper.USER_USERNAME_KEY);
                }

                DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);

                directoryToUpdate.setAttribute(LDAPPropertiesMapper.USER_USERNAME_RDN_KEY, rdnValue);

                // store the update to the database
                directoryManager.updateDirectory(directoryToUpdate);
            }
        }
    }

    public Collection getErrors()
    {
        return errors;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }
}