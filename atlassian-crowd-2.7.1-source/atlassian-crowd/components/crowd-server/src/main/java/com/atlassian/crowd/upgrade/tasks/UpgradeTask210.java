package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.manager.property.PropertyManagerException;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Upgrade task to add the gzip-enabled server property (true by default).
 */

public class UpgradeTask210 implements UpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask210.class);

    private PropertyManager propertyManager;
    private Collection errors = new ArrayList();

    public String getBuildNumber()
    {
        return "210";
    }

    public String getShortDescription()
    {
        return "Adding GZip compression property.";
    }

    public void doUpgrade() throws Exception
    {
        try
        {
            propertyManager.isGzipEnabled();
        }
        catch (PropertyManagerException e)
        {
            propertyManager.setGzipEnabled(true);
        }
    }

    //CLOVER:OFF
    public Collection getErrors()
    {
        return errors;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}
