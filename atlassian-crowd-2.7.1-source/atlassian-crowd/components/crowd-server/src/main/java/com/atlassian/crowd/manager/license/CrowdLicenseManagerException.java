package com.atlassian.crowd.manager.license;

public class CrowdLicenseManagerException extends Exception
{
    public CrowdLicenseManagerException()
    {
        super();
    }

    public CrowdLicenseManagerException(String message)
    {
        super(message);
    }

    public CrowdLicenseManagerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public CrowdLicenseManagerException(Throwable cause)
    {
        super(cause);
    }
}
