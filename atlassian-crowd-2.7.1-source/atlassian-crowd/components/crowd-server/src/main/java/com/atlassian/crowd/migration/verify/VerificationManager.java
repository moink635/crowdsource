package com.atlassian.crowd.migration.verify;

import com.atlassian.crowd.migration.ImportException;
import org.dom4j.Document;
import org.dom4j.Element;

import java.util.List;

/**
 * This manager will look after verifiers that need to run against the imported XML document <strong>before</strong> import
 * to validate that the import can proceed.
 */
public class VerificationManager
{
    private final List<Verifier> verifiers;

    public VerificationManager(List<Verifier> verifiers)
    {
        this.verifiers = verifiers;
    }

    /**
     * Will validate the given document against a list of {@link Verifier}'s
     *
     * @param document the document to validate
     * @throws ImportException if there are any errors during validation
     */
    public void validate(Document document) throws ImportException
    {
        final Element root = document.getRootElement();
        for (Verifier verifier : verifiers)
        {
            // Clear any state
            verifier.clearState();

            verifier.verify(root);

            if (verifier.hasErrors())
            {
                throw new ImportException("XML file failed validation", verifier.getErrors());
            }
        }

    }
}
