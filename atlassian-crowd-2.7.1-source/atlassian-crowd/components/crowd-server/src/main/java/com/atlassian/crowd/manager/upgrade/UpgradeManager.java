package com.atlassian.crowd.manager.upgrade;

import com.atlassian.crowd.upgrade.tasks.UpgradeTask;

import java.util.Collection;
import java.util.List;

/**
 * This manager is responsible for all things related to upgrades for Crowd. Upgrades are performed by
 * {@link com.atlassian.crowd.upgrade.tasks.UpgradeTask}'s and are associated to a given build number.
 */
public interface UpgradeManager
{
    /**
     * Determines whether we need to run the upgrade or not.
     * @return {@link boolean} whether the build was successful or not
     */
    boolean needUpgrade();

    /**
     * @return list of upgrade tasks that are required to be executed (in order).
     */
    List<UpgradeTask> getRequiredUpgrades();

    /**
     * Method to fire off the upgrade process
     * @return {@link java.util.Collection} a string collection of error messages. Collection not empty
     *          denotes error occurred on upgrade
     * @throws Exception error occurred during upgrade.
     */
    Collection<String> doUpgrade() throws Exception;

    /**
     * This gets the pre-build number. i.e The build number present in the database, BEFORE the upgrade
     * process has been run.
     * 
     * @return the current build number (in data store)
     */
    public int getDataBuildNumber();
}
