package com.atlassian.crowd.importer.model;

/**
 * DTO class representing a membership relationship.
 */
public final class MembershipDTO
{
    public enum ChildType
    {
        USER,
        GROUP
    }

    private String childName;
    private ChildType childType;
    private String parentName;

    public MembershipDTO(ChildType childType, String childName, String parentName)
    {
        this.childType = childType;
        this.childName = childName;
        this.parentName = parentName;
    }

    public String getChildName()
    {
        return childName;
    }

    public void setChildName(String username)
    {
        this.childName = username;
    }

    public String getParentName()
    {
        return parentName;
    }

    public void setParentName(String parentName)
    {
        this.parentName = parentName;
    }

    public ChildType getChildType()
    {
        return childType;
    }

    public void setChildType(ChildType childType)
    {
        this.childType = childType;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        MembershipDTO that = (MembershipDTO) o;

        if (childName != null ? !childName.equals(that.childName) : that.childName != null)
        {
            return false;
        }
        if (childType != that.childType)
        {
            return false;
        }
        if (parentName != null ? !parentName.equals(that.parentName) : that.parentName != null)
        {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode()
    {
        int result = childName != null ? childName.hashCode() : 0;
        result = 31 * result + (childType != null ? childType.hashCode() : 0);
        result = 31 * result + (parentName != null ? parentName.hashCode() : 0);
        return result;
    }

    /**
     * Will give a string representation of the relationship, username-parentName
     * @return a string representation of the relationship
     */
    public String getRelationship()
    {
        return getRelationshipKey(childType, childName, parentName);
    }

    public static String getRelationshipKey(ChildType childType, String childName, String containerName)
    {
        // This has to be somewhat readable.
        return new StringBuilder().append(childName).append("(")
                                  .append(childType).append(") - ")
                                  .append(containerName).toString();
    }

    public String toString()
    {
        return getRelationshipKey(childType, childName, parentName);
    }
}