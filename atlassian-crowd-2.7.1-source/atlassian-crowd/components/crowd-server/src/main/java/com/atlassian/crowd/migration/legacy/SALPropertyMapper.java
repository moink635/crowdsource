package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.dao.property.PropertyDAOHibernate;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.dom4j.Element;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

/**
 * This mapper will handle the mapping of a com.atlassian.crowd.model.salproperty.SALProperty objects
 * This mapper is here for legacy compatibility only, for releases of Crowd older the 1.6 that used SAL
 * integration (i.e. JIRA Stuio)
 */
public class SALPropertyMapper extends GenericLegacyImporter implements LegacyImporter
{
    private final Logger logger = LoggerFactory.getLogger(SALPropertyMapper.class);
    private final PropertyDAOHibernate propertyDAO;

    protected static final String SALPROPERTY_XML_ROOT = "salProperties";
    protected static final String SALPROPERTY_XML_NODE = "salProperty";
    protected static final String SALPROPERTY_XML_KEY = "key";
    protected static final String SALPROPERTY_XML_PROPERTY_NAME = "propertyName";
    protected static final String SALPROPERTY_XML_STRING_VALUE = "propertyValue";

    public SALPropertyMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, PropertyDAOHibernate propertyDAO)
    {
        super(sessionFactory, batchProcessor);
        this.propertyDAO = propertyDAO;
    }

    public void importXml(final Element root, final LegacyImportDataHolder importData) throws ImportException
    {
        final Element salPropertiesElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + SALPROPERTY_XML_ROOT);
        if (salPropertiesElement == null)
        {
            logger.info("No sal properties were found for importing!");
        }
        else
        {
            for (final Iterator<?> salProperties = salPropertiesElement.elementIterator(); salProperties.hasNext();)
            {
                final Element salPropertyElement = (Element) salProperties.next();
                final String key = salPropertyElement.element(SALPROPERTY_XML_KEY).getText();
                final String propertyName = salPropertyElement.element(SALPROPERTY_XML_PROPERTY_NAME).getText();
                final String stringValue = salPropertyElement.element(SALPROPERTY_XML_STRING_VALUE).getText();

                addEntityViaSave(new Property(PLUGIN_KEY_PREFIX + key, propertyName, stringValue));
            }
        }
    }
}
