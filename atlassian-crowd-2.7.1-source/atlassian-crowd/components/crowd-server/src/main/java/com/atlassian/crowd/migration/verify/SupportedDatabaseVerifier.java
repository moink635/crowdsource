package com.atlassian.crowd.migration.verify;

import com.atlassian.crowd.util.SystemInfoHelper;
import com.atlassian.crowd.util.persistence.hibernate.SQLServerIntlDialect;
import org.hibernate.dialect.HSQLDialect;
import org.hibernate.dialect.MySQL5Dialect;
import org.hibernate.dialect.MySQL5InnoDBDialect;
import org.hibernate.dialect.MySQLDialect;
import org.hibernate.dialect.MySQLInnoDBDialect;
import org.hibernate.dialect.MySQLMyISAMDialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.Oracle9iDialect;
import org.hibernate.dialect.OracleDialect;
import org.hibernate.dialect.PostgreSQLDialect;
import org.hibernate.dialect.SQLServerDialect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SupportedDatabaseVerifier extends DatabaseVerifier
{
    private static final Logger logger = LoggerFactory.getLogger(SupportedDatabaseVerifier.class);

    public static final String POSTGRESQL_HIBERNATE_DIALECT = PostgreSQLDialect.class.getName();
    public static final String HSQLDB_HIBERNATE_DIALECT = HSQLDialect.class.getName();
    public static final String MYSQL_HIBERNATE_DIALECT = MySQLDialect.class.getName();
    public static final String MYSQL5_HIBERNATE_DIALECT = MySQL5Dialect.class.getName();
    public static final String MYSQL_INNODB_HIBERNATE_DIALECT = MySQLInnoDBDialect.class.getName();
    public static final String MYSQL5_INNODB_HIBERNATE_DIALECT = MySQL5InnoDBDialect.class.getName();
    public static final String MYSQL_MYIDSAM_HIBERNATE_DIALECT = MySQLMyISAMDialect.class.getName();

    public static final String ORACLE10G_HIBERNATE_DIALECT = Oracle10gDialect.class.getName();
    public static final String ORACLE9I_HIBERNATE_DIALECT = Oracle9iDialect.class.getName();
    public static final String ORACLE8I_HIBERNATE_DIALECT = OracleDialect.class.getName();
    public static final String MSSQL_HIBERNATE_DIALECT = SQLServerDialect.class.getName();
    public static final String MSSQL_INT_HIBERNATE_DIALECT = SQLServerIntlDialect.class.getName();

    private String databaseHibernateDialect;

    public SupportedDatabaseVerifier(SystemInfoHelper systemInfoHelper)
    {
        databaseHibernateDialect = systemInfoHelper.getDatabaseHibernateDialect();
    }

    public void verify()
    {
        logger.info("Checking if database can be automatically migrated.");

        if (!isSupportedDatabase(databaseHibernateDialect))
        {
            errors.add("Crowd does not currently support automatic database migration for databases using the dialect: <" + databaseHibernateDialect + ">");
        }
    }


    private boolean isSupportedDatabase(String hibernateDialect)
    {
        return hibernateDialect != null && (
                isMySQL(hibernateDialect) ||
                isPostgreSQL(hibernateDialect) ||
                isHSQLDB(hibernateDialect) ||
                isMsSQLServer(hibernateDialect)
        );
    }

    public static boolean isMySQL(String hibernateDialect)
    {
        return (hibernateDialect.equals(MYSQL_HIBERNATE_DIALECT) ||
                hibernateDialect.equals(MYSQL5_HIBERNATE_DIALECT) ||
                hibernateDialect.equals(MYSQL_INNODB_HIBERNATE_DIALECT) ||
                hibernateDialect.equals(MYSQL5_INNODB_HIBERNATE_DIALECT) ||
                hibernateDialect.equals(MYSQL_MYIDSAM_HIBERNATE_DIALECT));
    }

    public static boolean isPostgreSQL(String hibernateDialect)
    {
        return hibernateDialect.equals(POSTGRESQL_HIBERNATE_DIALECT);
    }

    public static boolean isHSQLDB(String hibernateDialect)
    {
        return hibernateDialect.equals(HSQLDB_HIBERNATE_DIALECT);
    }

    public static boolean isMsSQLServer(String hibernateDialect)
    {
        return hibernateDialect.equals(MSSQL_HIBERNATE_DIALECT) || hibernateDialect.equals(MSSQL_INT_HIBERNATE_DIALECT);
    }

    public static boolean isOracle(String hibernateDialect)
    {
        return (hibernateDialect.equals(ORACLE8I_HIBERNATE_DIALECT) ||
                hibernateDialect.equals(ORACLE9I_HIBERNATE_DIALECT) ||
                hibernateDialect.equals(ORACLE10G_HIBERNATE_DIALECT));
    }
}

