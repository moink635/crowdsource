package com.atlassian.crowd.plugin.factory;

import com.atlassian.crowd.plugin.PluginDirectoryLocator;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.factories.PluginFactory;
import com.atlassian.plugin.loaders.BundledPluginLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.web.context.ServletContextAware;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

import javax.servlet.ServletContext;

/**
 * Responsible for loading bundled plugins that
 * are stored in "atlassian-bundled-plugins.zip"
 * on the root of the classpath, or, the jars
 * from the crowd-home/bundled-plugins directory.
 */
public class BundledPluginLoaderFactory implements FactoryBean<BundledPluginLoader>, ServletContextAware
{
    private static final Logger log = LoggerFactory.getLogger(BundledPluginLoaderFactory.class);
    private final PluginDirectoryLocator pluginDirectoryLocator;
    private final List<PluginFactory> pluginFactories;
    private final PluginEventManager pluginEventManager;
    private final String bundledPluginFileName;
    private static final String ATLASSIAN_PLUGINS_BUNDLED_DISABLE = "atlassian.plugins.bundled.disable";

    public BundledPluginLoaderFactory(PluginDirectoryLocator pluginDirectoryLocator, List<PluginFactory> pluginFactories, PluginEventManager pluginEventManager, String bundledPluginFileName)
    {
        this.pluginDirectoryLocator = pluginDirectoryLocator;
        this.pluginFactories = pluginFactories;
        this.pluginEventManager = pluginEventManager;
        this.bundledPluginFileName = bundledPluginFileName;
    }

    private ServletContext servletContext;

    @Override
    public void setServletContext(ServletContext servletContext)
    {
        this.servletContext = servletContext;
    }

    /**
     * Resolve the <code>bundledPluginFileName</code> to the URL of a resource.
     */
    URL getBundledPluginsUrl() throws MalformedURLException
    {
        try
        {
            URI uri = new URI(bundledPluginFileName);
            if (uri.isAbsolute())
            {
                return uri.toURL();
            }
        }
        catch (URISyntaxException e)
        {
            // Fall through and treat it as a path
        }

        String realPath = servletContext.getRealPath("/WEB-INF/classes/" + bundledPluginFileName);

        if (realPath != null)
        {
            File f = new File(realPath);
            if (f.exists())
            {
                return f.toURI().toURL();
            }
        }

        URL bundledPluginsURL = servletContext.getResource(bundledPluginFileName);


        if (bundledPluginsURL == null)
        {
            throw new IllegalStateException("Could not load <" + bundledPluginFileName + "> from classpath");
        }

        return bundledPluginsURL;
    }

    @Override
    public BundledPluginLoader getObject() throws Exception
    {
        if (Boolean.getBoolean(ATLASSIAN_PLUGINS_BUNDLED_DISABLE))
        {
            log.warn("Bundled plugins have been disabled. Removing bundled plugin loader.");
            return null;
        }
        else
        {
            File frameworkBundlesDirectory = pluginDirectoryLocator.getBundledPluginsDirectory();

            URL bundledPluginsURL = getBundledPluginsUrl();

            if (Boolean.getBoolean("atlassian.dev.mode"))
            {
            	// Not currently any difference between regular and -dev plugins
                log.info("We are in dev mode, using the following location for plugins <" + bundledPluginsURL.toExternalForm() + ">");
            }

            return new BundledPluginLoader(bundledPluginsURL, frameworkBundlesDirectory, pluginFactories, pluginEventManager);
        }
    }

    public Class<BundledPluginLoader> getObjectType()
    {
        return BundledPluginLoader.class;
    }

    public boolean isSingleton()
    {
        return true;
    }
}
