package com.atlassian.crowd.plugin.descriptors;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentFactory;
import org.dom4j.Element;
import org.dom4j.io.DOMWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ClassPathResource;

import java.util.Iterator;

/**
 * NOTE: This class has to be constructor injected since it's the only way moduleFactory can be set at its parent.
 */
public class SpringComponentModuleDescriptor extends AbstractModuleDescriptor implements StateAware, ApplicationContextAware
{
    private static final Logger log = LoggerFactory.getLogger(SpringComponentModuleDescriptor.class);
    String alias;
    private Document document;

    private ApplicationContext applicationContext;

    public SpringComponentModuleDescriptor(ModuleFactory moduleFactory)
    {
        super(moduleFactory);
    }

    public void init(Plugin plugin, Element element) throws PluginParseException
    {
        super.init(plugin, element);

        alias = element.attributeValue("alias");
        if (StringUtils.isBlank(alias))
        {
            alias = getKey();
        }
        DocumentFactory factory = DocumentFactory.getInstance();
        Element beans = factory.createElement("beans");
        Element bean = factory.createElement("bean");
        beans.add(bean);
        bean.addAttribute("id", alias);
        bean.addAttribute("class", getModuleClass().getName());
        for (Iterator i = element.attributes().iterator(); i.hasNext();)
        {
            Attribute a = (Attribute) i.next();
            if (!a.getName().equals("key") && !a.getName().equals("name") && !a.getName().equals("class") && !a.getName().equals("alias"))
            {
                bean.addAttribute(a.getName(), a.getValue());
            }
        }
        for (int i = 0; i < element.nodeCount(); ++i)
        {
            // ignore text nodes
            if (element.node(i) instanceof Element)
            {
                bean.add(((Element) element.node(i)).createCopy());
            }
        }
        document = factory.createDocument();
        document.add(beans);
    }

    public Object getModule()
    {
        return moduleFactory.createModule(moduleClassName, this);
    }

    public void enabled()
    {
        super.enabled();

        DefaultListableBeanFactory beanFactory = getGlobalBeanFactory();

        if (beanFactory != null)
        {
            // We should not overwrite an existing definition unless we were able to replace it when we uninstall this
            // module. Since this is currently not supported, disallowing the overwrite will have to do for now.
            // NOTE: It is not as simple as maintaining a reference to the previous value. Multiple plugins could
            //       accidentally overwrite the same value creating a potential mess when we start uninstalling them.
            boolean definitionExists;
            try
            {
                // using beanFactory.containsBeanDefinition will return true after we have uninstalled a bean since we do not
                // remove the key, just set its value to null. Hence, we need to use the lookup.
                definitionExists = beanFactory.getBeanDefinition(alias) != null;
            }
            catch (NoSuchBeanDefinitionException e)
            {
                definitionExists = false;
            }
            if (definitionExists)
            {
                throw new IllegalStateException("Can not overwrite an existing bean definition: " + alias);
            }
            log.debug("Creating bean definition for " + alias + " with class " + getModuleClass().getName());
            XmlBeanDefinitionReader reader = new XmlBeanDefinitionReader(beanFactory);

            if (plugin.isDynamicallyLoaded())
            {
                reader.setBeanClassLoader(plugin.getClassLoader());
            }

            DOMWriter writer = new DOMWriter();
            try
            {
                reader.registerBeanDefinitions(writer.write(document), new ClassPathResource("x"));
                // create the component now, so that any problem will cause the plugin load to fail
                applicationContext.getBean(alias);
            }
            catch (DocumentException de)
            {
                log.error("failed to convert Document to DOM", de);
            }
        }
    }

    public void disabled()
    {
        DefaultListableBeanFactory beanFactory = getGlobalBeanFactory();
        if (beanFactory != null)
        {
            log.debug("Removing bean definition for " + alias);
            unregisterBeanDefinition(alias);
        }

        super.disabled();
    }

    public void unregisterBeanDefinition(String beanAlias)
    {
        getGlobalBeanFactory().removeBeanDefinition(beanAlias);
    }


    /**
     * Support autowire of application context
     *
     * @param applicationContext
     * @throws org.springframework.beans.BeansException
     *
     */
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException
    {
        this.applicationContext = applicationContext;
    }

    private DefaultListableBeanFactory getGlobalBeanFactory()
    {
        if (applicationContext instanceof ConfigurableApplicationContext)
        {
            ConfigurableApplicationContext configurableApplicationContext = (ConfigurableApplicationContext) applicationContext;
            if (configurableApplicationContext.getBeanFactory() instanceof DefaultListableBeanFactory)
            {
                return (DefaultListableBeanFactory) configurableApplicationContext.getBeanFactory();
            }
            else
            {
                log.error("Failed to lookup global bean factory - BeanFactory was not a DefaultListableBeanFactory?");
            }
        }
        else
        {
            log.error("Failed to lookup global bean factory - ApplicationContext was not a ConfigurableApplicationContext?");
        }
        return null;
    }

}