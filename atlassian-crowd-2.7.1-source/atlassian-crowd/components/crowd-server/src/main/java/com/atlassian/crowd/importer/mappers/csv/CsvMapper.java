package com.atlassian.crowd.importer.mappers.csv;

import java.util.Arrays;

import com.atlassian.crowd.importer.exceptions.ImporterException;

import org.apache.commons.collections.OrderedBidiMap;
import org.apache.commons.lang3.StringUtils;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Abstract class that contains a common method that all sub-classes will require to read and manage
 * the mapping of attributes from a CSV line.
 */
public abstract class CsvMapper<T>
{
    private final OrderedBidiMap configuration;
    protected final Long directoryId;

    public CsvMapper(Long directoryId, OrderedBidiMap configuration)
    {
        this.directoryId = checkNotNull(directoryId);
        this.configuration = checkNotNull(configuration);
    }

    abstract T mapRow(String[] resultSet) throws ImporterException;

    protected boolean hasColumnFor(String key)
    {
        return configuration.containsValue(key);
    }

    public String getString(String[] userline, String key) throws ImporterException
    {
        checkNotNull(userline);

        String keyValue = (String) configuration.getKey(checkNotNull(key));
        String indexName = StringUtils.substringAfter(keyValue, ".");

        if (StringUtils.isBlank(indexName))
        {
            throw new ImporterException("Bad column mapping for " + key + ": " + keyValue);
        }

        int index = Integer.parseInt(indexName);

        if (userline.length > index)
        {
            return StringUtils.trimToNull(userline[index]);
        }
        else
        {
            throw new ImporterException("Missing column for " + key + ": " + Arrays.toString(userline));
        }
    }
}
