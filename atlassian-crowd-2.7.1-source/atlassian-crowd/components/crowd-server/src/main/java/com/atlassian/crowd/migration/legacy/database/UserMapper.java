package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.LegacyImportDataHolder;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchResultWithIdReferences;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class UserMapper extends DatabaseMapper implements DatabaseImporter
{
    private final DirectoryDAOHibernate directoryDAO;
    private final UserDAOHibernate userDAO;

    public UserMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, JdbcOperations jdbcTemplate, UserDAOHibernate userDAO, DirectoryDAOHibernate directoryDAO)
    {
        super(sessionFactory, batchProcessor, jdbcTemplate);
        this.directoryDAO = directoryDAO;
        this.userDAO = userDAO;
    }

    public void importFromDatabase(LegacyImportDataHolder importData) throws ImportException
    {
        List<UserTemplateWithCredentialAndAttributes> users = importUsersFromDatabase(importData.getOldToNewDirectoryIds());

        // now do the actual import as a batch
        BatchResultWithIdReferences<User> result = userDAO.addAll(users);
        for (User user : result.getFailedEntities())
        {
            logger.error("Unable to add user <" + user.getName() + "> in directory with id <" + user.getDirectoryId() + ">");
        }

        // die hard if there are any errors
        if (result.hasFailures())
        {
            throw new ImportException("Unable to import all users. See logs for more details.");
        }

        importData.setUserImportResults(result);
        logger.info("Successfully migrated " + users.size() + " users.");
    }

    protected List<UserTemplateWithCredentialAndAttributes> importUsersFromDatabase(final Map<Long, Long> oldToNewDirectoryIds) throws ImportException
    {
        List<UserTemplateWithCredentialAndAttributes> users = new ArrayList<UserTemplateWithCredentialAndAttributes>();

        List<InternalEntityTemplate> entityTemplates = getInternalEntityTemplatesForUsers();
        Map<EntityIdentifier, Map<String, Set<String>>> allUserAttributes = getAllUserAttributes();
        Map<EntityIdentifier, PasswordCredential> allUserCredentials = getCredentials();
        Map<EntityIdentifier, List<PasswordCredential>> allUserCredentialsHistory = getCredentialsHistory();

        for (InternalEntityTemplate entityTemplate : entityTemplates)
        {
            String userName = entityTemplate.getName();
            Date createdDate = entityTemplate.getCreatedDate();
            Date updatedDate = entityTemplate.getUpdatedDate();
            boolean active = entityTemplate.isActive();

            Long oldDirectoryId = entityTemplate.getId();
            Long directoryId = oldToNewDirectoryIds.get(oldDirectoryId);

            // Way to identify which user we're focused on - allows us to get attributes, credentials etc
            EntityIdentifier currentUser = new EntityIdentifier(oldDirectoryId, userName);

            // set the old directoryId to null
            entityTemplate.setId(null);

            // check the new directoryId is valid (ie. it exists)
            if (directoryId == null)
            {
                throw new IllegalArgumentException("User belongs to an unknown old directory with ID: " + oldDirectoryId);
            }

            // check if directory can be loaded?
            Directory directory = (Directory) directoryDAO.loadReference(directoryId);

            // firstname, lastname etc from legacy attributes table
            Map<String, Set<String>> attributes = allUserAttributes.get(currentUser);
            String firstName = getAttributeValue(UserConstants.FIRSTNAME, attributes, "");
            String lastName = getAttributeValue(UserConstants.LASTNAME, attributes, "");
            String displayName = getAttributeValue(UserConstants.DISPLAYNAME, attributes, "");
            String email = getAttributeValue(UserConstants.EMAIL, attributes, "");

            // get password
            PasswordCredential credential = allUserCredentials.get(currentUser);
            if (credential == null)
            {
                credential = PasswordCredential.NONE;                
            }

            // create default user template
            UserTemplateWithCredentialAndAttributes userTemplate = new UserTemplateWithCredentialAndAttributes(userName, directoryId, credential);
            userTemplate.setCreatedDate(createdDate);
            userTemplate.setUpdatedDate(updatedDate);
            userTemplate.setActive(active);
            userTemplate.setFirstName(firstName);
            userTemplate.setLastName(lastName);
            userTemplate.setDisplayName(displayName);
            userTemplate.setEmailAddress(email);

            // get password history - empty list if credential history does not exist for user
            List<PasswordCredential> records = allUserCredentialsHistory.get(currentUser);
            if (records == null)
            {
                records = new ArrayList<PasswordCredential>();
            }
            userTemplate.getCredentialHistory().addAll(records);

            // save all custom attributes
            attributes.remove(UserConstants.FIRSTNAME);
            attributes.remove(UserConstants.LASTNAME);
            attributes.remove(UserConstants.DISPLAYNAME);
            attributes.remove(UserConstants.EMAIL);
            userTemplate.getAttributes().putAll(attributes);

            users.add(userTemplate);
        }

        return users;
    }

    private List<InternalEntityTemplate> getInternalEntityTemplatesForUsers()
    {
        return jdbcTemplate.query(legacyTableQueries.getInternalEntityTemplatesForUsersSQL(), new UserTableMapper());
    }

    private class UserTableMapper implements RowMapper
    {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            // get the oldDirectoryId
            Long oldDirectoryId = rs.getLong("DIRECTORYID");

            // Grab the user data
            String userName = rs.getString("NAME");
            Date createDate = getDateFromDatabase(rs.getString("CONCEPTION"));
            Date updatedDate = getDateFromDatabase(rs.getString("LASTMODIFIED"));
            boolean active = rs.getBoolean("ACTIVE");

            return createInternalEntityTemplate(oldDirectoryId, userName, createDate, updatedDate, active);
        }
    }

    private Map<EntityIdentifier, PasswordCredential> getCredentials()
    {
        UserCredentialMapper userCredentialMapper = new UserCredentialMapper();
        jdbcTemplate.query(legacyTableQueries.getUserCredentialsSQL(), userCredentialMapper);
        return userCredentialMapper.getUserCredential();
    }

    private Map<EntityIdentifier, Map<String, Set<String>>> getAllUserAttributes()
    {
        AttributeMapper userAttributeMapper = new AttributeMapper("REMOTEPRINCIPALDIRECTORYID", "REMOTEPRINCIPALNAME");
        jdbcTemplate.query(legacyTableQueries.getAllUserAttributesSQL(), userAttributeMapper);

        Map<EntityIdentifier, List<Map<String, String>>> userAttributes = userAttributeMapper.getEntityAttributes();
        // Convert attributes from list of maps to a map.
        Map<EntityIdentifier, Map<String, Set<String>>> userAttributeMap = new HashMap<EntityIdentifier, Map<String, Set<String>>>();
        for (Map.Entry<EntityIdentifier, List<Map<String, String>>> entry : userAttributes.entrySet())
        {
            userAttributeMap.put(entry.getKey(), attributeListToMultiAttributeMap(entry.getValue()));
        }
        return userAttributeMap;
    }

    private Map<EntityIdentifier, List<PasswordCredential>> getCredentialsHistory()
    {
        UserCredentialMapper userCredentialMapper = new UserCredentialMapper();
        jdbcTemplate.query(legacyTableQueries.getUserCredentialsHistorySQL(), userCredentialMapper);
        return userCredentialMapper.getUserCredentialHistory();
    }

    private class UserCredentialMapper implements RowCallbackHandler
    {
        private final Map<EntityIdentifier, PasswordCredential> userCredential = new HashMap<EntityIdentifier, PasswordCredential>();
        private final Map<EntityIdentifier, List<PasswordCredential>> userCredentialHistory = new HashMap<EntityIdentifier, List<PasswordCredential>>();

        public void processRow(ResultSet rs) throws SQLException
        {
            String credential = rs.getString("CREDENTIAL");
            String username = rs.getString("REMOTEPRINCIPALNAME");
            Long directoryId = rs.getLong("REMOTEPRINCIPALDIRECTORYID");

            EntityIdentifier currentUser = new EntityIdentifier(directoryId, username);

            // If the user didn't have a pre-existing password.
            if (StringUtils.isBlank(credential))
            {
                credential = "password";
            }
            PasswordCredential passwordCredential = new PasswordCredential(credential, true);

            // Add to user's password credential
            userCredential.put(currentUser, passwordCredential);

            // Add to users's credential history
            if (!userCredentialHistory.containsKey(currentUser))
            {
                userCredentialHistory.put(currentUser, new ArrayList<PasswordCredential>());
            }
            userCredentialHistory.get(currentUser).add(passwordCredential);
        }

        public Map<EntityIdentifier, PasswordCredential> getUserCredential()
        {
            return userCredential;
        }

        public Map<EntityIdentifier, List<PasswordCredential>> getUserCredentialHistory()
        {
            return userCredentialHistory;
        }
    }
}
