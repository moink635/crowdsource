package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.LegacyImportDataHolder;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.application.GroupMapping;
import com.atlassian.crowd.model.application.RemoteAddress;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ApplicationMapper extends DatabaseMapper implements DatabaseImporter
{
    private final DirectoryDAOHibernate directoryDAO;

    public ApplicationMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, JdbcOperations jdbcTemplate, DirectoryDAOHibernate directoryDAO)
    {
        super(sessionFactory, batchProcessor, jdbcTemplate);
        this.directoryDAO = directoryDAO;
    }

    public void importFromDatabase(LegacyImportDataHolder importData) throws ImportException
    {
        List<ApplicationImpl> applications = importApplicationsFromDatabase(importData.getOldToNewDirectoryIds());
        for (Application application : applications)
        {
            addEntityViaMerge(application);
        }
        logger.info("Successfully migrated " + applications.size() + " applications.");
    }

    protected List<ApplicationImpl> importApplicationsFromDatabase(final Map<Long, Long> oldToNewDirectoryIds)
    {
        ApplicationTableMapper applicationTableMapper = new ApplicationTableMapper();
        List<ApplicationImpl> applications = getApplications(applicationTableMapper);
        Map<Application, Long> applicationIdMap = applicationTableMapper.getApplicationIdMap();

        // Maps of all info/data related with applications - specific entries can be retrieved with applicationId
        Map<Long, List<Map<String, String>>> allApplicationAttributes = getApplicationAttributes();
        Map<Long, List<AppDirMappingDataHolder>> allApplicationDirectoryMappingData = getDirectoryMappingData();
        Map<Long, PasswordCredential> allApplicationCredentials = getApplicationPasswordCredentials();
        Map<Long, Set<RemoteAddress>> allApplicationAddresses = getRemoteAddresses();

        for (ApplicationImpl application : applications)
        {
            Long oldApplicationId = applicationIdMap.get(application);

            // Get attributes (includes application type)
            final Map<String, String> applicationAttributes = attributeListToMap(allApplicationAttributes.get(oldApplicationId));
            application.setAttributes(applicationAttributes);
            application.setType(getApplicationType(applicationAttributes));

            List<DirectoryMapping> directoryMappings = new ArrayList<DirectoryMapping>();
            List<AppDirMappingDataHolder> directoryMappingData = allApplicationDirectoryMappingData.get(oldApplicationId);
            if (directoryMappingData == null)
            {
                directoryMappingData = new ArrayList<AppDirMappingDataHolder>();
            }
            for (AppDirMappingDataHolder mappingData : directoryMappingData)
            {
                // Properly create the DirectoryMapping now that we have reference to 'application'
                Long oldDirectoryId = mappingData.getOldDirectoryId();
                Directory directoryReference = (Directory) directoryDAO.loadReference(oldToNewDirectoryIds.get(oldDirectoryId));
                DirectoryMapping directoryMapping = new DirectoryMapping(application, directoryReference, mappingData.isAllowAllToAuthenticate());

                // Get Allowed Permissions associated with this mapping
                // TODO: how to check if the import was from super old 1.0.x, 1.1.x etc?
                directoryMapping.setAllowedOperations(mappingData.getAllowedOperations());

                // Create GroupMappings associated with this mapping based on the groupnames
                Set<String> groupNames = mappingData.getAuthorisedGroupNames();
                for (String groupName : groupNames)
                {
                    if (StringUtils.isNotBlank(groupName))
                    {
                        GroupMapping groupMapping = new GroupMapping(directoryMapping, groupName);
                        directoryMapping.getAuthorisedGroups().add(groupMapping);
                    }
                }
                directoryMappings.add(directoryMapping);
            }
            // Add the updated directoryMappings
            application.getDirectoryMappings().addAll(directoryMappings);

            // Set Application credentials
            PasswordCredential credential = allApplicationCredentials.get(oldApplicationId);
            if (credential == null)
            {
                credential = PasswordCredential.NONE;
            }
            application.setCredential(credential);

            // Set Application Remote addresses
            Set<RemoteAddress> remoteAddresses = allApplicationAddresses.get(oldApplicationId);
            if (remoteAddresses == null)
            {
                remoteAddresses = new HashSet<RemoteAddress>();
            }
            application.setRemoteAddresses(remoteAddresses);
        }

        return applications;
    }

    private List<ApplicationImpl> getApplications(ApplicationTableMapper applicationTableMapper)
    {
        return jdbcTemplate.query(legacyTableQueries.getApplicationsSQL(), applicationTableMapper);
    }

    private Map<Long, List<AppDirMappingDataHolder>> getDirectoryMappingData()
    {
        DirectoryMappingTableMapper directoryMappingTableMapper = new DirectoryMappingTableMapper();

        // Deal with the initial DirectoryMapping data
        jdbcTemplate.query(legacyTableQueries.getDirectoryMappingDataSQL(), directoryMappingTableMapper);
        Map<Long, List<AppDirMappingDataHolder>> allApplicationDirectoryMappings = directoryMappingTableMapper.getApplicationDirectoryMappingMap();

        // Update the mappings with the allowed groups for each DirectoryMapping map
        allApplicationDirectoryMappings = addAllowedOperations(allApplicationDirectoryMappings);

        allApplicationDirectoryMappings = addAssociatedGroups(allApplicationDirectoryMappings);

        return allApplicationDirectoryMappings;
    }

    private Map<Long, Set<RemoteAddress>> getRemoteAddresses()
    {
        RemoteAddressTableMapper remoteAddressTableMapper = new RemoteAddressTableMapper();
        jdbcTemplate.query(legacyTableQueries.getRemoteAddressesSQL(), remoteAddressTableMapper);
        return remoteAddressTableMapper.getApplicationAddresses();
    }

    private Map<Long, PasswordCredential> getApplicationPasswordCredentials()
    {
        ApplicationCredentialMapper applicationCredentialMapper = new ApplicationCredentialMapper();
        jdbcTemplate.query(legacyTableQueries.getApplicationPasswordCredentialsSQL(), applicationCredentialMapper);

        return applicationCredentialMapper.getApplicationCredential();
    }

    private Map<Long, List<Map<String, String>>> getApplicationAttributes()
    {
        ApplicationAttributeMapper applicationAttributeMapper = new ApplicationAttributeMapper();
        jdbcTemplate.query(legacyTableQueries.getApplicationAttributesSQL(), applicationAttributeMapper);
        Map<Long, List<Map<String, String>>> attributes = applicationAttributeMapper.getApplicationAttributes();
        if (attributes == null)
        {
            attributes = new HashMap<Long, List<Map<String, String>>>();
        }
        return attributes;
    }

    private Map<Long, List<AppDirMappingDataHolder>> addAllowedOperations(Map<Long, List<AppDirMappingDataHolder>> allApplicationDirectoryMappings)
    {
        AppDirOperationTableMapper appDirOperationTableMapper = new AppDirOperationTableMapper(allApplicationDirectoryMappings);
        // no quotes because it isn't needed! one of only the tables with lowercase name and columns
        jdbcTemplate.query(legacyTableQueries.getAllowedOperationsSQL(), appDirOperationTableMapper);

        Map<Long, List<AppDirMappingDataHolder>> allowedOperations = appDirOperationTableMapper.getApplicationDirectoryMappings();
        if (allowedOperations == null)
        {
            allowedOperations = new HashMap<Long, List<AppDirMappingDataHolder>>();
        }
        return allowedOperations;
    }

    private Map<Long, List<AppDirMappingDataHolder>> addAssociatedGroups(Map<Long, List<AppDirMappingDataHolder>> allApplicationDirectoryMappings)
    {
        AppDirGroupTableMapper appDirGroupTableMapper = new AppDirGroupTableMapper(allApplicationDirectoryMappings);
        jdbcTemplate.query(legacyTableQueries.getAssociatedGroupsSQL(), appDirGroupTableMapper);
        Map<Long, List<AppDirMappingDataHolder>> associatedGroups = appDirGroupTableMapper.getApplicationDirectoryMappings();
        if (associatedGroups == null)
        {
            associatedGroups = new HashMap<Long, List<AppDirMappingDataHolder>>();
        }
        return associatedGroups;
    }

    private ApplicationType getApplicationType(Map<String, String> applicationAttributes)
    {
        ApplicationType applicationType;
        String type = applicationAttributes.get("applicationType");
        if (StringUtils.isNotBlank(type))
        {
            try
            {
                applicationType = ApplicationType.valueOf(type);
            }
            catch (IllegalArgumentException e)
            {
                // invalid application type, default to generic
                applicationType = ApplicationType.GENERIC_APPLICATION;
            }
        }
        else
        {
            applicationType = ApplicationType.GENERIC_APPLICATION;
        }

        return applicationType;
    }

    private class ApplicationTableMapper implements RowMapper
    {
        private final Map<Application, Long> applicationIdMap = new HashMap<Application, Long>();

        public Object mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            // Application Basics (includes remote addresses)
            Long oldApplicationId = rs.getLong("ID");
            String name = rs.getString("NAME");
            Date createDate = getDateFromDatabase(rs.getString("CONCEPTION"));
            Date updatedDate = getDateFromDatabase(rs.getString("LASTMODIFIED"));
            boolean active = rs.getBoolean("ACTIVE");
            String description = rs.getString("DESCRIPTION");

            InternalEntityTemplate template = createInternalEntityTemplate(oldApplicationId, name, createDate, updatedDate, active);
            template.setId(null);

            // Setup the application
            ApplicationImpl application = new ApplicationImpl(template);
            application.setDescription(description);

            applicationIdMap.put(application, oldApplicationId);

            return application;
        }

        public Map<Application, Long> getApplicationIdMap()
        {
            return applicationIdMap;
        }
    }

    private class RemoteAddressTableMapper implements RowCallbackHandler
    {
        Map<Long, Set<RemoteAddress>> applicationAddresses = new HashMap<Long, Set<RemoteAddress>>();

        public void processRow(ResultSet rs) throws SQLException
        {
            Long applicationId = rs.getLong("APPLICATIONID");
            String address = rs.getString("ADDRESS");

            if (!applicationAddresses.containsKey(applicationId))
            {
                applicationAddresses.put(applicationId, new HashSet<RemoteAddress>());
            }
            applicationAddresses.get(applicationId).add(new RemoteAddress(address));
        }

        public Map<Long, Set<RemoteAddress>> getApplicationAddresses()
        {
            return applicationAddresses;
        }
    }

    private class ApplicationCredentialMapper implements RowCallbackHandler
    {
        private final Map<Long, PasswordCredential> applicationCredential = new HashMap<Long, PasswordCredential>();

        public void processRow(ResultSet rs) throws SQLException
        {
            Long applicationId = rs.getLong("APPLICATIONID");
            String credential = rs.getString("CREDENTIAL");
            PasswordCredential passwordCredential;

            // Create the credential, or give it a default if one does not exist
            if (StringUtils.isNotBlank(credential))
            {
                passwordCredential = new PasswordCredential(credential, true);
            }
            else
            {
                passwordCredential = PasswordCredential.NONE;
            }
            applicationCredential.put(applicationId, passwordCredential);
        }

        public Map<Long, PasswordCredential> getApplicationCredential()
        {
            return applicationCredential;
        }
    }

    private class ApplicationAttributeMapper implements RowCallbackHandler
    {
        private final Map<Long, List<Map<String, String>>> applicationAttributes = new HashMap<Long, List<Map<String, String>>>();

        public void processRow(ResultSet rs) throws SQLException
        {
            String attributeName = rs.getString("ATTRIBUTE");
            String attributeValue = rs.getString("VALUE");
            Long applicationId = rs.getLong("APPLICATIONID");
            Map<String, String> attributes = new HashMap<String, String>();
            attributes.put(attributeName, attributeValue);
            if (!applicationAttributes.containsKey(applicationId))
            {
                applicationAttributes.put(applicationId, new ArrayList<Map<String, String>>());
            }
            applicationAttributes.get(applicationId).add(attributes);
        }

        public Map<Long, List<Map<String, String>>> getApplicationAttributes()
        {
            return applicationAttributes;
        }
    }

    private class DirectoryMappingTableMapper implements RowCallbackHandler
    {
        private final Map<Long, List<AppDirMappingDataHolder>> applicationDirectoryMappingMap = new HashMap<Long, List<AppDirMappingDataHolder>>();

        public void processRow(ResultSet rs) throws SQLException
        {
            Long applicationId = rs.getLong("APPLICATIONID");
            Long oldDirectoryId = rs.getLong("DIRECTORYID");
            boolean allowAllToAuthenticate = rs.getBoolean("ALLOWALLTOAUTHENTICATE");

            AppDirMappingDataHolder appDirMappingDataHolder = new AppDirMappingDataHolder(oldDirectoryId);
            appDirMappingDataHolder.setAllowAllToAuthenticate(allowAllToAuthenticate);

            if (!applicationDirectoryMappingMap.containsKey(applicationId))
            {
                applicationDirectoryMappingMap.put(applicationId, new ArrayList<AppDirMappingDataHolder>());
            }
            applicationDirectoryMappingMap.get(applicationId).add(appDirMappingDataHolder);
        }

        public Map<Long, List<AppDirMappingDataHolder>> getApplicationDirectoryMappingMap()
        {
            return applicationDirectoryMappingMap;
        }
    }

    private class AppDirGroupTableMapper implements RowCallbackHandler
    {
        private Map<Long, List<AppDirMappingDataHolder>> applicationDirectoryMappings;

        public AppDirGroupTableMapper(Map<Long, List<AppDirMappingDataHolder>> applicationDirectoryMappings)
        {
            this.applicationDirectoryMappings = applicationDirectoryMappings;
        }

        public void processRow(ResultSet rs) throws SQLException
        {
            Long applicationId = rs.getLong("APPLICATIONID");
            Long oldDirectoryId = rs.getLong("DIRECTORYID");
            String name = rs.getString("NAME");

            List<AppDirMappingDataHolder> directoryMappings = applicationDirectoryMappings.get(applicationId);
            for (AppDirMappingDataHolder mapping : directoryMappings)
            {
                if (mapping.getOldDirectoryId().equals(oldDirectoryId))
                {
                    if (StringUtils.isNotBlank(name))
                    {
                        mapping.getAuthorisedGroupNames().add(name);
                    }
                }
            }
        }

        public Map<Long, List<AppDirMappingDataHolder>> getApplicationDirectoryMappings()
        {
            return applicationDirectoryMappings;
        }
    }

    private class AppDirOperationTableMapper implements RowCallbackHandler
    {
        private Map<Long, List<AppDirMappingDataHolder>> applicationDirectoryMappings;

        private AppDirOperationTableMapper(Map<Long, List<AppDirMappingDataHolder>> applicationDirectoryMappings)
        {
            this.applicationDirectoryMappings = applicationDirectoryMappings;
        }

        public void processRow(ResultSet rs) throws SQLException
        {
            Long applicationId = rs.getLong("APPLICATIONID");
            Long oldDirectoryId = rs.getLong("DIRECTORYID");
            String permissionType = rs.getString("PERMISSION_TYPE");
            OperationType operation = getOperationTypeFromLegacyPermissionName(permissionType);
            List<AppDirMappingDataHolder> directoryMappings = applicationDirectoryMappings.get(applicationId);
            for (AppDirMappingDataHolder mapping : directoryMappings)
            {
                if (mapping.getOldDirectoryId().equals(oldDirectoryId))
                {
                    if (operation != null)
                    {
                        mapping.getAllowedOperations().add(operation);
                    }
                }
            }
        }

        public Map<Long, List<AppDirMappingDataHolder>> getApplicationDirectoryMappings()
        {
            return applicationDirectoryMappings;
        }
    }

    private class AppDirMappingDataHolder
    {
        private Long oldDirectoryId;
        private boolean allowAllToAuthenticate;
        private Set<String> authorisedGroupNames = new HashSet<String>();
        private Set<OperationType> allowedOperations = new HashSet<OperationType>();

        private AppDirMappingDataHolder(Long oldDirectoryId)
        {
            this.oldDirectoryId = oldDirectoryId;
        }

        public void setAllowAllToAuthenticate(boolean allowAllToAuthenticate)
        {
            this.allowAllToAuthenticate = allowAllToAuthenticate;
        }

        public Set<OperationType> getAllowedOperations()
        {
            return allowedOperations;
        }

        public Set<String> getAuthorisedGroupNames()
        {
            return authorisedGroupNames;
        }

        public boolean isAllowAllToAuthenticate()
        {
            return allowAllToAuthenticate;
        }

        public Long getOldDirectoryId()
        {
            return oldDirectoryId;
        }
    }
}
