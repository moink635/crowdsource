package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import com.atlassian.crowd.search.query.entity.restriction.constants.UserTermKeys;
import com.atlassian.crowd.util.UserUtils;

import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This upgrade task will populate {@code displayName} and {@code lastName}
 * fields using {@code firstName}, {@code lastName} and {@code name} fields
 * for users with empty display name or empty last name in internal
 * directories.
 */
public class UpgradeTask426 implements UpgradeTask
{
    private static final int BATCH_SIZE = 20;

    private static final Logger log = LoggerFactory.getLogger(UpgradeTask426.class);

    private final Collection<String> errors = new ArrayList<String>();

    private DirectoryDao directoryDao;

    private UserDAOHibernate userDao;

    private SessionFactory sessionFactory;

    public String getBuildNumber()
    {
        return "426";
    }

    public String getShortDescription()
    {
        return "Updating users in internal directories to have non-empty display names and last names";
    }

    public void doUpgrade() throws Exception
    {
        checkNotNull(sessionFactory, "UpgradeTask426 must be injected a SessionFactory");

        for (Directory directory : findAllInternalDirectories())
        {
            log.debug("Updating users in directory {}", directory);
            try
            {
                updateDirectory(directory);
            }
            catch (DataAccessException e)
            {
                final String errorMessage = "Could not update users in directory " + directory;
                log.error(errorMessage, e);
                errors.add(errorMessage + ", error is " + e.getMessage());
            }
        }
    }

    private void updateDirectory(Directory directory) throws DataAccessException
    {
        /**
         * Flush previous changes, because we will soon clear the session.
         */
        sessionFactory.getCurrentSession().flush();
        final SearchRestriction displayNameOrLastNameEmptyRestriction = Combine.anyOf(
                Restriction.on(UserTermKeys.DISPLAY_NAME).exactlyMatching(""),
                Restriction.on(UserTermKeys.DISPLAY_NAME).isNull(),
                Restriction.on(UserTermKeys.LAST_NAME).exactlyMatching(""),
                Restriction.on(UserTermKeys.LAST_NAME).isNull());
        final List<User> users = userDao.search(directory.getId(), QueryBuilder.queryFor(User.class, EntityDescriptor.user()).with(displayNameOrLastNameEmptyRestriction).returningAtMost(EntityQuery.ALL_RESULTS));
        /**
         * The returned user objects can not be modified, so we might as
         * well drop them from the hibernate session. This helps keeping
         * the session size manageable.
         */
        sessionFactory.getCurrentSession().clear();

        log.debug("Users to be updated: {}", users.size());

        int counter = 0;
        for (User user : users)
        {
            // Updating will automatically populate user with a reasonable displayName
            try
            {
                userDao.update(UserUtils.populateNames(user));
            }
            catch (UserNotFoundException e)
            {
                throw new RuntimeException(e);
            }

            // Keep the hibernate session small
            ++counter;
            if (counter % BATCH_SIZE == 0)
            {
                sessionFactory.getCurrentSession().flush();
                sessionFactory.getCurrentSession().clear();
                if (log.isDebugEnabled())
                {
                    String percent = String.format("%.02f", (100.0 * counter) / users.size());
                    log.debug("Flushed {} ({}%)", counter, percent);
                }
            }
        }
    }

    private List<Directory> findAllInternalDirectories()
    {
        return directoryDao.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                .with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.INTERNAL))
                .returningAtMost(EntityQuery.ALL_RESULTS));
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }

    public void setUserDao(UserDAOHibernate userDao)
    {
        this.userDao = userDao;
    }

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }
}
