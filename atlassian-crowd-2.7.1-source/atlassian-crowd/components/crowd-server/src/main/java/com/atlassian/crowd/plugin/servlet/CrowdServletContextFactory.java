package com.atlassian.crowd.plugin.servlet;

import com.atlassian.plugin.servlet.ServletContextFactory;
import org.springframework.web.context.ServletContextAware;

import javax.servlet.ServletContext;

public class CrowdServletContextFactory implements ServletContextAware, ServletContextFactory
{
    private ServletContext servletContext;

    public void setServletContext(final ServletContext servletContext)
    {
        this.servletContext = servletContext;
    }

    public ServletContext getServletContext()
    {
        return servletContext;
    }
}
