package com.atlassian.crowd.service.soap;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.manager.license.CrowdLicenseManager;
import com.atlassian.crowd.manager.validation.ClientValidationException;
import com.atlassian.crowd.manager.validation.ClientValidationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.util.*;
import org.codehaus.xfire.transport.http.XFireServletController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

public class SOAPService
{
    private static final Logger logger = LoggerFactory.getLogger(SOAPService.class);

    private ApplicationManager applicationManager;
    private TokenAuthenticationManager tokenAuthenticationManager;
    private CrowdLicenseManager crowdLicenseManager;
    private I18nHelper i18nHelper;
    private ClientValidationManager clientValidationManager;

    public com.atlassian.crowd.integration.authentication.ValidationFactor[] getApplicationClientValidationFactors(String name)
    {
        HttpServletRequest request = getClientRequest();
        String remoteAddress = request.getRemoteAddr();
        String remoteHost = request.getRemoteHost();

        List<com.atlassian.crowd.integration.authentication.ValidationFactor> validationFactors = new ArrayList<com.atlassian.crowd.integration.authentication.ValidationFactor>();

        validationFactors.add(new com.atlassian.crowd.integration.authentication.ValidationFactor(ValidationFactor.REMOTE_ADDRESS, remoteAddress));
        validationFactors.add(new com.atlassian.crowd.integration.authentication.ValidationFactor(ValidationFactor.REMOTE_HOST, remoteHost)); // do we need this?
        validationFactors.add(new com.atlassian.crowd.integration.authentication.ValidationFactor(ValidationFactor.NAME, name));

        String remoteAddressXForwardFor = request.getHeader(ValidationFactor.X_FORWARDED_FOR);
        if (remoteAddressXForwardFor != null && !remoteAddressXForwardFor.equals(remoteAddress))
        {
            validationFactors.add(new com.atlassian.crowd.integration.authentication.ValidationFactor(ValidationFactor.X_FORWARDED_FOR, remoteAddressXForwardFor));
        }

        return validationFactors.toArray(new com.atlassian.crowd.integration.authentication.ValidationFactor[validationFactors.size()]);
    }

    public Application validateSOAPService(com.atlassian.crowd.integration.authentication.AuthenticatedToken applicationToken) throws RemoteException, InvalidAuthorizationTokenException
    {

        try
        {
            logger.debug("validating license key");
            validateLicense();

            logger.debug("validating application token: " + applicationToken.getToken());
            Token token = tokenAuthenticationManager.validateApplicationToken(applicationToken.getToken(), SoapObjectTranslator.fromSoapValidationFactors(getApplicationClientValidationFactors(applicationToken.getName())));

            logger.debug("loading application: " + token.getName());
            Application application = applicationManager.findByName(token.getName());

            clientValidationManager.validate(application, getClientRequest());

            return application;

        }
        catch (InvalidTokenException e)
        {
            throw new InvalidAuthorizationTokenException(i18nHelper.getText("soapservice.authenticationinvalid.exception"), e);
        }
        catch (ApplicationNotFoundException e)
        {
            throw new InvalidAuthorizationTokenException(i18nHelper.getText("soapservice.authenticationinvalid.exception"), e);
        }
        catch (ClientValidationException e)
        {
            throw new InvalidAuthorizationTokenException(e.getMessage());
        }
    }

    private HttpServletRequest getClientRequest()
    {
        return XFireServletController.getRequest();
    }

    protected void validateLicense() throws RemoteException
    {
        if (!crowdLicenseManager.isLicenseValid())
        {
            throw new RemoteException(i18nHelper.getText("soapservice.licenseinvalid.exception"));
        }
    }

    public void setCrowdLicenseManager(CrowdLicenseManager crowdLicenseManager)
    {
        this.crowdLicenseManager = crowdLicenseManager;
    }

    public void setApplicationManager(ApplicationManager applicationManager)
    {
        this.applicationManager = applicationManager;
    }

    public void setI18nHelper(I18nHelper i18nHelper)
    {
        this.i18nHelper = i18nHelper;
    }

    public void setTokenAuthenticationManager(TokenAuthenticationManager tokenAuthenticationManager)
    {
        this.tokenAuthenticationManager = tokenAuthenticationManager;
    }

    public void setClientValidationManager(ClientValidationManager clientValidationManager)
    {
        this.clientValidationManager = clientValidationManager;
    }
}