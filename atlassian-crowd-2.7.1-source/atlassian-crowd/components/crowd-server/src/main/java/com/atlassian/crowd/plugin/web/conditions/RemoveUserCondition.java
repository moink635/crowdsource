package com.atlassian.crowd.plugin.web.conditions;

import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Map;

/**
 * Stops displaying of any links where the username/directoryId matches that of the currently authenticated user.
 */
public class RemoveUserCondition implements Condition
{
    public void init(Map<String, String> params) throws PluginParseException
    {
    }

    public boolean shouldDisplay(Map<String, Object> context)
    {
        String username = String.valueOf(context.get("name"));
        long directoryId = NumberUtils.toLong(String.valueOf(context.get("directoryID")), 0L);

        // The current authenticated user
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        CrowdUserDetails userDetails = (CrowdUserDetails) auth.getPrincipal();

        return !(username.equals(userDetails.getUsername()) && directoryId == userDetails.getRemotePrincipal().getDirectoryId()) && !username.equals(auth.getName());
    }
}
