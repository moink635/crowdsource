package com.atlassian.crowd.console.embedded.api;

import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.embedded.api.ApplicationFactory;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.util.I18nHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

/**
 * This class has the responsibility of returning an instance of the 'Crowd'
 * application.
 *
 * @since 2.5
 */
public class CrowdApplicationFactory implements ApplicationFactory
{
    private final ApplicationDAO applicationDao;
    private ClientProperties clientProperties;

    public CrowdApplicationFactory(ApplicationDAO applicationDao, ClientProperties clientProperties)
    {
        this.applicationDao = applicationDao;
        this.clientProperties = clientProperties;
    }

    @Override
    public Application getApplication()
    {
        String applicationName = clientProperties.getApplicationName();
        try
        {
            return applicationDao.findByName(applicationName);
        }
        catch (ApplicationNotFoundException e)
        {
            throw new RuntimeException("Crowd application with name <" + applicationName + "> was not found in the database", e);
        }
    }
}
