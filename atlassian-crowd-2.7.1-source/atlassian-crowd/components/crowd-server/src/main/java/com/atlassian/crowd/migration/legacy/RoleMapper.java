package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.dao.membership.MembershipDAOHibernate;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.group.InternalGroupWithAttributes;
import com.atlassian.crowd.model.membership.InternalMembership;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchResultWithIdReferences;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RoleMapper extends GenericLegacyImporter implements LegacyImporter
{
    public static final String REMOTE_ROLE_XML_ROOT = "roles";
    public static final String REMOTE_ROLE_XML_PRINCIPAL_DIRECTORY_ID = "directoryId";
    public static final String REMOTE_ROLE_XML_DIRECTORY_ID = REMOTE_ROLE_XML_PRINCIPAL_DIRECTORY_ID;
    public static final String REMOTE_ROLE_XML_DESCRIPTION = "description";
    public static final String REMOTE_ROLE_XML_PRINCIPAL_NODE = "principals";
    public static final String REMOTE_ROLE_XML_PRINCIPAL = "principal";

    private final GroupDAOHibernate groupDAO;
    private final MembershipDAOHibernate membershipDAO;
    private final DirectoryDAOHibernate directoryDAO;

    public RoleMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, GroupDAOHibernate groupDAO, MembershipDAOHibernate membershipDAO, DirectoryDAOHibernate directoryDAO)
    {
        super(sessionFactory, batchProcessor);
        this.directoryDAO = directoryDAO;
        this.groupDAO = groupDAO;
        this.membershipDAO = membershipDAO;
    }

    public void importXml(final Element root, final LegacyImportDataHolder importData) throws ImportException
    {
        Element rolesElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + REMOTE_ROLE_XML_ROOT);
        if (rolesElement == null)
        {
            logger.error("No roles were found for importing!");
            return;
        }

        List<InternalGroupWithAttributes> roles = new ArrayList<InternalGroupWithAttributes>();
        for (Iterator remoteRoles = rolesElement.elementIterator(); remoteRoles.hasNext();)
        {
            Element roleElement = (Element) remoteRoles.next();

            InternalGroupWithAttributes role = getRoleAndAttributesFromXml(roleElement, importData.getOldToNewDirectoryIds());

            roles.add(role);
        }

        BatchResultWithIdReferences<Group> roleImportResults = groupDAO.addAll(roles);
        for (Group role : roleImportResults.getFailedEntities())
        {
            logger.error("Unable to add role <" + role.getName() + "> in directory with id <" + role.getDirectoryId() + ">");
        }

        // die hard if there are any errors
        if (roleImportResults.hasFailures())
        {
            throw new ImportException("Unable to import all roles. See logs for more details.");
        }

        // now import memberships (role by role)
        for (Iterator remoteRoles = rolesElement.elementIterator(); remoteRoles.hasNext();)
        {
            Element roleElement = (Element) remoteRoles.next();
            Set<InternalMembership> memberships = getMemberships(roleElement, importData, roleImportResults);

            BatchResult<InternalMembership> membershipResult = membershipDAO.addAll(memberships);

            for (InternalMembership membership : membershipResult.getFailedEntities())
            {
                logger.error("Unable to add user <" + membership.getChildName() + "> to role <" + membership.getParentName() + "> in directory with id <" + membership.getDirectory().getId() + ">");
            }

            if (membershipResult.hasFailures())
            {
                throw new ImportException("Unable to import all memberships. See logs for more details.");
            }
        }
    }

    protected InternalGroupWithAttributes getRoleAndAttributesFromXml(Element roleElement, Map<Long, Long> oldToNewDirectoryIds)
    {
        // pull directoryId
        Long oldDirectoryId = Long.parseLong(roleElement.element(REMOTE_ROLE_XML_DIRECTORY_ID).getText());
        Long directoryId = oldToNewDirectoryIds.get(oldDirectoryId);
        if (directoryId == null)
        {
            throw new IllegalArgumentException("Role belongs to an unknown old directory with ID: " + directoryId);
        }
        DirectoryImpl directory = (DirectoryImpl) directoryDAO.loadReference(directoryId);

        // pull all the default values
        InternalEntityTemplate internalEntityTemplate = getInternalEntityTemplateFromLegacyXml(roleElement);
        internalEntityTemplate.setName(internalEntityTemplate.getName());
        internalEntityTemplate.setId(null);

        GroupTemplate roleTemplate = new GroupTemplate(internalEntityTemplate.getName(), directory.getId(), GroupType.LEGACY_ROLE);
        roleTemplate.setActive(internalEntityTemplate.isActive());
        String description = "";
        Element descriptionElement = roleElement.element(REMOTE_ROLE_XML_DESCRIPTION);
        if (descriptionElement != null && descriptionElement.hasContent())
        {
            description = descriptionElement.getText();
        }
        roleTemplate.setDescription(description);

        // save the role
        InternalGroup role = new InternalGroup(internalEntityTemplate, directory, roleTemplate);

        Map<String, Set<String>> attributes = getMultiValuedAttributesMapFromXml(roleElement);

        return new InternalGroupWithAttributes(role, attributes);
    }

    protected Set<InternalMembership> getMemberships(Element roleElement, LegacyImportDataHolder importData, BatchResultWithIdReferences<Group> roleImportResults)
    {
        String roleName = roleElement.element(GENERIC_XML_NAME).getText();
        Long oldDirectoryId = Long.parseLong(roleElement.element(REMOTE_ROLE_XML_DIRECTORY_ID).getText());
        Long directoryId = importData.getOldToNewDirectoryIds().get(oldDirectoryId);
        if (directoryId == null)
        {
            throw new IllegalArgumentException("Role belongs to an unknown old directory with ID: " + directoryId);
        }

        DirectoryImpl directory = (DirectoryImpl) directoryDAO.loadReference(directoryId);
        Long roleId = roleImportResults.getIdReference(directoryId, roleName);

        Set<InternalMembership> memberships = new HashSet<InternalMembership>();

        Element membersElement = roleElement.element(REMOTE_ROLE_XML_PRINCIPAL_NODE);

        if (membersElement != null && membersElement.hasContent())
        {
            for (Iterator iterator = membersElement.elementIterator(REMOTE_ROLE_XML_PRINCIPAL); iterator.hasNext();)
            {
                Element memberElement = (Element) iterator.next();
                String memberName = memberElement.attributeValue(GENERIC_XML_NAME);
                Long userId = importData.getUserImportResults().getIdReference(directoryId, memberName);

                InternalMembership membership = new InternalMembership(null, roleId, userId, MembershipType.GROUP_USER, GroupType.LEGACY_ROLE, roleName, memberName, directory);
                memberships.add(membership);
            }
        }

        return memberships;
    }
}
