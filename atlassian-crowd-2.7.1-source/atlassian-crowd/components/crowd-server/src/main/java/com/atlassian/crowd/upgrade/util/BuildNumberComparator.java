package com.atlassian.crowd.upgrade.util;

import java.util.Comparator;

/**
 * A comparator for Crowd build numbers.
 */
public class BuildNumberComparator implements Comparator
{
    /**
     * Compares two string representation of build numbers.
     * <p/>
     * These strings must be parsable to a number.
     *
     * @param o1
     * @param o2
     * @return Underlying number comparison notation - 1 = greater than, 0 = equals, -1 = less than.
     */
    public int compare(Object o1, Object o2)
    {
        Integer number1 = Integer.valueOf((String) o1);
        Integer number2 = Integer.valueOf((String) o2);

        return number1.compareTo(number2);
    }

}
