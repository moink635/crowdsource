package com.atlassian.crowd.importer.importers;

import java.util.Collection;
import java.util.List;

import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.config.JdbcConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.model.MembershipDTO;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;

import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * General JDBC class that handles the fetching of users, groups and memberships.
 * Subclasses of this class implement Mappers and SQL to retrieve users, groups and memberships
 */
abstract class JdbcImporter extends BaseImporter implements Importer
{
    protected JdbcOperations jdbcTemplate;

    public JdbcImporter(DirectoryManager directoryManager)
    {
        super(directoryManager);
    }

    public abstract String getSelectAllUserGroupMembershipsSQL();

    public abstract RowMapper getMembershipMapper();

    public abstract String getSelectAllGroupsSQL();

    public abstract RowMapper getGroupMapper(Configuration configuration);

    public abstract String getSelectAllUsersSQL();

    public abstract RowMapper getUserMapper(Configuration configuration);

    /**
     * @param configuration the configuration that contains the directory to import the groups too
     * @return int the number of groups imported
     */
    @Override
    public Collection<GroupTemplate> findGroups(final Configuration configuration)
    {
        return jdbcTemplate.query(getSelectAllGroupsSQL(), getGroupMapper(configuration));
    }

    @Override
    public List<MembershipDTO> findUserToGroupMemberships(final Configuration configuration)
    {
        return jdbcTemplate.query(getSelectAllUserGroupMembershipsSQL(), getMembershipMapper());
    }

    @Override
    public Collection<MembershipDTO> findGroupToGroupMemberships(Configuration configuration) throws ImporterException
    {
        throw new UnsupportedOperationException("Nested group not supported yet for jdbc-based imports");
    }

    @Override
    public List<UserTemplateWithCredentialAndAttributes> findUsers(final Configuration configuration)
    {
        return jdbcTemplate.query(getSelectAllUsersSQL(), getUserMapper(configuration));
    }


    @Override
    public Class getConfigurationType()
    {
        return JdbcConfiguration.class;
    }

    @Override
    public void init(final Configuration configuration)
    {
        // Setup the datasource and JDBC template for the given configuration
        setJdbcConfiguration((JdbcConfiguration) configuration);
    }

    private void setJdbcConfiguration(JdbcConfiguration configuration)
    {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource(configuration.getDatabaseDriver(), configuration.getDatabaseURL(), configuration.getUsername(), configuration.getPassword());

        jdbcTemplate = new JdbcTemplate(driverManagerDataSource);
    }

    public void setJdbcTemplate(JdbcOperations jdbcTemplate)
    {
        this.jdbcTemplate = jdbcTemplate;
    }
}
