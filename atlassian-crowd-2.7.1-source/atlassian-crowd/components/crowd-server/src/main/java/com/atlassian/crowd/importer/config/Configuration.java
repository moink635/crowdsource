package com.atlassian.crowd.importer.config;

import com.atlassian.crowd.importer.exceptions.ImporterConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * This class represents the properties required to connect with a database,
 * specifically the details for a JIRA, Confluence or Bamboo database.
 */
public class Configuration
{
    /**
     * Override any data in the target directory from what is found in the source,
     */
    protected Boolean overwriteTarget;
    
    /**
     * The target directory to import users/groups/memberships too
     */
    private Long directoryID;

    private String application;

    /**
     * Are we importing passwords, or generating random ones.
     */
    private Boolean importPasswords;

    /**
     * Are we also importing nested groups?
     * This would in general be determined by whether source and destination both support nested groups.
     */
    private Boolean importNestedGroups;

    public Configuration()
    {
        // sensible defaults (it makes no sense for these to be null,
        // they should really be primitive types - maybe in a refactor)
        this.overwriteTarget = Boolean.FALSE;
        this.importPasswords = Boolean.TRUE;
        this.importNestedGroups = Boolean.FALSE;
    }

    /**
     *
     * @param directoryID the target directory to import users/groups/memberships too
     * @param application the name of the application, eg. 'csv', 'directory', 'jira' etc.
     * @param importPasswords whether or not passwords should be imported
     * @param overwriteTarget overwrite any current objects in the target directory
     */
    public Configuration(Long directoryID, String application, Boolean importPasswords, Boolean overwriteTarget)
    {
        this.directoryID = directoryID;
        this.application = application;
        this.importPasswords = importPasswords;
        this.overwriteTarget = overwriteTarget;
        this.importNestedGroups = Boolean.FALSE;
    }

    public String getApplication()
    {
        return application;
    }

    public void setApplication(String application)
    {
        this.application = application;
    }

    public Long getDirectoryID()
    {
        return directoryID;
    }

    public void setDirectoryID(Long directoryID)
    {
        this.directoryID = directoryID;
    }

    public Boolean isOverwriteTarget()
    {
        return overwriteTarget;
    }

    public Boolean isImportPasswords()
    {
        return importPasswords;
    }

    public void setImportPasswords(Boolean importPasswords)
    {
        this.importPasswords = importPasswords;
    }

    public Boolean getImportNestedGroups()
    {
        return importNestedGroups;
    }

    public void setImportNestedGroups(Boolean importNestedGroups)
    {
        this.importNestedGroups = importNestedGroups;
    }

    /**
     * Will make sure that all required properties have been set on the Configuration
     * object to perform an import.
     * Override this method if you requirs specific validation of your Configuration
     *
     * @throws com.atlassian.crowd.importer.exceptions.ImporterConfigurationException
     *          if there is a problem validating the configuration
     */
    public void isValid() throws ImporterConfigurationException
    {
        if(StringUtils.isBlank(application) || directoryID == null ||
                importPasswords == null)
        {
            throw new ImporterConfigurationException("A required Configuration value was null: " + this);
        }
    }

    public void setOverwriteTarget(Boolean overwriteTarget)
    {
        if (overwriteTarget == null)
        {
            this.overwriteTarget = Boolean.FALSE;
        }
        else
        {
            this.overwriteTarget = overwriteTarget;
        }
    }

    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Configuration that = (Configuration) o;

        if (application != null ? !application.equals(that.application) : that.application != null)
        {
            return false;
        }
        if (directoryID != null ? !directoryID.equals(that.directoryID) : that.directoryID != null)
        {
            return false;
        }
        if (importPasswords != null ? !importPasswords.equals(that.importPasswords) : that.importPasswords != null)
        {
            return false;
        }

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (directoryID != null ? directoryID.hashCode() : 0);
        result = 31 * result + (application != null ? application.hashCode() : 0);
        result = 31 * result + (importPasswords != null ? importPasswords.hashCode() : 0);
        return result;
    }

    public String toString()
    {
        return new ToStringBuilder(this).append("Directory ID", directoryID).append("Application", application).append("ImportPasswords", importPasswords).toString();
    }
}
