package com.atlassian.crowd.manager.threadlocal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.plugin.web.ExecutingHttpRequest;

import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionContext;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Provide access to the state of the application thread locals.
 * <p>
 * Currently handles:
 * <ul>
 *     <li>Spring Security SecurityContext</li>
 *     <li>WebWorks ActionContext</li>
 *     <li>the current HTTP request and response</li>
 * </ul>
 *
 * @since v2.7
 */
public class CrowdThreadLocalStateAccessor implements ThreadLocalStateAccessor
{
    @Override
    public ThreadLocalState getState()
    {
        final SecurityContext context = SecurityContextHolder.getContext();
        final ActionContext actionContext = ServletActionContext.getContext();
        final HttpServletRequest request = ExecutingHttpRequest.get();
        final HttpServletResponse response = ExecutingHttpRequest.getResponse();

        return new ThreadLocalState(context, actionContext, request, response);
    }

    @Override
    public void setState(final ThreadLocalState state)
    {
        SecurityContextHolder.setContext(state.getSecurityContext());
        ServletActionContext.setContext(state.getActionContext());
        ExecutingHttpRequest.set(state.getRequest(), state.getResponse());
    }

    @Override
    public void clearState()
    {
        SecurityContextHolder.clearContext();
        ServletActionContext.setContext(null);
        ExecutingHttpRequest.clear();
    }
}
