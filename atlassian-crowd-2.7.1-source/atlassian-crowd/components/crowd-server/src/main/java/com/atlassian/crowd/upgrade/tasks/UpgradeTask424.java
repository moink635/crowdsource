package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.*;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import org.apache.commons.lang3.math.NumberUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * All LDAP directories now have caching enabled. Removing unnecessary attributes and adding directory.cache.synchronise.interval
 */
public class UpgradeTask424 implements UpgradeTask
{
    private DirectoryManager directoryManager;

    private final Collection<String> errors = new ArrayList<String>();

    public static final String DEFAULT_SYNC_INTERVAL = "3600";
    private static final int MINIMUM_SYNC_INTERVAL = 600;

    // These Caching Attribute constants used to live in DirectoryImpl, but is not used in production any more
    public static final String ATTRIBUTE_KEY_USE_CACHING = "useCaching";
    public static final String ATTRIBUTE_KEY_USE_MONITORING = "useMonitoring";
    public static final String ATTRIBUTE_KEY_CACHE_CLASS = "cacheClass";
    public static final String ATTRIBUTE_KEY_CACHE_MAX_ELEMENTS_IN_MEMORY = "cacheMaxElements";
    public static final String ATTRIBUTE_KEY_POLLING_INTERVAL = "pollingInterval";

    public String getBuildNumber()
    {
        return "424";
    }

    public String getShortDescription()
    {
        return "All LDAP directories now have caching enabled. Removing unnecessary attributes and adding directory.cache.synchronise.interval";
    }

    public void doUpgrade() throws DirectoryNotFoundException
    {
        for (Directory directory : findAllConnectorAndDelegatingDirectories())
        {
            DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);

            // Update the sync interval if it is a connector directory
            if (directoryToUpdate.getType().equals(DirectoryType.CONNECTOR))
            {
                updateCacheSyncIntervalAttribute(directoryToUpdate);
            }
            
            // Remove the old unnecessary attributes
            removeOldAttributes(directoryToUpdate);

            // Update the directory
            directoryManager.updateDirectory(directoryToUpdate);
        }
    }

    private void updateCacheSyncIntervalAttribute(DirectoryImpl directoryToUpdate)
    {
        // Retrieve the pollingInterval and transfer it to CACHE_SYNCHRONISE_INTERVAL
        // If pollingInterval does not exist or is less than minimum interval, set it to the default
        int syncInterval = NumberUtils.toInt(directoryToUpdate.getValue(ATTRIBUTE_KEY_POLLING_INTERVAL));
        if (syncInterval < MINIMUM_SYNC_INTERVAL)
        {
            directoryToUpdate.setAttribute(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL, DEFAULT_SYNC_INTERVAL);
        }
        else
        {
            directoryToUpdate.setAttribute(SynchronisableDirectoryProperties.CACHE_SYNCHRONISE_INTERVAL, String.valueOf(syncInterval));
        }
    }

    private void removeOldAttributes(DirectoryImpl directoryToUpdate)
    {
        directoryToUpdate.removeAttribute(ATTRIBUTE_KEY_USE_CACHING);
        directoryToUpdate.removeAttribute(ATTRIBUTE_KEY_USE_MONITORING);
        directoryToUpdate.removeAttribute(ATTRIBUTE_KEY_CACHE_CLASS);
        directoryToUpdate.removeAttribute(ATTRIBUTE_KEY_CACHE_MAX_ELEMENTS_IN_MEMORY);
        directoryToUpdate.removeAttribute(ATTRIBUTE_KEY_POLLING_INTERVAL); // now replaced with CACHE_SYNCHRONISE_INTERVAL
    }

    private List<Directory> findAllConnectorAndDelegatingDirectories()
    {
        // For some reason delegating directories appear to have cache attributes as well...these should not even be there (so we'll remove them too)
        return directoryManager.searchDirectories(
                QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                        .with(Combine.anyOf(
                                Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.CONNECTOR),
                                Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.DELEGATING)))
                        .returningAtMost(EntityQuery.ALL_RESULTS));
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }
}
