package com.atlassian.crowd.event.listener;

import com.atlassian.crowd.event.EventJobExecutionException;
import com.atlassian.crowd.event.login.RequestUsernamesEvent;
import com.atlassian.crowd.exception.InvalidEmailAddressException;
import com.atlassian.crowd.manager.login.util.ForgottenLoginMailer;
import com.atlassian.crowd.manager.mail.MailSendException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listener responsible for handling {@link com.atlassian.crowd.event.login.RequestUsernamesEvent}'s
 * by sending an email to the user, letting them know their new password.
 */
public class RequestUsernamesListener
{
    private ForgottenLoginMailer forgottenLoginMailer;

    @com.atlassian.event.api.EventListener
    public void handleEvent(RequestUsernamesEvent requestUsernamesEvent)
    {
        try
        {
            forgottenLoginMailer.mailUsernames(requestUsernamesEvent.getUser(), requestUsernamesEvent.getUsernames());
        }
        catch (InvalidEmailAddressException e)
        {
            throw new EventJobExecutionException(e.getMessage(), e);
        }
        catch (MailSendException e)
        {
            throw new EventJobExecutionException(e.getMessage(), e);
        }
    }

    public void setForgottenLoginMailer(final ForgottenLoginMailer forgottenLoginMailer)
    {
        this.forgottenLoginMailer = checkNotNull(forgottenLoginMailer);
    }
}
