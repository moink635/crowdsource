package com.atlassian.crowd.migration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import com.atlassian.crowd.CrowdConstants;
import com.atlassian.crowd.event.migration.XMLRestoreFinishedEvent;
import com.atlassian.crowd.event.migration.XMLRestoreStartedEvent;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManagerException;
import com.atlassian.crowd.manager.application.CrowdApplicationPasswordManager;
import com.atlassian.crowd.manager.upgrade.UpgradeManager;
import com.atlassian.crowd.migration.legacy.LegacyXmlMigrator;
import com.atlassian.crowd.migration.verify.VerificationManager;
import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.build.BuildUtils;
import com.atlassian.hibernate.extras.ResetableHiLoGeneratorHelper;
import com.atlassian.hibernate.extras.ResettableTableHiLoGenerator;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.johnson.Johnson;
import com.atlassian.johnson.JohnsonEventContainer;
import com.atlassian.johnson.config.JohnsonConfig;
import com.atlassian.johnson.event.Event;
import com.atlassian.spring.container.ContainerManager;

import org.apache.commons.io.IOUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.jdbc.Work;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.web.context.ServletContextAware;

import net.sf.ehcache.CacheManager;

public class XmlMigrationManagerImpl implements XmlMigrationManager, ServletContextAware
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    // Injected properties via Spring.
    private SessionFactory sessionFactory;
    private XmlMigrator xmlMigrator;
    private LegacyXmlMigrator legacyXmlMigrator;
    private UpgradeManager upgradeManager;
    private ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper;
    private EventPublisher eventPublisher;
    private CrowdApplicationPasswordManager crowdApplicationPasswordManager;
    private CacheManager cacheManager;
    private ServletContext servletContext;
    private I18nHelper i18nHelper;
    private VerificationManager verificationManager;

    // XML node attributes
    public static final String XML_ROOT = "crowd";
    public static final String CROWD_XML_VERSION = "version";
    public static final String CROWD_XML_DATE = "creationDate";
    public static final String CROWD_XML_BUILD_NUMBER = "buildNumber";
    public static final String CROWD_XML_BUILD_DATE = "buildDate";

    public static final String OPTION_RESET_DOMAIN = "OPTION_RESET_DOMAIN";


    public synchronized long exportXml(String path, Map options) throws ExportException
    {
        long startTime = System.currentTimeMillis();

        // create xml for data in hibernate persistence
        Document document = DocumentHelper.createDocument();
        Element root = document.addElement(XML_ROOT);

        // add the high level root elements that specify just what Crowd version we are exporting.
        root.addElement(CROWD_XML_DATE).addText((new Date()).toString());
        root.addElement(CROWD_XML_VERSION).addText(BuildUtils.BUILD_VERSION);
        root.addElement(CROWD_XML_BUILD_NUMBER).addText(BuildUtils.BUILD_NUMBER);
        root.addElement(CROWD_XML_BUILD_DATE).addText(BuildUtils.BUILD_DATE);

        // export to Crowd 2.0
        xmlMigrator.exportXml(root, options);

        // based off the path provided create the file and the directory structure if needed.
        File exportFile = new File(path);
        if (exportFile.getParentFile() != null)
        {
            exportFile.getParentFile().mkdirs();
        }

        // Write the XML doc out the file.
        OutputStreamWriter xmlOut = null;
        XMLWriter writer = null;
        try
        {
            xmlOut = new OutputStreamWriter(new FileOutputStream(exportFile), CrowdConstants.DEFAULT_CHARACTER_ENCODING);
            writer = new XMLWriter(xmlOut, OutputFormat.createPrettyPrint());
            writer.write(document);
            writer.close();
            xmlOut.close();
        }
        catch (IOException e)
        {
            throw new ExportException(e);
        }
        finally
        {
            IOUtils.closeQuietly(xmlOut);
            try
            {
                if (writer != null)
                {
                    writer.close();
                }
            }
            catch (IOException e)
            {
                // meh
            }
        }

        long timeTaken = System.currentTimeMillis() - startTime;
        logger.info("Time taken to export (millis): " + timeTaken);
        return timeTaken;
    }

    public synchronized long importXml(String path) throws ImportException
    {
        long startTime = System.currentTimeMillis();

        // check that the File provided on the path exists
        File importFile = new File(path);
        if (!importFile.exists())
        {
            logger.warn("The import file does not exist. Import halted.");
            throw new ImportException("The file specified: " + path + " does not exist");
        }

        eventPublisher.publish(new XMLRestoreStartedEvent(this));

        // load the xml document
        SAXReader reader = new SAXReader();
        Document document = null;
        try
        {
            document = reader.read(new FileInputStream(importFile));
        }
        catch (DocumentException e)
        {
            throw new ImportException(e);
        }
        catch (FileNotFoundException e)
        {
            throw new ImportException(e);
        }

        Element root = document.getRootElement();

        verificationManager.validate(document);

        // flush and drop the database
        cleanDatabase();
        clearCaches();

        boolean legacyImport = isLegacyImport(root);

        // perform the appropriate import
        if (legacyImport)
        {
            // do a legacy import
            legacyXmlMigrator.importXml(root);
        }
        else
        {
            // do a 2.0+ import
            xmlMigrator.importXml(root);
        }

        // Now reset all the identifiers
        resetIdentifierGenerators();

        // flush hibernate session
        flushAndClearHibernateSession();

        clearCaches();

        // reset crowd password if required
        try
        {
            crowdApplicationPasswordManager.resetCrowdPasswordIfRequired();
        }
        catch (ObjectNotFoundException e)
        {
            throw new ImportException(e);
        }
        catch (ApplicationManagerException e)
        {
            throw new ImportException(e);
        }

        // And now run all upgrade tasks
        boolean notInError = performUpgrades();

        if (notInError)
        {
            eventPublisher.publish(new XMLRestoreFinishedEvent(this));
        }


        long timeTaken = System.currentTimeMillis() - startTime;
        logger.info("Time taken to import (millis): " + timeTaken);
        return timeTaken;
    }

    private boolean isLegacyImport(Element root)
    {
        Element versionElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + XmlMigrationManagerImpl.CROWD_XML_VERSION);
        String version = versionElement.getText();

        return version.startsWith("0.") || version.startsWith("1.");
    }

    /**
     * @return if any errors occured return false
     */
    boolean performUpgrades()
    {
        boolean notInError = true;
        Exception upgradeException = null;
        Collection<String> errors;
        try
        {
            errors = upgradeManager.doUpgrade();
        }
        catch (Exception e)
        {
            upgradeException = e;
            errors = Collections.emptyList();
        }

        if (upgradeException != null || !errors.isEmpty())
        {
            JohnsonEventContainer agentJohnson = Johnson.getEventContainer(servletContext);
            JohnsonConfig johnsonConfig = Johnson.getConfig();

            String errorMessage = i18nHelper.getText("xmlmigration.import.error");

            if (upgradeException != null)
            {
                agentJohnson.addEvent(new Event(johnsonConfig.getEventType("import"),
                                                errorMessage, upgradeException.getMessage(),
                                                johnsonConfig.getEventLevel("fatal")));
            }

            for (String error : errors)
            {
                agentJohnson.addEvent(new Event(johnsonConfig.getEventType("import"),
                                                errorMessage, error,
                                                johnsonConfig.getEventLevel("fatal")));
            }

            notInError = false;
        }

        return notInError;
    }

    protected void clearCaches()
    {
        cacheManager.clearAll();
    }

    protected void flushAndClearHibernateSession()
    {
        sessionFactory.getCurrentSession().flush();
        sessionFactory.getCurrentSession().clear();
    }

    protected void cleanDatabase()
    {
        // flush the session before we continue
        Session s = sessionFactory.getCurrentSession();
        if (s != null)
        {
            try
            {
                s.flush();
                commitUnderlyingConnection(s);
            }
            catch (HibernateException he)
            {
                logger.error("error flushing session or committing the transaction", he);
            }
        }

        // drop and re-create the schema
        deleteDatabase();

        if (s != null)
        {
            try
            {
                s.flush();
                commitUnderlyingConnection(s);
                s.clear();
            }
            catch (HibernateException he)
            {
                logger.error("error flushing session or committing the transaction", he);
            }
        }
    }

    private void commitUnderlyingConnection(Session session) throws HibernateException
    {
        session.doWork(new Work()
        {
            @Override
            public void execute(Connection connection) throws SQLException
            {
                connection.commit();
            }
        });
    }

    /**
     * This will drop and re-create the crowd Schema
     *
     * @throws HibernateException
     */
    private void deleteDatabase() throws HibernateException
    {
        LocalSessionFactoryBean sessionFactory = (LocalSessionFactoryBean) ContainerManager.getComponent("&sessionFactory");
        if (sessionFactory != null)
        {
            // consider using SchemaHelper instead
            SchemaExport schemaExport = new SchemaExport(sessionFactory.getConfiguration());

            schemaExport.drop(false, true);

            schemaExport.create(false, true);
        }
    }

    /**
     * Reset Hibernate incremental ID generators so we don't get ID collisions after a
     * restore.
     *
     * @throws ImportException if some hibernate error prevents us doing the reset
     */
    protected void resetIdentifierGenerators() throws ImportException
    {
        Map metadata = sessionFactory.getAllClassMetadata();
        for (Iterator it = metadata.keySet().iterator(); it.hasNext();)
        {
            EntityPersister persister = ((SessionFactoryImplementor) sessionFactory).getEntityPersister((String) it.next());
            IdentifierGenerator idGen = persister.getIdentifierGenerator();
            if (idGen instanceof ResettableTableHiLoGenerator)
            {
                ((ResettableTableHiLoGenerator) idGen).reset();
            }
        }

        // update the hi-lo table value
        List errors = new ArrayList();
        try
        {
            resetableHiLoGeneratorHelper.setNextHiValue(errors);
        }
        catch (Throwable e)
        {
            logger.error("Error adjusting the hibernat_unique_key.next_hi value", e);
        }

        if (!errors.isEmpty())
        {
            // Some type of exception was thrown from the helper
            StringBuffer errorMessages = new StringBuffer();
            errorMessages.append("The following errors were thrown: \n");
            for (Iterator iterator = errors.iterator(); iterator.hasNext();)
            {
                Object error = iterator.next();
                errorMessages.append(error.toString()).append("\n");
            }
            throw new ImportException(errorMessages.toString());
        }
    }

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    public void setCacheManager(CacheManager cacheManager)
    {
        this.cacheManager = cacheManager;
    }

    public void setResetableHiLoGeneratorHelper(ResetableHiLoGeneratorHelper resetableHiLoGeneratorHelper)
    {
        this.resetableHiLoGeneratorHelper = resetableHiLoGeneratorHelper;
    }

    public void setUpgradeManager(UpgradeManager upgradeManager)
    {
        this.upgradeManager = upgradeManager;
    }

    public void setEventPublisher(EventPublisher eventPublisher)
    {
        this.eventPublisher = eventPublisher;
    }

    public void setXmlMigrator(final XmlMigrator xmlMigrator)
    {
        this.xmlMigrator = xmlMigrator;
    }

    public void setLegacyXmlMigrator(final LegacyXmlMigrator legacyXmlMigrator)
    {
        this.legacyXmlMigrator = legacyXmlMigrator;
    }

    public void setCrowdApplicationPasswordManager(final CrowdApplicationPasswordManager crowdApplicationPasswordManager)
    {
        this.crowdApplicationPasswordManager = crowdApplicationPasswordManager;
    }

    public void setServletContext(ServletContext servletContext)
    {
        this.servletContext = servletContext;
    }

    public void setI18nHelper(I18nHelper i18nHelper)
    {
        this.i18nHelper = i18nHelper;
    }

    public void setVerificationManager(VerificationManager verificationManager)
    {
        this.verificationManager = verificationManager;
    }
}
