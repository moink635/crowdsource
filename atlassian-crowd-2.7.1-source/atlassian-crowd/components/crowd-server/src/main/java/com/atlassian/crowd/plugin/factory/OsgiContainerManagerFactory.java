package com.atlassian.crowd.plugin.factory;

import com.atlassian.crowd.plugin.CrowdOsgiContainerManager;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.osgi.container.OsgiContainerManager;
import com.atlassian.plugin.osgi.container.OsgiPersistentCache;
import com.atlassian.plugin.osgi.container.PackageScannerConfiguration;
import com.atlassian.plugin.osgi.hostcomponents.HostComponentProvider;
import org.springframework.beans.factory.FactoryBean;

public class OsgiContainerManagerFactory implements FactoryBean
{
    private final PluginEventManager pluginEventManager;
    private final PackageScannerConfiguration packageScannerConfiguration;
    private final HostComponentProvider provider;
    private final OsgiPersistentCache osgiPersistentCache;

    public OsgiContainerManagerFactory(PackageScannerConfiguration packageScannerConfiguration,
                                       HostComponentProvider provider, PluginEventManager pluginEventManager,
                                       OsgiPersistentCache osgiPersistentCache)
    {
        this.packageScannerConfiguration = packageScannerConfiguration;
        this.provider = provider;
        this.pluginEventManager = pluginEventManager;
        this.osgiPersistentCache = osgiPersistentCache;
    }

    public Object getObject() throws Exception
    {
        return new CrowdOsgiContainerManager(osgiPersistentCache,
                packageScannerConfiguration, provider, pluginEventManager);
    }

    public Class getObjectType()
    {
        return OsgiContainerManager.class;
    }

    public boolean isSingleton()
    {
        return true;
    }
}