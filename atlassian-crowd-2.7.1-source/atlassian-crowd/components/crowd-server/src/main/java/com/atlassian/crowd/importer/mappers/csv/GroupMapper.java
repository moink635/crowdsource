package com.atlassian.crowd.importer.mappers.csv;

import com.atlassian.crowd.importer.config.CsvConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;

import org.apache.commons.collections.OrderedBidiMap;

/**
 * GroupMapper that will map a row of data, eg. "jira-admin" to a RemoteGroup object
 */
public class GroupMapper extends CsvMapper<GroupTemplate>
{
    public GroupMapper(Long directoryId, OrderedBidiMap configuration)
    {
        super(directoryId, configuration);
    }

    public GroupTemplate mapRow(String[] resultSet) throws ImporterException
    {
        GroupTemplate group = null;
        if (resultSet != null)
        {
            String groupName = getString(resultSet, CsvConfiguration.GROUP_NAME);
            group = new GroupTemplate(groupName, directoryId, GroupType.GROUP);
            group.setActive(true);
            group.setDescription(groupName);
        }

        return group;
    }
}
