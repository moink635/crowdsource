package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.model.property.Property;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class will remove the cache time from the database if it exists.
 * This was removed as a property since we have moved to a configurable EHCache file
 * in our clients and server.
 */
public class UpgradeTask002 implements UpgradeTask
{
    private List errors = new ArrayList();

    private static final Logger log = LoggerFactory.getLogger(UpgradeTask002.class);

    private static final String BUILD_NUMBER = "2";

    // Spring managed dependency, this is auto-wired by the UpgradeManager
    private PropertyManager propertyManager;

    public String getBuildNumber()
    {
        return BUILD_NUMBER;
    }

    public String getShortDescription()
    {
        return "Removing cache time property from the database";
    }

    public void doUpgrade() throws Exception
    {
        // Try and remove the caching time property from the datastore since this is no longer used by Crowd
        try
        {
            // remove it
            propertyManager.removeProperty(Property.CACHE_TIME);
        }
        catch (Exception e)
        {
            // This could be a DataAccessException
            errors.add("An error occurred trying to remove the CACHE_TIME property, please look at the logs for more details");
            log.error("An error occurred trying to remove the CACHE_TIME property", e);
        }

    }

    public Collection getErrors()
    {
        return errors;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}
