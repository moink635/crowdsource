package com.atlassian.crowd.upgrade.util;

import org.hibernate.SessionFactory;

public class UpgradeUtilityDAOHibernate
{
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    public int executeUpdate(final String query)
    {
        return sessionFactory.getCurrentSession().createQuery(query).executeUpdate();
    }

    /**
     * Executes an update operation affecting multiple entities. This method flushes the session after performing
     * the update.
     *
     * @param query a HQL query with exactly one JPA-style parameter placeholder, that is, ?1
     * @param parameter parameter to the query (required)
     * @return the number of entities affected by this update operation
     */
    public int executeBulkUpdate(String query, Object parameter)
    {
        int affectedEntities = sessionFactory.getCurrentSession().createQuery(query)
            .setParameter("1", parameter).executeUpdate();
        sessionFactory.getCurrentSession().flush();
        return affectedEntities;
    }
}
