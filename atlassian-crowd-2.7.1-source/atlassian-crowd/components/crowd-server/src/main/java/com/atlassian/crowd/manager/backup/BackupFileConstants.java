package com.atlassian.crowd.manager.backup;

import java.util.regex.Pattern;

/**
 * Various constants related to backup file names
 *
 * @since v2.7
 */
public interface BackupFileConstants
{
    String TIMESTAMP_FORMAT = "yyyy-MM-dd-HHmmss";
    String DIRECTORIES_CONF_FILE_NAME = "directories.properties";
    String MANUAL_BACKUP_FILENAME_FORMAT = "atlassian-crowd-%s-backup-%s.xml";
    // Keep the following 2 in sync
    String AUTOMATED_BACKUP_FILENAME_FORMAT = "atlassian-crowd-%s-automated-backup-%s.xml";
    Pattern AUTOMATED_BACKUP_FILENAME_PATTERN = Pattern.compile("atlassian-crowd-(.+)-automated-backup-(.+).xml");
}
