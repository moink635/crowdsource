package com.atlassian.crowd.manager.token.factory;

import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.manager.proxy.TrustedProxyManager;
import com.atlassian.crowd.manager.validation.XForwardedForUtil;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public class TokenKeyGeneratorImpl implements TokenKeyGenerator
{
    private final static Logger LOGGER = LoggerFactory.getLogger(TokenKeyGeneratorImpl.class);

    private final String algorithm;
    private final TrustedProxyManager trustedProxyManager;

    public TokenKeyGeneratorImpl(TrustedProxyManager trustedProxyManager, String algorithm)
    {
        this.trustedProxyManager = trustedProxyManager;
        this.algorithm = algorithm;
    }

    public String generateKey(long directoryID, String name, List<ValidationFactor> validationFactors) throws InvalidTokenException
    {
        LOGGER.debug("Generating Token for principal: {}", name);

        MessageDigest messageDigest;
        try
        {
            messageDigest = MessageDigest.getInstance(algorithm);
        }
        catch (NoSuchAlgorithmException e)
        {
            throw new InvalidTokenException(e);
        }

        String message = constructMessage(directoryID, name, validationFactors);

        byte[] digest = messageDigest.digest(message.getBytes());

        return makeUrlSafe(Base64.encodeBase64String(digest)).trim();
    }

    private String constructMessage(long directoryID, String name, List<ValidationFactor> validationFactors)
    {
        StringBuilder message = new StringBuilder();

        addValidationFactors(message, validationFactors);

        // add the directory ID to the token, in the case of an application -1.
        message.append(Long.toString(directoryID));

        // add the name of the principal/application to the token
        message.append(StringUtils.lowerCase(name));

        return message.toString();
    }

    private String makeUrlSafe(String encodedToken)
    {
        // remove any base64 characters which may get URL encoded
        return encodedToken.replaceAll("\\+|/|=|\\*", "0");
    }

    /**
     * Reads the validation factors passed in by the client application and adds them to the message. Understands
     * X-Forwarded-For and trusted proxies.
     * <p/>
     * We need to handle the case where some web applications will be behind a proxy that uses X-Forwarded-For
     * (eg, mod_proxy) and others won't (eg, mod_ajp) without breaking SSO. This means that if the source IP is a
     * trusted proxy address we should use the first IP address on the X-Forwarded-For as part of the token.
     * <p/>
     * X-Forwarded-For should never be part of the token itself.
     *
     * @param message message to build
     * @param validationFactors list of validation factors
     */
    private void addValidationFactors(StringBuilder message, List<ValidationFactor> validationFactors)
    {
        if (validationFactors != null)
        {
            // Add the validation factors in order, so that it doesn't matter in which order the client application(s)
            // added them to the list of validation factors

            // Note: not checking user agent. IE8 sometimes gives IE7 user-agent string (see CWD-1827)

            // remote address (determine whether it's the REMOTE_ADDRESS or (part) of the XFF.
            ValidationFactor remoteIP = getFactor(validationFactors, ValidationFactor.REMOTE_ADDRESS);
            if (remoteIP != null)
            {
                ValidationFactor xForwardedFor = getFactor(validationFactors, ValidationFactor.X_FORWARDED_FOR);
                String xff = (xForwardedFor == null ? null : xForwardedFor.getValue());
                String remoteAddress = XForwardedForUtil.getTrustedAddress(trustedProxyManager, remoteIP.getValue(), xff);
                LOGGER.debug("Adding remote address of {}", remoteAddress);
                message.append(remoteAddress);
            }

            // remote host
            addValidationFactor(message, validationFactors, ValidationFactor.REMOTE_HOST);

            // name (applications only)
            addValidationFactor(message, validationFactors, ValidationFactor.NAME);

            // privilege level
            addValidationFactor(message, validationFactors, ValidationFactor.PRIVILEGE_LEVEL);

            // secret number
            addValidationFactor(message, validationFactors, ValidationFactor.RANDOM_NUMBER);
        }
    }

    /**
     * Fetches the specified validation factor from the array passed in. Returns null if the factor does not exist.
     *
     * @param validationFactors list of validation factors
     * @param factorName name of the validation factor to retrieve from the list
     * @return requested validation factor
     */
    private ValidationFactor getFactor(List<ValidationFactor> validationFactors, String factorName)
    {
        for (ValidationFactor validationFactor : validationFactors)
        {
            if (factorName.equals(validationFactor.getName()))
            {
                return validationFactor;
            }
        }
        return null;
    }

    /**
     * If it exists, adds the validation factor specified in <code>factorName</code> to the <code>message</code>.
     * Otherwise does nothing.
     *
     * @param message message to build
     * @param validationFactors list of validation factors
     * @param factorName name of validation factor to add
     */
    protected void addValidationFactor(StringBuilder message, List<ValidationFactor> validationFactors, String factorName)
    {
        ValidationFactor factor = getFactor(validationFactors, factorName);
        if (factor != null)
        {
            LOGGER.debug("Adding {} of {}", factorName, factor);
            message.append(factor.getValue());
        }
    }
}
