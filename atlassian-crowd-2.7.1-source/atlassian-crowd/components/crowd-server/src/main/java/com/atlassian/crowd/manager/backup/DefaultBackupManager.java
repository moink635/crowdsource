package com.atlassian.crowd.manager.backup;

import com.atlassian.crowd.file.FileConfigurationExporter;
import com.atlassian.crowd.migration.ExportException;
import com.atlassian.crowd.migration.XmlMigrationManager;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.util.build.BuildUtils;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.atlassian.crowd.manager.backup.BackupFileConstants.AUTOMATED_BACKUP_FILENAME_FORMAT;
import static com.atlassian.crowd.manager.backup.BackupFileConstants.DIRECTORIES_CONF_FILE_NAME;
import static com.atlassian.crowd.manager.backup.BackupFileConstants.MANUAL_BACKUP_FILENAME_FORMAT;
import static com.atlassian.crowd.manager.backup.BackupFileConstants.TIMESTAMP_FORMAT;
import static com.google.common.base.Preconditions.checkArgument;

/**
 * Default implementation of {@link BackupManager}
 *
 * @since v2.7
 */
public class DefaultBackupManager implements BackupManager
{
    private final XmlMigrationManager xmlMigrationManager;
    private final FileConfigurationExporter fileConfigurationExporter;
    private final BackupFileStore backupFileStore;

    public DefaultBackupManager(final XmlMigrationManager xmlMigrationManager, final FileConfigurationExporter fileConfigurationExporter, BackupFileStore backupFileStore)
    {
        this.xmlMigrationManager = xmlMigrationManager;
        this.fileConfigurationExporter = fileConfigurationExporter;
        this.backupFileStore = backupFileStore;
    }

    @Override
    public String generateManualBackupFileName()
    {
        return generateFileName(MANUAL_BACKUP_FILENAME_FORMAT);
    }

    @Override
    public String generateAutomatedBackupFileName()
    {
        return generateFileName(AUTOMATED_BACKUP_FILENAME_FORMAT);
    }

    @Override
    public String getBackupFileFullPath(final String fileName)
    {
        final File file = new File(backupFileStore.getBackupDirectory(), fileName);
        return file.getPath();
    }

    @Transactional
    @Override
    public synchronized long backup(final String exportFileName, final boolean resetDomain) throws ExportException
    {
        checkArgument(StringUtils.isNotEmpty(exportFileName));

        String xmlExportFilePath = getBackupFileFullPath(exportFileName);
        String directoriesExportFilePath = getBackupFileFullPath(DIRECTORIES_CONF_FILE_NAME);

        // configure our export options
        Map<String, Object> options = ImmutableMap.<String, Object>of(XmlMigrationManagerImpl.OPTION_RESET_DOMAIN, resetDomain);

        long timeTaken = xmlMigrationManager.exportXml(xmlExportFilePath, options);
        fileConfigurationExporter.exportDirectories(directoriesExportFilePath);

        return timeTaken;
    }

    @Override
    public BackupSummary getAutomatedBackupSummary()
    {
        final List<File> backupFiles = backupFileStore.getBackupFiles();
        int numBackups = backupFiles.size();
        Date oldest = null;
        Date youngest = null;

        if (numBackups > 0)
        {
            oldest = backupFileStore.extractTimestamp(backupFiles.get(0));
            youngest = backupFileStore.extractTimestamp(backupFiles.get(numBackups - 1));
        }
        return new BackupSummary(numBackups, oldest, youngest);
    }

    protected String generateFileName(String pattern)
    {
        SimpleDateFormat sdf = new SimpleDateFormat(TIMESTAMP_FORMAT);
        String timestamp = sdf.format(new Date());

        return String.format(pattern, timestamp, BuildUtils.BUILD_VERSION);
    }
}
