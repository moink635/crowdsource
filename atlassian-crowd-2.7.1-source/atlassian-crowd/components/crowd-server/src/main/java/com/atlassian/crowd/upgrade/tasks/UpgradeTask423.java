package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.embedded.impl.ConnectionPoolPropertyConstants;
import com.atlassian.crowd.manager.property.PropertyManager;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Add default values for LDAP connection pool settings to the database
 */
public class UpgradeTask423 implements UpgradeTask
{
    private PropertyManager propertyManager;
    private final Collection<String> errors = new ArrayList<String>();

    public String getBuildNumber()
    {
        return "423";
    }

    public String getShortDescription()
    {
        return "Add default values for LDAP connection pool settings to the database";
    }

    public void doUpgrade() throws Exception
    {       
        propertyManager.setProperty(ConnectionPoolPropertyConstants.POOL_INITIAL_SIZE, ConnectionPoolPropertyConstants.DEFAULT_INITIAL_POOL_SIZE);
        propertyManager.setProperty(ConnectionPoolPropertyConstants.POOL_PREFERRED_SIZE, ConnectionPoolPropertyConstants.DEFAULT_PREFERRED_POOL_SIZE);
        propertyManager.setProperty(ConnectionPoolPropertyConstants.POOL_MAXIMUM_SIZE, ConnectionPoolPropertyConstants.DEFAULT_MAXIMUM_POOL_SIZE);
        propertyManager.setProperty(ConnectionPoolPropertyConstants.POOL_TIMEOUT, ConnectionPoolPropertyConstants.DEFAULT_POOL_TIMEOUT_MS);
        propertyManager.setProperty(ConnectionPoolPropertyConstants.POOL_PROTOCOL, ConnectionPoolPropertyConstants.DEFAULT_POOL_PROTOCOL);
        propertyManager.setProperty(ConnectionPoolPropertyConstants.POOL_AUTHENTICATION, ConnectionPoolPropertyConstants.DEFAULT_POOL_AUTHENTICATION);
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}
