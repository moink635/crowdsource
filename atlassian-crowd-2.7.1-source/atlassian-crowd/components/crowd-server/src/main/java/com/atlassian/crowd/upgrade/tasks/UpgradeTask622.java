package com.atlassian.crowd.upgrade.tasks;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import com.atlassian.crowd.directory.DirectoryProperties;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Updates cached LDAP directories to include
 * {@link com.atlassian.crowd.model.directory.DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS} set to true to
 * preserve the behaviour previous to CWD-995.
 *
 * @since v2.7
 */
public class UpgradeTask622 implements UpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask542.class);

    private DirectoryDao directoryDao;

    private final Collection<String> errors = new ArrayList<String>();

    @Override
    public String getBuildNumber()
    {
        return "622";
    }

    @Override
    public String getShortDescription()
    {
        return "Upgrading cached LDAP directories to include the attribute " + DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS + " set to true.";
    }

    @Override
    public void doUpgrade() throws Exception
    {
        for (Directory directory : findLDAPDirectories())
        {
            if (Boolean.valueOf(directory.getValue(DirectoryProperties.CACHE_ENABLED))
                && directory.getValue(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS) == null
                && directory.getAllowedOperations().contains(OperationType.UPDATE_USER))
            {
                log.debug("Upgrading directory {}", directory);
                updateDirectory(directory);
            }
        }
    }

    private void updateDirectory(Directory directory)
    {
        DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
        directoryToUpdate.setAttribute(DirectoryImpl.ATTRIBUTE_KEY_LOCAL_USER_STATUS, Boolean.TRUE.toString());

        try
        {
            directoryDao.update(directoryToUpdate);
        }
        catch (DirectoryNotFoundException e)
        {
            final String errorMessage = "Could not update directory " + directory;
            log.error(errorMessage, e);
            errors.add(errorMessage + ", error is " + e.getMessage());
        }
    }

    @Override
    public Collection<String> getErrors()
    {
        return Collections.unmodifiableCollection(errors);
    }

    private Iterable<Directory> findLDAPDirectories()
    {
        return directoryDao.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                                       .with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.CONNECTOR))
                                       .returningAtMost(EntityQuery.ALL_RESULTS));

    }

    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }
}
