package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.dao.membership.MembershipDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.LegacyImportDataHolder;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.group.InternalGroupWithAttributes;
import com.atlassian.crowd.model.membership.InternalMembership;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.util.BatchResult;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchResultWithIdReferences;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class GroupMapper extends DatabaseMapper implements DatabaseImporter
{

    private final GroupDAOHibernate groupDAO;
    private final MembershipDAOHibernate membershipDAO;
    private final DirectoryDAOHibernate directoryDAO;

    public GroupMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, JdbcOperations jdbcTemplate, GroupDAOHibernate groupDAO, MembershipDAOHibernate membershipDAO, DirectoryDAOHibernate directoryDAO)
    {
        super(sessionFactory, batchProcessor, jdbcTemplate);

        this.groupDAO = groupDAO;
        this.membershipDAO = membershipDAO;
        this.directoryDAO = directoryDAO;
    }

    public void importFromDatabase(LegacyImportDataHolder importData) throws ImportException
    {
        List<InternalGroupWithAttributes> groups = importGroupsFromDatabase(importData.getOldToNewDirectoryIds());

        BatchResultWithIdReferences<Group> groupImportResults = groupDAO.addAll(groups);
        for (Group group : groupImportResults.getFailedEntities())
        {
            logger.error("Unable to add group <" + group.getName() + "> in directory with id <" + group.getDirectoryId() + ">");
        }

        // die hard if there are any errors
        if (groupImportResults.hasFailures())
        {
            throw new ImportException("Unable to import all groups. See logs for more details.");
        }

        // now import memberships (group by group)
        Set<InternalMembership> memberships = importMembershipsFromDatabase(importData, groupImportResults);
        BatchResult<InternalMembership> membershipResult = membershipDAO.addAll(memberships);
        for (InternalMembership membership : membershipResult.getFailedEntities())
        {
            logger.error("Unable to add user <" + membership.getChildName() + "> to group <" + membership.getParentName() + "> in directory with id <" + membership.getDirectory().getId() + ">");
        }

        if (membershipResult.hasFailures())
        {
            throw new ImportException("Unable to import all memberships. See logs for more details.");
        }

        logger.info("Successfully migrated " + groupImportResults.getTotalSuccessful() + " groups, with " + memberships.size() + " memberships.");
    }

    protected List<InternalGroupWithAttributes> importGroupsFromDatabase(final Map<Long, Long> oldToNewDirectoryIds)
    {
        List<InternalGroupWithAttributes> internalGroupWithAttributes = new ArrayList<InternalGroupWithAttributes>();

        GroupTableMapper groupTableMapper = new GroupTableMapper(oldToNewDirectoryIds);
        Map<InternalGroup, Long> groupToOldDirectoryId = groupTableMapper.getGroupToOldDirectoryIds();
        List<InternalGroup> groups = getGroups(groupTableMapper);

        // Get all the group attributes in one database call.
        Map<EntityIdentifier, Map<String, Set<String>>> allGroupAttributes = getGroupAttributes();

        for (InternalGroup group : groups)
        {
            EntityIdentifier currentGroup = new EntityIdentifier(groupToOldDirectoryId.get(group), group.getName());
            Map<String, Set<String>> attributes = allGroupAttributes.get(currentGroup);

            if (attributes == null)
            {
                attributes = new HashMap<String, Set<String>>();
            }
            internalGroupWithAttributes.add(new InternalGroupWithAttributes(group, attributes));
        }
        return internalGroupWithAttributes;
    }

    private List<InternalGroup> getGroups(GroupTableMapper groupTableMapper)
    {
        return jdbcTemplate.query(legacyTableQueries.getGroupsSQL(), groupTableMapper);
    }

    private Map<EntityIdentifier, Map<String, Set<String>>> getGroupAttributes()
    {
        AttributeMapper groupAttributeMapper = new AttributeMapper("REMOTEGROUPDIRECTORYID", "REMOTEGROUPNAME");
        jdbcTemplate.query(legacyTableQueries.getGroupAttributesSQL(), groupAttributeMapper);
        groupAttributeMapper.getEntityAttributes();

        Map<EntityIdentifier, List<Map<String, String>>> groupAttributes = groupAttributeMapper.getEntityAttributes();

        // Convert attributes from list of maps to a map.
        Map<EntityIdentifier, Map<String, Set<String>>> groupAttributeMap = new HashMap<EntityIdentifier, Map<String, Set<String>>>();
        for (Map.Entry<EntityIdentifier, List<Map<String, String>>> entry : groupAttributes.entrySet())
        {
            groupAttributeMap.put(entry.getKey(), attributeListToMultiAttributeMap(entry.getValue()));
        }
        return groupAttributeMap;
    }

    protected Set<InternalMembership> importMembershipsFromDatabase(LegacyImportDataHolder importData, BatchResultWithIdReferences<Group> groupImportResults)
    {
        MembershipTableMapper membershipTableMapper = new MembershipTableMapper(importData, groupImportResults);
        List<InternalMembership> memberships = jdbcTemplate.query(legacyTableQueries.getGroupMembershipsSQL(), membershipTableMapper);
        return new HashSet<InternalMembership>(memberships);
    }

    private class GroupTableMapper implements RowMapper
    {
        private final Map<Long, Long> oldToNewDirectoryIds;
        private final Map<InternalGroup, Long> groupToOldDirectoryIds = new HashMap<InternalGroup, Long>();

        public GroupTableMapper(Map<Long, Long> oldToNewDirectoryIds)
        {
            this.oldToNewDirectoryIds = oldToNewDirectoryIds;
        }

        public Object mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            Long oldDirectoryId = rs.getLong("DIRECTORYID");
            String groupName = rs.getString("NAME");
            boolean active = rs.getBoolean("ACTIVE");
            Date createDate = getDateFromDatabase(rs.getString("CONCEPTION"));
            Date updatedDate = getDateFromDatabase(rs.getString("LASTMODIFIED"));
            String description = rs.getString("DESCRIPTION");
            GroupType groupType = GroupType.GROUP;

            // check the old directoryId
            Long directoryId = oldToNewDirectoryIds.get(oldDirectoryId);
            if (directoryId == null)
            {
                throw new IllegalArgumentException("Group belongs to an unknown old directory with ID: " + directoryId);
            }

            Directory directory = (Directory) directoryDAO.loadReference(directoryId);

            // put in the default values
            InternalEntityTemplate internalEntityTemplate = createInternalEntityTemplate(directoryId, groupName, createDate, updatedDate, active);
            internalEntityTemplate.setId(null);

            // create the group
            GroupTemplate groupTemplate = new GroupTemplate(internalEntityTemplate.getName(), directoryId, groupType);
            groupTemplate.setActive(internalEntityTemplate.isActive());
            groupTemplate.setDescription(description);

            // Save the group
            InternalGroup group = new InternalGroup(internalEntityTemplate, directory, groupTemplate);
            groupToOldDirectoryIds.put(group, oldDirectoryId);

            return group;
        }

        public Map<InternalGroup, Long> getGroupToOldDirectoryIds()
        {
            return groupToOldDirectoryIds;
        }
    }

    private class MembershipTableMapper implements RowMapper
    {
        private final LegacyImportDataHolder importData;
        private final BatchResultWithIdReferences groupImportResults;

        public MembershipTableMapper(LegacyImportDataHolder importData, BatchResultWithIdReferences groupImportResults)
        {
            this.importData = importData;
            this.groupImportResults = groupImportResults;
        }

        public Object mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            String parentName = rs.getString("REMOTEGROUPNAME");
            String childName = rs.getString("REMOTEPRINCIPALNAME");
            Long oldDirectoryId = rs.getLong("REMOTEGROUPDIRECTORYID");

            Long directoryId = importData.getOldToNewDirectoryIds().get(oldDirectoryId);
            if (directoryId == null)
            {
                throw new IllegalArgumentException("Group belongs to an unknown old directory with ID: " + directoryId);
            }

            DirectoryImpl directory = (DirectoryImpl) directoryDAO.loadReference(directoryId);
            Long groupId = groupImportResults.getIdReference(directoryId, parentName);
            Long userId = importData.getUserImportResults().getIdReference(directoryId, childName);

            return new InternalMembership(null, groupId, userId, MembershipType.GROUP_USER, GroupType.GROUP, parentName, childName, directory);
        }
    }
}
