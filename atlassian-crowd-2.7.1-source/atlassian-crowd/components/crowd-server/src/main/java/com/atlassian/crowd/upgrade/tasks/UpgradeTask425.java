package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.InternalDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.password.factory.PasswordEncoderFactory;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * This upgrade task will switch internal directories using Atlassian SHA1 for user encryption to use Atlassian Security instead.
 */
public class UpgradeTask425 implements UpgradeTask
{
    private static final String ATLASSIAN_SHA1_KEY = "atlassian-sha1";

    private static final Logger log = LoggerFactory.getLogger(UpgradeTask425.class);

    private final Collection<String> errors = new ArrayList<String>();

    private DirectoryDao directoryDao;

    public String getBuildNumber()
    {
        return "425";
    }

    public String getShortDescription()
    {
        return "Upgrading internal directories using Atlassian SHA1 for user encryption to use Atlassian Security instead";
    }

    public void doUpgrade() throws Exception
    {
        for (Directory directory : findAllInternalDirectories())
        {
            if (needsUpgrade(directory))
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Upgrading directory " + directory);
                }
                try
                {
                    updateDirectory(directory);
                }
                catch (DataAccessException e)
                {
                    final String errorMessage = "Could not update directory " + directory;
                    log.error(errorMessage, e);
                    errors.add(errorMessage + ", error is " + e.getMessage());
                }
            }
            else
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Directory " + directory + " will not be upgraded.");
                }
            }
        }
    }

    private void updateDirectory(Directory directory) throws DataAccessException, DirectoryNotFoundException
    {
        DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
        directoryToUpdate.setAttribute(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD, PasswordEncoderFactory.ATLASSIAN_SECURITY_ENCODER);
        directoryDao.update(directoryToUpdate);
    }

    private boolean needsUpgrade(Directory directory)
    {
        return ATLASSIAN_SHA1_KEY.equals(directory.getValue(InternalDirectory.ATTRIBUTE_USER_ENCRYPTION_METHOD));
    }

    public List<Directory> findAllInternalDirectories()
    {
        return directoryDao.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                .with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.INTERNAL))
                .returningAtMost(EntityQuery.ALL_RESULTS));
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }
}
