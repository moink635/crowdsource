package com.atlassian.crowd.migration.verify;

import com.atlassian.crowd.migration.legacy.database.sql.LegacyTableQueries;

import java.util.ArrayList;
import java.util.List;


/**
 * Responsible for verifying the correctness/compatibility of pre 2.0 database to 2.0+ database
 */
public abstract class DatabaseVerifier
{
    protected final List<String> errors = new ArrayList<String>();
    protected LegacyTableQueries legacyTableQueries;

    /**
     * This method will verify if the correctness/compatibility of the database.
     *
     * If an error is detected, the error will be added to the errors list.
     */
    public abstract void verify();

    /**
     * This method will be called between runs of the verifier to make sure that state can be purged between runs if required.
     */
    public void clearState()
    {
        errors.clear();
    }

    /**
     * Checks if there are any errors as a result of the verification
     * @return true if there are no errors (errors list is empty), false otherwise
     */
    public boolean hasErrors()
    {
        return !errors.isEmpty();
    }

    /**
     * @return the list of error messages during verification
     */
    public List<String> getErrors()
    {
        return errors;
    }

    /**
     * Provides the appropriate legacy table querier to the verifier if it needs to query the database.
     * @param legacyTableQueries The appropriate table querier (eg. MySQL, Postgres)
     */
    public void setLegacyTableQueries(LegacyTableQueries legacyTableQueries)
    {
        this.legacyTableQueries = legacyTableQueries;
    }
}
