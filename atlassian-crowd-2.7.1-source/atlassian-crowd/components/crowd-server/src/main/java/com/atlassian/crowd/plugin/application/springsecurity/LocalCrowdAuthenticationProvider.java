package com.atlassian.crowd.plugin.application.springsecurity;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidAuthorizationTokenException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.integration.springsecurity.CrowdAuthenticationProvider;
import com.atlassian.crowd.integration.springsecurity.CrowdSSOTokenInvalidException;
import com.atlassian.crowd.integration.springsecurity.user.CrowdDataAccessException;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.service.soap.ObjectTranslator;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Application-aware local authentication provider.
 * <p/>
 * Does not make SOAP calls.
 */

public class LocalCrowdAuthenticationProvider extends CrowdAuthenticationProvider
{
    private static final String ROLE_PREFIX = "ROLE_";

    private final ApplicationManager applicationManager;
    private final ApplicationService applicationService;
    private final TokenAuthenticationManager tokenAuthenticationManager;

    public LocalCrowdAuthenticationProvider(Application application, ApplicationService applicationService, ApplicationManager applicationManager, TokenAuthenticationManager tokenAuthenticationManager)
    {
        super(application.getName());
        this.applicationService = applicationService;
        this.applicationManager = applicationManager;
        this.tokenAuthenticationManager = tokenAuthenticationManager;
    }

    protected boolean isAuthenticated(String token, ValidationFactor[] validationFactors) throws InvalidAuthorizationTokenException, ApplicationAccessDeniedException, RemoteException
    {
        try
        {
            // check the token
            tokenAuthenticationManager.validateUserToken(token, validationFactors, applicationName);

            return true;
        }
        catch (InvalidTokenException e)
        {
            return false;
        }
        catch (com.atlassian.crowd.manager.application.ApplicationAccessDeniedException e)
        {
            throw new ApplicationAccessDeniedException(e);
        }
        catch (OperationFailedException e)
        {
            throw new RemoteException(e.getMessage(), e);
        }
    }

    protected String authenticate(String username, String password, ValidationFactor[] validationFactors) throws InvalidAuthorizationTokenException, InvalidAuthenticationException, InactiveAccountException, ApplicationAccessDeniedException, RemoteException, ExpiredCredentialException
    {
        UserAuthenticationContext pac = new UserAuthenticationContext();
        pac.setName(username);
        pac.setCredential(new PasswordCredential(password));
        pac.setApplication(applicationName);
        pac.setValidationFactors(validationFactors);

        try
        {
            Token token = tokenAuthenticationManager.authenticateUser(pac, TokenLifetime.USE_DEFAULT);
            return token.getRandomHash();
        }
        catch (com.atlassian.crowd.manager.application.ApplicationAccessDeniedException e)
        {
            throw new ApplicationAccessDeniedException(e);
        }
        catch (OperationFailedException e)
        {
            throw new RemoteException(e.getMessage(), e);
        }
        catch (ApplicationNotFoundException e)
        {
            throw new InvalidAuthorizationTokenException(e);
        }
    }

    protected CrowdUserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException
    {
        try
        {
            Application application = getApplication();
            SOAPPrincipal principal = ObjectTranslator.processUser(applicationService.findUserByName(application, username));
            return new CrowdUserDetails(principal, findGrantedAuthorities(principal.getName()));
        }
        catch (UserNotFoundException e)
        {
            throw new UsernameNotFoundException("User " + username + " not found under application: " + applicationName, e);
        }
        catch (ApplicationNotFoundException e)
        {
            throw new CrowdDataAccessException(e);
        }
    }

    protected CrowdUserDetails loadUserByToken(String token) throws CrowdSSOTokenInvalidException, DataAccessException
    {
        // IMPORTANT: this does not guarantee the user returned is valid for the authentication context of the
        // application. You must explicitly call isAuthenticated to verify the token is valid for the application.

        try
        {
            SOAPPrincipal principal = ObjectTranslator.processUser(tokenAuthenticationManager.findUserByToken(token, getApplication().getName()));
            return new CrowdUserDetails(principal, findGrantedAuthorities(principal.getName()));
        }
        catch (OperationFailedException e)
        {
            throw new CrowdDataAccessException(e);
        }
        catch (InvalidTokenException e)
        {
            throw new CrowdSSOTokenInvalidException("Invalid token: " + token, e);
        }
        catch (ApplicationNotFoundException e)
        {
            throw new CrowdDataAccessException(e);
        }
        catch (com.atlassian.crowd.manager.application.ApplicationAccessDeniedException e)
        {
            throw new CrowdSSOTokenInvalidException("Invalid token: " + token, e);
        }
    }

    // NOTE: this could benefit from caching
    private GrantedAuthority[] findGrantedAuthorities(String username) throws ApplicationNotFoundException
    {
        List<GrantedAuthority> auths = new ArrayList<GrantedAuthority>();

        final List<String> groups = applicationService.searchNestedGroupRelationships(getApplication(), QueryBuilder.queryFor(String.class, EntityDescriptor.user()).childrenOf(EntityDescriptor.group(GroupType.GROUP)).withName(username).returningAtMost(EntityQuery.ALL_RESULTS));

        for (String group : groups)
        {
            auths.add(new SimpleGrantedAuthority(ROLE_PREFIX + group));
        }

        return auths.toArray(new GrantedAuthority[auths.size()]);
    }

    private Application getApplication() throws ApplicationNotFoundException
    {
        return applicationManager.findByName(applicationName);
    }

}