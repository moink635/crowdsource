package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.dao.application.ApplicationDAO;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.DirectoryMapping;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Upgrades directories to contain UPDATE_USER_ATTRIBUTE, UPDATE_GROUP_ATTRIBUTE and UPDATE_ROLE_ATTRIBUTE permissions.
 *
 * @since v2.2
 */
public class UpgradeTask452 implements UpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask452.class);

    private final Collection<String> errors = new ArrayList<String>();

    private DirectoryDao directoryDao; 

    private ApplicationDAO applicationDAO;

    public String getBuildNumber()
    {
        return "452";
    }

    public String getShortDescription()
    {
        return "Upgrading directories to contain UPDATE_USER_ATTRIBUTE, UPDATE_GROUP_ATTRIBUTE and UPDATE_ROLE_ATTRIBUTE permissions.";
    }

    public void doUpgrade() throws Exception
    {
        for (Directory directory : directoryDao.findAll())
        {
            log.debug("Upgrading directory {}", directory);
            try
            {
                updateDirectory(directory);
            }
            catch (DataAccessException e)
            {
                final String errorMessage = "Could not update directory " + directory;
                log.error(errorMessage, e);
                errors.add(errorMessage + ", error is " + e.getMessage());
            }
        }
        for (Application application : findAllApplications())
        {
            log.debug("Upgrading application {}", application);
            try
            {
                updateApplication(application);
            }
            catch (DataAccessException e)
            {
                final String errorMessage = "Could not update application " + application;
                log.error(errorMessage, e);
                errors.add(errorMessage + ", error is " + e.getMessage());
            }
        }
    }

    private void updateDirectory(Directory directory) throws DataAccessException, DirectoryNotFoundException
    {
        final DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);

        final Set<OperationType> allowedOperations = directoryToUpdate.getAllowedOperations();
        if (allowedOperations.contains(OperationType.UPDATE_GROUP))
        {
            directoryToUpdate.addAllowedOperation(OperationType.UPDATE_GROUP_ATTRIBUTE);
        }
        if (allowedOperations.contains(OperationType.UPDATE_USER))
        {
            directoryToUpdate.addAllowedOperation(OperationType.UPDATE_USER_ATTRIBUTE);
        }
        if (allowedOperations.contains(OperationType.UPDATE_ROLE))
        {
            directoryToUpdate.addAllowedOperation(OperationType.UPDATE_ROLE_ATTRIBUTE);
        }

        directoryDao.update(directoryToUpdate);
    }

    private void updateApplication(Application application) throws DataAccessException, ApplicationNotFoundException, DirectoryNotFoundException
    {
        for (DirectoryMapping directoryMapping : application.getDirectoryMappings())
        {
            final Set<OperationType> allowedOperations = directoryMapping.getAllowedOperations();
            if (allowedOperations.contains(OperationType.UPDATE_GROUP))
            {
                directoryMapping.addAllowedOperation(OperationType.UPDATE_GROUP_ATTRIBUTE);
            }
            if (allowedOperations.contains(OperationType.UPDATE_USER))
            {
                directoryMapping.addAllowedOperation(OperationType.UPDATE_USER_ATTRIBUTE);
            }
            if (allowedOperations.contains(OperationType.UPDATE_ROLE))
            {
                directoryMapping.addAllowedOperation(OperationType.UPDATE_ROLE_ATTRIBUTE);
            }

            applicationDAO.updateDirectoryMapping(
                    directoryMapping.getApplication().getId(),
                    directoryMapping.getDirectory().getId(),
                    directoryMapping.isAllowAllToAuthenticate(),
                    directoryMapping.getAllowedOperations());
        }
    }

    private List<Application> findAllApplications()
    {
        return applicationDAO.search(QueryBuilder.queryFor(Application.class, EntityDescriptor.application())
                .returningAtMost(EntityQuery.ALL_RESULTS));
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }

    public void setApplicationDao(ApplicationDAO applicationDao)
    {
        this.applicationDAO = applicationDao;
    }
}
