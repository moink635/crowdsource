package com.atlassian.crowd.migration;

import com.atlassian.crowd.dao.property.PropertyDAO;
import com.atlassian.crowd.migration.legacy.XmlMapper;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * This mapper will handle the mapping of a {@link com.atlassian.crowd.model.property.Property}
 */
public class PropertyMapper extends XmlMapper implements Mapper
{
    private final PropertyDAO propertyDAO;

    protected static final String PROPERTIES_XML_ROOT = "properties";
    protected static final String PROPERTY_XML_NODE = "property";
    protected static final String PROPERTY_XML_KEY = "key";
    protected static final String PROPERTY_XML_NAME = "name";
    protected static final String PROPERTY_XML_VALUE = "value";


    public PropertyMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, PropertyDAO propertyDAO)
 	{
 	    super(sessionFactory, batchProcessor);
 	    this.propertyDAO = propertyDAO;
 	}

    public Element exportXml(Map options) throws ExportException
    {
        // Create the properties element
        Element propertiesRoot = DocumentHelper.createElement(PROPERTIES_XML_ROOT);

        // Get all the properties from the datastore
        List properties = propertyDAO.findAll();

        // parse our options for this exporter

        Boolean resetDomainOption;

        if (options.containsKey(XmlMigrationManagerImpl.OPTION_RESET_DOMAIN))
        {
            resetDomainOption = (Boolean) options.get(XmlMigrationManagerImpl.OPTION_RESET_DOMAIN);
        }
        else
        {
            resetDomainOption = Boolean.FALSE;
        }

        if (properties != null && !properties.isEmpty())
        {
            for (Iterator iterator = properties.iterator(); iterator.hasNext();)
            {
                Property property = (Property) iterator.next();
                Element propertyElement = propertiesRoot.addElement(PROPERTY_XML_NODE);
                propertyElement.addElement(PROPERTY_XML_KEY).addText(property.getKey());
                propertyElement.addElement(PROPERTY_XML_NAME).addText(property.getName());

                if (Property.CROWD_PROPERTY_KEY.equals(property.getKey()) && Property.DOMAIN.equals(property.getName()) && resetDomainOption.booleanValue())
                {
                    propertyElement.addElement(PROPERTY_XML_VALUE).addText("");
                }
                else
                {
                    propertyElement.addElement(PROPERTY_XML_VALUE).addText(property.getValue() != null ? property.getValue() : "");
                }
            }
        }
        else
        {
            logger.error("Failed to find any properties to export!");
        }

        return propertiesRoot;
    }

    public void importXml(Element root) throws ImportException
    {
        importProperties(root);
    }

    private void importProperties(final Element root)
    {
        Element propertiesElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + PROPERTIES_XML_ROOT);
        if (propertiesElement == null)
        {
            logger.error("No properties were found for importing!");
        }
        else
        {
            for (Iterator properties = propertiesElement.elementIterator(); properties.hasNext();)
            {
                Element propertyElemet = (Element) properties.next();
                final Element propertyKeyElement = propertyElemet.element(PROPERTY_XML_KEY);

                String key;
                if (propertyKeyElement != null && propertyKeyElement.getText() != null)
                {
                    key = propertyKeyElement.getText();
                }
                else
                {
                    // default to a Crowd property
                    key = Property.CROWD_PROPERTY_KEY;
                }

                String name = propertyElemet.element(PROPERTY_XML_NAME).getText();
                String value = propertyElemet.element(PROPERTY_XML_VALUE).getText();

                // Now create the new property
                propertyDAO.add(new Property(key, name, value));
            }
        }

    }
}
