package com.atlassian.crowd.migration.legacy;

import com.atlassian.config.ConfigurationException;
import com.atlassian.crowd.dao.property.PropertyDAOHibernate;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Element;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Iterator;

public class PropertyMapper extends GenericLegacyImporter implements LegacyImporter
{
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected static final String PROPERTIES_XML_ROOT = "properties";
    protected static final String PROPERTY_XML_NODE = "property";
    protected static final String PROPERTY_XML_KEY = "key";
    protected static final String PROPERTY_XML_NAME = "name";
    protected static final String PROPERTY_XML_VALUE = "value";

    private final PropertyDAOHibernate propertyDAO;
    private final CrowdBootstrapManager bootstrapManager;

    public PropertyMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, PropertyDAOHibernate propertyDAO, CrowdBootstrapManager bootstrapManager)
    {
        super(sessionFactory, batchProcessor);
        this.propertyDAO = propertyDAO;
        this.bootstrapManager = bootstrapManager;
    }

    public void importXml(final Element root, final LegacyImportDataHolder importData) throws ImportException
    {
        Element propertiesElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + PROPERTIES_XML_ROOT);
        if (propertiesElement == null)
        {
            logger.error("No properties were found for importing!");
        }
        else
        {
            for (Iterator properties = propertiesElement.elementIterator(); properties.hasNext();)
            {
                Element propertyElemet = (Element) properties.next();
                String codeName = propertyElemet.element(PROPERTY_XML_NAME).getText();
                String value = propertyElemet.element(PROPERTY_XML_VALUE).getText();

                // hack: need to migrate server id from db to cfg.xml
                if (codeName.equals("22") && StringUtils.isNotBlank(value))
                {
                    try
                    {
                        bootstrapManager.setServerID(value);
                    }
                    catch (ConfigurationException e)
                    {
                        throw new ImportException(e);
                    }
                }

                // get the new name from the code
                String name = getNameFromLegacyCode(codeName);

                if (name != null)
                {
                    addEntityViaSave(new Property(Property.CROWD_PROPERTY_KEY, name, value));
                }
            }
        }
    }
}
