package com.atlassian.crowd.manager.backup;

import com.atlassian.crowd.migration.ExportException;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.concurrent.TimeUnit;

/**
 * Quartz job to run an export
 *
 * @since v2.7
 */
public class BackupJob extends QuartzJobBean
{
    private static final Logger log = LoggerFactory.getLogger(BackupJob.class);
    private BackupManager backupManager;
    private BackupFileStore backupFileStore;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException
    {
        try
        {
            final String fileName = backupManager.generateAutomatedBackupFileName();
            log.info("Starting scheduled backup to file {}...", fileName);
            long timeTaken = backupManager.backup(fileName, true);
            log.info("Scheduled backup to file '{}' finished in {} seconds", backupManager.getBackupFileFullPath(fileName), TimeUnit.MILLISECONDS.toSeconds(timeTaken));
            backupFileStore.cleanUpAutomatedBackupFiles();
        }
        catch (ExportException e)
        {
            log.error("Error while executing scheduled backup", e);
        }
    }

    public void setBackupManager(BackupManager backupManager)
    {
        this.backupManager = backupManager;
    }

    public void setBackupFileStore(BackupFileStore backupFileStore)
    {
        this.backupFileStore = backupFileStore;
    }
}
