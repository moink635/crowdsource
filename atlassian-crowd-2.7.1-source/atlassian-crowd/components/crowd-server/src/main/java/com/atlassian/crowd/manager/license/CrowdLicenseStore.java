package com.atlassian.crowd.manager.license;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.config.ConfigurationException;
import com.atlassian.config.bootstrap.AtlassianBootstrapManager;
import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.extras.api.AtlassianLicense;
import com.atlassian.extras.api.LicenseManager;
import com.atlassian.extras.api.Product;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.atlassian.license.LicensePair;

/**
 * Handles the storage and retrieval of a Crowd License. Used in both the setup process and internally within the {@link CrowdLicenseManager}
 */
public class CrowdLicenseStore
{
    private static final Logger log = LoggerFactory.getLogger(CrowdLicenseStore.class);

    private final LicenseManager licenseManager;

    private AtlassianBootstrapManager bootstrapManager;
    private CrowdLicense license;

    public CrowdLicenseStore(LicenseManager licenseManager)
    {
        this.licenseManager = licenseManager;
    }

    public AtlassianLicense getAtlassianLicense(final String license)
    {
        if (license != null)
        {
            return licenseManager.getLicense(license);
        }
        else
        {
            return null;
        }
    }

    public static CrowdLicense getCrowdLicense(final AtlassianLicense atlassianLicense)
    {
        if (atlassianLicense != null)
        {
            return (CrowdLicense) atlassianLicense.getProductLicense(Product.CROWD);
        }
        else
        {
            return null;
        }
    }

    private static final String LICENSE = "license";

    // Legacy properties names
    private static final String LICENSE_HASH = "license.hash";
    private static final String LICENSE_MESSAGE = "license.message";

    public CrowdLicense storeLicense(final String licenseText)
    {
        getBootstrapManager().setProperty(LICENSE, licenseText);
        try
        {
            getBootstrapManager().save();
            this.license = null;
        }
        catch (ConfigurationException e)
        {
            log.error("Failed to set license", e);
        }
        return getLicense();
    }

    CrowdLicense getLicense()
    {
        if (license == null)
        {
            String licenseText = (String) getBootstrapManager().getProperty(LICENSE);
            if (licenseText == null) // TODO replace this with an upgrade task.
            {
                final String licenceMessage = (String) getBootstrapManager().getProperty(LICENSE_MESSAGE);
                final String licenseHash = (String) getBootstrapManager().getProperty(LICENSE_HASH);
                try
                {
                    licenseText = new LicensePair(licenceMessage, licenseHash).getOriginalLicenseString();
                }
                catch (com.atlassian.license.LicenseException e)
                {
                    log.warn("Could NOT create license pair from license message <" + licenceMessage + "> and license hash <" + licenseHash + ">");
                }
            }

            this.license = getCrowdLicense(licenseManager.getLicense(licenseText));
        }
        return license;
    }

    private AtlassianBootstrapManager getBootstrapManager()
    {
        if (bootstrapManager == null)
        {
            bootstrapManager = BootstrapUtils.getBootstrapManager();
        }
        return bootstrapManager;
    }

    void setBootstrapManager(final CrowdBootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
    }
}
