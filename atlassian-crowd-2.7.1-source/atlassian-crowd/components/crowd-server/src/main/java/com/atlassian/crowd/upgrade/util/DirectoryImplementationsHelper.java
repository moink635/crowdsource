package com.atlassian.crowd.upgrade.util;

import java.util.HashMap;
import java.util.Map;

public class DirectoryImplementationsHelper
{
    private static final Map<String, String> DIRECTORY_IMPLEMENTATIONS = new HashMap<String, String>()
    {
        {
            put("com.atlassian.crowd.integration.directory.connector.ApacheDS", "com.atlassian.crowd.directory.ApacheDS");
            put("com.atlassian.crowd.integration.directory.connector.ApacheDS15", "com.atlassian.crowd.directory.ApacheDS15");
            put("com.atlassian.crowd.integration.directory.connector.AppleOpenDirectory", "com.atlassian.crowd.directory.AppleOpenDirectory");
            put("com.atlassian.crowd.integration.directory.delegated.DelegatedAuthenticationDirectory", "com.atlassian.crowd.directory.DelegatedAuthenticationDirectory");
            put("com.atlassian.crowd.integration.directory.connector.FedoraDS", "com.atlassian.crowd.directory.FedoraDS");
            put("com.atlassian.crowd.integration.directory.connector.GenericLDAP", "com.atlassian.crowd.directory.GenericLDAP");
            put("com.atlassian.crowd.integration.directory.internal.InternalDirectory", "com.atlassian.crowd.directory.InternalDirectory");
            put("com.atlassian.crowd.integration.directory.connector.MicrosoftActiveDirectory", "com.atlassian.crowd.directory.MicrosoftActiveDirectory");
            put("com.atlassian.crowd.integration.directory.connector.NovelleDirectory", "com.atlassian.crowd.directory.NovelleDirectory");
            put("com.atlassian.crowd.integration.directory.connector.OpenDS", "com.atlassian.crowd.directory.OpenDS");
            put("com.atlassian.crowd.integration.directory.connector.OpenLDAP", "com.atlassian.crowd.directory.OpenLDAP");
            put("com.atlassian.crowd.integration.directory.connector.OpenLDAPRfc2307", "com.atlassian.crowd.directory.OpenLDAPRfc2307");
            put("com.atlassian.crowd.integration.directory.connector.Rfc2307", "com.atlassian.crowd.directory.Rfc2307");
            put("com.atlassian.crowd.integration.directory.connector.SunONE", "com.atlassian.crowd.directory.SunONE");
        }
    };

    public static Map<String, String> getDirectoryImplementations()
    {
        return DIRECTORY_IMPLEMENTATIONS;
    }
}
