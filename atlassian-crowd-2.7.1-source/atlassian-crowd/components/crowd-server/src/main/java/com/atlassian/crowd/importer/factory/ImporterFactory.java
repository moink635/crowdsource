package com.atlassian.crowd.importer.factory;

import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.exceptions.ImporterConfigurationException;
import com.atlassian.crowd.importer.importers.Importer;

import java.util.Set;

/**
 * This factory will handle the responsibility of returning
 * an {@see ImporterDAO} based on a given configuration
 */
public interface ImporterFactory
{
    /**
     * Will return an importer based on a given Configuration
     *
     * @param configuration
     * @return an ImporterDAO from a given configuration
     */
    Importer getImporterDAO(Configuration configuration) throws ImporterConfigurationException;

    /**
     * Will return a list of supported applications that have an importer
     *
     * @return Set<String> applications with an importer
     */
    Set<String> getSupportedImporterApplications();

    /**
     * Will return a list of supported Atlassian applications that have an importer
     *
     * @return Set<String> applications with an importer
     */
    Set<String> getAtlassianSupportedImporterApplications();
}
