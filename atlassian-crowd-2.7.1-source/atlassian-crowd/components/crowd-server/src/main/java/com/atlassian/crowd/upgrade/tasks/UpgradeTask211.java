package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.manager.property.PropertyManager;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This task will add the default license user count to Crowd, '0'
 */
public class UpgradeTask211 implements UpgradeTask
{
    private static final Logger logger = LoggerFactory.getLogger(UpgradeTask211.class);

    private Collection errors = new ArrayList();

    private PropertyManager propertyManager;

    protected static final int DEFAULT_TOTAL = 0;

    public UpgradeTask211()
    {

    }

    public UpgradeTask211(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    public String getBuildNumber()
    {
        return "211";
    }

    public String getShortDescription()
    {
        return "Adding default user license count of '" + DEFAULT_TOTAL + "'";
    }

    public void doUpgrade() throws Exception
    {
        int currentCount = propertyManager.getCurrentLicenseResourceTotal();

        if (currentCount <= DEFAULT_TOTAL)
        {
            propertyManager.setCurrentLicenseResourceTotal(DEFAULT_TOTAL);
        }
    }

    public Collection getErrors()
    {
        return errors;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }
}
