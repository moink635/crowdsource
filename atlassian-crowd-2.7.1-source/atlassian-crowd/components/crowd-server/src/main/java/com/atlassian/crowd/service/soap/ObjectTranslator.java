package com.atlassian.crowd.service.soap;

import com.atlassian.crowd.embedded.api.Attributes;
import com.atlassian.crowd.integration.soap.*;
import com.atlassian.crowd.model.DirectoryEntity;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.GroupWithAttributes;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.crowd.util.SOAPPrincipalHelper;
import com.google.common.collect.Sets;

import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.crowd.model.user.UserConstants.*;

public class ObjectTranslator
{
    private static final Logger logger = LoggerFactory.getLogger(ObjectTranslator.class);

    private static final Set<String> PRIMARY_USER_ATTRIBUTES = Sets.newHashSet(FIRSTNAME, LASTNAME, DISPLAYNAME, USERNAME, EMAIL);

    public static SOAPGroup[] processSOAPGroupAndMemberNames(SOAPGroup[] groups)
    {
        for (SOAPGroup group : groups)
        {
            group.setName(group.getName());
            group.setMembers(group.getMembers());
        }

        return groups;
    }


    public static SOAPNestableGroup[] processSOAPGroupAndMemberNames(SOAPNestableGroup[] groups)
    {
        for (SOAPNestableGroup group : groups)
        {
            group.setName(group.getName());
            group.setGroupMembers(group.getGroupMembers());
        }

        return groups;
    }

    public static SOAPRole[] processSOAPRoleAndMemberNames(final SOAPRole[] roles)
    {
        for (SOAPRole role : roles)
        {
            role.setName(role.getName());
            role.setMembers(role.getMembers());
        }

        return roles;
    }

    public static SOAPNestableGroup[] processSOAPNestableGroupAndMemberNames(Collection<SOAPNestableGroup> groups)
    {
        SOAPNestableGroup[] soapGroups = new SOAPNestableGroup[groups.size()];

        Iterator groupIt = groups.iterator();
        for (int ctr = 0; ctr < soapGroups.length && groupIt.hasNext(); ctr++)
        {
            soapGroups[ctr] = processNestableGroup((SOAPNestableGroup) groupIt.next());
        }

        return soapGroups;
    }

    public static SOAPPrincipal[] processUsers(Collection<User> users)
    {
        SOAPPrincipal[] soapUsers = new SOAPPrincipal[users.size()];

        Iterator<User> userIt = users.iterator();
        for (int ctr = 0; ctr < soapUsers.length && userIt.hasNext(); ctr++)
        {
            soapUsers[ctr] = processUser(userIt.next());
        }

        return soapUsers;
    }

    public static SOAPGroup processGroup(Group group, List<String> nestedUserMembers)
    {
        SOAPGroup soapGroup = new SOAPGroup();
        processDirectoryEntity(group, soapGroup);
        soapGroup.setActive(group.isActive());
        soapGroup.setDescription(group.getDescription());
        soapGroup.setMembers(nestedUserMembers.toArray(new String[nestedUserMembers.size()]));

        soapGroup.setAttributes(new SOAPAttribute[0]);

        return soapGroup;
    }

    public static SOAPGroup processGroupWithAttributes(GroupWithAttributes group, List<String> nestedUserMembers)
    {
        SOAPGroup soapGroup = processGroup(group, nestedUserMembers);

        processSOAPEntityAttributes(group, soapGroup);

        return soapGroup;
    }

    public static SOAPNestableGroup processNestableGroup(Group group, List<String> nestedGroupMembers)
    {
        final SOAPNestableGroup soapGroup = new SOAPNestableGroup();

        processDirectoryEntity(group, soapGroup);
        soapGroup.setActive(group.isActive());
        soapGroup.setDescription(group.getDescription());
        soapGroup.setGroupMembers(nestedGroupMembers.toArray(new String[nestedGroupMembers.size()]));

        soapGroup.setAttributes(new SOAPAttribute[0]);

        return soapGroup;
    }

    public static SOAPNestableGroup processNestableGroup(SOAPNestableGroup group)
    {
        SOAPNestableGroup soapGroup = new SOAPNestableGroup();

        soapGroup.setName(group.getName());
        soapGroup.setGroupMembers(group.getGroupMembers());

        return soapGroup;
    }

    private static void appendSOAPAttribute(String name, String value, Set<SOAPAttribute> soapAttributes)
    {
        if (name != null && value != null)
        {
            soapAttributes.add(new SOAPAttribute(name, value));
        }
    }

    private static void appendSOAPAttribute(String name, Set<String> values, Set<SOAPAttribute> soapAttributes)
    {
        if (name != null && values != null)
        {
            soapAttributes.add(new SOAPAttribute(name, values.toArray(new String[values.size()])));
        }
    }

    public static SOAPPrincipal processUser(User user)
    {
        SOAPPrincipal soapUser = new SOAPPrincipal();
        processDirectoryEntity(user, soapUser);
        soapUser.setActive(user.isActive());

        // convert fields from User to SOAPAttributes if they are not null
        Set<SOAPAttribute> soapAttributes = new HashSet<SOAPAttribute>();
        appendSOAPAttribute(FIRSTNAME, user.getFirstName(), soapAttributes);
        appendSOAPAttribute(LASTNAME, user.getLastName(), soapAttributes);
        appendSOAPAttribute(DISPLAYNAME, user.getDisplayName(), soapAttributes);
        appendSOAPAttribute(EMAIL, user.getEmailAddress(), soapAttributes);

        soapUser.setAttributes(soapAttributes.toArray(new SOAPAttribute[soapAttributes.size()]));

        return soapUser;
    }

    public static SOAPPrincipal processUserWithAttributes(UserWithAttributes user)
    {
        SOAPPrincipal soapUser = processUser(user);

        processSOAPEntityAttributes(user, soapUser);

        return soapUser;
    }

    public static UserTemplate processUser(SOAPPrincipal soapUser)
    {
        SOAPPrincipalHelper helper = new SOAPPrincipalHelper();

        UserTemplate user = new UserTemplate(soapUser.getName(), soapUser.getDirectoryId());
        user.setActive(soapUser.isActive());
        user.setFirstName(helper.getFirstName(soapUser));
        user.setLastName(helper.getLastName(soapUser));
        user.setDisplayName(helper.getFullName(soapUser));
        user.setEmailAddress(helper.getEmail(soapUser));

        return user;
    }

    public static SOAPRole processRole(Group role, List<String> directMemberNames)
    {
        SOAPRole soapRole = new SOAPRole();
        soapRole.setName(role.getName());
        soapRole.setDirectoryId(role.getDirectoryId());
        soapRole.setActive(role.isActive());
        soapRole.setDescription(role.getDescription());
        soapRole.setMembers(directMemberNames.toArray(new String[directMemberNames.size()]));

        return soapRole;
    }

    public static GroupTemplate processGroup(SOAPGroup soapGroup)
    {
        GroupTemplate group = new GroupTemplate(soapGroup.getName(), soapGroup.getDirectoryId(), GroupType.GROUP);
        group.setActive(soapGroup.isActive());
        group.setDescription(soapGroup.getDescription());

        return group;
    }

    public static GroupTemplate processRole(SOAPRole soapRole)
    {
        GroupTemplate group = new GroupTemplate(soapRole.getName(), soapRole.getDirectoryId(), GroupType.LEGACY_ROLE);
        group.setActive(soapRole.isActive());
        group.setDescription(soapRole.getDescription());

        return group;
    }

    private static void processDirectoryEntity(DirectoryEntity directoryEntity, SOAPEntity soapEntity)
    {
        soapEntity.setName(directoryEntity.getName());
        soapEntity.setDirectoryId(directoryEntity.getDirectoryId());
    }

    private static void processSOAPEntityAttributes(Attributes entity, SOAPEntity soapEntity)
    {
        // convert attributes from entity to SOAPAttributes if they are not null
        Set<SOAPAttribute> soapAttributes = new HashSet<SOAPAttribute>();
        // getKeys does not retrieve primary attributes from principal - so need to add them first
        soapAttributes.addAll(Arrays.asList(soapEntity.getAttributes()));

        for (String attribute : entity.getKeys())
        {
            appendSOAPAttribute(attribute, entity.getValues(attribute), soapAttributes);
        }

        soapEntity.setAttributes(soapAttributes.toArray(new SOAPAttribute[soapAttributes.size()]));
    }

    public static Map<String, Set<String>> buildUserAttributeMap(SOAPPrincipal soapUser)
    {
        Map<String, Set<String>> attibutues = new HashMap<String, Set<String>>();

        for (int i = 0; i < soapUser.getAttributes().length; i++)
        {
            // only add secondary attributes to the user
            SOAPAttribute soapAttribute = soapUser.getAttributes()[i];
            if (!PRIMARY_USER_ATTRIBUTES.contains(soapAttribute.getName()))
            {
                attibutues.put(soapAttribute.getName(), Sets.newHashSet(soapAttribute.getValues()));
            }
        }
        return attibutues;
    }
}
