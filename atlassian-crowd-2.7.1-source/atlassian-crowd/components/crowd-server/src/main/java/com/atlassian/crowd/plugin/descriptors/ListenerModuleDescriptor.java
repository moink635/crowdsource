package com.atlassian.crowd.plugin.descriptors;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;

/**
 * NOTE: This class has to be constructor injected since it's the only way moduleFactory can be set at its parent.
 */
public class ListenerModuleDescriptor extends AbstractModuleDescriptor<Object> implements StateAware
{
    private EventPublisher eventPublisher;

    public ListenerModuleDescriptor(ModuleFactory moduleFactory, EventPublisher eventPublisher)
    {
        super(moduleFactory);
        this.eventPublisher = eventPublisher;
    }

    public Object getModule()
    {
        return moduleFactory.createModule(moduleClassName, this);
    }

    public void enabled()
    {
        super.enabled();

        Object eventListener = getModule();

        if (eventListener instanceof StateAware)
        {
            StateAware stateAware = (StateAware) eventListener;
            stateAware.enabled();
        }
        eventPublisher.register(eventListener);
    }

    public void disabled()
    {
        final Object eventListener = getModule();

        eventPublisher.unregister(eventListener);

        if (eventListener instanceof StateAware)
        {
            StateAware stateAware = (StateAware) eventListener;
            stateAware.disabled();
        }

        super.disabled();
    }
}
