package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.I18nHelper;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This upgrade task will enable auto-create on auth for existing delegated authentication directories.
 */
public class UpgradeTask422 implements UpgradeTask
{
    private final Collection<String> errors = new ArrayList<String>();

    private PropertyManager propertyManager;
    private I18nHelper i18nHelper;

    public String getBuildNumber()
    {
        return "422";
    }

    public String getShortDescription()
    {
        return "Add default forgotten username mail template";
    }

    public void doUpgrade() throws Exception
    {
        if (needsUpgrade())
        {
            propertyManager.setProperty(Property.FORGOTTEN_USERNAME_EMAIL_TEMPLATE, i18nHelper.getText("mailtemplate.template.forgotten.username.text"));
        }
    }

    private boolean needsUpgrade()
    {
        try
        {
            propertyManager.getProperty(Property.FORGOTTEN_USERNAME_EMAIL_TEMPLATE);
            return false;
        }
        catch (ObjectNotFoundException e)
        {
            return true;
        }
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setPropertyManager(PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    public void setI18nHelper(I18nHelper i18nHelper)
    {
        this.i18nHelper = i18nHelper;
    }
}
