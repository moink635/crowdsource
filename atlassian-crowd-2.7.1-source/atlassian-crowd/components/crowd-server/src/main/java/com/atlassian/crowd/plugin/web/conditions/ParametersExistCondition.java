package com.atlassian.crowd.plugin.web.conditions;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Will check a given Context for the existence of a list of parameter names
 * If all are present, shouldDisplay will return true
 */
public class ParametersExistCondition implements Condition
{
    private List<String> parameterNames = null;

    public void init(Map params) throws PluginParseException
    {
        String parameters = (String) params.get("parameters");

        this.parameterNames = Arrays.asList(parameters.split(","));
    }

    public boolean shouldDisplay(Map context)
    {
        boolean display = false;
        for (String parameterName : parameterNames)
        {
            display = StringUtils.isNotBlank((String) context.get(parameterName));
        }

        return display;
    }
}
