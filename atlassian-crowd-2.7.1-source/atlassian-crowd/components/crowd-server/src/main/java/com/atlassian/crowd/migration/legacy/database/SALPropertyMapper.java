package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.LegacyImportDataHolder;
import com.atlassian.crowd.model.property.Property;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.BadSqlGrammarException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SALPropertyMapper extends DatabaseMapper implements DatabaseImporter
{
    public SALPropertyMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, JdbcOperations jdbcTemplate)
    {
        super(sessionFactory, batchProcessor, jdbcTemplate);
    }

    public void importFromDatabase(LegacyImportDataHolder importData) throws ImportException
    {
        List<Property> salProperties = importSALPropertiesFromDatabase();
        for (Property salProperty : salProperties)
        {
            addEntityViaSave(salProperty);
        }
        logger.info("Successfully migrated " + salProperties.size() + " SAL properties.");
    }

    public List<Property> importSALPropertiesFromDatabase()
    {
        SALPropertyTableMapper salPropertyTableMapper = new SALPropertyTableMapper();
        List<Property> salProperties = new ArrayList<Property>();

        try
        {
            salProperties = jdbcTemplate.query(legacyTableQueries.getSALPropertiesSQL(), salPropertyTableMapper);
        }
        catch (BadSqlGrammarException e)
        {
            // most likely this was caught because SALPROPERTY table does not exist
            logger.info("No sal properties were found for importing!");
        }

        return salProperties;
    }

    private class SALPropertyTableMapper implements RowMapper
    {
        public Object mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            String key = rs.getString("KEY");
            String propertyName = rs.getString("PROPERTYNAME");
            String propertyValue = rs.getString("STRINGVALUE");
            return new Property(PLUGIN_KEY_PREFIX + key, propertyName, propertyValue);
        }
    }
}
