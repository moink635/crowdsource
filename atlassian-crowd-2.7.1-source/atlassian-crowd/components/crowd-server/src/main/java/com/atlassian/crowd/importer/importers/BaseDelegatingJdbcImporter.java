package com.atlassian.crowd.importer.importers;

import java.util.Set;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.config.JdbcConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.model.Result;
import com.atlassian.crowd.manager.directory.DirectoryManager;

import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

/**
 * Base implementation of Jdbc importer which does not perform import operation by itself, but delegate to an appropriate importer implementation.
 */
abstract class BaseDelegatingJdbcImporter implements Importer
{
    protected JdbcOperations jdbcTemplate;

    protected DirectoryManager directoryManager;

    public BaseDelegatingJdbcImporter(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }

    @Override
    public void init(Configuration configuration)
    {
        // Setup the datasource and JDBC template for the given configuration
        setJdbcTemplate((JdbcConfiguration) configuration);
    }

    @Override
    public Class getConfigurationType()
    {
        return JdbcConfiguration.class;
    }

    public void setJdbcTemplate(JdbcConfiguration configuration)
    {
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource(configuration.getDatabaseDriver(), configuration.getDatabaseURL(), configuration.getUsername(), configuration.getPassword());

        jdbcTemplate = new JdbcTemplate(driverManagerDataSource);
    }

    /**
     * Determines the appropriate importer to delegate the import operation to.
     *
     * @param configuration the configuration required for the import.
     * @return appropriate importer implementation.
     * @throws ImporterException if anything happened during the import.
     */
    abstract Importer determineImporter(Configuration configuration) throws ImporterException;

    /**
     * This method will handle the importing of Users, Groups and Memberships from Confluence.
     *
     * @param configuration the configuration required for the import
     * @return success or failure for the import.
     */
    @Override
    public Result importUsersGroupsAndMemberships(Configuration configuration) throws ImporterException
    {
        final Importer importer = determineImporter(configuration);

        return importer.importUsersGroupsAndMemberships(configuration);
    }

    @Override
    public boolean supportsMultipleDirectories(Configuration configuration) throws ImporterException
    {
        final Importer importer = determineImporter(configuration);

        return importer.supportsMultipleDirectories(configuration);
    }

    @Override
    public Set<Directory> retrieveRemoteSourceDirectory(Configuration configuration) throws ImporterException
    {
        final Importer importer = determineImporter(configuration);

        return importer.retrieveRemoteSourceDirectory(configuration);
    }
}