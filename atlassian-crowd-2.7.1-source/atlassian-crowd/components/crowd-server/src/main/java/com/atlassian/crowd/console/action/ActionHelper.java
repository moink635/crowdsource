package com.atlassian.crowd.console.action;

import java.util.List;
import java.util.Map;

import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.extras.api.crowd.CrowdLicense;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;
import com.atlassian.plugin.web.model.WebLink;

public interface ActionHelper
{
    boolean isLicenseExpired();

    boolean isEvaluation();

    boolean isSubscription();

    boolean isWithinGracePeriod();

    boolean isAdmin();

    List<WebSectionModuleDescriptor> getWebSectionsForLocation(String location);

    List<WebItemModuleDescriptor> getWebItemsForSection(String sectionName);

    String getDisplayableLink(WebLink link);

    String getSitemeshPageProperty(String propertyName);

    String getText(String key);

    CrowdLicense getLicense();

    boolean isAuthenticated();

    boolean hasAdminRole(CrowdUserDetails userDetails);

    CrowdBootstrapManager getBootstrapManager();

    UserWithAttributes getRemoteUser();

    Map<String, Object> getWebFragmentsContextMap();

    Map<String, String> getSitemeshPageProperties();
}
