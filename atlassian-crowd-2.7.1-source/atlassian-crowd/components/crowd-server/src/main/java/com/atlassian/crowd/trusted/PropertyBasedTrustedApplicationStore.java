package com.atlassian.crowd.trusted;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.security.auth.trustedapps.DefaultTrustedApplication;
import com.atlassian.security.auth.trustedapps.EncryptionProvider;
import com.atlassian.security.auth.trustedapps.RequestConditions;
import com.atlassian.security.auth.trustedapps.TrustedApplication;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.springframework.transaction.annotation.Transactional;

import static com.google.common.base.Predicates.notNull;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

/**
 * Implementation of {@link com.atlassian.crowd.trusted.TrustedApplicationStore} using {@link com.atlassian.crowd.manager.property.PropertyManager}.
 *
 * @since v2.7
 */
@Transactional
public class PropertyBasedTrustedApplicationStore implements TrustedApplicationStore
{
    private static final String TRUSTED_APPS_PREFIX = "trustedapps.";
    private static final String TRUSTED_APPS_KEYS = TRUSTED_APPS_PREFIX + "keys";
    private static final String PUBLIC_KEY_KEY = "public.key";
    private static final String TIMEOUT_KEY = "timeout";
    private static final String URLS_KEY = "urls";
    private static final String IPS_KEY = "ips";
    private static final String CURRENT_APP_PUBLIC_KEY = TRUSTED_APPS_PREFIX + "currentapp.public.key";
    private static final String CURRENT_APP_PRIVATE_KEY = TRUSTED_APPS_PREFIX + "currentapp.private.key";
    private static final String CURRENT_APP_UID = TRUSTED_APPS_PREFIX + "currentapp.uid";

    private final EncryptionProvider encryptionProvider;
    private final PropertyManager propertyManager;

    /**
     * Lock to guard access to the trusted apps settings, since we have to read a list, modify it and store it back in a non atomic way.
     */
    private final ReadWriteLock settingsLock = new ReentrantReadWriteLock();

    public PropertyBasedTrustedApplicationStore(final EncryptionProvider encryptionProvider, final PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
        this.encryptionProvider = encryptionProvider;
    }

    @Override
    public Iterable<TrustedApplication> getTrustedApplications()
    {
        settingsLock.readLock().lock();
        try
        {
            return filter(transform(trustedAppsIds(), idToTrustedApplication), notNull());
        }
        finally
        {
            settingsLock.readLock().unlock();
        }
    }

    @Override
    public boolean deleteApplication(final String id)
    {
        settingsLock.writeLock().lock();
        try
        {
            final List<String> ids = Lists.newArrayList(trustedAppsIds());
            if (ids.contains(id))
            {
                ids.remove(id);
                storeIds(ids);

                final PropertyAccessor accessor = accessor(id);

                accessor.remove(PUBLIC_KEY_KEY);
                accessor.remove(TIMEOUT_KEY);
                accessor.remove(URLS_KEY);
                accessor.remove(IPS_KEY);
                return true;
            }
        }
        finally
        {
            settingsLock.writeLock().unlock();
        }

        return false;
    }

    @Override
    public void addTrustedApplication(final TrustedApplication app)
    {
        settingsLock.writeLock().lock();
        try
        {
            final List<String> ids = Lists.newArrayList(trustedAppsIds());
            final String id = app.getID();
            if (!ids.contains(id))
            {
                ids.add(id);
                storeIds(ids);
            }

            final PropertyAccessor accessor = accessor(id);
            final RequestConditions conditions = app.getRequestConditions();

            accessor.set(PUBLIC_KEY_KEY, KeyUtils.encode(app.getPublicKey()));
            accessor.set(TIMEOUT_KEY, Long.toString(conditions.getCertificateTimeout()));
            accessor.set(URLS_KEY, iterableToCommaSeparatedString(conditions.getURLPatterns()));
            accessor.set(IPS_KEY, iterableToCommaSeparatedString(conditions.getIPPatterns()));
        }
        finally
        {
            settingsLock.writeLock().unlock();
        }
    }

    @Override
    public TrustedApplication getTrustedApplication(final String id)
    {
        settingsLock.readLock().lock();
        try
        {
            final PropertyAccessor accessor = accessor(id);
            final String publicKey = accessor.get(PUBLIC_KEY_KEY, null);
            if (publicKey == null)
            {
                // If there is no public key, there is no trusted app with this ID
                return null;
            }

            final long timeout = Long.parseLong(accessor.get(TIMEOUT_KEY, "0"));
            final String[] urls = Iterables.toArray(decodeCommaSeparatedString(accessor.get(URLS_KEY, null)), String.class);
            final String[] ips = Iterables.toArray(decodeCommaSeparatedString(accessor.get(IPS_KEY, null)), String.class);
            final RequestConditions requestConditions = RequestConditions
                    .builder()
                    .setCertificateTimeout(timeout)
                    .addURLPattern(urls)
                    .addIPPattern(ips)
                    .build();
            return new DefaultTrustedApplication(encryptionProvider, KeyUtils.decodePublicKey(encryptionProvider,
                    publicKey), id, requestConditions);
        }
        finally
        {
            settingsLock.readLock().unlock();
        }
    }

    // The following 2 methods are both synchronized to avoid returning an inconsistent value
    @Override
    public synchronized InternalCurrentApplication getCurrentApplication()
    {
        final String uid = propertyManager.getString(CURRENT_APP_UID, null);
        final String privateKey = propertyManager.getString(CURRENT_APP_PRIVATE_KEY, null);
        final String publicKey = propertyManager.getString(CURRENT_APP_PUBLIC_KEY, null);

        if (uid == null)
        {
            return null;
        }

        return new InternalCurrentApplication(
                uid,
                privateKey,
                publicKey);
    }

    @Override
    public synchronized void storeCurrentApplication(final InternalCurrentApplication currentApplication)
    {
        propertyManager.setProperty(CURRENT_APP_UID, currentApplication.getUid());
        propertyManager.setProperty(CURRENT_APP_PRIVATE_KEY, currentApplication.getPrivateKey());
        propertyManager.setProperty(CURRENT_APP_PUBLIC_KEY, currentApplication.getPublicKey());
    }

    private Iterable<String> trustedAppsIds()
    {
        final String keys = propertyManager.getString(TRUSTED_APPS_KEYS, "");
        return decodeCommaSeparatedString(keys);
    }

    @VisibleForTesting
    static Iterable<String> decodeCommaSeparatedString(final String str)
    {
        if (str == null)
        {
            return ImmutableList.of();
        }
        return transform(Splitter.on('\n').omitEmptyStrings().split(str), DECODER);
    }

    @VisibleForTesting
    static String iterableToCommaSeparatedString(final Iterable<String> iterable)
    {
        return Joiner.on('\n').join(transform(iterable, ENCODER));
    }

    private void storeIds(Iterable<String> ids)
    {
        if (Iterables.isEmpty(ids))
        {
            propertyManager.removeProperty(TRUSTED_APPS_KEYS);
        }
        else
        {
            propertyManager.setProperty(TRUSTED_APPS_KEYS, iterableToCommaSeparatedString(ids));
        }
    }

    /*
     * Used to turn lists into string and back.
     */
    private static final Function<String, String> ENCODER = new Function<String, String>()
    {
        @Override
        public String apply(final String value)
        {
            return value.replace("\\", "\\\\").replace("\n", "\\n");
        }
    };

    private static final Function<String, String> DECODER = new Function<String, String>()
    {
        @Override
        public String apply(final String value)
        {
            return value.replace("\\n", "\n").replace("\\\\", "\\");
        }
    };

    private PropertyAccessor accessor(final String id)
    {
        return new PropertyAccessor(id);
    }

    private class PropertyAccessor
    {
        private final String id;

        private PropertyAccessor(final String id)
        {
            this.id = id;
        }

        public String get(final String property, final String defaultValue)
        {
            return propertyManager.getString(propertyKey(property), defaultValue);
        }

        public void set(final String property, final String value)
        {
            propertyManager.setProperty(propertyKey(property), value);
        }

        public void remove(final String property)
        {
            propertyManager.removeProperty(propertyKey(property));
        }

        private String propertyKey(final String property)
        {
            return TRUSTED_APPS_PREFIX + id + "." + property;
        }
    }

    private final Function<String, TrustedApplication> idToTrustedApplication = new Function<String, TrustedApplication>()
    {
        @Override
        public TrustedApplication apply(final String id)
        {
            return getTrustedApplication(id);
        }
    };
}
