package com.atlassian.crowd.migration.legacy.database.sql;

public interface LegacyTableQueries
{
    // ------------------------
    // Verifiers
    // ------------------------
    String getVerifyGroupsSQL();

    String getVerifyRolesSQL();


    // ------------------------
    // Directory
    // ------------------------
    String getDirectoriesSQL();

    String getDirectoryOperationsSQL();

    String getDirectoryAttributesSQL();


    // ------------------------
    // User
    // ------------------------
    String getInternalEntityTemplatesForUsersSQL();

    String getUserCredentialsSQL();

    String getAllUserAttributesSQL();

    String getUserCredentialsHistorySQL();


    // ------------------------
    // Group
    // ------------------------
    String getGroupsSQL();

    String getGroupAttributesSQL();

    String getGroupMembershipsSQL();

    // ------------------------
    // Role
    // ------------------------
    String getRolesSQL();

    String getRoleAttributesSQL();

    String getRoleMembershipsSQL();

    // ------------------------
    // Application
    // ------------------------
    String getApplicationsSQL();

    String getDirectoryMappingDataSQL();

    String getRemoteAddressesSQL();

    String getApplicationPasswordCredentialsSQL();

    String getApplicationAttributesSQL();

    String getAllowedOperationsSQL();

    String getAssociatedGroupsSQL();

    // ------------------------
    // Property
    // ------------------------
    String getPropertiesSQL();

    // ------------------------
    // SAL Property
    // ------------------------
    String getSALPropertiesSQL();
}
