package com.atlassian.crowd.migration.legacy.database.sql;
import com.atlassian.crowd.migration.legacy.database.sql.MySQLLegacyTableQueries;


/**
 * Extends {@link GenericLegacyTableQueries} and overrides where SQL Server requires
 * different syntax.
 */
public class SqlServerLegacyTableQueries extends GenericLegacyTableQueries
{
    public String getDirectoryOperationsSQL()
    {
        return "SELECT DIRECTORYID, DIRECTORYPERMISSIONS.[KEY] "
            + "FROM DIRECTORYPERMISSIONS "
            + "WHERE VALUE = 'true'";
    }

    public String getUserCredentialsHistorySQL()
    {
        return "SELECT REMOTEPRINCIPALDIRECTORYID, REMOTEPRINCIPALNAME, CREDENTIAL "
            + "FROM PRINCIPALCREDENTIALHISTORY "
            + "ORDER BY PRINCIPALCREDENTIALHISTORY.[INDEX]";
    }
    
    public String getAllowedOperationsSQL()
    {
        return "SELECT APPLICATIONID, DIRECTORYID, PERMISSION_TYPE "
            + "FROM APPLICATIONDIRECTORYPERMISSION "
            + "WHERE ALLOWED = 1";
    }
    
    public String getAssociatedGroupsSQL()
    {
        return "SELECT APPLICATIONID, DIRECTORYID, NAME "
            + "FROM APPLICATIONGROUPS "
            + "WHERE ACTIVE = 1";
    }
    
    public String getSALPropertiesSQL()
    {
        return "SELECT SALPROPERTY.[KEY], PROPERTYNAME, STRINGVALUE FROM SALPROPERTY";
    }
}
