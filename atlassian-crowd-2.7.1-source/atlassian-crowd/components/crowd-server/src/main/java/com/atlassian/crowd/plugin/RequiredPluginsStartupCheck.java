package com.atlassian.crowd.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginState;
import com.atlassian.plugin.metadata.RequiredPluginProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RequiredPluginsStartupCheck
{
    private static final Logger log = LoggerFactory.getLogger(RequiredPluginsStartupCheck.class);

    private final PluginAccessor accessor;
    private final RequiredPluginProvider pmm;

    public RequiredPluginsStartupCheck(PluginAccessor accessor, RequiredPluginProvider pmm)
    {
        this.accessor = accessor;
        this.pmm = pmm;
    }

    public List<String> requiredPluginsNotEnabled()
    {
        List<String> notEnabled = new ArrayList<String>();

        Collection<Plugin> plugins = accessor.getPlugins();

        if (plugins.isEmpty())
        {
            log.warn("No plugins present.");
        }

        Set<String> seen = new HashSet<String>();

        log.debug("Listing all plugins...");
        for (Plugin p : plugins)
        {
            log.debug("Plugin: {}", p);
            if (isRequired(p) && p.getPluginState() != PluginState.ENABLED)
            {
                log.error("Required plugin is disabled: "  + p);

                String name = p.getName();
                if (name != null)
                {
                    notEnabled.add(name);
                }
                else
                {
                    notEnabled.add(p.toString());
                }
            }
            seen.add(p.getKey());
        }
        log.debug("...done");

        Set<String> expected = new HashSet<String>(pmm.getRequiredPluginKeys());
        expected.removeAll(seen);

        for (String s : expected)
        {
            log.error("Required plugin is missing: " + s);
            notEnabled.add(s);
        }

        return notEnabled;
    }

    boolean isRequired(Plugin p)
    {
        return pmm.getRequiredPluginKeys().contains(p.getKey());
    }
}
