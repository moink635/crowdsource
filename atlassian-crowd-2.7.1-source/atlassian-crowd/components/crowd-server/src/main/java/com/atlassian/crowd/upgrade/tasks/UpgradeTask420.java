package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.DelegatedAuthenticationDirectory;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.upgrade.util.DirectoryImplementationsHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * This upgrade task will migrate class names from the old com.atlassian.crowd.integration.directory.connector package
 * to the com.atlassian.crowd.directory package
 */
public class UpgradeTask420 implements UpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask420.class);

    // not making this a constant (static) so that it can be GCed
    private final Map<String, String> directoryImplementations = DirectoryImplementationsHelper.getDirectoryImplementations();
    private final Collection<String> errors = new ArrayList<String>();

    private DirectoryDao directoryDao;

    public String getBuildNumber()
    {
        return "420";
    }

    public String getShortDescription()
    {
        return "Upgrading directory class implementation names";
    }

    public void doUpgrade() throws Exception
    {
        for (Directory directory : getAllDirectories())
        {
            if (isUpgradableDirectory(directory))
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Upgrading directory " + directory);
                }
                try
                {
                    updateDirectory(directory);
                }
                catch (DataAccessException e)
                {
                    final String errorMessage = "Could not update directory " + directory;
                    log.error(errorMessage, e);
                    errors.add(errorMessage + ", error is " + e.getMessage());
                }
            }
            else
            {
                if (log.isDebugEnabled())
                {
                    log.debug("Directory " + directory + " will not be upgraded.");
                }
            }
        }
    }

    private void updateDirectory(Directory directory) throws DataAccessException, DirectoryNotFoundException
    {
        DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
        
        directoryToUpdate.setImplementationClass(getNewImplementationClass(directory.getImplementationClass()));

        // If it is a delegated directory, update the attribute as well (EMBCWD-434)
        if (directory.getType().equals(DirectoryType.DELEGATING))
        {
            String delegatedDirectoryType = directory.getValue(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS);
            directoryToUpdate.setAttribute(DelegatedAuthenticationDirectory.ATTRIBUTE_LDAP_DIRECTORY_CLASS, getNewImplementationClass(delegatedDirectoryType));
        }

        directoryDao.update(directoryToUpdate);
    }

    private String getNewImplementationClass(String implementationClass)
    {
        return directoryImplementations.get(implementationClass);
    }

    private boolean isUpgradableDirectory(Directory directory)
    {
        return directoryImplementations.containsKey(directory.getImplementationClass());
    }

    private List<Directory> getAllDirectories()
    {
        return directoryDao.search(getAllDirectoriesQuery());
    }

    private EntityQuery<Directory> getAllDirectoriesQuery()
    {
        // We're getting all the results in memory here which should be ok as far as we know.
        // There shouldn't be that many directories in a single crowd instance.
        return QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS);
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }
}
