package com.atlassian.crowd.trusted;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Collection;

import com.atlassian.security.auth.trustedapps.Application;
import com.atlassian.security.auth.trustedapps.ApplicationRetriever.RetrievalException;
import com.atlassian.security.auth.trustedapps.CurrentApplication;
import com.atlassian.security.auth.trustedapps.DefaultCurrentApplication;
import com.atlassian.security.auth.trustedapps.DefaultTrustedApplication;
import com.atlassian.security.auth.trustedapps.EncryptionProvider;
import com.atlassian.security.auth.trustedapps.RequestConditions;
import com.atlassian.security.auth.trustedapps.TrustedApplication;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsConfigurationManager;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;
import com.atlassian.util.concurrent.LazyReference;

import com.google.common.collect.ImmutableList;

/**
 * Crowd implementation of {@link com.atlassian.security.auth.trustedapps.TrustedApplicationsManager} and {@link com.atlassian.security.auth.trustedapps.TrustedApplicationsConfigurationManager}.
 *
 * @since 2.7
 */
public class CrowdTrustedApplicationsManager implements TrustedApplicationsManager, TrustedApplicationsConfigurationManager
{
    private final EncryptionProvider encryptionProvider;
    private final TrustedApplicationStore trustedApplicationStore;
    // This needs the database, so cannot be done in the constructor. LazyRef ensures this will be executed only once.
    private final LazyReference<CurrentApplication> currentApplication = new LazyReference<CurrentApplication>()
    {
        @Override
        protected CurrentApplication create() throws Exception
        {
            InternalCurrentApplication internalCurrentApplication = getInternalCurrentApplication();
            return new DefaultCurrentApplication(
                    encryptionProvider,
                    KeyUtils.decodePublicKey(encryptionProvider, internalCurrentApplication.getPublicKey()),
                    KeyUtils.decodePrivateKey(encryptionProvider, internalCurrentApplication.getPrivateKey()),
                    internalCurrentApplication.getUid());
        }
    };

    public CrowdTrustedApplicationsManager(final EncryptionProvider encryptionProvider,
                                           final TrustedApplicationStore trustedApplicationStore)
    {
        this.encryptionProvider = encryptionProvider;
        this.trustedApplicationStore = trustedApplicationStore;
    }

    @Override
    public CurrentApplication getCurrentApplication()
    {
        return currentApplication.get();
    }

    @Override
    public synchronized TrustedApplication getTrustedApplication(final String id)
    {
        return trustedApplicationStore.getTrustedApplication(id);
    }

    @Override
    public Application getApplicationCertificate(final String url) throws RetrievalException
    {
        return encryptionProvider.getApplicationCertificate(url);
    }

    @Override
    public TrustedApplication addTrustedApplication(final Application app, final RequestConditions conditions)
    {
        final TrustedApplication trustedApp = new DefaultTrustedApplication(
            encryptionProvider,
            app.getPublicKey(),
            app.getID(),
            conditions);
        trustedApplicationStore.addTrustedApplication(trustedApp);
        return trustedApp;
    }

    @Override
    public Collection<TrustedApplication> getTrustedApplications()
    {
        return ImmutableList.copyOf(trustedApplicationStore.getTrustedApplications());
    }

    @Override
    public boolean deleteApplication(final String id)
    {
        return trustedApplicationStore.deleteApplication(id);
    }

    private InternalCurrentApplication getInternalCurrentApplication() throws NoSuchProviderException, NoSuchAlgorithmException
    {
        InternalCurrentApplication internalCurrentApplication = trustedApplicationStore.getCurrentApplication();
        if (internalCurrentApplication == null)
        {
            final KeyPair keyPair = encryptionProvider.generateNewKeyPair();
            internalCurrentApplication = new InternalCurrentApplication(
                    encryptionProvider.generateUID(),
                    KeyUtils.encode(keyPair.getPrivate()),
                    KeyUtils.encode(keyPair.getPublic()));
            trustedApplicationStore.storeCurrentApplication(internalCurrentApplication);
        }
        return internalCurrentApplication;
    }
}
