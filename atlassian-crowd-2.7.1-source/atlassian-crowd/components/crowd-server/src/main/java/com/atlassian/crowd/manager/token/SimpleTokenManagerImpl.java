package com.atlassian.crowd.manager.token;

import java.util.Date;

import com.atlassian.crowd.dao.token.SessionTokenStorage;
import com.atlassian.crowd.exception.ObjectAlreadyExistsException;
import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.model.token.Token;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * A TokenManager that simply delegates to a single SessionTokenStorage.
 *
 * @since v2.7
 */
@Transactional
public class SimpleTokenManagerImpl implements TokenManager
{
    private final SessionTokenStorage tokenDAO;

    public SimpleTokenManagerImpl(SessionTokenStorage tokenDAO)
    {
        this.tokenDAO = tokenDAO;
    }

    @Override
    public Token findByRandomHash(String randomHash) throws ObjectNotFoundException
    {
        return tokenDAO.findByRandomHash(randomHash);
    }

    @Override
    public Token findByIdentifierHash(String identifierHash) throws ObjectNotFoundException
    {
        return tokenDAO.findByIdentifierHash(identifierHash);
    }

    // if the insertion fails because the ObjectAlreadyExists, then rollback the nested transaction to
    // give the caller a chance to recover from the problem and continue using the outer transaction (CWD-3688)
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = ObjectAlreadyExistsException.class)
    public Token add(Token token) throws ObjectAlreadyExistsException
    {
        return tokenDAO.add(token);
    }

    @Override
    public Token update(Token token) throws ObjectNotFoundException
    {
        return tokenDAO.update(token);
    }

    @Override
    public void remove(Token token)
    {
        tokenDAO.remove(token);
    }

    @Override
    public void remove(long directoryId, String name)
    {
        tokenDAO.remove(directoryId, name);
    }

    @Override
    public void removeExcept(long directoryId, String name, String exclusionToken)
    {
        tokenDAO.removeExcept(directoryId, name, exclusionToken);
    }

    @Override
    public void removeAll(long directoryId)
    {
        tokenDAO.removeAll(directoryId);
    }

    @Override
    public void removeExpiredTokens(Date currentTime, long maxLifeSeconds)
    {
        tokenDAO.removeExpiredTokens(currentTime, maxLifeSeconds);
    }

    @Override
    public void removeAll()
    {
        tokenDAO.removeAll();
    }
}
