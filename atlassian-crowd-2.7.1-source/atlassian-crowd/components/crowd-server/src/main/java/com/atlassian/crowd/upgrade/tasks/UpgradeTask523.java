package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.upgrade.util.UpgradeUtilityDAOHibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;

public class UpgradeTask523 implements UpgradeTask
{

    private static final Logger log = LoggerFactory.getLogger(UpgradeTask523.class);

    private UpgradeUtilityDAOHibernate upgradeUtilityDao;

    @Override
    public String getBuildNumber()
    {
        return "523";
    }

    @Override
    public String getShortDescription()
    {
        return "Designate pre-existing groups in delegated authentication directories as being local";
    }

    @Override
    public void doUpgrade() throws Exception
    {
        int groupsUpdated = upgradeUtilityDao.executeBulkUpdate("update InternalGroup g set local = true where g.directory in (select d from DirectoryImpl d where type = ?1)", DirectoryType.DELEGATING);
        log.info("{} groups updated", groupsUpdated);
    }

    @Override
    public Collection<String> getErrors()
    {
        return Collections.emptySet();
    }

    public void setUpgradeUtilityDao(UpgradeUtilityDAOHibernate upgradeUtilityDao)
    {
        this.upgradeUtilityDao = upgradeUtilityDao;
    }

}
