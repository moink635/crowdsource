package com.atlassian.crowd.manager.license;

import com.atlassian.extras.api.crowd.CrowdLicense;


public interface CrowdLicenseManager
{
    /**
     * This will retrieve the license for the currently running Crowd instance.
     * @return com.atlassian.license.License or null if not set.
     */
    CrowdLicense getLicense();

    /**
     * This will store the license in the LicenseStore for the currently running Crowd instance.
     */
    CrowdLicense storeLicense(String license);

    /**
     * This will evaluate against the current license stored in Crowd LicenseStore </p>
     * Will return true if the License has not gone beyond its resource total and if the user
     * is not running an evaluation version and it has not expired. This is an overriding method to @see isLicenseValid(License license),
     * so will call getLicense() internally.
     * @return true if License has <strong>not</strong> gone beyond resource limit or is not an evalution and
     * is expired.
     */
    boolean isLicenseValid();

    /**
     * Same logic as {@link CrowdLicenseManager#isLicenseValid()}
     */
    boolean isLicenseValid(CrowdLicense license);

    /**
     * Will validate that a given License key is valid
     * @param key the String to validate
     * @return true if an only if the license is valid, otherwise false
     */
    boolean isLicenseKeyValid(String key);

    /**
     * Checks if a license key valid for set up.
     *
     * In particular, it does not check the current user count
     * as there are no users configured when initially
     * entering a license in the setup process.
     *
     * @param key the String to validate.
     * @return true if an only if the license is valid, otherwise false.
     */
    boolean isSetupLicenseKeyValid(String key);

    /**
     * This will calculate the number of unique Principals currently being used by Crowd. Please note: this is an expensive call
     * and is actually stored in the database by a Quartz process {@link LicenseResourceJob}.
     * Please use consider using {@link com.atlassian.crowd.manager.property.PropertyManager#getCurrentLicenseResourceTotal()}
     * Note: Do not call this method from the web layer, as this is wrapped in a Spring managed transaction
     * @return the number of users/principals.
     * @throws CrowdLicenseManagerException
     */
    int getCurrentResourceUsageTotal() throws CrowdLicenseManagerException;

    /**
     * Calculates the percentage of currentResourceCount over the user limit on a license
     * and returns true or false if the percentage is above the passed in limit.
     * @param limit the percentage limit to test against
     * @param currentResourceCount the current number of resources in the system.
     * @return true if over limit, false otherwise.
     */
    boolean isResourceTotalOverLimit(float limit, int currentResourceCount);

    /**
     * Return's true if the current Build is within the maintenance period for the Crowd license
     * @param license
     */
    boolean isBuildWithinMaintenancePeriod(CrowdLicense license);

    boolean isBuildWithinMaintenancePeriod(String key);
}
