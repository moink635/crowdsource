package com.atlassian.crowd.importer.manager;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.exceptions.ImporterConfigurationException;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.model.Result;

import java.util.List;
import java.util.Set;

/**
 * Handles the importing of users, groups and memberships into Crowd.
 */
public interface ImporterManager
{
    /**
     * A list of supported applications
     * @return Set<String>'s representing each application
     */
    Set<String> getSupportedApplications();

    /**
     * A list of Atlassian supported applications
     * @return Set<String>'s representing each application
     */
    Set<String> getAtlassianSupportedApplications();

    /**
     * Will perform the import based on the given <code>configuration</code> into Crowd.
     * @param configuration The {@see Configuration} for the import
     * @return result This will contain details about the import process
     * @throws com.atlassian.crowd.importer.exceptions.ImporterException will be thrown if an error occurs connecting
     * or importing the users into Crowd, or if the provided <code>configuration</code> is invalid.
     */
    Result performImport(Configuration configuration) throws ImporterException;

    /**
     * Returns true if the source application supports multiple directories such as if it is crowdified.
     *
     * @param configuration the import configuration.
     * @return true
     * @throws com.atlassian.crowd.importer.exceptions.ImporterException will be thrown if an error occurs connecting,
     * or if the provided <code>configuration</code> is invalid.
     */
    boolean supportsMultipleDirectories(Configuration configuration) throws ImporterException;

    /**
     * The retrieve the set of remote source directories.
     *
     * @param configuration the import configuration.
     * @return the set of source directories.
     * @throws com.atlassian.crowd.importer.exceptions.ImporterException will be thrown if an error occurs connecting,
     * or if the provided <code>configuration</code> is invalid.
     */
    Set<Directory> retrieveRemoteSourceDirectories(Configuration configuration) throws ImporterException;

    /**
     * Tests that the connection succeeded or failed based on the provided configuration
     * @param configuration the configuration to use with the importer 
     * @throws ImporterConfigurationException thrown if there is an error connecting to the external datasource
     */
    void testConfiguration(Configuration configuration) throws ImporterConfigurationException;
}
