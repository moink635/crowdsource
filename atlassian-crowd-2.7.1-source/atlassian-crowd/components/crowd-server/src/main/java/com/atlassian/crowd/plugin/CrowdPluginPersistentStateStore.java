package com.atlassian.crowd.plugin;

import com.atlassian.crowd.manager.property.PluginPropertyManager;
import com.atlassian.plugin.manager.DefaultPluginPersistentState;
import com.atlassian.plugin.manager.PluginPersistentState;
import com.atlassian.plugin.manager.PluginPersistentStateStore;

import java.util.HashMap;
import java.util.Map;

/**
 * Persists into the DB via the {@link PluginPropertyManager} the state of Crowd's plugins
 * <p/>
 * It is crucial that the second-level cache in Hibernate for the {@link com.atlassian.crowd.model.property.Property}
 * is enabled for this class to work in an efficiently.
 */
public class CrowdPluginPersistentStateStore implements PluginPersistentStateStore
{
    private final PluginPropertyManager pluginPropertyManager;
    protected static final String GLOBAL_PLUGIN_STATE_PREFIX = "crowd.state.store.";

    public CrowdPluginPersistentStateStore(final PluginPropertyManager pluginPropertyManager)
    {
        this.pluginPropertyManager = pluginPropertyManager;
    }

    /**
     * This method is synchronised so that you don't have two people trying to save plugin state
     * at the same time
     */
    public synchronized void save(final PluginPersistentState state)
    {
        final Map<String, Boolean> map = state.getMap();
        for (final Map.Entry<String, Boolean> entry : map.entrySet())
        {
            pluginPropertyManager.setProperty(GLOBAL_PLUGIN_STATE_PREFIX, entry.getKey(), entry.getValue().toString());
        }
    }

    public PluginPersistentState load()
    {
        Map<String, String> properties = pluginPropertyManager.findAll(GLOBAL_PLUGIN_STATE_PREFIX);

        final Map<String, Boolean> stateMap = new HashMap<String, Boolean>();

        for (final Map.Entry<String, String> property : properties.entrySet())
        {
            stateMap.put(property.getKey(), Boolean.valueOf(property.getValue()));
        }

        return new DefaultPluginPersistentState(stateMap);
    }
}
