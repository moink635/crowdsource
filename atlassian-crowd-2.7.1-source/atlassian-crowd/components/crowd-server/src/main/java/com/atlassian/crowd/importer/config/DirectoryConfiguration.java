package com.atlassian.crowd.importer.config;

import com.atlassian.crowd.importer.exceptions.ImporterConfigurationException;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Configuration specific to the movement of users/groups/memberships
 * from one RemoteDirectory to another.
 */
public class DirectoryConfiguration extends Configuration
{
    /**
     * The target directory to place groups/users/memberships
     */
    private Long sourceDirectoryID;

    /**
     * The name of the 'application' as referenced in the applicationContext-CrowdImporter.xml
     */
    private static final String APPLICATION_NAME = "directory";

    public DirectoryConfiguration(Long directoryID, Long sourceDirectoryID, Boolean overwriteTarget)
    {
        super(directoryID, APPLICATION_NAME, Boolean.TRUE, overwriteTarget);
        this.sourceDirectoryID = sourceDirectoryID;
    }

    public Long getSourceDirectoryID()
    {
        return sourceDirectoryID;
    }

    public void setSourceDirectoryID(Long sourceDirectoryID)
    {
        this.sourceDirectoryID = sourceDirectoryID;
    }

    public void isValid() throws ImporterConfigurationException
    {
        super.isValid();

        if (this.getDirectoryID().equals(getSourceDirectoryID()))
        {
            throw new ImporterConfigurationException("The source and target directories cannot be the same");
        }
    }

    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (!(o instanceof DirectoryConfiguration))
        {
            return false;
        }
        if (!super.equals(o))
        {
            return false;
        }

        DirectoryConfiguration that = (DirectoryConfiguration) o;

        if (overwriteTarget != null ? !overwriteTarget.equals(that.overwriteTarget) : that.overwriteTarget != null)
        {
            return false;
        }
        if (sourceDirectoryID != null ? !sourceDirectoryID.equals(that.sourceDirectoryID) : that.sourceDirectoryID != null)
        {
            return false;
        }

        return true;
    }

    public int hashCode()
    {
        int result = super.hashCode();
        result = 31 * result + (sourceDirectoryID != null ? sourceDirectoryID.hashCode() : 0);
        result = 31 * result + (overwriteTarget != null ? overwriteTarget.hashCode() : 0);
        return result;
    }

    public String toString()
    {
        return new ToStringBuilder(this).
                appendSuper(super.toString()).
                append("Target Directory ID", sourceDirectoryID).
                append("Override Target", overwriteTarget).
                toString();
    }

}
