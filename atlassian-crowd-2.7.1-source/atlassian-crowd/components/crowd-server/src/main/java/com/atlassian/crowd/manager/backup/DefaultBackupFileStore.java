package com.atlassian.crowd.manager.backup;

import com.atlassian.config.HomeLocator;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Ordering;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.atlassian.crowd.CrowdConstants.CrowdHome.BACKUPS_LOCATION;
import static com.google.common.base.Predicates.notNull;
import static com.google.common.collect.Iterables.filter;

/**
 * Default implementation of {@link BackupFileStore}.
 *
 * @since v2.7
 */
public class DefaultBackupFileStore implements BackupFileStore
{
    private static final Logger log = LoggerFactory.getLogger(DefaultBackupFileStore.class);
    private static final int MAX_BACKUPS = 50;
    private final HomeLocator homeLocator;

    public DefaultBackupFileStore(final HomeLocator homeLocator)
    {
        this.homeLocator = homeLocator;
    }

    @Override
    public void cleanUpAutomatedBackupFiles()
    {
        final List<File> backupFiles = getBackupFiles();

        if (backupFiles.size() > MAX_BACKUPS)
        {
            final int numFilesToDelete = backupFiles.size() - MAX_BACKUPS;
            log.info("Maximum number of backup files exceeded. Deleting the {} oldest file(s)", numFilesToDelete);
            final List<File> toDelete = backupFiles.subList(0, numFilesToDelete);
            for (File backupFile : toDelete)
            {
                deleteFile(backupFile);
            }
        }
    }

    @Override
    public File getBackupDirectory()
    {
        String homeDir = homeLocator.getHomePath();

        return new File(homeDir, BACKUPS_LOCATION);
    }

    @Override
    public List<File> getBackupFiles()
    {
        Iterable<File> backupFiles = filter(filesInBackupDirectory(), isAutomatedBackup());

        return Ordering.natural().onResultOf(toTimeStamp()).sortedCopy(backupFiles);
    }

    @Override
    public Date extractTimestamp(File file)
    {
        return toTimeStamp().apply(file);
    }

    protected Function<File, Date> toTimeStamp()
    {
        return new FileToTimestampTransformer();
    }

    protected Predicate<File> isAutomatedBackup()
    {
        return Predicates.compose(notNull(), toTimeStamp());
    }

    protected void deleteFile(final File file)
    {
        if (file.delete())
        {
            log.info("Deleted backup file {}", file.getName());
        }
        else
        {
            log.error("Could not delete backup file: {}", file.getName());
        }
    }

    protected Iterable<File> filesInBackupDirectory()
    {
        final File[] files = getBackupDirectory().listFiles();
        return files == null ? ImmutableList.<File>of() : Arrays.asList(files);
    }
}
