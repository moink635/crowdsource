package com.atlassian.crowd.migration;

import com.atlassian.crowd.migration.legacy.XmlMapper;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.ResourceLocator;
import com.atlassian.crowd.util.PropertyUtils;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

/**
 * Mapper implementation that will take the values from a Crowd instances <code>crowd.properties</code>
 * and place this into Crowd XML export/import
 */
public class CrowdPropertiesMapper extends XmlMapper implements Mapper
{
    protected static final String PROPERTIES_XML_ROOT = "crowdproperties";
    protected static final String PROPERTY_XML_NODE = "property";
    protected static final String PROPERTY_XML_NAME = "name";
    protected static final String PROPERTY_XML_VALUE = "value";

    protected final ClientProperties clientProperties;
    private final ResourceLocator resourceLocator;
    private final PropertyUtils propertyUtils;

    public CrowdPropertiesMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, ClientProperties clientProperties, ResourceLocator resourceLocator, PropertyUtils propertyUtils)
 	{
 	    super(sessionFactory, batchProcessor);
 	    this.clientProperties = clientProperties;
 	    this.resourceLocator = resourceLocator;
 	    this.propertyUtils = propertyUtils;
 	}

    public Element exportXml(Map options) throws ExportException
    {
        // Create the crowd properties element
        Element crowdPropertiesRoot = DocumentHelper.createElement(PROPERTIES_XML_ROOT);

        Properties properties = resourceLocator.getProperties();

        if (properties != null && !properties.isEmpty())
        {
            for (Iterator iterator = properties.keySet().iterator(); iterator.hasNext();)
            {
                String propertyKey = (String) iterator.next();

                String propertyValue = properties.getProperty(propertyKey);

                Element propertyElement = crowdPropertiesRoot.addElement(PROPERTY_XML_NODE);
                propertyElement.addElement(PROPERTY_XML_NAME).addText(propertyKey);
                propertyElement.addElement(PROPERTY_XML_VALUE).addText(propertyValue);
            }
        }
        else
        {
            logger.error("Failed to find any properties in crowd.properties!");
        }

        return crowdPropertiesRoot;
    }

    public void importXml(Element root) throws ImportException
    {
        Element propertiesElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + PROPERTIES_XML_ROOT);
        if (propertiesElement == null)
        {
            logger.info("No properties were found for importing.");
            return;
        }

        for (Iterator properties = propertiesElement.elementIterator(); properties.hasNext();)
        {
            Element propertyElemet = (Element) properties.next();
            String name = propertyElemet.element(PROPERTY_XML_NAME).getText();
            String value = propertyElemet.element(PROPERTY_XML_VALUE).getText();

            // Now add/set this property to crowd.properties
            propertyUtils.updateProperty(resourceLocator.getResourceLocation(), name, value);
        }

        // Now update the SecurityServerClient with the new properties
        clientProperties.updateProperties(resourceLocator.getProperties());
    }
}
