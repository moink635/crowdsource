package com.atlassian.crowd.util;

import java.util.Locale;
import java.util.ResourceBundle;

import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.Resourced;
import com.atlassian.plugin.Resources;
import com.atlassian.plugin.elements.ResourceDescriptor;
import com.atlassian.plugin.event.PluginEventListener;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.plugin.event.events.PluginDisabledEvent;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.util.concurrent.ResettableLazyReference;

import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;

import static com.google.common.base.Predicates.notNull;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

/**
 * Implementation of {@link ResourceBundleProvider} that looks for resource bundles defined in plugins.
 *
 * @since v2.7
 */
public class PluginResourceBundleProvider implements ResourceBundleProvider
{
    private static final Predicate<ResourceDescriptor> I18N_FILTER = new Resources.TypeFilter("i18n");

    private final PluginAccessor pluginAccessor;
    private final Locale locale;
    private final PluginEventManager pluginEventManager;
    private final ResettableLazyReference<Iterable<ResourceBundle>> bundles = new ResettableLazyReference<Iterable<ResourceBundle>>()
    {
        @Override
        protected Iterable<ResourceBundle> create() throws Exception
        {
            return loadResourceBundles();
        }
    };

    public PluginResourceBundleProvider(final PluginAccessor pluginAccessor, final Locale locale, final PluginEventManager pluginEventManager)
    {
        this.pluginAccessor = pluginAccessor;
        this.locale = locale;
        this.pluginEventManager = pluginEventManager;
    }

    public void init()
    {
        pluginEventManager.register(this);
    }

    public void destroy()
    {
        pluginEventManager.unregister(this);
    }

    @Override
    public Iterable<ResourceBundle> getResourceBundles()
    {
        return bundles.get();
    }

    @PluginEventListener
    public void onPluginEnabled(PluginEnabledEvent event)
    {
        bundles.reset();
    }

    @PluginEventListener
    public void onPluginDisabled(PluginDisabledEvent event)
    {
        bundles.reset();
    }

    private Iterable<ResourceBundle> loadResourceBundles()
    {
        ImmutableList.Builder<ResourceBundle> resources = ImmutableList.builder();
        for (Plugin plugin : pluginAccessor.getEnabledPlugins())
        {
            // Where's my flatMap()?
            resources.addAll(getResourceBundlesForPlugin(plugin));
        }
        return resources.build();
    }

    private Iterable<ResourceBundle> getResourceBundlesForPlugin(final Plugin plugin)
    {
        ImmutableList.Builder<ResourceBundle> resources = ImmutableList.builder();

        // first locate i18n resources declared in the plugin scope
        resources.addAll(getResourceBundlesForResourced(plugin, plugin));
        // then locate i18n resources declared within plugin modules
        for (ModuleDescriptor<?> moduleDescriptor : plugin.getModuleDescriptors())
        {
            resources.addAll(getResourceBundlesForResourced(plugin, moduleDescriptor));
        }

        return resources.build();
    }

    private Iterable<ResourceBundle> getResourceBundlesForResourced(final Plugin plugin, final Resourced resourced)
    {
        // I want FluentIterable...
        return filter(transform(filter(resourced.getResourceDescriptors(), I18N_FILTER), toResourceBundle(plugin)), notNull());
    }

    private Function<ResourceDescriptor, ResourceBundle> toResourceBundle(final Plugin plugin)
    {
        return new Function<ResourceDescriptor, ResourceBundle>()
        {
            @Override
            public ResourceBundle apply(final ResourceDescriptor descriptor)
            {
                return ResourceBundle.getBundle(descriptor.getLocation(), locale, plugin.getClassLoader());
            }
        };
    }
}
