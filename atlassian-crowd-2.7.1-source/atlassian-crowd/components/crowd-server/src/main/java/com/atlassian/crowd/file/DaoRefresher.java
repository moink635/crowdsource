package com.atlassian.crowd.file;

import java.util.Collection;

import com.atlassian.crowd.dao.RefreshableDao;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * Asks refreshable DAOs to refresh themselves.
 */
public class DaoRefresher extends QuartzJobBean
{
    private static final Logger logger = LoggerFactory.getLogger(DaoRefresher.class);

    private Iterable<RefreshableDao> refreshableDaos;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException
    {
        logger.debug("Refreshing refreshable DAOs");
        for (RefreshableDao refreshableDao : refreshableDaos)
        {
            refreshableDao.refresh();
        }
    }

    public void setRefreshableDaos(Iterable<RefreshableDao> refreshableDaos)
    {
        this.refreshableDaos = refreshableDaos;
    }

}
