package com.atlassian.crowd.migration.legacy.database;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.migration.ImportException;
import com.atlassian.crowd.migration.legacy.LegacyImportDataHolder;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.hibernate.SessionFactory;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DirectoryMapper extends DatabaseMapper implements DatabaseImporter
{
    public DirectoryMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, JdbcOperations jdbcTemplate)
    {
        super(sessionFactory, batchProcessor, jdbcTemplate);
    }

    public void importFromDatabase(LegacyImportDataHolder importData) throws ImportException
    {
        importDirectoriesFromDatabase(importData.getOldToNewDirectoryIds());
    }

    protected List<DirectoryImpl> importDirectoriesFromDatabase(final Map<Long, Long> oldToNewDirectoryIds) throws ImportException
    {
        DirectoryTableMapper directoryTableMapper = new DirectoryTableMapper();
        List<DirectoryImpl> directories = getDirectories(directoryTableMapper);
        Map<Directory, Long> directoryIdMap = directoryTableMapper.getDirectoryIdMap();

        // Get all directory attributes/operations in one database call
        Map<Long, List<Map<String, String>>> allDirectoryAttributes = getDirectoryAttributes();
        Map<Long, Set<OperationType>> allDirectoryOperations = getDirectoryOperations();
        for (DirectoryImpl directory : directories)
        {
            Long oldDirectoryId = directoryIdMap.get(directory);

            Set<OperationType> allowedOperations = allDirectoryOperations.get(oldDirectoryId);
            if (allowedOperations == null)
            {
                allowedOperations = new HashSet<OperationType>();
            }
            directory.setAllowedOperations(allowedOperations);

            Map<String, String> directoryAttributes = attributeListToMap(allDirectoryAttributes.get(oldDirectoryId));
            directory.setAttributes(directoryAttributes);

            try
            {
                // add entity here to obtain new directory id
                Directory addedDirectory = (Directory) addEntityViaMerge(directory);
                long newDirectoryId = addedDirectory.getId();
                oldToNewDirectoryIds.put(oldDirectoryId, newDirectoryId);
            }
            catch (ImportException e)
            {
                logger.error("Error adding directory entity via merge", e);
            }
        }

        logger.info("Successfully migrated " + directories.size() + " directories.");
        return directories;
    }

    private List<DirectoryImpl> getDirectories(DirectoryTableMapper directoryTableMapper)
    {
        return jdbcTemplate.query(legacyTableQueries.getDirectoriesSQL(), directoryTableMapper);
    }

    private Map<Long, Set<OperationType>> getDirectoryOperations()
    {
        DirectoryOperationMapper directoryOperationMapper = new DirectoryOperationMapper();
        jdbcTemplate.query(legacyTableQueries.getDirectoryOperationsSQL(), directoryOperationMapper);
        return directoryOperationMapper.getAllDirectoryOperations();
    }

    private Map<Long, List<Map<String, String>>> getDirectoryAttributes()
    {
        DirectoryAttributeMapper directoryAttributeMapper = new DirectoryAttributeMapper();
        jdbcTemplate.query(legacyTableQueries.getDirectoryAttributesSQL(), directoryAttributeMapper);
        return directoryAttributeMapper.getDirectoryAttributes();
    }

    private final class DirectoryTableMapper implements RowMapper
    {
        private final Map<Directory, Long> directoryIdMap = new HashMap<Directory, Long>();

        public Object mapRow(ResultSet rs, int rowNum) throws SQLException
        {
            Long oldDirectoryid = rs.getLong("ID");
            String directoryName = rs.getString("NAME");
            boolean active = rs.getBoolean("ACTIVE");
            Date createdDate = getDateFromDatabase(rs.getString("CONCEPTION"));
            Date updatedDate = getDateFromDatabase(rs.getString("LASTMODIFIED"));

            String description = rs.getString("DESCRIPTION");
            String implementationClass = rs.getString("IMPLEMENTATIONCLASS");
            DirectoryType directoryType = getDirectoryTypeFromLegacyCode(Integer.parseInt(rs.getString("CODE")));

            InternalEntityTemplate template = createInternalEntityTemplate(oldDirectoryid, directoryName, createdDate, updatedDate, active);
            template.setId(null);

            // Create the directory
            DirectoryImpl directory = new DirectoryImpl(template);
            directory.setDescription(description);
            directory.setImplementationClass(implementationClass);
            directory.setType(directoryType);

            directoryIdMap.put(directory, oldDirectoryid);

            return directory;
        }

        public Map<Directory, Long> getDirectoryIdMap()
        {
            return directoryIdMap;
        }
    }

    private class DirectoryAttributeMapper implements RowCallbackHandler
    {
        private final Map<Long, List<Map<String, String>>> directoryAttributes = new HashMap<Long, List<Map<String, String>>>();

        public void processRow(ResultSet rs) throws SQLException
        {
            String attributeName = rs.getString("ATTRIBUTE");
            String attributeValue = rs.getString("VALUE");
            Long directoryId = rs.getLong("DIRECTORYID");
            Map<String, String> attributes = new HashMap<String, String>();
            attributes.put(attributeName, attributeValue);

            if (!directoryAttributes.containsKey(directoryId))
            {
                directoryAttributes.put(directoryId, new ArrayList<Map<String, String>>());
            }
            directoryAttributes.get(directoryId).add(attributes);
        }

        public Map<Long, List<Map<String, String>>> getDirectoryAttributes()
        {
            return directoryAttributes;
        }
    }

    private class DirectoryOperationMapper implements RowCallbackHandler
    {
        private final Map<Long, Set<OperationType>> allDirectoryOperations = new HashMap<Long, Set<OperationType>>();

        public void processRow(ResultSet rs) throws SQLException
        {
            Long directoryId = rs.getLong("DIRECTORYID");
            String operationType = rs.getString("KEY");

            if (!allDirectoryOperations.containsKey(directoryId))
            {
                allDirectoryOperations.put(directoryId, new HashSet<OperationType>());
            }
            allDirectoryOperations.get(directoryId).add(getOperationTypeFromLegacyPermissionName(operationType));
        }

        public Map<Long, Set<OperationType>> getAllDirectoryOperations()
        {
            return allDirectoryOperations;
        }
    }
}

