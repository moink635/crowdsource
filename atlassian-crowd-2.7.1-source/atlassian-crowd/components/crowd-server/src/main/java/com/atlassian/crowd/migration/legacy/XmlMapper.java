package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.embedded.api.Attributes;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.migration.GenericMapper;
import com.atlassian.crowd.model.InternalEntity;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.Combine;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.*;

/**
 * A generic mapper that contains helper methods and attributes to
 * map domain objects to database objects and vice-versa.
 */
public class XmlMapper extends GenericMapper
{
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String GENERIC_XML_ID = "id";
    public static final String GENERIC_XML_NAME = "name";
    public static final String GENERIC_XML_CREATED_DATE = "createdDate";
    public static final String GENERIC_XML_UPDATED_DATE = "updatedDate";
    public static final String GENERIC_XML_ACTIVE = "active";

    public static final String GENERIC_XML_ATTRIBUTES = "attributes";
    public static final String GENERIC_XML_ATTRIBUTE = "attribute";
    public static final String GENERIC_XML_ATTRIBUTE_NAME = "name";
    public static final String GENERIC_XML_ATTRIBUTE_VALUE = "value";

    // old (crowd 0.x and 1.x)
    public static final String LEGACY_GENERIC_XML_CONCEPTION = "conception";
    public static final String LEGACY_GENERIC_XML_LASTMODIFIED = "lastModified";

    public static final String LEGACY_GENERIC_XML_ATTRIBUTE_ID = "attributeId";
    public static final String LEGACY_GENERIC_XML_ATTRIBUTE_VALUES = "attributeValues";
    public static final String LEGACY_GENERIC_XML_ATTRIBUTE_VALUE = "attributeValue";
    public static final String LEGACY_GENERIC_XML_ATTRIBUTE_KEY = "attributeKey";

    public XmlMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor)
    {
        super(sessionFactory, batchProcessor);
    }

    /**
     * Exports an internal entity to an XML element.
     * <p/>
     * This exports: id, name, createdDate, updatedDate and active.
     *
     * @param entity  entity to export.
     * @param element XML element to export to.
     */
    protected void exportInternalEntity(InternalEntity entity, Element element)
    {
        element.addElement(GENERIC_XML_ID).addText(entity.getId().toString());
        element.addElement(GENERIC_XML_NAME).addText(entity.getName());
        element.addElement(GENERIC_XML_CREATED_DATE).addText(getDateAsFormattedString(entity.getCreatedDate()));
        element.addElement(GENERIC_XML_UPDATED_DATE).addText(getDateAsFormattedString(entity.getUpdatedDate()));
        element.addElement(GENERIC_XML_ACTIVE).addText(Boolean.toString(entity.isActive()));
    }

    /**
     * Exports Map<String, String> to an XML element.
     * <p/>
     * Only use this for entities with simple attributes (Directory, Application) and not complex attributes (User, Group).
     *
     * @param entity  entity with single valued attributes.
     * @param element XML element to export to.
     */
    protected void exportSingleValuedAttributes(Attributes entity, Element element)
    {
        Element attributesElement = element.addElement(GENERIC_XML_ATTRIBUTES);

        // Force the ordering of attributes on export
        final Set<String> attributeNames = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
        attributeNames.addAll(entity.getKeys());

        for (String name : attributeNames)
        {
            String value = entity.getValue(name);
            if (name != null && value != null)
            {
                Element attributeElement = attributesElement.addElement(GENERIC_XML_ATTRIBUTE);
                attributeElement.addElement(GENERIC_XML_ATTRIBUTE_NAME).addText(name);
                attributeElement.addElement(GENERIC_XML_ATTRIBUTE_VALUE).addText(value);
            }
        }
    }

    /**
     * Constructs an InternalEntityTemplate from an XML element.
     * <p/>
     * This imports: id, name, createdDate, updatedDate and active.
     *
     * @param element XML element to import from.
     * @return InternalEntityTemplate holding the values.
     */
    protected InternalEntityTemplate getInternalEntityTemplateFromXml(Element element)
    {
        InternalEntityTemplate template = new InternalEntityTemplate();
        template.setId(Long.parseLong(element.element(GENERIC_XML_ID).getText()));
        template.setName(element.element(GENERIC_XML_NAME).getText());
        template.setCreatedDate(getDateFromXml(element.element(GENERIC_XML_CREATED_DATE).getText()));
        template.setUpdatedDate(getDateFromXml(element.element(GENERIC_XML_UPDATED_DATE).getText()));
        template.setActive(Boolean.parseBoolean(element.element(GENERIC_XML_ACTIVE).getText()));
        return template;
    }

    /**
     * Constructs an Map<String, String> of attributes from an XML element.
     * <p/>
     * This imports attributes with single values.
     *
     * @param element XML element to import from.
     * @return Map<String, String> holding the attributes.
     */
    protected Map<String, String> getSingleValuedAttributesMapFromXml(Element element)
    {
        Map<String, String> attributesMap = new HashMap<String, String>();

        Element attributesElement = element.element(GENERIC_XML_ATTRIBUTES);

        for (Iterator attributes = attributesElement.elementIterator(); attributes.hasNext();)
        {
            Element attributeElement = (Element) attributes.next();
            attributesMap.put(attributeElement.element(GENERIC_XML_ATTRIBUTE_NAME).getText(), attributeElement.element(GENERIC_XML_ATTRIBUTE_VALUE).getText());
        }

        return attributesMap;
    }
}
