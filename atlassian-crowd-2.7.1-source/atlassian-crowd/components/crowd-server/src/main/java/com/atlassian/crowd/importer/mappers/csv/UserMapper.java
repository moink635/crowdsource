package com.atlassian.crowd.importer.mappers.csv;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.importer.config.CsvConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;

import org.apache.commons.collections.OrderedBidiMap;
import org.apache.commons.lang3.StringUtils;

public class UserMapper extends CsvMapper<UserTemplateWithCredentialAndAttributes>
{
    private Boolean setpassword;
    private Boolean encryptPassword;

    public UserMapper(Long directoryId, OrderedBidiMap configuration, Boolean setPassword, Boolean encryptPassword)
    {
        super(directoryId, configuration);
        this.setpassword = setPassword;
        this.encryptPassword = encryptPassword;
    }

    @Override
    public UserTemplateWithCredentialAndAttributes mapRow(String[] resultSet) throws ImporterException
    {
        UserTemplateWithCredentialAndAttributes user = null;
        if (resultSet != null && resultSet.length > 0)
        {
            String username = getString(resultSet, CsvConfiguration.USER_USERNAME);

            PasswordCredential credential;

            if (hasColumnFor(CsvConfiguration.USER_PASSWORD))
            {
                String password = getString(resultSet, CsvConfiguration.USER_PASSWORD);
                if (StringUtils.isNotBlank(password))
                {
                    if (encryptPassword.booleanValue())
                    {
                        credential = new PasswordCredential(password, false);
                    }
                    else
                    {
                        credential = new PasswordCredential(password, true);
                    }
                }
                else
                {
                    credential = PasswordCredential.NONE;
                }
            }
            else
            {
                credential = PasswordCredential.NONE;
            }

            user = new UserTemplateWithCredentialAndAttributes(username, directoryId, credential);

            user.setActive(true);
            user.setEmailAddress(getString(resultSet, CsvConfiguration.USER_EMAILADDRESS));
            user.setFirstName(getString(resultSet, CsvConfiguration.USER_FIRSTNAME));
            user.setLastName(getString(resultSet, CsvConfiguration.USER_LASTNAME));
        }

        return user;
    }
}
