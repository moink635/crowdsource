package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.migration.GenericMapper;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import org.dom4j.Element;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * A generic mapper that contains helper methods and attributes to
 * map domain objects to database objects and vice-versa.
 */
public class GenericLegacyImporter extends GenericMapper
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    protected static final String DATE_FORMAT = "EEE MMM dd HH:mm:ss Z yyyy";
    // ------------------------------------------------------------------------------------------------------- Constants
    public static final String GENERIC_XML_ID = "id";
    public static final String GENERIC_XML_NAME = "name";
    public static final String GENERIC_XML_ACTIVE = "active";
    public static final String GENERIC_XML_CONCEPTION = "conception";
    public static final String GENERIC_XML_LASTMODIFIED = "lastModified";

    public static final String GENERIC_XML_ATTRIBUTES_NODE = "attributes";
    public static final String GENERIC_XML_ATTRIBUTE = "attribute";
    public static final String GENERIC_XML_ATTRIBUTE_ID = "attributeId";
    public static final String GENERIC_XML_ATTRIBUTE_VALUES = "attributeValues";
    public static final String GENERIC_XML_ATTRIBUTE_VALUE = "attributeValue";
    public static final String GENERIC_XML_ATTRIBUTE_KEY = "attributeKey";

    private DirectoryManager directoryManager;

    public GenericLegacyImporter(SessionFactory sessionFactory, BatchProcessor batchProcessor)
    {
        super(sessionFactory, batchProcessor);
    }

    /**
     * Constructs an InternalEntityTemplate from a legacy (Crowd 1.x) XML element.
     * <p/>
     * This imports: id, name, createdDate, updatedDate and active.
     *
     * @param element XML element to import from.
     * @return InternalEntityTemplate holding the values.
     */
    protected InternalEntityTemplate getInternalEntityTemplateFromLegacyXml(Element element)
    {
        InternalEntityTemplate template = new InternalEntityTemplate();
        template.setId(Long.parseLong(element.element(GENERIC_XML_ID).getText()));
        template.setName(element.element(GENERIC_XML_NAME).getText());
        template.setCreatedDate(getDateFromXml(element.element(GENERIC_XML_CONCEPTION).getText()));
        template.setUpdatedDate(getDateFromXml(element.element(GENERIC_XML_LASTMODIFIED).getText()));
        template.setActive(Boolean.parseBoolean(element.element(GENERIC_XML_ACTIVE).getText()));
        return template;
    }

    /**
     * Constructs an Map<String, List<String>> of attributes from a legacy (Crowd 1.x) XML element.
     * <p/>
     * This imports attributes with multiple values.
     *
     * @param element XML element to import from.
     * @return Map<String, Set<String>> holding the attributes.
     */
    protected Map<String, Set<String>> getMultiValuedAttributesMapFromXml(Element element)
    {
        Map<String, Set<String>> attributes = new HashMap<String, Set<String>>();

        Element attributesNode = element.element(GENERIC_XML_ATTRIBUTES_NODE);

        if (attributesNode != null && attributesNode.hasContent())
        {
            for (Iterator iterator = attributesNode.elementIterator(GENERIC_XML_ATTRIBUTE); iterator.hasNext();)
            {
                Element attributeElement = (Element) iterator.next();

                // get the attribute key
                String key = attributeElement.attributeValue(GENERIC_XML_ATTRIBUTE_KEY);

                // Now get the attribute values element
                Element attributeValuesElement = attributeElement.element(GENERIC_XML_ATTRIBUTE_VALUES);

                if (attributeValuesElement != null)
                {
                    // we can ignore the id for 2.x
                    String attributeId = attributeValuesElement.attributeValue(GENERIC_XML_ATTRIBUTE_ID);

                    // Now iterate over the values for the given attribute
                    Set<String> values = new HashSet<String>();
                    for (Iterator attributeValuesIter = attributeValuesElement.elementIterator(GENERIC_XML_ATTRIBUTE_VALUE); attributeValuesIter.hasNext();)
                    {
                        Element attributeValue = (Element) attributeValuesIter.next();
                        values.add(attributeValue.getText());
                    }

                    attributes.put(key, values);
                }
            }
        }

        return attributes;
    }

    /**
     * Constructs an Map<String, String> of attributes from a legacy (Crowd 1.x) XML element.
     * <p/>
     * This imports attributes with multiple values and picks the first attribute value if more than one exists.
     *
     * @param element XML element to import from.
     * @return Map<String, String holding the attributes.
     */
    protected Map<String, String> getSingleValuedAttributesMapFromXml(Element element)
    {
        Map<String, Set<String>> attributes = getMultiValuedAttributesMapFromXml(element);

        Map<String, String> singleValuedAttributes = new HashMap<String, String>();

        for (Map.Entry<String, Set<String>> entry : attributes.entrySet())
        {
            if (!entry.getValue().isEmpty())
            {
                singleValuedAttributes.put(entry.getKey(), entry.getValue().iterator().next());
            }
        }

        return singleValuedAttributes;
    }

    public DirectoryManager getDirectoryManager()
    {
        return directoryManager;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }

    protected PasswordCredential getPasswordCredentialFromXml(Element parentElement)
    {
        // get the first password credential
        Element credentialsNode = parentElement.element("credentials");
        if (credentialsNode != null && credentialsNode.hasContent())
        {
            Element credentialElement = credentialsNode.element("credential");
            if (credentialElement != null)
            {
                return new PasswordCredential(credentialElement.getText(), true);
            }
        }

        // no password existed; default to no password
        return PasswordCredential.NONE;
    }
}
