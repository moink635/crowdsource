package com.atlassian.crowd.migration.legacy.database.sql;

public class HSQLLegacyTableQueries extends GenericLegacyTableQueries
{
    // HSQLDB seems to be able to handle generic queries, so just use the default queries
}
