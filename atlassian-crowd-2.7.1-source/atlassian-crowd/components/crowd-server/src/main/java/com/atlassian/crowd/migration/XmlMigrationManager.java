package com.atlassian.crowd.migration;

import java.util.Map;


public interface XmlMigrationManager
{
    /**
     * Exports the Crowd server's state to an XML file.
     *
     * This exports data from the Crowd database, crowd.cfg.xml and crowd.properties.
     *
     * The XML file will be a Crowd 2.x export, incompatible with Crowd 1.x.
     *
     * @param path file path of export XML.
     * @param options export options.
     * @return time taken to export (milliseconds).
     * @throws ExportException something went wrong during the export.
     */
    long exportXml(String path, Map options) throws ExportException;

    /**
     * Imports exported Crowd server state from an XML file.
     *
     * The XML file can be a Crowd 1.x export or a Crowd 2.x export.
     *
     * @param path file path of the XML backup to import.
     * @return time taken to import (milliseconds).
     * @throws ImportException something went wrong during the import.
     */
    long importXml(String path) throws ImportException;
}
