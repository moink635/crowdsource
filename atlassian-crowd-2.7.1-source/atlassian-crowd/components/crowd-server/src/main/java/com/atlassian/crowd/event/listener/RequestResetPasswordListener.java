package com.atlassian.crowd.event.listener;

import com.atlassian.crowd.event.EventJobExecutionException;
import com.atlassian.crowd.event.login.RequestResetPasswordEvent;
import com.atlassian.crowd.exception.InvalidEmailAddressException;
import com.atlassian.crowd.manager.login.util.ForgottenLoginMailer;
import com.atlassian.crowd.manager.mail.MailSendException;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listener responsible for handling {@link com.atlassian.crowd.event.login.RequestResetPasswordEvent}'s
 * by sending an email to the user, letting them know their new password.
 */
public class RequestResetPasswordListener
{
    private ForgottenLoginMailer forgottenLoginMailer;

    @com.atlassian.event.api.EventListener
    public void handleEvent(RequestResetPasswordEvent requestResetPasswordEvent)
    {
        try
        {
            forgottenLoginMailer.mailResetPasswordLink(requestResetPasswordEvent.getUser(), requestResetPasswordEvent.getResetLink());
        }
        catch (InvalidEmailAddressException e)
        {
            throw new EventJobExecutionException(e.getMessage(), e);
        }
        catch (MailSendException e)
        {
            throw new EventJobExecutionException(e.getMessage(), e);
        }
    }

    public void setForgottenLoginMailer(final ForgottenLoginMailer forgottenLoginMailer)
    {
        this.forgottenLoginMailer = checkNotNull(forgottenLoginMailer);
    }
}
