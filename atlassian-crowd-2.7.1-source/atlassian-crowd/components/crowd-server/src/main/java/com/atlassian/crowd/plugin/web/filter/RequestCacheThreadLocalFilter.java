package com.atlassian.crowd.plugin.web.filter;

import com.atlassian.crowd.plugin.web.RequestCacheThreadLocal;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;

/**
 * Servlet filter for initialising and cleaning up the RequestCacheThreadLocal.
 */
public class RequestCacheThreadLocalFilter implements Filter
{
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        try
        {
            RequestCacheThreadLocal.setRequestCache(new HashMap<String, Object>());

            if (servletRequest instanceof HttpServletRequest)
            {
                RequestCacheThreadLocal.getRequestCache().put(RequestCacheThreadLocal.CONTEXT_PATH_KEY, ((HttpServletRequest) servletRequest).getContextPath());
            }

            filterChain.doFilter(servletRequest, servletResponse);
        }
        finally
        {
            RequestCacheThreadLocal.clearRequestCache();
        }
    }

    public void init(FilterConfig filterConfig) throws ServletException
    {
    }

    public void destroy()
    {
    }
}
