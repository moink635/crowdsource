package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.SynchronisableDirectoryProperties;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.constants.DirectoryTermKeys;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Upgrades remote Crowd directories to include a {@link SynchronisableDirectoryProperties#INCREMENTAL_SYNC_ENABLED} properties set to
 * <tt>true</tt>.
 *
 * @since v2.3
 */
public class UpgradeTask542 implements UpgradeTask
{
    private static final Logger log = LoggerFactory.getLogger(UpgradeTask542.class);

    private final Collection<String> errors = new ArrayList<String>();

    private DirectoryDao directoryDao;

    public String getBuildNumber()
    {
        return "542";
    }

    public String getShortDescription()
    {
        return "Upgrading remote Crowd directories to include the attribute " + SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED + " set to true.";
    }

    public void doUpgrade() throws Exception
    {
        for (Directory directory : findAllRemoteCrowdDirectories())
        {
            log.debug("Upgrading directory {}", directory);
            try
            {
                updateDirectory(directory);
            }
            catch (DataAccessException e)
            {
                final String errorMessage = "Could not update directory " + directory;
                log.error(errorMessage, e);
                errors.add(errorMessage + ", error is " + e.getMessage());
            }
        }
    }

    private void updateDirectory(Directory directory) throws DataAccessException, DirectoryNotFoundException
    {
        DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
        directoryToUpdate.setAttribute(SynchronisableDirectoryProperties.INCREMENTAL_SYNC_ENABLED, Boolean.TRUE.toString());
        directoryDao.update(directoryToUpdate);
    }

    private Iterable<Directory> findAllRemoteCrowdDirectories()
    {
        return directoryDao.search(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory())
                .with(Restriction.on(DirectoryTermKeys.TYPE).exactlyMatching(DirectoryType.CROWD))
                .returningAtMost(EntityQuery.ALL_RESULTS));
    }

    public Collection<String> getErrors()
    {
        return errors;
    }

    public void setDirectoryDao(DirectoryDao directoryDao)
    {
        this.directoryDao = directoryDao;
    }
}
