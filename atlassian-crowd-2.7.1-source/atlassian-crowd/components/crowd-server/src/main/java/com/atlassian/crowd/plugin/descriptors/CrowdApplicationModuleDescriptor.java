package com.atlassian.crowd.plugin.descriptors;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.integration.springsecurity.DynamicProviderManager;
import com.atlassian.crowd.integration.springsecurity.RequestToApplicationMapper;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.manager.authentication.TokenAuthenticationManager;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationImpl;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.plugin.application.springsecurity.LocalCrowdAuthenticationProvider;
import com.atlassian.plugin.Plugin;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.StateAware;
import com.atlassian.plugin.descriptors.AbstractModuleDescriptor;
import com.atlassian.plugin.module.ModuleFactory;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationProvider;

public class CrowdApplicationModuleDescriptor extends AbstractModuleDescriptor implements StateAware
{
    private static final Logger logger = LoggerFactory.getLogger(CrowdApplicationModuleDescriptor.class);

    /**
     * Password which should be used by all applications of type plugin to authenticate. It isn't important that
     * the password is hardcoded, because plugin applications are never allowed to authenticate from any remote
     * address (i.e. even someone provides a plugin-app's name and this password when trying to do a rest call,
     * it would still fail because there are no allowed addresses associated with the plugin-app).
     */
    private static final PasswordCredential credential = PasswordCredential.unencrypted("plugin-password");

    private final ApplicationManager applicationManager;
    private final ApplicationService applicationService;
    private final TokenAuthenticationManager tokenAuthenticationManager;
    private final DynamicProviderManager authenticationManager;
    private final RequestToApplicationMapper requestToApplicationMapper;

    private String applicationName;
    private String applicationDescription;
    private String securePath;

    private LocalCrowdAuthenticationProvider localProvider;

    public CrowdApplicationModuleDescriptor(ModuleFactory moduleFactory, ApplicationManager applicationManager, ApplicationService applicationService, TokenAuthenticationManager tokenAuthenticationManager, DynamicProviderManager authenticationManager, RequestToApplicationMapper requestToApplicationMapper)
    {
        super(moduleFactory);
        this.applicationManager = applicationManager;
        this.applicationService = applicationService;
        this.tokenAuthenticationManager = tokenAuthenticationManager;
        this.authenticationManager = authenticationManager;
        this.requestToApplicationMapper = requestToApplicationMapper;
    }

    public void init(Plugin plugin, Element element) throws PluginParseException
    {
        super.init(plugin, element);

        applicationName = this.getKey();

        applicationDescription = this.getDescription();

        Element secureElt = element.element("secure");
        if (secureElt != null)
        {
            securePath = secureElt.getText();
        }

        if (StringUtils.isBlank(applicationName))
        {
            throw new PluginParseException("The 'key' attribute is blank in the crowd-application module for plugin: " + plugin.getName());
        }
        if (StringUtils.isBlank(applicationDescription))
        {
            throw new PluginParseException("The 'description' element is blank in the crowd-application module for plugin: " + plugin.getName());
        }
        if (StringUtils.isBlank(securePath))
        {
            throw new PluginParseException("The 'secure' element is blank in the crowd-application module for plugin: " + plugin.getName());
        }
    }

    public Object getModule()
    {
        if (localProvider == null)
        {
            try
            {
                Application application = applicationManager.findByName(applicationName);
                localProvider = new LocalCrowdAuthenticationProvider(application, applicationService, applicationManager, tokenAuthenticationManager);
            }
            catch (ApplicationNotFoundException e)
            {
                String msg = "Could not find application with name: " + applicationName + " for plugin " + plugin.getName();
                throw new IllegalStateException(msg, e);
            }
        }

        return localProvider;
    }

    public void enabled()
    {
        super.enabled();

        // add application
        try
        {
            Application existingApp = applicationManager.findByName(applicationName);

            // verify it is a plugin
            if (existingApp.getType() != ApplicationType.PLUGIN)
            {
                String msg = "Application with name " + applicationName + " exists but is not a plugin. Remove this application and restart Crowd to enable plugin: " + plugin.getName();
                throw new IllegalStateException(msg);
            }
        }
        catch (ApplicationNotFoundException e)
        {
            logger.info("Adding application '" + applicationName + "' to Crowd for plugin: " + plugin.getName());

            // application does not exist, so add it
            ApplicationImpl applicationImpl = ApplicationImpl.newInstanceWithCredential(applicationName, ApplicationType.PLUGIN, credential);
            applicationImpl.setDescription(applicationDescription);

            // by default, the application can only perform read operations on the associated directories

            try
            {
                applicationManager.add(applicationImpl);
            }
            catch (Exception e1)
            {
                String msg = "Unable to add application '" + applicationName + "' to Crowd for plugin: " + plugin.getName();
                throw new RuntimeException(msg, e1);
            }
        }

        // add security
        authenticationManager.addProvider((AuthenticationProvider) getModule());
        requestToApplicationMapper.addSecureMapping(securePath, applicationName);
    }

    public void disabled()
    {
        // deactivate application
        try
        {
            ApplicationImpl applicationImpl = ApplicationImpl.newInstance(applicationManager.findByName(applicationName));
            applicationImpl.setActive(false);
            applicationManager.update(applicationImpl);
        }
        catch (ApplicationNotFoundException e)
        {
            // do nothing as there is no app to disable
        }
        catch (Exception e)
        {
            String msg = "Unable to deactive application '" + applicationName + "' in Crowd for plugin: " + plugin.getName();
            throw new RuntimeException(msg, e);
        }

        // remove security
        if (localProvider != null)
        {
            authenticationManager.removeProvider((AuthenticationProvider) getModule());
        }
        requestToApplicationMapper.removeSecureMapping(securePath);
        localProvider = null;

        super.disabled();
    }
}