package com.atlassian.crowd.migration;

import org.apache.commons.lang3.StringUtils;

import java.util.List;

public class ImportException extends Exception
{
    public ImportException(String message)
    {
        super(message);
    }

    public ImportException(Exception exception)
    {
        super(exception);
    }

    public ImportException(String message, List<String> errors)
    {
        super(message +": "+ StringUtils.join(errors, "\n"));
    }
}