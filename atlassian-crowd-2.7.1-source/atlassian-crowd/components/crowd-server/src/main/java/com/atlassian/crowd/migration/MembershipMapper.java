package com.atlassian.crowd.migration;

import com.atlassian.crowd.dao.directory.DirectoryDAOHibernate;
import com.atlassian.crowd.dao.group.GroupDAOHibernate;
import com.atlassian.crowd.dao.membership.MembershipDAOHibernate;
import com.atlassian.crowd.dao.user.UserDAOHibernate;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.migration.legacy.PartialXmlMapper;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.group.InternalGroup;
import com.atlassian.crowd.model.membership.InternalMembership;
import com.atlassian.crowd.model.membership.MembershipType;
import com.atlassian.crowd.model.user.InternalUser;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Collections2;
import com.google.common.collect.Iterables;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This mapper will handle the mapping of a {@link com.atlassian.crowd.model.group.Group}.
 */
public class MembershipMapper extends PartialXmlMapper implements Mapper
{
    /** Types of directories for which users will need to be backed up */
    private static final Set<DirectoryType> INCLUDED_DIRECTORY_TYPES =
        EnumSet.of(DirectoryType.INTERNAL, DirectoryType.DELEGATING, DirectoryType.CONNECTOR);

    private final MembershipDAOHibernate membershipDAO;
    private final DirectoryDAOHibernate directoryDAO;
    private final UserDAOHibernate userDAO;
    private final GroupDAOHibernate groupDAO;

    protected static final String MEMBERSHIP_XML_ROOT = "memberships";
    private static final String MEMBERSHIP_XML_NODE = "membership";
    private static final String MEMBERSHIP_XML_DIRECTORY_ID = "directoryId";
    private static final String MEMBERSHIP_XML_PARENT_NAME = "parentName";
    private static final String MEMBERSHIP_XML_CHILD_NAME = "childName";
    private static final String MEMBERSHIP_XML_PARENT_ID = "parentId";
    private static final String MEMBERSHIP_XML_CHILD_ID = "childId";
    private static final String MEMBERSHIP_XML_MEMBERSHIP_TYPE = "membershipType";
    private static final String MEMBERSHIP_XML_MEMBERSHIP_GROUP_TYPE = "groupType";

    public MembershipMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, MembershipDAOHibernate membershipDAO,
                            DirectoryDAOHibernate directoryDAO, UserDAOHibernate userDAO, GroupDAOHibernate groupDAO,
                            DirectoryManager directoryManager)
    {
        super(sessionFactory, batchProcessor, directoryManager, INCLUDED_DIRECTORY_TYPES);
        this.membershipDAO = membershipDAO;
        this.directoryDAO = directoryDAO;
        this.userDAO = userDAO;
        this.groupDAO = groupDAO;
    }

    public Element exportXml(final Map options) throws ExportException
    {
        Element membershipRoot = DocumentHelper.createElement(MEMBERSHIP_XML_ROOT);

        Iterable<InternalMembership> memberships = findAllExportableMemberships();

        for (InternalMembership membership : memberships)
        {
            addMembershipToXml(membership, membershipRoot);
        }

        return membershipRoot;
    }

    private Iterable<InternalMembership> findAllExportableMemberships()
    {
        final List<Directory> exportableDirectories = findAllExportableDirectories();

        final Predicate<Directory> requiresExportOfNonLocalGroups = new Predicate<Directory>()
        {

            @Override
            public boolean apply(final Directory directory)
            {
                return isExportOfNonLocalGroupsRequired(directory.getType());
            }

        };
        final Collection<Directory> directoriesThatExportAllMemberships =
            Collections2.filter(exportableDirectories, requiresExportOfNonLocalGroups);
        final Collection<Directory> directoriesThatExportOnlyLocalMemberships =
            Collections2.filter(exportableDirectories, Predicates.not(requiresExportOfNonLocalGroups));
        return Iterables.concat(
                membershipDAO.findAll(directoriesThatExportAllMemberships),
                membershipDAO.findAllLocal(directoriesThatExportOnlyLocalMemberships)
        );
    }

    protected void addMembershipToXml(final InternalMembership membership, final Element membershipRoot)
    {
        Element membershipElement = membershipRoot.addElement(MEMBERSHIP_XML_NODE);
        membershipElement.addElement(GENERIC_XML_ID).addText(membership.getId().toString());
        membershipElement.addElement(MEMBERSHIP_XML_DIRECTORY_ID).addText(membership.getDirectory().getId().toString());
        membershipElement.addElement(MEMBERSHIP_XML_PARENT_NAME).addText(membership.getParentName());
        membershipElement.addElement(MEMBERSHIP_XML_CHILD_NAME).addText(membership.getChildName());
        membershipElement.addElement(MEMBERSHIP_XML_PARENT_ID).addText(membership.getParentId().toString());
        membershipElement.addElement(MEMBERSHIP_XML_CHILD_ID).addText(membership.getChildId().toString());
        membershipElement.addElement(MEMBERSHIP_XML_MEMBERSHIP_TYPE).addText(membership.getMembershipType().name());
        membershipElement.addElement(MEMBERSHIP_XML_MEMBERSHIP_GROUP_TYPE).addText(membership.getGroupType().name());
    }

    public void importXml(final Element root) throws ImportException
    {
        Element membershipsElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + MEMBERSHIP_XML_ROOT);
        if (membershipsElement == null)
        {
            logger.error("No memberships were found for importing!");
            return;
        }

        // TODO: we should stream the XML and chunkinise this process rather than beg for OOMEs
        List<InternalMembership> membershipsToReplicate = new ArrayList<InternalMembership>();
        List<InternalUser> placeholderUsers = new ArrayList<InternalUser>();
        List<InternalGroup> placeholderGroups = new ArrayList<InternalGroup>();

        for (Iterator membershipIter = membershipsElement.elementIterator(); membershipIter.hasNext(); )
        {
            Element membershipElement = (Element) membershipIter.next();

            InternalMembership membership = getMembershipFromXml(membershipElement);

            if (isImportableDirectory(membership.getDirectory()))
            {
                // possible optimisation: skip these checks if local groups are disabled for this directory
                if (isPlaceholderUserNeeded(membership))
                {
                    placeholderUsers.add(createPlaceholderUser(membership.getChildId(), membership.getChildName(),
                                                               membership.getDirectory()));
                }
                else if (isPlaceholderChildGroupNeeded(membership))
                {
                    placeholderGroups.add(createPlaceholderGroup(membership.getChildId(), membership.getChildName(),
                                                                 membership.getDirectory()));
                }
                else if (isPlaceholderParentGroupNeeded(membership))
                {
                    placeholderGroups.add(createPlaceholderGroup(membership.getParentId(), membership.getParentName(),
                                                                 membership.getDirectory()));
                }
                membershipsToReplicate.add(membership);
            }
        }

        if (!placeholderUsers.isEmpty())
        {
            logger.info("About to replicate {} cached users (they will be refreshed in the next synchronisation)",
                        placeholderUsers.size());
            addEntities(placeholderUsers);
            logger.info("Cached users replication complete");
        }

        if (!placeholderGroups.isEmpty())
        {
            logger.info("About to replicate {} cached groups (they will be refreshed in the next synchronisation)",
                        placeholderGroups.size());
            addEntities(placeholderGroups);
            logger.info("Cached groups replication complete");
        }

        logger.info("About to replicate " + membershipsToReplicate.size() + " memberships");
        addEntities(membershipsToReplicate);
        logger.info("Membership replication complete");
    }

    private static InternalUser createPlaceholderUser(Long id, String name, Directory directory)
    {
        InternalEntityTemplate internalEntityTemplate =
            new InternalEntityTemplate(id, name, true, new Date(), new Date());
        UserTemplate userTemplate = new UserTemplate(name, directory.getId());
        userTemplate.setActive(true);
        return new InternalUser(internalEntityTemplate, directory, userTemplate, PasswordCredential.NONE);
    }

    private static InternalGroup createPlaceholderGroup(Long id, String name, Directory directory)
    {
        InternalEntityTemplate internalEntityTemplate =
            new InternalEntityTemplate(id, name, true, new Date(), new Date());
        GroupTemplate groupTemplate = new GroupTemplate(name, directory.getId());
        groupTemplate.setActive(true);
        return new InternalGroup(internalEntityTemplate, directory, groupTemplate);
    }

    private boolean isPlaceholderUserNeeded(InternalMembership membership)
    {
        return membership.getMembershipType().equals(MembershipType.GROUP_USER)
            && !userExistsInDirectory(membership.getChildName(), membership.getDirectory());
    }

    private boolean isPlaceholderChildGroupNeeded(InternalMembership membership)
    {
        return membership.getMembershipType().equals(MembershipType.GROUP_GROUP)
               && !groupExistsInDirectory(membership.getChildName(), membership.getDirectory());
    }

    private boolean isPlaceholderParentGroupNeeded(InternalMembership membership)
    {
        return membership.getMembershipType().equals(MembershipType.GROUP_GROUP)
               && !groupExistsInDirectory(membership.getParentName(), membership.getDirectory());
    }

    private boolean userExistsInDirectory(String name, Directory directory)
    {
        try
        {
            userDAO.findByName(directory.getId(), name);
            return true;
        }
        catch (UserNotFoundException e)
        {
            return false;
        }
    }

    private boolean groupExistsInDirectory(String name, Directory directory)
    {
        try
        {
            groupDAO.findByName(directory.getId(), name);
            return true;
        }
        catch (GroupNotFoundException e)
        {
            return false;
        }
    }

    protected InternalMembership getMembershipFromXml(final Element membershipElement)
    {
        Long id = Long.parseLong(membershipElement.element(GENERIC_XML_ID).getText());
        Long directoryId = Long.parseLong(membershipElement.element(MEMBERSHIP_XML_DIRECTORY_ID).getText());
        Long childId = Long.parseLong(membershipElement.element(MEMBERSHIP_XML_CHILD_ID).getText());
        Long parentId = Long.parseLong(membershipElement.element(MEMBERSHIP_XML_PARENT_ID).getText());
        String childName = membershipElement.element(MEMBERSHIP_XML_CHILD_NAME).getText();
        String parentName = membershipElement.element(MEMBERSHIP_XML_PARENT_NAME).getText();
        MembershipType membershipType = MembershipType.valueOf(membershipElement.element(MEMBERSHIP_XML_MEMBERSHIP_TYPE).getText());
        GroupType groupType = GroupType.valueOf(membershipElement.element(MEMBERSHIP_XML_MEMBERSHIP_GROUP_TYPE).getText());

        DirectoryImpl directoryReference = (DirectoryImpl) directoryDAO.loadReference(directoryId);

        return new InternalMembership(id, parentId, childId, membershipType, groupType, parentName, childName, directoryReference);
    }
}
