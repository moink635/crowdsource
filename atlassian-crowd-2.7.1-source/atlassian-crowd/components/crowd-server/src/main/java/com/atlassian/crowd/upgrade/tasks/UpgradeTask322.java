package com.atlassian.crowd.upgrade.tasks;

import com.atlassian.crowd.directory.ldap.LDAPPropertiesMapper;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.builder.QueryBuilder;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Add "displayName" property to LDAP (non-internal) directories.
 */
public class UpgradeTask322 implements UpgradeTask
{
    private DirectoryManager directoryManager;

    public String getBuildNumber()
    {
        return "322";
    }

    public String getShortDescription()
    {
        return "Adding 'displayName' attribute mapping to non-internal directories";
    }

    public void doUpgrade() throws Exception
    {
        List<Directory> directories = directoryManager.searchDirectories(QueryBuilder.queryFor(Directory.class, EntityDescriptor.directory()).returningAtMost(EntityQuery.ALL_RESULTS));
        for (Directory directory : directories)
        {
            if (directory.getType() != DirectoryType.INTERNAL)
            {
                DirectoryImpl directoryToUpdate = new DirectoryImpl(directory);
                directoryToUpdate.setAttribute(LDAPPropertiesMapper.USER_DISPLAYNAME_KEY, UserConstants.DISPLAYNAME);
                directoryManager.updateDirectory(directoryToUpdate);
            }
        }
    }

    public Collection<String> getErrors()
    {
        return Collections.emptyList();
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }
}