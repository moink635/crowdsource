package com.atlassian.crowd.manager.threadlocal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.opensymphony.xwork.ActionContext;

import org.springframework.security.core.context.SecurityContext;

/**
 * Contains the state of the different thread locals in the application.
 *
 * @since v2.7
 */
public class ThreadLocalState
{
    private final SecurityContext securityContext;
    private final ActionContext actionContext;
    private final HttpServletRequest request;
    private final HttpServletResponse response;

    public ThreadLocalState(final SecurityContext securityContext, final ActionContext actionContext, final HttpServletRequest request, final HttpServletResponse response)
    {
        this.securityContext = securityContext;
        this.actionContext = actionContext;
        this.request = request;
        this.response = response;
    }

    public SecurityContext getSecurityContext()
    {
        return securityContext;
    }

    public ActionContext getActionContext()
    {
        return actionContext;
    }

    public HttpServletRequest getRequest()
    {
        return request;
    }

    public HttpServletResponse getResponse()
    {
        return response;
    }
}
