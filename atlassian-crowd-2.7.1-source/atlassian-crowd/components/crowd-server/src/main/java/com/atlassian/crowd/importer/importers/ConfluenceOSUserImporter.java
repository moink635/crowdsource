package com.atlassian.crowd.importer.importers;

import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.mappers.jdbc.GroupMapper;
import com.atlassian.crowd.importer.mappers.jdbc.UserMapper;
import com.atlassian.crowd.importer.mappers.jdbc.UserMembershipMapper;
import com.atlassian.crowd.manager.directory.DirectoryManager;

import org.springframework.jdbc.core.RowMapper;

/**
 * Importer specific to the Confluence OSUser format
 */
class ConfluenceOSUserImporter extends JdbcImporter
{

    // THIS SQL IS CASE SENSITIVE! DO NOT CHANGE!

    // OSUser SQL
    private static final String FIND_GROUPS_SQL = "SELECT groupname FROM os_group ORDER BY groupname";

    private static final String FIND_USERS_SQL = "SELECT username, passwd, " + "(SELECT pe.string_val FROM OS_PROPERTYENTRY pe WHERE pe.entity_name='OSUser_user' AND pe.entity_id = osu.id AND pe.entity_key = 'email') AS email, " + "(SELECT pe.string_val FROM OS_PROPERTYENTRY pe WHERE pe.entity_name='OSUser_user' AND pe.entity_id = osu.id AND pe.entity_key = 'fullName') AS fullname " + "FROM os_user osu";

    private static final String FIND_USER_GROUP_MEMBERSHIPS = "SELECT " + "(SELECT username FROM os_user osu WHERE osu.id=osug.user_id) AS username, " + "(SELECT groupname FROM os_group osg WHERE osg.id=osug.group_id) AS groupname " + "FROM os_user_group osug";

    public ConfluenceOSUserImporter(DirectoryManager directoryManager)
    {
        super(directoryManager);
    }

    @Override
    public String getSelectAllGroupsSQL()
    {
        return FIND_GROUPS_SQL;
    }

    @Override
    public String getSelectAllUsersSQL()
    {
        return FIND_USERS_SQL;
    }

    @Override
    public String getSelectAllUserGroupMembershipsSQL()
    {
        return FIND_USER_GROUP_MEMBERSHIPS;
    }

    @Override
    public RowMapper getGroupMapper(Configuration configuration)
    {
        return new GroupMapper("groupname", "groupname", configuration.getDirectoryID());
    }

    @Override
    public RowMapper getMembershipMapper()
    {
        return new UserMembershipMapper("username", "groupname");
    }

    @Override
    public RowMapper getUserMapper(Configuration configuration)
    {
        return new UserMapper(configuration, "username", "email", "fullname", "passwd");
    }
}
