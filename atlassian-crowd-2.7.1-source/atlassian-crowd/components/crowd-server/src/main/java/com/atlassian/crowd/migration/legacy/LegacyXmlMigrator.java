package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.migration.ImportException;
import org.dom4j.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;


public class LegacyXmlMigrator
{
    private final Logger logger = LoggerFactory.getLogger(LegacyXmlMigrator.class);

    private final List<LegacyImporter> mappers;

    public LegacyXmlMigrator(List<LegacyImporter> mappers)
    {
        this.mappers = mappers;
    }

    public void importXml(Element root) throws ImportException
    {                        
        // the directory mapper will populate this map so it is important that the directory mapper appears before other
        // mappers that rely on this map (in the spring xml list of legacy mappers)
        LegacyImportDataHolder importData = new LegacyImportDataHolder();

        for (LegacyImporter mapper : mappers)
        {
            logger.info("Using " + mapper.getClass().getName() + " to import.");
            mapper.importXml(root, importData);
        }
    }
}
