package com.atlassian.crowd.importer.factory;

import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.exceptions.ImporterConfigurationException;
import com.atlassian.crowd.importer.importers.Importer;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

/**
 * Main implementation of the {@see ImporterFactory}
 */
public class ImporterFactoryImpl implements ImporterFactory
{
    private Map genericImporters;

    private Map atlassianImporters;

    private Map allImporters;

    private Set supportedApplications;

    private Set atlassianSupportedApplication;

    public ImporterFactoryImpl(Map genericImporters, Map atlassianImporters)
    {
        this.genericImporters = genericImporters;
        this.atlassianImporters = atlassianImporters;

        this.atlassianSupportedApplication = Collections.unmodifiableSet(new HashSet(atlassianImporters.keySet()));

        Set genericKeys = new HashSet(this.genericImporters.keySet());
        Set atlassianSetKeys = new HashSet(this.atlassianImporters.keySet());
        genericKeys.addAll(atlassianSetKeys);
        this.supportedApplications = Collections.unmodifiableSet(genericKeys);

        this.allImporters = new HashMap();
        this.allImporters.putAll(atlassianImporters);
        this.allImporters.putAll(genericImporters);
    }

    public Importer getImporterDAO(Configuration configuration) throws ImporterConfigurationException
    {
        if (configuration == null)
        {
            throw new IllegalArgumentException("Importer Configuraiton cannot be null");            
        }

        Importer importer = null;
        
        // Make sure that the configuration has an application
        if (configuration.getApplication() != null)
        {
            importer = (Importer) allImporters.get(toLowerCase(configuration.getApplication()));
        }

        if (importer == null)
        {
            throw new ImporterConfigurationException("Failed to find importer for Configuration: " + configuration);             
        }

        return importer;
    }

    public Set<String> getSupportedImporterApplications()
    {
        return supportedApplications;
    }

    public Set<String> getAtlassianSupportedImporterApplications()
    {
        return atlassianSupportedApplication;
    }
}
