package com.atlassian.crowd.util;

import java.util.Calendar;

import javax.servlet.ServletContext;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.manager.bootstrap.CrowdBootstrapManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.manager.property.PropertyManager;
import com.atlassian.crowd.util.build.BuildUtils;

import com.opensymphony.webwork.ServletActionContext;

import org.slf4j.Logger;
import org.springframework.web.context.ServletContextAware;

public class SystemInfoHelperImpl implements SystemInfoHelper, ServletContextAware
{
    private ServletContext servletContext;

    private PropertyManager propertyManager;

    private CrowdBootstrapManager bootstrapManager;

    private DirectoryManager directoryManager;

    public SystemInfoHelperImpl()
    {

    }

    public SystemInfoHelperImpl(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }

    public String getApplicationServer()
    {
        String applicationServer = null;
        if (servletContext != null)
        {
            applicationServer = servletContext.getServerInfo();
        }
        else if (ServletActionContext.getServletContext() != null)
        {
            // try and get the context from the webwork servlet context
            applicationServer = ServletActionContext.getServletContext().getServerInfo();
        }
        return applicationServer;
    }

    public long getTotalMemory()
    {
        return Runtime.getRuntime().totalMemory() / MEGABYTE;
    }

    public long getFreeMemory()
    {
        return Runtime.getRuntime().freeMemory() / MEGABYTE;
    }

    public long getUsedMemory()
    {
        return (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / MEGABYTE;
    }

    public String getOperatingSystem()
    {
        return System.getProperty("os.name") + System.getProperty("os.version");
    }

    public String getJavaVersion()
    {
        return System.getProperty("java.version");
    }

    public String getJavaVendor()
    {
        return System.getProperty("java.vendor");
    }

    public String getJavaRuntime()
    {
        return System.getProperty("java.vm.name");
    }

    @Override
    public String getFileEncoding()
    {
        return System.getProperty("file.encoding");
    }

    public String getApplicationUsername()
    {
        return System.getProperty("user.name");
    }

    public String getArchitecture()
    {
        return System.getProperty("os.arch");
    }

    public String getCrowdVersion()
    {
        return BuildUtils.getVersion();
    }

    public String getTimeZone()
    {
        return Calendar.getInstance().getTimeZone().getDisplayName();
    }

    public String getJavaVMVersion()
    {
        return System.getProperty("java.vm.version");
    }

    public String getJavaVMVendor()
    {
        return System.getProperty("java.vm.vendor");
    }

    public String getServerId()
    {
        return getBootstrapManager().getServerID();
    }

    public String getDatabaseJdbcDriver()
    {
        return getBootstrapManager().getString("hibernate.connection.driver_class");
    }

    public String getDatabaseJdbcUsername()
    {
        return getBootstrapManager().getString("hibernate.connection.username");
    }

    public String getDatabaseJdbcUrl()
    {
        return getBootstrapManager().getString("hibernate.connection.url");
    }

    public String getDatabaseHibernateDialect()
    {
        return getBootstrapManager().getString("hibernate.dialect");
    }

    public String getDatabaseDatasourceJndiName()
    {
        return getBootstrapManager().getString("hibernate.connection.datasource");
    }

    public boolean isDatabaseDatasource()
    {
        return getDatabaseDatasourceJndiName() != null;
    }

    public void printMinimalSystemInfo(final Logger logger)
    {
        logger.info("System Information:");
        logger.info("\tTimezone: " + getTimeZone());
        logger.info("\tJava Version: " + getJavaVersion());
        logger.info("\tJava Vendor: " + getJavaVendor());
        logger.info("\tJVM Version: " + getJavaVMVersion());
        logger.info("\tJVM Vendor: " + getJavaVMVendor());
        logger.info("\tJVM Runtime: " + getJavaRuntime());
        logger.info("\tUsername: " + getApplicationUsername());
        logger.info("\tOperating System: " + getOperatingSystem());
        logger.info("\tArchitecture: " + getArchitecture());
        logger.info("\tFile Encoding: " + getFileEncoding());

        logger.info("JVM Statistics:");
        logger.info("\tTotal Memory: " + getTotalMemory() + "MB");
        logger.info("\tUsed Memory: " + getUsedMemory() + "MB");
        logger.info("\tFree Memory: " + getFreeMemory() + "MB");

        logger.info("Runtime Information:");
        logger.info("\tVersion: " + BuildUtils.BUILD_VERSION);
        logger.info("\tBuild Number: " + BuildUtils.BUILD_NUMBER);
        logger.info("\tBuild Date: " + BuildUtils.BUILD_DATE);
    }

    public void printSystemInfo(final Logger logger)
    {
        printMinimalSystemInfo(logger);

        logger.info("\tApplication Server: " + getApplicationServer());

        logger.info("Database Information:");
        if (isDatabaseDatasource())
        {
            logger.info("\tDatasource JNDI: " + getDatabaseDatasourceJndiName());
        }
        else
        {
            logger.info("\tJDBC URL: " + getDatabaseJdbcUrl());
            logger.info("\tJDBC Driver: " + getDatabaseJdbcDriver());
            logger.info("\tJDBC Username: " + getDatabaseJdbcUsername());
        }
        logger.info("\tHibernate Dialect: " + getDatabaseHibernateDialect());

        logger.info("License Information:");
        logger.info("\tLicense Server ID: " + getServerId());

        logger.info("Directories:");

        StringBuffer directoryList = new StringBuffer();
        for (Directory directory : getDirectoryManager().findAllDirectories())
        {
            String directoryType = directory.getImplementationClass();
            directoryType = directoryType.substring(directoryType.lastIndexOf(".") + 1, directoryType.length());
            directoryList.append("\t").append(directory.getName()).append(" (").append(directoryType).append(")").append("\n");
        }
        logger.info(directoryList.toString());

    }

    public PropertyManager getPropertyManager()
    {
        return propertyManager;
    }

    public void setPropertyManager(final PropertyManager propertyManager)
    {
        this.propertyManager = propertyManager;
    }

    public void setServletContext(ServletContext servletContext)
    {
        this.servletContext = servletContext;
    }

    public CrowdBootstrapManager getBootstrapManager()
    {
        return bootstrapManager;
    }

    public void setBootstrapManager(CrowdBootstrapManager bootstrapManager)
    {
        this.bootstrapManager = bootstrapManager;
    }

    public DirectoryManager getDirectoryManager()
    {
        return directoryManager;
    }

    public void setDirectoryManager(DirectoryManager directoryManager)
    {
        this.directoryManager = directoryManager;
    }
}
