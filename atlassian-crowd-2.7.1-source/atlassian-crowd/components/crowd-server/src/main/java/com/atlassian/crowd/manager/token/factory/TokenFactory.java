package com.atlassian.crowd.manager.token.factory;

import java.util.List;

import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.token.Token;
import com.atlassian.crowd.model.token.TokenLifetime;

/**
 * Responsible for the creation of {@link com.atlassian.crowd.model.token.Token}'s this should be the
 * only way you create a Token for Crowd
 */
public interface TokenFactory
{
    /**
     * Generates a token key based on the supplied <code>validationFactors</code>.
     *
     * @param directoryID       associated directory for the Token
     * @param name              the name for the token, this may be the 'username' of an associated Principal or application name.
     * @param tokenLifetime     Requested lifetime of the token
     * @param validationFactors The validation factors that are used when generating a key.
     * @return The generated key.
     * @throws com.atlassian.crowd.exception.InvalidTokenException If there was an error generating the key, usually if there was an encoding exception.
     */
    Token create(long directoryID, String name, TokenLifetime tokenLifetime,
                 List<ValidationFactor> validationFactors) throws InvalidTokenException;

    /**
     * Generates a token key based on the supplied <code>validationFactors</code>.
     *
     * @param directoryID       associated directory for the Token
     * @param name              the name for the token, this may be the 'username' of an associated Principal or application name.
     * @param tokenLifetime     Requested lifetime of the token
     * @param validationFactors The validation factors that are used when generating a key.
     * @param secretNumber      The 'secret number' to add as a validation factor
     * @return The generated key.
     * @throws InvalidTokenException If there was an error generating the key, usually if there was an encoding exception.
     */
    Token create(long directoryID, String name, TokenLifetime tokenLifetime,
                 List<ValidationFactor> validationFactors, long secretNumber) throws InvalidTokenException;
}
