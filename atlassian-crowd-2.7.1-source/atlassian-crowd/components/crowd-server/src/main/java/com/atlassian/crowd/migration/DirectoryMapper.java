package com.atlassian.crowd.migration;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.migration.legacy.XmlMapper;
import com.atlassian.crowd.model.InternalEntityTemplate;
import com.atlassian.crowd.model.directory.DirectoryImpl;
import com.atlassian.crowd.util.persistence.hibernate.batch.BatchProcessor;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.hibernate.SessionFactory;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

/**
 * This mapper will handle the mapping of a {@link com.atlassian.crowd.model.directory.DirectoryImpl}
 */
public class DirectoryMapper extends XmlMapper implements Mapper
{
    private final DirectoryManager directoryManager;

    protected static final String DIRECTORY_XML_ROOT = "directories";
    protected static final String DIRECTORY_XML_NODE = "directory";
    private static final String DIRECTORY_XML_DESCRIPTION = "description";
    private static final String DIRECTORY_XML_TYPE = "type";
    private static final String DIRECTORY_XML_IMPLEMENTATION_CLASS = "implementationClass";
    private static final String DIRECTORY_XML_PERMISSIONS = "permissions";
    private static final String DIRECTORY_XML_PERMISSION = "permission";

    public DirectoryMapper(SessionFactory sessionFactory, BatchProcessor batchProcessor, DirectoryManager directoryManager)
 	{
 	    super(sessionFactory, batchProcessor);
 	    this.directoryManager = directoryManager;
 	}

    public Element exportXml(Map options) throws ExportException
    {
        Element directoryRoot = DocumentHelper.createElement(DIRECTORY_XML_ROOT);

        List<Directory> directories = directoryManager.findAllDirectories();

        for (Directory directory : directories)
        {
            addDirectoryToXml((DirectoryImpl) directory, directoryRoot);
        }

        return directoryRoot;
    }

    protected void addDirectoryToXml(DirectoryImpl directory, Element directoryRoot)
    {
        Element directoryElement = directoryRoot.addElement(DIRECTORY_XML_NODE);
        exportInternalEntity(directory, directoryElement);
        directoryElement.addElement(DIRECTORY_XML_DESCRIPTION).addText(StringUtils.defaultString(directory.getDescription()));
        directoryElement.addElement(DIRECTORY_XML_TYPE).addText(directory.getType().name());
        directoryElement.addElement(DIRECTORY_XML_IMPLEMENTATION_CLASS).addText(directory.getImplementationClass());

        Element permissionsElement = directoryElement.addElement(DIRECTORY_XML_PERMISSIONS);
        TreeSet<OperationType> operations = Sets.newTreeSet(directory.getAllowedOperations());
        for (OperationType operation : operations)
        {
            permissionsElement.addElement(DIRECTORY_XML_PERMISSION).addText(operation.name());
        }

        exportSingleValuedAttributes(directory, directoryElement);
    }

    public void importXml(final Element root) throws ImportException
    {
        Element directoryRoot = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/" + DIRECTORY_XML_ROOT);
        if (directoryRoot == null)
        {
            logger.error("No directories were found for importing!");
            return;
        }

        for (Iterator directories = directoryRoot.elementIterator(); directories.hasNext();)
        {
            Element directoryElement = (Element) directories.next();
            Directory directory = getDirectoryFromXml(directoryElement);
            addEntity(directory);
        }
    }

    protected DirectoryImpl getDirectoryFromXml(Element directoryElement)
    {
        InternalEntityTemplate template = getInternalEntityTemplateFromXml(directoryElement);
        DirectoryImpl directory = new DirectoryImpl(template);

        directory.setDescription(directoryElement.element(DIRECTORY_XML_DESCRIPTION).getText());
        directory.setType(DirectoryType.valueOf(directoryElement.element(DIRECTORY_XML_TYPE).getText()));
        directory.setImplementationClass(directoryElement.element(DIRECTORY_XML_IMPLEMENTATION_CLASS).getText());

        Element permissionsElement = directoryElement.element(DIRECTORY_XML_PERMISSIONS);
        for (Iterator permissions = permissionsElement.elementIterator(); permissions.hasNext();)
        {
            Element permissionElement = (Element) permissions.next();
            directory.addAllowedOperation(OperationType.valueOf(permissionElement.getText()));
        }

        Map<String, String> attributes = getSingleValuedAttributesMapFromXml(directoryElement);
        directory.setAttributes(attributes);

        return directory;
    }

    public DirectoryManager getDirectoryManager()
    {
        return directoryManager;
    }
}
