package com.atlassian.crowd.migration.legacy;

import com.atlassian.crowd.migration.XmlMigrationManagerImpl;
import org.dom4j.Element;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.crowd.embedded.impl.IdentifierUtils.toLowerCase;

/**
 * Responsible for verifying the correctness of a legacy XML backup.
 *
 * Currently this verifies that roles and groups within a directory have
 * different names (case-insensitive comparison).
 *
 * NOTE: this class is stateful so do not use it in a singleton manner.
 */
public class LegacyXmlVerifier
{
    private List<String> errors = new ArrayList<String>();

    // wow, this will kill memory for large imports (but so will all our existing mappers)
    private Map<Long, Set<String>> directoryToRoleNames = new HashMap<Long, Set<String>>();
    private Map<Long, Set<String>> directoryToGroupNames = new HashMap<Long, Set<String>>();

    /**
     * Verifies an XML document to ensure:
     * 1. no group name matches any other group name in the same directory
     * 2. no role name matches any other role name in the same directory
     * 3. no group name matches any role name in the same directory
     *
     * Errors can be obtained by called <code>getErrors</code>.
     *
     * @param root root of the XML document.
     */
    public void verify(Element root)
    {
        // process group names
        Element groupsElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + GroupMapper.REMOTE_GROUP_XML_ROOT);
        for (Iterator remoteGroups = groupsElement.elementIterator(); remoteGroups.hasNext();)
        {
            Element groupElement = (Element) remoteGroups.next();
            Long directoryId = Long.parseLong(groupElement.element(GroupMapper.REMOTE_GROUP_XML_DIRECTORY_ID).getText());
            String groupName = groupElement.elementText(GenericLegacyImporter.GENERIC_XML_NAME);
            addGroupName(directoryId, groupName);
        }

        // process role names
        Element rolesElement = (Element) root.selectSingleNode("/" + XmlMigrationManagerImpl.XML_ROOT + "/ " + RoleMapper.REMOTE_ROLE_XML_ROOT);
        for (Iterator remoteRoles = rolesElement.elementIterator(); remoteRoles.hasNext();)
        {
            Element roleElement = (Element) remoteRoles.next();
            Long directoryId = Long.parseLong(roleElement.element(RoleMapper.REMOTE_ROLE_XML_DIRECTORY_ID).getText());
            String roleName = roleElement.elementText(GenericLegacyImporter.GENERIC_XML_NAME);
            addRoleName(directoryId, roleName);
        }                
    }

    private void addRoleName(Long directoryId, String roleName)
    {
        String standardRoleName = toLowerCase(roleName);

        Set<String> roleNames = directoryToRoleNames.get(directoryId);
        Set<String> groupNames = directoryToGroupNames.get(directoryId);

        if (roleNames == null)
        {
            roleNames = new HashSet<String>();
            directoryToRoleNames.put(directoryId, roleNames);
        }

        if (roleNames.contains(standardRoleName))
        {
            errors.add("Could not add role with name '" + roleName + "' as it matches another role name in the same directory with id: " + directoryId);
        }
        else if (groupNames != null && groupNames.contains(standardRoleName))
        {
            errors.add("Could not add role with name '" + roleName + "' as it matches another group name in the same directory with id: " + directoryId);
        }
        else
        {
            roleNames.add(standardRoleName);
        }
    }

    private void addGroupName(Long directoryId, String groupName)
    {
        String standardGroupName = toLowerCase(groupName);

        Set<String> roleNames = directoryToRoleNames.get(directoryId);
        Set<String> groupNames = directoryToGroupNames.get(directoryId);

        if (groupNames == null)
        {
            groupNames = new HashSet<String>();
            directoryToGroupNames.put(directoryId, groupNames);
        }

        if (groupNames.contains(standardGroupName))
        {
            errors.add("Could not add group with name '" + groupName + "' as it matches another group name in the same directory with id: " + directoryId);
        }
        else if (roleNames != null && roleNames.contains(standardGroupName))
        {
            errors.add("Could not add group with name '" + groupName + "' as it matches another role name in the same directory with id: " + directoryId);
        }
        else
        {
            groupNames.add(standardGroupName);
        }
    }

    public boolean hasErrors()
    {
        return !errors.isEmpty();
    }

    public List<String> getErrors()
    {
        return errors;
    }
}
