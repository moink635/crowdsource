package com.atlassian.crowd.plugin.web;

import java.util.Map;

/**
 * Interface to render a template into the output
 */
public interface TemplateRenderer
{
    String render(String templatePath, Map contextParams);

    String renderText(String text, Map contextParams);
}
