package com.atlassian.crowd.importer.mappers.csv;

import com.atlassian.crowd.importer.config.CsvConfiguration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.model.MembershipDTO;

import org.apache.commons.collections.OrderedBidiMap;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * MembershipMapper that will map a row of data, eg. "jira-admin", "psmith" to a GroupMembership object.
 * This does not support nested groups since Csv currently does not support.
 */
public class MembershipMapper extends CsvMapper<MembershipDTO>
{
    public MembershipMapper(Long directoryId, OrderedBidiMap configuration)
    {
        super(directoryId, configuration);
    }

    public MembershipDTO mapRow(String[] resultSet) throws ImporterException
    {
        checkNotNull(resultSet);

        String username = getString(resultSet, CsvConfiguration.GROUP_USERNAME);
        String groupName = getString(resultSet, CsvConfiguration.GROUP_NAME);

        // Csv import currently does not support nested groups.
        return new MembershipDTO(MembershipDTO.ChildType.USER, username, groupName);
    }
}
