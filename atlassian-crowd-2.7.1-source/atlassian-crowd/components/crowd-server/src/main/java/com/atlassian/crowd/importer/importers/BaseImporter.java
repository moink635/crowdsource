package com.atlassian.crowd.importer.importers;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.importer.config.Configuration;
import com.atlassian.crowd.importer.exceptions.ImporterException;
import com.atlassian.crowd.importer.model.MembershipDTO;
import com.atlassian.crowd.importer.model.Result;
import com.atlassian.crowd.manager.directory.BulkAddResult;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.model.user.UserTemplateWithCredentialAndAttributes;
import com.google.common.collect.Sets;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Importer that manages the basic import process for users, groups and memberships
 * into Crowd.
 */
abstract class BaseImporter implements Importer
{
    protected final Logger logger = LoggerFactory.getLogger(BaseImporter.class);

    protected final DirectoryManager directoryManager;

    public BaseImporter(DirectoryManager directoryManager)
    {
        if (directoryManager == null)
        {
            throw new IllegalArgumentException("DirectoryManager cannot be null");
        }

        this.directoryManager = directoryManager;
    }

    protected Result importGroups(Collection<GroupTemplate> groups, final Configuration configuration) throws ImporterException
    {
        Result importResult = new Result();

        if (groups != null)
        {
            logger.info("Importing " + groups.size() + " groups.");

            // set the directoryID appropriately for the import
            for (GroupTemplate group : groups)
            {
                group.setDirectoryId(configuration.getDirectoryID());
            }

            try
            {
                BulkAddResult<Group> opResult = directoryManager.addAllGroups(configuration.getDirectoryID(), groups, configuration.isOverwriteTarget());

                importResult.setGroupsImported(opResult.getAddedSuccessfully());
                for (Group entity : opResult.getFailedEntities())
                {
                    importResult.addGroupFailedImport(entity.getName());
                }
                for (Group entity : opResult.getExistingEntities())
                {
                    importResult.addExistingGroup(entity.getName());
                }
            }
            catch (Exception e)
            {
                logger.error("Error attempting to bulk import roles into directory with ID: " + configuration.getDirectoryID(), e);
                throw new ImporterException(e);
            }
        }

        return importResult;
    }

    protected Result importUsers(Collection<UserTemplateWithCredentialAndAttributes> users, final Configuration configuration) throws ImporterException
    {
        Result importResult = new Result();

        if (users != null)
        {
            logger.info("Importing " + users.size() + " users.");

            // set the directoryID appropriately for the import.
            // Also, remove the externalId, which may be meaningless in the new directory (upstream directories may be different)
            for (UserTemplate user : users)
            {
                user.setDirectoryId(configuration.getDirectoryID());
                user.setExternalId(null);
            }

            try
            {
                BulkAddResult<User> opResult = directoryManager.addAllUsers(configuration.getDirectoryID(), users, configuration.isOverwriteTarget());

                importResult.setUsersImported(opResult.getAddedSuccessfully());
                for (User entity : opResult.getFailedEntities())
                {
                    importResult.addUsersFailedImport(entity.getName());
                }
                for (User entity : opResult.getExistingEntities())
                {
                    importResult.addExistingUser(entity.getName());
                }
            }
            catch (Exception e)
            {
                logger.error("Error attempting to bulk import principals into directory with ID: " + configuration.getDirectoryID(), e);
                throw new ImporterException(e);
            }
        }

        return importResult;
    }

    private Map<String, Collection<String>> mapContainersToChildren(Iterable<? extends MembershipDTO> memberships)
    {
        Map<String, Collection<String>> containerToPrincipals = new HashMap<String, Collection<String>>();

        for (MembershipDTO membership : memberships)
        {
            if (containerToPrincipals.get(membership.getParentName()) == null)
            {
                containerToPrincipals.put(membership.getParentName(), new ArrayList<String>());
            }
            containerToPrincipals.get(membership.getParentName()).add(membership.getChildName());
        }

        return containerToPrincipals;
    }

    protected Result importUserMemberships(Iterable<MembershipDTO> memberships, final Configuration configuration) throws ImporterException
    {
        Result importResult = new Result();

        if (memberships != null)
        {
            logger.info("Importing user-to-group memberships.");

            // make a map of groupName -> set<principals>
            Map<String, Collection<String>> groupToPrincipalsMap = mapContainersToChildren(memberships);

            try
            {
                for (Map.Entry<String, Collection<String>> groupAndMembers : groupToPrincipalsMap.entrySet())
                {
                    try
                    {
                        BulkAddResult<String> opResult = directoryManager.addAllUsersToGroup(configuration.getDirectoryID(), groupAndMembers.getValue(), groupAndMembers.getKey());
                        importResult.addFailedUserMembershipImports(groupAndMembers.getKey(), opResult.getFailedEntities());
                        importResult.addFailedUserMembershipImports(groupAndMembers.getKey(), opResult.getExistingEntities());
                        importResult.addGroupMembershipsImported(opResult.getAddedSuccessfully());
                    }
                    catch (GroupNotFoundException e)
                    {
                        // move along (group did not exist)
                        logger.error("Could not add memberships for group '" + groupAndMembers.getKey() + "' as it does not exist in directory with ID: " + configuration.getDirectoryID(), e);
                        importResult.addFailedUserMembershipImports(groupAndMembers.getKey(), groupAndMembers.getValue());
                    }
                }

                logger.info("Completed importing " + importResult.getGroupMembershipsImported() + " user-to-group memberships.");
            }
            catch (Exception e)
            {
                logger.error("Error attempting to bulk import group relationships into directory with ID: " + configuration.getDirectoryID(), e);
                throw new ImporterException(e);
            }
        }

        return importResult;
    }

    protected Result importGroupMemberships(Iterable<MembershipDTO> memberships, final Configuration configuration) throws ImporterException
    {
        Result importResult = new Result();

        if (memberships != null)
        {
            logger.info("Importing group-to-group memberships.");

            try
            {
                for (MembershipDTO membership : memberships)
                {
                    try
                    {
                        directoryManager.addGroupToGroup(configuration.getDirectoryID(), membership.getChildName(), membership.getParentName());

                        // if we're here, that means the add operation was successful
                        importResult.addGroupMembershipsImported(1);
                    }
                    catch (GroupNotFoundException e)
                    {
                        // move along if a group does not exist since this should just mean that the group was modified or deleted by somebody else.
                        logger.error("Could not add group '" + membership.getChildName() + "' as child of " + membership.getParentName() + " in directory with ID: " + configuration.getDirectoryID(), e);
                        importResult.addFailedGroupMembershipImport(membership);
                    }
                }

                logger.info("Completed importing " + importResult.getGroupMembershipsImported() + " group-to-group memberships.");
            }
            catch (Exception e)
            {
                logger.error("Error attempting to import group-to-group relationships into directory with ID: " + configuration.getDirectoryID(), e);
                throw new ImporterException(e);
            }
        }

        return importResult;
    }

    @Override
    public Result importUsersGroupsAndMemberships(final Configuration configuration) throws ImporterException
    {
        if (configuration == null || !(getConfigurationType().isInstance((configuration))))
        {
            throw new IllegalArgumentException("The supplied configuration was of the incorrect type for this Importer, should have been: " + getConfigurationType().getCanonicalName());
        }

        init(configuration);

        final Result finalImportResult = new Result();

        // perform the fetching and importing of groups for the given directory
        Collection<GroupTemplate> groups = findGroups(configuration);
        if (groups != null && !groups.isEmpty())
        {
            Result groupsResult = importGroups(groups, configuration);
            finalImportResult.setGroupsImported(groupsResult.getGroupsImported());
            finalImportResult.setGroupsFailedImport(groupsResult.getGroupsFailedImport());
        }

        // perform the fetching and importing of users for the given directory
        Collection<UserTemplateWithCredentialAndAttributes> users = findUsers(configuration);
        if (users != null && !users.isEmpty())
        {
            Result usersResult = importUsers(users, configuration);
            finalImportResult.setUsersImported(usersResult.getUsersImported());
            finalImportResult.setUsersFailedImport(usersResult.getUsersFailedImport());
        }

        // import memberships.
        final Result addedUserMembershipResult = importUserToGroupMemberships(configuration);
        final Result addedGroupMembershipResult = importGroupToGroupMemberships(configuration);

        // construct the proper final result object.
        finalImportResult.setGroupMembershipsImported(addedUserMembershipResult.getGroupMembershipsImported() + addedGroupMembershipResult.getGroupMembershipsImported());
        finalImportResult.setGroupMembershipsFailedImport(Sets.union(addedUserMembershipResult.getGroupMembershipsFailedImport(),
                                                                     addedGroupMembershipResult.getGroupMembershipsFailedImport()));

        // If everything was successful, return the result
        return finalImportResult;
    }

    private Result importUserToGroupMemberships(Configuration configuration) throws ImporterException
    {
        final Collection<MembershipDTO> userToGroups = findUserToGroupMemberships(configuration);

        if (userToGroups != null)
        {
            return importUserMemberships(userToGroups, configuration);
        }

        return new Result();
    }

    private Result importGroupToGroupMemberships(Configuration configuration) throws ImporterException
    {
        if (configuration.getImportNestedGroups())
        {
            // perform the fetching and importing of group-to-group memberships
            final Collection<MembershipDTO> groupToGroups = findGroupToGroupMemberships(configuration);

            if (groupToGroups != null)
            {
                return importGroupMemberships(groupToGroups, configuration);
            }
        }

        return new Result();
    }

    @Override
    public boolean supportsMultipleDirectories(Configuration configuration)
    {
        return false;
    }

    @Override
    public Set<Directory> retrieveRemoteSourceDirectory(Configuration configuration)
    {
        return Collections.emptySet();
    }

    /**
     * Will locate and build a collection of {@link MembershipDTO} objects which
     * represent membership relationships between users and groups based on the passed in Configuration.
     *
     * @param configuration the Importer Configuration
     * @return a collection of GroupMemberships to be imported or Collections.EMPTY_LIST
     * @throws ImporterException if there is an error finding group memberships
     */
    public abstract Collection<MembershipDTO> findUserToGroupMemberships(final Configuration configuration) throws ImporterException;

    /**
     * Will locate and build a collection of {@link MembershipDTO} objects which
     * represent membership relationships between groups and groups based on the passed in Configuration.
     *
     * @param configuration the Importer Configuration
     * @return a collection of GroupMemberships to be imported or Collections.EMPTY_LIST
     * @throws ImporterException if there is an error finding group memberships
     */
    public abstract Collection<MembershipDTO> findGroupToGroupMemberships(final Configuration configuration) throws ImporterException;

    /**
     * Will locate and build a collection of GroupTemplate objects based on the passed in Configuration
     *
     * @param configuration the Importer Configuration
     * @return a collection of GroupTemplates to be imported or Collections.EMPTY_LIST
     * @throws ImporterException if there is an error finding the groups
     */
    public abstract Collection<GroupTemplate> findGroups(final Configuration configuration) throws ImporterException;

    /**
     * Will locate and build a collection of UserTemplate objects based on the passed in Configuration
     *
     * @param configuration the Importer Configuration
     * @return a collection of UserTemplates to be imported or Collections.EMPTY_LIST
     * @throws ImporterException if there is an error finding the users
     */
    public abstract Collection<UserTemplateWithCredentialAndAttributes> findUsers(final Configuration configuration) throws ImporterException;
}