package com.atlassian.crowd.event.user;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.model.user.User;

/**
 * An Event that represents the renaming of a {@link com.atlassian.crowd.model.user.User}
 */
public class UserRenamedEvent extends UserUpdatedEvent
{
    private final String oldUsername;

    public UserRenamedEvent(Object source, Directory directory, User user, String oldUsername)
    {
        super(source, directory, user);
        this.oldUsername = oldUsername;
    }

    public String getOldUsername()
    {
        return oldUsername;
    }
}
