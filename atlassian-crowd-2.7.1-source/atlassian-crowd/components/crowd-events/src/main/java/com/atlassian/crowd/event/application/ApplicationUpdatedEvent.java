package com.atlassian.crowd.event.application;

import com.atlassian.crowd.model.application.Application;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Occurs when the configuration of an application changes.
 *
 * @since v2.6.2
 *
 * @see ApplicationDirectoryAddedEvent
 * @see ApplicationDirectoryRemovedEvent
 * @see ApplicationDirectoryOrderUpdatedEvent
 * @see ApplicationRemoteAddressAddedEvent
 * @see ApplicationRemoteAddressRemovedEvent
 */
public class ApplicationUpdatedEvent
{
    private final Application application;

    public ApplicationUpdatedEvent(Application application)
    {
        this.application = checkNotNull(application);
    }

    public Application getApplication()
    {
        return application;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ApplicationUpdatedEvent that = (ApplicationUpdatedEvent) o;

        return application.equals(that.application);
    }

    @Override
    public int hashCode()
    {
        return application.hashCode();
    }
}
