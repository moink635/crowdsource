package com.atlassian.crowd.selenium.tests.console;

import com.atlassian.crowd.selenium.utils.CrowdSeleniumTestCase;

public class GroupGroupPickerRemoveTest extends CrowdSeleniumTestCase
{
    private static final String CROWD_ADMINISTRATORS_GROUP = "crowd-administrators";
    private static final String CROWD_TESTERS_GROUP = "crowd-testers";

    @Override
    protected void onSetUp()
    {
        super.onSetUp();
        //Load data for test
        restoreCrowdFromXML("picker_remove.xml");
        // NOTE: admin is already in 'crowd-administrators' group
        // Users 000-009, 042, 380, 990-999 are in 'crowd-test'

        // Matching on Name (ie. First000) for dialog since if group is already
        //  in the group, 'group000' will be in parent page.
        // Not matching for 'Super User' since that is always on parent page
    }

    public void testSearchEmpty()
    {
        log("Running: testSearchEmpty");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeGroups");
        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextNotPresent(getText("picker.removegroups.start.message")));
        assertThat.elementPresent(CROWD_TESTERS_GROUP);
        assertThat.elementPresent("group000");
        assertThat.elementPresent("group001");
        assertThat.elementPresent("group002");
        assertThat.elementPresent("group003");
        assertThat.elementPresent("group004");
        assertThat.elementPresent("group005");
        assertThat.elementPresent("group006");
        assertThat.elementPresent("group007");
        assertThat.elementPresent("group008");
        assertThat.elementPresent("group009");
        assertThat.elementNotPresent("group010");

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

    }

    public void testSearchSuccess()
    {
        log("Running: testSearchSuccess");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeGroups");
        _waitForPicker();

        client.type("searchString", "group000");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextNotPresent(getText("picker.removegroups.start.message")));
        assertThat.elementPresent("group000");

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }


    public void testSearchNoResults()
    {
        log("Running: testSearchNoResults");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeGroups");
        _waitForPicker();

        client.type("searchString", "non-existent-group");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(getText("picker.no.results.message")));
        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }


    public void testRemoveSingle()
    {
        log("Running: testRemoveSingle");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeGroups");
        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextNotPresent(getText("picker.removegroups.start.message")));

        client.click("group000"); //checkbox id is groupname
        client.click(htmlButton("picker.removeselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
        assertThat.textNotPresent("group000");   // Check group has been removed
    }


    public void testRemoveMultiple()
    {
        log("Running: testRemoveMultiple");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeGroups");
        _waitForPicker();
        client.click("searchButton");

        // ajax wait
        client.waitForCondition(seleniumJsTextNotPresent(getText("picker.removegroups.start.message")));
        assertThat.textPresent("Description009"); // last group


        client.click("group000"); //checkbox id is groupname
        client.click("group001");
        client.click("group005");

        client.click(htmlButton("picker.removeselected.groups.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        // retry search
        client.click("removeGroups");
        _waitForPicker();
        client.click("searchButton");
        assertThat.elementNotPresent("group000");
        assertThat.elementNotPresent("group001");
        assertThat.elementNotPresent("group005");

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
        assertThat.textNotPresent("group000"); // check new groups been removed
        assertThat.textNotPresent("group001");
        assertThat.textNotPresent("group005");

        assertThat.textPresent("group002"); //check original groups still there
        assertThat.textPresent("group003");
        assertThat.textPresent("group004");
        assertThat.textPresent("group006");
        assertThat.textPresent("group007");
        assertThat.textPresent("group008");
        assertThat.textPresent("group009");
        assertThat.textPresent(CROWD_TESTERS_GROUP);
    }


    public void testRemoveAll()
    {
        log("Running: testRemoveAll");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeGroups");
        _waitForPicker();
        client.click("searchButton");

        // ajax wait
        client.waitForCondition(seleniumJsTextNotPresent(getText("picker.removegroups.start.message")));

        client.click("selectAllRelations"); //checkbox to select all groups
        client.click(htmlButton("picker.removeselected.groups.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.elementNotPresent("removeGroups");
        assertThat.textNotPresent("group000"); // no more ppl in group
        assertThat.textNotPresent("group001");
        assertThat.textNotPresent("group002");
        assertThat.textNotPresent("group003");
        assertThat.textNotPresent("group004");
        assertThat.textNotPresent("group005");
        assertThat.textNotPresent("group006");
        assertThat.textNotPresent("group007");
        assertThat.textNotPresent("group008");
        assertThat.textNotPresent("group009");
        assertThat.textNotPresent(CROWD_TESTERS_GROUP);
    }


    public void testResultsPerPage()
    {
        log("Running: testResultsPerPage");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeGroups");
        _waitForPicker();
        // 100 results - total of 22 should show all groups (group000 ... group999)
        client.click("searchButton");
        // ajax wait
        client.waitForCondition(seleniumJsTextNotPresent(getText("picker.removegroups.start.message")));
        assertThat.elementPresent("group000");
        assertThat.elementPresent("group009");

        // change to 10 results - show up to group009 only
        client.select("resultsPerPage", "label=10");
        client.click("searchButton");

        // group009 checkbox should no longer be visible
        client.waitForCondition(seleniumJsElementNotPresent("group009"));
        assertThat.elementPresent("group000");
        assertThat.elementPresent("group008");
        assertThat.elementNotPresent("group009"); //to double check
    }

    public void testSearchExisting()
    {
        log("Running: testSearchExisting");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeGroups");
        _waitForPicker();

        // change to 10 results
        client.select("resultsPerPage", "label=10");
        // 10 results - last group: group009 (0...9)
        client.click("searchButton");

        // remove groups 3,6,9
        client.waitForCondition(seleniumJsTextNotPresent(getText("picker.removegroups.start.message")));
        client.click("group003");
        client.click("group006");
        client.click("group008");
        client.click(htmlButton("picker.removeselected.groups.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        // search again, groups should not be there
        client.click("removeGroups");
        _waitForPicker();
        client.click("searchButton");
        client.waitForCondition(seleniumJsTextNotPresent(getText("picker.removegroups.start.message")));
        assertThat.elementNotPresent("group003");
        assertThat.elementNotPresent("group006");
        assertThat.elementNotPresent("group008");
        assertThat.elementPresent("group009");

        client.type("searchString", "group003");
        client.click("searchButton");
        client.waitForCondition(seleniumJsTextPresent(getText("picker.no.results.message")));

        client.click(htmlButton("picker.close.label"));
        assertThat.textNotPresent(getText("browser.maximumresults.label"));
        assertThat.textNotPresent("group003"); // check new groups been removed
        assertThat.textNotPresent("group006");
        assertThat.textNotPresent("group008");

    }

}
