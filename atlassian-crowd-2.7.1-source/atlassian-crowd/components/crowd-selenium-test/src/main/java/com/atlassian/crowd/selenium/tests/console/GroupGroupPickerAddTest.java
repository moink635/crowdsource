package com.atlassian.crowd.selenium.tests.console;

import com.atlassian.crowd.selenium.utils.CrowdSeleniumTestCase;


public class GroupGroupPickerAddTest extends CrowdSeleniumTestCase
{
    private static final String CROWD_ADMINISTRATORS_GROUP = "crowd-administrators";
    private static final String CROWD_TESTERS_GROUP = "crowd-testers";


    @Override
    protected void onSetUp()
    {
        super.onSetUp();
        //Load data for test
        restoreCrowdFromXML("picker_add.xml");
        //NOTE: admin is already in 'crowd-administrators' group

        // crowd-administrators contains crowd-testers
        // groups: group000 - group010 exist
    }


    /**
     * Tests search when nothing is entered into search box.
     * Maxium results is default value (100)
     */
    public void testSearchEmpty()
    {
        log("Running: testSearchEmpty");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addGroups");
        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("Description000"));

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }

    /**
     * Test search when value entered into search box and results returned
     * Maximum results is default value (100)
     */
    public void testSearchSuccess()
    {
        log("Running: testSearchSuccess");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addGroups");
        _waitForPicker();

        client.type("searchString", "group007");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("Description007"));
        assertThat.textNotPresent("Description000");

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }

    /**
     * Test search when value entered into search box and no results returned
     * Maximum results is default value (100)
     */
    public void testSearchNoResults()
    {
        log("Running: testSearchNoResults");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addGroups");
        _waitForPicker();

        client.type("searchString", "non-existent-group");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(getText("picker.no.results.message")));

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

    }

    /**
     * Test adding one single group
     * Maximum results is default value (100)
     */
    public void testAddSingle()
    {
        log("Running: testAddSingle");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addGroups");
        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("Description000"));

        client.click("group000");
        client.click(htmlButton("picker.addselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
        assertThat.textPresent("group000");
    }


    /**
     * Test adding many groups
     * Maximum results is default value (100)
     */
    public void testAddMultiple()
    {
        log("Running: testAddMultiple");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addGroups");
        _waitForPicker();
        client.click("searchButton");

        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("Description000"));
        assertThat.textPresent("Description010");   // last group

        client.click("group000"); //checkbox id is groupname
        client.click("group001");
        client.click("group007"); // last group

        client.click(htmlButton("picker.addselected.groups.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        // now try retry the search
        client.click("addGroups");
        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("Description010"));
        assertThat.elementNotPresent("group000");
        assertThat.elementNotPresent("group001");
        assertThat.elementNotPresent("group007");

        client.click(htmlButton("picker.close.label"));

        // make sure that the members appear
        assertThat.textNotPresent(getText("browser.maximumresults.label"));
        assertThat.textPresent(CROWD_TESTERS_GROUP); // check testers still there
        assertThat.textPresent("group000");
        assertThat.textPresent("group001");
        assertThat.textPresent("group007");
    }

    /**
     * Testing the 'select all' checkbox to add all groups on page.
     * Max results is 10.
     */
    public void testAddAll()
    {
        log("Running: testAddAll");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        assertThat.textPresent(CROWD_TESTERS_GROUP); // originally in the group
        
        client.click("addGroups");
        _waitForPicker();
        client.select("resultsPerPage", "label=10");
        client.click("searchButton");
        client.waitForCondition(seleniumJsTextPresent("Description000"));

        client.click("selectAllRelations");
        assertThat.elementPresent("group000");
        assertThat.elementPresent("group009");
        assertThat.elementNotPresent(CROWD_TESTERS_GROUP); //already in group
        assertThat.elementNotPresent("group010"); // is the 11th search result (limit to 10)

        client.click(htmlButton("picker.addselected.groups.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textPresent(CROWD_TESTERS_GROUP); // originally in the group
        assertThat.textPresent("group000"); // newly added members
        assertThat.textPresent("group001");
        assertThat.textPresent("group002");
        assertThat.textPresent("group003");
        assertThat.textPresent("group004");
        assertThat.textPresent("group005");
        assertThat.textPresent("group006");
        assertThat.textPresent("group007");
        assertThat.textPresent("group008");
        assertThat.textPresent("group009");

        // now try search again
        client.click("addGroups");
        _waitForPicker();
        client.select("resultsPerPage", "label=10");
        client.click("searchButton");

        // search returns only 1 result (only group010 is not a member)
        client.waitForCondition(seleniumJsTextPresent("group010"));
        assertThat.elementNotPresent("group000");
        assertThat.elementNotPresent("group001");
        assertThat.elementNotPresent("group002");
        assertThat.elementNotPresent("group003");
        assertThat.elementNotPresent("group004");
        assertThat.elementNotPresent("group005");
        assertThat.elementNotPresent("group006");
        assertThat.elementNotPresent("group007");
        assertThat.elementNotPresent("group008");
        assertThat.elementNotPresent("group009");

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }


    public void testResultsPerPage()
    {
        log("Running: testResultsPerPage");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addGroups");
        _waitForPicker();
        // 100 results - last user: user099 (0...99)
        client.click("searchButton");
        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("Description000"));
        client.waitForCondition(seleniumJsTextPresent("Description010"));

        // change to 10 results
        client.select("resultsPerPage", "label=10");
        // 10 results - last user: user009 (0...9)
        client.click("searchButton");
        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("Description000"));
        client.waitForCondition(seleniumJsTextNotPresent("Description010"));
    }

    public void testSearchExisting()
    {
        log("Running: testSearchExisting");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addGroups");
        _waitForPicker();

        // change to 10 results
        client.select("resultsPerPage", "label=10");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("Description003"));
        client.click("group003");
        client.click("group006");
        client.click("group005");
        client.click(htmlButton("picker.addselected.groups.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        // search again, users should not be there
        client.click("addGroups");
        _waitForPicker();
        client.click("searchButton");
        client.waitForCondition(seleniumJsTextPresent("Description000"));
        assertThat.elementNotPresent("group003");
        assertThat.elementNotPresent("group006");
        assertThat.elementNotPresent("group005");

        client.type("searchString", "group003");
        client.click("searchButton");
        client.waitForCondition(seleniumJsTextPresent(getText("picker.no.results.message")));

        client.click(htmlButton("picker.close.label"));
        assertThat.textNotPresent(getText("browser.maximumresults.label"));
        assertThat.textPresent("group003"); 
        assertThat.textPresent("group006");
        assertThat.textPresent("group005");

    }
}