package com.atlassian.crowd.selenium.tests.console;

import com.atlassian.crowd.selenium.utils.CrowdSeleniumTestCase;


public class UserGroupPickerRemoveTest extends CrowdSeleniumTestCase
{
    private static final String CROWD_ADMINISTRATORS_GROUP = "crowd-administrators";
    private static final String CROWD_TESTERS_GROUP = "crowd-testers";

    private static final String DIRECTORY_ID = "1";


    @Override
    protected void onSetUp()
    {
        super.onSetUp();
        //Load data for test
        restoreCrowdFromXML("picker_remove.xml");
        // NOTE: admin is already in 'crowd-administrators' group
        // Users 000-009, 042, 380, 990-999 are in 'crowd-testers'

        // Matching on Name (ie. First000) for dialog since if user is already
        //  in the group, 'user000' will be in parent page.
        // Not matching for 'Super User' since that is always on parent page

        // For User/Group memberships add some memberships to user000
        addGroupsToUser("user000");
    }

    private void addGroupsToUser(String user)
    {
        // For User/Group memberships add some memberships to user
        gotoViewUser(user, DIRECTORY_ID);
        client.click("user-groups-tab"); // Groups tab
        client.click("addGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("group000"));
        client.click("group000");
        client.click("group001");
        client.click("group003");
        client.click(htmlButton("picker.addselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textPresent("group000");
        assertThat.textPresent("group001");
        assertThat.textPresent("group003");
        assertThat.textNotPresent("group002"); // didn't add this group
    }

    public void testRemoveSingle()
    {
        log("Running: testRemoveSingle");

        gotoViewUser("user000", DIRECTORY_ID);

        client.click("user-groups-tab");

        assertThat.textPresent(CROWD_TESTERS_GROUP);
        client.click("removeGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsElementPresent(CROWD_TESTERS_GROUP));
        client.click(CROWD_TESTERS_GROUP);
        client.click(htmlButton("picker.removeselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textNotPresent(CROWD_TESTERS_GROUP);
        assertThat.textPresent("group000");
        assertThat.textPresent("group001");
        assertThat.textPresent("group003");

    }

    public void testRemoveMultiple()
    {
       log("Running: testRemoveMultiple");

        gotoViewUser("user000", DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsElementPresent(CROWD_TESTERS_GROUP));
        client.click(CROWD_TESTERS_GROUP);
        client.click("group003");
        client.click(htmlButton("picker.removeselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textNotPresent(CROWD_TESTERS_GROUP); // removed
        assertThat.textNotPresent("group003"); // removed
        assertThat.textPresent("group000");
        assertThat.textPresent("group001");


    }

    public void testRemoveAll()
    {
       log("Running: testRemoveAll");

       gotoViewUser("user000", DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsElementPresent(CROWD_TESTERS_GROUP));
        client.click("selectAllRelations");

        client.click(htmlButton("picker.removeselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textNotPresent(CROWD_TESTERS_GROUP); // all memberships removed
        assertThat.textNotPresent("group000");
        assertThat.textNotPresent("group001");
        assertThat.textNotPresent("group003");
    }

    public void testSearchEmpty()
    {
        log("Running: testSearchEmpty");

        gotoViewUser("user000", DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(CROWD_TESTERS_GROUP));
        assertThat.textPresent("group000");
        assertThat.textPresent("group001");
        assertThat.textPresent("group003");
        assertThat.textNotPresent(CROWD_ADMINISTRATORS_GROUP); // not a membership

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }

    public void testSearchSpecific()
    {
        log("Running: testSearchSpecific");

        gotoViewUser("user000", DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.type("searchString", "tester");
        client.click("searchButton");

        client.waitForCondition(seleniumJsElementPresent(CROWD_TESTERS_GROUP));
        assertThat.elementNotPresent("group000");
        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }

    public void testSearchNoResults()
    {
        log("Running: testSearchNoResults");

        gotoViewUser("user000", DIRECTORY_ID);

       client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.type("searchString", "none");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(getText("picker.no.results.message")));
        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }

    public void testSearchNonMembership()
    {
        // Test that searching for a group that the user is not a member of will return nothing
        // Cannot remove a user/group membership if it does not exist.
        log("Running: testSearchNonMembership");

        gotoViewUser("user000", DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.type("searchString", CROWD_ADMINISTRATORS_GROUP);
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(getText("picker.no.results.message")));
        client.click(htmlButton("picker.close.label"));
        
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textPresent(CROWD_TESTERS_GROUP);
    }
}
