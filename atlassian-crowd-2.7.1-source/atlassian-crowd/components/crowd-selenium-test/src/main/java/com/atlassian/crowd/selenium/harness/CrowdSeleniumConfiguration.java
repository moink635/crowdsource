package com.atlassian.crowd.selenium.harness;

import com.atlassian.selenium.AbstractSeleniumConfiguration;

public class CrowdSeleniumConfiguration extends AbstractSeleniumConfiguration
{
    // Code stolen from ConfluenceSeleniumConfiguration.java (rev.72639)
    // Modify browser value to work on your machine.
    public static final String DEFAULT_SELENIUM_LOCATION = "localhost";
    public static final int DEFAULT_SELENIUM_PORT = 4444;
    public static final String DEFAULT_SELENIUM_BROWSER = "*firefox /Applications/Firefox 2.app/Contents/MacOS/firefox-bin";
    public static final String DEFAULT_BASEURL = "http://localhost:8095/crowd/";
    public static final int DEFAULT_MAX_WAIT_TIME = 10000;

    private static final String LOCATION = System.getProperty("selenium.location", DEFAULT_SELENIUM_LOCATION);
    private static final int PORT = Integer.getInteger("selenium.port", DEFAULT_SELENIUM_PORT);
    private static final String BROWSER = System.getProperty("selenium.browser", DEFAULT_SELENIUM_BROWSER);
    private static final String BASE_URL = System.getProperty("crowd.url", DEFAULT_BASEURL);
    private static final long MAX_WAIT_TIME = Integer.getInteger("selenium.max.wait.time", DEFAULT_MAX_WAIT_TIME);


    public String getServerLocation()
    {
        return LOCATION;
    }

    public int getServerPort()
    {
        return PORT;
    }

    public String getBrowserStartString()
    {
        return BROWSER;
    }

    public String getBaseUrl()
    {
        return BASE_URL;
    }

    public long getActionWait()
    {
        return MAX_WAIT_TIME;
    }

    public long getPageLoadWait()
    {
        return MAX_WAIT_TIME;
    }

    public boolean getStartSeleniumServer()
    {
        return false;
    }
}
