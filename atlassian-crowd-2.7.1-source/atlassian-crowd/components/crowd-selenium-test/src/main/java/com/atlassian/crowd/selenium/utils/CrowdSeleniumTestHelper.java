package com.atlassian.crowd.selenium.utils;

import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;

public class CrowdSeleniumTestHelper
{
    private static final String IT_TEST_RESOURCES = "target/it-classes";
    
    public static String getTestResourcesLocation()
    {
        return IT_TEST_RESOURCES;
    }

    public static File getResource(String fileName)
    {
        return new File(getTestResourcesLocation(), fileName);
    }
}
