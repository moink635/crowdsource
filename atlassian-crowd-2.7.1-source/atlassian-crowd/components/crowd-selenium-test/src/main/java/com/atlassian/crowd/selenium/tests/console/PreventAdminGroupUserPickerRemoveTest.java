package com.atlassian.crowd.selenium.tests.console;

import com.atlassian.crowd.selenium.utils.CrowdSeleniumTestCase;

public class PreventAdminGroupUserPickerRemoveTest extends CrowdSeleniumTestCase
{
    private static final String CROWD_ADMIN_GROUP1 = "crowd-administrators";
    private static final String CROWD_ADMIN_GROUP2 = "crowd-admin2";
    private static final String CROWD_SUB_GROUP1 = "subgroup1";
    private static final String CROWD_SUB_GROUP2 = "subgroup2";
    private static final String CROWD_NORMAL_GROUP1 = "normalgroup1";
    private static final String CROWD_NORMAL_GROUP2 = "normalgroup2";


    private static final String DEFAULT_PASSWORD = "password";

    private static final String DIRECTORY_ID = "1";
    private static final String USER2 = "user2";
    private static final String USER1 = "user1";
    private static final String USER3 = "user3";
    private static final String DIRECTORY_NAME = "Test Internal Directory";


    @Override
    protected void onSetUp()
    {
        super.onSetUp();
        //Load data for test
        restoreCrowdFromXML("prevent_admin_group_removal.xml");

        // == Group structure ==
        // crowd-administrators
        //  - subgroup1
        //  - subgroup2
        // crowd-admin2
        // normalgroup1
        // normalgroup2

        // crowd-administrators, crowd-admin2 given admin rights (therefore subgroup1, subgroup2 also have admin rights)

        // == Memberships ==
        // admin: crowd-administrators, normalgroup1, normalgroup2
        // user1: subgroup1, normalgroup1
        // user2: subgroup1, subgroup2, crowd-admin2
        // user3: crowd-administrators, crowd-admin2, subgroup1, subgroup2, normalgroup1, normalgroup2
        // normaluser: normalgroup1, normalgroup2
    }

    /**
     * Testing removing memberships related to logged in user
     */
    public void testRemoveLastAdminMembership()
    {
        // admin is only a member of crowd-administrators
        // removing the membership should be prevented
        log("Running: testRemoveLastAdminMembership");

        gotoViewGroup(CROWD_ADMIN_GROUP1, DIRECTORY_NAME);

        client.click("view-group-users", true);
        client.click("removeUsers");
        _waitForPicker();
        client.click("searchButton");

        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("User Three"));

        client.click("selectAllRelations"); //checkbox to select all users
        client.click(htmlButton("picker.removeselected.users.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textPresent(END_OF_WARNING_MESSAGE);

        assertThat.textPresent(ADMIN_USER); // admin remains
        assertThat.textNotPresent(USER3); // other user goes
    }

    public void testRemoveNonLastAdminMembership()
    {
        // user2 is a member of more than one admin group
        // should be able to remove from crowd-admin2
        log("Running: testRemoveNonLastAdminMembership");

        _loginAsUser(USER2, DEFAULT_PASSWORD);
        gotoViewGroup(CROWD_ADMIN_GROUP2, DIRECTORY_NAME);

        client.click("view-group-users", true);
        client.click("removeUsers");
        _waitForPicker();
        client.click("searchButton");

        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("User Three"));

        client.click("selectAllRelations"); //checkbox to select all users
        client.click(htmlButton("picker.removeselected.users.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textNotPresent(END_OF_WARNING_MESSAGE);

        // all users removed
        assertThat.textNotPresent(USER2);
        assertThat.textNotPresent(USER3);
    }

    /**
     * Testing removing group memberships of other users
     */
    public void testRemoveLastAdminMembershipForOtherUser()
    {
        // removing other users should be no problems
        log("Running: testRemoveLastAdminMembershipForOtherUser");

        gotoViewGroup(CROWD_SUB_GROUP1, DIRECTORY_NAME);

        client.click("view-group-users", true);
        client.click("removeUsers");
        _waitForPicker();
        client.click("searchButton");

        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("User Three"));

        client.click("selectAllRelations"); //checkbox to select all users
        client.click(htmlButton("picker.removeselected.users.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textNotPresent(END_OF_WARNING_MESSAGE);

        // all users removed
        assertThat.textNotPresent(USER1);
        assertThat.textNotPresent(USER2);
        assertThat.textNotPresent(USER3);
    }
}
