package com.atlassian.crowd.selenium.utils;

import com.atlassian.core.util.ClassLoaderUtils;
import com.atlassian.crowd.selenium.harness.CrowdSeleniumConfiguration;
import com.atlassian.selenium.SeleniumConfiguration;
import com.atlassian.selenium.SeleniumTest;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CrowdSeleniumTestCase extends SeleniumTest
{
    protected static final String ADMIN_GROUP = "administrators";
    protected static final String ADMIN_USER = "admin";
    protected static final String ADMIN_PW = "admin";
    protected static final String ADMIN_FULL_NAME = "Super User";
    protected static final String ADMIN_EMAIL = "crowd@example.com";

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
    private ResourceBundle i18n;
    
    protected static final String END_OF_WARNING_MESSAGE = "would remove your Crowd administration rights.";

    public SeleniumConfiguration getSeleniumConfiguration()
    {
        return new CrowdSeleniumConfiguration();
    }

    protected void onSetUp()
    {
        i18n = ResourceBundle.getBundle("com.atlassian.crowd.console.action.BaseAction");
    }

    protected String getText(String key)
    {
        return i18n.getString(key);
    }


    public void restoreCrowdFromXML(String xmlfilename)
    {

        // Locate the where the xmlfile is
        File fileLocation = CrowdSeleniumTestHelper.getResource("xml/" + xmlfilename);

        if (!fileLocation.exists())
        {
            URL resource = ClassLoaderUtils.getResource("xml/" + xmlfilename, this.getClass());
            fileLocation = new File(resource.getFile());
        }

        if (!fileLocation.exists())
        {
            throw new IllegalArgumentException("File: " + xmlfilename + " does not exist in the resources directory, under the 'xml' directory");
        }

        _loginAdminUser();

        gotoRestore();

        client.type("importFilePath", fileLocation.getAbsolutePath());

        logger.info("Restoring Crowd from: " + fileLocation.getAbsolutePath());

        client.submit("import", true);

        _loginAdminUser();
    }

    public void _loginAdminUser()
    {
        _loginAsUser(ADMIN_USER, ADMIN_PW);
    }

    public void _loginAsUser(String username, String password)
    {
        gotoHome();
        if (client.isTextPresent(getText("menu.logout.label")))
        {
            _logout();
            _loginAsUser(username, password);
        }
        else
        {
            client.type("j_username", username);
            client.type("j_password", password);
            client.submit("login", true);

            //Quick check logged in
            assertThat.textPresent(getText("menu.logout.label"));
        }
    }

    public void _logout()
    {
        client.open("console/logoff.action");
        assertThat.textPresent(getText("login.title"));
    }

    protected void log(String message)
    {
        logger.info(message);
    }

    // Making some Selenium calls clearer
    protected String htmlButton(String i8lnKey)
    {
        // for <button> foo </button> elements
        return ("//button[contains(text(), '" + getText(i8lnKey) + "')]");    
    }


    // Javscript required to test ajax calls (esp waitFor* calls)
    // TextPresent - checks to see if the text is on the page
    // ElementPresent - checks to see if the element with the ID is on the page
    protected String seleniumJsTextPresent(String text)
    {
        return "selenium.isTextPresent('" + text + "');";
    }

    protected String seleniumJsTextNotPresent(String text)
    {
        return "!selenium.isTextPresent('" + text + "');";
    }

    protected String seleniumJsElementPresent(String elementId)
    {
        return "selenium.isElementPresent('" + elementId + "')";
    }

    protected String seleniumJsElementNotPresent(String elementId)
    {
        return "!selenium.isElementPresent('" + elementId + "')";
    }


    // Note:
    //   baseURL must end WITH trailing '/'
    //   relativeURL must begin WITHOUT starting '/'
    //   for clien.open to go to the correct URL (otherwise context in baseURL can be lost)
    //   see http://clearspace.openqa.org/thread/14571
    // Easy 'goto' commands
    protected void gotoHome()
    {
        client.open("console/secure/console.action");
    }

    protected void gotoRestore()
    {
        client.open("console/secure/admin/restore.action");
    }

    protected void gotoViewGroup(String groupName, String directoryName)
    {
        client.open("console/secure/group/view.action?name=" + groupName + "&directoryName=" + directoryName);
    }

    protected void gotoViewRole(String roleName, String directoryName)
    {
        client.open("console/secure/role/view.action?name=" + roleName + "&directoryName=" + directoryName);
    }

    protected void gotoViewUser(String userName, String directoryID)
    {
        client.open("console/secure/user/view!default.action?directoryID="+directoryID+"&name="+userName);
    }

    protected void goToImportUsersCSV()
    {
        client.open("console/secure/dataimport/importcsv!default.action");
    }

    protected void gotoDirectories()
    {
        client.open("console/secure/directory/browse.action");
    }

    protected void _waitForPicker()
    {
        client.waitForCondition(seleniumJsTextPresent(getText("browser.maximumresults.label")));
        client.waitForCondition(seleniumJsElementPresent("searchString"));
    }
}
