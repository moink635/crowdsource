package com.atlassian.crowd.selenium.tests.console.directory;

import com.atlassian.crowd.directory.GenericLDAP;
import com.atlassian.crowd.directory.NovelleDirectory;
import com.atlassian.crowd.selenium.utils.CrowdSeleniumTestCase;

public class CreateConnectorTest extends CrowdSeleniumTestCase
{

    public static final String CUSTOM_USER_OBJECT_CLASS = "non-default user object class";
    public static final String USER_OBJECT_CLASS_FIELD = "userObjectClass";
    public static final String REFERRAL_FIELD = "referral";

    public void testDefaultValuesDoNotOverrideUserChangesAfterConnectionTest()
    {
        gotoNewConnectorConfigurationTab();

        // Selenium is not effectively changing the option when selected by value
        //client.selectOption("connector", "value=" + GenericLDAP.class.getCanonicalName());
        client.selectOption("connector", GenericLDAP.getStaticDirectoryType());
        assertEquals(GenericLDAP.class.getCanonicalName(), client.getSelectedValue("connector"));
        // invariant: the defaults for this connector
        assertFalse(client.isChecked(REFERRAL_FIELD));
        assertEquals("inetorgperson", client.getValue(USER_OBJECT_CLASS_FIELD));

        // change the values to be different from their default values
        client.check(REFERRAL_FIELD);
        client.type(USER_OBJECT_CLASS_FIELD, CUSTOM_USER_OBJECT_CLASS);

        client.click("test-connection");
        client.waitForPageToLoad();

        assertEquals(GenericLDAP.class.getCanonicalName(), client.getSelectedValue("connector"));
        assertTrue("User changes to this field must not be overridden by defaults", client.isChecked(REFERRAL_FIELD));
        assertEquals("User changes to this field must not be overridden by defaults",
                     CUSTOM_USER_OBJECT_CLASS, client.getValue(USER_OBJECT_CLASS_FIELD));
    }

    public void testDefaultValuesOverrideUserChangesWhenConnectorIsChanged()
    {
        gotoNewConnectorConfigurationTab();

        client.selectOption("connector", NovelleDirectory.getStaticDirectoryType());
        assertEquals(NovelleDirectory.class.getCanonicalName(), client.getSelectedValue("connector"));
        // invariant: the defaults for this connector
        assertFalse(client.isChecked(REFERRAL_FIELD));
        assertEquals("inetOrgPerson", client.getValue(USER_OBJECT_CLASS_FIELD));

        client.selectOption("connector", GenericLDAP.getStaticDirectoryType());
        assertEquals(GenericLDAP.class.getCanonicalName(), client.getSelectedValue("connector"));
        // invariant: the defaults for this connector
        assertFalse(client.isChecked(REFERRAL_FIELD));
        assertEquals("inetorgperson", client.getValue(USER_OBJECT_CLASS_FIELD));

        // change the values to be different from their default values
        client.check(REFERRAL_FIELD);
        client.type(USER_OBJECT_CLASS_FIELD, CUSTOM_USER_OBJECT_CLASS);

        // now change to a different connector type
        client.selectOption("connector", NovelleDirectory.getStaticDirectoryType());
        assertEquals(NovelleDirectory.class.getCanonicalName(), client.getSelectedValue("connector"));
        assertFalse("User changes to this field must be overridden by defaults", client.isChecked(REFERRAL_FIELD));
        assertEquals("User changes to this field must be overridden by defaults", "inetOrgPerson", client.getValue(USER_OBJECT_CLASS_FIELD));
    }

    private void gotoNewConnectorConfigurationTab()
    {
        _loginAdminUser();
        gotoDirectories();

        client.click("add-directory");
        client.waitForPageToLoad();
        client.waitForCondition(seleniumJsTextPresent(getText("directory.selector.title")));

        client.click("create-connector");
        client.waitForPageToLoad();
        client.waitForCondition(seleniumJsTextPresent(getText("directoryconnectorcreate.title")));

        client.click("//li[@id='hreftab2']/a");
        client.waitForCondition(seleniumJsElementPresent("connector"));
    }

}
