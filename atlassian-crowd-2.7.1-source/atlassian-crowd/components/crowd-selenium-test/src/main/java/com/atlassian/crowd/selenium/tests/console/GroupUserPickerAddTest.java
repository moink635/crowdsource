package com.atlassian.crowd.selenium.tests.console;

import com.atlassian.crowd.selenium.utils.CrowdSeleniumTestCase;


public class GroupUserPickerAddTest extends CrowdSeleniumTestCase
{
    private static final String CROWD_ADMINISTRATORS_GROUP = "crowd-administrators";
    private static final String CROWD_TESTERS_GROUP = "crowd-testers";


    @Override
    protected void onSetUp()
    {
        super.onSetUp();
        //Load data for test
        restoreCrowdFromXML("picker_add.xml");
        //NOTE: admin is already in 'crowd-administrators' group

        // Matching on Name (ie. First000) for dialog since if user is already
        //  in the group, 'user000' will be in parent page.
        // Not matching for 'Super User' since that is always on parent page
    }


    /**
     * Tests search when nothing is entered into search box.
     * Maxium results is default value (100)
     */
    public void testSearchEmpty()
    {

        log("Running: testSearchEmpty");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addUsers");
        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("First000"));

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

    }

    /**
     * Test search when value entered into search box and results returned
     * Maximum results is default value (100)
     */
    public void testSearchSuccess()
    {
        log("Running: testSearchSuccess");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addUsers");
        _waitForPicker();

        client.type("searchString", "user789");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("First789"));

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }

    /**
     * Test search when value entered into search box and no results returned
     * Maximum results is default value (100)
     */
    public void testSearchNoResults()
    {
        log("Running: testSearchNoResults");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addUsers");
        _waitForPicker();

        client.type("searchString", "non-existent-user");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextNotPresent("First000"));

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

    }

    /**
     * Test adding one single user
     * Maximum results is default value (100)
     * Note: Imported 1000 users, so wait for searches
     */
    public void testAddSingle()
    {
        log("Running: testAddSingle");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addUsers");
        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("First000"));

        client.click("user000"); //checkbox id is username
        client.click(htmlButton("picker.addselected.users.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
        assertThat.textPresent("user000");   // Check user has been added
    }


    /**
     * Test adding many users
     * Maximum results is default value (100)
     * Note: Imported 1000 users, so wait for searches
     */
    public void testAddMultiple()
    {
        log("Running: testAddMultiple");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addUsers");
        _waitForPicker();
        client.click("searchButton");

        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("First000"));
        assertThat.textPresent("First099");   // last user


        client.click("user000"); //checkbox id is username
        client.click("user&001");
        client.click("user\"002");
        client.click("user%20003");
        client.click("user010");
        client.click("user033");
        client.click("user099"); // last user

        client.click(htmlButton("picker.addselected.users.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        // now try retry the search
        client.click("addUsers");
        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextNotPresent("First000")); // Check no longer in search results
        client.waitForCondition(seleniumJsTextNotPresent(getText("picker.addusers.start.message")));
        assertThat.textNotPresent("First001");
        assertThat.textNotPresent("First002");
        assertThat.textNotPresent("First003");
        assertThat.textNotPresent("First010");
        assertThat.textNotPresent("First033");
        assertThat.textNotPresent("First099"); // users all been added
        assertThat.textPresent("First100"); //new user prev hidden now visible

        client.click(htmlButton("picker.close.label"));

        // make sure that the members appear
        assertThat.textNotPresent(getText("browser.maximumresults.label"));
        assertThat.textPresent("admin");   // Check admin still there
        assertThat.textPresent("user000"); // check new users been added
        assertThat.textPresent("user&001");
        assertThat.textPresent("user\"002");
        assertThat.textPresent("user%20003");
        assertThat.textPresent("user010");
        assertThat.textPresent("user033");
        assertThat.textPresent("user099");
    }

    /**
     * testing the 'select all' checkbox to add all users on page
     */
    public void testAddAll()
    {
        log("Running: testAddAll");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addUsers");
        _waitForPicker();
        client.click("searchButton");
        client.waitForCondition(seleniumJsTextPresent("First000"));

        client.click("selectAllRelations"); //checkbox to select all users
        client.click(htmlButton("picker.addselected.users.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textPresent("admin");   // Check admin still there
        assertThat.textPresent("user000"); // Check new users are added
        assertThat.textPresent("user042");
        assertThat.textPresent("user099");

        // now try search again
        client.click("addUsers");
        _waitForPicker();
        client.click("searchButton");
        client.waitForCondition(seleniumJsTextPresent("First100"));
        assertThat.textNotPresent("First000");
        assertThat.textNotPresent("First099");
        assertThat.textPresent("First100"); //new page of users
        assertThat.textPresent("First199");

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }


    public void testResultsPerPage()
    {
        log("Running: testResultsPerPage");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addUsers");
        _waitForPicker();
        // 100 results - last user: user099 (0...99)
        client.click("searchButton");
        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("First099"));
        client.waitForCondition(seleniumJsTextNotPresent("First100"));

        // change to 10 results
        client.select("resultsPerPage", "label=10");
        // 10 results - last user: user009 (0...9)
        client.click("searchButton");
        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("First009"));
        client.waitForCondition(seleniumJsTextNotPresent("First010"));

        // change to 20 results
        client.select("resultsPerPage", "label=20");
        // 10 results - last user: user019 (0...19)
        client.click("searchButton");
        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("First019"));
        client.waitForCondition(seleniumJsTextNotPresent("First020"));

        // change to 50 results
        client.select("resultsPerPage", "label=50");
        // 10 results - last user: user049 (0...49)
        client.click("searchButton");
        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("First049"));
        client.waitForCondition(seleniumJsTextNotPresent("First050"));

    }

    public void testSearchExisting()
    {
        log("Running: testSearchExisting");

        gotoViewGroup(CROWD_ADMINISTRATORS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("addUsers");
        _waitForPicker();

        // change to 10 results
        client.select("resultsPerPage", "label=10");
        // 10 results - last user: user009 (0...9)
        client.click("searchButton");

        // add users 3,6,9
        client.waitForCondition(seleniumJsTextPresent("First003"));
        client.click("user%20003");
        client.click("user006");
        client.click("user009");
        client.click(htmlButton("picker.addselected.users.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        // search again, users should not be there
        client.click("addUsers");
        _waitForPicker();
        client.click("searchButton");
        client.waitForCondition(seleniumJsTextNotPresent("First003"));
        assertThat.textNotPresent("First006");
        assertThat.textNotPresent("First009");

        client.type("searchString", "user%20003");
        client.click("searchButton");
        client.waitForCondition(seleniumJsTextNotPresent("First003"));

        client.click(htmlButton("picker.close.label"));
        assertThat.textNotPresent(getText("browser.maximumresults.label"));
        assertThat.textPresent("user%20003"); // check new users been added
        assertThat.textPresent("user006");
        assertThat.textPresent("user009");

    }
}