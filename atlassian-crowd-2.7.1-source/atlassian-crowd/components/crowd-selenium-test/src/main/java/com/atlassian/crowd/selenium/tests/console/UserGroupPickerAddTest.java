package com.atlassian.crowd.selenium.tests.console;

import com.atlassian.crowd.selenium.utils.CrowdSeleniumTestCase;


public class UserGroupPickerAddTest extends CrowdSeleniumTestCase
{
    private static final String CROWD_ADMINISTRATORS_GROUP = "crowd-administrators";
    private static final String CROWD_TESTERS_GROUP = "crowd-testers";

    private static final String DIRECTORY_ID = "1";


    @Override
    protected void onSetUp()
    {
        super.onSetUp();
        //Load data for test
        restoreCrowdFromXML("picker_add.xml");
        //NOTE: admin is already in 'crowd-administrators' group

        // Matching on Name (ie. First000) for dialog since if user is already
        //  in the group, 'user000' will be in parent page.
        // Not matching for 'Super User' since that is always on parent page
    }

    public void testAddSingle()
    {
        log("Running: testAddSingle");

        gotoViewUser("user000", DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("addGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(CROWD_TESTERS_GROUP));
        client.click(CROWD_TESTERS_GROUP);
        client.click(htmlButton("picker.addselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textPresent(CROWD_TESTERS_GROUP);
        
    }

    public void testAddMultiple()
    {
       log("Running: testAddMultiple");

        gotoViewUser("user000", DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("addGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(CROWD_TESTERS_GROUP));
        client.click(CROWD_TESTERS_GROUP);
        client.click("group001");
        client.click("group003");
        client.click(htmlButton("picker.addselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textPresent(CROWD_TESTERS_GROUP);
        assertThat.textPresent("group001");
        assertThat.textPresent("group003");
        assertThat.textNotPresent("group002"); // didn't add this group

    }

    public void testAddAll()
    {
       log("Running: testAddAll");

        gotoViewUser("user000", DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("addGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(CROWD_TESTERS_GROUP));
        client.click("selectAllRelations");
        client.click(htmlButton("picker.addselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        // All groups should be added - check a few
        assertThat.textPresent(CROWD_ADMINISTRATORS_GROUP);
        assertThat.textPresent(CROWD_TESTERS_GROUP);
        assertThat.textPresent("group000");
        assertThat.textPresent("group008");
        assertThat.textPresent("group010");
    }

    public void testSearchEmpty()
    {
        log("Running: testSearchEmpty");

        gotoViewUser("user000", DIRECTORY_ID);


        client.click("user-groups-tab"); // Groups tab
        client.click("addGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(CROWD_TESTERS_GROUP));
        assertThat.textPresent(CROWD_ADMINISTRATORS_GROUP);
        assertThat.textPresent("group000");

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }

    public void testSearchSpecific()
    {
        log("Running: testSearchSpecific");

        gotoViewUser("user000", DIRECTORY_ID);


        client.click("user-groups-tab"); // Groups tab
        client.click("addGroups");

        _waitForPicker();
        client.type("searchString", "tester");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(CROWD_TESTERS_GROUP));
        assertThat.textNotPresent(CROWD_ADMINISTRATORS_GROUP);
        assertThat.textNotPresent("group000");
        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }

    public void testSearchNoResults()
    {
        log("Running: testSearchSpecific");

        gotoViewUser("user000", DIRECTORY_ID);

       client.click("user-groups-tab"); // Groups tab
        client.click("addGroups");

        _waitForPicker();
        client.type("searchString", "none");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(getText("picker.no.results.message")));
        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }

    public void testSearchExistingMembership()
    {
        // Test that when you search, existing memberships are not displayed 
        // (since you should not be able to add the user/group membership already exists)
        log("Running: testSearchExistingMembership");

        gotoViewUser("user000", DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("addGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(CROWD_TESTERS_GROUP));
        client.click(CROWD_TESTERS_GROUP);
        client.click(htmlButton("picker.addselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textPresent(CROWD_TESTERS_GROUP);

        client.click("addGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent(CROWD_ADMINISTRATORS_GROUP));
        assertThat.elementNotPresent(CROWD_TESTERS_GROUP);
        

    }
}
