package com.atlassian.crowd.selenium.harness;

import com.atlassian.crowd.selenium.tests.console.GroupGroupPickerAddTest;
import com.atlassian.crowd.selenium.tests.console.GroupGroupPickerRemoveTest;
import com.atlassian.crowd.selenium.tests.console.GroupUserPickerAddTest;
import com.atlassian.crowd.selenium.tests.console.GroupUserPickerRemoveTest;
import com.atlassian.crowd.selenium.tests.console.PreventAdminGroupUserPickerRemoveTest;
import com.atlassian.crowd.selenium.tests.console.PreventAdminUserGroupPickerRemoveTest;
import com.atlassian.crowd.selenium.tests.console.UserGroupPickerAddTest;
import com.atlassian.crowd.selenium.tests.console.UserGroupPickerRemoveTest;
import com.atlassian.crowd.selenium.tests.console.directory.CreateConnectorTest;
import com.atlassian.crowd.selenium.tests.console.directory.CreateDelegateTest;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;


public class CrowdSeleniumTestHarness extends TestCase
{
    public static Test suite()
    {
        TestSuite suite = new TestSuite();
        suite.addTestSuite(GroupUserPickerAddTest.class);
        suite.addTestSuite(GroupUserPickerRemoveTest.class);
        suite.addTestSuite(GroupGroupPickerAddTest.class);
        suite.addTestSuite(GroupGroupPickerRemoveTest.class);
        suite.addTestSuite(UserGroupPickerAddTest.class);
        suite.addTestSuite(UserGroupPickerRemoveTest.class);
        suite.addTestSuite(PreventAdminGroupUserPickerRemoveTest.class);
        suite.addTestSuite(PreventAdminUserGroupPickerRemoveTest.class);
        suite.addTestSuite(CreateConnectorTest.class);
        suite.addTestSuite(CreateDelegateTest.class);

        return suite;
    }

}
