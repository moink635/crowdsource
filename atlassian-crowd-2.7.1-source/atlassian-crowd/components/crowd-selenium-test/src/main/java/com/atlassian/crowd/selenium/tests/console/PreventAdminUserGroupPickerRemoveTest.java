package com.atlassian.crowd.selenium.tests.console;

import com.atlassian.crowd.selenium.utils.CrowdSeleniumTestCase;

public class PreventAdminUserGroupPickerRemoveTest extends CrowdSeleniumTestCase
{
    private static final String CROWD_ADMIN_GROUP1 = "crowd-administrators";
    private static final String CROWD_ADMIN_GROUP2 = "crowd-admin2";
    private static final String CROWD_SUB_GROUP1 = "subgroup1";
    private static final String CROWD_SUB_GROUP2 = "subgroup2";
    private static final String CROWD_NORMAL_GROUP1 = "normalgroup1";
    private static final String CROWD_NORMAL_GROUP2 = "normalgroup2";


    private static final String DEFAULT_PASSWORD = "password";

    private static final String DIRECTORY_ID = "1";
    private static final String USER2 = "user2";
    private static final String USER1 = "user1";
    private static final String USER3 = "user3";


    @Override
    protected void onSetUp()
    {
        super.onSetUp();
        //Load data for test
        restoreCrowdFromXML("prevent_admin_group_removal.xml");

        // == Group structure ==
        // crowd-administrators
        //  - subgroup1
        //  - subgroup2
        // crowd-admin2
        // normalgroup1
        // normalgroup2

        // crowd-administrators, crowd-admin2 given admin rights (therefore subgroup1, subgroup2 also have admin rights)

        // == Memberships ==
        // admin: crowd-administrators, normalgroup1, normalgroup2
        // user1: subgroup1, normalgroup1
        // user2: subgroup1, subgroup2, crowd-admin2
        // user3: crowd-administrators, crowd-admin2, subgroup1, subgroup2, normalgroup1, normalgroup2
        // normaluser: normalgroup1, normalgroup2
    }

    /**
     * Testing removing groups from own account
     */

    public void testSelectAllNonAdminGroupsRemoved()
    {
        // admin member of 3 groups: 1 administrator and 2 normal
        // selecting all will remove the 2 normal but keep the 1 administrator
        log("Running: testSelectAllNonAdminGroupsRemoved");

        gotoViewUser(ADMIN_USER, DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsElementPresent(CROWD_ADMIN_GROUP1));
        client.click("selectAllRelations");

        client.click(htmlButton("picker.removeselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textPresent(END_OF_WARNING_MESSAGE);

        assertThat.textPresent(CROWD_ADMIN_GROUP1); // admin group not removed
        assertThat.textNotPresent(CROWD_NORMAL_GROUP1); // non-admin groups are removed
        assertThat.textNotPresent(CROWD_NORMAL_GROUP2);
    }

    public void testSelectAllNoGroupsRemoved()
    {
        // login as user2
        // all memberships are groups with admin rights
        // Selecting all will not remove any of the groups
        log("Running: testSelectAllNoGroupsRemoved");

        _loginAsUser(USER2, DEFAULT_PASSWORD);

        gotoViewUser(USER2, DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsElementPresent(CROWD_ADMIN_GROUP2));
        client.click("selectAllRelations");

        client.click(htmlButton("picker.removeselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textPresent(END_OF_WARNING_MESSAGE);

        assertThat.textPresent(CROWD_ADMIN_GROUP2); // admin group not removed
        assertThat.textPresent(CROWD_SUB_GROUP1); // admin group not removed
        assertThat.textPresent(CROWD_SUB_GROUP2); // admin group not removed
    }

    public void testRemoveOnlyAdminGroup()
    {
        // admin is only member of one admin group - specifically select that one to remove
        // group membership should not be removed
        log("Running: testRemoveOnlyAdminGroup");

        gotoViewUser(ADMIN_USER, DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsElementPresent(CROWD_ADMIN_GROUP1));
        client.click(CROWD_ADMIN_GROUP1);

        client.click(htmlButton("picker.removeselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textPresent(END_OF_WARNING_MESSAGE);

        assertThat.textPresent(CROWD_ADMIN_GROUP1); // admin group not removed
        assertThat.textPresent(CROWD_NORMAL_GROUP1); // other groups not affected
        assertThat.textPresent(CROWD_NORMAL_GROUP2);
    }

    public void testRemoveOnlyAdminSubGroup()
    {
        // user1 is a member of a subgroup that allows admin rights - specifically select that one to remove
        // group membership should not be removed
        log("Running: testRemoveOnlyAdminSubGroup");

        _loginAsUser(USER1, DEFAULT_PASSWORD);
        gotoViewUser(USER1, DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsElementPresent(CROWD_SUB_GROUP1));
        client.click(CROWD_SUB_GROUP1);

        client.click(htmlButton("picker.removeselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textPresent(END_OF_WARNING_MESSAGE);

        assertThat.textPresent(CROWD_SUB_GROUP1); // admin group not removed
        assertThat.textPresent(CROWD_NORMAL_GROUP1); // other groups not affected
    }

    public void testRemoveSubsetOfAdminGroups()
    {
        // user2 is member of 3 groups with admin rights
        // select 2 to remove - both should be removed
        log("Running: testRemoveSubsetOfAdminGroups");

        _loginAsUser(USER2, DEFAULT_PASSWORD);
        gotoViewUser(USER2, DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsElementPresent(CROWD_SUB_GROUP1));
        client.click(CROWD_SUB_GROUP1);
        client.click(CROWD_ADMIN_GROUP2);

        client.click(htmlButton("picker.removeselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textNotPresent(END_OF_WARNING_MESSAGE);

        assertThat.textPresent(CROWD_SUB_GROUP2); // last admin group still there
        assertThat.textNotPresent(CROWD_SUB_GROUP1); // two other admin groups removed
        assertThat.textNotPresent(CROWD_ADMIN_GROUP2);
    }

    public void testRemoveSubsetOfAdminGroupsWithNormal()
    {
        // user3 is member of all groups (4 admin, 2 normal)
        // select to remove 3 admin groups and 2 normal
        // should be left as a member of the one remaining admin group
        log("Running: testRemoveSubsetOfAdminGroupsWithNormal");

        _loginAsUser(USER3, DEFAULT_PASSWORD);
        gotoViewUser(USER3, DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsElementPresent(CROWD_SUB_GROUP1));
        client.click("selectAllRelations");
        client.click(CROWD_SUB_GROUP1); // uncheck a admin group

        client.click(htmlButton("picker.removeselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textNotPresent(END_OF_WARNING_MESSAGE);

        assertThat.textPresent(CROWD_SUB_GROUP1); // last admin group still there
        assertThat.textNotPresent(CROWD_SUB_GROUP2); // all other admin groups removed
        assertThat.textNotPresent(CROWD_ADMIN_GROUP1);
        assertThat.textNotPresent(CROWD_ADMIN_GROUP2);
        assertThat.textNotPresent(CROWD_NORMAL_GROUP1);
        assertThat.textNotPresent(CROWD_NORMAL_GROUP2);
    }


    /**
     * Testing removing groups for another user (should be able to do anything)
     */

    public void testRemoveAllFromOtherUser()
    {
        log("Running: testRemoveAllFromOtherUser");

        gotoViewUser(USER3, DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsElementPresent(CROWD_SUB_GROUP1));
        client.click("selectAllRelations");

        client.click(htmlButton("picker.removeselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textNotPresent(END_OF_WARNING_MESSAGE);

        // all groups are removed
        assertThat.textNotPresent(CROWD_SUB_GROUP1);
        assertThat.textNotPresent(CROWD_SUB_GROUP2);
        assertThat.textNotPresent(CROWD_ADMIN_GROUP1);
        assertThat.textNotPresent(CROWD_ADMIN_GROUP2);
        assertThat.textNotPresent(CROWD_NORMAL_GROUP1);
        assertThat.textNotPresent(CROWD_NORMAL_GROUP2);
    }

    public void testRemoveAllAdminGroupsFromOtherUser()
    {
        log("Running: testRemoveAllAdminGroupsFromOtherUser");

        gotoViewUser(USER1, DIRECTORY_ID);

        client.click("user-groups-tab"); // Groups tab
        client.click("removeGroups");

        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsElementPresent(CROWD_SUB_GROUP1));
        client.click(CROWD_SUB_GROUP1);

        client.click(htmlButton("picker.removeselected.groups.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.textNotPresent(END_OF_WARNING_MESSAGE);

        // all groups are removed
        assertThat.textNotPresent(CROWD_SUB_GROUP1);
        assertThat.textPresent(CROWD_NORMAL_GROUP1);
    }
}
