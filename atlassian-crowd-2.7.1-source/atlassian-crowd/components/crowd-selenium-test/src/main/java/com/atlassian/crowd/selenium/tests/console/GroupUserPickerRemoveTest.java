package com.atlassian.crowd.selenium.tests.console;

import com.atlassian.crowd.selenium.utils.CrowdSeleniumTestCase;

public class GroupUserPickerRemoveTest extends CrowdSeleniumTestCase
{
    private static final String CROWD_TESTERS_GROUP = "crowd-testers";


    @Override
    protected void onSetUp()
    {
        super.onSetUp();
        //Load data for test
        restoreCrowdFromXML("picker_remove.xml");
        // NOTE: admin is already in 'crowd-administrators' group
        // Users 000-009, 042, 380, 990-999 are in 'crowd-test'

        // Matching on Name (ie. First000) for dialog since if user is already
        //  in the group, 'user000' will be in parent page.
        // Not matching for 'Super User' since that is always on parent page
    }

    public void testSearchEmpty()
    {
        log("Running: testSearchEmpty");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeUsers");
        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("First000"));
        assertThat.textPresent("First042");
        assertThat.textPresent("First380");
        assertThat.textPresent("First999");
        assertThat.textNotPresent("First123"); //User not in group, so not visible for removal

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

    }

        public void testSearchSuccess()
    {
        log("Running: testSearchSuccess");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeUsers");
        _waitForPicker();

        client.type("searchString", "user000");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("First000"));

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
    }


    public void testSearchNoResults()
    {
        log("Running: testSearchNoResults");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeUsers");
        _waitForPicker();

        client.type("searchString", "non-existent-user");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextNotPresent("First000"));

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));

    }


    public void testRemoveSingle()
    {
        log("Running: testRemoveSingle");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeUsers");
        _waitForPicker();
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("First042"));

        client.click("user042"); //checkbox id is username
        client.click(htmlButton("picker.removeselected.users.label"), true);

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
        assertThat.textNotPresent("user042");   // Check user has been removed
    }


    public void testRemoveMultiple()
    {
        log("Running: testRemoveMultiple");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeUsers");
        _waitForPicker();
        client.click("searchButton");

        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("First000"));
        assertThat.textPresent("First999"); // last user


        client.click("user000"); //checkbox id is username
        client.click("user001");
        client.click("user042");
        client.click("user995");
        client.click("user999"); // last user

        client.click(htmlButton("picker.removeselected.users.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        // retry search
        client.click("removeUsers");
        _waitForPicker();
        client.click("searchButton");
        client.waitForCondition(seleniumJsTextPresent("First002")); // existing members still visible
        assertThat.textPresent("First380"); // existing members still visible
        
        assertThat.textNotPresent("First001"); // users all been removed
        assertThat.textNotPresent("First042");
        assertThat.textNotPresent("First995");
        assertThat.textNotPresent("First999");

        client.click(htmlButton("picker.close.label"));

        assertThat.textNotPresent(getText("browser.maximumresults.label"));
        assertThat.textNotPresent("user000"); // check new users been removed
        assertThat.textNotPresent("user001");
        assertThat.textNotPresent("user042");
        assertThat.textNotPresent("user995");
        assertThat.textNotPresent("user999");

        assertThat.textPresent("user003"); //check original users still there
        assertThat.textPresent("user004");
        assertThat.textPresent("user380");
        assertThat.textPresent("user990");
        assertThat.textPresent("user998");
    }


    public void testRemoveAll()
    {
        log("Running: testRemoveAll");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeUsers");
        _waitForPicker();
        client.click("searchButton");

        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("First000"));

        client.click("selectAllRelations"); //checkbox to select all users
        client.click(htmlButton("picker.removeselected.users.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        assertThat.elementNotPresent("removeUsers");
        assertThat.textNotPresent("user000"); // no more ppl in group
        assertThat.textNotPresent("user001");
        assertThat.textNotPresent("user042");
        assertThat.textNotPresent("user380");
        assertThat.textNotPresent("user999");
    }


    public void testResultsPerPage()
    {
        log("Running: testResultsPerPage");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeUsers");
        _waitForPicker();
        // 100 results - total of 22 should show all users (user000 ... user999)
        client.click("searchButton");
        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("First000"));
        client.waitForCondition(seleniumJsTextPresent("First999"));

        // change to 10 results - show up to user009 only
        client.select("resultsPerPage", "label=10");
        client.click("searchButton");
        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("First000"));
        client.waitForCondition(seleniumJsTextPresent("First009"));
        client.waitForCondition(seleniumJsTextNotPresent("First042"));

        // change to 20 results - show up to user997 (since 2 extra from user042, user380)
        client.select("resultsPerPage", "label=20");
        client.click("searchButton");
        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("First000"));
        client.waitForCondition(seleniumJsTextPresent("First997"));
        client.waitForCondition(seleniumJsTextNotPresent("First998"));

        // change to 50 results - show all
        client.select("resultsPerPage", "label=50");
        client.click("searchButton");
        // ajax wait
        client.waitForCondition(seleniumJsTextPresent("First000"));
        client.waitForCondition(seleniumJsTextPresent("First999"));

    }

    public void testSearchExisting()
    {
        log("Running: testSearchExisting");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeUsers");
        _waitForPicker();

        // change to 10 results
        client.select("resultsPerPage", "label=10");
        // 10 results - last user: user009 (0...9)
        client.click("searchButton");

        // remove users 3,6,9
        client.waitForCondition(seleniumJsTextPresent("First003"));
        client.click("user003");
        client.click("user006");
        client.click("user009");
        client.click(htmlButton("picker.removeselected.users.label"), true);
        assertThat.textNotPresent(getText("browser.maximumresults.label"));

        // search again, users should not be there
        client.click("removeUsers");
        _waitForPicker();
        client.click("searchButton");
        client.waitForCondition(seleniumJsTextNotPresent("First003"));
        assertThat.textNotPresent("First006");
        assertThat.textNotPresent("First009");

        client.type("searchString", "user003");
        client.click("searchButton");
        client.waitForCondition(seleniumJsTextNotPresent("First003"));

        client.click(htmlButton("picker.close.label"));
        assertThat.textNotPresent(getText("browser.maximumresults.label"));
        assertThat.textNotPresent("user003"); // check new users been removed
        assertThat.textNotPresent("user006");
        assertThat.textNotPresent("user009");

    }


    public void testSearchCutOffs()
    {
        log("Running: testSearchCutOffs");

        gotoViewGroup(CROWD_TESTERS_GROUP, "Atlassian");

        client.click("view-group-users", true);
        client.click("removeUsers");
        _waitForPicker();

        // Search for '0' (zero)...
        // Should return user000-009, user042, user380, user990
        client.type("searchString", "0");
        client.click("searchButton");

        client.waitForCondition(seleniumJsTextPresent("First000"));
        assertThat.textPresent("First001");
        assertThat.textPresent("First042");
        assertThat.textPresent("First380");
        assertThat.textPresent("First990");

        assertThat.textNotPresent("First991"); // Just to check these are not displayed
        assertThat.textNotPresent("First999"); 

        // In order to display user380, 990 must search ENTIRE directory
        // otherwise user380, 990 will not be displayed because PageResult is 100, #users is 22
        // search up to 122 users within the directory (user122)...which leaves out the later users...


    }

}
