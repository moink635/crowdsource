package com.atlassian.crowd.openid.server.manager.profile;

public class ProfileAccessViolationException extends ProfileManagerException
{

    public ProfileAccessViolationException()
    {
        super();
    }

    public ProfileAccessViolationException(String message)
    {
        super(message);
    }

    public ProfileAccessViolationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ProfileAccessViolationException(Throwable cause)
    {
        super(cause);   
    }
}
