package com.atlassian.crowd.openid.server.manager.profile;

public class DefaultProfileDeleteException extends ProfileManagerException
{

    public DefaultProfileDeleteException()
    {
        super();
    }

    public DefaultProfileDeleteException(String message)
    {
        super(message);
    }

    public DefaultProfileDeleteException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public DefaultProfileDeleteException(Throwable cause)
    {
        super(cause);  
    }
}
