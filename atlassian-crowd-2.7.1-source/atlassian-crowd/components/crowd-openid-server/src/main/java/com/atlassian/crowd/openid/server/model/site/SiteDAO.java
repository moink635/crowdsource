package com.atlassian.crowd.openid.server.model.site;

import com.atlassian.crowd.openid.server.model.EntityObjectDAO;

public interface SiteDAO extends EntityObjectDAO
{
    public Site findByURL(String url);
}
