package com.atlassian.crowd.openid.server.bootstrap;

import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.util.build.BuildUtils;
import com.opensymphony.xwork.util.LocalizedTextUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;


/**
 * Represents basic added dependencies for the initialization of the web-app context for the Crowd OpenID server.
 * <p/>
 * If the context can be initialized then Crowd has enough resources at its disposal to proceed with a
 * bootstrapManager (see CrowdContextLoaderListener)
 */
public class BootstrapLoaderListener implements ServletContextListener
{
    private static final Logger LOG = LoggerFactory.getLogger(BootstrapLoaderListener.class);
    private static final Logger STARTUP_LOG = LoggerFactory.getLogger("com.atlassian.crowd.startup");

    public void contextInitialized(ServletContextEvent event)
    {
        try
        {
            setStartupTime(event.getServletContext());

            // Set the default resource bundle for WW
            LocalizedTextUtil.addDefaultResourceBundle("com.atlassian.crowd.openid.server.action.BaseAction");

            ApplicationContext bootstrapContext = new ClassPathXmlApplicationContext(new String[]{"applicationContext-OpenIDBootstrap.xml"});

            BootstrapUtils.setBootstrapContext(bootstrapContext);

            if (STARTUP_LOG.isInfoEnabled())
            {
                STARTUP_LOG.info("Starting Crowd OpenID Server, Version: " + BuildUtils.getVersion());
                System.out.println();
                System.out.println("*********************************************************************************************");
                System.out.println("*");
                System.out.println("*  You can now use the Crowd OpenID server by visiting " + getCrowdProperties().getProperty("application.login.url"));
                System.out.println("*");
                System.out.println("*********************************************************************************************");
                System.out.println();
            }

        }
        catch (Exception e)
        {
            LOG.error("An error was encountered while bootstrapping Crowd (see below): \n" + e.getMessage(), e);
        }
    }

    private Properties getCrowdProperties() throws IOException
    {
        URL url = getClass().getResource("/" + Constants.PROPERTIES_FILE);

        // get an InputStream to it
        InputStream stream = url.openStream();

        // create the new Properties
        Properties clientProperties = new Properties();

        // load it up
        clientProperties.load(stream);

        // close the stream to it
        stream.close();

        return clientProperties;
    }

    public void contextDestroyed(ServletContextEvent event)
    {
        // do nothing
    }

    /**
     * Stores the system startup time in the context so that system uptime can be computed and displayed on system info
     * page
     *
     * @param context context to add attribute too
     */
    private void setStartupTime(ServletContext context)
    {
        if (context != null)
        {
            context.setAttribute("crowd_startup_time", new Long(System.currentTimeMillis()));
        }
    }
}
