package com.atlassian.crowd.openid.server.model;

import org.springframework.dao.DataAccessException;

import java.util.List;

public interface EntityObjectDAO extends ObjectDao
{
    public void save(EntityObject entityObject) throws DataAccessException;

    public void update(EntityObject entityObject) throws DataAccessException;

    public List findAll() throws DataAccessException;
}
