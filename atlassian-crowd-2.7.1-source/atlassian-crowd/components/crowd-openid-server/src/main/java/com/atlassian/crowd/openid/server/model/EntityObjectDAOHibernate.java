package com.atlassian.crowd.openid.server.model;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.springframework.dao.DataAccessException;


public abstract class EntityObjectDAOHibernate extends HibernateDao implements EntityObjectDAO
{
    public void save(EntityObject entityObject) throws DataAccessException
    {
        Date now = new Date();
        entityObject.setCreatedDate(now);
        entityObject.setUpdatedDate(now);
        super.save(entityObject);
    }

    public void update(EntityObject entityObject) throws DataAccessException
    {
        Date now = new Date();
        entityObject.setCreatedDate(now); // note that hibernate will only insert the createdDate on an INSERT not an UPDATE
        entityObject.setUpdatedDate(now);
        super.update(entityObject);
    }

    public List findAll()
    {
        Criteria criteria = currentSession().createCriteria(getPersistentClass());
        return criteria.list();
    }
}
