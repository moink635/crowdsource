package com.atlassian.crowd.openid.server.manager.login;

import com.atlassian.crowd.exception.*;
import com.atlassian.crowd.service.client.CrowdClient;

public class ForgottenLoginManagerImpl implements ForgottenLoginManager
{
    private CrowdClient crowdClient;

    public void sendResetLink(String username) throws UserNotFoundException, InvalidEmailAddressException
    {
        try
        {
            crowdClient.requestPasswordReset(username);
        }
        catch (OperationFailedException e)
        {
            throw new RuntimeException(e);
        }
        catch (InvalidAuthenticationException e)
        {
            throw new RuntimeException(e);
        }
        catch (ApplicationPermissionException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void sendUsernames(String email) throws InvalidEmailAddressException
    {
        try
        {
            crowdClient.requestUsernames(email);
        }
        catch (OperationFailedException e)
        {
            throw new RuntimeException(e);
        }
        catch (InvalidAuthenticationException e)
        {
            throw new RuntimeException(e);
        }
        catch (ApplicationPermissionException e)
        {
            throw new RuntimeException(e);
        }
    }

    public void setCrowdClient(CrowdClient crowdClient)
    {
        this.crowdClient = crowdClient;
    }
}
