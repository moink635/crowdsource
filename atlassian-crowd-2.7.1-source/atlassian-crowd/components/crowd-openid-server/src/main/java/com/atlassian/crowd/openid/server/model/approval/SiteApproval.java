package com.atlassian.crowd.openid.server.model.approval;

import com.atlassian.crowd.openid.server.model.EntityObject;
import com.atlassian.crowd.openid.server.model.profile.Profile;
import com.atlassian.crowd.openid.server.model.site.Site;
import com.atlassian.crowd.openid.server.model.user.User;

public class SiteApproval extends EntityObject
{
    private User user;
    private Site site;
    private Profile profile;
    private boolean alwaysAllow;

    public SiteApproval() { }

    public SiteApproval(User user, Profile profile, Site site, boolean alwaysAllow)
    {
        this.user = user;
        this.profile = profile;
        this.site = site;
        this.alwaysAllow = alwaysAllow;
    }

    public Site getSite()
    {
        return site;
    }

    public void setSite(Site site)
    {
        this.site = site;
    }

    public Profile getProfile()
    {
        return profile;
    }

    public void setProfile(Profile profile)
    {
        this.profile = profile;
    }

    public boolean isAlwaysAllow()
    {
        return alwaysAllow;
    }

    public void setAlwaysAllow(boolean alwaysAllow)
    {
        this.alwaysAllow = alwaysAllow;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }// site approval is unique based on site
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || !(o instanceof SiteApproval)) return false;

        SiteApproval that = (SiteApproval) o;

        if (site != null ? !site.equals(that.getSite()) : that.getSite() != null) return false;

        return true;
    }

    public int hashCode()
    {
        return site.hashCode();
    }
}
