package com.atlassian.crowd.openid.server.bootstrap;

import com.atlassian.config.bootstrap.BootstrapException;
import com.atlassian.config.bootstrap.DefaultAtlassianBootstrapManager;
import com.atlassian.config.db.DatabaseDetails;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.webwork.spring.WebWorkSpringObjectFactory;
import com.opensymphony.xwork.ObjectFactory;
import com.opensymphony.xwork.config.Configuration;
import com.opensymphony.xwork.config.ConfigurationManager;

/**
 * BootstrapManager is responsible for initializing the dependencies of the Crowd environment.
 */
public class DefaultBootstrapManager extends DefaultAtlassianBootstrapManager
{
    public DefaultBootstrapManager()
    {
    }


    public void init() throws BootstrapException
    {
        // super.init()
        // Don't call super init for now since we onl want to get crowd up and not use any of the other atlassian-config
        // functionality.
    }

    public void bootstrapDatabase(DatabaseDetails databaseDetails, boolean b) throws BootstrapException
    {
        super.bootstrapDatabase(databaseDetails, b);

        // Forces WebWork to use the spring object factory
        WebWorkSpringObjectFactory objectFactory = new WebWorkSpringObjectFactory();
        objectFactory.init(ServletActionContext.getServletContext());
        ObjectFactory.setObjectFactory(objectFactory);

        // Forces xwork to be reloaded after setup
        Configuration config = ConfigurationManager.getConfiguration();
        config.reload();
    }
}
