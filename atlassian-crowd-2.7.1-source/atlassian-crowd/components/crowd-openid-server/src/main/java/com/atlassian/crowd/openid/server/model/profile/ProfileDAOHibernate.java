package com.atlassian.crowd.openid.server.model.profile;

import java.util.List;

import com.atlassian.crowd.openid.server.model.EntityObjectDAOHibernate;
import com.atlassian.crowd.openid.server.model.user.User;

import org.springframework.orm.ObjectRetrievalFailureException;

public class ProfileDAOHibernate extends EntityObjectDAOHibernate implements ProfileDAO
{
    public Profile findProfileByName(String name, User user)
    {
        List profiles = currentSession().createQuery("select profile " +
                                                        "from " + User.class.getName() + " user " +
                                                        "inner join user.profiles as profile " +
                                                        "where user = :user and profile.name = :profileName")
            .setParameter("user", user)
            .setParameter("profileName", name)
            .list();

        if (profiles == null || profiles.isEmpty())
        {
            // we didn't find a user, so throw an exception
            throw new ObjectRetrievalFailureException(Profile.class, name);
        }
        else
        {
            return (Profile) profiles.get(0);
        }
    }

    public Class getPersistentClass()
    {
        return Profile.class;
    }
}
