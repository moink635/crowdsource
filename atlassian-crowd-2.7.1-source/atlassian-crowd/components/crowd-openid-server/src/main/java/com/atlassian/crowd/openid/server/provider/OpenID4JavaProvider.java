package com.atlassian.crowd.openid.server.provider;

import com.atlassian.crowd.integration.http.HttpAuthenticator;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.openid.server.action.BaseAction;
import com.atlassian.crowd.openid.server.manager.openid.OpenIDAuthenticationManager;
import com.atlassian.crowd.openid.server.manager.property.OpenIDPropertyManager;
import com.atlassian.crowd.openid.server.manager.property.OpenIDPropertyManagerException;
import org.openid4java.message.*;
import org.openid4java.message.sreg.SRegRequest;
import org.openid4java.message.sreg.SRegResponse;
import org.openid4java.server.ServerManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class OpenID4JavaProvider implements CrowdProvider
{
    private static final Logger logger = LoggerFactory.getLogger(OpenID4JavaProvider.class);

    // spring injected
    private ServerManager serverManager;
    private String userInteractionURL;
    private OpenIDAuthenticationManager openIDAuthenticationManager;
    private OpenIDPropertyManager openIDPropertyManager;
    private HttpAuthenticator httpAuthenticator;

    public void processOpenIDRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, OpenIDException
    {
        ParameterList requestParameters = new ParameterList(request.getParameterMap());

        // proceed to follow OpenID protocol based on what mode of the request
        String mode = requestParameters.hasParameter("openid.mode") ? requestParameters.getParameterValue("openid.mode") : null;

        logger.info("Servicing OpenID request, mode = " + mode);

        // establish an association
        if ("associate".equals(mode))
        {
            associate(request, response, requestParameters);
        }

        // respond to authentication request
        else if ("checkid_setup".equals(mode))
        {
            checkAuthentication(request, response, requestParameters);
        }

        // checking authentication immediately (no internal redirects)
        else if ("checkid_immediate".equals(mode))
        {
            try
            {
                // make sure check-immediate is allowed by the admin
                if (openIDPropertyManager.isEnableCheckImmediateMode().booleanValue())
                {
                    checkImmediateAuthentication(request, response, requestParameters);
                }
                else
                {
                    // if it's not allowed, fall back to checkid_setup
                    logger.warn("checkid_immediate mode is disabled but has been requested by "+request.getRequestURL().toString());
                    logger.warn("Proceeding to use checkid_setup mode");
                    checkAuthentication(request, response, requestParameters);
                }
            }
            catch (OpenIDPropertyManagerException e)
            {
                logger.error("Could not check if check-immediate mode has been enabled", e);
                throw new OpenIDException(e);
            }
        }

        // verify the authentication (dummy/stateless mode)
        else if ("check_authentication".equals(mode))
        {
            try
            {
                // see if stateless mode is disallowed
                if (openIDPropertyManager.isEnableStatelessMode().booleanValue())
                {
                    // verify the authentication if stateless mode is allowed
                    verifyAuthentication(request, response, requestParameters);
                }
                else
                {
                    // if it's not allowed (force stateful), return a direct error
                    logger.warn("check_authentication (stateless-mode) is disabled but has been requested by "+request.getRequestURL().toString());
                    logger.warn("Returning an immediate error response");
                    Message error = DirectError.createDirectError("Stateless mode is blocked by this OpenID Provider", true);
                    sendRPDirectResponse(request, response, error.keyValueFormEncoding());
                }
            }
            catch (OpenIDPropertyManagerException e)
            {
                logger.error("Could not check if stateless-mode has been allowed", e);
                throw new OpenIDException(e);
            }
        }

        // unknown or unspecified mode
        else
        {
            throw new OpenIDException("OpenID Server trying to process message with an unknown or unspecified openid.mode: "+mode);
        }
    }

    protected void sendRedirect(HttpServletRequest request, HttpServletResponse response, String destination) throws IOException
    {
        response.sendRedirect(destination);
    }

    protected void sendRPRedirect(HttpServletRequest request, HttpServletResponse response, String destination) throws IOException
    {
        removeOpenIDAuthRequest(request.getSession());
        sendRedirect(request, response, destination);
    }

    /**
     * Sends a direct (HTTP 200) response to the requester.
     *
     * @param response HttpServletResponse to write output to.
     * @param responseString response output to write.
     * @throws IOException error writing response.
     */
    protected void sendRPDirectResponse(HttpServletRequest request, HttpServletResponse response, String responseString) throws IOException
    {
        // we don't need this anymore.
        //removeOpenIDAuthRequest(request.getSession());

        response.setContentType("text/plain");
        ServletOutputStream os = response.getOutputStream();
        os.write(responseString.getBytes());
        os.close();
    }

    /**
     * Removes the OpenIDAuthRequest from session.
     * This is used when an error occurs with the processing the request,
     * and the app needs to forget about the old auth request.
     */
    protected void removeOpenIDAuthRequest(HttpSession session)
    {
        session.setAttribute(CrowdProvider.OPENID_AUTHENTICATION_REQUEST, null);
    }


    /**
     * Creates an association with the RP (client).
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param requestParameters representation of the request parameters.
     * @throws java.io.IOException error sending response to RP.
     */
    public void associate(HttpServletRequest request, HttpServletResponse response, ParameterList requestParameters) throws IOException
    {
        Message associationResponse = serverManager.associationResponse(requestParameters);
        sendRPDirectResponse(request, response, associationResponse.keyValueFormEncoding());
    }

    /**
     * Accepts requests for authentication and redirects
     * to authorization page so that users can select
     * which profile to send and whether to accept/deny
     * the request.
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param requestParameters representation of the request parameters
     * @throws java.io.IOException error redirecting to Allow Authentication screen
     * @throws OpenIDException error when trying to translate request parameters into an valid OpenID authentication request
     */
    public void checkAuthentication(HttpServletRequest request, HttpServletResponse response, ParameterList requestParameters) throws IOException, OpenIDException
    {
        // save auth request in session (which includes the url to dispatch to with the response)
        try
        {
            OpenIDAuthRequest authReq = new OpenIDAuthRequest(BaseAction.baseUrl(request), requestParameters, serverManager.getRealmVerifier());
            request.getSession().setAttribute(OPENID_AUTHENTICATION_REQUEST, authReq);
        }
        catch (MalformedOpenIDRequestException e)
        {
            logger.warn("Could not generate authentication request message from request parameters", e);
            throw e;
        }

        // forward to openid verification screen (user action url specified via spring)
        sendRedirect(request, response, userInteractionURL);
    }

    /**
     * Checks the authentication and immediately sends a response
     * to the relying-party without any internal redirects.
     *
     * For the checkImmediate to respond with a successful response:
     * 1. a user must be logged in
     * 2. the authentication request must be valid (as defined in
     *    OpenIDAuthenticationManager.validateRequest()).
     * 3. the user must have a pre-existing trust relationship with
     *    the site (as defined in OpenIDAuthenticationManager.checkImmediate).
     *
     * Otherwise, an unsuccessful response is generated.
     *
     */
    public void checkImmediateAuthentication(HttpServletRequest request, HttpServletResponse response, ParameterList requestParameters) throws IOException, OpenIDException
    {
        // we should immediately check if the authenticated user (if any) trusts this website and generate a successful response
        // or fail-fast and return an unsuccessful response

        // save auth request in session (which includes the url to dispatch to with the response)
        OpenIDAuthRequest authReq;
        try
        {
            authReq = new OpenIDAuthRequest(BaseAction.baseUrl(request), requestParameters, serverManager.getRealmVerifier());
            request.getSession().setAttribute(OPENID_AUTHENTICATION_REQUEST, authReq);
        }
        catch (MalformedOpenIDRequestException e)
        {
            logger.error("Could not generate authentication request message from request parameters", e);
            throw new OpenIDException("Could not generate authentication request message from request parameters", e);
        }

        OpenIDAuthResponse authResp;

        // see if a there is a user logged in
        SOAPPrincipal principal = getRemotePrincipal(request);
        if (principal != null)
        {
            // perform "check-immediate" via the manager
            authResp = openIDAuthenticationManager.checkImmediate(principal, request.getLocale(), authReq);
        }
        else
        {
            // no user logged in
            logger.info("User not authenticated for immediate request");

            authResp = new OpenIDAuthResponse(authReq.getIdentifier(), false);
        }

        // send the authentication response immediately
        sendAuthenticationResponse(request, response, authResp);
    }

    /**
     * Returns the authenticated remote principal
     * using Crowd's SecurityServerClient.
     *
     * @return null no valid remote principal.
     */
    private SOAPPrincipal getRemotePrincipal(HttpServletRequest request)
    {
        try
        {
            // find the principal off their authenticated token key.
            return getHttpAuthenticator().getPrincipal(request);
        }
        catch (Exception e)
        {
            return null;
        }
    }

    /**
     * Responds to an authentication request.
     * The authentication request to respond to must be
     * in session.
     */
    public void sendAuthenticationResponse(HttpServletRequest request, HttpServletResponse response, OpenIDAuthResponse authResp) throws IOException
    {
        // retrieve the auth request from session
        OpenIDAuthRequest authReq = (OpenIDAuthRequest)request.getSession().getAttribute(OPENID_AUTHENTICATION_REQUEST);
        ParameterList requestParameters = authReq.getParameterList();

        // process authentication request
        Message authResponseMessage = serverManager.authResponse(requestParameters, authResp.getIdentifier(), authResp.getClaimedIdentifier(), authResp.isAuthenticated(),
                BaseAction.baseUrl(request) + "op");

//        // process attributes (only for successful authentications which requested attribute exchange)
//        FetchRequest fetchReq = authReq.getFetchRequest();
//        if (fetchReq != null && authResponseMessage instanceof AuthSuccess)
//        {
//            logger.debug("Preparing AX response");
//
//            // create a fetch response for the fetch request using attribute values supplied in the authResp object
//            FetchResponse fetchResp = FetchResponse.createFetchResponse(fetchReq, authResp.getValues());
//
//            if (fetchResp != null)
//            {
//                try
//                {
//                    authResponseMessage.addExtension(fetchResp);
//                }
//                catch (MessageException e)
//                {
//                    // don't bubble up exception as AX is a second class citizen to OpenID, show must go on
//                    logger.error("Could not add FetchResponse AX extension to successful AuthResponse", e);
//                }
//            }
//        }

        // process SREG
        SRegRequest sregReq = authReq.getSregRequest();
        if (sregReq != null && authResponseMessage instanceof AuthSuccess)
        {
            AuthSuccess authSuccessMessage = (AuthSuccess)authResponseMessage;

            logger.debug("Preparing SREG response");
            try
            {
                // build SREG object for opendid4java
                SRegResponse sregResp = SRegResponse.createSRegResponse(sregReq, authResp.getAttributes());

                sregResp.setTypeUri(authReq.getSRegTypeUri());

                // add SREG attributes to the message response
                authSuccessMessage.addExtension(sregResp);

                // add the SREG namespace as an extension which needs to be signed
                authSuccessMessage.addSignExtension(authReq.getSRegTypeUri());

                // try to update the signature on the message
                try
                {
                    serverManager.sign(authSuccessMessage);
                }
                catch (Exception e)
                {
                    logger.error("Unable to sign SREG response", e);
                }
            }
            catch (MessageException e)
            {
                // don't bubble up exception as SREG is a second class citizen to OpenID, show must go on
                logger.error("Could not add SregResponse SREG extension to successful AuthResponse", e);
            }

        }

        // send the response based on what type of authentication message we are sending
        if (authResponseMessage instanceof DirectError)
        {
            // send an error message directly if there is an error with the authentication
            sendRedirect(request, response, authResponseMessage.keyValueFormEncoding());
        }
        else
        {
            // send an indirect successful response
            String destination = authResponseMessage.getDestinationUrl(true);
            sendRPRedirect(request, response, destination);

            // NOTE: optional (only useful if we're sending loads of data)
            // option2: HTML FORM Redirection
            //RequestDispatcher dispatcher =
            //        getServletContext().getRequestDispatcher("formredirection.jsp");
            //request.setAttribute("prameterMap", response.getParameterMap());
            //request.setAttribute("destinationUrl", response.getDestinationUrl(false));
            //dispatcher.forward(request, response);
            //return null;
        }
    }


    /**
     * Verifies authentication (used in dumb-mode).
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param requestParameters representation of the request parameters.
     * @throws java.io.IOException error while sending a direct response to the user.
     */
    public void verifyAuthentication(HttpServletRequest request, HttpServletResponse response, ParameterList requestParameters) throws IOException
    {
        Message verificationMessage = serverManager.verify(requestParameters);
        sendRPDirectResponse(request,  response, verificationMessage.keyValueFormEncoding());
    }


    /**
     * The serverManager is part of the openid4java library.
     *
     * @param serverManager serverManager to inject.
     */
    public void setServerManager(ServerManager serverManager)
    {
        this.serverManager = serverManager;
    }

    /**
     * The user interaction URL is a relative URL which specified
     * the location the user will be directed to in order to allow/deny
     * the authentication to a relying-party and select which
     * attributes to send (if any).
     *
     * @return spring injected userInteractionURL.
     */
    public String getUserInteractionURL()
    {
        return userInteractionURL;
    }

    /**
     * The user interaction URL is a relative URL which specified
     * the location the user will be directed to in order to allow/deny
     * the authentication to a relying-party and select which
     * attributes to send (if any).
     *
     * @param userInteractionURL userInteractionURL to inject.
     */
    public void setUserInteractionURL(String userInteractionURL)
    {
        this.userInteractionURL = userInteractionURL;
    }

    public OpenIDAuthenticationManager getOpenIDAuthenticationManager()
    {
        return openIDAuthenticationManager;
    }

    public void setOpenIDAuthenticationManager(OpenIDAuthenticationManager openIDAuthenticationManager)
    {
        this.openIDAuthenticationManager = openIDAuthenticationManager;
    }

    public OpenIDPropertyManager getOpenIDPropertyManager()
    {
        return openIDPropertyManager;
    }

    public void setOpenIDPropertyManager(OpenIDPropertyManager openIDPropertyManager)
    {
        this.openIDPropertyManager = openIDPropertyManager;
    }
    public HttpAuthenticator getHttpAuthenticator()
    {
        return httpAuthenticator;
    }

    public void setHttpAuthenticator(HttpAuthenticator httpAuthenticator)
    {
        this.httpAuthenticator = httpAuthenticator;
    }
}

// CODE USED FOR CHECKID_IMMEDIATE TO RETURN A user_setup_url WHICH POINTS
// TO A URL WHICH IS THE checkid_setup VERSION OF THE IMMEDIATE MESSAGE

//    protected String generateUserSetupUrl(AuthRequest authReq)
//    {
//        // save values
//        String authReqOPEndpoint = authReq.getOPEndpoint();
//        boolean authReqImmediate = authReq.isImmediate();
//
//        String userSetupUrl = _userSetupUrl;
//
//        try
//        {
//            // generate the URL for this request corresponding to a "checkid_setup"
//            authReq.setOPEndpoint(new URL(_opEndpointUrl));
//            authReq.setImmediate(false);
//
//            userSetupUrl = authReq.getDestinationUrl(true);
//        }
//        catch (MalformedURLException e)
//        {
//            // if url is malformed, the manual _userSetupUrl is used
//        }
//        catch (IllegalStateException e)
//        {
//            // if unable to get destination url off authRequest, the manual _userSetupUrl is used
//        }
//
//        // reset the modified values to the original saved values
//        try
//        {
//            authReq.setOPEndpoint(new URL(authReqOPEndpoint));
//        }
//        catch (MalformedURLException e)
//        {
//            // if this was malformed then we can't do much
//        }
//        authReq.setImmediate(authReqImmediate);
//
//
//        return userSetupUrl;
//    }
