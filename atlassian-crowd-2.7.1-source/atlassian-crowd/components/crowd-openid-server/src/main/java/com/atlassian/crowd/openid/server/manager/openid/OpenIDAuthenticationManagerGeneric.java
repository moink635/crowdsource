package com.atlassian.crowd.openid.server.manager.openid;

import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.openid.server.action.ViewPublicIdentity;
import com.atlassian.crowd.openid.server.manager.property.OpenIDPropertyManager;
import com.atlassian.crowd.openid.server.manager.property.OpenIDPropertyManagerException;
import com.atlassian.crowd.openid.server.manager.site.SiteManager;
import com.atlassian.crowd.openid.server.manager.site.SiteManagerException;
import com.atlassian.crowd.openid.server.manager.user.UserManager;
import com.atlassian.crowd.openid.server.model.approval.SiteApproval;
import com.atlassian.crowd.openid.server.model.record.AuthAction;
import com.atlassian.crowd.openid.server.model.record.AuthRecord;
import com.atlassian.crowd.openid.server.model.record.AuthRecordDAO;
import com.atlassian.crowd.openid.server.model.site.Site;
import com.atlassian.crowd.openid.server.model.user.User;
import com.atlassian.crowd.openid.server.provider.OpenIDAuthRequest;
import com.atlassian.crowd.openid.server.provider.OpenIDAuthResponse;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.openid4java.message.AuthRequest;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;

public class OpenIDAuthenticationManagerGeneric implements OpenIDAuthenticationManager
{
    private static final Logger logger = LoggerFactory.getLogger(OpenIDAuthenticationManagerGeneric.class);

    private SiteManager siteManager;
    private OpenIDPropertyManager openIDPropertyManager;
    private AuthRecordDAO authRecordDAO;
    private UserManager userManager;

    /**
     * Validates the OpenID authentication request.
     *
     * Does nothing if the request is valid. Throws a
     * corresponding exception if request is invalid.
     *
     * @param user logged in user.
     * @param authReq OpenID authentication request.
     * @throws InvalidRequestException if authReq is null or the return_to URL is invalid/malformed.
     * @throws SiteDisallowedException if the site is disallowed by whitelist/blacklist or the site is localhost and local return_to URLs have been disallowed.
     * @throws IdentifierViolationException if the principal does not own the OpenID identifier.
     */
    public void validateRequest(User user, OpenIDAuthRequest authReq) throws InvalidRequestException, SiteDisallowedException, IdentifierViolationException
    {
        if (authReq == null)
        {
            // there is no auth request
            throw new InvalidRequestException("Authentication request not found");
        }

        // check url is correctly formed
        URL authenticatingHost;
        try
        {
            authenticatingHost = new URL(authReq.getRealm());
        }
        catch (MalformedURLException e)
        {
            throw new InvalidRequestException("Malformed return_to URL in OpenID request", e);
        }

        if (!authReq.getIdentifier().equals(AuthRequest.SELECT_ID))
        {
            // check username of OpenID identifier matches the remote principal (user logged in)
            if (!getUserName(authReq).equals(user.getUsername()))
            {
                throw new IdentifierViolationException("User "+user.getUsername()+" cannot verify another user's OpenID: "+authReq.getIdentifier());
            }
        }

        // check if this is a stateless-mode request and if so, that stateless requests are allowed
        try
        {
            if (!openIDPropertyManager.isEnableStatelessMode().booleanValue() && !authReq.hasAssocHandle())
            {
                throw new SiteDisallowedException("Site is making a stateless authentication request. This has been disabled by your administrator");
            }
        }
        catch (OpenIDPropertyManagerException e)
        {
            // cant do much about it, assume it's allowed
            logger.error("Failed to check if stateless mode is permitted; assuming it is", e);
        }

        // check if host is globally allowed to authenticate (blacklist/whitelist)
        if (!siteManager.isSiteAllowedToAuthenticate(authenticatingHost))
        {
            throw new SiteDisallowedException("Site has been globally disallowed by blacklist/whitelist: "+authenticatingHost);
        }

        // check if localhost relying party authentications are allowed
        if (StringUtils.contains(authenticatingHost.getHost(), "localhost") || StringUtils.contains(authenticatingHost.getHost(), "127.0.0.1"))
        {
            try
            {
                if (!openIDPropertyManager.isEnableRelyingPartyLocalhostMode().booleanValue())
                {
                    throw new SiteDisallowedException("Localhost relying-parties have been disallowed: "+authenticatingHost);
                }
            }
            catch (OpenIDPropertyManagerException e)
            {
                // cant do much about it, assume it's allowed
                logger.error("Failed to check if localhost relying parties are permitted; assuming they are", e);
            }
        }
    }

    static String getUserName(OpenIDAuthRequest authReq) throws IdentifierViolationException
    {
        String baseUrl = authReq.getBaseUrl();
        String identifier = authReq.getIdentifier();

        return ViewPublicIdentity.nameFromIdentifier(baseUrl, identifier);
    }

    /**
     * A convenience wrapper for the autoAllowRequest method.
     *
     * This only returns a successful response if the user has
     * trusted the site requesting authentication.
     *
     * If any errors occur, eg. backend errors getting the
     * user object from the principal, or the requesting site
     * is blacklisted/blocked, an immediate unsuccessful
     * response is returned
     *
     * @param principal principal corresponding to logged in user.
     * @param locale locale of the logged in user.
     * @param authReq authentication request of the user.
     * @return successful OpenIDAuthResponse iff the request is valid, site is not banned and user has a pre-existing trust relationship with the site.
     */
    public OpenIDAuthResponse checkImmediate(SOAPPrincipal principal, Locale locale, OpenIDAuthRequest authReq)
    {
        try
        {
            // get the user
            User user = userManager.getUser(principal, locale);

            // validate the authentication request
            validateRequest(user, authReq);

            // only return a successful response if user trusts requesting site
            return autoAllowRequest(user, authReq);
        }
        catch (Exception e)
        {
            // return an unsuccessful response if user manager can't get user from principal
            return new OpenIDAuthResponse(authReq.getIdentifier(), false);
        }
    }

    /**
     * Processes a request if the "allow_always" flag has been set
     * for the requesting site.
     *
     * The OpenIDAuthResponse is successful and contains attributes from
     * the associated profile, if the site is trusted by the user.
     *
     * Otherwise, it is unsuccessful.
     *
     * @param user user processing request.
     * @param authReq OpenIDAuthRequest being processed.
     * @return OpenIDAuthResponse corresponding to success if the site is trusted by the user and has an associated profile set.
     */
    public OpenIDAuthResponse autoAllowRequest(User user, OpenIDAuthRequest authReq)
    {
        // fetch existing site approval (if any)
        SiteApproval existingApproval = siteManager.getSiteApproval(user, authReq.getRealm());

        // Can we upgrade this approval using our list of automatically approved sites?
        if (existingApproval == null || !existingApproval.isAlwaysAllow())
        {
            SiteApproval approvalFromWhitelist = siteManager.createApprovalFromWhitelist(user, authReq.getRealm());

            if (approvalFromWhitelist != null)
            {
                existingApproval = approvalFromWhitelist;
            }
        }

        OpenIDAuthResponse response;

        if (existingApproval != null && existingApproval.isAlwaysAllow())
        {
            // create a record of the auto authentication
            createAuthRecord(user, existingApproval.getSite(), AuthAction.ALLOW_ALWAYS_AUTO);

            String identifier = effectiveIdentifier(authReq, user);

            // create an appropriate authentication response
            response = new OpenIDAuthResponse(identifier, true);

            // append all SREG attributes to response
            response.setAttributes(existingApproval.getProfile().getAttributesAsMap());
        }
        else
        {
            // otherwise return an unsuccessful response
            response = new OpenIDAuthResponse(authReq.getIdentifier(), false);
        }

        return response;
    }

    /**
     * Process a request if the "deny" action is taken by the user
     * when a site has requested authentication.
     *
     * This creates an authentication record of the deny action and
     * returns an unsuccessful OpenIDAuthResponse.
     *
     * @param user user processing request.
     * @param authReq OpenIDAuthRequest being processed.
     * @return unsuccessful OpenIDAuthResponse.
     */
    public OpenIDAuthResponse denyRequest(User user, OpenIDAuthRequest authReq)
    {
        // create a record of the deny authentication
        createAuthRecord(user, siteManager.getSite(authReq.getRealm()), AuthAction.DENY);

        // return an unsuccessful response
        return new OpenIDAuthResponse(authReq.getIdentifier(), false);
    }

    /**
     * Process a request if the "allow" or "allow always" action
     * is taken by the user when a site has requested authentication.
     *
     * This creates an authentication record of the deny action and
     * returns an successful OpenIDAuthResponse containing attributes
     * from the user's profile.
     *
     * If an error occurs (such as, the user is trying to access someone
     * else's profile, or the profile selected does not exist) an
     * unsuccessful OpenIDAuthResponse is generated.
     *
     * @param user user processing request.
     * @param profileID ID of profile containing attributes to be used in response.
     * @param authReq OpenIDAuthRequest being processed.
     * @param alwaysAllow true if the user wants to trust the site.
     * @return successful OpenIDAuthResponse if request processed without errors, otherwise unsuccessful OpenIDAuthResponse.
     */
    public OpenIDAuthResponse allowRequest(User user, long profileID, OpenIDAuthRequest authReq, boolean alwaysAllow)
    {
        OpenIDAuthResponse response;

        try
        {
            // make the site approval (associate with profile, set allow/deny)
            SiteApproval siteApproval = siteManager.setSiteApproval(user, authReq.getRealm(), profileID, alwaysAllow);

            // create a record of the allow/allow always action
            createAuthRecord(user, siteApproval.getSite(), alwaysAllow ? AuthAction.ALLOW_ALWAYS : AuthAction.ALLOW_ONCE);

            // create an appropriate authentication response
            response = new OpenIDAuthResponse(effectiveIdentifier(authReq, user), true);

            // append all SREG attributes to response
            response.setAttributes(siteApproval.getProfile().getAttributesAsMap());

        }
        catch (SiteManagerException e)
        {
            // otherwise return an unsuccessful response
            response = new OpenIDAuthResponse(authReq.getIdentifier(), false);
        }

        return response;
    }

    /**
     * Select an identifier for this request, using the currently logged-in user if identifier
     * selection is requested.
     *
     * @throws IllegalArgumentException if identifier selection is requested but <code>user</code> is <code>null</code>
     */
    public static String effectiveIdentifier(OpenIDAuthRequest authReq, User user)
    {
        if (authReq.getIdentifier().equals(AuthRequest.SELECT_ID))
        {
            if (user!= null)
            {
                return ViewPublicIdentity.openIdIdentifier(authReq.getBaseUrl(), user.getUsername());
            }
            else
            {
                throw new IllegalStateException("Cannot use OP-driven identifier selection without a logged-in user");
            }
        }
        else
        {
            return authReq.getIdentifier();
        }
    }

    /**
     * Creates an AuthRecord for the user/site/action combination.
     * @param user user.
     * @param site site.
     * @param authAction AuthAction type.
     */
    protected void createAuthRecord(User user, Site site, AuthAction authAction)
    {
        AuthRecord record = new AuthRecord(user, site, authAction);
        authRecordDAO.update(record);
    }

    public SiteManager getSiteManager()
    {
        return siteManager;
    }

    public void setSiteManager(SiteManager siteManager)
    {
        this.siteManager = siteManager;
    }

    public OpenIDPropertyManager getOpenIDPropertyManager()
    {
        return openIDPropertyManager;
    }

    public void setOpenIDPropertyManager(OpenIDPropertyManager openIDPropertyManager)
    {
        this.openIDPropertyManager = openIDPropertyManager;
    }

    public AuthRecordDAO getAuthRecordDAO()
    {
        return authRecordDAO;
    }

    public void setAuthRecordDAO(AuthRecordDAO authRecordDAO)
    {
        this.authRecordDAO = authRecordDAO;
    }

    public UserManager getUserManager()
    {
        return userManager;
    }

    public void setUserManager(UserManager userManager)
    {
        this.userManager = userManager;
    }
}
