package com.atlassian.crowd.openid.server.model.record;

import java.util.List;

import com.atlassian.crowd.openid.server.model.EntityObjectDAOHibernate;
import com.atlassian.crowd.openid.server.model.user.User;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

public class AuthRecordDAOHibernate extends EntityObjectDAOHibernate implements AuthRecordDAO
{
    public Class getPersistentClass()
    {
        return AuthRecord.class;
    }

    public List findRecords(final User user)
    {
        Criteria criteria = currentSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("user", user));
        criteria.addOrder(Order.desc("createdDate"));
        return criteria.list();
    }

    public List findRecords(final User user, final int startIndex, final int numRecords)
    {
        Criteria criteria = currentSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("user", user));
        criteria.addOrder(Order.desc("createdDate"));
        criteria.setMaxResults(numRecords);
        criteria.setFirstResult(startIndex);
        return criteria.list();
    }

    public int findTotalRecords(final User user)
    {
        Criteria criteria = currentSession().createCriteria(getPersistentClass());

        criteria.add(Restrictions.eq("user", user));
        criteria.setProjection(Projections.rowCount());
        return ((Number) criteria.uniqueResult()).intValue();
    }
}
