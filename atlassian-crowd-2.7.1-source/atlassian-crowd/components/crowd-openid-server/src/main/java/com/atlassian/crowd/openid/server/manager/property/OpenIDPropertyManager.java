package com.atlassian.crowd.openid.server.manager.property;

public interface OpenIDPropertyManager
{
    public String getAdminGroup();

    void setAdminGroup(String adminGroup);

    String getBaseURL() throws OpenIDPropertyManagerException;

    void setBaseURL(String url) throws OpenIDPropertyManagerException;

    Boolean isEnableRelyingPartyLocalhostMode() throws OpenIDPropertyManagerException;

    void setAllowReplyingPartyLocalhostMode(boolean enabled) throws OpenIDPropertyManagerException;

    void setEnableCheckImmediateMode(boolean enabled) throws OpenIDPropertyManagerException;

    Boolean isEnableCheckImmediateMode() throws OpenIDPropertyManagerException;

    void setEnableStatelessMode(boolean enabled) throws OpenIDPropertyManagerException;

    Boolean isEnableStatelessMode() throws OpenIDPropertyManagerException;

    TrustRelationShipMode getTrustRelationShipMode() throws OpenIDPropertyManagerException;

    void setTrustRelationShipMode(TrustRelationShipMode mode) throws OpenIDPropertyManagerException;
}
