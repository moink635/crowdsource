package com.atlassian.crowd.openid.server.action;

import com.opensymphony.webwork.ServletActionContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Logoff extends BaseAction {

    private static final Logger logger = LoggerFactory.getLogger(Console.class);

    public String execute() throws Exception
    {
        getHttpAuthenticator().logoff(ServletActionContext.getRequest(), ServletActionContext.getResponse());

        getSession().invalidate();

        return SUCCESS;
    }
}