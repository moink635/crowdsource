package com.atlassian.crowd.openid.server.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Console extends BaseAction {
    private static final Logger logger = LoggerFactory.getLogger(Console.class);

    public String execute() throws Exception {
        return SUCCESS;
    }
}