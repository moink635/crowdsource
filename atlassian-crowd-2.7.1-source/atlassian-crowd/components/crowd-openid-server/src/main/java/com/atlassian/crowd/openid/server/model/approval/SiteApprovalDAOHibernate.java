package com.atlassian.crowd.openid.server.model.approval;

import java.util.List;

import com.atlassian.crowd.openid.server.model.EntityObjectDAOHibernate;
import com.atlassian.crowd.openid.server.model.profile.Profile;
import com.atlassian.crowd.openid.server.model.site.Site;
import com.atlassian.crowd.openid.server.model.user.User;

import org.springframework.orm.ObjectRetrievalFailureException;

public class SiteApprovalDAOHibernate extends EntityObjectDAOHibernate implements SiteApprovalDAO
{
    public Class getPersistentClass()
    {
        return SiteApproval.class;
    }

    public SiteApproval findBySite(User user, Site site)
    {
        List siteApprovals = currentSession().createQuery("from " + SiteApproval.class.getName() + " approval where approval.user=:user and approval.site=:site")
            .setParameter("user", user)
            .setParameter("site", site)
            .list();

        if (siteApprovals == null || siteApprovals.isEmpty())
        {
            // site not found for user
            throw new ObjectRetrievalFailureException(SiteApprovalDAOHibernate.class, site);
        }
        else
        {
            return (SiteApproval) siteApprovals.get(0);
        }
    }

    public List findAlwaysAllow(User user)
    {
        return currentSession().createQuery("from " + SiteApproval.class.getName() + " approval where approval.user=:user and approval.alwaysAllow = true")
                .setParameter("user", user)
                .list();
    }

    public List findAllForProfile(Profile profile)
    {
        return currentSession().createQuery("from " + SiteApproval.class.getName() + " approval where approval.profile=:profile")
                .setParameter("profile", profile)
                .list();
    }
}
