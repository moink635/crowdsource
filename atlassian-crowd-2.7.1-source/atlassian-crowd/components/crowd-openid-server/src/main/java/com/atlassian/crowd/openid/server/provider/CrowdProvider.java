package com.atlassian.crowd.openid.server.provider;

import org.openid4java.message.ParameterList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface CrowdProvider
{
    // session keys
    public static final String OPENID_AUTHENTICATION_REQUEST = CrowdProvider.class.getName() + ".openid.auth.request";
    
    public void processOpenIDRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, OpenIDException;
    public void associate(HttpServletRequest request, HttpServletResponse response, ParameterList requestParameters) throws IOException;
    public void checkAuthentication(HttpServletRequest request, HttpServletResponse response, ParameterList requestParameters) throws IOException, OpenIDException;
    public void checkImmediateAuthentication(HttpServletRequest request, HttpServletResponse response, ParameterList requestParameters) throws IOException, OpenIDException;
    public void sendAuthenticationResponse(HttpServletRequest request, HttpServletResponse response, OpenIDAuthResponse authResp) throws IOException;
    public void verifyAuthentication(HttpServletRequest request, HttpServletResponse response, ParameterList requestParameters) throws IOException;
}
