package com.atlassian.crowd.openid.server.model.site;

import java.util.List;

import com.atlassian.crowd.openid.server.model.EntityObjectDAOHibernate;

import org.springframework.orm.ObjectRetrievalFailureException;

public class SiteDAOHibernate extends EntityObjectDAOHibernate implements SiteDAO
{
    public Class getPersistentClass()
    {
        return Site.class;
    }

    public Site findByURL(String url)
    {
        List sites = currentSession().createQuery("from " + Site.class.getName() + " site where site.url=:url")
                .setParameter("url", url)
                .list();

        if (sites == null || sites.isEmpty())
        {
            // site not found for user
            throw new ObjectRetrievalFailureException(Site.class, url);
        }
        else
        {
            return (Site) sites.get(0);
        }
    }
}
