package com.atlassian.crowd.openid.server.interceptor;

import com.atlassian.crowd.openid.server.action.BaseAction;

import com.opensymphony.xwork.ActionInvocation;
import com.opensymphony.xwork.interceptor.Interceptor;


public class AdminActionInterceptor implements Interceptor
{
    public void init()
    {
    }

    public void destroy()
    {
    }

    public String intercept(ActionInvocation invocation) throws Exception
    {
        BaseAction action = (BaseAction) invocation.getAction();

        if (!action.isAdministrator())
        {
            return "accessdenied";
        }

        return invocation.invoke();
    }
}
