package com.atlassian.crowd.openid.server.model.record;

public class AuthAction
{
    // TODO replace this with an enumerated type.
    private long code;

    /**
     * An unknown directory implementation.
     */
    public static AuthAction UNKNOWN = new AuthAction(AuthAction.UNKNOWN_CODE);


    /**
     * An allow once authentication approval.
     */
    public static AuthAction ALLOW_ONCE = new AuthAction(AuthAction.ALLOW_ONCE_CODE);

    /**
     * An allow always authentication approval.
     */
    public static AuthAction ALLOW_ALWAYS = new AuthAction(AuthAction.ALLOW_ALWAYS_CODE);

    /**
     * A deny authentication approval.
     */
    public static AuthAction DENY = new AuthAction(AuthAction.DENY_CODE);

    /**
     * A passthrough authentication request.
     */
    public static AuthAction ALLOW_ALWAYS_AUTO = new AuthAction(AuthAction.ALLOW_ALWAYS_AUTO_CODE);

    /**
     * The persistance code for an unknown relying party authorization response.
     */
    public static final long UNKNOWN_CODE = 0;

    /**
     * The persistance code for an allow relying party authorization response.
     */
    public static final long DENY_CODE = 1;    

    /**
     * The persistance code for an allow relying party authorization response.
     */
    public static final long ALLOW_ONCE_CODE = 2;

    /**
     * The persistance code for an allow relying party authorization response.
     */
    public static final long ALLOW_ALWAYS_CODE = 3;

    /**
     * The persistance code for an a passthrough authentication request.
     */
    public static final long ALLOW_ALWAYS_AUTO_CODE = 4;

    /**
     * Default constructor.
     */
    public AuthAction()
    {
    }

    /**
     * Creates a authorization response with a known code.
     *
     * @param code The directory code.
     */
    public AuthAction(long code)
    {
        this.code = code;
    }

    /**
     * Gets the current code for the directory type.
     *
     * @return The code.
     */
    public long getCode()
    {
        return code;
    }

    /**
     * Sets the current code for the directory type.
     *
     * @param code The code.
     */
    public void setCode(long code)
    {
        this.code = code;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof AuthAction)) return false;

        final AuthAction directoryType = (AuthAction) o;

        return code == directoryType.code;

    }

    public int hashCode()
    {
        return (int) (code ^ (code >>> 32));
    }
}
