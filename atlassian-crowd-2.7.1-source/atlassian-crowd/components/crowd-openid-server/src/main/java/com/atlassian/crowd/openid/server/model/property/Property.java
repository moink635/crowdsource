package com.atlassian.crowd.openid.server.model.property;

import java.io.Serializable;

public class Property implements Serializable
{
    private long ID = -1;
    private long name;
    private String value;

    /**
     * Server Property: The server base URL, ie http://openid.atlassian.com/
     */
    public static final long BASE_URL = 1;

    /**
     * Server Property: Allow relying party localhost authentications.
     */
    public static final long DENY_RELYINGPARTY_LOCALHOST_MODE = 2;

    /**
     * Server Property: If RP authentications support check immediate mode.
     */
    public static final long CHECK_IMMEDIATE_MODE = 3;

    /**
     * Server Property: If RP authentications allow stateless/dummy mode.
     */
    public static final long STATELESS_MODE = 4;

    /**
     * Server Property: Server Security Rescrition type (none, whitelist, blacklist).
     */
    public static final long SERVER_TRUST_RESTRICTION_TYPE = 5;


    /**
     * Gets the unique identifier.
     *
     * @return The ID.
     */
    public long getID()
    {
        return ID;
    }

    /**
     * Sets the unique identifier.
     *
     * @param ID The ID.
     */
    public void setID(long ID)
    {
        this.ID = ID;
    }

    /**
     * Sets the property name.
     *
     * @return The name.
     */
    public long getName()
    {
        return name;
    }

    /**
     * Gets the property name.
     *
     * @param name The name.
     */
    public void setName(long name)
    {
        this.name = name;
    }

    /**
     * Gets the property value.
     *
     * @return The value.
     */
    public String getValue()
    {
        return value;
    }

    /**
     * Sets the property value.
     *
     * @param value The value.
     */
    public void setValue(String value)
    {
        this.value = value;
    }

    public boolean equals(Object o)
    {
        if (this == o)
        {
            return true;
        }
        if (o == null || getClass() != o.getClass())
        {
            return false;
        }

        Property property = (Property) o;

        if (name != property.name)
        {
            return false;
        }
        if (value != null ? !value.equals(property.value) : property.value != null)
        {
            return false;
        }

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (int) (name ^ (name >>> 32));
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }
}
