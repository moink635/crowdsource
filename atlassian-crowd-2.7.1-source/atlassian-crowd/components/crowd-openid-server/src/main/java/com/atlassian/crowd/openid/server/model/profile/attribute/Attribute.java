package com.atlassian.crowd.openid.server.model.profile.attribute;

import com.atlassian.crowd.openid.server.model.EntityObject;

import java.util.Date;

public class Attribute extends EntityObject
{
    private String name;
    private String value;

    public Attribute() { }

    /**
     * Creates an attribute with the createdDate and
     * updatedDate set to now.
     * @param name name of attribute.
     * @param value value of attribute.
     */
    public Attribute(String name, String value)
    {
        this.name = name;
        this.value = value;
        Date now = new Date();
        this.setCreatedDate(now);
        this.setUpdatedDate(now);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getValue()
    {
        return value;
    }

    public void setValue(String value)
    {
        this.value = value;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || !(o instanceof Attribute)) return false;

        Attribute attribute = (Attribute) o;

        // natural primary key for attribute is attribute name
        if (!name.equals(attribute.getName())) return false;

        return true;
    }

    public int hashCode()
    {
        return name.hashCode();
    }
}
