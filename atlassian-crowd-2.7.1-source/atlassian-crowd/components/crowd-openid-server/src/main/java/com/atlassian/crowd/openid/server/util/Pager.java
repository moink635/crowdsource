package com.atlassian.crowd.openid.server.util;

import java.util.List;
import java.util.ArrayList;

public class Pager
{
    private List items;             // page of items
    private int startIndex;         // global index of first item
    private int maxItemsPerPage;    // maximum number of items in one page
    private int totalResults;       // global total number of items
    private int maxPagesOnEitherSide = 5; // largest number of pages on either side of current page to consider for rightMost/leftMost page

    public Pager(List items, int startIndex, int maxItemsPerPage, int totalResults)
    {
        this.items = items;
        this.startIndex = startIndex;
        this.maxItemsPerPage = maxItemsPerPage;
        this.totalResults = totalResults;
    }

    // work out what page we are on
    private int convertIndexToPage(int index)
    {
        return index / maxItemsPerPage + 1;
    }

    public int getCurrentPage()
    {
        return convertIndexToPage(startIndex);
    }

    public int getLastPage()
    {
        return convertIndexToPage(totalResults);
    }

    public int getLeftMostPage()
    {
        int leftMostPage = 1;
        int currentPage = getCurrentPage();
        if (leftMostPage < currentPage - maxPagesOnEitherSide)
        {
            leftMostPage = currentPage - maxPagesOnEitherSide;
        }
        return leftMostPage;
    }

    public int getRightMostPage()
    {
        int rightMostPage = getLastPage();
        int currentPage = getCurrentPage();
        if (rightMostPage > currentPage + maxPagesOnEitherSide)
        {
            rightMostPage = currentPage + maxPagesOnEitherSide;
        }
        return rightMostPage;
    }


    public List getListOfPageNumbers()
    {
        List pageNumbers = new ArrayList();
        int end = getRightMostPage();
        for (int i = getLeftMostPage(); i <= end; i++)
        {
            pageNumbers.add(new Integer(i));
        }
        return pageNumbers;
    }

    public List getItems()
    {
        return items;
    }

    public int getStartIndex()
    {
        return startIndex;
    }

    public int getMaxItemsPerPage()
    {
        return maxItemsPerPage;
    }

    public int getTotalResults()
    {
        return totalResults;
    }

    public int getMaxPagesOnEitherSide()
    {
        return maxPagesOnEitherSide;
    }

    public void setMaxPagesOnEitherSide(int maxPagesOnEitherSide)
    {
        this.maxPagesOnEitherSide = maxPagesOnEitherSide;
    }
}
