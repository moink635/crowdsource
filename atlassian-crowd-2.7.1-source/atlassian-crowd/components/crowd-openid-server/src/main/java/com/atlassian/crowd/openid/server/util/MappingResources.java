package com.atlassian.crowd.openid.server.util;

import org.hibernate.MappingException;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Stores a collection of hbm.xml mapping files.
 * <p/>
 * You can either set a list of mappings (hbm.xml file locations)
 * OR a configLocation (a hibernate.cfg.xml).
 * <p/>
 * This class relies on a wrapped Hibernate Configuration object
 * (TransparentConfiguration) to do the parsing work.
 * <p/>
 * If you set both, the configLocation overrides mappings.
 * <p/>
 * This code has been migrated from bucket.
 */
public class MappingResources implements InitializingBean
{
    // either spring injected or generated from configLocation
    private List mappings = new ArrayList();

    // either spring injected or not used at all (ie. optional)
    private Resource configLocation;

    // initialised properties from the mappings
    private List tableNames;

    public void setMappings(List mappings)
    {
        this.mappings.addAll(mappings);
    }

    public void addMapping(String mapping)
    {
        mappings.add(mapping);
    }

    public List getMappings()
    {
        return mappings;
    }

    public Resource getConfigLocation()
    {
        return configLocation;
    }

    public void setConfigLocation(Resource configLocation)
    {
        this.configLocation = configLocation;
    }

    public String[] getMappingsAsArray()
    {
        String[] map = new String[mappings.size()];
        for (int i = 0; i < mappings.size(); i++)
        {
            map[i] = (String) mappings.get(i);
        }
        return map;
    }

    /**
     * Retrieves the table names using a transparent
     * configuration object built from the hbm.xml mappings.
     *
     * @return List<String> table names.
     */
    public List getTableNames()
    {
        return tableNames;
    }

    /**
     * Invoked by a BeanFactory after it has set all bean properties supplied
     * (and satisfied BeanFactoryAware and ApplicationContextAware).
     * <p>This method allows the bean instance to perform initialization only
     * possible when all bean properties have been set and to throw an
     * exception in the event of misconfiguration.
     *
     * @throws Exception in the event of misconfiguration (such
     *                   as failure to set an essential property) or if initialization fails.
     */
    public void afterPropertiesSet() throws Exception
    {
        // get hibernate to do all the work (parsing hibernate.cfg.xml, mappings.hbm.xml, etc)
        TransparentConfiguration config = new TransparentConfiguration();

        if (configLocation != null)
        {
            config.configure(configLocation.getURL());
            mappings = config.getMappingFiles();
        }
        else if (mappings != null)
        {
            for (Iterator iterator = mappings.iterator(); iterator.hasNext();)
            {
                String hbmFile = (String) iterator.next();
                config.addResource(hbmFile);
            }
        }
        config.buildMappings();

        // sanity check for development
        if (mappings == null || mappings.isEmpty())
        {
            throw new Exception("Misconfigured mappings/configLocation property on MappingResources bean: no hbm.xml mappings found.");
        }

        tableNames = new ArrayList(config.getTables().keySet());
    }


    private static class TransparentConfiguration extends Configuration
    {
        private List mappingFiles = new ArrayList();

        public Map getTables()
        {
            return tables;
        }

        /**
         * Read mappings as a application resourceName (i.e. classpath lookup)
         * trying different classloaders.
         *
         * @param resourceName The resource name
         * @return this (for method chaining purposes)
         * @throws org.hibernate.MappingException Indicates problems locating the resource or
         *                                        processing the contained mapping document.
         */
        public Configuration addResource(String resourceName) throws MappingException
        {
            mappingFiles.add(resourceName);
            return super.addResource(resourceName);
        }

        public List getMappingFiles()
        {
            return mappingFiles;
        }
    }


}
