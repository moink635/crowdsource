package com.atlassian.crowd.openid.server.model.profile;

import com.atlassian.crowd.openid.server.model.profile.attribute.Attribute;
import org.apache.commons.lang3.StringUtils;

import java.text.DecimalFormat;
import java.util.*;

/**
 * Stores a map of name-value pairs of attributes defined
 * by the SREG specification.
 *
 * Also provides convenience methods for accessing the attribute
 * values with JavaBean style getters and setters for the attributes.
 *
 * See http://openid.net/specs/openid-simple-registration-extension-1_0.html
 */

public class SREGAttributes
{
    private Map attributes; // Map<String attributeName, String attributeValue>

    private static final List SREG_ATTRIBS = new ArrayList(); // List<String attributeName>

    // SREG Attributes (http://openid.net/specs/openid-simple-registration-extension-1_0.html)
    public static final String NICKNAME = "nickname";
    public static final String EMAIL = "email";
    public static final String FULLNAME = "fullname";
    public static final String DOB = "dob";
    public static final String GENDER = "gender";
    public static final String POSTCODE = "postcode";
    public static final String COUNTRY = "country";
    public static final String LANGUAGE = "language";
    public static final String TIMEZONE = "timezone";

    static
    {
        SREG_ATTRIBS.add(NICKNAME);
        SREG_ATTRIBS.add(FULLNAME);
        SREG_ATTRIBS.add(EMAIL);
        SREG_ATTRIBS.add(DOB);
        SREG_ATTRIBS.add(GENDER);
        SREG_ATTRIBS.add(POSTCODE);
        SREG_ATTRIBS.add(COUNTRY);
        SREG_ATTRIBS.add(TIMEZONE);
        SREG_ATTRIBS.add(LANGUAGE);
    }

    public SREGAttributes()
    {
        attributes = new HashMap();
    }

    /**
     * Obtain SREG attributes from a profile's attributes.
     *
     * Uses attributes from profile which match an SREG attribute name.
     *
     * @param profile profile to search attributes.
     */
    public SREGAttributes(Profile profile)
    {
        attributes = new HashMap();
        Set profileAttributes = profile.getAttributes();

        if (profileAttributes != null)
        {
            for (Iterator iterator = profileAttributes.iterator(); iterator.hasNext();)
            {
                Attribute attribute = (Attribute) iterator.next();
                if (SREG_ATTRIBS.contains(attribute.getName()))
                {
                    attributes.put(attribute.getName(), attribute.getValue());
                }
            }
        }
    }

    /**
     * Safe "put" method to attributes map. Checks that both the name
     * and value are not blank (ie. not null, not empty, not whitespace).
     *
     * @param name name of attribute.
     * @param value value of attribute.
     */
    public void addAttribute(String name, String value)
    {
        if (StringUtils.isNotBlank(name) && StringUtils.isNotBlank(value))
        {
            attributes.put(name, value);
        }
    }

    /**
     * Converts the SREG attributes to a set of Attributes.
     * @return Set<Attribute>
     */
    public Set toAttributes()
    {
        Set attribs = new HashSet();
        for (Iterator iterator = attributes.keySet().iterator(); iterator.hasNext();)
        {
            String attributeName = (String) iterator.next();
            String attributeValue = (String) attributes.get(attributeName);
            Attribute attribute = new Attribute(attributeName, attributeValue);
            attribs.add(attribute);
        }
        return attribs;
    }

    /**
     * Returns of all SREG attribute names.
     * @return List<String name>
     */
    public static List getAttributeNames()
    {
        return SREG_ATTRIBS;
    }

    public String getAttribute(String name)
    {
        return (String) attributes.get(name);
    }

    public Map getAttributes()
    {
        return attributes;
    }

    public void setAttributes(Map attributes)
    {
        this.attributes = attributes;
    }

    public String getNickname()
    {
        return (String) attributes.get(NICKNAME);
    }

    public void setNickname(String nickname)
    {
        addAttribute(NICKNAME, nickname);
    }

    public String getEmail()
    {
        return (String) attributes.get(EMAIL);
    }

    public void setEmail(String email)
    {
        addAttribute(EMAIL, email);
    }

    public String getFullname()
    {
        return (String) attributes.get(FULLNAME);
    }

    public void setFullname(String fullname)
    {
        addAttribute(FULLNAME, fullname);
    }

    public String getDob()
    {
        return (String) attributes.get(DOB);
    }

    public void setDob(int day, int month, int year)
    {
        if (day + month + year == 0) {
            return; // do not set DOB if all values are zero (treat it as if the attribute is not included)
        }

        // make DOB in YYYY-MM-DD format as required by SREG spec
        StringBuffer sb = new StringBuffer();
        DecimalFormat df = new DecimalFormat("0000");
        sb.append(df.format(year));
        sb.append("-");
        df = new DecimalFormat("00");
        sb.append(df.format(month));
        sb.append("-");
        sb.append(df.format(day));
        setDob(sb.toString());
    }

    public int getDobDay()
    {
        String dob = getDob();
        if (dob != null)
        {
            return Integer.parseInt(dob.substring(8, 10));
        }
        else
        {
            return 0;
        }
    }

    public int getDobMonth()
    {
        String dob = getDob();
        if (dob != null)
        {
            return Integer.parseInt(dob.substring(5, 7));
        }
        else
        {
            return 0;
        }
    }

    public int getDobYear()
    {
        String dob = getDob();
        if (dob != null)
        {
            return Integer.parseInt(dob.substring(0, 4));
        }
        else
        {
            return 0;
        }
    }

    public void setDob(String dob)
    {
        addAttribute(DOB, dob);
    }

    public String getPostcode()
    {
        return (String) attributes.get(POSTCODE);
    }

    public void setPostcode(String postcode)
    {
        addAttribute(POSTCODE, postcode);
    }

    public String getGender()
    {
        return (String) attributes.get(GENDER);
    }

    public void setGender(String gender)
    {
        addAttribute(GENDER, gender);
    }

    public String getCountry()
    {
        return (String) attributes.get(COUNTRY);
    }

    public void setCountry(String country)
    {
        addAttribute(COUNTRY, country);
    }

    public String getLanguage()
    {
        return (String) attributes.get(LANGUAGE);
    }

    public void setLanguage(String language)
    {
        addAttribute(LANGUAGE, language);
    }

    public String getTimezone()
    {
        return (String) attributes.get(TIMEZONE);
    }

    public void setTimezone(String timezone)
    {
        addAttribute(TIMEZONE, timezone);
    }

    public String toString()
    {
        StringBuffer sb = new StringBuffer("SREG ATTRIBUTES\n");
        for (Iterator iterator = attributes.keySet().iterator(); iterator.hasNext();)
        {
            String key = (String) iterator.next();
            String val = (String) attributes.get(key);
            sb.append(" ").append(key).append(" = ").append(val).append("\n");
        }
        return sb.toString();
    }
}
