package com.atlassian.crowd.openid.server.manager.property;

import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.openid.server.model.property.Property;
import com.atlassian.crowd.openid.server.model.property.PropertyDAO;
import org.apache.commons.lang3.StringUtils;

import java.util.Properties;

public class OpenIDPropertyManagerGeneric implements OpenIDPropertyManager
{
    private Properties defaultProperties;
    private PropertyDAO propertyDAO;
    private String adminGroup;

    public void setDefaultProperties(Properties defaultProperties)
    {
        this.defaultProperties = defaultProperties;
    }

    public String getAdminGroup()
    {
        if (StringUtils.isEmpty(adminGroup))
        {
            adminGroup = (String) defaultProperties.get("adminGroup");
        }

        return adminGroup;
    }

    public void setAdminGroup(String adminGroup)
    {
        this.adminGroup = adminGroup;
    }

    protected Property getProperty(long name) throws OpenIDPropertyManagerException, ObjectNotFoundException
    {
        Property property;

        property = propertyDAO.findByName(name);

        return property;
    }

    protected void setProperty(long name, String value) throws OpenIDPropertyManagerException
    {
        try
        {
            Property property = null;

            try
            {
                property = getProperty(name);

            }
            catch (Exception e)
            {
                // ignore, we just want to update if the property already exist
            }

            if (property == null)
            {
                property = new Property();
                property.setName(name);
            }

            property.setValue(value);

            // add the property to the database
            propertyDAO.update(property);
        }
        catch (Exception e)
        {
            throw new OpenIDPropertyManagerException(e.getMessage(), e);
        }
    }

    public String getBaseURL() throws OpenIDPropertyManagerException
    {
        try
        {
            Property property = getProperty(Property.BASE_URL);

            return property.getValue();
        }
        catch (ObjectNotFoundException e)
        {
            // set an empty string
            setBaseURL(StringUtils.EMPTY);

            // return there is no base URL
            return StringUtils.EMPTY;
        }
        catch (Exception e)
        {
            throw new OpenIDPropertyManagerException(e.getMessage(), e);
        }
    }

    public void setBaseURL(String url) throws OpenIDPropertyManagerException
    {
        setProperty(Property.BASE_URL, url);
    }

    public Boolean isEnableRelyingPartyLocalhostMode() throws OpenIDPropertyManagerException
    {
        try
        {
            Property property = getProperty(Property.DENY_RELYINGPARTY_LOCALHOST_MODE);

            return Boolean.valueOf(property.getValue());
        }
        catch (ObjectNotFoundException e)
        {
            String defaultValueStr = (String) defaultProperties.get("enableReplyingPartyLocalhostMode");

            Boolean defaultValueB = Boolean.valueOf(defaultValueStr);

            setAllowReplyingPartyLocalhostMode(defaultValueB.booleanValue());

            return defaultValueB;
        }
    }

    public void setAllowReplyingPartyLocalhostMode(boolean enabled) throws OpenIDPropertyManagerException
    {
        setProperty(Property.DENY_RELYINGPARTY_LOCALHOST_MODE, Boolean.toString(enabled));
    }

    public void setEnableCheckImmediateMode(boolean enabled) throws OpenIDPropertyManagerException
    {
        setProperty(Property.CHECK_IMMEDIATE_MODE, Boolean.toString(enabled));
    }

    public Boolean isEnableCheckImmediateMode() throws OpenIDPropertyManagerException
    {
        try
        {
            Property property = getProperty(Property.CHECK_IMMEDIATE_MODE);

            return Boolean.valueOf(property.getValue());
        }
        catch (ObjectNotFoundException e)
        {
            String defaultValueStr = (String) defaultProperties.get("enableCheckImmediateMode");

            Boolean defaultValueB = Boolean.valueOf(defaultValueStr);

            setEnableCheckImmediateMode(defaultValueB.booleanValue());

            return defaultValueB;
        }
    }

    public void setEnableStatelessMode(boolean enabled) throws OpenIDPropertyManagerException
    {
        setProperty(Property.STATELESS_MODE, Boolean.toString(enabled));
    }

    public Boolean isEnableStatelessMode() throws OpenIDPropertyManagerException
    {
        try
        {
            Property property = getProperty(Property.STATELESS_MODE);

            return Boolean.valueOf(property.getValue());
        }
        catch (ObjectNotFoundException e)
        {
            String defaultValueStr = (String) defaultProperties.get("enableStatelessMode");

            Boolean defaultValueB = Boolean.valueOf(defaultValueStr);

            setEnableStatelessMode(defaultValueB.booleanValue());

            return defaultValueB;
        }
    }

    public TrustRelationShipMode getTrustRelationShipMode() throws OpenIDPropertyManagerException
    {
        try
        {
            Property property = getProperty(Property.SERVER_TRUST_RESTRICTION_TYPE);

            return new TrustRelationShipMode(Long.parseLong(property.getValue()));

        }
        catch (ObjectNotFoundException e)
        {
            String defaultValueStr = (String) defaultProperties.get("trustRelationshipRestrictionType");

            TrustRelationShipMode mode = new TrustRelationShipMode(Long.parseLong(defaultValueStr));

            setTrustRelationShipMode(mode);

            return mode;
        }
    }

    public void setTrustRelationShipMode(TrustRelationShipMode mode) throws OpenIDPropertyManagerException
    {
        // if the site mode is changing, delete the old restrictions
        /*
        if (!getTrustRelationShipMode().equals(mode))
        {
            siteManager.removeAllRPAddressRestrictions();
        }
        */

        setProperty(Property.SERVER_TRUST_RESTRICTION_TYPE, Long.toString(mode.getCode()));
    }

    public void setPropertyDAO(PropertyDAO propertyDAO)
    {
        this.propertyDAO = propertyDAO;
    }
}
