package com.atlassian.crowd.openid.server.action;

import java.net.URI;
import java.net.URISyntaxException;

import org.apache.commons.httpclient.URIException;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.crowd.openid.server.manager.openid.IdentifierViolationException;

public class ViewPublicIdentity extends BaseAction
{
    public static final Logger logger = LoggerFactory.getLogger(ViewPublicIdentity.class);

    // input
    private String name;

    public String execute() throws Exception
    {
        String requestUri = (String) getHttpRequest().getAttribute("javax.servlet.forward.request_uri");

        if (requestUri != null)
        {
            String localBaseUrl = getHttpRequest().getContextPath() + "/";

            try
            {
                this.name = nameFromIdentifier(localBaseUrl, requestUri);
            }
            catch (IdentifierViolationException e)
            {
                // Not serious
                logger.debug("Failed to decode username from URL", e);

                addActionError("Failed to decode username from URL: " + requestUri);
                return ERROR;
            }
        }

        return SUCCESS;
    }

    public String getName()
    {
        if (name == null)
        {
            return "";
        }
        else
        {
            return StringUtils.chomp(name, "/");
        }
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public static String openIdIdentifier(String baseUrl, String username)
    {
        try
        {
            return baseUrl + "users/" + URIUtil.encodeWithinPath(username, "UTF-8");
        }
        catch (URIException e)
        {
            throw new RuntimeException(e);
        }
    }

    public static String nameFromIdentifier(String baseUrl, String identifier)
        throws IdentifierViolationException
    {
        String root = baseUrl + "users/";

        if (identifier.startsWith(root))
        {
            String encodedName = identifier.substring(root.length());

            try
            {
                String path = new URI("./" + encodedName).getPath();
                if (path != null)
                {
                    /* Remove the dummy leading segment */
                    return path.substring(2);
                }
                else
                {
                    /* Unexpected */
                    throw new IdentifierViolationException("Unable to extract username from identifier URL: " + identifier);
                }
            }
            catch (URISyntaxException e)
            {
                throw new IdentifierViolationException("Unable to extract username from identifier URL: " + identifier, e);
            }
        }
        else
        {
            throw new IdentifierViolationException("The identifier must user this server's base URL of '" + root + "' but was '" + identifier + "'");
        }
    }

    @Override
    public String getIdentifier()
    {
        return openIdIdentifier(getBaseURL(), getName());
    }
}
