package com.atlassian.crowd.openid.server.model.user;

import java.util.HashSet;
import java.util.Set;

import com.atlassian.crowd.openid.server.model.EntityObject;
import com.atlassian.crowd.openid.server.model.profile.Profile;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

public class User extends EntityObject
{
    private String username;
    private Profile defaultProfile;
    private Set<Profile> profiles = new HashSet<Profile>();

    public User() { }

    public User(String username)
    {
        this.username = username;
        this.profiles = new HashSet<Profile>();
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public Profile getDefaultProfile()
    {
        return defaultProfile;
    }

    public void setDefaultProfile(Profile defaultProfile)
    {
        this.defaultProfile = defaultProfile;
    }

    public void addProfile(Profile profile)
    {
        this.profiles.add(profile);
    }

    public void removeProfile(Profile profile)
    {
        profiles.remove(profile);
    }

    public boolean hasProfile(Profile profile)
    {
        return profiles.contains(profile);
    }

    public boolean hasProfileNamed(final String profileName)
    {
        return Iterables.any(profiles, new Predicate<Profile>()
        {
            @Override
            public boolean apply(Profile input)
            {
                return profileName.equals(input.getName());
            }
        });
    }

    public Set<Profile> getProfiles()
    {
        return profiles;
    }

    public void setProfiles(Set<Profile> profiles)
    {
        this.profiles = profiles;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || !(o instanceof User)) return false;

        User user = (User) o;

        // username is the natural primary-key
        return username.equals(user.getUsername());
    }

    public int hashCode()
    {
        return username.hashCode();
    }
}
