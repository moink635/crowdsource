package com.atlassian.crowd.openid.server.manager.site;

import com.atlassian.crowd.openid.server.manager.profile.ProfileManager;
import com.atlassian.crowd.openid.server.manager.profile.ProfileManagerException;
import com.atlassian.crowd.openid.server.manager.property.OpenIDPropertyManager;
import com.atlassian.crowd.openid.server.manager.property.OpenIDPropertyManagerException;
import com.atlassian.crowd.openid.server.manager.property.TrustRelationShipMode;
import com.atlassian.crowd.openid.server.model.approval.SiteApproval;
import com.atlassian.crowd.openid.server.model.approval.SiteApprovalDAO;
import com.atlassian.crowd.openid.server.model.profile.Profile;
import com.atlassian.crowd.openid.server.model.record.AuthRecordDAO;
import com.atlassian.crowd.openid.server.model.security.AddressRestriction;
import com.atlassian.crowd.openid.server.model.security.AddressRestrictionDAO;
import com.atlassian.crowd.openid.server.model.site.Site;
import com.atlassian.crowd.openid.server.model.site.SiteDAO;
import com.atlassian.crowd.openid.server.model.user.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ObjectRetrievalFailureException;

import java.net.URL;
import java.util.Iterator;
import java.util.List;

public class SiteManagerGeneric implements SiteManager
{
    private static final Logger logger = LoggerFactory.getLogger(SiteManagerGeneric.class);

    private SiteDAO siteDAO;
    private SiteApprovalDAO siteApprovalDAO;
    private ProfileManager profileManager;
    private AuthRecordDAO authRecordDAO;
    private AddressRestrictionDAO addressRestrictionDAO;
    private OpenIDPropertyManager openIDPropertyManager;
    private ApprovalWhitelist siteApprovalWhitelist;

    /**
     * Gets the SiteApproval of a user for a URL.
     * Returns null if user does not have SiteApproval for the URL.
     *
     * @param user user to search in.
     * @param url URL of Site in SiteApproval.
     * @return SiteApproval or null if none exists.
     */
    public SiteApproval getSiteApproval(User user, String url)
    {
        try
        {
            return siteApprovalDAO.findBySite(user, getSite(url));
        }
        catch (ObjectRetrievalFailureException e)
        {
            return null;
        }
    }

    @Override
    public SiteApproval createApprovalFromWhitelist(User user, String url)
    {
        if (siteApprovalWhitelist.isAutomaticallyApproved(url))
        {
            try
            {
                return setSiteApproval(user, url,  user.getDefaultProfile().getId(), true);
            }
            catch (SiteManagerException e)
            {
                logger.warn("Failed to set site approval for whitelisted site '{}'", url, e);
                return null;
            }
        }
        else
        {
            logger.debug("No white list entry for '{}'", url);
            return null;
        }
    }

    /**
     * Gets the Site object corresponding to the URL or
     * creates a Site object corresponding to the URL.
     *
     * @param url URL of the Site.
     * @return Site.
     */
    public Site getSite(String url)
    {
        try
        {
            // return existing site from backend (if available)
            return siteDAO.findByURL(url);
        }
        catch (ObjectRetrievalFailureException e)
        {
            // add new site into backend
            Site site = new Site(url);
            siteDAO.update(site);
            return site;
        }
    }

    /**
     * Updates or creates site approval for a particular
     * site (URL) associated with a user and a profile of
     * that user.
     *
     * @param user user to associate to.
     * @param url URL of site.
     * @param profileID default profile to use for attribute information when interacting with site.
     * @param alwaysAllow true if authentication with this site is always allowed.
     * @throws SiteManagerException thrown if profile with given profileID does not exist OR user does not own profile.
     * @return SiteApproval which just got added or updated.
     */
    public SiteApproval setSiteApproval(User user, String url, long profileID, boolean alwaysAllow) throws SiteManagerException
    {
        // look up the profile
        Profile profile;
        try
        {
            profile = profileManager.getProfile(user, profileID);
        }
        catch (ProfileManagerException e)
        {
            throw new SiteManagerException(e.getMessage(), e);
        }

        // see if there is an existing site approval for this url
        SiteApproval existingSiteApproval = getSiteApproval(user, url);
        if (existingSiteApproval == null)
        {
            // create a new site approval
            Site site = getSite(url);
            existingSiteApproval = new SiteApproval(user, profile, site, alwaysAllow);
        }
        else
        {
            // update the existing site approval
            existingSiteApproval.setProfile(profile);
            existingSiteApproval.setAlwaysAllow(alwaysAllow);
        }

        siteDAO.update(existingSiteApproval);
        return existingSiteApproval;
    }

    /**
     * Returns all the sites marked 'always allow' for a user.
     *
     * @param user user to search for.
     * @return List<SiteApproval>
     */
    public List getAllAlwaysAllowSites(User user)
    {
        return siteApprovalDAO.findAlwaysAllow(user);
    }

    /**
     * Updates which sites are always allowed to authenticate.
     *
     * @param user user to update.
     * @param urls list of URLS of sites.
     * @param profileIDs corresponding list of default profile IDs for the sites.
     * @throws SiteManagerException thrown if URL/ProfileID lists are null or not the same size OR profile with given profileID does not exist OR user does not own profile.
     */
    public void updateAlwaysAllowApprovals(User user, List urls, List profileIDs) throws SiteManagerException
    {
        if (urls == null || profileIDs == null || urls.size() != profileIDs.size())
        {
            throw new SiteManagerException("URL-ProfileID lists do not correspond");
        }

        List approvals = getAllAlwaysAllowSites(user);

        // unset alwaysAllow for all approvals which are not in the list of URLs
        for (Iterator iterator = approvals.iterator(); iterator.hasNext();)
        {
            SiteApproval approval = (SiteApproval) iterator.next();
            if (!urls.contains(approval.getSite().getUrl()) )
            {
                approval.setAlwaysAllow(false);
                siteApprovalDAO.update(approval);
            }
        }

        // update/add all sites in the urls list
        Iterator profileIDIter = profileIDs.iterator();
        for (Iterator urlIter = urls.iterator(); urlIter.hasNext();)
        {
            String url = (String) urlIter.next();
            long profileID = Long.parseLong((String) profileIDIter.next());

            setSiteApproval(user, url, profileID, true);
        }
    }

    public List findAllRPAddressRestrictions()
    {
        return addressRestrictionDAO.findAll();
    }

    public void addRPAddressRestriction(AddressRestriction restriction)
    {
        // check if the address already exist, if it does not add it.
        List addresses = addressRestrictionDAO.findAll();

        if (!addresses.contains(restriction)) {
            addressRestrictionDAO.save(restriction);
        }
    }

    public void removeAllRPAddressRestrictions()
    {
        addressRestrictionDAO.removeAll();
    }

    public void removeRPAddressRestriction(String address)
    {
        AddressRestriction addressRestriction = addressRestrictionDAO.findbyAddress(address);

        addressRestrictionDAO.remove(addressRestriction);
    }

    /**
     * Determines is a URL is allowed to authenticate.
     *
     * 1. on blacklist: disallow.
     * 2. on whitelist: allow.
     * 3. otherwise: allow.
     *
     * @param url url of the site.
     * @return true if site is allowed to authenticate, false otherwise.
     */
    public boolean isSiteAllowedToAuthenticate(URL url)
    {
        try
        {
            // TODO - write a check to find this quickly once

            // find address on blacklist/whitelist
            List addresses = findAllRPAddressRestrictions();
            AddressRestriction restriction = new AddressRestriction();
            restriction.setAddress(url.getHost());

            // get the current trust mode
            TrustRelationShipMode mode = openIDPropertyManager.getTrustRelationShipMode();

            // see if address is on blacklist/whitelist
            boolean addressOnList = addresses.contains(restriction);

            // if we're not on a blacklist, allow
            if (mode.getCode() == TrustRelationShipMode.BLACKLIST_CODE && !addressOnList)
            {
                return true;
            }

            // if we're on a whitelist, allow
            if (mode.getCode() == TrustRelationShipMode.WHITELIST_CODE && addressOnList)
            {
                return true;
            }

            // if there is no black/white list, allow
            if (mode.getCode() == TrustRelationShipMode.NONE_CODE)
            {
                return true;
            }
        }
        catch (OpenIDPropertyManagerException e)
        {
            logger.debug(e.getMessage(), e);
        }

        // disallow by default (protection)
        return false;
    }

    public void removeSiteApproval(SiteApproval approval)
    {
        siteApprovalDAO.remove(approval);
    }

    public SiteDAO getSiteDAO()
    {
        return siteDAO;
    }

    public void setSiteDAO(SiteDAO siteDAO)
    {
        this.siteDAO = siteDAO;
    }


    public ProfileManager getProfileManager()
    {
        return profileManager;
    }

    public void setProfileManager(ProfileManager profileManager)
    {
        this.profileManager = profileManager;
    }

    public SiteApprovalDAO getSiteApprovalDAO()
    {
        return siteApprovalDAO;
    }

    public void setSiteApprovalDAO(SiteApprovalDAO siteApprovalDAO)
    {
        this.siteApprovalDAO = siteApprovalDAO;
    }

    public AuthRecordDAO getAuthRecordDAO()
    {
        return authRecordDAO;
    }

    public void setAuthRecordDAO(AuthRecordDAO authRecordDAO)
    {
        this.authRecordDAO = authRecordDAO;
    }

    public void setAddressRestrictionDAO(AddressRestrictionDAO addressRestrictionDAO)
    {
        this.addressRestrictionDAO = addressRestrictionDAO;
    }

    public void setOpenIDPropertyManager(OpenIDPropertyManager openIDPropertyManager)
    {
        this.openIDPropertyManager = openIDPropertyManager;
    }

    public void setSiteApprovalWhitelist(ApprovalWhitelist whitelist)
    {
        this.siteApprovalWhitelist = whitelist;
    }
}
