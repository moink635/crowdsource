package com.atlassian.crowd.openid.server.model.profile;

import com.atlassian.crowd.openid.server.model.EntityObject;
import com.atlassian.crowd.openid.server.model.profile.attribute.Attribute;
import com.atlassian.crowd.openid.server.model.user.User;

import java.util.*;

public class Profile extends EntityObject
{
    private User user;
    private String name;
    private Set attributes;

    public Profile() { }

    public Profile(User user, String name)
    {
        this.user = user;
        this.name = name;
        this.attributes = new HashSet();
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public User getUser()
    {
        return user;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * Adds or updates the attribute in the Profile's set of attributes.
     *
     * @param attribute attribute to add or update.
     */
    public void addAttribute(Attribute attribute)
    {
        // see if an attribute with the same name exists
        for (Iterator iterator = attributes.iterator(); iterator.hasNext();)
        {
            Attribute attrib = (Attribute) iterator.next();

            // if we find a matching attribute, update it's value and return
            if (attrib.equals(attribute)) {
                attrib.setValue(attribute.getValue());
                return;
            }
        }

        // if attribute doesn't exist in profile, add it
        attributes.add(attribute);
    }

    public Set getAttributes()
    {
        return attributes;
    }

    public void setAttributes(Set attributes)
    {
        this.attributes = attributes;
    }

    public Map getAttributesAsMap()
    {
        Map attribMap = new HashMap();
        if (attributes != null)
        {
            for (Iterator iterator = attributes.iterator(); iterator.hasNext();)
            {
                Attribute attribute = (Attribute) iterator.next();
                attribMap.put(attribute.getName(), attribute.getValue());
            }
        }
        return attribMap;
    }

    public int hashCode()
    {
        return getName().hashCode() ^ getName().hashCode();
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || !(o instanceof Profile)) return false;

        Profile profile = (Profile) o;

        return getUser().equals(profile.getUser()) && getName().equals(profile.getName());
    }
}
