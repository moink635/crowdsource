package com.atlassian.crowd.openid.server.model.security;

import com.atlassian.crowd.openid.server.model.EntityObject;


public class AddressRestriction extends EntityObject
{
    private String address;

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }

    public int hashCode()
    {
        return address.hashCode();
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || !(o instanceof AddressRestriction)) return false;

        AddressRestriction site = (AddressRestriction) o;

        if (!address.equals(site.getAddress())) return false;

        return true;
    }
}