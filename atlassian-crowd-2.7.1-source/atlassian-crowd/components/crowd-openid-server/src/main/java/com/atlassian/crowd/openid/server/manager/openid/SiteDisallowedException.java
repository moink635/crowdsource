package com.atlassian.crowd.openid.server.manager.openid;

public class SiteDisallowedException extends Exception
{
    public SiteDisallowedException()
    {
        super();
    }

    public SiteDisallowedException(String message)
    {
        super(message);
    }

    public SiteDisallowedException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public SiteDisallowedException(Throwable cause)
    {
        super(cause);
    }
}
