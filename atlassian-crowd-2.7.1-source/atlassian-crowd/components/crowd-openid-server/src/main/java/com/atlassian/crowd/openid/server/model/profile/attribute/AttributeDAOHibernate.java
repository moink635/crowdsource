package com.atlassian.crowd.openid.server.model.profile.attribute;

import java.util.List;

import com.atlassian.crowd.openid.server.model.EntityObjectDAOHibernate;
import com.atlassian.crowd.openid.server.model.profile.Profile;

import org.springframework.orm.ObjectRetrievalFailureException;


public class AttributeDAOHibernate extends EntityObjectDAOHibernate implements AttributeDAO
{
    public Class getPersistentClass()
    {
        return Attribute.class;
    }

    public Attribute findAttributeByName(String name, Profile profile)
    {
        List attribs = currentSession().createQuery("select attribute " +
                                                        "from " + Profile.class.getName() + " profile " +
                                                        "inner join profile.attributes as attribute " +
                                                        "where profile = :profile and attribute.name = :attributeName")
            .setParameter("profile", profile)
            .setParameter("attributeName", name)
            .list();

        if (attribs == null || attribs.isEmpty())
        {
            // we didn't find a user, so throw an exception
            throw new ObjectRetrievalFailureException(Attribute.class, name);
        }
        else
        {
            return (Attribute) attribs.get(0);
        }
    }
}
