package com.atlassian.crowd.openid.server.manager.profile;

import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.openid.server.model.approval.SiteApproval;
import com.atlassian.crowd.openid.server.model.approval.SiteApprovalDAO;
import com.atlassian.crowd.openid.server.model.profile.Profile;
import com.atlassian.crowd.openid.server.model.profile.ProfileDAO;
import com.atlassian.crowd.openid.server.model.profile.SREGAttributes;
import com.atlassian.crowd.openid.server.model.profile.attribute.Attribute;
import com.atlassian.crowd.openid.server.model.profile.attribute.AttributeDAO;
import com.atlassian.crowd.openid.server.model.user.User;
import com.atlassian.crowd.openid.server.model.user.UserDAO;
import com.atlassian.crowd.util.SOAPPrincipalHelper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class ProfileManagerGeneric implements ProfileManager
{
    private static final Logger logger = LoggerFactory.getLogger(ProfileManagerGeneric.class);

    // spring injected
    private ProfileDAO profileDAO;
    private UserDAO userDAO;
    private AttributeDAO attributeDAO;
    private SOAPPrincipalHelper principalHelper;
    private SiteApprovalDAO siteApprovalDAO;

    /**
     * Creates and adds a new profile to a given user. Default profile
     * data is obtained from the SOAPPrincipal/Locale. The following mapping
     * is used:
     *
     * 1. fullname = FirstName + " " + LastName
     * 2. nickname = Username
     * 3. email = Email
     * 4. country = Locale.getCountry()
     * 5. language = Locale.getLanguage()
     *
     * @param user the user object to add the new profile to.
     * @param principal the SOAPPrincipal which contains basic attribute data.
     * @param locale the Locale of the user (to get country/language information).
     * @param profileName the name of the new profile.
     * @return new profile.
     * @throws ProfileAlreadyExistsException if the user already has a profile with the given profile name.
     *
     */
    public Profile addNewProfile(User user, SOAPPrincipal principal, Locale locale, String profileName) throws ProfileAlreadyExistsException
    {
        // check if user already has a profile (ie. same profile name)
        if (user.hasProfileNamed(profileName))
        {
            throw new ProfileAlreadyExistsException("User " + user.getUsername() + " already has a profile " + profileName);
        }

        Profile profile = new Profile(user, profileName);

        // associate the profile to the user
        user.addProfile(profile);

        // add the default attributes from the principal
        Attribute fullname = new Attribute(SREGAttributes.FULLNAME, principalHelper.getFullName(principal));
        profile.addAttribute(fullname);
        attributeDAO.save(fullname);

        Attribute nickname = new Attribute(SREGAttributes.NICKNAME, principal.getName());
        profile.addAttribute(nickname);
        attributeDAO.save(nickname);

        Attribute email = new Attribute(SREGAttributes.EMAIL, principalHelper.getEmail(principal));
        profile.addAttribute(email);
        attributeDAO.save(email);

        // add default attributes from the locale
        Attribute country = new Attribute(SREGAttributes.COUNTRY, locale.getCountry());
        profile.addAttribute(country);
        attributeDAO.save(country);

        Attribute language = new Attribute(SREGAttributes.LANGUAGE, locale.getLanguage());
        profile.addAttribute(language);
        attributeDAO.save(language);

        // save profile and update user
        profileDAO.save(profile);
        userDAO.update(user);

        // return the newly created profile
        return profile;
    }

    /**
     * Adds a new profile based on attribute name/value pairs.
     *
     * @param user user that owns the profile.
     * @param profileName the name of the new profile.
     * @param attributes Map<String attributeName, String attributeValue>
     * @throws ProfileAlreadyExistsException if the user already has a profile with the given profile name.
     * @return newly created profile.
     */
    public Profile addNewPopulatedProfile(User user, String profileName, Map attributes) throws ProfileAlreadyExistsException
    {
        // check if user already has a profile (ie. same profile name)
        if (user.hasProfileNamed(profileName))
        {
            throw new ProfileAlreadyExistsException("User " + user.getUsername() + " already has a profile " + profileName);
        }

        Profile profile = new Profile(user, profileName);

        // associate the profile to the user
        user.addProfile(profile);
        profileDAO.save(profile);
        userDAO.update(user);

        // update the profile with the attributes
        try
        {
            updateProfile(user, profile.getId().longValue(), attributes);
        }
        catch (ProfileManagerException e)
        {
            // this should never happen as we just created the profile for this user
            logger.error("Unable to update a newly created profile", e);
        }

        // return the newly created profile
        return profile;
    }

     /**
     * Updates a user's profile given a map of attributes. The
     * user must own the profile referenced by the profileID. The
     * map of attribtue name-values must be non-null.
     * Any attribute names (keys) which are stored in the
     * profile but not the attribute map, will be deleted.
     * All other attribute names will be updated or added.
     *
     * @param user user that owns the profile.
     * @param profileID profile ID of the profile to update.
     * @param attributes Map<String attributeName, String attributeValue>
     * @throws ProfileDoesNotExistException if profile with requested profileID does not exist.
     * @throws ProfileAccessViolationException if the user does not own the profile with the requested profileID.
     */
    public void updateProfile(User user, long profileID, Map attributes) throws ProfileDoesNotExistException, ProfileAccessViolationException
    {
        Profile profile = getProfile(user, profileID);

        // remove attributes from profile which do not appear in attributes map keys
        Set currentAttribs = profile.getAttributes();
        for (Iterator currentAttribsIterator = currentAttribs.iterator(); currentAttribsIterator.hasNext(); )
        {
            Attribute attribute = (Attribute) currentAttribsIterator.next();
            if (!attributes.keySet().contains(attribute.getName()))
            {
                currentAttribsIterator.remove();
            }
        }

        // update/add remaining attributes
        for (Iterator i = attributes.keySet().iterator(); i.hasNext(); )
        {
            String name = (String) i.next();
            String value = (String) attributes.get(name);

            Attribute attribute = new Attribute(name, value);
            profile.addAttribute(attribute);
            //attributeDAO.update(attribute); // we can't do this because the attribute is yet to be associated with a profile at this stage
        }

        profileDAO.update(profile);
    }

    /**
     * Changes the default profile of a user.
     *
     * @param user user that owns the profile.
     * @param profileID profile ID of the profile to set as default.
     * @throws ProfileDoesNotExistException if profile with requested profileID does not exist.
     * @throws ProfileAccessViolationException if the user does not own the profile with the requested profileID.
     */
    public void makeDefaultProfile(User user, long profileID) throws ProfileDoesNotExistException, ProfileAccessViolationException
    {
        Profile profile = getProfile(user, profileID);
        user.setDefaultProfile(profile);
        userDAO.update(user);
    }

    /**
     * Deletes a the given profile from a user.
     *
     * Note: a user is not allowed to delete their default profile.
     *
     * @param user user that owns the profile.
     * @param profileID profile ID of the profile to update.
     * @throws ProfileDoesNotExistException if profile with requested profileID does not exist.
     * @throws ProfileAccessViolationException if the user does not own the profile with the requested profileID.
     * @throws DefaultProfileDeleteException if the profile requested for deletion is the default profile of the user.
     */
    public void deleteProfile(User user, long profileID) throws ProfileDoesNotExistException, ProfileAccessViolationException, DefaultProfileDeleteException
    {
        Profile profile = getProfile(user, profileID);

        if (user.getDefaultProfile().equals(profile))
        {
            throw new DefaultProfileDeleteException("Cannot delete default profile "+profile.getName());
        }

        // remove the associations this profile has on the approved sites
        List associatedSitesApprovals = siteApprovalDAO.findAllForProfile(profile);

        for (Iterator siteApprovals = associatedSitesApprovals.iterator(); siteApprovals.hasNext();)
        {
            SiteApproval siteApproval = (SiteApproval) siteApprovals.next();
            siteApprovalDAO.remove(siteApproval);
        }

        user.removeProfile(profile);
        profileDAO.remove(profile);
        userDAO.update(user);
    }

    /**
     * Rename an existing profile of a user. The new name
     * must not clash with the other existing profile names
     * the user has.
     *
     * @param user user user that owns the profile.
     * @param profileID profileID profile ID of the profile to rename.
     * @param newName new name of the profile.
     * @throws ProfileDoesNotExistException if profile with requested profileID does not exist.
     * @throws ProfileAccessViolationException if the user does not own the profile with the requested profileID.
     * @throws ProfileAlreadyExistsException if user already has a profile with the new name.
     */
    public void renameProfile(User user, long profileID, String newName) throws ProfileDoesNotExistException, ProfileAccessViolationException, ProfileAlreadyExistsException
    {
        Profile profile = getProfile(user, profileID);

        // only rename if the new profile name is different to the current profile name
        if (!profile.getName().equals(newName)) {

            // make sure user does not have a profile with the same name
            if (user.hasProfileNamed(newName))
            {
                throw new ProfileAlreadyExistsException("User already has a profile named "+newName);
            }
            else
            {
                profile.setName(newName);
                profileDAO.update(profile);
                userDAO.update(user); // update user object to reference new profile
            }
        }
    }

    /**
     * Retrieves the requested profile from a user.
     *
     * @param user user that owns the profile.
     * @param profileID profile ID of the profile to retrieve.
     * @return profile corresponding to profileID owned by user.
     * @throws ProfileAccessViolationException profile with given profileID exists but is not owned by the user.
     * @throws ProfileDoesNotExistException profile with given profileID does not exist.
     */
    public Profile getProfile(User user, long profileID) throws ProfileDoesNotExistException, ProfileAccessViolationException
    {
        Profile profile;
        try
        {
            profile = (Profile) profileDAO.load(profileID);
        }
        catch (ObjectNotFoundException e)
        {
            profile = null;
        }

        // check profile is returned from back end
        if (profile == null)
        {
            throw new ProfileDoesNotExistException("Profile with ID "+profileID+" does not exist");
        }

        // make sure user owns the profile
        if (user.hasProfile(profile))
        {
            return profile;
        }
        else
        {
            throw new ProfileAccessViolationException("User "+user.getUsername()+" does not own profile with ID "+profileID);
        }
    }

    public ProfileDAO getProfileDAO()
    {
        return profileDAO;
    }

    public void setProfileDAO(ProfileDAO profileDAO)
    {
        this.profileDAO = profileDAO;
    }

    public UserDAO getUserDAO()
    {
        return userDAO;
    }

    public void setUserDAO(UserDAO userDAO)
    {
        this.userDAO = userDAO;
    }

    public SOAPPrincipalHelper getPrincipalHelper()
    {
        return principalHelper;
    }

    public void setPrincipalHelper(SOAPPrincipalHelper principalHelper)
    {
        this.principalHelper = principalHelper;
    }

    public AttributeDAO getAttributeDAO()
    {
        return attributeDAO;
    }

    public void setAttributeDAO(AttributeDAO attributeDAO)
    {
        this.attributeDAO = attributeDAO;
    }

    public void setSiteApprovalDAO(SiteApprovalDAO siteApprovalDAO)
    {
        this.siteApprovalDAO = siteApprovalDAO;
    }
}
