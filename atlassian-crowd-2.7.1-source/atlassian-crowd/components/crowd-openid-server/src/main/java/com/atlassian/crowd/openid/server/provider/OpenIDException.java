package com.atlassian.crowd.openid.server.provider;

public class OpenIDException extends Exception
{
    public OpenIDException()
    {
        super();
    }

    public OpenIDException(String message)
    {
        super(message);
    }

    public OpenIDException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public OpenIDException(Throwable cause)
    {
        super(cause);
    }
}
