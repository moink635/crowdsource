package com.atlassian.crowd.openid.server.util;

import com.atlassian.config.ConfigurationException;
import com.atlassian.config.db.HibernateConfig;
import org.hibernate.MappingException;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.SchemaUpdate;

import java.util.Properties;

/**
 * Migrated from bucket.
 */
public class SchemaHelper
{
    public static final String COMPONENT_REFERENCE = "schemaHelper";

    private Configuration configuration;
    private MappingResources mappingResources;
    private HibernateConfig hibernateConfig;
    // If these are not set, we fall back to looking in the ApplicationConfig for anything
    // starting with hibernate.*
    private Properties hibernateConfigProperties;

    public void setHibernateProperties(Properties props)
    {
        this.hibernateConfigProperties = props;
    }

    public void setHibernateConfig(HibernateConfig hibernateConfig)
    {
        this.hibernateConfig = hibernateConfig;
    }

    /**
     * @return Returns the config.
     */
    public Configuration getConfiguration() throws MappingException
    {
        if (configuration == null)
        {
            configuration = makeBaseHibernateConfiguration();

            if (this.mappingResources != null)
            {
                // register given Hibernate mapping definitions
                for (int i = 0, n = mappingResources.getMappings().size(); i < n; i++)
                {
                    configuration.addResource((String) mappingResources.getMappings().get(i),
                            Thread.currentThread().getContextClassLoader());
                }
            }
        }
        return configuration;
    }

    /**
     * @param configuration The config to set.
     */
    public void setConfiguration(Configuration configuration)
    {
        this.configuration = configuration;
    }

    /**
     * @return Returns the mappings.
     */
    public MappingResources getMappingResources()
    {
        return mappingResources;
    }

    /**
     * @param mappings The mappings to set.
     */
    public void setMappingResources(MappingResources mappings)
    {
        this.mappingResources = mappings;
    }

    public void dropTables() throws ConfigurationException
    {
        try
        {
            new SchemaExport(getConfiguration()).drop(true, true);
        }
        catch (Exception e)
        {
            throw new ConfigurationException("Cannot update schema", e);
        }
    }

    public void createTables() throws ConfigurationException
    {
        try
        {
            new SchemaExport(getConfiguration()).create(true, true);
        }
        catch (Exception e)
        {
            throw new ConfigurationException("Cannot update schema", e);
        }
    }

    /**
     * If there are any updates to the Schema needed, do them.
     */
    public void updateSchemaIfNeeded() throws ConfigurationException
    {
        updateSchemaIfNeeded(true);
    }


    /**
     * @param showDDL - false will output DDL to stdout
     * @throws ConfigurationException
     */
    public void updateSchemaIfNeeded(boolean showDDL) throws ConfigurationException
    {
        try
        {
            new SchemaUpdate(getConfiguration()).execute(showDDL, true);
        }
        catch (Exception e)
        {
            throw new ConfigurationException("Cannot update schema", e);
        }
    }

    private Configuration makeBaseHibernateConfiguration()
    {
        if (hibernateConfigProperties != null)
        {
            return new Configuration().setProperties(hibernateConfigProperties);
        }

        return new Configuration().setProperties(hibernateConfig.getHibernateProperties());
    }
}

