package com.atlassian.crowd.openid.server.model.user;

import com.atlassian.crowd.openid.server.model.EntityObjectDAO;
import org.springframework.orm.ObjectRetrievalFailureException;

public interface UserDAO extends EntityObjectDAO
{
    User findByUsername(String name) throws ObjectRetrievalFailureException;
}
