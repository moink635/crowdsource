package com.atlassian.crowd.openid.server.manager.login;

import com.atlassian.crowd.exception.InvalidEmailAddressException;
import com.atlassian.crowd.exception.UserNotFoundException;

/**
 * Manages functionality related to retrieving forgotten usernames or resetting forgotten passwords.
 *
 * @since v2.1.0
 */
public interface ForgottenLoginManager
{
    /**
     * Sends a reset link to the first user with the matching <tt>username</tt> from all the active directories assigned
     * to the application.
     *
     * @param username username of the user to send the password reset link
     * @throws UserNotFoundException if no user with the supplied username exists
     * @throws InvalidEmailAddressException if the user does not have a valid email address to send the password reset email to
     */
    void sendResetLink(String username)
            throws UserNotFoundException, InvalidEmailAddressException;

    /**
     * Sends the usernames associated with the given email address. No email will be sent if there are no usernames
     * associated with a given <code>email</code>.
     *
     * @param email email address of the user
     * @throws com.atlassian.crowd.exception.InvalidEmailAddressException if the <code>email</code> is not valid
     */
    void sendUsernames(String email) throws InvalidEmailAddressException;
}
