package com.atlassian.crowd.openid.server.manager.profile;


public class ProfileManagerException extends Exception
{
    public ProfileManagerException()
    {
        super();    
    }

    public ProfileManagerException(String message)
    {
        super(message);
    }

    public ProfileManagerException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ProfileManagerException(Throwable cause)
    {
        super(cause);
    }
}
