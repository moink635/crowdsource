package com.atlassian.crowd.openid.server.model.profile;

import com.atlassian.crowd.openid.server.model.EntityObjectDAO;
import com.atlassian.crowd.openid.server.model.user.User;

public interface ProfileDAO extends EntityObjectDAO
{
    /**
     * Finds a profile for a user given a specific name.
     * @param name name of profile.
     * @param user owner of the profile.
     * @return profile for the user with the given name.
     */
    public Profile findProfileByName(String name, User user);
}
