package com.atlassian.crowd.openid.server.provider;

import org.openid4java.message.AuthRequest;
import org.openid4java.message.MessageException;
import org.openid4java.message.MessageExtension;
import org.openid4java.message.ParameterList;
import org.openid4java.message.ax.AxMessage;
import org.openid4java.message.ax.FetchRequest;
import org.openid4java.message.sreg.SRegMessage;
import org.openid4java.message.sreg.SRegRequest;
import org.openid4java.server.RealmVerifier;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;


public class OpenIDAuthRequest implements Serializable {

    private final String baseUrl;
    private final ParameterList requestParameters;
    private FetchRequest fetchRequest;
    private SRegRequest sregRequest;
    private final AuthRequest authReq;
    private final String sregType;

    public OpenIDAuthRequest(String baseUrl, ParameterList requestParameters, RealmVerifier realmVerifier) throws MalformedOpenIDRequestException
    {
        this.baseUrl = baseUrl;

        // store the request parameters for reference (required when responding to OpenIDAuthRequest)
        this.requestParameters = requestParameters;

        // store the underlying openid4java authRequest
        try
        {
            this.authReq = AuthRequest.createAuthRequest(requestParameters, realmVerifier);
        }
        catch (MessageException e)
        {
            throw new MalformedOpenIDRequestException("AuthRequest could not be reconstructed from RequestParameters", e);
        }


        // pull out the attribute exchange fetch request (if any)
        try
        {
            if (authReq.hasExtension(AxMessage.OPENID_NS_AX))
            {
                MessageExtension ext = authReq.getExtension(AxMessage.OPENID_NS_AX);

                if (ext instanceof FetchRequest)
                {
                    this.fetchRequest = (FetchRequest) ext;
                }
            }
        }
        catch (MessageException e)
        {
            // no fetch request (fetchRequest = null by default)
        }


        // pull out the sreg request (if any)
        String requestSregType = SRegMessage.OPENID_NS_SREG;

        try
        {
            if (authReq.hasExtension(SRegMessage.OPENID_NS_SREG11))
            {
                MessageExtension ext = authReq.getExtension(SRegMessage.OPENID_NS_SREG11);

                if (ext instanceof SRegRequest)
                {
                    this.sregRequest = (SRegRequest) ext;
                    requestSregType = SRegMessage.OPENID_NS_SREG11;
                }
            }
            else if (authReq.hasExtension(SRegMessage.OPENID_NS_SREG))
            {
                MessageExtension ext = authReq.getExtension(SRegMessage.OPENID_NS_SREG);

                if (ext instanceof SRegRequest)
                {
                    this.sregRequest = (SRegRequest) ext;
                    requestSregType = SRegMessage.OPENID_NS_SREG;
                }
            }
        }
        catch (MessageException e)
        {
            // no fetch request (sregRequest = null by default)
        }

        this.sregType = requestSregType;
    }

    public ParameterList getParameterList()
    {
        return requestParameters;
    }

    public FetchRequest getFetchRequest()
    {
        return fetchRequest;
    }


    public SRegRequest getSregRequest()
    {
        return sregRequest;
    }

    public String getBaseUrl()
    {
        return baseUrl;
    }

    public String getIdentifier()
    {
        return authReq.getIdentity();
    }

    public String getRealm()
    {
        return authReq.getRealm();
    }

    // retrieves SREG required attribs
    public Collection getRequiredAttributes()
    {
        if (sregRequest != null)
        {
            return sregRequest.getAttributes(true);
        }
        else
        {
            return Collections.EMPTY_LIST;
        }
    }

    // retrieves SREG optional attribs
    public Collection getOptionalAttributes()
    {
        if (sregRequest != null)
        {
            return sregRequest.getAttributes(false);
        }
        else
        {
            return Collections.EMPTY_LIST;
        }
    }

    /**
     * Returns true if the OpenID request has
     * an association handle set.
     *
     * @return true if openid.assoc_handle parameter is set.
     */
    public boolean hasAssocHandle()
    {
        return requestParameters.hasParameter("openid.assoc_handle");
    }

    String getSRegTypeUri()
    {
        return sregType;
    }


// ATTRIBUTE EXCHANGE STUFF REMOVED
//    public Collection getRequiredAttributes()
//    {
//        if (fetchRequest != null)
//        {
//            return fetchRequest.getValues(true).keySet();
//        }
//        else
//        {
//            return Collections.EMPTY_SET;
//        }
//    }
//
//    public Collection getOptionalAttributes()
//    {
//        if (fetchRequest != null)
//        {
//            return fetchRequest.getValues(false).keySet();
//        }
//        else
//        {
//            return Collections.EMPTY_SET;
//        }
//    }


}
