package com.atlassian.crowd.openid.server.action.secure.interaction;

import java.util.Collection;

import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.openid.server.action.BaseAction;
import com.atlassian.crowd.openid.server.manager.openid.IdentifierViolationException;
import com.atlassian.crowd.openid.server.manager.openid.InvalidRequestException;
import com.atlassian.crowd.openid.server.manager.openid.OpenIDAuthenticationManagerGeneric;
import com.atlassian.crowd.openid.server.manager.openid.SiteDisallowedException;
import com.atlassian.crowd.openid.server.manager.user.UserManagerException;
import com.atlassian.crowd.openid.server.model.profile.Profile;
import com.atlassian.crowd.openid.server.model.profile.SREGAttributes;
import com.atlassian.crowd.openid.server.model.user.User;
import com.atlassian.crowd.openid.server.provider.CrowdProvider;
import com.atlassian.crowd.openid.server.provider.OpenIDAuthRequest;
import com.atlassian.crowd.openid.server.provider.OpenIDAuthResponse;
import com.atlassian.crowd.openid.server.servlet.OpenIDServerServlet;
import com.atlassian.crowd.openid.server.util.ProfileAttributesHelper;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.google.common.collect.ImmutableList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AllowAuthentication extends BaseAction
{
    private static final Logger logger = LoggerFactory.getLogger(AllowAuthentication.class);

    // spring injected
    private ProfileAttributesHelper profileAttributesHelper;

    // input
    private long profileID;

    // output
    private User user;
    private Profile currentProfile;
    private SREGAttributes sregAttributes;
    private String requestingSite;
    private String identifier;
    private Collection requiredAttributes;
    private Collection optionalAttributes;

    /**
     * Retrieve the OpenIDAuthRequest from session.
     * This should be put in session by the CrowdProvider.
     * @return OpenIDAuthRequest from session, or null if it hasn't been set.
     */
    protected OpenIDAuthRequest getOpenIDAuthRequest()
    {
        return (OpenIDAuthRequest)getSession().getAttribute(CrowdProvider.OPENID_AUTHENTICATION_REQUEST);
    }

    /**
     * Removes the OpenIDAuthRequest from session.
     * This is used when an error occurs with the processing the request,
     * and the app needs to forget about the old auth request.
     */
    protected void removeOpenIDAuthRequest()
    {
        getSession().setAttribute(CrowdProvider.OPENID_AUTHENTICATION_REQUEST, null);
    }

    /**
     * Set the OpenIDAuthResponse into the request attributes.
     * This should be called before a "SUCCESS" is returned from this
     * action, as this object is expected by the OpenIDServerServlet.
     *
     * @param authResp OpenIDAuthResponse, should not be null.
     */
    protected void setOpenIDAuthResponse(OpenIDAuthResponse authResp)
    {
        getRequest().setAttribute(OpenIDServerServlet.OPENID_AUTHENTICATION_APPLICATION_RESPONSE, authResp);
    }

    /**
     * Check if the OpenID auth request is valid.
     *
     * The criteria for validation is delegated to the
     * OpenIDAuthenticationManager.
     *
     * This method appropriately adds actionError messages
     * corresponding to the different types of invalid
     * authentication requests.
     *
     * @return true if the OpenIDRequest is valid.
     */
    private boolean isOpenIDRequestValid()
    {
        try
        {
            User user = userManager.getUser(getRemotePrincipal(), getLocale());
            openIDAuthenticationManager.validateRequest(user, getOpenIDAuthRequest());
        }
        catch (UserManagerException e)
        {
            logger.info(e.getMessage(), e);
            addActionError(getText("exception.invalid.principal"));
        }
        catch (InvalidRequestException e)
        {
            logger.info(e.getMessage(), e);
            addActionError(getText("exception.invalid.openid.request"));
        }
        catch (SiteDisallowedException e)
        {
            logger.info(e.getMessage(), e);
            addActionError(getText("exception.site.disallowed", ImmutableList.of(getOpenIDAuthRequest().getRealm())));
        }
        catch (IdentifierViolationException e)
        {
            logger.warn(e.getMessage(), e);
            addActionError(getText("exception.identifier.violation"));
        }
        catch (InvalidAuthenticationException e)
        {
            logger.error(e.getMessage(), e);
            addActionError(getText("exception.invalid.principal"));
        }

        // return true if no errors
        return !hasActionErrors();
    }

    /**
     * Display the Allow/Disallow authentication screen.
     *
     * The profile selected will be a 'remembered' profile from previous
     * 'allow' authentication actions, or, the user's default profile
     * (if the user hasn't previously allowed authentication to the site).
     *
     * If the user has previously 'always allowed' verification to the
     * requesting site, this screen is bypassed and the default profile
     * associated with the site will be used for attribute values.
     *
     * @return ERROR if there is no authentication request message in session, otherwise INPUT.
     * @throws Exception
     */
    public String doDefault() throws Exception
    {
        OpenIDAuthRequest authReq = getOpenIDAuthRequest();

        // check if OpenID authentication request exists and is for the current user's OpenID
        if (!isOpenIDRequestValid())
        {
            removeOpenIDAuthRequest();
            return ERROR;
        }

        // retrieve data from the authentication request
        identifier = effectiveIdentifier(authReq);
        requestingSite = authReq.getRealm();
        requiredAttributes = authReq.getRequiredAttributes();
        optionalAttributes = authReq.getOptionalAttributes();

        // retrieve the user for the request
        user = userManager.getUser(getRemotePrincipal(), getLocale());

        // see if the site requesting authentication is trusted by the user (ie. see if we can successfully auto allow)
        OpenIDAuthResponse autoResponse = openIDAuthenticationManager.autoAllowRequest(user, getOpenIDAuthRequest());
        if (autoResponse.isAuthenticated())
        {
            // store the reponse in the request object
            setOpenIDAuthResponse(autoResponse);
            return SUCCESS;
        }

        // otherwise, display the allow/always allow/deny screen
        else
        {
            // check if we aren't flicking through the drop-down of profiles (profileID == 0 if it isn't manually set by the drop-down)
            if (profileID == 0)
            {

//                // see if we've got a default profile for this site
//                SiteApproval approval = siteManager.getSiteApproval(user, requestingSite);
//
//                if (approval != null)
//                {
//                    profileID = approval.getProfile().getId().longValue();
//                }

                // select the user's default profile
                profileID = user.getDefaultProfile().getId().longValue();
            }

            currentProfile = profileManager.getProfile(user, profileID);
            sregAttributes = new SREGAttributes(currentProfile);

            return INPUT;
        }
    }

    /**
     * Allow OpenID authentication to a client site (RP).
     *
     * The profile selected (when the user clicks AllowAuthentication) will
     * be 'remembered' for next time.
     *
     * @return SUCCESS
     * @throws Exception
     */
    @RequireSecurityToken(true)
    public String doAllow() throws Exception
    {
        // allow
        return processAllowAction(false);
    }

    /**
     * Always allow OpenID authentication to a client site (RP).
     *
     * The profile selected (when the user clicks AllowAuthentication) will
     * be 'remembered' for next time. All following authentications with this
     * site will occur automatically.
     *
     * @return SUCCESS
     * @throws Exception
     */
    @RequireSecurityToken(true)
    public String doAllowAlways() throws Exception
    {
        // allow always
        return processAllowAction(true);
    }

    private String processAllowAction(boolean alwaysAllow) throws Exception
    {
        // check if OpenID authentication request exists and is for the current user's OpenID
        if (!isOpenIDRequestValid())
        {
            removeOpenIDAuthRequest();
            return ERROR;
        }

        // make sure a profileID has been passed in
        if (profileID <= 0)
        {
            addActionError("error.profile.not.found");
        }

        // get the current user
        user = userManager.getUser(getRemotePrincipal(), getLocale());

        // make an auth reponse for allow/always allow for the user with their selected profile
        OpenIDAuthResponse response = openIDAuthenticationManager.allowRequest(user, profileID, getOpenIDAuthRequest(), alwaysAllow);

        // store the reponse in the request object
        setOpenIDAuthResponse(response);

        return SUCCESS;
    }

    /**
     * Deny OpenID authentication to a client site (RP).
     *
     * @return SUCCESS
     * @throws Exception
     */
    @RequireSecurityToken(true)
    public String doDeny() throws Exception
    {
        // check if OpenID authentication request exists and is for the current user's OpenID
        if (!isOpenIDRequestValid())
        {
            removeOpenIDAuthRequest();
            return ERROR;
        }

        // get the current user
        user = userManager.getUser(getRemotePrincipal(), getLocale());

        // make an unsuccessful auth response
        OpenIDAuthResponse response = openIDAuthenticationManager.denyRequest(user, getOpenIDAuthRequest());

        // add auth response to request
        setOpenIDAuthResponse(response);

        return SUCCESS;
    }

    public String getRequestingSite()
    {
        return requestingSite;
    }

    public void setRequestingSite(String requestingSite)
    {
        this.requestingSite = requestingSite;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    public Collection getRequiredAttributes()
    {
        return requiredAttributes;
    }

    public void setRequiredAttributes(Collection requiredAttributes)
    {
        this.requiredAttributes = requiredAttributes;
    }

    public Collection getOptionalAttributes()
    {
        return optionalAttributes;
    }

    public void setOptionalAttributes(Collection optionalAttributes)
    {
        this.optionalAttributes = optionalAttributes;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }


    public long getProfileID()
    {
        return profileID;
    }

    public void setProfileID(long profileID)
    {
        this.profileID = profileID;
    }

    public Profile getCurrentProfile()
    {
        return currentProfile;
    }

    public void setCurrentProfile(Profile currentProfile)
    {
        this.currentProfile = currentProfile;
    }

    public SREGAttributes getSregAttributes()
    {
        return sregAttributes;
    }

    public void setSregAttributes(SREGAttributes sregAttributes)
    {
        this.sregAttributes = sregAttributes;
    }

    public ProfileAttributesHelper getProfileAttributesHelper()
    {
        return profileAttributesHelper;
    }

    public void setProfileAttributesHelper(ProfileAttributesHelper profileAttributesHelper)
    {
        this.profileAttributesHelper = profileAttributesHelper;
    }

    String effectiveIdentifier(OpenIDAuthRequest authReq) throws InvalidAuthenticationException, UserManagerException
    {
        User user;

        SOAPPrincipal remotePrincipal = getRemotePrincipal();

        if (remotePrincipal != null)
        {
            user = userManager.getUser(remotePrincipal, getLocale());
        }
        else
        {
            user = null;
        }

        return OpenIDAuthenticationManagerGeneric.effectiveIdentifier(authReq, user);
    }
}
