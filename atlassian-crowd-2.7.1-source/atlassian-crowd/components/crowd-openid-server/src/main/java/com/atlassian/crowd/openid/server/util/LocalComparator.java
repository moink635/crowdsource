package com.atlassian.crowd.openid.server.util;

import java.util.Comparator;
import java.util.Locale;

/**
 * Compares two Locales based on their displayLanguage
 * or their displayCountry. Useful for alphabetically
 * sorting a list of Locales.
 */

public class LocalComparator implements Comparator
{
    public static final int DISPLAY_LANGUAGE = 1;
    public static final int DISPLAY_COUNTRY  = 2;

    private int comparatorField = DISPLAY_LANGUAGE;

    public int compare(Object o1, Object o2)
    {
        if (o1 instanceof Locale && o2 instanceof Locale)
        {
            Locale l1 = (Locale) o1;
            Locale l2 = (Locale) o2;

            // compare based on country name
            if (comparatorField == DISPLAY_COUNTRY)
            {
                return l1.getDisplayCountry().compareTo(l2.getDisplayCountry());
            }

            // compare based on language name
            else if (comparatorField == DISPLAY_LANGUAGE)
            {
                return l1.getDisplayLanguage().compareTo(l2.getDisplayLanguage());
            }
        }

        // could not compare
        return -1;
    }

    public int getComparatorField()
    {
        return comparatorField;
    }

    public void setComparatorField(int comparatorField)
    {
        this.comparatorField = comparatorField;
    }
}
