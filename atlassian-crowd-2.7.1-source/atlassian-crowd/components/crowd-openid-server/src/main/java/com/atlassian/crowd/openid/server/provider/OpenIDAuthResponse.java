package com.atlassian.crowd.openid.server.provider;


import java.util.HashMap;
import java.util.Map;

public class OpenIDAuthResponse
{
    private String identifier;
    private String claimedIdentifier;
    private Map attributes; // Map<String attribname, String value>
    private boolean authenticated;

    /**
     * Creates a new OpenID authentication response object.
     *
     * @param identifier the OpenID identifier and claimed identifier.
     *        If the claimed identifier is different, then set it with the corresponding setter.
     * @param authenticated true if the OpenID identifier has been authenticated.
     */
    public OpenIDAuthResponse(String identifier, boolean authenticated)
    {
        this.identifier = identifier;
        this.claimedIdentifier = identifier;
        this.authenticated = authenticated;
    }

    public String getIdentifier()
    {
        return identifier;
    }

    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    public String getClaimedIdentifier()
    {
        return claimedIdentifier;
    }

    public void setClaimedIdentifier(String claimedIdentifier)
    {
        this.claimedIdentifier = claimedIdentifier;
    }

    public Map getAttributes()
    {
        return attributes;
    }

    public void setAttributes(Map attributes)
    {
        this.attributes = attributes;
    }

    public boolean isAuthenticated()
    {
        return authenticated;
    }

    public void setAuthenticated(boolean authenticated)
    {
        this.authenticated = authenticated;
    }

    public void addAttribute(String name, String value)
    {
        if (attributes == null)
        {
            attributes = new HashMap();
        }
        attributes.put(name, value);
    }

    public boolean hasAttributes()
    {
        return attributes != null && attributes.size() > 0;
    }

}
