package com.atlassian.crowd.openid.server.manager.openid;

public class IdentifierViolationException extends Exception
{

    public IdentifierViolationException()
    {
        super();
    }

    public IdentifierViolationException(String message)
    {
        super(message);
    }

    public IdentifierViolationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public IdentifierViolationException(Throwable cause)
    {
        super(cause);   
    }
}
