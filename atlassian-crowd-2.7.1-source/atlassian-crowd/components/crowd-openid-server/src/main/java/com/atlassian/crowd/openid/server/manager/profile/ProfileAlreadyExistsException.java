package com.atlassian.crowd.openid.server.manager.profile;

public class ProfileAlreadyExistsException extends ProfileManagerException
{
    public ProfileAlreadyExistsException()
    {
        super();
    }

    public ProfileAlreadyExistsException(String message)
    {
        super(message);
    }

    public ProfileAlreadyExistsException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ProfileAlreadyExistsException(Throwable cause)
    {
        super(cause); 
    }
}
