package com.atlassian.crowd.openid.server.model.site;

import com.atlassian.crowd.openid.server.model.EntityObject;

public class Site extends EntityObject
{
    private String url;

    public Site() { }

    public Site(String url)
    {
        this.url = url;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public int hashCode()
    {
        return url.hashCode();
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || !(o instanceof Site)) return false;

        Site site = (Site) o;

        // the site url is the natural primary-key (given a user/profile)
        if (!url.equals(site.getUrl())) return false;

        return true;
    }
}
