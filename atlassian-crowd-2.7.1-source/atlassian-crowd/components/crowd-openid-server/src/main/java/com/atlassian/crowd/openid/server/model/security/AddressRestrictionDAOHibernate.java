package com.atlassian.crowd.openid.server.model.security;

import com.atlassian.crowd.openid.server.model.EntityObjectDAOHibernate;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.orm.ObjectRetrievalFailureException;

public class AddressRestrictionDAOHibernate extends EntityObjectDAOHibernate implements AddressRestrictionDAO
{
    public Class getPersistentClass()
    {
        return AddressRestriction.class;
    }

    public void removeAll()
    {
        currentSession().createQuery("DELETE " + AddressRestriction.class.getName()).executeUpdate();
    }

    public AddressRestriction findbyAddress(final String address)
    {
        Criteria criteria = currentSession().createCriteria(getPersistentClass());
        criteria.add(Restrictions.eq("address", address));
        Object result = criteria.uniqueResult();

        if (result == null)
        {
            throw new ObjectRetrievalFailureException(getPersistentClass(), address);
        }

        return (AddressRestriction) result;
    }
}
