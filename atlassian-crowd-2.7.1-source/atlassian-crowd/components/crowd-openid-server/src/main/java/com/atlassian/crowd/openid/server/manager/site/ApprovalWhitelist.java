package com.atlassian.crowd.openid.server.manager.site;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.CharMatcher;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A set of approved URLs. This can be constant, or backed by a file, which will be re-checked as necessary
 * to keep the contents current.
 */
public abstract class ApprovalWhitelist
{
    private static final Logger logger = LoggerFactory.getLogger(ApprovalWhitelist.class);

    public boolean isAutomaticallyApproved(String uri)
    {
        return getApprovedUrls().contains(uri);
    }

    abstract Set<String> getApprovedUrls();

    public static ApprovalWhitelist fromResource(String resourceName) throws IOException
    {
        return fromResource(resourceName, TimeUnit.MINUTES.toMillis(1));
    }

    /**
     * Construct an instance from a resource. If the resource is missing, the whitelist will be empty.
     * If it refers to a local file, that file will be polled for changes. Otherwise, it will be read
     * once and used.
     */
    public static ApprovalWhitelist fromResource(String resourceName, long checkIntervalMillis) throws IOException
    {
        return fromResource(ApprovalWhitelist.class.getResource(resourceName), checkIntervalMillis);
    }

    @VisibleForTesting
    static ApprovalWhitelist fromResource(ClassLoader classLoader, String resourceName, long checkIntervalMillis) throws IOException
    {
        return fromResource(classLoader.getResource(resourceName), checkIntervalMillis);
    }

    private static ApprovalWhitelist fromResource(URL resource, long checkIntervalMillis) throws IOException
    {
        if (resource == null)
        {
            logger.info("No site approval whitelist found. Using an empty list.");
            return new ConstantApprovalWhitelist(Collections.<String>emptySet());
        }
        else
        {
            try
            {
                File f = new File(resource.toURI());
                logger.info("Using file as a site approval whitelist: {}", f);
                return new FileBackedApprovalWhitelist(f, checkIntervalMillis);
            }
            catch (IllegalArgumentException e)
            {
                logger.info("Reading site approval whitelist from resource: {}", resource);
                return new ConstantApprovalWhitelist(readFromStream(resource.openStream()));
            }
            catch (URISyntaxException e)
            {
                logger.info("Reading site approval whitelist from resource: {}", resource);
                return new ConstantApprovalWhitelist(readFromStream(resource.openStream()));
            }
        }
    }

    /* A very loose definition of a valid URI to catch encoding errors */
    private static Predicate<String> VALID_URI = new Predicate<String>()
    {
        @Override
        public boolean apply(String input)
        {
            return CharMatcher.ASCII.matchesAllOf(input);
        }
    };

    static Set<String> readFromStream(InputStream stream) throws IOException
    {
        Iterable<String> lines =
                Iterables.filter(
                        IOUtils.readLines(stream, "utf-8"),
                        Predicates.not(Predicates.equalTo("")));

        for (String invalid : Iterables.filter(lines, Predicates.not(VALID_URI)))
        {
            logger.warn("Ignoring invalid non-ASCII URI in approval whitelist: {}", invalid);
        }

        return ImmutableSet.copyOf(Iterables.filter(lines, VALID_URI));
    }

    private static class ConstantApprovalWhitelist extends ApprovalWhitelist
    {
        private final Set<String> approvedUrls;

        public ConstantApprovalWhitelist(Set<String> urls)
        {
            this.approvedUrls = ImmutableSet.copyOf(urls);
        }

        @Override
        Set<String> getApprovedUrls()
        {
            return approvedUrls;
        }
    }

    static class FileBackedApprovalWhitelist extends ApprovalWhitelist
    {
        private final File file;
        private final long checkIntervalMillis;
        private long nextCheck;
        private long lastModified;
        private Set<String> approvedUrls;

        public FileBackedApprovalWhitelist(File f, long checkIntervalMillis)
        {
            this.file = f;
            Preconditions.checkArgument(checkIntervalMillis > 0, "checkIntervalMillis must be > 0");
            this.checkIntervalMillis = checkIntervalMillis;
            nextCheck = Long.MIN_VALUE;
            makeEmpty();
        }

        private void makeEmpty()
        {
            lastModified = Long.MIN_VALUE;
            approvedUrls = Collections.emptySet();
        }

        @Override
        synchronized Set<String> getApprovedUrls()
        {
            if (System.currentTimeMillis() >= nextCheck)
            {
                try
                {
                    checkApprovedUrls();
                }
                finally
                {
                    nextCheck = System.currentTimeMillis() + checkIntervalMillis;
                }
            }

            return approvedUrls;
        }

        synchronized void checkApprovedUrls()
        {
            long updated = file.lastModified();

            if (updated == 0 && !file.exists())
            {
                makeEmpty();
            }
            else if (updated > lastModified)
            {
                try
                {
                    InputStream in = new FileInputStream(file);
                    try
                    {
                        approvedUrls = readFromStream(in);
                        lastModified = updated;

                        logger.info("Loaded URL whitelist (entries: {})", approvedUrls.size());
                    }
                    finally
                    {
                        in.close();
                    }
                }
                catch (FileNotFoundException e)
                {
                    makeEmpty();
                }
                catch (IOException e)
                {
                    logger.warn("Failed to read approval whitelist from '{}'", file, e);
                }
            }
        }
    }
}
