package com.atlassian.crowd.openid.server.manager.profile;

public class ProfileDoesNotExistException extends ProfileManagerException
{

    public ProfileDoesNotExistException()
    {
        super();
    }

    public ProfileDoesNotExistException(String message)
    {
        super(message);
    }

    public ProfileDoesNotExistException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public ProfileDoesNotExistException(Throwable cause)
    {
        super(cause);   
    }
}
