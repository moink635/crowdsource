package com.atlassian.crowd.openid.server.model;

import com.atlassian.crowd.exception.ObjectNotFoundException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;

public abstract class HibernateDao implements ObjectDao
{
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory)
    {
        this.sessionFactory = sessionFactory;
    }

    protected Session currentSession()
    {
        return sessionFactory.getCurrentSession();
    }

    /**
     * Saves a new DAO object to the persistence store.
     *
     * @param persistentObject The object to save.
     * @throws org.springframework.dao.DataAccessException A persistence exception has occurred.
     */
    public void save(Object persistentObject) throws DataAccessException
    {
        logger.debug("Saving object: "+ persistentObject);

        currentSession().save(persistentObject);
    }

    /**
     * Saves or updates DAO object to the persistence store.
     *
     * @param persistentObject The object to save or update.
     * @throws DataAccessException A persistence exception has occurred.
     */
    public void saveOrUpdate(Object persistentObject) throws DataAccessException
    {
        logger.debug("Saving or updating object: "+ persistentObject);

        currentSession().saveOrUpdate(persistentObject);
    }

    /**
     * Updates an existing DAO object, if the object does not exist it will be added to the persistence store.
     *
     * @param persistentObject The object to update.
     * @throws DataAccessException A persistence exception has occurred.
     */
    public void update(Object persistentObject) throws DataAccessException
    {
        logger.debug("Updating object: "+ persistentObject);

        currentSession().saveOrUpdate(persistentObject);
    }

    /**
     * Removes the DAO object from the persistence store.
     *
     * @param persistentObject The object to remove.
     * @throws DataAccessException A persistence exception has occurred.
     */
    public void remove(Object persistentObject) throws DataAccessException
    {
        logger.debug("Deleting object: "+ persistentObject);

        currentSession().delete(persistentObject);
    }

    /**
     * Loads a persistence DAO object from the persistence store.
     *
     * @param ID              The unique identifier of the object to load from the persistence store.
     * @return The populated object from the database.
     */
    public Object load(final long ID) throws ObjectNotFoundException
    {
        Object obj = currentSession().get(getPersistentClass(), Long.valueOf(ID));

        if (obj == null)
        {
            throw new ObjectNotFoundException(getPersistentClass(), ID);
        }

        logger.debug("Loaded object: "+ obj);

        return obj;
    }

    /**
     * This method calls the <code>session.load</code> method to
     * obtain a proxy (or actual instance if the object is in session)
     * by NOT hitting the database immediately.
     *
     * Do NOT call this method unless you are SURE that the object
     * with the supplied identifier exists.
     *
     * @param id unique identifier to load.
     * @return a proxy object (or actual instance if the object is in session)
     */
    public Object loadReference(final long id)
    {
        return currentSession().load(getPersistentClass(), id);
    }

    /**
     * All subclasses of HibernateDAO <em>must<em> implement this method for {@link HibernateDao#load(long)} to
     * work correctly
     *
     * @return Class
     */
    public abstract Class getPersistentClass();
}