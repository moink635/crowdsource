package com.atlassian.crowd.openid.server.model.record;

import com.atlassian.crowd.openid.server.model.EntityObjectDAO;
import com.atlassian.crowd.openid.server.model.user.User;

import java.util.List;

public interface AuthRecordDAO extends EntityObjectDAO
{
    /**
     * Finds all the AuthRecords for a given user.
     *
     * The records are sorted descendingly based on time.
     *
     * @param user owner of the authentication records.
     * @return List<AuthRecord>.
     */
    List findRecords(User user);

    /**
     * Finds a subset of the AuthRecords for a given user.
     *
     * The records are sorted descendingly based on time.
     *
     * @param user owner of the authentication records.
     * @param startIndex the start index of the record set. A startIndex of 0 implies the first record.
     * @param numRecords the maximum number of records to include in the set.
     * @return List<AuthRecord>.
     */
    List findRecords(User user, int startIndex, int numRecords);

    /**
     * Finds the total number of authentication records
     * that exist for a given user.
     *
     * @param user owner of the authentication records.
     * @return number of authentication records.
     */
    public int findTotalRecords(final User user);
}
