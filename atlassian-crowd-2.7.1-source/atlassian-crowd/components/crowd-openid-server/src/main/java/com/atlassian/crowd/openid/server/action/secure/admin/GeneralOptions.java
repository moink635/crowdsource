package com.atlassian.crowd.openid.server.action.secure.admin;

import com.atlassian.crowd.openid.server.action.BaseAction;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;

public class GeneralOptions extends BaseAction
{
    private String baseURL;
    private boolean enableRelyingPartyLocalhostMode;
    private boolean enableCheckImmediateMode;
    private boolean enableStatelessMode;

    public String doDefault() throws Exception
    {
        baseURL = openIDPropertyManager.getBaseURL();

        enableRelyingPartyLocalhostMode = openIDPropertyManager.isEnableRelyingPartyLocalhostMode().booleanValue();

        enableCheckImmediateMode = openIDPropertyManager.isEnableCheckImmediateMode().booleanValue();

        enableStatelessMode = openIDPropertyManager.isEnableStatelessMode().booleanValue();

        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate() throws Exception
    {
        actionMessageAlertColor = ALERT_BLUE;

        if (!StringUtils.isEmpty(baseURL))
        {
            openIDPropertyManager.setBaseURL(baseURL);
        }

        openIDPropertyManager.setAllowReplyingPartyLocalhostMode(enableRelyingPartyLocalhostMode);

        openIDPropertyManager.setEnableCheckImmediateMode(enableCheckImmediateMode);

        openIDPropertyManager.setEnableStatelessMode(enableStatelessMode);

        String message = getText("updatesuccessful.label");

        addActionMessage(message);

        return SUCCESS;
    }

    public String getBaseURL()
    {
        if (StringUtils.isEmpty(baseURL))
        {
            baseURL = super.getBaseURL();
        }

        return baseURL;
    }

    public void setBaseURL(String baseURL)
    {
        this.baseURL = baseURL;
    }

    public boolean isEnableCheckImmediateMode()
    {
        return enableCheckImmediateMode;
    }

    public void setEnableCheckImmediateMode(boolean enableCheckImmediateMode)
    {
        this.enableCheckImmediateMode = enableCheckImmediateMode;
    }

    public boolean isEnableStatelessMode()
    {
        return enableStatelessMode;
    }

    public void setEnableStatelessMode(boolean enableStatelessMode)
    {
        this.enableStatelessMode = enableStatelessMode;
    }

    public boolean isEnableRelyingPartyLocalhostMode()
    {
        return enableRelyingPartyLocalhostMode;
    }

    public void setEnableRelyingPartyLocalhostMode(boolean enableRelyingPartyLocalhostMode)
    {
        this.enableRelyingPartyLocalhostMode = enableRelyingPartyLocalhostMode;
    }
}
