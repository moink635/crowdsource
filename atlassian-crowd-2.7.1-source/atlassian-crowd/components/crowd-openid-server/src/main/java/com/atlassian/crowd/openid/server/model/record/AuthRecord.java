package com.atlassian.crowd.openid.server.model.record;

import com.atlassian.crowd.openid.server.model.EntityObject;
import com.atlassian.crowd.openid.server.model.site.Site;
import com.atlassian.crowd.openid.server.model.user.User;

public class AuthRecord extends EntityObject
{
    private User user;
    private Site site;

    private AuthAction authAction;

    public AuthRecord() { }

    public AuthRecord(User user, Site site, AuthAction authAction)
    {
        this.user = user;
        this.site = site;
        this.authAction = authAction;
    }

    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public Site getSite()
    {
        return site;
    }

    public void setSite(Site site)
    {
        this.site = site;
    }

    public AuthAction getAuthAction()
    {
        return authAction;
    }

    public void setAuthAction(AuthAction authAction)
    {
        this.authAction = authAction;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || !(o instanceof AuthRecord)) return false;

        AuthRecord that = (AuthRecord) o;

        if (!this.getCreatedDate().equals(that.getCreatedDate())) return false;
        if (site != null ? !site.equals(that.getSite()) : that.getSite() != null) return false;
        if (user != null ? !user.equals(that.getUser()) : that.getUser() != null) return false;

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (user != null ? user.hashCode() : 0);
        result = 31 * result + (site != null ? site.hashCode() : 0);
        result = 31 * result + this.getCreatedDate().hashCode();
        return result;
    }
}
