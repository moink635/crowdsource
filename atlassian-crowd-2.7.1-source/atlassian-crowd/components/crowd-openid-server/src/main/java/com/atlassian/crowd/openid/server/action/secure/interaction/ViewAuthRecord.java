package com.atlassian.crowd.openid.server.action.secure.interaction;

import com.atlassian.crowd.openid.server.action.BaseAction;
import com.atlassian.crowd.openid.server.model.user.User;
import com.atlassian.crowd.openid.server.util.Pager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class ViewAuthRecord extends BaseAction
{
    private static final Logger logger = LoggerFactory.getLogger(ViewAuthRecord.class);

    private static final int MAX_RESULTS_PER_PAGE = 25;

    private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    // input
    private int startIndex = 0;

    // output
    private Pager recordPager;

    public String execute() throws Exception
    {
        User user = userManager.getUser(getRemotePrincipal(), getLocale());

        List authRecords = userManager.getAuthRecords(user, getStartIndex(), MAX_RESULTS_PER_PAGE);
        int totalRecords = userManager.getTotalAuthRecords(user);
        recordPager = new Pager(authRecords, startIndex, MAX_RESULTS_PER_PAGE, totalRecords);

        return SUCCESS;
    }

    public String formatDate(Date date)
    {
        return sdf.format(date);
    }

    public int getStartIndex()
    {
        return startIndex;
    }

    public void setStartIndex(int startIndex)
    {
        this.startIndex = startIndex;
    }

    public Pager getRecordPager()
    {
        return recordPager;
    }
}
