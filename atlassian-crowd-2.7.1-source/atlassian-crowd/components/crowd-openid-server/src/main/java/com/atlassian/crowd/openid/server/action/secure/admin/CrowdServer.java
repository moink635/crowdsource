package com.atlassian.crowd.openid.server.action.secure.admin;

import com.atlassian.crowd.openid.server.action.BaseAction;
import com.atlassian.crowd.service.client.ClientProperties;

public class CrowdServer extends BaseAction
{
    private String serverURL;
    private String application;
    private ClientProperties clientProperties;

    public String execute() throws Exception
    {

        serverURL = clientProperties.getBaseURL();

        application = clientProperties.getApplicationName();

        return SUCCESS;
    }

    public void setClientProperties(ClientProperties clientProperties)
    {
        this.clientProperties = clientProperties;
    }

    public String getApplication()
    {
        return application;
    }

    public String getServerURL()
    {
        return serverURL;
    }
}
