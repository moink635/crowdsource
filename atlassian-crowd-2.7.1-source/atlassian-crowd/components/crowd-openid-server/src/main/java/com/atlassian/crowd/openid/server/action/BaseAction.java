package com.atlassian.crowd.openid.server.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.integration.http.HttpAuthenticator;
import com.atlassian.crowd.integration.soap.SOAPAttribute;
import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.model.user.UserConstants;
import com.atlassian.crowd.openid.server.manager.openid.OpenIDAuthenticationManager;
import com.atlassian.crowd.openid.server.manager.profile.ProfileManager;
import com.atlassian.crowd.openid.server.manager.property.OpenIDPropertyManager;
import com.atlassian.crowd.openid.server.manager.site.SiteManager;
import com.atlassian.crowd.openid.server.manager.user.UserManager;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.xwork.XsrfTokenGenerator;

import com.opensymphony.util.TextUtils;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionSupport;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseAction extends ActionSupport
{
    private static final Logger logger = LoggerFactory.getLogger(BaseAction.class);

    protected Boolean authenticated = null;

    private SOAPPrincipal remotePrincipal;

    String baseURL;

    private XsrfTokenGenerator xsrfTokenGenerator;

    /**
     * Webwork action messages color settings. Displays the type of message dressing to use.
     */
    protected String actionMessageAlertColor;

    /**
     * @see BaseAction#actionMessageAlertColor;
     */
    public final static String ALERT_BLUE = "blue";

    /**
     * @see BaseAction#actionMessageAlertColor;
     */
    public final static String ALERT_YELLOW = "yellow";

    /**
     * @see BaseAction#actionMessageAlertColor;
     */
    public final static String ALERT_RED = "red";

    protected OpenIDPropertyManager openIDPropertyManager;
    protected UserManager userManager;
    protected SiteManager siteManager;
    protected ProfileManager profileManager;
    protected OpenIDAuthenticationManager openIDAuthenticationManager;
    private HttpAuthenticator httpAuthenticator;
    private SecurityServerClient securityServerClient;


    protected Boolean isAdministrator;

    public String getXsrfTokenName()
    {
        return xsrfTokenGenerator.getXsrfTokenName();
    }

    public String getXsrfToken()
    {
        return xsrfTokenGenerator.generateToken(getHttpRequest());
    }

    /**
     * Checks if a principal is currently authenticated verses the Crowd security server.
     *
     * @return <code>true</code> if and only if the principal is currently authenticated, otherwise <code>false</code>.
     */
    public boolean isAuthenticated()
    {
        if (authenticated == null)
        {
            try
            {
                HttpServletRequest request = ServletActionContext.getRequest();
                HttpServletResponse response = ServletActionContext.getResponse();

                if (request != null && response != null)
                {
                    authenticated = getHttpAuthenticator().isAuthenticated(request, response);
                }
                else
                {
                    authenticated = Boolean.FALSE;
                }
            }
            catch (Exception e)
            {
                logger.info(e.getMessage(), e);
                authenticated = Boolean.FALSE;
            }
        }
        return authenticated;
    }

    /**
     * Gets the HTTP session for the current user.
     *
     * @return The HTTP session.
     */
    protected HttpSession getSession()
    {
        return getRequest().getSession();
    }

    /**
     * Gets the underlying HTTP request from the Action.
     *
     * @return the underlying HTTP request.
     */
    protected HttpServletRequest getRequest()
    {
        return ServletActionContext.getRequest();
    }


    public SOAPPrincipal getRemotePrincipal() throws InvalidAuthenticationException
    {
        if (!isAuthenticated())
        {
            return null;
        }

        if (remotePrincipal == null)
        {

            try
            {
                // find the principal off their authenticated token key.
                remotePrincipal = getHttpAuthenticator().getPrincipal(ServletActionContext.getRequest());
            }
            catch (Exception e)
            {
                logger.info(e.getMessage(), e);

                throw new InvalidAuthenticationException("Cannot get remote principal from the token", e);
            }
        }

        return remotePrincipal;
    }


    /**
     * Gets the user's fullname for display.
     *
     * @return The fullname.
     * @throws InvalidAuthenticationException
     */
    public String getPrincipalName() throws InvalidAuthenticationException
    {
        if (!isAuthenticated())
        {
            return null;
        }

        String principalName = "";

        if (getRemotePrincipal() != null)
        {
            String firstName = getFirstAttribute(UserConstants.FIRSTNAME);
            String lastName = getFirstAttribute(UserConstants.LASTNAME);

            if (TextUtils.stringSet(firstName))
            {
                principalName = firstName;
            }

            if (TextUtils.stringSet(lastName))
            {
                if (TextUtils.stringSet(principalName) && principalName.length() > 0)
                {
                    principalName += " ";
                }

                principalName += lastName;
            }

            if (!TextUtils.stringSet(principalName))
            {
                principalName = getRemotePrincipal().getName();
            }
        }

        return principalName;
    }

    public String getFirstAttribute(String name) throws InvalidAuthenticationException
    {
        SOAPAttribute attribute = getAttribute(name);

        if (attribute == null)
        {
            return null;
        }

        if (attribute.getValues().length > 0)
        {
            return attribute.getValues()[0];
        }

        return null;
    }

    public SOAPAttribute getAttribute(String name) throws InvalidAuthenticationException
    {
        if (!isAuthenticated())
        {
            return null;
        }

        SOAPPrincipal remotePrincipal = getRemotePrincipal();

        SOAPAttribute[] attributes = remotePrincipal.getAttributes();

        for (int i = 0; i < attributes.length; i++)
        {
            if (attributes[i].getName().equals(name))
            {
                return attributes[i];
            }
        }

        // unable to find the attribute, return a new empty one.
        SOAPAttribute soapAttribute = new SOAPAttribute();
        soapAttribute.setName(name);

        return soapAttribute;
    }

    public String getBaseURL()
    {
        if (StringUtils.isEmpty(baseURL))
        {
            try
            {
                baseURL = openIDPropertyManager.getBaseURL();

                if (StringUtils.isEmpty(baseURL))
                {
                    baseURL = baseUrl(getHttpRequest());
                }
            }
            catch (Exception e)
            {
                logger.warn(e.getMessage(), e);
            }
        }

        return baseURL;
    }

    public static String baseUrl(HttpServletRequest request)
    {
        if (request == null)
            return null;
        String port = "";
        if (!isStandardPort(request.getScheme(), request.getServerPort()))
        {
            port = ":" + request.getServerPort();
        }
        return request.getScheme() + "://" + request.getServerName() + port + request.getContextPath() + "/";
    }

    private static boolean isStandardPort(String scheme, int port)
    {
        if (scheme.equalsIgnoreCase("http") && port == 80) return true;
        if (scheme.equalsIgnoreCase("https") && port == 443) return true;
        return false;
    }

    protected HttpServletRequest getHttpRequest()
    {
        return ServletActionContext.getRequest();
    }

    public boolean isAdministrator()
    {
        try
        {
            if (isAdministrator == null)
            {
                SOAPPrincipal principal = getRemotePrincipal();

                if (principal == null)
                {
                    isAdministrator = Boolean.FALSE;
                }
                else
                {
                    String username = principal.getName();

                    boolean result = userManager.isAdministrator(username);

                    isAdministrator = new Boolean(result);
                }

            }
        }
        catch (Exception e)
        {
            logger.warn(e.getMessage(), e);

            return false;
        }

        return isAdministrator.booleanValue();
    }

    /**
     * Gets the action messageo color dressing to use with the decorator.
     *
     * @return The window dressing color to use.
     */
    public String getActionMessageAlertColor()
    {
        if (StringUtils.isEmpty(actionMessageAlertColor))
        {
            return ALERT_YELLOW;
        }
        else
        {
            return actionMessageAlertColor;
        }
    }

    public boolean containsActionMessages()
    {
        return getActionMessages() != null && !getActionMessages().isEmpty();
    }

    public void setPropertyManager(OpenIDPropertyManager openIDPropertyManager)
    {
        this.openIDPropertyManager = openIDPropertyManager;
    }

    public void setUserManager(UserManager userManager)
    {
        this.userManager = userManager;
    }

    public void setSiteManager(SiteManager siteManager)
    {
        this.siteManager = siteManager;
    }

    public void setProfileManager(ProfileManager profileManager)
    {
        this.profileManager = profileManager;
    }

    public void setOpenIDAuthenticationManager(OpenIDAuthenticationManager openIDAuthenticationManager)
    {
        this.openIDAuthenticationManager = openIDAuthenticationManager;
    }

    public String getIdentifier() throws InvalidAuthenticationException
    {
        if (getRemotePrincipal() != null)
        {
            return ViewPublicIdentity.openIdIdentifier(getBaseURL(), getRemotePrincipal().getName());
        }
        else
        {
            return null;
        }
    }

    /**
     * Sets a UI message and the color type for the user.
     * @param color The color to use.
     * @param message The message to display.
     */
    public void addActionMessage(String color, String message)
    {
        actionMessageAlertColor = color;
        addActionMessage(message);
    }

    public HttpAuthenticator getHttpAuthenticator()
    {
        return httpAuthenticator;
    }

    public void setHttpAuthenticator(HttpAuthenticator httpAuthenticator)
    {
        this.httpAuthenticator = httpAuthenticator;
    }

    public SecurityServerClient getSecurityServerClient()
    {
        return securityServerClient;
    }

    public void setSecurityServerClient(SecurityServerClient securityServerClient)
    {
        this.securityServerClient = securityServerClient;
    }

    public void setXsrfTokenGenerator(XsrfTokenGenerator xsrfTokenGenerator)
    {
        this.xsrfTokenGenerator = xsrfTokenGenerator;
    }
}