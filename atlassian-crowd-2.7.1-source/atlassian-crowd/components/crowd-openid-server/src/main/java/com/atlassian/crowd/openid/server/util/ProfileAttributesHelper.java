package com.atlassian.crowd.openid.server.util;


import org.apache.commons.lang3.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class ProfileAttributesHelper
{
    private static Map days;
    private static Map months;
    private static Map years;
    private static Map genders;
    private static Map countries;
    private static Map timezones;
    private static Map languages;

    static
    {
        initialise();
    }

    public Map getDays()
    {
        if (days == null)
        {
            initialise();
        }
        return days;
    }

    public Map getMonths()
    {
        if (months == null)
        {
            initialise();
        }
        return months;
    }

    public Map getYears()
    {
        if (years == null)
        {
            initialise();
        }
        return years;
    }

    public Map getGenders()
    {
        if (genders == null)
        {
            initialise();
        }
        return genders;
    }

    public Map getCountries()
    {
        if (countries == null)
        {
            initialise();
        }
        return countries;
    }

    public Map getLanguages()
    {
        if (languages == null)
        {
            initialise();
        }
        return languages;
    }

    public Map getTimezones()
    {
        if (timezones == null)
        {
            initialise();
        }
        return timezones;
    }

    public String getNiceDate(String dateString)
    {
        if (dateString == null || dateString.length() != 10)
        {
            return null;
        }

        StringBuffer dateParseBuffer = new StringBuffer();
        StringBuffer dateFormatBuffer = new StringBuffer();

        // check for day
        if (!dateString.substring(8, 10).equals("00"))
        {
            dateFormatBuffer.append("d ");
            dateParseBuffer.insert(0, "-dd");
        }
        else
        {
            dateParseBuffer.insert(0, "-00");
        }

        // check for month
        if (!dateString.substring(5, 7).equals("00"))
        {
            dateFormatBuffer.append("MMMM ");
            dateParseBuffer.insert(0, "-MM");
        }
        else
        {
            dateParseBuffer.insert(0, "-00");
        }

        // check for year
        if (!dateString.substring(0, 4).equals("0000"))
        {
            dateFormatBuffer.append("yyyy");
            dateParseBuffer.insert(0, "yyyy");
        }
        else
        {
            dateParseBuffer.insert(0, "0000");
        }

        // check if any of the above is there (day/month/year)
        if (dateFormatBuffer.length() == 0 || dateParseBuffer.length() == 0)
        {
            return null;
        }

        // parse the incoming date
        SimpleDateFormat sdfParser = new SimpleDateFormat(dateParseBuffer.toString());
        Date date;
        try
        {
            date = sdfParser.parse(dateString);
        }
        catch (ParseException e)
        {
            return null;
        }

        // format the date in a nice format
        SimpleDateFormat sdfFormatter = new SimpleDateFormat(dateFormatBuffer.toString());
        return sdfFormatter.format(date);
    }


    public static void initialise()
    {
        // instantiate maps
        days = new LinkedHashMap();
        months = new LinkedHashMap();
        years = new LinkedHashMap();
        genders = new LinkedHashMap();
        countries = new LinkedHashMap();
        languages = new LinkedHashMap();
        timezones = new LinkedHashMap();

        // put in defaults (ie. no selections)
        days.put("0", "Day");
        months.put("0", "Month");
        years.put("0", "Year");
        genders.put("", "--");
        countries.put("", "--");
        languages.put("", "--");
        timezones.put("", "--");

        // make days from 1 to 31
        for(int i = 1; i <= 31; i++)
        {
            String day = Integer.toString(i);
            days.put(day, day);
        }

        // make months from January to December
        SimpleDateFormat sdfParser = new SimpleDateFormat("MM");
        SimpleDateFormat sdfFormatter = new SimpleDateFormat("MMMM");
        for(int i = 1; i <= 12; i++)
        {
            String n = Integer.toString(i);
            try
            {
                months.put(n, sdfFormatter.format(sdfParser.parse(n)));
            }
            catch (ParseException e)
            {
                // can't do much
            }
        }

        // make years from last year backwards
        Date now = new Date();
        SimpleDateFormat yearExtractor = new SimpleDateFormat("yyyy");
        for (int year = Integer.parseInt(yearExtractor.format(now))-1; year >= 1900; year--)
        {
            String n = Integer.toString(year);
            years.put(n, n);
        }

        // make male/female genders
        genders.put("M", "Male");
        genders.put("F", "Female");

        // get all locales
        Locale[] locales = Locale.getAvailableLocales();

        // make a map for (country-code, country-name) sorted by country-name
        LocalComparator localComparator = new LocalComparator();
        localComparator.setComparatorField(LocalComparator.DISPLAY_COUNTRY);
        Arrays.sort(locales, localComparator);
        for (int i = 0; i < locales.length; i++)
        {
            if (StringUtils.isNotBlank(locales[i].getCountry()) && StringUtils.isNotBlank(locales[i].getDisplayCountry()))
            {
                countries.put(locales[i].getCountry(), locales[i].getDisplayCountry());
            }
        }

        // make a map for (language-code, language-name) sorted by language-name
        localComparator.setComparatorField(LocalComparator.DISPLAY_LANGUAGE);
        Arrays.sort(locales, localComparator);
        languages.put("en", "English"); // put english at the top
        for (int i = 0; i < locales.length; i++)
        {
            if (StringUtils.isNotBlank(locales[i].getLanguage()) && StringUtils.isNotBlank(locales[i].getDisplayLanguage()))
            {
                languages.put(locales[i].getLanguage(), locales[i].getDisplayLanguage());
            }
        }        

        // make map for (timezone-code, timezone-code) sorted by timezone-code
        String[] timeZoneIds = TimeZone.getAvailableIDs();
        Arrays.sort(timeZoneIds);
        for (int i = 0; i < timeZoneIds.length; i++)
        {
            if (!timeZoneIds[i].startsWith("SystemV/"))
            {
                timezones.put(timeZoneIds[i], timeZoneIds[i]);
            }
        }
    }
}
