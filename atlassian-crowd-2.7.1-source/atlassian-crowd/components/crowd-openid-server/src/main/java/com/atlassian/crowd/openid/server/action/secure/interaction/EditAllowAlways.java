package com.atlassian.crowd.openid.server.action.secure.interaction;

import com.atlassian.crowd.openid.server.action.BaseAction;
import com.atlassian.crowd.openid.server.model.user.User;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Iterator;

public class EditAllowAlways extends BaseAction
{
    private static final Logger logger = LoggerFactory.getLogger(EditAllowAlways.class);

    // output
    private List siteApprovals;
    private Collection profiles;

    // input
    private List urls = new ArrayList();
    private List profileIDs = new ArrayList();

    public String doDefault() throws Exception
    {
        User user = userManager.getUser(getRemotePrincipal(), getLocale());
        profiles = user.getProfiles();
        siteApprovals = siteManager.getAllAlwaysAllowSites(user);
        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate() throws Exception
    {
        User user = userManager.getUser(getRemotePrincipal(), getLocale());

        // update allow-always sites
        siteManager.updateAlwaysAllowApprovals(user, urls, profileIDs);

        addActionMessage(ALERT_BLUE, getText("updatesuccessful.label"));

        // display the default screen
        return doDefault();
    }


    public List getSiteApprovals()
    {
        return siteApprovals;
    }

    public void setSiteApprovals(List siteApprovals)
    {
        this.siteApprovals = siteApprovals;
    }

    public Collection getProfiles()
    {
        return profiles;
    }

    public void setProfiles(Collection profiles)
    {
        this.profiles = profiles;
    }

    public List getUrls()
    {
        return urls;
    }

    public void setUrls(List urls)
    {
        this.urls = urls;
    }

    public List getProfileIDs()
    {
        return profileIDs;
    }

    public void setProfileIDs(List profileIDs)
    {
        this.profileIDs = profileIDs;
    }
}
