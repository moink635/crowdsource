package com.atlassian.crowd.openid.server.manager.openid;

import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.openid.server.model.user.User;
import com.atlassian.crowd.openid.server.provider.OpenIDAuthRequest;
import com.atlassian.crowd.openid.server.provider.OpenIDAuthResponse;

import java.util.Locale;

public interface OpenIDAuthenticationManager
{
    /**
     * Validates the OpenID authentication request.
     *
     * Does nothing if the request is valid. Throws a
     * corresponding exception if request is invalid.
     *
     * @param user logged in user.
     * @param authReq OpenID authentication request.
     * @throws com.atlassian.crowd.openid.server.manager.openid.InvalidRequestException if authReq is null or the return_to URL is invalid/malformed.
     * @throws com.atlassian.crowd.openid.server.manager.openid.SiteDisallowedException if the site is disallowed by whitelist/blacklist or the site is localhost and local return_to URLs have been disallowed.
     * @throws com.atlassian.crowd.openid.server.manager.openid.IdentifierViolationException if the principal does not own the OpenID identifier.
     */
    void validateRequest(User user, OpenIDAuthRequest authReq) throws InvalidRequestException, SiteDisallowedException, IdentifierViolationException;

    /**
     * Processes a request if the "allow_always" flag has been set
     * for the requesting site.
     *
     * The OpenIDAuthResponse is successful and contains attributes from
     * the associated profile, if the site is trusted by the user.
     *
     * Otherwise, it is unsuccessful.
     *
     * @param user user processing request.
     * @param authReq OpenIDAuthRequest being processed.
     * @return OpenIDAuthResponse corresponding to success if the site is trusted by the user and has an associated profile set.
     */
    OpenIDAuthResponse autoAllowRequest(User user, OpenIDAuthRequest authReq);

    /**
     * Process a request if the "deny" action is taken by the user
     * when a site has requested authentication.
     *
     * This creates an authentication record of the deny action and
     * returns an unsuccessful OpenIDAuthResponse.
     *
     * @param user user processing request.
     * @param authReq OpenIDAuthRequest being processed.
     * @return unsuccessful OpenIDAuthResponse.
     */
    OpenIDAuthResponse denyRequest(User user, OpenIDAuthRequest authReq);

    /**
     * Process a request if the "allow" or "allow always" action
     * is taken by the user when a site has requested authentication.
     *
     * This creates an authentication record of the deny action and
     * returns an successful OpenIDAuthResponse containing attributes
     * from the user's profile.
     *
     * If an error occurs (such as, the user is trying to access someone
     * else's profile, or the profile selected does not exist) an
     * unsuccessful OpenIDAuthResponse is generated.
     *
     * @param user user processing request.
     * @param profileID ID of profile containing attributes to be used in response.
     * @param authReq OpenIDAuthRequest being processed.
     * @param alwaysAllow true if the user wants to trust the site.
     * @return successful OpenIDAuthResponse if request processed without errors, otherwise unsuccessful OpenIDAuthResponse.
     */
    OpenIDAuthResponse allowRequest(User user, long profileID, OpenIDAuthRequest authReq, boolean alwaysAllow);

    /**
     * A convenience wrapper for the autoAllowRequest method.
     *
     * This only returns a successful response if the user has
     * trusted the site requesting authentication.
     *
     * If any errors occur, eg. backend errors getting the
     * user object from the principal, or the requesting site
     * is blacklisted/blocked, an immediate unsuccessful
     * response is returned
     *
     * @param principal principal corresponding to logged in user.
     * @param locale locale of the logged in user.
     * @param authReq authentication request of the user.
     * @return successful OpenIDAuthResponse iff the request is valid, site is not banned and user has a pre-existing trust relationship with the site.
     */
    OpenIDAuthResponse checkImmediate(SOAPPrincipal principal, Locale locale, OpenIDAuthRequest authReq);
}
