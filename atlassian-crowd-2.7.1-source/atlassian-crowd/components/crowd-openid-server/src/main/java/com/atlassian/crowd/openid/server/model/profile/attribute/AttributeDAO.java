package com.atlassian.crowd.openid.server.model.profile.attribute;

import com.atlassian.crowd.openid.server.model.EntityObjectDAO;
import com.atlassian.crowd.openid.server.model.profile.Profile;

public interface AttributeDAO extends EntityObjectDAO
{
    /**
     * Finds an attribute in a profile given a specific name.
     * @param name name of attribute.
     * @param profile profile to look in.
     * @return attribute object (name-value pair).
     */
    public Attribute findAttributeByName(String name, Profile profile);
}
