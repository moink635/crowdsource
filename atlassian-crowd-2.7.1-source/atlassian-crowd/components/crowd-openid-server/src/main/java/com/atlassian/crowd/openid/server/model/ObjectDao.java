package com.atlassian.crowd.openid.server.model;

import com.atlassian.crowd.exception.ObjectNotFoundException;
import org.springframework.dao.DataAccessException;

public interface ObjectDao
{
    /**
     * The class that is being persisted
     *
     * @return Class
     */
    Class getPersistentClass();

    /**
     * Saves a new DAO object to the persistence store.
     *
     * @param persistentObject The object to save.
     * @throws org.springframework.dao.DataAccessException
     *          A persistence exception has occurred.
     */
    void save(Object persistentObject) throws DataAccessException;

    /**
     * Updates an existing DAO object, if the object does not exist it will be added to the persistence store.
     *
     * @param persistentObject The object to update.
     * @throws DataAccessException A persistence exception has occurred.
     */
    void update(Object persistentObject) throws DataAccessException;

    /**
     * Removes the DAO object from the persistence store.
     *
     * @param persistentObject The object to remove.
     * @throws DataAccessException A persistence exception has occurred.
     */
    void remove(Object persistentObject) throws DataAccessException;

    /**
     * Loads a persistnce DAO object from the persistence store.
     *
     * @param ID The unique identifier of the object to load from the persistence store.
     * @return Object The populated object from the database
     * @throws com.atlassian.crowd.exception.ObjectNotFoundException
     *          if the given object with <code>ID</code>
     *          for the persistent class does not exist
     */
    Object load(long ID) throws ObjectNotFoundException;
}
