package com.atlassian.crowd.openid.server.manager.property;

public class OpenIDPropertyManagerException extends Exception
{
    /**
     * Default constructor.
     */
    public OpenIDPropertyManagerException() {
    }

    /**
     * Default constructor.
     *
     * @param s the message.
     */
    public OpenIDPropertyManagerException(String s) {
        super(s);
    }

    /**
     * Default constructor.
     *
     * @param s         the message.
     * @param throwable the {@link Exception Exception}.
     */
    public OpenIDPropertyManagerException(String s, Throwable throwable) {
        super(s, throwable);
    }

    /**
     * Default constructor.
     *
     * @param throwable the {@link Exception Exception}.
     */
    public OpenIDPropertyManagerException(Throwable throwable) {
        super(throwable);
    }
}