package com.atlassian.crowd.openid.server.model.property;

import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.openid.server.model.EntityObjectDAO;

public interface PropertyDAO extends EntityObjectDAO
{
    Property findByName(long name) throws ObjectNotFoundException;
}
