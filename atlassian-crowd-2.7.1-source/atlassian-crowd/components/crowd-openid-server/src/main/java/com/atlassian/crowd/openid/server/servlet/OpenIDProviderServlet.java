package com.atlassian.crowd.openid.server.servlet;

import java.io.IOException;
import java.util.Set;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.atlassian.crowd.openid.server.provider.CrowdProvider;
import com.atlassian.crowd.openid.server.provider.MalformedOpenIDRequestException;
import com.atlassian.crowd.openid.server.provider.OpenIDAuthResponse;
import com.atlassian.crowd.openid.server.provider.OpenIDException;

import com.google.common.collect.ImmutableSet;

import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.message.ParameterList;
import org.openxri.xml.Service;
import org.openxri.xml.XRD;
import org.openxri.xml.XRDS;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class OpenIDProviderServlet extends HttpServlet
{
    private CrowdProvider crowdProvider = null;

    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        // get the crowdConsumer bean from spring
        final WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
        crowdProvider = context.getBean("crowdProvider", CrowdProvider.class);
    }

    static XRDS createOpenId2XrdsForOp(String url)
    {
        XRDS x = new XRDS();

        XRD descriptor = new XRD();

        Service svc = new Service();

        svc.addType(DiscoveryInformation.OPENID2_OP);
        svc.addURI(url);

        descriptor.addService(svc);

        x.add(descriptor);

        return x;
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        if (isRedirectedClientRequest(request))
        {
            try
            {
                crowdProvider.processOpenIDRequest(request, response);
            }
            catch (MalformedOpenIDRequestException e)
            {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
            }
            catch (OpenIDException e)
            {
                // throw a 500 error if we have an error servicing the OpenID request
                throw new ServletException("Error servicing OpenID request", e);
            }
        }
        else
        {
            serveXrdsForEndpoint(request, response);
        }
    }

    private void serveXrdsForEndpoint(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        XRDS x = createOpenId2XrdsForOp(req.getRequestURL().toString());

        try
        {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();

            Element element = x.toDOM(doc);

            TransformerFactory tf = TransformerFactory.newInstance();

            Transformer identity = tf.newTransformer();

            identity.setOutputProperty(OutputKeys.INDENT, "yes");
            identity.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

            resp.setContentType("application/xrds+xml");

            identity.transform(new DOMSource(element), new StreamResult(resp.getOutputStream()));
        }
        catch (TransformerException e)
        {
            throw new ServletException(e);
        }
        catch (ParserConfigurationException e)
        {
            throw new ServletException(e);
        }
        catch (FactoryConfigurationError e)
        {
            throw new ServletException(e);
        }
    }

    private static final Set<String> REDIRECTED_MODES = ImmutableSet.of("checkid_setup", "checkid_immediate");

    private static boolean isRedirectedClientRequest(HttpServletRequest req)
    {
        ParameterList requestParameters = new ParameterList(req.getParameterMap());
        if (requestParameters.hasParameter("openid.mode"))
        {
            return REDIRECTED_MODES.contains(requestParameters.getParameterValue("openid.mode"));
        }
        else
        {
            return false;
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            // check if we are dispatching an authentication response (from the application)
            if (request.getAttribute(OpenIDServerServlet.OPENID_AUTHENTICATION_APPLICATION_RESPONSE) != null)
            {
                crowdProvider.sendAuthenticationResponse(request, response, (OpenIDAuthResponse)request.getAttribute(OpenIDServerServlet.OPENID_AUTHENTICATION_APPLICATION_RESPONSE));
            }

            // otherwise we are processing an OpenID request from the client
            else
            {
                crowdProvider.processOpenIDRequest(request, response);
            }
        }
        catch (MalformedOpenIDRequestException e)
        {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
        }
        catch (OpenIDException e)
        {
            // throw a 500 error if we have an error servicing the OpenID request
            throw new ServletException("Error servicing OpenID request", e);
        }
    }
}
