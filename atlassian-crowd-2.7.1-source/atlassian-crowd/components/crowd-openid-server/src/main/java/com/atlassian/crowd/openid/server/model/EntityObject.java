package com.atlassian.crowd.openid.server.model;

import java.util.Date;
import java.io.Serializable;

/**
 * Base entity class which represents any object that requires persistence.
 */

public abstract class EntityObject implements Serializable
{
    private Long id;
    private Date createdDate;
    private Date updatedDate;    

    public EntityObject()
    {
        // default constructor
    }

    public Long getId()
    {
        return id;
    }

    public void setId(Long id)
    {
        this.id = id;
    }

    public Date getCreatedDate()
    {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;
    }

    public Date getUpdatedDate()
    {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate)
    {
        this.updatedDate = updatedDate;
    }

    public abstract int hashCode();

    public abstract boolean equals(Object o);

}
