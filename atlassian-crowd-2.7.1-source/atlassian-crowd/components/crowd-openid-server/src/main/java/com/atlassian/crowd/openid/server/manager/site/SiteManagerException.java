package com.atlassian.crowd.openid.server.manager.site;

public class SiteManagerException extends Exception
{
    public SiteManagerException()
    {
        super();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public SiteManagerException(String message)
    {
        super(message);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public SiteManagerException(String message, Throwable cause)
    {
        super(message, cause);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public SiteManagerException(Throwable cause)
    {
        super(cause);    //To change body of overridden methods use File | Settings | File Templates.
    }
}
