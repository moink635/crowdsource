package com.atlassian.crowd.openid.server.provider;

public class MalformedOpenIDRequestException extends OpenIDException
{

    public MalformedOpenIDRequestException()
    {
        super();
    }

    public MalformedOpenIDRequestException(String message)
    {
        super(message);
    }

    public MalformedOpenIDRequestException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MalformedOpenIDRequestException(Throwable cause)
    {
        super(cause);
    }
}
