package com.atlassian.crowd.openid.server.action;

import com.atlassian.crowd.integration.http.VerifyTokenFilter;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.ServletActionContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Logs a user into the Crowd OpenID server.
 */
public class Login extends BaseAction
{
    private static final Logger logger = LoggerFactory.getLogger(Login.class);

    private String username;
    private String password;

    @Override
    public String doDefault() throws Exception
    {
        if (isAuthenticated())
        {
            return SUCCESS;
        }
        else
        {
            return INPUT;
        }
    }

    @RequireSecurityToken(true)
    public String doUpdate()
    {
        try
        {
            if (username != null && !username.equals("") && password != null)
            {
                getHttpAuthenticator().authenticate(ServletActionContext.getRequest(), ServletActionContext.getResponse(), username, password);

                String requestingPage = (String) getSession().getAttribute(VerifyTokenFilter.ORIGINAL_URL);

                if (requestingPage != null)
                {
                    ServletActionContext.getResponse().sendRedirect(requestingPage);

                    return NONE;

                }
                else
                {
                    return SUCCESS;
                }

            }
            else
            {
                // didn't supply authentication information, check if already authenticated
                if (isAuthenticated())
                {
                    return SUCCESS;
                }
            }

        }
        catch (Exception e)
        {
            addActionError(getText("invalidlogin.label"));

            logger.info(e.getMessage(), e);
        }
        return INPUT;
    }

    public String getUsername()
    {
        return username;
    }

    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
}