package com.atlassian.crowd.openid.server.model.property;

import com.atlassian.crowd.exception.ObjectNotFoundException;
import com.atlassian.crowd.openid.server.model.EntityObjectDAOHibernate;

import org.hibernate.criterion.Restrictions;

public class PropertyDAOHibernate extends EntityObjectDAOHibernate implements PropertyDAO
{
    public Class getPersistentClass()
    {
        return Property.class;
    }

    /**
     * @see com.atlassian.crowd.model.property.PropertyDAO#findByName(long)
     */
    public Property findByName(final long name) throws ObjectNotFoundException
    {
        Object result = currentSession().createCriteria(Property.class).add(Restrictions.eq("name", Long.valueOf(name))).uniqueResult();

        if (result == null)
        {
            throw new ObjectNotFoundException(getPersistentClass(), name);
        }
        return (Property) result;
    }
}
