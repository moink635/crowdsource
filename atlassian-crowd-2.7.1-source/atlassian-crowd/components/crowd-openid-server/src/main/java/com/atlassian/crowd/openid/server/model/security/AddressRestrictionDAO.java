package com.atlassian.crowd.openid.server.model.security;

import com.atlassian.crowd.openid.server.model.EntityObjectDAO;

import java.util.List;

public interface AddressRestrictionDAO extends EntityObjectDAO
{
    void removeAll();

    AddressRestriction findbyAddress(String address);
}
