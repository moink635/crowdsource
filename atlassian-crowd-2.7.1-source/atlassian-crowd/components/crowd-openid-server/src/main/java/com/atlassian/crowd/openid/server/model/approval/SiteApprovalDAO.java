package com.atlassian.crowd.openid.server.model.approval;

import com.atlassian.crowd.openid.server.model.EntityObjectDAO;
import com.atlassian.crowd.openid.server.model.profile.Profile;
import com.atlassian.crowd.openid.server.model.site.Site;
import com.atlassian.crowd.openid.server.model.user.User;

import java.util.List;

public interface SiteApprovalDAO extends EntityObjectDAO
{
    public SiteApproval findBySite(User user, Site site);
    public List findAlwaysAllow(User user);
    public List findAllForProfile(Profile profile);
}
