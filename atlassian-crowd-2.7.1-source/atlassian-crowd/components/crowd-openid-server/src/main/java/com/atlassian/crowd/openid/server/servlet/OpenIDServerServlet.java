package com.atlassian.crowd.openid.server.servlet;

import com.atlassian.crowd.openid.server.provider.CrowdProvider;
import com.atlassian.crowd.openid.server.provider.OpenIDAuthResponse;
import com.atlassian.crowd.openid.server.provider.OpenIDException;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class OpenIDServerServlet extends HttpServlet
{
    // request attribute key
    public static final String OPENID_AUTHENTICATION_APPLICATION_RESPONSE = OpenIDServerServlet.class.getName() + ".openid.auth.app.response";

    // spring injected
    private CrowdProvider crowdProvider = null;

    /**
     * Initialise the servlet to inject the CrowdConsumer from Spring.
     *
     * @param config ServletConfig
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        // get the crowdConsumer bean from spring
        final WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
        crowdProvider = context.getBean("crowdProvider", CrowdProvider.class);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        try
        {
            // check if we are dispatching an authentication response (from the application)
            if (request.getAttribute(OPENID_AUTHENTICATION_APPLICATION_RESPONSE) != null)
            {
                crowdProvider.sendAuthenticationResponse(request, response, (OpenIDAuthResponse)request.getAttribute(OPENID_AUTHENTICATION_APPLICATION_RESPONSE));
            }

            // otherwise we are processing an OpenID request from the client
            else
            {
                crowdProvider.processOpenIDRequest(request, response);
            }
        }
        catch (OpenIDException e)
        {
            // throw a 500 error if we have an error servicing the OpenID request
            throw new ServletException("Error servicing OpenID request", e);
        }
    }
}
