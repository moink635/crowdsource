package com.atlassian.crowd.openid.server.action.secure.admin;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import com.atlassian.core.util.PairType;
import com.atlassian.crowd.openid.server.action.BaseAction;
import com.atlassian.crowd.openid.server.manager.property.OpenIDPropertyManagerException;
import com.atlassian.crowd.openid.server.manager.property.TrustRelationShipMode;
import com.atlassian.crowd.openid.server.model.security.AddressRestriction;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.google.common.collect.ImmutableList;

import org.apache.commons.lang3.StringUtils;

import static com.google.common.base.Preconditions.checkNotNull;

public class WhiteAndBlacklist extends BaseAction
{
    TrustRelationShipMode trustRelationShipMode;
    long trustType;
    String address;
    List trustTypes;
    List addresses;

    public String doDefault() throws Exception
    {
        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate() throws OpenIDPropertyManagerException
    {
        openIDPropertyManager.setTrustRelationShipMode(new TrustRelationShipMode(trustType));

        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doAddAddress() throws OpenIDPropertyManagerException, UnknownHostException
    {
        if (StringUtils.isBlank(address))
        {
            addFieldError("address", getText("trusts.address.error", ImmutableList.of(address)));
            return INPUT;
        }

        try
        {
            InetAddress.getByName(checkNotNull(address));
        }
        catch (UnknownHostException e)
        {
            addFieldError("address", getText("trusts.address.error", ImmutableList.of(e.getMessage())));
            return INPUT;
        }

        AddressRestriction addressRestriction = new AddressRestriction();

        addressRestriction.setAddress(address);

        siteManager.addRPAddressRestriction(addressRestriction);

        return SUCCESS;
    }

    @RequireSecurityToken(true)
    public String doRemoveAddress() throws OpenIDPropertyManagerException
    {
        siteManager.removeRPAddressRestriction(address);

        return SUCCESS;
    }


    public long getTrustType() throws OpenIDPropertyManagerException
    {
        if (trustRelationShipMode == null)
        {
            TrustRelationShipMode trustMode = openIDPropertyManager.getTrustRelationShipMode();

            trustType = trustMode.getCode();
        }

        return trustType;
    }

    public void setTrustType(long trustType)
    {
        this.trustType = trustType;
    }


    public void setAddress(String address)
    {
        this.address = address;
    }

    public List getTrustTypes()
    {
        if (trustTypes == null)
        {
            trustTypes = new ArrayList(3);

            trustTypes.add(new PairType(new Long(TrustRelationShipMode.NONE_CODE), getText("none.label")));
            trustTypes.add(new PairType(new Long(TrustRelationShipMode.BLACKLIST_CODE), getText("blacklist.label")));
            trustTypes.add(new PairType(new Long(TrustRelationShipMode.WHITELIST_CODE), getText("whitelist.label")));
        }

        return trustTypes;
    }

    public List getAddresses()
    {
        if (addresses == null)
        {
            addresses = siteManager.findAllRPAddressRestrictions();
        }

        return addresses;
    }
}
