package com.atlassian.crowd.openid.server.manager.site;

import com.atlassian.crowd.openid.server.model.approval.SiteApproval;
import com.atlassian.crowd.openid.server.model.security.AddressRestriction;
import com.atlassian.crowd.openid.server.model.site.Site;
import com.atlassian.crowd.openid.server.model.user.User;

import java.net.URL;
import java.util.List;

public interface SiteManager
{
    /**
     * Gets the SiteApproval of a user for a URL.
     * Returns null if user does not have SiteApproval for the URL.
     *
     * @param user user to search in.
     * @param url URL of Site in SiteApproval.
     * @return SiteApproval or null if none exists.
     */
    SiteApproval getSiteApproval(User user, String url);

    /**
     * Generate approval if this site is configured in the server's white list,
     * otherwise return <code>null</code>.
     *
     * @param user
     * @param url
     * @return SiteApproval or <code>null</code>
     */
    SiteApproval createApprovalFromWhitelist(User user, String url);

    /**
     * Gets the Site object corresponding to the URL or
     * creates a Site object corresponding to the URL.
     *
     * @param url URL of the Site.
     * @return Site.
     */
    Site getSite(String url);

    /**
     * Updates or creates site approval for a particular
     * site (URL) associated with a user and a profile of
     * that user.
     *
     * @param user user to associate to.
     * @param url URL of site.
     * @param profileID default profile to use for attribute information when interacting with site.
     * @param alwaysAllow true if authentication with this site is always allowed.
     * @throws com.atlassian.crowd.openid.server.manager.site.SiteManagerException thrown if profile with given profileID does not exist OR user does not own profile.
     * @return SiteApproval which just got added or updated.
     */
    SiteApproval setSiteApproval(User user, String url, long profileID, boolean alwaysAllow) throws SiteManagerException;

    /**
     * Returns all the sites marked 'always allow' for a user.
     *
     * @param user user to search for.
     * @return List<SiteApproval>
     */
    List getAllAlwaysAllowSites(User user);

    /**
     * Updates which sites are always allowed to authenticate.
     *
     * @param user user to update.
     * @param urls list of URLS of sites.
     * @param profileIDs corresponding list of default profile IDs for the sites.
     * @throws com.atlassian.crowd.openid.server.manager.site.SiteManagerException thrown if URL/ProfileID lists are null or not the same size OR profile with given profileID does not exist OR user does not own profile.
     */
    void updateAlwaysAllowApprovals(User user, List urls, List profileIDs) throws SiteManagerException;

    /**
     * Finds all of the whitelist or blacklist server approvals.
     *
     * @see com.atlassian.crowd.openid.server.manager.property.OpenIDPropertyManager#getTrustRelationShipMode()
     * @return The list of whitelist or blacklist mode to enforce.
     */
    List findAllRPAddressRestrictions();

    void addRPAddressRestriction(AddressRestriction restriction);

    void removeAllRPAddressRestrictions();

    void removeRPAddressRestriction(String address);

    /**
     * Determines is a URL is allowed to authenticate.
     *
     * 1. on blacklist: disallow.
     * 2. on whitelist: allow.
     * 3. otherwise: allow.
     *
     * @param url url of the site.
     * @return true if site is allowed to authenticate, false otherwise.
     */
    boolean isSiteAllowedToAuthenticate(URL url);

    /**
     * Removes an allow always approval from the database.
     * @param approval The site approval to remove.
     */
    void removeSiteApproval(SiteApproval approval);
}
