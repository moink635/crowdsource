package com.atlassian.crowd.openid.server.manager.property;

import java.io.Serializable;

public class TrustRelationShipMode implements Serializable
{
    // TODO replace this with an enumerated type.
    private long code;

    public static TrustRelationShipMode NONE = new TrustRelationShipMode(TrustRelationShipMode.NONE_CODE);

    public static TrustRelationShipMode WHITELIST = new TrustRelationShipMode(TrustRelationShipMode.WHITELIST_CODE);

    public static TrustRelationShipMode BLACKLIST = new TrustRelationShipMode(TrustRelationShipMode.BLACKLIST_CODE);

    /**
     * The persistance code for an unknown directory implementation.
     */
    public static final long NONE_CODE = 0;

    /**
     * The persistance code for an internal directory implementation.
     */
    public static final long WHITELIST_CODE = 1;

    /**
     * The persistance code for an LDAP directory implementation.
     */
    public static final long BLACKLIST_CODE = 2;


    /**
     * Default constructor.
     */
    public TrustRelationShipMode()
    {
    }

    /**
     * Creates a directory type with a known code.
     *
     * @param code The directory code.
     */
    public TrustRelationShipMode(long code)
    {
        this.code = code;
    }


    /**
     * Gets the current code for the directory type.
     *
     * @return The code.
     */
    public long getCode()
    {
        return code;
    }

    /**
     * Sets the current code for the directory type.
     *
     * @param code The code.
     */
    public void setCode(long code)
    {
        this.code = code;
    }

    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (!(o instanceof TrustRelationShipMode)) return false;

        final TrustRelationShipMode directoryType = (TrustRelationShipMode) o;

        return code == directoryType.code;

    }

    public int hashCode()
    {
        return (int) (code ^ (code >>> 32));
    }
}
