package com.atlassian.crowd.openid.server.action.secure.profile;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.openid.server.action.BaseAction;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import com.opensymphony.webwork.ServletActionContext;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ChangePassword extends BaseAction
{
    private static final Logger logger = LoggerFactory.getLogger(ChangePassword.class);

    private String originalPassword;
    private String password;
    private String confirmPassword;

    public String doDefault() throws Exception
    {
        return INPUT;
    }

    @RequireSecurityToken(true)
    public String doUpdate() throws Exception
    {

        // check input to be there
        doValidation();

        if (hasErrors() || hasActionErrors())
        {
            return INPUT;
        }

        // do they know their original password?
        try {
            UserAuthenticationContext userAuthenticationContext =
                    getHttpAuthenticator()
                            .getPrincipalAuthenticationContext(ServletActionContext.getRequest(),
                            ServletActionContext.getResponse(), getRemotePrincipal().getName(), originalPassword);

            getSecurityServerClient().authenticatePrincipal(userAuthenticationContext);

        } catch (Exception e)
        {
            logger.debug(e.getMessage(), e);

            addFieldError("originalPassword", getText("password.invalid"));

            return INPUT;
        }

        // update their password
        try
        {
            PasswordCredential passwordCredential = new PasswordCredential(password);

            getSecurityServerClient().updatePrincipalCredential(getRemotePrincipal().getName(), passwordCredential);

            addActionMessage(ALERT_BLUE, getText("passwordupdate.message"));

            return SUCCESS;
        }
        catch (Exception e)
        {
            logger.error("Failed to update password credential", e);

            addActionError(getText("passwordupdate.error.message"));
        }

        return INPUT;
    }

    protected void doValidation()
    {
        if (StringUtils.isEmpty(originalPassword))
        {
            addFieldError("originalPassword", getText("password.invalid"));
        }
        else
        {
            if (StringUtils.isEmpty(password) || StringUtils.isEmpty(confirmPassword))
            {
                addFieldError("password", getText("passwordempty.invalid"));
            }
            else if (!StringUtils.equals(password, confirmPassword))
            {
                addFieldError("password", getText("passworddonotmatch.invalid"));
            }
        }
    }

    public String getConfirmPassword()
    {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword)
    {
        this.confirmPassword = confirmPassword;
    }

    public String getOriginalPassword()
    {
        return originalPassword;
    }

    public void setOriginalPassword(String originalPassword)
    {
        this.originalPassword = originalPassword;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }
}
