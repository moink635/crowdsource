package com.atlassian.crowd.openid.server.manager.user;

import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.openid.server.manager.profile.ProfileManager;
import com.atlassian.crowd.openid.server.manager.profile.ProfileManagerException;
import com.atlassian.crowd.openid.server.manager.property.OpenIDPropertyManager;
import com.atlassian.crowd.openid.server.model.profile.Profile;
import com.atlassian.crowd.openid.server.model.record.AuthRecordDAO;
import com.atlassian.crowd.openid.server.model.user.User;
import com.atlassian.crowd.openid.server.model.user.UserDAO;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.orm.ObjectRetrievalFailureException;

import java.util.List;
import java.util.Locale;

public class UserManagerGeneric implements UserManager
{
    private static final Logger logger = LoggerFactory.getLogger(UserManagerGeneric.class);

    // spring-injected
    private ProfileManager profileManager;
    private UserDAO userDAO;
    private AuthRecordDAO authRecordDAO;
    private OpenIDPropertyManager openIDPropertyManager;
    private SecurityServerClient securityServerClient;

    /**
     * Retrieves or creates a User from the database matching
     * the name of the supplied SOAPPrincipal.
     *
     * @param principal the SOAP principal corresponding to the user.
     * @param locale the Locale of the user (to get country/language information) if default profile needs to get created.
     * @return user corresponding to principal (either retrieved or created).
     * @throws UserManagerException error creating a default profile for a new user.
     *
     */
    public User getUser(SOAPPrincipal principal, Locale locale) throws UserManagerException
    {
        User user;
        try
        {
            // try fetching user from database
            user = userDAO.findByUsername(principal.getName());
        }
        catch (ObjectRetrievalFailureException e)
        {
            // create a user as the user doesn't exist in our database (eg. first-time user)
            user = new User(principal.getName());

            // save the new user
            userDAO.save(user);

            // create a new profile
            Profile defaultProfile;
            try
            {
                defaultProfile = profileManager.addNewProfile(user, principal, locale, "My Profile");
            }
            catch (ProfileManagerException e1)
            {
                throw new UserManagerException("Could not create new profile for new user "+user.getUsername());
            }

            // make the profile default for the user
            user.setDefaultProfile(defaultProfile);
            userDAO.update(user);
        }

        return user;
    }

    public boolean isAdministrator(String username)
    {
        try
        {
            String adminGroup = openIDPropertyManager.getAdminGroup();

            return securityServerClient.isGroupMember(adminGroup, username);
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public void setPropertyManager(OpenIDPropertyManager openIDPropertyManager)
    {
        this.openIDPropertyManager = openIDPropertyManager;
    }

    public List getAuthRecords(User user, int startIndex, int maxResults)
    {
        return authRecordDAO.findRecords(user, startIndex, maxResults);
    }

    public int getTotalAuthRecords(User user)
    {
        return authRecordDAO.findTotalRecords(user);
    }

    public UserDAO getUserDAO()
    {
        return userDAO;
    }

    public void setUserDAO(UserDAO userDAO)
    {
        this.userDAO = userDAO;
    }

    public ProfileManager getProfileManager()
    {
        return profileManager;
    }

    public void setProfileManager(ProfileManager profileManager)
    {
        this.profileManager = profileManager;
    }

    public AuthRecordDAO getAuthRecordDAO()
    {
        return authRecordDAO;
    }

    public void setAuthRecordDAO(AuthRecordDAO authRecordDAO)
    {
        this.authRecordDAO = authRecordDAO;
    }

    public SecurityServerClient getSecurityServerClient()
    {
        return securityServerClient;
    }

    public void setSecurityServerClient(SecurityServerClient securityServerClient)
    {
        this.securityServerClient = securityServerClient;
    }
}
