package com.atlassian.crowd.openid.server.model.user;

import java.util.List;

import com.atlassian.crowd.openid.server.model.EntityObjectDAOHibernate;

import org.springframework.orm.ObjectRetrievalFailureException;

public class UserDAOHibernate extends EntityObjectDAOHibernate implements UserDAO
{
    public Class getPersistentClass()
    {
        return User.class;
    }

    public User findByUsername(String username) throws ObjectRetrievalFailureException
    {
        List<User> users = currentSession().createQuery("from " + User.class.getName() + " user where user.username = :username")
                .setParameter("username", username)
                .list();

        if (users == null || users.isEmpty())
        {
            // We didn't find a user, so throw an exception
            throw new ObjectRetrievalFailureException(User.class, username);
        }
        else
        {
           return (User) users.get(0);
        }
    }
}
