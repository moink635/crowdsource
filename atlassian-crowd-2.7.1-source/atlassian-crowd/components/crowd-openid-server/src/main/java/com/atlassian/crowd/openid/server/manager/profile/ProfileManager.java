package com.atlassian.crowd.openid.server.manager.profile;

import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.openid.server.model.profile.Profile;
import com.atlassian.crowd.openid.server.model.user.User;

import java.util.Locale;
import java.util.Map;

public interface ProfileManager
{

    /**
     * Creates and adds a new profile to a given user. Default profile
     * data is obtained from the SOAPPrincipal/Locale. The following mapping
     * is used:
     *
     * 1. fullname = FirstName + " " + LastName
     * 2. nickname = Username
     * 3. email = Email
     * 4. country = Locale.getCountry()
     * 5. language = Locale.getLanguage()
     *
     * @param user the user object to add the new profile to.
     * @param principal the SOAPPrincipal which contains basic attribute data.
     * @param locale the Locale of the user (to get country/language information).
     * @param profileName the name of the new profile.
     * @return new profile.
     * @throws ProfileAlreadyExistsException if the user already has a profile with the given profile name.
     *
     */
    public Profile addNewProfile(User user, SOAPPrincipal principal, Locale locale, String profileName) throws ProfileAlreadyExistsException;

    /**
     * Adds a new profile based on attribute name/value pairs.
     *
     * @param user user that owns the profile.
     * @param profileName the name of the new profile.
     * @param attributes Map<String attributeName, String attributeValue>
     * @throws com.atlassian.crowd.openid.server.manager.profile.ProfileAlreadyExistsException if the user already has a profile with the given profile name.
     * @return newly created profile.
     */
    Profile addNewPopulatedProfile(User user, String profileName, Map attributes) throws ProfileAlreadyExistsException;

    /**
     * Updates a user's profile given a map of attributes. The
    * user must own the profile referenced by the profileID. The
    * map of attribtue name-values must be non-null.
    * Any attribute names (keys) which are stored in the
    * profile but not the attribute map, will be deleted.
    * All other attribute names will be updated or added.
    *
    * @param user user that owns the profile.
    * @param profileID profile ID of the profile to update.
    * @param attributes Map<String attributeName, String attributeValue>
    * @throws com.atlassian.crowd.openid.server.manager.profile.ProfileDoesNotExistException if profile with requested profileID does not exist.
    * @throws com.atlassian.crowd.openid.server.manager.profile.ProfileAccessViolationException if the user does not own the profile with the requested profileID.
    */
    void updateProfile(User user, long profileID, Map attributes) throws ProfileDoesNotExistException, ProfileAccessViolationException;

    /**
     * Changes the default profile of a user.
     *
     * @param user user that owns the profile.
     * @param profileID profile ID of the profile to set as default.
     * @throws com.atlassian.crowd.openid.server.manager.profile.ProfileDoesNotExistException if profile with requested profileID does not exist.
     * @throws com.atlassian.crowd.openid.server.manager.profile.ProfileAccessViolationException if the user does not own the profile with the requested profileID.
     */
    void makeDefaultProfile(User user, long profileID) throws ProfileDoesNotExistException, ProfileAccessViolationException;

    /**
     * Deletes a the given profile from a user.
     *
     * Note: a user is not allowed to delete their default profile.
     *
     * @param user user that owns the profile.
     * @param profileID profile ID of the profile to update.
     * @throws com.atlassian.crowd.openid.server.manager.profile.ProfileDoesNotExistException if profile with requested profileID does not exist.
     * @throws com.atlassian.crowd.openid.server.manager.profile.ProfileAccessViolationException if the user does not own the profile with the requested profileID.
     * @throws com.atlassian.crowd.openid.server.manager.profile.DefaultProfileDeleteException if the profile requested for deletion is the default profile of the user.
     */
    void deleteProfile(User user, long profileID) throws ProfileDoesNotExistException, ProfileAccessViolationException, DefaultProfileDeleteException;

    /**
     * Rename an existing profile of a user. The new name
     * must not clash with the other existing profile names
     * the user has.
     *
     * @param user user user that owns the profile.
     * @param profileID profileID profile ID of the profile to rename.
     * @param newName new name of the profile.
     * @throws com.atlassian.crowd.openid.server.manager.profile.ProfileDoesNotExistException if profile with requested profileID does not exist.
     * @throws com.atlassian.crowd.openid.server.manager.profile.ProfileAccessViolationException if the user does not own the profile with the requested profileID.
     * @throws com.atlassian.crowd.openid.server.manager.profile.ProfileAlreadyExistsException if user already has a profile with the new name.
     */
    void renameProfile(User user, long profileID, String newName) throws ProfileDoesNotExistException, ProfileAccessViolationException, ProfileAlreadyExistsException;

    /**
     * Retrieves the requested profile from a user.
     *
     * @param user user that owns the profile.
     * @param profileID profile ID of the profile to retrieve.
     * @return profile corresponding to profileID owned by user.
     * @throws com.atlassian.crowd.openid.server.manager.profile.ProfileAccessViolationException profile with given profileID exists but is not owned by the user.
     * @throws com.atlassian.crowd.openid.server.manager.profile.ProfileDoesNotExistException profile with given profileID does not exist.
     */
    Profile getProfile(User user, long profileID) throws ProfileDoesNotExistException, ProfileAccessViolationException;
}
