package com.atlassian.crowd.openid.server.action.secure.profile;

import com.atlassian.crowd.openid.server.action.BaseAction;
import com.atlassian.crowd.openid.server.manager.profile.DefaultProfileDeleteException;
import com.atlassian.crowd.openid.server.manager.profile.ProfileAccessViolationException;
import com.atlassian.crowd.openid.server.manager.profile.ProfileAlreadyExistsException;
import com.atlassian.crowd.openid.server.manager.profile.ProfileDoesNotExistException;
import com.atlassian.crowd.openid.server.model.profile.Profile;
import com.atlassian.crowd.openid.server.model.profile.SREGAttributes;
import com.atlassian.crowd.openid.server.model.profile.attribute.Attribute;
import com.atlassian.crowd.openid.server.model.user.User;
import com.atlassian.crowd.openid.server.util.ProfileAttributesHelper;
import com.atlassian.crowd.util.SOAPPrincipalHelper;
import com.atlassian.crowd.xwork.RequireSecurityToken;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class EditProfiles extends BaseAction
{
    private static final Logger logger = LoggerFactory.getLogger(EditProfiles.class);

    private static final long NEW_PROFILE_ID = -1l;
    private static final long DEFAULT_PROFILE_ID = 0;

    private ProfileAttributesHelper profileAttributesHelper;
    private SOAPPrincipalHelper principalHelper;

    // input
    private long profileID = DEFAULT_PROFILE_ID;

    // input/output
    private String profileName;
    private String nickname;
    private String fullName;
    private String email;
    private int dobDay;
    private int dobMonth;
    private int dobYear;
    private String gender;
    private String postcode;
    private String country;
    private String timezone;
    private String language;

    // output
    private User user;
    private boolean defaultProfile;
    private Profile currentProfile;
    private Map<Long, String> profiles = Collections.<Long, String>emptyMap();

    public String execute() throws Exception
    {
        // retrieve the user
        try
        {
            user = userManager.getUser(this.getRemotePrincipal(), getLocale());

            // get profiles for user, populate Map<Long id, String name> profiles
            profiles = new LinkedHashMap();
            profiles.put(new Long(NEW_PROFILE_ID), getText("profiles.newprofile.text"));
            for (Iterator iterator = user.getProfiles().iterator(); iterator.hasNext();)
            {
                Profile profile = (Profile) iterator.next();
                if (user.getDefaultProfile().equals(profile))
                {
                    StringBuffer defaultProfileName = new StringBuffer(user.getDefaultProfile().getName());
                    defaultProfileName.append(" ");
                    defaultProfileName.append(getText("profiles.default.append"));
                    profiles.put(profile.getId(), defaultProfileName.toString());
                }
                else
                {
                    profiles.put(profile.getId(), profile.getName());
                }
            }

            // work what profile to display
            if (profileID == NEW_PROFILE_ID)
            {
                // build a new profile with a blank name and non-db id
                currentProfile = new Profile(user, "");
                currentProfile.setId(new Long(NEW_PROFILE_ID));

                // add nickname, fullname, email from principal
                currentProfile.addAttribute(new Attribute(SREGAttributes.NICKNAME, getRemotePrincipal().getName()));
                currentProfile.addAttribute(new Attribute(SREGAttributes.FULLNAME, principalHelper.getFullName(getRemotePrincipal())));
                currentProfile.addAttribute(new Attribute(SREGAttributes.EMAIL, principalHelper.getEmail(getRemotePrincipal())));

                // add language, country from locale
                currentProfile.addAttribute(new Attribute(SREGAttributes.LANGUAGE, getLocale().getLanguage()));
                currentProfile.addAttribute(new Attribute(SREGAttributes.COUNTRY, getLocale().getCountry()));

            }
            else if (profileID == DEFAULT_PROFILE_ID)
            {
                currentProfile = user.getDefaultProfile();
            }
            else
            {
                currentProfile = profileManager.getProfile(user, profileID);
            }

            // get the current profile name
            profileName = currentProfile.getName();

            // get the attributes for the current profile in a convenient form
            SREGAttributes attributes = new SREGAttributes(currentProfile);
            nickname = attributes.getNickname();
            fullName = attributes.getFullname();
            email = attributes.getEmail();
            dobDay = attributes.getDobDay();
            dobMonth = attributes.getDobMonth();
            dobYear = attributes.getDobYear();
            gender = attributes.getGender();
            postcode = attributes.getPostcode();
            country = attributes.getCountry();
            timezone = attributes.getTimezone();
            language = attributes.getLanguage();

            // work out if current profile is the default
            defaultProfile = currentProfile.equals(user.getDefaultProfile());
        }
        catch (ProfileDoesNotExistException e)
        {
            addActionError(getText("exception.profile.does.not.exist.exception"));
        }
        catch (ProfileAccessViolationException e)
        {
            addActionError(getText("exception.profile.access.violation.exception"));
        }

        return INPUT;

    }

    @RequireSecurityToken(true)
    public String doMakeDefault() throws Exception
    {
        // retrieve the user
        try
        {
            user = userManager.getUser(this.getRemotePrincipal(), getLocale());
            profileManager.makeDefaultProfile(user, profileID);
        }
        catch (ProfileDoesNotExistException e)
        {
            addActionError(getText("exception.profile.does.not.exist.exception"));
        }
        catch (ProfileAccessViolationException e)
        {
            addActionError(getText("exception.profile.access.violation.exception"));
        }

        return execute();
    }

    @RequireSecurityToken(true)
    public String doUpdate() throws Exception
    {

        // retrieve the user
        try
        {
            user = userManager.getUser(this.getRemotePrincipal(), getLocale());

            // make appropriate SREG attributes from user input
            SREGAttributes sreg = new SREGAttributes();
            sreg.setNickname(nickname);
            sreg.setFullname(fullName);
            sreg.setEmail(email);
            sreg.setDob(dobDay, dobMonth, dobYear);
            sreg.setGender(gender);
            sreg.setPostcode(postcode);
            sreg.setCountry(country);
            sreg.setTimezone(timezone);
            sreg.setLanguage(language);

            // create a new profile if "New Profile" was selected
            if (profileID == NEW_PROFILE_ID)
            {
                if (StringUtils.isBlank(profileName))
                {
                    addFieldError("profileName", getText("profiles.nameempty.error"));

                    return execute();
                }

                Profile newProfile = profileManager.addNewPopulatedProfile(user, profileName, sreg.getAttributes());
                profileID = newProfile.getId().longValue();
            }

            // otherwise update the existing profile
            else
            {
                profileManager.updateProfile(user, profileID, sreg.getAttributes());
            }

            addActionMessage(ALERT_BLUE, getText("profiles.updated.message"));
        }
        catch (ProfileAlreadyExistsException e)
        {
            addFieldError("profileName", getText("exception.profile.already.exists"));
        }
        catch (ProfileDoesNotExistException e)
        {
            addActionError(getText("exception.profile.does.not.exist.exception"));
        }
        catch (ProfileAccessViolationException e)
        {
            addActionError(getText("exception.profile.access.violation.exception"));
        }

        // show the normal screen again
        return execute();
    }

    @RequireSecurityToken(true)
    public String doDelete() throws Exception
    {

        // retrieve the user
        try
        {
            user = userManager.getUser(this.getRemotePrincipal(), getLocale());

            // delete the profile
            profileManager.deleteProfile(user, profileID);

            // load up the default profile
            profileID = user.getDefaultProfile().getId().longValue();

            addActionMessage(ALERT_BLUE, getText("profiles.deleted.message"));
        }
        catch (DefaultProfileDeleteException e)
        {
            addActionError(getText("exception.default.profile.delete"));
        }
        catch (ProfileDoesNotExistException e)
        {
            addActionError(getText("exception.profile.does.not.exist.exception"));
        }
        catch (ProfileAccessViolationException e)
        {
            addActionError(getText("exception.profile.access.violation.exception"));
        }

        return execute();
    }


    public User getUser()
    {
        return user;
    }

    public void setUser(User user)
    {
        this.user = user;
    }

    public long getProfileID()
    {
        return profileID;
    }

    public void setProfileID(long profileID)
    {
        this.profileID = profileID;
    }

    public Profile getCurrentProfile()
    {
        return currentProfile;
    }

    public void setCurrentProfile(Profile currentProfile)
    {
        this.currentProfile = currentProfile;
    }

    public Map getProfiles()
    {
        return profiles;
    }

    public void setProfiles(Map profiles)
    {
        this.profiles = profiles;
    }


    public String getProfileName()
    {
        return profileName;
    }

    public void setProfileName(String profileName)
    {
        this.profileName = profileName;
    }

    public ProfileAttributesHelper getProfileAttributesHelper()
    {
        return profileAttributesHelper;
    }

    public void setProfileAttributesHelper(ProfileAttributesHelper profileAttributesHelper)
    {
        this.profileAttributesHelper = profileAttributesHelper;
    }

    public int getDobDay()
    {
        return dobDay;
    }

    public void setDobDay(int dobDay)
    {
        this.dobDay = dobDay;
    }

    public int getDobMonth()
    {
        return dobMonth;
    }

    public void setDobMonth(int dobMonth)
    {
        this.dobMonth = dobMonth;
    }

    public int getDobYear()
    {
        return dobYear;
    }

    public void setDobYear(int dobYear)
    {
        this.dobYear = dobYear;
    }

    public String getNickname()
    {
        return nickname;
    }

    public void setNickname(String nickname)
    {
        this.nickname = nickname;
    }

    public String getFullName()
    {
        return fullName;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getGender()
    {
        return gender;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public String getPostcode()
    {
        return postcode;
    }

    public void setPostcode(String postcode)
    {
        this.postcode = postcode;
    }

    public String getCountry()
    {
        return country;
    }

    public void setCountry(String country)
    {
        this.country = country;
    }

    public String getTimezone()
    {
        return timezone;
    }

    public void setTimezone(String timezone)
    {
        this.timezone = timezone;
    }

    public String getLanguage()
    {
        return language;
    }

    public void setLanguage(String language)
    {
        this.language = language;
    }

    public boolean isDefaultProfile()
    {
        return defaultProfile;
    }

    public void setDefaultProfile(boolean defaultProfile)
    {
        this.defaultProfile = defaultProfile;
    }

    public SOAPPrincipalHelper getPrincipalHelper()
    {
        return principalHelper;
    }

    public void setPrincipalHelper(SOAPPrincipalHelper principalHelper)
    {
        this.principalHelper = principalHelper;
    }
}
