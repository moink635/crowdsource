package com.atlassian.crowd.openid.server.manager.user;

import com.atlassian.crowd.integration.soap.SOAPPrincipal;
import com.atlassian.crowd.openid.server.model.user.User;

import java.util.List;
import java.util.Locale;

public interface UserManager
{
    /**
     * Retrieves or creates a User from the database matching
     * the name of the supplied SOAPPrincipal.
     *
     * @param principal the SOAP principal corresponding to the user.
     * @param locale the Locale of the user (to get country/language information) if default profile needs to get created.
     * @return user corresponding to principal (either retrieved or created).
     * @throws UserManagerException error creating a default profile for a new user.
     */
    public User getUser(SOAPPrincipal principal, Locale locale) throws UserManagerException;

    /**
     * Retrieves a subset of the authentication history for a given user,
     * in reverse chronological order.
     *
     * @param user owner of authentication records.
     * @param startIndex start index.
     * @param maxResults maximum number of results.
     * @return List<AuthRecord>.
     */
    List getAuthRecords(User user, int startIndex, int maxResults);

    /**
     * Retrieves the total number of authentication records for a given user.
     *
     * @param user owner of the authentication records.
     * @return number of authentication records.
     */
    int getTotalAuthRecords(User user);

    boolean isAdministrator(String username);
}
