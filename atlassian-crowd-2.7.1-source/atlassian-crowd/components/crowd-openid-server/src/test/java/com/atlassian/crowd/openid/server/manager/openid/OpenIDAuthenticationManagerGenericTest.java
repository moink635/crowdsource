package com.atlassian.crowd.openid.server.manager.openid;

import com.atlassian.crowd.openid.server.manager.site.SiteManager;
import com.atlassian.crowd.openid.server.model.approval.SiteApproval;
import com.atlassian.crowd.openid.server.model.profile.Profile;
import com.atlassian.crowd.openid.server.model.record.AuthRecordDAO;
import com.atlassian.crowd.openid.server.model.user.User;
import com.atlassian.crowd.openid.server.provider.OpenIDAuthRequest;
import com.atlassian.crowd.openid.server.provider.OpenIDAuthResponse;

import org.junit.Test;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class OpenIDAuthenticationManagerGenericTest
{
    private static final String BASE_URL = "http://localhost/";

    @Test
    public void getNameExtractStringsFromUris() throws IdentifierViolationException
    {
        OpenIDAuthRequest authReq = mock(OpenIDAuthRequest.class);
        when(authReq.getIdentifier()).thenReturn("http://localhost/users/username");
        when(authReq.getBaseUrl()).thenReturn(BASE_URL);

        assertEquals("username",
                OpenIDAuthenticationManagerGeneric.getUserName(authReq));
    }

    @Test
    public void correctlyEscapedSlashesArePermittedInIdentifiers() throws IdentifierViolationException
    {
        OpenIDAuthRequest authReq = mock(OpenIDAuthRequest.class);
        when(authReq.getIdentifier()).thenReturn("http://localhost/users/user%2Fwith%2Fslash");
        when(authReq.getBaseUrl()).thenReturn(BASE_URL);

        assertEquals("user/with/slash",
                OpenIDAuthenticationManagerGeneric.getUserName(authReq));
    }

    @Test
    public void slashesAreNotIgnoredInIdentifiers() throws IdentifierViolationException
    {
        OpenIDAuthRequest authReq = mock(OpenIDAuthRequest.class);
        when(authReq.getIdentifier()).thenReturn("http://localhost/users/not/the/admin");
        when(authReq.getBaseUrl()).thenReturn(BASE_URL);

        assertEquals("not/the/admin",
                OpenIDAuthenticationManagerGeneric.getUserName(authReq));
    }

    @Test
    public void identifiersAreDecodedAsPathComponentsWithPlusSigns() throws IdentifierViolationException
    {
        OpenIDAuthRequest authReq = mock(OpenIDAuthRequest.class);
        when(authReq.getIdentifier()).thenReturn("http://localhost/users/user+tag");
        when(authReq.getBaseUrl()).thenReturn(BASE_URL);

        assertEquals("user+tag",
                OpenIDAuthenticationManagerGeneric.getUserName(authReq));
    }

    @Test
    public void identifiersAreDecodedAsPathComponentsWithPercentEncoding() throws IdentifierViolationException
    {
        OpenIDAuthRequest authReq = mock(OpenIDAuthRequest.class);
        when(authReq.getIdentifier()).thenReturn("http://localhost/users/user%20name");
        when(authReq.getBaseUrl()).thenReturn(BASE_URL);

        assertEquals("user name",
                OpenIDAuthenticationManagerGeneric.getUserName(authReq));
    }

    @Test(expected = IdentifierViolationException.class)
    public void identifiersThatDoNotMatchTheServerAreRejected() throws IdentifierViolationException
    {
        OpenIDAuthRequest authReq = mock(OpenIDAuthRequest.class);
        when(authReq.getBaseUrl()).thenReturn(BASE_URL);
        when(authReq.getIdentifier()).thenReturn("http://some-other-host/users/not/the/admin");
        OpenIDAuthenticationManagerGeneric.getUserName(authReq);
    }

    @Test
    public void identifierIsSubstitutedWhenIdentitySelectIsRequested()
    {
        OpenIDAuthRequest authReq = mock(OpenIDAuthRequest.class);
        when(authReq.getBaseUrl()).thenReturn("http://example.com/");
        when(authReq.getIdentifier()).thenReturn("http://specs.openid.net/auth/2.0/identifier_select");

        User user = mock(User.class);
        when(user.getUsername()).thenReturn("username");

        String identifier = OpenIDAuthenticationManagerGeneric.effectiveIdentifier(authReq, user);

        assertEquals("http://example.com/users/username", identifier);
    }

    @Test
    public void identifierIsUsedDirectlyWhenAnIdentityIsSpecified()
    {
        OpenIDAuthRequest authReq = mock(OpenIDAuthRequest.class);
        when(authReq.getBaseUrl()).thenReturn("http://example.com/");
        when(authReq.getIdentifier()).thenReturn("http://example.com/users/specified-username");

        User user = mock(User.class);

        String identifier = OpenIDAuthenticationManagerGeneric.effectiveIdentifier(authReq, user);

        assertEquals("http://example.com/users/specified-username", identifier);
    }

    @Test(expected = IllegalStateException.class)
    public void identitySelectFailsWithExceptionIfNoUserIsLoggedIn()
    {
        OpenIDAuthRequest authReq = mock(OpenIDAuthRequest.class);
        when(authReq.getBaseUrl()).thenReturn("http://example.com/");
        when(authReq.getIdentifier()).thenReturn("http://specs.openid.net/auth/2.0/identifier_select");

        OpenIDAuthenticationManagerGeneric.effectiveIdentifier(authReq, null);
    }

    @Test
    public void autoAllowRequestUpgradesApprovalFromUnknownUsingWhitelist()
    {
        User user = mock(User.class);
        String identifier = "http://localhost/users/user";
        String url = "http://example.com/";

        /* Mocks */
        OpenIDAuthRequest authReq = mock(OpenIDAuthRequest.class);
        when(authReq.getRealm()).thenReturn(url);
        when(authReq.getIdentifier()).thenReturn(identifier);

        SiteManager siteManager = mock(SiteManager.class);
        when(siteManager.getSiteApproval(user, url)).thenReturn(null);

        SiteApproval siteApprovalFromWhitelist = mock(SiteApproval.class);
        when(siteApprovalFromWhitelist.isAlwaysAllow()).thenReturn(true);
        when(siteApprovalFromWhitelist.getProfile()).thenReturn(mock(Profile.class));

        when(siteManager.createApprovalFromWhitelist(user, url)).thenReturn(siteApprovalFromWhitelist);

        /* Instance */
        OpenIDAuthenticationManagerGeneric authManager = new OpenIDAuthenticationManagerGeneric();
        authManager.setSiteManager(siteManager);
        authManager.setAuthRecordDAO(mock(AuthRecordDAO.class));

        /* Test */
        OpenIDAuthResponse resp = authManager.autoAllowRequest(user, authReq);
        assertTrue(resp.isAuthenticated());
        assertEquals(identifier, resp.getIdentifier());

        verify(siteManager).createApprovalFromWhitelist(user, url);
    }

    @Test
    public void autoAllowRequestUpgradesApprovalFromNotAlwaysAllowUsingWhitelist()
    {
        User user = mock(User.class);
        String identifier = "http://localhost/users/user";
        String url = "http://example.com/";

        /* Mocks */
        OpenIDAuthRequest authReq = mock(OpenIDAuthRequest.class);
        when(authReq.getRealm()).thenReturn(url);
        when(authReq.getIdentifier()).thenReturn(identifier);

        SiteApproval siteApproval = mock(SiteApproval.class);
        when(siteApproval.isAlwaysAllow()).thenReturn(false);
        when(siteApproval.getProfile()).thenReturn(mock(Profile.class));

        SiteManager siteManager = mock(SiteManager.class);
        when(siteManager.getSiteApproval(user, url)).thenReturn(siteApproval);

        SiteApproval siteApprovalFromWhitelist = mock(SiteApproval.class);
        when(siteApprovalFromWhitelist.isAlwaysAllow()).thenReturn(true);
        when(siteApprovalFromWhitelist.getProfile()).thenReturn(mock(Profile.class));

        when(siteManager.createApprovalFromWhitelist(user, url)).thenReturn(siteApprovalFromWhitelist);

        /* Instance */
        OpenIDAuthenticationManagerGeneric authManager = new OpenIDAuthenticationManagerGeneric();
        authManager.setSiteManager(siteManager);
        authManager.setAuthRecordDAO(mock(AuthRecordDAO.class));

        /* Test */
        OpenIDAuthResponse resp = authManager.autoAllowRequest(user, authReq);
        assertTrue(resp.isAuthenticated());
        assertEquals(identifier, resp.getIdentifier());

        verify(siteManager).createApprovalFromWhitelist(user, url);
    }

    @Test
    public void autoAllowRequestDoesNotConsultWhitelistIfSiteAlreadyAlwaysApproved()
    {
        User user = mock(User.class);
        String identifier = "http://localhost/users/user";
        String url = "http://example.com/";

        /* Mocks */
        OpenIDAuthRequest authReq = mock(OpenIDAuthRequest.class);
        when(authReq.getRealm()).thenReturn(url);
        when(authReq.getIdentifier()).thenReturn(identifier);

        SiteApproval siteApproval = mock(SiteApproval.class);
        when(siteApproval.isAlwaysAllow()).thenReturn(true);
        when(siteApproval.getProfile()).thenReturn(mock(Profile.class));

        SiteManager siteManager = mock(SiteManager.class);
        when(siteManager.getSiteApproval(user, url)).thenReturn(siteApproval);

        /* Instance */
        OpenIDAuthenticationManagerGeneric authManager = new OpenIDAuthenticationManagerGeneric();
        authManager.setSiteManager(siteManager);
        authManager.setAuthRecordDAO(mock(AuthRecordDAO.class));

        /* Test */
        OpenIDAuthResponse resp = authManager.autoAllowRequest(user, authReq);
        assertTrue(resp.isAuthenticated());
        assertEquals("http://localhost/users/user", resp.getIdentifier());

        verify(siteManager, Mockito.never()).createApprovalFromWhitelist(user, url);
    }
}
