package com.atlassian.crowd.openid.server.action;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.openid.server.manager.openid.IdentifierViolationException;
import com.atlassian.crowd.openid.server.manager.property.OpenIDPropertyManager;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.Action;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ViewPublicIdentityTest
{
    private OpenIDPropertyManager propertyManager = mock(OpenIDPropertyManager.class);

    @Test
    public void asciiUrlIsGeneratedFromNonAsciiUsername() throws Exception
    {
        ViewPublicIdentity viewPublicIdentity = new ViewPublicIdentity();
        viewPublicIdentity.setName("john.tøstinógé");
        viewPublicIdentity.setPropertyManager(propertyManager);

        when(propertyManager.getBaseURL()).thenReturn("http://localhost/");

        assertThat(viewPublicIdentity.getIdentifier(), is("http://localhost/users/john.t%C3%B8stin%C3%B3g%C3%A9"));
    }

    @Test
    public void usernameWithSlashIsPercentEncodedInIdentifier() throws Exception
    {
        ViewPublicIdentity viewPublicIdentity = new ViewPublicIdentity();
        viewPublicIdentity.setName("john.somebody/name");
        viewPublicIdentity.setPropertyManager(propertyManager);

        when(propertyManager.getBaseURL()).thenReturn("http://localhost/");

        assertThat(viewPublicIdentity.getIdentifier(), is("http://localhost/users/john.somebody%2Fname"));
    }

    /**
     * Strip a trailing slash from identifiers for CWD-472.
     */
    @Test
    public void getNameStripsASingleTrailingSlash()
    {
        ViewPublicIdentity viewPublicIdentity = new ViewPublicIdentity();

        viewPublicIdentity.setName("name");
        assertEquals("name", viewPublicIdentity.getName());

        viewPublicIdentity.setName("name/");
        assertEquals("name", viewPublicIdentity.getName());

        viewPublicIdentity.setName("name//");
        assertEquals("name/", viewPublicIdentity.getName());
    }

    @Test
    public void usernameWithSpaceIsPercentEncodedInIdentifier() throws Exception
    {
        ViewPublicIdentity viewPublicIdentity = new ViewPublicIdentity();
        viewPublicIdentity.setName("user name");
        viewPublicIdentity.setPropertyManager(propertyManager);

        when(propertyManager.getBaseURL()).thenReturn("http://localhost/");

        assertThat(viewPublicIdentity.getIdentifier(), is("http://localhost/users/user%20name"));
    }

    @Test
    public void usernameWithAtSignIsNotPercentEncodedInIdentifier() throws Exception
    {
        ViewPublicIdentity viewPublicIdentity = new ViewPublicIdentity();
        viewPublicIdentity.setName("user@host");
        viewPublicIdentity.setPropertyManager(propertyManager);

        when(propertyManager.getBaseURL()).thenReturn("http://localhost/");

        assertThat(viewPublicIdentity.getIdentifier(), is("http://localhost/users/user@host"));
    }

    @Test
    public void usernameWithPlusIsNotPercentEncodedInIdentifier() throws Exception
    {
        ViewPublicIdentity viewPublicIdentity = new ViewPublicIdentity();
        viewPublicIdentity.setName("user+name");
        viewPublicIdentity.setPropertyManager(propertyManager);

        when(propertyManager.getBaseURL()).thenReturn("http://localhost/");

        assertThat(viewPublicIdentity.getIdentifier(), is("http://localhost/users/user+name"));
    }

    @Test(expected = IdentifierViolationException.class)
    public void usernameFromDifferentBaseUrlCausesException() throws IdentifierViolationException
    {
        ViewPublicIdentity.nameFromIdentifier("http://example.com/", "http://localhost/users/user");
    }

    @Test
    public void usernameIsReturnedWhenSameBaseUrl() throws IdentifierViolationException
    {
        assertEquals("user",
                ViewPublicIdentity.nameFromIdentifier("http://localhost/", "http://localhost/users/user"));
    }

    @Test
    public void usernameIsDecodedWhenIncludesSpace() throws IdentifierViolationException
    {
        assertEquals("user name",
                ViewPublicIdentity.nameFromIdentifier("http://localhost/", "http://localhost/users/user%20name"));
    }

    @Test
    public void usernameWithPlusIsLeftAsIs() throws IdentifierViolationException
    {
        assertEquals("user+name",
                ViewPublicIdentity.nameFromIdentifier("http://localhost/", "http://localhost/users/user+name"));
    }

    @Test
    public void usernameWithNonAsciiCharactersIsDecoded() throws IdentifierViolationException
    {
        assertEquals("john.tøstinógé",
                ViewPublicIdentity.nameFromIdentifier("http://localhost/", "http://localhost/users/john.t%C3%B8stin%C3%B3g%C3%A9"));
    }

    @Test(expected = IdentifierViolationException.class)
    public void badPercentEncodingCausesAnException() throws IdentifierViolationException
    {
        ViewPublicIdentity.nameFromIdentifier("http://localhost/", "http://localhost/users/a%2x");
    }

    @Test
    public void leadingSlashesInUsernamesAreExtractedFromIdentifiers() throws IdentifierViolationException
    {
        assertEquals("/name",
                ViewPublicIdentity.nameFromIdentifier("http://localhost/", "http://localhost/users//name"));
        assertEquals("/name",
                ViewPublicIdentity.nameFromIdentifier("http://localhost/", "http://localhost/users/%2Fname"));
        assertEquals("/some/thing",
                ViewPublicIdentity.nameFromIdentifier("http://localhost/", "http://localhost/users//some/thing"));
    }

    @Test
    public void pathTraversalIsNotResolvedWhenExtractingUsernamesFromIdentifiers() throws IdentifierViolationException
    {
        assertEquals("name/../name2",
                ViewPublicIdentity.nameFromIdentifier("http://localhost/", "http://localhost/users/name/../name2"));
    }

    @Test
    public void identifierExceptionsAreNotPropagatedByAction() throws Exception
    {
        ViewPublicIdentity vpi = new ViewPublicIdentity();

        HttpServletRequest request = mock(HttpServletRequest.class);

        when(request.getAttribute("javax.servlet.forward.request_uri")).thenReturn("http://localhost/users/a%xb");

        when(request.getContextPath()).thenReturn("http://localhost");

        ServletActionContext.setRequest(request);

        assertEquals(Action.ERROR, vpi.execute());

        assertEquals("An unknown username is reported as the empty string", "", vpi.getName());
    }

    @Test
    public void correctlyParsedIdentifiersReturnSuccess() throws Exception
    {
        ViewPublicIdentity vpi = new ViewPublicIdentity();

        HttpServletRequest request = mock(HttpServletRequest.class);

        when(request.getAttribute("javax.servlet.forward.request_uri")).thenReturn("http://localhost/users/username");

        when(request.getContextPath()).thenReturn("http://localhost");

        ServletActionContext.setRequest(request);

        assertEquals(Action.SUCCESS, vpi.execute());

        assertEquals("username", vpi.getName());
    }
}
