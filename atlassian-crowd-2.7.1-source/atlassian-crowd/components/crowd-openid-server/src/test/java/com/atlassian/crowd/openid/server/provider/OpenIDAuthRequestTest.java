package com.atlassian.crowd.openid.server.provider;

import com.google.common.collect.ImmutableList;

import org.junit.Test;
import org.openid4java.message.Message;
import org.openid4java.message.Parameter;
import org.openid4java.message.ParameterList;
import org.openid4java.message.sreg.SRegResponse;
import org.openid4java.server.RealmVerifier;
import org.openid4java.server.RealmVerifierFactory;

import static org.junit.Assert.assertNotNull;

import static org.junit.Assert.assertNull;

import static org.mockito.Mockito.mock;

import static org.junit.Assert.assertEquals;

public class OpenIDAuthRequestTest
{
    @Test(expected = MalformedOpenIDRequestException.class)
    public void badRequestCausesMalformedRequestException() throws MalformedOpenIDRequestException
    {
        RealmVerifier realmVerifier = new RealmVerifierFactory(null).getRealmVerifierForServer();

        // Malformed because no openid.mode specified
        ParameterList pl = new ParameterList();
        new OpenIDAuthRequest("", pl, realmVerifier);
    }

    private static ParameterList dummyParams()
    {
        ParameterList requestParameters = new ParameterList();
        requestParameters.set(new Parameter("openid.mode", "checkid_immediate"));
        requestParameters.set(new Parameter("openid.ns", Message.OPENID2_NS));
        requestParameters.set(new Parameter("openid.realm", "http://localhost/"));
        requestParameters.set(new Parameter("openid.claimed_id", "http://localhost/op"));
        requestParameters.set(new Parameter("openid.identity", "http://specs.openid.net/auth/2.0/identifier_select"));

        return requestParameters;
    }

    @Test
    public void authRequestDefaultsToSReg10TypeWhenNoSRegRequestIsPresent() throws MalformedOpenIDRequestException
    {
        ParameterList requestParameters = dummyParams();

        RealmVerifier realmVerifier = mock(RealmVerifier.class);

        OpenIDAuthRequest req = new OpenIDAuthRequest(null, requestParameters, realmVerifier);
        assertNull(req.getSregRequest());
        assertEquals(SRegResponse.OPENID_NS_SREG, req.getSRegTypeUri());
    }

    @Test
    public void authRequestUsesSReg10TypeWhenUnnamespacedRequestIsPresent() throws MalformedOpenIDRequestException
    {
        ParameterList requestParameters = dummyParams();

        requestParameters.set(new Parameter("openid.sreg.required", "name"));

        RealmVerifier realmVerifier = mock(RealmVerifier.class);

        OpenIDAuthRequest req = new OpenIDAuthRequest(null, requestParameters, realmVerifier);
        assertNotNull(req.getSregRequest());
        assertEquals(ImmutableList.of("name"), req.getSregRequest().getAttributes(true));
        assertEquals(SRegResponse.OPENID_NS_SREG, req.getSRegTypeUri());
    }

    @Test
    public void authRequestUsesSReg10TypeWhenSpecified() throws MalformedOpenIDRequestException
    {
        ParameterList requestParameters = dummyParams();

        requestParameters.set(new Parameter("openid.ns.sreg", SRegResponse.OPENID_NS_SREG));
        requestParameters.set(new Parameter("openid.sreg.required", "name"));

        RealmVerifier realmVerifier = mock(RealmVerifier.class);

        OpenIDAuthRequest req = new OpenIDAuthRequest(null, requestParameters, realmVerifier);
        assertNotNull(req.getSregRequest());
        assertEquals(ImmutableList.of("name"), req.getSregRequest().getAttributes(true));
        assertEquals(SRegResponse.OPENID_NS_SREG, req.getSRegTypeUri());
    }

    @Test
    public void authRequestUsesSReg11TypeWhenSpecified() throws MalformedOpenIDRequestException
    {
        ParameterList requestParameters = dummyParams();

        requestParameters.set(new Parameter("openid.ns.sreg", SRegResponse.OPENID_NS_SREG11));
        requestParameters.set(new Parameter("openid.sreg.required", "name"));

        RealmVerifier realmVerifier = mock(RealmVerifier.class);

        OpenIDAuthRequest req = new OpenIDAuthRequest(null, requestParameters, realmVerifier);
        assertNotNull(req.getSregRequest());
        assertEquals(ImmutableList.of("name"), req.getSregRequest().getAttributes(true));
        assertEquals(SRegResponse.OPENID_NS_SREG11, req.getSRegTypeUri());
    }

    @Test
    public void authRequestUsesSReg10TypeWhenSRegExtensionIsUsedForSomethingElse() throws MalformedOpenIDRequestException
    {
        ParameterList requestParameters = dummyParams();

        requestParameters.set(new Parameter("openid.ns.sreg", "x:some-other-extension"));
        requestParameters.set(new Parameter("openid.sreg.required", "name"));

        RealmVerifier realmVerifier = mock(RealmVerifier.class);

        OpenIDAuthRequest req = new OpenIDAuthRequest(null, requestParameters, realmVerifier);
        assertNull(req.getSregRequest());
        assertEquals(SRegResponse.OPENID_NS_SREG, req.getSRegTypeUri());
    }
}
