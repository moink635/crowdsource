package com.atlassian.crowd.openid.server.action;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.integration.http.HttpAuthenticator;

import org.junit.Test;

import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BaseActionTest
{
    private static BaseAction createSubjectUnderTest(final boolean isAuthenticated)
    {
        return new BaseAction()
        {
            @Override
            public boolean isAuthenticated()
            {
                return isAuthenticated;
            }
        };
    }

    @Test
    public void testIdentifierIsNullWhenUserIsNotLoggedIn() throws Exception
    {
        BaseAction baseAction = createSubjectUnderTest(false);

        assertNull(baseAction.getIdentifier());
    }

    @Test (expected = InvalidAuthenticationException.class)
    public void testGetRemotePrincipalWhenUserCannotBeFoundByItsToken() throws Exception
    {
        HttpAuthenticator httpAuthenticator = mock(HttpAuthenticator.class);
        when(httpAuthenticator.getPrincipal(any(HttpServletRequest.class))).thenThrow(new InvalidTokenException(""));
        BaseAction baseAction = createSubjectUnderTest(true);
        baseAction.setHttpAuthenticator(httpAuthenticator);

        baseAction.getRemotePrincipal();
    }
}