package com.atlassian.crowd.openid.server.manager.site;

import java.io.File;
import java.io.IOException;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import org.apache.commons.io.FileUtils;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ApprovalWhitelistTest
{
    @Test
    public void returnsEmptyListIfFilesDoesNotExist()
    {
        assertThat(
                new ApprovalWhitelist.FileBackedApprovalWhitelist(new File("no-such-file"), 60000).getApprovedUrls(),
                IsEmptyCollection.empty());
    }

    @Test
    public void newFileIsRead() throws IOException
    {
        File f = File.createTempFile(getClass().getName(), ".urls");
        f.deleteOnExit();
        f.setLastModified(0);

        ApprovalWhitelist.FileBackedApprovalWhitelist aw = new ApprovalWhitelist.FileBackedApprovalWhitelist(f, 60000);
        assertThat(aw.getApprovedUrls(), IsEmptyCollection.empty());

        FileUtils.writeLines(f, ImmutableList.of("first"));
        f.setLastModified(10000);
        aw.checkApprovedUrls();
        assertEquals(ImmutableSet.of("first"), aw.getApprovedUrls());
        assertTrue(aw.isAutomaticallyApproved("first"));

        FileUtils.writeLines(f, ImmutableList.of("second"));
        f.setLastModified(20000);
        aw.checkApprovedUrls();
        assertEquals(ImmutableSet.of("second"), aw.getApprovedUrls());
    }

    @Test
    public void deletingTheFileGivesAnEmptySet() throws IOException
    {
        File f = File.createTempFile(getClass().getName(), ".urls");
        f.deleteOnExit();

        FileUtils.writeLines(f, ImmutableList.of("first"));

        ApprovalWhitelist.FileBackedApprovalWhitelist aw = new ApprovalWhitelist.FileBackedApprovalWhitelist(f, 60000);
        assertEquals(ImmutableSet.of("first"), aw.getApprovedUrls());

        assertTrue(f.delete());

        aw.checkApprovedUrls();
        assertThat(aw.getApprovedUrls(), IsEmptyCollection.empty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void failsWithInvalidCheckInterval()
    {
        new ApprovalWhitelist.FileBackedApprovalWhitelist(new File("no-such-file"), 0);
    }

    @Test
    public void blankLinesAreIgnored() throws IOException
    {
        File f = File.createTempFile(getClass().getName(), ".urls");
        f.deleteOnExit();

        FileUtils.writeLines(f, ImmutableList.of("first", "", "second"));

        ApprovalWhitelist aw = new ApprovalWhitelist.FileBackedApprovalWhitelist(f, 60000);
        assertEquals(ImmutableSet.of("first", "second"), aw.getApprovedUrls());
    }

    @Test
    public void emptyInstanceIsReturnedForMissingResource() throws IOException
    {
        ApprovalWhitelist aw = ApprovalWhitelist.fromResource("/no-such-resource", 60000);
        assertThat(aw.getApprovedUrls(), IsEmptyCollection.empty());
    }

    @Test
    public void instanceIsConstructedFromAvailableResource() throws IOException
    {
        ApprovalWhitelist aw = ApprovalWhitelist.fromResource(ApprovalWhitelistTest.class.getClassLoader(), "com/atlassian/crowd/openid/server/manager/site/sample-whitelist", 60000);

        assertEquals(ImmutableSet.of("first", "second"), aw.getApprovedUrls());
    }

    @Test
    public void urisWithNonAsciiCharactersAreIgnored() throws IOException
    {
        File f = File.createTempFile(getClass().getName(), ".urls");
        f.deleteOnExit();

        FileUtils.writeLines(f, ImmutableList.of("first", "t€st", "second"));

        ApprovalWhitelist aw = new ApprovalWhitelist.FileBackedApprovalWhitelist(f, 60000);
        assertEquals(ImmutableSet.of("first", "second"), aw.getApprovedUrls());
    }
}
