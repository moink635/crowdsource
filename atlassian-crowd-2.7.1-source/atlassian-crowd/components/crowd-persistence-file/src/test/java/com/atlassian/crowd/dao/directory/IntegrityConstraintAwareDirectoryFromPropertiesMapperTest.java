package com.atlassian.crowd.dao.directory;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IntegrityConstraintAwareDirectoryFromPropertiesMapperTest
{

    private static final Long DIRECTORY_ID = 99L;

    private IntegrityConstraintAwareDirectoryPropertiesMapper factory;

    @Mock DirectoryDao directoryDao;
    @Mock Directory directory;

    @Before
    public void setUp() throws Exception
    {
        factory = new IntegrityConstraintAwareDirectoryPropertiesMapper(directoryDao);
    }

    @Test
    public void testExistingDirectoryCanBeImported() throws Exception
    {
        when(directoryDao.findById(DIRECTORY_ID)).thenReturn(directory);
        assertTrue("An existing directory should be instantiable",
                   factory.canImportDirectory(DIRECTORY_ID));
    }

    @Test
    public void testNonExistingDirectoryCannotBeImported() throws Exception
    {
        when(directoryDao.findById(DIRECTORY_ID)).thenThrow(new DirectoryNotFoundException(DIRECTORY_ID));
        assertFalse("A non-existing directory should not be instantiable",
                    factory.canImportDirectory(DIRECTORY_ID));
    }
}
