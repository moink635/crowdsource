package com.atlassian.crowd.dao.directory;

import java.util.List;

import com.atlassian.crowd.dao.DaoDiscriminator;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.search.query.entity.EntityQuery;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FailoverDirectoryDAOTest
{

    private static final Long DIRECTORY_ID = 99L;
    private static final String DIRECTORY_NAME = "My directory name";

    @Mock private DirectoryDao primaryDao;
    @Mock private DirectoryDao secondaryDao;
    @Mock private DaoDiscriminator daoDiscriminator;
    @Mock private Directory directory;
    @Mock private EntityQuery<Directory> entityQuery;
    private FailoverDirectoryDAO directoryDao;

    @Before
    public void setUp() throws Exception
    {
        directoryDao = new FailoverDirectoryDAO(primaryDao, secondaryDao, daoDiscriminator);
    }

    @Test
    public void findByIdWhenPrimaryIsActive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(true);
        when(primaryDao.findById(DIRECTORY_ID)).thenReturn(directory);

        Directory actualDirectory = directoryDao.findById(DIRECTORY_ID);

        assertSame(actualDirectory, directory);
        verify(primaryDao).findById(DIRECTORY_ID);
        verifyZeroInteractions(secondaryDao);
    }

    @Test
    public void findByIdWhenPrimaryIsInactive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(false);
        when(secondaryDao.findById(DIRECTORY_ID)).thenReturn(directory);

        Directory actualDirectory = directoryDao.findById(DIRECTORY_ID);

        assertSame(actualDirectory, directory);
        verify(secondaryDao).findById(DIRECTORY_ID);
        verifyZeroInteractions(primaryDao);
    }

    @Test
    public void findByNameWhenPrimaryIsActive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(true);
        when(primaryDao.findByName(DIRECTORY_NAME)).thenReturn(directory);

        Directory actualDirectory = directoryDao.findByName(DIRECTORY_NAME);

        assertSame(actualDirectory, directory);
        verify(primaryDao).findByName(DIRECTORY_NAME);
        verifyZeroInteractions(secondaryDao);
    }

    @Test
    public void findByNameWhenPrimaryIsInactive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(false);
        when(secondaryDao.findByName(DIRECTORY_NAME)).thenReturn(directory);

        Directory actualDirectory = directoryDao.findByName(DIRECTORY_NAME);

        assertSame(actualDirectory, directory);
        verify(secondaryDao).findByName(DIRECTORY_NAME);
        verifyZeroInteractions(primaryDao);
    }

    @Test
    public void findAllWhenPrimaryIsActive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(true);
        when(primaryDao.findAll()).thenReturn(ImmutableList.of(directory));

        List<Directory> actualDirectories = directoryDao.findAll();

        assertEquals(ImmutableList.of(directory), actualDirectories);
        verify(primaryDao).findAll();
        verifyZeroInteractions(secondaryDao);
    }

    @Test
    public void findAllWhenPrimaryIsInactive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(false);
        when(secondaryDao.findAll()).thenReturn(ImmutableList.of(directory));

        List<Directory> actualDirectories = directoryDao.findAll();

        assertEquals(ImmutableList.of(directory), actualDirectories);
        verify(secondaryDao).findAll();
        verifyZeroInteractions(primaryDao);
    }

    @Test
    public void addWhenPrimaryIsActive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(true);

        directoryDao.add(directory);

        verify(primaryDao).add(directory);
        verifyZeroInteractions(secondaryDao);
    }

    @Test
    public void addWhenPrimaryIsInactive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(false);

        directoryDao.add(directory);

        verify(secondaryDao).add(directory);
        verifyZeroInteractions(primaryDao);
    }

    @Test
    public void updateWhenPrimaryIsActive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(true);

        directoryDao.update(directory);

        verify(primaryDao).update(directory);
        verifyZeroInteractions(secondaryDao);
    }

    @Test
    public void updateWhenPrimaryIsInactive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(false);

        directoryDao.update(directory);

        verify(secondaryDao).update(directory);
        verifyZeroInteractions(primaryDao);
    }

    @Test
    public void removeWhenPrimaryIsActive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(true);

        directoryDao.remove(directory);

        verify(primaryDao).remove(directory);
        verifyZeroInteractions(secondaryDao);
    }

    @Test
    public void removeWhenPrimaryIsInactive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(false);

        directoryDao.remove(directory);

        verify(secondaryDao).remove(directory);
        verifyZeroInteractions(primaryDao);
    }

    @Test
    public void searchWhenPrimaryIsActive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(true);
        when(primaryDao.search(entityQuery)).thenReturn(ImmutableList.of(directory));

        List<Directory> actualDirectories = directoryDao.search(entityQuery);

        assertEquals(ImmutableList.of(directory), actualDirectories);
        verify(primaryDao).search(entityQuery);
        verifyZeroInteractions(secondaryDao);
    }

    @Test
    public void searchWhenPrimaryIsInactive() throws Exception
    {
        when(daoDiscriminator.isActive()).thenReturn(false);
        when(secondaryDao.search(entityQuery)).thenReturn(ImmutableList.of(directory));

        List<Directory> actualDirectories = directoryDao.search(entityQuery);

        assertEquals(ImmutableList.of(directory), actualDirectories);
        verify(secondaryDao).search(entityQuery);
        verifyZeroInteractions(primaryDao);
    }
}
