package com.atlassian.crowd.dao.directory;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;

import com.google.common.base.Predicates;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

import org.junit.Before;
import org.junit.Test;

import static com.google.common.base.Predicates.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SortedPropertiesTest
{
    private SortedProperties properties;
    private List<String> shuffledKeys;

    @Before
    public void setUp() throws Exception
    {
        properties = new SortedProperties();
        shuffledKeys = new LinkedList<String>();
        for ( int i = 0 ; i < 9 ; i++ )
        {
            shuffledKeys.add("key" + i);
        }
        Collections.shuffle(shuffledKeys);
        for ( String key : shuffledKeys)
        {
            properties.setProperty(key, "value");
        }
    }

    @Test
    public void testKeysAreOrdered() throws Exception
    {
        ArrayList<String> sortedKeys = new ArrayList<String>(shuffledKeys);
        Collections.sort(sortedKeys);
        ArrayList<String> keys = Collections.list((Enumeration<String>) (Object) properties.keys());
        assertEquals("keys should be ordered", sortedKeys, keys);
    }

    @Test
    public void testStoreProducesOrderedFile() throws Exception
    {
        StringWriter writer = new StringWriter();
        properties.store(writer, "");
        Iterable<String> lines = Iterables.filter(Splitter.on("\n").omitEmptyStrings().split(writer.toString()),
                                                              not(Predicates.containsPattern("^#")));
        assertTrue("lines should be ordered: " + lines, Ordering.natural().isStrictlyOrdered(lines));
    }
}
