package com.atlassian.crowd.dao.util;

import java.io.File;

import com.atlassian.config.HomeLocator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DatabaseFileLocatorImplTest
{
    private static final String FILENAME = "test-database-file";
    private static final String HOME_PATH = "/some/directory";

    private DatabaseFileLocator databaseFileLocator;

    @Mock HomeLocator homeLocator;

    @Before
    public void setUp() throws Exception
    {
        databaseFileLocator = new DatabaseFileLocatorImpl(homeLocator, FILENAME);
    }

    @Test
    public void testGetFile() throws Exception
    {
        when(homeLocator.getHomePath()).thenReturn(HOME_PATH);
        File file = databaseFileLocator.getFile();
        assertEquals(new File(HOME_PATH, FILENAME), file);
    }
}
