package com.atlassian.crowd.dao.directory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import com.atlassian.crowd.dao.util.DatabaseFileLocator;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.SearchRestriction;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestriction;

import com.google.common.collect.ImmutableList;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static com.atlassian.crowd.search.builder.QueryBuilder.queryFor;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DirectoryDAOFileTest
{
    private static final Long DIRECTORY_ID = 102L;
    private static final Long NON_EXISTING_DIRECTORY_ID = 104L;

    private static final String DIRECTORY_NAME = "My directory name";
    private static final String NON_EXISTING_DIRECTORY_NAME = "Another directory name";

    private static final String CROWD_HOME_PATH = "crowd-home-" + DirectoryDAOFileTest.class.getName();

    @Mock private DatabaseFileLocator databaseFileLocator;
    @Mock private Directory directory;
    @Mock private Directory secondDirectory;
    @Mock private DirectoryPropertiesMapper directoryPropertiesMapper;
    private File homeDir;
    private File databaseFile;
    private DirectoryDAOFile directoryDao;
    private List<Directory> directoriesFromFile;

    @Before
    public void setUp() throws Exception
    {
        directoriesFromFile = ImmutableList.of(directory, secondDirectory);

        homeDir = new File(FileUtils.getTempDirectory(), CROWD_HOME_PATH);
        homeDir.mkdirs();
        databaseFile = new File(homeDir, "directory.conf");

        when(databaseFileLocator.getFile()).thenReturn(databaseFile);
        directoryDao = new DirectoryDAOFile(databaseFileLocator, directoryPropertiesMapper);
    }

    @After
    public void clearCrowdHome() throws IOException
    {
        if (homeDir.exists() && homeDir.isDirectory())
        {
            homeDir.deleteOnExit();
            FileUtils.deleteDirectory(homeDir);
        }
    }

    @Test
    public void directoriesAreRefreshedWhenFileIsModified() throws Exception
    {
        Properties oldProperties = new Properties();
        oldProperties.setProperty("name", "oldValue");
        FileWriter oldFileWriter = new FileWriter(databaseFile);
        oldProperties.store(oldFileWriter, "");
        oldFileWriter.close();
        databaseFile.setLastModified(0); // this file was created long time ago

        directoryDao.refresh(); // load the old value

        Properties newProperties = new Properties();
        newProperties.setProperty("name", "newValue");
        FileWriter newFileWriter = new FileWriter(databaseFile);
        newProperties.store(newFileWriter, "");
        newFileWriter.close();
        databaseFile.setLastModified(TimeUnit.SECONDS.toMillis(1)); // the file was updated

        assertEquals("oldValue", directoryDao.getProperties().getProperty("name"));
        directoryDao.refresh();
        assertEquals("newValue", directoryDao.getProperties().getProperty("name"));
    }

    @Test
    public void testFindByExistingId() throws Exception
    {
        when(directoryPropertiesMapper.importDirectory(any(Properties.class), anyLong(), any(Date.class)))
            .thenReturn(directory);

        Directory directory = directoryDao.findById(DIRECTORY_ID);

        assertSame(this.directory, directory);
    }

    @Test (expected = DirectoryNotFoundException.class)
    public void testFindByNonExistingId() throws Exception
    {
        when(directoryPropertiesMapper.importDirectory(any(Properties.class), anyLong(), any(Date.class)))
            .thenThrow(new DirectoryNotFoundException(NON_EXISTING_DIRECTORY_ID));

        Directory directory = directoryDao.findById(NON_EXISTING_DIRECTORY_ID);

        assertSame(this.directory, directory);
    }

    @Test
    public void testFindByExistingName() throws Exception
    {
        List<Directory> directoriesFromFile = ImmutableList.of(directory);
        when(directoryPropertiesMapper.importAllDirectories(any(Properties.class), any(Date.class))).thenReturn(
            directoriesFromFile);
        when(directory.getName()).thenReturn(DIRECTORY_NAME);

        Directory directory = directoryDao.findByName(DIRECTORY_NAME);

        assertSame(this.directory, directory);
    }

    @Test (expected = DirectoryNotFoundException.class)
    public void testFindByNonExistingName() throws Exception
    {
        List<Directory> directoriesFromFile = ImmutableList.of(directory);
        when(directoryPropertiesMapper.importAllDirectories(any(Properties.class), any(Date.class))).thenReturn(
            directoriesFromFile);
        when(directory.getName()).thenReturn(DIRECTORY_NAME);

        directoryDao.findByName(NON_EXISTING_DIRECTORY_NAME);
    }

    @Test
    public void testFindAll() throws Exception
    {
        when(directoryPropertiesMapper.importAllDirectories(any(Properties.class), any(Date.class))).thenReturn(
            directoriesFromFile);

        List<Directory> directories = directoryDao.findAll();

        assertEquals(directoriesFromFile, directories);
    }

    @Test
    public void testSearchWithoutRestriction() throws Exception
    {
        when(directoryPropertiesMapper.importAllDirectories(any(Properties.class), any(Date.class))).thenReturn(
            directoriesFromFile);

        // return all (without restriction)
        List<Directory> directories =
            directoryDao.search(queryFor(Directory.class, EntityDescriptor.directory())
                                    .returningAtMost(EntityQuery.ALL_RESULTS));
        assertEquals(directoriesFromFile, directories);
    }

    @Test
    public void testSearchWithNullRestriction() throws Exception
    {
        when(directoryPropertiesMapper.importAllDirectories(any(Properties.class), any(Date.class))).thenReturn(
            directoriesFromFile);

        NullRestriction restriction = new NullRestriction() { };
        List<Directory> directories =
            directoryDao.search(queryFor(Directory.class, EntityDescriptor.directory()).with(restriction)
                                    .returningAtMost(EntityQuery.ALL_RESULTS));
        assertEquals(directoriesFromFile, directories);
    }

    @Test
    public void testSearchWithoutRestrictionHandlesLargePages() throws Exception
    {
        when(directoryPropertiesMapper.importAllDirectories(any(Properties.class), any(Date.class))).thenReturn(
            directoriesFromFile);

        List<Directory> directories =
            directoryDao.search(queryFor(Directory.class, EntityDescriptor.directory())
                                    .returningAtMost(directoriesFromFile.size() + 1)); // page size > #directories

        assertEquals(directoriesFromFile, directories);
    }

    @Test
    public void testSearchWithoutRestrictionDoesPagination() throws Exception
    {
        when(directoryPropertiesMapper.importAllDirectories(any(Properties.class), any(Date.class))).thenReturn(
            directoriesFromFile);

        // return just first
        List<Directory> directories =
            directoryDao.search(queryFor(Directory.class, EntityDescriptor.directory())
                                    .startingAt(0).returningAtMost(1));

        assertEquals(1, directories.size());
        assertThat(directories, hasItem(directory));

        // return just second
        directories =
            directoryDao.search(queryFor(Directory.class, EntityDescriptor.directory())
                                    .startingAt(1).returningAtMost(1));

        assertEquals(1, directories.size());
        assertThat(directories, hasItem(secondDirectory));
    }

    @Test (expected = UnsupportedOperationException.class)
    public void testSearchWithUnsupportedRestriction() throws Exception
    {
        directoryDao.search(queryFor(Directory.class, EntityDescriptor.directory()).with(new SearchRestriction() {})
                                .returningAtMost(EntityQuery.ALL_RESULTS));
    }

    @Test
    public void testIsActiveWhenThereIsAFile() throws Exception
    {
        // put a file in place
        FileUtils.touch(databaseFile);
        DirectoryDAOFile directoryDaoWithAFile =
            new DirectoryDAOFile(databaseFileLocator, directoryPropertiesMapper);
        assertTrue("A database file should activate the DAO", directoryDaoWithAFile.isActive());
    }

    @Test
    public void testAddingTheFileActivatesTheDao() throws Exception
    {
        DirectoryDAOFile directoryDaoThatChangesState =
            new DirectoryDAOFile(databaseFileLocator, directoryPropertiesMapper);
        assertFalse("DAO should be initially inactive", directoryDaoThatChangesState.isActive());

        FileUtils.touch(databaseFile);
        directoryDaoThatChangesState.refresh();
        assertTrue("The DAO should be active after deleting the file", directoryDaoThatChangesState.isActive());
    }

    @Test
    public void testRemovingTheFileDeactivatesTheDao() throws Exception
    {
        // put a file in place
        FileUtils.touch(databaseFile);
        DirectoryDAOFile directoryDaoThatChangesState =
            new DirectoryDAOFile(databaseFileLocator, directoryPropertiesMapper);
        assertTrue("A database file should activate the DAO", directoryDaoThatChangesState.isActive());

        FileUtils.forceDelete(databaseFile);
        directoryDaoThatChangesState.refresh();
        assertFalse("The DAO should be inactive after deleting the file", directoryDaoThatChangesState.isActive());
    }

    @Test
    public void testIsActiveReturnsFalseWhenThereIsNoFile() throws Exception
    {
        DirectoryDAOFile directoryDaoWithoutAFile =
            new DirectoryDAOFile(databaseFileLocator, directoryPropertiesMapper);
        assertFalse("Lack of database file should de-activate the DAO", directoryDaoWithoutAFile.isActive());
    }
}
