package com.atlassian.crowd.dao.directory;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.exception.DirectoryNotFoundException;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterables;

import org.hamcrest.core.IsCollectionContaining;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class DirectoryPropertiesMapperTest
{

    public static final Properties VALID_DIRECTORY_DEFINITION = buildDirectoryProperties(1,
        "My directory name",
        "My directory description",
        "My encryption type",
        "CONNECTOR",
        "CREATE_GROUP, UPDATE_USER",
        "true",
        "my.implementation.Class");

    public static final Properties VALID_DIRECTORY_ATTRIBUTES = buildAttributeProperties(1,
        ImmutableMap.of("name1", "value1", "name2", "value2"));

    public static final Properties SECOND_VALID_DIRECTORY_DEFINITION = buildDirectoryProperties(2,
        "My other name",
        "My other description",
        "My other encryption type",
        "CONNECTOR",
        "CREATE_GROUP, UPDATE_USER",
        "true",
        "my.other.implementation.Class");

    private static final Long DIRECTORY_ID = 1L;
    private static final Long SECOND_DIRECTORY_ID = 2L;

    private DirectoryPropertiesMapper directoryPropertiesMapper;
    private Properties properties = new Properties();
    private Date timestamp = new Date(0);

    @Before
    public void setUp()
    {
        directoryPropertiesMapper = new DirectoryPropertiesMapper();
    }

    @Test
    public void testImportValidDirectory() throws Exception
    {
        properties.putAll(VALID_DIRECTORY_DEFINITION);
        properties.putAll(VALID_DIRECTORY_ATTRIBUTES);
        Directory directory = directoryPropertiesMapper.importDirectory(properties, DIRECTORY_ID, timestamp);
        assertEquals(DIRECTORY_ID, directory.getId());
        assertEquals("My directory name", directory.getName());
        assertEquals("My directory description", directory.getDescription());
        assertEquals("My encryption type", directory.getEncryptionType());
        assertEquals("my.implementation.Class", directory.getImplementationClass());
        assertTrue("Directory should be active", directory.isActive());
        assertEquals(DirectoryType.CONNECTOR, directory.getType());
        assertEquals(ImmutableSet.of(OperationType.CREATE_GROUP, OperationType.UPDATE_USER),
                     directory.getAllowedOperations());
        assertEquals(timestamp, directory.getCreatedDate());
        assertEquals(timestamp, directory.getUpdatedDate());
        assertEquals(ImmutableMap.of("name1", "value1", "name2", "value2"), directory.getAttributes());
    }

    @Test (expected = DirectoryNotFoundException.class)
    public void testImportDirectoryImportNotPermitted() throws Exception
    {
        DirectoryPropertiesMapper factory = new DirectoryPropertiesMapper() {
            @Override
            protected boolean canImportDirectory(Long id)
            {
                return false;
            }
        };
        factory.importDirectory(properties, DIRECTORY_ID, timestamp);
    }

    @Test (expected = DirectoryNotFoundException.class)
    public void testImportDirectoryMissingRequiredProperty() throws Exception
    {
        properties.putAll(VALID_DIRECTORY_DEFINITION);
        properties.remove("directory.1.name"); // remove a required property
        directoryPropertiesMapper.importDirectory(properties, DIRECTORY_ID, timestamp);
    }

    @Test
    public void testImportDirectoryMissingOptionalEncryptionType() throws Exception
    {
        properties.putAll(VALID_DIRECTORY_DEFINITION);
        properties.remove("directory.1.encryptionType"); // remove an optional property
        Directory directory = directoryPropertiesMapper.importDirectory(properties, DIRECTORY_ID, timestamp);
        assertNull("Encryption type should be null", directory.getEncryptionType());
    }

    @Test (expected = DirectoryNotFoundException.class)
    public void testImportDirectoryInvalidDirectoryType() throws Exception
    {
        properties.putAll(VALID_DIRECTORY_DEFINITION);
        properties.setProperty("directory.1.type", "INVALID DIRECTORY TYPE"); // change property value
        directoryPropertiesMapper.importDirectory(properties, DIRECTORY_ID, timestamp);
    }

    @Test (expected = DirectoryNotFoundException.class)
    public void testImportDirectoryInvalidAllowedOperations() throws Exception
    {
        properties.putAll(VALID_DIRECTORY_DEFINITION);
        properties.setProperty("directory.1.allowedOperations", "UNKNOWN, DIRECTORY, OPERATIONS"); // change property value
        directoryPropertiesMapper.importDirectory(properties, DIRECTORY_ID, timestamp);
    }

    @Test
    public void testImportAllDirectories() throws Exception
    {
        properties.putAll(VALID_DIRECTORY_DEFINITION);
        properties.putAll(SECOND_VALID_DIRECTORY_DEFINITION);
        List<Directory> directories = directoryPropertiesMapper.importAllDirectories(properties, timestamp);
        assertEquals(2, directories.size());
        assertThat(directories, IsCollectionContaining.<Directory>hasItems(
            hasProperty("id", is(DIRECTORY_ID)), hasProperty("id", is(SECOND_DIRECTORY_ID))));
    }

    @Test
    public void testExportDirectoryWithAllProperties() throws Exception
    {
        Date createdDate = new Date(30);
        Date updatedDate = new Date(31);
        Directory directory = new ImmutableDirectory(DIRECTORY_ID, "My directory name", true,
                                                     "My directory description", "My encryption type",
                                                     DirectoryType.DELEGATING, "my.implementation.Class",
                                                     createdDate, updatedDate,
                                                     ImmutableSet.of(OperationType.UPDATE_GROUP, OperationType.CREATE_USER),
                                                     ImmutableMap.of(
                                                         "name1", "value1",
                                                         "name2", "value2"
                                                     ));

        Properties actualProperties = directoryPropertiesMapper.exportProperties(ImmutableSet.of(directory));

        String prefix = "directory." + DIRECTORY_ID + ".";
        assertEquals("My directory name",
                     actualProperties.getProperty(prefix + DirectoryPropertiesMapper.NAME_PROPERTY));
        assertEquals("true", actualProperties.getProperty(prefix + DirectoryPropertiesMapper.ACTIVE_PROPERTY));
        assertEquals("My directory description",
                     actualProperties.getProperty(prefix + DirectoryPropertiesMapper.DESCRIPTION_PROPERTY));
        assertEquals("My encryption type",
                     actualProperties.getProperty(prefix + DirectoryPropertiesMapper.ENCRYPTION_TYPE_PROPERTY));
        assertEquals("DELEGATING", actualProperties.getProperty(prefix + DirectoryPropertiesMapper.TYPE_PROPERTY));
        assertEquals("my.implementation.Class",
                     actualProperties.getProperty(prefix + DirectoryPropertiesMapper.IMPLEMENTATION_CLASS_PROPERTY));
        assertEquals("CREATE_USER,UPDATE_GROUP", // order is guaranteed
                     actualProperties.getProperty(prefix + DirectoryPropertiesMapper.ALLOWED_OPERATIONS_PROPERTY));
        assertEquals("value1",
                     actualProperties.getProperty(prefix + DirectoryPropertiesMapper.ATTRIBUTES_PROPERTY + ".name1"));
        assertEquals("value2",
                     actualProperties.getProperty(prefix + DirectoryPropertiesMapper.ATTRIBUTES_PROPERTY + ".name2"));
        // dates are not tested
    }

    @Test
    public void testExportDirectoryWithoutEncryptionType() throws Exception
    {
        Date createdDate = new Date(0);
        Date updatedDate = new Date(0);
        Directory directory = new ImmutableDirectory(DIRECTORY_ID, "My directory name", true,
                                                     "My directory description", null,
                                                     DirectoryType.DELEGATING, "my.implementation.Class",
                                                     createdDate, updatedDate,
                                                     ImmutableSet.of(OperationType.UPDATE_GROUP, OperationType.CREATE_USER),
                                                     ImmutableMap.of(
                                                         "name1", "value1",
                                                         "name2", "value2"
                                                     ));

        Properties actualProperties = directoryPropertiesMapper.exportProperties(ImmutableSet.of(directory));

        String prefix = "directory." + DIRECTORY_ID + ".";
        assertFalse("Encryption type should not exist in the properties",
                    actualProperties.containsKey(DirectoryPropertiesMapper.ENCRYPTION_TYPE_PROPERTY));
    }

    private static Properties buildDirectoryProperties(long id, String name, String description, String encryptionType,
                                                       String type, String allowedOperations, String active,
                                                       String implementationClass)
    {
        Properties properties = new Properties();
        String prefix = "directory." + id + ".";
        properties.setProperty(prefix + "name", name);
        properties.setProperty(prefix + "description", description);
        properties.setProperty(prefix + "encryptionType", encryptionType);
        properties.setProperty(prefix + "type", type);
        properties.setProperty(prefix + "allowedOperations", allowedOperations);
        properties.setProperty(prefix + "active", active);
        properties.setProperty(prefix + "implementationClass", implementationClass);
        return properties;
    }

    private static Properties buildAttributeProperties(long id, ImmutableMap<String,String> attributes)
    {
        Properties properties = new Properties();
        String prefix = "directory." + id + ".attributes.";
        for (Map.Entry<String, String> entry : attributes.entrySet())
        {
            properties.setProperty(prefix + entry.getKey(), entry.getValue());
        }
        return properties;
    }

    @Test
    public void allowedOperationsAreSplitAndTrimmed() throws DirectoryNotFoundException
    {
        Properties definition = buildDirectoryProperties(1,
                "name",
                "",
                "",
                "CONNECTOR",
                "   CREATE_USER  , DELETE_GROUP   ",
                "",
                "");

        Directory directory = directoryPropertiesMapper.importDirectory(definition, 1L, new Date(0));

        assertEquals(ImmutableSet.of(OperationType.CREATE_USER, OperationType.DELETE_GROUP),
                directory.getAllowedOperations());
    }

    @Test
    public void directoriesAreReturnedFromFindAllInOrder()
    {
        Properties props = new Properties();

        for (int i = 0; i < 10; i++)
        {
            props.putAll(buildDirectoryProperties(i,
                "My directory name",
                "My directory description",
                "My encryption type",
                "CONNECTOR",
                "CREATE_GROUP, UPDATE_USER",
                "true",
                "my.implementation.Class"));
        }

        List<Directory> directories = directoryPropertiesMapper.importAllDirectories(props, timestamp);
        assertThat(directories, hasSize(10));

        Iterable<Long> ids = Iterables.transform(directories, new Function<Directory, Long>()
        {
            public Long apply(Directory input)
            {
                return input.getId();
            }
        });

        assertThat(ids, contains(0L, 1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L));
    }
}
