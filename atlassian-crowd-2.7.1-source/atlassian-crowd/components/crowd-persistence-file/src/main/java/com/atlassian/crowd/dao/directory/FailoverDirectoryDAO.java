package com.atlassian.crowd.dao.directory;

import java.util.List;

import com.atlassian.crowd.dao.DaoDiscriminator;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.search.query.entity.EntityQuery;

/**
 * Delegates to a primary DAO, but only if it is active, or to a secondary DAO otherwise.
 */
public class FailoverDirectoryDAO implements DirectoryDao
{

    private final DirectoryDao primaryDao;
    private final DirectoryDao secondaryDao;
    private final DaoDiscriminator daoDiscriminator;

    /**
     * @param primaryDao DAO that will be used unless it is inactive
     * @param secondaryDao failover DAO that will be used only if the primary is inactive
     * @param daoDiscriminator discriminator that indicates whether the primary DAO is active or not
     */
    public FailoverDirectoryDAO(DirectoryDao primaryDao, DirectoryDao secondaryDao, DaoDiscriminator daoDiscriminator)
    {
        this.primaryDao = primaryDao;
        this.secondaryDao = secondaryDao;
        this.daoDiscriminator = daoDiscriminator;
    }

    @Override
    public Directory findById(long directoryId) throws DirectoryNotFoundException
    {
        return getEffectiveDao().findById(directoryId);
    }

    @Override
    public Directory findByName(String name) throws DirectoryNotFoundException
    {
        return getEffectiveDao().findByName(name);
    }

    @Override
    public List<Directory> findAll()
    {
        return getEffectiveDao().findAll();
    }

    @Override
    public Directory add(Directory directory)
    {
        return getEffectiveDao().add(directory);
    }

    @Override
    public Directory update(Directory directory) throws DirectoryNotFoundException
    {
        return getEffectiveDao().update(directory);
    }

    @Override
    public void remove(Directory directory) throws DirectoryNotFoundException
    {
        getEffectiveDao().remove(directory);
    }

    @Override
    public List<Directory> search(EntityQuery<Directory> entityQuery)
    {
        return getEffectiveDao().search(entityQuery);
    }

    private DirectoryDao getEffectiveDao()
    {
        if (daoDiscriminator.isActive())
        {
            return primaryDao;
        }
        else
        {
            return secondaryDao;
        }
    }

}
