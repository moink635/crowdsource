package com.atlassian.crowd.dao.directory;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Properties;
import java.util.TreeSet;

/**
 * Extends Properties to ensure a consistent key order
 */
public class SortedProperties extends Properties
{
    @Override
    public synchronized Enumeration<Object> keys()
    {
        return Collections.enumeration(new TreeSet<Object>(super.keySet()));
    }
}
