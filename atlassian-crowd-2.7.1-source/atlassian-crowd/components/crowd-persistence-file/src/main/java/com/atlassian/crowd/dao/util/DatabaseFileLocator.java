package com.atlassian.crowd.dao.util;

import java.io.File;

/**
 * Abstracts the location of the database file
 */
public interface DatabaseFileLocator
{

    File getFile();

}
