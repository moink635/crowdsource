package com.atlassian.crowd.dao.directory;

import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;

/**
 * In order to prevent the instantiation of directories that do not exist in the
 * database, this class only authorises directories that match an existing id
 * in the database.
 */
public class IntegrityConstraintAwareDirectoryPropertiesMapper extends DirectoryPropertiesMapper
{

    private final DirectoryDao backendDirectoryDao;

    public IntegrityConstraintAwareDirectoryPropertiesMapper(DirectoryDao backendDirectoryDao)
    {
        this.backendDirectoryDao = backendDirectoryDao;
    }

    @Override
    protected boolean canImportDirectory(Long id)
    {
        try
        {
            backendDirectoryDao.findById(id);
            return true;
        }
        catch (DirectoryNotFoundException e)
        {
            return false;
        }
    }
}
