package com.atlassian.crowd.dao.directory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import com.atlassian.crowd.dao.DaoDiscriminator;
import com.atlassian.crowd.dao.util.DatabaseFileLocator;
import com.atlassian.crowd.dao.RefreshableDao;
import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.spi.DirectoryDao;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.search.EntityDescriptor;
import com.atlassian.crowd.search.query.entity.EntityQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestriction;
import com.atlassian.crowd.search.util.SearchResultsUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Read-only Directory DAO that reads the data from a file. Update operations are not
 * supported. Search operation is not supported.
 */
public class DirectoryDAOFile implements DirectoryDao, DaoDiscriminator, RefreshableDao
{
    private static final Logger logger = LoggerFactory.getLogger(DirectoryDAOFile.class);

    private Properties properties;
    private Date timestamp;
    private final DirectoryPropertiesMapper directoryPropertiesMapper;
    private final DatabaseFileLocator databaseFileLocator;

    public DirectoryDAOFile(DatabaseFileLocator databaseFileLocator,
                            DirectoryPropertiesMapper directoryPropertiesMapper)
    {
        this.databaseFileLocator = databaseFileLocator;
        this.directoryPropertiesMapper = directoryPropertiesMapper;
        refresh();
    }

    @Override
    public synchronized void refresh()
    {
        File file = databaseFileLocator.getFile();
        if (file.exists())
        {
            if (timestamp == null || timestamp.getTime() < file.lastModified())
            {
                timestamp = new Date(file.lastModified());
                properties = readProperties(file);
            }
            else
            {
                logger.debug("Skipping refresh because the file modification date has not changed");
            }
        }
        else
        {
            if (properties != null)
            {
                logger.info("File {} is missing, no longer using it for directory configuration", file.getPath());
            }
            timestamp = null;
            properties = null;
        }
    }

    private static Properties readProperties(File file)
    {
        Properties properties = new Properties();
        try
        {
            FileReader reader = new FileReader(file);
            try
            {
                properties.load(reader);
                logger.info("Directory configuration has been read from file {}", file.getCanonicalPath());
                return properties;
            }
            finally
            {
                reader.close();
            }
        }
        catch (FileNotFoundException e)
        {
            // ok, there is no override file
            return null;
        }
        catch (IOException e)
        {
            logger.error("While reading the DAO file override", e);
            return null;
        }
    }

    @Override
    public Directory findById(long directoryId) throws DirectoryNotFoundException
    {
        return directoryPropertiesMapper.importDirectory(properties, directoryId, timestamp);
    }

    @Override
    public Directory findByName(String name) throws DirectoryNotFoundException
    {
        for (Directory directory : findAll())
        {
            if (name.equals(directory.getName()))
            {
                return directory; // uniqueness is not guaranteed, stop on first match
            }
        }
        throw new DirectoryNotFoundException(name);
    }

    @Override
    public List<Directory> findAll()
    {
        return directoryPropertiesMapper.importAllDirectories(properties, timestamp);
    }

    @Override
    public Directory add(Directory directory)
    {
        throw new UnsupportedOperationException("The file-based directory configuration is read-only");
    }

    @Override
    public Directory update(Directory directory) throws DirectoryNotFoundException
    {
        throw new UnsupportedOperationException("The file-based directory configuration is read-only");
    }

    @Override
    public void remove(Directory directory) throws DirectoryNotFoundException
    {
        throw new UnsupportedOperationException("The file-based directory configuration is read-only");
    }

    @Override
    public List<Directory> search(EntityQuery<Directory> entityQuery)
    {
        if (entityQuery.getEntityDescriptor().equals(EntityDescriptor.directory()) && entityQuery.getSearchRestriction() instanceof NullRestriction)
        {
            return SearchResultsUtil.constrainResults(findAll(), entityQuery.getStartIndex(), entityQuery.getMaxResults());
        }
        else
        {
            throw new UnsupportedOperationException("The file-based directory configuration does not support non-trivial search");
        }
    }

    @Override
    public synchronized boolean isActive()
    {
        return properties != null;
    }

    public Properties getProperties()
    {
        return properties;
    }
}
