package com.atlassian.crowd.dao.directory;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.atlassian.crowd.embedded.api.Directory;
import com.atlassian.crowd.embedded.api.DirectoryType;
import com.atlassian.crowd.embedded.api.OperationType;
import com.atlassian.crowd.exception.DirectoryNotFoundException;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.common.collect.Iterables;
import com.google.common.collect.Ordering;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.collect.Iterables.transform;

/**
 * A mapper that builds immutable directories by parsing the data from a properties
 * (it does not do any caching) and exports directories to properties.
 */
public class DirectoryPropertiesMapper
{

    private static final Logger logger = LoggerFactory.getLogger(DirectoryPropertiesMapper.class);
    public static final String IMPLEMENTATION_CLASS_PROPERTY = "implementationClass";
    public static final String DESCRIPTION_PROPERTY = "description";
    public static final String ENCRYPTION_TYPE_PROPERTY = "encryptionType";
    public static final String ACTIVE_PROPERTY = "active";
    public static final String NAME_PROPERTY = "name";
    public static final String ATTRIBUTES_PROPERTY = "attributes";
    public static final String TYPE_PROPERTY = "type";
    public static final String ALLOWED_OPERATIONS_PROPERTY = "allowedOperations";

    public Directory importDirectory(Properties properties, Long id, Date timestamp) throws DirectoryNotFoundException
    {
        if (canImportDirectory(id))
        {
            ImmutableDirectory.Builder builder = ImmutableDirectory.newBuilder();
            builder.setId(id);
            builder.setName(getDirectoryName(properties, id));
            builder.setDescription(getDirectoryDescription(properties, id));
            builder.setActive(getIsActiveDirectory(properties, id));
            builder.setType(getDirectoryType(properties, id));
            builder.setAllowedOperations(getDirectoryAllowedOperations(properties, id));
            builder.setEncryptionType(getDirectoryEncryptionType(properties, id));
            builder.setImplementationClass(getDirectoryImplementationClass(properties, id));
            builder.setCreatedDate(timestamp);
            builder.setUpdatedDate(timestamp);
            builder.setAttributes(getDirectoryAttributes(properties, id));
            return builder.toDirectory();
        }
        else
        {
            logger.info("Instantiation of directory {} from property file is not permitted", id);
            throw new DirectoryNotFoundException(id);
        }
    }

    /**
     * Determines if a directory can be created using this mapper. Subclasses can override
     * this method to implement their own policies.
     * @param id directory id
     * @return true if the directory can be created
     */
    protected boolean canImportDirectory(Long id)
    {
        return true;
    }

    private String getPropertyName(Long id, String propertyName)
    {
        return "directory." + id + "." + propertyName;
    }

    private String getRequiredPropertyStringValueOrFail(Properties properties, Long id, String unqualifiedPropertyName)
        throws DirectoryNotFoundException
    {
        String value = properties.getProperty(getPropertyName(id, unqualifiedPropertyName));
        if (value == null)
        {
            logger.error("Directory {} is missing required property '{}'", id, unqualifiedPropertyName);
            throw new DirectoryNotFoundException(id);
        }
        else
        {
            return value;
        }
    }

    private String getOptionalPropertyStringValue(Properties properties, Long id, String unqualifiedPropertyName)
    {
        return properties.getProperty(getPropertyName(id, unqualifiedPropertyName));
    }

    private String getDirectoryName(Properties properties, Long id) throws DirectoryNotFoundException
    {
        return getRequiredPropertyStringValueOrFail(properties, id, NAME_PROPERTY);
    }

    private boolean getIsActiveDirectory(Properties properties, Long id) throws DirectoryNotFoundException
    {
        return Boolean.valueOf(getRequiredPropertyStringValueOrFail(properties, id, ACTIVE_PROPERTY));
    }

    private String getDirectoryEncryptionType(Properties properties, Long id) throws DirectoryNotFoundException
    {
        return getOptionalPropertyStringValue(properties, id, ENCRYPTION_TYPE_PROPERTY);
    }

    private String getDirectoryDescription(Properties properties, Long id) throws DirectoryNotFoundException
    {
        return getRequiredPropertyStringValueOrFail(properties, id, DESCRIPTION_PROPERTY);
    }

    private String getDirectoryImplementationClass(Properties properties, Long id) throws DirectoryNotFoundException
    {
        return getRequiredPropertyStringValueOrFail(properties, id, IMPLEMENTATION_CLASS_PROPERTY);
    }

    private DirectoryType getDirectoryType(Properties properties, Long id) throws DirectoryNotFoundException
    {
        try
        {
            return DirectoryType.valueOf(getRequiredPropertyStringValueOrFail(properties, id, TYPE_PROPERTY));
        }
        catch (IllegalArgumentException e)
        {
            throw new DirectoryNotFoundException("Unknown directory type", e);
        }
    }

    public Map<String, String> getDirectoryAttributes(Properties properties, Long id)
    {
        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();
        String propertyPrefix = getPropertyName(id, ATTRIBUTES_PROPERTY) + ".";
        for (String propertyName : properties.stringPropertyNames())
        {
            if (propertyName.startsWith(propertyPrefix))
            {
                builder.put(propertyName.substring(propertyPrefix.length()), properties.getProperty(propertyName));
            }
        }
        return builder.build();
    }

    private Set<OperationType> getDirectoryAllowedOperations(Properties properties, Long id) throws DirectoryNotFoundException
    {
        String commaSeparatedOperationNames = getRequiredPropertyStringValueOrFail(properties, id,
                                                                                   ALLOWED_OPERATIONS_PROPERTY);
        Iterable<String> operationNames = Splitter.on(',').trimResults().split(
            commaSeparatedOperationNames);
        try
        {
            return ImmutableSet.copyOf(transform(operationNames, new Function<String,OperationType>() {
                @Override
                public OperationType apply(String input)
                {
                    return OperationType.valueOf(input);
                }
            }));
        }
        catch (IllegalArgumentException e)
        {
            throw new DirectoryNotFoundException("Unknown operation type", e);
        }
    }

    public List<Directory> importAllDirectories(Properties properties, Date timestamp)
    {
        ImmutableList.Builder<Directory> builder = ImmutableList.builder();
        for (Long id : Ordering.natural().immutableSortedCopy(findAllIds(properties)))
        {
            try
            {
                builder.add(importDirectory(properties, id, timestamp));
            }
            catch (DirectoryNotFoundException e)
            {
                logger.info("Skipping directory {} from the configuration file", id);
            }
        }
        return builder.build();
    }

    private Set<Long> findAllIds(Properties properties)
    {
        Pattern propertyNamePattern = Pattern.compile("^directory\\.([0-9]+)\\..*$");
        ImmutableSet.Builder<Long> directoryIds = ImmutableSet.builder();
        for (String propertyName : properties.stringPropertyNames())
        {
            Matcher matcher = propertyNamePattern.matcher(propertyName);
            if (matcher.find())
            {
                directoryIds.add(Long.parseLong(matcher.group(1)));
            }
        }
        return directoryIds.build();
    }

    public Properties exportProperties(Iterable<Directory> allDirectories)
    {
        Properties properties = new SortedProperties();
        for (Directory directory : allDirectories)
        {
            // required properties
            properties.setProperty(getPropertyName(directory.getId(), NAME_PROPERTY), directory.getName());
            properties.setProperty(getPropertyName(directory.getId(), ACTIVE_PROPERTY), Boolean.toString(directory.isActive()));
            properties.setProperty(getPropertyName(directory.getId(), DESCRIPTION_PROPERTY), directory.getDescription());
            properties.setProperty(getPropertyName(directory.getId(), TYPE_PROPERTY), directory.getType().toString());
            properties.setProperty(getPropertyName(directory.getId(), IMPLEMENTATION_CLASS_PROPERTY), directory.getImplementationClass());
            properties.setProperty(getPropertyName(directory.getId(), ALLOWED_OPERATIONS_PROPERTY),
                                   Joiner.on(",").join(ImmutableSortedSet.copyOf(directory.getAllowedOperations())));
            for (Map.Entry<String,String> attributeEntry : directory.getAttributes().entrySet())
            {
                properties.setProperty(getPropertyName(directory.getId(), ATTRIBUTES_PROPERTY + "." + attributeEntry.getKey()),
                                       attributeEntry.getValue());
            }
            // optional properties
            if (directory.getEncryptionType() != null)
            {
                properties.setProperty(getPropertyName(directory.getId(), ENCRYPTION_TYPE_PROPERTY),
                                       directory.getEncryptionType());
            }
        }
        return properties;
    }
}
