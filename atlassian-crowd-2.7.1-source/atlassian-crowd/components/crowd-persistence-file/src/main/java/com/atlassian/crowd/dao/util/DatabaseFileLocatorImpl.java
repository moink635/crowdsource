package com.atlassian.crowd.dao.util;

import java.io.File;

import com.atlassian.config.HomeLocator;

public class DatabaseFileLocatorImpl implements DatabaseFileLocator
{

    private final HomeLocator homeLocator;
    private final String filename;

    public DatabaseFileLocatorImpl(HomeLocator homeLocator, String filename)
    {
        this.homeLocator = homeLocator;
        this.filename = filename;
    }

    @Override
    public File getFile()
    {
        return new File(getHomeDirectory(), filename);
    }

    private File getHomeDirectory()
    {
        return new File(homeLocator.getHomePath());
    }

}
