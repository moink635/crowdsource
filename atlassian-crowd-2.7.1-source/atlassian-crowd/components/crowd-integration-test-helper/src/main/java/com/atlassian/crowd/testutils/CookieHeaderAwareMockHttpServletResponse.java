package com.atlassian.crowd.testutils;

import org.apache.commons.lang3.StringUtils;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.Cookie;
import java.util.HashMap;
import java.util.Map;

/**
 * This class adds on top of {@link MockHttpServletResponse} the ability to recognize Set-Cookie headers and treat them as cookies.
 */
public class CookieHeaderAwareMockHttpServletResponse extends MockHttpServletResponse
{
    @Override
    public void addHeader(String name, String value)
    {
        // If it's a cookie header, we treat it as cookie.
        if (name.equals("Set-Cookie"))
        {
            final String[] components = StringUtils.split(value, ';');
            if (components.length >= 1)
            {
                // first component is the key-value.
                final String[] keyValue = StringUtils.split(components[0], '=');
                String key = StringUtils.trim(keyValue[0]);
                String val = unquote(StringUtils.trim(keyValue[1]));

                final Map<String, String> flags = new HashMap<String, String>();
                for(int i = 1; i < components.length ;i++)
                {
                    final String[] data = StringUtils.split(components[i], '=');
                    flags.put(StringUtils.trim(data[0]), data.length == 2 ? unquote(StringUtils.trim(data[1])):"");
                }

                final Cookie cookie = new Cookie(key, val);
                if (flags.containsKey("Secure"))
                {
                    cookie.setSecure(true);
                }

                cookie.setMaxAge(0);

                super.addCookie(cookie);
            }
        }

        super.addHeader(name, value);
    }

    /**
     * Returns an unquoted string from a possibly quoted one.
     * Doesn't handle escaping.
     */
    private static String unquote(String possiblyQuoted)
    {
        if (possiblyQuoted.length() >= 2 &&
            possiblyQuoted.charAt(0) == '"' &&
            possiblyQuoted.charAt(possiblyQuoted.length()-1) == '"')
        {
            return possiblyQuoted.substring(1, possiblyQuoted.length()-1);
        }

        return possiblyQuoted;
    }
}
