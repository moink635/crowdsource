package com.atlassian.sal.crowd;

import com.atlassian.config.HomeLocator;
import com.atlassian.crowd.plugin.web.ExecutingHttpRequest;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.sal.api.UrlMode;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class CrowdApplicationPropertiesTest
{
    @InjectMocks
    private CrowdApplicationProperties crowdApplicationProperties;

    @Mock
    private ClientProperties clientProperties;
    @Mock
    private HomeLocator homeLocator;

    @Test
    public void testGetBaseUrlCanonical() throws Exception
    {
        assertUrl("https://www.mycrowd.test/crowd").withRequest().forMode(UrlMode.CANONICAL).now();
    }

    @Test
    public void testGetBaseUrlCanonical_noRequest() throws Exception
    {
        assertUrl("https://www.mycrowd.test/crowd").forMode(UrlMode.CANONICAL).now();
    }

    @Test
    public void testGetBaseUrlAbsolute() throws Exception
    {
        assertUrl("http://my.server.test:8080/mycrowd").withRequest().forMode(UrlMode.ABSOLUTE).now();
    }

    @Test
    public void testGetBaseUrlAbsolute_noRequest() throws Exception
    {
        assertUrl("https://www.mycrowd.test/crowd").forMode(UrlMode.ABSOLUTE).now();
    }

    @Test
    public void testGetBaseUrlRelative() throws Exception
    {
        assertUrl("/mycrowd").withRequest().forMode(UrlMode.RELATIVE).now();
    }

    @Test
    public void testGetBaseUrlRelative_noRequest() throws Exception
    {
        assertUrl("/crowd").forMode(UrlMode.RELATIVE).now();
    }

    @Test
    public void testGetBaseUrlRelativeCanonical() throws Exception
    {
        assertUrl("/crowd").withRequest().forMode(UrlMode.RELATIVE_CANONICAL).now();
    }

    @Test
    public void testGetBaseUrlRelativeCanonical_noRequest() throws Exception
    {
        assertUrl("/crowd").forMode(UrlMode.RELATIVE_CANONICAL).now();
    }

    @Test
    public void testGetBaseUrlAuto() throws Exception
    {
        assertUrl("/mycrowd").withRequest().forMode(UrlMode.AUTO).now();
    }

    @Test
    public void testGetBaseUrlAuto_noRequest() throws Exception
    {
        assertUrl("https://www.mycrowd.test/crowd").forMode(UrlMode.AUTO).now();
    }

    @After
    public void tearDown()
    {
        ExecutingHttpRequest.clear();
    }

    private Fixture assertUrl(String url)
    {
        return new Fixture(url);
    }

    private class Fixture
    {
        private String expectedUrl;
        private boolean withRequest = false;
        private UrlMode mode;

        public Fixture(String url)
        {
            this.expectedUrl = url;
        }

        public Fixture withRequest()
        {
            withRequest = true;
            return this;
        }

        public Fixture forMode(UrlMode mode)
        {
            this.mode = mode;
            return this;
        }

        private void setRequestUrl()
        {
            final HttpServletRequest request = mock(HttpServletRequest.class);
            when(request.getScheme()).thenReturn("http");
            when(request.getServerName()).thenReturn("my.server.test");
            when(request.getServerPort()).thenReturn(8080);
            when(request.getContextPath()).thenReturn("/mycrowd");

            ExecutingHttpRequest.set(request, mock(HttpServletResponse.class));
        }

        public void now()
        {
            when(clientProperties.getBaseURL()).thenReturn("https://www.mycrowd.test/crowd");
            if (withRequest)
            {
                setRequestUrl();
            }

            assertThat(crowdApplicationProperties.getBaseUrl(mode), is(expectedUrl));
        }
    }
}