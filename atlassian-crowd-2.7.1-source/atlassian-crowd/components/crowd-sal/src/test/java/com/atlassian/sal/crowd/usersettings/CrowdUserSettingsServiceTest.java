package com.atlassian.sal.crowd.usersettings;

import com.atlassian.crowd.embedded.api.ApplicationFactory;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;
import com.atlassian.fugue.Option;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.usersettings.UserSettings;
import com.atlassian.sal.api.usersettings.UserSettingsBuilder;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Set;
import javax.annotation.Nullable;

import static com.atlassian.sal.api.usersettings.UserSettingsService.USER_SETTINGS_PREFIX;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class CrowdUserSettingsServiceTest
{
    @Mock
    private ApplicationFactory applicationFactory;
    @Mock
    private ApplicationService applicationService;
    @Mock
    private Application application;

    private CrowdUserSettingsService crowdUserSettingsService;
    private UserTemplateWithAttributes userTemplateWithAttributes;

    @Before
    public void setUp()
    {
        when(applicationFactory.getApplication()).thenReturn(application);
        crowdUserSettingsService = new CrowdUserSettingsService(applicationService, applicationFactory);

        userTemplateWithAttributes = new UserTemplateWithAttributes("admin", 1l);
        userTemplateWithAttributes.setAttribute(USER_SETTINGS_PREFIX + "favourite-colour", ImmutableSet.of("green"));
    }

    @Test
    public void getUserSettings() throws Exception
    {
        userTemplateWithAttributes.setAttribute(USER_SETTINGS_PREFIX + "favourite-colour", ImmutableSet.of("green"));

        when(applicationService.findUserWithAttributesByName(application, "admin")).thenReturn(userTemplateWithAttributes);

        UserSettings admin = crowdUserSettingsService.getUserSettings("admin");
        assertNotNull(admin);
        assertFalse(admin.getKeys().isEmpty());

        Option<String> stringOption = admin.getString("favourite-colour");
        assertTrue(stringOption.isDefined());
        assertEquals(Option.some("green"), stringOption);
    }

    @Test
    public void updateUserSettings() throws Exception
    {
        when(applicationService.findUserWithAttributesByName(application, "admin")).thenReturn(userTemplateWithAttributes);

        crowdUserSettingsService.updateUserSettings("admin", new Function<UserSettingsBuilder, UserSettings>()
        {
            @Override
            public UserSettings apply(@Nullable UserSettingsBuilder input)
            {
                input.put("favourite-food", "hotdogs");
                return input.build();
            }
        });

        verify(applicationService, times(1)).storeUserAttributes(application, "admin", ImmutableMap
                .<String, Set<String>>of(USER_SETTINGS_PREFIX + "favourite-colour", ImmutableSet.of("green"), USER_SETTINGS_PREFIX + "favourite-food", ImmutableSet.of("hotdogs")));
    }

    @Test
    public void addUserSettingsOfTypeBooleanAndString() throws Exception
    {
        when(applicationService.findUserWithAttributesByName(application, "admin")).thenReturn(userTemplateWithAttributes);

        crowdUserSettingsService.updateUserSettings("admin", new Function<UserSettingsBuilder, UserSettings>()
        {
            @Override
            public UserSettings apply(@Nullable UserSettingsBuilder input)
            {
                input.put("loves-afl", Boolean.TRUE);
                return input.build();
            }
        });

        verify(applicationService, times(1)).storeUserAttributes(application, "admin", ImmutableMap
                .<String, Set<String>>of(USER_SETTINGS_PREFIX + "favourite-colour", ImmutableSet.of("green"), USER_SETTINGS_PREFIX + "loves-afl", ImmutableSet.of("true")));
    }

    @Test
    public void addUserSettingsOfTypeLongAndString() throws Exception
    {
        when(applicationService.findUserWithAttributesByName(application, "admin")).thenReturn(userTemplateWithAttributes);

        crowdUserSettingsService.updateUserSettings("admin", new Function<UserSettingsBuilder, UserSettings>()
        {
            @Override
            public UserSettings apply(@Nullable UserSettingsBuilder input)
            {
                input.put("favourite-number", 10L);
                return input.build();
            }
        });

        verify(applicationService, times(1)).storeUserAttributes(application, "admin", ImmutableMap
                .<String, Set<String>>of(USER_SETTINGS_PREFIX + "favourite-colour", ImmutableSet.of("green"), USER_SETTINGS_PREFIX + "favourite-number", ImmutableSet.of("10")));
    }

    @Test (expected = UnsupportedOperationException.class)
    public void getUserSettingsWithKeyIsUnsupported()
    {
        crowdUserSettingsService.getUserSettings(new UserKey("admin"));
    }

    @Test (expected = UnsupportedOperationException.class)
    public void getUpdateSettingsWithKeyIsUnsupported()
    {
        crowdUserSettingsService.updateUserSettings(new UserKey("admin"), new Function<UserSettingsBuilder, UserSettings>()
        {
            @Override
            public UserSettings apply(@Nullable UserSettingsBuilder input)
            {
                input.put("favourite-number", 10L);
                return input.build();
            }
        });
    }
}
