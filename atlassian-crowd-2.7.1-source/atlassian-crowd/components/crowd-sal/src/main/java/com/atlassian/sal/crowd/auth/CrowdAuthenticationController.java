package com.atlassian.sal.crowd.auth;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.exception.ApplicationNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationManager;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.model.application.Application;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.service.UserService;
import com.atlassian.sal.api.auth.AuthenticationController;

import com.google.common.base.Preconditions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Implementation of {@link AuthenticationController}
 *
 * @since v2.7
 */
public class CrowdAuthenticationController implements AuthenticationController
{
    private static final Logger log = LoggerFactory.getLogger(CrowdAuthenticationController.class);
    private final UserService userService;
    private final ApplicationService applicationService;
    private final ApplicationManager applicationManager;

    public CrowdAuthenticationController(final UserService userService, final ApplicationService applicationService, final ApplicationManager applicationManager)
    {
        this.userService = userService;
        this.applicationService = checkNotNull(applicationService);
        this.applicationManager = checkNotNull(applicationManager);
    }

    @Override
    public boolean shouldAttemptAuthentication(final HttpServletRequest request)
    {
        return userService.getAuthenticatedUsername(request) == null;
    }

    @Override
    public boolean canLogin(final Principal principal, final HttpServletRequest request)
    {
        try
        {
            final Application application = applicationManager.findByName(ApplicationType.CROWD.getDisplayName());
            return applicationService.isUserAuthorised(application, principal.getName());

        }
        catch (ApplicationNotFoundException e)
        {
            log.error("Failed to retrieve the 'Crowd' application", e);
            return false;
        }
    }
}
