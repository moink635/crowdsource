package com.atlassian.sal.crowd.lifecycle;

import com.atlassian.config.util.BootstrapUtils;
import com.atlassian.plugin.event.PluginEventManager;
import com.atlassian.sal.core.lifecycle.DefaultLifecycleManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CrowdLifecycleManager extends DefaultLifecycleManager
{
    private static final Logger log = LoggerFactory.getLogger(CrowdLifecycleManager.class);

    public CrowdLifecycleManager(PluginEventManager pluginEventManager)
    {
        super(pluginEventManager);
    }

    public boolean isApplicationSetUp()
	{
		return BootstrapUtils.getBootstrapManager().isSetupComplete();
	}

    public void afterRestore()
    {
        log.info("Notifying LifecycleAware listeners after restore.");
        notifyOnStart();
    }
}
