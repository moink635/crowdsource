package com.atlassian.sal.crowd.lifecycle;

import com.atlassian.config.lifecycle.events.ApplicationStartedEvent;
import com.atlassian.crowd.event.migration.XMLRestoreFinishedEvent;
import com.atlassian.event.api.EventListener;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Listens to {@link ApplicationStartedEvent} and
 * {@link XMLRestoreFinishedEvent} and notifies lifecycle manager.
 */
public class ApplicationReadyListener
{
    private final CrowdLifecycleManager lifecycleManager;

    public ApplicationReadyListener(final CrowdLifecycleManager lifecycleManager)
    {
        this.lifecycleManager = checkNotNull(lifecycleManager);
    }

    @EventListener
    public void handleEvent(final ApplicationStartedEvent event)
    {
        lifecycleManager.start();
    }

    @EventListener
    public void afterRestore(final XMLRestoreFinishedEvent event)
    {
        lifecycleManager.afterRestore();
    }
}
