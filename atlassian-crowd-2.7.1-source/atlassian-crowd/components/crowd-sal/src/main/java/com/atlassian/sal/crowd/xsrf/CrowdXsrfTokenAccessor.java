package com.atlassian.sal.crowd.xsrf;

import com.atlassian.crowd.xwork.XsrfTokenGenerator;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Crowd specific implementation of XsrfTokenAccessor.
 *
 * @since v2.3.2
 */
public class CrowdXsrfTokenAccessor implements XsrfTokenAccessor
{
    private final XsrfTokenGenerator xsrfTokenGenerator;

    public CrowdXsrfTokenAccessor(XsrfTokenGenerator xsrfTokenGenerator)
    {
        this.xsrfTokenGenerator = xsrfTokenGenerator;
    }

    @Override
    public String getXsrfToken(HttpServletRequest request, HttpServletResponse response, boolean create)
    {
        return xsrfTokenGenerator.getToken(request, create);
    }
}
