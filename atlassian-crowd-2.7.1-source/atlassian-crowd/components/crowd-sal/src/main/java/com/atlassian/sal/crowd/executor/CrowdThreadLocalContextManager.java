package com.atlassian.sal.crowd.executor;

import com.atlassian.crowd.manager.threadlocal.ThreadLocalStateAccessor;
import com.atlassian.crowd.manager.threadlocal.ThreadLocalState;
import com.atlassian.sal.api.executor.ThreadLocalContextManager;

/**
 * Crowd thread local context manager.
 *
 * @since 2.0.0
 */
public class CrowdThreadLocalContextManager implements ThreadLocalContextManager<ThreadLocalState>
{
    private final ThreadLocalStateAccessor threadLocalStateAccessor;

    public CrowdThreadLocalContextManager(final ThreadLocalStateAccessor threadLocalStateAccessor)
    {
        this.threadLocalStateAccessor = threadLocalStateAccessor;
    }

    public ThreadLocalState getThreadLocalContext()
    {
        return threadLocalStateAccessor.getState();
    }

    public void setThreadLocalContext(ThreadLocalState context)
    {
        threadLocalStateAccessor.setState(context);
    }

    public void clearThreadLocalContext()
    {
        threadLocalStateAccessor.clearState();
    }
}
