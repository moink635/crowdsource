package com.atlassian.sal.crowd;

import com.atlassian.config.HomeLocator;
import com.atlassian.crowd.model.application.ApplicationType;
import com.atlassian.crowd.plugin.web.ExecutingHttpRequest;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.util.build.BuildUtils;
import com.atlassian.fugue.Option;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.google.common.base.Supplier;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

public class CrowdApplicationProperties implements ApplicationProperties
{
    private final ClientProperties clientProperties;
    private final HomeLocator homeLocator;

    private final Supplier<String> CANONICAL_BASE_URL_SUPPLIER = new Supplier<String>()
    {
        @Override
        public String get()
        {
            return getCanonicalBaseUrl();
        }
    };

    private final Supplier<String> CANONICAL_CONTEXT_PATH_SUPPLIER = new Supplier<String>()
    {
        @Override
        public String get()
        {
            return getCanonicalContextPath();
        }
    };

    public CrowdApplicationProperties(final ClientProperties clientProperties, HomeLocator homeLocator)
    {
        this.clientProperties = clientProperties;
        this.homeLocator = homeLocator;
    }

    public String getBaseUrl()
    {
        return clientProperties.getBaseURL();
    }

    public String getBaseUrl(final UrlMode urlMode)
    {
        switch (urlMode)
        {
            case CANONICAL:
                // The configured base URL
                return getCanonicalBaseUrl();
            case ABSOLUTE:
                // The base URL of the request, or the configured base URL if not request
                return getBaseUrlFromRequest().getOrElse(CANONICAL_BASE_URL_SUPPLIER);
            case RELATIVE:
                // Context path of the request, or context path of configured base URL
                return getContextPathFromRequest().getOrElse(CANONICAL_CONTEXT_PATH_SUPPLIER);
            case RELATIVE_CANONICAL:
                // Context path of configured base URL
                return getCanonicalContextPath();
            case AUTO:
                // relative URL of the request, or base URL otherwise
                return getContextPathFromRequest().getOrElse(CANONICAL_BASE_URL_SUPPLIER);
            default:
                throw new UnsupportedOperationException("Not implemented yet");
        }
    }

    private String getCanonicalBaseUrl()
    {
        return getBaseUrl();
    }

    private String getCanonicalContextPath()
    {
        final String baseUrl = getCanonicalBaseUrl();
        try
        {
            return new URL(baseUrl).getPath();
        }
        catch (MalformedURLException e)
        {
            throw new IllegalStateException("Base URL misconfigured", e);
        }
    }

    private Option<String> getBaseUrlFromRequest()
    {
        final HttpServletRequest request = ExecutingHttpRequest.get();
        return request != null ? Option.some(constructBaseUrl(request)) : Option.<String>none();
    }

    private static String constructBaseUrl(HttpServletRequest request)
    {
        StringBuilder baseUrl = new StringBuilder(32);
        baseUrl.append(request.getScheme());
        baseUrl.append("://");
        baseUrl.append(request.getServerName());
        if (request.getServerPort() != 80)
        {
            baseUrl.append(":");
            baseUrl.append(request.getServerPort());
        }
        baseUrl.append(request.getContextPath());
        return baseUrl.toString();
    }

    private Option<String> getContextPathFromRequest()
    {
        final HttpServletRequest request = ExecutingHttpRequest.get();
        return request != null ? Option.some(request.getContextPath()) : Option.<String>none();
    }

    public String getDisplayName()
    {
        return ApplicationType.CROWD.getDisplayName();
    }

    public String getVersion()
    {
        return BuildUtils.BUILD_VERSION;
    }

    public Date getBuildDate()
    {
        return BuildUtils.getCurrentBuildDate();
    }

    public String getBuildNumber()
    {
        return BuildUtils.BUILD_NUMBER;
    }

    public File getHomeDirectory()
    {
        String path = homeLocator.getHomePath();
        if (path != null)
        {
            return new File(path);
        }
        else
        {
            return null;
        }
    }

    @Override
    public String getPropertyValue(final String key)
    {
        return null;
    }
}
