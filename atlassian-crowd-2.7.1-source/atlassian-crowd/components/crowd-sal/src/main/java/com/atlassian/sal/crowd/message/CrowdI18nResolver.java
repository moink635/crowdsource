package com.atlassian.sal.crowd.message;

import com.atlassian.crowd.util.I18nHelper;
import com.atlassian.crowd.util.I18nHelperConfiguration;
import com.atlassian.sal.api.component.ComponentLocator;
import com.atlassian.sal.core.message.AbstractI18nResolver;

import java.io.Serializable;
import java.util.Locale;
import java.util.Map;

/**
 * Crowd i18n resolver
 *
 * @since 2.0.0
 */
public class CrowdI18nResolver extends AbstractI18nResolver
{
    private final I18nHelper helper;
    private final I18nHelperConfiguration i18nHelperConfiguration;

    public CrowdI18nResolver(I18nHelper helper, I18nHelperConfiguration i18nHelperConfiguration)
    {
        this.helper = helper;
        this.i18nHelperConfiguration = i18nHelperConfiguration;
    }

    @Override
    public String getRawText(final String key)
    {
        return helper.getUnescapedText(key);
    }

    @Override
    public String resolveText(String key, Serializable[] arguments)
    {
        return helper.getText(key, arguments);
    }

    @Override
    public String resolveText(Locale locale, String key, Serializable... arguments)
    {
        return helper.getText(i18nHelperConfiguration.getLocale(), key, arguments);
    }

    @Override
    public Map<String, String> getAllTranslationsForPrefix(String prefix)
    {
        return helper.getAllTranslationsForPrefix(prefix);
    }

    @Override
    public Map<String, String> getAllTranslationsForPrefix(final String prefix, final Locale locale)
    {
        return helper.getAllTranslationsForPrefix(prefix);
    }
}
