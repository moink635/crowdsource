package com.atlassian.sal.crowd.license;

import com.atlassian.sal.api.license.LicenseHandler;
import com.atlassian.crowd.manager.license.CrowdLicenseManager;
import com.atlassian.extras.api.crowd.CrowdLicense;

/**
 * Crowd license handler that stores the license
 *
 * @since 2.0
 */
public class CrowdLicenseHandler implements LicenseHandler
{
    private final CrowdLicenseManager licenseManager;

    public CrowdLicenseHandler(CrowdLicenseManager licenseManager)
    {
        this.licenseManager = licenseManager;
    }

    public void setLicense(String license)
    {
        // @TODO if license is barfed throw new IllegalArgumentException("Specified license was invalid.");

        licenseManager.storeLicense(license);
    }

    @Override
    public String getServerId()
    {
        CrowdLicense l = licenseManager.getLicense();
        if (l != null)
        {
            return l.getServerId();
        }
        else
        {
            return null;
        }
    }

    @Override
    public String getSupportEntitlementNumber()
    {
        CrowdLicense l = licenseManager.getLicense();
        if (l != null)
        {
            return l.getSupportEntitlementNumber();
        }
        else
        {
            return null;
        }
    }
}
