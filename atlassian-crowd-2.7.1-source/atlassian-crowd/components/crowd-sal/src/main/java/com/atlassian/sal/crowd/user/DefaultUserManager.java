package com.atlassian.sal.crowd.user;

import com.atlassian.crowd.embedded.api.CrowdService;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.search.builder.Restriction;
import com.atlassian.crowd.search.query.entity.GroupQuery;
import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.search.query.entity.restriction.constants.GroupTermKeys;
import com.atlassian.crowd.service.UserService;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.sal.api.user.UserResolutionException;
import com.atlassian.sal.api.web.context.HttpContext;
import org.apache.commons.lang.StringUtils;

import java.net.URI;
import java.security.Principal;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;

/**
 * Crowd implementation of the UserManager
 */
public class DefaultUserManager implements UserManager
{
    private final UserService userService;
    private final CrowdService crowdService;
    private final HttpContext httpContext;

    public DefaultUserManager(final UserService userService, CrowdService crowdService, final HttpContext httpContext)
    {
        this.userService = userService;
        this.crowdService = crowdService;
        this.httpContext = httpContext;
    }

    public String getRemoteUsername()
    {
        return userService.getAuthenticatedUsername(httpContext.getRequest());
    }

    @Nullable
    @Override
    public UserProfile getRemoteUser()
    {
        com.atlassian.crowd.embedded.api.User user = crowdService.getUser(getRemoteUsername());
        return user != null ? new CrowdUserProfile(user) : null;
    }

    @Nullable
    @Override
    public UserKey getRemoteUserKey()
    {
        throw new UnsupportedOperationException("UserKey is currently not implemented in Crowd");
    }

    public boolean isSystemAdmin(final String username)
    {
        return userService.isSystemAdmin(username);
    }

    @Override
    public boolean isSystemAdmin(@Nullable UserKey userKey)
    {
        throw new UnsupportedOperationException("UserKey is currently not implemented in Crowd");
    }

    public boolean isAdmin(String username)
    {
        return userService.isSystemAdmin(username);
    }

    @Override
    public boolean isAdmin(@Nullable UserKey userKey)
    {
        throw new UnsupportedOperationException("UserKey is currently not implemented in Crowd");
    }

    public boolean isUserInGroup(final String username, final String group)
    {
        return userService.isUserInGroup(username, group);
    }

    @Override
    public boolean isUserInGroup(@Nullable UserKey userKey, @Nullable String group)
    {
        throw new UnsupportedOperationException("UserKey is currently not implemented in Crowd");
    }

    public boolean authenticate(final String username, final String password)
    {
        return userService.authenticate(username, password);
    }

    public Principal resolve(final String username) throws UserResolutionException
    {
        try
        {
            return userService.resolve(username);
        }
        catch (org.springframework.dao.DataAccessException e)
        {
            throw new UserResolutionException("Failed to find user with name <" + username + ">", e);
        }
    }

    @Override
    public Iterable<String> findGroupNamesByPrefix(String prefix, int startIndex, int maxResults)
    {
        GroupQuery<String> groupQuery = new GroupQuery<String>(String.class,
                GroupType.GROUP,
                StringUtils.isBlank(prefix) ? NullRestrictionImpl.INSTANCE : Restriction.on(GroupTermKeys.NAME).startingWith(prefix),
                startIndex,
                maxResults);

        return crowdService.search(groupQuery);
    }

    public String getRemoteUsername(final HttpServletRequest request)
    {
        return userService.getAuthenticatedUsername(request);
    }

    @Nullable
    @Override
    public UserProfile getRemoteUser(HttpServletRequest httpServletRequest)
    {
        return getRemoteUser();
    }

    @Nullable
    @Override
    public UserKey getRemoteUserKey(HttpServletRequest httpServletRequest)
    {
        throw new UnsupportedOperationException("UserKey is currently not implemented in Crowd");
    }

    public UserProfile getUserProfile(String username)
    {
        final Principal userPrincipal = userService.resolve(username);

        if (userPrincipal == null)
        {
            return null;
        }
        else if (userPrincipal instanceof User)
        {
            User user = (User) userPrincipal;
            return new CrowdUserProfile(user.getName(), user.getDisplayName(), user.getEmailAddress());
        }
        else
        {
            return new CrowdUserProfile(userPrincipal.getName(), null, null);
        }
    }

    @Nullable
    @Override
    public UserProfile getUserProfile(@Nullable UserKey userKey)
    {
        throw new UnsupportedOperationException("UserKey is currently not implemented in Crowd");
    }

    private static class CrowdUserProfile implements UserProfile
    {
        private final String userName;
        private final String fullName;
        private final String email;

        private CrowdUserProfile(String userName, String fullName, String email)
        {
            this.userName = userName;
            this.fullName = fullName;
            this.email = email;
        }

        public CrowdUserProfile(com.atlassian.crowd.embedded.api.User user)
        {
            this(user.getName(), user.getDisplayName(), user.getEmailAddress());
        }

        @Override
        public UserKey getUserKey()
        {
            throw new UnsupportedOperationException("UserKey is currently not implemented in Crowd");
        }

        public String getUsername()
        {
            return userName;
        }

        public String getFullName()
        {
            return fullName;
        }

        public String getEmail()
        {
            return email;
        }

        public URI getProfilePictureUri(int width, int height)
        {
            return null;
        }

        public URI getProfilePictureUri()
        {
            return null;
        }

        public URI getProfilePageUri()
        {
            return null;
        }
    }
}