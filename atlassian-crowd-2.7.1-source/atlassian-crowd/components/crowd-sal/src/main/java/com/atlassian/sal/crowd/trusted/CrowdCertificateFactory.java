package com.atlassian.sal.crowd.trusted;

import com.atlassian.sal.core.trusted.CertificateFactory;
import com.atlassian.security.auth.trustedapps.EncryptedCertificate;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsManager;

public class CrowdCertificateFactory implements CertificateFactory
{
    private final TrustedApplicationsManager trustedApplicationsManager;

    public CrowdCertificateFactory(final TrustedApplicationsManager trustedApplicationsManager)
    {
        this.trustedApplicationsManager = trustedApplicationsManager;
    }

    public EncryptedCertificate createCertificate(final String username)
    {
        return trustedApplicationsManager.getCurrentApplication().encode(username, null);
    }
}
