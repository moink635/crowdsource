package com.atlassian.sal.crowd.xsrf;

import com.atlassian.crowd.xwork.XsrfTokenGenerator;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;

import javax.servlet.http.HttpServletRequest;

/**
 * Crowd specific implementation of XsrfTokenValidator.
 *
 * @since v2.3.2
 */
public class CrowdXsrfTokenValidator implements XsrfTokenValidator
{
    private final XsrfTokenGenerator xsrfTokenGenerator;

    public CrowdXsrfTokenValidator(XsrfTokenGenerator xsrfTokenGenerator)
    {
        this.xsrfTokenGenerator = xsrfTokenGenerator;
    }

    @Override
    public boolean validateFormEncodedToken(HttpServletRequest request)
    {
        final String parameterTokenValue = request.getParameter(getXsrfParameterName());
        return xsrfTokenGenerator.validateToken(request, parameterTokenValue);
    }

    @Override
    public String getXsrfParameterName()
    {
        return xsrfTokenGenerator.getXsrfTokenName();
    }
}
