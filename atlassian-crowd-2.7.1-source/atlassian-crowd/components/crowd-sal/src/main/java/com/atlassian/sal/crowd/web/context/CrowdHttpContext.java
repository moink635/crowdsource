package com.atlassian.sal.crowd.web.context;

import com.atlassian.crowd.plugin.web.ExecutingHttpRequest;
import com.atlassian.sal.api.web.context.HttpContext;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Crowd implementation of {@link HttpContext}.
 *
 * @since v2.7
 */
public class CrowdHttpContext implements HttpContext
{
    @Nullable
    @Override
    public HttpServletRequest getRequest()
    {
        return ExecutingHttpRequest.get();
    }

    @Nullable
    @Override
    public HttpServletResponse getResponse()
    {
        return ExecutingHttpRequest.getResponse();
    }

    @Nullable
    @Override
    public HttpSession getSession(boolean createSession)
    {
        final HttpServletRequest request = getRequest();
        if (request != null)
        {
            return request.getSession(createSession);
        }
        else
        {
            return null;
        }
    }
}
