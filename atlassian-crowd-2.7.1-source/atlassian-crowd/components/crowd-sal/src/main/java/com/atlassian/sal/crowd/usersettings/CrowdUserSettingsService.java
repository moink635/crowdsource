package com.atlassian.sal.crowd.usersettings;

import com.atlassian.crowd.embedded.api.ApplicationFactory;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.manager.application.ApplicationService;
import com.atlassian.crowd.model.user.UserTemplateWithAttributes;
import com.atlassian.crowd.model.user.UserWithAttributes;
import com.atlassian.fugue.Option;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.usersettings.UserSettings;
import com.atlassian.sal.api.usersettings.UserSettingsBuilder;
import com.atlassian.sal.api.usersettings.UserSettingsService;
import com.atlassian.sal.core.usersettings.DefaultUserSettings;
import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;

import java.util.Set;
import javax.annotation.Nullable;

import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;

public class CrowdUserSettingsService implements UserSettingsService
{
    private final ApplicationFactory applicationFactory;
    private final ApplicationService applicationService;

    private static final Function<String, String> PREFIX_STRIPPING_FUNCTION = new Function<String, String>()
    {
        @Override
        public String apply(String input)
        {
            return input.substring(USER_SETTINGS_PREFIX.length());
        }
    };

    private static final Predicate<String> USER_SETTING_PREDICATE = new Predicate<String>()
    {
        @Override
        public boolean apply(@Nullable String input)
        {
            return input != null && input.startsWith(USER_SETTINGS_PREFIX);
        }
    };

    public CrowdUserSettingsService(ApplicationService applicationService, ApplicationFactory applicationFactory)
    {
        this.applicationService = applicationService;
        this.applicationFactory = applicationFactory;
    }

    @Override
    public UserSettings getUserSettings(String username)
    {
        return newUserSettingsBuilder(username).build();
    }

    @Override
    public UserSettings getUserSettings(UserKey userKey)
    {
        throw new UnsupportedOperationException("UserKey based implementation not implemented");
    }

    @Override
    public void updateUserSettings(String username, Function<UserSettingsBuilder, UserSettings> updateFunction)
    {
        updateFunction.apply(newUserSettingsBuilder(username));
    }

    @Override
    public void updateUserSettings(UserKey userKey, Function<UserSettingsBuilder, UserSettings> updateFunction)
    {
        throw new UnsupportedOperationException("UserKey based implementation not implemented");
    }

    private static UserSettings buildUserSettings(UserWithAttributes userWithAttributes)
    {
        UserSettingsBuilder settings = DefaultUserSettings.builder();

        final Iterable<String> settingKeys = transform(filter(userWithAttributes.getKeys(), USER_SETTING_PREDICATE), PREFIX_STRIPPING_FUNCTION);

        for (String settingKey : settingKeys)
        {
            settings.put(settingKey, userWithAttributes.getValue(USER_SETTINGS_PREFIX + settingKey));
        }
        return settings.build();
    }

    class CrowdUserSettingsBuilder implements UserSettingsBuilder
    {
        private final UserTemplateWithAttributes userWithAttributes;

        public CrowdUserSettingsBuilder(com.atlassian.crowd.model.user.UserWithAttributes userWithAttributes)
        {
            this.userWithAttributes = new UserTemplateWithAttributes(userWithAttributes);
        }

        @Override
        public UserSettingsBuilder put(String key, String value)
        {
            checkArgumentKey(key);
            checkArgumentValue(value);
            userWithAttributes.setAttribute(USER_SETTINGS_PREFIX + key, value);
            updateUserAttributes();
            return this;
        }

        private void updateUserAttributes()
        {
            try
            {
                applicationService.storeUserAttributes(applicationFactory.getApplication(), userWithAttributes.getName(), userWithAttributes.getAttributes());
            }
            catch (Exception e)
            {
                throw new RuntimeException("Failed to update user attributes", e);
            }
        }

        @Override
        public UserSettingsBuilder put(String key, boolean value)
        {
            checkArgumentKey(key);
            userWithAttributes.setAttribute(USER_SETTINGS_PREFIX + key, String.valueOf(value));
            updateUserAttributes();
            return this;
        }

        @Override
        public UserSettingsBuilder put(String key, long value)
        {
            checkArgumentKey(key);
            userWithAttributes.setAttribute(USER_SETTINGS_PREFIX + key, String.valueOf(value));
            updateUserAttributes();
            return this;
        }

        @Override
        public UserSettingsBuilder remove(String key)
        {
            checkArgumentKey(key);
            userWithAttributes.removeAttribute(USER_SETTINGS_PREFIX + key);
            return this;
        }

        @Override
        public Option<Object> get(String key)
        {
            checkArgumentKey(key);
            return Option.<Object>some(userWithAttributes.getValue(USER_SETTINGS_PREFIX + key));
        }

        @Override
        public Set<String> getKeys()
        {
            return ImmutableSet.copyOf(transform(userWithAttributes.getKeys(), PREFIX_STRIPPING_FUNCTION));
        }

        @Override
        public UserSettings build()
        {
            return buildUserSettings(userWithAttributes);
        }

        private void checkArgumentKey(String key)
        {
            Preconditions.checkArgument(key != null, "key cannot be null");
            Preconditions.checkArgument(key.length() <= MAX_KEY_LENGTH, "key cannot be longer than %s characters", MAX_KEY_LENGTH);
        }

        private void checkArgumentValue(String value)
        {
            Preconditions.checkArgument(value != null, "value cannot be null");
            Preconditions.checkArgument(value.length() <= MAX_STRING_VALUE_LENGTH, "value cannot be longer than %s characters", MAX_STRING_VALUE_LENGTH);
        }
    }

    private UserSettingsBuilder newUserSettingsBuilder(String username)
    {
        try
        {
            return new CrowdUserSettingsBuilder(applicationService.findUserWithAttributesByName(applicationFactory.getApplication(), username));
        }
        catch (UserNotFoundException e)
        {
            throw new IllegalArgumentException("No user exists with the username " + username);
        }
    }
}