package com.atlassian.sal.crowd.auth;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.crowd.service.UserService;
import com.atlassian.sal.api.auth.AuthenticationListener;
import com.atlassian.sal.api.auth.Authenticator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Crowd implementation of {@link AuthenticationListener}
 *
 * @since v2.7
 */
public class CrowdAuthenticationListener implements AuthenticationListener
{
    private static final Logger log = LoggerFactory.getLogger(CrowdAuthenticationListener.class);

    private final UserService userService;

    public CrowdAuthenticationListener(final UserService userService)
    {
        this.userService = checkNotNull(userService);
    }

    public void authenticationSuccess(final Authenticator.Result result, final HttpServletRequest request, final HttpServletResponse response)
    {
        final String username = result.getPrincipal().getName();
        if (username != null)
        {
            if (!userService.setAuthenticatedUser(username))
            {
                log.warn("Error while authenticating user '{}'", username);
            }
        }
    }

    public void authenticationFailure(final Authenticator.Result result, final HttpServletRequest request, final HttpServletResponse response)
    {
    }

    public void authenticationError(final Authenticator.Result result, final HttpServletRequest request, final HttpServletResponse response)
    {
    }

    public void authenticationNotAttempted(final HttpServletRequest request, final HttpServletResponse response)
    {
    }
}
