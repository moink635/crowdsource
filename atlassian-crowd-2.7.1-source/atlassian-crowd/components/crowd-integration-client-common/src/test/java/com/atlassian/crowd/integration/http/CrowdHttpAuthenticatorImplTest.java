package com.atlassian.crowd.integration.http;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.integration.http.util.CrowdHttpTokenHelper;
import com.atlassian.crowd.integration.http.util.CrowdHttpValidationFactorExtractor;
import com.atlassian.crowd.model.authentication.CookieConfiguration;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.service.client.ClientProperties;
import com.atlassian.crowd.service.client.CrowdClient;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CrowdHttpAuthenticatorImplTest
{
    private CrowdHttpAuthenticatorImpl crowdHttpAuthenticatorImpl;

    @Mock private CrowdClient client;
    @Mock private ClientProperties clientProperties;
    @Mock private CrowdHttpTokenHelper tokenHelper;
    @Mock private CrowdHttpValidationFactorExtractor crowdHttpValidationFactorExtractor;
    @Mock private ValidationFactor validationFactor;
    private List<ValidationFactor> validationFactors = Arrays.asList(validationFactor);

    private MockHttpServletRequest httpServletRequest;
    private MockHttpServletResponse httpServletResponse;

    @Before
    public void setUp()
    {
        httpServletRequest = new MockHttpServletRequest();
        httpServletResponse = new MockHttpServletResponse();
        crowdHttpAuthenticatorImpl = new CrowdHttpAuthenticatorImpl(client, clientProperties, tokenHelper);
    }

    @Test
    public void testIsAuthenticatedDoesNotCreateSessionOnUnauthenticatedRequestWithNoSessionAndNoToken() throws Exception
    {
        when(clientProperties.getCookieTokenKey(null)).thenReturn("name-set-in-properties");

        boolean isAuthenticated = crowdHttpAuthenticatorImpl.isAuthenticated(httpServletRequest, httpServletResponse);
        assertFalse(isAuthenticated);
        assertNull(httpServletRequest.getSession(false));
    }

    @Test
    public void testIsAuthenticatedDoesNotCreateSessionOnUnauthenticatedRequestWithNoSessionAndInvalidToken() throws Exception
    {
        when(tokenHelper.getCrowdToken(any(HttpServletRequest.class), anyString())).thenReturn("someToken");
        when(tokenHelper.getValidationFactorExtractor()).thenReturn(crowdHttpValidationFactorExtractor);
        when(crowdHttpValidationFactorExtractor.getValidationFactors(httpServletRequest)).thenReturn(validationFactors);
        doThrow(new InvalidTokenException()).when(client).validateSSOAuthentication("someToken", validationFactors);
        when(clientProperties.getCookieTokenKey(null)).thenReturn("name-set-in-properties");

        boolean isAuthenticated = crowdHttpAuthenticatorImpl.isAuthenticated(httpServletRequest, httpServletResponse);
        assertFalse(isAuthenticated);
        assertNull(httpServletRequest.getSession(false));
    }

    @Test
    public void tokenHelperIsUsedToSetTokenPropertiesWhenTokenIsValid() throws Exception
    {
        when(tokenHelper.getCrowdToken(any(HttpServletRequest.class), anyString())).thenReturn("someToken");
        when(tokenHelper.getValidationFactorExtractor()).thenReturn(crowdHttpValidationFactorExtractor);
        when(clientProperties.getCookieTokenKey(null)).thenReturn("name-set-in-properties");

        assertTrue(crowdHttpAuthenticatorImpl.isAuthenticated(httpServletRequest, httpServletResponse));
        verify(tokenHelper).setCrowdToken(httpServletRequest, httpServletResponse, "someToken", clientProperties, client.getCookieConfiguration());
    }

    @Test
    public void logoutUsesNameFromConfigurationCookieNameWhenNotSetInProperties() throws Exception
    {
        when(clientProperties.getCookieTokenKey("configured-cookie-name")).thenReturn("configured-cookie-name");

        CookieConfiguration config = new CookieConfiguration("domain", false, "configured-cookie-name");
        when(client.getCookieConfiguration()).thenReturn(config);

        when(tokenHelper.getCrowdToken(httpServletRequest, "configured-cookie-name")).thenReturn("session");

        crowdHttpAuthenticatorImpl.logout(httpServletRequest, httpServletResponse);

        verify(client).invalidateSSOToken("session");
    }

    @Test
    public void getTokenUsesCookieNameWhenConfiguredInProperties() throws Exception
    {
        when(clientProperties.getCookieTokenKey(Mockito.anyString())).thenReturn("name-set-in-properties");
        crowdHttpAuthenticatorImpl.getToken(httpServletRequest);
        verify(tokenHelper).getCrowdToken(httpServletRequest, "name-set-in-properties");
        verify(client, Mockito.never()).getCookieConfiguration();
    }

    @Test
    public void getTokenUsesCookieConfigurationWhenNameNotConfiguredInProperties() throws Exception
    {
        when(clientProperties.getCookieTokenKey(null)).thenReturn(null);
        CookieConfiguration config = new CookieConfiguration("domain", false, "configured-cookie-name");
        when(client.getCookieConfiguration()).thenReturn(config);
        crowdHttpAuthenticatorImpl.getToken(httpServletRequest);
        verify(tokenHelper).getCrowdToken(httpServletRequest, "configured-cookie-name");
    }

    @Test
    public void getTokenFallsBackToDefaultWhenNotConfiguredAndCrowdCallFails() throws Exception
    {
        when(clientProperties.getCookieTokenKey(null)).thenReturn(null);
        when(clientProperties.getCookieTokenKey()).thenReturn("default-when-no-property-set");
        when(client.getCookieConfiguration()).thenThrow(new OperationFailedException());
        crowdHttpAuthenticatorImpl.getToken(httpServletRequest);
        verify(tokenHelper).getCrowdToken(httpServletRequest, "default-when-no-property-set");
    }

    @Test
    public void noTokenCheckIsMadeWhenASessionIsAlreadyValid() throws OperationFailedException
    {
        when(clientProperties.getCookieTokenKey()).thenReturn("name-set-in-properties");
        when(clientProperties.getSessionLastValidation()).thenReturn("session-last-validation");
        when(clientProperties.getSessionValidationInterval()).thenReturn(1L);

        when(tokenHelper.getCrowdToken(httpServletRequest, "name-set-in-properties")).thenThrow(new RuntimeException("Should not check for cookie"));

        httpServletRequest.getSession().setAttribute("session-last-validation", new Date());

        // This method gets the current time; success assumes the clock is read as less than one minute later than the new Date() above.
        assertTrue(crowdHttpAuthenticatorImpl.isAuthenticated(httpServletRequest, httpServletResponse));

        verify(tokenHelper, never()).getCrowdToken(Mockito.<HttpServletRequest>any(), Mockito.anyString());
    }

    @Test
    public void authenticationCheckUsesCookieConfigurationForCookieName() throws Exception
    {
        when(clientProperties.getCookieTokenKey()).thenReturn(null);
        CookieConfiguration config = new CookieConfiguration("domain", false, "configured-cookie-name");
        when(client.getCookieConfiguration()).thenReturn(config);

        when(tokenHelper.getCrowdToken(httpServletRequest, "configured-cookie-name")).thenReturn(null);

        assertFalse(crowdHttpAuthenticatorImpl.isAuthenticated(httpServletRequest, httpServletResponse));

        verify(tokenHelper).getCrowdToken(httpServletRequest, "configured-cookie-name");
    }
}
