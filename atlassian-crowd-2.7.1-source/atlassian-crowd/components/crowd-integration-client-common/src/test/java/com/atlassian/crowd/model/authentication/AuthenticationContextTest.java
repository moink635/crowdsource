package com.atlassian.crowd.model.authentication;

import com.atlassian.crowd.embedded.api.PasswordCredential;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class AuthenticationContextTest
{
    @Test
    public void canBeConstructedWithNullCredential()
    {
        AuthenticationContext context = new AuthenticationContext("name", null, new ValidationFactor[0]){};
        assertNull(context.getCredential());
    }

    @Test
    public void canBeConstructedWithPlaintextCredential()
    {
        AuthenticationContext context = new AuthenticationContext("name", PasswordCredential.unencrypted("password"), new ValidationFactor[0]){};
        assertNotNull(context.getCredential());
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotBeConstructedWithEncryptedCredential()
    {
        AuthenticationContext context = new AuthenticationContext("name", PasswordCredential.encrypted("hash"), new ValidationFactor[0]){};
        assertNotNull(context.getCredential());
    }

    @Test(expected = IllegalArgumentException.class)
    public void cannotHaveEncryptedCredentialSet()
    {
        AuthenticationContext context = new AuthenticationContext("name", PasswordCredential.unencrypted("password"), new ValidationFactor[0]){};
        assertNotNull(context.getCredential());

        context.setCredential(PasswordCredential.encrypted("hash"));
    }
}
