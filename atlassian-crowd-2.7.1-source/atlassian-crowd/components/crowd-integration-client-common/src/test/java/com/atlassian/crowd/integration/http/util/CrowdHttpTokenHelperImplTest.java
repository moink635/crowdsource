package com.atlassian.crowd.integration.http.util;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.crowd.model.authentication.CookieConfiguration;
import com.atlassian.crowd.service.client.ClientProperties;

import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class CrowdHttpTokenHelperImplTest
{
    CrowdHttpTokenHelper helper;
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    ClientProperties clientProperties;
    CookieConfiguration cookieConfig;

    @Before
    public void setUp()
    {
        helper = CrowdHttpTokenHelperImpl.getInstance(null);
        request = new MockHttpServletRequest();
        response = new MockHttpServletResponse();
        clientProperties = mock(ClientProperties.class);
        cookieConfig = new CookieConfiguration("domain", false, "cookie-configuration-name");

        when(clientProperties.getSessionTokenKey()).thenReturn("session");
        when(clientProperties.getCookieTokenKey("cookie-configuration-name")).thenReturn("cookie-configuration-name");
    }

    @Test
    public void removeCrowdTokenUsesCookieConfigAsDefault()
    {
        helper.removeCrowdToken(request, response, clientProperties, cookieConfig);
        verify(clientProperties).getCookieTokenKey("cookie-configuration-name");
    }

    @Test
    public void setCrowdTokenUsesCookieConfigAsDefault()
    {
        when(clientProperties.getSessionLastValidation()).thenReturn("session-last-validation");

        helper.setCrowdToken(request, response, "token", clientProperties, cookieConfig);
        verify(clientProperties).getCookieTokenKey("cookie-configuration-name");
    }

    @Test
    public void setCrowdTokenShouldTakeTheSSODomainNameFromThePropertiesFile()
    {
        when(clientProperties.getSSOCookieDomainName()).thenReturn("atlassian.com");
        when(clientProperties.getSessionLastValidation()).thenReturn("sessionLastValidation");

        helper.setCrowdToken(request, response, "token", clientProperties,cookieConfig);

        assertThat(response.getHeader("Set-Cookie"), containsString("Domain=atlassian.com"));
    }

    @Test
    public void setCrowdTokenShouldSetSSODomainNameFromTheCookieIfPropertiesFileHasNotOverriddenTheDomain()
    {
        when(clientProperties.getSSOCookieDomainName()).thenReturn(null);
        when(clientProperties.getSessionLastValidation()).thenReturn("sessionLastValidation");

        helper.setCrowdToken(request, response, "token", clientProperties, cookieConfig);
        assertThat(response.getHeader("Set-Cookie"), containsString("Domain=domain"));
    }

    @Test
    public void firstMatchingCookieIsUsed()
    {
        Cookie cookie1 = new Cookie("not-token", "first-cookie");
        Cookie cookie2 = new Cookie("token", "second-cookie");
        Cookie cookie3 = new Cookie("token", "third-cookie");

        Cookie[] a = {cookie1, cookie2, cookie3};

        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getCookies()).thenReturn(a);

        assertEquals("second-cookie", helper.getCrowdToken(req, "token"));
    }

    @Test
    public void cookiesWithoutDomainsDoNotCauseFailure()
    {
        Cookie[] cookies = {
                new Cookie("token", "token1"),
                new Cookie("token", "token2")
        };

        HttpServletRequest req = mock(HttpServletRequest.class);
        when(req.getCookies()).thenReturn(cookies);

        assertNotNull(helper.getCrowdToken(req, "token"));
    }
}
