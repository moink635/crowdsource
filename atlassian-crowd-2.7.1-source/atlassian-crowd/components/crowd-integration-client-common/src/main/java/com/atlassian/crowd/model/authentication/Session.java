package com.atlassian.crowd.model.authentication;

import java.util.Date;

/**
 * A Session represents an authenticated, time-bound information exchange opportunity granted by a server to a client.
 * It is created at a certain point in time, and eventually expires, although its expiry date can
 * be renewed if the information exchange is alive.
 *
 * @version 2.6
 */
public interface Session
{
    /**
     * A session has an associated token that is granted by the server, and which can be presented by the client to
     * identify the session.
     * @return the identifier (key) of the token associated to the session.
     */
    String getToken();

    /**
     * @return the date when the session was created.
     */
    Date getCreatedDate();

    /**
     * @return the current expiry date after which the session will no longer be valid.
     */
    Date getExpiryDate();
}
