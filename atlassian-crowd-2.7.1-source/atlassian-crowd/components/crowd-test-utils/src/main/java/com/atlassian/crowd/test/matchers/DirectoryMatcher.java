package com.atlassian.crowd.test.matchers;

import javax.annotation.Nullable;

import com.atlassian.crowd.embedded.api.Directory;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static org.hamcrest.Matchers.is;

public class DirectoryMatcher extends TypeSafeMatcher<Directory>
{
    @Nullable
    private final Matcher<String> implementationClassMatcher;

    public DirectoryMatcher(@Nullable Matcher<String> implementationClassMatcher)
    {
        this.implementationClassMatcher = implementationClassMatcher;
    }

    /**
     * A matcher that matches any directory.
     * @return a matcher which matches any directory
     */
    @Factory
    public static DirectoryMatcher directory()
    {
        return new DirectoryMatcher(null);
    }

    /**
     * Builds a new specialised matcher to match on the implementation class.
     * @param externalIdMatcher matcher which must match the directory's implementation class in order for this
     *                          matcher to match
     * @return a matcher
     */
    public DirectoryMatcher withImplementationClassMatching(Matcher<String> externalIdMatcher)
    {
        return new DirectoryMatcher(externalIdMatcher);
    }

    /**
     * Builds a new specialised matcher to match the given implementation class.
     * @param implementationClass the provided implementation class must equal the directory's implementation class
     *                            in order for this matcher to match
     * @return a matcher
     */
    public DirectoryMatcher withImplementationClassOf(Class implementationClass)
    {
        return withImplementationClassMatching(is(implementationClass.getCanonicalName()));
    }

    @Override
    protected boolean matchesSafely(Directory item)
    {
        return item != null
                && (implementationClassMatcher == null
                    || implementationClassMatcher.matches(item.getImplementationClass()));
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("Directory");
        if (implementationClassMatcher != null)
        {
            description.appendText(" with implementation class ").appendDescriptionOf(implementationClassMatcher);
        }
    }
}
