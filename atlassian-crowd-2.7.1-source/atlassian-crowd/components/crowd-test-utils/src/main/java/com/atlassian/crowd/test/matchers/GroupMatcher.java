package com.atlassian.crowd.test.matchers;

import javax.annotation.Nullable;

import com.atlassian.crowd.model.group.Group;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static org.hamcrest.Matchers.is;

public class GroupMatcher<E extends Group> extends TypeSafeMatcher<E>
{
    @Nullable
    private final Matcher<String> nameMatcher;
    @Nullable
    private final Matcher<Boolean> activeMatcher;

    public GroupMatcher(@Nullable Matcher<String> nameMatcher, @Nullable Matcher<Boolean> activeMatcher)
    {
        this.nameMatcher = nameMatcher;
        this.activeMatcher = activeMatcher;
    }

    /**
     * A matcher that matches any group.
     * @param <T> type of group, to avoid casting
     * @return a matcher which matches any group
     */
    @Factory
    public static <T extends Group> GroupMatcher<T> group()
    {
        return new GroupMatcher<T>(null, null);
    }

    /**
     * A matcher that matches any group; synonymous with {@link #group()} but its argument is used to set the matcher type.
     * @param <T> type of group, to avoid casting
     * @param type type of group, to avoid casting (this type is not checked at runtime).
     * @return a matcher which matches any group
     */
    @Factory
    public static <T extends Group> GroupMatcher<T> group(@SuppressWarnings("unused") Class<T> type)
    {
        return group();
    }

    /**
     * A matcher that matches a group with the given name.
     * @param <T> type of group, to avoid casting
     * @return a matcher which matches any group
     */
    @Factory
    public static <T extends Group> GroupMatcher<T> groupNamed(String name)
    {
        return GroupMatcher.<T>group().withNameOf(name);
    }

    /**
     * Builds a new specialised matcher to match on group name; the returned matcher will only match if all this matcher's
     * non-group-name constraints are matched AND the given matcher matches the group's name. Any existing group name
     * constraint is discarded.
     * @param nameMatcher matcher which must match the group's name in order for this matcher to match
     * @return a matcher will only match if all this matcher's non-group name constraints are matched AND the given
     * matcher matches the group's name
     */
    public GroupMatcher<E> withNameMatching(Matcher<String> nameMatcher)
    {
        return new GroupMatcher<E>(nameMatcher, activeMatcher);
    }

    /**
     * Builds a new specialised matcher to match the given group name exactly; the returned matcher will only match if all
     * this matcher's non-group-name constraints are matched AND the given string is equal to the group's name. Any
     * existing group name constraint is discarded.
     * @param name string which must equal the group's name in order for this matcher to match
     * @return a matcher will only match if all this matcher's non-group name constraints are matched AND the given
     * string is equal to the group's name
     */
    public GroupMatcher<E> withNameOf(String name)
    {
        return new GroupMatcher<E>(is(name), activeMatcher);
    }

    /**
     * Builds a new specialised matcher to match the given active status; the returned matcher will only match if all
     * this matcher's non-group name constraints are matched AND the given status is equal to the group's active status.
     * Any existing group name constraint is discarded.
     * @param active the status must equal the group's status (as per
     * {@link com.atlassian.crowd.model.group.Group#isActive()}) in order for this matcher to match
     * @return a matcher will only match if all this matcher's non-group name constraints are matched AND the given
     * status is equal to the group's active status
     */
    public GroupMatcher<E> withActive(boolean active)
    {
        return new GroupMatcher<E>(nameMatcher, is(active));
    }

    @Override
    protected boolean matchesSafely(E item)
    {
        return item != null
                && (nameMatcher == null || nameMatcher.matches(item.getName()))
                && (activeMatcher == null || activeMatcher.matches(item.isActive()));
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("Group");
        boolean useAnd = false;
        if (nameMatcher != null)
        {
            description.appendText(" with name ").appendDescriptionOf(nameMatcher);
            useAnd = true;
        }
        if (activeMatcher != null)
        {
            if (useAnd)
            {
                description.appendText(" and");
            }
            description.appendText(" with active ").appendDescriptionOf(activeMatcher);
        }
    }
}
