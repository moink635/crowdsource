package com.atlassian.crowd.test.matchers;

import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.user.User;

public class CrowdMatchers
{
    /**
     * @param <T> type of user, to avoid casting
     * @return a matcher which matches any user
     * @see UserMatcher
     */
    public static <T extends User> UserMatcher<T> user()
    {
        return UserMatcher.user();
    }

    /**
     * A matcher that matches any user; synonymous with {@link #user()} but its argument is used to set the matcher type.
     * @param <T> type of user, to avoid casting
     * @param type type of user, to avoid casting (this type is not checked at runtime).
     * @return a matcher which matches any user
     * @see UserMatcher
     */
    public static <T extends User> UserMatcher<T> user(Class<T> type)
    {
        return UserMatcher.user(type);
    }

    /**
     * @param <T> type of user, to avoid casting
     * @return a matcher which matches a user with the given name
     * @see UserMatcher
     */
    public static <T extends User> UserMatcher<T> userNamed(String name)
    {
        return UserMatcher.userNamed(name);
    }

    /**
     * @param <T> type of group, to avoid casting
     * @return a matcher which matches any group
     * @see GroupMatcher
     */
    public static <T extends Group> GroupMatcher<T> group()
    {
        return GroupMatcher.group();
    }

    /**
     * A matcher that matches any group; synonymous with {@link #group()} but its argument is used to set the matcher type.
     * @param <T> type of group, to avoid casting
     * @param type type of group, to avoid casting (this type is not checked at runtime).
     * @return a matcher which matches any group
     * @see GroupMatcher
     */
    public static <T extends Group> GroupMatcher<T> group(Class<T> type)
    {
        return GroupMatcher.group(type);
    }

    /**
     * @param <T> type of group, to avoid casting
     * @return a matcher which matches a group of the given name
     * @see GroupMatcher
     */
    public static <T extends Group> GroupMatcher<T> groupNamed(String name)
    {
        return GroupMatcher.groupNamed(name);
    }

    /**
     * @return a matcher that matches any directory
     */
    public static DirectoryMatcher directory()
    {
        return DirectoryMatcher.directory();
    }
}
