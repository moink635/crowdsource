package com.atlassian.crowd.test.matchers;

import javax.annotation.Nullable;

import com.atlassian.crowd.model.user.User;

import org.hamcrest.Description;
import org.hamcrest.Factory;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static org.hamcrest.Matchers.is;

public class UserMatcher<E extends User> extends TypeSafeMatcher<E>
{
    @Nullable
    private final Matcher<String> nameMatcher;
    @Nullable
    private final Matcher<Boolean> activeMatcher;
    @Nullable
    private final Matcher<String> externalIdMatcher;

    public UserMatcher(@Nullable Matcher<String> nameMatcher, @Nullable Matcher<Boolean> activeMatcher,
                       @Nullable Matcher<String> externalIdMatcher)
    {
        this.nameMatcher = nameMatcher;
        this.activeMatcher = activeMatcher;
        this.externalIdMatcher = externalIdMatcher;
    }

    /**
     * A matcher that matches any user.
     * @param <T> type of user, to avoid casting
     * @return a matcher which matches any user
     */
    @Factory
    public static <T extends User> UserMatcher<T> user()
    {
        return new UserMatcher<T>(null, null, null);
    }

    /**
     * A matcher that matches any user; synonymous with {@link #user()} but its argument is used to set the matcher type.
     * @param <T> type of user, to avoid casting
     * @param type type of user, to avoid casting (this type is not checked at runtime).
     * @return a matcher which matches any user
     */
    @Factory
    public static <T extends User> UserMatcher<T> user(@SuppressWarnings("unused") Class<T> type)
    {
        return user();
    }

    /**
     * A matcher that matches a user with the given name.
     * @param <T> type of user, to avoid casting
     * @return a matcher which matches a user with the given name.
     */
    @Factory
    public static <T extends User> UserMatcher<T> userNamed(String name)
    {
        return UserMatcher.<T>user().withNameOf(name);
    }

    /**
     * Builds a new specialised matcher to match on username; the returned matcher will only match if all this matcher's
     * non-username constraints are matched AND the given matcher matches the user's username. Any existing username
     * constraint is discarded.
     * @param nameMatcher matcher which must match the user's username in order for this matcher to match
     * @return a matcher will only match if all this matcher's non-username constraints are matched AND the given
     * matcher matches the user's username
     */
    public UserMatcher<E> withNameMatching(Matcher<String> nameMatcher)
    {
        return new UserMatcher<E>(nameMatcher, activeMatcher, externalIdMatcher);
    }

    /**
     * Builds a new specialised matcher to match the given username exactly; the returned matcher will only match if all
     * this matcher's non-username constraints are matched AND the given string is equal to the user's username. Any
     * existing username constraint is discarded.
     * @param name string which must equal the user's username in order for this matcher to match
     * @return a matcher will only match if all this matcher's non-username constraints are matched AND the given
     * string is equal to the user's username
     */
    public UserMatcher<E> withNameOf(String name)
    {
        return new UserMatcher<E>(is(name), activeMatcher, externalIdMatcher);
    }

    /**
     * Builds a new specialised matcher to match the given active status; the returned matcher will only match if all
     * this matcher's non-username constraints are matched AND the given status is equal to the user's active status.
     * @param active the status must equal the user's status (as per
     * {@link com.atlassian.crowd.model.user.User#isActive()}) in order for this matcher to match
     * @return a matcher will only match if all this matcher's non-active-status constraints are matched AND the given
     * status is equal to the user's active status
     */
    public UserMatcher<E> withActive(boolean active)
    {
        return new UserMatcher<E>(nameMatcher, is(active), externalIdMatcher);
    }

    /**
     * Builds a new specialised matcher to match on the externalId.
     * @param externalIdMatcher matcher which must match the user's externalId (as per
     * {@link com.atlassian.crowd.model.user.User#getExternalId()} ()}) in order for this matcher to match
     * @return a matcher
     */
    public UserMatcher<E> withExternalIdMatching(Matcher<String> externalIdMatcher)
    {
        return new UserMatcher<E>(nameMatcher, activeMatcher, externalIdMatcher);
    }

    /**
     * Builds a new specialised matcher to match the given externalId.
     * @param externalId the provided externalId must equal the user's externalId (as per
     * {@link com.atlassian.crowd.model.user.User#getExternalId()} ()}) in order for this matcher to match
     * @return a matcher
     */
    public UserMatcher<E> withExternalIdOf(String externalId)
    {
        return withExternalIdMatching(is(externalId));
    }

    @Override
    protected boolean matchesSafely(E item)
    {
        return item != null
                && (nameMatcher == null || nameMatcher.matches(item.getName()))
                && (activeMatcher == null || activeMatcher.matches(item.isActive()))
                && (externalIdMatcher == null || externalIdMatcher.matches(item.getExternalId()));
    }

    @Override
    public void describeTo(Description description)
    {
        description.appendText("User");
        boolean useAnd = false;
        if (nameMatcher != null)
        {
            description.appendText(" with name ").appendDescriptionOf(nameMatcher);
            useAnd = true;
        }
        if (activeMatcher != null)
        {
            if (useAnd)
            {
                description.appendText(" and");
            }
            description.appendText(" with active ").appendDescriptionOf(activeMatcher);
            useAnd = true;
        }
        if (externalIdMatcher != null)
        {
            if (useAnd)
            {
                description.appendText(" and");
            }
            description.appendText(" with externalId ").appendDescriptionOf(externalIdMatcher);
        }
    }
}
