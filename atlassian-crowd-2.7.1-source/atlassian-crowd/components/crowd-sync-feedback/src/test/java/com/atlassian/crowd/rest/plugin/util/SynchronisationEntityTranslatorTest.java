package com.atlassian.crowd.rest.plugin.util;

import java.io.Serializable;

import com.atlassian.crowd.embedded.api.DirectorySynchronisationRoundInformation;
import com.atlassian.sal.api.message.I18nResolver;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class SynchronisationEntityTranslatorTest
{
    @Mock
    private I18nResolver i18nResolver;

    @Before
    public void setUp() throws Exception
    {
        when(i18nResolver.getText(isNull(String.class))).thenThrow(new NullPointerException());
        when(i18nResolver.getText(anyString(), isNull(String.class))).thenThrow(new NullPointerException());
        when(i18nResolver.getText(anyString())).thenReturn("text");
    }

    @Test
    public void testToSynchronisationInformationEntity() throws Exception
    {
        final DirectorySynchronisationRoundInformation dsri = new DirectorySynchronisationRoundInformation(0, 0, "key", ImmutableList.<Serializable>of("param1", "param2"));
        SynchronisationEntityTranslator.toSynchronisationInformationEntity(i18nResolver, dsri);
    }

    @Test
    public void testToSynchronisationInformationEntity_NullStatusKey() throws Exception
    {
        final DirectorySynchronisationRoundInformation dsri = new DirectorySynchronisationRoundInformation(0, 0, null, ImmutableList.<Serializable>of());
        SynchronisationEntityTranslator.toSynchronisationInformationEntity(i18nResolver, dsri);
    }

    @Test
    public void testToSynchronisationInformationEntity_NullStatusParameters() throws Exception
    {
        final DirectorySynchronisationRoundInformation dsri = new DirectorySynchronisationRoundInformation(0, 0, "key", null);
        SynchronisationEntityTranslator.toSynchronisationInformationEntity(i18nResolver, dsri);
    }

    @Test
    public void testToSynchronisationInformationEntity_NullStatusKeyAndParameters() throws Exception
    {
        final DirectorySynchronisationRoundInformation dsri = new DirectorySynchronisationRoundInformation(0, 0, null, null);
        SynchronisationEntityTranslator.toSynchronisationInformationEntity(i18nResolver, dsri);
    }
}
