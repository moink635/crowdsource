package com.atlassian.crowd.rest.plugin.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "directorySynchronisationRoundInformation")
public class DirectorySynchronisationRoundInformationEntity
{
    // e.g. 13 Jan 2011 17:25:25
    @XmlElement
    private final String startTime;

    // e.g. 16 seconds
    @XmlElement
    private final String duration;

    // e.g. Completed successfully
    @XmlElement
    private final String status;

    public DirectorySynchronisationRoundInformationEntity(String startTime, String duration, String status)
    {

        this.startTime = startTime;
        this.duration = duration;
        this.status = status;
    }
}
