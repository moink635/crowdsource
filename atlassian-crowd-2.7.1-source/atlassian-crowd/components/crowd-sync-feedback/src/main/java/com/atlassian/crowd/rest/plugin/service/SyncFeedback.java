package com.atlassian.crowd.rest.plugin.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.atlassian.crowd.embedded.api.DirectorySynchronisationInformation;
import com.atlassian.crowd.exception.DirectoryInstantiationException;
import com.atlassian.crowd.exception.DirectoryNotFoundException;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.crowd.rest.plugin.entity.DirectorySynchronisationInformationEntity;
import com.atlassian.crowd.rest.plugin.util.SynchronisationEntityTranslator;
import com.atlassian.plugins.rest.common.security.jersey.SysadminOnlyResourceFilter;
import com.atlassian.sal.api.message.I18nResolver;

import com.sun.jersey.spi.container.ResourceFilters;

@Path("/")
@ResourceFilters(SysadminOnlyResourceFilter.class)
public class SyncFeedback
{
    private final DirectoryManager directoryManager;
    private final I18nResolver i18nResolver;

    public SyncFeedback(DirectoryManager directoryManager, I18nResolver i18nResolver)
    {
        this.directoryManager = directoryManager;
        this.i18nResolver = i18nResolver;
    }

    @GET
    @Produces("application/json")
    @Path("/directory/{id}")
    public Response getDirectory(@PathParam("id") final Long id) throws DirectoryInstantiationException, DirectoryNotFoundException
    {
        final DirectorySynchronisationInformation syncInfo = directoryManager.getDirectorySynchronisationInformation(id);
        if (syncInfo != null)
        {
            final DirectorySynchronisationInformationEntity syncInfoEntity = SynchronisationEntityTranslator.toSynchronisationStatusEntity(i18nResolver, syncInfo);
            return Response.ok(syncInfoEntity).build();
        }
        else
        {
            return Response.status(Response.Status.NOT_FOUND).entity(new DirectorySynchronisationInformationEntity(null, null)).build();
        }
    }
}
