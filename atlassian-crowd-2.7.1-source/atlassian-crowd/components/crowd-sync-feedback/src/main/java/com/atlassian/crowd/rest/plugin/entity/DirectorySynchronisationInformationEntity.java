package com.atlassian.crowd.rest.plugin.entity;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "directorySynchronisationInformation")
public class DirectorySynchronisationInformationEntity
{
    @XmlElement
    private final DirectorySynchronisationRoundInformationEntity lastRound;

    @XmlElement
    private final DirectorySynchronisationRoundInformationEntity activeRound;

    public DirectorySynchronisationInformationEntity(DirectorySynchronisationRoundInformationEntity lastRound, DirectorySynchronisationRoundInformationEntity activeRound)
    {
        this.lastRound = lastRound;
        this.activeRound = activeRound;
    }
}
