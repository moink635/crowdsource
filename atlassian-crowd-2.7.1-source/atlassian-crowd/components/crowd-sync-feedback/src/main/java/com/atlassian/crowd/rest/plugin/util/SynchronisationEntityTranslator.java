package com.atlassian.crowd.rest.plugin.util;

import com.atlassian.core.util.DateUtils;
import com.atlassian.crowd.embedded.api.DirectorySynchronisationInformation;
import com.atlassian.crowd.embedded.api.DirectorySynchronisationRoundInformation;
import com.atlassian.crowd.rest.plugin.entity.DirectorySynchronisationInformationEntity;
import com.atlassian.crowd.rest.plugin.entity.DirectorySynchronisationRoundInformationEntity;
import com.atlassian.sal.api.message.I18nResolver;

import java.io.Serializable;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;

public class SynchronisationEntityTranslator
{
    private SynchronisationEntityTranslator()
    {
        // Utility class
    }

    public static DirectorySynchronisationInformationEntity toSynchronisationStatusEntity(I18nResolver i18nResolver, DirectorySynchronisationInformation syncInfo)
    {
        final DirectorySynchronisationRoundInformationEntity lastSync = toSynchronisationInformationEntity(
                i18nResolver,
                syncInfo.getLastRound());

        final DirectorySynchronisationRoundInformationEntity activeSync = toSynchronisationInformationEntity(
                i18nResolver,
                    syncInfo.getActiveRound());

        return new DirectorySynchronisationInformationEntity(lastSync, activeSync);
    }

    public static DirectorySynchronisationRoundInformationEntity toSynchronisationInformationEntity(final I18nResolver i18nResolver, final DirectorySynchronisationRoundInformation roundInfo)
    {
        if (roundInfo != null)
        {
            final String duration = DateUtils.getDurationPrettySecondsResolution(roundInfo.getDurationMs() / 1000, new ResourceBundle()
            {

                @Override
                protected Object handleGetObject(String key)
                {
                    return i18nResolver.getText(key);
                }

                @Override
                public Enumeration<String> getKeys()
                {
                    throw new UnsupportedOperationException();
                }
            });
            return new DirectorySynchronisationRoundInformationEntity(
                    i18nResolver.getText("directory.caching.sync.start.time", new Date(roundInfo.getStartTime())),
                    duration,
                    getStatusMessage(i18nResolver, roundInfo));
        }
        else
        {
            return null;
        }
    }

    private static String getStatusMessage(final I18nResolver i18nResolver, final DirectorySynchronisationRoundInformation roundInfo)
    {
        final String statusKey = roundInfo.getStatusKey();
        if (statusKey == null)
        {
            return "";
        }

        final List<Serializable> statusParameters = roundInfo.getStatusParameters();
        if (statusParameters == null)
        {
            return i18nResolver.getText(statusKey);
        }

        final Serializable[] statusParametersArray = statusParameters.toArray(new Serializable[statusParameters.size()]);
        return i18nResolver.getText(statusKey, statusParametersArray);
    }
}
