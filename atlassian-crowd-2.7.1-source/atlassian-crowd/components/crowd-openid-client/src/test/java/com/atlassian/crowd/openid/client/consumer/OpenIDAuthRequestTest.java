package com.atlassian.crowd.openid.client.consumer;

import com.atlassian.crowd.openid.client.consumer.OpenIDAuthRequest;

import com.google.inject.internal.ImmutableList;

import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.Matchers.emptyIterable;

import static org.junit.Assert.assertThat;

public class OpenIDAuthRequestTest
{
    @Test
    public void attributeListsAreNeverNull()
    {
        OpenIDAuthRequest req = new OpenIDAuthRequest(null, null);

        assertThat(req.getRequiredAttributes(), emptyIterable());
        assertThat(req.getOptionalAttributes(), emptyIterable());
    }

    @Test
    public void setAttributeIsReturned()
    {
        OpenIDAuthRequest req = new OpenIDAuthRequest(null, null);

        req.addRequiredAttribute("required");

        assertThat(req.getRequiredAttributes(), Matchers.contains("required"));
    }

    @Test
    public void setAttributeListIsReturned()
    {
        OpenIDAuthRequest req = new OpenIDAuthRequest(null, null);

        req.setRequiredAttributes(ImmutableList.of("one", "two"));

        assertThat(req.getRequiredAttributes(), Matchers.contains("one", "two"));
    }

    @Test
    public void setAttributeListIsCopiedDefensively()
    {
        OpenIDAuthRequest req = new OpenIDAuthRequest(null, null);

        req.setRequiredAttributes(ImmutableList.of("one", "two"));
        req.addRequiredAttribute("three");

        assertThat(req.getRequiredAttributes(), Matchers.contains("one", "two", "three"));
    }

    @Test(expected = NullPointerException.class)
    public void setAttributeListDoesNotAcceptNull()
    {
        OpenIDAuthRequest req = new OpenIDAuthRequest(null, null);

        req.setRequiredAttributes(null);
    }
}
