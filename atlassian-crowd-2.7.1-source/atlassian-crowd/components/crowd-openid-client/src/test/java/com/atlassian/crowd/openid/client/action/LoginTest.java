package com.atlassian.crowd.openid.client.action;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LoginTest
{
    @Test
    public void normalUriIsPassedUnchanged()
    {
        assertEquals("http://www.example.com", Login.normaliseUri("http://www.example.com"));
        assertEquals("http://www.example.com/", Login.normaliseUri("http://www.example.com/"));
        assertEquals("http://www.example.com/path", Login.normaliseUri("http://www.example.com/path"));
        assertEquals("http://www.example.com/path/", Login.normaliseUri("http://www.example.com/path/"));
    }

    @Test
    public void nonAsciiCharactersInPathArePercentEncodedAsUtf8()
    {
        assertEquals("http://example.com/users/john.t%C3%B8stin%C3%B3g%C3%A9", Login.normaliseUri("http://example.com/users/john.tøstinógé"));
    }
}
