package com.atlassian.crowd.openid.client.consumer;

import java.util.Map;
import java.util.List;
import java.util.Collections;
import java.io.Serializable;

/**
 * OpenIDPrincipal represents an authenticated OpenID user.
 *
 * The OpenIDPrincipal must have an OpenID identifier, and
 * may have attributes (retrieved via Attribute Exchange).
 */

public class OpenIDPrincipal
{
    private String identifier;
    private Map attributesMap;

    /**
     * Construct an OpenIDPrincipal from an OpenIDAuthResponse message.
     * @param resp OpenIDAuthResponse message.
     * @throws OpenIDAuthResponseException if the OpenIDAuthResponse is an error message.
     */
    public OpenIDPrincipal(OpenIDAuthResponse resp) throws OpenIDAuthResponseException
    {
        if (!resp.hasError())
        {
            this.identifier = resp.getIdentifier();
            this.attributesMap = resp.getAttributes();
        }
        else
        {
            throw new OpenIDAuthResponseException("Cannot create OpenIDPrincipal as OpenIDAuthResponse is an error message");
        }
    }

    /**
     * @return URI/XRI OpenID.
     */
    public String getIdentifier()
    {
        return identifier;
    }

    /**
     * @param identifier URI/XRI OpenID.
     */
    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    /**
     * Map of attributes (may be null if no attributes present).
     * @return Map< String attribname, List<String> values >
     */
    public Map getAttributesMap()
    {
        return attributesMap;
    }

    /**
     * Map of attributes (may be null if no attributes present).
     * @param attributesMap Map< String attribname, List<String> values >
     */
    public void setAttributesMap(Map attributesMap)
    {
        this.attributesMap = attributesMap;
    }

    /**
     * Returns the value of an attribute.
     * @param key attribute name.
     * @return returns null if attribute does not exist.
     */
    public String getAttribute(String key)
    {
        if (attributesMap != null)
        {
            return (String) attributesMap.get(key);
        }
        return null;
    }
}
