package com.atlassian.crowd.openid.client.filter;

import com.atlassian.crowd.openid.client.action.BaseAction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

/**
 * Checks if web-user is authenticated. If they are not redirect them to the login page.
 *
 * This filter is run once per request.
 */
public class VerifyOpenIDAuthenticationFilter extends OncePerRequestFilter
{
    /**
     * The session key stored as a <code>String<code>, is the requested secure url before redirect to the authentication
     * page.
     */
    public static final String ORIGINAL_URL = VerifyOpenIDAuthenticationFilter.class.getName() + ".ORIGINAL_URL";

    /**
     * create a static reference to the logger.
     */
    private static final Logger logger = LoggerFactory.getLogger(VerifyOpenIDAuthenticationFilter.class);

    public static final String AUTHENTICATED_PRINCIPAL_SESSION_KEY = BaseAction.AUTHENTICATED_PRINCIPAL_SESSION_KEY;

    /**
     * Checks if a principal is authenticated.
     *
     * @param request  The HTTP request.
     * @param response The HTTP response.
     * @return <code>true</code> if and only if the principal is authenticated, otherwise <code>false</code>.
     * @throws java.io.IOException I/O related problems.
     * @throws ServletException    {@link javax.servlet.Servlet} related problems.
     */
    protected boolean isAuthenticated(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        // if the object is in session, assume they have a valid authentication
        return request.getSession().getAttribute(BaseAction.AUTHENTICATED_PRINCIPAL_SESSION_KEY) != null;
    }

    /**
     * Stores the URL the user was originally requesting.
     *
     * @param request The HTTP request.
     * @return The requested URL.
     */
    protected String getOringinalURL(HttpServletRequest request) {

        // get the request URL
        StringBuffer originalURL = request.getRequestURL();

        // store the original path
        boolean foundParameter = false;

        if (request.getParameterMap().size() > 0)
        {
            originalURL.append("?");

            Enumeration params = request.getParameterNames();
            for (; params.hasMoreElements();)
            {
                if (foundParameter == false)
                {
                    foundParameter = true;
                }
                else
                {
                    originalURL.append("&");
                }

                String name = (String) params.nextElement();
                String values[] = request.getParameterValues(name);


                for (int i = 0; i < values.length; i++)
                {
                    originalURL.append(name).append("=").append(values[i]);
                }
            }
        }

        return originalURL.toString();
    }

    /**
     * Verifies the user is authenticated, otherwise redirects them to the login screen
     * with the ORIGINAL_URL appropriately set to the secure page they were trying to
     * access.
     *
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param filterChain FilterChain
     * @throws ServletException if an error occurs while processing filter chain or sending a redirect to the login screen
     * @throws IOException if an error occurs while processing filter chain or sending a redirect to the login screen
     */
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException
    {
        logger.debug("Running VerifyOpenIDAuthenticationFilter");

        // check if they are authenticated
        if (!isAuthenticated(request, response))
        {
            // get the request URL
            String originalURL = getOringinalURL(request);

            logger.debug("Requesting URL is: " + originalURL);
            request.getSession().setAttribute(ORIGINAL_URL, originalURL.toString());

            logger.info("Authentication is not valid, redirecting to: " + request.getContextPath());

            // send them to the login page, they are not authenticated
            response.sendRedirect(request.getContextPath());
        }
        else
        {
            // send the mon their merry way to the page they are trying to access
            request.getSession().removeAttribute(ORIGINAL_URL);

            filterChain.doFilter(request, response);
        }
    }
}