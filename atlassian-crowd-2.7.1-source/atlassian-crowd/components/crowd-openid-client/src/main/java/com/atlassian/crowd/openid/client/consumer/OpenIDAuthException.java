package com.atlassian.crowd.openid.client.consumer;

/**
 * Superclass for all OpenID related exceptions.
 */
public abstract class OpenIDAuthException extends Exception
{
    public OpenIDAuthException()
    {
        super();
    }

    public OpenIDAuthException(String message)
    {
        super(message);
    }

    public OpenIDAuthException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public OpenIDAuthException(Throwable cause)
    {
        super(cause);
    }

    /**
     * Subclasses required to return a niceMessage explaining
     * the exception error. This message could be used to
     * explain the error to the end user.
     *
     * @return niceMessage.
     */
    public abstract String getNiceMessage();
}
