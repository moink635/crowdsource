package com.atlassian.crowd.openid.client.util;

import java.util.List;
import java.util.ArrayList;

public class SREGAttributes
{
    private static final List SREG_ATTRIBS = new ArrayList(); // List<String attributeName>

    // SREG Attributes (http://openid.net/specs/openid-simple-registration-extension-1_0.html)
    private static final String NICKNAME = "nickname";
    private static final String EMAIL = "email";
    private static final String FULLNAME = "fullname";
    private static final String DOB = "dob";
    private static final String GENDER = "gender";
    private static final String POSTCODE = "postcode";
    private static final String COUNTRY = "country";
    private static final String LANGUAGE = "language";
    private static final String TIMEZONE = "timezone";

    static
    {
        SREG_ATTRIBS.add(NICKNAME);
        SREG_ATTRIBS.add(EMAIL);
        SREG_ATTRIBS.add(FULLNAME);
        SREG_ATTRIBS.add(DOB);
        SREG_ATTRIBS.add(GENDER);
        SREG_ATTRIBS.add(POSTCODE);
        SREG_ATTRIBS.add(COUNTRY);
        SREG_ATTRIBS.add(LANGUAGE);
        SREG_ATTRIBS.add(TIMEZONE);
    }

    /**
     * Returns a List<String> of SREG attribute field names.
     * @return List<String>
     */
    public static List getSregAttributes() 
    {
        return SREG_ATTRIBS;
    }
}
