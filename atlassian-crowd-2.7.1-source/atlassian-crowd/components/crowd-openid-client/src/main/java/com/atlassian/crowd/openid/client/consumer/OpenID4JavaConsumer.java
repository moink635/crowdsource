/*
 * Copyright 2006-2007 Sxip Identity Corporation
 */

package com.atlassian.crowd.openid.client.consumer;

import org.openid4java.OpenIDException;
import org.openid4java.consumer.ConsumerManager;
import org.openid4java.consumer.InMemoryConsumerAssociationStore;
import org.openid4java.consumer.VerificationResult;
import org.openid4java.discovery.DiscoveryInformation;
import org.openid4java.discovery.Identifier;
import org.openid4java.message.AuthRequest;
import org.openid4java.message.AuthSuccess;
import org.openid4java.message.MessageException;
import org.openid4java.message.ParameterList;
import org.openid4java.message.ax.FetchRequest;
import org.openid4java.message.sreg.SRegMessage;
import org.openid4java.message.sreg.SRegRequest;
import org.openid4java.message.sreg.SRegResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;


/**
 * OpenID4JavaConsumer is an implementation of CrowdConsumer,
 * which uses the openid4java library to implement the OpenID
 * protocol.
 */
public class OpenID4JavaConsumer implements CrowdConsumer
{
    private static final String OPENID_DISCOVERY_INFORMATION = OpenID4JavaConsumer.class.getName() + ".openid.discovery.info";
    private static final String AX_SCHEMA_PREFIX = "http://schema.openid.net/types/";

    private static final Logger logger = LoggerFactory.getLogger(OpenID4JavaConsumer.class);

    // spring injected
    private ConsumerManager consumerManager;


    /**
     * Make authentication request to the OpenID Provider.
     *
     * This follows the OpenID protocol:
     *
     * <ol>
     * <li> Normalise the supplied OpenID identifier (URI/XRI)
     * <li> Perform discovery (find resultant XRDS or HTML file)
     * <li> Create association with OP if possible (shared secret)
     * <li> Redirect to request authentication from OpenID Provider.
     * </ol>
     *
     * Refer to OpenID specifications for more information regarding
     * each of the individual steps.
     *
     * Ultimately, the control is given to the OpenID Provider.
     */
    public void authenticateRequest(OpenIDAuthRequest openidReq, HttpServletRequest request, HttpServletResponse response)
            throws OpenIDAuthRequestException
    {
        try
        {
            // perform discovery on the user-supplied identifier
            List discoveries = consumerManager.discover(openidReq.getIdentifier());

            // see if request is to be stateless
            // (note: in real OpenID clients, you should ALWAYS associate, ie. use stateful/smart mode)
            if (openidReq.isStateless())
            {
                // this is a cheap hack, we set max assoc attempts to 0 so no new associations are made
                // and we nuke the current association store.
                consumerManager.setMaxAssocAttempts(0);
                consumerManager.setAssociations(new InMemoryConsumerAssociationStore());
            }
            else
            {
                // if we aren't making a stateless request, set the max assoc attempts to the default value
                consumerManager.setMaxAssocAttempts(4);
            }

            // attempt to associate with the OpenID provider
            // and retrieve one service endpoint for authentication
            DiscoveryInformation discovered = consumerManager.associate(discoveries);

            // store the discovery information in the user's session
            request.getSession().setAttribute(OPENID_DISCOVERY_INFORMATION, discovered);

            // return url is the same url of the request (ie. this servlet's URL)
            String returnURL = request.getRequestURL().toString();

            // set the realm to the context path URL
            String realm = getContextPathURL(request);

            // obtain a AuthRequest message to be sent to the OpenID provider
            AuthRequest authReq = consumerManager.authenticate(discovered, returnURL, realm);

            // set the request mode to either check-setup or check immediate
            authReq.setImmediate(openidReq.isImmediate());

            // ATTRIBUTE EXCHANGE IS DISABLED FOR NOW, USE SIMPLE REGISTRATION
//            // attribute exchange (if required) TODO: check OP supports AX before attaching AX fetch request
//            FetchRequest fetch = FetchRequest.createFetchRequest();
//            appendAttributesToFetch(openidReq.getRequiredAttributes(), fetch, true);
//            appendAttributesToFetch(openidReq.getOptionalAttributes(), fetch, false);
//            if (!fetch.getAttributes().isEmpty())
//            {
//                logger.debug("Adding AX attributes to the authentication request");
//                authReq.addExtension(fetch);
//            }

            // SREG
            SRegRequest sregReq = SRegRequest.createFetchRequest();
            appendAttributesToSREGRequest(openidReq.getRequiredAttributes(), sregReq, true);
            appendAttributesToSREGRequest(openidReq.getOptionalAttributes(), sregReq, false);
            if (!sregReq.getAttributes().isEmpty())
            {
                logger.debug("Adding SREG attributes to the authentication request");
                authReq.addExtension(sregReq);
            }

            // send auth request to OP
            if (!discovered.isVersion2())
            {
                // HTTP redirect
                logger.debug("Using HTTP redirect to request authentication from OP");
                response.sendRedirect(authReq.getDestinationUrl(true));
            }
            else
            {
                String destinationUrl = authReq.getDestinationUrl(true);
                if (destinationUrl.length() <= 2048)
                {
                    response.sendRedirect(destinationUrl);
                }
                else
                {
                    // HTML form redirection (allows payloads > 2048 bytes)
                    logger.debug("Using HTML Form redirect to request authentication from OP");
                    request.setAttribute("parameterMap", authReq.getParameterMap());
                    request.setAttribute("destinationUrl", authReq.getDestinationUrl(false));
                    request.getRequestDispatcher("/consumer/formredirection.jsp").forward(request, response);
                }
            }
        }
        catch (OpenIDException e)
        {
            throw new OpenIDAuthRequestException(e);
        }
        catch (ServletException e)
        {
            throw new OpenIDAuthRequestException(e);
        }
        catch (IOException e)
        {
            throw new OpenIDAuthRequestException(e);
        }
    }

    // convenience method for building an SREG request
    private void appendAttributesToSREGRequest(Iterable<String> attributes, SRegRequest request, boolean required)
    {
        // add each attribute to the SREG request
        for (String attrib : attributes)
        {
            request.addAttribute(attrib, required);
        }
    }

    // convenience method for building an Attribute Exchange request
    private void appendAttributesToFetch(Iterable<String> attributes, FetchRequest fetch, boolean required) throws MessageException
    {
        // add each attribute to the fetch request
        for (String attrib : attributes)
        {
            // add a default schema (this will be modified as the Attribute Exchange spec matures)
            fetch.addAttribute(attrib, AX_SCHEMA_PREFIX + attrib, required);
        }
    }

    /**
     * Verifies an authentication response from the OpenID Provider.
     *
     * The basic verification process is delegated to the openid4java
     * library which verifies the nonce, signatures and discovery information.
     *
     * See the OpenID protocol specifications for more information.
     */
    public OpenIDAuthResponse verifyResponse(HttpServletRequest request)
    {
        // build an OpenIDAuthResponse object to pass back
        OpenIDAuthResponse openidResp = new OpenIDAuthResponse();

        try
        {
            // extract the parameters from the authentication response
            // (which comes in as a HTTP request from the OpenID provider)
            ParameterList response = new ParameterList(request.getParameterMap());

            // retrieve the previously stored discovery information
            DiscoveryInformation discovered = (DiscoveryInformation) request.getSession().getAttribute(OPENID_DISCOVERY_INFORMATION);

            // extract the receiving URL from the HTTP request
            StringBuffer receivingURL = request.getRequestURL();
            String queryString = request.getQueryString();
            if (queryString != null && queryString.length() > 0)
            {
                receivingURL.append("?").append(request.getQueryString());
            }

            // verify the response
            VerificationResult verification = consumerManager.verify(receivingURL.toString(), response, discovered);

            // examine the verification result and extract the verified identifier
            Identifier verified = verification.getVerifiedId();
            if (verified != null)
            {
                // populate successful response
                openidResp.setIdentifier(verified.getIdentifier());

                // ATTRIBUTE EXCHANGE IS DISABLED FOR NOW, USE SIMPLE REGISTRATION
//                // retrieve any Attribute Exchange attributes
//                AuthSuccess authSuccess = (AuthSuccess) verification.getAuthResponse();
//                if (authSuccess.hasExtension(AxMessage.OPENID_NS_AX))
//                {
//                    FetchResponse fetchResp = (FetchResponse) authSuccess.getExtension(AxMessage.OPENID_NS_AX);
//                    openidResp.setAttributes(fetchResp.getAttributes());
//                }

                AuthSuccess authSuccess = (AuthSuccess) verification.getAuthResponse();
                if (authSuccess.hasExtension(SRegMessage.OPENID_NS_SREG))
                {
                    SRegResponse sregResponse = (SRegResponse) authSuccess.getExtension(SRegMessage.OPENID_NS_SREG);
                    openidResp.setAttributes(sregResponse.getAttributes());
                }

            }
            else
            {
                // populate error response
                openidResp.setError(new OpenIDAuthFailedException("Verification failed"));
            }
        }
        catch (OpenIDException e)
        {
            // populate error response
            openidResp.setError(new OpenIDAuthResponseException(e));
        }

        return openidResp;
    }

    // convenience method to build the URL to the context path
    private static String getContextPathURL(HttpServletRequest request)
    {
        StringBuffer url = new StringBuffer();
        url.append(request.getScheme());
        url.append("://");
        url.append(request.getServerName());
        if (!isStandardPort(request.getScheme(), request.getServerPort()))
        {
            url.append(":");
            url.append(request.getServerPort());
            url.append(request.getContextPath());
        }
        return url.toString();
    }

    private static boolean isStandardPort(String scheme, int port)
    {
        if (scheme.equalsIgnoreCase("http") && port == 80) return true;
        if (scheme.equalsIgnoreCase("https") && port == 443) return true;
        return false;
    }

    /**
     * The consumerManager is part of the openid4java library.
     *
     * @return injected consumerManager.
     */
    public ConsumerManager getConsumerManager()
    {
        return consumerManager;
    }

    /**
     * The consumerManager is part of the openid4java library.
     *
     * @param consumerManager to inject.
     */
    public void setConsumerManager(ConsumerManager consumerManager)
    {
        this.consumerManager = consumerManager;
    }
}
