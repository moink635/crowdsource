package com.atlassian.crowd.openid.client.consumer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * OpenIDAuthRequest contains all the information required
 * to launch an OpenID authentication request to an
 * OpenID Provider.
 *
 * The identifier and returnURL are required fields.
 *
 * The requiredAttributes and optionalAttributes are
 * optional (may be set to null).
 */
public class OpenIDAuthRequest implements Serializable
{
    private String identifier;
    private String returnURL;
    private boolean immediate = false; // corresponds to the "checkid_immediate" mode of the OpenID protocol
    private boolean stateless = false; // corresponds to the dummy/stateless mode where an association is not made prior to requesting authentication
    private final List<String> requiredAttributes = new ArrayList<String>();
    private final List<String> optionalAttributes = new ArrayList<String>();

    /**
     * Creates an OpenID authentication request for an OpenID.
     *
     * @param identifier the URI or XRI representing a users OpenID.
     * @param returnURL URL OpenID Provider will redirect the authentication response to.
     */
    public OpenIDAuthRequest(String identifier, String returnURL)
    {
        this.identifier = identifier;
        this.returnURL = returnURL;
    }

    /**
     * Adds an attribute to the list of required attributes.
     *
     * @param attrib name of attribute to add.
     */
    public void addRequiredAttribute(String attrib)
    {
        requiredAttributes.add(attrib);
    }

    /**
     * Adds an optional attribute to the list of optional attributes.
     *
     * @param attrib name of attribute to add.
     */
    public void addOptionalAttribute(String attrib)
    {
        optionalAttributes.add(attrib);
    }

    /**
     * @return true if request has any requireAttributes.
     */
    public boolean hasRequiredAttributes()
    {
        return (requiredAttributes != null && !requiredAttributes.isEmpty());
    }

    /**
     * @return true if request has any optionalAttributes.
     */
    public boolean hasOptionalAttributes()
    {
        return (optionalAttributes != null && !optionalAttributes.isEmpty());
    }

    /**
     * @return URI/XRI OpenID this request will verify.
     */
    public String getIdentifier()
    {
        return identifier;
    }

    /**
     * @param identifier URI/XRI OpenID to set for this request.
     */
    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    /**
     * Return the list of required attibutes.
     * @return List<String>: list may be null or zero sized if there are no required attributes.
     */
    public Iterable<String> getRequiredAttributes()
    {
        return requiredAttributes;
    }

    /**
     * Return the list of optional attibutes.
     * @return List<String>: list may be null or zero sized if there are no optional attributes.
     */
    public Iterable<String> getOptionalAttributes()
    {
        return optionalAttributes;
    }

    /**
     * @return URL OpenID Provider will redirect the authentication response to.
     */
    public String getReturnURL()
    {
        return returnURL;
    }

    /**
     * @param returnURL sets the URL the OpenID Provider will redirect the authentication response to.
     */
    public void setReturnURL(String returnURL)
    {
        this.returnURL = returnURL;
    }

    /**
     * @param requiredAttributes required attribute names
     */
    public void setRequiredAttributes(List<String> requiredAttributes)
    {
        this.requiredAttributes.clear();
        this.requiredAttributes.addAll(requiredAttributes);
    }

    /**
     * @param optionalAttributes optional attribute names
     */
    public void setOptionalAttributes(List<String> optionalAttributes)
    {
        this.optionalAttributes.clear();
        this.optionalAttributes.addAll(optionalAttributes);
    }

    public boolean isImmediate()
    {
        return immediate;
    }

    public void setImmediate(boolean immediate)
    {
        this.immediate = immediate;
    }

    public boolean isStateless()
    {
        return stateless;
    }

    public void setStateless(boolean stateless)
    {
        this.stateless = stateless;
    }
}
