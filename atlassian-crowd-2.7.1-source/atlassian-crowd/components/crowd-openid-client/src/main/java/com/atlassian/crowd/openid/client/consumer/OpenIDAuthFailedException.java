package com.atlassian.crowd.openid.client.consumer;

public class OpenIDAuthFailedException extends OpenIDAuthException
{
    public OpenIDAuthFailedException()
    {
        super();
    }

    public OpenIDAuthFailedException(String message)
    {
        super(message);
    }

    public OpenIDAuthFailedException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public OpenIDAuthFailedException(Throwable cause)
    {
        super(cause);
    }

    public String getNiceMessage()
    {
        return "Your OpenID provider was unable to authenticate the OpenID URL. Log in to your OpenID provider and try again.";
    }
}
