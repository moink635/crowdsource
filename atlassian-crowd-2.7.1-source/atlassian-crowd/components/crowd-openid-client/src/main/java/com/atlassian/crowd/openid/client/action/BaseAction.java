package com.atlassian.crowd.openid.client.action;

import com.atlassian.crowd.openid.client.consumer.OpenIDPrincipal;
import com.opensymphony.webwork.ServletActionContext;
import com.opensymphony.xwork.ActionSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpSession;

/**
 * Superclass for WebWork actions.
 *
 * Contains convenience methods for use in actions.
 */

public class BaseAction extends ActionSupport
{
    private static final Logger logger = LoggerFactory.getLogger(BaseAction.class);

    public static final String AUTHENTICATED_PRINCIPAL_SESSION_KEY = BaseAction.class.getName() + ".AUTHENTICATED_PRINCIPAL_SESSION_KEY";

    /**
     * Convenience method to check if action contains errorMessages.
     *
     * @return true if there are more than zero errorMessages.
     */
    public boolean containsErrorMessages()
    {
        return getActionErrors() != null && !getActionErrors().isEmpty();
    }

    /**
     * Convenience method to check if action contains actionMessages.
     *
     * @return true if there are more than zero actionMessages.
     */
    public boolean containsActionMessages()
    {
        return getActionMessages() != null && !getActionMessages().isEmpty();
    }

    /**
     * Convenience method for accessing the HttpSession object.
     *
     * @return HttpSession object.
     */
    public HttpSession getHttpSession()
    {
        return ServletActionContext.getRequest().getSession();
    }

    /**
     * Puts the OpenIDPrincipal into session.
     * This signifies the OpenIDPrincipal has authenticated
     * for the session.
     *
     * @param principal OpenIDPrincipal to put into session.
     */
    public void setOpenIDPrincipal(OpenIDPrincipal principal)
    {
        getHttpSession().setAttribute(AUTHENTICATED_PRINCIPAL_SESSION_KEY, principal);
    }

    /**
     * Retrieve the authenticated OpenIDPrincipal.
     * Returns null if user is not authenticated.
     *
     * @return the OpenIDPrincipal in session.
     */
    public OpenIDPrincipal getOpenIDPrincipal()
    {
        return (OpenIDPrincipal) getHttpSession().getAttribute(AUTHENTICATED_PRINCIPAL_SESSION_KEY);
    }

    /**
     * Convenience method to check if the user is authenticated.
     *
     * @return true if there is an OpenIDPrincipal in session.
     */
    public boolean isAuthenticated()
    {
        return getOpenIDPrincipal() != null;
    }

    /**
     * Returns the "fullname" attribtue from the profile if present.
     * Otherwise returns the text corresponding to "unknown.name.label".
     *
     * @return non-null string.
     */
    public String getPrincipalName ()
    {
        if (isAuthenticated())
        {
            String name = getOpenIDPrincipal().getAttribute("fullname");

            if (name != null)
            {
                return name;
            }

        }

        return getText("unknown.name.label");
    }
}