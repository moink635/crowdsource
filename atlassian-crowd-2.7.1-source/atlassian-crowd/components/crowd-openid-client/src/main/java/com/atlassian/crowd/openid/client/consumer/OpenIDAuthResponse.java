package com.atlassian.crowd.openid.client.consumer;

import com.atlassian.crowd.openid.client.consumer.OpenIDAuthException;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.io.Serializable;

/**
 * OpenIDAuthResponse is a representation of the
 * authentication response sent back from the OpenID
 * Provider.
 *
 * The hasError() method should always be called to
 * check if there was a problem in authenticating
 * the OpenID.
 */

public class OpenIDAuthResponse implements Serializable {
    
    private String identifier;
    private Map attributes; // Map<String attribname, String value>
    private OpenIDAuthException error;

    /**
     * Default constructor.
     */
    public OpenIDAuthResponse()
    {
    }

    /**
     * Create an OpenIDAuthResponse for an OpenID.
     * @param identifier URI/XRI OpenID.
     */
    public OpenIDAuthResponse(String identifier)
    {
        this.identifier = identifier;
    }

    /**
     * @return URI/XRI OpenID.
     */
    public String getIdentifier()
    {
        return identifier;
    }

    /**
     * @param identifier URI/XRI OpenID to set.
     */
    public void setIdentifier(String identifier)
    {
        this.identifier = identifier;
    }

    /**
     * @return Map< String attribname, String value >
     */
    public Map getAttributes()
    {
        return attributes;
    }

    /**
     * Add attribute/value pair to map of attribtues.
     *
     * @param name attribute name.
     * @param value attribute value.
     */
    public void addAttribute(String name, String value)
    {
        if (attributes == null)
        {
            attributes = new HashMap();
        }
        attributes.put(name, value);
    }

    /**
     * @param attributes Map< String attribname, String value >
     */
    public void setAttributes(Map attributes)
    {
        this.attributes = attributes;
    }

    /**
     * If the OpenID provider failed to authenticate the
     * OpenID authentication request, hasError() will return
     * true.
     *
     * If the user was successfully authenticated, hasError()
     * will return false.
     *
     * The error can be retrived be the getError() method.
     *
     * @return false if authentication successful.
     */
    public boolean hasError()
    {
        return error != null;
    }

    /**
     * Retrieves the OpenIDAuthException corresponding to the
     * error occured while authenticating with the OpenID Provider.
     *
     * @return OpenIDAuthException or null if no error.
     */
    public OpenIDAuthException getError()
    {
        return error;
    }

    /**
     * @param error OpenIDAuthException or null if no error.
     */
    public void setError(OpenIDAuthException error)
    {
        this.error = error;
    }
}
