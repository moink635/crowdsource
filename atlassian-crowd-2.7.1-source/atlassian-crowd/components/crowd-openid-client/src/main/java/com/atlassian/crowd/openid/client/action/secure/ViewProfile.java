package com.atlassian.crowd.openid.client.action.secure;

import com.atlassian.crowd.openid.client.action.BaseAction;

/**
 * This action displays the profile of a user
 * that has authenticated. The actual user data is
 * provided as attributes from the OpenID provider.
 */

public class ViewProfile extends BaseAction
{

    public String execute() throws Exception
    {
        return SUCCESS;
    }
    
}
