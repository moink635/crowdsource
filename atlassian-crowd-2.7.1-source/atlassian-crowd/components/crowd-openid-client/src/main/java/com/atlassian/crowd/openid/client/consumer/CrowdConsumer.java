package com.atlassian.crowd.openid.client.consumer;

import com.atlassian.crowd.openid.client.consumer.OpenIDAuthRequestException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * A generic interface for the Relying Party aka
 * OpenID Consumer aka OpenID Client.
 *
 * Implementations are required to be able to
 * authenticate a request and verify the response
 * to the OpenID Provider.
 */
public interface CrowdConsumer
{
    // request attribute keys
    public String OPENID_AUTH_RESPONSE = CrowdConsumer.class.getName() + ".openid.auth.response";
    public String OPENID_AUTH_REQUEST  = CrowdConsumer.class.getName() + ".openid.auth.request";

    // session attribute keys
    public String OPENID_RETURN_TO_URL = CrowdConsumer.class.getName() + ".openid.returnto.url";

    /**
     * Makes a request to an OpenIDProvider to
     * authenticate a particular identifier.
     * The details of the
     *
     * It is responsible for sending a redirect, and
     * thus handing over control to the OpenID Provider.
     *
     * The eventual response (resulting redirect from
     * the OpenIDProvider and any other processing on
     * the server side) should go to the return URL
     * specified in the OpenIDAuthRequest object.
     *
     * @param openidReq contains all OpenID request data.
     * @param request HttpServletRequest object.
     * @param response HttpServletResponse object.
     * @throws OpenIDAuthRequestException if an error occurs while making the request.
     */
    public void authenticateRequest(OpenIDAuthRequest openidReq, HttpServletRequest request, HttpServletResponse response)
            throws OpenIDAuthRequestException;

    /**
     * Verifies an OpenID response from the OpenID server.
     *
     * @param request HttpServletRequest object.
     * @return response object containing principal details or an error message.
     */
    public OpenIDAuthResponse verifyResponse(HttpServletRequest request);
}
