package com.atlassian.crowd.openid.client.servlet;

import com.atlassian.crowd.openid.client.consumer.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.ServletConfig;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * OpenIDClientServlet acts as the proxy to handling OpenID authentication requests
 * and responses.
 *
 * This servlet is called by either:
 * <ol>
 * <li> the application: an authentication request is to be sent to an OpenID Provider
 *      (in which case an OpenIDAuthRequest needs to be in the HttpServletRequest object).
 *      The application is responsible for setting a Return URL on the OpenIDAuthRequest
 *      object so that when the request from OpenID provider comes back to this servlet,
 *      the servlet knows where to pass control back to after processing the response.
 *
 * <li> the OpenID provider: an authentication response is posted to this servlet
 *      via HTTP redirects. This servlet then processes the OpenID authentication response
 *      and creates an OpenIDAuthResponse object representing the response. This object is
 *      put into the request and control is forwarded to the Return URL specified in the
 *      original OpenIDAuthRequest.
 * </ol>
 *
 * <p>
 * This design allows the application code to be separated from OpenID specific code.
 * All the application needs to do is create an OpenIDAuthRequest, put it into the
 * request, and forward control to this servlet.
 * </p>
 *
 * <p>
 * The OpenID provider will take it's time to verify the authentication, eg. allow the
 * user to login, select a profile, etc. Once this is complete, the OpenID server
 * will asynchronously respond to the authentication request and will redirect an
 * authentication response to this servlet.
 * </p>
 *
 * <p>
 * This servlet will then receive the response from the OpenID Provider, build an
 * OpenIDAuthResponse object and forward control back to the application. The application
 * can process the OpenIDAuthResponse and create an OpenIDPrincipal from it if required.
 * </p>
 *
 * <p>
 * The application will need to implement a Servlet/Action/etc to make the
 * authentication request (eg. Login action) and one to process the authentication
 * response (eg. LoginResponse action). It has been done this way for clarity, however,
 * it could easily be refactored into one action.
 * </p>
 */

public class OpenIDClientServlet extends HttpServlet
{
    private static final Logger logger = LoggerFactory.getLogger(OpenIDClientServlet.class);

    // spring injected
    private CrowdConsumer crowdConsumer = null;

    /**
     * Initialise the servlet to inject the CrowdConsumer from Spring.
     *
     * @param config ServletConfig
     * @throws ServletException
     */
    public void init(ServletConfig config) throws ServletException
    {
        super.init(config);

        // get the crowdConsumer bean from spring
        final WebApplicationContext context = WebApplicationContextUtils.getWebApplicationContext(config.getServletContext());
        crowdConsumer = (CrowdConsumer) context.getBean("crowdConsumer");
    }

    /**
     * Forward to doPost().
     *
     * @param httpServletRequest httpServletRequest
     * @param httpServletResponse httpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws ServletException, IOException {
        doPost(httpServletRequest, httpServletResponse);
    }

    /**
     * The servlet is used to both send requests and recieve responses.
     * <ol>
     * <li> If there is an OpenIDAuthRequest in the HttpServletRequest object,
     *      treat the request as an OpenID authentication request and make the request
     *      to the OpenID provider.
     * <li> If there is an "openid.mode" parameter in the request, then treat
     *      this request as an OpenID authentication response from the OpenID Provider.
     * <li> If neither is present, report throw a ServletException as the servlet
     *      isn't being used to request authentication or process authentication
     *      responses.
     * </ol>
     *
     * @param request httpServletRequest
     * @param response httpServletResponse
     * @throws ServletException
     * @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {

        // see if there is an OpenIDAuthRequest in the request object
        OpenIDAuthRequest openidReq = (OpenIDAuthRequest)request.getAttribute(CrowdConsumer.OPENID_AUTH_REQUEST);

        // process the authentication request
        if (openidReq != null)
        {
            logger.debug("Processing authentication request");

            try
            {
                // save returnURL in session (so that we can read it back when responding to an AuthenticationResponse form the OpenID provider)
                request.getSession().setAttribute(CrowdConsumer.OPENID_RETURN_TO_URL, openidReq.getReturnURL());

                // this will redirect control to the OpenID Provider
                crowdConsumer.authenticateRequest(openidReq, request, response);
            }
            catch (OpenIDAuthRequestException e)
            {
                logger.error("Failed to authenticate OpenID request", e);

                // send error response
                OpenIDAuthResponse openidResp = new OpenIDAuthResponse(openidReq.getIdentifier());
                openidResp.setError(e);
                fowardResponse(openidResp, request, response);
            }
        }

        // if there if an openid.mode parameter in the request, the request is an inderect response from the OpenID Provider
        else if (request.getParameter("openid.mode") != null)
        {
            logger.debug("Processing authentication response");

            // make the OpenIDAuthResponse object from the response
            OpenIDAuthResponse openidResp = crowdConsumer.verifyResponse(request);

            // put OpenIDAuthResponse into the request
            request.setAttribute(CrowdConsumer.OPENID_AUTH_RESPONSE, openidResp);

            // forward to the return url specified on the original OpenIDAuthRequest (saved in session)
            String returnURL = (String) request.getSession().getAttribute(CrowdConsumer.OPENID_RETURN_TO_URL);
            if (returnURL == null)
            {
                throw new ServletException("Failed to find " + CrowdConsumer.OPENID_RETURN_TO_URL + " in session");
            }
            request.getRequestDispatcher(returnURL).forward(request, response);
        }

        // this is not an OpenID authentication request or response, throw a 500 error
        else
        {
            throw new ServletException("ERROR: Request to servlet is not for an OpenID authentication request or reponse.");
        }

    }

    // convenience method to put the OpenIDAuthResponse into the request and forward control back to the return URL
    private void fowardResponse(OpenIDAuthResponse openidResp, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        // put OpenIDAuthResponse into the request
        request.setAttribute(CrowdConsumer.OPENID_AUTH_RESPONSE, openidResp);

        // retrieve the returnURL (saved in session)
        String returnURL = (String) request.getSession().getAttribute(CrowdConsumer.OPENID_RETURN_TO_URL);
        if (returnURL == null)
        {
            throw new ServletException("Failed to find " + CrowdConsumer.OPENID_RETURN_TO_URL + " in session");
        }

        // forward to supplied returnURL
        request.getRequestDispatcher(returnURL).forward(request, response);
    }
}
