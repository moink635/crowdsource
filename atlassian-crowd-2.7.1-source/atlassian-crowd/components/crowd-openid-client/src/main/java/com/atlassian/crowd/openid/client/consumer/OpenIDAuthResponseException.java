package com.atlassian.crowd.openid.client.consumer;

public class OpenIDAuthResponseException extends OpenIDAuthException
{
    public OpenIDAuthResponseException()
    {
        super();
    }

    public OpenIDAuthResponseException(String message)
    {
        super(message);
    }

    public OpenIDAuthResponseException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public OpenIDAuthResponseException(Throwable cause)
    {
        super(cause);
    }

    public String getNiceMessage()
    {
        return "Could not recieve authentication response from your OpenID provider. Contact your OpenID server's administrator.";
    }
}
