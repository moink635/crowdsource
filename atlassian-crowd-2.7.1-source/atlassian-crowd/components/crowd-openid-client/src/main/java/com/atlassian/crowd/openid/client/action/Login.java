package com.atlassian.crowd.openid.client.action;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;

import com.atlassian.crowd.openid.client.consumer.CrowdConsumer;
import com.atlassian.crowd.openid.client.consumer.OpenIDAuthRequest;
import com.atlassian.crowd.openid.client.consumer.OpenIDAuthResponse;
import com.atlassian.crowd.openid.client.consumer.OpenIDPrincipal;
import com.atlassian.crowd.openid.client.filter.VerifyOpenIDAuthenticationFilter;
import com.atlassian.crowd.openid.client.util.SREGAttributes;

import com.opensymphony.webwork.ServletActionContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Login extends BaseAction
{
    private static final Logger logger = LoggerFactory.getLogger(Login.class);

    private String openid_identifier;
    private List requiredAttribs = Collections.emptyList();
    private List optionalAttribs = Collections.emptyList();

    private boolean checkImmediate = false;
    private boolean dummyMode = false;


    public String execute() throws Exception
    {
        return doDefault();
    }

    /**
     * Show the login screen. If the user is already logged in, take
     * them to the secure section of the site.
     *
     * @return "input"/"success"
     * @throws Exception
     */
    public String doDefault() throws Exception
    {
        if (isAuthenticated())
        {
            return SUCCESS;
        }
        else
        {
            requiredAttribs = SREGAttributes.getSregAttributes();
            return INPUT;
        }
    }

    /**
     * Request authentication to the OpenID server using the
     * provided OpenID identifier. Dispatch to the OpenID client
     * servlet to make the actual OpenID request. If there is no
     * OpenID identifier supplied, perform the default method.
     *
     * @return "openid"/doDefault()
     * @throws Exception
     */
    public String doLogin() throws Exception
    {
        // if an OpenID identifier (URL/XRI) has been supplied, make the OpenID authentication request
        if (StringUtils.isNotBlank(openid_identifier) && !hasActionErrors())
        {
            // set the openid provider and authentication successful result page
            OpenIDAuthRequest openidReq = new OpenIDAuthRequest(normaliseUri(openid_identifier), "login!loginResponse.action");

            // set which attributes we need from the openid server
            openidReq.setRequiredAttributes(requiredAttribs);
            openidReq.setOptionalAttributes(optionalAttribs);

            // set advanced authentication options
            openidReq.setImmediate(checkImmediate);
            openidReq.setStateless(dummyMode);

            ServletActionContext.getRequest().setAttribute(CrowdConsumer.OPENID_AUTH_REQUEST, openidReq);

            logger.debug("Login.action about to make request to AuthenticationRequestServlet");

            // send to authentication redirect servlet
            return "openid-authrequest";
        }
        else
        {
            return doDefault();
        }
    }

    /**
     * Process a possibly incorrectly-encoded URI provided by a user before
     * using it. Ensure that non-ASCII characters are correctly encoded,
     * presuming UTF-8, before using it as an identifier.
     */
    static String normaliseUri(String identifier)
    {
        try
        {
            return new URI(identifier).toASCIIString();
        }
        catch (URISyntaxException e)
        {
            return identifier;
        }
    }

    /**
     * When an authentication response is received by the
     * OpenID client servlet, this method is called. The
     * response is examined and the user is either redirected
     * to their requesting URL or the default secure area.
     * If there are errors, display the login screen.
     *
     * @return "success" (default secure area)/ "none" (login requesting page) / "input" (via doDefault())
     * @throws Exception
     */
    public String doLoginResponse() throws Exception
    {
        // get the OpenID response from the request (this is set by the OpenIDResponseServlet)
        OpenIDAuthResponse authResponse = (OpenIDAuthResponse) ServletActionContext.getRequest().getAttribute(CrowdConsumer.OPENID_AUTH_RESPONSE);

        // if we can't find the response object, then it's likely someone is calling this action directly
        if (authResponse == null)
        {
            addActionError(CrowdConsumer.OPENID_AUTH_RESPONSE + " not defined in HttpServletRequest");
            return doDefault();
        }

        // if the reponse is an error, return the appropriate error message
        if (authResponse.hasError())
        {
            logger.error("OpenIDAuthResponse reports authentication error", authResponse.getError());

            StringBuffer errorMsg = new StringBuffer("Authentication Failed: ");
            errorMsg.append(authResponse.getError().getNiceMessage());
            addActionError(errorMsg.toString());

            return doDefault();
        }

        // otherwise, set the successfully authenticated OpenID Principal into session
        setOpenIDPrincipal(new OpenIDPrincipal(authResponse));

        // work out where to send successful login
        String requestingPage = (String) getHttpSession().getAttribute(VerifyOpenIDAuthenticationFilter.ORIGINAL_URL);
        if (requestingPage != null)
        {
            // if a requesting page is specified, redirect to that page
            ServletActionContext.getResponse().sendRedirect(requestingPage);
            return NONE;
        }
        else
        {
            // otherwise return default page defined in xwork.xml
            return SUCCESS;
        }
    }

    public String getOpenid_identifier()
    {
        return openid_identifier;
    }

    public void setOpenid_identifier(String openid_identifier)
    {
        this.openid_identifier = openid_identifier.trim();
    }

    public List getRequiredAttribs()
    {
        return requiredAttribs;
    }

    public void setRequiredAttribs(List requiredAttribs)
    {
        this.requiredAttribs = requiredAttribs;
    }

    public List getOptionalAttribs()
    {
        return optionalAttribs;
    }

    public void setOptionalAttribs(List optionalAttribs)
    {
        this.optionalAttribs = optionalAttribs;
    }

    public boolean isCheckImmediate()
    {
        return checkImmediate;
    }

    public void setCheckImmediate(boolean checkImmediate)
    {
        this.checkImmediate = checkImmediate;
    }

    public boolean isDummyMode()
    {
        return dummyMode;
    }

    public void setDummyMode(boolean dummyMode)
    {
        this.dummyMode = dummyMode;
    }
}
