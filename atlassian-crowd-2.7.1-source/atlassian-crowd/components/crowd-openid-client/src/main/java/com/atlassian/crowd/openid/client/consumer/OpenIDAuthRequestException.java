package com.atlassian.crowd.openid.client.consumer;

public class OpenIDAuthRequestException extends OpenIDAuthException
{
    public OpenIDAuthRequestException()
    {
        super();
    }

    public OpenIDAuthRequestException(String message)
    {
        super(message);
    }

    public OpenIDAuthRequestException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public OpenIDAuthRequestException(Throwable cause)
    {
        super(cause); 
    }

    public String getNiceMessage()
    {
        return "Could not make an authentication request to your OpenID provider. Check your OpenID URL points to valid OpenID provider.";
    }
}
