package com.atlassian.crowd.openid.client.action;

/**
 * Action removes invalidates session (thus removing
 * any authenticated OpenIDPrincipal from session).
 */

public class Logoff extends BaseAction
{
    public String execute() throws Exception
    {
        getHttpSession().invalidate();

        return SUCCESS;
    }
}
