@echo off

set OLD_M2_HOME=%M2_HOME%
set M2_HOME=maven

set OLD_PATH=%PATH%
set PATH=%M2_HOME%\bin;%PATH%

call mvn clean install -Dsourcedistribution.skip -Dmaven.test.skip=true %*

set M2_HOME=%OLD_M2_HOME%
set PATH=%OLD_PATH%
